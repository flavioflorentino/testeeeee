//
//  ReceiptParkView.m
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 07/06/16.
//
//

#import "ReceiptParkView.h"

@interface ReceiptParkView (){
    CGSize _intrinsicContentSize;
}
@end

@implementation ReceiptParkView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // 1. load the interface
        [[NSBundle mainBundle] loadNibNamed:@"ReceiptParkView" owner:self options:nil];
        
        // 2. add as subview
        [self addSubview:self.view];
        
        // 3. adjust sizes
        self.bounds = self.view.bounds;
        _intrinsicContentSize = self.bounds.size;
        
        // 4. allow for autolayout
        [self.view setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        // 5. add constraints to span entire view
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];

    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // 1. load the interface
        [[NSBundle mainBundle] loadNibNamed:@"ReceiptParkView" owner:self options:nil];
        
        // 2. add as subview
        [self addSubview:self.view];
        
        // 3. adjust sizes
        self.bounds = self.view.bounds;
        _intrinsicContentSize = self.bounds.size;
        
        // 4. allow for autolayout
        [self.view setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.dateLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
 
        // 5. add constraints to span entire view
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
        
        self.licensePlateButton.layer.borderWidth = 0.5f;
        self.licensePlateButton.layer.borderColor = [[UIColor grayColor]CGColor];
        self.licensePlateButton.layer.cornerRadius = 5.0f;
    }
    return self;
}

- (CGSize)intrinsicContentSize{
    return _intrinsicContentSize;
}

@end
