//
//  ChatLabel.m
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 18/07/16.
//
//

#import "ChatLabel.h"

@implementation ChatLabel

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self){
        [self commonInit];
    }
    return self;
}

- (void) commonInit{
    self.userInteractionEnabled = YES;
    [self addGestureRecognizer:[[UILongPressGestureRecognizer alloc ] initWithTarget:self action:@selector(showMenu:)]];
}

- (void) drawTextInRect:(CGRect)rect{
    UIEdgeInsets insets = {self.radius, self.radius, self.radius, self.radius};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
    //[super drawTextInRect:rect];
    
    UIRectCorner corner = 0;
    if(self.userMessage){
        corner = UIRectCornerTopLeft | UIRectCornerBottomLeft | UIRectCornerBottomRight;
    }else{
        corner = UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight;
    }
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(self.radius, self.radius)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines
{
    UIEdgeInsets insets = {self.radius, self.radius, self.radius, self.radius};
    return [super textRectForBounds:UIEdgeInsetsInsetRect(bounds,insets) limitedToNumberOfLines:numberOfLines];
}

- (CGSize)intrinsicContentSize
{
    CGSize size = [super intrinsicContentSize];
    size.width  += self.radius * 2.0;
    size.height += self.radius * 2.0;
    return size;
}

# pragma mark - Clipboard Copy

- (void) showMenu:(id) sender{
    [self becomeFirstResponder];
    UIMenuController *menu = [UIMenuController sharedMenuController];
    if (!menu.menuVisible){
        [menu setTargetRect:self.bounds inView:self];
        [menu setMenuVisible:YES animated:YES];
    }
}

- (BOOL)canBecomeFirstResponder{
    return true;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    if (action == @selector(copy:)) {
        return YES;
    }
    return NO;
}

- (void) copy: (id) sender
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.text;
}

@end
