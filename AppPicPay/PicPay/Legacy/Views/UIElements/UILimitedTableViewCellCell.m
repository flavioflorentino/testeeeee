//
//  UILimitedTableViewCellCell.m
//  MoBuy 
//
//  Created by Diogo Carneiro on 31/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UILimitedTableViewCellCell.h"

@implementation UILimitedTableViewCellCell

@synthesize limitedTextLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
		self.limitedTextLabel = [[UILabel alloc] init];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews{
	[super layoutSubviews];
	
	//self.textLabel.hidden = YES;
	self.limitedTextLabel.frame = CGRectMake(5, 13, 283-self.detailTextLabel.frame.size.width, 20);
	[self addSubview:self.limitedTextLabel];

}

@end
