//
//  UIPPFollowButton.swift
//  PicPay
//
//  ☝🏽☁️ Created by Luiz Henrique Guimaraes on 05/12/16.
//
//

import UI
import UIKit

@objc enum FollowerButtonAction: Int {
    case undefined = 0
    case follow = 1
    case allow = 2
    case unfollow = 3
    case dismiss = 4
    case cancelRequest = 5
}

final class UIPPFollowButton: UIPPButton {
    
    @objc var action: FollowerButtonAction = FollowerButtonAction.undefined
    var indicator:UIActivityIndicatorView?
    var tempImage:UIImage?
    var showAllowButton:Bool = false
    var currentBorderColor:UIColor?
    var centerLoading = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    @objc func setup(){
        self.indicator = UIActivityIndicatorView()
        self.indicator?.isHidden = true
        self.addSubview(indicator!)
        
        self.addTarget(self, action: #selector(touchDownHandler), for: .touchDown)
        self.addTarget(self, action: #selector(touchUpHandler), for: .touchUpInside)
        self.addTarget(self, action: #selector(touchUpHandler), for: .touchCancel)
    }
    
    /**
      Start the loading animation
     */
    @objc internal func startLoadingAnimation(){
        if var frame = self.imageView?.frame{
            frame.size.width = frame.size.width * 0.7
            frame.size.height = frame.size.height * 0.7
            frame.origin.y = centerLoading ? (self.frame.size.height * 0.5) - (frame.size.height/2) : frame.origin.y + (frame.size.height * 0.3)
            frame.origin.x = centerLoading ? (self.frame.size.width * 0.5) - (frame.size.width/2) : frame.origin.x + (frame.size.width * 0.3)
            self.indicator?.frame = frame
            self.indicator?.isHidden = false
            self.tempImage = self.image(for: .normal)
            self.setImage(nil, for: .normal)
            self.indicator?.startAnimating()
        }
    }
    
    /**
     Stop the loading animation
     */
    @objc internal func stopLoadingAnimation(){
        self.indicator?.stopAnimating()
        self.indicator?.isHidden = true
        if self.tempImage != nil {
            self.setImage(self.tempImage, for: .normal)
        }
    }
    
    /**
     Change button action according to follow status
     */
    @objc internal func changeButtonWithStatus(_ followerStatus:FollowerStatus, consumerStatus:FollowerStatus){
        self.setAttributedTitle(nil ,for:.normal)
        self.isHidden = false
        self.isEnabled = true
        self.setTitle("", for: .normal)
        self.action = FollowerButtonAction.undefined
        stopLoadingAnimation()
        
        if showAllowButton &&  followerStatus == FollowerStatus.waitingAnswer {
            self.normalTitleColor = Palette.ppColorGrayscale000.color
            self.normalBackgrounColor = Palette.ppColorBranding300.color
            self.borderColor = Palette.ppColorBranding300.color
            self.setTitle("Permitir", for: .normal)
            self.action = FollowerButtonAction.allow
        }else{
            switch consumerStatus {
            case .waitingAnswer, .canceled:
                self.normalTitleColor = UIColor.lightGray
                self.normalBackgrounColor = Palette.ppColorGrayscale000.color
                self.borderColor = UIColor.lightGray
                self.setTitle("Solicitado", for: .normal)
                self.action = FollowerButtonAction.cancelRequest
                break
            case .following:
                self.normalTitleColor = Palette.ppColorGrayscale000.color
                self.normalBackgrounColor = UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0)
                self.borderColor = UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0)
                self.setTitle("Seguindo", for: .normal)
                self.action = FollowerButtonAction.unfollow
                break
            case .notFollowing, .deleted:
                self.borderColor = Palette.ppColorBranding300.color
                self.normalTitleColor = Palette.ppColorBranding300.color
                self.normalBackgrounColor = UIColor.clear
                self.setTitle("Seguir", for: .normal)
                self.action = FollowerButtonAction.follow
                break
            case .loading:
                self.setImage(#imageLiteral(resourceName: "ico_friends_empty"), for: .disabled)
                self.isEnabled = false
                self.setTitle("", for: .disabled)
                startLoadingAnimation()
                break
            /*case .Canceled:
                self.enabled = false
                self.setTitle("Cancelado", forState: UIControlState.Disabled)
                break*/
            default:
                self.isHidden = true
            }
        }
    }
    
    // MARK: - User Actions
    
    @objc func touchDownHandler(){
        // work around to adjust borderColor when tap
        self.currentBorderColor = self.borderColor
        self.borderColor = self.highlightedBackgrounColor
    }
    
    @objc func touchUpHandler(){
        // work around to adjust borderColor when tap
        if let color = self.currentBorderColor{
            self.borderColor = color
        }
        
    }

}
