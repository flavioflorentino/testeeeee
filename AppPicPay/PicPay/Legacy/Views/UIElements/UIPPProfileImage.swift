//
//  UIPPProfileImage.m
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 13/09/16.
//
//
import SDWebImage

@objc protocol UIPPProfileImageDelegate: AnyObject {
    @objc optional func profileImageViewDidTap(_ profileImage:UIPPProfileImage, contact:PPContact);
    @objc optional func profileImageViewDidTap(_ profileImage:UIPPProfileImage, store:PPStore);
    @objc optional func profileImageViewDidTap(_ profileImage:UIPPProfileImage, digitalGood:DGItem);
    @objc optional func profileImageViewDidTap(_ profileImage:UIPPProfileImage, producer:ProducerProfileItem);
    @objc optional func profileImageViewDidTap(_ profileImage:UIPPProfileImage, news:News);
    @objc optional func profileImageViewDidTap();
}

final class UIPPProfileImage:UIView {
    
    internal var imageView: UICircularImageView?
    internal var badgeVerified: UIImageView?
    internal var badgePro: UIImageView?
    internal var isHomeNavigationBar: Bool = false
    
    @objc weak var delegate: UIPPProfileImageDelegate?
    
    fileprivate var contact: PPContact?
    fileprivate var store: PPStore?
    fileprivate var digitalGood: DGItem?
    fileprivate var producer: ProducerProfileItem?
    fileprivate var p2m: P2MItem?
    private var p2p: P2PItem?
    var tapGesture: UITapGestureRecognizer?
    fileprivate var news: News?
    internal var origin:String?
    
    var shouldHideBadgePro: Bool = false
    
    // MARK: - Initialize
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    func commonInit() {
        self.isUserInteractionEnabled = true
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTap))
        
        if let tap = tapGesture {
            self.addGestureRecognizer(tap)
        }
        
        // add profile image
        self.imageView = UICircularImageView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
        self.imageView?.contentMode = .scaleToFill
        self.imageView?.borderColor = UIColor(red: 221/255, green: 221/255, blue: 221/255, alpha: 1.0)
        self.imageView?.borderWidth = 1.0 / UIScreen.main.scale
        
        self.addSubview(self.imageView!)
        
        // add verified badge
        if self.frame.size.width <= 53.0 {
            self.badgeVerified = UIImageView(image: #imageLiteral(resourceName: "ico_verified_feed.png")) // ico_verified_feed
        }else{
            self.badgeVerified = UIImageView(image: #imageLiteral(resourceName: "ico_verified_profile.png")) // ico_verifield_profile
        }
        self.badgeVerified?.isHidden = true
        self.badgeVerified?.contentMode = .scaleAspectFit
        self.addSubview(self.badgeVerified!)
        
        // add pro badge
        if self.frame.size.width <= 53.0 {
            self.badgePro = UIImageView(image: #imageLiteral(resourceName: "ico_pro_badge_feed.png"))  // ico_pro_badge_feed
        }else {
            self.badgePro = UIImageView(image: #imageLiteral(resourceName: "ico_pro_badge_profile.png")) // ico_pro_badge_profile
        }
        self.badgePro?.isHidden = true
        self.badgePro?.contentMode = .scaleAspectFit
        self.addSubview(self.badgePro!)
        
        self.backgroundColor = .clear
    }
    
    // MARK: - Layout
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.imageView!.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        let verifiedWidth = self.frame.size.width * CGFloat(0.33);
        let padding = self.frame.size.width * CGFloat(0.04);
        self.badgeVerified!.frame = CGRect(x: self.frame.size.width - verifiedWidth + padding, y: self.frame.size.height - verifiedWidth + padding, width: verifiedWidth, height: verifiedWidth)
        
        if self.frame.size.width <= 40 {
            let proWidth = self.frame.size.width * CGFloat(0.65);
            let proHeihgt = self.frame.size.width * CGFloat(0.33);
            self.badgePro!.frame = CGRect(x: self.frame.size.width/2 - (proWidth/2), y: self.frame.size.height - ((proHeihgt+5)/2) , width: proWidth, height: proHeihgt)
        }else{
            let proWidth = self.frame.size.width * CGFloat(0.54);
            let proHeihgt = self.frame.size.width * CGFloat(0.28);
            self.badgePro!.frame = CGRect(x: self.frame.size.width/2 - (proWidth/2), y: self.frame.size.height - ((proHeihgt+5)/2) , width: proWidth, height: proHeihgt)
        }
    }
    
    // MARK: - Public Methods
    func removeTapGesture() {
        guard let tap = tapGesture else {
            return
        }
        self.removeGestureRecognizer(tap)
    }
    
    /**
     Configure the profile image with contact data
     */
    @objc func setContact(_ contact: PPContact) {
        self.contact = contact
        self.store = nil
        
        if contact.isStore {
            if let img = contact.imgUrl {
                self.imageView?.sd_setImage(
                    with: URL(string:img),
                    placeholderImage: UIImage(named:"avatar_place")
                )
            } else {
                self.imageView?.sd_setImage(
                    with: URL(string:""),
                    placeholderImage: UIImage(named:"avatar_place")
                )
            }
            self.badgeVerified?.isHidden = true;
            self.badgePro?.isHidden = true;
        } else {
            if let img = contact.imgUrl{
                self.imageView?.sd_setImage(
                    with: URL(string:img),
                    placeholderImage: contact.photoPlaceholder()
                )
            } else {
                self.imageView?.sd_setImage(
                    with: URL(string:""),
                    placeholderImage: contact.photoPlaceholder()
                )
            }
            self.badgeVerified?.isHidden = !contact.isVerified;
            self.badgePro?.isHidden = !contact.businessAccount || contact.isVerified;
            
            if shouldHideBadgePro {
                self.badgePro?.isHidden = true
            }
        }
    }
    
    /**
     Configure the profile image with a store data
     */
    @objc func setStore(_ store:PPStore){
        self.contact = nil
        self.store = store
        if let img = store.img_url {
            self.imageView?.sd_setImage(
                with: URL(string:img),
                placeholderImage: PPStore.photoPlaceholder()
            )
        } else {
            self.imageView?.sd_setImage(
                with: URL(string:""),
                placeholderImage: PPStore.photoPlaceholder()
            )
        }
        self.badgeVerified?.isHidden = !store.isVerified;
        self.badgePro?.isHidden = true;
    }
    
    /**
     Configure the profile image with a digital good data
     */
    @objc func setDigitalGood(_ digitalGood:DGItem){
        self.contact = nil
        self.store = nil
        self.digitalGood = digitalGood
        if let img = digitalGood.imgUrl {
            self.imageView?.sd_setImage(
                with: URL(string:img),
                placeholderImage: PPStore.photoPlaceholder()
            )
        } else {
            self.imageView?.sd_setImage(
                with: URL(string:""),
                placeholderImage: PPStore.photoPlaceholder()
            )
        }
        self.badgeVerified?.isHidden = true;
        self.badgePro?.isHidden = true;
    }
    
    /**
     Configure the profile image with a digital good data
     */
    @objc func setP2M(_ p2m:P2MItem) {
        self.contact = nil
        self.store = nil
        self.p2m = p2m
        if p2m.imageUrl != "" {
            self.imageView?.sd_setImage(
                with: URL(string:p2m.imageUrl),
                placeholderImage: PPStore.photoPlaceholder()
            )
        } else {
            self.imageView?.sd_setImage(
                with: URL(string:""),
                placeholderImage: PPStore.photoPlaceholder()
            )
        }
        self.badgeVerified?.isHidden = true;
        self.badgePro?.isHidden = true;
    }

    @objc
    func setP2P(_ p2p: P2PItem) {
        self.p2p = p2p
        contact = nil
        store = nil
        p2m = nil

        badgeVerified?.isHidden = true
        badgePro?.isHidden = true

        imageView?.setImage(url: URL(string: p2p.imageUrl), placeholder: PPStore.photoPlaceholder())
    }
    
    /**
     Configure the profile image with a subscriber data
     */
    @objc func setProducer(_ producer:ProducerProfileItem){
        self.contact = nil
        self.store = nil
        self.digitalGood = nil
        self.producer = producer
        self.imageView?.sd_setImage(
            with: URL(string:producer.profileImageUrl ?? ""),
            placeholderImage: ProducerProfileItem.photoPlaceholder()
        )
        self.badgeVerified?.isHidden = true;
        self.badgePro?.isHidden = true;
    }
    
    
    @objc
    func setNews(_ news:News){
        self.news = news

        imageView?.setImage(
            url: URL(string: news.imageURL),
            placeholder: news.viewed ?
                Assets.Search.newsPlaceholderHasNoBorder.image : Assets.Search.newsPlaceholderHasBorder.image
        )
        
        badgeVerified?.isHidden = true;
        badgePro?.isHidden = true;
    }
    
    func setPixSearchItem(_ item: PixSearchItem) {
        imageView?.setImage(url: URL(string: item.imageUrl), placeholder: Assets.Search.newsPlaceholderHasNoBorder.image)
        
        badgeVerified?.isHidden = true
        badgePro?.isHidden = true
    }

    func setSearchFeatureItem(_ item: SearchFeatureItem) {
        self.imageView?.sd_setImage(with: URL(string: item.imageUrl),
                                    placeholderImage: Assets.Search.newsPlaceholderHasNoBorder.image)

        badgeVerified?.isHidden = true
        badgePro?.isHidden = true
    }

    func set(deeplink: DeeplinkSearchResultItem){
        self.contact = nil
        self.store = nil
        self.digitalGood = nil
        self.badgeVerified?.isHidden = true;
        self.badgePro?.isHidden = true;

        self.imageView?.sd_setImage(with: deeplink.imageURL)
    }

    /**
     Configure the profile image with a search result item
     */
    @objc
    func setAny(_ object: NSObject) {
        if let contact = object as? PPContact {
            setContact(contact)
        }
        
        if let store = object as? PPStore {
            setStore(store)
        }
        
        if let digitalGood = object as? DGItem {
            setDigitalGood(digitalGood)
        }
        
        if let p2m = object as? P2MItem {
            setP2M(p2m)
        }

        if let p2p = object as? P2PItem {
            setP2P(p2p)
        }
        
        if let producer = object as? ProducerProfileItem {
            setProducer(producer)
        }
        
        if let news = object as? News {
            setNews(news)
        }
        
        if let pixSearchItem = object as? PixSearchItem {
            setPixSearchItem(pixSearchItem)
        }

        if let featureSearchItem = object as? SearchFeatureItem {
            setSearchFeatureItem(featureSearchItem)
        }

        if let deeplinkItem = object as? DeeplinkSearchResultItem {
            set(deeplink: deeplinkItem)
        }
    }
    
    func setStudentAccountBadge() {
        guard let pro = badgePro else {
            return
        }
        let badgeStudent = UIImageView(frame: pro.frame)
        badgeStudent.image = Assets.Icons.icoStudentBadgeFeed.image
        addSubview(badgeStudent)
        
        badgePro?.isHidden = true
        badgeStudent.isHidden = false
    }
    
    // MARK: - User Actions
    
    @objc func didTap(_ sender:AnyObject){
        if isHomeNavigationBar {
            self.delegate?.profileImageViewDidTap?()
            return
        }
        if let store = store {
            self.delegate?.profileImageViewDidTap?(self, store: store)
            return
        }
        if let contact = contact {
            self.delegate?.profileImageViewDidTap?(self, contact: contact)
            return
        }
        if let digitalGood = digitalGood {
            self.delegate?.profileImageViewDidTap?(self, digitalGood: digitalGood)
            return
        }
        if let producer = producer {
            self.delegate?.profileImageViewDidTap?(self, producer: producer)
            return
        }
    }
}
