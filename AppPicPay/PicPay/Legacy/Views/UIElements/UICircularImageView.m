//
//  UICircularImageView.m
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 23/06/16.
//
//

#import "UICircularImageView.h"

@implementation UICircularImageView

@synthesize borderColor, borderWidth;

- (void)layoutSubviews{
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.frame.size.width / 2;
    self.layer.borderWidth = self.borderWidth;
    if(self.borderColor){
        self.layer.borderColor = self.borderColor.CGColor;
    }
    self.clipsToBounds = YES;
}

/*
- (void)layoutSubviews{
    [super layoutSubviews];
    
    UIRectCorner corner = UIRectCornerTopRight | UIRectCornerTopLeft | UIRectCornerBottomLeft | UIRectCornerBottomRight;
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(self.frame.size.width / 2,self.frame.size.width / 2)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}*/


@end
