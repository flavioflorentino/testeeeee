//
//  UILimitedTableViewCellCell.h
//  MoBuy
//
//  Created by Diogo Carneiro on 31/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILimitedTableViewCellCell : UITableViewCell

@property (strong, nonatomic) UILabel *limitedTextLabel;

@end
