import UI
import UIKit

final class UIRoundedButton: UIButton {

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.setTitleColor(Palette.ppColorGrayscale000.color, for: .normal)
        self.backgroundColor = UIColor.red
        self.layer.cornerRadius = 22.0
        self.layer.masksToBounds = true;
        self.layer.borderColor = UIColor.red.cgColor
        self.layer.borderWidth = 1;

        clipsToBounds = true
    }
    
}
