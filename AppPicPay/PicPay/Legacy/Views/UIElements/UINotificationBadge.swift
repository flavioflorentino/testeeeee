import UI
import UIKit

final class UINotificationBadge: UILabel {

    @IBInspectable var topInset: CGFloat = 1.0
    @IBInspectable var bottomInset: CGFloat = 1.0
    @IBInspectable var leftInset: CGFloat = 3.0
    @IBInspectable var rightInset: CGFloat = 3.0
    
    override init(frame:CGRect){
        super.init(frame:frame)
        self.font = UIFont.systemFont(ofSize: 11, weight: UIFont.Weight.medium)
        self.textColor = Palette.ppColorGrayscale000.color
        self.backgroundColor = UIColor.red
        self.textAlignment = .center
        self.layer.cornerRadius = 8
        self.layer.masksToBounds = true
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        rect.inset(by: insets)
        var minRect = rect.inset(by: insets)
        if (minRect.size.width < 10.0){
            minRect.size.width = 10.0
        }
        super.draw(minRect)
    }
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let insetRect = bounds.inset(by: UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset))
        var textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
        if (textRect.size.width < 10.0) {
            textRect.size.width = 10.0
        }
        let invertedInsets = UIEdgeInsets(top: -topInset,
                                          left: -leftInset,
                                          bottom: -bottomInset,
                                          right: -rightInset)
        return textRect.inset(by: invertedInsets)
    }
}
