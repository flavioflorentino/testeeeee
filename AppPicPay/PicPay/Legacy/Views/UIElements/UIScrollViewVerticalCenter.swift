//
//  UIScrollViewVerticalCenter.swift
//  PicPay
//
//  ☝🏽☁️ Created by Luiz Henrique Guimarães on 23/06/17.
//
//

import UIKit
import SnapKit

final class UIScrollViewVerticalCenter: UIScrollView {

    @IBOutlet public var contentViewTopConstraint: NSLayoutConstraint?
    @IBOutlet public var contentView: UIView?

    func centerContentView(){
        /// Verificar um forma melhor de remover essa constraint
        if let top = contentViewTopConstraint {
            NSLayoutConstraint.deactivate([top])
        }
        
        guard let content = contentView else {
            return
        }
        content.snp.remakeConstraints { (make) in
            make.top.equalTo( (self.frame.height/2) - (content.frame.height/2) )
        }
        
        setNeedsLayout()
        superview?.layoutIfNeeded()
    }

}
