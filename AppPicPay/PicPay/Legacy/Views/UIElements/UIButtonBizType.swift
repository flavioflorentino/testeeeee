import UI
import UIKit

final class UIButtonBizType: UIButton {
    enum Layout {
        static let spacing: CGFloat = 10
        static let topMargin: CGFloat = 10
        static let leadingTrailingMargin: CGFloat = 5
        static let titleHeight: CGFloat = 50

    }
    var iconBackground: UIImage? {
        didSet { setImage(iconBackground, for: .normal) }
    }
            
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        tintColor = .white
        backgroundColor = Palette.ppColorBranding300.color
        layer.cornerRadius = 20

        titleLabel?.font = UIFont.systemFont(ofSize: 13.0)
        titleLabel?.lineBreakMode = .byWordWrapping
        titleLabel?.textAlignment = .center
        setTitleColor(.white, for: .normal)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
                
        let spacing: CGFloat = Layout.spacing
        let imageSize: CGSize = iconBackground?.size ?? .zero

        let imageFrame = CGRect(x: (frame.width - imageSize.width) / 2, y: (spacing + Layout.topMargin), width: imageSize.width, height: imageSize.height)
        imageView?.frame = imageFrame

        let titleMargin = Layout.leadingTrailingMargin
        let titleHeight = Layout.titleHeight
        let titleFrame = CGRect(x: titleMargin, y: (imageFrame.maxY + spacing), width: (frame.width - (titleMargin * 2)), height: titleHeight)
        titleLabel?.frame = titleFrame
    }
}
