#import <UI/UI.h>
#import "Loader.h"
#import "PicPay-Swift.h"

@implementation Loader

+ (UIView *)getLoadingView:(NSString *)title{
	
	if (!title) {
		title = @"Comunicando com servidor...";
	}
	
	CGFloat heighView = [[UIScreen mainScreen] bounds].size.height;
	CGFloat widthView = [[UIScreen mainScreen] bounds].size.width;
	CGFloat centerView = [[UIScreen mainScreen] bounds].size.width / 2;
	
	UIView *indicatorBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, widthView, heighView)];
    indicatorBg.backgroundColor = [PaletteObjc alwaysBlack];
	indicatorBg.alpha = .8f;
	UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc]
										initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	
	spinner.center = CGPointMake(centerView, (heighView*0.40));
	spinner.color = [PaletteObjc alwaysWhite];
	spinner.hidesWhenStopped = YES;
	[spinner startAnimating];
	
	UILabel *info = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, widthView, 30)];
	info.center = CGPointMake(centerView, (heighView*0.40+30));
	info.backgroundColor = [UIColor clearColor];
	info.textColor = [UIColor colorWithRed:.8 green:.8 blue:.8 alpha:1];
	info.textAlignment = NSTextAlignmentCenter;
	info.text = title;
	
	[indicatorBg addSubview:info];
	[indicatorBg addSubview:spinner];
	
	indicatorBg.hidden = YES;
	
	return indicatorBg;
}

+ (UIView *)getKeyboardLoadingView:(NSString *)title{
	if (!title) {
		title = @"Comunicando com servidor...";
	}
	
	CGFloat heighView = [[UIScreen mainScreen] bounds].size.height;
	CGFloat widthView = [[UIScreen mainScreen] bounds].size.width;
	CGFloat centerView = [[UIScreen mainScreen] bounds].size.width / 2;
	
	UIView *indicatorBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, widthView, heighView)];
	indicatorBg.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
	UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc]
										initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	
	spinner.center = CGPointMake(centerView, (heighView*0.25));
	spinner.color = [UIColor colorWithRed:255 green:255 blue:255 alpha:255];
	spinner.hidesWhenStopped = YES;
	[spinner startAnimating];
	
	UILabel *info = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, widthView, 30)];
	info.center = CGPointMake(centerView, (heighView*0.25+30));
	info.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
	info.textColor = [UIColor colorWithRed:.8 green:.8 blue:.8 alpha:1];
	info.textAlignment = NSTextAlignmentCenter;
	info.text = title;
	
	[indicatorBg addSubview:info];
	[indicatorBg addSubview:spinner];
	
	indicatorBg.hidden = YES;
	
	return indicatorBg;
}

+ (UIView *)getNoTabbarLoadingView:(NSString *)title{
	
	if (!title) {
		title = @"Comunicando com servidor...";
	}
	CGFloat heighView = [[UIScreen mainScreen] bounds].size.height;
	CGFloat widthView = [[UIScreen mainScreen] bounds].size.width;
	CGFloat centerView = [[UIScreen mainScreen] bounds].size.width / 2;

	UIView *indicatorBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, widthView, heighView)];
	indicatorBg.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
	UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc]
										initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	
	spinner.center = CGPointMake(centerView, (heighView*0.40));
	spinner.color = [UIColor colorWithRed:255 green:255 blue:255 alpha:255];
	spinner.hidesWhenStopped = YES;
	[spinner startAnimating];
	
	UILabel *info = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, widthView, 30)];
	info.center = CGPointMake(centerView, (heighView*0.40+30));
	info.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
	info.textColor = [UIColor colorWithRed:.8 green:.8 blue:.8 alpha:1];
	info.textAlignment = NSTextAlignmentCenter;
	info.text = title;
	
	[indicatorBg addSubview:info];
	[indicatorBg addSubview:spinner];
	
	indicatorBg.hidden = YES;
	
	return indicatorBg;
}

+ (UIView *)getMiniLoadingView:(NSString *)title{
	
	if (!title) {
		title = @"Comunicando com servidor...";
	}

	CGFloat heighView = [[UIScreen mainScreen] bounds].size.height;
	CGFloat widthView = [[UIScreen mainScreen] bounds].size.width;
	CGFloat centerView = [[UIScreen mainScreen] bounds].size.width / 2;
	
	UIView *indicatorBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, widthView, heighView)];
	indicatorBg.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
	UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc]
										initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	
	spinner.center = CGPointMake(centerView, (heighView*0.40));
	spinner.color = [UIColor colorWithRed:255 green:255 blue:255 alpha:255];
	spinner.hidesWhenStopped = YES;
	[spinner startAnimating];
	
	UILabel *info = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, widthView, 30)];
	info.center = CGPointMake(centerView, (heighView*0.40+30));
	info.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
	info.textColor = [UIColor colorWithRed:.8 green:.8 blue:.8 alpha:1];
	info.textAlignment = NSTextAlignmentCenter;
	info.text = title;
	
	[indicatorBg addSubview:info];
	[indicatorBg addSubview:spinner];
	
	indicatorBg.hidden = YES;
	
	return indicatorBg;
}

@end
