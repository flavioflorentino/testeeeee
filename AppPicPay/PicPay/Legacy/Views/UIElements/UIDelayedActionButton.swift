//
//  UIDelayedActionButton.swift
//  PicPay
//
//  Created by Vagner Orlandi on 20/07/17.
//

import UIKit
import QuartzCore

@objc protocol UIDelayedActionButtonDelegate:NSObjectProtocol {
    func delayedActionButtomTimeDidFinish(_ sender:UIDelayedActionButton) -> Void
    @objc optional func delayedActionButtomTimeDidStart(_ sender:UIDelayedActionButton) -> Void
    @objc optional func delayedActionButtomWasCancelled(_ button:UIDelayedActionButton) -> Void
}

final class UIDelayedActionButton:UIControl {
    //Estados do Botão
    @objc enum ProgresStatus:Int {
        case notStarted
        case running
        case done
    }
    
    //MARK: Subviews
    lazy private var lblTitle:UILabel = {
        let _lbl = UILabel()
        _lbl.numberOfLines = 0
        _lbl.isUserInteractionEnabled = false
        _lbl.textAlignment = .center
        _lbl.baselineAdjustment = .alignCenters
        _lbl.translatesAutoresizingMaskIntoConstraints = false
        return _lbl
        }()
    
    lazy private var countDownView:UIView = {
        let _view = UIView()
        _view.translatesAutoresizingMaskIntoConstraints = false
        _view.backgroundColor = .green
        _view.isUserInteractionEnabled = false
        return _view
        }()
    
    lazy private var activityView:UIActivityIndicatorView = {
        let _activityView = UIActivityIndicatorView()
        _activityView.isUserInteractionEnabled = false
        _activityView.translatesAutoresizingMaskIntoConstraints = false
        _activityView.style = .gray
        _activityView.hidesWhenStopped = true
        return _activityView
    }()
    
    //MARK: Private properties
    private var progresStatus:ProgresStatus = .notStarted {
        //Controlar a mudança de estado
        didSet {
            switch self.progresStatus {
            case .notStarted:
                self.lblTitle.text = self.notStartedTitle
                self.accessibilityLabel = self.notStartedTitle
            case .running:
                self.lblTitle.text = self.runningTitle
                self.accessibilityLabel = self.runningTitle
            case .done:
                self.lblTitle.text = self.doneTitle
                self.accessibilityLabel = self.doneTitle
            }
        }
    }
    
    
    
    private var delagateCalled:Bool = false
    
    private var notStartedTitle:String = ""
    
    private var runningTitle:String = "Desfazer"
    
    private var doneTitle:String = "Ignorada"
    
    private var maskWidthConstraint:NSLayoutConstraint? = nil
    
    //MARK: Public Properties
    @objc weak var delegate: UIDelayedActionButtonDelegate? = nil
    
    @objc weak var sender: AnyObject? = nil
    
    var shouldAutomaticallyRestart = false
    
    var timerStartColor:UIColor = UIColor(white: 0.95, alpha: 0.3)
    
    var timerEndColor:UIColor = UIColor(white: 0.95, alpha: 1)
    
    //Controla o tempo de cooldown
    @objc var cooldown:TimeInterval = 1 {
        didSet (oldValue) {
            //Apenas valores positivos
            if self.cooldown.isLessThanOrEqualTo(0) {
                self.cooldown = oldValue
            }
        }
    }
    
    //Exibe um UIActivityIndicatorView no lugar do texto
    @objc var isBusy = false {
        didSet {
            if self.isBusy {
                self.isUserInteractionEnabled = false
                self.activityView.startAnimating()
                self.lblTitle.isHidden = true
            } else {
                self.isUserInteractionEnabled = true
                self.activityView.stopAnimating()
                self.lblTitle.isHidden = false
            }
        }
    }
    
    @objc var isRunning:Bool {
        get {
            return self.progresStatus == .running
        }
    }
    
    //MARK: Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        selfInit()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        selfInit()
    }
    
    private func selfInit() {
        self.layer.masksToBounds = true
        
        //To show in UITests as a button
        self.isAccessibilityElement = true
        self.accessibilityTraits = .button
        
        self.addSubview(self.countDownView)
        self.addSubview(self.activityView)
        self.addSubview(self.lblTitle)
        
        self.configureConstraints()
        self.progresStatus = .notStarted
        self.countDownView.backgroundColor = self.timerStartColor
    }
    
    private func configureConstraints() {
        let lblTitleConstraints:[NSLayoutConstraint] = [
            NSLayoutConstraint(item: self.lblTitle, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.lblTitle, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.lblTitle, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0)
        ]
        
        let countDownViewConstraints:[NSLayoutConstraint] = [
            NSLayoutConstraint(item: self.countDownView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.countDownView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.countDownView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.countDownView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 0.0001, constant: 0)
        ]
        
        //Essa constraint é necessário armazenar a referencia, pois será alterada para animação.
        self.maskWidthConstraint = countDownViewConstraints.first(where: {$0.firstAttribute == .width && $0.secondAttribute == .width} )
        
        let activityViewConstraints:[NSLayoutConstraint] = [
            NSLayoutConstraint(item: self.activityView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.activityView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        ]
        
        NSLayoutConstraint.activate(lblTitleConstraints)
        NSLayoutConstraint.activate(countDownViewConstraints)
        NSLayoutConstraint.activate(activityViewConstraints)
    }
    
    //MARK: UIView
    override func layoutSubviews() {
        super.layoutSubviews()
        self.lblTitle.preferredMaxLayoutWidth = self.frame.size.width
        self.layer.cornerRadius = self.frame.size.height / 2
    }
    
    //MARK: UIControl
    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        guard let location = touch?.location(in: self) else {
            return
        }
        if (self.bounds.contains(location)) {
            self.sendAction(#selector(UIDelayedActionButton.buttomPressed), to: self, for: event)
        }
    }
    
    @objc private func buttomPressed() {
        switch self.progresStatus {
        case .notStarted:
            self.start()
            self.delegate?.delayedActionButtomTimeDidStart?(self)
            self.sendActions(for: .valueChanged)
        case .running:
            self.cancel()
            self.delegate?.delayedActionButtomWasCancelled?(self)
            self.sendActions(for: .valueChanged)
        case .done:
            if self.shouldAutomaticallyRestart {
                self.restart()
                self.sendActions(for: .valueChanged)
            }
        }
    }
    
    //MARK: Private Methods
    private func removeAnimations() {
        self.layer.removeAllAnimations()
        self.countDownView.layer.removeAllAnimations()
        self.lblTitle.layer.removeAllAnimations()
    }
    
    private func returnToInitialState() {
        self.progresStatus = .notStarted
        
        if let oldConstraint = self.maskWidthConstraint {
            self.removeConstraint(oldConstraint)
            let newConstraint = NSLayoutConstraint(item: self.countDownView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 0.0001, constant: 0)
            NSLayoutConstraint.activate([newConstraint])
            self.maskWidthConstraint = newConstraint
        }
        
        UIView.animate(withDuration: self.cooldown / 12, delay: 0, options: .beginFromCurrentState, animations: {
            self.countDownView.alpha = 0
            self.lblTitle.alpha = 1
        }, completion: { done in
            self.countDownView.backgroundColor = self.timerStartColor
            self.countDownView.alpha = 1
            self.layoutIfNeeded()
        })
    }
    
    private func start() -> Void {
        if self.progresStatus == .notStarted {
            self.animate()
        }
    }
    
    private func cancel() -> Void {
        if self.progresStatus == .running {
            self.removeAnimations()
            self.returnToInitialState()
        }
    }
    
    private func animate() {
        if let initialConstraint = self.maskWidthConstraint {
            self.progresStatus = .running
            self.layoutIfNeeded()
            self.removeConstraint(initialConstraint)
            let fillMaskConstraint = NSLayoutConstraint(item: self.countDownView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([fillMaskConstraint])
            self.maskWidthConstraint = fillMaskConstraint
            //Fade no titulo
            UIView.animate(withDuration: self.cooldown / 2, animations: {
                self.lblTitle.alpha = 0.5
            })
            //Altera a cor e tamanho da mask view
            UIView.animate(withDuration: self.cooldown, delay: 0, options: .curveLinear, animations: {
                self.countDownView.backgroundColor = self.timerEndColor
                self.layoutIfNeeded()
            }, completion: { done in
                if done {
                    self.lblTitle.alpha = 0
                    UIView.animate(withDuration: self.cooldown / 6, delay: 0, options: .curveEaseInOut, animations: {
                        self.countDownView.alpha = 0
                        self.lblTitle.alpha = 1
                    }, completion: nil)
                    self.progresStatus = .done
                    self.delagateCalled = true
                    self.delegate?.delayedActionButtomTimeDidFinish(self)
                } else {
                    if self.progresStatus == .running {
                        //O botão não foi cancelado, mas a animação foi interrompida
                        self.countDownView.alpha = 0
                        self.lblTitle.alpha = 1
                        self.progresStatus = .done
                        self.delagateCalled = true
                        self.delegate?.delayedActionButtomTimeDidFinish(self)
                    }
                }
            })
        }
    }
    
    //MARK: Public Methods
    @objc func setTitle(_ title:String, forStatus status:ProgresStatus) -> Void {
        switch status {
        case .notStarted:
            self.notStartedTitle = title
        case .running:
            self.runningTitle = title
        case .done:
            self.doneTitle = title
        }
        
        if status == self.progresStatus {
            self.lblTitle.text = title
        }
    }
    
    func getTitle(forStatus status:ProgresStatus) -> String {
        switch status {
        case .notStarted:
            return self.notStartedTitle
        case .running:
            return self.runningTitle
        case .done:
            return self.doneTitle
        }
    }
    
    func setFont(_ font:UIFont) {
        self.lblTitle.font = font
    }
    
    func setActivityViewStyle(_ style:UIActivityIndicatorView.Style) -> Void {
        self.activityView.style = style
    }
    
    func setTextColor(_ color:UIColor) -> Void {
        self.lblTitle.textColor = color
    }
    
    func restart(force: Bool = false) -> Void {
        if force {
            self.progresStatus = .done
        }
        if self.progresStatus == .done {
            self.returnToInitialState()
        }
    }
    
    func forceStart() {
        if self.progresStatus == .notStarted {
            self.start()
            self.delegate?.delayedActionButtomTimeDidStart?(self)
        }
    }
    
    func forceCompletion() {
        if self.progresStatus != .done {
            self.progresStatus = .done
            self.removeAnimations()
            self.countDownView.alpha = 0
            self.lblTitle.alpha = 1
            self.delagateCalled = true
            self.delegate?.delayedActionButtomTimeDidFinish(self)
        }
    }
    
    func forceStatus(_ status:ProgresStatus) {
        self.progresStatus = status
    }
}

