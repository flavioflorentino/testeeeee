#import "WSItemRequest.h"
#import "PicPay-Swift.h"

@import CoreLegacy;
@import FeatureFlag.Swift;

@implementation WSItemRequest

/*!
 * Retorna um item com base no id da loja e do vendedor
 *
 * Callback:
 *
 *   - @b MBItem => Item
 *
 *   - @b NSError => erro
 *
 * @param storeId
 * @param sellerId
 * @param completionHandler callback
 */
+ (void)itemByStoreId:(NSString *)storeId andSellerId:(NSString *)sellerId completionHandler:(void (^)(MBItem *item, NSError *error)) callback;{
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    [params setObject:storeId forKey:@"store_id"];
    if (sellerId != nil) {
        [params setObject:sellerId forKey:@"seller_id"];
    }
    
    [WebServiceInterface dictionaryWithPostVars:params serviceMethodUrl:kWsUrlItemByStoreId completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(nil, err);
            return;
        }
        
        NSError *error;
        MBItem *item = [WSItemRequest itemByJson:(NSDictionary *)json error:&error];
        callback(item, error);
        return;
        
    }];
}

+ (MBItem *)itemByJson:(NSDictionary *)json error:(NSError **)error{
    NSString *data = [json valueForKey:@"data"];
    NSArray *wsError = [json valueForKey:@"Error"];
    
    if (!data) {
        *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
        
        return nil;
    }
    
    return [self itemByJson:[json valueForKey:@"data"]];
}

+ (MBItem * _Nullable)itemByJson:(NSDictionary *)json{
	NSDictionary *data = json;
	
	MBItem *item = [[MBItem alloc] init];
	
	item.wsId = [[[data valueForKey:@"Item"] valueForKey:@"id"] intValue];
	item.name = [[data valueForKey:@"Item"] valueForKey:@"nome"];
	item.imageSrc = [[data valueForKey:@"Item"] valueForKey:@"imagem"];
	item.description = [[data valueForKey:@"Item"] valueForKey:@"descricao"];
	item.freeShipping = [[[data valueForKey:@"Item"] valueForKey:@"frete_gratis"] boolValue];
	
	@try {
		//item.quantityLimit = [[[[data valueForKey:@"Item"] valueForKey:@"parameters"] valueForKey:@"limite_compra"] intValue];
	}
	@catch (NSException *exception) {
		
	}
    
    NSInteger wsId = [[[data valueForKey:@"Seller"] valueForKey:@"id"] integerValue];
	NSString *name = [[data valueForKey:@"Seller"] valueForKey:@"name"];
	NSString *logoUrl = [[data valueForKey:@"Seller"] valueForKey:@"img_url"];
    NSDictionary* dictionary = [data valueForKey:@"Seller"];
    BOOL isVerified = [dictionary valueForKey:@"is_verified"] != [NSNull null] && [dictionary valueForKey:@"is_verified"] != nil ? [[dictionary valueForKey:@"is_verified"] boolValue] : NO;
    
	@try {
		if ((NSNull *)logoUrl == [NSNull null] || logoUrl.length == 0) {
			logoUrl = nil;
		}
	}
	@catch (NSException *exception) {
		logoUrl = nil;
	}
	
	MBPlanType *currentPlanType = [MBPlanType new];
	[currentPlanType setPlanTypeId:[[[data valueForKey:@"Item"] valueForKey:@"plan_type"] characterAtIndex:0]];
	
	
	NSArray *installmentJson = [data valueForKey:@"Installments"];
	NSNumber *interest = [NSNumber numberWithFloat:[[installmentJson valueForKey:@"cc_interests"] floatValue]];
	NSDecimalNumber *minQuotaValue = [NSDecimalNumber decimalNumberWithDecimal:[[installmentJson valueForKey:@"parcela_minima"] decimalValue]];
	int maxQuotaQuantity = [[installmentJson valueForKey:@"max_parcelas"] intValue];
    PPPaymentConfig *paymentConfig = [[PPPaymentConfig alloc] init];
    [paymentConfig updateWithInterest: interest minQuotaValue: minQuotaValue maxQuotaQuantity: maxQuotaQuantity];
	
    Seller *seller = [[Seller alloc] initWithWsId:wsId name:name isVerified:isVerified logoUrl:logoUrl currentPlanType:currentPlanType paymentConfig:paymentConfig];
	
    item.seller = seller;
    
	if ([item.seller.currentPlanType isKindOfPlan:'E']) {
		item.event_date = [NSString stringWithFormat:@"%@",[[[data valueForKey:@"Item"] valueForKey:@"parameters"] valueForKey:@"data_evento"]];
	}else if ([item.seller.currentPlanType isKindOfPlan:'A'] || [item.seller.currentPlanType isKindOfPlan:'C']) {
		
		if ([item.seller.currentPlanType isKindOfPlan:'C']) {
			item.value = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",[[data valueForKey:@"Item"] valueForKey:@"value"]]];
		}
		
		item.consumer_credit_description = [NSString stringWithFormat:@"%@",[[data valueForKey:@"ConsumerCreditInfo"] valueForKey:@"consumer_credit_description_p2p"]];
		
		@try {
			item.consumer_credit_value_seller = [NSDecimalNumber decimalNumberWithDecimal:[[[data valueForKey:@"ConsumerCreditInfo"] valueForKey:@"consumer_credit_value_seller"] decimalValue]];
			
			if ([item.consumer_credit_value_seller isEqualToNumber:[NSDecimalNumber notANumber]]) {
				item.consumer_credit_value_seller = [NSDecimalNumber zero];
			}
		}
		@catch (NSException *exception) {
			item.consumer_credit_value_seller = [NSDecimalNumber zero];
		}
		
		@try {
			PPRewardProgram *rewardProgram = [[PPRewardProgram alloc] init];
			rewardProgram.progress = [[[data valueForKey:@"RewardProgramInfo"] valueForKey:@"progress"] intValue];
			rewardProgram.progressDescription = [[data valueForKey:@"RewardProgramInfo"] valueForKey:@"progress_description"];
			rewardProgram.programDescription = [[data valueForKey:@"RewardProgramInfo"] valueForKey:@"reward_program_description"];
			rewardProgram.progressBarMin = [[data valueForKey:@"RewardProgramInfo"] valueForKey:@"progressbar_min"];
			rewardProgram.progressBarMax = [[data valueForKey:@"RewardProgramInfo"] valueForKey:@"progressbar_max"];
			item.rewardProgram = rewardProgram;
		} @catch (NSException *exception) {
			//item does not have reward program
		}
		
	}
	
	NSMutableArray *combinations = [[NSMutableArray alloc] init];
	NSMutableArray *variantStrings  = [[NSMutableArray alloc] init];
	
	for (NSArray *o in [data valueForKey:@"Combination"]) {
		NSInteger wsId = [[o valueForKey:@"id"] intValue];
		NSString *variantString = [[NSString alloc] init];
		
		for (NSArray *subO in [o valueForKey:@"OptionVariant"]){
			variantString = [variantString stringByAppendingString:[subO valueForKey:@"id"]];
			variantString = [variantString stringByAppendingString:@","];
		}
        
		[combinations addObject: [NSNumber numberWithInteger:wsId]];
		[variantStrings addObject:variantString];
		
	}
	
	item.combinations = [NSDictionary dictionaryWithObjects:combinations forKeys:variantStrings];
	
	if([item.combinations count] == 1 && ([seller.currentPlanType isKindOfPlan:'P'] || [seller.currentPlanType isKindOfPlan:'A'])) {
        if ([combinations objectAtIndex:0] != nil) {
            NSInteger firstCombination = [[combinations objectAtIndex:0] integerValue];
            item.selectedCombination = firstCombination;
        }
	}
	
	@try {
		if ((NSNull *)[data valueForKey:@"Parking"] != [NSNull null]) {
			
			item.isParkingPayment = YES;
			
            // Old parking fields
			item.parkingPlates = [[NSMutableArray alloc] init];
			for (NSString *plate in [[data valueForKey:@"Parking"] valueForKey:@"ConsumerLicensePlate"]) {
				PPParkingCarPlate *newPlate = [[PPParkingCarPlate alloc] init];
				newPlate.plate = plate;
				[item.parkingPlates addObject:newPlate];
			}
			
			item.parkingTimes = [[NSMutableArray alloc] init];
			for (NSArray *parkingTime in [[data valueForKey:@"Parking"] valueForKey:@"ParkingTimes"]) {
				PPParkingTime *newTime = [[PPParkingTime alloc] init];
				newTime.pickerString = [parkingTime objectAtIndex:0];
				newTime.expiryDate = [NSDate dateWithTimeIntervalSince1970:[[parkingTime objectAtIndex:3] integerValue]];
				newTime.expiryDateString = [parkingTime objectAtIndex:4];
				newTime.totalValue = [NSDecimalNumber decimalNumberWithString:[[parkingTime objectAtIndex:2] stringValue]];
				
				[item.parkingTimes addObject:newTime];
			}
			
			item.defaultTime = [[[data valueForKey:@"Parking"] valueForKey:@"defaultTime"] integerValue];
			item.parkingInfo = [[data valueForKey:@"Parking"] valueForKey:@"ParkingInfo"];
            
            // New parking fields
            item.storeId = [[[data valueForKey:@"Parking"] valueForKey:@"Store"] valueForKey:@"id"] ;
            item.storeName = [[[data valueForKey:@"Parking"] valueForKey:@"Store"] valueForKey:@"name"];
            item.isParking = [[[data valueForKey:@"Parking"] valueForKey:@"Store"] valueForKey:@"is_parking"];
            item.sellerId = [[[data valueForKey:@"Parking"] valueForKey:@"Store"] valueForKey:@"seller_id"] ;
		}
	}
	@catch (NSException *exception) {
		item.isParkingPayment = NO;
	}
    
    // -----  ThirdPartyData --------
    @try {
        if ((NSNull *)[data valueForKey:@"ThirdPartyData"] != [NSNull null]) {
            
            //NSString *processor = [[data valueForKey:@"ThirdPartyData"]  valueForKey:@"processor"];
            //if ([processor isEqualToString:@"ZonaAzul"]){
            //    item.thirdPartyProcessor = PPItemThirdPartyProcessorZonaAzul;
            //    item.thirdPartyData = [[PPZonaAzulThirdPartyData alloc] initWithDictionary:[data valueForKey:@"ThirdPartyData"]];
            //}
        }
    }
    @catch (NSException *exception) {
        item.thirdPartyData = nil;
    }
	
	return item;
}

@end
