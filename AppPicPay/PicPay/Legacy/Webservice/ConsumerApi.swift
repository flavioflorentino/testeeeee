import Core
import CoreLegacy
import FeatureFlag
import Foundation
import SwiftyJSON

protocol ConsumerApiProtocol {
    func uploadProfileImage(imageData: Data, uploadProgress: @escaping ((Double) -> Void), completion: @escaping ((PicPayResult<ProfileImageUpload>) -> Void))
    func validatePromotionalCode(_ code: String, completion: @escaping ((PicPayResult<PPPromoCode>) -> Void))
    func updateNotificationToken(completion: ((PicPayResult<BaseApiEmptyResponse>) -> Void)?)
    func fetchBankInfo(completion: @escaping (Result<SettingsBankInfo, ApiError>) -> Void)
}

final class ConsumerApi: BaseApi, ConsumerApiProtocol {
    typealias Dependencies = HasFeatureManager & HasMainQueue
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    // Upload user profile image
    func uploadProfileImage(imageData: Data, uploadProgress: @escaping ((Double) -> Void), completion: @escaping ((PicPayResult<ProfileImageUpload>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let files: [FileUpload] = [
                FileUpload(key: "image", data: imageData, fileName: "picture.jpg", mimeType: "image/jpeg")
            ]
            requestManager.apiUploadCodable(WebServiceInterface.apiEndpoint(kWsUrlUpdateConsumerImage),
                                            method: .post,
                                            files: files,
                                            uploadProgress: uploadProgress,
                                            completion: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func validatePromotionalCode(_ code: String, completion: @escaping ((PicPayResult<PPPromoCode>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlValidateReferralCode, method: .post, parameters: ["referral_code": code])
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func updateNotificationToken(completion: ((PicPayResult<BaseApiEmptyResponse>) -> Void)? = nil) {
        guard let notificationToken = AppParameters.global().pushToken, User.isAuthenticated else {
            return
        }
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let request = requestManager
                .apiRequest(endpoint: kWsUrlAddNotificationToken, method: .put, parameters: ["token": notificationToken])
            if let completionCallback = completion {
                request.responseApiObject(completionHandler: completionCallback)
            }
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func fetchBankInfo(completion: @escaping (Result<SettingsBankInfo, ApiError>) -> Void) {
        let api = Api<SettingsBankInfo>(endpoint: ConsumerApiEndpoints.bankInfo)
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}

enum ConsumerApiEndpoints {
    case bankInfo
}

extension ConsumerApiEndpoints: ApiEndpointExposable {
    var path: String {
        "payment_account/bank-account"
    }
}
