import SwiftyJSON

final class WSReceipt: NSObject {
    static func getReceipt(transactionId: String, type: String, completion callback: @escaping (_ widgets: [ReceiptWidgetItem]?, _ error: Error?) -> Void ) {
        let post = [ "type": type, "transaction_id": transactionId]
        
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlReceipt) { result, _, _, err in
            var widgets: [ReceiptWidgetItem]?
            var error: Error?
            
            if result == nil {
                error = err ?? WebServiceInterface.error(withTitle: "Não foi possível efetuar a operação.", code: 0)
            } else if let wsError = result?["Error"] as? [AnyHashable: Any] {
                let code = wsError["id"] as? Int ?? 0
                error = WebServiceInterface.error(withTitle: wsError["description_pt"] as? String, code: code)
            } else if let data = result?["data"] as? [AnyHashable: Any] {
                widgets = []
                if let list = data["receipt"] as? NSArray {
                    for case let receiptWidget as [AnyHashable: Any] in list {
                        if let receiptItem = WSReceipt.createReceiptWidgetItem(jsonDict: receiptWidget) {
                            widgets?.append(receiptItem)
                        }
                    }
                }
                
                if widgets.isEmpty {
                    widgets = nil
                    error = WebServiceInterface.error(withTitle: "Não foi possível efetuar a operação.", code: 0)
                }
            }
            
            callback(widgets, error)
        }
    }
    
    // swiftlint:disable cyclomatic_complexity
    @objc
    static func createReceiptWidgetItem(jsonDict json: [AnyHashable: Any]) -> ReceiptWidgetItem? {
        if let receiptType = ReceiptWidgetType(rawValue: json["type"] as? String ?? "") {
            switch receiptType {
            case .transaction:
                return TransactionReceiptWidgetItem(jsonDict: json)
            case .transactionStatus:
                return TransactionStatusReceiptWidgetItem(jsonDict: json)
            case .voucher:
                return VoucherReceiptWidgetItem(jsonDict: json)
            case .expiryAt:
                return ExpiryAtReceiptWidgetItem(jsonDict: json)
            case .list:
                return ListReceiptWidgetItem(jsonDict: json)
            case .subscription:
                return SubscriptionReceiptWidgetItem(jsonDict: json)
            case .openUrl:
                return OpenUrlReceiptWidgetItem(jsonDict: json)
            case .buttonAction:
                return ButtonActionReceiptWidgetItem(jsonDict: json)
            case .labelValue:
                return LabelValueReceiptWidgetItem(jsonDict: json)
            case .centralizedLabelValue:
                return LabelValueCentralizedReceiptWidgetItem(jsonDict: json)
            case .webview:
                return HtmlRenderReceiptWidgetItem(jsonDict: json)
            case .mgmOffer, .imageLabelValueWithButton:
                return ImageLabelValueButtonReceiptWidgetItem(jsonDict: json, type: receiptType)
            case .cashback:
                return CashbackReceipWidgetItem(jsonDict: json)
            case .popup:
                return PopupReceiptWidgetItem(jsonDict: json)
            case .incentiveTransaction:
                return IncentiveTransactionWidgetItem(jsonDict: json)
            case .faq:
                return FaqReceiptWidgetItem(jsonDict: json)
            case .pixTransaction:
                return PixTransactionReceiptWidgetItem(jsonDict: json)
            case .unkown:
                return nil
            }
        }
        return nil
    }
}

extension WSReceipt {
    static func createReceiptWidgetItem(_ json: JSON) -> [ReceiptWidgetItem] {
        return WSReceipt.createReceiptWidgetItem(json.array)
    }
    
    static func createReceiptWidgetItem(_ json: [JSON]?) -> [ReceiptWidgetItem] {
        guard let array = json else {
            return []
        }
        return WSReceipt.createReceiptWidgetItem(array)
    }
    
    static func createReceiptWidgetItem(_ array: [JSON]) -> [ReceiptWidgetItem] {
        var widgets: [ReceiptWidgetItem] = []
        
        array.forEach { item in
            let dictionary = item.dictionaryObject ?? [:]
            if let widget = WSReceipt.createReceiptWidgetItem(jsonDict: dictionary) {
                widgets.append(widget)
            }
        }
        
        return widgets
    }
}
