//
//  WSNewFeed.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 10/11/16.
//
//

import UIKit

final class WSNewFeed: NSObject {
    
    /**
     Load a page of feed itens
     @param last the last feed id displayed
     @param visibility
     */
    static func feed(_ feedItemId:String, completion callback:@escaping (_ item:FeedItem?, _ error:Error?) -> Void ){
        WSNewFeed.feeds(nil, visibility: nil, feedItemId: feedItemId, onCacheLoaded: nil) { (items, hasNext, error) in
            if ( error == nil && items != nil ){
                if let feedItem = items?.first{
                    callback(feedItem, nil)
                    return
                }
            }
            
            callback(nil, error)
            return
        }
    }
    
    /**
     Load a page of feed itens
     @param last the last feed id displayed
     @param visibility
     */
    static func feeds(_ last:String, visibility:Int, targetId:String? = nil, targetType:String? = nil, onCacheLoaded: ((_ items:[FeedItem]?, _ hasNext:Bool) -> Void)? , completion callback:@escaping (_ items:[FeedItem]?, _ hasNext:Bool, _ error:Error?) -> Void ){
        WSNewFeed.feeds(last, visibility: visibility, feedItemId:nil, targetId: targetId, targetType: targetType, onCacheLoaded: onCacheLoaded, completion: callback)
    }
    
    /**
     Load a page of profile feed itens
     @param last the last feed id displayed
     @param visibility
     @para feedItemId
     */
    static func profileFeeds(profileId: String, last:String?, visibility:Int?, feedItemId:String?, completion callback:@escaping (_ items:[FeedItem]?, _ hasNext:Bool, _ error:Error?) -> Void ){
        var post:[AnyHashable: Any] = [:]
        if last != nil {
            post["start_id"] = last
        }
        if visibility != nil {
            post["visibility"] = visibility
        }
        post["feed_item_id"] = feedItemId
        post["profile_consumer_id"] = profileId

        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlGetProfileFeed) { (result, response, responseData, err) in
            WSNewFeed.parseRawData(result: result, response: response, responseData: responseData, err: err, completion: callback, last: last, visibility: visibility, feedItemId: feedItemId)
        }
        
    }
    
    /**
     Load a page of feed itens
     @param last the last feed id displayed
     @param visibility 
     @para feedItemId
    */
    static func feeds(_ last:String?, visibility:Int?, feedItemId:String?, targetId:String? = nil, targetType:String? = nil, onCacheLoaded: ((_ items:[FeedItem]?, _ hasNext:Bool) -> Void)?  ,completion callback:@escaping (_ items:[FeedItem]?, _ hasNext:Bool, _ error:Error?) -> Void ){
        
        var cacheKey = ""
        
        var post:[AnyHashable: Any] = [:]
        if last != nil {
            post["start_id"] = last
        }
        if let visibilityNum = visibility {
            post["visibility"] = visibilityNum
            cacheKey = String(describing: visibilityNum)
        }
        
        //if feedItemId != nil {
            post["feed_item_id"] = feedItemId
        //}
        
        //Filtro para o feed
        if let target = targetId {
            post["target_id"] = target
            cacheKey += "#target_id:\(target)"
        }
        
        if let type = targetType {
            post["target_type"] = type
            cacheKey += "#target_type:\(type)"
        }
        
        if (last == nil || last == ""){
            WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlGetFeed, cacheKey: cacheKey, cachecompletionHandler: { (result) in
                WSNewFeed.parseRawData(result: result, response: nil, responseData: nil, err: nil, completion: { (list, next, error) in
                    onCacheLoaded?(list, next)
                }, last: last, visibility: visibility, feedItemId: feedItemId)
            }) { (result, response, responseData, err) in
                WSNewFeed.parseRawData(result: result, response: response, responseData: responseData, err: err, completion: callback, last: last, visibility: visibility, feedItemId: feedItemId)
            }
        }else {
            WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlGetFeed, completionHandler: { (result, response, responseData, err) in
                WSNewFeed.parseRawData(result: result, response: response, responseData: responseData, err: err, completion: callback, last: last, visibility: visibility, feedItemId: feedItemId)
            })
        }
        
    }
    
    static func parseRawData(result: [AnyHashable: Any]?,
                             response: URLResponse?,
                             responseData: Data?,
                             err: Error?,
                             completion callback:@escaping (_ items:[FeedItem]?, _ hasNext:Bool, _ error:Error?) -> Void,
                             last: String?,
                             visibility: Int?,
                             feedItemId: String?) {
        if err != nil{
            callback(nil,false, err)
            return
        }
        
        guard let json = result else{
            let error = WSNewFeed.genericError()
            callback(nil, false, error)
            return
        }
        
        if let data = json["data"] as? [AnyHashable: Any]{
            var items:[FeedItem] = [FeedItem]()
            var hasNext = false
            
            // Prevent request document on load one feed or pagination
            if ( (feedItemId == nil || feedItemId == "") && (last==nil || last=="") ) {
                
                //  Feed Document Request
                if let documentRequest = data["document_request"] as? Bool {
                    if (documentRequest){
                        // mock for document request
                        let i:[AnyHashable: Any] = ["Config":["Title":["Value":"<font color='White'><b>Verificação de documentos</b></font>"],"Text":["Value":"<font color='White'>Para melhorar sua segurança precisamos que nos envie alguns documentos.</font>"],"Image":["Url":"https://s3-sa-east-1.amazonaws.com/picpay/img/novo-feed/avt_pinned_documents.png"]],"Type":"RequestDocument","UIComponent":"RequestDocument"]
                        
                        if let item = WSNewFeed.parseFeedItem(itemData: i) {
                            items.append(item)
                        }
                    }
                }
            }
            
            // feed list
            if let feed = data["feed"] as? [NSObject] {
                for itemData in feed {
                    if let i = itemData as? [AnyHashable: Any], let item = WSNewFeed.parseFeedItem(itemData: i) {
                        items.append(item)
                    }
                }
                
                // check if hasNext page
                if let next = data["next"] as? Bool{
                    hasNext = next
                }
            }
            
            callback(items, hasNext, nil)
            return
        }else{
            if let wsError = json["Error"] as? [AnyHashable: Any] {
                var code = 0
                if let c = wsError["id"] as? Int {
                    code = c
                }
                let error = WebServiceInterface.error( withTitle: wsError["description_pt"] as? String, code: code)
                callback(nil, false, error)
                return
            }
        }
        
        let error = WSNewFeed.genericError()
        callback(nil, false, error)
        return
    }
    
    /**
     Parse the feed item according to item type
    */
    static func parseFeedItem(itemData i:[AnyHashable: Any]) -> FeedItem? {
        var item: FeedItem?
        
        // checks the feed type to perform the parse according to type
        if let _ = i["Type"] as? String {
            item = DefaultFeedItem(jsonDict: i as [NSObject : AnyObject])
        }
        
        return item
    }
    
    
    /**
     Get comment list
     @param currentMembers list of members
     @param last the last comment id displayed
     */
    static func feedComments(_ feedId:String, currentMembers members:[AnyHashable: Any]?, startId:String, completion callback:@escaping (_ comments:[FeedComment]?, _ members:[AnyHashable: Any]?, _ likes:TextCheckFeedWidget?, _ hasNext:Bool, _ error:Error?) -> Void ){
        
        // if is for the first request
        var currentMembers = members
        if currentMembers == nil {
            currentMembers = [AnyHashable: Any]()
        }
        
        //get current members keys
        var membersKey:[String] = [""]
        
        let componentArray = Array(currentMembers!.keys)
        if let keys =  componentArray as? [String]{
            membersKey = keys
        }
        
        let post = ["feed_common_data_id":feedId, "excluded_consumers": membersKey, "start_id": startId] as [String : Any]
        WebServiceInterface.dictionary(withPostVarsSwift: post as [AnyHashable: Any], serviceMethodUrl: kWsUrlGetFeedComment) { (result, response, responseData, err) in
            
            if err != nil{
                callback(nil, nil, nil, false, err as Error?)
                return
            }
            
            guard let json = result else{
                let error = WSNewFeed.genericError()
                callback(nil, nil, nil, false, error)
                return
            }
            
            if let data = json["data"] as? [AnyHashable: Any]{
                var items:[FeedComment] = [FeedComment]()
                
                if let comments = data["comments"] as? [NSObject]{
                    if let members = data["members"] as? [AnyHashable: Any] {
                        
                        //add new members to array
                        for memberKey in members.keys{
                            if let key = memberKey as? String {
                                if let contactData = members[key] as? [AnyHashable: Any] {
                                    let contact = PPContact(profileDictionary: contactData)
                                    currentMembers![key] = contact
                                }
                            }
                        }
                        
                        // Get comments
                        for itemData in comments{
                            if let i = itemData as? [AnyHashable: Any]{
                                if let consumerId = i["ConsumerId"] as? String{
                                    if let contact = currentMembers![consumerId] as? PPContact{
                                        let item:FeedComment? = FeedComment(jsonDict:i as [NSObject : AnyObject], profile:contact)
                                        if item != nil {
                                            items.append(item!)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                }
                
                // Get like data
                var likes:TextCheckFeedWidget? = nil
                if let likeData = data["likes"] as? [AnyHashable: Any]{
                    likes = TextCheckFeedWidget(jsonDict: likeData as [NSObject : AnyObject])
                }
                
                // check if hasNext page
                var hasNext = false
                if let next = data["next"] as? Bool{
                    hasNext = next
                }
                
                callback(items, currentMembers, likes, hasNext, nil)
                return
                
            }else{
                if let wsError = json["Error"] as? [AnyHashable: Any] {
                    var code = 0
                    if let c = wsError["id"] as? Int {
                        code = c
                    }
                    let error = WebServiceInterface.error( withTitle: wsError["description_pt"] as? String, code: code)
                    callback(nil, nil, nil, false, error as Error?)
                    return
                }
            }
            
            let error = WSNewFeed.genericError()
            callback(nil, nil, nil, false, error)
            return
        }
    }
    
    /**
     Create a new comment
     @param message message text
     */
    static func addComment(_ feedId:String, text:String, completion callback:@escaping (_ comment:FeedComment?, _ numComments:String, _ success:Bool, _ error:Error?) -> Void ){
        let post = ["text":text, "feed_common_data_id":feedId]
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlAddFeedComment) { (result, response, responseData, err) in
      
            if err != nil{
                callback(nil, "", false, err as Error?)
                return
            }
            
            guard let json = result else{
                callback(nil, "", false, nil)
                return
            }
            
            if let data = json["data"] as? [AnyHashable: Any]{
                var currentMembers = [AnyHashable: Any]()
                var items:[FeedComment] = [FeedComment]()
                
                if let comments = data["comments"] as? [NSObject]{
                    if let members = data["members"] as? [AnyHashable: Any] {
                        
                        //add new members to array
                        for memberKey in members.keys{
                            if let key = memberKey as? String {
                                if let contactData = members[key] as? [AnyHashable: Any] {
                                    let contact = PPContact(profileDictionary: contactData)
                                    currentMembers[key] = contact
                                }
                            }
                        }
                        
                        // Get comments
                        for itemData in comments{
                            if let i = itemData as? [AnyHashable: Any]{
                                if let consumerId = i["ConsumerId"] as? String{
                                    if let contact = currentMembers[consumerId] as? PPContact{
                                        let item:FeedComment? = FeedComment(jsonDict:i as [NSObject : AnyObject], profile:contact)
                                        if item != nil {
                                            items.append(item!)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                }
                
                var numComments = ""
                if let numString = data["num"] as? String {
                    numComments = numString
                }
                
                // get the new comment
                if items.count > 0 {
                    callback(items[0],  numComments, true, nil)
                    return
                }
            }else{
                if let wsError = json["Error"] as? [AnyHashable: Any] {
                    var code = 0
                    if let c = wsError["id"] as? Int {
                        code = c
                    }
                    let error = WebServiceInterface.error( withTitle: wsError["description_pt"] as? String, code: code)
                    callback(nil, "", false, error as Error?)
                    return
                }
            }
            
            let error = WSNewFeed.genericError()
            callback(nil, "", false, error)
            return
        }
    }
    
    
    /**
     likefeed
     @param message message text
     */
    static func like(_ id:String, like:Bool, completion callback:@escaping (_ likeCheck:TextCheckFeedWidget?, _ success:Bool, _ error:Error?) -> Void ){
        let post = ["feed_common_data_id":id, "like_value": (like ? "1" : "0") ]
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlLikeFeedItem) { (result, response, responseData, err) in
            
            if err != nil{
                callback(nil,false, err as Error?)
                return
            }
            
            guard let json = result else{
                let error = WSNewFeed.genericError()
                callback(nil, false, error)
                return
            }
            
            if let data = json["data"] as? [AnyHashable: Any]{
                
                // Get comments
                //if let likeData = data["like"] as? [NSObject:AnyObject]{
                    let likeCheckText = TextCheckFeedWidget(jsonDict: data as [NSObject : AnyObject])
                    callback(likeCheckText, true, nil)
                    return
                    
                //}
                
            }else{
                if let wsError = json["Error"] as? [AnyHashable: Any] {
                    var code = 0
                    if let c = wsError["id"] as? Int {
                        code = c
                    }
                    let error = WebServiceInterface.error( withTitle: wsError["description_pt"] as? String, code: code)
                    callback(nil, false, error as Error?)
                    return
                }
            }
            
            let error = WSNewFeed.genericError()
            callback(nil, false, error)
            return
        }
    }
    
    /**
     Change the privacy of the feed item
     @param message message text
     */
    static func changeVisibility(_ feedId:String, visibility:FeedItemVisibility, completion callback:@escaping (_ feedItem:DefaultFeedItem?, _ success:Bool, _ error:Error?) -> Void ){
        let post = ["feed_common_data_id":feedId, "privacy": visibility.rawValue]
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlChangeFeedItemPrivacy) { (result, response, responseData, err) in
            
            if err != nil{
                callback(nil,false, err as Error?)
                return
            }
            
            guard let json = result else{
                let error = WSNewFeed.genericError()
                callback(nil, false, error)
                return
            }
            
            if let data = json["data"] as? [AnyHashable: Any]{
                if let feedItem = WSNewFeed.parseFeedItem(itemData: data) as? DefaultFeedItem{
                    callback(feedItem, true, nil)
                    return
                }else{
                    let error = WebServiceInterface.error(withTitle: "Ocorreu um item ao recuperar o item do feed.", code: 0)
                    callback(nil, false, error as Error?)
                    return
                }
            }else{
                if let wsError = json["Error"] as? [AnyHashable: Any] {
                    var code = 0
                    if let c = wsError["id"] as? Int {
                        code = c
                    }
                    let error = WebServiceInterface.error( withTitle: wsError["description_pt"] as? String, code: code)
                    callback(nil, false, error as Error?)
                    return
                }
            }
            
            let error = WSNewFeed.genericError()
            callback(nil, false, error)
            return
        }
    }
    
    static func genericError() -> Error {
        return WebServiceInterface.error(withTitle: "Não foi possível efetuar a operação.", code: 0) as Error
    }
}
