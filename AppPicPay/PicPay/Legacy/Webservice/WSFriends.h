//
//  WSFriends.h
//  PicPay
//
//  Created by Diogo Carneiro on 29/08/13.
//
//

#import <Foundation/Foundation.h>
#import "WebServiceInterface.h"

@interface WSFriends : NSObject

/*!
 * Retorna a lista de emails sugeridos
 *
 * Callback:
 *
 *   - @b NSArray => Lista de emails
 *
 *   - @b NSError => erro
 *
 * @param completionHandler callback
 */
+ (void)emailAddressesSuggestions:(void (^ _Nonnull)(NSArray * _Nullable data, NSError * _Nullable error))callback;

/*!
 * Retorna a lista de sugestão de telefone
 *
 * Callback:
 *
 *   - @b NSArray => Informações de telefones
 *
 *   - @b NSError => erro
 *
 * @param completionHandler callback
 */
+ (void)phoneNumbersSuggestions:(void (^ _Nonnull)(NSArray * _Nullable data, NSError * _Nullable error))callback;

@end
