//
//  WSItemRequest.h
//  MoBuy
//
//  Created by Diogo Carneiro on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLegacy;

@interface WSItemRequest : NSObject

/*!
 * Retorna um item com base no id da loja e do vendedor
 *
 * Callback:
 *
 *   - @b MBItem => Item
 *
 *   - @b NSError => erro
 *
 * @param storeId
 * @param sellerId
 * @param completionHandler callback
 */
+ (void)itemByStoreId:(NSString * _Nonnull)storeId andSellerId:(NSString * _Nonnull)sellerId completionHandler:(void (^ _Nullable)(MBItem * _Nullable item, NSError * _Nullable error)) callback;

+ (MBItem * _Nullable)itemByJson:(NSDictionary * _Nonnull)json;

@end
