//
//  WSParcelamento.h
//  MoBuy
//
//  Created by Joao victor V lana on 14/08/12.
//
//

#import <Foundation/Foundation.h>

@interface WSParcelamento : NSObject

/*!
 * Retorna as taxas de parcelamento para o usuário
 *
 * Callback:
 *
 *   - @b NSA => installments
 *
 *   - @b NSError => erro
 *
 * @param NSString payee id
 * @param completionHandler callback
 */
+ (void) getInstallmentsSellerAccount:(NSString *)sellerId completionHandler:(void (^)(NSArray *installments, NSError *error)) callback;


/*!
 * Retorna as taxas de parcelamento para o usuário
 *
 * Callback:
 *
 *   - @b NSA => installments
 *
 *   - @b NSError => erro
 *
 * @param NSString payee id
 * @param completionHandler callback
 */
+ (void) getInstallmentsBusinessAccount:(NSString *)payeeId completionHandler:(void (^)(NSArray *installments, NSError *error)) callback;

/*!
 * Retorna as possibilidades de recebimento parcelado para usuários PRO
 *
 * Callback:
 *
 *   - @b NSDictionary => dados
 *
 *   - @b NSError => erro
 *
 */
+ (void) getMaximumTaxesFreeInstallments:(void (^)(NSArray *installments, NSString *text, NSError *error)) callback;
+ (void) setMaximumTaxesFreeInstallments:(NSString *)installmentId completionHandler:(void (^)(NSArray *installments, NSString *text, NSError *error)) callback;

@end
