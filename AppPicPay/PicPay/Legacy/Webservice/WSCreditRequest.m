//
//  WSCreditRequest.m
//  MoBuy
//
//  Created by Joao Victor on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WSCreditRequest.h"

@implementation WSCreditRequest

+ (NSDecimalNumber *)userCredit {
	return [NSDecimalNumber decimalNumberWithString:@"0"];
}

@end
