//
//  WSConsumer.h
//  MoBuy
//
//  Created by Diogo Carneiro on 08/08/12.
//
//

#import <Foundation/Foundation.h>
#import "WebServiceInterface.h"

@import CoreLegacy;

@class CardBank;
@class RechargeMethod;
@class Recharge;
@class AvailableWithdrawalModel;

@interface WSConsumer : NSObject

# pragma mark Dados do Usuário

/*!
 * Desativa um consumer.
 * Callback:
 *   - @b NSError => erro
 * @param pin pin
 */
+ (void)deactivateConsumer:(NSString *_Nullable)pin completionHandler:(void (^_Nullable)(BOOL success, NSError * _Nullable error)) callback;

/*!
 * Reativa um consumer
 * Callback:
 *   - @b NSError => erro
 * @param login login
 * @param password password
 * @param completionHandler callback
 */
+ (void)reactivateConsumer:(NSString *_Nullable)login password:(NSString *_Nullable)password completionHandler:(void (^_Nullable)(BOOL success, NSError * _Nullable error)) callback;

/*!
 * Atualiza os dados pessoais.
 *
 * Callback:
 *
 *   - @b BOOL => success
 *
 *   - @b NSError => erro
 *
 * @param email email
 * @param cpf cpf
 * @param birth_date data de nascimento
 * @param completionHandler callback
 */
+ (void)updatePersonalData:(NSString *_Nullable)name cpf:(NSString *_Nullable)cpf birth_date:(NSString *_Nullable)birth_date name_mother:(NSString *_Nullable)name_mother completionHandler:(void (^_Nullable)(BOOL success, NSError * _Nullable error))callback;

/*!
 * create username.
 * Callback:
 *   - @b NSError => erro
 * @param email username
 * @param completionHandler callback
 */
+ (void)createUsername:(NSString *_Nullable)username completionHandler:(void (^_Nullable)(NSError * _Nullable error))callback;

/*!
 * update consumer email.
 * Callback:
 *   - @b BOOL => success
 *   - @b NSError => erro
 * @param email
 * @param pin
 * @param completionHandler callback
 */
+ (void)updateEmail:(NSString *_Nullable)email pin:(NSString *_Nullable)pin completionHandler:(void (^_Nullable)(BOOL success, NSError *_Nullable error))callback;

/*!
 * check consumer email status.
 * Callback:
 *   - @b BOOL => success
 *   - @b NSError => erro
 * @param email
 * @param pin
 * @param completionHandler callback
 */
+ (void)checkEmailStatus:(void (^_Nullable)(BOOL success, NSError *_Nullable error))callback;

/*!
 * resend consumer confimation email.
 * Callback:
 *   - @b BOOL => success
 *   - @b NSError => erro
 * @param email
 * @param pin
 * @param completionHandler callback
 */
+ (void)resendEmailConfirmation:(void (^_Nullable)(BOOL success, NSError *_Nullable error))callback;

/*!
 * Adiciona uma nova conta de banco.
 * Callback:
 *   - @b NSError => erro
 * @param bankAccountDictionary bankAccountDictionary
 * @param origin origin
 * @param completionHandler callback
 */
+ (void)addBankAccount:(NSDictionary *_Nullable)bankAccountDictionary origin:(NSString *_Nullable)origin completionHandler:(void (^_Nullable)(NSError * _Nullable error))callback;

/*!
 * Deleta uma conta de banco.
 * Callback:
 *   - @b NSError => erro
 * @param bankAccount bankAccount
 * @param completionHandler callback
 */
+ (void)deleteBankAccount:(PPBankAccount *_Nullable)bankAccount completionHandler:(void (^_Nullable)(NSError * _Nullable error))callback;

# pragma mark Validação de Sessão

/*!
 * Verifica se a senha do usuário está correta.
 *
 * Callback:
 *
 *   - @b BOOL => verified
 *
 *   - @b NSError => erro
 *
 * @param password password
 * @param completionHandler callback
 */
+ (void)verifyPassword:(NSString * _Nonnull)password completionHandler:(nullable void (^)(BOOL verified,NSError * _Nullable error)) callback;

/*!
 * Efetua o logout do usuáiro.
 * Callback:
 *   - @b NSError => error
 * @param wsId id do usuário
 * @param completionHandler callback
 */
+ (void)logout:(NSString * _Nonnull)wsId token:(NSString * _Nonnull)token completionHandler:(nullable void (^)(NSError * _Nullable error)) callback;

/*!
 * Verifica se o usuário logado possui conta PRO.
 *
 * Callback:
 *
 *   - @b BOOL => isPro
 *
 *   - @b NSError => error
 * @param completionHandler callback
 */
+ (void)amIPro:(nullable void (^)(BOOL isPro, NSError * _Nullable error)) callback;

/*!
 * Transaforma o usário PRO em usuário normal
 *
 * Callback:
 *
 *   - @b BOOL => isPro
 *
 *   - @b NSError => error
 * @param completionHandler callback
 */
+ (void)disableBusinessAccount:(nullable void (^)(NSString * _Nullable message, NSError * _Nullable error)) callback;

# pragma mark Notificações

/*!
 * Retorna a lista de notificações.
 *
 * Callback:
 *
 *   - @b NSArray => lista de notificações
 *
 *   - @b NSError => error
 * @param completionHandler callback
 */
+ (void)getNotifications:(void (^_Nullable)(NSArray * _Nullable notifications, NSArray * _Nullable followNotifications, NSError * _Nullable error)) callback cacheLoaded:(nullable void (^)(NSArray * _Nullable notifications, NSArray * _Nullable followNotifications, NSError * _Nullable error)) cacheLoaded;

/*!
 * Limpa as notificações não lidas.
 * Callback:
 *   - @b NSError => error
 * @param completionHandler callback
 */
+ (void)clearUnseenNotifications:(nullable void (^)(NSError * _Nullable error)) callback;

/*!
 * Marca a notificação como lida.
 * Callback:
 *   - @b NSError => error
 * @param notificationId id da Notificação
 * @param completionHandler callback
 */
+ (void)readNotificationWithId:(NSString * _Nonnull)notificationId completionHandler:(nonnull void (^)(NSError *  _Nullable error)) callback;

/*!
 * envia ack para uma notificação.
 * Callback:
 *   - @b NSError => error
 * @param notificationId id da Notificação
 * @param completionHandler callback
 */
+ (void)ackNotificationWithId:(NSString *_Nullable)notificationId completionHandler:(void (^_Nullable)(NSError * _Nullable error)) callback;

/*!
 * makeWithdrawalForAccount.
 * Callback:
 *   - @b NSError => error
 * @param bankAccount bankAccount
 * @param value value
 * @param pin pin
 * @param completionHandler callback
 */
+ (void)makeWithdrawalForAccount:(PPBankAccount *_Nullable)bankAccount value:(NSDecimalNumber *_Nullable)value pin:(NSString *_Nullable)pin completionHandler:(void (^_Nullable)(PPWithdrawal* _Nullable withdrawal, NSError * _Nullable error)) callback;

/*!
 * getLastWithdrawal.
 *
 * Callback:
 *
 *   - @b PPWithdrawal => PPWithdrawal
 *
 *   - @b NSError => error
 * @param completionHandler callback
 */
+ (void)getLastWithdrawal:(void (^_Nullable)(PPWithdrawal * _Nullable withdrawal, AvailableWithdrawalModel * _Nullable availableWithdrawal, PPBank * _Nullable bankAccount, NSError * _Nullable error)) callback;

# pragma mark Recargar

/*!
 * Retorna a ultima recarga efetuada.
 *
 * Callback:
 *
 *   - @b Recharge => recarga
 *
 *   - @b NSError => error
 * @param completionHandler callback
 */
+ (void)getLastRecharge:(void (^_Nullable)(Recharge * _Nullable recharge, NSMutableArray * _Nullable availableRechargeMethods, NSError * _Nullable error)) callback;

/*!
 * efetua uma recara recarga.
 * Callback:
 *   - @b Recharge => recarga
 *   - @b NSError => error
 * @param rechargeType rechargeType
 * @param value value
 * @param destinationBank destinationBank
 * @param completionHandler callback
 */
+ (void)makeRecharge:(RechargeMethod *_Nullable)rechargeMethod value:(NSDecimalNumber *_Nullable)value securityRequest:(NSString *_Nullable)securityRequest completionHandler:(void (^_Nullable)(Recharge * _Nullable recharge, NSError * _Nullable error)) callback;

typedef OBJC_ENUM(NSInteger, WSConsumerMakeRechargeOpenBank)
{
    WSConsumerMakeRechargeOpenBankToken = 11111703,
    WSConsumerMakeRechargeOpenBankPush = 11111704
};

/*!
 * Cancela uma recarga.
 *
 * Callback:
 *
 *   - @b Recharge => recarga
 *
 *   - @b NSError => error
 * @param rechargeId id da recarga
 * @param completionHandler callback
 */
+ (void)cancelRecharge:(NSString *_Nullable)rechargeId completionHandler:(void (^_Nullable)(Recharge * _Nullable recharge, NSError * _Nullable error)) callback;

/*!
 * Efetua o upload da imagem de recarga
 *
 * Callback:
 *
 *   - @b Recharge > recharge
 *
 *   - @b NSError => erro
 *
 * @param rechargeId recharde id
 * @param image imagem
 * @param completionHandler callback
 */
+ (void)uploadRechargePhoto:(NSString *_Nullable)rechargeId image:(UIImage *_Nullable)image completionHandler:(void (^_Nullable)(Recharge * _Nullable recharge, NSError * _Nullable error))callback;

# pragma mark Validação Telefone

/*!
 * Solicita ao WS que envie um SMS para o telefone com o código de verficiação.
 * Callback:
 *   - @b NSError => erro
 *   - @b NSInteger => smsDelay
 *   - @b BOOL => isCallEnable
 *   - @b NSInteger => callDelay
 * @param ddd ddd
 * @param number numero do telefone
 * @param completionHandler callback
 */
+ (void)sendSmsVerification:(NSString *_Nullable)ddd phone:(NSString *_Nullable)number completionHandler:(void (^_Nullable)(NSInteger smsDelay, BOOL isCallEnable, NSInteger callDelay, NSError * _Nullable error))callback;

/*!
 * Solicita ao WS que ligue para o telefone e informe o código de verificação.
 * Callback:
 *   - @b NSError => erro
 * @param ddd ddd
 * @param number numero do telefone
 * @param completionHandler callback
 */
+ (void)callVerification:(NSString *_Nullable)ddd phone:(NSString *_Nullable)number completionHandler:(void (^_Nullable)(NSInteger smsDelay, BOOL isCallEnable, NSInteger callDelay, NSError * _Nullable error))callback;

/*!
 * Verifica o telefone.
 * Callback:
 *   - @b NSString => verifiedNumber Número verificado
 *   - @b NSError => erro
 * @param code código de validação
 * @param completionHandler callback
 */
+ (void)verifyPhoneNumber:(NSString * _Nullable)code completionHandler:(nullable void (^)(NSString * _Nullable ddd, NSString * _Nullable number, NSError * _Nullable error))callback;

# pragma mark Validation

/*!
 * Check if the Name is avaliable for a new account.
 * Callback:
 *   - @b NSError => error
 * @param name for validation
 * @param completionHandler callback
 */
+ (void)isValidName:(NSString * _Nullable)name completionHandler:(nullable void (^)(NSError * _Nullable error))callback;


/*!
 * Check if the password  is avaliable for a new account.
 * Callback:
 *   - @b NSError => error
 * @param password for validation
 * @param completionHandler callback
 */
+ (void)isValidPass:(NSString * _Nullable)password completionHandler:(nullable void (^)(NSError * _Nullable error))callback;

# pragma mark Account Configs

+ (void)getAccountConfig:(NSString * _Nonnull)name completionHandler:(nullable void (^)(NSArray * _Nullable data, NSError * _Nullable error))callback;
+ (void)setAccountConfig:(NSString * _Nonnull)name params:(NSDictionary * _Nullable)params completionHandler:(nullable void (^)(NSArray * _Nullable data, NSError * _Nullable error))callback;

# pragma mark Outros

/*!
 * Ping no ws
 * Callback:
 *   - @b NSError => erro
 * @param completionHandler callback
 */
+ (void)ping:(nullable void (^)(NSError * _Nullable error))callback;

/*!
 * Envia a classificação do App para WS.
 * Callback:
 *   - @b NSError => erro
 * @param stars numero de estrelas
 * @param completionHandler callback
 */
+ (void)reviewRating:(int)stars completionHandler:(nullable void (^)(NSError * _Nullable error))callback;

//Enum for logMgmRequest
typedef enum PPLogMgmTouchOrigin : NSUInteger
{
    PPLogMgmTouchOriginNone,
    PPLogMgmTouchOriginFeed,
    PPLogMgmTouchOriginContacts,
    PPLogMgmTouchOriginNotification,
    PPLogMgmTouchOriginProfile,
    PPLogMgmTouchOriginSelectContacts,
    PPLogMgmTouchOriginWhatsapp,
    PPLogMgmTouchOriginFacebook,
    PPLogMgmTouchOriginTwitter,
    PPLogMgmTouchOriginCopyLink,
    PPLogMgmTouchOriginSelectShareLink,
    PPLogMgmTouchOriginSidebar
} PPLogMgmTouchOrigin;

/*!
 * logMgmRequest
 * Callback:
 *   - @b NSError => erro
 * @param touchOrigin touchOrigin
 * @param completionHandler callback
 */
+ (void)logMgmRequest:(PPLogMgmTouchOrigin)touchOrigin completionHandler:(void (^_Nullable)(NSError * _Nullable error))callback;

# pragma mark Contatos

/*!
 * Solicita ao WS que convide os contatos informados.
 * Callback:
 *   - @b BOOL => success
 *   - @b NSError => erro
 * @param phones lista de telefones
 * @param emails emails lista de emails
 * @param completionHandler callback
 */
+ (void)inviteContactsWithPhones:(NSArray *_Nullable)phones emails:(NSArray *_Nullable)emails completionHandler:(void (^_Nullable)(BOOL success, NSError * _Nullable error))callback;

/*!
 * markNotificationsAsRead.
 * Callback:
 *   - @b NSError => error
 * @param completionHandler callback
 */
+ (void)markNotificationsAsRead:(void (^_Nullable)(NSError * _Nullable error)) callback;

# pragma mark Não Utilizados

/*!
 * Delete a license Plate
 *
 * Callback:
 *
 *   - @b BOOL => success
 *
 *   - @b NSError => erro
 *
 * @param licensePlate license Plate
 * @param completionHandler callback
 */
+ (void) removeLicensePlate:(NSString * _Nullable)licensePlate completionHandler:(void (^_Nonnull)(BOOL success, NSError * _Nullable error)) callback;


/**
 Send notificatin for consumer change to picpay pro

 @param payload payload of error
 @param completion completion handle
 */
+ (void)sendNotificationPicPayProWith:(NSDictionary * _Nonnull)payload completion:(void (^_Nonnull)(BOOL success, NSError * _Nullable error)) completion;

@end
