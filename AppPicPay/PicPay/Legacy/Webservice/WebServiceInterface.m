#import "WebServiceInterface.h"
#import "WSConnection.h"
#import <sys/utsname.h>

#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

#import <AdSupport/AdSupport.h>
#import "PicPay-Swift.h"
#import "NSURLRequest+cURL.h"
@import Core;
@import Core.Swift;
@import CoreSentinel;

@implementation WebServiceInterface

+ (NSString *)apiHost{
#ifdef DEVELOPMENT
    if ([[[NSProcessInfo processInfo] arguments] containsObject:@"UITests"]) {
        if ([[[NSProcessInfo processInfo] environment] valueForKey:@"API_URL"] != nil ){
            return [[[NSProcessInfo processInfo] environment] valueForKey:@"API_URL"];
        }
    } else {
        return [AppParameters globalParameters].developmentApiHost ? [AppParameters globalParameters].developmentApiHost : [Environment apiUrl];
    }
    return nil;
#else
    return [Environment apiUrl];
#endif
}

/*!
 * Returns the complete endpoint
 * @param methodPath
 */
+ (NSString *)apiEndpoint:(NSString *)methodPath{
    if ([methodPath rangeOfString:@"http"].location != NSNotFound) { // If the request have a complete url
        return methodPath;
    }
	return [[WebServiceInterface apiHost] stringByAppendingString:methodPath];
}

/*!
 * Efetua uma chamada ao webservice com a possibilidade de envio de dados.
 * @param postVars dados que seram enviados para o webservice
 * @param methodPath
 * @param cacheCompletionHandler CodeBlock para callback do cache
 * @param completionHandler CodeBlock para callback
 *
 * @note Toda Requisição feita ao webservice deve ser realizada dentro de uma Thread (@b NÃO utilizar na Thread Principal).
 */
+ (void)dictionaryWithPostVars:(NSDictionary *)postVars serviceMethodUrl:(NSString *)methodPath
                      cacheKey: (NSString *) cacheKeyPrefix
               cacheCompletion: (void (^)(NSDictionary *data)) cacheCompletionHandler
             completionHandler:(void (^)(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error))callback{
    
    if (cacheKeyPrefix == nil) {
        cacheKeyPrefix = @"_";
    }
    
    __block NSString *cacheKey = [NSString stringWithFormat:@"old_web_%@_%@_", cacheKeyPrefix, methodPath];
    __block BOOL requestCompleted = NO;
    [CacheManagerOBJC.shared getApiResponseCacheForKey:cacheKey onLoad:^(NSDictionary * _Nonnull data)  {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            // workaroung to prevent the cache call after internet result
            if (requestCompleted == NO) {
                cacheCompletionHandler(data);
            }
        });
    }];
    
    [WebServiceInterface dictionaryWithPostVars:postVars serviceMethodUrl:methodPath completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error) {
        requestCompleted = YES;
        if ([data isKindOfClass:[NSDictionary class]] && error == nil) {
            NSDictionary *dict = (NSDictionary*) data;
            if([dict valueForKey:@"Error"] == nil ){
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [CacheManagerOBJC.shared setApiResponseCacheWithDict:dict key:cacheKey];
                });
            }
        }
        
        callback(data, response, responseData, error);
    }];
}

/*!
 * Efetua uma chamada ao webservice com a possibilidade de envio de dados e headers.
 * @param postVars dados que serão enviados para webservice
 * @param headers dados que serão enviados para webservice
 * @param methodPath
 * @param completionHandler CodeBlock para callback
 *
 * @note Toda Requisição feita ao webservice deve ser realizada dentro de uma Thread (@b NÃO utilizar na Thread Principal).
 */
+ (void)dictionaryWithPostVars:(NSDictionary *)postVars andHeaders:(NSDictionary *)headers serviceMethodUrl:(NSString *)methodPath
             completionHandler:(void (^)(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error))callback{
    // Verifica se dados informados pode ser serializados no formato JSON (Contrato com backend),
    // caso não seja possível encerra a execução do método
    if(![NSJSONSerialization isValidJSONObject:postVars])
    {
        return;
    }
    
    NSURL *url = [NSURL URLWithString:[WebServiceInterface apiEndpoint:methodPath]];
    NSData *__jsonData;
    NSString *__jsonString;
    NSMutableURLRequest *request;
    
    // Serializando dados que serão enviados
    __jsonData = [NSJSONSerialization dataWithJSONObject:postVars options:0 error:nil];
    __jsonString = [[NSString alloc] initWithData:__jsonData encoding:NSUTF8StringEncoding];
    
    //if (DEBUG_MODE_METHOD) DebugLogWs(@"Method %@",[WebServiceInterface apiEndpoint:methodPath]);
    //if (DEBUG_MODE_JSON_OUT) DebugLogWs(@"Envio  %@",__jsonString);
    //if (DEBUG_MODE_USR_TOKEN) DebugLogWs(@"Token  %@", [[AppParameters globalParameters] userToken]);
    
    // Be sure to properly escape your url string.
    if (![methodPath isEqualToString:kWsUrlItemByQrCode] && ![methodPath isEqualToString:kWsUrlGetStoresNearby]) {
        request = [NSMutableURLRequest requestWithURL:url];
    }else{
        request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:15];
    }
    
    // Configurando Request
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody: __jsonData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // Adiciona o token a requisição para validação de autenticação no backend
    if([User isAuthenticated]) {
        [request setValue:[User token] forHTTPHeaderField:@"token"];
    }
    
    LocationManagerObjcWrapper *locationManager = [[LocationManagerObjcWrapper alloc] init];
    LocationHeadersBridge *locationHeaders = [[LocationHeadersBridge alloc] init];
        
    if (locationManager.location != nil) {
        CLLocation *location = locationManager.location;
        [request setValue:[NSString stringWithFormat:@"%f",location.coordinate.latitude] forHTTPHeaderField:locationHeaders.latitude];
        [request setValue:[NSString stringWithFormat:@"%f",location.coordinate.longitude] forHTTPHeaderField:locationHeaders.longitude];
        [request setValue:[NSString stringWithFormat:@"%f",location.horizontalAccuracy] forHTTPHeaderField:locationHeaders.accuracy];
        [request setValue:[NSString stringWithFormat:@"%0.0f",location.timestamp.timeIntervalSince1970 * 1000] forHTTPHeaderField:locationHeaders.locationTimestamp];
        [request setValue:[NSString stringWithFormat:@"%0.0f",[[NSDate date] timeIntervalSince1970] * 1000] forHTTPHeaderField:locationHeaders.currentTimestamp];
    }
    NSLog(@"Adicionado %@", request.allHTTPHeaderFields);
    
    @try {
        [request setValue:[[UIDevice currentDevice] installationId] forHTTPHeaderField:@"installation_id"];
    }
    @catch (NSException *exception) {
        [request setValue:@"HOUSTON-WE-HAVE-A-PROBLEM" forHTTPHeaderField:@"installation_id"];
    }
    
    @try {
        NSString *deviceId = [DeviceHelperObjc deviceId];
        [request setValue:deviceId forHTTPHeaderField:@"device_id"];
    }
    @catch (NSException *exception) {
        [request setValue:@"HOUSTON-WE-HAVE-A-PROBLEM" forHTTPHeaderField:@"device_id"];
    }
    
    @try {
        [request setValue:[NSString stringWithFormat:@"%ld", (long) CreditCardManager.shared.defaultCreditCardId] forHTTPHeaderField:@"selected_credit_card_id"];
    }
    @catch (NSException *exception) {
    }
    
    @try {
        [request setValue:[ConsumerManager useBalance] ? @"1" : @"0" forHTTPHeaderField:@"use_balance"];
    }
    @catch (NSException *exception) {
    }
    
    @try {
        CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
        CTCarrier *carrier = [netinfo subscriberCellularProvider];
        [request setValue:[carrier carrierName] forHTTPHeaderField:@"carrier"];
    }
    @catch (NSException *exception) {
        [request setValue:@"HOUSTON-WE-HAVE-A-PROBLEM" forHTTPHeaderField:@"carrier"];
    }
    
    //app version
    [request setValue:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forHTTPHeaderField:@"app_version"];
    
    //device OS
    [request setValue:@"ios" forHTTPHeaderField:@"device_os"];
    
    // CorrelationId
    NSString *correlationId = CorrelationId.id;
    [request setValue:correlationId forHTTPHeaderField:CorrelationId.headerField];
    
    //timezone
    [request setValue:[[NSTimeZone localTimeZone] name] forHTTPHeaderField:@"timezone"];
    
    //device model
    [request setValue:[[UIDevice currentDevice] deviceModel] forHTTPHeaderField:@"device_model"];
    
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[__jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
    //Add Extra Headers
    if (headers) {
        for(NSString* key in headers) {
            NSString* value = [headers objectForKey:key];
            if (value) {
                [request setValue:key forHTTPHeaderField:value];
            }
        }
    }
    
    // DEBOG Request
    NSString *requestLog = @"----------------- REQUEST -------------------\n";
    requestLog = [requestLog stringByAppendingString:[request cURLCommandString]];
    requestLog = [requestLog stringByAppendingString:@"\n=========== REQUEST END ============"];
    [Log infoWithDomain:@[[LogDomain network], @"NetworkRequest"] message:requestLog file:[NSString stringWithUTF8String:__FILE__] line:__LINE__ function:[NSString stringWithUTF8String:__FUNCTION__]];
    // Fim Debug Request
    
    // Enviando requisição
    WSConnection *connection = [[WSConnection alloc] init];
    __weak typeof(self) weakSelf = self;
    [connection sendRequest:request completionHandler:^(NSData *data, NSURLResponse *theResponse, NSError *error){
        // caso ocorra um erro na requisição retorna com o error no callback
        NSInteger statusCode = [(NSHTTPURLResponse*) theResponse statusCode];
        if (error) {
            if ([error code] == NSURLErrorNotConnectedToInternet) {
                callback(nil, nil, nil, [PicPayError notConnectedToInternet]);
            } else {
                callback(nil, nil, nil, error);
            }
            return;
        } else {
            // Trabalhando com dados globais da aplicação que podem ser retornados em qualquer requisição ao backend
            //reading http header for unread notifications number
            @try {
                NSDictionary* allHeaders = [(NSHTTPURLResponse*) theResponse allHeaderFields];
                if ([allHeaders objectForKey:@"syncdata"] != nil && [[allHeaders objectForKey:@"syncdata"] isKindOfClass:[NSString class]]) {
                    NSString *syncDataJsonString = [NSString stringWithFormat:@"%@",[allHeaders objectForKey:@"syncdata"]];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ApiNotificationSyncData" object:nil userInfo:@{ @"json": syncDataJsonString}];
                }
            }
            @catch (NSException *exception) {
                //[[AppParameters globalParameters] setUnreadNotificationsCount:0];
            }
            
            //json to log events on analytics services
            @try {
                NSError *headerJsonError2;
                NSDictionary *anlyticsEvents = [NSJSONSerialization JSONObjectWithData: [[NSString stringWithFormat:@"%@",[[(NSHTTPURLResponse*) theResponse allHeaderFields] objectForKey:@"mobile_analytics_events"]] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &headerJsonError2];
                
                @try {
                    if ([anlyticsEvents valueForKey:@"mp_events"] != nil) {
                        
                        for (NSArray *eventJson in [anlyticsEvents valueForKey:@"mp_events"]) {
                            
                            NSString *eventName = [eventJson valueForKey:@"name"];
                            
                            NSMutableDictionary *eventProperties = [[NSMutableDictionary alloc] init];
                            
                            if ([eventJson valueForKey:@"properties"] != nil) {
                                
                                for (NSArray *eventPropertieJson in [eventJson valueForKey:@"properties"]) {
                                    
                                    [eventProperties setObject:[eventPropertieJson valueForKey:@"value"] forKey:[eventPropertieJson valueForKey:@"name"]];
                                }
                            }
                            
                            [PPAnalytics trackEvent:eventName properties:eventProperties];
                        }
                    }
                }
                @catch (NSException *exception) {}
            }
            @catch (NSException *exception) {}
            
            // DEBOG response
            NSString *responseLog = @"------------- REQUEST RESPONSE --------------\n";
            responseLog = [responseLog stringByAppendingString:[NSString stringWithFormat:@"URL: %@ \n", [[(NSHTTPURLResponse *)theResponse URL] absoluteString]]];
            if (DEBUG_MODE_HTTP_STATUS_CODE)responseLog = [responseLog stringByAppendingString:[NSString stringWithFormat:@"STATUS: %ld \n", (long)[(NSHTTPURLResponse *)theResponse statusCode]]];
            if (DEBUG_MODE_HEADER_IN) responseLog = [responseLog stringByAppendingString:[NSString stringWithFormat:@"HEADERS: %@ \n", [(NSHTTPURLResponse*)theResponse allHeaderFields]]];
            if (DEBUG_MODE_JSON_IN) responseLog = [responseLog stringByAppendingString:[NSString stringWithFormat:@"\n%@\n", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]]];
            responseLog = [responseLog stringByAppendingString:@"\n========= REQUEST RESPONSE END ==========="];
            [Log infoWithDomain:@[[LogDomain network], @"NetworkResponse"] message:responseLog file:[NSString stringWithUTF8String:__FILE__] line:__LINE__ function:[NSString stringWithUTF8String:__FUNCTION__]];
            // fim DEBUG response
        }
        //verifica se houve erro http
        if (statusCode != 200) {
            if (statusCode == 426) {
                if([(NSHTTPURLResponse*) theResponse statusCode] == 426){
                    // ----------- NOTIFIES THAT APP NEED TO BE UPDATED ----------
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WSAppNeedUpdate" object:nil];
                    // ==============================================================
                } else {
                    NSError *errorStatus = [self errorWithTitle:@"Não foi possível obter uma resposta do servidor." code:7];
                    callback(nil, nil, nil, errorStatus);
                }
            } else {
                NSError *jsonParsingError = nil;
                NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers|NSJSONReadingAllowFragments error:&jsonParsingError];
                if (jsonArray) {
                    if ([jsonArray isKindOfClass:[NSDictionary class]]){
                        PicPayError *picpayError = (PicPayError *)[self picpayError:(NSDictionary *)jsonArray statuscode:statusCode];
                        callback(jsonArray, theResponse, data, picpayError);
                        return;
                    }
                }
                NSError *errorStatus = [self errorWithTitle:@"Não foi possível obter uma resposta do servidor." code:7];
                callback(nil, nil, nil, errorStatus);
            }
        } else {
            // Efetuando parsing do retorno (Decode do JSON)
            NSError *jsonParsingError = nil;
            NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers|NSJSONReadingAllowFragments error:&jsonParsingError];
            
            if (DEBUG_MODE_JSON_DICT) DebugLogWs(@"DEBUG_MODE_JSON_DICT: %@",[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParsingError]);
            
            //checks if the request was returned with the error: "invalid sessein" (token is not valid).
            NSArray *wsError = [jsonArray valueForKey:@"Error"];
            if (wsError) {
                @try {
                    NSInteger errorId = [[wsError valueForKey:@"id"] integerValue];
                    if ([[jsonArray valueForKey:@"Error"] isKindOfClass:[NSDictionary class]]){
                        error = (NSError *)[self picpayError:(NSDictionary *)wsError statuscode:statusCode];
                    }
                    if (errorId == 1) {
                        [weakSelf notifiesUserIsNotAuthenticatedWith:error];
                    }
                    
                    NSString *message;
                    NSString *code;
                    
                    // Message error
                    if ([wsError valueForKey:@"description_pt"] != nil && [[wsError valueForKey:@"description_pt"] isKindOfClass: [NSString class]]) {
                        message = [NSString stringWithFormat:@"%@", [wsError valueForKey:@"description_pt"]];
                    } else {
                        message = @"Sem mensagem de erro";
                    }
                    
                    // Error code
                    if ([wsError valueForKey:@"id"] != nil) {
                        code = [NSString stringWithFormat:@"%ld", (long)[[wsError valueForKey:@"id"] integerValue]];
                    } else {
                        code = @"Empty code";
                    }
                    
                    // Track Error Event
                    NSString *statusCode = [NSString stringWithFormat:@"%ld", (long)[(NSHTTPURLResponse *) theResponse statusCode]];
                    [PPAnalytics trackEvent:@"API Error" properties:@{@"code": code,
                                                                      @"message": message,
                                                                      @"endpoint": request.URL.path,
                                                                      @"status_code": statusCode}];
                    NSString *newPaymentArchitecture = @"";
                    NSString *newPaymentHeader = request.allHTTPHeaderFields[NewPaymentArchitecture.headerField];
                    if(newPaymentHeader != nil) {
                        newPaymentArchitecture = newPaymentHeader;
                    }
                    
                    [OBJCSentinel trackWithType:@"api_error"
                                           name:nil
                                     attributes:@{@"code": code,
                                                  @"message": message,
                                                  @"endpoint": request.URL.path,
                                                  @"status_code": statusCode,
                                                  @"x_request_id": correlationId,
                                                  @"X-New-Payment": newPaymentArchitecture}
                                      providers:@[@"newRelic"]];

                } @catch (NSException *exception) {}
            }
            callback(jsonArray, theResponse, data, error);
        }
    }];
    //[task resume];
}


/**
 Notifies that user is not authenticated
 */
+ (void)notifiesUserIsNotAuthenticatedWith:(NSError *)error {
    NSDictionary *dict;
    if (error) {
        dict = @{ @"error": error };
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"WSUserNotAuthenticated" object:nil userInfo:dict];
}

/*!
 * Efetua uma chamada ao webservice com a possibilidade de envio de dados.
 * @param postVars dados que seram enviados para o webservice
 * @param methodPath
 * @param completionHandler CodeBlock para callback
 *
 * @note Toda Requisição feita ao webservice deve ser realizada dentro de uma Thread (@b NÃO utilizar na Thread Principal).
 */
+ (void)dictionaryWithPostVars:(NSDictionary *)postVars serviceMethodUrl:(NSString *)methodPath
             completionHandler:(void (^)(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error))callback{
    
    [WebServiceInterface dictionaryWithPostVars:postVars andHeaders:nil serviceMethodUrl:methodPath completionHandler:callback];
}

/*!
 * Efetua uma chamada ao webservice com a possibilidade de envio de dados.
 * @param postVars dados que seram enviados para o webservice
 * @param methodPath
 * @param headers
 * @param completionHandler CodeBlock para callback
 *
 * @note Toda Requisição feita ao webservice deve ser realizada dentro de uma Thread (@b NÃO utilizar na Thread Principal).
 */
+ (void)dictionaryWithPostVars:(NSDictionary *)postVars serviceMethodUrl:(NSString *)methodPath andHeaders:(NSDictionary *)headers
             completionHandler:(void (^)(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error))callback{
    
    [WebServiceInterface dictionaryWithPostVars:postVars andHeaders:headers serviceMethodUrl:methodPath completionHandler:callback];
}

/*!
 * Efetua o upload de imagem
 * @param image imagem
 * @param postVars dados que seram enviados para o webservice
 * @param methodPath
 * @param completionHandler CodeBlock para callback
 * @param progressCallback CodeBlock para progresso
 *
 * @note Toda Requisição feita ao webservice deve ser realizada dentro de uma Thread (@b NÃO utilizar na Thread Principal).
 */
+ (void)uploadImage:(NSData *)imageData serviceMethodUrl:(NSString *)methodPath postVars:(NSDictionary *)postVars completionHandler:(void (^)(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error))callback progressCallback:(void (^)(CGFloat progress))progressCallback;{
    
    if (DEBUG_MODE_METHOD) {
       DebugLogWs(@"Method %@",[WebServiceInterface apiEndpoint:methodPath]);
    }
    
    if (DEBUG_MODE_USR_TOKEN) {
      DebugLogWs(@"Token  %@", User.token);
    }
    
    NSURL *url = [NSURL URLWithString:[WebServiceInterface apiEndpoint:methodPath]];
    NSString *boundary = @"----WebKitFormBoundarycC4YiaUFwM44F6rT";
    
    // Be sure to properly escape your url string.
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    [request setValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary] forHTTPHeaderField:@"Content-Type"];
    
    // the body of the post
    NSMutableData *body = [NSMutableData data];
    
    // Now we need to append the different data 'segments'. We first start by adding the boundary.
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Now append the image
    // Note that the name of the form field is exactly the same as in the trace ('attachment[file]' in my case)!
    // You can choose whatever filename you want.
    [body appendData:[@"Content-Disposition: form-data; name=\"image\"; filename=\"picture.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // We now need to tell the receiver what content type we have
    // In my case it's a png image. If you have a jpg, set it to 'image/jpg'
    [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Now we append the actual image data
    [body appendData:[NSData dataWithData:imageData]];
    
    if (postVars != nil) {
        [postVars enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", key, obj] dataUsingEncoding:NSUTF8StringEncoding]];
        }];
    }
    
    
    
    // and again the delimiting boundary
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    if (DEBUG_MODE_BODY_OUT) DebugLogWs(@"%@", [[NSString alloc] initWithData:body encoding:NSASCIIStringEncoding]);
    
    
    // adding the body we've created to the request
    [request setHTTPBody:body];
    
    
    if([User isAuthenticated]){
        [request setValue:[User token] forHTTPHeaderField:@"token"];
    }
    
    //app version
    [request setValue:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forHTTPHeaderField:@"app_version"];
    
    //device OS
    [request setValue:@"ios" forHTTPHeaderField:@"device_os"];
    
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    //device model
    [request setValue:[NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding] forHTTPHeaderField:@"device_model"];
    
    if (DEBUG_MODE_HEADER_OUT) DebugLogWs(@"HTTP Headers OUT  %@", [request allHTTPHeaderFields]);
    
    // Enviando requisição
    WSConnection *connection = [[WSConnection alloc] init];
    [connection sendRequest:request completionHandler:^(NSData *data, NSURLResponse *theResponse, NSError *error){
        if (DEBUG_MODE_HEADER_IN) DebugLogWs(@"HTTP Headers IN from %@  %@", methodPath, [(NSHTTPURLResponse*)theResponse allHeaderFields]);
        if (DEBUG_MODE_JSON_IN) DebugLogWs(@"%@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        
        if (error) {
            if ([error code] == NSURLErrorNotConnectedToInternet) {
                error = [PicPayError notConnectedToInternet];
            }
        } else if ([(NSHTTPURLResponse*) theResponse statusCode] != 200) {
            error = [self errorWithTitle:@"Não foi possível obter uma resposta do servidor." code:7];
        } else {
            NSError *jsonParsingError = nil;
            NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers|NSJSONReadingAllowFragments error:&jsonParsingError];
            
            if (DEBUG_MODE_JSON_DICT) DebugLogWs(@"%@",[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParsingError]);
            
          
            callback(jsonArray, theResponse, data, error);
            return;
        }
        
        callback(nil, nil, nil, error);
        return;
    } progressCallback:progressCallback];
}

+ (NSError *)errorWithTitle:(NSString *)message code:(NSInteger)code{
	NSMutableDictionary *dict = [NSMutableDictionary dictionary];
	[dict setValue:message forKey:NSLocalizedDescriptionKey];
	NSError *error = [NSError errorWithDomain:@"com.picpay" code:code userInfo:dict];
	return error;
}

+ (PicPayError *)picpayError:(NSDictionary *)errorData statuscode:(NSInteger)statuscode {
    if (statuscode == 200) {
        return [[PicPayError alloc] initWithApiJSON:errorData];
    } else {
        return [[PicPayError alloc] initWithErrorJSON:errorData];
    }
}

+ (PicPayError *)picpayError:(NSDictionary *)errorData {
    return (PicPayError *)[self picpayError:errorData statuscode: 0];
}

+ (NSError *)genericError {
    return [WebServiceInterface errorWithTitle:@"Não foi possível efetuar a operação." code:0];
}

/*!
 * WORKAROUND SWIFT
 * Efetua uma chamada ao webservice com a possibilidade de envio de dados.
 * @param postVars dados que seram enviados para o webservice
 * @param methodPath
 * @param completionHandler CodeBlock para callback
 *
 * @note Toda Requisição feita ao webservice deve ser realizada dentro de uma Thread (@b NÃO utilizar na Thread Principal).
 */
+ (void)dictionaryWithPostVarsSwift:(NSDictionary *)postVars serviceMethodUrl:(NSString *)methodPath
             completionHandler:(void (^)(NSDictionary *data, NSURLResponse *response, NSData *responseData, NSError *error))callback{

    [WebServiceInterface dictionaryWithPostVars:postVars serviceMethodUrl:methodPath completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error) {
        
        @try {
            NSDictionary *dict = (NSDictionary *) data;
            callback(dict,response,responseData,error);
            return;
        } @catch (NSException *exception) {
            callback(nil,response,responseData,[WebServiceInterface errorWithTitle:@"Não foi possível obter uma resposta do servidor." code:7]);
            return;
        }
    }];
    
}

/*!
 * WORKAROUND SWIFT
 * Efetua uma chamada ao webservice com a possibilidade de envio de dados.
 * @param postVars dados que seram enviados para o webservice
 * @param methodPath
 * @param completionHandler CodeBlock para callback
 *
 * @note Toda Requisição feita ao webservice deve ser realizada dentro de uma Thread (@b NÃO utilizar na Thread Principal).
 */
+ (void)dictionaryWithPostVarsSwift:(NSDictionary *)postVars serviceMethodUrl:(NSString *)methodPath andHeaders:(NSDictionary *)headers
                  completionHandler:(void (^)(NSDictionary *data, NSURLResponse *response, NSData *responseData, NSError *error))callback{
    
    [WebServiceInterface dictionaryWithPostVars:postVars serviceMethodUrl:methodPath andHeaders:headers completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error) {
        @try {
            NSDictionary *dict = (NSDictionary *) data;
            callback(dict,response,responseData,error);
            return;
        } @catch (NSException *exception) {
            callback(nil,response,responseData,[WebServiceInterface errorWithTitle:@"Não foi possível obter uma resposta do servidor." code:7]);
            return;
        }
    }];
}

/*!
 * WORK AROUND SWIFT
 * Efetua uma chamada ao webservice com a possibilidade de envio de dados.
 * @param postVars dados que seram enviados para webservice
 * @param methodPath
 * @param cacehKey chave do cache
 * @param cacheCompletionHandler CodeBlock para callback do cache
 * @param completionHandler CodeBlock para callback
 *
 * @note Toda Requisição feita ao webservice deve ser realizada dentro de uma Thread (@b NÃO utilizar na Thread Principal).
 */
+ (void)dictionaryWithPostVarsSwift:(NSDictionary *)postVars
                   serviceMethodUrl:(NSString *)methodPath
                           cacheKey: (NSString *) cacheKey
             cachecompletionHandler:(void (^)(NSDictionary *data)) cacheCallback
                  completionHandler:(void (^)(NSDictionary *data, NSURLResponse *response, NSData *responseData, NSError *error))callback{
    
    [WebServiceInterface dictionaryWithPostVars:postVars serviceMethodUrl:methodPath cacheKey:cacheKey cacheCompletion:cacheCallback completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error) {
        
        @try {
            NSDictionary *dict = (NSDictionary *) data;
            callback(dict,response,responseData,error);
            return;
        } @catch (NSException *exception) {
            callback(nil,response,responseData,[WebServiceInterface errorWithTitle:@"Não foi possível obter uma resposta do servidor." code:7]);
            return;
        }
    }];
}

@end

/**!
 * Category to make it easier to identify a connection error
 */
@implementation NSError (ConnectionError)

- (BOOL) isConnectionError{
    if ( [self.domain isEqualToString:NSURLErrorDomain]){
        return YES;
    } else if (([self.domain isEqualToString:@"com.picpay"] && self.code == 22) || ([self.domain isEqualToString:@"PicPay"] && self.code == 22)) {
        return YES;
    }
    return NO;
}

@end
