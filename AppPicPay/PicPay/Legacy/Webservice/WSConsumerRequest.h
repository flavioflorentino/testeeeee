//
//  WSConsumerRequest.h
//  MoBuy
//
//  Created by Joao victor V lana on 08/08/12.
//
//

#import <Foundation/Foundation.h>
#import "WebServiceInterface.h"
@import CoreLegacy;
@class PPPromoCode;

@interface WSConsumerRequest : NSObject

/*!
 * Retorna todos os dados do usuário (Consumer).
 *
 * Callback:
 *   - @b MBConsumer => consumer
 *
 *   - @b NSError => erro
 * @param useCache flag para utilizar o cache (Caso disponível).
 * @param completionHandler callback
 */
+ (void)getAllConsumerData:(BOOL)useCache completionHandler:(void (^)(MBConsumer *consumer, NSError *error))callback;

@end
