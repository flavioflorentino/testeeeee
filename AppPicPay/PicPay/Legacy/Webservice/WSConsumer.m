//
//  WSConsumer.m
//  MoBuy
//
//  Created by Diogo Carneiro on 08/08/12.
//
//

#import "WSConsumer.h"
#import "PPNotification.h"
#import "PicPay-Swift.h"
@import Core;

@implementation WSConsumer

# pragma mark Dados do Usuário

/*!
 * Desativa um consumer
 * Callback:
 *   - @b NSError => erro
 * @param pin pin
 * @param completionHandler callback
 */
+ (void)deactivateConsumer:(NSString *)pin completionHandler:(void (^)(BOOL success, NSError * error)) callback{
    
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: pin, nil] forKeys:[NSArray arrayWithObjects:@"pin", nil]] serviceMethodUrl:kWsUrlDeactivateConsumer completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(NO, err);
            return;
        }
        
        NSString *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        if (data) {
            callback(YES, err);
            return;
        } else {
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(NO, error);
            return;
        }
    }];
}

/*!
 * Reativa um consumer
 * Callback:
 *   - @b NSError => erro
 * @param login login
 * @param password password
 * @param completionHandler callback
 */
+ (void)reactivateConsumer:(NSString *)login password:(NSString *)password completionHandler:(void (^)(BOOL success, NSError * error)) callback{
    
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:login, password, nil] forKeys:[NSArray arrayWithObjects:@"email", @"password", nil]] serviceMethodUrl:kWsUrlRequestConsumerReactivation completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(NO, err);
            return;
        }
        
        NSString *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        if (data) {
            
            callback(YES, err);
        }else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(NO, error);
            return;
        }
        
        callback(NO, err);
    }];
}

/*!
 * Atualiza os dados pessoais
 * Callback:
 *   - @b BOOL => success
 *   - @b NSError => erro
 * @param email email
 * @param cpf cpf
 * @param birth_date data de nascimento
 * @param completionHandler callback
 */
+ (void)updatePersonalData:(NSString *)name cpf:(NSString *)cpf birth_date:(NSString *)birth_date name_mother:(NSString *)name_mother completionHandler:(void (^)(BOOL success, NSError *error))callback{
    
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:name, cpf, birth_date, name_mother, nil] forKeys:[NSArray arrayWithObjects:@"name", @"cpf", @"birth_date", @"name_mother", nil]] serviceMethodUrl:kWsUrlUpdateConsumer completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(NO, err);
            return;
        }
        
        NSString *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        
        if (data) {
            //Caching name and email for display
            if((NSNull *)name != [NSNull null]) {
                [[KVStore new] setString:name with:KVKeyUserName];
            }
            
            [ConsumerManager.shared.consumer setName:name];
            [ConsumerManager.shared.consumer setCpf:cpf];
            [ConsumerManager.shared.consumer setBirth_date:birth_date];
            [ConsumerManager.shared.consumer setName_mother:name_mother];
            callback(YES, err);
            return;
        }else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(NO, error);
            return;
        }
    }];
}

/*!
 * create username.
 * Callback:
 *   - @b NSError => erro
 * @param email username
 * @param completionHandler callback
 */
+ (void)createUsername:(NSString *)username completionHandler:(void (^)(NSError *error))callback{
    
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:username, nil] forKeys:[NSArray arrayWithObjects:@"username", nil]] serviceMethodUrl:kWsUrlCreateConsumerUsername completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(err);
            return;
        }
        
        NSString *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        
        if (data) {
            
            callback(err);
            return;
            
        } else {
            NSString *description = [wsError valueForKey:@"description_pt"];
            NSString *errorID = [wsError valueForKey:@"id"];
            
            if (!([errorID isEqual:[NSNull null]] || [description isEqual:[NSNull null]])) {
                NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
                return callback(error);
            }
            NSError *error = [WebServiceInterface errorWithTitle: @"Ops, tivemos problemas ao enviar suas informações, por favor, tente novamente." code: [@"000" intValue]];
            callback(error);
        }
    }];
}

/*!
 * Atualiza o email do consumer
 * Callback:
 *   - @b BOOL => success
 *   - @b NSError => erro
 * @param email
 * @param pin
 * @param completionHandler callback
 */
+ (void)updateEmail:(NSString *)email pin:(NSString *)pin completionHandler:(void (^)(BOOL success, NSError *error))callback{
    
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:email, pin, nil] forKeys:[NSArray arrayWithObjects:@"email", @"pin", nil]] serviceMethodUrl:kWsUrlUpdateConsumerEmail completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        BOOL success = NO;
        
        if(err) {
            callback(success, err);
            return;
        }
        
        NSError *error = nil;
        NSDictionary* data = [json valueForKey:@"data"];
        NSArray* wsError = [json valueForKey:@"Error"];
        
        if ([data isKindOfClass:[NSDictionary class]]) {
            @try {
                
                NSString* email = [data valueForKey:@"email"];
                if((NSNull *)email != [NSNull null]) {
                    [[KVStore new] setString:email with:KVKeyEmail];
                    [ConsumerManager.shared.consumer setEmail:email];
                }
                
                NSNumber* verified = [data objectForKey:@"email_confirmed"];
                if (verified != nil) {
                    [[KVStore new] setBool: [verified boolValue] with:KVKeyEmail_confirmed];
                    [ConsumerManager.shared.consumer setEmailConfirmed:[verified boolValue]];
                }
                
                NSNumber* pending = [data objectForKey:@"change_pending"];
                if (pending != nil) {
                    [[KVStore new] setBool: [pending boolValue] with:KVKeyEmail_pending];
                    [ConsumerManager.shared.consumer setEmailPending:[pending boolValue]];
                }
                
                success = YES;
            } @catch (NSException *exception) {
                error = [WebServiceInterface errorWithTitle:@"Não foi possivel obter as informações." code:0];
            }
        }else{
            error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
        }
        
        callback(success, error);
        
    }];
}

/*!
 * Verifica o status do email do consumer
 * Callback:
 *   - @b BOOL => success
 *   - @b NSError => erro
 * @param email
 * @param pin
 * @param completionHandler callback
 */
+ (void)checkEmailStatus:(void (^)(BOOL success, NSError *error))callback{
    NSDictionary* body = [[NSDictionary alloc] init];
    [WebServiceInterface dictionaryWithPostVars:body serviceMethodUrl:kWsUrlCheckConsumerEmailStatus completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        BOOL success = NO;
        
        if(err) {
            callback(success, err);
            return;
        }
        
        NSError *error = nil;
        NSDictionary* data = [json valueForKey:@"data"];
        NSArray* wsError = [json valueForKey:@"Error"];
        
        if ([data isKindOfClass:[NSDictionary class]]) {
            @try {
                
                NSString* email = [data valueForKey:@"email"];
                if((NSNull *)email != [NSNull null]) {
                    [[KVStore new] setString:email with:KVKeyEmail];
                    [ConsumerManager.shared.consumer setEmail:email];
                }
                
                NSNumber* verified = [data objectForKey:@"email_confirmed"];
                if (verified != nil) {
                    [[KVStore new] setBool: [verified boolValue] with:KVKeyEmail_confirmed];
                    [ConsumerManager.shared.consumer setEmailConfirmed:[verified boolValue]];
                }
                
                NSNumber* pending = [data objectForKey:@"change_pending"];
                if (pending != nil) {
                    [[KVStore new] setBool: [pending boolValue] with:KVKeyEmail_pending];
                    [ConsumerManager.shared.consumer setEmailPending:[pending boolValue]];
                }
                
                success = YES;
            } @catch (NSException *exception) {
                error = [WebServiceInterface errorWithTitle:@"Não foi possivel obter as informações." code:0];
            }
        }else{
            error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
        }
        
        callback(success, error);
    }];
}

/*!
 * Verifica o status do email do consumer
 * Callback:
 *   - @b BOOL => success
 *   - @b NSError => erro
 * @param email
 * @param pin
 * @param completionHandler callback
 */
+ (void)resendEmailConfirmation:(void (^)(BOOL success, NSError *error))callback{
    NSDictionary* body = [[NSDictionary alloc] init];
    [WebServiceInterface dictionaryWithPostVars:body serviceMethodUrl:kWsUrlResendConfirmationConsumerEmail completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        BOOL success = NO;
        
        if(err) {
            callback(success, err);
            return;
        }
        
        NSError *error = nil;
        NSDictionary *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        
        if (data) {
            success = YES;
        } else {
            error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
        }
        
        callback(success, error);
    }];
}

# pragma mark Conta Bancária

/*!
 * Adiciona uma nova conta de banco
 * Callback:
 *   - @b NSError => erro
 * @param bankAccountDictionary bankAccountDictionary
 * @param origin origin
 * @param completionHandler callback
 */
+ (void)addBankAccount:(NSDictionary *)bankAccountDictionary origin:(NSString *)origin completionHandler:(void (^)(NSError *error))callback{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:bankAccountDictionary];
    [dict setObject:origin forKey:@"origin"];
    
    [WebServiceInterface dictionaryWithPostVars:dict serviceMethodUrl:kWsUrlAddBankAccount completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        if(err) {
            callback(err);
            return;
        }
        
        NSArray *wsError = [json valueForKey:@"Error"];
        if ([json valueForKey:@"data"] != [NSNull null] && [[json valueForKey:@"data"] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *data = [json valueForKey:@"data"];
            
            if ([data valueForKey:@"BankAccount"] != [NSNull null] && [[data valueForKey:@"BankAccount"] isKindOfClass:[NSDictionary class]]) {
                PPBankAccount *returnedBankAccount = [[PPBankAccount alloc] initWithDictionary:[data valueForKey:@"BankAccount"]];
                
                if ([data valueForKey:@"Banco"] != [NSNull null] && [[data valueForKey:@"Banco"] isKindOfClass:[NSDictionary class]]) {
                    PPBank* bank = [[PPBank alloc] initWithDictionary:[data valueForKey:@"Banco"]];
                    returnedBankAccount.bank = bank;
                }
                
                ConsumerManager.shared.consumer.bankAccount = returnedBankAccount;
            }
            
        }else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(error);
            return;
        }
        
        callback(nil);
        return;
    }];
}

/*!
 * Deleta uma conta de banco
 * Callback:
 *   - @b NSError => erro
 * @param bankAccount bankAccount
 * @param completionHandler callback
 */
+ (void)deleteBankAccount:(PPBankAccount *)bankAccount completionHandler:(void (^)(NSError *error))callback{
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:bankAccount.wsId, nil] forKeys:[NSArray arrayWithObjects:@"bank_account_id", nil]] serviceMethodUrl:kWsUrlDeleteBankAccount completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(err);
            return;
        }
        
        NSString *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        if (data) {
            ConsumerManager.shared.consumer.bankAccount = nil;
        }else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(error);
            return;
        }
        
        callback(nil);
        return;
    }];
}

# pragma mark Validação de Sessão

/*!
 * Verifica se a senha do usuário está correta.
 *
 * Callback:
 *
 *   - @b BOOL => verified
 *
 *   - @b NSError => erro
 *
 * @param password password
 * @param completionHandler callback
 */
+ (void)verifyPassword:(NSString *)password completionHandler:(void (^)(BOOL verified,NSError * error)) callback{
    [WebServiceInterface dictionaryWithPostVars:@{@"password": password} serviceMethodUrl:kWsUrlVerifyPassword completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(NO, err);
            return;
        }
        
        NSString *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        
        if (data) {
            
            if([data isEqualToString:@"ok"]){
                callback(YES, err);
                return;
            }
            
        }else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(NO, error);
            return;
        }
        
        callback(NO, err);
        return;
    }];
}

/*!
 * Efetua o logout do usuário.
 * Callback:
 *   - @b NSError => error
 * @param wsId id do usuário
 * @param completionHandler callback
 */
+ (void)logout:(NSString *)wsId token:(NSString *)token completionHandler:(void (^)(NSError * error)) callback{
    if (token == nil){
        callback(nil);
        return;
    }
    [WebServiceInterface
     dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: wsId, nil] forKeys:[NSArray arrayWithObjects: @"user_id",nil]]
     andHeaders:[NSDictionary dictionaryWithObjectsAndKeys:@"token", token, nil]
     serviceMethodUrl:kWsUrlUserLogout
     completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error) {
         callback(error);
     }];
}

/*!
 * Verifica se o usuário logado possui conta PRO.
 * Callback:
 *   - @b BOOL => isPro
 *   - @b NSError => error
 * @param completionHandler callback
 */
+ (void)amIPro:(void (^)(BOOL isPro, NSError * error)) callback{
    
    /*
     *
     * Gambiarra requisitada por Diego Sana
     *
     */
    
    [WebServiceInterface dictionaryWithPostVars:@{@"is_pro": @""} serviceMethodUrl:kWsUrlPing completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(NO, err);
            return;
        }
        
        @try {
            
            NSString *data = [json valueForKey:@"data"];
            callback([data boolValue], err);
            return;
        }
        @catch (NSException *exception) {
            callback(NO, err);
            return;
        }
        
    }];
}

/*!
 * Transaforma o usário PRO em usuário normal
 *
 * Callback:
 *
 *   - @b BOOL => isPro
 *
 *   - @b NSError => error
 * @param completionHandler callback
 */
+ (void)disableBusinessAccount:(nullable void (^)(NSString * _Nullable message, NSError * _Nullable error)) callback {
    [WebServiceInterface dictionaryWithPostVars:@{@"business_account": @"0" } serviceMethodUrl:kWsUrlSetConsumerBusinessAccount completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(nil, err);
            return;
        }
        
        NSArray *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        if (data) {
            NSString * message = @"";
            if([[data valueForKey:@"msg"] isKindOfClass:[NSString class]]) {
                message = [NSString stringWithFormat:@"%@", [data valueForKey: @"msg"]];
            }
            
            callback(message, nil);
            return;
            
        }else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(nil, error);
            return;
        }
    }];
}

# pragma mark Notificações

/*!
 * Retorna a lista de notificações.
 * Callback:
 *   - @b NSArray => lista de notificações
 *   - @b NSError => error
 * @param completionHandler callback
 */
+ (void)getNotifications:(void (^)(NSArray *notifications, NSArray * _Nullable followNotifications, NSError * error)) callback cacheLoaded:(void (^)(NSArray *notifications, NSArray * _Nullable followNotifications, NSError * error)) cacheLoaded{
    
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray array] forKeys:[NSArray array]]
                               serviceMethodUrl:kWsUrlGetNotifications
                                       cacheKey: nil
                                cacheCompletion:^(NSDictionary *data) {
        
        [WSConsumer processNotificationsResult:data error:nil callback:cacheLoaded];
    } completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error) {
        NSDictionary *json;
        if ([data isKindOfClass:[NSDictionary class]]) {
            json = (NSDictionary *) data;
        }
        [WSConsumer processNotificationsResult:json error:error callback:callback];
    }];
}

+ (void) processNotificationsResult:(NSDictionary *)json error:(NSError *) error callback:(void (^)(NSArray *notifications, NSArray * _Nullable followNotifications, NSError * error)) callback{
    
    NSMutableArray *notifications = [[NSMutableArray alloc] init];
    NSMutableArray *followNotifications = [[NSMutableArray alloc] init];
    NSArray *wsError = [json valueForKey:@"Error"];
    NSArray *followersRequest;
    
    if (error != nil) {
        callback(nil, nil, error);
        return;
    }
    
    @try {
        followersRequest = [[json valueForKey:@"data"] objectForKey:@"FollowersRequest"];
    }
    @catch (NSException *exception) {
        followersRequest = nil;
    }
    
    @try {
        json = [[json valueForKey:@"data"] objectForKey:@"Notifications"];
        
    }
    @catch (NSException *exception) {
        json = nil;
    }
    
    if (wsError) {
        NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] integerValue]];
        callback(nil, nil, error);
        return;
    }else{
        if (followersRequest){
            for (NSDictionary *rowJson in followersRequest) {
                @try{
                    // create a fake notificaiton with requests
                    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:rowJson];
                    [dict setObject:@[@"2",@"follower_request"] forKey:@"l"];
                    [dict setObject:@"" forKey:@"i"];
                    [dict setObject:[rowJson objectForKey:@"message"] forKey:@"message"];
                    [dict setObject:[rowJson objectForKey:@"message"] forKey:@"title"];
                    [dict setObject:@"" forKey:@"message_alert"];
                    [dict setObject:[rowJson objectForKey:@"data"] forKey:@"data"];
                    [dict setObject:[rowJson objectForKey:@"metadata"] forKey:@"metadata"];
                    PPFollowerNotification *notification = [[PPFollowerNotification alloc] initWithDictionaty:dict viaPush:NO];
                    [followNotifications addObject:notification];
                }@catch (NSException *exception) {
                    DebugLog(@"Não foi possível efetuar o parse da requisição de follower");
                }
            }
        }
        
        if (json) {
            for (NSDictionary *rowJson in json) {
                
                @try{
                    PPNotification *notification;
                    
                    // check notification type
                    NSString *type = @"";
                    @try{
                        if ([rowJson objectForKey:@"l"] != nil &&  [[rowJson objectForKey:@"l"] isKindOfClass: [NSArray class]] ){
                            NSArray *l = (NSArray*) [rowJson objectForKey:@"l"];
                            if ( [l count] > 1){
                                type = [[rowJson objectForKey:@"l"] objectAtIndex:1];
                            }
                            NSString *model_id = (NSString*) [rowJson objectForKey:@"model_id"];
                            if ([model_id isEqualToString:@"withdraw_canceled"]) {
                                type = @"withdrawal";
                            }
                        }
                    }@catch (NSException *exception) {
                        DebugLog(@"Não foi possível identificar o tipo da notificação");
                    }
                    
                    if ([type isEqualToString:@"following"]) {
                        notification = [[PPFollowerNotification alloc] initWithDictionaty:rowJson viaPush:NO];
                    } else if ([type isEqualToString:@"consumer_profile"] || [type isEqualToString:@"new_user_gift"]){
                        notification = [[PPConsumerProfileNotification alloc] initWithDictionaty:rowJson viaPush:NO];
                    } else if ([type isEqualToString:@"withdrawal"]){
                        notification = [[PPWithdrawalNotification alloc] initWithDictionaty:rowJson viaPush:NO];
                    } else if ([type isEqualToString:@"ecommerce_payment"]) {
                        notification = [[EcommerceNotification alloc] initWithDictionaty:rowJson viaPush:NO];
                    }else{
                        notification = [[PPNotification alloc] initWithDictionaty:rowJson viaPush:NO];
                    }
                    
                    [notifications addObject:notification];
                }@catch (NSException *exception) {
                    DebugLog(@"Não foi possível efetuar o parse da notificacao");
                }
                
            }
            
        }
        if (followNotifications.count == 0) {
            callback(notifications, nil, nil);
        } else {
            callback(notifications, followNotifications, nil);
        }
        return;
        
    }
}

/*!
 * Limpa as notificações não lidas.
 * Callback:
 *   - @b NSError => error
 * @param completionHandler callback
 */
+ (void)clearUnseenNotifications:(void (^)(NSError * error)) callback{
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray array] forKeys:[NSArray array]] serviceMethodUrl:kWsUrlClearUnseenNotifications completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error) {
        callback(error);
    }];
    
}

/*!
 * Marca a notificação como lida.
 * Callback:
 *   - @b NSError => error
 * @param notificationId id da Notificação
 * @param completionHandler callback
 */
+ (void)readNotificationWithId:(NSString *)notificationId completionHandler:(void (^)(NSError * error)) callback{
    @try {
        [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: notificationId,nil] forKeys:[NSArray arrayWithObjects: @"notification_id",nil]] serviceMethodUrl:kWsUrlReadNotificationById completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error) {
            callback(error);
        }];
    } @catch (NSException *exception) {
        DebugLog(@"Failed when tried do mark noification as read - %@", exception);
    }
}

/*!
 * Marca a notificação como lida.
 * Callback:
 *   - @b NSError => error
 * @param notificationId id da Notificação
 * @param completionHandler callback
 */
+ (void)ackNotificationWithId:(NSString *)notificationId completionHandler:(void (^)(NSError * error)) callback{
    @try {
        [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: notificationId,nil] forKeys:[NSArray arrayWithObjects: @"notification_id",nil]] serviceMethodUrl:kWsUrlAckNotificationById completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error) {
            callback(error);
        }];
    } @catch (NSException *exception) {        
    }
}

/*!
 * makeWithdrawalForAccount.
 * Callback:
 *   - @b NSError => error
 * @param bankAccount bankAccount
 * @param value value
 * @param pin pin
 * @param completionHandler callback
 */
+ (void)makeWithdrawalForAccount:(PPBankAccount *)bankAccount value:(NSDecimalNumber *)value pin:(NSString *)pin completionHandler:(void (^)(PPWithdrawal* _Nullable withdrawal, NSError * _Nullable error)) callback{
    
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:bankAccount.wsId, [value stringValue], nil] forKeys:[NSArray arrayWithObjects:@"bank_account_id", @"value", nil]]
    andHeaders:[NSDictionary dictionaryWithObjectsAndKeys:@"Password", pin, nil]
    serviceMethodUrl:kWsUrlMakeWithdrawal
    completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if (err) {
            callback(nil, err);
            return;
        }
        
        NSDictionary *data = [json valueForKey:@"data"];
        NSDictionary *wsError = [json valueForKey:@"Error"];
        if (data) {
            
            @try {
                //Balances
                ConsumerManager.shared.consumer.balance = [NSDecimalNumber decimalNumberWithString:[data valueForKey:@"balance"]];
                ConsumerManager.shared.consumer.withdrawBalance = [NSDecimalNumber decimalNumberWithString:[data valueForKey:@"withdraw_balance"]];
            }
            @catch (NSException *exception) {
                
            }
            
            PPWithdrawal * withdrawal = [[PPWithdrawal alloc] initWithDictionary:data];
            callback(withdrawal, nil);
            return;
            
        }else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(nil, error);
            return;
        }
        
        
    }];
}

/*!
 * getLastWithdrawal.
 * Callback:
 *   - @b PPWithdrawal => PPWithdrawal
 *   - @b NSError => error
 * @param completionHandler callback
 */
+ (void)getLastWithdrawal:(void (^)(PPWithdrawal *withdrawal, AvailableWithdrawalModel *availableWithdrawal, PPBank * _Nullable bankAccount, NSError * error)) callback{
    [WebServiceInterface dictionaryWithPostVars:@{@"": @""} serviceMethodUrl:kWsUrlGetLastWithdrawal completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(nil, nil, nil, err);
            return;
        }
        
        NSDictionary *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        AvailableWithdrawalModel* availableWithdrawal;
        
        if (data) {
            
            @try {
                NSDecimalNumber *balance;
                NSDecimalNumber *promotional;
                NSDecimalNumber *withdrawalBalance;
                
                 //Balances
               if ([data valueForKey:@"balance"] != nil) {
                   balance = [NSDecimalNumber decimalNumberWithString:[data valueForKey:@"balance"]];
                   ConsumerManager.shared.consumer.balance = balance;
               }
               if ([data valueForKey:@"promotional_balance"] != nil) {
                   promotional = [NSDecimalNumber decimalNumberWithString:[data valueForKey:@"promotional_balance"]];
               }
               if ([data valueForKey:@"withdraw_balance"] != nil) {
                   withdrawalBalance = [NSDecimalNumber decimalNumberWithString:[data valueForKey:@"withdraw_balance"]];
                   ConsumerManager.shared.consumer.withdrawBalance = withdrawalBalance;
               }
               availableWithdrawal = [[AvailableWithdrawalModel alloc] initWithBalance:balance promotional:promotional withdrawal: withdrawalBalance];
            }
            @catch (NSException *exception) {
                
            }
            
            @try {
                if ([data valueForKey:@"Withdrawal"] != [NSNull null]) {
                    
                    PPWithdrawal * withdrawal = [[PPWithdrawal alloc] initWithDictionary:data];
                    
                    PPBank * bank;
                    if ([data valueForKey:@"Bank"] != nil && [data valueForKey:@"Bank"] != [NSNull null] && [[data valueForKey:@"Bank"] isKindOfClass:[NSDictionary class]]) {
                        bank = [[PPBank alloc] initWithDictionary:[data valueForKey:@"Bank"]];
                    }
                    
                    callback(withdrawal, availableWithdrawal, bank, err);
                    return;
                }
            }
            @catch (NSException *exception) {
                //last withdrawal is nil
            }
            
            
        } else {
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(nil, nil, nil, error);
            return;
        }
        
        callback(nil, availableWithdrawal, nil, err);
        return;
    }];
}

# pragma mark Recargar

/*!
 * Retorna a ultima recarga efetuada.
 * Callback:
 *   - @b Recharge => recarga
 *   - @b NSError => error
 * @param completionHandler callback
 */
+ (void)getLastRecharge:(void (^)(Recharge *recharge, NSMutableArray *availableRechargeMethods, NSError * error)) callback{
    
    [WebServiceInterface dictionaryWithPostVars:@{@"": @""} serviceMethodUrl:kWsUrlGetLastRecharge completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(nil, nil, err);
            return;
        }
        
        NSString *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        if (data) {
            @try {
                
                NSMutableArray *availableRechargeMethods = [[NSMutableArray alloc] init];
                
                if([data valueForKey:@"AvailableRechargeMethods"] != NULL){
                    for (NSArray *availableRechargeMethodJson in [data valueForKey:@"AvailableRechargeMethods"]) {
                        RechargeMethod* rechargeMethod = [RechargeMethod createFrom:availableRechargeMethodJson];
                        if (rechargeMethod != nil) {
                            [availableRechargeMethods addObject:rechargeMethod];
                        }
                    }
                }
                
                Recharge *lastRecharge = nil;
                
                @try {
                    if((NSNull *)[data valueForKey:@"Recharge"] != [NSNull null]) {
                        lastRecharge = [Recharge createFrom:[data valueForKey:@"Recharge"]];
                    }
                } @catch (NSException *exception) {
                    lastRecharge = nil;
                }
                
                callback(lastRecharge, availableRechargeMethods, err);
                return;
            }
            @catch (NSException *exception) {
                DebugLog(@">>> %@", exception);
            }
        }else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(nil, nil, error);
            return;
        }
        
        callback(nil, nil, nil);
        return;
    }];
}


/*!
 * efetua uma recara recarga.
 * Callback:
 *   - @b Recharge => recarga
 *   - @b NSError => error
 * @param rechargeType rechargeType
 * @param value value
 * @param destinationBank destinationBank
 * @param completionHandler callback
 */
+ (void)makeRecharge:(RechargeMethod *)rechargeMethod value:(NSDecimalNumber *)value securityRequest:(NSString *)securityRequest completionHandler:(void (^)(Recharge * recharge, NSError * error)) callback{
    
    [WebServiceInterface dictionaryWithPostVars:@{@"value": [value stringValue], @"recharge_type_id":@(rechargeMethod.typeId), @"bank_id": rechargeMethod.bankId, @"openbank_security_response": securityRequest, @"openbank_api_version": @"v2" } serviceMethodUrl:kWsUrlMakeRecharge completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(nil, err);
            return;
        }
        
        NSArray *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        if (data) {
            
            NSError *fakeTokenError;
            if ((NSNull *)[data valueForKey:@"security_request"] != [NSNull null] && [[data valueForKey:@"security_request"] length]) {
                NSInteger securityRequest = [[data valueForKey:@"security_request"] isEqualToString:@"token"] ? WSConsumerMakeRechargeOpenBankToken : WSConsumerMakeRechargeOpenBankPush;
                
                if ((NSNull *)[data valueForKey:@"security_message"] != [NSNull null] && [[data valueForKey:@"security_message"] length]) {
                    fakeTokenError = [WebServiceInterface errorWithTitle:[data valueForKey:@"security_message"] code:securityRequest];
                }
            }
            
            Recharge *recharge = nil;
            if([data valueForKey:@"Recharge"] != NULL) {
                recharge = [Recharge createFrom:[data valueForKey:@"Recharge"]];
            }
            callback(recharge, fakeTokenError ? fakeTokenError : err);
            return;
            
        }else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(nil, error);
            return;
        }
    }];
    
}

/*!
 * Cancela uma recarga.
 *
 * Callback:
 *
 *   - @b Recharge => recarga
 *
 *   - @b NSError => error
 * @param rechargeId id da recarga
 * @param completionHandler callback
 */
+ (void)cancelRecharge:(NSString *)rechargeId completionHandler:(void (^)(Recharge * recharge, NSError * error)) callback{
    
    [WebServiceInterface dictionaryWithPostVars:@{@"recharge_id": rechargeId} serviceMethodUrl:kWsUrlCancelRecharge completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(nil, err);
            return;
        }
        
        NSString *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        if (data) {
            
            Recharge *recharge = nil;
            if((NSNull *)[data valueForKey:@"Recharge"] != [NSNull null]) {
                recharge = [Recharge createFrom:[data valueForKey:@"Recharge"]];
            }
            callback(recharge, err);
            return;
            
        }else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(nil, error);
            return;
        }
    }];
}

/**
 Efetua o upload da imagem de recarga
 
 @param rechargeId recharde id
 @param image image
 @param callback completion handle
 */
+ (void)uploadRechargePhoto:(NSString *)rechargeId image:(UIImage *)image completionHandler:(void (^)(Recharge *recharge, NSError *error))callback {
    UIImage *imageForUpload = [image resizeImageWithTargetSize:CGSizeMake(1200, 1200)];
    if (imageForUpload == nil) {
        PicPayError *error = [[PicPayError alloc] initWithMessage:@"Não foi possível usar a imagem selecionada"
                                                             code:@"-1"
                                                            title:nil
                                                       dictionary:nil];
        callback(nil, error);
        return;
    }
    NSData *imageData = UIImageJPEGRepresentation(imageForUpload, 0.8f);
    if (imageData == nil) {
        PicPayError *error = [[PicPayError alloc] initWithMessage:@"Não foi possível usar a imagem selecionada"
                                                             code:@"-1"
                                                            title:nil
                                                       dictionary:nil];
        callback(nil, error);
        return;
    }
    [WebServiceInterface uploadImage:imageData
                    serviceMethodUrl:kWsUrlReceiveDepositRechargeProof
                            postVars:@{@"recharge_id": rechargeId}
                   completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
                       if (err) {
                           callback(nil, err);
                           return;
                       }
                       
                       NSString *data = [json valueForKey:@"data"];
                       NSArray *wsError = [json valueForKey:@"Error"];
                       if (data) {
                           Recharge *recharge = nil;
                           if((NSNull *)[data valueForKey:@"Recharge"] != [NSNull null]) {
                               recharge = [Recharge createFrom:[data valueForKey:@"Recharge"]];
                           }
                           callback(recharge, err);
                           return;
                       } else {
                           NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
                           callback(nil, error);
                           return;
                       }
                   } progressCallback:nil];
}

# pragma mark Validação Telefone

/*!
 * Solicita ao WS que envie um SMS para o telefone com o código de verificação.
 * Callback:
 *   - @b NSError => erro
 * @param ddd ddd
 * @param number numero do telefone
 * @param completionHandler callback
 */
+ (void)sendSmsVerification:(NSString *)ddd phone:(NSString *)number completionHandler:(void (^)(NSInteger smsDelay, BOOL isCallEnable, NSInteger callDelay, NSError *error))callback{
    
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:ddd, number, nil] forKeys:[NSArray arrayWithObjects:@"ddd",@"number", nil]] serviceMethodUrl:kWsUrlSendSmsVerification completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(0,NO, 0, err);
            return;
        }
        
        NSString *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        
        if (data) {
            BOOL isCallEnable = [[NSString stringWithFormat:@"%@",[data valueForKey:@"call_enable"] ] boolValue];
            NSInteger callDelay = [[NSString stringWithFormat:@"%@",[data valueForKey:@"call_delay"] ] integerValue];
            NSInteger smsDelay = [[NSString stringWithFormat:@"%@",[data valueForKey:@"sms_delay"] ] integerValue];
            
            if(!wsError){
                callback(smsDelay, isCallEnable, callDelay, nil);
            }else{
                NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
                callback(smsDelay, isCallEnable, callDelay, error);
            }
            
            return;
        }else{
            BOOL isCallEnable = [[NSString stringWithFormat:@"%@",[[wsError valueForKey:@"data"] valueForKey:@"call_enable"] ] boolValue];
            NSInteger callDelay = [[NSString stringWithFormat:@"%@",[[wsError valueForKey:@"data"] valueForKey:@"call_delay"] ] integerValue];
            NSInteger smsDelay = [[NSString stringWithFormat:@"%@",[[wsError valueForKey:@"data"] valueForKey:@"sms_delay"] ] integerValue];
            
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(smsDelay, isCallEnable, callDelay, error);
            return;
        }
    }];
}

/*!
 * Solicita ao WS que ligue para o telefone e informe o código de verificação.
 * Callback:
 *   - @b NSError => erro
 * @param ddd ddd
 * @param number numero do telefone
 * @param completionHandler callback
 */
+ (void)callVerification:(NSString *)ddd phone:(NSString *)number completionHandler:(void (^)(NSInteger smsDelay, BOOL isCallEnable, NSInteger callDelay, NSError *error))callback{
    
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:ddd, number, nil] forKeys:[NSArray arrayWithObjects:@"ddd",@"number", nil]] serviceMethodUrl:kWsUrlcallVerification completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(0,NO, 0, err);
            return;
        }
        
        NSString *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        if (data) {
            BOOL isCallEnable = [[NSString stringWithFormat:@"%@",[data valueForKey:@"call_enable"] ] boolValue];
            NSInteger callDelay = [[NSString stringWithFormat:@"%@",[data valueForKey:@"call_delay"] ] integerValue];
            NSInteger smsDelay = [[NSString stringWithFormat:@"%@",[data valueForKey:@"sms_delay"] ] integerValue];
            
            if(!wsError){
                callback(smsDelay, isCallEnable, callDelay, nil);
            }else{
                NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
                callback(smsDelay, isCallEnable, callDelay, error);
            }
            
            return;
        }else{
            BOOL isCallEnable = [[NSString stringWithFormat:@"%@",[[wsError valueForKey:@"data"] valueForKey:@"call_enable"] ] boolValue];
            NSInteger callDelay = [[NSString stringWithFormat:@"%@",[[wsError valueForKey:@"data"] valueForKey:@"call_delay"] ] integerValue];
            NSInteger smsDelay = [[NSString stringWithFormat:@"%@",[[wsError valueForKey:@"data"] valueForKey:@"sms_delay"] ] integerValue];
            
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(smsDelay, isCallEnable, callDelay, error);
            return;
        }
    }];
}

/*!
 * Verifica o telefone.
 * Callback:
 *   - @b NSString => verifiedNumber número verificado
 *   - @b NSError => erro
 * @param code código de validação
 * @param completionHandler callback
 */
+ (void)verifyPhoneNumber:(NSString *)code completionHandler:(void (^)(NSString *ddd, NSString *number, NSError *error))callback{
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:code, nil] forKeys:[NSArray arrayWithObjects:@"verification_code", nil]] serviceMethodUrl:kWsUrlVerifyPhoneNumber completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(nil, nil, err);
            return;
        }
        
        NSString *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        if (data) {
            
            NSString *phone = [NSString stringWithFormat:@"%@",[[data valueForKey:@"PhoneNumberVerifications"] valueForKey:@"number"]];
            NSString *ddd = [NSString stringWithFormat:@"%@", [[data valueForKey:@"PhoneNumberVerifications"] valueForKey:@"area_code"]];
            
            callback(ddd, phone, err);
            return;
        }else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(nil, nil, error);
            return;
        }
        
        return;
    }];
    
}

# pragma mark Validation

/*!
 * Check if the username is avaliable for a new account.
 * Callback:
 *   - @b NSError => error
 * @param username for validation
 * @param completionHandler callback
 */
+ (void)isValidName:(NSString *)name completionHandler:(void (^)(NSError *error))callback{
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:name, nil] forKeys:[NSArray arrayWithObjects:@"name", nil]] serviceMethodUrl:kWsUrlIsValidName completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(err);
            return;
        }
        
        NSString *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        if (data && wsError == nil) {
            callback(nil);
            return;
        }else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(error);
            return;
        }
        
        return;
    }];
}

/*!
 * Check if the password  is avaliable for a new account.
 * Callback:
 *   - @b NSError => error
 * @param password for validation
 * @param completionHandler callback
 */
+ (void)isValidPass:(NSString *)password completionHandler:(void (^)(NSError *error))callback{
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:password, nil] forKeys:[NSArray arrayWithObjects:@"pass", nil]] serviceMethodUrl:kWsUrlIsValidPassword completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(err);
            return;
        }
        
        NSString *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        if (data && wsError == nil) {
            callback(nil);
            return;
        }else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(error);
            return;
        }
        
        return;
    }];
}

# pragma mark Account Configs

+ (void)getAccountConfig:(NSString *)name completionHandler:(void (^)(NSArray *data, NSError *error))callback{
    [WebServiceInterface dictionaryWithPostVars:@{@"name": name} serviceMethodUrl:kWsUrlGetAccountConfig completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(nil, err);
            return;
        }
        
        callback(json, err);
    }];
}

+ (void)setAccountConfig:(NSString *)name params:(NSDictionary *)params completionHandler:(void (^)(NSArray *data, NSError *error))callback{
    
    NSMutableDictionary *newParams = [NSMutableDictionary dictionaryWithDictionary:params];
    [newParams addEntriesFromDictionary:@{@"name": name}];
    
    [WebServiceInterface dictionaryWithPostVars:newParams serviceMethodUrl:kWsUrlSetAccountConfig completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(nil, err);
            return;
        }
        
        callback(json, err);
    }];
}

# pragma mark Contatos

/*!
 * Solicita ao WS que convide os contatos informados.
 * Callback:
 *   - @b BOOL => success
 *   - @b NSError => erro
 * @param phones lista de telefones
 * @param emails emails lista de emails
 * @param completionHandler callback
 */
+ (void)inviteContactsWithPhones:(NSArray *)phones emails:(NSArray *)emails completionHandler:(void (^)(BOOL success, NSError *error))callback{
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: phones, emails, nil] forKeys:[NSArray arrayWithObjects: @"telefone",@"email", nil]] serviceMethodUrl:kWsUrlInviteAddressbook completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        NSString *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        if (data) {
            callback(YES, err);
            return;
        }else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(NO, error);
            return;
        }
    }];
    
}

# pragma mark Outros

/*!
 * Ping no WS.
 * Callback:
 *   - @b NSError => erro
 * @param completionHandler callback
 */
+ (void)ping:(void (^)(NSError *error))callback{
    /*	[WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"1", nil] forKeys:[NSArray arrayWithObjects:@"askforreview", nil]] serviceMethodUrl:kWsUrlPing completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error) {
     callback(error);
     }];*/
    //	[WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"1", nil] forKeys:[NSArray arrayWithObjects:@"enablemgm", nil]] serviceMethodUrl:kWsUrlPing error:&err];
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: @"",nil] forKeys:[NSArray arrayWithObjects: @"",nil]] serviceMethodUrl:kWsUrlPing completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error) {
        callback(error);
    }];
}

/*!
 * Envia a classificação do App para WS.
 * Callback:
 *   - @b NSError => erro
 * @param stars numero de estrelas
 * @param completionHandler callback
 */
+ (void)reviewRating:(int)stars completionHandler:(void (^)(NSError *error))callback{
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%d",stars], nil] forKeys:[NSArray arrayWithObjects:@"stars", nil]] serviceMethodUrl:kWsUrlReviewRating completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error) {
        callback(error);
    }];
}


/*!
 * logMgmRequest
 * Callback:
 *   - @b NSError => erro
 * @param touchOrigin touchOrigin
 * @param completionHandler callback
 */
+ (void)logMgmRequest:(PPLogMgmTouchOrigin)touchOrigin completionHandler:(void (^)(NSError *error))callback{
    
    NSString *clickedFrom;
    
    switch (touchOrigin) {
        case PPLogMgmTouchOriginFeed:
            clickedFrom = @"feed";
            break;
            
        case PPLogMgmTouchOriginContacts:
            clickedFrom = @"contacts";
            break;
            
        case PPLogMgmTouchOriginNotification:
            clickedFrom = @"notification";
            break;
            
        case PPLogMgmTouchOriginProfile:
            clickedFrom = @"profile";
            break;
            
        case PPLogMgmTouchOriginSelectContacts:
            clickedFrom = @"selecionar_contatos";
            break;
            
        case PPLogMgmTouchOriginSelectShareLink:
            clickedFrom = @"selecionar_compartilhar";
            break;
            
        case PPLogMgmTouchOriginWhatsapp:
            clickedFrom = @"whatsapp";
            break;
            
        case PPLogMgmTouchOriginFacebook:
            clickedFrom = @"facebook";
            break;
            
        case PPLogMgmTouchOriginTwitter:
            clickedFrom = @"twitter";
            break;
            
        case PPLogMgmTouchOriginCopyLink:
            clickedFrom = @"link";
            break;
            
        case PPLogMgmTouchOriginSidebar:
            clickedFrom = @"sidebar";
            break;
            
        default:
            clickedFrom = @"none";
            break;
    }
    
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: clickedFrom, nil] forKeys:[NSArray arrayWithObjects: @"clicked_from", nil]] serviceMethodUrl:kWsUrlLogMgmRequest completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error) {
        if (callback) {
            callback(error);
        }
    }];
}

# pragma mark Não Utilizados

/*!
 * markNotificationsAsRead.
 * Callback:
 *   - @b NSError => error
 * @param completionHandler callback
 */
+ (void)markNotificationsAsRead:(void (^)(NSError * error)) callback{
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray array] forKeys:[NSArray array]] serviceMethodUrl:kWsUrlReadNotifications completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *error) {
        [AppManager.shared setUnreadNotificationsCount:0];
    }];
}

/*!
 * Delete a license Plate
 *
 * Callback:
 *
 *   - @b BOOL => success
 *
 *   - @b NSError => erro
 *
 * @param licensePlate license Plate
 * @param completionHandler callback
 */
+ (void) removeLicensePlate:(NSString * )licensePlate completionHandler:(void (^_Nonnull)(BOOL success, NSError * _Nullable error)) callback{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:licensePlate forKey:@"license_plate"];
    
    [WebServiceInterface dictionaryWithPostVars:data serviceMethodUrl:kWsUrlRemoveLicensePlate completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(NO, err);
            return;
        }
        NSArray *wsError = [json valueForKey:@"Error"];
        NSDictionary *data = [json valueForKey:@"data"];
        
        if (data != nil) {
            
            callback(YES, nil);
            return;
        }else{
            NSError *error;
            error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] integerValue]];
            
            callback(NO, error);
            return;
        }
        
    }];
}

/**
 Send notification to user for change to picpay pro
 
 @param payload payload of error
 @param completion completion
 */
+ (void)sendNotificationPicPayProWith:(NSDictionary *)payload completion:(void (^_Nonnull)(BOOL success, NSError * _Nullable error)) completion {
    [WebServiceInterface dictionaryWithPostVars:payload serviceMethodUrl:kWSUrlPicPayProPostNotification completionHandler:^(NSArray *jsonData, NSURLResponse *response, NSData *responseData, NSError *error) {
        if (error) {
            completion(NO, error);
        } else if ([jsonData valueForKey:@"data"]) {
            completion(YES, nil);
        } else {
            NSArray *wsError = [jsonData valueForKey:@"Error"];
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] integerValue]];
            completion(NO, error);
        }
    }];
}

@end
