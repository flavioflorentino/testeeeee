//
//  WSAppVersion.h
//
//  Created by Diogo Carneiro on 04/07/12
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceInterface.h"


@interface WSAppVersion : NSObject

/*!
 * Verfica se app necessita ser atualizado
 *
 * Callback:
 *
 *   - @b BOOL => needUpdate
 *
 *   - @b NSError => erro
 *
 *   - @b NSString => message
 *
 * @param callback
 */
+ (void)needUpdateAppVersion:(void (^)(BOOL needUpdate,NSString *message, NSError *error)) callback;

@end