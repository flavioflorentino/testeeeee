//
//  WSTransaction.m
//  MoBuy
//
//  Created by Joao victor V lana on 19/08/12.
//
//

#import "WSTransaction.h"
#import "PPAuth.h"
#import "PicPay-Swift.h"
@import CoreLegacy;

@interface WSTransaction()
@end

@implementation WSTransaction
PciPavService *service;

# pragma mark Create Methods
/*!
 * Cria uma nova transação
 * Callback:
 *   - @b MBTransaction => Transação
 *   - @b NSError => erro
 * @param args dados da transação
 * @param origin Origem
 * @param completionHandler callback
 */
+ (void)createTransaction:(NSArray *)args withOrigin:(NSString *)origin withAdditionalInfo: (NSDictionary *)additionalInfo completionHandler:(void (^)(MBTransaction *transaction, NSError *error)) callback{
    [WSTransaction createTransaction:args withCvv:nil withOrigin:origin withAdditionalInfo:additionalInfo withExtra:nil completionHandler:callback];
}

/*!
 * Cria uma nova transação
 * Callback:
 *   - @b MBTransaction => Transação
 *   - @b NSError => erro
 * @param args dados da transação
 * @param origin Origem
 * @param completionHandler callback
 */
+ (void)createTransaction:(NSArray *)args withCvv:(NSString *)cvv withOrigin:(NSString *)origin withAdditionalInfo:(NSDictionary *)additionalInfo withExtra:(NSDictionary *)extra completionHandler:(void (^)(MBTransaction *transaction, NSError *error)) callback{
    service = [PciPavService new];
    
    NSString *pin           = [args objectAtIndex:0];
    NSString *sellerId      = [args objectAtIndex:1];
    NSString *planType      = [args objectAtIndex:2];
    NSString *total         = [args objectAtIndex:3];
    NSString *shipping      = [args objectAtIndex:4];
	NSString *shippingId    = [args objectAtIndex:5];
    NSString *creditCardId  = [args objectAtIndex:6];
    NSString *addressId     = [args objectAtIndex:7];
    NSString *parcelas		= [args objectAtIndex:8];
	NSString *credit		= [args objectAtIndex:9];
    NSMutableArray *items   = [args objectAtIndex:10];
	NSString *surcharge		= [args objectAtIndex:11];
	NSString *from_explore;
	NSString *someErrorOccurredString;
	
	@try {
		from_explore= [args objectAtIndex:12];
	}
	@catch (NSException *exception) {
		from_explore	= @"";
	}
	
	@try {
		someErrorOccurredString = [args objectAtIndex:13];
	}
	@catch (NSException *exception) {
		someErrorOccurredString	= @"";
	}
	
	
	NSString *biometryString = [args objectAtIndex:14];
	
	NSString *bestGpsAcc = [args objectAtIndex:15];
	NSString *bestGpsLat = [args objectAtIndex:16];
	NSString *bestGpsLng = [args objectAtIndex:17];
	
	NSString *licensePlate;
	
	@try {
		licensePlate = [args objectAtIndex:18];
	}
	@catch (NSException *exception) {
		licensePlate	= @"";
	}
	
	NSString *privacyConfig;
	@try {
		privacyConfig = [args objectAtIndex:19];
	}
	@catch (NSException *exception) {
		privacyConfig = @"2";
	}
    
    if (additionalInfo == nil) {
        additionalInfo = @{};
    }
    
	//NSError *err;
	
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjects:[NSArray arrayWithObjects:pin, sellerId, planType, total, shipping, shippingId, creditCardId, addressId, parcelas, credit, items, surcharge, from_explore, someErrorOccurredString, origin, additionalInfo, biometryString, bestGpsLat, bestGpsLng, bestGpsAcc, licensePlate, [NSNumber numberWithBool:![ConsumerManager useBalance]], privacyConfig, nil] forKeys:[NSArray arrayWithObjects:@"pin", @"seller_id", @"plan_type", @"total", @"shipping", @"shipping_id", @"credit_card_id", @"address_id", @"parcelas", @"credit" ,@"itens", @"surcharge", @"from_explore", @"duplicated", @"origin", @"additional_info", @"biometry", @"lat", @"lng", @"gps_acc", @"license_plate", @"ignore_balance", @"feed_visibility", nil]];
    
    if (extra!=nil){
        [dict addEntriesFromDictionary:extra];
    }
    
    __weak typeof(self) weakSelf = self;
    if (service.paymentIsPci) {
        [service createTransactionWithCvv:cvv password:pin request:dict valueCard:[total doubleValue] onSuccess:^(NSDictionary<NSString *,id> * result) {
            if (weakSelf) {
                __strong typeof(weakSelf) strongSelf = weakSelf;
                [strongSelf requestSuccess:result pin:pin completionHandler:callback];
            }
        } onError:^(NSError *error) {
            callback(nil, error);
        }];
    } else {
        [WebServiceInterface dictionaryWithPostVars:dict serviceMethodUrl:kWsUrlCreateTransaction completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
            if (weakSelf) {
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if(err) {
                    callback(nil, err);
                    return;
                }
                
                [NSNumber numberWithBool:![ConsumerManager useBalance]];
                
                NSDictionary *data = [json valueForKey:@"data"];
                NSDictionary *wsError = [json valueForKey:@"Error"];
                
                if (data) {
                    [strongSelf requestSuccess:data pin:pin completionHandler:callback];
                }else{
                    [strongSelf requestError:wsError completionHandler:callback];
                }
            }
        }];
    }
}

+ (void)requestSuccess:(NSDictionary *)data pin:(NSString *)pin completionHandler:(void (^_Nullable)(MBTransaction * _Nullable transaction, NSError * _Nullable error)) callback {
   [PPAuth updateBiometricAuthenticationTokenIfNeed:pin];
   
   NSInteger status = [[data valueForKey:@"authorized"] integerValue];
   if(status == 1) {
       MBTransaction *transaction = [[MBTransaction alloc] init];
       NSArray *transactionJson = [data valueForKey:@"Transaction"];
       
       
       //saving plan_type on transaction
       transaction.plan_type = [[MBPlanType alloc] init];
       [transaction.plan_type setPlanTypeId:[[transactionJson valueForKey:@"plan_type"] characterAtIndex:0]];
       
       
       transaction.seller_name = [[data valueForKey:@"Seller"] valueForKey:@"name"];
       transaction.status = [[data valueForKey:@"Status"] valueForKey:@"name"];
       
       @try {
           transaction.quantidadeParcelas = [[transactionJson valueForKey:@"quant_parcelas"] integerValue];
       }
       @catch (NSException *exception) {
           transaction.quantidadeParcelas = 1;
       }
       
       @try {
           transaction.receiptCategory = [[data valueForKey:@"receipt_category"] integerValue];
       }
       @catch (NSException *exception) {
           transaction.receiptCategory = 1; // Normal
       }
       NSDictionary *categoryData = [data valueForKey:@"category_data"];
       [WSTransaction populateTransactionReceiptData:transaction withData:categoryData];
       
       transaction.valorParcela = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",[transactionJson valueForKey:@"valor_parcela"]]];
       transaction.surcharge = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",[transactionJson valueForKey:@"surcharge"]]];
       transaction.subtotal = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",[transactionJson valueForKey:@"site_value"]]];
       transaction.total = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",[transactionJson valueForKey:@"site_value"]]];
       transaction.credit_picpay = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",[transactionJson valueForKey:@"credit_picpay"]]];
       transaction.credit_seller = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",[transactionJson valueForKey:@"credit_seller"]]];
       
       @try {
           transaction.chargedFromCreditCard = [NSDecimalNumber decimalNumberWithString:[transactionJson valueForKey:@"total_value"]];
       }
       @catch (NSException *exception) {
           transaction.chargedFromCreditCard = [NSDecimalNumber decimalNumberWithString:@"0"];
       }
       
       @try {
           transaction.seller_logo_url = [NSString stringWithFormat:@"%@", [[data valueForKey:@"Seller"] valueForKey:@"img_url"]];
       }
       @catch (NSException *exception) {
           transaction.seller_logo_url = nil;
       }
       
       
       if ([transaction.plan_type isKindOfPlan:'E']) {
           if (transaction.surcharge != [NSDecimalNumber notANumber]) {
               transaction.total = [transaction.subtotal decimalNumberByAdding:transaction.surcharge];
           }
           
           transaction.event_name = [NSString stringWithFormat:@"%@", [[transactionJson valueForKey:@"additional_info"] valueForKey:@"event_name"]];
           transaction.event_date = [NSString stringWithFormat:@"%@", [[transactionJson valueForKey:@"additional_info"] valueForKey:@"event_date"]];
           transaction.event_location = [NSString stringWithFormat:@"%@", [[transactionJson valueForKey:@"additional_info"] valueForKey:@"event_location"]];
           transaction.event_opening = [NSString stringWithFormat:@"%@", [[transactionJson valueForKey:@"additional_info"] valueForKey:@"event_opening"]];
           transaction.event_ticket_text = [NSString stringWithFormat:@"%@", [[transactionJson valueForKey:@"additional_info"] valueForKey:@"ticket_text"]];
           transaction.show_tickets = [[transactionJson valueForKey:@"show_tickets"] boolValue];
           transaction.show_tickets_error = [NSString stringWithFormat:@"%@", [transactionJson valueForKey:@"show_tickets_error"]];
       }else if ([transaction.plan_type isKindOfPlan:'A']){
           @try {
               PPRewardProgram *rewardProgram = [[PPRewardProgram alloc] init];
               rewardProgram.progress = [[[data valueForKey:@"RewardProgramInfo"] valueForKey:@"progress"] intValue];
               rewardProgram.progressDescription = [[data valueForKey:@"RewardProgramInfo"] valueForKey:@"progress_description"];
               rewardProgram.programDescription = [[data valueForKey:@"RewardProgramInfo"] valueForKey:@"reward_program_description"];
               rewardProgram.progressBarMin = [[data valueForKey:@"RewardProgramInfo"] valueForKey:@"progressbar_min"];
               rewardProgram.progressBarMax = [[data valueForKey:@"RewardProgramInfo"] valueForKey:@"progressbar_max"];
               transaction.rewardProgram = rewardProgram;
           } @catch (NSException *exception) {
               //item does not have reward program
           }
       }
       
       transaction.WsId = [[transactionJson valueForKey:@"id"] integerValue];
       transaction.justCreated = YES;
       transaction.usedCreditCard = [transactionJson valueForKey:@"credit_card"];
       
       NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
       [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
       [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
       transaction.date = [dateFormatter dateFromString: [transactionJson valueForKey:@"transaction_date"]];
       
       if (transaction.plan_type.needAddress) {
           [self transactionNeedAddress:transaction json:transactionJson];
       }
       
       transaction.items = [[NSMutableArray alloc] init];
       for (NSArray *itemJson in [data valueForKey:@"TransactionItem"]) {
           MBItem *transactionItem = [[MBItem alloc] init];
           transactionItem.name = [[NSString alloc] initWithFormat:@"%@ %@",[itemJson valueForKey:@"name"],[itemJson valueForKey:@"option_variant_names"]];
           transactionItem.price = [NSDecimalNumber decimalNumberWithString:[itemJson valueForKey:@"price"]];
           transactionItem.quantity = [[itemJson valueForKey:@"amount"] intValue];
           transactionItem.option_variant_names = [itemJson valueForKey:@"option_variant_names"];
           
           if ([transaction.plan_type isKindOfPlan:'E']) {
               transactionItem.ticket_code = [NSString stringWithFormat:@"%@", [[itemJson valueForKey:@"additional_info"] valueForKey:@"ticket_code"]];
               transactionItem.ticket_datamatrix_url = [NSString stringWithFormat:@"%@", [itemJson valueForKey:@"ticket_datamatrix_url"]];
               
               @try {
                   transactionItem.ticket_sent_to = [NSString stringWithFormat:@"%@", [[(NSArray *)[itemJson valueForKey:@"SentTicket"] objectAtIndex:0] valueForKey:@"sent_to"]];
               }
               @catch (NSException *exception) {
                   transactionItem.ticket_sent_to = @"";
               }
               
           }
           
           [transaction.items addObject:transactionItem];
       }
       
       NSString *statusId = [[data valueForKey:@"Status"] valueForKey:@"id"];
       if ([statusId isEqualToString:@"P"] || [statusId isEqualToString:@"C"]) {
           transaction.isCancelled = NO;
       }else{
           transaction.isCancelled = YES;
       }
       
       @try {
           transaction.shareText = [[data valueForKey:@"Share"] valueForKey:@"text"];
           transaction.shareUrl = [[data valueForKey:@"Share"] valueForKey:@"url"];
           
           if ((NSNull *)transaction.shareText == [NSNull null] || transaction.shareText.length == 0) {
               transaction.shareText = nil;
           }
       }
       @catch (NSException *exception) {
           transaction.shareText = nil;
       }
       
       @try {
           transaction.receiptText = [data valueForKey:@"receipt_text"];
           
           if ((NSNull *)transaction.receiptText == [NSNull null] || transaction.receiptText.length == 0) {
               transaction.receiptText = nil;
           }
       }
       @catch (NSException *exception) {
           transaction.receiptText = nil;
       }
       
       @try {
           NSMutableArray *widgets = [[NSMutableArray alloc] init];
           NSArray *receipt = [data valueForKey:@"receipt"];
           for (NSDictionary *item in receipt) {
               [widgets addObject:[WSReceipt createReceiptWidgetItemWithJsonDict:item]];
           }
           
           if ([widgets count] > 0) {
               transaction.receiptItems = widgets;
           } else {
               transaction.receiptItems = nil;
           }
       }
       @catch (NSException *exception) {
           transaction.receiptItems = nil;
       }
       
       @try {
           if ([data valueForKey:@"ThirdPartyData"] != nil && [data valueForKey:@"ThirdPartyData"] != [NSNull null] ) {
               transaction.thirdPartyData = [data valueForKey:@"ThirdPartyData"];
           }
       }
       @catch (NSException *exception) {
           transaction.thirdPartyData = nil;
       }
       
       callback(transaction, nil);
       return;
   } else {
       NSError *error = [WebServiceInterface errorWithTitle:@"Pedido não autorizado." code:2];
       callback(nil, error);
       return;
   }
}

+ (void)requestError:(NSDictionary *)wsError completionHandler:(void (^_Nullable)(MBTransaction * _Nullable transaction, NSError * _Nullable error)) callback {
      NSError *error;
      @try {
          error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] integerValue]];
      } @catch (NSException *exception) {
          error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:(int)[wsError valueForKey:@"id"]];
      }
      callback(nil, error);
      return;
}

+ (void)transactionNeedAddress:(MBTransaction *)transaction json:(NSArray *)transactionJson{
    NSString *numero = [transactionJson valueForKey:@"billing_numero"];
    NSString *logradouro = [transactionJson valueForKey:@"billing_logradouro"];
    NSString *complemento = [transactionJson valueForKey:@"billing_complemento"];
    NSString *cidade = [transactionJson valueForKey:@"billing_cidade"];
    NSString *bairro = [transactionJson valueForKey:@"billing_bairro"];
    NSString *cep = [transactionJson valueForKey:@"billing_cep"];
    NSString *uf = [transactionJson valueForKey:@"billing_uf"];
    
    if (complemento.length) {
        transaction.shippingAddress = [NSString stringWithFormat:@"%@, %@. %@. %@, %@ - %@. CEP %@", logradouro, numero, complemento, bairro, cidade, uf, cep];
    } else {
        transaction.shippingAddress = [NSString stringWithFormat:@"%@, %@. %@, %@ - %@. CEP %@", logradouro, numero, bairro, cidade, uf, cep];
    }
    
    transaction.frete = [NSDecimalNumber decimalNumberWithString:[transactionJson valueForKey:@"shipping_value"]];
    
    if(transaction.frete) {
        transaction.subtotal        = [[NSDecimalNumber decimalNumberWithString:[transactionJson valueForKey:@"site_value"]] decimalNumberBySubtracting:transaction.frete];
    }
}

/*!
 * Cria uma nova transação P2P
 * Callback:
 *   - @b PPP2PTransaction => Transação
 *   - @b NSError => erro
 * @param args dados da transação
 * @param completionHandler callback
 */
+ (void)createP2PTransaction:(P2PTransactionRequest *)request completionHandler:(void (^)(PPP2PTransaction *transaction, NSError *error)) callback{
	
    NSDictionary* params = [request toParams];
    if (!params) {
        callback(nil, [WebServiceInterface errorWithTitle:@"Problema na requisição. Tente novamente" code:1043]);
        return;
    }
	[WebServiceInterface dictionaryWithPostVars: params serviceMethodUrl:kWsUrlCreateP2PTransaction completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(nil, err);
            return;
        }
        
        NSString *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        
        if (data) {
            // with the touch id needes update the saved pin the validate pin
            // sent to create transaction is saved
            [PPAuth updateBiometricAuthenticationTokenIfNeed:request.pin];
            
            NSInteger status = [[data valueForKey:@"authorized"] integerValue];
            if(status == 1) {
                
                PPP2PTransaction *transaction = [[PPP2PTransaction alloc] init];
                NSArray *transactionJson = [data valueForKey:@"P2PTransaction"];
                
                transaction.wsId = [NSString stringWithFormat:@"%@", [transactionJson valueForKey:@"id"]];
                
                @try {
                    transaction.sendedValue = [NSDecimalNumber decimalNumberWithString:[transactionJson valueForKey:@"consumer_value"]];
                }
                @catch (NSException *exception) {
                    transaction.sendedValue = [NSDecimalNumber decimalNumberWithString:@"0"];
                }
                
                @try {
                    NSMutableArray *widgets = [[NSMutableArray alloc] init];
                    NSArray *receipt = [data valueForKey:@"receipt"];
                    for (NSDictionary *item in receipt) {
                        [widgets addObject:[WSReceipt createReceiptWidgetItemWithJsonDict:item]];
                    }
                    
                    if ([widgets count] > 0) {
                        transaction.receiptItems = widgets;
                    } else {
                        transaction.receiptItems = nil;
                    }
                }
                @catch (NSException *exception) {
                    transaction.receiptItems = nil;
                }
                
                callback(transaction, err);
                return;
                
            } else {
                NSError *error = [WebServiceInterface errorWithTitle:@"Pedido não autorizado." code:2];
                callback(nil, error);
                return;
            }
        }else{
            NSError *error;
            @try {
                error = (NSError *)[WebServiceInterface picpayError:(NSDictionary*)wsError];
            }
            @catch (NSException *exception) {
                error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:(int)[wsError valueForKey:@"id"]];
            }
            
            callback(nil, error);
            return;
        }
    }];
}

# pragma mark Chat Methods

/*!
 * Populates the transactino with receipt category data
 * @apram transaction to populate
 * @param data from ws
 */
+ (void) populateTransactionReceiptData:(MBTransaction *)transaction withData:(NSDictionary *) categoryData{
    //NSDictionary *categoryData = [data valueForKey:@"category_data"];
    
    if(!categoryData){
        return;
    }
    
    // Parking data
    if(transaction.receiptCategory == PPReceiptCategoryParking){
        PPReceiptParking *receiptData= [[PPReceiptParking alloc] init];
        receiptData.licensePlate = [NSString stringWithFormat:@"%@",[categoryData valueForKey:@"license_plate"] ];
        receiptData.expiresDay = [NSString stringWithFormat:@"%@",[categoryData valueForKey:@"expires_day"] ];
        receiptData.expiresHour = [NSString stringWithFormat:@"%@",[categoryData valueForKey:@"expires_hour"] ];
        transaction.receiptCategoryData = receiptData;
    }
}


/*!
 * Check soft descriptor of card Visa and Master through AnalysisAcceleration screen
 * @param transaction_id ID of transaction
 * @param verification_code Code entered by the customer
 * @param action_type Type of action on screen
 * Callback:
 *   - @b NSError => error
 * @param completionHandler callback
 */
+ (void)setSoftDescriptorByActionType:(NSString *)transaction_id verification_code:(NSString *)verification_code action_type:(NSString *)action_type completionHandler:(void (^)(NSString *, NSError *))callback {
    
    NSString *method = [NSString stringWithFormat:@"%@", kWsUrlClearP2PTransactionOnHold ];
    NSDictionary *postVars = @{@"p2p_transaction_id": transaction_id, @"verification_code": verification_code};
    
    // If check by Pav transaction
    if([action_type isEqualToString:@"openPAVTransactionVerification"] || [action_type isEqualToString:@"pav_transaction"]){
        method = [NSString stringWithFormat:@"%@", kWsUrlClearPAVTransactionOnHold ];
        postVars = @{@"transaction_id": transaction_id, @"verification_code": verification_code};
    }
    
    [WebServiceInterface dictionaryWithPostVars:postVars serviceMethodUrl:method completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *error) {
        if(error) {
            callback(nil, error);
            return;
        }
        
        NSArray *wsError = [json valueForKey:@"Error"];
        
        if (wsError) {
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(nil, error);
            return;
        }
        
        NSLog(@"JSON: %@", json);
  
        @try{
            NSDictionary *data = [json valueForKey:@"data"];
            if(data){
                NSString *msg = [data objectForKey:@"msg"];
                if(msg){
                    callback([NSString stringWithFormat:@"%@",msg],nil);
                    return;
                }
            }
        }@catch (NSException *ex){
            callback(@"Ocorreu um erro no servidor.",nil);
            return;
        }

    }];
    
}

@end
