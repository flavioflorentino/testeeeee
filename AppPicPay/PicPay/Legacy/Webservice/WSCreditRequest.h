//
//  WSCreditRequest.h
//  MoBuy
//
//  Created by Joao Victor on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceInterface.h"

@interface WSCreditRequest : NSObject

+ (NSDecimalNumber *)userCredit;

@end
