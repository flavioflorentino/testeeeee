//
//  WSCommon.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 15/05/17.
//
//

import Foundation

final class WSCommon : NSObject{
    
    /// Return all avaliable bank
    ///
    /// - Parameter completion: callback
    static func bankList(url: String = kWsUrlGetBanksList, completion: @escaping(_ list: [BankListSection]?,_ error: Error?) -> Void) {
        WebServiceInterface.dictionary(withPostVarsSwift: [:], serviceMethodUrl: url) { (json, response, responseData, error) in
            if let error = error {
                completion(nil, error)
                return
            }
            
            if let data = json?["data"] as? [AnyHashable: Any] {
                if let list = data["list"] as? [[AnyHashable: Any]] {
                    var sectionsList: [BankListSection] = []
                    
                    for section in list {
                        var banks:[PPBank] = []
                        
                        if let banksList = section["banks"] as? [[AnyHashable: Any]] {
                            for bank in banksList {
                                banks.append(PPBank(dictionary: bank))
                            }
                            
                            if let title = section["label"] as? String{
                                sectionsList.append(BankListSection(title: title, rows: banks))
                            }
                        }
                    }
                    
                    completion(sectionsList, nil)
                    return
                }
            }
            
            if let wsError = json?["Error"] as? [AnyHashable: Any] {
                var code = 0
                if let c = wsError["id"] as? Int{
                    code = c
                }
                let error = WebServiceInterface.error( withTitle: wsError["description_pt"] as? String, code: code)
                completion(nil , error)
                return
            }
            
            completion(nil, WSCommon.genericError())
            return
        }
    }
    
    static func genericError() -> Error {
        return WebServiceInterface.error(withTitle: "Não foi possível efetuar a operação.", code: 0)
    }
}
