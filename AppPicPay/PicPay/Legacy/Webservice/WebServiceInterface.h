//
//  WebServiceInterface.h
//  MoBuy
//
//  Created by Diogo Carneiro on 04/07/12.
//
//

#import <Foundation/Foundation.h>
@import CoreLegacy;

#ifdef __OBJC__

//#warning REATIVAR O CACHE DE CONTATOOOOOSSS
//#warning ENABLE NSAppTransportSecurity!!!!!
#define DEBUG_MODE_JSON_OUT     YES
#define DEBUG_MODE_METHOD     YES
#define DEBUG_MODE_JSON_DICT NO
#define DEBUG_MODE_JSON_IN     YES
#define DEBUG_MODE_USR_TOKEN  YES
#define DEBUG_MODE_HEADER_IN  YES
#define DEBUG_MODE_HEADER_OUT YES
#define DEBUG_MODE_BODY_OUT YES
#define DEBUG_MODE_HTTP_STATUS_CODE YES

#ifdef DEBUG
#define DebugLog( s, ... ) NSLog( @"<%@:(%d)> %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#define DebugLogWs( s, ... ) NSLog( @"%@", [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define DebugLog( s, ... )
#define DebugLogWs( s, ... )
#endif

#define kWsUrlItemByQrCode @"api/itemByQrCode.json"
#define kWsUrlChangePassword @"api/changePassword.json"
#define kWsUrlGetCreditCardList @"api/getCreditCardList.json"
#define kWsUrlGetPaymentMethods @"api/getPaymentMethods.json"
#define kWsUrlUpdateConsumer @"api/updateConsumer.json"
#define kWsUrlUpdateConsumerEmail @"api/updateConsumerEmail.json"
#define kWsUrlCheckConsumerEmailStatus @"api/checkConsumerEmailStatus.json"
#define kWsUrlResendConfirmationConsumerEmail @"api/resendConsumerEmailVerification.json"
#define kWsUrlUserLogin @"auth/login"
#define kWsUrlGetAllConsumerData @"api/getAllConsumerData.json"
#define kWsUrlcalcShipping @"api/calcShipping.json"
#define kWsUrlValidateCreditCard @"api/validateCreditCard.json"
#define kWsUrlInsertCreditCard @"api/insertCreditCard.json"
#define kWsUrlInsertAddress @"api/insertAddress.json"
#define kWsUrlCreditCardBandeirasList @"api/getCreditCardBandeiraList.json"
#define kWsUrlUpdateAddress @"api/updateAddress.json"
#define kWsUrlDeleteAddress @"api/deleteAddress.json"
#define kWsUrlVerifyCard @"api/credit_card_verify.json"
#define kWsUrlVerifyCardStatus @"api/credit_card_verify_status.json"
#define kWsUrlVerifyCardStatusStart @"api/credit_card_verify_start.json"
#define KWsUrlVerifyCardStatusCancel @"api/credit_card_verify_cancel.json"
#define kWsUrlEditCreditCard @"api/editCreditCard.json"
#define kWsUrlCreateTransaction @"api/createTransaction.json"
#define kWsUrlParkingsDigitalGoods @"digitalgoods/parkings/transactions/app"
#define kWsUrlTransportPassDigitalGoods @"digitalgoods/transportpass/transactions/app"
#define kWsUrlDeleteCreditCard @"api/deleteCreditCard.json"
#define kWsUrlTransactionsList @"api/transactionsList.json"
#define kWsUrlVerifyVersionUpdate @"api/verifyVersionUpdate.json"
#define kWsUrlBuscaEndereco @"api/buscaEndereco.json"
#define kWsUrlUpdateConsumerImage @"api/updateConsumerImage.json"
#define kWsUrlGetStoresNearby @"api/getStoresNearby.json"
#define kWsUrlGetStoresFilters @"api/getStoresFilters.json"
#define kWsUrlGetMemberGetMember @"api/getMgm.json"
#define kWsUrlGetGlobalCredits @"api/getGlobalCredit.json"
#define kWsUrlCreateP2PTransaction @"api/createP2PTransaction.json"
#define kWSUrlPicPayProPostNotification @"api/sendProNotification.json"
#define kWsUrlTEFCheckTransaction @"api/checkTransaction.json"
#define kWsUrlTEFCancelTransaction @"api/cancelTransaction.json"
#define kWsUrlAddBankAccount @"api/addBankAccount.json"
#define kWsUrlDeleteBankAccount @"api/deleteBankAccount.json"
#define kWsUrlGetBanksList @"api/getBanksList.json"
#define kWsUrlGetInstitutions @"institutions"
#define kWsUrlMakeWithdrawal @"api/makeWithdrawal.json"
#define kWsUrlSendSmsVerification @"api/sendSmsVerification.json"
#define kWsUrlcallVerification @"api/callVerification.json"
#define kWsUrlVerifyPhoneNumber @"api/verifyPhoneNumber.json"
#define kWsUrlIdentifyContacts @"api/identifyContacts.json"
#define kWsUrlAddConsumer @"api/addConsumer.json"
#define kWsUrlGetTransactionById @"api/transactionById.json"
#define kWsUrlAddNotificationToken @"notifications/token"
#define kWsUrlReadNotifications @"api/readNotifications.json"
#define kWsUrlGetNotifications @"api/getNotifications.json"
#define kWsUrlClearUnseenNotifications @"api/clearUnseenNotifications.json"
#define kWsUrlReadNotificationById @"api/readNotificationById.json"
#define kWsUrlAckNotificationById @"api/ackNotificationById.json"
#define kWsUrlPing @"api/ping.json"
#define kWsUrlReviewRating @"api/reviewRating.json"
#define kWsUrlInviteAddressbook @"api/inviteAddressbook.json"
#define kWsUrlLogMgmRequest @"api/logMgmRequest.json"
#define kWsUrlGetNumbersToSuggest @"api/getNumbersToSuggest.json"
#define kWsUrlGetEmailsToSuggest @"api/getEmailsToSuggest.json"
#define kWsUrlVerifyPassword @"api/verifyPassword.json"
#define kWsUrlUserLogout @"auth/logout"
#define kWsUrlMakeRecharge @"api/makeRecharge.json"
#define kWsUrlCancelRecharge @"api/cancelRecharge.json"
#define kWsUrlReceiveDepositRechargeProof @"api/receiveDepositRechargeProof.json"
#define kWsUrlGetLastWithdrawal @"api/getLastWithdrawal.json"
#define kWsUrlGetLastRecharge @"api/getLastRecharge.json"
#define kWsUrlItemByStoreId @"api/itemByStoreId.json"
#define kWsUrlLinkInstagramAccount @"api/linkInstagramAccount.json"
#define kWsUrlUnlinkInstagramAccount @"api/unlinkInstagramAccount.json"
#define kWsUrlCheckUserPro @"api/checkUserPro.json"
#define kWsUrlGetInstallmentsBusinessAccount @"api/getInstallmentsBusinessAccount.json"
#define kWsUrlGetInstallmentsSellerAccount @"api/getInstallmentsSeller.json"
#define kWsUrlCreateConsumerUsername @"api/createConsumerUsername.json"
#define kWsUrlSearch @"api/search.json"
#define kWsUrlSearchPeople @"api/searchPeople.json"
#define kWsUrlGetAntiFraudChecklist @"api/getAntiFraudChecklist.json"
#define kWsUrlClearP2PTransactionOnHold @"api/clearP2PTransactionOnHold.json"
#define kWsUrlClearPAVTransactionOnHold @"api/clearPAVTransactionOnHold.json"
#define kWsUrlDocumentResquetUpload @"api/documentResquetUpload.json"
#define kWsUrlConcludeAntiFraudChecklist @"api/concludeAntiFraudChecklist.json"
#define kWsUrlGetFeed @"api/getFeed.json"
#define kWsUrlGetProfileFeed @"api/getProfileFeed.json"
#define kWsUrlGetZonaAzulById @"api/getZonaAzulById.json"
#define kWsUrlRemoveLicensePlate @"api/removeLicensePlate.json"
#define kWsUrlFeedItemAction @"api/feedItemAction.json"
#define kWsUrlSetConsumerBusinessAccount @"api/setConsumerBusinessAccount.json"

#define kWvUrlZonaAzulInfo @"api/webviewZonaAzulInfo"
#define kWvUrlZonaAzulRule @"api/webviewZonaAzulRegras"

#define kWsUrlGetAccountConfig @"api/getAccountConfig.json"
#define kWsUrlSetAccountConfig @"api/setAccountConfig.json"

#define kWsUrlConsumerConfig @"me/configs"
#define kWsUrlNotificationConfig @"notifications/settings"

#define kWsUrlConsumerAcceptFollower @"api/acceptConsumerFollower.json"
#define kWsUrlConsumerFollow @"api/followConsumer.json"
#define kWsUrlConsumerUnFollow @"api/unfollowConsumer.json"
#define kWsUrlConsumerDismissFollower @"api/dismissConsumerFollower.json"
#define kWsUrlGetFeedComment @"api/getFeedComments.json"
#define kWsUrlAddFeedComment @"api/addFeedComment.json"
#define kWsUrlLikeFeedItem @"api/likeFeedItem.json"
#define kWsUrlGetProfile @"api/getProfile.json"
#define kWsUrlGetProfileBlockFilter @"api/getProfile.json/block-filter"
#define kWsUrlChangeFeedItemPrivacy @"api/changeFeedItemPrivacy.json"
#define kWsUrlConsumerFollowing @"api/getFollowing.json"
#define kWsUrlConsumerFollowers @"api/getFollowers.json"
#define kWsUrlGetWhoLiked @"api/getLikes.json"
#define kWsUrlRemoveConsumerFollower @"api/removeConsumerFollower.json"
#define kWsUrlIsValidEmail @"api/isValidEmail.json"
#define kWsUrlIsValidName @"api/isValidName.json"
#define kWsUrlIsValidPassword @"api/isValidPass.json"
#define kWsUrlGetFollowsSuggestions @"api/getFollowsSuggestions.json"
#define kWsUrlGetConsumerMgm @"api/getConsumerMgm.json"
#define kWsUrlValidateReferralCode @"api/validateReferralCode.json"
#define kWsUrlReportFlow @"api/getAbuseReportScreens.json"
#define kWsUrlReportAbuse @"api/reportAbuse.json"
#define kWsUrlTaxAndLimits @"api/getLimits.json"
#define KWsUrlSearchPeopleByTag @"api/searchPeopleByTag.json"
#define kWsUrlDeactivateConsumer @"api/deactivateConsumer.json"
#define kWsUrlRequestConsumerReactivation @"api/requestConsumerReactivation.json"
#define kWsUrlReceipt @"api/receipt.json"

// Ecommerce
#define kWsUrlEcommercePayment @"ecommerce/app/payments/{orderId}"

/* Membership Producer */
#define kWsUrlMembershipCategoriesByProducerId @"membership/producers/{id}/categories"
#define kWsUrlMembershipProducerById @"membership/producers/{id}"
#define kWsUrlMembershipPlanHistoryById @"membership/plans/{id}/update_history"

// DeepLink
#define kWsUrlDeepLinkUsername @"deeplink/username/{username}"

// PicPay Credit
#define kWsUrlCreditPicpayAccount @"credit/account"
#define kWsUrlCreditPicpayTimeline @"credit/account/feed"
#define kWsUrlCreditPicpayRegistration @"credit/registration"
#define kWsUrlCreditPicpayPatrimony @"credit/domain/patrimony"
#define kWsUrlCreditPicpayDueDay @"credit/account/dueday"
#define kWsUrlCreditPicpayLimit @"credit/account/limit"
#define kWsUrlCreditPicpayDocumentFile @"credit/registration/files?type={type}"
#define kWsUrlCreditPicpayInvoice @"credit/invoice"
#define kWsUrlCreditPicpayInvoiceDetail @"credit/invoice/{id}"
#define kWsUrlCreditPicpayInvoicePayment @"credit/invoice/{id}/payment"
#define kWsUrlCreditPicpayInvoicePaymentMessage @"credit/invoice/{id}/payment-msg?payment_value={value}"
#define kWsUrlCreditPicpayInvoiceEmailBoleto @"credit/invoice/{id}/email"
#define kWsUrlCreditPicpayInvoiceDonwload @"credit/invoice/{id}/donwload"
#define kWsUrlCreditPicpayInvoiceSendEmail @"credit/invoice/{id}/email"
#define kWsUrlCreditPicpaySettingsDueDay @"credit/configuration/due-day"
#define kWsUrlCreditPicpaySettingsChangeDueDay @"credit/account/dueday"
#define kWsUrlCreditPicpayDocument @"credit/domain/document"
#define kWSUrlCreditPicpayDocumentsType @"credit/registration/user-document-type"

//Savings
#define kWsUrlSavingsPicPay @"savings/wallet"

//Recharge
#define kWsUrlRechargeByDebit @"deposit/debit"

//Digital Accounts
#define kWsUrlIncomeRangeOptions @"payment_account/enum/income_range"
#define kWsUrlIncomeRangeSelect @"payment_account/income_range"
#define kWsUrlSelectAddress @"payment_account/address"
#define kWsUrlDigitalAccountInfo @"payment_account"
#define kWsUrlUpgradeChecklist @"payment_account/standard_setup_checklist"
#define kWsUrlUpgradeChecklistConfirm @"payment_account/upgrade"
#define kWsUrlUpgradeChecklistIdentity @"verifications/identity/step/account"
#define kWsUrlUpgradeStatus @"payment_account/status"

//Withdraw
#define kWsUrlWithdrawOptions @"withdrawal/options"
#define kWsUrlWithdrawCashoutRequest @"withdrawal/cef/request"

// Address
#define kWsUrlAddressPicPayCountries @"address/countries"
#define kWsUrlAddressPicPayStates @"address/states"
#define kWsUrlAddressPicPayCities @"address/states/{state}/cities"

/* Membership Subscriber */
#define kWsUrlMembershipSubscriberSubscriptions @"membership/subscribers/subscriptions"
#define kWsUrlMembershipSubscribeByPlanId @"membership/subscribers/plans/{id}/subscription"
#define kWsUrlMembershipCancelBySubscriptionId @"membership/subscribers/subscriptions/{id}/cancel"
#define kWsUrlMembershipUpdatePaymentBySubscriptionId @"membership/subscribers/subscriptions/{id}/payment"
#define kWsUrlMembershipSubscriptionById @"membership/subscribers/subscriptions/{id}"
#define kWsUrlMembershipUpdateAddressBySubscriptionId @"membership/subscribers/subscriptions/{id}/address"
#define kWsUrlMembershipMarkSeenUpdatesBySubscriptionId @"membership/subscribers/subscriptions/{id}/plan_last_update_seen"
#define kWsUrlMembershipReSubscribeBySubscriptionId @"membership/subscribers/subscriptions/{id}/resubscribe"

// Ecommerce
#define kWsUrlIdentityValidationStatus @"verifications/identity/status"
#define kWsUrlIdentityValidationStep @"verifications/identity/step"
#define kWsUrlIdentityValidationStepAnswers @"verifications/identity/step/answers"
#define kWsUrlIdentityValidationStepImage @"verifications/identity/step/image"

//ConsumerAddress
#define kWsUrlConsumerAddressList @"address/addresses/consumer"
#define kWsUrlConsumerAddress @"address/addresses/consumer/address"
#define kWsUrlSearchAddressByZipcode @"address/search?zipcode={zipcode}"

// Bills
#define kWsUrlBillsAnalysesChoice @"bills/analyses/choice"
#define kWsUrlBillsPayment @"bills/v2/payment"
#define kWsUrlBillsBarCode @"bills/barcode"

// P2M
#define kWsUrlP2MPayment @"p2m/payment"
#define kWsUrlP2MPaymentRefund @"p2m/payment/cancel"

// Scanner
#define kWsUrlScannerQRCodeDecode @"api/qrCodeInterpreter.json"

// Linked accounts
#define kWsUrlLinkedAccountsList @"tps/v1/accounts"
#define kWsUrlLinkedAccountsAuthLink @"tps/v1/{service_type}/auth/link"
#define kWsUrlLinkedAccountsUnlink @"tps/v1/accounts/{service_type}/{id}"
#define kWsUrlLinkedAccountsUpdate @"tps/v1/accounts/{id}"

// 2FA
#define kWsUrl2FACodeValidation @"auth/tfa/code/validate"
#define kWsUrl2FACode @"auth/tfa/code"
#define kWsUrl2FACodeValidationV2 @"auth/tfa/v2/validate"


/* App Store */
#define KWsUrlAppStoreWriteReviewLink @"https://itunes.apple.com/app/id{appStoreId}?action=write-review"

#endif

@class PicPayErrorDisplayable;
@interface WebServiceInterface : NSObject

+ (NSString * _Nonnull)apiHost;

/*!
 * Returns the complete endpoint
 * @param methodPath
 */
+ (NSString * _Null_unspecified)apiEndpoint:(NSString * _Nonnull)methodPath;

/*!
 * Efetua uma chamada ao webservice com a possibilidade de envio de dados.
 * @param postVars dados que seram enviados para webservice
 * @param methodPath
 * @param completionHandler CodeBlock para callback
 *
 * @note Toda Requisição feita ao webservice deve ser realizada dentro de uma Thread (@b NÃO utilizar na Thread Principal).
 */
+ (void)dictionaryWithPostVars:(NSDictionary * _Nonnull)postVars serviceMethodUrl:(NSString * _Nonnull)methodPath
             completionHandler:(void (^ _Nullable)(NSArray * _Nullable data, NSURLResponse * _Nullable response, NSData * _Nullable responseData, NSError * _Nullable error))callback NS_SWIFT_UNAVAILABLE("Use dictionaryWithPostVarsSwift instead");

/*!
 * Efetua uma chamada ao webservice com a possibilidade de envio de dados.
 * @param postVars dados que seram enviados para webservice
 * @param methodPath
 * @param headers
 * @param completionHandler CodeBlock para callback
 *
 * @note Toda Requisição feita ao webservice deve ser realizada dentro de uma Thread (@b NÃO utilizar na Thread Principal).
 */
+ (void)dictionaryWithPostVars:(NSDictionary *_Nullable)postVars serviceMethodUrl:(NSString *_Nullable)methodPath andHeaders:(NSDictionary *_Nullable)headers
             completionHandler:(void (^_Nullable)(NSArray * _Nullable data, NSURLResponse * _Nullable response, NSData * _Nullable responseData, NSError * _Nullable error))callback NS_SWIFT_UNAVAILABLE("Use dictionaryWithPostVarsSwift instead");
/*!
 * Efetua uma chamada ao webservice com a possibilidade de envio de dados e headers.
 * @param postVars dados que serão enviados para webservice
 * @param headers dados que serão enviados para webservice
 * @param methodPath
 * @param completionHandler CodeBlock para callback
 *
 * @note Toda Requisição feita ao webservice deve ser realizada dentro de uma Thread (@b NÃO utilizar na Thread Principal).
 */
+ (void)dictionaryWithPostVars:(NSDictionary * _Nonnull)postVars andHeaders:(NSDictionary * _Nullable)headers serviceMethodUrl:(NSString * _Nonnull)methodPath
             completionHandler:(void (^ _Nullable)(NSArray * _Nullable data, NSURLResponse * _Nullable response, NSData * _Nullable responseData, NSError * _Nullable error))callback NS_SWIFT_UNAVAILABLE("Use dictionaryWithPostVarsSwift instead");

/*!
 * Efetua uma chamada ao webservice com a possibilidade de envio de dados.
 * @param postVars dados que seram enviados para o webservice
 * @param methodPath
 * @param cacheCompletionHandler CodeBlock para callback do cache
 * @param completionHandler CodeBlock para callback
 *
 * @note Toda Requisição feita ao webservice deve ser realizada dentro de uma Thread (@b NÃO utilizar na Thread Principal).
 */
+ (void)dictionaryWithPostVars:(NSDictionary * _Nonnull)postVars serviceMethodUrl:(NSString * _Nonnull)methodPath
                      cacheKey: (NSString * _Nullable) cacheKeyPrefix
               cacheCompletion: (void (^ _Nullable)(NSDictionary * _Nullable data)) cacheCompletionHandler
             completionHandler:(void (^ _Nullable)(NSArray * _Nullable data, NSURLResponse * _Nullable response, NSData * _Nullable responseData, NSError * _Nullable error))callback;

/*!
 * Efetua o upload de imagem
 * @param imageData imagem
 * @param postVars dados que seram enviados para o webservice
 * @param methodPath
 * @param completionHandler CodeBlock para callback
 * @param progressCallback CodeBlock para progresso
 *
 * @note Toda Requisição feita ao webservice deve ser realizada dentro de uma Thread (@b NÃO utilizar na Thread Principal).
 */
+ (void)uploadImage:(NSData * _Nonnull)imageData serviceMethodUrl:(NSString * _Nonnull)methodPath postVars:(NSDictionary * _Nonnull)postVars completionHandler:(void (^ _Nullable)(NSArray * _Nullable data, NSURLResponse * _Nullable response, NSData * _Nullable responseData, NSError * _Nullable error))callback progressCallback:(void (^ _Nullable)(CGFloat progress))progressCallback;
+ (PicPayErrorDisplayable * _Null_unspecified)picpayError: (NSDictionary * _Null_unspecified) errorData statuscode: (NSInteger)statuscode;
+ (PicPayErrorDisplayable * _Null_unspecified)picpayError: (NSDictionary * _Null_unspecified) errorData;

+ (NSError * _Nonnull)errorWithTitle:(NSString * _Null_unspecified)message code:(NSInteger)code;

/*!
 * WORK AROUND SWIFT
 * Efetua uma chamada ao webservice com a possibilidade de envio de dados.
 * @param postVars dados que seram enviados para webservice
 * @param methodPath
 * @param completionHandler CodeBlock para callback
 *
 * @note Toda Requisição feita ao webservice deve ser realizada dentro de uma Thread (@b NÃO utilizar na Thread Principal).
 */
+ (void)dictionaryWithPostVarsSwift:(NSDictionary * _Nonnull)postVars serviceMethodUrl:(NSString * _Nonnull)methodPath
             completionHandler:(void (^ _Nullable)(NSDictionary * _Nullable data, NSURLResponse * _Nullable response, NSData * _Nullable responseData, NSError * _Nullable error))callback;


/*!
 * WORK AROUND SWIFT
 * Efetua uma chamada ao webservice com a possibilidade de envio de dados.
 * @param postVars dados que seram enviados para webservice
 * @param methodPath
 * @param headers
 * @param completionHandler CodeBlock para callback
 *
 * @note Toda Requisição feita ao webservice deve ser realizada dentro de uma Thread (@b NÃO utilizar na Thread Principal).
 */
+ (void)dictionaryWithPostVarsSwift:(NSDictionary *_Nullable)postVars serviceMethodUrl:(NSString *_Nullable)methodPath andHeaders:(NSDictionary *_Nullable)headers
                  completionHandler:(void (^_Nullable)(NSDictionary * _Nullable data, NSURLResponse * _Nullable response, NSData * _Nullable responseData, NSError * _Nullable error))callback;
/*!
 * WORK AROUND SWIFT
 * Efetua uma chamada ao webservice com a possibilidade de envio de dados.
 * @param postVars dados que seram enviados para webservice
 * @param methodPath
 * @param cacehKey chave do cache
 * @param cacheCompletionHandler CodeBlock para callback do cache
 * @param completionHandler CodeBlock para callback
 *
 * @note Toda Requisição feita ao webservice deve ser realizada dentro de uma Thread (@b NÃO utilizar na Thread Principal).
 */
+ (void)dictionaryWithPostVarsSwift:(NSDictionary * _Nonnull)postVars
                   serviceMethodUrl:(NSString * _Nonnull)methodPath
                           cacheKey: (NSString * _Nullable) cacheKey
             cachecompletionHandler:(void (^ _Nullable)(NSDictionary * _Nullable data)) cacheCallback
                  completionHandler:(void (^ _Nullable)(NSDictionary * _Nullable data, NSURLResponse * _Nullable response, NSData * _Nullable responseData, NSError * _Nullable error))callback;

+ (NSError * _Null_unspecified)genericError;
@end

/**!
 * Category to make it easier to identify a connection error
 */
@interface NSError (ConnectionError)

- (BOOL) isConnectionError;

@end
