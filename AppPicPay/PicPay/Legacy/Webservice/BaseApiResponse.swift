import CoreLegacy
import Foundation
import SwiftyJSON

class BaseApi {
    var requestManager: RequestManager
    
    init(requestManager: RequestManager = RequestManager.shared){
        self.requestManager = requestManager
    }
}

protocol BaseApiResponse: CoreLegacy.BaseApiResponse { }

protocol BaseApiRequest {
    func toParams() throws -> [String: Any]
}

typealias PicPayResult<ResponseType> = Result<ResponseType, PicPayError>

final class BaseApiGenericResponse: NSObject, BaseApiResponse {
    var json: JSON
    
    required init?(json: JSON) {
        self.json = json
        super.init()
    }
    
    init(dictionary: [String: Any] = [:]) {
        self.json = JSON(dictionary)
        super.init()
    }
  
    func decodeObject<T: Decodable>(type: T.Type, strategy: JSONDecoder.KeyDecodingStrategy) -> T? {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .customISO8601
        decoder.keyDecodingStrategy = strategy
        
        do {
            let data = try json.rawData()
            let object = try decoder.decodeNestedData(T.self, from: data)
            return object
        } catch let error {
            Log.debug(.model, error.localizedDescription)
            return nil
        }
    }
}

final class BaseApiEmptyResponse: NSObject, BaseApiResponse {
    required init?(json: JSON) {
        super.init()
    }
}

final class BaseApiAlertResponse: NSObject, BaseApiResponse {
    var alert: Alert
    
    required init?(json: JSON) {
        guard let alert = Alert(json: json["alert"]) else {
     return nil
}
        self.alert = alert
        super.init()
    }
}

typealias WrapperResponse = PicPayResult<BaseApiGenericResponse>

class BaseApiListResponse<ValueType: BaseApiResponse>: BaseApiResponse {
    var list:[ValueType] = []
    
    init(list: [ValueType] = []) {
        self.list = list
    }

    required init?(json:JSON){
        if let listObject = json.array {
            for j in listObject {
                if let o = ValueType(json:j) {
                    list.append(o)
                }
            }
        }
    }
}

final class ApiNotificationsName {
    static var syncData = NSNotification.Name(rawValue: "ApiNotificationSyncData")
    static var unseenNotificationsChanged = NSNotification.Name(rawValue: "unseenNotificationsChanged")
}

final class BaseCodableEmptyResponse: Codable {
}
