//
//  WSParcelamento.m
//  MoBuy
//
//  Created by Joao victor V lana on 14/08/12.
//
//

#import "WSParcelamento.h"
#import "WSConsumer.h"
#import "WebServiceInterface.h"

@implementation WSParcelamento

/*!
 * Retorna as taxas de parcelamento para o seller
 *
 * Callback:
 *
 *   - @b NSA => installments
 *
 *   - @b NSError => erro
 *
 * @param NSString seller id
 * @param completionHandler callback
 */
+ (void) getInstallmentsSellerAccount:(NSString *)sellerId completionHandler:(void (^)(NSArray *installments, NSError *error)) callback{
    
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:sellerId, nil] forKeys:[NSArray arrayWithObjects:@"seller_id", nil]] serviceMethodUrl:kWsUrlGetInstallmentsSellerAccount completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(nil, err);
            return;
        }
        
        NSMutableArray *installments = [[NSMutableArray alloc] init];
        
        NSArray *wsError = [json valueForKey:@"Error"];
        NSArray *dataJson = [json valueForKey:@"data"];
        
        if (dataJson) {
            NSArray * installmentsList = [dataJson valueForKey:@"installments"];
            if(installmentsList){
                @try {
                    for (NSNumber *installment in installmentsList) {
                        NSNumber * number = [NSNumber numberWithDouble:[installment doubleValue]];
                        [installments addObject:number];
                    }
                    
                    callback(installments, nil);
                } @catch (NSException *exception) {
                    callback(nil, [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] integerValue]]);
                }
            }
        } else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] integerValue]];
            callback(nil, error);
            return;
        }
    }];
}


/*!
 * Retorna as taxas de parcelamento para o usuário
 *
 * Callback:
 *
 *   - @b NSA => installments
 *
 *   - @b NSError => erro
 *
 * @param NSString payee id
 * @param completionHandler callback
 */
+ (void) getInstallmentsBusinessAccount:(NSString *)payeeId completionHandler:(void (^)(NSArray *installments, NSError *error)) callback{
	
    [WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:payeeId, nil] forKeys:[NSArray arrayWithObjects:@"payee_id", nil]] serviceMethodUrl:kWsUrlGetInstallmentsBusinessAccount completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(nil, err);
            return;
        }
        
        NSMutableArray *installments = [[NSMutableArray alloc] init];
        
        NSArray *wsError = [json valueForKey:@"Error"];
        NSArray *dataJson = [json valueForKey:@"data"];
        
        if (dataJson) {
            NSArray * installmentsList = [dataJson valueForKey:@"installments"];
            if(installmentsList){
                @try {
                    for (NSNumber *installment in installmentsList) {
                        NSNumber * number = [NSNumber numberWithDouble:[installment doubleValue]];
                        [installments addObject:number];
                    }
                    
                    callback(installments, nil);
                } @catch (NSException *exception) {
                    callback(nil, [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] integerValue]]);
                }
            }
        } else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] integerValue]];
            callback(nil, error);
            return;
        }
    }];
}

/*!
 * Retorna as possibilidades de recebimento parcelado para usuários PRO
 *
 * Callback:
 *
 *   - @b NSDictionary => dados
 *
 *   - @b NSError => erro
 *
 */
+ (void) getMaximumTaxesFreeInstallments:(void (^)(NSArray *installments, NSString *text, NSError *error)) callback{
	
	[WSConsumer getAccountConfig:@"installments" completionHandler:^(NSArray *json, NSError *error) {
		NSArray *jsonData = [json valueForKey:@"data"];
		NSArray *wsError = [json valueForKey:@"Error"];
		if (jsonData) {
			callback([[jsonData valueForKey:@"Installments"] valueForKey:@"options"], [[jsonData valueForKey:@"Installments"] valueForKey:@"text"], nil);
			return;
		}else{
			NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
			callback(nil,nil,error);
			return;
		}
	}];
	
	
}

+ (void) setMaximumTaxesFreeInstallments:(NSString *)installmentId completionHandler:(void (^)(NSArray *installments, NSString *text, NSError *error)) callback{
	
	[WSConsumer setAccountConfig:@"installments" params:@{@"value":installmentId} completionHandler:^(NSArray *json, NSError *error) {
		NSArray *jsonData = [json valueForKey:@"data"];
		NSArray *wsError = [json valueForKey:@"Error"];
		if (jsonData) {
			callback(nil,nil,nil);
			return;
		}else{
			NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
			callback(nil,nil,error);
			return;
		}
	}];
	
	
}


@end
