//
//  WSFriends.m
//  PicPay
//
//  Created by Diogo Carneiro on 29/08/13.
//
//

#import "WSFriends.h"
#import "PicPay-Swift.h"
@import CoreLegacy;
@implementation WSFriends

/*!
 * Retorna a lista de emails sugeridos
 *
 * Callback:
 *
 *   - @b NSArray => Lista de emails
 *
 *   - @b NSError => erro
 *
 * @param completionHandler callback
 */
+ (void)emailAddressesSuggestions:(void (^_Nonnull)(NSArray * _Nullable data, NSError * _Nullable error)) callback{
	[WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray array] forKeys:[NSArray array]] serviceMethodUrl:kWsUrlGetEmailsToSuggest completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        NSArray *json = [data valueForKey:@"data"];
        
        if(err) {
            callback(nil, err);
            return;
        }
        
        callback(json, err);
        return;
    }];
}

/*!
 * Retorna a lista de sugestão de telefone
 *
 * Callback:
 *
 *   - @b NSArray => Informações de telefones
 *
 *   - @b NSError => erro
 *
 * @param completionHandler callback
 */
+ (void)phoneNumbersSuggestions:(void (^_Nonnull)(NSArray * _Nullable data, NSError * _Nullable error)) callback {
	[WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray array] forKeys:[NSArray array]] serviceMethodUrl:kWsUrlGetNumbersToSuggest completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        NSArray *json = [data valueForKey:@"data"];
        
        if(err) {
            callback(nil, err);
            return;
        }
        
        callback(json, err);
        return;
    }];
    
    
}

@end
