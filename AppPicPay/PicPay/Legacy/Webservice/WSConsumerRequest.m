//
//  WSConsumerRequest.m
//  MoBuy
//
//  Created by Joao victor V lana on 08/08/12.
//
//

#import "WSConsumerRequest.h"
#import "PicPay-Swift.h"

@import CoreLegacy;
@import FirebaseAnalytics;

@implementation WSConsumerRequest

/*!
 * Retorna todos os dados do usuário (Consumer).
 * Callback:
 *   - @b MBConsumer => consumer
 *   - @b NSError => erro
 * @param useCache flag para utilizar o cache (Caso disponível).
 * @param completionHandler callback
 */
+ (void)getAllConsumerData:(BOOL)useCache completionHandler:(void (^)(MBConsumer *consumer, NSError *error))callback{
    NSDictionary *completeJson = [ConsumerManager.shared getAllConsumerDataCache];
    id consumer = [[completeJson valueForKey:@"data"] valueForKey:@"Consumer"];
	
    //there's a cache to use?
	if (useCache && consumer && [consumer isKindOfClass:[NSDictionary class]]) {
		
		DebugLog(@"USANDO CACHE!!!");
		
		//restore cache
		completeJson = [ConsumerManager.shared getAllConsumerDataCache];
        [WSConsumerRequest proccessConsumerJson:completeJson useCache:useCache completionHandler:callback webserviceError:nil];

	}else{
		
        NSString *phoneName;
        @try {
            phoneName = [[UIDevice currentDevice] name];
        }
        @catch (NSException *exception) {
            phoneName = @"";
        }
        
		DebugLog(@"USANDO CACHE NÃO!!!!!");
		
		[WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:phoneName, nil] forKeys:[NSArray arrayWithObjects:@"name_on_device", nil]] serviceMethodUrl:kWsUrlGetAllConsumerData completionHandler:^(NSArray *completeJson, NSURLResponse *response, NSData *responseData, NSError *error) {
            
            [WSConsumerRequest proccessConsumerJson:(NSDictionary*)completeJson useCache:useCache completionHandler:callback webserviceError:error];
        }];
	}
}

/*!
 * Processa os dados json do consumer.
 * Callback:
 *   - @b MBConsumer => consumer
 *   - @b NSError => erro
 * @param useCache flag para utilizar o cache (Caso disponível).
 * @param completionHandler callback
 */
+ (void)proccessConsumerJson:(NSDictionary *)completeJson useCache:(BOOL) useCache completionHandler:(void (^)(MBConsumer *consumer, NSError *error))callback webserviceError:(NSError *)webserviceError{
	
	if (webserviceError) {
		callback(nil, webserviceError);
		return;
	}
	
    MBConsumer *consumer = [[MBConsumer alloc] init];
	
    NSArray *wsError = [completeJson valueForKey:@"Error"];
    NSDictionary *json = [completeJson valueForKey:@"data"];
    id consumerJson = [json valueForKey:@"Consumer"];

    if (consumerJson && [consumerJson isKindOfClass:[NSDictionary class]]) {
        
        //It's ok now, save the return json in cache
        [ConsumerManager.shared setGetAllConsumerDataWithCachedData:completeJson];
        
        consumer.name               = [consumerJson valueForKey:@"name"];
        consumer.email              = [consumerJson valueForKey:@"email"];
        consumer.emailConfirmed     = [[consumerJson valueForKey:@"email_confirmed"] boolValue];
        consumer.share_hash         = [consumerJson valueForKey:@"share_hash"];
        consumer.businessAccount	= [[consumerJson valueForKey:@"business_account"] boolValue];
        consumer.verifiedPhoneNumber = [consumerJson valueForKey:@"mobile_phone_verified"];
        
        @try {
            NSDictionary *analyticsProperties = [json valueForKey:@"AnalyticsProperties"];
            [analyticsProperties enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                if (![key isKindOfClass:[NSString class]] || ![obj isKindOfClass:[NSString class]]) {
                    @throw([NSException exceptionWithName: @"Could not parse" reason: @"Wrong key/value types" userInfo: nil]);
                }
            }];
            consumer.analyticsProperties = analyticsProperties;
        }@catch  (NSException *exception) {
            consumer.analyticsProperties = nil;
        }
        
        @try {
            consumer.verifiedAccount = [[consumerJson valueForKey:@"verified"] boolValue];
        }@catch  (NSException *exception) {
            consumer.verifiedAccount  = NO;
        }
        
        @try {
            consumer.isPicPayLover = [[json valueForKey:@"is_picpay_lover"] boolValue];
        }@catch  (NSException *exception) {
            consumer.isPicPayLover = NO;
        }
        
        @try {
            consumer.username = [consumerJson valueForKey:@"username"];
            if([consumer.username isEqual:[NSNull null]]){
                consumer.username = @"";
            }
        }
        @catch (NSException *exception) {
            consumer.username = @"";
        }
        
        @try {
            if([consumerJson valueForKey:@"name_mother"] == nil || [[consumerJson valueForKey:@"name_mother"] isEqual:[NSNull null]]){
                consumer.name_mother = @"";
            }else{
                consumer.name_mother = [consumerJson valueForKey:@"name_mother"];
            }
        }
        @catch (NSException *exception) {
            consumer.name_mother = @"";
        }
        
        @try {
            if([consumerJson valueForKey:@"birth_date"] == nil || [[consumerJson valueForKey:@"birth_date"] isEqual:[NSNull null]]){
                consumer.birth_date = @"";
            } else {
                consumer.birth_date = [consumerJson valueForKey:@"birth_date"];
            }
        }
        @catch (NSException *exception) {
            consumer.birth_date = @"";
        }
        
        @try {
            if ((NSNull *)consumer.verifiedPhoneNumber == [NSNull null] || consumer.verifiedPhoneNumber.length == 0) {
                consumer.verifiedPhoneNumber = nil;
            }
        }
        @catch (NSException *exception) {
            consumer.verifiedPhoneNumber = nil;
        }
        
        @try {
            consumer.bypass_sms = [[consumerJson valueForKey:@"bypass_sms"] boolValue];
        }
        @catch (NSException *exception) {
            consumer.bypass_sms = NO;
        }
        
        consumer.img_url = [consumerJson valueForKey:@"img_url"];
        
        @try {
            if ((NSNull *)consumer.img_url == [NSNull null] || consumer.img_url.length == 0) {
                consumer.img_url = nil;
            }
        }
        @catch (NSException *exception) {
            consumer.img_url = nil;
        }
        
        @try {
            if (!useCache){
                if([json valueForKey:@"ConsumerNotification"] != nil && [json valueForKey:@"ConsumerNotification"] != [NSNull null]){
                    [AppManager.shared setUnreadNotificationsCount:[[json valueForKey:@"ConsumerNotification"] integerValue]];
                }
            }
        }
        @catch (NSException *exception) {
            [AppManager.shared setUnreadNotificationsCount:0];
        }
        
        @try {
            consumer.numericKeyboardForPassword = [[consumerJson valueForKey:@"kb_type"] boolValue];
        }
        @catch (NSException *exception) {
            consumer.numericKeyboardForPassword = NO;
        }
        
		@try {
			consumer.consumerLabels = [[NSMutableArray alloc] init];
			for (NSArray *consumerLabel in [json valueForKey:@"ConsumerLabels"]) {
				[consumer.consumerLabels addObject:consumerLabel];
			}
		} @catch (NSException *exception) {
			consumer.consumerLabels = [[NSMutableArray alloc] init];
		}
		
		
        NSNumber *interest;
        NSDecimalNumber *minQuotaValue;
        int maxQuotaQuantity;
        @try {
            NSArray *installmentJson = [json valueForKey:@"Installments"];
            interest = [NSNumber numberWithFloat:[[installmentJson valueForKey:@"cc_interests"] floatValue]];
            minQuotaValue = [NSDecimalNumber decimalNumberWithDecimal:[[installmentJson valueForKey:@"parcela_minima"] decimalValue]];
            maxQuotaQuantity = [[installmentJson valueForKey:@"max_parcelas"] intValue];
        }
        @catch (NSException *exception) {
            interest = [NSNumber numberWithFloat:2.99];
            minQuotaValue = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:10] decimalValue]];
            maxQuotaQuantity = 12;
        }
        [[PPPaymentConfig shared] updateWithInterest: interest minQuotaValue: minQuotaValue maxQuotaQuantity: maxQuotaQuantity];
        
        
        PPMgmConfigs *mgmConfigs;
        @try {
            if ([[json valueForKey:@"Mgm"] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *mgmJson = (NSDictionary *)[json valueForKey:@"Mgm"];
                mgmConfigs = [[PPMgmConfigs alloc] initWith:mgmJson];
            } else {
                mgmConfigs = [[PPMgmConfigs alloc] init];
                mgmConfigs.mgmEnabled = NO;
            }
        }
        @catch (NSException *exception) {
            mgmConfigs = [[PPMgmConfigs alloc] init];
            mgmConfigs.mgmEnabled = NO;
        }
        [[AppParameters globalParameters] setMgmConfigs:mgmConfigs];
        
        consumer.cpf                = [consumerJson valueForKey:@"cpf"];
        consumer.wsId               = [[consumerJson valueForKey:@"id"] integerValue];
        
        @try {
            //Bank account
            
            if ([json valueForKey:@"BankAccount"] !=nil && [[json valueForKey:@"BankAccount"] isKindOfClass:[NSArray class]]){
            
                NSArray *bankArray = (NSArray *) [json valueForKey:@"BankAccount"];
                if ([bankArray count] > 0 ) {
                    
                        
                        if ([[[json valueForKey:@"BankAccount"] objectAtIndex:0] isKindOfClass:[NSDictionary class]]) {
                            NSDictionary *bankAccountJson = [[json valueForKey:@"BankAccount"] objectAtIndex:0];
                            PPBankAccount *returnedBankAccount = [[PPBankAccount alloc] initWithDictionary:bankAccountJson];
                            
                            consumer.bankAccount = returnedBankAccount;
                        }
                    }
                }
        }
        @catch (NSException *exception) {
            //consumer bank account is nil
        }
        
        @try {
            //Balance
            consumer.balance = [NSDecimalNumber decimalNumberWithString:[consumerJson valueForKey:@"balance"]];
        }
        @catch (NSException *exception) {
            
        }
        
        @try {
            //Withdraw Balance
            consumer.withdrawBalance = [NSDecimalNumber decimalNumberWithString:[consumerJson valueForKey:@"withdraw_balance"]];
        }
        @catch (NSException *exception) {
            
        }
        
        callback(consumer, nil);
        return;
    }else{
        NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] integerValue]];
        callback(nil, error);
        return;
    }
}

@end
