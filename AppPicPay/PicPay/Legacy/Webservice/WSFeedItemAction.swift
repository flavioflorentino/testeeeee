//
//  WSGenericAction.swift
//  PicPay
//
//  Created by Vagner Orlandi on 15/08/17.
//
//

import UIKit

final class WSFeedItemAction: NSObject {
    
    /**
     * Get a behavior of a generic action from server
     */
    static func requestBehavior(_ data:[AnyHashable: Any], completion callback:@escaping (_ success:Bool, _ error:Error?, _ behavior:FeedItemBehavior?) -> Void ){
        WebServiceInterface.dictionary(withPostVarsSwift: data, serviceMethodUrl: kWsUrlFeedItemAction) { (result, response, data, err) in
            var behavior:FeedItemBehavior? = nil
            
            if err != nil {
                callback(false, err, nil)
                return
            }
            
            if let json = result?["data"] as? [AnyHashable: Any] {
                if let behaviorData = json["Behavior"] as? [AnyHashable: Any] {
                    behavior = FeedItemBehavior(jsonDict: behaviorData)
                }
            }
            
            callback(true, err, behavior)
        }
    }

}
