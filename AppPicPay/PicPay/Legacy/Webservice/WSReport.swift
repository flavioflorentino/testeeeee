//
//  WSReport.swift
//  PicPay
//
//  ☝️☁️ Created by Luiz Henrique Guimarães on 28/06/17.
//
//

import UIKit

final class WSReport: NSObject {
    
    /**
     Get report flow
     @param reportRequest data for request
     */
    static func getReportFlow(data: PPReportRequest, completion callback:@escaping (_ flow:PPReportFlow?, _ error:Error?) -> Void ){
        
        WebServiceInterface.dictionary(withPostVarsSwift: data.toDictionary(), serviceMethodUrl: kWsUrlReportFlow) { (result, response, friends, err) in
            guard err == nil else {
                callback(nil, err)
                return
            }
            
            guard let json = result else{
                let error = WebServiceInterface.genericError()
                callback(nil, error)
                return
            }
            
            if let data = json["data"] as? [AnyHashable: Any]{
                let flow = PPReportFlow(dictionary: data)
                callback(flow, nil)
                return
                
            }
            
            if let wsError = json["Error"] as? [AnyHashable: Any] {
                var code = 0
                if let c = wsError["id"] as? Int{
                    code = c
                }
                let error = WebServiceInterface.error( withTitle: wsError["description_pt"] as? String, code: code)
                callback(nil, error)
                return
            }
            let error = WebServiceInterface.genericError()
            callback(nil, error)
        }
    }
    
    /**
     Report abuse
     @param reportRequest data for request
     */
    static func reportAbuse(_ reportRequest:PPReportRequest , completion callback:@escaping (_ success:Bool, _ error:Error?) -> Void ){
        
        let post = reportRequest.toDictionary()
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlReportAbuse) { (result, response, friends, err) in
            
            guard err == nil else {
                callback(false, err)
                return
            }
            
            guard let json = result else{
                let error = WebServiceInterface.genericError()
                callback(false, error)
                return
            }
            
            if let _ = json["data"] as? [AnyHashable: Any]{
                callback(true, nil)
                return
                
            }
            
            if let wsError = json["Error"] as? [AnyHashable: Any] {
                var code = 0
                if let c = wsError["id"] as? Int{
                    code = c
                }
                let error = WebServiceInterface.error( withTitle: wsError["description_pt"] as? String, code: code)
                callback(false, error)
                return
            }
            
            let error = WebServiceInterface.genericError()
            callback(false, error)
            return
        }
    }

}
