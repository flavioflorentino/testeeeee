//
//  WSSocial.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 25/11/16.
//
//

import UIKit
import CoreLegacy

final class WSSocial: NSObject {
    
    /**
     Load profile data
     @param profile_id consumer id
     */
    @objc static func getConsumerProfile(_ profileId:String , completion callback:@escaping (_ profile:PPContact?, _ consumerStatus:FollowerStatus, _ followerStatus:FollowerStatus, _ error:Error?) -> Void ){
        WSSocial.getConsumerProfile(profileId, username: nil, completion: callback)
    }
    
    /**
     Load profile data
     @param profile_id consumer id
     */
    @objc static func getConsumerProfile(_ profileId:String, username:String? , completion callback:@escaping (_ profile:PPContact?, _ consumerStatus:FollowerStatus, _ followerStatus:FollowerStatus, _ error:Error?) -> Void ){
        
        let post:[AnyHashable: Any] = ["profile_id":profileId, "profile_type": "consumer"]
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlGetProfileBlockFilter) { (result, response, friends, err) in
            
            if err != nil{
                callback(nil ,FollowerStatus.undefined, FollowerStatus.undefined, err)
                return
            }
            
            guard let json = result else{
                let error = WSSocial.genericError()
                callback(nil ,FollowerStatus.undefined, FollowerStatus.undefined, error)
                return
            }
            
            if let data = json["data"] as? [AnyHashable: Any]{
                var profile:PPContact? = nil
                
                // paser consumer profile
                if let profileData = data["profile"] as? [AnyHashable: Any]{
                    profile = PPContact(profileDictionary: profileData)
                }
                
                // parse consumer status follower
                var consumerStatus = FollowerStatus.undefined
                if let cs = data["consumer_status_follower"] as? String{
                    if let csInt = Int(cs){
                        if let ss = FollowerStatus(rawValue: csInt) {
                            consumerStatus = ss
                        }
                    }
                }
            
                var followerStatus = FollowerStatus.undefined
                if let fs = data["follower_status_consumer"] as? String{
                    if let fsInt = Int(fs){
                        if let ss = FollowerStatus(rawValue: fsInt) {
                            followerStatus = ss
                        }
                    }
                }
                
                if let isBlocked = data["blocked"] as? Bool {
                    profile?.isBlocked = isBlocked
                }
                
                if let canSee = data["can_see"] as? Int{
                    profile?.isOpenFollowersAndFollowings = (canSee == 1 ? true : false)
                }
                if let canSee = data["can_see"] as? Bool{
                    profile?.isOpenFollowersAndFollowings = canSee
                }
                
                if let canSee = data["can_see"] as? Bool{
                    profile?.isOpenFollowersAndFollowings = canSee
                }
                
                if let counts = data["counts"] as? [AnyHashable: Any] {
                    if let following = counts["following"] as? Int {
                        profile?.following = following
                    }
                    
                    if let followers = counts["followers"] as? Int {
                        profile?.followers = followers
                    }
                }
                callback(profile ,consumerStatus, followerStatus, err)
                return

            }else{
                if let wsError = json["Error"] as? [AnyHashable: Any] {
                    var code = 0
                    if let c = wsError["id"] as? Int{
                        code = c
                    }
                    let error = WebServiceInterface.error( withTitle: wsError["description_pt"] as? String, code: code)
                    callback(nil ,FollowerStatus.undefined, FollowerStatus.undefined, error)
                    return
                }
            }
            
            let error = WSSocial.genericError()
            callback(nil ,FollowerStatus.undefined, FollowerStatus.undefined, error)
            return
        }
    }
    
    /**
     Load profile data
     @param profile_id seller id
     */
    @objc static func getStoreProfile(_ profileId:String, completion callback:@escaping (_ profile:PPStore?, _ error:Error?) -> Void ){
        
        let post = ["profile_id":profileId, "profile_type": "seller"]
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlGetProfile) { (result, response, friends, err) in
            
            if err != nil{
                callback(nil , err)
                return
            }
            
            guard let json = result else{
                let error = WSSocial.genericError()
                callback(nil , error)
                return
            }
            
            if let data = json["data"] as? [AnyHashable: Any]{
                var profile:PPStore? = nil
                
                // paser consumer profile
                if let profileData = data["profile"] as? [AnyHashable: Any]{
                    profile = PPStore(profileDictionary: profileData)
                }
                
                callback(profile , err)
                return
                
            }else{
                if let wsError = json["Error"] as? [AnyHashable: Any] {
                    var code = 0
                    if let c = wsError["id"] as? Int{
                        code = c
                    }
                    let error = WebServiceInterface.error( withTitle: wsError["description_pt"] as? String, code: code)
                    callback(nil , error as Error?)
                    return
                }
            }
            
            let error = WSSocial.genericError()
            callback(nil , error)
            return
        }
    }
    
    /**
     * Execute an action over the follow
     */
    @objc static func followAction(_ action:FollowerButtonAction, follower:String, completion callback:@escaping (_ success:Bool, _ consumerStatus:FollowerStatus, _ followerStatus:FollowerStatus, _ error:Error?) -> Void ){
        
        switch action {
        case .follow:
            WSSocial.follow(follower, completion: callback)
            break;
        case .unfollow, .cancelRequest:
            WSSocial.unfollow(follower, completion: callback)
            break;
        case .dismiss:
            WSSocial.dismissFollower(follower, completion: callback)
            break;
        case .allow:
            WSSocial.acceptFollower(follower, completion: callback)
            break;
        default: break
        }
    }
    
    /**
     Accept the follower request
     @param follower id
     */
    @objc static func acceptFollower(_ follower:String, completion callback:@escaping (_ success:Bool, _ consumerStatus:FollowerStatus, _ followerStatus:FollowerStatus, _ error:Error?) -> Void ){
        let post = ["follower":follower]
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlConsumerAcceptFollower) { (result, response, friends, err) in
            WSSocial.processResultFollow(result, error: err, completion: callback)
        }
    }
    
    /**
     Follow a consumer
     @param follow consumer id
     */
    @objc static func follow(_ follow:String, completion callback:@escaping (_ success:Bool, _ consumerStatus:FollowerStatus, _ followerStatus:FollowerStatus, _ error:Error?) -> Void ){
        let post = ["followed":follow]
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlConsumerFollow) { (result, response, friends, err) in
            WSSocial.processResultFollow(result, error: err, completion: callback)
        }
    }
    
    /**
     Follow a consumer
     @param follow consumer id
     */
    @objc static func follow(consumers:[String], completion callback:@escaping (_ success:Bool, _ error:Error?) -> Void ){
        let post = ["followed":consumers]
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlConsumerFollow) { (result, response, friends, err) in
            
            if err != nil{
                callback(false, err)
                return
            }
            
            guard let json = result else{
                let error = WSSocial.genericError()
                callback(false, error)
                return
            }
            
            if let wsError = json["Error"] as? [AnyHashable: Any] {
                var code = 0
                if let c = wsError["id"] as? Int{
                    code = c
                }
                let error = WebServiceInterface.error( withTitle: wsError["description_pt"] as? String, code: code)
                callback(false, error)
                return
            }else if let _ = json["data"] as? [AnyHashable: Any]{
                callback(true, nil)
                return
            }
            
            let error = WSSocial.genericError()
            callback(false, error)
            return
            
        }
    }
    
    /**
     UnFollow a consumer
     @param follow consumer id
     */
    @objc static func unfollow(_ follow:String, completion callback:@escaping (_ success:Bool, _ consumerStatus:FollowerStatus, _ followerStatus:FollowerStatus, _ error:Error?) -> Void ){
        let post = ["followed":follow]
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlConsumerUnFollow) { (result, response, friends, err) in
            WSSocial.processResultFollow(result, error: err, completion: callback)
        }
    }
    
    /**
     Dismiss the follower request
     @param follow consumer id
     */
    @objc static func dismissFollower(_ follow:String, completion callback:@escaping (_ success:Bool, _ consumerStatus:FollowerStatus, _ followerStatus:FollowerStatus, _ error:Error?) -> Void ){
        let post = ["follower":follow]
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlConsumerDismissFollower) { (result, response, friends, err) in
            WSSocial.processResultFollow(result, error: err, completion: callback)
        }
    }
    
    /**
     Proccess the follow operation result
     @param result result from server
     @param error error from server
     @param completion completion callback
     */
    fileprivate static func processResultFollow(_ result:[AnyHashable: Any]?,error err:Error?, completion callback:(_ success:Bool, _ consumerStatus:FollowerStatus, _ followerStatus:FollowerStatus, _ error:Error?) -> Void ){
       
        if err != nil{
            callback(false, FollowerStatus.undefined, FollowerStatus.undefined, err)
            return
        }
        
        guard let json = result else{
            let error = WSSocial.genericError()
            callback(false, FollowerStatus.undefined, FollowerStatus.undefined, error)
            return
        }
        
        if let data = json["data"] as? [AnyHashable: Any]{
            
            if let success = data["success"] as? Bool{
                guard   let cs = data["consumer_status_follower"] as? String,
                    let csInt = Int(cs),
                    let fs = data["follower_status_consumer"] as? String,
                    let fsInt = Int(fs)
                    else{
                        callback(success, FollowerStatus.undefined, FollowerStatus.undefined, nil)
                        return
                }
                
                var consumerStatus = FollowerStatus.undefined
                if let ss = FollowerStatus(rawValue: csInt) {
                    consumerStatus = ss
                }
                
                var followerStatus = FollowerStatus.undefined
                if let ss = FollowerStatus(rawValue: fsInt) {
                    followerStatus = ss
                }
                
                callback(success, consumerStatus, followerStatus, nil)
                return
            }
        }else{
            if let wsError = json["Error"] as? [AnyHashable: Any] {
                var code = 0
                if let c = wsError["id"] as? Int{
                    code = c
                }
                let error = WebServiceInterface.error( withTitle: wsError["description_pt"] as? String, code: code)
                callback(false, FollowerStatus.undefined, FollowerStatus.undefined, error)
                return
            }
        }
        
        let error = WSSocial.genericError()
        callback(false, FollowerStatus.undefined, FollowerStatus.undefined, error)
        return

    }
    
    @objc static func genericError() -> Error {
        return WebServiceInterface.error(withTitle: "Não foi possível efetuar a operação.", code: 0)
    }
    
    /**
     Load all consumers that the user following
     @para profileId id do consumer
     */
    @objc static func following(_ profileId:String?, completion callback:@escaping (_ items:[PPFollowing]?, _ error:Error?) -> Void ){
        var post:[AnyHashable: Any] = [:]
        post["consumer_id"] = profileId
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlConsumerFollowing) { (result, response, responseData, err) in
                WSSocial.proccessResultListOfContacts(result, err: err, completition:callback)
        }
    }
    
    /**
     Load all consumers that the user followers
     @para profileId id do consumer
     */
    @objc static func followers(_ profileId:String?, completion callback:@escaping (_ items:[PPFollowing]?, _ error:Error?) -> Void ){
        var post:[AnyHashable: Any] = [:]
        post["consumer_id"] = profileId
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlConsumerFollowers) { (result, response, responseData, err) in
            WSSocial.proccessResultListOfContacts(result, err: err, completition:callback)
        }
    }

    
    /**
     Load all consumers that liked a feed item
     @para profileId id do consumer
     */
    @objc static func getListWhoLiked(_ feedId:String, completion callback:@escaping (_ items:[PPFollowing]?, _ error:Error?) -> Void ){
        var post:[AnyHashable: Any] = [:]
        post["feed_id"] = feedId
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlGetWhoLiked) { (result, response, responseData, err) in
            WSSocial.proccessResultListOfContacts(result, err: err, completition: callback)
        }
    }
    
    /**
     Process a list of contacts
     @para profileId id do consumer
     */
    @objc static func proccessResultListOfContacts(_ result:[AnyHashable:Any]?, err:Error?, completition callback:(_ items:[PPFollowing]?, _ error:Error?) -> Void ){
        if err != nil{
            callback(nil, err)
            return
        }
        
        guard let json = result else{
            let error = WSNewFeed.genericError()
            callback(nil, error)
            return
        }
        
        if let data = json["data"] as? [AnyHashable: Any]{
            var items:[PPFollowing] = [PPFollowing]()
            
            // feed list
            if let feed = data["list"] as? [NSObject]{
                for itemData in feed{
                    if let i = itemData as? [AnyHashable: Any]{
                        if let item = PPFollowing(jsonDict: i as [NSObject : AnyObject]) {
                            items.append(item)
                        }
                    }
                }
            }
            
            callback(items, nil)
            return
        }else{
            if let wsError = json["Error"] as? [AnyHashable: Any] {
                var code = 0
                if let c = wsError["id"] as? Int {
                    code = c
                }
                let error = WebServiceInterface.error( withTitle: wsError["description_pt"] as? String, code: code)
                callback(nil, error as Error?)
                return
            }
        }
        
        let error = WSNewFeed.genericError()
        callback(nil, error)
        return
    }
    
    /**
     Remove the followers
     @param profile_id consumer id
     */
    @objc static func removeFollower(_ profileId:String, completion callback:@escaping (_ success:Bool, _ error:Error?) -> Void ){
        
        let post:[AnyHashable: Any] = ["consumer_id":profileId]
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlRemoveConsumerFollower) { (result, response, friends, err) in
            
            if err != nil{
                callback(false, err as Error?)
                return
            }
            
            guard let json = result else{
                let error = WSSocial.genericError()
                callback(false, error)
                return
            }
            
            if let data = json["data"] as? [AnyHashable: Any]{
                
                // paser consumer profile
                if let success = data["success"] as? Bool{
                    callback(success, err)
                    return
                }
                
                callback(false, err)
                return
                
            }else{
                if let wsError = json["Error"] as? [AnyHashable: Any] {
                    var code = 0
                    if let c = wsError["id"] as? Int{
                        code = c
                    }
                    let error = WebServiceInterface.error( withTitle: wsError["description_pt"] as? String, code: code)
                    callback(false, error)
                    return
                }
            }
            
            let error = WSSocial.genericError()
            callback(false, error)
            return
        }
    }
    
    @objc static func consumerMgmWith(complete: @escaping (PPMgmConfigs?, Error?) -> Void) {
        let params: [String: Any] = [:]
        WebServiceInterface.dictionary(withPostVarsSwift: params, serviceMethodUrl: kWsUrlGetConsumerMgm) { (result, response, responseData, error) in
            if let error = error {
                complete(nil, error as Error?)
                return
            }
            guard let jsonResult = result else {
                complete(nil, WSSocial.genericError())
                return
            }
            if let data = jsonResult["data"] as? [String: Any], let mgm = data["Mgm"] as? [String: Any] {
                let mgm: PPMgmConfigs = PPMgmConfigs(mgm)
                AppParameters.global().mgmConfigs = mgm
                complete(mgm, nil)
            }
        }
    }
    
    /**
     The list of follow suggestions
     @param profile_id consumer id
     */
    @objc static func followSuggestions(_ callback:@escaping (_ list:[PPContact]?, _ error:Error?) -> Void ){
        
        guard let consumerId = ConsumerManager.shared.consumer?.wsId else {
            return
        }
        
        let post:[AnyHashable: Any] = ["consumer_id": "\(consumerId)"]
        WebServiceInterface.dictionary(withPostVarsSwift: post, serviceMethodUrl: kWsUrlGetFollowsSuggestions) { (result, response, responseData, err) in
            
            if err != nil{
                callback(nil, err as Error?)
                return
            }
            
            guard let json = result else{
                let error = WSSocial.genericError()
                callback(nil, error)
                return
            }
            
            if let data = json["data"] as? [AnyHashable: Any]{
                var list:[PPContact] = []
                
                // paser consumer profile
                if let suggestions = data["suggestions"] as? [[AnyHashable: Any]] {
                    for item in suggestions {
                        if let profile = item["Consumer"] as? [AnyHashable: Any] {
                            list.append(PPContact(profileDictionary: profile))
                        }
                    }
                }
                
                callback(list, err)
                return
                
            }else{
                if let wsError = json["Error"] as? [AnyHashable: Any] {
                    var code = 0
                    if let c = wsError["id"] as? Int{
                        code = c
                    }
                    let error = WebServiceInterface.error( withTitle: wsError["description_pt"] as? String, code: code)
                    callback(nil, error)
                    return
                }
            }
            
            let error = WSSocial.genericError()
            callback(nil, error)
            return
        }
    }
}


