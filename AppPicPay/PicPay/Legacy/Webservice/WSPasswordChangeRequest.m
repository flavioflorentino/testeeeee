//
//  WSPasswordChangeRequest.m
//  MoBuy
//
//  Created by Diogo Carneiro on 07/08/12.
//
//

#import "WSPasswordChangeRequest.h"
#import "PicPay-Swift.h"

@implementation WSPasswordChangeRequest

/*!
 * Solicita alteração de senha do usuário
 * Callback:
 *   - @b BOOL => success
 *   - @b NSError => erro
 * @param currentPassword
 * @param newPassword
 * @param callback
 */
+ (void)changePassword: (NSString *)currentPassword newPassword:(NSString *)newPassword completionHandler:(void (^)(BOOL success, NSError *error)) callback{
	//NSError *err;
	[WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:currentPassword, newPassword, nil] forKeys:[NSArray arrayWithObjects:@"oldpass", @"newpass", nil]] serviceMethodUrl:kWsUrlChangePassword completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(NO, err);
            return;
        }
        
        NSString *data = [json valueForKey:@"data"];
        NSArray *wsError = [json valueForKey:@"Error"];
        
        if (data) {
            [User updateToken:data];
            callback(true, err);
            return;
        }else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] intValue]];
            callback(false, error);
            return;
        }
    }];
	
	
}


@end
