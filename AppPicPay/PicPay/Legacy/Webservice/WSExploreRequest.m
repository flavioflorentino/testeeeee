//
//  WSExploreRequest.m
//  PicPay
//
//  Created by Diogo Carneiro on 03/04/13.
//
//

#import "WSExploreRequest.h"
#import "WebServiceInterface.h"

@import CoreLegacy;

@implementation WSExploreRequest

/*!
 * Retorna os creditos globais do usuário (Crédito na carteira)
 * Callback:
 *   - @b NSDecimalNumber => crédito do usuário
 *   - @b NSError => erro
 * @param completionHandler callback
 */
+ (void)globalCredits:(void (^)(NSDecimalNumber *credits, NSError *error)) callback{
	
	[WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObject:@""] forKeys:[NSArray arrayWithObject:@""]] serviceMethodUrl:kWsUrlGetGlobalCredits completionHandler:^(NSArray *json, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        if(err) {
            callback(nil, err);
            return;
        }
        
        NSArray *wsError = [json valueForKey:@"Error"];
        NSArray *data = [json valueForKey:@"data"];
        
        if (data) {
            @try {
                NSString *balanceString = [data valueForKey:@"balance"];
                NSDecimalNumber *balance = [NSDecimalNumber decimalNumberWithString:balanceString];
                callback(balance, err);
                return;
            }
            @catch (NSException *exception) {
                callback(0, err);
                return;
            }
            
        } else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] integerValue]];
            callback(nil, error);
            return;
        }
    }];
}

@end
