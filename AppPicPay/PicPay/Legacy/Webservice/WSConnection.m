//
//  WSConnection.m
//  PicPay
//
//  Created by Luiz Henrique ; on 26/04/16.
//
//

#import "WSConnection.h"
@import Core;

/*!
 * Class Wrapper para efeutar uma requisiçao com delegate baseada em code block
 */
@implementation WSConnection{
    
}



/*!
 * efetua uma requisição baseado no request.
 * Wrapper para efeutar uma requisiçao com codeblock baseada em delegate
 */
- (void) sendRequest:(NSURLRequest* )request completionHandler:(ConnectionCallback)callback progressCallback:(ConnectionProgressCallback)progressCallback{
    self.callback = callback;
    self.progressCallback = progressCallback;
    self->_data = [NSMutableData data];
    
    __weak WSConnection* selfWeak = self;
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session=[NSURLSession sessionWithConfiguration:config delegate:selfWeak delegateQueue:nil];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request];
    
    [task resume];
}

/*!
 * efetua uma requisição baseado no request.
 * Wrapper para efeutar uma requisiçao com codeblock baseada em delegate
 */
- (void) sendRequest:(NSURLRequest* )request completionHandler:(ConnectionCallback)callback{
    
    [self sendRequest:request completionHandler:callback progressCallback:nil];
}

# pragma mark NSURLSessionTaskDelegate

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    if(error){
        self.callback(nil, [task response] ,error);
    }else{
        self.callback([NSData dataWithData:self->_data], [task response]  , nil);
    }
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data
{
    if(data!=nil){
        [self->_data appendData:data];
    }
    
}

- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
   didSendBodyData:(int64_t)bytesSent
    totalBytesSent:(int64_t)totalBytesSent
totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend{
    if(_progressCallback != nil){
        self.progressCallback((totalBytesSent * 100)/totalBytesExpectedToSend);
    }
}

// MARK: -Pinning configuration
- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler {
    PinningHandler *pinningHandler = [[PinningHandler alloc]init];
    [pinningHandler handleWithChallenge:challenge completionHandler:completionHandler];
}

@end
