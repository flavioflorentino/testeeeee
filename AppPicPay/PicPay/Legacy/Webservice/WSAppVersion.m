//
//  WSAppVersion.m
//
//  Created by Diogo Carneiro on 04/07/12
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WSAppVersion.h"


@implementation WSAppVersion

/*!
 * Envia um novo contato
 *
 * Callback:
 *
 *   - @b BOOL => needUpdate
 *
 *   - @b NSError => erro
 * 
 *   - @b NSString => message
 *
 * @param callback
 */
+ (void)needUpdateAppVersion:(void (^)(BOOL needUpdate,NSString *message, NSError *error)) callback{
    
	[WebServiceInterface dictionaryWithPostVars:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"], @"ios", nil] forKeys:[NSArray arrayWithObjects:@"version", @"os", nil]] serviceMethodUrl:kWsUrlVerifyVersionUpdate completionHandler:^(NSArray *data, NSURLResponse *response, NSData *responseData, NSError *err) {
        
        NSArray *wsError = [data valueForKey:@"Error"];
        NSArray *json = [data valueForKey:@"data"];
        
        if(err) {
            callback(NO, nil, err);
            return;
        }
        
        if (json) {
            BOOL needUpdate = [[json valueForKey:@"version_deprecated"] boolValue];
            NSString *string = [json valueForKey:@"description_pt"];
            callback(needUpdate, string, err);
            return;
        }else{
            NSError *error = [WebServiceInterface errorWithTitle:[wsError valueForKey:@"description_pt"] code:[[wsError valueForKey:@"id"] integerValue]];
            callback(NO, nil, error);
            return;
        }
    }];
}

@end
