import Foundation
import Registration

final class AuthProxy: RegistrationRenewalAuthDelegate {
    private var auth: PPAuth?
    
    func authenticate(
        sucessHandler: @escaping (_ password: String, _ isBiometric: Bool) -> Void,
        cancelHandler: @escaping () -> Void
    ) {
        auth = PPAuth.authenticate({ password, biometry in
            guard let password = password else {
                return
            }
            
            sucessHandler(password, biometry)
        }, canceledByUserBlock: cancelHandler)
    }
}
