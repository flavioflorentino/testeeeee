import UIKit
import SwiftyJSON

enum TransactionStatus: String {
    case autoClearance = "AUTO_CLEARANCE"
    case inAnalysis = "IN_ANALYSIS"
}

final class DefaultFeedItem: FeedItem {
    var image: ImageFeedWidget?
    var title: TextLinkedFeedWidget
    var text: TextFeedWidget?
    var money: TextFeedWidget?
    var timeAgo: TextFeedWidget?
    var timeAgoIcon: IconFeedWidget?
    var comments: TextCheckFeedWidget?
    var like: TextCheckFeedWidget?
    var alert: AlertFeedWidget?
    var deprecatedButton: ButtonFeedWidget?
    var buttons = [ButtonType: Button]()
    var transactionStatus: TransactionStatus?
    
    enum ButtonType: String {
        case primary
        case secondary
    }
    
    override init() {
        title = TextLinkedFeedWidget(jsonDict: ["Value" as NSObject: "" as AnyObject])!
        super.init()
    }
    
    override init?(jsonDict: [AnyHashable: Any]) {
        guard let config = jsonDict["Config"] as? [AnyHashable: Any],
            let titleData = config["Title"] as? [AnyHashable: Any],
            let title = TextLinkedFeedWidget(jsonDict: titleData as [NSObject: AnyObject]) else {
                return nil
        }
        
        self.title = title
        super.init(jsonDict: jsonDict as [NSObject: AnyObject])
        
        if let status = jsonDict["TransactionStatus"] as? String {
            self.transactionStatus = TransactionStatus(rawValue: status)
        }
        if let image = config["Image"] as? [AnyHashable: Any] {
            self.image = ImageFeedWidget(jsonDict: image as [NSObject: AnyObject])
        }
        if let text = config["Text"] as? [AnyHashable: Any] {
            self.text = TextFeedWidget(jsonDict: text as [NSObject: AnyObject])
        }
        if let money = config["MoneyLabel"] as? [AnyHashable: Any] {
            self.money = TextFeedWidget(jsonDict: money as [NSObject: AnyObject])
        }
        if let timeAgoData = config["TimeAgo"] as? [AnyHashable: Any] {
            if let timeAgo = timeAgoData["Text"] as? [AnyHashable: Any] {
                self.timeAgo = TextFeedWidget(jsonDict: timeAgo as [NSObject: AnyObject])
            }
            if let timeAgoIcon = timeAgoData["Icon"] as? [AnyHashable: Any] {
                self.timeAgoIcon = IconFeedWidget(jsonDict: timeAgoIcon as [NSObject: AnyObject])
            }
        }
        if let comments = config["Comments"] as? [AnyHashable: Any] {
            self.comments = TextCheckFeedWidget(jsonDict: comments as [NSObject: AnyObject])
        }
        if let like = config["Liked"] as? [AnyHashable: Any] {
            self.like = TextCheckFeedWidget(jsonDict: like as [NSObject: AnyObject])
        }
        if let alert = config["Alert"] as? [AnyHashable: Any] {
            self.alert = AlertFeedWidget(jsonDict: alert as [NSObject: AnyObject])
        }
        if let buttonsArray = config["Buttons"] as? [NSObject] {
            for buttonItem in buttonsArray {
                parseButton(buttonItem)
            }
        }
        if let button = config["Button"] as? [NSObject: AnyObject] {
            self.deprecatedButton = ButtonFeedWidget(jsonDict: button)
        }
    }

    private func parseButton(_ buttonItem: NSObject) {
        guard let buttonDict = buttonItem as? [AnyHashable: Any],
            let typeRawValue = buttonDict["Place"] as? String,
            let descriptionData = buttonDict["Params"] as? [AnyHashable: Any],
            let buttonType = ButtonType(rawValue: typeRawValue),
            let button = Button(json: JSON(descriptionData)) else {
            return
        }
        buttons[buttonType] = button
    }
}
