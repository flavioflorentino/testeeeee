#import "PPAuth.h"
#import "WSConsumer.h"
#import "PicPay-Swift.h"
#import "ViewsManager.h"
#import "Loader.h"

@import Core;
@import FeatureFlag;
@import UI;

@interface PPAuth()
@property (nonatomic, strong, readonly) PopupPassword *alert;
@end

@implementation PPAuth

@synthesize alert;

- (id)init{
    self = [super init];
    if (self != nil) {
        context = [[LAContext alloc] init];
        BiometricTypeAuth *biometric = [[BiometricTypeAuth alloc] init];
        context.localizedFallbackTitle = @"Digite a senha PicPay";
        
        if ([biometric biometricType] == BiometricTypeFaceID) {
            reason = @"Use o Face ID para essa ação.";
            alertMessages = @{@"accepted" : @"Agora suas ações poderão ser autenticadas com o Face ID.",
                              @"refused" : @"Você pode habilitar o Face ID a qualquer momento na tela do seu Perfil."};
        } else {
            reason = @"Use o Touch ID para essa ação.";
            alertMessages = @{@"accepted" : @"Agora suas ações poderão ser autenticadas com o Touch ID.",
                              @"refused" : @"Você pode habilitar o Touch ID a qualquer momento na tela do seu Perfil."};
        }
        alert = [[PopupPassword alloc] init];
    }
    
    return self;
}

- (void)handleError:(NSError *)error callback:(void(^)(NSError *error))callback{
    if (error.code == 6003) {
        if ([PPAuth useTouchId]) {
            [PPAuth setUseTouchId:NO];
            auxiliarErrorTreatmentAuthComponent = [PPAuth authenticateAutoCancel:0
                                                                         success:self.successBlock
                                                             canceledByUserBlock:self.canceledByUserBlock];
        } else {
            [PPAuthSwift handlePasswordError];
        }
    } else {
        callback(error);
    }
}


+ (PPAuth *)authenticate:(void(^)(NSString *authToken, BOOL biometry))successBlock
     canceledByUserBlock:(void(^)(void))canceledByUserBlock{
    return [PPAuth authenticateAutoCancel:0 success:successBlock canceledByUserBlock:canceledByUserBlock];
}

+ (PPAuth *)authenticateAutoCancel:(NSTimeInterval)timeInterval
                           success:(void(^)(NSString *authToken, BOOL biometry))successBlock
               canceledByUserBlock:(void(^)(void))canceledByUserBlock {
    PPAuth *auth = [[PPAuth alloc] init];
    auth.successBlock = successBlock;
    auth.canceledByUserBlock = canceledByUserBlock;
    auth.timeInterval = timeInterval;
    
    if ([auth touchIdAvailable] && [PPAuth useTouchId]) {
        [auth openBiometricAuthentication];
    } else {
        [auth openManualEntryAhtentication];
    }
    
    return auth;
}

- (void)enableBiometricAuthentication {
    if ([self touchIdAvailable]) {
        [self openManualEntryAhtenticationToEnableTouchId];
    }
}

/*!
 * Update the authentication token for touch id
 */
+ (void)updateBiometricAuthenticationTokenIfNeed:(NSString* )pin{
    @try {
        if ([PPAuth useTouchId] && [[KVStore new] boolFor:KVKeyNeedUpdateBiometricAuthPin]) {
            [PPAuthSwift setAuthenticationTokenWithBiometryWithValue:pin onSuccess:^{} onError:^(NSError *error) {}];
        }
        
    }
    @catch (NSException *exception) {
        DebugLog(@"ERROR: Houve um erro ao salvar o TouchID authentication Token");
    }
}

- (void)biometricAuthenticationWelcomeAlertForViewController:(UIViewController *)viewController
                                              withCompletion:(void(^)(void))callback{
    
    if ([self touchIdAvailable]) {
        if ([PPAuthSwift temporaryAuthenticationTokenStatus] == PPAuthTempTokenStatusRegister) {
            [self askToEnableBiometricAuthentication];
        }else{
            __weak PPAuth * wealSelf = self;
            touchIdRequestView = [TouchIdRequestPopUpController new];
            touchIdRequestView.buttonTapAction = ^void() {
                [wealSelf popUpRequestAccepted];
                callback();
            };
            touchIdRequestView.closeTapAction = ^void() {
                [wealSelf popUpRequestRefused];
                callback();
            };
            
            if ([PPAuth isTouchIdKeychainBug]) {
                BiometricTypeAuth *biometric = [[BiometricTypeAuth alloc] init];
                [touchIdRequestView.actionButton setTitle:@"Confirmar senha" forState:UIControlStateNormal];
                
                if ([biometric biometricType] == BiometricTypeFaceID) {
                    [touchIdRequestView.textLabel setText:@"Para continuar usando o Face ID em seus pagamentos, é necessário confirmar sua senha PicPay novamente."];
                } else {
                    [touchIdRequestView.textLabel setText:@"Para continuar usando o Touch ID em seus pagamentos, é necessário confirmar sua senha PicPay novamente."];
                }
                [touchIdRequestView.titleLabel setText:@"Confirme sua senha"];
            }
            popupPresenter = [PopupPresenter new];
            [popupPresenter showPopup:touchIdRequestView];
        }
    }
}

- (void)biometricAuthenticationStatus:(BOOL)enabled {
    [PPAuth setTouchIdAsked:YES];
    [PPAuth setUseTouchId:enabled];
    [PPAuthSwift setTemporaryAuthenticationToken:nil status:PPAuthTempTokenStatusInexistent];
    [touchIdRequestView dismissViewControllerAnimated:YES completion:nil];
    
    if (!enabled) {
        UIViewController *controller = [[[AppManager shared] mainScreenCoordinator] currentController];
        if (controller != nil) {
            [AlertMessage showAlertWithMessage: [alertMessages objectForKey:@"refused"] controller: controller];
        }
    }
}

- (void)biometricAuthenticationPopupResult:(BOOL)enabled {
    __weak typeof(self) weakSelf = self;
    NSString *authToken = (enabled ? [PPAuthSwift temporaryAuthenticationToken] : nil);
    [PPAuthSwift setAuthenticationTokenWithBiometryWithValue:authToken onSuccess:^{
        [weakSelf biometricAuthenticationStatus:enabled];
    } onError:^(NSError *error) {}];
}

/*!
 * get the representation of the current set of enrolled fingers
 * the biometric authentication is disabled and a prompt is shown for that
 * the user insert the password to validate the new enrolled fingers
 */
+ (void)saveEvaluatedPolicyDomainState {
    LAContext *context = [[LAContext alloc] init];
    [context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil];
    NSData * domainState = [context evaluatedPolicyDomainState];
    if( domainState != nil) {
        [[KVStore new] setBool:NO with:KVKeyNeedUpdateBiometricAuthPin];
    }
}

- (void)popUpRequestAccepted{
    if([PPAuthSwift temporaryAuthenticationTokenStatus] == PPAuthTempTokenStatusInexistent){
        auxiliarAuthComponent = [[PPAuth alloc] init];
        auxiliarAuthComponent.delegate = self;
        [auxiliarAuthComponent enableBiometricAuthentication];
    }else{
        [self biometricAuthenticationPopupResult:YES];
    }
}

- (void)biometricAuthenticationWillUseNetwork{
    // splash screen
    if (loaderView == nil) {
        loaderView = [Loader getLoadingView:@"Aguarde..."];
    }
    
    loaderView.hidden = NO;
    [touchIdRequestView.actionButton startLoadingAnimatingWithStyle:UIActivityIndicatorViewStyleWhite isEnabled:NO];
}

- (void)biometricAuthenticationDidUseNetwork{
    [loaderView removeFromSuperview];
}

- (void)popUpRequestRefused{
    [self biometricAuthenticationPopupResult:NO];
}

- (void)disableBiometricAuthentication{
    [PPAuth setUseTouchId:NO];
}

+ (void)clearAuthenticationData{
    [PPAuthSwift clearAuthenticationToken];
    [PPAuth setUseTouchId:NO];
    [PPAuthSwift setTemporaryAuthenticationToken:nil status:PPAuthTempTokenStatusInexistent];
}

- (BOOL)touchIdAvailable {
    LAContext *context = [LAContext new];
    NSError *authError = nil;
    return [context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError];
}

- (void)openBiometricAuthentication {
    __weak typeof(self) weakSelf = self;
    if ([self touchIdAvailable]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [PPAuthSwift authenticationTokenWithBiometry:^(NSString *authToken) {
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    if (authToken == nil) {
                        [weakSelf openManualEntryAhtentication];
                        return;
                    }
                    weakSelf.successBlock(authToken, YES);
                });
            } onError:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [weakSelf openManualEntryAhtentication];
                });
            }];
        });
    } else{
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [weakSelf openManualEntryAhtentication];
        });
    }
}

- (void)openManualEntryAhtentication{
    [self openManualEntryAhtenticationForTransaction];
}

- (void)openManualEntryAhtenticationForTransaction {
    [self openManualEntryAhtenticationWithIdPopup:1 andMessage:nil];
}

- (void)openManualEntryAhtenticationToEnableTouchId{
    [self openManualEntryAhtenticationWithId:2 andMessage:nil];
}

- (void)openManualEntryAhtenticationWithIdPopup:(int)alertId andMessage:(NSString *)message {
    __weak typeof(self) weakSelf = self;
    [PPAuthSwift clearAuthenticationToken];
    [alert showAlertWithIsBiometricActive:YES onPay:^(NSString * _Nonnull password, BOOL biometric, PasswordPopupViewController * _Nonnull popup, UIPPButton * _Nonnull button) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (biometric) {
                [weakSelf selectAuthenticationWithBiometric:password makePayment:YES popup:popup button:button];
            } else {
                [popup dismissViewControllerAnimated:YES completion:^{
                    weakSelf.successBlock(password, NO);
                }];
            }
        });
    } onClose:^{
        if (weakSelf.canceledByUserBlock != nil) {
            weakSelf.canceledByUserBlock();
        }
    }];
}

- (void)activateAuthenticationWithBiometric:(NSString *)password
                                      popup:(PasswordPopupViewController *)popup
                                     button:(UIPPButton *) button {
    __weak typeof(self) weakSelf = self;
    if ([self.delegate respondsToSelector:@selector(biometricAuthenticationWillUseNetwork)]) {
        [self.delegate biometricAuthenticationWillUseNetwork];
    }
    
    [WSConsumer verifyPassword:password completionHandler:^(BOOL validPassword, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [popup enableAllButtons];
            [popup dismissViewControllerAnimated:YES completion:^{
                if (weakSelf) {
                    __strong typeof(weakSelf) strongSelf = weakSelf;

                    if ([strongSelf.delegate respondsToSelector:@selector(biometricAuthenticationDidUseNetwork)]) {
                        [strongSelf.delegate biometricAuthenticationDidUseNetwork];
                    }
                    if (error) {
                        [AlertMessage showCustomAlertWithError: error controller: nil completion: nil];
                    }
                    if (validPassword) {
                        // Execute rules enter function
                        [strongSelf prepareAndSetAutenticateWithBiometry:password isPayment:NO paymentBlock:^{}];
                        return;
                    } else {
                        [PPAuthSwift clearAuthenticationToken];
                        [PPAuth setUseTouchId:NO];
                    }
                    [strongSelf verifyAndExecuteDelegateForBiometricAuthenticationStatusIsPayment:NO paymentBlock:^{}];
                }
            }];
        });
    }];
}

- (void)verifyAndExecuteDelegateForBiometricAuthenticationStatusIsPayment:(BOOL)isPayment paymentBlock:(void(^)(void))paymentBlock {
    if ([self.delegate respondsToSelector:@selector(biometricAuthenticationStatus:)]) {
        [self.delegate biometricAuthenticationStatus:[PPAuth useTouchId]];
    }
    if (isPayment) {
        paymentBlock();
    }
}

/// Set configurations do keychain auth token. executing delegates and configurations for segues workflows
/// @param password password for keychain
/// @param isPayment params for check rule for payment
- (void)prepareAndSetAutenticateWithBiometry:(NSString *)password isPayment:(BOOL)isPayment paymentBlock:(void(^)(void))paymentBlock {
    __weak typeof(self) weakSelf = self;
    [PPAuthSwift setAuthenticationTokenWithBiometryWithValue:password onSuccess:^{
        [PPAuth setUseTouchId:YES];
        [weakSelf verifyAndExecuteDelegateForBiometricAuthenticationStatusIsPayment:isPayment paymentBlock:^{
            paymentBlock();
        }];
        NSLog(@"autenticação feita com sucesso");
    } onError:^(NSError *error) {
        [PPAuthSwift clearAuthenticationToken];
        [PPAuth setUseTouchId:NO];
        [weakSelf verifyAndExecuteDelegateForBiometricAuthenticationStatusIsPayment:isPayment paymentBlock:^{
            paymentBlock();
        }];
    }];
}

- (void)selectAuthenticationWithBiometric:(NSString *)password
                              makePayment:(BOOL)payment
                                    popup:(PasswordPopupViewController *)popup
                                   button:(UIPPButton *) button {
    
    if ([self.delegate respondsToSelector:@selector(biometricAuthenticationWillUseNetwork)]) {
        [self.delegate biometricAuthenticationWillUseNetwork];
    }
    
    [button startLoadingAnimatingWithStyle:UIActivityIndicatorViewStyleWhite isEnabled:false];
    [popup disableAllButtons];
    
    __weak typeof(self) weakSelf = self;

    [WSConsumer verifyPassword:password completionHandler:^(BOOL validPassword, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [popup enableAllButtons];
            [popup dismissViewControllerAnimated:YES completion:^{
                if (weakSelf) {
                    __strong typeof(weakSelf) strongSelf = weakSelf;

                    if ([strongSelf.delegate respondsToSelector:@selector(biometricAuthenticationDidUseNetwork)]) {
                        [strongSelf.delegate biometricAuthenticationDidUseNetwork];
                    }
                    if (error && !payment) {
                        [AlertMessage showCustomAlertWithError: error controller: popup completion: nil];
                    }
                    if (validPassword) {
                        [strongSelf prepareAndSetAutenticateWithBiometry:password isPayment:payment paymentBlock:^{
                            strongSelf.successBlock(password, NO);
                        }];
                        return;
                    } else {
                        [PPAuthSwift clearAuthenticationToken];
                        [PPAuth setUseTouchId:NO];
                    }
                    
                    [strongSelf verifyAndExecuteDelegateForBiometricAuthenticationStatusIsPayment:payment paymentBlock:^{
                        strongSelf.successBlock(password, NO);
                    }];
                }
            }];
        });
    }];
}

- (void)openManualEntryAhtenticationWithId:(int)alertId andMessage:(NSString *)message{
    __weak typeof(self) weakSelf = self;
    [alert showAlertWithIsBiometricActive:NO
                                    onPay:^(NSString * _Nonnull password, BOOL biometric, PasswordPopupViewController * _Nonnull popup, UIPPButton * _Nonnull button) {
        [weakSelf touchIdActivationConfirm: password popup: popup button: button];
    } onClose:^{
        [weakSelf touchIdActivationCancel];
    }];
}

- (void)askToEnableBiometricAuthentication{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *continuar = [UIAlertAction actionWithTitle:@"Habilitar"
                                                        style:UIAlertActionStyleDefault
                                                      handler: ^(UIAlertAction * action) {
        [self popUpRequestAccepted];
    }];
    
    UIAlertAction *cancelar = [UIAlertAction actionWithTitle: @"Agora não"
                                                       style: UIAlertActionStyleCancel
                                                     handler: ^(UIAlertAction * action) {
        [self popUpRequestRefused];
    }];
    
    [alert addAction: cancelar];
    [alert addAction: continuar];
    
    UIViewController *v = [UIViewController alloc];
    
    BiometricTypeAuth *biometric = [[BiometricTypeAuth alloc] init];
    
    if ([biometric biometricType] == BiometricTypeFaceID) {
        UIView *xib = [[[NSBundle mainBundle] loadNibNamed: @"FaceIdRequestAlert" owner:self options:nil] lastObject];
        [v.view addSubview:xib];
    } else {
        UIView *xib = [[[NSBundle mainBundle] loadNibNamed:@"TouchIdRequestAlert" owner:self options:nil] lastObject];
        [v.view addSubview:xib];
    }
    
    [alert setValue:v forKey:@"contentViewController"];
    
    UIViewController *controller = [[[AppManager shared] mainScreenCoordinator] homeNavigation];
    if (controller != nil) {
        
        [controller presentViewController:alert animated:YES completion:nil];
    }
    
}

- (void)touchIdActivationCancel {
    if ([self.delegate respondsToSelector:@selector(biometricAuthenticationStatus:)]) {
        [self.delegate biometricAuthenticationStatus:[PPAuth useTouchId]];
    }
}

- (void)touchIdActivationConfirm: (NSString* )password popup:(PasswordPopupViewController *)popup button:(UIPPButton *)button {
    [self activateAuthenticationWithBiometric:password popup:popup button:button];
}

+ (void)setUseTouchId:(BOOL)use {
    [[KVStore new] setBool:use with:KVKeyUse_touch_id];
    
    if (!use) {
        [PPAuthSwift clearAuthenticationToken];
    }
    
    if ([FeatureManager isActiveTouchIdAnalyticsOBJC]){
        NSDictionary* properties = @{@"callStack": [NSThread callStackSymbols]};
        [PPAnalytics trackEvent:use ? @"TouchId_ON" : @"TouchId_OFF" properties: properties];
    }
}

+ (BOOL)useTouchId {
    return [[KVStore new] boolFor: KVKeyUse_touch_id];
}

+ (BOOL)shouldAskForBiometricAuthentication{
    PPAuth *auth = [[PPAuth alloc] init];
    if (![auth touchIdAvailable]){
        return NO;
    }

    return ![[KVStore new] boolFor: KVKeyTouch_id_asked] && ![PPAuth useTouchId];
}

+ (void)setTouchIdAsked:(BOOL)asked {
    [[KVStore new] setBool:asked with:KVKeyTouch_id_asked];
}


+ (BOOL)isTouchIdKeychainBug {
    return [[KVStore new] boolFor:KVKeyIsTouchIdKeychainBug];
}

@end
