//
//  PPFollowing.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 22/12/16.
//
//

final class PPFollowing: NSObject {
    
    var contact:PPContact
    var status:FollowerStatus
    
    init(contact: PPContact, status: FollowerStatus) {
        self.contact = contact
        self.status = status
    }
    
    init?(jsonDict:[AnyHashable: Any]) {
        guard
        let profile = jsonDict["profile"] as? [AnyHashable: Any]
            else{ return nil }
        
        self.contact = PPContact(profileDictionary: profile)
        self.status = FollowerStatus.undefined
        
        if let statusString = jsonDict["consumer_status_follower"] as? String{
            if let status = Int(statusString) {
                if let followSt = FollowerStatus(rawValue: status){
                    self.status = followSt
                }
            }
        }
        
        if let status = jsonDict["consumer_status_follower"] as? Int{
            if let followSt = FollowerStatus(rawValue: status){
                self.status = followSt
            }
        }
        
    }
}
