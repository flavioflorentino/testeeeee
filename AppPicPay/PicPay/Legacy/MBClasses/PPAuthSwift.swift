import Core
import NewRelic

@objcMembers
final class PPAuthSwift: NSObject {
    typealias Dependencies = HasKeychainManager & HasMainQueue & HasGlobalQueue
    private static let dependencies: Dependencies = DependencyContainer()
    
    private static let topmostViewControllerFrom: ((UIViewController) -> UIViewController) = { vc in
        var vcStack: [UIViewController] = [vc]
        while true {
            if let nextVC = vcStack.last?.presentedViewController {
                if !vcStack.contains(nextVC) {
                    vcStack.append(nextVC)
                } else {
                    //Caso cair nessa condição, significa que...
                    //algo pode estar errado na construção da navegação das viewControllers,
                    //existe um ciclo de apresentação de viewControllers
                    return nextVC
                }
            } else {
                if vcStack.last is UINavigationController {
                    return (vcStack.last as? UINavigationController)?.viewControllers.last ?? vc
                } else {
                    return vcStack.last ?? vc
                }
            }
        }
    }
    
    private static func closeAction(alertPopupController: AlertPopupViewController) {
        //Para os pagamentos P2P e PAV, a barra de pagamento é uma accessoryView.
        //Ao disparar que a viewController está visivel novamente, move o foco para
        //edição do valor, fazendo o teclado aparecer, juntamente com a barra de pagamento
        guard let rootVC = UIApplication.shared.keyWindow?.rootViewController else {
            return
        }
        alertPopupController.dismiss(animated: true) {
            PPAuthSwift.topmostViewControllerFrom(rootVC).viewDidAppear(true)
        }
    }
    
    private static func recoverPasswordAction(alertPopupController: AlertPopupViewController) {
        guard let rootVC = UIApplication.shared.keyWindow?.rootViewController else {
            return
        }
        alertPopupController.dismiss(animated: true) {
            let webViewUrl = WsWebViewURL.passRemember.endpoint
            if let url = URL(string: webViewUrl) {
                let webView = WebViewFactory.make(with: url)
                BreadcrumbManager.shared.addCrumb("WebView", typeOf: BreadcrumbManager.CrumbType.webView, withProperties: ["url": webViewUrl])
                PPAuthSwift.topmostViewControllerFrom(rootVC).present(PPNavigationController(rootViewController: webView), animated: true)
            }
        }
    }
    
    static func handlePasswordError() {
        let popUp = Alert(title: Strings.Legacy.wrongPassword, text: Strings.Legacy.enteredIncorrectPassword)
        popUp.dismissOnTouchBackground = false
        popUp.dismissWithGesture = false
        popUp.showCloseButton = false
        
        let recoverPasswordButton = Button(title: DefaultLocalizable.recoverPassword.text, type: .cta) { popupController, _ in
            PPAuthSwift.recoverPasswordAction(alertPopupController: popupController)
        }
        
        let cancelButton = Button(title: DefaultLocalizable.notNow.text, type: .inline) { popupController, _ in
            PPAuthSwift.closeAction(alertPopupController: popupController)
        }
        popUp.buttons = [recoverPasswordButton, cancelButton]
        
        dependencies.mainQueue.async {
            AlertMessage.showAlert(popUp, controller: nil)
        }
    }
    
    /// Migrates the old password to the new one with biometrics, forcing the user to insert biometrics to make this key transition.
    static func migrateKeychainAuth() {
        guard !KVStore().boolFor(.hasMigratedAuthToken), let pin = dependencies.keychain.getData(key: KeychainKey.authToken) else { return }
        do {
            dependencies.keychain.clearValue(key: KeychainKey.authToken)
            try dependencies.keychain.setValueWithBiometry(key: KeychainKey.authToken, value: pin)
            KVStore().set(value: true, with: .hasMigratedAuthToken)
        } catch {
            recordInternalError(error: error)
        }
    }

    static func authenticationTokenWithBiometry(_ onSuccess: @escaping (_ authToken: String?) -> Void, onError: @escaping (_ error: Error) -> Void) {
        dependencies.globalQueue.async {
            do {
                let authToken = try self.dependencies.keychain.getValueWithBiometry(key: KeychainKey.authToken, authenticationPrompt: Strings.Legacy.Ppauth.authenticationPrompt)
                self.dependencies.mainQueue.async {
                    onSuccess(authToken)
                }
            } catch let error {
                self.recordInternalError(error: error)
                self.dependencies.mainQueue.async {
                    onError(error)
                }
            }
        }
    }
    
    static func setAuthenticationTokenWithBiometry(value: String, onSuccess: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) {
        dependencies.globalQueue.async {
            do {
                try self.dependencies.keychain.setValueWithBiometry(key: KeychainKey.authToken, value: value)
                self.dependencies.mainQueue.async {
                    onSuccess()
                }
            } catch let error {
                self.recordInternalError(error: error)
                self.dependencies.mainQueue.async {
                    onError(error)
                }
            }
        }
    }
    
    private static func recordInternalError(error: Error) {
        NewRelic.recordError(error, attributes: ["class": #file])
    }

    static func clearAuthenticationToken() {
        dependencies.keychain.clearValue(key: KeychainKey.authToken)
    }
    
    static func setTemporaryAuthenticationToken(_ token: String?, status: PPAuthTempTokenStatus) {
        if let token = token {
            dependencies.keychain.set(key: KeychainKey.tempAuthToken, value: token)
        } else {
            dependencies.keychain.clearValue(key: KeychainKey.tempAuthToken)
        }
        
        var newStatus = "inexistent"
        if token == nil {
            switch status {
            case PPAuthTempTokenStatusLogin:
                newStatus = "login"
            case PPAuthTempTokenStatusRegister:
                newStatus = "register"
            default:
                break
            }
        }
        
        dependencies.keychain.set(key: KeychainKey.tempAuthTokenStatus, value: newStatus)
    }
    
    static func temporaryAuthenticationTokenStatus() -> PPAuthTempTokenStatus {
        guard let status = dependencies.keychain.getData(key: KeychainKey.tempAuthTokenStatus) else {
            return PPAuthTempTokenStatusInexistent
        }
        switch status {
        case "login":
            return PPAuthTempTokenStatusLogin
        case "register":
            return PPAuthTempTokenStatusRegister
        default:
            return PPAuthTempTokenStatusInexistent
        }
    }
    
    static func temporaryAuthenticationToken() -> String? {
        dependencies.keychain.getData(key: KeychainKey.tempAuthToken)
    }
}
