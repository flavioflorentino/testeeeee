import CorePayment
import SwiftyJSON
import Validator

@objc
final class PicPayError: NSError, PicPayErrorDisplayable, FailurePayment {
    var message: String
    var picpayCode: String
    var title: String
    var messageId: Int? = nil
    var data: SwiftyJSONable?
    var alert: Alert?
    
    private static func convertCodeToSuper(_ code: String) -> Int {
        var convertedError = -1
        
        if let intError = Int(code) {
            convertedError = intError
        }
        return convertedError
    }
    
    @objc init(message: String, code: String = "", title: String? = nil, dictionary: NSDictionary? = nil) {
        self.message = message
        self.picpayCode = code
        self.title = title ?? ""
        
        if let data = dictionary {
            self.data = JSON(data)
        }
        
        super.init(domain: "PicPay", code: PicPayError.convertCodeToSuper(self.picpayCode), userInfo: nil)
    }
    
    // Old API  - Old Error init
    @objc init(apiJSON: NSDictionary) {
        let json = JSON(apiJSON)
        
        self.message = json["description_pt"].string ?? ""
        let possibleCode = json["id"].string
        if let code = possibleCode {
            self.picpayCode = code
        } else if let code = json["id"].int {
            self.picpayCode = String(code)
        } else {
            self.picpayCode = "Empty code"
        }
        
        self.data = JSON(json["data"].object)
        self.title = ""
        
        super.init(domain: "PicPay", code: PicPayError.convertCodeToSuper(self.picpayCode), userInfo: nil)
    }
    
    // Old API - New Error init
    @objc init(errorJSON: NSDictionary) {
        let json = JSON(errorJSON)
        
        self.message = json["message"].string ?? ""
        
        if let code = json["code"].string {
            self.picpayCode = code
        } else if let code = json["code"].int {
            self.picpayCode = String(code)
        } else {
            self.picpayCode = "Empty code"
        }
        
        if json["_ui"]["alert"].exists() {
            self.alert = Alert(json: json["_ui"]["alert"])
        }
        
        if json["data"].exists() {
            self.data = JSON(json["data"].object)
        }
        
        self.title = json["short_message"].string ?? ""
        
        super.init(domain: "PicPay", code: PicPayError.convertCodeToSuper(self.picpayCode), userInfo: nil)
    }
    
    // New API init
    init(json: SwiftyJSONable) {
        guard let json = json as? JSON else {
            self.message = ""
            self.picpayCode = "Empty code"
            self.title = ""
            super.init(domain: "PicPay", code: PicPayError.convertCodeToSuper(self.picpayCode), userInfo: nil)
            return
        }
        
        self.message = json["message"].string ?? "Tente novamente mais tarde"
        // TODO: New api code should be a string?
        
        if let code = json["code"].string {
            self.picpayCode = code
        } else if let code = json["code"].int {
            self.picpayCode = String(code)
        } else {
            self.picpayCode = "Empty code"
        }
        
        self.messageId = json["messageId"].int ?? -1
        self.title = json["short_message"].string ?? "Erro na requisição"
        
        if json["_ui"]["alert"].exists() {
            self.alert = Alert(json: json["_ui"]["alert"])
        }
        
        if json["data"].exists() {
            self.data = JSON(json["data"].object)
        }
        
        super.init(domain: "PicPay", code: PicPayError.convertCodeToSuper(self.picpayCode), userInfo: nil)
    }
    
    init(oldJSON: SwiftyJSONable) {
        guard
            let error = oldJSON as? JSON,
            let json = error["Error"].dictionary
            else {
                self.message = ""
                self.picpayCode = "Empty code"
                self.title = ""
                super.init(domain: "PicPay", code: PicPayError.convertCodeToSuper(self.picpayCode), userInfo: nil)
                return
        }
        
        self.message = json["description_pt"]?.string ?? ""
        let possibleCode = json["id"]?.string
        if let code = possibleCode {
            self.picpayCode = code
        } else if let code = json["id"]?.int {
            self.picpayCode = String(code)
        } else {
            self.picpayCode = "Empty code"
        }
        
        if let data = json["data"]?.object {
            self.data = JSON(data)
        }
        
        self.title = ""
        super.init(domain: "PicPay", code: PicPayError.convertCodeToSuper(self.picpayCode), userInfo: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var localizedDescription: String {
        get{
            return self.message
        }
    }
    
    override var description: String {
        return self.message
    }
    
    @objc
    func dataDictionary() -> [String: Any]? {
        return data?.dictionaryObject
    }
}

// MARK: Constants
@objc
extension PicPayError {
    public static let notConnectedToInternet = PicPayError(message: "Sem conexão com a internet.")
    static let generalError = PicPayError(message: "Ops! Ocorreu um erro. Tente novamente.")
}

extension PicPayError: LocalizedError {
    public var errorDescription: String? {
        return NSLocalizedString(self.message, comment: "")
    }
}

extension PicPayError: ValidationError { }
