@import FirebaseAnalytics;
@import Core;
@import CoreLegacy;
@import AnalyticsModule;

#import "PPAnalytics.h"
#import "PicPay-Swift.h"
#import <AnalyticsModule/AnalyticsModule.h>
#import <AppsFlyerLib/AppsFlyerLib.h>

@implementation PPAnalytics

+ (void)trackEvent:(NSString *)eventName withLabel:(NSString *)label andValue:(NSNumber *)value properties:(NSDictionary *)properties{
    [PPAnalytics trackEvent:eventName properties:properties];
}

+ (void)timeEvent:(NSString *)eventName{
    #if DEBUG
    [[AnalyticsLogger shared] infoWith:eventName properties:@{} from:@"mixpanel"];
    #else
    MixPanelOBJC *mixpanel = [MixPanelOBJC new];
    [mixpanel timeEvent:eventName];
    #endif
}

+ (void)trackEventWithCustomProperties:(NSString *)eventName properties:(NSDictionary *)properties {
    #if DEBUG
        [[AnalyticsLogger shared] infoWith:eventName properties:properties from:@"mixpanel"];
    #else
        MixPanelOBJC *mixpanel = [MixPanelOBJC new];
        [mixpanel track:eventName properties:properties];
    #endif
    
    [OBJCAnalytics logFirebaseWithName:eventName properties:properties];
}

+ (void)trackEvent:(NSString *)eventName properties:(NSDictionary *)properties includeMixpanel:(BOOL) includeMixpanel{
    NSMutableDictionary *newProperties  = [[NSMutableDictionary alloc] init];
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.calendar = [NSCalendar currentCalendar];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"pt_BR_POSIX"];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZZZZZ";
    NSString* timestamp = [formatter stringFromDate:[NSDate date]];
    [newProperties setObject:timestamp forKey:@"timestamp"];
    
    @try {
        [newProperties setObject:((CardsCountForObjc.count > 0) ? @"true" : @"false") forKey:@"Cartão de crédito cadastrado"];
    }
    @catch (NSException *exception) {
        [newProperties setObject:@"false" forKey:@"Cartão de crédito cadastrado"];
    }
    
    @try {
        [newProperties setObject:[NSString stringWithFormat:@"%lu", (long)ConsumerManager.shared.consumer.wsId] forKey:@"consumer_id"];
    }
    @catch (NSException *exception) {
        [newProperties setObject:@"0" forKey:@"consumer_id"];
    }
    
    @try {
        [newProperties setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"installation_id"];
    }
    @catch (NSException *exception) {
        [newProperties setObject:@"0" forKey:@"installation_id"];
    }
    
    @try {
        BOOL isPicPayLover = ConsumerManager.shared.consumer.isPicPayLover;
        [newProperties setObject:(isPicPayLover ? @"yes" : @"no") forKey:@"is_picpay_lover"];
    }
    @catch (NSException *exception) {
        [newProperties setObject:@"no" forKey:@"is_picpay_lover"];
    }
    
    
    @try {
        if (properties != nil) {
            [newProperties addEntriesFromDictionary:properties];
        }
    }
    @catch (NSException *exception) {
        
    }
    
    /*
     * MIXPANEL
     */
    if (includeMixpanel){
    #if DEBUG
        [[AnalyticsLogger shared] infoWith:eventName properties:newProperties from:@"mixpanel"];
    #else
        [OBJCAnalytics logMixpanelWithName:eventName properties:newProperties];
    #endif
    }
    [OBJCAnalytics logFirebaseWithName:eventName properties:newProperties];
}

+ (void)trackEvent:(NSString *)eventName properties:(NSDictionary *)properties{
    [PPAnalytics trackEvent:eventName properties:properties includeMixpanel:YES];
}

+ (void)trackFirstTimeOnlyEvent:(NSString *)eventName properties:(NSDictionary *)properties includeMixpanel:(BOOL) includeMixpanel {
    
    if (![[KVStore new] getFirstTimeOnlyEvent:eventName]) {
        [PPAnalytics trackEvent:eventName properties:properties includeMixpanel: includeMixpanel];
        [[KVStore new] setFirstTimeOnlyEvent:eventName];
    }
}

+ (void)trackFirstTimeOnlyEvent:(NSString *)eventName properties:(NSDictionary *)properties{
    [PPAnalytics trackFirstTimeOnlyEvent:eventName properties:properties includeMixpanel:YES];
}

+ (NSString *)distinctId{
	return [[MixPanelOBJC new] distinctId];
}

+ (NSString *)appsFlyerId{
    return [[AppsFlyerLib shared] getAppsFlyerUID];
}

+ (NSString *)idfa{
    return [[AppsFlyerLib shared] advertisingIdentifier];
}

+ (void)setMixpanelUserProperties:(NSDictionary * _Nonnull) properties {
    MixPanelOBJC *mixpanel = [MixPanelOBJC new];
    for(NSString *key in [properties allKeys]) {
        NSString *value = [properties objectForKey:key];
        [mixpanel setWithPeople:@{key: value}];
    }
}

@end
