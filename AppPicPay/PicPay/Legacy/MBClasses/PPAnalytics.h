#import <Foundation/Foundation.h>

@interface PPAnalytics : NSObject

//Mixpanel
+ (void)trackEvent:(NSString * _Nullable)eventName withLabel:(NSString * _Nullable)label andValue:(NSNumber * _Nullable)value properties:(NSDictionary * _Nullable)properties;
+ (void)trackEvent:(NSString *_Nullable)eventName properties:(NSDictionary * _Nullable)properties;
+ (void)trackEventWithCustomProperties:(NSString * _Nullable)eventName properties:(NSDictionary * _Nullable)properties;
+ (void)trackFirstTimeOnlyEvent:(NSString * _Nullable)eventName properties:(NSDictionary * _Nullable)properties;
+ (void)trackEvent:(NSString * _Nullable)eventName properties:(NSDictionary * _Nullable)properties includeMixpanel:(BOOL) includeMixpanel;
+ (void)trackFirstTimeOnlyEvent:(NSString * _Nullable)eventName properties:(NSDictionary * _Nullable)properties includeMixpanel:(BOOL) includeMixpanel;
+ (void)setMixpanelUserProperties:(NSDictionary * _Nonnull) properties;
+ (void)timeEvent:(NSString * _Nullable)eventName;

+ (NSString * _Nullable)distinctId;
+ (NSString * _Nullable)appsFlyerId;
+ (NSString * _Nullable)idfa;
+ (NSString * _Nullable)screenNameFromViewController:(id _Nonnull)viewController withVariation:(NSString * _Nullable)variation;

@end
