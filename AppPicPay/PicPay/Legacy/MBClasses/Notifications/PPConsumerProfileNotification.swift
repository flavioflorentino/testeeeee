//
//  PPConsumerProfileNotification.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 30/12/16.
//
//

import UIKit

@objc enum FollowerStatus: Int {
    case notFollowing = 0
    case following = 1
    case waitingAnswer = 2
    case deleted = 3
    case canceled = 4
    case loading = 99
    case undefined = 1000
}

class PPConsumerProfileNotification: PPNotification {
    
    @objc var consumerStatusFollower: FollowerStatus
    @objc var followerStatusConsumer: FollowerStatus
    @objc var profile: PPContact?
    
    override init(dictionaty jsonDict:[AnyHashable: Any]?, viaPush:Bool) {
        
        // check the status of follower request
        var consumerStatusFollower:FollowerStatus = .notFollowing
        var followerStatusConsumer:FollowerStatus = .notFollowing
        
        if let data = jsonDict?["data"] as? [AnyHashable: Any]{
            if let s = data["consumer_status_follower"] as? String {
                if let followerStatus = FollowerStatus(rawValue: Int(s)!){
                    consumerStatusFollower = followerStatus
                }
            }
            if let s = data["follower_status_consumer"] as? String {
                if let followerStatus = FollowerStatus(rawValue: Int(s)!){
                    followerStatusConsumer = followerStatus
                }
            }
            if let profileData = data["profile"] as? [AnyHashable: Any]{
                self.profile = PPContact(profileDictionary: profileData)
            }
        }
        
        self.consumerStatusFollower = consumerStatusFollower
        self.followerStatusConsumer = followerStatusConsumer
        
        super.init(dictionaty:jsonDict, viaPush:viaPush)!
    }
}
