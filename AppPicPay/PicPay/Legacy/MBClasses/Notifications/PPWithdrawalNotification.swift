//
//  WithdrawalNotification.swift
//  PicPay
//
//  Created by Marcos Timm on 05/05/17.
//
//

import UIKit

final class PPWithdrawalNotification: PPNotification {

    override init?(dictionaty jsonDict:[AnyHashable: Any]?, viaPush:Bool) {

        super.init(dictionaty:jsonDict, viaPush:viaPush)
    }
}
