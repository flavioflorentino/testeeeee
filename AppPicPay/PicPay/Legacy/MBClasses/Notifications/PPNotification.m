//
//  PPNotification.m
//  PicPay
//
//  Created by Diogo Carneiro on 03/12/13.
//
//

#import "PPNotification.h"
#import "PicPay-Swift.h"

@import CoreLegacy;
@import Billet;
@import FeatureFlag;

@implementation PPNotification

- (id) initWithDictionaty:(NSDictionary *)userInfo viaPush:(BOOL)viaPush{
    self = [super init];
    if (self != nil) {
        
		NSDictionary *rowJson = userInfo;
		
		NSString *currentVersionOfApp = [[AppParameters globalParameters] appVersion];
		
		self.version = [rowJson objectForKey:@"v"];
		self.wsId = [rowJson objectForKey:@"i"];

		if (!viaPush) {
			self.title = [rowJson objectForKey:@"title"];
            if ([[rowJson objectForKey:@"message"] isKindOfClass:[NSString class]]) {
                self.message = [NSString stringWithFormat:@"%@",[rowJson objectForKey:@"message"]];
                self.message = self.message;
            }
			self.creationDate = [[rowJson objectForKey:@"metadata"] objectForKey:@"created"];
			self.read = [[[rowJson objectForKey:@"metadata"] objectForKey:@"read"] boolValue];
			self.imgUrl = [rowJson objectForKey:@"img_url"];
            self.message = [self replaceToBoldTagsIn:self.message];
            
            @try {
                if ([[rowJson objectForKey:@"data"] isKindOfClass:[NSDictionary class]]) {
                    self.data = [rowJson objectForKey:@"data"];
                }
            }
            @catch (NSException *exception) {
                self.data = nil;
            }
            
		}else{
			@try {
                
                // Title
                if ([[rowJson objectForKey:@"title"] isKindOfClass:[NSString class]]){
                    self.title = [rowJson objectForKey:@"title"];
                }
                
                // Alert / Body
				if ([[[rowJson objectForKey:@"aps"] objectForKey:@"alert"] isKindOfClass:[NSString class]]) {
					self.alert = [[rowJson objectForKey:@"aps"] objectForKey:@"alert"];
                    
                    if ([[[rowJson objectForKey:@"aps"] objectForKey:@"title"] isKindOfClass:[NSString class]]){
                        self.title = [[rowJson objectForKey:@"aps"] objectForKey:@"title"];
                    }
                    
				} else if ([[[rowJson objectForKey:@"aps"] objectForKey:@"alert"] isKindOfClass:[NSDictionary class]]){
					self.alert = [[[rowJson objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"body"];
                    
                    if ([[[[rowJson objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"title"] isKindOfClass:[NSString class]]){
                        self.title = [[[rowJson objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"title"];
                    }
				} else {
					self.alert = @"";
				}
                self.alert = [self replaceToBoldTagsIn:self.alert];
			}
			@catch (NSException *exception) {
				self.alert = @"";
			}
		}
		
		//actions
		int i = 0;
		for (NSString *value in [rowJson objectForKey:@"l"]) {
			if (i == 0) {
				self.actionType = [NSString stringWithFormat:@"%@", value];
			}else if (i == 1){
				if ([self.actionType isEqualToString:@"3"]) {
					[self setLandingUrl:value];
				}else{
					[self setLandingScreenName:value];
				}
			}else{
				//add parameters at landingScreenParameters Array until its done cuz the count can be anything
				[self addLandingPageParameter:value];
				
				
				@try {
					if (i == 2 && [landingScreen isEqualToString:@"filtered_places"]) {
						[self setLandingPlacesFilter:value];
					}else if(i == 2 && [landingScreen isEqualToString:@"p2p_comments"]){
						[self setChatTransactionId:value];
                    }else if(i == 2 && [landingScreen isEqualToString:@"search_text"]){
                        [self setSearchText:value];
                    }else if(i == 3 && [landingScreen isEqualToString:@"search_text"]){
                        [self setSearchPage:value];
                    }
                    
                    if(i == 2){
                        [self setParam1:[NSString stringWithFormat:@"%@",value]];
                    }else if(i == 3){
                        [self setParam2:[NSString stringWithFormat:@"%@",value]];
                    }
				}
				@catch (NSException *exception) {
					
				}
			}
			i++;
		}
		
		if ([self.version compare:currentVersionOfApp options:NSNumericSearch] == NSOrderedDescending) {
			self.unsuportedVersion = YES;
		}
    }
    return self;
}

- (NSString *)replaceToBoldTagsIn:(NSString *)text {
    text = [text stringByReplacingOccurrencesOfString:@"[b]" withString:@"<b>"];
    text = [text stringByReplacingOccurrencesOfString:@"[/b]" withString:@"</b>"];
    return text;
}

- (void)setLandingScreenName:(NSString *)screenName{
    if ([screenName isKindOfClass:[NSString class]] && ![screenName isEqual:[NSNull null]]){
        landingScreen = screenName;
        self.landingScreenStringName = screenName;
    }
}

- (id)landingScreen {
	if ([_actionType isEqualToString:@"3"]) {
        BOOL sendHeaders = NO;
        if ([landingScreenParameters count] > 0) {
            sendHeaders = [[landingScreenParameters objectAtIndex:0] integerValue] == 1 ? YES : NO;
        }
        
        // prevent encoding url.
        NSString *removedEncoding = [self.landingUrl stringByRemovingPercentEncoding];
        NSString *encodedUrlString = [removedEncoding stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *url = [[NSURL alloc] initWithString:encodedUrlString];
        
        if (url != nil) {
           return [WebViewFactory makeWith: url
                            includeHeaders: sendHeaders
                             customHeaders: nil
                           shouldObfuscate: false
                                completion: nil];
        }
        return nil;
	}else if ([landingScreen isEqualToString:@"receipt_p2p"]) {
        ReceiptWidgetViewModel *receiptModel = [[ReceiptWidgetViewModel alloc]
                                                initWithTransactionId:[NSString stringWithFormat:@"%@", [landingScreenParameters objectAtIndex:0]]
                                                type:ReceiptTypeP2P];
        
        ReceiptWidgetViewController *nextScreen = [[ReceiptWidgetViewController alloc] initWithViewModel:receiptModel];
		
		return nextScreen;
	}else if ([landingScreen isEqualToString:@"receipt_pav"]){
        ReceiptWidgetViewModel *receiptModel = [[ReceiptWidgetViewModel alloc]
                                                initWithTransactionId:[NSString stringWithFormat:@"%@", [landingScreenParameters objectAtIndex:0]]
                                                type:ReceiptTypePAV];
        
        ReceiptWidgetViewController *nextScreen = [[ReceiptWidgetViewController alloc] initWithViewModel:receiptModel];
		
		return nextScreen;
    }else if ([landingScreen isEqualToString:@"card_debit_receipt"]){
        ReceiptWidgetViewModel *receiptModel = [[ReceiptWidgetViewModel alloc]
                                                initWithTransactionId:[NSString stringWithFormat:@"%@", [landingScreenParameters objectAtIndex:0]]
                                                type:ReceiptTypeDebitReceipt];

        ReceiptWidgetViewController *nextScreen = [[ReceiptWidgetViewController alloc] initWithViewModel:receiptModel];

        return nextScreen;
	}else if ([landingScreen isEqualToString:@"p2p_comments"]) {
        @try {
            if (landingScreenParameters.count > 1) {
                NSString *externalId = [NSString stringWithFormat:@"%@", [landingScreenParameters objectAtIndex:1]];
                
                if (landingScreenParameters.count == 4) {
                    NSString *transactionId = [NSString stringWithFormat:@"%@", [landingScreenParameters objectAtIndex:2]];
                    NSString *notificationType = [NSString stringWithFormat:@"%@", [landingScreenParameters objectAtIndex:3]];
                
                    if ([self canOpenBilletFeedDetailWith:notificationType]) {
                        UIViewController *nextScreen = [BilletFeedDetailFactory makeWithTransactionId:transactionId externalId:externalId];
                        return nextScreen;
                    }
                }
                
                FeedDetailViewModel *model = [[FeedDetailViewModel alloc] initWithFeedItemId:externalId];
                UIViewController *nextScreen = [FeedDetailViewControllerObjc createFeedDetailViewController:model];
                return nextScreen;
            }
        } @catch (NSException *exception) {
            return nil;
        }
	}else if ([landingScreen isEqualToString:@"filtered_places"]){
		return landingScreen;
	}else if([landingScreen isEqualToString:@"phone_verification"]){
        return [ChangePhoneNumberFactoryObjc make];
	}else if([landingScreen isEqualToString:@"withdrawal"]){
		return @"withdrawal";
    }else if([landingScreen isEqualToString:@"services_tab"]){
        [self setSearchText:@""];
        [self setSearchPage:@"store"];
        return @"search_text";
    }else if([landingScreen isEqualToString:@"services_by_id"]){
        if ([_param1 isEqualToString:@"boleto"]) {
            return @"boleto";
        }
        return @"services_by_id";
    }else if([landingScreen isEqualToString:@"birthday_gift"]){
        return @"birthday_gift";
    }else if([landingScreen isEqualToString:@"new_user_gift"]){
        return @"new_user_gift";
    }else if([landingScreen isEqualToString:@"bank_account"]){
        return @"";
	}else if([landingScreen isEqualToString:@"mgm"]){
		if ([[[AppParameters globalParameters] mgmConfigs] mgmEnabled]) {
			return @"mgm";
		}
	}else if([landingScreen isEqualToString:@"share_link"]){
		if ([[[AppParameters globalParameters] mgmConfigs] mgmEnabled]) {
			return @"share_link";
		}
    }else if([landingScreen isEqualToString:@"text"]){
        if ([[[AppParameters globalParameters] mgmConfigs] mgmEnabled]) {
            return @"text";
        }
	}else if([landingScreen isEqualToString:@"personal_data"]){
        UIViewController *controller = [PersonalDetailsFactory make];
        if (controller) {
             return controller;
        }
	}else if([landingScreen isEqualToString:@"credit_cards"]){
		// Essa parte foi retirado pois dava erro no Aplicativo
        //return [ViewsManager mainStoryboardViewControllerWithIdentifier:@"PaymentMethodSelection"];
	}else if([landingScreen isEqualToString:@"change_password"]){
		return [[ChangePasswordViewController alloc] init];
	}else if([landingScreen isEqualToString:@"anti_fraud_checklist"]){
        return [[AntiFraudChecklistViewController alloc] init];
	}else if([landingScreen isEqualToString:@"profile"]){
		return @"profile";
	}else if([landingScreen isEqualToString:@"recharge_canceled"]){
		return @"wallet";
	}else if([landingScreen isEqualToString:@"recharge_complete"]){
		return @"wallet";
	}else if([landingScreen isEqualToString:@"wallet"]){
		return @"wallet";
    }else if ([landingScreen isEqualToString:@"search_text"]){
        return landingScreen;
    }/*else if ([landingScreen isEqualToString:@"zona_azul_use"]){
        
        PaymentZonaAzulViewController *nextScreen = (PaymentZonaAzulViewController*) [ViewsManager paymentZonaAzulViewControllerWithIdentifier:@"PaymentViewController"];
        PPStore *store = [[PPStore alloc] init];
        
        if (self.param1 != nil && self.param2 != nil ){
            store.sellerId = self.param1;
            store.wsId = [self.param2 integerValue];
            nextScreen.store = store;
            return nextScreen;
        }else{
            return nil;
        }
        //return nil;
        
    }*/else if([landingScreen isEqualToString:@"consumer_profile"]){
        return @"consumer_profile";
    }else if([landingScreen isEqualToString:@"fixed_value_transaction"]){
        return landingScreen;
    }else if([landingScreen isEqualToString:@"contact_us"]){
        return @"contact_us";
    }else if([landingScreen isEqualToString:@"search_people"]){
        return @"search_people";
    }else if([landingScreen isEqualToString:@"boleto"]){
        return @"boleto";
    }else if([landingScreen isEqualToString:@"share_invite_code"]){
        return @"share_invite_code";
    }else if([landingScreen isEqualToString:@"mgb"]){
        if ([[[AppParameters globalParameters] mgmConfigs] mgmEnabled]) {
            return @"mgb";
        }
    }else if([landingScreen isEqualToString:@"change_email"]){
        return @"change_email";
    }else if([landingScreen isEqualToString:@"checkout"]){
        return @"checkout";
    }else if([landingScreen isEqualToString:@"subscription_details"]){
        return @"subscription_details";
    }else if([landingScreen isEqualToString:@"follow_accept"]){
        return @"follow_accept";
    }else if([landingScreen isEqualToString:@"open_profile"]){
        return @"open_profile";
    }else if([landingScreen isEqualToString:@"deeplink"]){
        return @"deeplink";
    }
    
    else if([landingScreen isEqualToString:@"credit_available_screen"]){
        return @"credit_available_screen";
    }else if([landingScreen isEqualToString:@"credit_review_requested_screen"]){
        return @"credit_review_requested_screen";
    }else if([landingScreen isEqualToString:@"credit_registration_approved_screen"]){
        return @"credit_registration_approved_screen";
    }else if([landingScreen isEqualToString:@"credit_registration_approved"]){
        return @"credit_registration_approved";
    }else if([landingScreen isEqualToString:@"credit_limit_update_screen"]){
        return @"credit_limit_update_screen";
    }else if([landingScreen isEqualToString:@"credit_invoice_closed_screen"]){
        return @"credit_invoice_closed_screen";
    }else if([landingScreen isEqualToString:@"identity_analysis"]){
        return @"identity_analysis";
    }else if([landingScreen isEqualToString:@"2fa_login_notification"]){
        return @"2fa_login_notification";
    }
    
	return @"";
}

- (void)addLandingPageParameter:(NSString *)newParameter{
	if(landingScreenParameters == nil){
		landingScreenParameters = [[NSMutableArray alloc] init];
	}
	[landingScreenParameters addObject:newParameter];
}

- (void)markAsRead{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
		self.read = YES;
		[WSConsumer readNotificationWithId:self.wsId completionHandler:^(NSError *error) {
            
        }];
	});
}

- (BOOL)canOpenBilletFeedDetailWith:(NSString*)notificationType {
    return notificationType != nil && [notificationType isEqual: @"bills"];
}

@end
