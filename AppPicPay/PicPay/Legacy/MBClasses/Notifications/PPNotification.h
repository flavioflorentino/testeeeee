
//
//  PPNotification.h
//  PicPay
//
//  Created by Diogo Carneiro on 03/12/13.
//
//

#import <Foundation/Foundation.h>

@interface PPNotification : NSObject {
	NSString *landingScreen; //Some ViewController that will be opening whem touching the notification
	NSMutableArray *landingScreenParameters;
}

@property (strong, nonatomic) NSString * _Nullable title;
@property (strong, nonatomic) NSString * _Nullable message;
@property (strong, nonatomic) NSString * _Nullable alert;
@property (strong, nonatomic) NSString * _Nullable creationDate;
@property BOOL read;
@property BOOL unsuportedVersion;
@property (strong, nonatomic) NSString * _Nullable version;
@property (strong, nonatomic) NSString * _Nullable imgUrl; //not ready yet

@property (strong, nonatomic) NSString * _Nullable chatTransactionId; //to prevent push on chat

/*
 * Notification ID
 */
@property (strong, nonatomic) NSString * _Nullable wsId;

@property (strong, nonatomic) NSObject * _Nullable data;

/*
 * actionType:
 *	 0: aplicação deverá requisitar ao webservice o conteúdo completo da notificaçao, passando o wsId da notificação no parametro 'notification_id' para o método getNotifications.json
 *	 1: ao clicar na notificação, deverá abrir a tela padrão (mesmo comportamente de todos os pushs atualmente)
 *	 2: ao clicar na notificação, deverá abrir a tela cujo nome será especificado estiver especificado em landingScreen.
 *	 3: ao clicar na notificação, deverá abrir uma webview que carregue a url que estiver especificada em landingUrl
 */
@property (strong, nonatomic) NSString * _Nullable actionType;

@property (strong, nonatomic) NSString * _Nullable landingUrl;
@property (strong, nonatomic) NSString * _Nullable landingPlacesFilter;
@property (strong, nonatomic) NSString * _Nullable searchText;
@property (strong, nonatomic) NSString * _Nullable searchPage;

@property (strong, nonatomic) NSString * _Nullable param1;
@property (strong, nonatomic) NSString * _Nullable param2;
@property (strong, nonatomic, nullable) NSString  *landingScreenStringName ;

- (id _Nullable ) initWithDictionaty:(NSDictionary *_Nullable)userInfo viaPush:(BOOL)viaPush;

- (void)setLandingScreenName:(NSString *_Nullable)screenName;
- (void)addLandingPageParameter:(NSString *_Nullable)newParameter;

- (id _Nullable )landingScreen;

- (void)markAsRead;

@end


/*
 
 //VERSION COMPARE
 
 NSString* requiredVersion = @"1.2.0";
 NSString* actualVersion = @"1.1.5";
 
 if ([requiredVersion compare:actualVersion options:NSNumericSearch] == NSOrderedDescending) {
 // actualVersion is lower than the requiredVersion
 }
 */
