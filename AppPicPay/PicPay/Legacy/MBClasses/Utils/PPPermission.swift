import PermissionsKit
import UIKit
import UserNotifications

@objc
final class PPPermission: NSObject {
    struct Alert {
        var title: String?
        var message: String?
        var cancel: String?
        var settings: String?
        
        init(title: String? = nil, message: String? = nil, cancel: String? = nil, settings: String? = nil) {
            self.title = title
            self.message = message
            self.cancel = cancel
            self.settings = settings
        }
    }
    
    @objc
    internal static func requestNotifications(){
        guard !ProcessInfo.processInfo.environment.keys.contains("XCTestConfigurationFilePath") else {
            return
        }
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (_, _) in }
    }
    
    @objc
    internal static func contactsOBJC(_ callback: @escaping ((_ authorized: Bool) -> Void)) {
        if contacts().status == PermissionStatus.authorized {
            callback(true)
        } else {
            PPPermission.requestContacts { (status) in
                callback((status == PermissionStatus.authorized))
            }
        }
    }
    
    internal static func contacts(withDeniedAlert deniedAlert: Alert? = nil) -> Permission {
        let permission: Permission = .contacts
        
        let alert = permission.deniedAlert
        alert.title = deniedAlert?.title ?? "Permissão de agenda necessária"
        alert.message = deniedAlert?.message ?? "Para identificar seus contatos que usam PicPay é necessário acessar sua agenda. Clique no botão Configurações e ative a opção Contatos."
        alert.cancel = deniedAlert?.cancel ?? "Cancelar"
        alert.settings = deniedAlert?.settings ?? "Configurações"
        
        return permission
    }
    
    internal static func camera(withDeniedAlert deniedAlert: Alert? = nil) -> Permission {
        let permission: Permission = .camera
        
        let alert = permission.deniedAlert
        alert.title = deniedAlert?.title ?? "Permissão de câmera necessária"
        alert.message = deniedAlert?.message ?? "O PicPay utiliza a câmera do aparelho para fotografar os documentos solicitados."
        alert.cancel = deniedAlert?.cancel ?? "Cancelar"
        alert.settings = deniedAlert?.settings ?? "Configurações"
        
        return permission
    }
    
    internal static func requestContacts(_ callback: @escaping Permission.Callback) {
        contacts().request { (status) in
            if status == PermissionStatus.authorized{
                PPPermission.trackEvent(permissionType: .contacts)
                PPAnalytics.trackFirstTimeOnlyEvent("Respondeu requisição de contatos do iOS", properties: ["Autorização": "true"])
            } else {
                PPAnalytics.trackFirstTimeOnlyEvent("Respondeu requisição de contatos do iOS", properties: ["Autorização": "false"])
            }
            callback(status)
        }
    }

    public static func request(permission: Permission, _ callback: @escaping Permission.Callback) {
        permission.request { (status) in
            if status == PermissionStatus.authorized{
                PPPermission.trackEvent(permissionType: permission.type)
            }
            callback(status)
        }
    }
    
    internal static func location(withDeniedAlert deniedAlert: Alert? = nil, withDisabledAlert disabledAlert: Alert? = nil) -> Permission {
        let permission: Permission = .locationWhenInUse
        
        let alert = permission.deniedAlert
        alert.title = deniedAlert?.title ?? "Permissão de Localização necessária"
        alert.message = deniedAlert?.message ?? "Para encontrar lugares próximos de você é necessário habilitar a localização. Clique no botão Configurações e ative a opção Localização."
        alert.cancel = deniedAlert?.cancel ?? "Cancelar"
        alert.settings = deniedAlert?.settings ?? "Configurações"
        
        let alertDisabled = permission.disabledAlert
        alertDisabled.title = disabledAlert?.title ?? "A localização está desabilitada"
        alertDisabled.message = disabledAlert?.message ?? "Para encontrar lugares próximos de você é necessário habilitar a localização."
        alertDisabled.cancel = disabledAlert?.cancel ?? "Cancelar"
        alertDisabled.settings = disabledAlert?.settings ?? "Configurações"
        
        return permission
    }
    
    internal static func requestLocation(_ callback: @escaping Permission.Callback) {
        location().request { (status) in
            if status == PermissionStatus.authorized {
                PPPermission.trackEvent(permissionType: .locationWhenInUse)
            }
            callback(status)
        }
    }
    
    internal static func requestCamera(_ callback: @escaping Permission.Callback) {
        camera().request { (status) in
            if status == PermissionStatus.authorized {
                PPPermission.trackEvent(permissionType: .camera)
            }
            callback(status)
        }
    }
    
    fileprivate static func trackEvent(permissionType: PermissionType) {
        if permissionType == .contacts {
            PPAnalytics.trackFirstTimeOnlyEvent("Permissao autorizada - Contatos", properties: nil)
        }
        if permissionType == .camera {
            PPAnalytics.trackFirstTimeOnlyEvent("Permissao autorizada - Camera", properties: nil)
        }
        if permissionType == .locationWhenInUse || permissionType == .locationAlways {
            PPAnalytics.trackFirstTimeOnlyEvent("Permissao autorizada - Localizacao", properties: nil)
        }
    }
}
