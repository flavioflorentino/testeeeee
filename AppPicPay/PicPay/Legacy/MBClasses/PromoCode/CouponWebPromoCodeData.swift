import SwiftyJSON

struct CouponWebPromoCodeData: BaseApiResponse {
    let campaignType: String
    let url: String
    let code: String
    
    init?(json: JSON) {
        guard let campaignType = json["coupon_info"]["campaign_type"].string,
            let code = json["coupon_info"]["code"].string,
            let webview = json["webview"].string else {
                return nil
        }
        
        self.campaignType = campaignType
        self.url = webview
        self.code = code
    }
}
