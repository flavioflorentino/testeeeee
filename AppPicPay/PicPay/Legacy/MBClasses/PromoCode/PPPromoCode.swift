import SwiftyJSON

final class PPPromoCode: BaseApiResponse {
    enum Reward: String {
        case inviterReward = "default"
        case couponWeb = "coupon-webview"
        case couponCustom = "coupon-custom"
    }
    
    enum PromoCodeData {
        case inviterReward(PPRegisterRewardPromoCode)
        case couponWeb(CouponWebPromoCodeData)
        case studentAccount(String)
        case unknown
    }
    
    var rewardCategory: Reward = .couponCustom
    var data: PromoCodeData = .unknown
    
    required init?(json: JSON) {
        if let category = json["data"]["reward_category"].string,
            let rewardCategory = Reward(rawValue: category) {
            self.rewardCategory = rewardCategory
        }
        
        switch rewardCategory {
        case .inviterReward:
            guard let dataDict = json["data"].dictionaryObject else {
                return nil
            }
            self.data = .inviterReward(PPRegisterRewardPromoCode(dictionary: dataDict))
        case .couponWeb:
            guard let couponWeb = CouponWebPromoCodeData(json: json["data"])  else {
                return nil
            }
            self.data = .couponWeb(couponWeb)
        case .couponCustom:
            guard let type = json["data"]["app_screen_path"].string, type == "student-account" else { return nil }
            let code = json["data"]["coupon_info"]["code"].string ?? "CUPOM UTILIZADO"
            self.data = .studentAccount(code)
        }
    }
}
