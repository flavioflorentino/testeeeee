import Foundation
import FeatureFlag

@objc
protocol InstallmentsSelectorViewModelDelegate: AnyObject {
    func didSelectedInstallment(_ paymentManager: PPPaymentManager)
}

final class InstallmentsSelectorViewModel: NSObject {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies = DependencyContainer()
    
    private let paymentManager: PPPaymentManager
    private let payeeId: Int?
    private let sellerId: String?
    private let origin: String?
    private let titleHeader: String?
    private let descriptionHeader: String?
    private let worker: InstallmentsSelectorWorkerProtocol
    
    private var installments = [(installmentNumber: Int, installmentValue: Decimal)]()
    
    @objc weak var delegate: InstallmentsSelectorViewModelDelegate?
    
    var selectedQuota: Int {
        installments[paymentManager.selectedQuotaQuantity - 1].installmentNumber
    }
    
    @objc
    init(paymentManager: PPPaymentManager,
         payeeId: NSNumber? = nil,
         sellerId: String? = nil,
         origin: String? = nil,
         titleHeader: String? = nil,
         descriptionHeader: String? = nil,
         worker: InstallmentsSelectorWorkerProtocol? = nil) {
        self.paymentManager = paymentManager
        self.payeeId = payeeId?.intValue
        self.sellerId = sellerId
        self.origin = origin
        self.titleHeader = titleHeader
        self.descriptionHeader = descriptionHeader
        self.worker = worker ?? InstallmentsSelectorWorker()
        super.init()
    }
    
    func sendEventOnAppear() {
        let times = InstallmentsTooltip.timesTooltipDisplay()
        PPAnalytics.trackFirstTimeOnlyEvent("Installments screen accessed FT", properties: ["Times seen installments hint": times])
    }
    
    func numberOfSections() -> Int {
        return installments.isEmpty ? 0 : 1
    }
    
    func numberOfRows(in section: Int) -> Int {
        return installments.count
    }
    
    func textForCell(in indexPath: IndexPath) -> String? {
        guard installments.indices.contains(indexPath.row) else {
            return nil
        }
        let installmentValue = NSDecimalNumber(decimal: installments[indexPath.row].installmentValue)
        let value = installmentValue.decimalValue.toCurrencyString()
        let installmentCount = NSDecimalNumber(value: installments[indexPath.row].installmentNumber)
        let bahavior = NSDecimalNumberHandler(
            roundingMode: .plain,
            scale: 8,
            raiseOnExactness: false,
            raiseOnOverflow: false,
            raiseOnUnderflow: false,
            raiseOnDivideByZero: false
        )
        let interestToCompare = installmentValue.multiplying(by: installmentCount, withBehavior: bahavior)
        let interest = paymentManager.cardTotalWithoutInterest()?.rounding(accordingToBehavior: bahavior) ?? 0
        let rate = interest.compare(interestToCompare) == .orderedSame ? DefaultLocalizable.interestFree.text : ""
        return String(format: DefaultLocalizable.insterestRateInstallment.text, installmentCount.intValue, value ?? "", rate)
    }
    
    func cellIsChecked(at indexPath: IndexPath) -> Bool {
        return indexPath.row == selectedQuota - 1
    }
    
    func headerViewTitle(for section: Int) -> String? {
        if let title = titleHeader {
            return title
        }
        if origin == "p2p" {
            return DefaultLocalizable.titleInstallmentsSelector.text
        }
        return nil
    }
    
    func footerTitle(in section: Int) -> String {
        return FeatureManager.text(.textInstallmentNoInterestFooter)
    }
    
    func headerHeight(for section: Int) -> CGFloat {
        return headerViewTitle(for: section) == nil && headerViewDescription(for: section) == nil ? CGFloat.leastNonzeroMagnitude : UITableView.automaticDimension
    }
    
    func headerViewDescription(for section: Int) -> String? {
        if let description = descriptionHeader {
            return description
        }
        if let balance = paymentManager.balance(), !balance.isEqual(to: NSDecimalNumber.zero), paymentManager.usePicPayBalance() {
            return DefaultLocalizable.descriptionInstallmentsSelector.text
        }
        return nil
    }
    
    func loadData(onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        if let payeeId = self.payeeId, payeeId != 0 {
            loadBusinessAccount(with: "\(payeeId)", onSuccess: onSuccess, onError: onError)
        } else if let sellerId = self.sellerId {
            loadSellerAccount(with: sellerId, onSuccess: onSuccess, onError: onError)
        } else {
            updatePaymentConfig()
            onSuccess()
        }
    }
    
    func didSelectedRow(at indexPath: IndexPath) {
        let installmentRow = installments[indexPath.row].installmentNumber
        paymentManager.selectedQuotaQuantity = installmentRow
        delegate?.didSelectedInstallment(paymentManager)
    }
}

extension InstallmentsSelectorViewModel {
    private func loadBusinessAccount(with payeeId: String, onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        worker.loadBusinnesAccount(with: payeeId) { [weak self] (installments, error) in
            if let error = error {
                onError(error)
            } else {
                self?.updatePaymentConfig(with: installments)
                onSuccess()
            }
        }
    }
    
    private func loadSellerAccount(with sellertId: String, onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        worker.loadSellerAccount(with: sellertId) { [weak self] (installments, error) in
            if let error = error {
                onError(error)
            } else {
                self?.updatePaymentConfig(with: installments)
                onSuccess()
            }
        }
    }
    
    private func updatePaymentConfig(with installmentsAny: [Any]? = nil) {
        guard let config = self.paymentManager.config() else {
            return
        }
        config.updateInstallmentsBusiness(installmentsAny)
        paymentManager.change(config)
        guard let installments = paymentManager.installmentsList() as? [Decimal] else {
            return
        }
        
        if dependencies.featureManager.isActive(.experimentInvertedInstallment) {
            for (i, value) in installments.sorted(by: { $1 > $0}).enumerated() {
                self.installments.append((installmentNumber: installments.count - i, installmentValue: value))
            }
        } else {
            for (i, value) in installments.enumerated() {
                self.installments.append((installmentNumber: i + 1, installmentValue: value))
            }
        }
    }
}
