import UI
import UIKit

final class InstallmentSelectorHeaderView: UITableViewHeaderFooterView {
    static let reuseIdentifier = String(describing: self)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = Palette.ppColorGrayscale500.color
        label.setContentHuggingPriority(UILayoutPriority(1000), for: .vertical)
        return label
    }()
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.numberOfLines = 0
        label.textColor = Palette.ppColorGrayscale400.color
        label.setContentHuggingPriority(UILayoutPriority(250), for: .vertical)
        return label
    }()
    private lazy var mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 8
        stackView.distribution = .fillProportionally
        return stackView
    }()
    
    var titleText: String? {
        didSet {
            guard let titleText = self.titleText else {
                titleLabel.isHidden = true
                return
            }
            titleLabel.isHidden = false
            titleLabel.text = titleText
        }
    }
    
    var descriptionText: String? {
        didSet {
            guard let descriptionText = self.descriptionText else {
                descriptionLabel.isHidden = true
                return
            }
            descriptionLabel.isHidden = false
            descriptionLabel.text = descriptionText
        }
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = Palette.ppColorGrayscale000.color
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        mainStackView.addArrangedSubview(titleLabel)
        mainStackView.addArrangedSubview(descriptionLabel)
        contentView.addSubview(mainStackView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            mainStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            mainStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            mainStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16),
            mainStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16)])
    }
}
