import Foundation

@objc
protocol InstallmentsSelectorWorkerProtocol {
    func loadBusinnesAccount(with payeedId: String, _ completion: @escaping ([Any]?, Error?) -> Void)
    func loadSellerAccount(with sellertId: String, _ completion: @escaping ([Any]?, Error?) -> Void)
}

final class InstallmentsSelectorWorker: InstallmentsSelectorWorkerProtocol {
    func loadBusinnesAccount(with payeedId: String, _ completion: @escaping ([Any]?, Error?) -> Void) {
        WSParcelamento.getInstallmentsBusinessAccount(payeedId) { (installments, error) in
            completion(installments, error)
        }
    }
    func loadSellerAccount(with sellertId: String, _ completion: @escaping ([Any]?, Error?) -> Void) {
        WSParcelamento.getInstallmentsSellerAccount(sellertId) { (installments, error) in
            completion(installments, error)
        }
    }
}
