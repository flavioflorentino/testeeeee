import UI
import UIKit

final class InstallmentsSelectorViewController: PPBaseViewController {
    private let cellIdentifier = "tableCellIdentifier"
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 44
        tableView.estimatedSectionHeaderHeight = 44
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(InstallmentSelectorHeaderView.self,
                           forHeaderFooterViewReuseIdentifier: InstallmentSelectorHeaderView.reuseIdentifier)
        tableView.backgroundColor = Palette.ppColorGrayscale100.color
        return tableView
    }()
    private let viewModel: InstallmentsSelectorViewModel
    var statusbarStyle: UIStatusBarStyle = .default
    var hasCloseButton: Bool = false
    
    override var shouldAddBreadcrumb: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusbarStyle
    }
    
    @objc
    init(with viewModel: InstallmentsSelectorViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        title = DefaultLocalizable.installment.text
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
        layoutComponents()
        loadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.sendEventOnAppear()
    }
    
    private func addComponents() {
        view.addSubview(tableView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor)])
    }
    
    private func loadData() {
        startLoadingView()
        viewModel.loadData(onSuccess: { [weak self] in
            DispatchQueue.main.async {
                self?.stopLoadingView()
                self?.tableView.reloadData()
            }
        }, onError: { [weak self] error in
            DispatchQueue.main.async {
                self?.stopLoadingView()
                AlertMessage.showCustomAlertWithError(error, controller: self) {
                    self?.navigationController?.popViewController(animated: true)
                }
            }
        })
    }
}

extension InstallmentsSelectorViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        if let reuseCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) {
            cell = reuseCell
        } else {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        }
        cell.tintColor = Palette.ppColorBranding300.color
        cell.accessoryType = viewModel.cellIsChecked(at: indexPath) ? .checkmark : .none
        cell.textLabel?.text = viewModel.textForCell(in: indexPath)
        cell.textLabel?.textColor = Palette.ppColorGrayscale500.color
        cell.textLabel?.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        cell.backgroundColor = Palette.ppColorGrayscale000.color
        return cell
    }
}

extension InstallmentsSelectorViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.cellForRow(at: IndexPath(row: viewModel.selectedQuota, section: indexPath.section))?.accessoryType = .none
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        viewModel.didSelectedRow(at: indexPath)
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: InstallmentSelectorHeaderView.reuseIdentifier) as? InstallmentSelectorHeaderView else {
            return nil
        }
        view.titleText = viewModel.headerViewTitle(for: section)
        view.descriptionText = viewModel.headerViewDescription(for: section)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return viewModel.headerHeight(for: section)
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return viewModel.footerTitle(in: section)
    }
}
