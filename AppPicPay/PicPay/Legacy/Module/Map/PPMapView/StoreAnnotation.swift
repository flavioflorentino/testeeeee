import Foundation
import UIKit
import MapKit

final class StoreAnnotation: MKPointAnnotation{
    var store: PPStore
    
    init(store: PPStore) {
        self.store = store
        super.init()
        coordinate = CLLocationCoordinate2D(latitude: store.latitude, longitude: store.longitude)
    }
}

final class StoreAnnotationView: MKAnnotationView{
    
    var storeImage:UIPPProfileImage?
    fileprivate let pinSize = CGSize(width: 44, height: 104)
    fileprivate var backgroundPin = UIImageView()
    fileprivate let halfSizeView = UIView()

    init(annotation: StoreAnnotation?, onTapDelegate: UIPPProfileImageDelegate? = nil) {
        guard let store = annotation?.store else {
            super.init(annotation: MKPointAnnotation(), reuseIdentifier: "")
            return
        }
        
        super.init(annotation: annotation, reuseIdentifier: StoreAnnotationView.reuseIdForStore(store: store))
        
        
        storeImage = UIPPProfileImage(frame: CGRect(x: 4, y: 4, width: pinSize.width - 8, height: pinSize.width - 8))
        storeImage?.delegate = onTapDelegate
        storeImage?.setStore(store)
        
        backgroundPin.frame = CGRect(x: 0, y: 0, width: pinSize.width, height: pinSize.height)
        backgroundPin.image = #imageLiteral(resourceName: "map-pin")
        
        self.frame = backgroundPin.frame

        addSubview(backgroundPin)
        if let image = storeImage {
            addSubview(image)
        }
        
        // Half of the "backgroundPin" image is transparent (so we don't have to calculate the offset). So we need to have a view half the annotation
        // frame to capture the touch in the "real" image
        var frame = backgroundPin.frame
        frame.size.height /= 2
        halfSizeView.frame = frame
        
        let touch = UITapGestureRecognizer(target: self, action: #selector(onTap))
        touch.cancelsTouchesInView = true
        halfSizeView.addGestureRecognizer(touch)
        
        addSubview(halfSizeView)
    }
    
    @objc func onTap() {
        storeImage?.didTap(self)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    static func reuseIdForStore(store: PPStore) -> String{
        return String(store.wsId)
    }
}
