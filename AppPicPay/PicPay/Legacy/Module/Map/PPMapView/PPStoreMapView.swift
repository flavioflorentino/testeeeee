import AnalyticsModule
import Core
import Foundation
import UIKit
import MapKit

@objc protocol PPStoreMapViewDelegate {
    @objc optional func didTap(store: PPStore)
    @objc optional func didChangeRegion(onMap map: MKMapView)
}

final class PPStoreMapView: MKMapView {
    
    fileprivate var didCenterToUserLocationOnce = false
    weak var ppMapDelegate:PPStoreMapViewDelegate?
    
    var updatedUserLocation = false
    var shouldFollowUser = true
    var zoom:Double = 500
    
    init(){
        super.init(frame: CGRect.zero)
        commonInit()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(frame: CGRect.zero)
        commonInit()
    }
    
    func commonInit(){
        self.showsUserLocation = true
        self.userTrackingMode = .follow
        self.delegate = self
    }
    
    func setupAnnotations(forData data: [LegacySearchResultRow]){
        if let userLoc = LocationManager.shared.authorizedLocation?.coordinate {
            centerToLocation(loc: userLoc, animated: false)
        }
        
        // Clear annotations
        let oldAnnotations = self.annotations
        
        for result in data {
            guard let store = result.fullData as? PPStore,
                store.isParkingPayment != IsOldParking else {
                continue
            }
    
            self.addAnnotation(StoreAnnotation(store: store))
        }
        
        self.removeAnnotations(oldAnnotations)
    }
    
    fileprivate func centerToLocation(loc: CLLocationCoordinate2D, animated: Bool = true) {
        let region = MKCoordinateRegion(center: loc, latitudinalMeters: zoom, longitudinalMeters: zoom)
        self.setRegion(region, animated: animated)
    }
}

extension PPStoreMapView: MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let storeAnnotation = annotation as? StoreAnnotation,
            !(annotation is MKUserLocation) else { // Do not mess with user's location annotation
            return nil
        }
        
        if let reusableView = mapView.dequeueReusableAnnotationView(withIdentifier: StoreAnnotationView.reuseIdForStore(store: storeAnnotation.store)) {
            return reusableView
        }
        
        return StoreAnnotationView(annotation: storeAnnotation, onTapDelegate: self)
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        updatedUserLocation = true
        if shouldFollowUser || !didCenterToUserLocationOnce{
            didCenterToUserLocationOnce = true
            centerToLocation(loc: userLocation.coordinate)
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        mapView.selectedAnnotations = []
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        self.ppMapDelegate?.didChangeRegion?(onMap: mapView)
    }
}

extension PPStoreMapView: UIPPProfileImageDelegate {
    func profileImageViewDidTap(_ profileImage: UIPPProfileImage, store: PPStore) {
        Analytics.shared.log(PlacesEvent.didTapPinOnMap(type: store.isCielo ? .cielo : .biz, storeName: store.name ?? ""))
        ppMapDelegate?.didTap?(store: store)
    }
}
