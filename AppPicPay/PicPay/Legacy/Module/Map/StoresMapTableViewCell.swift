import Foundation
import UI
import UIKit
import MapKit

final class StoresMapTableViewCell: UITableViewCell {
    
    fileprivate let map = PPStoreMapView()
    fileprivate let overlay = UIView()
    fileprivate let loading = UIActivityIndicatorView(style: .white)
    
    var isLoading: Bool = false {
        didSet {
            overlay.isHidden = !isLoading
        }
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        setupConstraints()
    }
    
    func setupViews(){
        map.isUserInteractionEnabled = false
        addSubview(map)
        
        overlay.backgroundColor = Palette.ppColorGrayscale600.color.withAlphaComponent(0.3)
        addSubview(overlay)
        
        loading.startAnimating()
        overlay.addSubview(loading)
    }
    
    func setupConstraints(){
        map.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        overlay.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        loading.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
    
    func configure(withData data: [LegacySearchResultRow]){
        map.setupAnnotations(forData: data)
        if !data.isEmpty {
            isLoading = false
        }
    }
}



