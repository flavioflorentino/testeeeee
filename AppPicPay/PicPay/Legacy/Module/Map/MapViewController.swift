import AnalyticsModule
import Foundation
import MapKit
import UI

final class StoresMapViewController: PPBaseViewController {
    fileprivate let map = PPStoreMapView()
    fileprivate let centerToUserIcon = UIButton()
    fileprivate var data: [LegacySearchResultRow]
    fileprivate let gradientHeight = 100

    private var establishmentDetailsCoordinator: EstablishmentDetailsCoordinating?
    
    init(with data: [LegacySearchResultRow]) {
        self.data = data
        
        super.init(nibName: nil, bundle: nil)
        
        setupViews()
        setupConstraints()
        
        Analytics.shared.log(PlacesEvent.didShowFullMap)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){
        
        map.translatesAutoresizingMaskIntoConstraints = false
        centerToUserIcon.translatesAutoresizingMaskIntoConstraints = false
        
        view.backgroundColor = Palette.ppColorGrayscale000.color
        
        map.setupAnnotations(forData: data)
        map.shouldFollowUser = false
        map.ppMapDelegate = self
        
        centerToUserIcon.setImage(#imageLiteral(resourceName: "map-icon"), for: .normal)
        centerToUserIcon.addTarget(self, action: #selector(centerToUser), for: .touchUpInside)
        centerToUserIcon.isHidden = true
        
        view.addSubview(map)
        view.addSubview(centerToUserIcon)
        
    }
    
    func dismissVc(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func centerToUser(){
        self.map.setCenter(map.userLocation.coordinate, animated: true)
    }
    
    func setupConstraints(){
        let views: [String: UIView] = [
            "map": map,
            "centerIcon": centerToUserIcon
        ]
        
        let metrics: [String: CGFloat] = [
            "iconsSize": 60,
            "rightDistance": 20,
            "bottomDistance": 20
        ]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[map]|", options: [], metrics: metrics, views: views))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[centerIcon(iconsSize)]-bottomDistance-|", options: [], metrics: metrics, views: views))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[map]|", options: [], metrics: metrics, views: views))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[centerIcon(iconsSize)]-rightDistance-|", options: [], metrics: metrics, views: views))
        
    }
}

extension StoresMapViewController: PPStoreMapViewDelegate {
    func didTap(store: PPStore) {
        establishmentDetailsCoordinator = EstablishmentCoordinatorFactory().makeDetailsCoordinator(
            store: store,
            navigationController: navigationController,
            fromViewController: self
        )

        establishmentDetailsCoordinator?.didFinishFlow = { [weak self] in
            self?.establishmentDetailsCoordinator = nil
        }

        establishmentDetailsCoordinator?.start()
    }
    
    func didChangeRegion(onMap map: MKMapView) {
        if (!self.map.updatedUserLocation){
            return
        }
        centerToUserIcon.isHidden = map.isUserLocationVisible
    }
}
