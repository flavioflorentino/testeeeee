import Foundation

final class SetupChecklistInfo: Codable {
    private enum CodingKeys: String, CodingKey {
        case screenStatus = "screen_status"
        case nameMother = "name_mother"
        case addressId = "address_id"
        case incomeRange = "income_range"
        case validateIdentity = "verification_status"
        case buttonStatus = "upgrade_status"
    }
    
    let screenStatus: ScreenStatus
    let buttonStatus: ButtonStatus
    var nameMother: String?
    var addressId: String?
    var incomeRange: String?
    var validateIdentity: IdentityStatus?
}

extension SetupChecklistInfo {
    enum ScreenStatus: String, Codable {
        case checklist = "CHECKLIST"
        case pending = "PENDING"
        case rejected = "REJECTED"
        case approved = "APPROVED"
    }
    
    enum IdentityStatus: String, Codable {
        case pending = "PENDING"
        case verified = "VERIFIED"
        case notCreated = "NOT_CREATED"
        case toVerify = "TO_VERIFY"
        case rejected = "REJECTED"
        case disabled = "DISABLED"
    }
    
    enum ButtonStatus: String, Codable {
        case enable = "ENABLED"
        case disable = "DISABLED"
    }
}
