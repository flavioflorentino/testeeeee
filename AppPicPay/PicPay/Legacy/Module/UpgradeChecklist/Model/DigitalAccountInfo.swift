struct DigitalAccountInfo: Decodable {
    let offerUpgradeAccount: Card
    let isJudiciallyBlocked: Bool
    let isPep: Bool
    let registerStatus: RegisterStatus?
    let status: Status?
    
    init(
        offerUpgradeAccount: Card,
        isJudiciallyBlocked: Bool = false,
        isPep: Bool = false,
        registerStatus: RegisterStatus? = nil,
        status: Status? = nil
    ) {
        self.offerUpgradeAccount = offerUpgradeAccount
        self.isJudiciallyBlocked = isJudiciallyBlocked
        self.isPep = isPep
        self.registerStatus = registerStatus
        self.status = status
    }
}

extension DigitalAccountInfo {
    enum Card: String, Decodable {
        case cardLess = "LESS_5K"
        case cardMore = "MORE_5K"
        case cardLimit = "CARD_LIMIT"
        case none = "NONE"
    }
    
    enum RegisterStatus: String, Decodable {
        case manual = "MANUAL_PEP_ANALYSIS"
        case unknown
        
        init(from decoder: Decoder) throws {
            let container = try decoder.singleValueContainer()
            let string = try container.decode(String.self)
            self = RegisterStatus(rawValue: string) ?? .unknown
        }
    }
    
    enum Status: String, Decodable {
        case limited = "LIMITED"
        case unknown
        
        init(from decoder: Decoder) throws {
            let container = try decoder.singleValueContainer()
            let string = try container.decode(String.self)
            self = Status(rawValue: string) ?? .unknown
        }
    }
}

extension DigitalAccountInfo {
    private enum CodingKeys: String, CodingKey {
        case offerUpgradeAccount = "card_bacen"
        case isJudiciallyBlocked = "has_jud_block"
        case registerStatus = "register_status"
        case status
        case isPep = "pep"
    }
}
