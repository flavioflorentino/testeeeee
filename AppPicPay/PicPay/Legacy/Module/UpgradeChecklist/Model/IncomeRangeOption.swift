import Foundation

struct IncomeRangeOption: Codable {
    enum CodingKeys: String, CodingKey {
        case id = "incomeRange"
        case text = "displayText"
    }
    
    var id: String
    var text: String
}
