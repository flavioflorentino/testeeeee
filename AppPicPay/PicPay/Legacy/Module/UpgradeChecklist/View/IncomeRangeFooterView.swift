import UI
import UIKit

final class IncomeRangeFooterView: UIView {
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        let attributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: Palette.ppColorGrayscale500.color,
            .font: UIFont.systemFont(ofSize: 14, weight: .regular),
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let attributedTex = NSMutableAttributedString(string: UpgradeChecklistLocalizable.incomeRangeFooter.text, attributes: attributes)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.attributedText = attributedTex
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        addSubview(descriptionLabel)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: topAnchor),
            descriptionLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20)
        ])
    }
}
