import UI
import UIKit

final class IncomeRangeOptionTableViewCell: UITableViewCell {
    private let chevronImageSize = 14
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale200.color
        return view
    }()
    
    private lazy var chevronImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "green_arrow")
        return imageView
    }()
    
    private lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.isHidden = true
        imageView.contentMode = .left
        imageView.image = #imageLiteral(resourceName: "cardSelected")
        return imageView
    }()
    
    var infoText: String {
        set {
            infoLabel.text = newValue
        }
        get {
            return infoLabel.text ?? ""
        }
    }
    
    var addressSelected: Bool = false {
        didSet {
            selectImageView.isHidden = !addressSelected
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        contentView.addSubview(infoLabel)
        contentView.addSubview(chevronImageView)
        contentView.addSubview(selectImageView)
        contentView.addSubview(lineView)
    }
    
    private func layoutComponents() {
        infoLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(20)
            make.leading.equalToSuperview().offset(20)
        }
        
        selectImageView.snp.makeConstraints { make in
            make.width.height.equalTo(16)
            make.height.height.equalTo(16)
            make.centerY.equalToSuperview()
            make.leading.equalTo(infoLabel.snp.trailing).offset(16)
        }
        
        chevronImageView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.width.height.equalTo(chevronImageSize)
            make.leading.greaterThanOrEqualTo(selectImageView.snp.trailing).offset(16)
            make.trailing.equalToSuperview().offset(-16)
        }
        
        lineView.snp.makeConstraints { make in
            make.top.equalTo(infoLabel.snp.bottom).offset(20)
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
    }
}
