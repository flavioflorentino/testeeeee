import Foundation
import UI

final class UpgradeChecklistOptionView: UIView {
    private let statusImageSize: CGFloat = 24
    private let chevronImageSize: CGFloat = 14
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = 2.0
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var statusImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "empty_info")
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        return label
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        label.textColor = Palette.ppColorNeutral300.color
        return label
    }()
    
    private lazy var chevronImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "green_arrow")
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale300.color
        
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var statusChecklist: UpgradeChecklistViewModel.OptiontStatus = .pending {
        didSet {
            descLabel.isHidden = true
            switch statusChecklist {
            case .checked:
                statusImageView.image = #imageLiteral(resourceName: "iconChecked")
            case .analysis:
                statusImageView.image = #imageLiteral(resourceName: "upgrade_wait")
                descLabel.text = UpgradeChecklistLocalizable.descriptionAnalysis.text
                descLabel.isHidden = false
            case .rejected:
                statusImageView.image = #imageLiteral(resourceName: "ico_personal_data_attention")
            default:
                statusImageView.image = #imageLiteral(resourceName: "empty_info")
            }
        }
    }
    
    var didTapOption: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = Palette.ppColorGrayscale000.color
        isUserInteractionEnabled = true
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
    }
    
    private func addComponents() {
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descLabel)
        
        addSubview(stackView)
        addSubview(statusImageView)
        addSubview(chevronImageView)
        addSubview(lineView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            statusImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            statusImageView.centerYAnchor.constraint(equalTo: stackView.centerYAnchor),
            statusImageView.widthAnchor.constraint(equalToConstant: statusImageSize),
            statusImageView.heightAnchor.constraint(equalToConstant: statusImageSize)
        ])
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            stackView.bottomAnchor.constraint(equalTo: lineView.topAnchor, constant: -20),
            stackView.leadingAnchor.constraint(equalTo: statusImageView.trailingAnchor, constant: 14),
            stackView.trailingAnchor.constraint(equalTo: chevronImageView.leadingAnchor, constant: -10)
        ])
        
        NSLayoutConstraint.activate([
            chevronImageView.centerYAnchor.constraint(equalTo: stackView.centerYAnchor),
            chevronImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            chevronImageView.widthAnchor.constraint(equalToConstant: chevronImageSize),
            chevronImageView.heightAnchor.constraint(equalToConstant: chevronImageSize)
        ])
        
        NSLayoutConstraint.activate([
            lineView.bottomAnchor.constraint(equalTo: bottomAnchor),
            lineView.leadingAnchor.constraint(equalTo: leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: trailingAnchor),
            lineView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    @objc
    private func didTapView() {
        didTapOption?()
    }
    
    func config(with option: ChecklistOption, status: UpgradeChecklistViewModel.OptiontStatus = .pending) {
        titleLabel.text = option.text
        statusChecklist = status
    }
}

extension UpgradeChecklistOptionView {
    enum ChecklistOption {
        case motherName
        case address
        case income
        case validateIdentity
        
        var text: String {
            switch self {
            case .motherName:
                return UpgradeChecklistLocalizable.motherTitle.text
            case .address:
                return UpgradeChecklistLocalizable.yourAddress.text
            case .income:
                return UpgradeChecklistLocalizable.incomeTitle.text
            case .validateIdentity:
                return UpgradeChecklistLocalizable.validateIdentity.text
            }
        }
    }
}
