import SnapKit
import UI
import UIKit

final class UpgradeChecklistProgressBar: UIView {
    private let progressBarHeight: CGFloat = 8
    
    private lazy var progressLabel: AnimatedLabel = {
        let progressLabel = AnimatedLabel()
        progressLabel.kerning = 0.3
        progressLabel.textAlignment = .center
        progressLabel.font = UIFont.systemFont(ofSize: 12)
        progressLabel.textColor = Palette.ppColorGrayscale500.color
        progressLabel.translatesAutoresizingMaskIntoConstraints = false
        progressLabel.countFromZero(to: 0)
        progressLabel.customFormatBlock = { [weak self] value in
            guard let strongSelf = self else {
                return NSAttributedString(string: "")
            }
            return NSAttributedString(string: "\(Int(value))%")
        }
        return progressLabel
    }()

    private lazy var progressBar: UIProgressView = {
        let progressBar = UIProgressView()
        progressBar.trackTintColor = Palette.ppColorGrayscale300.color
        progressBar.progressTintColor = Palette.ppColorNeutral300.color
        progressBar.clipsToBounds = true
        progressBar.layer.cornerRadius = progressBarHeight / 2
        progressBar.translatesAutoresizingMaskIntoConstraints = false
        
        return progressBar
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        addSubview(progressLabel)
        addSubview(progressBar)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            progressLabel.topAnchor.constraint(equalTo: topAnchor),
            progressLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            progressLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            progressLabel.widthAnchor.constraint(equalToConstant: 40)
        ])
        
        NSLayoutConstraint.activate([
            progressBar.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            progressBar.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
            progressBar.leadingAnchor.constraint(equalTo: progressLabel.trailingAnchor, constant: 4),
            progressBar.trailingAnchor.constraint(equalTo: trailingAnchor),
            progressBar.heightAnchor.constraint(equalToConstant: progressBarHeight)
        ])
    }
    
    func animate(to progressValue: Float) {
        // Foi necessario por conta de um bug que acontecia com a internet lenta ao animar o progressBar.
        DispatchQueue.main.async { [weak self] in
            let percent = Float(ceil(progressValue * 100))
            self?.progressLabel.countFromZero(to: Float(percent), duration: 0.8)
            self?.progressBar.setProgress(progressValue, animated: true)
        }
    }
}
