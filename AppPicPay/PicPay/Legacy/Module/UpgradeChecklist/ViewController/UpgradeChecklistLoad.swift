import UI
import UIKit

protocol UpgradeChecklistLoadDelegate: AnyObject {
    func open(screen: SetupChecklistInfo.ScreenStatus, model: SetupChecklistInfo?)
    func close()
}

final class UpgradeChecklistLoad: PPBaseViewController {
    private let viewModel: UpgradeChecklistLoadViewModel
    weak var delegate: UpgradeChecklistLoadDelegate?
    
    init(with: UpgradeChecklistLoadViewModel) {
        self.viewModel = with
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getSetupChecklistInfo()
    }
    
    private func setupView() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_round_close_green"), style: .done, target: self, action: #selector(didTapCloseButton))
    }
    
    private func getSetupChecklistInfo() {
        startLoadingView()
        viewModel.setupChecklistInfo(onSuccess: { [weak self] in
            self?.stopLoadingView()
            self?.setupChecklisSuccess()
        }, onError: { [weak self] error in
            self?.stopLoadingView()
            self?.setupChecklisError(error: error)
        })
    }
    
    private func setupChecklisSuccess() {
        let status = viewModel.screenStatus()
        let model = viewModel.checklistInfo()
        delegate?.open(screen: status, model: model)
    }
    
    private func setupChecklisError(error: Error) {
        AlertMessage.showCustomAlertWithError(error, controller: self) { [weak self] in
            self?.delegate?.close()
        }
    }
    
    @objc
    private func didTapCloseButton() {
        delegate?.close()
    }
}
