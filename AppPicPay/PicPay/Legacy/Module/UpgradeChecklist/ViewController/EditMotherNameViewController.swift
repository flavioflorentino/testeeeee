import Foundation
import UI

protocol EditMotherNameViewControllerDelegate: AnyObject {
    func completeEditMother(upgrade: SetupChecklistInfo)
}

final class EditMotherNameViewController: PPBaseViewController {
    private lazy var textField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = UpgradeChecklistLocalizable.editMotherNamePlaceholder.text
        textField.textColor = Palette.ppColorGrayscale600.color
        textField.selectedLineColor = Palette.ppColorBranding300.color(withCustomDark: .ppColorGrayscale000)
        textField.placeholderColor = Palette.ppColorGrayscale400.color(withCustomDark: .ppColorGrayscale000)
        textField.selectedTitleColor = Palette.ppColorBranding300.color
        textField.addTarget(self, action: #selector(validateMotherName), for: .editingChanged)
        return textField
    }()
    
    private lazy var confirmButton: UIPPButton = {
        let confirmButton = UIPPButton()
        let button = Button(title: DefaultLocalizable.btConfirm.text, type: Button.ButtonType.cta)
        confirmButton.configure(with: button, font: UIFont.systemFont(ofSize: 16, weight: .semibold))
        confirmButton.setTitleColor(Palette.ppColorGrayscale600.color)
        confirmButton.cornerRadius = 24
        confirmButton.isEnabled = false
        confirmButton.addTarget(self, action: #selector(didTapConfirmButton), for: .touchUpInside)
        return confirmButton
    }()
    
    private let viewModel: EditMotherNameViewModel
    weak var delegate: EditMotherNameViewControllerDelegate?
    
    init(with viewModel: EditMotherNameViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        addComponents()
        layoutComponents()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    private func setupView() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        title = UpgradeChecklistLocalizable.motherTitle.text
        textField.text = viewModel.motherName()
    }
    
    private func addComponents() {
        view.addSubview(textField)
        view.addSubview(confirmButton)
    }
    
    private func layoutComponents() {
        textField.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(40)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        confirmButton.snp.makeConstraints { make in
            make.top.equalTo(textField.snp.bottom).offset(31)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo(48)
        }
    }
    
    @objc
    private func validateMotherName() {
        guard let motherName = textField.text else {
            return
        }
        let valid = viewModel.sanitizedMotherName(name: motherName)
        textField.text = valid
        confirmButton.isEnabled = !valid.isEmpty
    }
    
    @objc
    private func didTapConfirmButton() {
        guard let motherName = textField.text else {
            return
        }
        confirmButton.startLoadingAnimating()
        viewModel.updateMotherName(motherName: motherName, onSuccess: { [weak self] result in
            self?.confirmButton.stopLoadingAnimating()
            self?.delegate?.completeEditMother(upgrade: result)
        }, onError: { [weak self] error in
            self?.confirmButton.stopLoadingAnimating()
            AlertMessage.showAlert(error, controller: self)
        })
    }
}
