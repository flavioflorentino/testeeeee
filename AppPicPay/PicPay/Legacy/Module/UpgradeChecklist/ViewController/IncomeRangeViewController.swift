import UI
import UIKit

protocol IncomeRangeViewControllerDelegate: AnyObject {
    func completeSelectIncomeRange(upgrade: SetupChecklistInfo)
    func didTapHelp()
}

final class IncomeRangeViewController: PPBaseViewController {
    private let backButtonSize = 20
    
    private lazy var headerView: UIView = {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 80))
        let headerIncome = IncomeRangeHeaderView(frame: headerView.frame)
        headerView.addSubview(headerIncome)
        return headerView
    }()
    
    private lazy var footerView: UIView = {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 80))
        let footerIncome = IncomeRangeFooterView(frame: footerView.frame)
        let target = UITapGestureRecognizer(target: self, action: #selector(didTapHelp))
        footerIncome.addGestureRecognizer(target)
        footerView.addSubview(footerIncome)
        return footerView
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.register(IncomeRangeOptionTableViewCell.self, forCellReuseIdentifier: String(describing: IncomeRangeOptionTableViewCell.self))
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        return tableView
    }()
    
    private let viewModel: IncomeRangeViewModel
    weak var delegate: IncomeRangeViewControllerDelegate?
    
    init(viewModel: IncomeRangeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        loadIncomeRange()
    }
    
    func loadIncomeRange() {
        startLoadingView()
        viewModel.incomeRangeOptions(onSucess: { [weak self] in
            self?.stopLoadingView()
            self?.tableView.reloadData()
        }, onError: { [weak self] error in
            guard let strongSelf = self else {
            return
        }
            strongSelf.stopLoadingView()
            AlertMessage.showCustomAlertWithError(error, controller: strongSelf)
        })
    }
    
    private func setupView() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        title = UpgradeChecklistLocalizable.incomeTitle.text
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalToSuperview()
            make.top.equalToSuperview().offset(12)
        }
        
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = footerView
    }
    
    private func showConfirmPopUp(with option: IncomeRangeOption) {
        let alert = Alert(title: UpgradeChecklistLocalizable.confirmIncomeRange.text, text: option.text)
        let buttonConfirm = Button(title: DefaultLocalizable.btConfirm.text, type: .cta) { [weak self] popupController, uiButton in
            self?.selectOption(id: option.id, uiButton: uiButton, alert: popupController)
        }
        let buttonCancel = Button(title: DefaultLocalizable.btCancel.text, type: .inline, action: .close)
        alert.buttons = [buttonConfirm, buttonCancel]
        AlertMessage.showAlert(alert, controller: self)
    }
    
    private func selectOption(id: String, uiButton: UIPPButton, alert: AlertPopupViewController) {
        uiButton.startLoadingAnimating()
        viewModel.selectIncomeRange(id: id, onSuccess: { [weak self] result in
            uiButton.stopLoadingAnimating()
            alert.dismiss(animated: true, completion: {
                self?.delegate?.completeSelectIncomeRange(upgrade: result)
            })
        }, onError: { [weak self] error in
            uiButton.stopLoadingAnimating()
            alert.dismiss(animated: true, completion: {
                AlertMessage.showCustomAlertWithError(error, controller: self)
            })
        })
    }
    
    @objc
    private func didTapHelp() {
        delegate?.didTapHelp()
    }
}

extension IncomeRangeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: false)
        let option = viewModel.incomeRangeSelected(at: indexPath.row)
        showConfirmPopUp(with: option)
    }
}

extension IncomeRangeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(type: IncomeRangeOptionTableViewCell.self)
        let row = indexPath.row
        
        cell.infoText = viewModel.title(index: row)
        cell.addressSelected = viewModel.addressIsSelected(index: row)
        return cell
    }
}
