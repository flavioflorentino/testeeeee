import UI
import UIKit

final class IncomeRangeHelpViewController: UIViewController {
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.textColor = Palette.ppColorGrayscale500.color
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        addComponents()
        layoutComponents()
    }
    
    private func setupView() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        title = UpgradeChecklistLocalizable.incomeHelpTitle.text
        subtitleLabel.text = UpgradeChecklistLocalizable.incomeHelpSubtitle.text
        descriptionLabel.text = UpgradeChecklistLocalizable.incomeHelpDescription.text
    }
    
    private func addComponents() {
        view.addSubview(subtitleLabel)
        view.addSubview(descriptionLabel)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            subtitleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 15),
            subtitleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            subtitleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
        ])
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: subtitleLabel.bottomAnchor, constant: 28),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
        ])
    }
}
