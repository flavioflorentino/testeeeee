import UI

protocol UpgradeChecklistViewControllerDelegate: AnyObject {
    func didTapClose()
    func didTapEditMother(name: String)
    func didTapIncomeRange(id: String)
    func didTapValidateIdentity()
    func completeAddressList(upgrade: SetupChecklistInfo)
    func completeSendUpgrade()
}

// swiftlint:disable:next type_body_length
final class UpgradeChecklistViewController: PPBaseViewController {
    private lazy var contentView: UIView = {
        return UIView()
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = 0.0
        return stackView
    }()
    
    private lazy var rockerImageView: UIImageView = {
        let rockerImageView = UIImageView()
        rockerImageView.image = #imageLiteral(resourceName: "rocket")
        return rockerImageView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.text = UpgradeChecklistLocalizable.accountDescription.text
        descriptionLabel.font = UIFont.systemFont(ofSize: 16, weight: .light)
        descriptionLabel.textColor = Palette.ppColorGrayscale500.color
        descriptionLabel.numberOfLines = 0
        descriptionLabel.kerning = 0.3
        return descriptionLabel
    }()
    
    private lazy var progressBar: UpgradeChecklistProgressBar = {
        return UpgradeChecklistProgressBar()
    }()
    
    private lazy var motherNameOption: UpgradeChecklistOptionView = {
        let option = UpgradeChecklistOptionView()
        option.config(with: .motherName)
        option.didTapOption = { [weak self] in
            self?.goToEditMothersName()
        }
        return option
    }()
    
    private lazy var addressOption: UpgradeChecklistOptionView = {
        let option = UpgradeChecklistOptionView()
        option.config(with: .address)
        option.didTapOption = { [weak self] in
            self?.goToAddressList()
        }
        return option
    }()
    
    private lazy var incomeOption: UpgradeChecklistOptionView = {
        let option = UpgradeChecklistOptionView()
        option.config(with: .income)
        option.didTapOption = { [weak self] in
            self?.goToIncomeRangeOption()
        }
        return option
    }()
    
    private lazy var validateIdentityOption: UpgradeChecklistOptionView = {
        let option = UpgradeChecklistOptionView()
        option.config(with: .validateIdentity)
        option.didTapOption = { [weak self] in
            self?.goToValidateIdentity()
        }
        return option
    }()
    
    private lazy var confirmButton: UIPPButton = {
        let confirmButton = UIPPButton()
        let button = Button(title: DefaultLocalizable.btConfirm.text, type: Button.ButtonType.cta)
        confirmButton.configure(with: button, font: UIFont.systemFont(ofSize: 16, weight: .semibold))
        confirmButton.cornerRadius = 24
        confirmButton.addTarget(self, action: #selector(didTapConfirmButton), for: .touchUpInside)
        return confirmButton
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.backgroundColor = Palette.ppColorGrayscale000.color
        return scrollView
    }()
    
    private let viewModel: UpgradeChecklistViewModel
    weak var delegate: UpgradeChecklistViewControllerDelegate?
    
    init(viewModel: UpgradeChecklistViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PPAnalytics.trackFirstTimeOnlyEvent(UpgradeChecklistEvent.checklistHome, properties: nil)
        getSetupChecklistInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        progressBar.animate(to: viewModel.checklistProgress)
    }
    
    private func getSetupChecklistInfo() {
        configChecklistView()
        trackEventChecklist()
    }
    
    private func trackEventChecklist() {
        if viewModel.motherNameStatus() == .checked {
            PPAnalytics.trackFirstTimeOnlyEvent(UpgradeChecklistEvent.checklistMother, properties: nil)
        }
        
        if viewModel.addressStatus() == .checked {
            PPAnalytics.trackFirstTimeOnlyEvent(UpgradeChecklistEvent.checklistAddress, properties: nil)
        }
        
        if viewModel.incomeRangeStatus() == .checked {
            PPAnalytics.trackFirstTimeOnlyEvent(UpgradeChecklistEvent.checklistIncome, properties: nil)
        }
        
        if viewModel.validateIdentityStatus() == .checked {
            PPAnalytics.trackFirstTimeOnlyEvent(UpgradeChecklistEvent.checklistIdentity, properties: nil)
        }
    }
    
    private func configChecklistView() {
        motherNameOption.config(with: .motherName, status: viewModel.motherNameStatus())
        addressOption.config(with: .address, status: viewModel.addressStatus())
        incomeOption.config(with: .income, status: viewModel.incomeRangeStatus())
        validateIdentityOption.config(with: .validateIdentity, status: viewModel.validateIdentityStatus())
        
        validate()
    }
    
    private func validate() {
        confirmButton.isEnabled = viewModel.buttonIsEnabled()
    }
    
    private func setupView() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_round_close_green"), style: .done, target: self, action: #selector(didTapBackButton))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "green_help_button"), style: .done, target: self, action: #selector(openHelpCenter))
        title = UpgradeChecklistLocalizable.accountTitle.text
        view.backgroundColor = Palette.ppColorGrayscale000.color
        
        configScrollView()
        configContentView()
    }
    
    private func configScrollView() {
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { make in
            make.top.bottom.leading.trailing.equalToSuperview()
        }
        
        scrollView.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalTo(scrollView)
            make.height.equalTo(view.snp.height).priority(.low)
            make.width.equalTo(view.snp.width)
        }
    }
    
    private func configContentView() {
        contentView.addSubview(rockerImageView)
        rockerImageView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(19)
            make.leading.equalToSuperview().offset(19)
            make.height.width.equalTo(64)
        }
        contentView.addSubview(descriptionLabel)
        descriptionLabel.snp.makeConstraints { make in
            make.top.greaterThanOrEqualToSuperview().offset(19)
            make.centerY.equalTo(rockerImageView.snp.centerY)
            make.leading.equalTo(rockerImageView.snp.trailing).offset(15)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        contentView.addSubview(progressBar)
        progressBar.snp.makeConstraints { make in
            make.top.equalTo(rockerImageView.snp.bottom).offset(15)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        configOptionViews()
    }
    
    private func configOptionViews() {
        contentView.addSubview(stackView)
        stackView.addArrangedSubview(motherNameOption)
        stackView.addArrangedSubview(addressOption)
        stackView.addArrangedSubview(incomeOption)
        if viewModel.validateIdentityStatus() != .disabled {
            stackView.addArrangedSubview(validateIdentityOption)
        }
        
        stackView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(progressBar.snp.bottom).offset(10)
        }
       
        contentView.addSubview(confirmButton)
        confirmButton.snp.makeConstraints { make in
            make.top.equalTo(stackView.snp.bottom).offset(20)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.greaterThanOrEqualToSuperview()
            make.height.equalTo(48)
        }
    }
    
    @objc
    private func didTapBackButton() {
        delegate?.didTapClose()
    }
    
    @objc
    private func openHelpCenter() {
        let url = UpgradeChecklistDeeplink.upgrade
        DeeplinkHelper.handleDeeplink(withUrl: URL(string: url), from: self)
    }
    
    private func goToEditMothersName() {
        let name = viewModel.motherName()
        delegate?.didTapEditMother(name: name)
    }
    
    private func goToIncomeRangeOption() {
        let id = viewModel.incomeRange()
        delegate?.didTapIncomeRange(id: id)
    }
    
    private func goToValidateIdentity() {
        delegate?.didTapValidateIdentity()
    }
    
    private func goToAddressList() {
        let idAddress = viewModel.addressId()
        let addressListVM = AddressListViewModel(displayContextHeader: true)
        if !idAddress.isEmpty, let id = Int(idAddress) {
            let addressItem = ConsumerAddressItem(id: id)
            addressListVM.selectedAddress = addressItem
        }
        
        let consumerAddressList = AddressListViewController(viewModel: addressListVM)
        consumerAddressList.addressContextTitle = UpgradeChecklistLocalizable.homeAddress.text
        consumerAddressList.addressContextText = UpgradeChecklistLocalizable.homeAddressDescription.text
        consumerAddressList.addressContextImage = #imageLiteral(resourceName: "home")
        
        if #available(iOS 11.0, *) {
            consumerAddressList.navigationItem.largeTitleDisplayMode = .never
        }
        
        consumerAddressList.onAddressSelected = { [weak self] address in
            guard let strongSelf = self, let address = address else {
                return
            }
            
            let alert = Alert(title: UpgradeChecklistLocalizable.confirmAddress.text, text: "\(address.getSubtitle())")
            let buttonYes = Button(title: DefaultLocalizable.btConfirm.text, type: .cta) { [weak self] popupController, uiButton in
                if let id = address.id {
                    self?.selectAddress(id: id, uiButton: uiButton, alert: popupController)
                }
            }
            let buttonNo = Button(title: UpgradeChecklistLocalizable.btItsNotThis.text, type: .inline, action: .close)
            alert.buttons = [buttonYes, buttonNo]
            AlertMessage.showAlert(alert, controller: strongSelf)
        }
        self.navigationController?.pushViewController(consumerAddressList, animated: true)
    }
    
    private func selectAddress(id: Int, uiButton: UIPPButton, alert: AlertPopupViewController) {
        uiButton.startLoadingAnimating()
        let id = String(id)
        viewModel.selectAddress(with: id, onSuccess: { [weak self] result in
            uiButton.stopLoadingAnimating()
            alert.dismiss(animated: true, completion: {
                self?.delegate?.completeAddressList(upgrade: result)
            })
        }, onError: { [weak self] error in
            uiButton.stopLoadingAnimating()
            alert.dismiss(animated: true, completion: {
                AlertMessage.showCustomAlertWithError(error, controller: self)
            })
        })
    }
    
    @objc
    private func didTapConfirmButton() {
        confirmButton.startLoadingAnimating()
        viewModel.confirmUpgradeChecklist(onSuccess: { [weak self] in
            self?.confirmButton.stopLoadingAnimating()
            self?.delegate?.completeSendUpgrade()
        }, onError: { [weak self] error in
            self?.confirmButton.stopLoadingAnimating()
            AlertMessage.showCustomAlertWithError(error, controller: self)
        })
    }
}
