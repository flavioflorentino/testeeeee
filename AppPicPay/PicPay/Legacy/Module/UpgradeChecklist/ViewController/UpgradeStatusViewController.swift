import UI
import UIKit

final class UpgradeStatusViewController: PPBaseViewController {
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 20
        return stackView
    }()
    
    private lazy var statusImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale600.color
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 28)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        label.textAlignment = .center
        label.kerning = 0.3
        label.font = UIFont.systemFont(ofSize: 16, weight: .light)
        return label
    }()
    
    private lazy var confirmButton: UIPPButton = {
        let uiButton = UIPPButton()
        let button = Button(title: DefaultLocalizable.btOkUnderstood.text, type: .cta)
        uiButton.configure(with: button, font: UIFont.boldSystemFont(ofSize: 16))
        uiButton.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        uiButton.cornerRadius = 24
        return uiButton
    }()
    
    private lazy var extraInfoButton: UIPPButton = {
        let uiButton = UIPPButton()
        let button = Button(title: DefaultLocalizable.learnMore.text, type: .underlined)
        button.titleColor = PicPayStyle.mediumGrayHex
        uiButton.configure(with: button, font: UIFont.systemFont(ofSize: 14, weight: .semibold))
        uiButton.addTarget(self, action: #selector(openHelpCenter), for: .touchUpInside)
        return uiButton
    }()
    
    private let upgradeStatus: UpgradeStatus
    private let statusImageSize = 88
    
    init(status: UpgradeStatus) {
        self.upgradeStatus = status
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        addComponents()
        layoutComponents()
    }
    
    private func setupView() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_round_close_green"), style: .done, target: self, action: #selector(didTapClose))
        titleLabel.text = upgradeStatus.title
        descriptionLabel.text = upgradeStatus.descriptionText
        statusImageView.image = upgradeStatus.image
    }
    
    private func addComponents() {
        stackView.addArrangedSubview(statusImageView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descriptionLabel)
        stackView.addArrangedSubview(confirmButton)
        if upgradeStatus.shouldShowExtraInfoButton {
            stackView.addArrangedSubview(extraInfoButton)
        }
        
        view.addSubview(stackView)
    }
    
    private func layoutComponents() {
        statusImageView.snp.makeConstraints { make in
            make.height.equalTo(statusImageSize)
        }
        
        confirmButton.snp.makeConstraints { make in
            make.height.equalTo(48)
        }
        
        stackView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
    }
    
    @objc
    private func didTapClose() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc
    private func openHelpCenter() {
        let url = UpgradeChecklistDeeplink.status
        DeeplinkHelper.handleDeeplink(withUrl: URL(string: url), from: self)
    }
}

extension UpgradeStatusViewController {
    enum UpgradeStatus {
        case success
        case failure
        case pending
        
        var title: String {
            switch self {
            case .success:
                return UpgradeChecklistLocalizable.successStatusTitle.text
            case .failure:
                return UpgradeChecklistLocalizable.failureStatusTitle.text
            case .pending:
                return UpgradeChecklistLocalizable.waitingStatusTitle.text
            }
        }
        
        var descriptionText: String {
            switch self {
            case .success:
                return UpgradeChecklistLocalizable.successStatusDescription.text
            case .failure:
                return UpgradeChecklistLocalizable.failureStatusDescription.text
            case .pending:
                return UpgradeChecklistLocalizable.waitingStatusDescription.text
            }
        }
        
        var image: UIImage {
            switch self {
            case .success:
                return #imageLiteral(resourceName: "rocket")
            case .failure:
                return #imageLiteral(resourceName: "iluSadFace")
            case .pending:
                return #imageLiteral(resourceName: "icon-green-clock-copy")
            }
        }
        
        var shouldShowExtraInfoButton: Bool {
            return self == .failure
        }
    }
}
