import UI

// Forces transition to white bar if no other bar tint color is attributed inside view controller
// Fix animation color bug after pressing the back button
final class UpgradeNavigationController: PPNavigationController {
    @discardableResult
    override func popViewController(animated: Bool) -> UIViewController? {
        let viewController = super.popViewController(animated: animated)
        navigationBar.barTintColor = Palette.ppColorGrayscale000.color
        return viewController
    }
}
