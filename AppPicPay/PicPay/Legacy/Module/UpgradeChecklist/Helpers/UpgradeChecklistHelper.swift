import Foundation
import FeatureFlag

struct UpgradeChecklistEvent {
    static let checklistHome = "Upgrade - Iniciou"
    static let checklistMother = "Upgrade - Preencheu nome da mae"
    static let checklistAddress = "Upgrade - Preencheu endereço"
    static let checklistIncome = "Upgrade - Preencheu renda"
    static let checklistIdentity = "Upgrade - Validou identidade"
    static let checklistTitle = "Upgrade - Tocou na oferta"
    static let checklistAttribute = "Canal"
    static let feesAndLimits = "Tela de taxas e limites"
}

struct UpgradeChecklistDeeplink {
    static let upgrade = FeatureManager.text(.upgradeDeeplink)
    static let fees = FeatureManager.text(.upgradeDeeplinkFees)
    static let status = FeatureManager.text(.upgradeDeeplinkStatus)
}
