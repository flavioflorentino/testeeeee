import Core
import FeatureFlag
import Foundation

struct UpgradeAccountWorkerConstants {
    static let incomeRange = "income_range"
    static let addressId = "address_id"
}

protocol UpgradeAccountWorkerProtocol {
    func incomeRangeOptions(completion: @escaping (PicPayResult<[IncomeRangeOption]>) -> Void)
    func selectRangeOption(_ incomeRange: String, completion: @escaping (PicPayResult<SetupChecklistInfo>) -> Void)
    func selectAddress(with id: String, completion: @escaping (PicPayResult<SetupChecklistInfo>) -> Void)
    func setupChecklistInfo(completion: @escaping (PicPayResult<SetupChecklistInfo>) -> Void)
    func updateMotherName(motherName: String, completion: @escaping (PicPayResult<SetupChecklistInfo>) -> Void)
    func confirmVerificationIdentity(completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void)
    func confirmUpgradeChecklist(completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void)
}

final class UpgradeAccountWorker: BaseApi, UpgradeAccountWorkerProtocol {
    typealias Dependencies = HasFeatureManager & HasMainQueue
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func incomeRangeOptions(completion: @escaping (PicPayResult<[IncomeRangeOption]>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlIncomeRangeOptions, method: .get)
                .responseApiCodable(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func selectRangeOption(_ incomeRange: String, completion: @escaping (PicPayResult<SetupChecklistInfo>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let params = [UpgradeAccountWorkerConstants.incomeRange: incomeRange]
            requestManager
                .apiRequest(endpoint: kWsUrlIncomeRangeSelect, method: .put, parameters: params)
                .responseApiObject { [weak self] (result: PicPayResult<BaseApiGenericResponse>) in
                    self?.dependencies.mainQueue.async {
                        switch result {
                        case .success:
                            self?.setupChecklistInfo(completion: completion)
                        case .failure(let error):
                            completion(PicPayResult.failure(error))
                        }
                    }
                }
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func selectAddress(with id: String, completion: @escaping (PicPayResult<SetupChecklistInfo>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let params = [UpgradeAccountWorkerConstants.addressId: id]
            requestManager
                .apiRequest(endpoint: kWsUrlSelectAddress, method: .put, parameters: params)
                .responseApiObject { [weak self] (result: PicPayResult<BaseApiGenericResponse>) in
                    self?.dependencies.mainQueue.async {
                        switch result {
                        case .success:
                            self?.setupChecklistInfo(completion: completion)
                        case .failure(let error):
                            completion(PicPayResult.failure(error))
                        }
                    }
                }
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func digitalAccountInfo(completion: @escaping (PicPayResult<DigitalAccountInfo>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlDigitalAccountInfo, method: .get)
                .responseApiCodable(strategy: .useDefaultKeys, completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func setupChecklistInfo(completion: @escaping (PicPayResult<SetupChecklistInfo>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlUpgradeChecklist, method: .get)
                .responseApiCodable(strategy: .useDefaultKeys, completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func updateMotherName(motherName: String, completion: @escaping (PicPayResult<SetupChecklistInfo>) -> Void) {
        guard let consumer = ConsumerManager.shared.consumer,
              let cpf = consumer.cpf,
              let name = consumer.name,
              let birthDate = consumer.birth_date else {
            return
        }
        WSConsumer.updatePersonalData(name, cpf: cpf, birth_date: birthDate, name_mother: motherName) { [weak self] _, error in
            self?.dependencies.mainQueue.async {
                if let error = error {
                    let picpayError = PicPayError(message: error.localizedDescription)
                    completion(PicPayResult.failure(picpayError))
                } else {
                    self?.setupChecklistInfo(completion: completion)
                }
            }
        }
    }
    
    func confirmVerificationIdentity(completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlUpgradeChecklistIdentity, method: .post)
                .responseApiObject { [weak self] (result: PicPayResult<BaseApiGenericResponse>) in
                    self?.dependencies.mainQueue.async {
                        switch result {
                        case .success(let value):
                            completion(PicPayResult.success(value))
                        case .failure(let error):
                            completion(PicPayResult.failure(error))
                        }
                    }
                }
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func confirmUpgradeChecklist(completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlUpgradeChecklistConfirm, method: .post)
                .responseApiObject { [weak self] (result: PicPayResult<BaseApiGenericResponse>) in
                    self?.dependencies.mainQueue.async {
                        switch result {
                        case .success(let value):
                            completion(PicPayResult.success(value))
                        case .failure(let error):
                            completion(PicPayResult.failure(error))
                        }
                    }
                }
            return
        }
        // TODO: - adicionar helper para o core network
    }
}
