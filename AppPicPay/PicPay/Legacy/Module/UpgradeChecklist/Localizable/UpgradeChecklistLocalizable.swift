import Foundation

enum UpgradeChecklistLocalizable: String, Localizable {
    case accountTitle
    case accountDescription
    case motherTitle
    case yourAddress
    case incomeTitle
    case validateIdentity
    case homeAddress
    case homeAddressDescription
    case incomeRangeTitle
    case incomeRangeHeader
    case incomeRangeFooter
    case incomeHelpTitle
    case incomeHelpSubtitle
    case incomeHelpDescription
    case confirmIncomeRange
    case confirmAddress
    case btItsNotThis
    case successStatusTitle
    case successStatusDescription
    case waitingStatusTitle
    case waitingStatusDescription
    case failureStatusTitle
    case failureStatusDescription
    case editMotherNamePlaceholder
    case descriptionAnalysis
    
    var file: LocalizableFile {
        return .upgradeChecklist
    }
    
    var key: String {
        return self.rawValue
    }
}
