import Foundation

final class UpgradeChecklistViewModel {
    private let worker: UpgradeAccountWorkerProtocol
    private var model: SetupChecklistInfo
    
    var checklistProgress: Float {
        var checkedItems: Float = 0
        if motherNameStatus() == .checked {
            checkedItems += 1
        }
        if addressStatus() == .checked {
            checkedItems += 1
        }
        if incomeRangeStatus() == .checked {
            checkedItems += 1
        }
        if validateIdentityStatus() == .checked {
            checkedItems += 1
        }
        return checkedItems / totalItems
    }
    
    var totalItems: Float {
        return validateIdentityStatus() != .disabled ? 4 : 3
    }
    
    init(model: SetupChecklistInfo, worker: UpgradeAccountWorkerProtocol = UpgradeAccountWorker()) {
        self.model = model
        self.worker = worker
    }
    
    func motherNameStatus() -> OptiontStatus {
        return model.nameMother == nil ? .pending : .checked
    }
    
    func addressStatus() -> OptiontStatus {
        return model.addressId == nil ? .pending : .checked
    }
    
    func incomeRangeStatus() -> OptiontStatus {
        return model.incomeRange == nil ? .pending : .checked
    }
    
    func validateIdentityStatus() -> OptiontStatus {
        guard let validateIdentity = model.validateIdentity else {
            return .disabled
        }
        
        switch validateIdentity {
        case .disabled:
            return .disabled
        case .toVerify:
            return .analysis
        case .verified:
            return .checked
        case .rejected:
            return .rejected
        default:
            return .pending
        }
    }
    
    func buttonIsEnabled() -> Bool {
        return model.buttonStatus == .enable
    }
    
    func motherName() -> String {
        return model.nameMother ?? ""
    }
    
    func incomeRange() -> String {
        return model.incomeRange ?? ""
    }
    
    func addressId() -> String {
        return model.addressId ?? ""
    }
    
    func setChecklist(upgrade: SetupChecklistInfo) {
        model = upgrade
    }
    
    func selectAddress(with id: String, onSuccess: @escaping(SetupChecklistInfo) -> Void, onError: @escaping(PicPayErrorDisplayable) -> Void) {
        worker.selectAddress(with: id) { result in
            switch result {
            case .success(let value):
                onSuccess(value)
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    func confirmUpgradeChecklist(onSuccess: @escaping() -> Void, onError: @escaping(PicPayErrorDisplayable) -> Void) {
        worker.confirmUpgradeChecklist { result in
            switch result {
            case .success:
                onSuccess()
            case .failure(let error):
                onError(error)
            }
        }
    }
}

extension UpgradeChecklistViewModel {
    enum OptiontStatus {
        case pending
        case checked
        case analysis
        case disabled
        case rejected
    }
}
