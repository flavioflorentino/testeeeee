import Foundation

final class IncomeRangeViewModel {
    private let worker: UpgradeAccountWorkerProtocol
    private var model: [IncomeRangeOption]
    private let selectedId: String
    
    init(selectedId: String = "", worker: UpgradeAccountWorkerProtocol = UpgradeAccountWorker()) {
        self.worker = worker
        self.selectedId = selectedId
        self.model = []
    }
    
    func title(index: Int) -> String {
        return model[index].text
    }
    
    func addressIsSelected(index: Int) -> Bool {
        return model[index].id == selectedId
    }
    
    func incomeRangeOptions(onSucess: @escaping() -> Void, onError: @escaping(PicPayErrorDisplayable) -> Void) {
        worker.incomeRangeOptions { [weak self] result in
            switch result {
            case .success(let value):
                self?.model = value
                onSucess()
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    func selectIncomeRange(id: String, onSuccess: @escaping(SetupChecklistInfo) -> Void, onError: @escaping(PicPayErrorDisplayable) -> Void) {
        worker.selectRangeOption(id) { result in
            switch result {
            case .success(let value):
                onSuccess(value)
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    func incomeRangeSelected(at indexPathRow: Int) -> IncomeRangeOption {
        return model[indexPathRow]
    }
    
    func numberOfRows() -> Int {
        return model.count
    }
}
