import Foundation

final class UpgradeChecklistLoadViewModel {
    private let worker: UpgradeAccountWorkerProtocol
    private var model: SetupChecklistInfo?
    
    init(worker: UpgradeAccountWorkerProtocol = UpgradeAccountWorker()) {
        self.worker = worker
    }
    
    func screenStatus() -> SetupChecklistInfo.ScreenStatus {
        return model?.screenStatus ?? .checklist
    }
    
    func checklistInfo() -> SetupChecklistInfo? {
        return model
    }
    
    func setupChecklistInfo(onSuccess: @escaping() -> Void, onError: @escaping(PicPayErrorDisplayable) -> Void) {
        worker.setupChecklistInfo { [weak self] result in
            switch result {
            case .success(let info):
                self?.model = info
                self?.confirmVerificationIdentity(onSuccess: onSuccess, onError: onError)
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    private func confirmVerificationIdentity(onSuccess: @escaping() -> Void, onError: @escaping(PicPayErrorDisplayable) -> Void) {
        worker.confirmVerificationIdentity { result in
            switch result {
            case .success:
                onSuccess()
            case .failure(let error):
                onError(error)
            }
        }
    }
}
