import Foundation

final class EditMotherNameViewModel {
    private let name: String
    private let worker: UpgradeAccountWorkerProtocol
    
    init(motherName: String = "", worker: UpgradeAccountWorkerProtocol = UpgradeAccountWorker()) {
        name = motherName
        self.worker = worker
    }
    
    func motherName() -> String {
        return name
    }
    
    func sanitizedMotherName(name: String) -> String {
        return name.replacingOccurrences(of: "[^a-zA-Z\u{00C0}-\u{00FF} ]", with: "", options: .regularExpression)
    }
    
    func updateMotherName(motherName: String, onSuccess: @escaping(SetupChecklistInfo) -> Void, onError: @escaping(Error) -> Void) {
        worker.updateMotherName(motherName: motherName) { result in
            switch result {
            case .success(let value):
                onSuccess(value)
            case .failure(let error):
                onError(error)
            }
        }
    }
}
