import UI

final class UpgradeCoordinator: Coordinator {
    var currentCoordinator: Coordinator?
    
    private let navigationController: UINavigationController
    private let rootNavigation = UpgradeNavigationController()
    private var viewModel: UpgradeChecklistViewModel?
    
    public var rootViewController: UINavigationController {
        return rootNavigation
    }
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        rootNavigation.setUpToUpgradeChecklist(enableLargeTitle: true)
    }
    
    func start() {
        let viewModel = UpgradeChecklistLoadViewModel()
        let vc = UpgradeChecklistLoad(with: viewModel)
        rootNavigation.viewControllers = [vc]
        rootNavigation.navigationBar.barTintColor = Palette.ppColorGrayscale000.color
        rootNavigation.view.backgroundColor = Palette.ppColorGrayscale000.color
        vc.delegate = self
        navigationController.present(rootNavigation, animated: true)
    }
    
    private func configure(largeTitle: Bool, viewController: UIViewController) {
        if #available(iOS 11.0, *) {
            viewController.navigationItem.largeTitleDisplayMode = largeTitle ? .automatic : .never
        }
    }
}

extension UpgradeCoordinator: UpgradeChecklistLoadDelegate {
    func open(screen: SetupChecklistInfo.ScreenStatus, model: SetupChecklistInfo?) {
        switch screen {
        case .checklist:
            guard let model = model else {
                return
            }
            let viewModel = UpgradeChecklistViewModel(model: model)
            self.viewModel = viewModel
            let viewController = UpgradeChecklistViewController(viewModel: viewModel)
            viewController.delegate = self
            rootNavigation.pushViewController(viewController, animated: false)
        case .approved:
            showUpgradeStatus(status: .success)
        case .pending:
            showUpgradeStatus(status: .pending)
        case .rejected:
            showUpgradeStatus(status: .failure)
        }
    }
    
    private func showUpgradeStatus(status: UpgradeStatusViewController.UpgradeStatus) {
        let viewController = UpgradeStatusViewController(status: status)
        configure(largeTitle: false, viewController: viewController)
        rootNavigation.pushViewController(viewController, animated: false)
    }
    
    func close() {
        navigationController.dismiss(animated: true)
    }
}

extension UpgradeCoordinator: UpgradeChecklistViewControllerDelegate {
    func didTapClose() {
        navigationController.dismiss(animated: true)
    }
    
    func didTapEditMother(name: String) {
        let vm = EditMotherNameViewModel(motherName: name)
        let vc = EditMotherNameViewController(with: vm)
        vc.delegate = self
        rootNavigation.pushViewController(vc, animated: true)
    }
    
    func didTapIncomeRange(id: String) {
        let vm = IncomeRangeViewModel(selectedId: id)
        let vc = IncomeRangeViewController(viewModel: vm)
        vc.delegate = self
        rootNavigation.pushViewController(vc, animated: true)
    }
    
    func completeSendUpgrade() {
        showUpgradeStatus(status: .pending)
    }
    
    func completeAddressList(upgrade: SetupChecklistInfo) {
        viewModel?.setChecklist(upgrade: upgrade)
        rootNavigation.popViewController(animated: true)
    }
    
    func didTapValidateIdentity() {
        let coordinator = IdentityValidationFlowCoordinator(from: rootNavigation, originFlow: .upgradeChecklist)
        coordinator.didBeginIdentityValidation = { [weak self] in
            self?.rootNavigation.popViewController(animated: false)
        }
        currentCoordinator = coordinator
        coordinator.start()
    }
}

extension UpgradeCoordinator: EditMotherNameViewControllerDelegate {
    func completeEditMother(upgrade: SetupChecklistInfo) {
        viewModel?.setChecklist(upgrade: upgrade)
        rootNavigation.popViewController(animated: true)
    }
}

extension UpgradeCoordinator: IncomeRangeViewControllerDelegate {
    func completeSelectIncomeRange(upgrade: SetupChecklistInfo) {
        viewModel?.setChecklist(upgrade: upgrade)
        rootNavigation.popViewController(animated: true)
    }
    
    func didTapHelp() {
        let viewController = IncomeRangeHelpViewController()
        configure(largeTitle: false, viewController: viewController)
        rootNavigation.pushViewController(viewController, animated: true)
    }
}
