final class PromoCodeWorker {
    static func validate(_ code: String, completion: @escaping (PicPayResult<PPPromoCode>) -> Void) {
        ConsumerApi().validatePromotionalCode(code) { (result) in
            completion(result)
            switch result {
            case .success(let promoCode):
                logAnalytics(promoCode)
            default: return
            }
        }
    }
    
    // TODO ...
    static func logAnalytics(_ code: PPPromoCode) {
        
    }
}
