import Core
import FeatureFlag
import PCI
import UIKit

final class P2MPaymentViewModel: NSObject {
    
    enum QRCodeDecodeResult {
        case p2m(P2MPaymentInfo)
        case linx(LinxPaymentInfo)
    }
    
    struct SupportedTypes {
        static let p2m = "P2M"
        static let linx = "LINX"
    }
    
    var paymentInfo: P2MPaymentInfo? {
        didSet {
            paymentManager.subtotal = NSDecimalNumber(value: paymentInfo?.value.doubleValue ?? 0.0)
            self.paymentManager.forceCreditCard = paymentInfo?.installments > 1
        }
    }
    var paymentManager: PPPaymentManager
    var p2mPaymentApi: P2MPaymentApi
    
    lazy var scannerApi: ScannerApi = {
        return ScannerApi()
    }()
    
    private let service: P2MServiceProtocol
    
    typealias Dependencies = HasFeatureManager & HasKeychainManager
    private let dependencies: Dependencies
    
    init(paymentInfo: P2MPaymentInfo? = nil,
         paymentManager: PPPaymentManager = PPPaymentManager(),
         p2mPaymentApi: P2MPaymentApi = P2MPaymentApi(),
         service: P2MServiceProtocol = P2MService(),
         dependencies: Dependencies = DependencyContainer()
    ) {
        self.paymentInfo = paymentInfo
        self.paymentManager = paymentManager
        self.paymentManager.subtotal = NSDecimalNumber(value: paymentInfo?.value.doubleValue ?? 0.0)
        self.p2mPaymentApi = p2mPaymentApi
        self.service = service
        self.dependencies = dependencies
        self.paymentManager.forceCreditCard = paymentInfo?.installments > 1
        
        super.init()
    }
    
    /// get information from qrcode
    ///
    /// - Parameters:
    ///   - hash: qrcode data
    ///   - completion: completion block
    func qrCodeDecode(text: String, completion: @escaping ((PicPayResult<QRCodeDecodeResult>)->Void)) {
        scannerApi.qrCodeDecode(text, type: "P2M") { (result) in
            DispatchQueue.main.async {
                switch result{
                case .success(let qrCodeInfo):
                    guard let data = qrCodeInfo.data else {
                        completion(PicPayResult.failure(PicPayError(message: DefaultLocalizable.anErrorHasOccurred.text)))
                        return
                    }
                    
                    if qrCodeInfo.type == SupportedTypes.p2m, let p2mPaymentInfo = P2MPaymentInfo(json: data) {
                        completion(PicPayResult.success(.p2m(p2mPaymentInfo)))
                        return
                    }
                    
                    if qrCodeInfo.type == SupportedTypes.linx, self.dependencies.featureManager.isActive(.featureP2mTefScannerBool), let linxPaymentInfo = P2MLinxModelAdapter.transformToLinxPaymentInfo(from: data) {
                        completion(PicPayResult.success(.linx(linxPaymentInfo)))
                        return
                    }
                    
                    completion(PicPayResult.failure(PicPayError(message: DefaultLocalizable.anErrorHasOccurred.text)))
                    
                case .failure(let error):
                    completion(PicPayResult.failure(error))
                }
            }
        }
    }
    
    /// Create a new P2M transaction
    ///
    /// - Parameters:
    ///   - pin: Password
    ///   - request: Transaction Data
    ///   - callback: callback for completion
    func createTransaction(
        pin: String,
        request: P2MPaymentApi.PaymentRequest,
        informedCvv: String?,
        _ callback: @escaping ((PicPayResult<P2MPaymentApi.CreateTransactionResponse>) -> Void)
    ) {
        if FeatureManager.isActive(.pciP2M) {
            createTransactionPCI(password: pin, request: request, informedCvv: informedCvv, completion: callback)
            return
        }
        
        p2mPaymentApi.createTransaction(pin: pin, request: request) { (result) in
            DispatchQueue.main.async {
                callback(result)
            }
        }
    }
    
    /// refound P2M transaction
    func refund(pin: String, _ callback: @escaping ((PicPayResult<P2MPaymentApi.RefundResponse>) -> Void) ) {
        guard let session = paymentInfo?.session else {
            return
        }
        p2mPaymentApi.refundTransaction(pin: pin, session: session) { (result) in
            DispatchQueue.main.async {
                callback(result)
            }
        }
    }
    
    private func saveCvv(informedCvv: String?) {
        guard let cvv = informedCvv else {
            return
        }
        
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        dependencies.keychain.set(key: KeychainKeyPF.cvv(id), value: cvv)
    }
    
    func checkNeedCvv() -> Bool {
        guard let cardBank = p2mPaymentApi.defauldCard, let cardValue = paymentManager.cardTotal()?.doubleValue else {
            return false
        }
        
        return p2mPaymentApi.pciCvvIsEnable(cardBank: cardBank, cardValue: cardValue)
    }
    
    private func createTransactionPCI(
        password: String,
        request: P2MPaymentApi.PaymentRequest,
        informedCvv: String?,
        completion: @escaping (PicPayResult<P2MPaymentApi.CreateTransactionResponse>) -> Void
    ) {
        guard let p2mPayload = createP2MPayload(request: request) else {
            let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            completion(PicPayResult.failure(error))
            return
        }
        let cvv = createCVVPayload(informedCvv: informedCvv)
        let payload = PaymentPayload<P2MPayload>(cvv: cvv, generic: p2mPayload)
        service.createTransaction(password: password, payload: payload, isNewArchitecture: false) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let value):
                    self?.saveCvv(informedCvv: informedCvv)
                    let response = P2MPaymentApi.CreateTransactionResponse(
                        transactionId: value.transactionId ?? "",
                        receiptWidgets: value.receiptWidgets
                    )
                    
                    completion(.success(response))
                    
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
               
            }
        }
    }
    
    private func createP2MPayload(request: P2MPaymentApi.PaymentRequest) -> P2MPayload? {
        guard let data = try? JSONSerialization.data(withJSONObject: request.toParams(), options: .prettyPrinted) else {
            return nil
        }
        return try? JSONDecoder.decode(data, to: P2MPayload.self)
    }
        
    private func createCVVPayload(informedCvv: String?) -> CVVPayload? {
        guard let cvv = informedCvv else {
            return createLocalStoreCVVPayload()
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func createLocalStoreCVVPayload() -> CVVPayload? {
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        guard
            paymentManager.cardTotal()?.doubleValue != .zero,
            let cvv = dependencies.keychain.getData(key: KeychainKeyPF.cvv(id))
            else {
                return nil
        }
        
        return CVVPayload(value: cvv)
    }
}
