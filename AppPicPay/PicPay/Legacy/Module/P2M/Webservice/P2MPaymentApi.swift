import Core
import FeatureFlag
import SwiftyJSON

final class P2MPaymentApi: BaseApi {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    /// Request object to create an cielo transaction
    class PaymentRequest: BaseApiRequest {
        // variables
        var creditCardId: String?
        var total: NSDecimalNumber?
        var credit: NSDecimalNumber?
        var parcelas: String?
        var someErrorOccurred: Bool?
        var biometry: Bool?
        var paymentPrivacyConfig: String?
        var origin: String?
        var session: [String: Any]?
        var paramsToBeMerged: [String: Any]?
        
        // All params are obligatory
        func obligatoryParams(params: [String: Any?]) throws -> [String: Any] {
            let nonNil = params.nilsRemoved
            
            if nonNil.keys.count < params.keys.count {
                throw PicPayError(message: "Missing params")
            }
            
            return nonNil
        }
        
        func mergeToParams(params: [String: Any]) {
            self.paramsToBeMerged = params
        }
        
        func toParams() throws -> [String: Any] {
            let params: [String: Any?] = [
                "origin": origin ?? "p2m",
                "consumer_credit_card_id": creditCardId ?? "0",
                "value": total != nil ? String(format: "%.02f", total!.floatValue) : "0",
                "credit": credit != nil ? String(format: "%.02f", credit!.floatValue) : "0",
                "parcelas": parcelas ?? "1",
                "feed_visibility": paymentPrivacyConfig ?? "2",
                "biometry": biometry,
                "duplicated": someErrorOccurred,
                "session": session ?? [:]
            ]
            
            return try obligatoryParams(params: params)
        }
    }
    
    /// Result for `EccomerceApi.createTransaction` call
    final class CreateTransactionResponse: BaseApiResponse {
        let transactionId: String
        let receiptWidgets: [ReceiptWidgetItem]
        
        init(transactionId: String, receiptWidgets: [[String: Any]]) {
            self.transactionId = transactionId
            self.receiptWidgets = receiptWidgets.compactMap { WSReceipt.createReceiptWidgetItem(jsonDict: $0) }
        }
        
        required init?(json: JSON) {
            guard let receipt = json["receipt"].array else {
                return nil
            }
            receiptWidgets = WSReceipt.createReceiptWidgetItem(receipt)            
            transactionId = json["transaction_id"].string ?? ""
        }
    }
    
    final class RefundResponse: BaseApiResponse {
        var alert: Alert?
        
        required init?(json: JSON) {
            self.alert = Alert(json: json["alert"])
        }
    }
    
    var defauldCard: CardBank? {
        return CreditCardManager.shared.defaultCreditCard
    }
    
    /// Create a new P2M transaction
    ///
    /// - Parameters:
    ///   - pin: password
    ///   - request: data for request
    ///   - completion: callback for completion
    func createTransaction(pin: String, request: PaymentRequest, _ completion: @escaping ((PicPayResult<CreateTransactionResponse>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            do {
                let params = try request.toParams()
                requestManager
                    .apiRequest(endpoint: kWsUrlP2MPayment,
                                method: .post,
                                parameters: params,
                                headers: ["X-Api-Version": "mobile"], pin: pin)
                    .responseApiObject(completionHandler: completion)
            } catch {
                guard let error = error as? PicPayError else {
                    return
                }
                completion(PicPayResult.failure(error))
            }
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    /// refund a P2M transaction
    ///
    /// - Parameters:
    ///   - pin: password
    ///   - request: data for request
    ///   - completion: callback for completion
    func refundTransaction(pin: String, session: [String: Any], _ completion: @escaping ((PicPayResult<RefundResponse>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let params = ["session": session]
            requestManager
                .apiRequest(endpoint: kWsUrlP2MPaymentRefund,
                            method: .post,
                            parameters: params,
                            headers: ["X-Api-Version": "mobile"],
                            pin: pin)
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool {
        return CvvRegisterFlowCoordinator.cvvFlowEnable(cardBank: cardBank, cardValue: cardValue, paymentType: .p2m)
    }
}
