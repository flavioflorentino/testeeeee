import FeatureFlag
import UIKit
import UI

final class P2MHelpers: NSObject {
    
    final class PPAuthBlockWrapper {
        fileprivate var auth: PPAuth?
        
        typealias AuthenticateActionBlock =  ((String, Bool) -> Void)
        typealias CanceledByUserBlock = (() -> Void)
        
        var authenticateAction: AuthenticateActionBlock?
        var canceledByUserBlockAction: CanceledByUserBlock?
        
        func authenticate(_ authenticateBlock: @escaping AuthenticateActionBlock, cancelHandler: @escaping CanceledByUserBlock) {
            self.authenticateAction = authenticateBlock
            self.canceledByUserBlockAction = cancelHandler
            
            auth = PPAuth.authenticate({ [weak self] (authToken, biometry) in
                self?.authenticateAction?(authToken ?? "", biometry)
                }, canceledByUserBlock: { [weak self] in
                    self?.canceledByUserBlockAction?()
            })
        }
    }
    
    lazy var auth: PPAuthBlockWrapper = {
        return PPAuthBlockWrapper()
    }()
    
    
    static private var shared: P2MHelpers = {
        return P2MHelpers()
    }()
    
    static func presentScanner(from controller: UIViewController) {
        let scanner = P2MScannerViewController.controllerFromStoryboard()
        controller.present(PPNavigationController(rootViewController: scanner), animated: true, completion: nil)
    }
    
    static func present(_ paymentInfo: P2MPaymentInfo, from controller: UIViewController, completion: ((_ showed: Bool)->Void)? = nil){
        P2MHelpers.show(paymentInfo, from: controller, isPresent: true, completion: completion)
    }
    
    static func presentP2MLinx(_ paymentInfo: LinxPaymentInfo, from controller: UIViewController, completion: ((_ showed: Bool)->Void)? = nil) {
        let viewController = P2MLinxFactory.make(paymentInfo: paymentInfo)
        viewController.view.backgroundColor = Palette.ppColorGrayscale000.color
        let navigation = PPNavigationController(rootViewController: viewController)
        navigation.modalPresentationStyle = .fullScreen
        controller.present(navigation, animated: true)
    }
    
    static func showP2MLinx(_ paymentInfo: LinxPaymentInfo, from controller: UIViewController) {
        let viewController = P2MLinxFactory.make(paymentInfo: paymentInfo)
        controller.navigationController?.pushViewController(viewController, animated: true)
    }
    
    static func push(_ paymentInfo: P2MPaymentInfo, from controller: UIViewController, completion: ((_ showed: Bool)->Void)? = nil){
        P2MHelpers.show(paymentInfo, from: controller, isPresent: false, completion: completion)
    }
    
    static func show(_ paymentInfo: P2MPaymentInfo, from controller: UIViewController, isPresent: Bool = true, completion: ((_ showed: Bool) -> Void)? = nil) {
        if paymentInfo.type == P2MPaymentInfo.P2MType.refund {
            if let alert = paymentInfo.alert {
                alert.dismissWithGesture = false
                alert.dismissOnTouchBackground = false
                
                AlertMessage.showAlert(alert, controller: nil) { (popup, model, button) in
                    if model.action == .refund {
                        button.startLoadingAnimating()
                        popup.disableAllButtons()
                        
                        P2MHelpers.makeRefund(paymentInfo, { result in
                            popup.enableAllButtons()
                            
                            switch result {
                            case .success(let response):
                                popup.dismiss(animated: true, completion: {
                                    completion?(false)
                                    if let alert = response.alert {
                                        AlertMessage.showAlert(alert, controller: nil)
                                    }
                                })
                            case .failure(let error):
                                popup.dismiss(animated: true, completion: {
                                    AlertMessage.showAlert(error, controller: controller, alertButton: { alert, _, _ in
                                        alert.dismiss(animated: true, completion: {
                                            completion?(false)
                                        })
                                    })
                                })
                            }
                        }, cancel: {
                            popup.enableAllButtons()
                            button.stopLoadingAnimating()
                        })
                    } else {
                        completion?(false)
                    }
                }
            }
        } else {
            guard let p2mController = createP2MPayment(paymentInfo: paymentInfo, isPresent: isPresent) else {
                return
            }
            if isPresent {
                controller.present(PPNavigationController(rootViewController: p2mController), animated: true, completion: {
                    completion?(true)
                })
            } else {
                controller.navigationController?.pushViewController(p2mController, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.35) {
                    completion?(true)
                }
            }
        }
    }
    
    static private func createP2MPayment(paymentInfo: P2MPaymentInfo, isPresent: Bool) -> UIViewController? {
        return FeatureManager.isActive(.newP2M) ? newP2MPayment(paymentInfo: paymentInfo, isPresent: isPresent) : legacyP2MPayment(paymentInfo: paymentInfo)
    }
    
    static private func newP2MPayment(paymentInfo: P2MPaymentInfo, isPresent: Bool) -> UIViewController? {
        let model = P2MPaymentOrchestratorModel(origin: .mainScanner, isModal: isPresent, session: paymentInfo.session)
        let paymentOrchestrator = P2MPaymentOrchestrator(paymentInfo: paymentInfo, orchestrator: model)
        
        return paymentOrchestrator.paymentViewController
    }
    
    static private func legacyP2MPayment(paymentInfo: P2MPaymentInfo) -> UIViewController {
        let viewController = P2MPaymentViewController.controlerFromStoryboard()
        viewController.model = P2MPaymentViewModel(paymentInfo: paymentInfo)
        
        return viewController
    }
    
    static func makeRefund(_ paymentInfo: P2MPaymentInfo, _ callback: @escaping ((PicPayResult<P2MPaymentApi.RefundResponse>) -> Void), cancel: @escaping ()->Void){
        
        P2MHelpers.shared.auth.authenticate({ (authToken, biometry) in
            let model = P2MPaymentViewModel(paymentInfo: paymentInfo)
            model.refund(pin: authToken, callback)
        }) {
            cancel()
        }
    }
}
