import AnalyticsModule
import CoreLegacy
import UI

final class P2MPaymentViewController: PPBaseViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func navigationBarIsTransparent() -> Bool {
        return true
    }
    
    // MARK: Outlets
    @IBOutlet weak var sellerImageView: UICircularImageView!
    @IBOutlet weak var sellerNameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var toolbarContainer: UIView!
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var backButton: UIButton!
    
    private var toolbarController: PaymentToolbarController?
    @objc public var model: P2MPaymentViewModel!
    
    private var loadingView: UIView?
    private var auth: PPAuth?
    private var privacyConfig: String?
    private var someErrorOccurred : Bool = false
    private var showedCreditCardPopup: Bool = false
    private var coordinator: Coordinating?
    
    @objc var origin: String?
    
    override var shouldAddBreadcrumb: Bool {
        return false
    }
    
    // MARK: - Factory
    @objc static func controlerFromStoryboard() -> P2MPaymentViewController {
        return P2MPaymentViewController.controllerFromStoryboard(.p2m)
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupContraints()
        
        if navigationController?.viewControllers.count > 1 {
            // TODO: Colocar botão de back
            backButton.setTitle("", for: .normal)
            backButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        }
        
        if model.paymentInfo?.needLoad == true {
            loadOrderInfo()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if model.paymentInfo?.installments > 1 && CreditCardManager.shared.cardList.count == 0 && !showedCreditCardPopup {
            let creditCardRequired = CreditCardRequiredViewController()
            creditCardRequired.text = P2MLocalizable.installmentPaymentsCreditCard.text
            creditCardRequired.creditCardDelegate = self
            showedCreditCardPopup = true
            self.showPopup(creditCardRequired)
            // to prenvent the missed credit card list
            CreditCardManager.shared.loadCardList({ (_) in })
        }
    }
    
    // MARK: - Setup Views
    
    fileprivate func setupViews() {
        // Setup payment toolbar
        toolbarController = PaymentToolbarController(paymentManager: model.paymentManager, parent: self, pay: { privacyConfig in  })
        toolbarController?.canUseBalance = !model.paymentManager.forceCreditCard
        toolbarController?.paymentMethodHeaderText = P2MLocalizable.installmentPaymentsCannot.text
        if let toolbar = toolbarController?.toolbar {
            toolbarContainer.addSubview(toolbar)
        }
        toolbarController?.payAction = { [weak self] privacyConfig in
            self?.privacyConfig = privacyConfig
            self?.pay()
        }
        
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = bannerView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        bannerView.addSubview(blurEffectView)
        
        updateData()
        
        if self.hasSafeAreaInsets {
            self.paintSafeAreaBottomInset(withColor: self.toolbarController?.bottomBarBackgroundColor ?? .clear)
        }
        
        bannerView.applyGradient(withColours: [UIColor(red: 86/255, green: 86/255, blue: 86/255, alpha: 1), Palette.ppColorGrayscale600.color], gradientOrientation: GradientOrientation.horizontal)
    }
    
    fileprivate func setupContraints() {
        toolbarController?.toolbar.snp.makeConstraints({ make in
            make.edges.equalToSuperview()
        })
    }
    
    func updateData() {
        // setup model data on view
        valueLabel.text = CurrencyFormatter.brazillianRealString(from: model.paymentManager.subtotal).replacingOccurrences(of: "R$", with: "")
        sellerNameLabel.text = model.paymentInfo?.sellerName ?? ""
        let url = URL(string: model.paymentInfo?.sellerImage ?? "")
        sellerImageView.setImage(url: url, placeholder: #imageLiteral(resourceName: "ilu_p2m_placeholder"))
        orderNumberLabel.text = "\(DefaultLocalizable.installment.text): \(model.paymentInfo?.installments ?? 1)x"
        
        if let alert = model.paymentInfo?.alert {
            AlertMessage.showAlert(alert, controller: self)
        }
    }
    
    // MARK: - Internal Method
    
    /// Load order
    func loadOrderInfo(){
        loadingView?.removeFromSuperview()
        loadingView = UILoadView(superview: view, position: .center)
    }
    
    func createLoadingView(_ title: String) {
        loadingView?.removeFromSuperview()
        loadingView = Loader.getLoadingView(title)
        view.addSubview(loadingView!)
        loadingView?.isHidden = false
    }
    
    // MARK: - User Actions
    
    @IBAction private func close(_ sender: Any?) {
        if self.presentingViewController != nil && navigationController?.viewControllers.count == 1 {
            dismiss(animated: true, completion: nil)
        }else{
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    /// Inti the pay proccess
    func pay(){
        auth = PPAuth.authenticate({ [weak self] (authToken, biometry) in
            self?.makeTransaction(withPin: authToken ?? "", biometry: biometry)
            }, canceledByUserBlock: {
                
        })
    }
    private func createCvvFlowCoordinator(completedCvv: @escaping (String) -> Void, completedNoCvv: @escaping () -> Void) {
        guard let navigationController = navigationController else {
            return
        }
        
        let coordinator = CvvRegisterFlowCoordinator(
            navigationController: navigationController,
            paymentType: .p2m,
            finishedCvv: completedCvv,
            finishedWithoutCvv: completedNoCvv
        )
        coordinator.start()
       
        self.coordinator = coordinator
    }
    
    func makeTransaction(withPin pin: String, biometry: Bool) {
        guard model.checkNeedCvv() else {
            createTransaction(withPin: pin, biometry: biometry)
            return
        }
        
        createCvvFlowCoordinator(completedCvv: { [weak self] cvv in
            self?.createTransaction(withPin: pin, biometry: biometry, informedCvv: cvv)
        }, completedNoCvv: { [weak self] in
            self?.showCvvError()
        })
    }
    
    /// Create a new transaction with the order data
    ///
    /// - Parameters:
    ///   - pin: password
    ///   - biometry: flag for biometry authentication
    func createTransaction(withPin pin: String, biometry: Bool, informedCvv: String? = nil) {
        createLoadingView(DefaultLocalizable.completingTransaction.text)
        
        // request data
        let request = P2MPaymentApi.PaymentRequest()
        request.creditCardId = "\(CreditCardManager.shared.defaultCreditCardId)"
        request.credit = model.paymentManager.balanceTotal()
        request.total = model.paymentManager.cardTotalWithoutInterest()
        request.parcelas = "\(model.paymentInfo?.installments ?? 1)"
        request.someErrorOccurred = someErrorOccurred
        request.paymentPrivacyConfig = privacyConfig
        request.biometry = biometry
        request.origin = origin
        request.session = model.paymentInfo?.session
        
        model.createTransaction(pin: pin, request: request, informedCvv: informedCvv) {[weak self] (result) in
            guard let self = self else { return }
            
            self.loadingView?.isHidden = true
            
            switch result {
            case .success(let transaction):
                // Notifies the app that a new payment has been made
                NotificationCenter.default.post(name: Notification.Name.Payment.new,
                                                object: nil,
                                                userInfo: ["type": "P2M"])
                
                Analytics.shared.log(PaymentEvent.transaction(.p2m,
                                                       id: transaction.transactionId,
                                                       value: request.total?.decimalValue ?? 0))
                
                if self.model.paymentInfo?.type == P2MPaymentInfo.P2MType.payment {
                    PPAnalytics.trackEvent("Transacao POS", properties: nil)
                }
                
                self.showReceipt(forTransaction: transaction.transactionId,
                                 withWidgets: transaction.receiptWidgets)
                
            case .failure(let error):
                self.someErrorOccurred = true
                self.auth?.handleError(error) { error in
                    AlertMessage.showAlert(error, controller: self, alertButton: { [weak self] (popup, button, _) in
                        popup.dismiss(animated: true) {
                            if button.action == .backToScanner {
                                self?.close(nil)
                            }
                        }
                    })
                }
            }
        }
    }
    
    ///  show the transaction receipt
    ///
    /// - Parameter withWidgets: Receipt Widgets
    func showReceipt(forTransaction id: String, withWidgets widgets: [ReceiptWidgetItem]){
        let receiptModel = ReceiptWidgetViewModel(transactionId: id, type: .PAV)
        receiptModel.setReceiptWidgets(items: widgets)
        
        TransactionReceipt.showReceiptSuccess(viewController: self, receiptViewModel: receiptModel)
    }
    
    private func showCvvError() {
        loadingView?.isHidden = true
        let error = PicPayError(message: DefaultLocalizable.cvvNotInformed.text)
        AlertMessage.showCustomAlertWithError(error, controller: self)
    }
}

// MARK: - CreditCardRequiredDelegate
extension P2MPaymentViewController: CreditCardRequiredDelegate {
    func openCreditCardForm() {
        guard let addCreditCardVC = ViewsManager.paymentMethodsStoryboardViewController(withIdentifier: "AddNewCreditCardViewController") as? AddNewCreditCardViewController else {
            return
        }
        let viewModel = AddNewCreditCardViewModel(origin: .payment)
        addCreditCardVC.viewModel = viewModel
        addCreditCardVC.hidesBottomBarWhenPushed = true
        addCreditCardVC.isCloseButton = true
        parent?.present(PPNavigationController(rootViewController:addCreditCardVC), animated: true, completion: nil)
    }
    
    func dismissPopup() {
        if model.paymentInfo?.installments > 1 || model.paymentManager.subtotal.doubleValue > model.paymentManager.balance().doubleValue {
            self.close(nil)
        }
    }
}

extension P2MPaymentViewController: AddNewCreditCardDelegate {    
    func creditCardDidInsert() {
        CreditCardManager.shared.loadCardList({ (_) in })
    }
}
