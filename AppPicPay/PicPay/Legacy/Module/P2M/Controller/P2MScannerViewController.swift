import FeatureFlag
import Foundation
import SnapKit
import UI
import ZXingObjC

final class P2MScannerViewController: PPBaseViewController {
    
    override func navigationBarIsTransparent() -> Bool {
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    // MARK: Outlets
    @IBOutlet weak var scannerView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var scanTarget: UIImageView!
    @IBOutlet weak var leftBg: UIView!
    @IBOutlet weak var topBg: UIView!
    @IBOutlet weak var bottomBg: UIView!
    @IBOutlet weak var rightBg: UIView!
    @IBOutlet weak var infoBottomBar: UIView!
    
    @IBOutlet weak var infoBottomBarTitleLabel: UILabel!
    @IBOutlet weak var infoBottomBarSubtitleLabel: UILabel!
    
    
    // MARK: Private vars
    private let infoBottomBarBackgroundColor = UIColor(red: 32/255.0, green: 198/255.0, blue: 99/255.0, alpha: 1.0)
    private var scanner:PPScanner?
    lazy var permissionView: AskForCameraPermissionView = {
        let view = AskForCameraPermissionView()
        view.cancelButton.isHidden = true
        return view
    }()
    lazy private var cameraPermission = {
        return PPPermission.camera(withDeniedAlert: PPPermission.Alert(
            title: DefaultLocalizable.authorizePicPayCamera.text,
            message: P2MLocalizable.scanPaymentCodes.text,
            settings: DefaultLocalizable.settings.text
        ))
    }()
    let loader = Loader.getLoadingView(DefaultLocalizable.wait.text)
    
    static func controllerFromStoryboard() -> P2MScannerViewController {
        return controllerFromStoryboard(.p2m)
    }
    
    let model = P2MPaymentViewModel()
    
    private lazy var newScannerView = NewScannerView(scannerType: .p2m)
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBottomTexts()
        setupViews()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scanner?.preventNewReads = false
        tryStartScanner()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        scanner?.stop()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    internal func initScanner() {
        if scanner == nil {
            // Scanner object
            scanner = PPScanner(origin: .p2m, view: scannerView, supportedTypes: [.qr, .dataMatrix], onRead: { [weak self] (text) in
                self?.onRead(scannedText: text)
            })
        }
    }
    
    func setupViews() {
        if FeatureManager.shared.isActive(.experimentNewAccessQrCode) {
            configureForNewScanner()
        }
        
        permissionView.action = { [weak self] (button) in
            guard let strongSelf = self else {
            return
        }
            PPPermission.request(permission: strongSelf.cameraPermission, { status in
                guard status == .authorized else {
            return
        }
                self?.tryStartScanner()
            })
        }
        
        permissionView.view.backgroundColor = Palette.black.color.withAlphaComponent(0.95)
        permissionView.descriptionLabel.textColor = Palette.white.color
        permissionView.isHidden = cameraPermission.status == .authorized
        
        scannerView.addSubview(permissionView)
        scannerView.bringSubviewToFront(backButton)
        permissionView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        let bgColor = Palette.black.color.withAlphaComponent(0.8)
        leftBg.backgroundColor = bgColor
        rightBg.backgroundColor = bgColor
        topBg.backgroundColor = bgColor
        bottomBg.backgroundColor = bgColor
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.showAditionalInformation))
        infoBottomBar.addGestureRecognizer(tap)
        
        if let loaderView = self.loader {
            self.view.addSubview(loaderView)
        }
        paintSafeAreaBottomInset(withColor: infoBottomBar.backgroundColor)
    }
    
    private func configureForNewScanner() {
        backButton.isHidden = true
        scanTarget.isHidden = true
        leftBg.isHidden = true
        topBg.isHidden = true
        bottomBg.isHidden = true
        rightBg.isHidden = true
        
        scannerView.addSubview(newScannerView)
        
        newScannerView.snp.makeConstraints {
            $0.edges.equalTo(scannerView)
        }
    }
    
    // MARK: Public setup
    func setup() {
    }
    
    // MARK: Scanner/permissions
    fileprivate func onRead(scannedText: String) {
        loader?.isHidden = false
        model.qrCodeDecode(text: scannedText) { [weak self] result in
            guard let strongSelf = self else {
            return
        }
            self?.loader?.isHidden = true
            switch result{
            case .success(let decodedResult):
                switch decodedResult {
                case .p2m(let paymentInfo):
                    P2MHelpers.push(paymentInfo, from: strongSelf) { [weak self] goToOtherController in
                        if !goToOtherController {
                            self?.scanner?.preventNewReads = false
                        }
                    }
                    
                case .linx(let paymentInfo):
                    P2MHelpers.showP2MLinx(paymentInfo, from: strongSelf)
                }
                
            case .failure(let error):
                AlertMessage.showAlert(error, controller: self, completion: {
                    self?.scanner?.preventNewReads = false
                })
            }
        }
    }
    
    fileprivate func removeRequestCameraView() {
        permissionView.isHidden = true
    }
    
    fileprivate func presentRequestCameraView() {
        permissionView.isHidden = false
    }
    
    fileprivate func setupBottomTexts(){
        if FeatureManager.isActive(.featureP2mScannerAcquirersEnabled){
            infoBottomBarTitleLabel.text = FeatureManager.text(.featureP2mScannerBottomTitle)
            infoBottomBarSubtitleLabel.text = FeatureManager.text(.featureP2mScannerBottomSubtitle)
            infoBottomBar.backgroundColor = infoBottomBarBackgroundColor
        }
    }
    
    internal func tryStartScanner() {
        switch cameraPermission.status {
        case .authorized:
            removeRequestCameraView()
            if scanner == nil {
                initScanner()
            }
            self.scanner?.start()
        default:
            presentRequestCameraView()
        }
    }
    
    @objc fileprivate func showAditionalInformation() {
        let endPoint = FeatureManager.isActive(.featureP2mScannerAcquirersEnabled) ?
            WsWebViewURL.aboutP2mAcquirers.endpoint : WsWebViewURL.aboutCielo.endpoint
        
        ViewsManager.pushWebViewController(
            withUrl: endPoint,
            fromViewController: self,
            title: DefaultLocalizable.p2mDevices.text,
            hideBottomBar: true
        )
    }
    
}

// MARK: Actions
extension P2MScannerViewController {
    @IBAction private func back(_ sender: Any) {
        navigationController?.dismiss(animated: true, completion: { [weak self] in
            self?.scanner?.prepareForDismiss()
        })
    }
}
