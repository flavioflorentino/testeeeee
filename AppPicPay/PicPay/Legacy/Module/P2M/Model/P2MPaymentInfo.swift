//
//  FixedValuePayment.swift
//  PicPay
//
//  ☝🏽☁️ Created by Luiz Henrique Guimarães on 12/08/18.
//

import Foundation
import SwiftyJSON

final class P2MPaymentInfo: BaseApiResponse {
    enum P2MType: String {
        case payment
        case refund
    }
    
    var needLoad: Bool = false
    var id: String = ""
    
    var type: P2MType = .payment
    
    var sellerImage: String?
    var sellerName: String = ""
    var value: NSNumber = NSNumber(value: 0.0)
    var installments: Int = 1
    var alert: Alert?
    var session: [String : Any]? = nil
    
    init() {
        
    }
    
    required init?(json: JSON) {
        id = json["_id"].string ?? ""
        sellerImage = json["seller_image"].string
        sellerName = json["seller_name"].string ?? ""
        installments = json["installments"].int ?? 1
        value = json["value"].number ?? NSNumber(value: 0)
        session = json["session"].dictionaryObject ?? nil
        type = P2MType(rawValue: json["type"].string ?? "") ?? P2MType.payment
        
        if  json["_ui"]["alert"].exists() {
            alert = Alert(json: json["_ui"]["alert"])
        }
    }
}

