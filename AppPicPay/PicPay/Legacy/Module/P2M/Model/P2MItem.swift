//
//  P2MItem.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 30/08/2018.
//

import UIKit
import SwiftyJSON

final class P2MItem: NSObject {
    
    enum ServiceType: String {
        case cielo = "cielo"
        case recommendation = "recommendation"
        case unknown = "unknown"
    }
    
    var name: String = ""
    var descriptionText: String = ""
    var imageUrl: String = ""
    var service: ServiceType = .unknown

    override init() {
        super.init()
    }

    init?(json: JSON){
        self.name = json["name"].string ?? ""
        self.descriptionText = json["description"].string ?? ""
        self.imageUrl = json["image_url"].string ?? ""
        self.service = ServiceType(rawValue: json["service"].string ?? "") ?? .unknown
    }
    
}
