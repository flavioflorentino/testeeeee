enum P2MLocalizable: String, Localizable {

    case installmentPaymentsCreditCard
    case installmentPaymentsCannot
    case scanPaymentCodes
    case paymentCanInstallments
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .p2mLegacy
    }
}
