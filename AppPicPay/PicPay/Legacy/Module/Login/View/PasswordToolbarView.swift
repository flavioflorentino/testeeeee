import UI
import UIKit

protocol PasswordToolbarViewDelegate: AnyObject {
    func didTapItem(index: Int)
}

final class PasswordToolbarView: UIView {
    private lazy var segmentedControl: UISegmentedControl = {
        let segmentedControl = UISegmentedControl()
        segmentedControl.tintColor = Palette.ppColorBranding300.color
        segmentedControl.addTarget(self, action: #selector(didTapItem(_:)), for: .valueChanged)
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        return segmentedControl
    }()
    
    var tabItens: [String] = [] {
        didSet {
            addTabItens()
        }
    }
    
    var selectIndex: Int = 0 {
        didSet {
            segmentedControl.selectedSegmentIndex = selectIndex
        }
    }
    
    weak var delegate: PasswordToolbarViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale100.color
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        addSubview(segmentedControl)
    }
    
    private func addTabItens() {
        for i in 0..<tabItens.count {
            let title = tabItens[i]
            segmentedControl.insertSegment(withTitle: title, at: i, animated: false)
        }
    }
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            segmentedControl.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            segmentedControl.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            segmentedControl.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5)
        ])
    }
    
    @objc
    private func didTapItem(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        delegate?.didTapItem(index: index)
    }
}
