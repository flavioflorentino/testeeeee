import Foundation
import Registration
import SecurityModule
import AnalyticsModule

enum LoginEvents: AnalyticsKeyProtocol {
    case userSignedIn
    case userSignedOut

    var name: String {
        switch self {
        case .userSignedIn:
            return "User Signed In"
        case .userSignedOut:
            return "User Signed Out"
        }
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, providers: [.eventTracker])
    }
}

final class LoginViewModel {
    private let worker: LoginPicpayWorker
    private let logDetectionService: LogDetectionServicing
    private let analytics: AnalyticsProtocol
    
    init(worker: LoginPicpayWorker = LoginPicpayService(),
         logDetectionService: LogDetectionServicing = LogDetectionService(),
         analytics: AnalyticsProtocol = Analytics.shared) {
        self.worker = worker
        self.logDetectionService = logDetectionService
        self.analytics = analytics
    }
    
    func loginUser(login: String,
                   password: String,
                   captchaCode: String? = nil,
                   onSuccess: @escaping (NextScreen) -> Void,
                   onError: @escaping (ErrorType, PicPayErrorDisplayable) -> Void) {
        worker.loginUser(
            login: login,
            password: password,
            anonymousId: PPAnalytics.distinctId() ?? "",
            captchaCode: captchaCode, onSuccess: { [weak self] in
                guard
                    let localConsumer = self?.worker.getConsumer(),
                    let isIncompleteAccount = self?.worker.isIncompleteAccount() else {
                    let picpayError = PicPayError(message: DefaultLocalizable.unexpectedError.text)
                    onError(.none, picpayError)
                    return
                }
                ThirdParties().identify()
                RegistrationCacheHelper().cleanCachedData()

                self?.sendLogDetection()
                self?.analytics.log(LoginEvents.userSignedIn)


                if isIncompleteAccount {
                    onSuccess(.personalData)
                } else {
                    if (localConsumer.verifiedPhoneNumber != nil && !localConsumer.verifiedPhoneNumber.isEmpty) || localConsumer.bypass_sms {
                        onSuccess(.home)
                    }
            }
        }, onError: { [weak self] error in
            if  error.code == 1007 || error.picpayCode == "1007" {
                onError(.reactivateAccount, error)
            } else if  error.code == 10584 || error.picpayCode == "10584" {
                onError(.tfa, error)
            } else if error.code == 2674 || error.picpayCode == "2674" {
                onError(.captcha, error)
            } else if error.code == 4015 || error.picpayCode == "4015" {
                self?.loginUserWithRestriction(
                    login: login,
                    password: password,
                    onSuccess: onSuccess,
                    onError: onError
                )
            } else {
                onError(.none, error)
            }
        })
    }
    
    private func loginUserWithRestriction(
        login: String,
        password: String,
        onSuccess: @escaping (NextScreen) -> Void,
        onError: @escaping (ErrorType, PicPayErrorDisplayable) -> Void
    ) {
        worker.loginUserWithRestriction(login: login, password: password) { result in
            switch result {
            case .success(let model):
                onSuccess(.registrationCompliance(result: model.step, consumer: model.consumer))
            case .failure(let error):
               onError(.none, error)
            }
        }
    }
    
    func reactivateAccount(login: String, password: String, onSuccess: @escaping() -> Void, onError: @escaping(Error) -> Void) {
        worker.reactivateAccount(login: login, password: password, onSuccess: onSuccess, onError: onError)
    }
    
    func fieldsAreValid(login: String, password: String) -> (login: Bool, password: Bool) {        
        return (login: !login.isEmpty, password: !password.isEmpty)
    }
}

extension LoginViewModel {
    enum NextScreen: Equatable {
        case personalData
        case home
        case registrationCompliance(result: RegistrationComplianceResult, consumer: RegistrationComplianceConsumer?)
        
        static func == (lhs: LoginViewModel.NextScreen, rhs: LoginViewModel.NextScreen) -> Bool {
            switch (lhs, rhs) {
            case (.personalData, .personalData),
                 (.home, .home),
                 (.registrationCompliance, .registrationCompliance):
                return true
            default:
                return false
            }
        }
    }
    
    enum ErrorType {
        case reactivateAccount
        case tfa
        case captcha
        case none
    }
}

extension LoginViewModel {
    func sendLogDetection() {
        logDetectionService.sendLog()
    }
}
