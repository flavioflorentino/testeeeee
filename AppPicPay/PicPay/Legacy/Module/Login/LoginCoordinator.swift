import FeatureFlag
import Foundation
import PasswordReset
import Registration

final class LoginCoordinator: NSObject, Coordinator {
    var currentCoordinator: Coordinator?
    var tfaCoordinator: TFAFlowCoordinator?
    var passwordResetCoordinator: PasswordResetCoordinator?
    
    private let navigationController: UINavigationController
    
    public var rootNavigationController: UINavigationController {
        return navigationController
    }
    
    @objc
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    @objc
    func start() {
        let viewModel = LoginViewModel()
        let viewController = LoginViewController(viewModel: viewModel)
        viewController.delegate = self
        navigationController.pushViewController(viewController, animated: true)
    }
}

extension LoginCoordinator: LoginViewControllerDelegate {
    func forgotPassword() {
        guard let viewController = navigationController.visibleViewController else {
            return
        }
        
        passwordResetCoordinator = PasswordResetCoordinator(presenterController: viewController)
        passwordResetCoordinator?.start()
    }
    
    func homeApp() {
        guard let viewController = navigationController.visibleViewController else {
            return
        }
        PaginationCoordinator.destroy()
        AppManager.shared.goToApp(fromContext: viewController)
    }
    
    func personalData() {
        let personalDetailViewController = PersonalDetailsFactory.make()
        navigationController.pushViewController(personalDetailViewController, animated: true)
        
        guard let firstViewControllerOnStack = navigationController.viewControllers.first else {
            return
        }
        
        let newViewControllers = [firstViewControllerOnStack, personalDetailViewController]
        navigationController.viewControllers = newViewControllers
    }
    
    func tfa(id: String, completion: @escaping() -> Void) {
        tfaCoordinator = TFAFlowCoordinator(id: id, presenter: navigationController) { complete in
            if complete {
                completion()
            }
        }
        
        tfaCoordinator?.start()
    }
    
    func captcha(onSuccess: @escaping(String) -> Void, onError: @escaping(PicPayErrorDisplayable) -> Void) {
        let captchaVC = CaptchaViewController()
        captchaVC.captchaCallback = { result in
            switch result {
            case .success(let token):
                onSuccess(token)
            case .failure(let error):
                onError(error)
            }
        }
        
        let nav = UINavigationController(rootViewController: captchaVC)
        navigationController.present(nav, animated: true, completion: nil)
    }
    
    func registrationCompliance(step: RegistrationComplianceResult, consumer: RegistrationComplianceConsumer?) {
        let coordinator = RegistrationComplianceFlowCoordinator(with: navigationController)
        guard (step == .inconsistency || step == .biometry), let consumer = consumer else {
            coordinator.startCompliance(status: RegistrationComplianceStatus.status(with: step))
            return
        }
        
        coordinator.startReview(model: RegistrationCompReviewModel(
            name: consumer.name,
            birthdate: consumer.birthdate,
            cpf: consumer.cpf)
        )
    }
}
