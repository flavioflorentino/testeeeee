import AnalyticsModule
import UI
import SwiftyJSON
import Registration
import UIKit
import FeatureFlag

protocol LoginViewControllerDelegate: AnyObject {
    func forgotPassword()
    func homeApp()
    func personalData()
    func tfa(id: String, completion: @escaping() -> Void)
    func captcha(onSuccess: @escaping(String) -> Void, onError: @escaping(PicPayErrorDisplayable) -> Void)
    func registrationCompliance(step: RegistrationComplianceResult, consumer: RegistrationComplianceConsumer?)
}

final class LoginViewController: PPBaseViewController {
    private enum Layout {
        static let loginPasswordBackgroundColor: UIColor = Palette.black.color.withAlphaComponent(0.2).withCustomDarkColor(Palette.ppColorGrayscale000.color)
        static let loginPasswordTextColor: UIColor = Palette.white.color
        static let loginPasswordPlaceholderColor: UIColor = Palette.white.color.withAlphaComponent(0.5)
        static let loginButtonTitleColor: UIColor = Palette.ppColorGrayscale600.color
        static let loginButtonBackgroundColor: UIColor = Palette.ppColorGrayscale000.color(withCustomDark: .ppColorBranding200)
    }
    
    enum AccessibilityIdentifier: String {
        case loginTextField
        case passwordTextField
        case loginButton
        case forgotPasswordButton
    }
    
    private var keyboardShow: Bool = false
    private let viewModel: LoginViewModel
    
    weak var delegate: LoginViewControllerDelegate?
    
    private lazy var passwordToolbarView: PasswordToolbarView = {
        let passwordToolbarView = PasswordToolbarView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 45))
        passwordToolbarView.tabItens = [LoginLocalizable.passwordNumber.text, LoginLocalizable.passwordText.text]
        passwordToolbarView.selectIndex = 0
        passwordToolbarView.delegate = self
        return passwordToolbarView
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBOutlet weak var loginTextField: UIRoundedTextField!
    @IBOutlet weak var passwordTextField: UIRoundedTextField!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var loginButton: UIPPButton! {
        didSet {
            loginButton.setTitle(LoginLocalizable.loginButtonTitle.text, for: .normal)
            loginButton.setTitle("", for: .disabled)
        }
    }
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var loginTopConstraint: NSLayoutConstraint!
    
    init(viewModel with: LoginViewModel) {
        viewModel = with
        super.init(nibName: LoginViewController.nibName, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addObservers()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction private func login(_ sender: AnyObject) {
        let login = loginTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        let fields = viewModel.fieldsAreValid(login: login, password: password)
        
        deemphasizeTextField(loginTextField)
        deemphasizeTextField(passwordTextField)
        
        if !fields.login {
            emphasizeTextField(loginTextField)
        } else if !fields.password {
            emphasizeTextField(passwordTextField)
        } else {
            loginUser()
        }
    }
    
    @IBAction private func forgotPassword(_ sender: AnyObject?) {
        delegate?.forgotPassword()
    }
    
    private func setupView() {
        title = LoginLocalizable.title.text
        messageLabel.text = ""
        passwordTextField.inputAccessoryView = passwordToolbarView
        setupColors()
        
        if !UIAccessibility.isVoiceOverRunning {
            loginTextField.becomeFirstResponder()
        }
    }
    
    private func setupColors() {
        view.backgroundColor = Palette.ppColorPositive300.color(withCustomDark: .ppColorGrayscale600)
        
        setupNavBarColor()
        setupLoginTextFieldColor()
        setupPasswordTextFieldColor()
        setupLoginButtonColor()
        setupForgotPasswordButton()
    }
    
    private func setupNavBarColor() {
        navigationController?.navigationBar.barTintColor = Palette.ppColorPositive300.color(withCustomDark: .ppColorGrayscale600)
        navigationController?.navigationBar.tintColor = Palette.white.color
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: Palette.white.color]
    }
    
    private func setupLoginTextFieldColor() {
        loginTextField.layer.borderWidth = 0
        loginTextField.backgroundColor = Layout.loginPasswordBackgroundColor
        loginTextField.textColor = Layout.loginPasswordTextColor
        loginTextField.placeHolderTextColor = Layout.loginPasswordPlaceholderColor
        loginTextField.accessibilityIdentifier = AccessibilityIdentifier.loginTextField.rawValue
    }
    
    private func setupPasswordTextFieldColor() {
        passwordTextField.layer.borderWidth = 0
        passwordTextField.backgroundColor = Layout.loginPasswordBackgroundColor
        passwordTextField.textColor = Layout.loginPasswordTextColor
        passwordTextField.placeHolderTextColor = Layout.loginPasswordPlaceholderColor
        passwordTextField.accessibilityIdentifier = AccessibilityIdentifier.passwordTextField.rawValue
    }
    
    private func setupLoginButtonColor() {
        loginButton.setTitleColor(Layout.loginButtonTitleColor)
        loginButton.setBackgroundColor(Layout.loginButtonBackgroundColor)
        loginButton.accessibilityIdentifier = AccessibilityIdentifier.loginButton.rawValue
    }
    
    private func setupForgotPasswordButton() {
        forgotPasswordButton.accessibilityIdentifier = AccessibilityIdentifier.forgotPasswordButton.rawValue
    }
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    // MARK: - User Actions
    
    private func emphasizeTextField(_ textField: UITextField) {
        textField.layer.borderColor = Palette.ppColorGrayscale000.cgColor
        textField.layer.borderWidth = 1
        loginView.shakeView()
        HapticFeedback.notificationFeedbackError()
    }
    
    private func deemphasizeTextField(_ textField: UITextField) {
        textField.layer.borderWidth = 0
    }
        
    private func loginUser(captchaCode: String? = nil) {
        let login = loginTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        startLoading()
        viewModel.loginUser(login: login, password: password, captchaCode: captchaCode, onSuccess: { [weak self] screen in
            HapticFeedback.notificationFeedbackSuccess()
            self?.stopLoading()
            self?.successScreen(screen)
        }, onError: { [weak self] type, error in
            HapticFeedback.notificationFeedbackError()
            self?.stopLoading()
            
            switch type {
            case .reactivateAccount:
                self?.showReactivateAccountPopup(login: login, password: password, error: error)
            case .tfa:
                guard let id = (error.data as? JSON)?["id"].string else {
                    return
                }
                self?.showTFA(id: id)
            case .captcha:
                self?.showCaptcha()
            case .none:
                AlertMessage.showCustomAlertWithError(error, controller: self, completion: nil)
            }
        })
    }
    
    private func successScreen(_ screen: LoginViewModel.NextScreen) {
        switch screen {
        case .home:
            delegate?.homeApp()
        case .personalData:
            delegate?.personalData()
        case let .registrationCompliance(step, consumer):
            delegate?.registrationCompliance(step: step, consumer: consumer)
        }
    }
    
    private func showTFA(id: String) {
        delegate?.tfa(id: id) { [weak self] in
            self?.loginUser()
        }
    }
    
    private func showReactivateAccountPopup(login: String, password: String, error: PicPayErrorDisplayable) {
        let alert = Alert(title: nil, text: error.localizedDescription)
        let reactivateAccountButton = Button(title: LoginLocalizable.reactivateAccount.text, type: .cta) { [weak self] popupController, buttonView in
            buttonView.startLoadingAnimating()
            self?.reactivateAccount(login: login, password: password, alertPopup: popupController)
        }
        let notNowButton = Button(title: DefaultLocalizable.notNow.text, type: .clean, action: .close)
        alert.buttons = [reactivateAccountButton, notNowButton]
        AlertMessage.showAlert(alert, controller: self)
    }
    
    private func reactivateAccount(login: String, password: String, alertPopup: AlertPopupViewController) {
        viewModel.reactivateAccount(login: login, password: password, onSuccess: {}, onError: { error in
            alertPopup.dismiss(animated: true) {
                AlertMessage.showCustomAlertWithError(error, controller: self, completion: nil)
            }
        })
    }
    
    private func showCaptcha() {
        delegate?.captcha(onSuccess: { [weak self] token in
            self?.loginUser(captchaCode: token)
        }, onError: { error in
            AlertMessage.showCustomAlertWithError(error, controller: self)
        })
    }
    
    private func startLoading() {
        loginTextField.isEnabled = false
        passwordTextField.isEnabled = false
        forgotPasswordButton.isEnabled = false
        loginButton.startLoadingAnimating(style: .gray)
        
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.loginTextField.layer.opacity = 0.5
            self?.passwordTextField.layer.opacity = 0.5
        }
    }
    
    private func stopLoading() {
        loginTextField.isEnabled = true
        passwordTextField.isEnabled = true
        forgotPasswordButton.isEnabled = true
        loginButton.stopLoadingAnimating()
        
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.loginTextField.layer.opacity = 1.0
            self?.passwordTextField.layer.opacity = 1.0
        }
    }
    
    // MARK: - Keyboard Events Handler
    
    @objc
    private func keyboardWillShow(notification: NSNotification) {
        guard let offset = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            !keyboardShow else { return }
        let height: CGFloat = ((view.frame.height - offset.height) / 2) - loginView.frame.height / 2
        loginTopConstraint.constant = height
        
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutIfNeeded()
        }
        
        keyboardShow = true
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == loginTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            login(passwordTextField)
        }
        
        return true
    }
}

extension LoginViewController: PasswordToolbarViewDelegate {
    func didTapItem(index: Int) {
        let isPasswordNumericKeyboard = index == 0 ? true : false
        passwordTextField.resignFirstResponder()
        passwordTextField.keyboardType = (isPasswordNumericKeyboard ? .numberPad : .default)
        passwordTextField.becomeFirstResponder()
    }
}
