import Foundation

enum LoginLocalizable: String, Localizable {
    // Login
    case title
    case reactivateAccount
    case passwordNumber
    case passwordText
    case forgotPasswordTitle
    case loginButtonTitle
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .login
    }
}
