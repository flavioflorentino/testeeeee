import Foundation

enum CaptchaLocalizable: String, Localizable {
    case title
    case verifyCaptcha
    case captchaErrorVerify
    case captchaError
    case captchaErrorExpired
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .captcha
    }
}
