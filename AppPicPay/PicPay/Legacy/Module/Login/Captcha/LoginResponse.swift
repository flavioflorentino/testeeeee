import Foundation
import SwiftyJSON

final class LoginResponse: BaseApiResponse {
    let token: String
    
    required init?(json: JSON) {
        guard let token = json["token"].string else {
            return nil
        }
        self.token = token
    }
}
