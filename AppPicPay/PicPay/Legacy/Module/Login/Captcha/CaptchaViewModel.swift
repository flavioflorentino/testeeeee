import WebKit

final class CaptchaViewModel {
    func getRelativeURL() -> URL? {
        return URL(string: "http://picpay.com")
    }
    
    func getCaptchaHtmlFilePath() -> String? {
        if let path = Bundle.main.path(forResource: "recaptcha", ofType: "html") {
            return path
        }
        Log.error(.model, "Error: recaptcha.html not found")
        return nil
    }
    
    func getStringContentFromFile(at path: String?) -> String? {
        guard let filePath = path else {
            return nil
        }

        do {
            let content = try String(contentsOfFile: filePath)
            return content
        } catch {
            Log.error(.model, "Error parsing recaptcha.html")
        }
        return nil
    }
    
    func handleWebViewMessage(message: WKScriptMessage, captchaCallback: ((PicPayResult<String>) -> Void)?) {
        switch message.name {
        case "success":
            if let token = message.body as? String {
                captchaCallback?(PicPayResult.success(token))
            } else {
                captchaCallback?(PicPayResult.failure(PicPayError(message: CaptchaLocalizable.captchaErrorVerify.text)))
            }
        case "error":
            captchaCallback?(PicPayResult.failure(PicPayError(message: CaptchaLocalizable.captchaError.text)))
    
        case "expired":
            captchaCallback?(PicPayResult.failure(PicPayError(message: CaptchaLocalizable.captchaErrorExpired.text)))
        default:
            break
        }
    }
}
