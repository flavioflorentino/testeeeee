import UI
import UIKit
import WebKit

final class CaptchaViewController: PPBaseViewController {
    private let viewModel = CaptchaViewModel()
    
    var captchaCallback: ((PicPayResult<String>) -> Void)?
    
    // The observer for \WKWebView.estimatedProgress
    private var loadingObservation: NSKeyValueObservation?
    
    // Indicates if the script has already been loaded by the webView
    private var didFinishLoadingPage = false {
        didSet {
            guard didFinishLoadingPage else {
                return
            }
            stopLoadingView()
        }
    }
    
    private lazy var webView: WKWebView = {
        let webView = WKWebView(frame: view.frame, configuration: webViewConfiguration)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.accessibilityIdentifier = "webview"
        webView.scrollView.isScrollEnabled = true
        webView.scrollView.bounces = false
        webView.scrollView.panGestureRecognizer.isEnabled = false
        webView.allowsBackForwardNavigationGestures = false
        webView.scrollView.delegate = self
        webView.uiDelegate = self
        webView.contentMode = .scaleToFill
        webView.isOpaque = false
        webView.backgroundColor = UIColor.clear
        webView.scrollView.backgroundColor = UIColor.clear
        
        loadingObservation = webView.observe(\.estimatedProgress, options: [.new]) { [weak self] _, progress in
            self?.didFinishLoadingPage = progress.newValue == 1.0
        }
        return webView
    }()
    
    private lazy var webViewConfiguration: WKWebViewConfiguration = {
        let controller = WKUserContentController()
        controller.add(self, name: "success")
        controller.add(self, name: "error")
        controller.add(self, name: "expired")
        let conf = WKWebViewConfiguration()
        conf.userContentController = controller
        return conf
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        
        configureLoadingView(text: CaptchaLocalizable.verifyCaptcha.text, textColor: .white, activityIndicatorStyle: .white, backgroundColor: .clear)
        startLoadingView()
        
        let filePath = viewModel.getCaptchaHtmlFilePath()
        if let content = viewModel.getStringContentFromFile(at: filePath) {
            webView.loadHTMLString(content, baseURL: viewModel.getRelativeURL())
        }
    }
    
    private func setup() {
        title = CaptchaLocalizable.title.text
        view.backgroundColor = Palette.ppColorBranding300.color
        navigationController?.navigationBar.barTintColor = Palette.ppColorBranding300.color
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: Palette.ppColorGrayscale000.color]
        let cancelBarButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(close))
        cancelBarButton.tintColor = Palette.ppColorGrayscale000.color
        navigationItem.leftBarButtonItem = cancelBarButton
        
        view.addSubview(webView)
        NSLayoutConstraint.activate([
            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            webView.topAnchor.constraint(equalTo: view.topAnchor),
            webView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            webView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }

    @objc
    private func close() {
      dismiss(animated: true, completion: nil)
    }
}

extension CaptchaViewController: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        dismiss(animated: true, completion: { [weak self] () -> Void in
            self?.viewModel.handleWebViewMessage(message: message, captchaCallback: self?.captchaCallback)
        })
    }
}

// Delegate used to disable zoom
extension CaptchaViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        webView.scrollView.pinchGestureRecognizer?.isEnabled = false
    }
}

// Delegate used to allow links to be opened on Safari
extension CaptchaViewController: WKUIDelegate {
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil {
            guard let url = navigationAction.request.url, UIApplication.shared.canOpenURL(url) else {
     return nil
}
            UIApplication.shared.open(url, options: [:])
        }
        return nil
    }
}
