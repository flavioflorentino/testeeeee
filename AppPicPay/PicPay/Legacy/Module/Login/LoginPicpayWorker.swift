import Core
import FeatureFlag
import Foundation
import Registration

protocol LoginPicpayWorker {
    func isIncompleteAccount() -> Bool
    func getConsumer() -> MBConsumer?
    func loginUser(login: String, password: String, anonymousId: String, captchaCode: String?, onSuccess: @escaping() -> Void, onError: @escaping(PicPayError) -> Void)
    func reactivateAccount(login: String, password: String, onSuccess: @escaping() -> Void, onError: @escaping(Error) -> Void)
    func loginUserWithRestriction(login: String, password: String, completion: @escaping(Result<RegistrationComplianceLoginModel, PicPayError>) -> Void)
}

final class LoginPicpayService: BaseApi, LoginPicpayWorker {
    typealias Dependencies = HasFeatureManager & HasMainQueue
    private let dependencies: Dependencies
    private let registrationLoginHelper: RegistrationComplianceLoginHelperContract
    
    init(
        with dependencies: Dependencies = DependencyContainer(),
        registrationLoginHelper: RegistrationComplianceLoginHelperContract = RegistrationComplianceLoginHelperFactory.make()) {
        self.dependencies = dependencies
        self.registrationLoginHelper = registrationLoginHelper
    }
    
    func isIncompleteAccount() -> Bool {
        return ConsumerManager.shared.isIncompleteAccount
    }
    
    func getConsumer() -> MBConsumer? {
        return ConsumerManager.shared.consumer
    }
    
    func loginUser(login: String, password: String, anonymousId: String, captchaCode: String? = nil, onSuccess: @escaping() -> Void, onError: @escaping(PicPayError) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            var params = [
                "login": login,
                "password": password,
                "distinct_id": anonymousId
            ]
            if let token = captchaCode {
                params["captcha_code"] = token
            }
            
            requestManager
                .apiRequest(endpoint: kWsUrlUserLogin, method: .post, parameters: params)
                .responseApiObject { [weak self] (result: PicPayResult<LoginResponse>) in
                    self?.dependencies.mainQueue.async {
                        switch result {
                        case .success(let data):
                            User.updateToken(data.token)
                            self?.getConsumer(password: password, onSuccess: onSuccess, onError: onError)
                        case .failure(let error):
                            onError(error)
                        }
                    }
                }
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func loginUserWithRestriction(
        login: String,
        password: String,
        completion: @escaping(Result<RegistrationComplianceLoginModel, PicPayError>) -> Void) {
        
        registrationLoginHelper.loginUserWithRestriction(
            login: login,
            password: password) { result in
                switch result {
                case .success(let model):
                    User.updateToken(model.token.code)
                    completion(.success(model))
                case .failure(let error):
                    let picpayError = PicPayError(message: error.localizedDescription)
                    completion(.failure(picpayError))
                }
        }
    }
    
    func reactivateAccount(login: String, password: String, onSuccess: @escaping() -> Void, onError: @escaping(Error) -> Void) {
        WSConsumer.reactivate(login, password: password) { [weak self] _, error in
            self?.dependencies.mainQueue.async {
                if let error = error {
                    onError(error)
                } else {
                    onSuccess()
                }
            }
        }
    }
    
    private func getConsumer(password: String, onSuccess: @escaping() -> Void, onError: @escaping(PicPayError) -> Void) {
        ConsumerManager.shared.loadConsumerDataCached(useCache: false) { [weak self] _, error in
            self?.dependencies.mainQueue.async {
                Contacts().identifyPicpayContacts()
                if let error = error {
                    let picpayError = PicPayError(message: error.localizedDescription, code: "100000")
                    onError(picpayError)
                } else {
                    PPAuth.clearAuthenticationData()
                    PPAuthSwift.setTemporaryAuthenticationToken(password, status: PPAuthTempTokenStatusLogin)
                    ConsumerApi().updateNotificationToken()
                    BreadcrumbManager.shared.clear()
                    onSuccess()
                }
            }
        }
        
        CreditCardManager.shared.loadCardList({ _ in })
    }
}
