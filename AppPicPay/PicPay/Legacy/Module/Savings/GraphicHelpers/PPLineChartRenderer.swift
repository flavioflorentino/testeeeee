//
//  PPLineChartRenderer.swift
//  PicPay
//
//  Created by PicPay-DEV-PP on 26/11/18.
//

import Charts
import Foundation
import UI

final class PPLineChartRenderer: LineChartRenderer {
    var lineChartDataSets: [LineChartDataSet] = []
    
    override func drawHighlightLines(context: CGContext, point: CGPoint, set: ILineScatterCandleRadarChartDataSet) {
        context.beginPath()
        context.move(to: CGPoint(x: point.x, y: viewPortHandler.contentTop))
        let width = context.boundingBoxOfClipPath.width / CGFloat(7)
        let halfWidth = width / 2

        let bezier = UIBezierPath(roundedRect: CGRect(x: point.x - halfWidth, y: viewPortHandler.contentTop, width: width, height: (viewPortHandler.contentBottom - viewPortHandler.contentTop)), cornerRadius: 4)
        bezier.addClip()
        
        // Drawing code
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let startColorComponents = Palette.white.color.withAlphaComponent(0).rgba
        let endColorComponents = Palette.white.color.withAlphaComponent(0.33).rgba
        
        if let ctx = UIGraphicsGetCurrentContext() {
            let colorComponents = [startColorComponents.red, startColorComponents.green, startColorComponents.blue, startColorComponents.alpha, endColorComponents.red, endColorComponents.green, endColorComponents.blue, endColorComponents.alpha]
            let locations: [CGFloat] = [0.0, 1.0]
            
            guard let gradient = CGGradient(colorSpace: colorSpace, colorComponents: colorComponents, locations: locations, count: 2) else {
                return
            }
            
            let startPoint = CGPoint(x: point.x, y: viewPortHandler.contentTop)
            let endPoint = CGPoint(x: point.x, y: viewPortHandler.contentBottom)
            ctx.drawLinearGradient(gradient, start: startPoint, end: endPoint, options: .drawsBeforeStartLocation)
            context.addPath(bezier.cgPath)
        }
    }
}
