//
//  PPXAxisRenderer.swift
//  PicPay
//
//  Created by PicPay-DEV-PP on 26/11/18.
//

import Charts
import Foundation
import UI

final class PPXAxisRenderer: XAxisRenderer {
    var highlighted: Highlight?
    
    override func drawGridLine(context: CGContext, x: CGFloat, y: CGFloat) {
        if x >= viewPortHandler.offsetLeft && x <= viewPortHandler.chartWidth {
            context.beginPath()
            context.move(to: CGPoint(x: x, y: viewPortHandler.contentTop))
            // Drawing code
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            let startColorComponents = Palette.white.color.withAlphaComponent(0).rgba
            let endColorComponents = Palette.white.color.withAlphaComponent(0.33).rgba
            
            let colorComponents = [startColorComponents.red, startColorComponents.green, startColorComponents.blue, startColorComponents.alpha, endColorComponents.red, endColorComponents.green, endColorComponents.blue, endColorComponents.alpha]
            let locations: [CGFloat] = [0.0, 1.0]
            guard let gradient = CGGradient(colorSpace: colorSpace, colorComponents: colorComponents, locations: locations, count: 2) else {
                return
            }
            let startPoint = CGPoint(x: x, y: viewPortHandler.contentTop)
            let endPoint = CGPoint(x: x, y: viewPortHandler.contentBottom)
            context.addLine(to: CGPoint(x: x, y: viewPortHandler.contentBottom))
            context.replacePathWithStrokedPath()
            context.clip()
            context.drawLinearGradient(gradient, start: startPoint, end: endPoint, options: .drawsBeforeStartLocation)
            context.resetClip()
            context.strokePath()
        }
    }
    
    override func renderGridLines(context: CGContext) {
        guard let xAxis = self.axis as? XAxis, let transformer = self.transformer else {
            return
        }
        
        if !xAxis.isDrawGridLinesEnabled || !xAxis.isEnabled {
            return
        }
        
        context.saveGState()
        defer { context.restoreGState() }
        context.clip(to: self.gridClippingRect)
        
        context.setShouldAntialias(xAxis.gridAntialiasEnabled)
        context.setStrokeColor(xAxis.gridColor.cgColor)
        context.setLineWidth(xAxis.gridLineWidth)
        context.setLineCap(xAxis.gridLineCap)
        
        if xAxis.gridLineDashLengths != nil {
            context.setLineDash(phase: xAxis.gridLineDashPhase, lengths: xAxis.gridLineDashLengths)
        } else {
            context.setLineDash(phase: 0.0, lengths: [])
        }
        
        let valueToPixelMatrix = transformer.valueToPixelMatrix
        
        var position = CGPoint(x: 0.0, y: 0.0)
        
        let entries = xAxis.entries
        
        for i in stride(from: 0, to: entries.count, by: 1) {
            position.x = CGFloat(entries[i] - 0.5)
            position.y = position.x
            position = position.applying(valueToPixelMatrix)
            
            drawGridLine(context: context, x: position.x, y: position.y)
        }
    }
    
    override func drawLabels(context: CGContext, pos: CGFloat, anchor: CGPoint) {
        guard let xAxis = self.axis as? XAxis, let transformer = self.transformer else {
            return
        }
        
        guard let paraStyle = NSParagraphStyle.default.mutableCopy() as? NSMutableParagraphStyle else {
            return
        }
        paraStyle.alignment = .center
        
        let labelRotationAngleRadians = xAxis.labelRotationAngle.DEG2RAD
        let centeringEnabled = xAxis.isCenterAxisLabelsEnabled
        let valueToPixelMatrix = transformer.valueToPixelMatrix
        var position = CGPoint(x: 0.0, y: 0.0)
        var labelMaxSize = CGSize()
        
        if xAxis.isWordWrapEnabled {
            labelMaxSize.width = xAxis.wordWrapWidthPercent * valueToPixelMatrix.a
        }
        
        let entries = xAxis.entries
        
        for i in stride(from: 0, to: entries.count, by: 1) {
            position.x = getInitialXPosition(xAxis: xAxis, centeringEnabled: centeringEnabled, index: i)
            
            position.y = 0.0
            position = position.applying(valueToPixelMatrix)
            
            let labelAttrs = getLabelAttributtes(isEqualPositionAndDraw: position.x == highlighted?.drawX, paraStyle: paraStyle)
            
            if viewPortHandler.isInBoundsX(position.x) {
                let label = xAxis.valueFormatter?.stringForValue(xAxis.entries[i], axis: xAxis) ?? ""
                
                let labelns = label as NSString
                
                if xAxis.isAvoidFirstLastClippingEnabled {
                    // avoid clipping of the last
                    if i == xAxis.entryCount - 1 && xAxis.entryCount > 1 {
                        let width = labelns.boundingRect(with: labelMaxSize, options: .usesLineFragmentOrigin, attributes: labelAttrs, context: nil).size.width
                        
                        if width > viewPortHandler.offsetRight * 2.0 && position.x + width > viewPortHandler.chartWidth {
                            position.x -= width / 2.0
                        }
                    } else if i == 0 { // avoid clipping of the first
                        let width = labelns.boundingRect(with: labelMaxSize, options: .usesLineFragmentOrigin, attributes: labelAttrs, context: nil).size.width
                        position.x += width / 2.0
                    }
                }
                
                drawLabel(context: context, formattedLabel: label, x: position.x, y: pos, attributes: labelAttrs, constrainedToSize: labelMaxSize, anchor: anchor, angleRadians: labelRotationAngleRadians)
            }
        }
    }
    
    private func getInitialXPosition(xAxis: XAxis, centeringEnabled: Bool, index i: Int) -> CGFloat {
        return centeringEnabled ? CGFloat(xAxis.centeredEntries[i]) : CGFloat(xAxis.entries[i])
    }
    
    private func getLabelAttributtes(isEqualPositionAndDraw: Bool, paraStyle: NSMutableParagraphStyle) -> [NSAttributedString.Key: Any] {
        var labelAttrs: [NSAttributedString.Key: Any] = [NSAttributedString.Key.paragraphStyle: paraStyle]
        if isEqualPositionAndDraw {
            labelAttrs[NSAttributedString.Key.font] = UIFont.boldSystemFont(ofSize: 12)
            labelAttrs[NSAttributedString.Key.foregroundColor] = Palette.white.color
        } else {
            labelAttrs[NSAttributedString.Key.font] = UIFont.systemFont(ofSize: 12)
            labelAttrs[NSAttributedString.Key.foregroundColor] = Palette.white.color.withAlphaComponent(0.5)
        }
        
        return labelAttrs
    }
}

extension FloatingPoint {
    var DEG2RAD: Self {
        return self * .pi / 180
    }
    
    var RAD2DEG: Self {
        return self * 180 / .pi
    }
    
    /// - returns: An angle between 0.0 < 360.0 (not less than zero, less than 360)
    /// NOTE: Value must be in degrees
    var normalizedAngle: Self {
        let angle = truncatingRemainder(dividingBy: 360)
        return (sign == .minus) ? angle + 360 : angle
    }
}
