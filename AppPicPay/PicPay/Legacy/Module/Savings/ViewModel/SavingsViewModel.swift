//
//  SavingsViewModel.swift
//  PicPay
//
//  Created by PicPay-DEV-PP on 26/11/18.
//

import SwiftyJSON
import UIKit

enum SavingType: Int {
    case past = 0
    case today = 1
    case projected = 2
    
    var analyticsText: String {
        switch self {
        case .past:
            return AnalyticsSavingsKeys.past
        case .today:
            return AnalyticsSavingsKeys.present
        case .projected:
            return AnalyticsSavingsKeys.future
        }
    }
    
    var numberOfRows: Int {
        switch self {
        case .past:
            return 1
        case .today:
            return 2
        case .projected:
            return 1
        }
    }
}

final class SavingsViewModel {
    //Used to centralize the chart with values array
    private let maxMonthsToShow = 7
    let apiSavings: ApiSavingsProtocol
    var indexGap: Int = 0
    var savings = [Saving]() {
        didSet {
            indexGap = -(savings.count - maxMonthsToShow)
        }
    }
    var selectedSavingType: SavingType
    var selectedIndex: Int {
        didSet {
            if let saving = getSelectedSaving() {
                selectedSavingType = saving.type
                onValueSelected?()
            }
        }
    }
    var selectedSaving: Saving? {
        if savings.count > selectedIndex, let saving = getSelectedSaving() {
            return saving
        }
        return nil
    }
    var receivedError: PicPayErrorDisplayable?
    
    var onValueSelected: (() -> Void)?
    var didTapAddButton: (() -> Void)?
    var didTapWithdrawButton: (() -> Void)?
    var tryAgainAction: (() -> Void)?
    
    init(with api: ApiSavingsProtocol = ApiSavings()) {
        self.apiSavings = api
        self.selectedIndex = 0
        self.selectedSavingType = .today
    }
    
    func loadSavings(completion: @escaping (Error?) -> Void) {
        receivedError = nil
        apiSavings.getSavings { [weak self] result in
            guard let strongSelf = self else {
                return
            }

            switch result {
            case .success(let response):
                strongSelf.receivedError = nil
                strongSelf.savings = response.list.order()
                strongSelf.selectedIndex = strongSelf.savings.getPresentIndex()
                completion(nil)
            case .failure(let error):
                strongSelf.receivedError = error
                completion(error)
            }
        }
    }
    
    func setSelectedValue(index: Int) {
        selectedIndex = index - indexGap
        trackChangeMonthEvent()
    }
    
    func trackChangeMonthEvent() {
        PPAnalytics.trackEvent(AnalyticsSavingsKeys.changeMonth, properties: [AnalyticsSavingsKeys.time: selectedSavingType.analyticsText])
    }
    
    func trackScrollToTheEnd() {
        if !savings.isEmpty {
            PPAnalytics.trackEvent(AnalyticsSavingsKeys.scrollToTheEnd, properties: nil)
        }
    }
    
    func numberOfRowsInSection() -> Int {
        if receivedError != nil {
            return 0
        }
        
        return selectedSavingType.numberOfRows
    }
    
    func cellForSaving(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell? {
        var saving: Saving?
        if savings.count > indexPath.row, let savingAux = getSelectedSaving() {
            saving = savingAux
        }
        switch selectedSavingType {
        case .past, .today:
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(type: SavingsExplanationTableViewCell.self, forIndexPath: indexPath)
                cell.config(type: selectedSavingType)
                return cell
            } else if selectedSavingType == .today, indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(type: SavingsButtonsTableViewCell.self, forIndexPath: indexPath)
                cell.configCell(didTapAddButton: didTapAddButton, didTapWithdrawButton: didTapWithdrawButton)
                return cell
            } else {
                return nil
            }
        case .projected:
            guard let saving = saving else {
                return nil
            }
            let cell = tableView.dequeueReusableCell(type: ProjectedInfoTableViewCell.self, forIndexPath: indexPath)
            cell.config(saving: saving)
            return cell
        }
    }
    
    private func getSavingInfoCellValue(cellType: SavingsInfoTableViewCell.SavingInfoType, saving: Saving?) -> Double? {
        var value: Double?
        if let saving = saving {
            switch cellType {
            case .totalBalance:
                value = saving.balance
            case .totalReceived:
                value = saving.deposited
            case .totalSpent:
                value = saving.spent
            case .totalYield:
                value = saving.yield
            }
        }
        
        return value
    }
    
    private func getSelectedSaving() -> Saving? {
        if savings.indices.contains(selectedIndex) {
            return savings[selectedIndex]
        }
        
        return nil
    }
    
    func getTableViewHeader() -> UIView? {
        guard let error = receivedError else {
            return nil
        }
        
        let errorView = SavingsErrorView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.6))
        errorView.setup(error: error, action: tryAgainAction)
        return errorView
    }
}
