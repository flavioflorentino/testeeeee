import CustomerSupport
import SecurityModule
import UI
import UIKit

final class SavingsViewController: PPBaseViewController, Obfuscable {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private let viewModel: SavingsViewModel
    private let savingsInfoView = SavingInfoView()
    private var didLayoutSubviews = false
    private var sendScrollToTheEndMixPanelEvent = true
    //Values when is not available iOS 11, because there is no safeArea to calculate this values
    private let spaceToCoverStatusBar: CGFloat = -20
    private let spaceBetweenTopOfScreenAndFirstButton: CGFloat = 40
    
    //Screen obfuscation
    var appSwitcherObfuscator: AppSwitcherObfuscating?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewToTopSafeAreaConstraint: NSLayoutConstraint!
    
    init(with viewModel: SavingsViewModel = SavingsViewModel()) {
        self.viewModel = viewModel
        super.init(nibName: "SavingsViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        setTableViewProperties()
    
        savingsInfoView.setValues(saving: nil, type: .today)
        
        loadInfo()
                
        setViewModelClosures()
        setSavingsInfoViewClosures()
        tableView.contentInset = UIEdgeInsets(top: savingsInfoView.infoView.frame.height, left: 0, bottom: 0, right: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObfuscationConfiguration()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deinitObfuscator()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !didLayoutSubviews {
            didLayoutSubviews = true
            tableView.backgroundView = savingsInfoView
            if #available(iOS 11.0, *) {
                savingsInfoView.setTopConstraint(topInset: view.safeAreaInsets.top)
                tableViewToTopSafeAreaConstraint.constant = -view.safeAreaInsets.top
            } else {
                savingsInfoView.setTopConstraint(topInset: spaceBetweenTopOfScreenAndFirstButton)
                tableViewToTopSafeAreaConstraint.constant = spaceToCoverStatusBar
            }
            savingsInfoView.setGradient()
            tableView.contentOffset = CGPoint(x: 0, y: -savingsInfoView.infoView.frame.height + 8)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        dismiss(animated: true, completion: nil)
    }
    
    private func setTableViewHeader() {
        tableView.tableHeaderView = viewModel.getTableViewHeader()
    }
    
    private func setTableViewProperties() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = Palette.ppColorGrayscale000.color
        tableView.estimatedRowHeight = 70
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCell(type: SavingsInfoTableViewCell.self)
        tableView.registerCell(type: SavingsButtonsTableViewCell.self)
        tableView.registerCell(type: SavingsExplanationTableViewCell.self)
        tableView.registerCell(type: ProjectedInfoTableViewCell.self)
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.isSkeletonable = true
    }
    
    private func setViewModelClosures() {
        viewModel.onValueSelected = { [weak self] in
            guard let strongSelf = self else {
            return
        }
            
            DispatchQueue.main.async {
                strongSelf.setTableViewHeader()
                strongSelf.reloadData()
                strongSelf.savingsInfoView.setValues(saving: strongSelf.viewModel.selectedSaving, type: strongSelf.viewModel.selectedSavingType)
            }
        }
        
        viewModel.didTapAddButton = { [weak self] in
            self?.openAddFunds()
        }
        viewModel.didTapWithdrawButton = { [weak self] in
            self?.openWithdrawFunds()
        }
        viewModel.tryAgainAction = { [weak self] in
            self?.loadInfo()
            self?.savingsInfoView.setValues(saving: nil, type: .today)
            self?.setTableViewHeader()
            self?.reloadData()
        }
    }
    
    private func setSavingsInfoViewClosures() {
        savingsInfoView.close = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        
        savingsInfoView.helpAction = { [weak self] in
            self?.openHelpCenter()
        }
        
        savingsInfoView.onValueSelected = { [weak self] index in
            self?.viewModel.setSelectedValue(index: index)
        }
    }
    
    private func openAddFunds() {
        let viewController = RechargeLoadFactory.make()
        let rootNavigation = PPNavigationController(rootViewController: viewController)
        
        present(rootNavigation, animated: true)
    }
    
    private func openWithdrawFunds() {
        let nav = UINavigationController(rootViewController: WithdrawOptionsFactory.make())
        present(nav, animated: true)
    }
    
    private func openHelpCenter() {
        let helpCenterDeeplink = "picpay://picpay/helpcenter/section/360010046612"
        guard let url = URL(string: helpCenterDeeplink) else {
            return
        }
        DeeplinkHelper.handleDeeplink(withUrl: url)
    }
    
    private func loadInfo() {
        viewModel.loadSavings { [weak self] error in
            guard let strongSelf = self else {
            return
        }
            strongSelf.setTableViewHeader()
            if let error = error as? PicPayErrorDisplayable {
                strongSelf.savingsInfoView.showErrorState(error: error)
                strongSelf.reloadData()
            } else {
                strongSelf.savingsInfoView.setGraphicValues(values: strongSelf.viewModel.savings)
                strongSelf.savingsInfoView.setValues(saving: strongSelf.viewModel.selectedSaving, type: strongSelf.viewModel.selectedSavingType)
            }
        }
    }
    
    private func changeBounceIfNeeded() {
        tableView.layoutIfNeeded()
        tableView.bounces = (savingsInfoView.infoView.frame.height + tableView.contentSize.height) > view.frame.height
    }
    
    private func reloadData() {
        tableView.reloadData()
        changeBounceIfNeeded()
    }
}

extension SavingsViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tableView.contentOffset.y >= (tableView.contentSize.height - tableView.frame.size.height) {
            if !sendScrollToTheEndMixPanelEvent {
                viewModel.trackScrollToTheEnd()
                sendScrollToTheEndMixPanelEvent = true
            }
        } else {
            sendScrollToTheEndMixPanelEvent = false
        }
    }
}

extension SavingsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.cellForSaving(tableView: tableView, indexPath: indexPath) ?? UITableViewCell()
    }
}
