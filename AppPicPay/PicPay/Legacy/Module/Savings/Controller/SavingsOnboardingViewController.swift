import Core
import UI
import UIKit

final class SavingsOnboardingViewController: PPBaseViewController {
    let topConstraint: CGFloat = 8.0
    var didTapSeeNow: (() -> Void)?
    
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
            titleLabel.textColor = Palette.ppColorGrayscale600.color
        }
    }
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.font = UIFont.systemFont(ofSize: 16, weight: .light)
            descriptionLabel.textColor = Palette.ppColorGrayscale500.color
            descriptionLabel.kerning = 0.3
            descriptionLabel.text = SavingsRemoteConfigWrapper.getOnboardingDescription()
        }
    }
    @IBOutlet weak var seeNowButton: UIButton! {
        didSet {
            seeNowButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
            seeNowButton.setTitleColor(Palette.white.color, for: .normal)
            seeNowButton.setTitle(DefaultLocalizable.seeNow.text, for: .normal)
            seeNowButton.backgroundColor = Palette.ppColorBranding300.color
            seeNowButton.layer.cornerRadius = seeNowButton.frame.height / 2
            seeNowButton.clipsToBounds = true
        }
    }
    @IBOutlet weak var closeToTopConstraint: NSLayoutConstraint! {
        didSet {
            closeToTopConstraint.constant = hasSafeAreaInsets ? topConstraint : UIApplication.shared.statusBarFrame.height + topConstraint
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        KVStore().setBool(true, with: .savings_onboarding_saw)
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    @IBAction private func closeOnboardingAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction private func seeNowAction(_ sender: Any) {
        dismiss(animated: false) { [weak self] in
            guard let strongSelf = self else {
            return
        }
            strongSelf.didTapSeeNow?()
        }
    }
}
