//
//  Saving.swift
//  PicPay
//
//  Created by PicPay-DEV-PP on 27/11/18.
//

import Foundation
import SwiftyJSON

struct Saving: BaseApiResponse {
    let dateTime: Date
    let type: SavingType
    let spent: Double
    let balance: Double
    let deposited: Double
    let yield: Double
    
    init?(json: JSON) {
        guard let dateTime = json[SavingDictKeys.dateTime].int, let type = json[SavingDictKeys.type].int, let spent = json[SavingDictKeys.spent].double, let balance = json[SavingDictKeys.balance].double, let deposited = json[SavingDictKeys.deposited].double, let yield = json[SavingDictKeys.yield].double else {
            return nil
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        if let date = dateFormatter.date(from: "\(dateTime)") {
            self.dateTime = date
        } else {
            return nil
        }
        
        if let savingType = SavingType(rawValue: type) {
            self.type = savingType
        } else {
            return nil
        }
        
        self.spent = spent
        self.balance = balance
        self.deposited = deposited
        self.yield = yield
    }
}

extension Array where Element == Saving {
    func getPresentElement() -> Saving? {
        return self.first(where: { saving -> Bool in
            return saving.type == .today
        })
    }
    
    func getMaxElementValue() -> Double {
        var max: Double = 0
        for balance in self.map({ saving -> Double in return saving.balance }) where balance > max {
            max = balance
        }
        
        return max
    }
    
    func getPresentIndex() -> Int {
        if let index = self.firstIndex(where: { saving -> Bool in
                return saving.type == .today
        }) {
            return index
        }
        
        return 0
    }
    
    func order() -> [Saving] {
        return self.sorted { saving1, saving2 -> Bool in
            return saving1.dateTime.compare(saving2.dateTime) == ComparisonResult.orderedAscending
        }
    }
}
