//
//  SavingsInfoHeaderView.swift
//  PicPay
//
//  Created by PicPay-DEV-PP on 27/11/18.
//

import UIKit

final class SavingsInfoHeaderView: NibView {
    @IBOutlet weak var headerLabel: UILabel! {
        didSet {
            headerLabel.text = SavingsLocalizable.monthSummary.text
            headerLabel.font = UIFont.boldSystemFont(ofSize: 13)
        }
    }
    
    @IBOutlet weak var headerContentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        headerContentView.layoutIfNeeded()
        headerContentView.applyRadiusTopCorners()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
