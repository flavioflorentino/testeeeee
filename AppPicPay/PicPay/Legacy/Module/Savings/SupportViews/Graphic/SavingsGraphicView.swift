import Charts
import UI
import UIKit

final class SavingsGraphicView: NibView {
    var onValueSelected: ((_ index: Int) -> Void)?
    
    @IBOutlet weak var lineChart: LineChartView! {
        didSet {
            lineChart.pinchZoomEnabled = false
            lineChart.dragEnabled = false
            lineChart.scaleXEnabled = false
            lineChart.scaleYEnabled = false
            lineChart.doubleTapToZoomEnabled = false
            if UIView.areAnimationsEnabled {
                lineChart.animate(yAxisDuration: 1)
            }
            lineChart.legend.enabled = false
            lineChart.chartDescription?.enabled = false
            lineChart.leftAxis.drawLabelsEnabled = false
            lineChart.leftAxis.drawGridLinesEnabled = false
            lineChart.leftAxis.drawAxisLineEnabled = false
            lineChart.leftAxis.axisMinimum = -0.5
            lineChart.leftAxis.axisMaximum = 1.5
            lineChart.rightAxis.drawLabelsEnabled = false
            lineChart.rightAxis.drawGridLinesEnabled = false
            lineChart.rightAxis.drawAxisLineEnabled = false
            lineChart.xAxis.drawAxisLineEnabled = false
            lineChart.xAxis.axisMinimum = -1
            lineChart.xAxis.axisMaximum = 7
            lineChart.xAxis.labelPosition = .bottom
            lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: getAxisData())
            lineChart.xAxis.labelTextColor = Palette.white.color
            lineChart.xAxis.labelFont = UIFont.boldSystemFont(ofSize: 12)
            lineChart.drawMarkers = true
            lineChart.xAxisRenderer = PPXAxisRenderer(viewPortHandler: lineChart.viewPortHandler, xAxis: lineChart.xAxis, transformer: lineChart.getTransformer(forAxis: YAxis.AxisDependency.left))
            lineChart.xAxis.yOffset = -30
            let marker = PPCircleView.viewFromXib()!
            marker.chartView = lineChart
            lineChart.marker = marker
            let sectionWidth = lineChart.viewPortHandler.chartWidth / 9 - 10
            lineChart.setViewPortOffsets(left: -sectionWidth, top: 0, right: -sectionWidth, bottom: 0)
            lineChart.delegate = self
        }
    }
    
    private func setLineDataSet(dataSet: LineChartDataSet) {
        dataSet.drawIconsEnabled = true
        dataSet.drawCirclesEnabled = false
        dataSet.setCircleColor(Palette.white.color)
        dataSet.lineWidth = 3
        dataSet.circleRadius = 5
        dataSet.lineCapType = .square
        dataSet.drawCircleHoleEnabled = false
        dataSet.valueFont = .systemFont(ofSize: 9)
        dataSet.formLineDashLengths = [5, 2.5]
        dataSet.formLineWidth = 1
        dataSet.formSize = 15
        dataSet.mode = .horizontalBezier
        dataSet.cubicIntensity = 1
        dataSet.drawValuesEnabled = false
        dataSet.setDrawHighlightIndicators(false)
    }
    
    private func getAxisData() -> [String] {
        var months = [String]()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "pt_BR")
        dateFormatter.dateFormat = "MMM"
        for i in -3...3 {
            if i != 0 {
                if let date = Calendar.current.date(byAdding: .month, value: i, to: Date()) {
                    months.append(dateFormatter.string(from: date).capitalized)
                }
            } else {
                months.append("Hoje")
            }
        }
        
        return months
    }
    
    func setGraphicValues(values: [Saving]) {
        guard let first = values.first, let last = values.last, let todaySaving = values.getPresentElement() else {
            return
        }
        
        var max = values.getMaxElementValue()
        if max == 0 {
            max = Double.greatestFiniteMagnitude
        }
        let gap = -(values.count - 7)
        
        var firstDataSetValues = [ChartDataEntry]()
        
        if gap == 0 {
            firstDataSetValues.append(ChartDataEntry(x: -1, y: first.balance / max))
        }
        
        for i in 0...values.getPresentIndex() {
            let value = values[i]
            firstDataSetValues.append(ChartDataEntry(x: Double(i + gap), y: value.balance / max))
        }
        
        let lineChatDataSet = LineChartDataSet(entries: firstDataSetValues, label: "")
        setLineDataSet(dataSet: lineChatDataSet)
        lineChatDataSet.setColor(Palette.white.color)
        
        var lastDataSetValues = [ChartDataEntry]()
        
        for i in values.getPresentIndex()...values.indices.last! {
            let value = values[i]
            lastDataSetValues.append(ChartDataEntry(x: Double(i + gap), y: value.balance / max))
        }
        
        lastDataSetValues.append(ChartDataEntry(x: Double(values.indices.last! + gap + 1), y: last.balance / max))
        
        let lineChatDataSet2 = LineChartDataSet(entries: lastDataSetValues, label: "")
        setLineDataSet(dataSet: lineChatDataSet2)
        
        lineChatDataSet2.setColor(PicPayStyle.savingsLighterGreen)
        lineChatDataSet2.lineDashLengths = [5, 2.5]
        lineChatDataSet2.highlightLineDashLengths = [5, 2.5]
        
        let data = LineChartData(dataSets: [lineChatDataSet, lineChatDataSet2])
        
        lineChart.data = data
        let renderer = PPLineChartRenderer(dataProvider: lineChart, animator: lineChart.chartAnimator, viewPortHandler: lineChart.viewPortHandler)
        if let dataSets = lineChart.data?.dataSets as? [LineChartDataSet] {
            renderer.lineChartDataSets = dataSets
        }
        lineChart.renderer = renderer
        lineChart.highlighter = PPChartHighlighter(chart: lineChart)
        
        lineChart.highlightValue(Highlight(x: Double(values.getPresentIndex() + gap), y: todaySaving.balance / max, dataSetIndex: 0), callDelegate: true)
    }
}

extension SavingsGraphicView: ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        guard let renderer = lineChart.xAxisRenderer as? PPXAxisRenderer else {
            return
        }
        
        renderer.highlighted = highlight
        onValueSelected?(Int(entry.x))
    }
    
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        guard let renderer = lineChart.xAxisRenderer as? PPXAxisRenderer, let highlight = renderer.highlighted else {
            return
        }
        
        lineChart.highlightValue(highlight)
    }
}
