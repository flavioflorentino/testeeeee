import Foundation
import UI

final class SavingsErrorView: NibView {
    var tryAgainAction: (() -> Void)?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var boldSubtitle: UILabel!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.applyRadiusTopCorners()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc
    private func didTapView() {
        tryAgainAction?()
    }
    
    func setup(error: PicPayErrorDisplayable, action: (() -> Void)?) {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        imageView.image = #imageLiteral(resourceName: "ilu_search_error_con")
        title.text = error.title
        title.textColor = Palette.ppColorGrayscale400.color
        subtitle.text = error.message
        subtitle.textColor = Palette.ppColorGrayscale400.color
        boldSubtitle.text = DefaultLocalizable.tapHereTryAgain.text
        boldSubtitle.textColor = Palette.ppColorGrayscale400.color
        
        if let imageWidthValue = imageView.image?.size.width {
            imageWidth.constant = imageWidthValue
        }
        
        self.isUserInteractionEnabled = true
        tryAgainAction = action
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
    }
}
