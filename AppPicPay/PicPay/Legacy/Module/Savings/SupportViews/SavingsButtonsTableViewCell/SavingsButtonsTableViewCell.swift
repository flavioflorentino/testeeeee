//
//  SavingsButtonsTableViewCell.swift
//  PicPay
//
//  Created by PicPay-DEV-PP on 28/11/18.
//

import UI
import UIKit

final class SavingsButtonsTableViewCell: UITableViewCell {
    var didTapAddButton: (() -> Void)?
    var didTapWithdrawButton: (() -> Void)?
    
    @IBOutlet weak var addButton: UIButton! {
        didSet {
            addButton.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
            addButton.tintColor = Palette.ppColorBranding300.color
            addButton.layer.borderWidth = 1
            addButton.layer.borderColor = Palette.ppColorBranding300.color.withAlphaComponent(0.4).cgColor
            addButton.layer.cornerRadius = addButton.frame.height / 2
            addButton.setImage(#imageLiteral(resourceName: "icon_add_funds"), for: .normal)
            addButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -8, bottom: 0, right: 0)
        }
    }
    @IBOutlet weak var withdrawButton: UIButton! {
        didSet {
            withdrawButton.setTitleColor(Palette.ppColorNegative300.color, for: .normal)
            withdrawButton.tintColor = Palette.ppColorNegative300.color
            withdrawButton.layer.borderWidth = 1
            withdrawButton.layer.cornerRadius = withdrawButton.frame.height / 2
            withdrawButton.layer.borderColor = Palette.ppColorNegative300.color.withAlphaComponent(0.4).cgColor
            withdrawButton.setImage(#imageLiteral(resourceName: "icon_bank"), for: .normal)
            withdrawButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -8, bottom: 0, right: 0)
        }
    }
    
    @IBAction private func addButtonAction(_ sender: Any) {
        PPAnalytics.trackEvent(AnalyticsSavingsKeys.addFundsTapped, properties: nil)
        didTapAddButton?()
    }
    
    @IBAction private func withdrawButtonAction(_ sender: Any) {
        PPAnalytics.trackEvent(AnalyticsSavingsKeys.withdrawTapped, properties: nil)
        didTapWithdrawButton?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(didTapAddButton: (() -> Void)?, didTapWithdrawButton: (() -> Void)?) {
        self.didTapAddButton = didTapAddButton
        self.didTapWithdrawButton = didTapWithdrawButton
    }
}
