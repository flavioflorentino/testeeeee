//
//  ProjectedInfoHeaderView.swift
//  PicPay
//
//  Created by PicPay-DEV-PP on 28/11/18.
//

import UI
import UIKit

final class ProjectedInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var projectedTitleLabel: UILabel! {
        didSet {
            projectedTitleLabel.textColor = Palette.ppColorGrayscale600.color
            projectedTitleLabel.font = UIFont.boldSystemFont(ofSize: 12)
        }
    }
    @IBOutlet weak var projectedDescriptionLabel: UILabel! {
        didSet {
            projectedDescriptionLabel.textColor = Palette.ppColorGrayscale500.color
            projectedDescriptionLabel.font = UIFont.systemFont(ofSize: 12, weight: .light)
        }
    }
    
    @IBOutlet weak var internalView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        internalView.layoutIfNeeded()
        internalView.applyRadiusTopCorners()
        selectionStyle = .none
        internalView.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    func config(saving: Saving) {
        let text = SavingsRemoteConfigWrapper.getSummaryText(with: .projected)
        let dateFormatter = DateFormatter.brazillianFormatter()
        projectedTitleLabel.text = String(format: text.title, dateFormatter.string(from: saving.dateTime))
        projectedDescriptionLabel.text = text.description
    }
}
