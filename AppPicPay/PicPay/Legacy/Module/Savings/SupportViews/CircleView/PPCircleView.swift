import Charts
import UI
import UIKit

final class PPCircleView: MarkerView {
    @IBOutlet weak var circleView: UIView! {
        didSet {
            circleView.backgroundColor = Palette.white.color.withAlphaComponent(0.7)
            circleView.layer.cornerRadius = circleView.frame.height / 2
            circleView.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var internalCircleView: UIView! {
        didSet {
            internalCircleView.backgroundColor = Palette.white.color
            internalCircleView.layer.cornerRadius = internalCircleView.frame.height / 2
            internalCircleView.clipsToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.offset = CGPoint(x: -12, y: -12)
        self.backgroundColor = UIColor.clear
    }
}
