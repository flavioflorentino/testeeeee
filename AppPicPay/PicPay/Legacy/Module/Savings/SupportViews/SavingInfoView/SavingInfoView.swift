import SkeletonView
import UI
import UIKit

final class SavingInfoView: NibView {
    var close: (() -> Void)?
    var helpAction: (() -> Void)?
    var onValueSelected: ((_ value: Int) -> Void)?
    
    @IBOutlet weak var balanceDateLabel: UILabel! {
        didSet {
            balanceDateLabel.font = UIFont.systemFont(ofSize: 14)
            balanceDateLabel.textColor = Palette.white.color
            balanceDateLabel.isSkeletonable = true
            balanceDateLabel.linesCornerRadius = 8
        }
    }
    
    @IBOutlet weak var balanceLabel: AnimatedLabel! {
        didSet {
            balanceLabel.font = UIFont.systemFont(ofSize: 36, weight: .thin)
            balanceLabel.textColor = Palette.white.color
            balanceLabel.isSkeletonable = true
            balanceLabel.linesCornerRadius = 8
            balanceLabel.customFormatBlock = { [weak self] value in
                guard let strongSelf = self else {
                    return NSAttributedString(string: "")
                }
                return self?.attributedTextInCurrencyString(string: Double(value).getFormattedPrice(), font: strongSelf.balanceLabel.font) ?? NSAttributedString(string: "")
            }
        }
    }
    @IBOutlet weak var yieldLabel: UILabel! {
        didSet {
            yieldLabel.font = UIFont.systemFont(ofSize: 12)
            yieldLabel.textColor = Palette.white.color
            yieldLabel.isSkeletonable = true
            yieldLabel.linesCornerRadius = 8
        }
    }
    
    @IBOutlet weak var graphicView: SavingsGraphicView!
    @IBOutlet weak var infoView: UIView! {
        didSet {
            infoView.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView! {
        didSet {
            activityIndicator.hidesWhenStopped = true
            activityIndicator.color = Palette.white.color
            activityIndicator.startAnimating()
        }
    }
    @IBOutlet weak var errorView: SavingsErrorInfoView!
    
    @IBOutlet weak var closeTopToSafeAreaConstraints: NSLayoutConstraint!
    @IBOutlet weak var helpTopToSafeAreaConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.graphicView.onValueSelected = { [weak self] index in
            guard let strongSelf = self else {
            return
        }
            strongSelf.onValueSelected?(index)
        }
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction private func didTapCloseButton(_ sender: Any) {
        close?()
    }
    
    @IBAction private func didTapHelpButton(_ sender: Any) {
        PPAnalytics.trackEvent(AnalyticsSavingsKeys.helpTapped, properties: nil)
        helpAction?()
    }
    
    func setGraphicValues(values: [Saving]) {
        graphicView.setGraphicValues(values: values)
    }
    
    func setValues(saving: Saving?, type: SavingType) {
        if let saving = saving {
            setSavingValue(saving: saving, type: type)
        } else {
            setEmptyValues()
        }
    }
    
    private func setSavingValue(saving: Saving, type: SavingType) {
        let balanceText = SavingsRemoteConfigWrapper.getBalanceText(type: type)
        
        let formatter = DateFormatter.brazillianFormatter()
        balanceDateLabel.text = String(format: balanceText.title, formatter.string(from: saving.dateTime))
        
        let yieldString = String(format: balanceText.description, saving.yield.getFormattedPrice())
        yieldLabel.attributedText = yieldString.attributedString(withBoldIn: [yieldString.fullRange], using: yieldLabel.font)
        
        balanceLabel.countFromCurrent(to: Float(saving.balance))
        stopLoading()
    }
    
    private func setEmptyValues() {
        startLoading()
    }
    
    private func stopLoading() {
        activityIndicator.stopAnimating()
        graphicView.isHidden = false
        shouldHideLabels(false)
        errorView.isHidden = true
        hideAllSkeleton()
    }
    
    private func startLoading() {
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        shouldHideLabels(false)
        graphicView.isHidden = true
        errorView.isHidden = true
        showAllSkeleton()
    }
    
    func showErrorState(error: PicPayErrorDisplayable) {
        activityIndicator.stopAnimating()
        graphicView.isHidden = true
        errorView.isHidden = false
        shouldHideLabels(true)
        errorView.center = graphicView.center
        errorView.setup(error: error)
    }
    
    private func shouldHideLabels(_ hide: Bool) {
        balanceLabel.isHidden = hide
        balanceDateLabel.isHidden = hide
        yieldLabel.isHidden = hide
    }
    
    private func showAllSkeleton() {
        let skeletonGradient = SkeletonGradient(baseColor: Palette.ppColorGrayscale000.color.withAlphaComponent(0.2), secondaryColor: Palette.ppColorGrayscale000.color.withAlphaComponent(0.4))
        balanceDateLabel.showAnimatedGradientSkeleton(usingGradient: skeletonGradient, animation: nil)
        balanceLabel.showAnimatedGradientSkeleton(usingGradient: skeletonGradient, animation: nil)
        yieldLabel.showAnimatedGradientSkeleton(usingGradient: skeletonGradient, animation: nil)
    }
    
    private func hideAllSkeleton() {
        balanceDateLabel.hideSkeleton()
        balanceLabel.hideSkeleton()
        yieldLabel.hideSkeleton()
    }
    
    func setGradient() {
        let colors = [PicPayStyle.savingsLightGreen, PicPayStyle.savingsMediumGreen, PicPayStyle.savingsDarkGreen]
        self.infoView.applyGradient(withColours: colors, gradientOrientation: .topLeftBottomRight)
        self.backgroundColor = UIColor.clear
    }
    
    func setTopConstraint(topInset: CGFloat) {
        let constraintValue = topInset + 12
        closeTopToSafeAreaConstraints.constant = constraintValue
        helpTopToSafeAreaConstraint.constant = constraintValue
        self.updateConstraints()
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    private func attributedTextInCurrencyString(string: String, font: UIFont) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string,
                                                         attributes: [.font: font])
        let semiBoldFontAttribute: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: font.pointSize, weight: .semibold)]
        let semiBoldString = string.substring(fromIndex: 2)
        let range = (string as NSString).range(of: semiBoldString)
        attributedString.addAttributes(semiBoldFontAttribute, range: range)
        return attributedString
    }
}
