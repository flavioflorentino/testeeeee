//
//  SavingsInfoTableViewCell.swift
//  PicPay
//
//  Created by PicPay-DEV-PP on 26/11/18.
//

import SkeletonView
import UI
import UIKit

final class SavingsInfoTableViewCell: UITableViewCell {
    enum SavingInfoType: Int {
        case totalReceived = 0
        case totalSpent = 1
        case totalYield = 2
        case totalBalance = 3
        
        func getCell(indexPath: IndexPath, tableView: UITableView, value: Double?) -> UITableViewCell? {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SavingsInfoTableViewCell.self), for: indexPath) as? SavingsInfoTableViewCell else {
                return nil
            }
            
            cell.configCell(type: self, value: value)
            
            return cell
        }
        
        func getText() -> String {
            switch self {
            case .totalReceived:
                return SavingsLocalizable.entry.text
            case .totalSpent:
                return SavingsLocalizable.exits.text
            case .totalYield:
                return SavingsLocalizable.totalYield.text
            case .totalBalance:
                return SavingsLocalizable.totalBalance.text
            }
        }
        
        func getImage() -> UIImage? {
            switch self {
            case .totalReceived, .totalYield:
                return #imageLiteral(resourceName: "icon_plus")
            case .totalSpent:
                return #imageLiteral(resourceName: "icon_minus")
            case .totalBalance:
                return #imageLiteral(resourceName: "icon_money")
            }
        }
        
        func getValueColor() -> UIColor {
            switch self {
            case .totalReceived, .totalYield:
                return Palette.ppColorBranding300.color
            case .totalSpent:
                return Palette.ppColorNegative300.color
            case .totalBalance:
                return Palette.ppColorGrayscale500.color
            }
        }
    }
    
    @IBOutlet weak var infoImage: UIImageView! {
        didSet {
            infoImage.isSkeletonable = true
            infoImage.layer.cornerRadius = infoImage.frame.height / 2
            infoImage.clipsToBounds = true
        }
    }
    @IBOutlet weak var infoLabel: UILabel! {
        didSet {
            infoLabel.font = UIFont.systemFont(ofSize: 13)
            infoLabel.textColor = Palette.ppColorGrayscale400.color
            infoLabel.isSkeletonable = true
            infoLabel.linesCornerRadius = 8
        }
    }
    @IBOutlet weak var balanceLabel: UILabel! {
        didSet {
            balanceLabel.font = UIFont.systemFont(ofSize: 20)
            balanceLabel.isSkeletonable = true
            balanceLabel.linesCornerRadius = 8
        }
    }
    @IBOutlet weak var separatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.isSkeletonable = true
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(type: SavingInfoType, value: Double?) {
        if let value = value {
            infoImage.image = type.getImage()
            
            infoLabel.text = type.getText()
            
            balanceLabel.textColor = type.getValueColor()
            balanceLabel.text = value.getFormattedPrice()
            hideAllSkeleton()
        } else {
            showAllSkeleton()
        }
        
        if type == .totalBalance {
            separatorView.isHidden = true
        }
    }
    
    private func showAllSkeleton() {
        infoLabel.showAnimatedGradientSkeleton()
        balanceLabel.showAnimatedGradientSkeleton()
        infoImage.showAnimatedGradientSkeleton()
    }

    private func hideAllSkeleton() {
        infoLabel.hideSkeleton()
        balanceLabel.hideSkeleton()
        infoImage.hideSkeleton()
    }
}
