//
//  SavingsErrorInfoView.swift
//  PicPay
//
//  Created by PicPay-DEV-PP on 20/12/18.
//

import UIKit

final class SavingsErrorInfoView: NibView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func setup(error: PicPayErrorDisplayable) {
        titleLabel.text = error.title
        descriptionLabel.text = error.message
    }
}
