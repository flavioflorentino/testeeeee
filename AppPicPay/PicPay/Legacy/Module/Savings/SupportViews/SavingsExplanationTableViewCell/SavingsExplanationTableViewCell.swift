import UI
import UIKit

final class SavingsExplanationTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.font = UIFont.systemFont(ofSize: 16)
            titleLabel.kerning = 0.3
            titleLabel.textColor = Palette.ppColorGrayscale500.color
        }
    }
    @IBOutlet weak var infoLabel: UILabel! {
        didSet {
            infoLabel.font = UIFont.systemFont(ofSize: 16)
            infoLabel.kerning = 0.3
            infoLabel.textColor = Palette.ppColorGrayscale400.color
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        applyRadiusTopCorners()
        selectionStyle = .none
        backgroundColor = Palette.ppColorGrayscale000.color
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func config(type: SavingType) {
        let text = SavingsRemoteConfigWrapper.getSummaryText(with: type)
        titleLabel.text = text.title
        infoLabel.text = text.description
    }
}
