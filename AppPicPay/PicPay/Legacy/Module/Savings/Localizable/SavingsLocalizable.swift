enum SavingsLocalizable: String, Localizable {
    
    case monthSummary
    case entry
    case exits
    case totalYield
    case totalBalance
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .saving
    }
}
