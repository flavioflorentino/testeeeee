import Foundation
import FeatureFlag

final class SavingsRemoteConfigWrapper {
    enum WalletDescription: Int {
        case noMoney = 0
        case withMoney = 1
    }
    
    static func getSavingsDict() -> [String: Any]? {
        FeatureManager.json(.featureSavings)
    }
    
    static private func isActive() -> Bool {
        return getSavingsDict()?[FeatureSavingsKeys.savingsAvailable] as? Bool ?? false
    }
    
    static func isActiveForCurrentUser() -> Bool {
        if isActive() {
            guard let consumerIdArray = getSavingsDict()?[FeatureSavingsKeys.savingsConsumerIdAvailable] as? [String] else {
                return false
            }
            
            if consumerIdArray.isEmpty {
                return true
            } else {
                if let id = ConsumerManager.shared.consumer?.wsId, consumerIdArray.contains("\(id)") {
                    return true
                } else {
                    return false
                }
            }
        } else {
            return false
        }
    }
    
    static func getOnboardingDescription() -> String {
        guard let dict = getSavingsDict(),
              dict.keys.contains(FeatureSavingsKeys.onboardingDescription),
              let onboardingText = dict[FeatureSavingsKeys.onboardingDescription] as? String else {
            return """
                Isso mesmo, deixe sua grana no PicPay e ela vai render a 100% do CDI diariamente.
                Bem melhor que guardar dinheiro debaixo do colchão ou na sua antiga poupança.
                """
        }
        return onboardingText
    }
    
    static func getWalletBalanceDescription(for type: WalletDescription) -> String {
        guard let dict = getSavingsDict() else {
            return ""
        }
        
        guard let arrayWalletBalanceDescription = dict[FeatureSavingsKeys.walletBalanceDescription] as? [String] else {
            return ""
        }
        
        return arrayWalletBalanceDescription[type.rawValue]
    }
    
    static func getBalanceText(type: SavingType) -> (title: String, description: String) {
        guard let dict = SavingsRemoteConfigWrapper.getSavingsDict() else {
            return (title: "", description: "")
        }

        guard let arrayBalanceTitle = dict[FeatureSavingsKeys.balanceTitle] as? [String], let arrayBalanceDescription = dict[FeatureSavingsKeys.balanceDescription] as? [String] else {
            return (title: "", description: "")
        }
        
        let balanceTitle = arrayBalanceTitle[type.rawValue]
        let balanceDescription = arrayBalanceDescription[type.rawValue]
        
        return (title: balanceTitle, description: balanceDescription)
    }
    
    static func getSummaryText(with type: SavingType) -> (title: String, description: String) {
        guard let dict = SavingsRemoteConfigWrapper.getSavingsDict(), let summaryText = dict[FeatureSavingsKeys.summaryText] as? [String: Any] else {
            return (title: "", description: "")
        }
        
        let summaryTextKey: String
        switch type {
        case .past:
            summaryTextKey = FeatureSavingsKeys.past
        case .today:
            summaryTextKey = FeatureSavingsKeys.present
        case .projected:
            summaryTextKey = FeatureSavingsKeys.future
        }
        
        guard let futureSummaryText = summaryText[summaryTextKey] as? [String: Any], let title = futureSummaryText[FeatureSavingsKeys.title] as? String, let description = futureSummaryText[FeatureSavingsKeys.description] as? String else {
            return (title: "", description: "")
        }
        
        return (title: title, description: description)
    }
}
