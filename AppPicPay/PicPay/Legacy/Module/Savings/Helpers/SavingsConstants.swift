import Foundation

struct FeatureSavingsKeys {
    static let balanceTitle = "balance_title"
    static let balanceDescription = "balance_description"
    static let futureSummaryTitle = "future_summary_title"
    static let futureSummaryText = "future_summary_text"
    static let onboardingDescription = "onboarding_description"
    static let walletBalanceDescription = "wallet_balance_description"
    static let savingsConsumerIdAvailable = "savings_consumer_id_available"
    static let savingsAvailable = "savings_available"
    static let summaryText = "summary_texts"
    static let past = "past"
    static let present = "present"
    static let future = "future"
    static let title = "title"
    static let description = "description"
}

struct SavingDictKeys {
    static let dateTime = "dateTime"
    static let type = "type"
    static let balance = "balance"
    static let spent = "spent"
    static let deposited = "deposited"
    static let yieldBalance = "yieldBalance"
    static let yield = "yield"
}

struct AnalyticsSavingsKeys {
    static let helpTapped = "Rendimento - Acessou FAQ"
    static let addFundsTapped = "Rendimento - Adicionar"
    static let withdrawTapped = "Rendimento - Retirar"
    static let infoDetailTapped = "Rendimento - tocou no resumo"
    static let scrollToTheEnd = "Rendimento - Scroll no resumo"
    static let changeMonth = "Rendimento - Navegou entre meses"
    static let enterSavings = "Rendimento - Entrou na tela de detalhamento"
    static let time = "Tempo"
    static let present = "Presente"
    static let past = "Passado"
    static let future = "Futuro"
    static let enterWay = "Forma de entrada"
    static let buttonClick = "Tocou botao"
    static let balanceClick = "Tocou no saldo"
}
