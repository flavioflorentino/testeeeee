import Core
import FeatureFlag
import Foundation

protocol ApiSavingsProtocol {
    func getSavings(completion: @escaping (PicPayResult<BaseApiListResponse<Saving>>) -> Void)
}

final class ApiSavings: ApiSavingsProtocol {
    typealias Dependencies = HasFeatureManager & HasMainQueue
    
    private let requestManager = RequestManager.shared
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func getSavings(completion: @escaping (PicPayResult<BaseApiListResponse<Saving>>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            requestManager.apiRequest(endpoint: kWsUrlSavingsPicPay, method: .get).responseApiObject { [weak self] response in
                self?.dependencies.mainQueue.async {
                    completion(response)
                }
            }
            return
        }
        
        newApi(with: completion)
    }
    
    private func newApi(with completion: @escaping (PicPayResult<BaseApiListResponse<Saving>>) -> Void) {
        let enpoint = SavingsEndpoint.get
        Api<Data>(endpoint: enpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                ApiSwiftJsonWrapper(with: result).transform(completion)
            }
        }
    }
}

private extension ApiSavings {
    enum SavingsEndpoint: ApiEndpointExposable {
        case get
        
        var path: String {
            "savings/wallet"
        }
        
        var method: HTTPMethod {
            .get
        }
    }
}
