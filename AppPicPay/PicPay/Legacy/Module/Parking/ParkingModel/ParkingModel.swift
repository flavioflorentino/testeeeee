struct ParkingStore: Codable {
    let name: String
    let disclaimer_url: String
    let seller_id: String
    let img_url: String?
    private let plates: [ParkingVehicle]?
    var vehicles: [ParkingVehicle]? {
        if let updatedList = updatedList {
            return updatedList
        }
        return plates
    }
    
    let areas: [ParkingArea]
    
    var areaSelected: ParkingArea? {
        return areas.first(where: { $0.selected == true })
    }
    
    var vehicleSelected: ParkingVehicle? {
        return vehicles?.first
    }
    
    var updatedList: [ParkingVehicle]?
    
    func vehicleForPlate(_ plate: String) -> ParkingVehicle? {
        return vehicles?.first(where: { $0.plate == plate })
    }
    
    mutating func updateOrAddVehicle(_ vehicle: ParkingVehicle) {
        if vehicles == nil {
            updatedList = [vehicle]
            return
        }
        
        guard var vehicles = vehicles else {
            return
        }
        
        if let foundVehicleIndex = vehicles.firstIndex(of: vehicle) {
            vehicles.remove(at: foundVehicleIndex)
            vehicles.insert(vehicle, at: foundVehicleIndex)
        } else {
            vehicles.append(vehicle)
        }
        updatedList = vehicles
    }
    
    mutating func removeVehicle(_ plate: String) {
        guard var vehicles = vehicles else {
            return
        }
        
        let vehicle = ParkingVehicle(plate: plate, type: "")
        if let foundVehicleIndex = vehicles.firstIndex(of: vehicle) {
            vehicles.remove(at: foundVehicleIndex)
        }
        
        updatedList = vehicles
    }
}

struct ParkingVehicle: Codable, Equatable {
    let plate: String
    let type: String?
    
    init(plate: String, type: String) {
        self.plate = plate
        self.type = type
    }
    
    enum CodingKeys: String, CodingKey {
        case plate
        case type
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        type = try? values.decode(String.self, forKey: .type)
        let decodedPlate = try values.decode(String.self, forKey: .plate)
        guard let maskedPlate = PlateValidator.mask(decodedPlate) else {
            throw DecodingError.dataCorrupted(.init(codingPath: [CodingKeys.plate], debugDescription: "Expecting a valid plate and got an invalid"))
        }
        plate = maskedPlate
    }
    
    static func == (lhs: ParkingVehicle, rhs: ParkingVehicle) -> Bool {
        return lhs.plate == rhs.plate
    }
}

struct ParkingArea: Codable {
    let id: String
    let name: String
    let types: [ParkingType]
    let selected: Bool
}

struct ParkingType: Codable {
    let type: String
    let label: String
    let fares: [ParkingFare]
}

struct ParkingFare: Codable {
    let minutes: String
    let cents: String
    let amount: String
    
    var minutesLabel: String {
        guard let minute = Int(minutes) else {
            return ""
        }
        if minute < 60 {
            return "\(minutes)min"
        } else {
            let hours: Int = (minute / 60)
            let hourLabel = hours == 1 ? "hora" : "horas"
            let minutes = minute % 60
            if minutes == 0 {
                return "\(hours) \(hourLabel)"
            } else {
                return "\(hours) \(hourLabel) e \(minutes)min"
            }
        }
    }
    
    var valueToPayDecimal: NSDecimalNumber? {
        guard let valueToPayDouble = Double(cents) else {
            return nil
        }
        return NSDecimalNumber(value: valueToPayDouble / 100.0)
    }
}
