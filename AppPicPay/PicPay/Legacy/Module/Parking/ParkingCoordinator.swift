import SafariServices
import UI
import UIKit

final class ParkingCoordinator {
    private lazy var navigation = UINavigationController(rootViewController: parkingRoot)
    private let parkingRoot: UIViewController
    private var coordinator: Coordinating?
    
    let parkingViewModel = ParkingRootViewModel()
    
    var storeId = ""
    var sellerId = ""
    
    init() {
        parkingRoot = ParkingRootViewController(viewModel: parkingViewModel)
    }
    
    init(viewController: UIViewController) {
        parkingRoot = viewController
    }
    
    func start(sellerId: String, storeId: String) -> UIViewController {
        self.storeId = storeId
        self.sellerId = sellerId
        parkingViewModel.coordinator = self
        return navigation
    }
    
    func close() {
        navigation.dismiss(animated: true, completion: nil)
    }
    
    func goToExtraInfo() {
        guard let disclaimer = URL(string: parkingViewModel.store?.disclaimer_url ?? "") else {
            return
        }
        
        let safari = SFSafariViewController(url: disclaimer)
        navigation.present(safari, animated: true, completion: nil)
    }
    
    func goToNeighbordhood() {
        let parkingAreaViewController = ParkingAreaViewController()
        let model = ParkingAreaViewModel(areas: parkingViewModel.store?.areas ?? [], coordinator: self)
        parkingAreaViewController.viewModel = model
        navigation.pushViewController(parkingAreaViewController, animated: true)
    }
    
    func goBackToRoot(selected area: ParkingArea) {
        parkingViewModel.userSelectedArea = area.id
        navigation.popViewController(animated: true)
    }
    
    func goToVehicleAdd(with plate: String? = nil) {
        let types = parkingViewModel.getArea?.types ?? []
        let viewModel = ParkingVehicleEditViewModel(types: types, coordinator: self, plate: plate)
        let parkingEditViewController = ParkingVehicleEditViewController(viewModel)
        navigation.pushViewController(parkingEditViewController, animated: true)
    }
    
    func goBackToRoot(selected vehicle: ParkingVehicle) {
        parkingViewModel.store?.updateOrAddVehicle(vehicle)
        parkingViewModel.userSelectedVehicle = vehicle.plate
        navigation.popViewController(animated: true)
    }
    
    func goToCvvFlow(completedCvv: @escaping (String) -> Void, completedNoCvv: @escaping () -> Void) {
        let coordinator = CvvRegisterFlowCoordinator(
            navigationController: navigation,
            paymentType: .bluezone,
            finishedCvv: completedCvv,
            finishedWithoutCvv: completedNoCvv
        )
        coordinator.start()
        
        self.coordinator = coordinator
    }
    
    func showReceipt(_ receipt: ReceiptWidgetViewModel) {
        TransactionReceipt.showReceiptSuccess(viewController: parkingRoot, receiptViewModel: receipt)
    }
}
