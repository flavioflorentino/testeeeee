enum ParkingLocalizable: String, Localizable {
    case area
    case vehicle
    case pickNeighborhood
    case informNeighborhood
    case informPlate
    case timeParking
    case plateNumber
    case registerVehicle
    case vehicleType
    case vehicleType2
    case pickVehicle
    case addNewVehicle
    case excludeVehicle
    case excludePickVehicle
    case newTransaction
    case errorAreaNotSelected
    case genericError
    case errorFillNeightborhood
    case errorFillVehicle
    case errorFillBoth
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .parkingLegacy
    }
}

