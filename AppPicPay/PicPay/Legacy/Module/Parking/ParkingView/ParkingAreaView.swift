import UI
import UIKit

final class ParkingAreaView: UIView {
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100.0
        tableView.tableFooterView = UIView()
        return tableView
    }()
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addComponents()
        layoutComponents()
        registerCells()
    }
    
    func addComponents() {
        addSubview(tableView)
    }
    
    func registerCells() {
        tableView.register(ParkingTitleTableViewCell.self, forCellReuseIdentifier: ParkingTitleTableViewCell.identifier)
        tableView.register(ParkingNeighborhoodTableViewCell.self, forCellReuseIdentifier: ParkingNeighborhoodTableViewCell.identifier)
    }
    
    func layoutComponents() {
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}

class ParkingTableViewCell: UITableViewCell {
    enum ParkingCellStyle {
        case title
        case normal
        
        var font: UIFont {
            if self == .title {
                return UIFont.systemFont(ofSize: 26.0, weight: .medium)
            } else {
                return UIFont.systemFont(ofSize: 16.0)
            }
        }
        
        var textColor: UIColor {
            return self == .title ? Palette.ppColorGrayscale600.color : Palette.ppColorGrayscale500.color
        }
    }
    
    var style = ParkingCellStyle.normal {
        didSet {
            label.font = style.font
            label.textColor = style.textColor
        }
    }
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    var labelBottomAnchor: CGFloat = -20.0
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addComponents()
        selectionStyle = .none
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    func addComponents() {
        contentView.addSubview(label)
        layoutComponents()
    }
    
    func layoutComponents() {
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20.0),
            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20.0),
            label.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20.0),
            label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: labelBottomAnchor)
        ])
    }
}

final class ParkingTitleTableViewCell: ParkingTableViewCell {
    static let identifier = String(describing: type(of: self))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.style = .title
        self.labelBottomAnchor = -40.0
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

final class ParkingNeighborhoodTableViewCell: ParkingTableViewCell {
    static let identifier = String(describing: type(of: self))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.style = .normal
        self.labelBottomAnchor = -20.0
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
