import UI

final class ParkingRootTwoLabelsView: UIView {    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.distribution = .fillProportionally
        stackView.axis = .horizontal
        stackView.spacing = 6.0
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    lazy var leftLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12.0)
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()
    
    lazy var rightLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = UIFont.systemFont(ofSize: 14.0, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 2
        return label
    }()
    
    var imageRight: UIImage?
    
    convenience init(frame: CGRect, leftLabel: String, rightLabel: String, imageRightLabel: UIImage? = nil) {
        self.init(frame: frame)
        
        self.leftLabel.text = leftLabel
        self.rightLabel.text = rightLabel
        self.imageRight = imageRightLabel
        appendImageRightLabel()
    }
    
    func updateLabel(text: String, color: UIColor? = nil) {
        if let textColor = color {
            rightLabel.textColor = textColor
        }
        rightLabel.text = text
        appendImageRightLabel()
    }
    
    func appendImageRightLabel() {
        if let icon = imageRight, let text = rightLabel.text {
            self.rightLabel.attributedText = NSAttributedString.withImageOn(side: .left, image: icon, text: text, font: self.rightLabel.font)
        }
    }
    
    func updateRightLabelWith(_ attributedString: NSAttributedString) {
        rightLabel.attributedText = attributedString
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale000.color
        
        addComponents()
        layoutComponents()
        isUserInteractionEnabled = true
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        rootStackView.addArrangedSubview(leftLabel)
        rootStackView.addArrangedSubview(rightLabel)
        addSubview(rootStackView)
    }
    
    private func layoutComponents() {
        leftLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 1000.0), for: .horizontal)
        leftLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000.0), for: .horizontal)
        
        NSLayoutConstraint.activate([
            rootStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            rootStackView.topAnchor.constraint(equalTo: topAnchor),
            rootStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            rootStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            rootStackView.heightAnchor.constraint(greaterThanOrEqualToConstant: 48.0)
        ])
    }
    
    func rectForPopTip(up: Bool = true) -> CGRect {
        let label = self.rightLabel
        
        let labelWidth = label.intrinsicContentSize.width
        let xLabelFrame = label.frame.origin.x
        let actualXForContentSize = (label.frame.width - labelWidth) + xLabelFrame
        
        let spacing: CGFloat = up ? 12.0 : -12.0
        let originYPlusSpacing = self.frame.origin.y + spacing
        
        return CGRect(x: actualXForContentSize,
                      y: originYPlusSpacing,
                      width: labelWidth,
                      height: self.frame.height)
    }
}
