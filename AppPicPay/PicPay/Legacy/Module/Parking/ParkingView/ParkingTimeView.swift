import UI

final class ParkingTimeView: UIView {
    private lazy var topLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 12.0)
        label.textColor = Palette.ppColorGrayscale500.color
        label.text = ParkingLocalizable.timeParking.text
        return label
    }()
    
    lazy var bottomLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 24.0, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale200.color
        label.text = "-"
        return label
    }()
    
    lazy var slider: UISlider = {
        let slider = UISlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.minimumValue = 0
        return slider
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale000.color
        
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        addSubview(topLabel)
        addSubview(bottomLabel)
        addSubview(slider)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            topLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 27.0),
            topLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -27.0),
            topLabel.topAnchor.constraint(equalTo: topAnchor, constant: 50.0),
            
            bottomLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 27.0),
            bottomLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -27.0),
            bottomLabel.topAnchor.constraint(equalTo: topLabel.bottomAnchor, constant: 16.0),
            
            slider.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 27.0),
            slider.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -27.0),
            slider.topAnchor.constraint(equalTo: bottomLabel.bottomAnchor, constant: 32.0),
        ])
    }
    
    func enableSlider() {
        slider.isEnabled = true
        slider.thumbTintColor = Palette.ppColorBranding300.color
        slider.tintColor = Palette.ppColorBranding300.color
        bottomLabel.textColor = Palette.ppColorBranding300.color
    }
    
    func disableSlider() {
        slider.isEnabled = false
        slider.thumbTintColor = Palette.ppColorGrayscale200.color
        bottomLabel.textColor = Palette.ppColorGrayscale200.color
        bottomLabel.text = "-"
    }
    
    private func currentIndexRoundedUp() -> Int {
        return slider.value != 0.0 ? Int(slider.value + 0.5) : 0
    }
    
    func getIndexAndUpdate() -> Int {
        let newIndex = currentIndexRoundedUp()
        slider.setValue(Float(newIndex), animated: false)
        return newIndex
    }
}
