import AMPopTip
import UI

final class ParkingRootView: UIView {
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.axis = .vertical
        return stackView
    }()
    
    lazy var topInfo: ParkingTopInfoView = {
        let topInfo = ParkingTopInfoView(frame: .zero)
        return topInfo
    }()
    
    lazy var area: ParkingRootTwoLabelsView = {
        return ParkingRootTwoLabelsView(frame: .zero,
                                        leftLabel: ParkingLocalizable.area.text,
                                        rightLabel: "",
                                        imageRightLabel: UIImage(named: "pencil")!)
    }()
    
    lazy var vehicle: ParkingRootTwoLabelsView = {
        return ParkingRootTwoLabelsView(frame: .zero,
                                        leftLabel: ParkingLocalizable.vehicle.text,
                                        rightLabel: "",
                                        imageRightLabel: UIImage(named: "pencil"))
    }()
    
    lazy var parkingTime: ParkingTimeView = {
        let parkingTime = ParkingTimeView(frame: .zero)
        return parkingTime
    }()
    
    lazy var popTip: PopTip = {
        let popTip = PopTip()
        popTip.bubbleColor = #colorLiteral(red: 0, green: 0.6862745098, blue: 0.9333333333, alpha: 1)
        popTip.offset = 4
        popTip.edgeInsets = UIEdgeInsets(top: 6, left: 8, bottom: 6, right: 8)
        popTip.cornerRadius = 4
        popTip.arrowSize = CGSize(width: 16, height: 16)
        popTip.textColor = Palette.ppColorGrayscale000.color
        popTip.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        popTip.edgeMargin = 8
        return popTip
    }()
    
    private lazy var separatorFull: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = Palette.ppColorGrayscale200.color
        return view
    }()
    
    private lazy var separatorSmall: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = Palette.ppColorGrayscale200.color
        return view
    }()
    
    lazy var paymentToolbar: PaymentToolbar = {
        let toolbar = PaymentToolbar(frame: .zero)
        toolbar.isUserInteractionEnabled = true
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        return toolbar
    }()
    
    private lazy var separatorSmall1: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = Palette.ppColorGrayscale200.color
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale000.color
        
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        rootStackView.addArrangedSubview(topInfo)
        rootStackView.addArrangedSubview(separatorFull)
        rootStackView.addArrangedSubview(area)
        rootStackView.addArrangedSubview(separatorSmall)
        rootStackView.addArrangedSubview(vehicle)
        rootStackView.addArrangedSubview(separatorSmall1)
        rootStackView.addArrangedSubview(parkingTime)
        rootStackView.addArrangedSubview(paymentToolbar)
        addSubview(rootStackView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            rootStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            rootStackView.topAnchor.constraint(equalTo: topAnchor),
            rootStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            rootStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            separatorFull.heightAnchor.constraint(equalToConstant: 1),
            separatorFull.leadingAnchor.constraint(equalTo: leadingAnchor),
            separatorFull.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            separatorSmall.heightAnchor.constraint(equalToConstant: 1),
            separatorSmall.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 13),
            separatorSmall.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -13),
            
            separatorSmall1.heightAnchor.constraint(equalToConstant: 1),
            separatorSmall1.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 13),
            separatorSmall1.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -13),
            
            topInfo.leadingAnchor.constraint(equalTo: leadingAnchor),
            topInfo.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            area.leadingAnchor.constraint(equalTo: leadingAnchor),
            area.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            vehicle.leadingAnchor.constraint(equalTo: leadingAnchor),
            vehicle.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            parkingTime.leadingAnchor.constraint(equalTo: leadingAnchor),
            parkingTime.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            paymentToolbar.heightAnchor.constraint(equalToConstant: 92),
            paymentToolbar.leadingAnchor.constraint(equalTo: leadingAnchor),
            paymentToolbar.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
}
