import UI

final class ParkingTopInfoView: UIView {
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.axis = .horizontal
        stackView.spacing = 6.0
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 24)
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var infoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 28
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "avatar_place.png")!
        return imageView
    }()
    
    lazy var topLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12.0)
        label.textColor = Palette.ppColorGrayscale400.color
        return label
    }()
    
    lazy var bottomLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 24.0)
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()
    
    lazy var info: UIButton = {
        let button = UIButton(type: .infoLight)
        button.tintColor = Palette.ppColorBranding300.color
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale000.color
        
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addComponents() {
        rootStackView.addArrangedSubview(imageView)
        infoStackView.addArrangedSubview(topLabel)
        infoStackView.addArrangedSubview(bottomLabel)
        rootStackView.addArrangedSubview(infoStackView)
        rootStackView.addArrangedSubview(info)
        addSubview(rootStackView)
    }
    
    func layoutComponents() {
        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalToConstant: 56),
            imageView.heightAnchor.constraint(equalToConstant: 56),
            rootStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            rootStackView.topAnchor.constraint(equalTo: topAnchor),
            rootStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            rootStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            rootStackView.heightAnchor.constraint(greaterThanOrEqualToConstant: 88.0)
        ])
    }
}
