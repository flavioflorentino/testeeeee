import SkyFloatingLabelTextField
import UI

final class ParkingVehicleEditView: UIView {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 28.0, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale600.color
        label.numberOfLines = 2
        label.text = ParkingLocalizable.registerVehicle.text
        return label
    }()
    
    lazy var plateField: SkyFloatingLabelTextField = {
        let field = SkyFloatingLabelTextField()
        field.translatesAutoresizingMaskIntoConstraints = false
        field.placeholder = ParkingLocalizable.plateNumber.text
        field.selectedTitle = ParkingLocalizable.plateNumber.text
        field.selectedTitleColor = Palette.ppColorGrayscale500.color
        field.selectedLineColor = Palette.ppColorBranding300.color
        field.textColor = Palette.ppColorGrayscale500.color
        field.autocapitalizationType = .allCharacters
        field.titleFormatter = { $0 }
        field.autocorrectionType = .no
        return field
    }()
    
    lazy var vehicleTypeField: SkyFloatingLabelTextField = {
        let field = SkyFloatingLabelTextField()
        field.translatesAutoresizingMaskIntoConstraints = false
        field.placeholder = ParkingLocalizable.vehicleType.text
        field.selectedTitleColor = Palette.ppColorGrayscale500.color
        field.selectedTitle = ParkingLocalizable.vehicleType.text
        field.selectedLineColor = Palette.ppColorBranding300.color
        field.textColor = Palette.ppColorGrayscale500.color
        field.titleFormatter = { $0 }
        field.autocorrectionType = .no
        return field
    }()
    
    lazy var proceedButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = 48.0 / 2
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(DefaultLocalizable.btConfirm.text, for: .normal)
        button.isEnabled = false
        button.disabledBackgrounColor = Palette.ppColorGrayscale300.color
        button.disabledTitleColor = Palette.ppColorGrayscale000.color
        button.normalBackgrounColor = Palette.ppColorBranding300.color
        button.normalTitleColor = Palette.ppColorGrayscale000.color
        return button
    }()
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addComponents()
        layoutComponents()
    }
    
    private func addComponents() {
        addSubview(titleLabel)
        addSubview(plateField)
        addSubview(vehicleTypeField)
        addSubview(proceedButton)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            
            plateField.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 30),
            plateField.heightAnchor.constraint(equalToConstant: 50.0),
            plateField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            plateField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            
            vehicleTypeField.topAnchor.constraint(equalTo: plateField.bottomAnchor, constant: 20),
            vehicleTypeField.heightAnchor.constraint(equalToConstant: 50.0),
            vehicleTypeField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            vehicleTypeField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            
            proceedButton.heightAnchor.constraint(equalToConstant: 48.0),
            proceedButton.topAnchor.constraint(equalTo: vehicleTypeField.bottomAnchor, constant: 30),
            proceedButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            proceedButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20)
        ])
    }
}
