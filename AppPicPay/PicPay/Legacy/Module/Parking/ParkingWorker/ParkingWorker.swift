import Core
import FeatureFlag
import Foundation

final class ParkingWorker {
    typealias Dependencies = HasFeatureManager & HasMainQueue
    private let dependencies: Dependencies

    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    var defauldCard: CardBank? {
        return CreditCardManager.shared.defaultCreditCard
    }
    
    func removePlate(_ plate: String, completion: @escaping (PicPayResult<BaseApiEmptyResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let endpoint = "digitalgoods/bluezones/plates"
            let param: [String: Any] = ["plate": PlateValidator.unmask(plate)]
            RequestManager.shared
                .apiRequest(endpoint: endpoint, method: .delete, parameters: param, headers: [:])
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func loadStore(_ storeId: String,
                   sellerId: String,
                   success: @escaping (_ store: ParkingStore) -> Void,
                   failure:  @escaping (_ error: Error) -> Void) {
        getStore(storeId: storeId, sellerId: sellerId) { [weak self] response in
            self?.dependencies.mainQueue.async {
                switch response {
                case .success(let jsonData):
                    if let data = try? jsonData.json.rawData(),
                       let parkingStore = try? JSONDecoder().decode(ParkingStore.self, from: data) {
                        success(parkingStore)
                    } else {
                        let ppError = PicPayError(message: "JSON inválido")
                        failure(ppError)
                    }
                case .failure(let error):
                    failure(error)
                }
            }
        }
    }
    
    func makeTransaction(params: [String: Any],
                         password: String,
                         success: @escaping (_ receipt: ReceiptWidgetViewModel) -> Void,
                         failure: @escaping (_ error: Error) -> Void) {
        makeTransaction(params, password: password) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let parkingReceipt):
                    let receiptModel = ReceiptWidgetViewModel(type: .PAV)
                    receiptModel.setReceiptWidgets(items: parkingReceipt.widgets)
                    
                    success(receiptModel)
                    
                case .failure(let error):
                    failure(error)
                }
            }
        }
    }
    
    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool {
        return CvvRegisterFlowCoordinator.cvvFlowEnable(cardBank: cardBank, cardValue: cardValue, paymentType: .bluezone)
    }
}

extension ParkingWorker {
    private func getStore(storeId: String,
                          sellerId: String,
                          completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            var endpoint = "digitalgoods/bluezones/areas"
            if sellerId.isEmpty {
                endpoint += "/vix?store_id=\(storeId)"
            } else {
                endpoint += "?seller_id=\(sellerId)&store_id=\(storeId)"
            }

            RequestManager.shared
                .apiRequest(endpoint: endpoint, method: .get)
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    private func makeTransaction(_ param: [String: Any],
                         password: String,
                         completion: @escaping ((PicPayResult<ParkingReceipt>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let service = "digitalgoods/bluezones/transactions/app"
            RequestManager.shared
                .apiRequest(endpoint: service, method: .post, parameters: param, headers: [:], pin: password)
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
}
