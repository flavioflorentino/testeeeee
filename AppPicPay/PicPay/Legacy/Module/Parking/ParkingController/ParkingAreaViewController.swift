import UI

final class ParkingAreaViewController: PPBaseViewController {
    var viewModel: ParkingAreaViewModel?
    private lazy var parkingAreaView: ParkingAreaView = {
        let view = ParkingAreaView()
        return view
    }()
    
    var tableView: UITableView {
        return parkingAreaView.tableView
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupController()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupController() {
        view = parkingAreaView        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
}

extension ParkingAreaViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = viewModel, let selectedArea = viewModel.areaForIndex(indexPath) else {
            return
        }
        if viewModel.isFirstRow(indexPath) { return }
        viewModel.coordinator.goBackToRoot(selected: selectedArea)
    }
}

extension ParkingAreaViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel else {
                return UITableViewCell()
            }
        
        if viewModel.isFirstRow(indexPath) {
            if let cell = tableView.dequeueReusableCell(withIdentifier: ParkingTitleTableViewCell.identifier) as? ParkingTitleTableViewCell {
                cell.label.text = ParkingLocalizable.informNeighborhood.text
                cell.label.textColor = Palette.ppColorGrayscale600.color
                return cell
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: ParkingNeighborhoodTableViewCell.identifier) as? ParkingNeighborhoodTableViewCell {
                cell.label.text = viewModel.areaForIndex(indexPath)?.name
                cell.label.textColor = Palette.ppColorGrayscale600.color
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
}
