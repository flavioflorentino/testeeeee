struct ParkingAreaViewModel {
    let areas: [ParkingArea]
    let coordinator: ParkingCoordinator
    
    let firstRowIndex = 0
    let firstRow = 1
    var numberOfRows: Int {
        return areas.count + firstRow
    }
    
    func isFirstRow(_ indexPath: IndexPath) -> Bool {
        return indexPath.row == firstRowIndex
    }
    
    func areaForIndex(_ indexPath: IndexPath) -> ParkingArea? {
        let index = indexPath.row - firstRow
        guard areas.indices.contains(index) else {
            return nil
        }
        return areas[index]
    }
}
