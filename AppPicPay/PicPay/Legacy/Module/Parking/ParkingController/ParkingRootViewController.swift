import UI

final class ParkingRootViewController: PPBaseViewController {
    lazy var closeButton: UIBarButtonItem = {
        let icon = UIImage(named: "iconClose")!
        let button = UIBarButtonItem(image: icon, style: .plain, target: self, action: #selector(close))
        return button
    }()
    
    private(set) var viewModel: ParkingRootViewModel
    
    lazy private var loadingView: UILoadView = {
        let loadingView = UILoadView(superview: view, position: .center)
        view.addSubview(loadingView)
        return loadingView
    }()
    
    lazy private var loadingTransactionView: UIView = {
        let loadingTransactionView = Loader.getLoadingView(DefaultLocalizable.completingTransaction.text) ?? UILoadView(superview: view, position: .center)
        return loadingTransactionView
    }()
    
    lazy private var rootView: ParkingRootView = {
        let rootView = ParkingRootView()
        view.addSubview(rootView)
        rootView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            rootView.topAnchor.constraint(equalTo: view.topAnchor),
            rootView.bottomAnchor.constraint(equalTo: bottomLayoutGuide.topAnchor),
            rootView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            rootView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            ])
        return rootView
    }()
    
    private var paymentManager = PPPaymentManager()
    private var toolBarController: PaymentToolbarController?
    
    //PPAuth does not work without having a reference in this class
    private var auth: PPAuth?
    // 💩💩💩💩
    
    init(viewModel: ParkingRootViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        setupController()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupController() {
        title = ParkingLocalizable.newTransaction.text
        navigationItem.leftBarButtonItem = closeButton
        view.addSubview(rootView)
        loadingView.startLoading()
        if hasSafeAreaInsets {
            paintSafeAreaBottomInset(withColor: toolBarController?.bottomBarBackgroundColor ?? .clear)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewModel()
        setupSlider()
        setupGestureRecognizers()
        setupToolbar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.checkAndUpdateState()
    }
    
    private func setupToolbar() {
        guard let manager = paymentManager else {
            return
        }
        toolBarController = PaymentToolbarController(paymentManager: manager, parent: self, toolbar: rootView.paymentToolbar, pay: { [weak self] privacyConfig in
            self?.viewModel.userSelectedPrivacyConfig = privacyConfig
            self?.checkout()
        })
    }
    
    private func setupGestureRecognizers() {
        rootView.topInfo.info.addTarget(self, action: #selector(extraInfo), for: .touchUpInside)
        rootView.area.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToNeighborhood)))
        rootView.vehicle.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToVehicle)))
    }
    
    @objc
    private func extraInfo() {
        viewModel.goToExtraInfo()
    }
    
    @objc
    private func goToNeighborhood() {
        viewModel.goToNeighbordhood()
    }
    
    @objc
    private func goToVehicle() {
        viewModel.goToVehicle()
    }
    
    @objc
    private func close() {
        viewModel.close()
    }
}

// Mark - ViewModel
extension ParkingRootViewController {
    private func setupViewModel() {
        setupViewModelViewFlow()
        setupViewModelLoading()
        setupViewModelError()
        setupViewModelStateChange()
        setupViewModelToolTip()
    }
    
    private func setupViewModelStateChange() {
        viewModel.onStateChange = { [weak self] state in
            self?.updateStoreName()
            self?.loadStoreImage()
            self?.disableSlider()
            self?.updateParkingCostEmpty()
            
            switch state {
            case .goodToGo:
                self?.updateArea()
                self?.updateVehicle()
                self?.enableSlider()
            case .needNeighborhood:
                self?.needAreaInput()
                self?.updateVehicle()
            case .needVehicle:
                self?.needVehicleInput()
                self?.updateArea()
            case .needBoth:
                self?.needVehicleInput()
                self?.needAreaInput()
            case .notLoaded:
                self?.viewModel.loadIfNecessary()
            }
        }
    }
    
    private func setupViewModelViewFlow() {
        viewModel.onTap = { [weak self] goTo in
            switch goTo {
            case .pickOnList:
                self?.openVehicleActionSheet()
            case .addNew:
                self?.viewModel.goToVehicleAdd()
            case .pay:
                self?.checkoutPPAuth()
            }
        }
    }
    
    private func setupViewModelLoading() {
        viewModel.isLoading = { [weak self] isLoading in
            if isLoading {
                self?.loadingView.startLoading()
            } else {
                self?.loadingView.animatedRemoveFromSuperView()
                self?.loadingView.isHidden = true
            }
        }
        
        viewModel.isLoadingDark = { [weak self] isLoading in
            guard let strongSelf = self else {
            return
        }
            if isLoading {
                strongSelf.navigationController?.view.addSubview(strongSelf.loadingTransactionView)
            }
            strongSelf.loadingTransactionView.isHidden = !isLoading
        }
    }
    
    private func setupViewModelError() {
        viewModel.onError = { [weak self] error in
            AlertMessage.showCustomAlertWithError(error, controller: self)
        }
    }
    
    private func setupViewModelToolTip() {
        viewModel.onToolTipAlert = { [weak self] (message, state) in
            guard let strongSelf = self else {
            return
        }
            switch state {
            case .needNeighborhood:
                strongSelf.showPopTipNeighborhood(message)
            case .needVehicle:
                strongSelf.showPopTipVehicle(message)
            case .needBoth:
                strongSelf.showPopTipBoth(message)
            default:
                break
            }
        }
        
        viewModel.hideToolTipAlert = { [weak self] in
            self?.rootView.popTip.hide()
        }
    }
}

// Update TopInfoView
extension ParkingRootViewController {
    private func loadStoreImage() {
        if let urlString = viewModel.store?.img_url {
            let storeImageView = rootView.topInfo.imageView
            let url = URL(string: urlString)
            storeImageView.setImage(url: url, placeholder: UIImage(named: "avatar_place.png")!)
        }
    }
    
    private func updateStoreName() {
        let storeNameLabel = rootView.topInfo.topLabel
        storeNameLabel.text = viewModel.store?.name
    }
    
    private func updateParkingCostEmpty() {
        updateParkingCost("R$0,00")
    }
    
    private func updateParkingCost(_ text: String) {
        let parkingCostLabel = rootView.topInfo.bottomLabel
        parkingCostLabel.text = text
    }
}

// Mark - update views TwoLabels
extension ParkingRootViewController {
    private func needAreaInputWarning() {
        rootView.area.updateLabel(text: ParkingLocalizable.pickNeighborhood.text, color: Palette.ppColorNegative300.color)
    }
    
    private func needVehicleInputWarning() {
        rootView.vehicle.updateLabel(text: ParkingLocalizable.informPlate.text, color: Palette.ppColorNegative300.color)
    }
    
    private func needAreaInput() {
        rootView.area.updateLabel(text: ParkingLocalizable.pickNeighborhood.text, color: Palette.ppColorBranding300.color)
    }
    
    private func needVehicleInput() {
        rootView.vehicle.updateLabel(text: ParkingLocalizable.informPlate.text, color: Palette.ppColorBranding300.color)
    }
    
    func updateVehicle() {
        let label = viewModel.vehicleLabel
        let color: UIColor = label == ParkingLocalizable.informPlate.text ? Palette.ppColorBranding300.color : Palette.ppColorGrayscale500.color
        rootView.vehicle.updateLabel(text: label, color: color)
    }
    
    private func updateArea() {
        if let area = viewModel.getArea {
            rootView.area.updateLabel(text: area.name, color: Palette.ppColorGrayscale500.color)
        }
    }
}

extension ParkingRootViewController {
    var slider: UISlider {
        return rootView.parkingTime.slider
    }
    
    private func setupSlider() {
        slider.addTarget(self, action: #selector(handleValueChanged), for: .valueChanged)
    }
    
    private func enableSlider() {
        rootView.parkingTime.enableSlider()
        updateSlider()
    }
    
    private func disableSlider() {
        rootView.parkingTime.disableSlider()
        updateParkingCost("R$0,00")
        rootView.parkingTime.bottomLabel.text = "-"
    }
    
    private func updateSlider() {
        slider.maximumValue = viewModel.maximumValueSlider
        slider.setValue(viewModel.defaultSlider, animated: true)
        if let fare = viewModel.defaultFare {
            viewModel.userSelectedFare = fare
            updateParkingCost(fare.amount)
            rootView.parkingTime.bottomLabel.text = fare.minutesLabel
            updatePaymentToolbarWithTotal(fare)
        }
    }

    @objc
    private func handleValueChanged() {
        let index = rootView.parkingTime.getIndexAndUpdate()
        if let parkingFare = viewModel.fareAtIndex(index) {
            viewModel.userSelectedFare = parkingFare
            updateParkingCost(parkingFare.amount)
            rootView.parkingTime.bottomLabel.text = parkingFare.minutesLabel
            updatePaymentToolbarWithTotal(parkingFare)
        }
    }
}

extension ParkingRootViewController {
    private func updatePaymentToolbarWithTotal(_ fare: ParkingFare) {
        paymentManager?.subtotal = fare.valueToPayDecimal
        toolBarController?.paymentManager = paymentManager
    }
}

extension ParkingRootViewController {
    private func checkout() {
        viewModel.pay()
    }
    
    private func checkoutPPAuth() {
        auth = PPAuth.authenticate({ [weak self] (password, biometry) in
            self?.checkoutProceed(password, isBiometry: biometry)
        }, canceledByUserBlock: {
            //There's no nil check, if I pass nil to the block it crashes.
        })
    }
    
    private func checkoutProceed(_ password: String?, isBiometry: Bool) {
        viewModel.createTransaction(password, isBiometry: isBiometry)
    }
}

// Mark - PopTip
extension ParkingRootViewController {
    private func showPopTipNeighborhood(_ message: String) {
        showPopTip(from: rootView.area, with: message)
    }
    
    private func showPopTipVehicle(_ message: String) {
        showPopTip(from: rootView.vehicle, with: message)
    }
    
    private func showPopTipBoth(_ message: String) {
        showPopTip(from: rootView.area, with: message)
    }
    
    private func showPopTip(from view: ParkingRootTwoLabelsView, with message: String) {
        let newFrame = view.rectForPopTip()
        showPopTip(from: newFrame, with: message)
    }
    
    private func showPopTip(from frame: CGRect, with message: String) {
        rootView.popTip.show(text: message, direction: .up, maxWidth: 240, in: rootView, from: frame)
    }
}
