import SkyFloatingLabelTextField
import UI

final class ParkingVehicleEditViewController: PPBaseViewController {
    private var viewModel: ParkingVehicleEditViewModel!
    
    private lazy var rootView: ParkingVehicleEditView = {
        let view = ParkingVehicleEditView()
        return view
    }()
    
    var plateTextField: SkyFloatingLabelTextField {
        return rootView.plateField
    }
    
    var typeTextField: SkyFloatingLabelTextField {
        return rootView.vehicleTypeField
    }
    
    var proceedButton: UIPPButton {
        return rootView.proceedButton
    }
    
    convenience init(_ viewModel: ParkingVehicleEditViewModel) {
        self.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
        setupViewModel()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupController()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupController() {
        view = rootView
        view.backgroundColor = Palette.ppColorGrayscale000.color
        plateTextField.delegate = self
        typeTextField.delegate = self
        proceedButton.addTarget(self, action: #selector(proceed), for: .touchUpInside)
    }
    
    private func setupViewModel() {
        viewModel.onStateChange = { [weak self] state in
            self?.proceedButton.isEnabled = state.bothOk
            self?.typeTextField.isSelected = state.type
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let plate = viewModel.getPlateAlreadyFilled() {
            updatePlate(with: plate)
        } else {
           plateTextField.becomeFirstResponder()
        }
    }
    
    private func updatePlate(with plate: String) {
        plateTextField.text = plate
        plateTextField.isSelected = true
        viewModel.updatePlate(plate)
    }
    
    @objc
    private func proceed() {
        guard viewModel.canProceed(), let vehicle = viewModel.vehicle() else {
            return
        }
        viewModel.goBackToRoot(with: vehicle)
    }
}

extension ParkingVehicleEditViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {        
        if textField == plateTextField {
            if let text = textField.text {
                let newString = (text as NSString).replacingCharacters(in: range, with: string)
                textField.text = PlateValidator.mask(newString)
                viewModel.updatePlate(newString)
                return false
            }
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == typeTextField {
            openVehicleActionSheet()
            return false
        }
        return true
    }
}

extension ParkingVehicleEditViewController {
    func openVehicleActionSheet() {
        let actionSheet = UIAlertController(title: ParkingLocalizable.vehicleType2.text, message: nil, preferredStyle: .actionSheet)
        let actions = viewModel.types.map { type in  createActionForVehicleWith(type.label) }
        let actionCancel = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil)
        
        actions.forEach { action in
            actionSheet.addAction(action)
        }
        actionSheet.addAction(actionCancel)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func createActionForVehicleWith(_ type: String) -> UIAlertAction {
        let action = UIAlertAction(title: type, style: .default) { [weak self] (action) in
            self?.viewModel.updateType(action.title)
            self?.typeTextField.text = action.title
        }
        return action
    }
}
