struct ParkingVehicleEditViewModel {
    let types: [ParkingType]
    private let coordinator: ParkingCoordinator
    
    private var plate: String?
    private var type: String?
    private var state = State(plate: false, type: false) {
        didSet {
            onStateChange(state)
        }
    }
    
    struct State {
        var plate: Bool
        var type: Bool
        var bothOk: Bool {
            return plate && type
        }
    }
    
    var onStateChange: (_ state: State) -> Void = { _ in }
    
    init(types: [ParkingType], coordinator: ParkingCoordinator, plate: String? = nil) {
        self.types = types
        self.coordinator = coordinator
        self.plate = plate
    }
    
    func goBackToRoot(with vehicle: ParkingVehicle) {
        coordinator.goBackToRoot(selected: vehicle)
    }
    
    func typeIsValid(_ type: String?) -> Bool {
        guard let type = type else {
            return false
        }
        return typeForLabel(type) != nil
    }
    
    func typeForLabel(_ typeLabel: String) -> String? {
        return types.first(where: { $0.label == typeLabel })?.type
    }
    
    mutating func getPlateAlreadyFilled() -> String? {
        return PlateValidator.mask(self.plate ?? "")
    }
    
    mutating func updatePlate(_ plate: String?) {
        self.plate = plate
        let unmasked = PlateValidator.unmask(plate ?? "")
        state.plate = PlateValidator.isValid(unmasked)
    }
    
    mutating func updateType(_ type: String?) {
        self.type = type
        state.type = typeIsValid(type)
    }
    
    func canProceed() -> Bool {
        return state.bothOk
    }
    
    func vehicle() -> ParkingVehicle? {
        guard let type = type,
            let plate = plate,
            let label = typeForLabel(type) else {
                return nil
        }
        return ParkingVehicle(plate: plate, type: label)
    }
}
