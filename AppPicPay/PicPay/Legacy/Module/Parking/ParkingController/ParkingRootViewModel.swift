import AnalyticsModule
import FeatureFlag
import Core
import PCI

final class ParkingRootViewModel {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    
    var store: ParkingStore?
    var coordinator: ParkingCoordinator?
    
    var duplicated = -1
    var userSelectedPrivacyConfig: String?
    var userSelectedArea: String?
    var userSelectedVehicle: String?
    var userSelectedFare: ParkingFare?
    private var fares: [ParkingFare] = []
    private let service: BluezoneServiceProtocol = BluezoneService()
    private let parkingWorker = ParkingWorker()
    
    enum State {
        case goodToGo
        case needNeighborhood
        case needVehicle
        case needBoth
        case notLoaded
    }
    
    //Unificar isso aqui
    var isLoading: (_ loading: Bool) -> Void = { _ in }
    var isLoadingDark: (_ loading: Bool) -> Void = { _ in }
    
    var onStateChange: (_ state: State) -> Void = { _ in }
    var onError: (_ error: Error) -> Void = { _ in }
    var onTap: (_ goTo: GoTo) -> Void = { _ in }
    var onToolTipAlert: (_ message: String, _ state: State) -> Void = { _,_  in }
    var hideToolTipAlert: () -> Void = { }
    
    enum GoTo {
        case pickOnList
        case addNew
        case pay
    }
    
    func loadIfNecessary() {
        if store == nil {
            loadData()
        }
    }
    
    func checkAndUpdateState() {
        updateState(currentState())
    }
    
    func currentState() -> State {
        if store == nil {
            return .notLoaded
        }
        
        let currentArea = getArea != nil
        let currentVehicle = getVehicle != nil
        updateCurrentFareList()
        
        if currentArea && currentVehicle {
            return .goodToGo
        } else if currentArea {
            return .needVehicle
        } else if currentVehicle {
            return .needNeighborhood
        }
        
        return .needBoth
    }
    
    func updateState(_ state: State) {
        onStateChange(state)
    }
    
    var vehicleLabel: String {
        guard let parkingType = getParkingType,
            let vehicle = getVehicle else {
                return ParkingLocalizable.informPlate.text
        }
        return "\(parkingType.label) - \(vehicle.plate)"
    }
    
    var getArea: ParkingArea? {
        if let areaId = userSelectedArea,
            let store = store,
            let userSelectedArea = store.areas.first(where: { $0.id == areaId }) {
            return userSelectedArea
        } else {
            return store?.areaSelected
        }
    }
    
    var getVehicle: ParkingVehicle? {
        if let vehicle = userSelectedVehicle,
            let store = store,
            let vehicles = store.vehicles,
            let userSelectedVehicle = vehicles.first(where: { $0.plate == vehicle }) {
            return userSelectedVehicle
        } else {
            if let firstVehicleInListWithType = store?.vehicleSelected,
                firstVehicleInListWithType.type != nil {
                return firstVehicleInListWithType
            }
            return nil
        }
    }
    
    var getParkingType: ParkingType? {
        if let area = getArea,
            let vehicle = getVehicle,
            let type = vehicle.type,
            let parkingType = area.types.first(where: { $0.type == type }) {
            return parkingType
        } else {
            return nil
        }
    }
    
    private let defaultMinutesForFare = "60"
    var defaultSlider: Float {
        if let sixtyMinutesIndexFare = fares.firstIndex(where: { $0.minutes == defaultMinutesForFare }) {
            return Float(sixtyMinutesIndexFare)
        }
        return 0.0
    }
    
    var defaultFare: ParkingFare? {
        if let sixtyMinutesFare = fares.first(where: { $0.minutes == defaultMinutesForFare }) {
            return sixtyMinutesFare
        }
        return fares.first
    }
    
    var getFare: [ParkingFare] {
        return getParkingType?.fares ?? []
    }
    
    var maximumValueSlider: Float {
        return Float(getFare.count - 1)
    }
    
    func fareAtIndex(_ index: Int) -> ParkingFare? {
        return fares.count > index ? fares[index] : nil
    }
    
    private func updateCurrentFareList() {
        fares = getFare
    }
    
    func pay() {
        let state = currentState()
        
        switch state {
        case .goodToGo:
            onTap(.pay)
        case .needBoth:
            onToolTipAlert(ParkingLocalizable.errorFillBoth.text, state)
        case .needNeighborhood:
            onToolTipAlert(ParkingLocalizable.errorFillNeightborhood.text, state)
        case .needVehicle:
            onToolTipAlert(ParkingLocalizable.errorFillVehicle.text, state)
        case .notLoaded:
            break
        }
    }
    
    func goToExtraInfo() {
        hideToolTipAlert()
        coordinator?.goToExtraInfo()
    }
    
    func goToNeighbordhood() {
        hideToolTipAlert()
        coordinator?.goToNeighbordhood()
    }
    
    func goToVehicle() {
        let shouldPickAreaFirst = getArea == nil
        let thereIsVehicleToChooseFrom = store?.vehicles != nil && !(store?.vehicles?.isEmpty ?? false)
        
        if shouldPickAreaFirst {
            onToolTipAlert(ParkingLocalizable.errorFillNeightborhood.text, .needNeighborhood)
        } else if thereIsVehicleToChooseFrom {
            onTap(.pickOnList)
        } else {
            onTap(.addNew)
        }
    }
    
    func goToVehicleAdd() {
        hideToolTipAlert()
        coordinator?.goToVehicleAdd()
    }
    
    func goToVehicleAdd(with plate: String) {
        hideToolTipAlert()
        coordinator?.goToVehicleAdd(with: plate)
    }
    
    func close() {
        coordinator?.close()
    }
    
    func getBalance() -> (fromCreditCard: String, fromUserBalance: String)? {
        guard let balance = ConsumerManager.shared.consumer?.balance,
            let valueToPayStr = userSelectedFare?.cents,
            let valueToPayDouble = Double(valueToPayStr) else {
                return nil
        }
        
        let valueToPay = NSDecimalNumber(value: valueToPayDouble / 100.0)
        let isBigger = balance.compare(valueToPay) == .orderedDescending
        
        if !ConsumerManager.useBalance() {
            return (fromCreditCard: valueToPay.stringValue,
                    fromUserBalance: NSDecimalNumber(value: 0.0).stringValue)
        }
        
        if isBigger {
            return (fromCreditCard: NSDecimalNumber(value: 0.0).stringValue,
                    fromUserBalance: valueToPay.stringValue)
        }
        
        return (fromCreditCard: valueToPay.subtracting(balance).stringValue,
                fromUserBalance: balance.stringValue)
    }
    
    func filteredVehiclesList() -> [ParkingVehicle] {
        var vehicleList: [ParkingVehicle] = []
        
        guard let store = store,
            let vehicles = store.vehicles,
            let area = getArea
            else { return vehicleList }
        
        vehicles.forEach { vehicle in
            if area.types.first(where: { $0.type == vehicle.type || vehicle.type == nil }) != nil {
                vehicleList.append(vehicle)
            }
        }
        return vehicleList
    }
    
    func APIParameters(_ password: String, isBiometry: Bool) -> [String: Any]? {
        duplicated += 1
        
        guard let vehicle = getVehicle,
            let vehicleType = vehicle.type,
            let fare = userSelectedFare,
            let privacyConfig = userSelectedPrivacyConfig,
            let area = getArea,
            let storeId = coordinator?.storeId,
            let sellerId = coordinator?.sellerId,
            let balance = getBalance()
            else {
                return nil
        }
        
        let total = balance.fromCreditCard
        let credit = balance.fromUserBalance
        
        let parkingDetails: [String: Any] = ["store_id": storeId,
                                             "type": "2",
                                             "area_id": area.id,
                                             "minutes": fare.minutes,
                                             "vehicle_type": vehicleType,
                                             "vehicle_plate": PlateValidator.unmask(vehicle.plate)]
        
        let params: [String: Any] = ["seller_id": sellerId,
                                     "duplicated": duplicated,
                                     "address_id": "0",
                                     "plan_type": "A",
                                     "origin": "tela_de_onde_veio_que_não_sei_como_preencher",
                                     "digitalgoods": parkingDetails,
                                     "id_digitalgoods": "",
                                     "pin": password,
                                     "biometry": isBiometry,
                                     "parcelas": 1,
                                     "total": total,
                                     "credit": credit,
                                     "feed_visibility": privacyConfig,
                                     "credit_card_id": String(CreditCardManager.shared.defaultCreditCardId)]
        
        return params
    }
    
    private func loadData() {
        guard let coordinator = coordinator else {
            onError(PicPayError(message: ParkingLocalizable.genericError.text))
            return
        }
        
        isLoading(true)
        
        parkingWorker.loadStore(coordinator.storeId, sellerId: coordinator.sellerId, success: { [weak self] store in
            guard let self = self else { return }
            
            self.store = store
            
            let currentState = self.currentState()
            self.updateState(currentState)
            
            self.isLoading(false)
            
        }, failure: { [weak self] error in
            guard let self = self else { return }
            self.onError(error)
        })
    }
    
    private func createBluezonePayload(password: String, isBiometry: Bool) -> DigitalGoodsPayload<Bluezone>? {
        guard let params = APIParameters(password, isBiometry: isBiometry),
            let data = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) else {
                return nil
        }
        return try? JSONDecoder.decode(data, to: DigitalGoodsPayload<Bluezone>.self)
    }

    private func createCVVPayload(informedCvv: String?) -> CVVPayload? {
        guard let cvv = informedCvv else {
            return createLocalStoreCVVPayload()
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func createLocalStoreCVVPayload() -> CVVPayload? {
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        guard let balance = getBalance(),
            let value = Double(balance.fromCreditCard),
            !value.isZero,
            let cvv = dependencies.keychain.getData(key: KeychainKeyPF.cvv(id)) else {
                return nil
        }
        
        return CVVPayload(value: cvv)
    }
    
    func createTransaction(_ password: String?, isBiometry: Bool) {
        guard checkNeedCvv() else {
            makeTransaction(password, isBiometry: isBiometry)
            return
        }
        
        coordinator?.goToCvvFlow(completedCvv: { [weak self] cvv in
            self?.makeTransaction(password, isBiometry: isBiometry, informedCvv: cvv)
        }, completedNoCvv: { [weak self] in
            self?.showCvvError()
        })
    }
    
    private func showCvvError() {
        isLoadingDark(false)
        let error = PicPayError(message: DefaultLocalizable.cvvNotInformed.text)
        onError(error)
    }
    
    private func makeTransaction(_ password: String?, isBiometry: Bool, informedCvv: String? = nil) {
        isLoadingDark(true)

        guard let password = password, let params = APIParameters(password, isBiometry: isBiometry) else {
            isLoadingDark(false)
            onError(PicPayError(message: ParkingLocalizable.genericError.text))
            return
        }
        
        if FeatureManager.isActive(.pciBluezone) {
            createTransactionPCI(password: password, isBiometry: isBiometry, informedCvv: informedCvv)
            return
        }
        
        parkingWorker.makeTransaction(params: params, password: password, success: { [weak self] receipt in
            guard let self = self else { return }
            
            self.isLoadingDark(false)
            self.coordinator?.showReceipt(receipt)
            
            if let balance = self.getBalance(),
                let value = Decimal(string: balance.fromCreditCard) {
                
                Analytics.shared.log(PaymentEvent.transaction(.parking,
                                                       id: receipt.transactionId,
                                                       value: value))
            } else {
                Analytics.shared.log(PaymentEvent.transaction(.parking,
                                                       id: receipt.transactionId, value: 0))
            }
            
            NotificationCenter.default.post(name: Notification.Name.Payment.new,
                                            object: nil,
                                            userInfo: ["type": "Digital Goods"])
        }, failure: { [weak self] error in
            guard let self = self else { return }
            
            self.isLoadingDark(false)
            self.onError(error)
        })
    }
    
    private func createTransactionPCI(password: String, isBiometry: Bool, informedCvv: String?) {
        guard let bluezonePayload = createBluezonePayload(password: password, isBiometry: isBiometry) else {
            isLoadingDark(false)
            onError(PicPayError(message: ParkingLocalizable.genericError.text))
            return
        }
        let cvv = createCVVPayload(informedCvv: informedCvv)
        let payload = PaymentPayload<DigitalGoodsPayload<Bluezone>>(cvv: cvv, generic: bluezonePayload)
        service.makeTransaction(password: password, payload: payload, isNewArchitecture: false) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let value):
                    var array: [ReceiptWidgetItem] = []
                    for element in value.receiptWidgets {
                        guard let receipt = WSReceipt.createReceiptWidgetItem(jsonDict: element) else {
                            return
                        }
                        array.append(receipt)
                    }
                    let receiptModel = ReceiptWidgetViewModel(type: .PAV)
                    receiptModel.setReceiptWidgets(items: array)
                    
                    NotificationCenter.default.post(name: Notification.Name.Payment.new, object: nil, userInfo: ["type": "Digital Goods"])
                    
                    self?.saveCvv(informedCvv: informedCvv)
                    self?.coordinator?.showReceipt(receiptModel)
                    
                case .failure(let error):
                    self?.isLoadingDark(false)
                    self?.onError(error.picpayError)
                }
            }

        }
    }
    
    private func saveCvv(informedCvv: String?) {
        guard let cvv = informedCvv else {
            return
        }
        
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        dependencies.keychain.set(key: KeychainKeyPF.cvv(id), value: cvv)
    }
    
    private func checkNeedCvv() -> Bool {
        guard
            let balance = getBalance(),
            let value = Double(balance.fromCreditCard),
            let cardBank = parkingWorker.defauldCard
            else {
                return false
            }
        
        return parkingWorker.pciCvvIsEnable(cardBank: cardBank, cardValue: value)
    }
}
