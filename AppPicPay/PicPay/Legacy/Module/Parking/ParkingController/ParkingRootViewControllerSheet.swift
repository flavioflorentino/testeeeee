// Mark - Melhorar a forma como isso aqui funciona
extension ParkingRootViewController {
    func openVehicleActionSheet() {
        var actions: [UIAlertAction] = []
        viewModel.filteredVehiclesList().forEach { vehicle in
            actions.append(createActionForVehicleWith(vehicle.plate))
        }
        
        let actionSheet = UIAlertController(title: ParkingLocalizable.pickVehicle.text, message: nil, preferredStyle: .actionSheet)
        let actionCancel = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil)
        
        actions.forEach { action in
            actionSheet.addAction(action)
        }
        actionSheet.addAction(addVehicleAction())
        actionSheet.addAction(removeVehicleAction())
        actionSheet.addAction(actionCancel)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    private func addVehicleAction() -> UIAlertAction {
        return UIAlertAction(title: ParkingLocalizable.addNewVehicle.text, style: .default) { [weak self] _ in
            self?.viewModel.coordinator?.goToVehicleAdd()
        }
    }
    
    private func removeVehicleAction() -> UIAlertAction {
        return UIAlertAction(title: ParkingLocalizable.excludeVehicle.text, style: .destructive) { [weak self] _ in
            self?.openRemoveVehicleActionSheet()
        }
    }
    
    private func createActionForVehicleWith(_ plate: String) -> UIAlertAction {
        let action = UIAlertAction(title: plate, style: .default) { [weak self] (action) in
            guard let store = self?.viewModel.store,
                let vehicles = store.vehicles,
                let plate = action.title,
                let parkingVehicle = vehicles.first(where: { $0.plate == plate })
                else {
            return
        }
            
            let hasType = parkingVehicle.type != nil
            
            if hasType {
                self?.viewModel.userSelectedVehicle = plate
                self?.viewModel.checkAndUpdateState()
            } else {
                self?.viewModel.goToVehicleAdd(with: plate)
            }
        }
        return action
    }
    
    func openRemoveVehicleActionSheet() {
        let actions = viewModel.filteredVehiclesList().map({ vehicle in removeVehicle(vehicle.plate) })
        let actionSheet = UIAlertController(title: ParkingLocalizable.excludePickVehicle.text, message: nil, preferredStyle: .actionSheet)
        let actionCancel = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil)
        
        actions.forEach({ action in actionSheet.addAction(action) })
        actionSheet.addAction(actionCancel)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    private func removeVehicle(_ plate: String) -> UIAlertAction {
        let action = UIAlertAction(title: plate, style: .default) { [weak self] (action) in
            ParkingWorker().removePlate(action.title ?? "", completion: { _ in })
            self?.viewModel.store?.removeVehicle(action.title ?? "")
            self?.updateVehicle()
            self?.viewModel.checkAndUpdateState()
            self?.openVehicleActionSheet()
        }
        return action
    }
}
