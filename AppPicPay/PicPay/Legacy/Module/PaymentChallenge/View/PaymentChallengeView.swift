import UI
import UIKit

final class PaymentChallengeView: UIView {
    private lazy var mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 16
        return stackView
    }()
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 28)
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    private lazy var blankSpace: UIView = {
        let view = UIView()
        return view
    }()
    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 8
        stackView.alignment = .center
        return stackView
    }()
    lazy var primaryButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = 24
        button.configure(with: Button(title: "", type: .cta))
        button.addTarget(self, action: #selector(primaryButtonTapped), for: .touchUpInside)
        return button
    }()
    lazy var secondaryButton: UIPPButton = {
        let button = UIPPButton()
        button.addTarget(self, action: #selector(secondaryButtonTapped), for: .touchUpInside)
        return button
    }()
    var didTapPrimaryButton: (() -> Void)?
    var didTapSecondaryButton: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale000.color
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        mainStackView.addArrangedSubview(imageView)
        mainStackView.addArrangedSubview(titleLabel)
        mainStackView.addArrangedSubview(subtitleLabel)
        mainStackView.addArrangedSubview(blankSpace)
        
        buttonsStackView.addArrangedSubview(primaryButton)
        buttonsStackView.addArrangedSubview(secondaryButton)
        
        mainStackView.addArrangedSubview(buttonsStackView)

        addSubview(mainStackView)
    }
    
    private func layoutComponents() {
        var topAnchorContraint = topAnchor
        if #available(iOS 11.0, *) {
            topAnchorContraint = safeAreaLayoutGuide.topAnchor
        }
        
        NSLayoutConstraint.activate([
            mainStackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            mainStackView.topAnchor.constraint(greaterThanOrEqualTo: topAnchorContraint),
            mainStackView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor),
            mainStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            mainStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            
            imageView.heightAnchor.constraint(equalToConstant: 115),
            
            buttonsStackView.leadingAnchor.constraint(equalTo: mainStackView.leadingAnchor),
            buttonsStackView.trailingAnchor.constraint(equalTo: mainStackView.trailingAnchor),
            
            blankSpace.heightAnchor.constraint(equalToConstant: 4),
            
            primaryButton.heightAnchor.constraint(equalToConstant: 48),
            primaryButton.leadingAnchor.constraint(equalTo: buttonsStackView.leadingAnchor),
            primaryButton.trailingAnchor.constraint(equalTo: buttonsStackView.trailingAnchor),
            
            secondaryButton.heightAnchor.constraint(equalToConstant: 48),
            secondaryButton.leadingAnchor.constraint(equalTo: buttonsStackView.leadingAnchor),
            secondaryButton.trailingAnchor.constraint(equalTo: buttonsStackView.trailingAnchor)
        ])
    }
    
    func setupSubtitleLabelWith(text: String) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        let attributes = [NSAttributedString.Key.paragraphStyle: paragraphStyle]
        let attributedText = NSAttributedString(string: text, attributes: attributes)
        subtitleLabel.attributedText = attributedText
        subtitleLabel.textAlignment = .center
    }

    func setupPrimaryButtonWith(text: String, type: Button.ButtonType = .cta) {
        let primaryButtonConfig = Button(title: text, type: type)
        primaryButton.configure(with: primaryButtonConfig)
    }
    
    func setupSecondaryButtonWith(text: String?) {
        if let secondaryButtonText = text {
            let buttonConfig = Button(title: secondaryButtonText, type: .cleanUnderlined)
            secondaryButton.configure(with: buttonConfig)
        } else {
            secondaryButton.isHidden = true
        }
    }
    
    @objc
    private func primaryButtonTapped() {
        didTapPrimaryButton?()
    }
    
    @objc
    private func secondaryButtonTapped() {
        didTapSecondaryButton?()
    }
}
