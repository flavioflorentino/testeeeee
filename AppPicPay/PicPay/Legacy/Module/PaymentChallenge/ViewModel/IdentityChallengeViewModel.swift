import FeatureFlag

final class IdentityChallengeViewModel {
    enum ScreenState: String {
        case startValidation = "inicio"
        case continueValidation = "continuar"
        case success = "sucesso"
        case wait = "analise"
    }
    
    private let validationStatusService: IdentityValidationStatusServicing
    private let startValidationService: IdentityValidationInitialServicing
    var screenState: ScreenState?
    
    var imageAsset: UIImage?
    var screenConstants: ChallengeScreenConfigs?
    
    init(
        statusService: IdentityValidationStatusServicing = IdentityValidationStatusService(),
        startValidationService: IdentityValidationInitialServicing = IdentityValidationInitialService()
    ) {
        validationStatusService = statusService
        self.startValidationService = startValidationService
    }
    
    func getIdentityValidationStatus(onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        validationStatusService.status { [weak self] result in
            switch result {
            case .success(let validationStatus):
                let status = validationStatus.status
                self?.updateScreenState(for: status)
                self?.updateScreenConstants()
                onSuccess()
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    func skipFirstStepIdentityValidation(onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        startValidationService.startValidation(originFlow: .paymentChallenge) { result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    onSuccess()
                case .failure(let error):
                    onError(error)
                }
            }
        }
    }
    
    private func updateScreenState(for validationStatus: IVIdentityValidationStatus.Status) {
        switch validationStatus {
        case .notCreated, .notVerified, .undefined:
            screenState = .startValidation
        case .pending:
            screenState = .continueValidation
        case .toVerify:
            screenState = .wait
        case .verified:
            screenState = .success
        case .rejected, .disabled:
            break
        }
    }
    
    private func updateScreenConstants() {
        guard let state = screenState else {
            return
        }
        let constants = FeatureManager.object(.featureChallengeIdentity, type: IdentityChallengeConfigs.self)
        
        switch state {
        case .startValidation:
            screenConstants = constants?.startScreen
            imageAsset = #imageLiteral(resourceName: "ilu_identity_validation")
        case .continueValidation:
            screenConstants = constants?.continueScreen
            imageAsset = #imageLiteral(resourceName: "ilu_identity_validation")
        case .wait:
            screenConstants = constants?.onReviewScreen
            imageAsset = #imageLiteral(resourceName: "ico_clock_green")
        case .success:
            screenConstants = constants?.successScreen
            imageAsset = #imageLiteral(resourceName: "icon-big-success")
        }
    }
    
    func closingAlert() -> Alert? {
        guard let dictionary = FeatureManager.json(.featureChallengePopupCancel) else {
            return nil
        }
        return Alert(dictionary: dictionary)
    }
    
    func trackScreenEvent() {
        guard let state = screenState else {
            return
        }
        let eventName = "Visualizacao de tela"
        let properties = ["nome": "desafio de identidade - \(state.rawValue)"]
        PPAnalytics.trackEvent(eventName, properties: properties)
    }
}
