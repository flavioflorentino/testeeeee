import FeatureFlag

final class CardChallengeViewModel: NSObject {
    enum ScreenState: String {
        case startValidation = "inicio"
        case success = "sucesso"
        case failure = "falha"
    }
    
    private let api: VerifyCreditCardProtocol
    private let cardId: String
    var screenState: ScreenState?
    
    var imageAsset: UIImage?
    var screenConstants: ChallengeScreenConfigs?
    
    var isPrimayButtonDestructive: Bool {
        return screenState == .failure
    }
    
    init(cardId: String, api: VerifyCreditCardProtocol = ApiPaymentMethods()) {
        self.cardId = cardId
        self.api = api
    }
    
    func getCardValidationStatus(onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        api.verifyStatus(id: cardId) { [weak self] response in
            switch response {
            case .success(let statusResponse):
                let status = statusResponse.verificationStatus.status
                self?.updateScreenState(for: status)
                self?.updateScreenConstants()
                onSuccess()
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    func getCardModel(onSuccess: @escaping (CardBank) -> Void, onError: @escaping (Error?) -> Void) {
        if let card = CreditCardManager.shared.cardWithId(cardId) {
            onSuccess(card)
            return
        }
        CreditCardManager.shared.loadCardList { [weak self] error in
            guard let id = self?.cardId else {
                return
            }
            if error == nil, let card = CreditCardManager.shared.cardWithId(id) {
                onSuccess(card)
            } else {
                onError(error)
            }
        }
    }
    
    private func updateScreenState(for validationStatus: CardBank.Status) {
        switch validationStatus {
        case .verifiable, .waitingFillValue:
            screenState = .startValidation
        case .verified:
            screenState = .success
        case .notAvailable:
            screenState = .failure
        case .unknown:
            break
        }
    }
    
    private func updateScreenConstants() {
        guard let state = screenState else {
            return
        }
        let constants = FeatureManager.object(.cardVerification, type: CardChallengeConfigs.self)
        
        switch state {
        case .startValidation:
            screenConstants = constants?.startScreen
            imageAsset = #imageLiteral(resourceName: "creditCardValidationOnboarding")
        case .success:
            screenConstants = constants?.successScreen
            imageAsset = #imageLiteral(resourceName: "icon-big-success")
        case .failure:
            screenConstants = constants?.failureScreen
            imageAsset = #imageLiteral(resourceName: "creditCardValidationFail")
        }
    }
    
    func closingAlert() -> Alert? {
        guard let dictionary = FeatureManager.json(.featureChallengePopupCancel) else {
            return nil
        }
        return Alert(dictionary: dictionary)
    }
    
    func trackScreenEvent() {
        guard let state = screenState else {
            return
        }
        let eventName = "Visualizacao de tela"
        let properties = ["nome": "desafio de cartao - \(state.rawValue)"]
        PPAnalytics.trackEvent(eventName, properties: properties)
    }
}
