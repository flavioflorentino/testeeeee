import FeatureFlag
import UI
import UIKit

final class IdentityChallengeViewController: PPBaseViewController {
    private let challengeView = PaymentChallengeView()
    private lazy var closeButton: UIBarButtonItem = {
        let closeImage = #imageLiteral(resourceName: "icon_round_close_green")
        let closeButton = UIBarButtonItem(image: closeImage, style: .done, target: self, action: #selector(close))
        closeButton.accessibilityLabel = DefaultLocalizable.btClose.text
        return closeButton
    }()
    private var identityValidationCoordinator: IdentityValidationFlowCoordinator?
    private let viewModel = IdentityChallengeViewModel()
    
    override func loadView() {
        view = challengeView
        setupNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateScreenInformation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    private func updateScreenInformation() {
        startLoadingView()
        viewModel.getIdentityValidationStatus(onSuccess: { [weak self] in
            DispatchQueue.main.async {
                self?.viewModel.trackScreenEvent()
                self?.setupViews()
                self?.stopLoadingView()
            }
        }, onError: { [weak self] error in
            DispatchQueue.main.async {
                self?.closeWithErrorAlert(error)
            }
        })
    }
    
    private func setupNavigationBar() {
        navigationItem.leftBarButtonItem = closeButton
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale000.color
    }
    
    private func closeWithErrorAlert(_ error: Error) {
        let presentingController = presentingViewController
        dismiss(animated: true, completion: {
            AlertMessage.showAlert(error, controller: presentingController)
        })
    }
    
    private func setupViews() {
        challengeView.imageView.image = viewModel.imageAsset
        setupTexts()
        setupActions()
    }
    
    private func setupTexts() {
        guard let constants = viewModel.screenConstants else {
            return
        }
        challengeView.titleLabel.text = constants.title
        challengeView.setupSubtitleLabelWith(text: constants.description)
        challengeView.primaryButton.setTitle(constants.primaryButtonText, for: .normal)
        challengeView.setupSecondaryButtonWith(text: constants.secondaryButtonText)
    }
    
    private func setupActions() {
        guard let screenState = viewModel.screenState else {
            return
        }
        switch screenState {
        case .startValidation:
            closeButton.action = #selector(closeWithCustomAlert)
            challengeView.didTapPrimaryButton = startIdentityValidation
            challengeView.didTapSecondaryButton = openHelpcenter
        case .continueValidation:
            closeButton.action = #selector(closeWithCustomAlert)
            challengeView.didTapPrimaryButton = continueIdentityValidation
            challengeView.didTapSecondaryButton = openHelpcenter
        case .wait:
            closeButton.action = #selector(close)
            challengeView.didTapPrimaryButton = close
        case .success:
            challengeView.didTapPrimaryButton = close
        }
    }
    
    private func openHelpcenter() {
        guard let string = viewModel.screenConstants?.helpcenterUrl, let url = URL(string: string) else {
            return
        }
        DeeplinkHelper.handleDeeplink(withUrl: url, from: self)
    }

    private func startIdentityValidation() {
        viewModel.skipFirstStepIdentityValidation(onSuccess: { [weak self] in
            self?.presentIdentityValidation()
        }, onError: { error in
            AlertMessage.showAlert(error, controller: self)
        })
    }
    
    private func continueIdentityValidation() {
        presentIdentityValidation()
    }
    
    private func presentIdentityValidation() {
        let coordinator = IdentityValidationFlowCoordinator(from: self, originFlow: .paymentChallenge)
        identityValidationCoordinator = coordinator
        coordinator.start()
    }
    
    private func dismiss() {
        navigationController?.dismiss(animated: true)
    }
    
    @objc
    private func close() {
        dismiss()
    }
    
    @objc
    private func closeWithCustomAlert() {
        guard let alert = viewModel.closingAlert() else {
            return
        }
        AlertMessage.showAlert(alert, controller: self, action: { [weak self] alertController, button, _ in
            alertController.dismiss(animated: true, completion: {
                if button.action == .finish {
                    self?.dismiss()
                }
            })
        })
    }
}
