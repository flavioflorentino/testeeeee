import UI
import UIKit

final class CardChallengeViewController: PPBaseViewController {
    private let challengeView = PaymentChallengeView()
    private lazy var closeButton: UIBarButtonItem = {
        let closeImage = #imageLiteral(resourceName: "icon_round_close_green")
        let closeButton = UIBarButtonItem(image: closeImage, style: .done, target: self, action: #selector(close))
        closeButton.accessibilityLabel = DefaultLocalizable.btClose.text
        return closeButton
    }()
    
    private let viewModel: CardChallengeViewModel
    
    @objc
    init(cardId: String) {
        viewModel = CardChallengeViewModel(cardId: cardId)
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = challengeView
        setupNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateScreenInformation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    private func updateScreenInformation() {
        startLoadingView()
        viewModel.getCardValidationStatus(onSuccess: { [weak self] in
            DispatchQueue.main.async {
                self?.viewModel.trackScreenEvent()
                self?.setupViews()
                self?.stopLoadingView()
            }
        }, onError: { [weak self] error in
            DispatchQueue.main.async {
                self?.closeWithErrorAlert(error)
            }
        })
    }
    
    private func setupNavigationBar() {
        navigationItem.leftBarButtonItem = closeButton
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale000.color
    }
    
    private func closeWithErrorAlert(_ error: Error) {
        let presentingController = presentingViewController
        dismiss(animated: true, completion: {
            AlertMessage.showAlert(error, controller: presentingController)
        })
    }
    
    private func setupViews() {
        challengeView.imageView.image = viewModel.imageAsset
        setupTexts()
        setupActions()
    }
    
    private func setupTexts() {
        guard let constants = viewModel.screenConstants else {
            return
        }
        challengeView.titleLabel.text = constants.title
        challengeView.setupSubtitleLabelWith(text: constants.description)
        
        let primaryButtonType: Button.ButtonType = viewModel.isPrimayButtonDestructive ? .destructive : .cta
        challengeView.setupPrimaryButtonWith(text: constants.primaryButtonText, type: primaryButtonType)
        challengeView.setupSecondaryButtonWith(text: constants.secondaryButtonText)
    }
    
    private func setupActions() {
        guard let screenState = viewModel.screenState else {
            return
        }
        switch screenState {
        case .startValidation:
            closeButton.action = #selector(closeWithCustomAlert)
            challengeView.didTapPrimaryButton = startCreditCardValidation
            challengeView.didTapSecondaryButton = openHelpcenter
        case .success:
            challengeView.didTapPrimaryButton = close
        case .failure:
            closeButton.action = #selector(close)
            challengeView.didTapPrimaryButton = close
            challengeView.didTapSecondaryButton = startAddNewCreditCard
        }
    }
    
    private func openHelpcenter() {
        guard let string = viewModel.screenConstants?.helpcenterUrl, let url = URL(string: string) else {
            return
        }
        DeeplinkHelper.handleDeeplink(withUrl: url, from: self)
    }
    
    private func startCreditCardValidation() {
        challengeView.primaryButton.startLoadingAnimating()
        viewModel.getCardModel(onSuccess: { [weak self] card in
            DispatchQueue.main.async {
                self?.challengeView.primaryButton.stopLoadingAnimating()
                
                let viewModel = CardVerificationViewModel(with: card)
                viewModel.willFinishVerification = { nav in
                    nav.popToRootViewController(animated: true)
                    return false
                }
                let controller = CardVerificationHelpers.controller(withModel: viewModel)
                self?.navigationController?.pushViewController(controller, animated: true)
            }
        }, onError: { [weak self] error in
            DispatchQueue.main.async {
                self?.challengeView.primaryButton.stopLoadingAnimating()
                guard let error = error else {
                    return
                }
                self?.closeWithErrorAlert(error)
            }
        })
    }
    
    private func startAddNewCreditCard() {
        guard let addCreditCardViewController = ViewsManager.paymentMethodsStoryboardViewController(withIdentifier: "AddNewCreditCardViewController") as? AddNewCreditCardViewController else {
            return
        }
        addCreditCardViewController.viewModel = AddNewCreditCardViewModel(origin: .payment)
        addCreditCardViewController.isCloseButton = true
        navigationController?.pushViewController(addCreditCardViewController, animated: true)
    }
    
    private func dismiss() {
        navigationController?.dismiss(animated: true)
    }
    
    @objc
    private func close() {
        dismiss()
    }
    
    @objc
    private func closeWithCustomAlert() {
        guard let alert = viewModel.closingAlert() else {
            return
        }
        AlertMessage.showAlert(alert, controller: self, action: { [weak self] alertController, button, _ in
            alertController.dismiss(animated: true, completion: {
                if button.action == .finish {
                    self?.dismiss()
                }
            })
        })
    }
}
