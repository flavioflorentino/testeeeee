final class ChallengeScreenConfigs: Codable {
    let title: String
    let description: String
    let primaryButtonText: String
    let secondaryButtonText: String?
    let helpcenterUrl: String?
}

final class IdentityChallengeConfigs: Codable {
    let startScreen: ChallengeScreenConfigs
    let continueScreen: ChallengeScreenConfigs
    let successScreen: ChallengeScreenConfigs
    let onReviewScreen: ChallengeScreenConfigs
}
final class CardChallengeConfigs: Codable {
    let startScreen: ChallengeScreenConfigs
    let successScreen: ChallengeScreenConfigs
    let failureScreen: ChallengeScreenConfigs
}
