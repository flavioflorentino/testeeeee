import AnalyticsModule
import Core
import CoreLegacy
import UIKit
import FeatureFlag

final class RegistrationViewModel: NSObject {
    enum RequestCodeType {
        case sms
        case whatsApp
    }
    
    // form data
    var firstName = ""
    var lastName = ""
    var email = ""
    var ddd = ""
    var phone = ""
    var code = ""
    var cpf = ""
    var dateOfBirth = ""
    var password = ""
    var username = ""
    let helpURLPath: String
    
    // Aditional data
    private let beginRegistrationTime: CFTimeInterval = CACurrentMediaTime()
    var invalidEmailCounter = 0
    var invalidCpfCounter = 0
    var copyPasteCpf = false
    
    private let api = RegisterConsumerApi()
    private let service: RegisterConsumerServicing = RegisterConsumerService()
    
    private (set) var requestCodeType: RequestCodeType = .sms
    
    private var addConsumerParameters: [String: Any] {
        let consumerDict: [String: Any] = [
            "email": email,
            "pass": password,
            "cpf": cpf,
            "birth_date": dateOfBirth,
            "name": "\(firstName) \(lastName)",
            "indication": "",
            "notification_token": AppParameters.global().pushToken ?? "",
            "distinct_id": PPAnalytics.distinctId() ?? "",
            "appsflyer_id": PPAnalytics.appsFlyerId() ?? "",
            "advertising_id": PPAnalytics.idfa() ?? ""
        ]
        
        let registrationTimeInMiliseconds = Int((CACurrentMediaTime() - beginRegistrationTime) * 1000)
        let additionalInfo: [String: Any] = [
            "registration_elapsed_time": String(registrationTimeInMiliseconds),
            "registration_tried_invalid_email":  String(invalidEmailCounter),
            "registration_tried_invalid_cpf": String(invalidCpfCounter),
            "registration_cpf_copy_paste": String(copyPasteCpf)
        ]
        
        return consumerDict.deepMerge(additionalInfo)
    }
    
    //When install from dynamic link with referral code
    private(set) var referralCode: String?
    
    override init() {
        helpURLPath = FeatureManager.text(.codeVerifyHelpCenterPath)
        referralCode = DynamicLinkHelper.getReferralCodeToRegistration()
        super.init()
        
    }
    
    init(promoCode: String?) {
        helpURLPath = FeatureManager.text(.codeVerifyHelpCenterPath)
        referralCode = promoCode
        super.init()
    }
    
    // Check if
    func isCallVerificationEnable() -> Bool {
        return AppParameters.global().isEnabledPhoneVerificationCall()
    }
    
    /// Return the delay to make varification through a call
    func callDelay() -> Int {
        return AppParameters.global().getPhoneVerificationCallDelay()
    }
    
    /// Return the delay to make varification through sms
    func smsDelay() -> Int {
        return AppParameters.global().getPhoneVerificationSMSDelay()
    }
    
    /// Return the delay to make verification through sms
    func whatsAppDelay() -> Int {
        return AppParameters.global().getPhoneVerificationWhatsAppDelay()
    }
    
    /// Return the date of the last phone verification
    func phoneVerificationDate() -> Date{
        if let date = AppParameters.global().getPhoneVerificationSentDate() as Date? {
            return date
        }
        return Date()
    }
    
    func requestNewCode(callback: @escaping (_ error: Error?) -> Void) {
       finishPhoneStepWith(type: requestCodeType, callback: callback)
    }
    
    /// Finish the phone step
    func finishPhoneStepWith(type: RequestCodeType, callback: @escaping (_ error: Error?) -> Void) {
        requestCodeType = type
        
        if case RequestCodeType.sms = type {
            requestSmsCode(callback)
        } else {
            requestWhatsAppCode(callback)
        }
    }
    
    /// request code with SMS
    func requestSmsCode(_ callback: @escaping (_ error: Error?) -> Void) {
        WSConsumer.sendSmsVerification(ddd, phone: phone) { [weak self] (smsDelay, callEnabled, callDelay, error: Error?) in
            if error == nil {
                self?.updateVerificationData(date: Date(), smsDelay: smsDelay, callDelay: callDelay, callEnabled: callEnabled)
            }
            callback(error)
        }
    }
    
    /// request code with WhatsApp
    func requestWhatsAppCode(_ callback: @escaping (_ error: Error?) -> Void) {
        service.requestWhatsAppCode(ddd: ddd, phone: phone) { [weak self] result in
            switch result {
            case .success(let data):
                self?.updateVerificationData(
                    date: Date(),
                    whatsAppDelay: data.whatsAppDelay ?? 0,
                    callDelay: data.callDelay,
                    callEnabled: data.callEnabled
                )
                
                callback(nil)
            case .failure(let error):
                callback(error)
            }
        }
    }
    
    func requestPhoneCall(_ callback: @escaping (_ error: Error?) -> Void){
        WSConsumer.callVerification(ddd, phone: phone) { [weak self] (smsDelay, callEnabled, callDelay, error:Error?) in
            if error == nil {
                self?.updateVerificationData(date: Date(), smsDelay: smsDelay, callDelay: callDelay, callEnabled: callEnabled)
            }
            callback(error)
        }
    }
    
    /// Finish the code verification step
    func finishVerifyPhoneStep(_ callback:@escaping (_ error:Error?) -> Void ){
        WSConsumer.verifyPhoneNumber(code) { [weak self] (ddd:String?, number:String?, error:Error?) in
            if error == nil{
                self?.ddd = ddd!
                self?.phone = number!
            }
            callback(error)
        }
    }
    
    /// check if the string is a valid email
    func isValidEmail(_ testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let isValid = emailTest.evaluate(with: testStr)
        return isValid
    }
    
    /// Finish the email step
    func finishEmailStep(onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        let invalidEmailCode = 5004
        let alreadyRegisteredEmailCode = 5005
        
        api.isValidEmail(email: email) { [weak self] result in
            switch result {
            case .success:
                onSuccess()
            case .failure(let error):
                if error.code == invalidEmailCode || error.code == alreadyRegisteredEmailCode {
                    self?.invalidEmailCounter += 1
                }
                onError(error)
            }
        }
    }

    /// Finish the username step
    func finishNameStep(_ callback:@escaping (_ error:Error?) -> Void) {
        WSConsumer.isValidName(self.firstName.trim() + " " + self.lastName.trim()) { (error:Error?) in
            callback(error)
        }
    }
    
    /// Finishi the password step
    func finishPsswordStep(_ callback:@escaping (_ error:Error?) -> Void) {
        WSConsumer.isValidPass(self.password) { (error:Error?) in
            callback(error)
        }
    }
    
    func finishRegistration(onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        let invalidCpfResponseCode = 5018
        
        api.addConsumer(parameters: addConsumerParameters) { [weak self] result in
            switch result {
            case .success(let response):
                User.updateToken(response.token)
                
                ConsumerManager.shared.loadConsumerDataCached(useCache: false, completion: { (success, error) in
                    DispatchQueue.global(qos: .userInitiated).async {
                        Contacts().identifyPicpayContacts()
                    }
                    self?.updateGlobalData()
                    
                    DispatchQueue.main.async {
                        onSuccess()
                    }
                })
            case .failure(let error):
                if error.code == invalidCpfResponseCode {
                    self?.invalidCpfCounter += 1
                }
                
                Analytics.shared.log(RegistrationEvent.documentError(.errorInserting))
                
                onError(error)
            }
        }
    }
    
    private func updateVerificationData(date: Date, smsDelay: Int = 0, whatsAppDelay: Int = 0, callDelay: Int, callEnabled: Bool) {
        AppParameters.global().setPhoneVerificationSentDate(date)
        AppParameters.global().setPhoneVerificationSMSDelay(smsDelay)
        AppParameters.global().setPhoneVerificationWhatsAppDelay(whatsAppDelay)
        AppParameters.global().setPhoneVerificationCallDelay(callDelay)
        AppParameters.global().setEnabledPhoneVerificationCall(callEnabled)
    }
    
    private func updateGlobalData() {
        // clear all previews authentication data to prevent some glitch
        PPAuth.clearAuthenticationData()
        
        // Stores temporarily the password used to create the account
        // This password will be use for enable touch id
        PPAuthSwift.setTemporaryAuthenticationToken(password, status: PPAuthTempTokenStatusRegister)
        
        // Register events
        sendFinishRegistrationEvents()
        
        //deleting the persisted verified phone number, otherwise the user is gonna be trapped at the green screen.
        AppParameters.global().setVerifiedNumber(nil)
        AppParameters.global().referalCodeBannerDismissDate = Date().addingTimeInterval(12600) // 03:30
        AppParameters.global().setHasAtLeastOneFeedOnce(false)
        AppParameters.global().setOnboardSocial(true)
        
        // Set that the current user need create a user name
        KVStore().setBool(true, with: .isRegisterUsernameStep)
    }
    
    private func sendFinishRegistrationEvents() {
        if ConsumerManager.shared.consumer != nil {
            ThirdParties().identify(isNewUser: true)
        }
        Analytics.shared.log(UserEvent.register)
    }
    
    /// Finish the username step
    func finishUsernameStep(_ attempts: Int, usedSuggestion: Bool, callback: @escaping (_ error: Error?) -> Void) {
        if self.username == "" {
            if let username = ConsumerManager.shared.consumer?.username {
                self.username = username
            }
        }
        WSConsumer.createUsername(username) { [weak self] (error: Error?) in
            callback(error)
            
            PPAnalytics.trackEvent(
                "Cadastrou username",
                properties: [
                    "Tentativas": NSNumber(value: attempts),
                    "Usou sugestão": (usedSuggestion ? "Sim" : "Não")
                ]
            )
            
            ConsumerManager.shared.consumer?.username = self?.username
            KVStore().setBool(false, with: .isRegisterUsernameStep)
        }
    }
    
    /// Load all consumer data
    func loadConsumerData(_ callback: @escaping (_ success:Bool, _ error:Error?) -> Void){
        DispatchQueue.global(qos: .userInitiated).async { 
            ConsumerManager.shared.loadConsumerDataCached(useCache: false, completion: { [weak self] (success, error:Error?) in
                
                if let user = ConsumerManager.shared.consumer,
                    let username = user.username {
                    self?.username = username
                }
                
                DispatchQueue.main.async(execute: { 
                    callback(success, error)
                })
            })
        }
    }
    
    /// Deactivate Account
    func deactivateAccount(pin: String, _ callback: @escaping (_ success:Bool, _ error:Error?) -> Void){
        DispatchQueue.global(qos: .userInitiated).async {
            WSConsumer.deactivate(pin, completionHandler: { (success, error) in
                callback(success, error)
            })
        }
    }
    
    
    /// Return the sms time delay do resend the verification code
    func smsDelayTime() -> String{
        let verificationSentDate = self.phoneVerificationDate()
        let smsDelay = self.smsDelay()
        
        var interval = Int(verificationSentDate.timeIntervalSinceNow)
        interval = smsDelay - (interval * -1)
        
        if interval > 0 {
            return self.timeFormatted(interval)
        }else{
            return ""
        }
    }
    
    /// Return the sms time delay do resend the verification code
    func callDelayTime() -> String{
        let verificationSentDate = self.phoneVerificationDate()
        let callDelay = self.callDelay()
        
        var interval = Int(verificationSentDate.timeIntervalSinceNow)
        interval = callDelay - (interval * -1)
        
        if interval > 0 {
            return self.timeFormatted(interval)
        }else{
            return ""
        }
    }

    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        let hours: Int = totalSeconds / 3600
        if hours > 0 {
            return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        }
        else {
            return String(format: "%02d:%02d", minutes, seconds)
        }
    }
    
    func removeReferralCodeToRegistration() {
        referralCode = nil
        DynamicLinkHelper.removeReferralCodeToRegistration()
    }
    
    func documentFeatureConfiguration() -> DocumentFeature {
        let replacementString = ", \(firstName)"
        
        return DocumentFeature(
            title: RegisterLocalizable.documentStepTitle.text,
            text: String(format: RegisterLocalizable.documentStepDescription.text, replacementString),
            buttonTitle: DefaultLocalizable.learnMore.text
        )
    }
}
