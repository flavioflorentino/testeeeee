import Core
import CoreLegacy
import UI

final class RegistrationUsernameStepViewController: RegistrationStepViewController {
    var codeInUse: String?
    
    private lazy var promoCodeView: PromoCodeUserMoneyView = {
        let view = PromoCodeUserMoneyView()
        view.translatesAutoresizingMaskIntoConstraints = false
        promoCodePlaceHolder?.addSubview(view)
        return view
    }()
    
    private lazy var promoCodeStudent: StudentAccountCodeView = {
        let view = StudentAccountCodeView()
        view.translatesAutoresizingMaskIntoConstraints = false
        promoCodePlaceHolder?.addSubview(view)
        return view
    }()
    
    var attempts: Int = 0
    
    @IBOutlet weak var usernameTextField: UIRoundedTextField!
    @IBOutlet weak var promoCodeButton: UIButton!
    @IBOutlet weak var promoCodePlaceHolder: UIView!
    @IBOutlet weak var promoCodeHeight: NSLayoutConstraint!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // disable the user go back to the first step
        canBack = false
        
        controls.append(usernameTextField)
        usernameTextField.delegate = self
        
        updateContetViewPosition()
        fillPlaceholderWithUsername()
        
        if let code = model?.referralCode {
            validatePromoCode(code)
        }
        
        setupAccessibility(with: [promoCodeButton])
        usernameTextField.setDefaultColors()
        
        NotificationCenter.default.addObserver(self, selector: #selector(showActivatedStudentPromoCode), name: StudentAccountCoordinator.completedSignUp, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateContetViewPosition()
        usernameTextField.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateContetViewPosition()
    }
    
    // MARK: - User Actions
    
    /**
     Save data and finish the step
     */
    @IBAction private func finishStep( _ sender: UIButton?) {
        self.validate(true)
        
        if valid {
            startLoading()
            saveFormData()
            
            // check if user has tap a new useranem
            let usedSuggestion: Bool = !usernameTextField.text.isEmpty
            attempts += 1
            
            // send data to server
            model?.finishUsernameStep(attempts, usedSuggestion: usedSuggestion, callback: { [weak self] error in
                DispatchQueue.main.async(execute: {
                    self?.stopLoading()
                    
                    if error == nil {
						if ConsumerManager.shared.consumer != nil {
                            ConsumerManager.shared.loadConsumerDataCached(useCache: false, completion: nil)
						}
                        self?.finishRegistration()
                    } else {
                        if let err = error as NSError?, err.code == 5050 {
                            self?.finishRegistration()
                        }
						
                        if let text = error?.localizedDescription {
                            self?.showMessage(text)
                        }
                    }
                })
            })
        }
    }
    
    /// Skip the username set up
    @IBAction private func skip( _ sender: Any) {
        finishRegistration()
    }
    
    // MARK: Promotional Code
    @IBAction private func insertPromoCode(_ sender: Any) {
        var promoCodeTextField: UITextField?
        let alert = UIAlertController(title: RegisterLocalizable.enterYourCode.text, message: RegisterLocalizable.yourPromotionalCode.text, preferredStyle: .alert)
        alert.addTextField { textField in
            textField.placeholder = RegisterLocalizable.promotionalCode.text
            textField.text = self.model?.referralCode
            promoCodeTextField = textField
        }
        alert.addAction(UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: DefaultLocalizable.enable.text, style: .default, handler: { [weak self] _ in
            if let code = promoCodeTextField?.text {
                self?.validatePromoCode(code)
                PromoCodeAnalytics.send(.actionActivate(code))
            }
        }))
        present(alert, animated: true, completion: nil)
        
        PromoCodeAnalytics.send(.actionInUse(.signUp))
    }
    
    /// Show the first app screen
    fileprivate func finishRegistration() {        
        KVStore().setBool(false, with: .isRegisterUsernameStep)
        launchOnboarding()     
    }
    
    private func launchOnboarding() {
        guard let navigationController = navigationController else {
            return
        }
        let onboardingCoordinator = OnboardingFlowCoordinator(
            navigationController: navigationController,
            dependencies: DependencyContainer()
        )
        onboardingCoordinator.start()
    }
    
    // MARK: - Internal Methods
    
    /**
     Fill the placeholder with the username
     OBS.: If the username is not load then load the consumer data
    */
    fileprivate func fillPlaceholderWithUsername() {
        if self.model!.username.isEmpty {
            self.startLoading()
            
            self.model?.loadConsumerData { [weak self] success, _ in
                self?.stopLoading()
                if success {
                    self?.usernameTextField.text = self!.model!.username
                }
            }
        } else {
            self.usernameTextField.text = self.model!.username
        }
    }
    
    override func firstTextField() -> UITextField? {
        return self.usernameTextField
    }
    
    /// Validate the form
    func validate( _ showMessage: Bool = false) {
        let isValid = true
        hideMessage()
        usernameTextField.hasError = false
        valid = isValid
    }
    
    func saveFormData() {
        self.model!.username = "\(self.usernameTextField.text!)"
    }
    
    func showInvalidPromoCode(_ error: Error) {
        let alert = Alert(title: "Ops!", text: error.localizedDescription)
        let insertCodeButton = Button(title: RegisterLocalizable.useAnotherCode.text, type: .cta) { [weak self] popupController, _ in
            self?.insertPromoCode("")
            popupController.dismiss(animated: true)
        }
        let cancelButton = Button(title: DefaultLocalizable.btCancel.text, type: .clean, action: .close)
        alert.buttons = [insertCodeButton, cancelButton]
        
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func validatePromoCode( _ code: String) {
        codeInUse = code
        self.startLoading()
        PromoCodeWorker.validate(code) { [weak self] result in
            DispatchQueue.main.async {
                self?.stopLoading()
                
                switch result {
                case .success(let promoCode):
                    AppParameters.global().referalCodeBannerDismissDate = nil
                    self?.showPromoCodeInfo(promoCode.data)
                case .failure(let error):
                    PromoCodeAnalytics.send(.actionError(error.description))
                    self?.model?.removeReferralCodeToRegistration()
                    self?.showInvalidPromoCode(error)
                }
            }
        }
    }
    
    private func startStudentAccount() {
        StudentAccountCoordinator(from: self).start(from: .signUp)
    }
    
    private func showPromoCodeInfo(_ promoData: PPPromoCode.PromoCodeData) {
        switch promoData {
        case .inviterReward(let code):
            showActivatedPromoCode(rewardPromoCode: code)
        case .couponWeb(let couponWeb):
            showCouponWeb(coupon: couponWeb)
        case .studentAccount:
            startStudentAccount()
        case .unknown:
            AlertMessage.showAlert(withMessage: SettingsLocalizable.codeUsedWithSuccess.text, controller: self)
        }
    }
    
    private func showCouponWeb(coupon: CouponWebPromoCodeData) {
        ViewsManager.presentWebViewController(withUrl: coupon.url, fromViewController: self)
    }
    
    @objc
    func showActivatedStudentPromoCode() {
        promoCodeButton.isHidden = true
        setupConstraintsForCodeTitleAnd(promoCodeView: promoCodeStudent)
        updateContetViewPosition()
        promoCodeStudent.update(with: codeInUse ?? "")
        setupAccessibility()
    }
    
    func showActivatedPromoCode(rewardPromoCode: PPRegisterRewardPromoCode) {
        if let url = rewardPromoCode.webviewUrl {
            presentWebView(with: url)
        } else if let contact = rewardPromoCode.inviter {
            showInvite(from: contact)
        }

        model?.removeReferralCodeToRegistration()
        promoCodeButton.isHidden = true
        promoCodeView.configure(rewardPromoCode, placeholder: PPContact.photoPlaceholder())
        
        setupConstraintsForCodeTitleAnd(promoCodeView: promoCodeView)
        updateContetViewPosition()
        setupAccessibility()
    }
    
    private func presentWebView(with url: String) {
        ViewsManager.presentWebViewController(withUrl: url, fromViewController: self)
    }
    
    private func showInvite(from contact: PPContact) {
        let popup = ProfilePromoCodeFactory.make(contact: contact, wsId: ConsumerManager.shared.consumer?.wsId)
        popup.modalPresentationStyle = .overCurrentContext
        #if swift(>=5.1)
        if #available(iOS 13.0, *) {
            popup.isModalInPresentation = true
        }
        #endif
        present(popup, animated: true)
    }
    
    private func setupConstraintsForCodeTitleAnd(promoCodeView: UIView) {
        NSLayoutConstraint.deactivate([promoCodeHeight])
        
        guard let promoCode = promoCodePlaceHolder else {
            return            
        }
        
        NSLayoutConstraint.activate([
            promoCodeView.topAnchor.constraint(equalTo: promoCode.topAnchor),
            promoCodeView.leadingAnchor.constraint(equalTo: promoCode.leadingAnchor, constant: 1),
            promoCodeView.trailingAnchor.constraint(equalTo: promoCode.trailingAnchor, constant: -1),
            promoCodeView.bottomAnchor.constraint(equalTo: promoCode.bottomAnchor)
        ])
    }
}

extension RegistrationUsernameStepViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // Workaround to the textfield jump
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == usernameTextField {
            self.finishStep(nil)
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let acceptableCharacters = "abcdefghijklmnopqrstuvwxyz0123456789_."
        let cs = CharacterSet(charactersIn: acceptableCharacters).inverted
        let filtered = (string.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
        
        self.validate()
        
        return string == filtered
    }
}
