import AnalyticsModule
import UIKit

final class RegistrationEmailStepViewController: RegistrationStepViewController {
    @IBOutlet weak var emailTextField: UIRoundedTextField!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.controls.append(emailTextField)
        self.emailTextField.delegate = self
        
        if !UIAccessibility.isVoiceOverRunning {
            emailTextField.becomeFirstResponder()
        }
        
        emailTextField.setDefaultColors()
        
        updateContetViewPosition()
        setupAccessibility()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        populateForm()
        emailTextField.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        PPAnalytics.trackEvent("cadastro_email", properties: nil)
        
        emailTextField.layoutIfNeeded()
    }
    
    // MARK: - User Actions
    
    @IBAction private func showBackAlertConfirm( _ sender: Any) {
        let alert = UIAlertController(title: nil, message: RegisterLocalizable.alertTextBackToPhoneStep.text, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: DefaultLocalizable.btYes.text, style: .default) { [weak self] _ in
            self?.back(sender)
        }
        let cancelAction = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        alert.addAction(yesAction)
        present(alert, animated: true, completion: nil)
    }
    
    /// Save data and finish the step
    @IBAction private func finishStep( _ sender: UIButton?) {
        guard validate(true) else {
            return
        }
        startLoading()
        saveFormData()
        
        model?.finishEmailStep(onSuccess: { [weak self] in
            self?.stopLoading()
            self?.sendToNextStep()
        }, onError: { [weak self] error in
            self?.stopLoading()
            self?.showMessage(error.localizedDescription)
        })
    }
    
    // MARK: - Internal Methods
    
    /// populate form with model data
    func populateForm() {
        guard let model = model else {
            return
        }
        emailTextField.text = model.email
    }
    
    /// Get data from form and save at model
    func saveFormData() {
        model?.email = "\(self.emailTextField.text!)"
    }
    
    /// Send to nextStep
    func sendToNextStep() {
        Analytics.shared.log(RegistrationEvent.email)
        if let controller = ViewsManager.registrationStoryboardViewController(withIdentifier: "RegistrationPasswordStep") as? RegistrationPasswordStepViewController {
            controller.setup(self.model!)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    /// Validate the form
    @discardableResult
    func validate( _ showMessage: Bool = false) -> Bool {
        var isValid = true
        
        let email = "\(self.emailTextField.text!)"
        
        guard let model = model else {
            return false
        }
        if !model.isValidEmail(email) {
            isValid = false
            if showMessage {
                self.showMessage(RegisterLocalizable.invalidEmail.text, show: showMessage)
                self.emailTextField.hasError = true
            }
        } else {
            self.hideMessage()
            self.emailTextField.hasError = false
        }
        
        self.valid = isValid
        return isValid
    }
}

extension RegistrationEmailStepViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // Workaround to the textfield jump
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            self.finishStep(nil)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.validate()
        return true
    }
}
