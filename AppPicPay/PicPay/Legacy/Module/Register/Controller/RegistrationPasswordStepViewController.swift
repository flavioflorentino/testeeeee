import AnalyticsModule
import UIKit

final class RegistrationPasswordStepViewController: RegistrationStepViewController {
    @IBOutlet weak var passwordTextField: UIRoundedTextField!
    @IBOutlet weak var passwordVisibilityButton: UIButton!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.controls.append(passwordTextField)
        self.passwordTextField.delegate = self
        
        if !UIAccessibility.isVoiceOverRunning {
            passwordTextField.becomeFirstResponder()
        }
        
        passwordTextField.setDefaultColors()
        
        updateContetViewPosition()
        setupAccessibility()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        passwordTextField.layoutIfNeeded()
        updateContetViewPosition()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        PPAnalytics.trackEvent("Cadastro senha", properties: nil)
    }
    
    // MARK: - User Action
    
    @IBAction private func togglePassworVisibility(_ sender: AnyObject) {
        self.passwordTextField.isSecureTextEntry = !self.passwordTextField.isSecureTextEntry
        updateAccessibilityAttributes()
    }
    
    /// Save data and finish the step
    @IBAction private func finishStep( _ sender: UIButton?) {
        let isValid = validate(true)
        
        if isValid {
            startLoading()
            saveFormData()
            model?.finishPsswordStep { [weak self] error in
                DispatchQueue.main.async(execute: {
                    self?.stopLoading()
                    if error == nil {
                        self?.goToNextStep()
                    } else {
                        if let text = error?.localizedDescription {
                            self?.showMessage(text)
                        }
                    }
                })
            }
        }
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.back(sender)
    }
    
    // MARK: - Internal Methods
    
    func goToNextStep() {
        Analytics.shared.log(RegistrationEvent.password)
        if let controller = ViewsManager.registrationStoryboardViewController(withIdentifier: "RegistrationCPFStep") as? RegistrationDocumentStepViewController {
            controller.setup(self.model!)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }

    /// Get data from form and save at model
    func saveFormData() {
        model?.password = "\(self.passwordTextField.text!)"
    }
    
    /// Validate the form
    @discardableResult
    func validate(_ showMessage: Bool = false) -> Bool {
        var isValid = true
        
        let password = "\(self.passwordTextField.text!)"
        if password.length < 4 {
            isValid = false
            if showMessage {
                self.showMessage(RegisterLocalizable.invalidPassword.text, show: showMessage)
                self.passwordTextField.hasError = true
            }
        } else {
            hideMessage()
            passwordTextField.hasError = false
        }
        
        valid = isValid
        return isValid
    }
}

extension RegistrationPasswordStepViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // Workaround to the textfield jump
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == passwordTextField {
            self.finishStep(nil)
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let acceptableCharacters = "0123456789"
        let cs = CharacterSet(charactersIn: acceptableCharacters).inverted
        let filtered = (string.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
        
        self.validate()
        
        return string == filtered
    }
}

extension RegistrationPasswordStepViewController {
    // MARK: - Methods Accessibility
    
    override func updateAccessibilityAttributes() {
        super.updateAccessibilityAttributes()
        
        passwordVisibilityButton.accessibilityLabel = RegisterLocalizable.showPassword.text
        
        if !passwordTextField.isSecureTextEntry {
            passwordVisibilityButton.accessibilityLabel = RegisterLocalizable.hidePassword.text
        }
    }
}
