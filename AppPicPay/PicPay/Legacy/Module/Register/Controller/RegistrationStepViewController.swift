import UI
import UIKit

class RegistrationStepViewController: PPBaseViewController {
    var step: Int = 0
    var valid: Bool = true
    var loading: Bool = false
    var model: RegistrationViewModel?
    var canBack: Bool = true
    
    var controls: [UIControl?] = [UIControl]()
    
    fileprivate var nextButtonTitle: String = ""
    
    // MARK: - Config View Apperance
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    @IBOutlet weak var scrollView: UIScrollView?
    @IBOutlet weak var backButton: UIButton?
    @IBOutlet weak var contentView: UIView?
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var textLabel: UILabel?
    @IBOutlet weak var messageLabel: UILabel?
    @IBOutlet weak var nextButton: UIPPButton?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
    @IBOutlet weak var contentViewTopConstraint: NSLayoutConstraint?
    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint?
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        self.activityIndicator?.isHidden = true
        self.messageLabel?.text = ""
        
        if let title = nextButton?.titleLabel?.text {
            self.nextButtonTitle = title
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tapGesture)
        
        super.viewDidLoad()
        
        view.backgroundColor = Palette.ppColorGrayscale100.color
        contentView?.backgroundColor = Palette.ppColorGrayscale100.color
        scrollView?.backgroundColor = Palette.ppColorGrayscale100.color
        backButton?.setTitleColor(Palette.ppColorGrayscale400.color(withCustomDark: .ppColorGrayscale000), for: .normal)
        textLabel?.textColor = Palette.ppColorGrayscale400.color(withCustomDark: .ppColorGrayscale200)
        
        // Keyboard events observers
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillOpen), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        configureDefaultButtonStyle(nextButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = self.canBack
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    // MARK: - User Actions
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Dependence Injection
    
    @objc
    func setup(_ model: RegistrationViewModel) {
        self.model = model
    }
    
    // MARK: - Internal Methods
    
    func startLoading() {
        loading = true
        
        if let title = self.nextButton?.titleLabel?.text {
            nextButtonTitle = title
        }
        
        self.activityIndicator?.isHidden = false
        self.activityIndicator?.startAnimating()
        UIAccessibility.post(notification: .announcement, argument: RegisterLocalizable.loading.text)
        
        self.nextButton?.setTitle("", for: .disabled)
        self.nextButton?.isEnabled = false
        
        //disable all controlls
        for control in controls {
            control?.isEnabled = false
        }
        
        UIView.animate(withDuration: 0.5) {
            for control in self.controls {
                control?.layer.opacity = 0.35
            }
        }
    }
    
    func stopLoading() {
        loading = false
        self.nextButton?.isEnabled = valid
        self.nextButton?.setTitle(nextButtonTitle, for: .disabled)
        self.activityIndicator?.stopAnimating()
        self.activityIndicator?.isHidden = true
        
        //enable all controlls
        for control in controls {
            control?.isEnabled = true
        }
        
        UIView.animate(withDuration: 0.5) {
            for control in self.controls {
                control?.layer.opacity = 1.0
            }
        }
    }
    
    @objc
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    /// show the messagem on screen
    func showMessage( _ text: String, show: Bool = true) {
        if show {
            messageLabel?.isHidden = false
            messageLabel?.text = text + "\n"
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                UIAccessibility.post(notification: .announcement, argument: text)
            }
        }
    }
    
    func hideMessage() {
        messageLabel?.text = ""
        messageLabel?.isHidden = true
    }
    
    func configureDefaultButtonStyle(_ button: UIPPButton?) {
        guard let button = button else {
            return
        }
        
        button.highlightedTitleColor = Palette.white.color
        button.highlightedBackgrounColor = Palette.ppColorBranding300.color.withAlphaComponent(0.5)
        button.borderWidth = 0
        button.borderColor = .clear
        button.disabledBorderColor = .clear
        button.normalTitleColor = Palette.white.color
        button.disabledTitleColor = Palette.white.color.withAlphaComponent(0.5)
        button.normalBackgrounColor = Palette.ppColorBranding300.color
        button.disabledBackgrounColor = Palette.ppColorBranding300.color.withAlphaComponent(0.5)
        button.cornerRadius = 20
    }
    
    func configureSecondaryButtonStyle(_ button: UIPPButton?) {
        guard let button = button else {
            return
        }
        
        button.highlightedTitleColor = Palette.ppColorBranding300.color.withAlphaComponent(0.5)
        button.highlightedBackgrounColor = .clear
        button.borderWidth = 1
        button.borderColor = Palette.ppColorBranding300.color
        button.disabledBorderColor = Palette.ppColorBranding300.color
        button.normalTitleColor = Palette.ppColorBranding300.color
        button.disabledTitleColor = Palette.ppColorBranding300.color.withAlphaComponent(0.5)
        button.normalBackgrounColor = .clear
        button.disabledBackgrounColor = .clear
        button.cornerRadius = 20
    }
    
    /// Update the content view position on vertical center
    func updateContetViewPosition(withDuration duration: Double = 0.25) {
        if let content = contentView, let scroll = scrollView {
            let heightForAdjust: CGFloat = 40.0
            var minTopDistance: CGFloat = 20.0
            
            // Special size for 5s <=
            if  UIScreen.main.bounds.size.height <= 568 {
                minTopDistance = 0.0
            }
            
            // -40.0 = status bar visual compensation
            let scrollHeight = CGFloat(scroll.frame.size.height - heightForAdjust) / 2.0
            let contentHeight = CGFloat(content.frame.height) / 2.0
            let top = max(CGFloat(scrollHeight - contentHeight), minTopDistance)
            
            // center the content view
            if top != contentViewTopConstraint?.constant {
                scroll.layoutIfNeeded()
                contentViewTopConstraint?.constant = top
                
                UIView.animate(withDuration: duration) {
                    scroll.layoutIfNeeded()
                }
            }
        }
    }
    
    /// Return the first text field
    func firstTextField() -> UITextField? {
        return nil
    }
    
    func registrationSteOpenLink(url: URL) {
        if url.absoluteString == "terms" && self.navigationController!.navigationBar.isHidden {
            navigationController?.setNavigationBarHidden(false, animated: false)
            ViewsManager.pushWebViewController(withUrl: WebServiceInterface.apiEndpoint(WsWebViewURL.termsOfUse.endpoint), fromViewController: self, title: SettingsLocalizable.termsOfUse.text)
        }
        if url.absoluteString == "privacy_policy" && self.navigationController!.navigationBar.isHidden {
            navigationController?.setNavigationBarHidden(false, animated: false)
            ViewsManager.pushWebViewController(withUrl: WebServiceInterface.apiEndpoint(WsWebViewURL.privacyPolicy.endpoint), fromViewController: self, title: SettingsLocalizable.privacyPolicy.text)
        }
    }
    
    // MARK: - Keyboard Events Handler
    
    @objc
    func keyboardWillOpen(_ keyboardNotification: Notification) {
        if  let animationDuration = ((keyboardNotification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey]) as? NSNumber)?.doubleValue,
            let keyboardRectValue = (keyboardNotification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            // animate the view to adjust height with the opened keyboard
            self.view.layoutIfNeeded()
            self.viewBottomConstraint?.constant = keyboardRectValue.height
            
            UIView.animate(withDuration: animationDuration) {
                self.view.layoutIfNeeded()
            }
        
            updateContetViewPosition(withDuration: animationDuration)
                
            // Hide the back button for 5s <=
            if UIScreen.main.bounds.size.height <= 568 {
                UIView.animate(withDuration: animationDuration) {
                    self.backButton?.layer.opacity = 0.0
                }
            }
        }
    }
    
    @objc
    func keyboardWillHide( _ keyboardNotification: Notification) {
        if  let animationDuration = ((keyboardNotification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey]) as? NSNumber)?.doubleValue {
            // animate the view to adjust height with the opened keyboard
            self.view.layoutIfNeeded()
            self.viewBottomConstraint?.constant = 0.0
            
            UIView.animate(withDuration: animationDuration) {
                self.view.layoutIfNeeded()
            }
            
            updateContetViewPosition(withDuration: animationDuration)
            
            // Show the back button for 5s <=
            if UIScreen.main.bounds.size.height <= 568 {
                UIView.animate(withDuration: animationDuration) {
                    self.backButton?.layer.opacity = 1.0
                }
            }
        }
    }
}

extension RegistrationStepViewController {
    // MARK: - Methods Accessibility
    
    public func setupAccessibility(with views: [UIView]? = nil) {
        if let backButton = backButton, let contentView = contentView {
            view.accessibilityElements = [backButton, contentView]
        }
        
        if let elementsView = views {
            view.accessibilityElements?.append(contentsOf: elementsView)
        }
        
        backButton?.isAccessibilityElement = true
        backButton?.accessibilityTraits = .button
        
        titleLabel?.isAccessibilityElement = true
        titleLabel?.accessibilityTraits = .staticText
        
        textLabel?.isAccessibilityElement = true
        textLabel?.accessibilityTraits = .staticText
        
        nextButton?.isAccessibilityElement = true
        nextButton?.accessibilityTraits = .button
        
        updateAccessibilityAttributes()
    }
    
    @objc
    public func updateAccessibilityAttributes() { }
}
