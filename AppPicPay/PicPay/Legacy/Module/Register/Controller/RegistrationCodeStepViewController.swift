import AnalyticsModule
import CustomerSupport
import UI
import UIKit
import FeatureFlag

// swiftlint:disable type_body_length
final class RegistrationCodeStepViewController: RegistrationStepViewController {
    struct Layout {
        static let alphaComponent: CGFloat = 0.5
        static let secondariesTextsFontSize: CGFloat = 12
        static let imageEdgeInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
    }
    
    var timer: Timer?
    
    @IBOutlet weak var codeTextField: UIRoundedTextField!
    @IBOutlet weak var smsSendButton: UIButton!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var helpButton: UIButton!
    @IBOutlet weak var orLabel: UILabel!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateVerficationButton()
        timer?.fire()
        
        appendControls()
        
        codeTextField.delegate = self
        codeTextField.setDefaultColors()
        
        if !UIAccessibility.isVoiceOverRunning {
            codeTextField.becomeFirstResponder()
        }
        
        updateContetViewPosition()
        
        if model?.isCallVerificationEnable() == false {
            setupAccessibility(with: [smsSendButton])
        } else {
            setupAccessibility(with: [callButton, smsSendButton])
        }
        
        configureViews()
        configureTextLabel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Analytics.shared.log(RegistrationEvent.phoneCodeVerification)

        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateVerficationButton), userInfo: nil, repeats: true)
        timer?.fire()
        
        updateVerficationButton()
        updateContetViewPosition()
    }
    
    override func viewWillDisappear( _ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
    }
    
    // MARK: - User Actions
    
    @IBAction private func sendSMSCode(_ sender: AnyObject) {
        let title = "alert_phone_confirm".localized()
        let msg = "alert_phone_confirm_txt".localized(self.model!.ddd, self.model!.phone)
        
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: DefaultLocalizable.btOk.text, style: .default, handler: { [weak self] _ in
            Analytics.shared.log(RegistrationEvent.phoneConfirmation(.ok))
            self?.sendSMSCodeRequest()
        })
        alert.addAction(okAction)
        let cancelAction = UIAlertAction(title: DefaultLocalizable.btEdit.text, style: .cancel, handler: { [weak self] _ in
            Analytics.shared.log(RegistrationEvent.phoneConfirmation(.edit))
            self?.back(sender)
        })
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
        
        Analytics.shared.log(RegistrationEvent.smsConfirmation(.resendCode))
    }
    
    @IBAction private func callToPhone(_ sender: AnyObject) {
        let title = "alert_phone_confirm".localized()
        let msg = "alert_phone_confirm_txt".localized(self.model!.ddd, self.model!.phone)
        
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: DefaultLocalizable.btOk.text, style: .default, handler: { [weak self] _ in
            Analytics.shared.log(RegistrationEvent.phoneConfirmation(.ok))
            self?.callToPhoneRequest()
        })
        alert.addAction(okAction)
        let cancelAction = UIAlertAction(title: DefaultLocalizable.btEdit.text, style: .cancel, handler: { [weak self] _ in
            Analytics.shared.log(RegistrationEvent.phoneConfirmation(.edit))
            self?.back(sender)
        })
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: { })
    }
    
    /// Save data and finish the step
    @IBAction private func finishStep( _ sender: UIButton?) {
        self.validate(true)
        
        if valid {
            self.startLoading()
            
            self.model?.finishVerifyPhoneStep { [weak self] error in
                DispatchQueue.main.async(execute: {
                    self?.stopLoading()
                    if error != nil {
                        if let text = error?.localizedDescription {
                            self?.showMessage(text)
                        }
                        
                        Analytics.shared.log(RegistrationEvent.codeError)
                    } else {
                        self?.goToNextStep()
                    }
                })
            }
        }
    }
    
    override func back(_ sender: Any) {
        Analytics.shared.log(RegistrationEvent.smsConfirmation(.editNumber))
        super.back(sender)
    }
    
    @objc
    func showHelp() {
        Analytics.shared.log(RegistrationEvent.smsConfirmation(.help))

        let chatbotUrlPath = FeatureManager.shared.text(.chatbotUnlogedUser)
        let chatbotViewController = ChatBotFactory.make(urlPath: chatbotUrlPath)
        let navigation = UINavigationController(rootViewController: chatbotViewController)
        present(navigation, animated: true)
    }
    
    // MARK: - Internal Methods
    
    override func firstTextField() -> UITextField? {
        return self.codeTextField
    }
    
    /**
     Validate the form
     */
    func validate( _ showMessage: Bool = false) {
        var isValid = true
        
        self.model!.code = "\(self.codeTextField.text!)"

        if self.model!.code.count < 4 {
            self.showMessage(RegisterLocalizable.invalidCode.text, show: showMessage)
            if showMessage {
                self.codeTextField.hasError = true
            }
            isValid = false
        } else {
            self.hideMessage()
            self.codeTextField.hasError = false
        }
        
        self.valid = isValid
        self.nextButton?.isEnabled = isValid
    }
    
    /**
     Update the time / enable the sms and call buttons
    */
    @objc
    func updateVerficationButton() {
        // SMS button
        let delay = self.model!.smsDelayTime()
        if !delay.isEmpty {
            // disble button
            smsSendButton.isEnabled = false
            smsSendButton.setTitle("bt_resend_sms_delay".localized(delay), for: .disabled)
        } else if loading {
            smsSendButton.isEnabled = false
            smsSendButton.setTitle("bt_resend_sms".localized(), for: .disabled)
        } else {
            // enable call buton
            smsSendButton.isEnabled = true
            smsSendButton.setTitle("bt_resend_sms".localized(), for: .normal)
        }
        
        // Call Buttton
        if self.model!.isCallVerificationEnable() {
            let callDelay = self.model!.callDelayTime()
            if !callDelay.isEmpty {
                // disble button
                callButton.isEnabled = false
                callButton.setTitle("bt_call_me_delay".localized(callDelay), for: .disabled)
            } else if loading {
                callButton.isEnabled = false
                callButton.setTitle("bt_call_me".localized(), for: .disabled)
            } else {
                // enable call buton
                callButton.isEnabled = true
                callButton.setTitle("bt_call_me".localized(), for: .normal)
            }
        } else {
            callButton.isEnabled = false
            callButton.isHidden = true
        }
    }
    
    /**
     Send the mobile phone to server
     */
    func sendSMSCodeRequest() {
        Analytics.shared.log(RegistrationEvent.verificationOption(.resendSMS))
        
        startLoading()
        
        smsSendButton.isEnabled = false
        model?.requestNewCode { [weak self] error in
            DispatchQueue.main.async(execute: {
                self?.stopLoading()
                self?.updateVerficationButton()
                if error != nil {
                    if let text = error?.localizedDescription {
                        self?.showMessage(text)
                    }
                }
            })
        }
    }
    
    /**
     Send the mobile phone to server
     */
    func callToPhoneRequest() {
        Analytics.shared.log(RegistrationEvent.verificationOption(.receiveCall))
        
        startLoading()
        
        smsSendButton.isEnabled = false
        model?.requestPhoneCall { [weak self] error in
            DispatchQueue.main.async(execute: {
                guard let strongSelf = self else {
                    return
                }
                strongSelf.stopLoading()
                strongSelf.updateVerficationButton()
                if error != nil {
                    if let text = error?.localizedDescription {
                        strongSelf.showMessage(text)
                    }
                } else {
                    AlertMessage.showAlert(withMessage: "msg_call_sucess".localized(), controller: strongSelf)
                }
            })
        }
    }

    func goToNextStep() {
        Analytics.shared.log(RegistrationEvent.phoneCode)
        if let controller = ViewsManager.registrationStoryboardViewController(withIdentifier: "RegistrationEmailStep") as? RegistrationEmailStepViewController {
            controller.setup(self.model!)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func configureTextLabel() {
        let ddd = model?.ddd ?? "00"
        let phone = model?.phone ?? "00000-0000"
        let fullPhone = "(\(ddd)) \(phone)"
        let information = String(format: RegisterLocalizable.codeStepPhoneTextLabel.text, fullPhone)
        
        if let currentPhoneRange = information.range(of: fullPhone), let fontBold = textLabel?.font?.bold() {
            let range = NSRange(currentPhoneRange, in: information)
            let attributedString = NSMutableAttributedString(string: information)

            attributedString.addAttributes([NSAttributedString.Key.font: fontBold], range: range)
            textLabel?.attributedText = attributedString
        }
    }
    
    private func configureViews() {
        configureSmsSendButton()
        configureCallButton()
        configureEditButton()
        configureHelpButton()
        configureOrLabel()
    }
    
    private func configureSmsSendButton() {
        smsSendButton.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        smsSendButton.setTitleColor(Palette.ppColorBranding300.color.withAlphaComponent(Layout.alphaComponent), for: .disabled)
        smsSendButton.titleLabel?.font = .systemFont(ofSize: Layout.secondariesTextsFontSize)
        smsSendButton.setImage(Assets.NewGeneration.iconMessage.image, for: .normal)
        smsSendButton.imageEdgeInsets = Layout.imageEdgeInsets
        smsSendButton.addTarget(self, action: #selector(sendSMSCode(_:)), for: .touchUpInside)
    }
    
    private func configureCallButton() {
        callButton.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        callButton.setTitleColor(Palette.ppColorBranding300.color.withAlphaComponent(Layout.alphaComponent), for: .disabled)
        callButton.titleLabel?.font = .systemFont(ofSize: Layout.secondariesTextsFontSize)
        callButton.setImage(Assets.NewGeneration.iconPhone.image, for: .normal)
        callButton.imageEdgeInsets = Layout.imageEdgeInsets
        callButton.addTarget(self, action: #selector(callToPhone(_:)), for: .touchUpInside)
    }
    
    private func configureEditButton() {
        let editButtonText = NSAttributedString(string: "<u>\(RegisterLocalizable.editPhone.text)</u>").underlinefy()
        editButton.tintColor = Palette.ppColorGrayscale400.color
        editButton.titleLabel?.font = .systemFont(ofSize: Layout.secondariesTextsFontSize)
        editButton.setAttributedTitle(editButtonText, for: .normal)
        editButton.addTarget(self, action: #selector(back(_:)), for: .touchUpInside)
    }
    
    private func configureHelpButton() {
        let helpButtonText = NSAttributedString(string: "<u>\(RegisterLocalizable.needHelp.text)</u>").underlinefy()
        helpButton.tintColor = Palette.ppColorGrayscale400.color
        helpButton.titleLabel?.font = .systemFont(ofSize: Layout.secondariesTextsFontSize)
        helpButton.setAttributedTitle(helpButtonText, for: .normal)
        helpButton.addTarget(self, action: #selector(showHelp), for: .touchUpInside)
    }
    
    private func configureOrLabel() {
        orLabel.text = DefaultLocalizable.or.text
        orLabel.textColor = Palette.ppColorGrayscale400.color
        orLabel.font = .systemFont(ofSize: Layout.secondariesTextsFontSize)
    }
    
    private func appendControls() {
        controls.append(codeTextField)
        controls.append(smsSendButton)
        controls.append(callButton)
        controls.append(editButton)
        controls.append(helpButton)
    }
}

extension RegistrationCodeStepViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.codeTextField {
            self.finishStep(nil)
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.codeTextField {
            self.validate()
            
            // jump to the next field when the ddd is completed
            let newCode = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            if newCode.count >= 4 {
                let range: Range<String.Index> = newCode.startIndex..<newCode.index(newCode.startIndex, offsetBy: 4)
                self.codeTextField.text = String(newCode[range])
                self.finishStep(nil)
                return false
            }
        }
        
        return true
    }
}
