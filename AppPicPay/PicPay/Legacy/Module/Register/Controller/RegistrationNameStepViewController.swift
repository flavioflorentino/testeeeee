import AnalyticsModule
import UI
import UIKit

final class RegistrationNameStepViewController: RegistrationStepViewController {
    @IBOutlet weak var firstNameTextField: UIRoundedTextField!
    @IBOutlet weak var lastNameTextField: UIRoundedTextField!
    @IBOutlet weak var bottomTextView: UITextView!
    @IBOutlet weak var greetingTitle: UILabel!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.controls.append(firstNameTextField)
        self.controls.append(lastNameTextField)
        
        self.firstNameTextField.delegate = self
        self.lastNameTextField.delegate = self
        
        firstNameTextField.setDefaultColors()
        lastNameTextField.setDefaultColors()
        
        self.firstNameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.lastNameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        self.bottomTextView.layoutIfNeeded()
		
		self.updateGreetings()
        
        // workaround to prevent first touch error
        self.contentView?.layer.opacity = 0.0
        UIView.animate(withDuration: 0.5) {
            self.contentView?.layer.opacity = 1.0
        }
        bottomTextView.isSelectable = true
        
        // add links recognizer
        if let attr = bottomTextView.attributedText.mutableCopy() as? NSMutableAttributedString {
            attr.setAsLink(RegisterLocalizable.termsOfService.text, linkURL: "terms")
            attr.setAsLink(RegisterLocalizable.privacyPolicy.text, linkURL: "privacy_policy")
            bottomTextView.attributedText = attr
            bottomTextView.delegate = self
            bottomTextView.isUserInteractionEnabled = true
            bottomTextView.delaysContentTouches = false
            bottomTextView.isSelectable = true
            bottomTextView.linkTextAttributes = [.foregroundColor: Palette.ppColorBranding300.color]
            bottomTextView.textColor = Palette.ppColorGrayscale400.color(withCustomDark: .ppColorGrayscale200)
        }
        
        if !UIAccessibility.isVoiceOverRunning {
            firstNameTextField.becomeFirstResponder()
        }
        
        updateContetViewPosition()
        firstNameTextField.layoutIfNeeded()
        lastNameTextField.layoutIfNeeded()
        setupAccessibility()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        PPAnalytics.trackEvent("Cadastro nome", properties: nil)
    }

    // MARK: - Internal Methods
    
    /// Save data and finish the step
    @IBAction private func finishStep( _ sender: UIButton?) {
        let isValid = validate(true)
        
        if isValid {
            startLoading()
            saveFormData()

            model?.finishNameStep { [weak self] error in
                DispatchQueue.main.async(execute: {
                    self?.stopLoading()
                    if let text = error?.localizedDescription {
                        self?.showMessage(text)
                    } else {
                        Analytics.shared.log(RegistrationEvent.name)
                        self?.sendToNextStep()
                    }
                })
            }
        }
    }
    
    /// Get data from form and save at model
    func saveFormData() {
        self.model!.firstName = "\(self.firstNameTextField.text!)"
        self.model!.lastName = "\(self.lastNameTextField.text!)"
    }
    
    /// Send to nextStep
    func sendToNextStep() {
        if let controller = ViewsManager.registrationStoryboardViewController(withIdentifier: "RegistrationPhoneStep") as? RegistrationStepViewController {
            controller.setup(self.model!)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    /// Update greetings title message with user name
    func updateGreetings() {
		let firstName = self.firstNameTextField.text!.trim()
		let lastName = self.lastNameTextField.text!.trim()
		let nameSeparator = lastName.length > 0 ? " " : ""
		
		let completeName = firstName + nameSeparator + lastName
		var greetingMessage: String
		
        if completeName.length - nameSeparator.length == 0 {
			greetingMessage = RegisterLocalizable.nameStepTitle.text
		} else {
			greetingMessage = "Oi, " + completeName + "!"
		}
		
        self.greetingTitle.textColor = Palette.ppColorGrayscale600.color
		self.greetingTitle.text = greetingMessage
		self.greetingTitle.changeTextColor(Palette.ppColorBranding300.color, forStringsWithPattern: completeName)
        updateContetViewPosition()
    }
    
    /// Validate the form data
    @discardableResult
    func validate( _ showMessage: Bool = false) -> Bool {
        var isValid = true
        
        let firstName = "\(self.firstNameTextField.text!)"
        let lastName = "\(self.lastNameTextField.text!)"
    
        if firstName.length < 3 && lastName.length < 3 {
            isValid = false
            if showMessage {
                self.showMessage(RegisterLocalizable.nameStepError.text, show: showMessage)
                self.firstNameTextField.hasError = true
                self.lastNameTextField.hasError = true
            }
        } else if firstName.length < 3 {
            isValid = false
            if showMessage {
                self.showMessage(RegisterLocalizable.nameStepMustContainAtLeast.text, show: showMessage)
                self.firstNameTextField.hasError = true
            }
            self.lastNameTextField.hasError = false
        } else if lastName.length < 3 {
            isValid = false
            if showMessage {
                self.showMessage(RegisterLocalizable.lastNameStepMustContainAtLeast.text, show: showMessage)
                self.lastNameTextField.hasError = true
            }
            self.firstNameTextField.hasError = false
        } else {
            self.hideMessage()
            self.firstNameTextField.hasError = false
            self.lastNameTextField.hasError = false
        }
        
        self.valid = isValid
        return isValid
    }

    // MARK: Helper Methods TextField

    @objc
    private func textFieldDidChange(_ textField: UITextField) {
        validate()
        updateGreetings()
    }
}

extension RegistrationNameStepViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // Workaround to the textfield jump
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn( _ textField: UITextField) -> Bool {
        if textField == firstNameTextField {
            // send to the next field
            self.lastNameTextField.becomeFirstResponder()
        } else {
            self.finishStep(nil)
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == lastNameTextField && firstNameTextField.text!.length + lastNameTextField.text!.length == 0 {
            DispatchQueue.main.async {
                self.firstNameTextField.becomeFirstResponder()
            }
        }
    }
}

extension RegistrationNameStepViewController: UITextViewDelegate {
    // MARK: - UITextViewDelegate

    func textViewDidChangeSelection(_ textView: UITextView) {
        let zeroRange = NSRange(location: 0, length: 0)
        if !NSEqualRanges(textView.selectedRange, zeroRange) {
            textView.selectedRange = zeroRange
        }
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        return self.shouldInteractWithURL(URL)
    }
    
    private func textView(
            textView: UITextView,
            shouldInteractWith URL: URL,
            in characterRange: NSRange,
            interaction: UITextItemInteraction
        ) -> Bool {
        return self.shouldInteractWithURL(URL)
    }
    
    func shouldInteractWithURL( _ URL: Foundation.URL) -> Bool {
        self.registrationSteOpenLink(url: URL)
        return false
    }
}

extension RegistrationNameStepViewController {
    // MARK: - Methods Accessibility
    
    override func updateAccessibilityAttributes() {
        super.updateAccessibilityAttributes()
        greetingTitle.accessibilityTraits = .updatesFrequently
        bottomTextView.accessibilityTraits = .staticText
    }
}
