import AnalyticsModule
import UI
import UIKit

final class RegistrationDocumentStepViewController: RegistrationStepViewController {
    @IBOutlet weak var cpfTextField: UIRoundedTextField!
    @IBOutlet weak var dateOfBirthTextField: UIRoundedTextField!
    
    private lazy var cpfNumberMasker: TextFieldMasker = {
        let cpfMask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        return TextFieldMasker(textMask: cpfMask)
    }()

    private lazy var dateNumberMasker: TextFieldMasker = {
        let dateMask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.date)
        return TextFieldMasker(textMask: dateMask)
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureMasks()
        controls.append(cpfTextField)
        controls.append(dateOfBirthTextField)
        
        cpfTextField.delegate = self
        dateOfBirthTextField.delegate = self
        
        cpfTextField.setDefaultColors()
        dateOfBirthTextField.setDefaultColors()
        
        textLabel?.isUserInteractionEnabled = true
        textLabel?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showCPFHelp(_:))))
        
        configureInformations()
        
        if !UIAccessibility.isVoiceOverRunning {
            cpfTextField.becomeFirstResponder()
        }
        
        setupAccessibility()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Analytics.shared.log(RegistrationEvent.registerDocument)

        populateForm()
        cpfTextField.layoutIfNeeded()
        dateOfBirthTextField.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func configureMasks() {
        cpfNumberMasker.bind(to: cpfTextField)
        dateNumberMasker.bind(to: dateOfBirthTextField)
    }
    
    // MARK: - User Actions
    
    @IBAction private func showCPFHelp(_ sender: AnyObject) {
        Analytics.shared.log(RegistrationEvent.documentHelp)
        
        let cpfHelpAlert = UIAlertController(title: RegisterLocalizable.alertDocumentHelpTitle.text,
                                             message: RegisterLocalizable.alertDocumentHelpText.text,
                                             preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: DefaultLocalizable.btOkUnderstood.text, style: .default, handler: nil)
        cpfHelpAlert.addAction(defaultAction)
        present(cpfHelpAlert, animated: true, completion: nil)
    }
    
    /// Save data and finish the step
    @IBAction private func finishStep( _ sender: UIButton?) {
        guard validate(true) else {
            return
        }
        startLoading()
        saveFormData()
        
        model?.finishRegistration(onSuccess: { [weak self] in
            self?.stopLoading()
            self?.goToNextStep()
        }, onError: { [weak self] error in
            self?.stopLoading()
            self?.showMessage(error.localizedDescription)
        })
    }
    
    // MARK: - Internal Methods
	
    /// populate form with model data
    func populateForm() {
        guard let model = model else {
            return
        }
        cpfTextField.text = model.cpf
        dateOfBirthTextField.text = model.dateOfBirth
    }
	
    /// Get data from form and save at model
    func saveFormData() {
        self.model!.cpf = "\(self.cpfTextField.text!)"
        self.model!.dateOfBirth = "\(self.dateOfBirthTextField.text!)"
    }
    
    /// Validate the form
    @discardableResult
    func validate( _ showMessage: Bool = false) -> Bool {
        var isValid = true
        
        let cpf = "\(self.cpfTextField.text!)"
        let dateOfBirth = "\(self.dateOfBirthTextField.text!)"
        
        if cpf.length < 14 && dateOfBirth.length < 10 {
            isValid = false
            if showMessage {
                self.showMessage(RegisterLocalizable.invalidCpfAndDateOfBirth.text, show: showMessage)
                self.cpfTextField.hasError = true
                self.dateOfBirthTextField.hasError = true
                Analytics.shared.log(RegistrationEvent.documentError(.invalidNumber))
            }
        } else if cpf.length < 14 {
            isValid = false
            if showMessage {
                self.showMessage(RegisterLocalizable.invalidCpf.text, show: showMessage)
                self.cpfTextField.hasError = true
                Analytics.shared.log(RegistrationEvent.documentError(.invalidNumber))
            }
            self.dateOfBirthTextField.hasError = false
        } else if dateOfBirth.length < 3 {
            isValid = false
            if showMessage {
                self.showMessage(RegisterLocalizable.invalidDateOfBirth.text, show: showMessage)
                self.dateOfBirthTextField.hasError = true
            }
            self.cpfTextField.hasError = false
        } else {
            self.hideMessage()
            self.cpfTextField.hasError = false
            self.dateOfBirthTextField.hasError = false
        }
        
        self.valid = isValid
        return isValid
    }
    
    func goToNextStep() {
        Analytics.shared.log(RegistrationEvent.cpf)
        
        if let controller = ViewsManager.registrationStoryboardViewController(withIdentifier: "RegistrationUsernameStep") as? RegistrationUsernameStepViewController {
            controller.setup(self.model!)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func configureInformations() {
        guard let documentFeature = model?.documentFeatureConfiguration() else {
            return
        }
        
        titleLabel?.text = documentFeature.title
        textLabel?.text = "\(documentFeature.text)\n\n<u>\(documentFeature.buttonTitle)</u>"
        textLabel?.changeTextColor(Palette.ppColorBranding300.color, forStringsWithPattern: documentFeature.buttonTitle)
        textLabel?.attributedText = textLabel?.attributedText?.underlinefy()
    }
}

extension RegistrationDocumentStepViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // Workaround to the textfield jump
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == cpfTextField {
            // send to the next field
            self.dateOfBirthTextField.becomeFirstResponder()
        } else {
            self.finishStep(nil)
        }
        
        return true
    }
    
    func textField( _ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let pasteboardString = UIPasteboard.general.string, !pasteboardString.isEmpty, string.contains(pasteboardString) {
            model?.copyPasteCpf = true
        }
        
        if textField == self.cpfTextField {
            let newCPF = textField.text!
            self.validate()
            
            if newCPF.count >= 14 && !string.isEmpty {
                self.dateOfBirthTextField.becomeFirstResponder()
                return false
            }
            
            return true
        } else {
            // back to ddd textfield
            if string.isEmpty && textField.text?.count == range.length {
                self.dateOfBirthTextField.text = ""
                self.cpfTextField.becomeFirstResponder()
                return false
            }
            return true
        }
    }
}

extension RegistrationDocumentStepViewController {
    // MARK: - Methods Accessibility
    
    override func updateAccessibilityAttributes() {
        super.updateAccessibilityAttributes()
        textLabel?.accessibilityTraits = [.staticText, .allowsDirectInteraction]
    }
}
