import AnalyticsModule
import FeatureFlag
import UIKit

final class RegistrationPhoneViewController: RegistrationStepViewController {
    private struct Event {
        static let phoneRegister = "Cadastro telefone"
    }
    
    var timer: Timer?
    
    @IBOutlet weak var dddTextField: UIRoundedTextField!
    @IBOutlet weak var phoneTextField: UIRoundedTextField!
    @IBOutlet weak var sendCodeWhatsAppButton: UIPPButton?
    @IBOutlet weak var sendCodeSMSButton: UIPPButton?
    @IBOutlet weak var haveCodeButton: UIButton!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        controls.append(dddTextField)
        controls.append(phoneTextField)
        controls.append(sendCodeSMSButton)
        
        dddTextField.delegate = self
        phoneTextField.delegate = self
        
        dddTextField.setDefaultColors()
        phoneTextField.setDefaultColors()
        
        if !UIAccessibility.isVoiceOverRunning {
            dddTextField.becomeFirstResponder()
        }
        
        updateContetViewPosition(withDuration: 0.0)
        setupAccessibility(with: [haveCodeButton])
        
        configureTexts()
        checkRegisterWithWhatsApp()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PPAnalytics.trackEvent(Event.phoneRegister, properties: nil)
        
        // Schedled check button status
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateVerficationButton), userInfo: nil, repeats: true)
        timer?.fire()
        
        updateVerficationButton()
        updateContetViewPosition()
    }
    
    override func viewWillDisappear( _ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
    }
    
    // MARK: - User Actions
    
    @IBAction private func haveCodeAction(_ sender: AnyObject) {
        self.valid = true
        Analytics.shared.log(RegistrationEvent.receiveCodeOption(.haveCode))
        goToNextStep()
    }
    
    /// Save data and finish the step
    @IBAction private func finishStep( _ sender: UIButton?) {
        let isValid = validate(true)
        if isValid {
            let requestCodeType: RegistrationViewModel.RequestCodeType = sender == sendCodeSMSButton ? .sms : .whatsApp
            
            let ddd = self.dddTextField.text ?? ""
            let phone = self.phoneTextField.text ?? ""
            
            let title = RegisterLocalizable.alertPhoneConfirmTitle.text
            let msg = String(format: RegisterLocalizable.alertPhoneConfirmMessage.text, ddd, phone)
            
            let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
            let okAction = UIAlertAction(title: DefaultLocalizable.btOk.text, style: .default, handler: { [weak self] _ in
                self?.sendPhoneToServerWith(type: requestCodeType)
            })
            alert.addAction(okAction)
            let cancelAction = UIAlertAction(title: DefaultLocalizable.btEdit.text, style: .cancel, handler: { [weak self] _ in
                self?.phoneTextField.becomeFirstResponder()
                Analytics.shared.log(RegistrationEvent.phoneConfirmation(.edit))
            })
            alert.addAction(cancelAction)
            
            present(alert, animated: true)
        }
    }
    
    // MARK: - Internal Methods
    
    /// Send the mobile phone to server
    func sendPhoneToServerWith(type: RegistrationViewModel.RequestCodeType) {
        startLoading()
        saveFormData()
        
        model?.finishPhoneStepWith(type: type) { [weak self] error in
            DispatchQueue.main.async {
                self?.stopLoading()
                if let text = error?.localizedDescription {
                    self?.showMessage(text)
                } else {
                    self?.goToNextStep()
                }
            }
        }
        
        let eventType: RegistrationEvent.ReceiveCodeOptionType = type == .sms ? .sms : .whatsApp
        Analytics.shared.log(RegistrationEvent.receiveCodeOption(eventType))
        Analytics.shared.log(RegistrationEvent.phoneConfirmation(.ok))
    }
    
    /// Send to nextStep
    func goToNextStep() {
        Analytics.shared.log(RegistrationEvent.phone)
        if let controller = ViewsManager.registrationStoryboardViewController(withIdentifier: "RegistrationCodeStep") as? RegistrationCodeStepViewController {
            controller.setup(self.model!)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    /// Get data from form and save at model
    func saveFormData() {
        model?.ddd = self.dddTextField.text ?? ""
        model?.phone = self.phoneTextField.text ?? ""
    }
    
    /// Validate the form
    @discardableResult
    func validate(_ showMessage: Bool = false) -> Bool {
        var isValid = true
        
        let ddd = self.dddTextField.text ?? ""
        let phone = self.phoneTextField.text ?? ""
        
        if ddd.length < 2 && phone.length < 9 {
            isValid = false
            self.showMessage(RegisterLocalizable.invalidDDDAndPhone.text, show: showMessage)
            if showMessage {
                self.dddTextField.hasError = true
                self.phoneTextField.hasError = true
            }
        } else if ddd.length < 2 {
            isValid = false
            self.showMessage(RegisterLocalizable.invalidDDD.text, show: showMessage)
            if showMessage {
                self.dddTextField.hasError = true
            }
            self.phoneTextField.hasError = false
        } else if phone.length < 9 {
            isValid = false
            self.showMessage(RegisterLocalizable.invalidPhone.text, show: showMessage)
            if showMessage {
                self.phoneTextField.hasError = true
            }
            self.dddTextField.hasError = false
        } else {
            messageLabel?.text = ""
            self.dddTextField.hasError = false
            self.phoneTextField.hasError = false
        }
        
        self.valid = isValid
        return isValid
    }
    
    /// Update the time / enable the sms buttons
    @objc
    func updateVerficationButton() {
        guard let model = model else {
            return
        }
        
        // SMS button
        let delay = model.smsDelayTime()
        if !delay.isEmpty {
            nextButton?.isEnabled = false
            nextButton?.setTitle(String(format: RegisterLocalizable.sendSMSDelay.text, delay), for: .disabled)
        } else if self.loading {
            nextButton?.isEnabled = false
            nextButton?.setTitle("", for: .disabled)
        } else {
            nextButton?.isEnabled = true
            nextButton?.setTitle(RegisterLocalizable.phoneStepSMSTitle.text, for: .normal)
        }
    }
    
    private func configureTexts() {
        titleLabel?.text = RegisterLocalizable.phoneStepTitle.text
        textLabel?.text = RegisterLocalizable.phoneStepText.text
        sendCodeWhatsAppButton?.setTitle(RegisterLocalizable.phoneStepWhatsAppTitle.text, for: .normal)
        sendCodeWhatsAppButton?.accessibilityLabel = RegisterLocalizable.phoneStepWhatsAppTitle.text
        sendCodeSMSButton?.setTitle(RegisterLocalizable.phoneStepSMSTitle.text, for: .normal)
    }
    
    private func checkRegisterWithWhatsApp() {
        guard FeatureManager.isActive(.phoneRegisterWithWhatsApp) else {
            configureDefaultButtonStyle(sendCodeSMSButton)
            Analytics.shared.log(RegistrationEvent.receiveCodeWhatsAppFlow(false))
            return
        }
            
        configureDefaultButtonStyle(sendCodeWhatsAppButton)
        configureSecondaryButtonStyle(sendCodeSMSButton)

        controls.append(sendCodeWhatsAppButton)

        sendCodeWhatsAppButton?.isEnabled = true
        sendCodeWhatsAppButton?.isHidden = false
        
        Analytics.shared.log(RegistrationEvent.receiveCodeWhatsAppFlow(true))
    }
}

extension RegistrationPhoneViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn( _ textField: UITextField) -> Bool {
        if textField == self.dddTextField {
            // send to the next field
            self.phoneTextField.becomeFirstResponder()
        } else {
            self.finishStep(nil)
        }
        
        return true
    }
    
    func textField( _ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == dddTextField {
            return formatDDDTextfield(charactersIn: range, replacementString: string)
        } else {
            return formatPhoneTextField(charactersIn: range, replacementString: string)
        }
    }
    
    private func formatDDDTextfield(charactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldValue = dddTextField.text as NSString?
        let newDDD = textFieldValue?.replacingCharacters(in: range, with: string).onlyNumbers ?? ""
        
        if newDDD.isEmpty && !string.isEmpty {
            return false
        }
        
        if string == UIPasteboard.general.string ?? "" && newDDD.count < 2 {
            dddTextField.text = newDDD
            validate()
            return false
        }
        
        // jump to the next field when the ddd is completed
        if newDDD.count >= 2 && !string.isEmpty {
            let range: Range<String.Index> = newDDD.startIndex..<newDDD.index(newDDD.startIndex, offsetBy: 2)
            dddTextField.text = String(newDDD[range])
            phoneTextField.becomeFirstResponder()
            validate()
            return false
        }
        
        validate()
        return true
    }
    
    private func formatPhoneTextField(charactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldValue = phoneTextField.text as NSString?
        // back to ddd textfield
        if string.isEmpty && phoneTextField.text?.count == range.length {
            phoneTextField.text = ""
            dddTextField.becomeFirstResponder()
            return false
        }
        
        //format phone
        var newPhone = textFieldValue?.replacingCharacters(in: range, with: string) ?? ""
        newPhone = newPhone.replacingOccurrences(of: "-", with: "").onlyNumbers
        
        if newPhone.isEmpty && !string.isEmpty {
            return false
        }
        
        if string == UIPasteboard.general.string ?? "" && newPhone.count <= 4 {
            phoneTextField.text = newPhone
            validate()
            return false
        }
        
        if newPhone.count > 4 {
            phoneTextField.text = newPhone.formatAsPhoneNumberWithHyphen()
            validate()
            return false
        }
        
        validate()
        return true
    }
}
