final class RegisterConsumerApi: BaseApi {
    func isValidEmail(email: String, completion: @escaping ((PicPayResult<BaseCodableEmptyResponse>) -> Void)) {
        requestManager.apiRequest(endpoint: kWsUrlIsValidEmail, method: .post, parameters: ["email": email]).responseApiCodable(completionHandler: completion)
    }
    
    func addConsumer(parameters: [String: Any], completion: @escaping ((PicPayResult<AddConsumerResponse>) -> Void)) {
        requestManager.apiRequest(endpoint: kWsUrlAddConsumer, method: .post, parameters: parameters).responseApiCodable(completionHandler: completion)
    }
}
