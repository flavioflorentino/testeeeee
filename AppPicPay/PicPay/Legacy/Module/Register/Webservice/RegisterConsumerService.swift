import Core

protocol RegisterConsumerServicing {
    func requestWhatsAppCode(ddd: String, phone: String, complete: @escaping (Result<PhoneNumberCodeResponse, Error>) -> Void)
}

struct RegisterConsumerService: RegisterConsumerServicing {
    private let jsonDecoder: JSONDecoder
    
    init(jsonDecoder: JSONDecoder = JSONDecoder()) {
        self.jsonDecoder = jsonDecoder
        self.jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
    }
    
    func requestWhatsAppCode(ddd: String, phone: String, complete: @escaping (Result<PhoneNumberCodeResponse, Error>) -> Void) {
        let api = Api<RegisterResponse<PhoneNumberCodeResponse>>(
            endpoint: RegisterEndpoint.requestWhatsAppCode(ddd: ddd, number: phone)
        )
        
        api.execute { result in
            DispatchQueue.main.async {
            let mappedResult = result.map { $0.model }
                switch mappedResult {
                case .success(let currentData):
                    guard let data = currentData.data else {
                        if let error = currentData.error {
                            complete(.failure(error))
                        }
                        return
                    }
                    
                    complete(.success(data))
                case .failure(let error):
                    complete(.failure(error))
                }
            }
        }
    }
}
