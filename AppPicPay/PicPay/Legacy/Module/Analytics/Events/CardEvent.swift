import Foundation
import AnalyticsModule

enum CardEvent: AnalyticsKeyProtocol {
    case didTapRequestOffer
    case didTapActivatedCardOffer
    case didChangeDefault(option: CardDefault)
    case didTapUnlockCardOffer
    case didTapWalletBadge
    case didTapWalletCardSignUpSeeDetails
    case didTapWalletRequestCard(action: RequestWalletAction)
    case didTapAppSettingsCardSession(action: CardEventAppSettingsAction)
    case didTapCardHomeMenu(action: CardHomeMenuAction)
    case didTapWalletCardBottomMenu(action: CardEventWalletAction)
    
    //Screen-event-
    private var name: String {
        switch self {
        case .didTapRequestOffer:
            return "PicPay Card Request - Offer Clicked"
        case .didTapActivatedCardOffer:
            return "PicPay Card Activation - Offer Clicked"
        case .didChangeDefault(let option):
            return "Is PicPay Card Default \(option.rawValue)"
        case .didTapUnlockCardOffer:
            return "PicPay Card - Unblock - Wallet Offer Clicked"
        case .didTapWalletBadge:
            return "PicPay Card - Unblock - Notification Badge"
        case .didTapWalletCardSignUpSeeDetails:
            return "PicPay Card - Request - Sign Up - Wallet View Details"
        case .didTapWalletRequestCard:
            return "PicPay Card - Request - Wallet Request Card"
        case .didTapAppSettingsCardSession:
            return "PicPay Card - Settings - Configuration"
        case .didTapCardHomeMenu:
            return "PicPay Card - Home - Menu"
        case .didTapWalletCardBottomMenu:
            return "PicPay Card - Wallet - Menu"
        }
    }
    
    private var properties: [String : Any] {
        switch self {
        case .didTapWalletBadge:
            return [
                "action": "act-app-wallet-badge"
            ]
        case .didTapWalletCardSignUpSeeDetails:
            return [
                "action": "act-view-details"
            ]
        case .didChangeDefault, .didTapUnlockCardOffer, .didTapRequestOffer, .didTapActivatedCardOffer:
            return [:]
        case .didTapWalletRequestCard(let option):
            return [
                "action": option.rawValue
            ]
        case .didTapAppSettingsCardSession(let action):
            return [
                "action": action.rawValue
            ]
        case .didTapCardHomeMenu(let action):
            return [
                "action": action.rawValue
            ]
        case .didTapWalletCardBottomMenu(let action):
            return [
                "action": action.rawValue
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        return [.firebase, .mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

extension CardEvent {
    enum CardEventWalletAction: String {
        case ppcardHome = "act-wallet-card-home"
        case ppcardSettings = "act-wallet-config-card"
    }
    
    enum RequestWalletAction: String {
        case configCard = "act-card-config-request"
        case selectPicPay = "act-pipcpay-card"
    }
    
    enum CardDefault: String {
        case yes = "TRUE"
        case no = " FALSE"
    }
    
    enum CardEventAppSettingsAction: String {
        case home = "act-card-home"
        case settings = "act-card-settings"
    }
    
    enum CardHomeMenuAction: String {
        case invoice = "act-invoice-card"
        case settings = "act-config-card"
        case virtualCard = "act-access-virtual-card"
        case cancel = "act-close-card-menu"
    }
}

