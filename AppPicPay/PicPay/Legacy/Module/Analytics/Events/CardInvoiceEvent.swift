import Foundation
import AnalyticsModule

enum CardInvoiceEvent: AnalyticsKeyProtocol {
    
    case homePicPayCard(action: HomePicPayCardAction)
    case creditPayment(action: InvoicePaymentType)
    case paymentInvoiceAuthAlert(action: AuthAction)
    case additonalTaxAlert(action: CardAdditonalTax)
    case methodsPayment(action: CardPaymentMethod)
    case paymentChoice(action: CardPaymentChoice)
    case payOtherAmount
    case cardInvoicePaymentMethod
    case billPaymentAlertConfirmEmail
    case billPayment(action: CreditPicPayBillAction)
    case billPaymentInfoAlert
    case billPaymentConfirmAlert(action: BillAlertEnablePaymentAction)
    case billNotificationOptionsAlert(action: BillAlertNoticationPaymentAction)
    private var name: String {
        switch self {
        case .homePicPayCard:
            return "Pay Invoice"
        case .additonalTaxAlert:
            return "Tax payment"
        case .methodsPayment:
            return "Payment Method"
        case .paymentChoice:
            return "Payment Choice"
        case .payOtherAmount:
            return "Pay Other Amount"
        case .creditPayment:
            return "Pay Value"
        case .cardInvoicePaymentMethod:
            return "Method PicPay"
        case .paymentInvoiceAuthAlert:
            return "Payment Authentication"
        case .billPaymentAlertConfirmEmail:
            return "Billet send email"
        case .billPaymentInfoAlert, .billNotificationOptionsAlert:
            return "Billet Processing"
        case .billPaymentConfirmAlert:
            return "Processing time"
        case .billPayment:
            return "Payment billet"
        }
    }
    
    private var properties: [String : Any] {
        switch self {
        case .homePicPayCard(let action):
            return [
                "action": action.rawValue
            ]
        case .creditPayment(let action):
            return [
                "action": action.rawValue
            ]
        case .paymentInvoiceAuthAlert(let action):
            return [
                "action": action.rawValue
            ]
        case .additonalTaxAlert(let action):
            return [
                "action": action.rawValue
            ]
        case .paymentChoice(let action):
            return [
                "action": action.rawValue
            ]
        case .payOtherAmount:
            return [
                "action": "act-pay-with-billet-other"
            ]
        case .cardInvoicePaymentMethod:
            return [
                "action": "act-picpay-card"
            ]
        case .billPaymentAlertConfirmEmail:
            return [
                "action": "act-billet-send-email"
            ]
        case .billPayment(let action):
            return [
                "action": action.rawValue
            ]
        case .billPaymentInfoAlert:
            return [
                "action": "act-success-message-close"
            ]
        case .billPaymentConfirmAlert(let action):
            return [
                "action": action.rawValue
            ]
        case .methodsPayment(action: let action):
            return [
                "action": action.rawValue
            ]
        case .billNotificationOptionsAlert(action: let action):
            return [
                "action": action.rawValue
            ]
        }
    }
   
    private var providers: [AnalyticsProvider] {
        return [.firebase, .mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent("PicPay Card - Invoice Payment - " + name, properties: properties, providers: providers)
    }
    
    enum HomePicPayCardAction: String {
        case menu = "act-view-card-menu"
        case pay = "act-pay-invoice"
    }
    
    enum CardPaymentMethod: String {
        case payWithPicpay = "act-pay-with-wallet"
        case payWithBillet = "act-pay-with-billet"
    }
    
    enum CardPaymentChoice: String {
        case payTotalAmount = "act-pay-total-amount"
        case payAnotherAmount = "act-pay-other-amount"
    }
    
    enum BillAlertEnablePaymentAction: String {
        case billAlertEnabledNotNow = "act-not-now"
        case billContinue = "act-pay-with-billet"
    }
    
    enum CreditPicPayBillAction: String {
        case billSendEmail = "act-billet-send-email"
        case billCopyBarCode = "act-copy-barcode"
    }
    enum BillAlertNoticationPaymentAction: String {
        case payWithPicpay = "act-pay-with-wallet-billet-processing"
        case receiveNotification = "act-billet-notified"
    }
    enum InvoicePaymentType: String {
        case balance = "st-payment-method-picpay"
        case balanceAndCard = "st-payment-method-picpay-and-card"
        case card = "st-payment-method-card"
    }
    
    enum AuthAction: String {
        case fingerPrintUsed = "act-fingerprint-used"
        case fingerPrintCancelAlert = "act-fingerprint-cancel"
        case authenticationValidated = "st-authentication-validated"
        case authenticationFailed = "st-authentication-failed"
    }
    
    enum CardAdditonalTax: String {
        case agree = "act-agree-additonal-tax"
        case notNow = "act-not-now"
    }
}
