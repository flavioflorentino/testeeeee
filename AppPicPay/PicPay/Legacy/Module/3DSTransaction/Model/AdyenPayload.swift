final class AdyenPayload: Decodable {
    let transactionId: String
    let transactionType: String
    let threeDS2Token: String
    let threeDSServerTransID: String
    let directoryServerId: String
    let directoryServerPublicKey: String
    
    enum CodingKeys: String, CodingKey {
        case transaction
        case transactionId = "id"
        case transactionType = "type"
        case acquirerData = "acquirer_data"
        case additionalData
        case threeDS2Token = "threeds2.threeDS2Token"
        case threeDSServerTransID = "threeds2.threeDSServerTransID"
        case directoryServerId = "threeds2.threeDS2DirectoryServerInformation.directoryServerId"
        case directoryServerPublicKey = "threeds2.threeDS2DirectoryServerInformation.publicKey"
    }
    
    init(_ payload: [String: String]) {
        self.transactionId = payload["transactionId"] ?? ""
        self.transactionType = payload["transactionType"] ?? ""
        self.threeDS2Token = payload["threeDS2Token"] ?? ""
        self.threeDSServerTransID = payload["threeDSServerTransID"] ?? ""
        self.directoryServerId = payload["directoryServerId"] ?? ""
        self.directoryServerPublicKey = payload["directoryServerPublicKey"] ?? ""
    }
    
    var dict: [String: String] {
        return ["transactionId": transactionId,
                "transactionType": transactionType,
                "threeDS2Token": threeDS2Token,
                "threeDSServerTransID": threeDSServerTransID,
                "directoryServerId": directoryServerId,
                "directoryServerPublicKey": directoryServerPublicKey]
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let transaction = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .transaction)
        transactionId = try transaction.decode(String.self, forKey: .transactionId)
        transactionType = try transaction.decode(String.self, forKey: .transactionType)
        
        let acquirerData = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .acquirerData)
        let additionalData = try acquirerData.nestedContainer(keyedBy: CodingKeys.self, forKey: .additionalData)
        threeDS2Token = try additionalData.decode(String.self, forKey: .threeDS2Token)
        threeDSServerTransID = try additionalData.decode(String.self, forKey: .threeDSServerTransID)
        directoryServerId = try additionalData.decode(String.self, forKey: .directoryServerId)
        directoryServerPublicKey = try additionalData.decode(String.self, forKey: .directoryServerPublicKey)
    }
}
