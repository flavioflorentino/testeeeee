import Foundation

enum AdyenErrorStatusReason: Int, Decodable {
    case carAuthenticationFailed = 1
    case unknownDevice
    case unsupportedDevice
    case exceedsAuthenticationFrequencyLimit
    case expiredCard
    case invalidCardNumber
    case invalidTransaction
    case noCardRecord
    case securityFailure
    case stolenCard
    case suspectedFraud
    case transactionNotPermittedToCardholder
    case cardholderNotEnrolledInService
    case transactionTimedOutACS
    case lowConfidence
    case mediumConfidence
    case highConfidence
    case veryHighConfidence
    case exceedsACSMaximumChallenges
    case NonPaymentTransactionNotSupported
    case transaction3RINotSupported
    case unknown
}

struct AdyenErrorPayload: Decodable {
    let transactionStatus: String?
    let transactionStatusReason: AdyenErrorStatusReason?
    let refusalReason: String?
    
    enum CodingKeys: String, CodingKey {
        case transactionStatus = "trans_status"
        case transactionStatusReason = "trans_status_reason"
        case refusalReason = "refusal_reason"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        transactionStatus = try container.decode(String.self, forKey: .transactionStatus)
        refusalReason = try container.decode(String.self, forKey: .refusalReason)
        
        guard let rawValue = Int(try container.decode(String.self, forKey: .transactionStatusReason)) else {
            transactionStatusReason = .unknown
            return
        }
        transactionStatusReason = AdyenErrorStatusReason(rawValue: rawValue)
    }
}
