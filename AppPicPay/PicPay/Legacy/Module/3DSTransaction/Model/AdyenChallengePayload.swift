final class AdyenChallengePayload: Decodable {
    let acsSignedContent: String
    let acsTransactionIdentifier: String
    let serverTransactionIdentifier: String
    let acsReferenceNumber: String
    
    enum CodingKeys: String, CodingKey {
        case acquirerData = "acquirer_data"
        case additionalData
        case acsSignedContent = "threeds2.threeDS2ResponseData.acsSignedContent"
        case acsTransactionIdentifier = "threeds2.threeDS2ResponseData.acsTransID"
        case serverTransactionIdentifier = "threeds2.threeDS2ResponseData.threeDSServerTransID"
        case acsReferenceNumber = "threeds2.threeDS2ResponseData.acsReferenceNumber"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let acquirerData = try values.nestedContainer(keyedBy: CodingKeys.self, forKey: .acquirerData)
        let additionalData = try acquirerData.nestedContainer(keyedBy: CodingKeys.self, forKey: .additionalData)
        acsSignedContent = try additionalData.decode(String.self, forKey: .acsSignedContent)
        acsTransactionIdentifier = try additionalData.decode(String.self, forKey: .acsTransactionIdentifier)
        serverTransactionIdentifier = try additionalData.decode(String.self, forKey: .serverTransactionIdentifier)
        acsReferenceNumber = try additionalData.decode(String.self, forKey: .acsReferenceNumber)
    }
}

extension AdyenChallengePayload {
    static func challenge(from data: Data) -> ADYChallengeParametersProtocol? {
        guard let payload = try? JSONDecoder().decode(AdyenChallengePayload.self, from: data) else {
            return nil
        }
        return ADYChallengeParameters(serverTransactionIdentifier: payload.serverTransactionIdentifier,
                                      acsTransactionIdentifier: payload.acsTransactionIdentifier,
                                      acsReferenceNumber: payload.acsReferenceNumber,
                                      acsSignedContent: payload.acsSignedContent)
    }
}
