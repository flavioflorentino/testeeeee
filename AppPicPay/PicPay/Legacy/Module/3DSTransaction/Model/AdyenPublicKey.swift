struct AdyenPublicKey: Codable {
    let x: String
    let y: String
    let crv: String
    let kty: String
    
    static func sdkEphemeralPublicKey(_ json: String) -> AdyenPublicKey? {
        guard let data = json.data(using: .utf8),
            let key = try? JSONDecoder().decode(AdyenPublicKey.self, from: data) else {
                return nil
        }
        return key
    }
}
