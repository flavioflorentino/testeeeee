import SwiftyJSON
import Foundation

struct AdyenReceipt: BaseApiResponse {
    let widgets: [ReceiptWidgetItem]
    
    init?(json: JSON) {
        guard let receipt = json["data"]["receipt"].array else {
            return nil
        }
        widgets = WSReceipt.createReceiptWidgetItem(receipt)
    }
}
