@objc
final class Adyen3DS: NSObject {
    @objc
    static func is3ds(error: PicPayError) -> Bool {
        return error.code == AdyenConstant.code
    }
    
    @objc
    static func challengeCancelled(_ error: NSError) -> Bool {
        return error.code == AdyenConstant.errorAdyChallengeCancelled
    }
    
    @objc
    static func adyenPayload(error: PicPayError) -> [String: String]? {
        guard
            let dictionary = error.dataDictionary(),
            let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted),
            let adyenPayload = try? JSONDecoder().decode(AdyenPayload.self, from: data)
            else {
                return nil
        }
        
        return adyenPayload.dict
    }
}
