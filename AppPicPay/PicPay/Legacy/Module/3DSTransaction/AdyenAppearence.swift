import UI
import Adyen3DS2

extension ADYAppearanceConfiguration {
    static var PPStyle: ADYAppearanceConfiguration = {
        let config = ADYAppearanceConfiguration()
        config.accessibilityLanguage = "pt-BR"
        
        // Screen
        config.backgroundColor = .background
        
        // Title
        config.labelAppearance.headingFont = .bold28
        config.labelAppearance.headingTextColor = .fullColor
        
        // Subtitle
        config.labelAppearance.font = .light14
        config.labelAppearance.textColor = .darkText
        
        // TextField Title
        config.labelAppearance.subheadingFont = .semibold14
        config.labelAppearance.subheadingTextColor = .darkText
        
        // TextField
        config.textFieldAppearance.borderColor = .border
        config.textFieldAppearance.borderWidth = 1.0
        
        // Submit
        let submitButton = ADYAppearanceButtonType.submit
        config.buttonAppearance(for: submitButton).font = .semibold16
        config.buttonAppearance(for: submitButton).cornerRadius = 24
        config.buttonAppearance(for: submitButton).backgroundColor = .ppgreen
        
        // Reesend
        let resendButton = ADYAppearanceButtonType.resend
        config.buttonAppearance(for: resendButton).font = .semibold14
        config.buttonAppearance(for: resendButton).textColor = .ligthText
        
        // Cancel
        let cancelButton = ADYAppearanceButtonType.cancel
        config.buttonAppearance(for: cancelButton).font = UIFont.systemFont(ofSize: 17.0)
        config.buttonAppearance(for: cancelButton).textColor = .ppgreen
        
        // Expandable footer texts
        config.infoAppearance.headingTextColor = .darkText
        config.infoAppearance.headingFont = .semibold14
        config.infoAppearance.selectionIndicatorTintColor = .ppgreen
        config.infoAppearance.borderColor = .border
        config.infoAppearance.font = .light14
        config.infoAppearance.textColor = .darkText
        
        return config
    }()
}

final class AdyenWarningView: UIView {
    // MARK: - Layout
    private enum Layout {
        static let sizeButton: CGFloat = 44
        static let spacing: CGFloat = 20
        static let margin: CGFloat = 20
        static let cornerRadius: CGFloat = sizeButton / 2
    }
    
    // MARK: - Visual Components
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = Layout.spacing
        return stackView
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "warning3ds"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = AdyenConstant.warningTitle
        label.font = .bold28
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.text = AdyenConstant.warningSubtitle
        label.font = .light16
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private(set) lazy var submitButton: UIButton = {
        let button = UIButton()
        button.setTitle(AdyenConstant.warningButton, for: .normal)
        button.layer.cornerRadius = Layout.cornerRadius
        button.clipsToBounds = true
        button.titleLabel?.font = .semibold16
        return button
    }()
    
    // MARK: - Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        configViewHierarchy()
        configConstraints()
        configColors()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configViewHierarchy() {
        addSubview(rootStackView)
        rootStackView.addArrangedSubview(iconImageView)
        rootStackView.addArrangedSubview(titleLabel)
        rootStackView.addArrangedSubview(subtitleLabel)
        rootStackView.addArrangedSubview(submitButton)
    }
    
    private func configConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: self, for: [rootStackView], constant: Layout.margin)
        
        NSLayoutConstraint.activate([
            rootStackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            submitButton.heightAnchor.constraint(equalToConstant: Layout.sizeButton)
        ])
    }
    
    private func configColors() {
        backgroundColor = .background
        submitButton.backgroundColor = .ppgreen
        submitButton.setTitleColor(Palette.white.color, for: .normal)
        subtitleLabel.textColor = .darkText
        titleLabel.textColor = .fullColor
    }
}

// MARK: - UIFont Extension
private extension UIFont {
    static var bold28: UIFont = {
        return UIFont.boldSystemFont(ofSize: 28.0)
    }()
    
    static var light14: UIFont = {
        return UIFont.systemFont(ofSize: 14.0, weight: .light)
    }()
    
    static var light16: UIFont = {
        return UIFont.systemFont(ofSize: 16.0, weight: .light)
    }()
    
    static var semibold14: UIFont = {
        return UIFont.systemFont(ofSize: 14.0, weight: .semibold)
    }()
    
    static var semibold16: UIFont = {
        return UIFont.systemFont(ofSize: 14.0, weight: .semibold)
    }()
}

// MARK: - UIColor Extension
private extension UIColor {
    static var background: UIColor = {
        return Palette.ppColorGrayscale000.color
    }()
    
    static var ppgreen: UIColor = {
        return Palette.ppColorBranding300.color
    }()
    
    static var fullColor: UIColor = {
        return Palette.ppColorGrayscale600.color
    }()
    
    static var darkText: UIColor = {
        return Palette.ppColorGrayscale500.color
    }()
    
    static var ligthText: UIColor = {
        return Palette.ppColorGrayscale400.color
    }()
    
    static var border: UIColor = {
        return Palette.ppColorGrayscale300.color
    }()
}
