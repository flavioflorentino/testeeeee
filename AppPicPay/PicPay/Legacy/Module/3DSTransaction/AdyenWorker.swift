import AnalyticsModule
import Core
import FeatureFlag
import Foundation
import Alamofire

protocol AdyenServicing {
    var firstAccess3DS: Bool { get }
    func transaction(with payload: [String: String],
                     hasChallenge: @escaping (ADYTransactionProtocol, ADYChallengeParametersProtocol, [String: Any]) -> Void,
                     succeeded: @escaping (ReceiptWidgetViewModel) -> Void,
                     error: @escaping (PicPayError) -> Void,
                     injecting service: ADYServiceProtocol.Type)
    
    func complete(payload: [String: Any], succeeded: @escaping (ReceiptWidgetViewModel) -> Void, error: @escaping (PicPayError) -> Void)
}

final class AdyenWorker: NSObject, AdyenServicing {
    enum State {
        case challenge(ADYChallengeParametersProtocol)
        case succeeded(ReceiptWidgetViewModel)
        case error(PicPayError)
    }
    
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies = DependencyContainer()
    
    @objc
    var firstAccess3DS: Bool {
        return !KVStore().boolFor(.viewedWarning3dsTransaction)
    }
    
    @objc
    func transaction(with payload: [String: String],
                     hasChallenge: @escaping (ADYTransactionProtocol, ADYChallengeParametersProtocol, [String: Any]) -> Void,
                     succeeded: @escaping (ReceiptWidgetViewModel) -> Void,
                     error: @escaping (PicPayError) -> Void,
                     injecting service: ADYServiceProtocol.Type = ADYService.self) {
        
        
        let payload = AdyenPayload(payload)
        
        adyTransaction(with: payload, completion: { (transaction) in
            let param = self.parameters(with: transaction.authenticationRequestParameters, and: payload)
            
            self.makeTransaction(param) { response in
                let result = self.treatTransaction(response, id: payload.transactionId)
                DispatchQueue.main.async {
                    switch result {
                    case .challenge(let payload):
                        Analytics.shared.log(AdyenEvent.transaction(.challenge))
                        hasChallenge(transaction, payload, param)
                    case .succeeded(let receipt):
                        succeeded(receipt)
                    case .error(let hasError):
                        error(hasError)
                    }
                }
            }
        }, error: { AdyError in
            DispatchQueue.main.async {
                error(AdyError)
            }
        }, injecting: service)
    }
    
    @objc
    func complete(payload: [String: Any],
                  succeeded: @escaping (ReceiptWidgetViewModel) -> Void,
                  error: @escaping (PicPayError) -> Void) {
        
        let param = self.completeParameters(with: payload)
        completeTransaction(param) { response in
            DispatchQueue.main.async {
                let result = self.treatTransaction(response, id: "")
                switch result {
                case .succeeded(let receipt):
                    succeeded(receipt)
                case .challenge(_):
                    error(AdyenConstant.errorTreatTransaction)
                case .error(let hasError):
                    error(hasError)
                }
            }
        }
    }
    
    func treatTransaction(_ response: PicPayResult<BaseApiGenericResponse>, id: String) -> State {
        var result: State = .error(AdyenConstant.errorTreatTransaction)
        
        switch response {
        case .success(let type):
            if let legacyError = Legacy.hasError(in: type.json) {
                result = .error(legacyError)
            }
            
            if let receipt = self.parseReceipt(type.json, id: id) {
                result = .succeeded(receipt)
            }
            
            if let data = try? type.json["data"].rawData(),
                let payload = AdyenChallengePayload.challenge(from: data) {
                result = .challenge(payload)
            }
            
        case .failure(let transactionError):
            result = .error(transactionError)
        }
        
        return result
    }
    
    func adyTransaction(with payload: AdyenPayload,
                        completion: @escaping (ADYTransactionProtocol) -> Void,
                        error: @escaping (PicPayError) -> Void,
                        injecting service: ADYServiceProtocol.Type = ADYService.self) {
        
        let parameters = ADYServiceParameters.with(payload)
        service.transaction(
            with: parameters,
            appearanceConfiguration: ADYAppearanceConfiguration.PPStyle,
            completionHandler: { [weak self] transaction, _, adyError in
                guard let transaction = transaction else {
                    let picpayError = self?.errorAdyTransaction(error: adyError) ?? PicPayError(message: DefaultLocalizable.unexpectedError.text)
                    error(picpayError)
                    return
                }
                completion(transaction)
        })
    }
    
    private func errorAdyTransaction(error: Error?) -> PicPayError {
        guard let message = error?.localizedDescription else {
            return AdyenConstant.errorAdyTransactionInternal
        }
        
        return PicPayError(message: message)
    }
}

extension ADYServiceParameters {
    static func with(_ payload: AdyenPayload) -> ADYServiceParameters {
        let parameters = ADYServiceParameters()
        parameters.directoryServerIdentifier = payload.directoryServerId
        parameters.directoryServerPublicKey = payload.directoryServerPublicKey
        return parameters
    }
}

extension AdyenWorker {
    private func makeTransaction(_ param: Parameters, completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let service = AdyenConstant.APIPerformChallenge
            RequestManager.shared.apiRequest(endpoint: service, method: .post, parameters: param, headers: [:], pin: nil).responseApiObject(completionHandler: completion)
            return
        }
        
        // TODO: - adicionar helper para o core network
    }
    
    private func completeTransaction(_ param: Parameters, completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let service = AdyenConstant.APICompleteChallenge
            RequestManager.shared.apiRequest(endpoint: service, method: .post, parameters: param, headers: [:], pin: nil).responseApiObject(completionHandler: completion)
            return
        }
        
        // TODO: - adicionar helper para o core network
    }
}

import SwiftyJSON
extension AdyenWorker {
    func parseReceipt(_ json: JSON, id: String) -> ReceiptWidgetViewModel? {
        if let receipt = AdyenReceipt(json: json) {
            let viewModel = ReceiptWidgetViewModel(transactionId: id, type: .P2P)
            viewModel.setReceiptWidgets(items: receipt.widgets, isLoading: false)
            return viewModel
        }
        return nil
    }
}

//Isso ta um lixo, mas ta testado. ¯\_(ツ)_/¯
extension AdyenWorker {
    func parameters(with adyParameters: ADYAuthenticationRequestParametersProtocol, and payload: AdyenPayload) -> [String: Any] {
        guard let publicKey = AdyenPublicKey.sdkEphemeralPublicKey(adyParameters.sdkEphemeralPublicKey) else {
            return [:]
        }
        
        let parameters: [String: Any] = [
            "transaction": [
                "id": payload.transactionId,
                "type": payload.transactionType
            ],
            "acquirer_data": [
            "threeDS2RequestData": [
                "sdkAppID": adyParameters.sdkApplicationIdentifier,
                "sdkEncData": adyParameters.deviceInformation,
                "sdkEphemPubKey": [
                    "crv": publicKey.crv,
                    "kty": publicKey.kty,
                    "x": publicKey.x,
                    "y": publicKey.y
                ],
                "sdkReferenceNumber": adyParameters.sdkReferenceNumber,
                "sdkTransID": adyParameters.sdkTransactionIdentifier
            ],
            "threeDS2Token": payload.threeDS2Token
            ]
        ]
        
        return parameters
    }
    
    func completeParameters(with payload: [String: Any]) -> [String: Any] {
        var transactionId = ""
        var transactionType = ""
        var token = ""

        if let transaction = payload["transaction"] as? [String: Any] {
            if let id = transaction["id"] as? String {
                transactionId = id
            }
        }
        
        if let transaction = payload["transaction"] as? [String: Any] {
            if let type = transaction["type"] as? String {
                transactionType = type
            }
        }
        
        if let acquirerData = payload["acquirer_data"] as? [String: Any] {
            if let threeDS2Token = acquirerData["threeDS2Token"] as? String {
                token = threeDS2Token
            }
        }
        
        let param: [String: Any] = [
            "transaction": [
                "id": transactionId,
                "type": transactionType,
            ],
            "acquirer_data": [
                "threeDS2Result": [
                    "transStatus": "Y"
                ],
                "threeDS2Token": token
            ]
        ]
        return param
    }
}

fileprivate struct Legacy {
    static func hasError(in json: JSON) -> PicPayError? {
        guard let dictionary = json["Error"].toDictionary() else {
            return nil
        }
        
        return PicPayError(apiJSON: dictionary as NSDictionary)
    }
}
