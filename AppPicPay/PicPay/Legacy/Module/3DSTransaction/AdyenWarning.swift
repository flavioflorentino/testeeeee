import Core
import Foundation
import UIKit

@objc
final class AdyenWarning: UIViewController {
    var didTapContinue: () -> Void = { }
    
    private let rootView: AdyenWarningView = {
       return AdyenWarningView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
    }()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupController()
        setupAction()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupController() {
        view.backgroundColor = .white
        view.addSubview(rootView)
        title = ParkingLocalizable.newTransaction.text
    }
    
    private func setupAction() {
        rootView.submitButton.addTarget(self, action: #selector(close), for: .touchUpInside)
    }
    
    @objc
    private func close() {
        dismiss(animated: true, completion: nil)
        didTapContinue()
    }
    
    @objc
    func show(in controller: UIViewController, completion: @escaping (() -> Void)) {
        DispatchQueue.main.async {
            KVStore().setBool(true, with: .viewedWarning3dsTransaction)
            #if swift(>=5.1)
            if #available(iOS 13.0, *) {
                self.isModalInPresentation = true
            }
            #endif
            controller.present(self, animated: true)
        }
        
        didTapContinue = {
            DispatchQueue.main.async {
                completion()
            }
        }
    }
}
