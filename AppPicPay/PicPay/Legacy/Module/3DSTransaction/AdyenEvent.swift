import AnalyticsModule
import Foundation

enum AdyenEvent: AnalyticsKeyProtocol {
    case transaction(_ type: AdyenType)
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .transaction(let type):
            return AnalyticsEvent(
                "Visualizacao de tela",
                properties: ["nome": type.description],
                providers: [.appsFlyer, .firebase, .mixPanel]
            )
        }
    }
}

extension AdyenEvent {
    enum AdyenType: String, CustomStringConvertible {
        case challenge = "Autenticacao 3ds"
        
        var description: String {
            return self.rawValue
        }
    }
}
