import Adyen3DS2

struct AdyenConstant {
    // MARK: Model
    static let code = 2512
    static let notAuthorized = 7666
    static let domain = "3DSTransaction"
    static let invalidJsonCode = 9088
    static let messageInvalidJson = "Houve um erro ao processar o objeto data. Erro: \(AdyenConstant.invalidJsonCode)"
    
    // MARK: Errors
    static let errorAdyTransactionInternal = PicPayError(message: "Internal 3DS Error while making new transaction.", code: "5532")
    static let errorTreatTransaction = PicPayError(message: "Internal 3DS error while parsing perform challenge respose", code: "9012")
    static let errorAdyChallengeCancelled = ADYRuntimeErrorCode.challengeCancelled.rawValue
    
    // MARK: API URLS
    static let APIPerformChallenge = "api/performChallenge.json"
    static let APICompleteChallenge = "api/challengeComplete.json"
    
    // MARK: Waring Screen
    static let warningTitle = "Autenticação de pagamento"
    static let warningSubtitle = "Para aumentar a segurança do seu pagamento, o emissor do seu cartão solicitou uma autenticação extra"
    static let warningButton = "Continuar"
}
