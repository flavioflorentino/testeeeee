import Adyen3DS2

extension ADYService: ADYServiceProtocol { }
extension ADYChallengeParameters: ADYChallengeParametersProtocol { }

@objc
protocol ADYTransactionProtocol {
    var authenticationRequestParameters: ADYAuthenticationRequestParameters { get }
}

extension ADYTransaction: ADYTransactionProtocol { }

protocol ADYServiceParametersProtocol {
    var directoryServerIdentifier: String? { get set }
    var directoryServerPublicKey: String? { get set }
}

extension ADYServiceParameters: ADYServiceParametersProtocol { }

protocol ADYWarningProtocol {
    var identifier: String { get }
    var message: String { get }
}

extension ADYWarning: ADYWarningProtocol { }

protocol ADYAuthenticationRequestParametersProtocol {
    var deviceInformation: String { get }
    var sdkEphemeralPublicKey: String { get }
    var sdkApplicationIdentifier: String { get }
    var sdkReferenceNumber: String { get }
    var sdkTransactionIdentifier: String { get }
}

extension ADYAuthenticationRequestParameters: ADYAuthenticationRequestParametersProtocol { }
