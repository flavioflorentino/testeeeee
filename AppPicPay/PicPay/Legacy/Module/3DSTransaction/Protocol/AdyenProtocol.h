#import <Foundation/Foundation.h>
@import Adyen3DS2;


//Waiting for Adyen to Fix the SDK so we can update those protocols and remove thoose warnings
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
@protocol ADYServiceProtocol
+ (void)transactionWithParameters:(ADYServiceParameters *)parameters appearanceConfiguration:(nullable ADYAppearanceConfiguration *)appearanceConfiguration completionHandler:(void (^)(ADYTransaction * _Nullable transaction, NSArray<ADYWarning *> * _Nullable warnings, NSError * _Nullable error))completionHandler;
@end

@protocol ADYChallengeParametersProtocol
@property (nonatomic, copy, readonly) NSString *serverTransactionIdentifier;
@property (nonatomic, copy, readonly) NSString *ACSTransactionIdentifier;
@property (nonatomic, copy, readonly) NSString *ACSReferenceNumber;
@property (nonatomic, copy, readonly) NSString *ACSSignedContent;
@end
#pragma clang diagnostic pop
