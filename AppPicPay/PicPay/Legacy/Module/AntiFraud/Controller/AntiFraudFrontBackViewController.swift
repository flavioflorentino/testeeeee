import UI
import UIKit

@objc protocol AntiFraudFrontBackViewControllerDelegate {
    /**
     This method is called when the user finished the process
     (took out a new photo or has chosen a file from the library for all steps).
     */
    func antiFraudFrontBackDidSelectImages(_ controller: AntiFraudFrontBackViewController, images: [UIImage])
}

final class AntiFraudFrontBackViewController: PPBaseViewController {
    var step: Int = 1
    var totalStep: Int = 2
    var images: [UIImage] = []
    var antiFraudChecklistItem: AntiFraudChecklistItem?
    weak var delegate: AntiFraudFrontBackViewControllerDelegate?
    var antiFraudChecklistRow: Int = 0
    
    // MARK: - Outlet
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var contentButtonView: UIView!
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    // MARK: - User Actions
    
    @IBAction private func showPhotoSourceOptions(_ sender: AnyObject) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let actionPhoto = UIAlertAction(title: AntiFraudLocalizable.takePicture.text, style: .default) { [weak self] _ in
            self?.takePicture()
        }
        let actionGallery = UIAlertAction(title: AntiFraudLocalizable.chooseFromLibrary.text, style: .default) { [weak self] _ in
            self?.openLibrary()
        }
        let actionCancel = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil)
        
        actionSheet.addAction(actionPhoto)
        actionSheet.addAction(actionGallery)
        actionSheet.addAction(actionCancel)
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    func takePicture() {
        openImagePicker(true)
    }
    
    func openLibrary() {
        openImagePicker(false)
    }
    
    /**
     This method is called when the user takes a picture or select an image from library
     */
    func seletecPhoto(_ image: UIImage) {
        // stores the curret image in the list
        // (the images will be sent only when all step are completed)
        images.append(image)
        
        // if the current step is the last
        // goes back to the list of requested documents.
        if step == totalStep {
            // send selected imagens to delegate
            delegate?.antiFraudFrontBackDidSelectImages(self, images: images)
            
            // goes back to list
            let i = (navigationController?.viewControllers.count)! - totalStep - 1
            navigationController?.popToViewController((navigationController?.viewControllers[i])!, animated: true)
        } else {
            goToNextStep()
        }
    }
    
    // MARK: - Internal Methods
    
    /**
     Configures the view elements (image,text,button)
     according to the step (Front or Back)
     */
    func configureView() {
        if step == 2 {
            imageView.image = #imageLiteral(resourceName: "ilu_document_back")
            textLabel.text = AntiFraudLocalizable.documentBack.text
        }
        view.backgroundColor = Palette.ppColorGrayscale000.color
        contentButtonView.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    /// navigates to the next step
    func goToNextStep() {
        guard let controller = ViewsManager.antiFraudStoryboardViewController(withIdentifier: "AntiFraudFrontBackViewController") as? AntiFraudFrontBackViewController else {
            return
        }
        controller.totalStep = totalStep
        controller.step = step + 1
        controller.images = images
        controller.antiFraudChecklistItem = antiFraudChecklistItem
        controller.antiFraudChecklistRow = antiFraudChecklistRow
        controller.delegate = delegate
        navigationController?.pushViewController(controller, animated: true)
    }
    
    /// Just open the native image picker
    func openImagePicker(_ camera: Bool) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        if camera && UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.checkCameraPermission(cameraActionDescription: AntiFraudLocalizable.photographDocuments.text) { [weak self] () -> Void in
                self?.present(imagePicker, animated: true)
            }
        } else {
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true)
        }
    }
}

// MARK: - AntiFraudChecklistViewControllerDelegate

extension AntiFraudFrontBackViewController: AntiFraudChecklistViewControllerDelegate {
    func checklistItemUploaded() {
        antiFraudChecklistItem?.isUploaded = true
    }
}

// MARK: - UIImagePickerControllerDelegate

extension AntiFraudFrontBackViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        dismiss(animated: true) {
            if let image = info[.originalImage] as? UIImage {
                self.seletecPhoto(image)
            }
        }
    }
}
