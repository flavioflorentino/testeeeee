import UIKit

protocol AntiFraudChecklistViewControllerDelegate: AnyObject {
    func checklistItemUploaded()
}

final class AntiFraudChecklistViewController: PPBaseViewController {
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerCell(type: AntiFraudTableViewCell.self)
        tableView.estimatedRowHeight = 70.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
    private let viewModel = AntiFraudChecklistViewModel()
    private var loadingView: UIView?
    
    private var lastSelectedRow: Int?
    private var concludeButton: UIBarButtonItem?
    weak private var delegate: AntiFraudChecklistViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupConstraints()
        setupNavBar()
        setupLoadingView()
        loadChecklist()
    }
    
    // MARK: - Setup views
    
    private func setupLoadingView() {
        loadingView = Loader.getLoadingView(AntiFraudLocalizable.loadingWait.text)
        guard let loader = loadingView else {
            return
        }
        loader.isHidden = false
        view.addSubview(loader)
    }
    
    private func setupNavBar() {
        title = AntiFraudLocalizable.documentsVerification.text
        concludeButton = UIBarButtonItem(title: DefaultLocalizable.btSend.text, style: .done, target: self, action: #selector(concludeChecklist))
        navigationItem.rightBarButtonItem = concludeButton
        concludeButton?.isEnabled = false
    }
    
    private func setupConstraints() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    // MARK: - Private methods
    
    private func loadChecklist() {
        viewModel.loadChecklist(onSuccess: { [weak self] in
            DispatchQueue.main.async {
                self?.loadingView?.isHidden = true
                self?.tableView.reloadData()
                self?.checkIfReadyToConclude()
            }
        }, onError: { [weak self] error in
            DispatchQueue.main.async {
                self?.loadingView?.isHidden = true
                AlertMessage.showCustomAlertWithError(error, controller: self, completion: {
                    self?.navigationController?.popViewController(animated: true)
                })
            }
        })
    }
    
    @objc
    private func concludeChecklist() {
        loadingView?.isHidden = false
        
        viewModel.concludeAntiFraudChecklist(onSuccess: { [weak self] in
            DispatchQueue.main.async {
                self?.loadingView?.isHidden = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: FeedViewController.Observables.FeedNeedReload.rawValue), object: nil)
                self?.navigationController?.popViewController(animated: true)
            }
        }, onError: { [weak self] error in
            DispatchQueue.main.async {
                self?.loadingView?.isHidden = true
                AlertMessage.showAlertWithError(error, controller: self)
            }
        })
    }
    
    private func openImagePicker(useCamera: Bool, checklistItem: AntiFraudChecklistItem) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        if useCamera || checklistItem.useCameraOnly {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
                imagePicker.cameraDevice = checklistItem.useFrontalCamera ? .front : .rear
                imagePicker.checkCameraPermission(cameraActionDescription: AntiFraudLocalizable.photographDocuments.text) { [weak self] in
                    self?.present(imagePicker, animated: true, completion: nil)
                }
            }
        } else {
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true, completion: nil)
        }
    }

    private func setProgress(_ progress: Double, forAttachment attachmentId: Int) {
        if let cell = self.tableView.cellForRow(at: IndexPath(item: attachmentId, section: 0)) as? AntiFraudTableViewCell {
            cell.progressOfUpload = Float(progress)
        }
    }
    
    private func resetProgressAndUpdateChecklist(row: Int, setUploaded: Bool) {
        if let cell = tableView.cellForRow(at: IndexPath(row: row, section: 0)) as? AntiFraudTableViewCell {
            cell.progressOfUpload = 0.0
        }
        if setUploaded {
            viewModel.setItemUploaded(at: row)
        }
        tableView.reloadData()
        checkIfReadyToConclude()
    }
    
    private func checkIfReadyToConclude() {
        guard viewModel.isReadyToConclude else {
            concludeButton?.isEnabled = false
            return
        }
        concludeButton?.isEnabled = true
        showSendNowAlert()
    }
    
    private func showSendNowAlert() {
        let alert = Alert(title: AntiFraudLocalizable.wantToSendNow.text, text: AntiFraudLocalizable.wantToSendNowDescription.text)
        let sendButton = Button(title: DefaultLocalizable.btSend.text, type: .cta) { [weak self] _, _ in
            self?.concludeChecklist()
        }
        let cancelButton = Button(title: DefaultLocalizable.btCancel.text, type: .clean, action: .close)
        alert.buttons = [sendButton, cancelButton]
        
        AlertMessage.showAlert(alert, controller: self)
    }
    
    private func openCameraForDoubleSidedDocument(row: Int, checklistItem: AntiFraudChecklistItem) {
        guard let frontBackVC = ViewsManager.antiFraudStoryboardViewController(withIdentifier: "AntiFraudFrontBackViewController") as? AntiFraudFrontBackViewController else {
            return
        }
        frontBackVC.antiFraudChecklistRow = row
        frontBackVC.antiFraudChecklistItem = checklistItem
        frontBackVC.delegate = self
        delegate = frontBackVC
        navigationController?.pushViewController(frontBackVC, animated: true)
    }
    
    private func openMenuForChoosingTheDocumentInput(checklistItem: AntiFraudChecklistItem) {
        let alert = Alert(title: checklistItem.description, text: nil)
        let takePictureButton = Button(title: AntiFraudLocalizable.takePicture.text, type: .cta) { [weak self] controller, _ in
            self?.openImagePicker(useCamera: true, checklistItem: checklistItem)
            controller.dismiss(animated: false)
        }
        let openLibraryButton = Button(title: AntiFraudLocalizable.chooseFromLibrary.text, type: .cta) { [weak self] controller, _ in
            self?.openImagePicker(useCamera: false, checklistItem: checklistItem)
            controller.dismiss(animated: false)
        }
        let cancelButton = Button(title: DefaultLocalizable.btCancel.text, type: .clean, action: Button.Action.close)
        alert.buttons = [takePictureButton, openLibraryButton, cancelButton]
        AlertMessage.showAlert(alert, controller: self)
    }
    
    private func uploadFrontAndBackOfDocument(front: AntiFraudChecklistViewModel.ImageAttachment, back: AntiFraudChecklistViewModel.ImageAttachment, firstHalf: Bool, row: Int) {
        let file = firstHalf ? front : back
        
        viewModel.uploadAttachment(attachment: file, updateProgress: { [weak self] progress in
            guard let strongSelf = self else {
                return
            }
            let relativeProgress = strongSelf.viewModel.relativeProgress(progress, firstHalf: firstHalf)
            strongSelf.setProgress(relativeProgress, forAttachment: row)
        }, onSuccess: { [weak self] in
                if firstHalf {
                    self?.uploadFrontAndBackOfDocument(front: front, back: back, firstHalf: false, row: row)
                } else {
                    self?.delegate?.checklistItemUploaded()
                    self?.resetProgressAndUpdateChecklist(row: row, setUploaded: true)
                }
        }, onError: { [weak self] error in
            AlertMessage.showAlertWithError(error, controller: self)
            self?.resetProgressAndUpdateChecklist(row: row, setUploaded: false)
        })
    }
}

// MARK: - UITableViewDataSource

extension AntiFraudChecklistViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsChecklist
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(type: AntiFraudTableViewCell.self, forIndexPath: indexPath)
        guard let checklistItem = viewModel.checklistItem(at: indexPath.row) else {
            return UITableViewCell()
        }
        cell.configureCell(title: checklistItem.title, description: checklistItem.description, isUploaded: checklistItem.isUploaded)
        return cell
    }
}

// MARK: - UITableViewDelegate

extension AntiFraudChecklistViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let checklistItem = viewModel.checklistItem(at: indexPath.row) else {
            return
        }
        tableView.deselectRow(at: indexPath, animated: true)
        lastSelectedRow = indexPath.row
        
        switch viewModel.getDocumentCaptureOption(checklistItem) {
        case .cameraDoubleSided:
            openCameraForDoubleSidedDocument(row: indexPath.row, checklistItem: checklistItem)
        case .cameraOnly:
            openImagePicker(useCamera: true, checklistItem: checklistItem)
        case .userChoice:
            openMenuForChoosingTheDocumentInput(checklistItem: checklistItem)
        }
    }
}

// MARK: - AntiFraudFrontBackViewControllerDelegate

extension AntiFraudChecklistViewController: AntiFraudFrontBackViewControllerDelegate {
    func antiFraudFrontBackDidSelectImages(_ controller: AntiFraudFrontBackViewController, images: [UIImage]) {
        guard images.count >= 2, let wsId = controller.antiFraudChecklistItem?.wsId else {
            return
        }
        let row = controller.antiFraudChecklistRow
        let attachmentFront = AntiFraudChecklistViewModel.ImageAttachment(image: images[0], id: wsId, isLast: false)
        let attachmentBack = AntiFraudChecklistViewModel.ImageAttachment(image: images[1], id: wsId, isLast: true)
        uploadFrontAndBackOfDocument(front: attachmentFront, back: attachmentBack, firstHalf: true, row: row)
    }
}

// MARK: - UIImagePickerControllerDelegate

extension AntiFraudChecklistViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[.originalImage] as? UIImage, let row = lastSelectedRow, let wsId = viewModel.checklistItem(at: row)?.wsId else {
            return
        }
        let attachmentImage = AntiFraudChecklistViewModel.ImageAttachment(image: image, id: wsId, isLast: true)
        
        viewModel.uploadAttachment(attachment: attachmentImage, updateProgress: { [weak self] progress in
            self?.setProgress(progress, forAttachment: row)
        }, onSuccess: { [weak self] in
            self?.resetProgressAndUpdateChecklist(row: row, setUploaded: true)
        }, onError: { [weak self] error in
            AlertMessage.showAlertWithError(error, controller: self)
            self?.resetProgressAndUpdateChecklist(row: row, setUploaded: false)
        })
    }
}
