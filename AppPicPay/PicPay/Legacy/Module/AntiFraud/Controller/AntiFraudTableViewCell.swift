import UI
import UIKit

final class AntiFraudTableViewCell: UITableViewCell {
	@objc var progressOfUpload: Float = 0.0 {
		didSet {
			progressBar.setProgress(progressOfUpload, animated: false)
		}
	}
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tintColor = Palette.ppColorBranding300.color
        backgroundColor = Palette.ppColorGrayscale000.color
        titleLabel.textColor = Palette.ppColorGrayscale600.color
        subtitleLabel.textColor = Palette.ppColorGrayscale500.color(withCustomDark: .ppColorGrayscale300)
        progressBar.tintColor = Palette.ppColorBranding300.color
		progressOfUpload = 0.0
    }
	
	override func prepareForReuse() {
		super.prepareForReuse()
		progressBar.setProgress(progressOfUpload, animated: false)
	}
    
    func configureCell(title: String?, description: String?, isUploaded: Bool) {
        titleLabel.text = title
        subtitleLabel.text = description
        accessoryType = isUploaded ? .checkmark : .disclosureIndicator
        progressBar.isHidden = false
    }
}
