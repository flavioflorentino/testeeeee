import Foundation
import QuartzCore
import SkyFloatingLabelTextField
import UI

final class AnalysisAccelerationViewController: PPBaseViewController {
    var feedItemId: String = ""
    var transactionId: String = ""
    var actionType: String = ""
    var topConstraintInitialValue: CGFloat = 0
    
    @IBOutlet weak var textCode: SkyFloatingLabelTextFieldWithIcon! {
        didSet {
            textCode.textColor = Palette.ppColorGrayscale600.color
            textCode.iconColor = Palette.ppColorGrayscale500.color(withCustomDark: .ppColorGrayscale300)
        }
    }
    @IBOutlet weak var buttonSendCode: UIPPButton! {
        didSet {
            buttonSendCode.setTitleColor(Palette.white.color)
            buttonSendCode.setBackgroundColor(Palette.ppColorBranding300.color)
        }
    }
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var viewTopContraint: NSLayoutConstraint!
    @IBOutlet weak var explainText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        errorLabel.text = ""
        textCode.delegate = self
        textCode.iconFont = UIFont.boldSystemFont(ofSize: 20)
        
        buttonSendCode.isEnabled = false
        buttonSendCode.cornerRadius = 22
        buttonSendCode.clipsToBounds = true
        buttonSendCode.alpha = 0.5
        
        setupColors()
    }
    
    @IBAction private func buttonSendCodeClicked(_ sender: Any) {
        guard let textCodeString = textCode.text else {
            return
        }
        textCode.resignFirstResponder()
        buttonSendCode.startLoadingAnimating()
        errorLabel.text = ""
        
        WSTransaction.setSoftDescriptorByActionType(transactionId, verification_code: textCodeString, action_type: actionType) { [weak self] _, error in
            DispatchQueue.main.async {
                self?.descriptorByActionHandler(error: error)
            }
        }
    }
    
    private func descriptorByActionHandler(error: Error?) {
        if error != nil {
            errorLabel.text = error?.localizedDescription
            textCode.errorMessage = " "
            
            textCode.text = ""
            buttonSendCode.stopLoadingAnimating()
            buttonSendCode.isEnabled = false
            buttonSendCode.alpha = 0.5
        } else {
            textCode.errorMessage = nil
            
            if !transactionId.isEmpty {
                // Envia mensagem para atualizar o recibo
                NotificationCenter.default.post(
                    name: NSNotification.Name(rawValue: ReceiptWidgetViewController.Observables.NeedReloadReceipt.rawValue),
                    object: nil,
                    userInfo: [ReceiptWidgetViewController.UserInfoKey.TransactionId: transactionId]
                )
                
                WSNewFeed.feed(feedItemId, completion: { [weak self] item, _ in
                    if let feedItem = item {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: FeedViewController.Observables.NeedReloadItem.rawValue), object: nil, userInfo: ["feedItem": feedItem])
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FeedDetailViewControllerReloadFeedItem"), object: nil, userInfo: ["feedItem": feedItem])
                    }
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                        self?.showConfirmationAlertAndPopView()
                    })
                })
            } else {
                textCode.text = ""
                buttonSendCode.stopLoadingAnimating()
                buttonSendCode.isEnabled = false
                buttonSendCode.alpha = 0.5
            }
        }
    }
    
    private func showConfirmationAlertAndPopView() {
        navigationController?.popViewController(animated: true)
        let alert = UIAlertController(title: AntiFraudLocalizable.ready.text, message: AntiFraudLocalizable.waitForPaymentConclusion.text, preferredStyle: .alert)
        let okAction = UIAlertAction(title: DefaultLocalizable.btOk.text, style: .default, handler: nil)
        alert.addAction(okAction)
        navigationController?.parent?.present(alert, animated: true, completion: nil)
    }
    
    private func setupColors() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationController?.navigationBar.backgroundColor = Palette.ppColorGrayscale100.color
    }
    
    // MARK: Handler
    
    /**
     * Handler apenas para telas pequenas que o teclado fica por cima do campo de texto
     */
    @objc
    func keyboardWillChange(_ notification: Notification) {
        guard let userInfo = (notification as NSNotification).userInfo,
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        let duration: TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
        let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
        let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
        let animationCurve = UIView.AnimationOptions(rawValue: animationCurveRaw)
        
        if endFrame.origin.y >= UIScreen.main.bounds.size.height {
            viewTopContraint.constant = topConstraintInitialValue
        } else {
            topConstraintInitialValue = viewTopContraint.constant
            viewTopContraint.constant -= (viewTopContraint.constant + explainText.intrinsicContentSize.height)
        }
        
        UIView.animate(withDuration: duration, delay: TimeInterval(0), options: animationCurve, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}

// MARK: UITextFieldDelegate

extension AnalysisAccelerationViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let text = errorLabel.text else {
            return
        }
        if !text.isEmpty {
            errorLabel.text = ""
            textCode.errorMessage = nil
        }
    }
    
    func textField(_ textCode: UITextField, shouldChangeCharactersIn range: NSRange, replacementString text: String) -> Bool {
        guard let charCount = textCode.text?.count else {
            return false
        }
        
        let lenght = charCount + (text.count - range.length)
        
        if lenght > 1 {
            buttonSendCode.isEnabled = true
            buttonSendCode.alpha = 1
        } else {
            buttonSendCode.isEnabled = false
            buttonSendCode.alpha = 0.5
        }
        
        return lenght <= 3
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
