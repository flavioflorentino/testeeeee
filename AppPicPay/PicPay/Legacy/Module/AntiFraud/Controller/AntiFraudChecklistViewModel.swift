import UIKit

final class AntiFraudChecklistViewModel {
    typealias ImageAttachment = (image: UIImage, id: String, isLast: Bool)
    
    enum DocumentCaptureOption {
        case cameraOnly, cameraDoubleSided, userChoice
    }
    
    private var checklist = [AntiFraudChecklistItem]()
    let api: AntiFraudApiProtocol
    
    var numberOfRowsChecklist: Int {
        return checklist.count
    }
    
    var isReadyToConclude: Bool {
        if !checklist.isEmpty {
            return !checklist.contains(where: { !$0.isUploaded })
        }
        return false
    }
    
    init(with api: AntiFraudApiProtocol = AntiFraudApi()) {
        self.api = api
    }
    
    func loadChecklist(onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        api.getAntiFraudChecklist { [weak self] result in
            switch result {
            case .success(let data):
                if let loadedChecklist = data.checklist {
                    self?.checklist = loadedChecklist
                }
                onSuccess()
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    func uploadAttachment(attachment: ImageAttachment, updateProgress: @escaping ((Double) -> Void), onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        guard let imageForUpload = attachment.image.resizeImage(targetSize: CGSize(width: 1200, height: 1200)), let imageData = imageForUpload.jpegData(compressionQuality: 0.8) else {
            return
        }
        api.uploadAttachment(imageData: imageData, attachmentId: attachment.id, lastAttachment: attachment.isLast, uploadProgress: { progress in
            DispatchQueue.main.async {
                updateProgress(progress)
            }
        }, { result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    onSuccess()
                case .failure(let error):
                    onError(error)
                }
            }
        })
    }
    
    func concludeAntiFraudChecklist(onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        api.concludeAntiFraudChecklist { result in
            switch result {
            case .success:
                onSuccess()
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    func checklistItem(at row: Int) -> AntiFraudChecklistItem? {
        if row < checklist.count {
            return checklist[row]
        }
        return nil
    }
    
    func getDocumentCaptureOption(_ checklistItem: AntiFraudChecklistItem) -> DocumentCaptureOption {
        if checklistItem.isDoubleSided {
            return .cameraDoubleSided
        } else if checklistItem.useCameraOnly {
            return .cameraOnly
        }
        return .userChoice
    }
    
    func setItemUploaded(at row: Int) {
        if row < checklist.count {
            checklist[row].isUploaded = true
        }
    }
    
    func relativeProgress(_ progress: Double, firstHalf: Bool) -> Double {
        return (firstHalf ? 0 : 0.5) + (progress / 2)
    }
}
