import Foundation

enum AntiFraudLocalizable: String, Localizable {
    case loadingWait
    case documentsVerification
    case photographDocuments
    case wantToSendNow
    case wantToSendNowDescription
    case takePicture
    case chooseFromLibrary
    case documentBack
    case ready
    case waitForPaymentConclusion
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .antiFraud
    }
}
