import Foundation

protocol AntiFraudApiProtocol {
    func getAntiFraudChecklist(_ completion: @escaping (PicPayResult<AntiFraudChecklist>) -> Void)
    func uploadAttachment(imageData: Data, attachmentId: String, lastAttachment: Bool, uploadProgress: @escaping ((Double) -> Void), _ completion: @escaping ((PicPayResult<AntiFraudGenericResponse>) -> Void))
    func concludeAntiFraudChecklist(_ completion: @escaping ((PicPayResult<AntiFraudGenericResponse>) -> Void))
}

final class AntiFraudApi: BaseApi, AntiFraudApiProtocol {
    func getAntiFraudChecklist(_ completion: @escaping (PicPayResult<AntiFraudChecklist>) -> Void) {
        requestManager.apiRequest(endpoint: kWsUrlGetAntiFraudChecklist, method: .get).responseApiCodable(completionHandler: completion)
    }
    
    func uploadAttachment(imageData: Data, attachmentId: String, lastAttachment: Bool, uploadProgress: @escaping ((Double) -> Void), _ completion: @escaping ((PicPayResult<AntiFraudGenericResponse>) -> Void)) {
        let files: [FileUpload] = [FileUpload(key: "image", data: imageData, fileName: "imagem.jpg", mimeType: "image/jpeg")]
        let params = ["attachment_id": attachmentId, "last_attachment": (lastAttachment ? "1" : "0" )]
        requestManager.apiUploadCodable(WebServiceInterface.apiEndpoint(kWsUrlDocumentResquetUpload), method: .post, files: files, parameters: params, uploadProgress: uploadProgress, completion: completion)
    }
    
    func concludeAntiFraudChecklist(_ completion: @escaping ((PicPayResult<AntiFraudGenericResponse>) -> Void)) {
        requestManager.apiRequest(endpoint: WebServiceInterface.apiEndpoint(kWsUrlConcludeAntiFraudChecklist), method: .post, parameters: ["": ""]).responseApiCodable(completionHandler: completion)
    }
}
