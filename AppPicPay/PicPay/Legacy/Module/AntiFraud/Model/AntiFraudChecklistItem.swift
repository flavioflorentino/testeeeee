import Foundation

struct AntiFraudChecklist: Codable {
    enum CodingKeys: String, CodingKey {
        case data
        case checklist = "AntiFraudChecklist"
        case error = "Error"
    }
    
    var checklist: [AntiFraudChecklistItem]?

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dataContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        checklist = try dataContainer.decodeIfPresent([AntiFraudChecklistItem].self, forKey: .checklist)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        var data = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        try data.encode(checklist, forKey: .checklist)
    }
}

struct AntiFraudChecklistItem: Codable {
    enum CodingKeys: String, CodingKey {
        case title
        case wsId = "id"
        case description
        case isUploaded = "uploaded"
        case useCameraOnly = "cameraOnly"
        case useFrontalCamera = "frontalCamera"
        case isDoubleSided = "doubleSided"
        case attachmentType
    }
    
    let title: String
    let wsId: String
    let description: String
    var isUploaded: Bool
    let useCameraOnly: Bool
    let useFrontalCamera: Bool
    let isDoubleSided: Bool
    let attachmentType: String
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        wsId = try container.decode(String.self, forKey: .wsId)
        description = try container.decode(String.self, forKey: .description)
        
        let uploaded = try container.decode(String.self, forKey: .isUploaded)
        isUploaded = uploaded == "true"
        
        let cameraOnly = try container.decode(String.self, forKey: .useCameraOnly)
        useCameraOnly = cameraOnly == "1"
        
        let frontalCamera = try container.decode(String.self, forKey: .useFrontalCamera)
        useFrontalCamera = frontalCamera == "1"
        
        let doubleSided = try container.decode(Int.self, forKey: .isDoubleSided)
        isDoubleSided = doubleSided == 1
        
        attachmentType = try container.decode(String.self, forKey: .attachmentType)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(title, forKey: .title)
        try container.encode(wsId, forKey: .wsId)
        try container.encode(description, forKey: .description)
        try container.encode(isUploaded, forKey: .isUploaded)
        try container.encode(useCameraOnly, forKey: .useCameraOnly)
        try container.encode(useFrontalCamera, forKey: .useFrontalCamera)
        try container.encode(isDoubleSided, forKey: .isDoubleSided)
        try container.encode(attachmentType, forKey: .attachmentType)
    }
}

struct AntiFraudGenericResponse: Codable {
    var data: String?
}
