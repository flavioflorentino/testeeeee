import Core
import FeatureFlag
import Foundation
import PCI

final class PciPavService: NSObject {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    private let service: PAVServiceProtocol = PAVService()
    
    @objc var paymentIsPci: Bool {
        FeatureManager.isActive(.pciPAV)
    }
    
    @objc
    func createTransaction(
        cvv: String?,
        password: String,
        request: NSDictionary,
        valueCard: Double,
        onSuccess: @escaping ([String: Any]) -> Void,
        onError: @escaping (NSError) -> Void
    ) {
        guard let pavPayload = createPAVPayload(request: request) else {
            let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            onError(error)
            return
        }
        let cvv = createCVVPayload(valueCard: valueCard, informedCvv: cvv)
        let isFeedzai = FeatureManager.shared.isActive(.transaction_pav_v2_feedzai)
        let payload = PaymentPayload<PAVPayload>(cvv: cvv, generic: pavPayload)
        service.createTransaction(
            password: password,
            isFeedzai: isFeedzai,
            payload: payload,
            isNewArchitecture: false
        ) { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let value):
                    onSuccess(value.json)
                    
                case .failure(let error):
                    onError(error.picpayError)
                }
            }
        }
    }
    
    private func createPAVPayload(request: NSDictionary) -> PAVPayload? {
        guard let data = try? JSONSerialization.data(withJSONObject: request, options: .prettyPrinted) else {
            return nil
        }
        
        return try? JSONDecoder.decode(data, to: PAVPayload.self)
    }
    
    private func createCVVPayload(valueCard: Double, informedCvv: String?) -> CVVPayload? {
        guard let cvv = informedCvv else {
            return createLocalStoreCVVPayload(valueCard: valueCard)
        }
       
        return CVVPayload(value: cvv)
    }
    
    private func createLocalStoreCVVPayload(valueCard: Double) -> CVVPayload? {
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        guard !valueCard.isZero,
              let cvv = dependencies.keychain.getData(key: KeychainKeyPF.cvv(id)) else {
            return nil
        }

        return CVVPayload(value: cvv)
    }
}
