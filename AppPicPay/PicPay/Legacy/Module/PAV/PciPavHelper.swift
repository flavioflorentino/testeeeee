import Core
import Foundation
import PCI
import UI

final class PciPavHelper: NSObject {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    private var coordinator: Coordinating?
   
    @objc var informedCvv: String?
    @objc
    func checkCvvFlow(manager: PPPaymentManager, navigationController: UINavigationController?, onSuccess: @escaping () -> Void, onError:  @escaping (PicPayError) -> Void) {
        guard checkNeedCvv(manager: manager)  else {
            onSuccess()
            return
        }
        
        createCvvFlowCoordinator(
            navigationController: navigationController,
            completedCvv: { [weak self] cvv in
                self?.informedCvv = cvv
                onSuccess()
            },
            completedNoCvv: {
                let error = PicPayError(message: DefaultLocalizable.cvvNotInformed.text)
                onError(error)
            }
        )
    }
    
    @objc
    func saveCvv() {
        guard let cvv = informedCvv else {
            return
        }
        
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        dependencies.keychain.set(key: KeychainKeyPF.cvv(id), value: cvv)
    }
    
    private func checkNeedCvv(manager: PPPaymentManager) -> Bool {
        guard let cardBank = CreditCardManager.shared.defaultCreditCard, let cardValue = manager.cardTotal()?.doubleValue else {
            return false
        }
        
        return CvvRegisterFlowCoordinator.cvvFlowEnable(cardBank: cardBank, cardValue: cardValue, paymentType: .pav)
    }
    
    private func createCvvFlowCoordinator(
        navigationController: UINavigationController?,
        completedCvv: @escaping (String) -> Void,
        completedNoCvv: @escaping () -> Void
    ) {
        guard let navigationController = navigationController else {
            return
        }
        
        let coordinator = CvvRegisterFlowCoordinator(
            navigationController: navigationController,
            paymentType: .pav,
            finishedCvv: completedCvv,
            finishedWithoutCvv: completedNoCvv
        )
        coordinator.start()

        self.coordinator = coordinator
    }
}
