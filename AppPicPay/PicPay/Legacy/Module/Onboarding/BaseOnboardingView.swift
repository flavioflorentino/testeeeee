import UI
import UIKit

protocol BaseOnboardingViewDelegate: AnyObject {
   func didTapSkipButton()
}

final class BaseOnboardingView: UIView {
    enum Layout {
        static let margin: CGFloat = 24.0
        static let buttonHeight: CGFloat = 48.0
        static let imageHeight: CGFloat = 100.0
        static let closeButtonSize: CGFloat = 28.0
        static let closeButtonMargin: CGFloat = 16.0
    }
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setTitle(OnboardingLocalizable.skip.text, for: .normal)
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.addTarget(self, action: #selector(tapSkipButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .center
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        label.textColor = Palette.ppColorGrayscale500.color
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.textColor = Palette.ppColorGrayscale500.color
        label.textWith(spacing: 4, aligment: .center)
        return label
    }()
    
    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 8
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.layoutMargins = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    var image: UIImage? {
        didSet {
            imageView.image = image
        }
    }
    
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    var message: String? {
        didSet {
            messageLabel.text = message
            messageLabel.textWith(spacing: 4, aligment: .center)
        }
    }
    
    weak var delegate: BaseOnboardingViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildViewHierarchy() {
        mainStackView.addArrangedSubview(imageView)
        mainStackView.addArrangedSubview(titleLabel)
        mainStackView.addArrangedSubview(messageLabel)
        mainStackView.addArrangedSubview(buttonsStackView)
        addSubview(mainStackView)
        addSubview(closeButton)
    }
    
    private func setupConstraints() {
        var topConstant: CGFloat = 20 // status bar height
        if #available(iOS 11, *) {
            topConstant = 0
        }
        
        NSLayoutConstraint.activate([
            mainStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Layout.margin),
            mainStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Layout.margin),
            mainStackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            imageView.heightAnchor.constraint(equalToConstant: Layout.imageHeight),
            
            closeButton.topAnchor.constraint(equalTo: compatibleSafeAreaLayoutGuide.topAnchor, constant: Layout.closeButtonMargin + topConstant),
            closeButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Layout.closeButtonMargin),
            closeButton.heightAnchor.constraint(equalToConstant: Layout.closeButtonSize)
        ])
    }
    
    private func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    func addNewButtonView(_ view: UIView) {
        buttonsStackView.addArrangedSubview(view)
        view.heightAnchor.constraint(equalToConstant: Layout.buttonHeight).isActive = true
    }
    
    @objc
    private func tapSkipButton() {
        delegate?.didTapSkipButton()
    }
}
