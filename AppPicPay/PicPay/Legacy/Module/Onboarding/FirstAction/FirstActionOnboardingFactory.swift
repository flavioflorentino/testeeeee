final class FirstActionOnboardingFactory {
    static func make(with delegate: OnboardingFirstActionDelegate) -> FirstActionOnboardingViewController {
        let service: FirstActionOnboardingServicing = FirstActionOnboardingService(consumerWorker: ConsumerWorker())
        let viewModel: FirstActionOnboardingViewModelType = FirstActionOnboardingViewModel(
            service: service,
            dependencies: DependencyContainer()
        )
        var coordinator: FirstActionOnboardingCoordinating = FirstActionOnboardingCoordinator()
        let viewController = FirstActionOnboardingViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        coordinator.delegate = delegate
        viewModel.outputs = viewController

        return viewController
    }
}
