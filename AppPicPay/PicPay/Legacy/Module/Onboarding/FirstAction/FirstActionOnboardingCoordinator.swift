enum FirstActionOnboardingAction {
    case close
    case openDeeplink(URL?)
}

protocol OnboardingFirstActionDelegate: AnyObject {
    func exitOnboarding()
}

protocol FirstActionOnboardingCoordinating {
    var viewController: UIViewController? { get set }
    var delegate: OnboardingFirstActionDelegate? { get set }
    func perform(action: FirstActionOnboardingAction)
}

final class FirstActionOnboardingCoordinator: FirstActionOnboardingCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: OnboardingFirstActionDelegate?
    
    func perform(action: FirstActionOnboardingAction) {
        switch action {
        case .close:
            delegate?.exitOnboarding()
            
        case .openDeeplink(let url):
            delegate?.exitOnboarding()
            DeeplinkHelper.handleDeeplink(withUrl: url)
        }
    }
}
