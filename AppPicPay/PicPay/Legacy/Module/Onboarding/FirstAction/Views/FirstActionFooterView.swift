import UI

protocol FirstActionFooterViewDelegate: AnyObject {
    func didTapGoToHomeButton()
}

final class FirstActionFooterView: UICollectionReusableView {
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .light)
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        label.text = OnboardingLocalizable.firstActionFooterMessage.text
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var goToHomeButton: UIButton = {
        let button = UIButton()
        let text = OnboardingLocalizable.exploreTheApp.text
        let titleFont = UIFont.systemFont(ofSize: 16, weight: .bold)
        var title = NSMutableAttributedString(string: text, attributes: [.font: titleFont, .foregroundColor: Palette.ppColorBranding400.color, .underlineStyle: 1])
        button.setAttributedTitle(title, for: .normal)
        button.addTarget(self, action: #selector(tapGoToHomeButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    weak var delegate: FirstActionFooterViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        buildViewHierarchy()
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildViewHierarchy() {
        addSubview(textLabel)
        addSubview(goToHomeButton)
    }
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            textLabel.topAnchor.constraint(equalTo: topAnchor),
            textLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            textLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            goToHomeButton.topAnchor.constraint(equalTo: textLabel.bottomAnchor, constant: 6),
            goToHomeButton.leadingAnchor.constraint(equalTo: leadingAnchor),
            goToHomeButton.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor)
        ])
    }
    
    @objc
    private func tapGoToHomeButton() {
        delegate?.didTapGoToHomeButton()
    }
}
