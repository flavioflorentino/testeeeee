import AnalyticsModule
import Core
import FeatureFlag
import UI

protocol FirstActionOnboardingViewModelInputs {
    func fetchData()
    func didTapGoToHomeButton()
    func tapActionCell(at index: Int)
}

protocol FirstActionOnboardingViewModelOutputs: AnyObject {
    func setupDataSourceHandler(withData data: [FirstActionCellPresenter])
    func perform(action: FirstActionOnboardingAction)
    func setupCustomTitle(_ title: NSAttributedString)
}

protocol FirstActionOnboardingViewModelType: AnyObject {
    var inputs: FirstActionOnboardingViewModelInputs { get }
    var outputs: FirstActionOnboardingViewModelOutputs? { get set }
}

final class FirstActionOnboardingViewModel: FirstActionOnboardingViewModelType, FirstActionOnboardingViewModelInputs {
    typealias Dependencies = HasFeatureManager
    var inputs: FirstActionOnboardingViewModelInputs { return self }
    weak var outputs: FirstActionOnboardingViewModelOutputs?

    private let service: FirstActionOnboardingServicing
    private let dependencies: Dependencies
    
    private var actionList: [FirstActionCellPresenter]
    
    private var customTitle: NSAttributedString {
        let helloText = OnboardingLocalizable.hello.text
        let userFirstName = service.userFullName?.components(separatedBy: " ").first ?? ""
        let commaAndSpace = userFirstName.isEmpty ? "" : ", "
        
        let titleFont = UIFont.systemFont(ofSize: 28, weight: .bold)
        let titleAttributes: [NSAttributedString.Key: Any] = [.font: titleFont, .foregroundColor: Palette.ppColorGrayscale500.color]
        let title = NSMutableAttributedString(string: "\(helloText)\(commaAndSpace)\(userFirstName).", attributes: titleAttributes)
        
        let firstNameRange = NSRange(location: helloText.count + commaAndSpace.count, length: userFirstName.count)
        title.addAttribute(.foregroundColor, value: Palette.ppColorBranding400.color, range: firstNameRange)
        return title
    }

    init(service: FirstActionOnboardingServicing, dependencies: Dependencies) {
        self.service = service
        self.dependencies = dependencies
        actionList = []
    }

    func fetchData() {
        outputs?.setupCustomTitle(customTitle)
        
        actionList = service.firstActionOptions
        outputs?.setupDataSourceHandler(withData: actionList)
    }
    
    func didTapGoToHomeButton() {
        Analytics.shared.log(OnboardingEvent.firstAction("Explorar o app"))
        
        if dependencies.featureManager.isActive(.appTour) {
            KVStore().setBool(true, with: .showAppTour)
        }
        outputs?.perform(action: .close)
    }
    
    func tapActionCell(at index: Int) {
        guard index < actionList.count, let url = actionList[index].deeplinkUrl else {
            return
        }
        let actionName = actionList[index].name
        Analytics.shared.log(OnboardingEvent.firstAction(actionName))
        
        outputs?.perform(action: .openDeeplink(url))
    }
}
