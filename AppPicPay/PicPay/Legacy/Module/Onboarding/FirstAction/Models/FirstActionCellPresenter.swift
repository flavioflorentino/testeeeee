struct FirstActionCellPresenter: Decodable {
    let name: String
    private let image: String
    private let deeplink: String
    
    var text: String {
        return name
    }
    
    var imageUrl: URL? {
        return URL(string: image)
    }
    
    var deeplinkUrl: URL? {
        return URL(string: deeplink)
    }
}
