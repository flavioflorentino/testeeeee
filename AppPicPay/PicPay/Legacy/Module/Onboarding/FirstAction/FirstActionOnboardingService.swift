import FeatureFlag

protocol FirstActionOnboardingServicing {
    var firstActionOptions: [FirstActionCellPresenter] { get }
    var userFullName: String? { get }
}

final class FirstActionOnboardingService: FirstActionOnboardingServicing {
    private let consumerWorker: ConsumerWorkerProtocol
    
    var userFullName: String? {
        return consumerWorker.consumer?.name
    }
    
    var firstActionOptions: [FirstActionCellPresenter] {
        let constants = FeatureManager.object(.onboardingFirstActionList, type: [FirstActionCellPresenter].self)
        return constants ?? []
    }
    
    init(consumerWorker: ConsumerWorkerProtocol) {
        self.consumerWorker = consumerWorker
    }
}
