import UI

final class FirstActionOnboardingViewController: LegacyViewController<FirstActionOnboardingViewModelType, FirstActionOnboardingCoordinating, UIView> {
    private enum Layout {
        static let cellHeight: CGFloat = 128
        static let interitemSpacing: CGFloat = 10
        static let numbersOfCellsPerRow: CGFloat = 3
        static let collectionViewMargin: CGFloat = 24
    }
    
    private enum ReuseIdentifier {
        static let cell = String(describing: FirstActionViewCell.self)
        static let header = String(describing: FirstActionHeaderView.self)
        static let footer = String(describing: FirstActionFooterView.self)
    }
    
    private lazy var cellWidth: CGFloat = {
        let totalInteritemSpacing = Layout.interitemSpacing * (Layout.numbersOfCellsPerRow - 1)
        return (collectionView.frame.width - totalInteritemSpacing) / Layout.numbersOfCellsPerRow
    }()
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 32, left: 0, bottom: 26, right: 0)
        
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.delegate = self
        view.showsVerticalScrollIndicator = false
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.register(FirstActionViewCell.self, forCellWithReuseIdentifier: ReuseIdentifier.cell)
        view.register(FirstActionHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ReuseIdentifier.header)
        view.register(FirstActionFooterView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: ReuseIdentifier.footer)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var headerView: UICollectionReusableView {
        let indexPath = IndexPath(item: 0, section: 0)
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ReuseIdentifier.header, for: indexPath)
        guard let firstActionHeader = header as? FirstActionHeaderView, let title = customTitle else {
            return header
        }
        firstActionHeader.configure(title: title)
        return firstActionHeader
    }
    
    private var footerView: UICollectionReusableView {
        let indexPath = IndexPath(item: 0, section: 0)
        let footer = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: ReuseIdentifier.footer, for: indexPath)
        guard let firstActionFooter = footer as? FirstActionFooterView else {
            return footer
        }
        firstActionFooter.delegate = self
        return firstActionFooter
    }
    
    private var dataHandler: CollectionViewHandler<FirstActionCellPresenter, FirstActionViewCell>?
    
    private var customTitle: NSAttributedString?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buildViewHierarchy()
        setupConstraints()
        configureViews()
        viewModel.inputs.fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(collectionView)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.collectionViewMargin),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Layout.collectionViewMargin),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
}

extension FirstActionOnboardingViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: Layout.cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.inputs.tapActionCell(at: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
}

extension FirstActionOnboardingViewController: FirstActionFooterViewDelegate {
    func didTapGoToHomeButton() {
        viewModel.inputs.didTapGoToHomeButton()
    }
}

// MARK: View Model Outputs
extension FirstActionOnboardingViewController: FirstActionOnboardingViewModelOutputs {
    func setupDataSourceHandler(withData data: [FirstActionCellPresenter]) {
        dataHandler = CollectionViewHandler(data: data, cellType: FirstActionViewCell.self, configureCell: { _, model, cell in
            cell.configure(imageUrl: model.imageUrl, text: model.text)
        })
        
        dataHandler?.supplementaryView = { [weak self] kind, _ in
            switch kind {
            case UICollectionView.elementKindSectionHeader:
                return self?.headerView
            case UICollectionView.elementKindSectionFooter:
                return self?.footerView
            default:
                return nil
            }
        }
        
        collectionView.dataSource = dataHandler
        collectionView.reloadData()
    }
    
    func perform(action: FirstActionOnboardingAction) {
        coordinator.perform(action: action)
    }
    
    func setupCustomTitle(_ title: NSAttributedString) {
        customTitle = title
    }
}
