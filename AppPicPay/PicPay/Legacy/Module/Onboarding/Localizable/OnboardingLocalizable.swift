enum OnboardingLocalizable: String, Localizable {
    case skip
    
    case onboardingSocialTitle
    case onboardingSocialMessage
    case authorizeAddressBook
    case notNow
    
    case inviteFriendsDefaultTitle
    case inviteFriendsDefaultMessage
    case inviteFriends
    
    case followFriendsLocalContacts
    case followFriendsRecommendedContacts
    case followLocalFriendsTitle
    case followRecommendedFriendsTitle
    
    case addCardOnboardTitle
    case addCardOnboardMessage
    case addCreditCard
    case noCard
    case addCardLater
    case or
    
    case hello
    case firstActionHeaderMessage
    case firstActionFooterMessage
    case exploreTheApp
    
    case beginCashInCarouselPage
    case beginCashInCarouselTitle
    case beginCashInCarouselMessage
    
    case cardCashInCarouselPage
    case cardCashInCarouselTitle
    case cardCashInCarouselMessage
    
    case balanceCashInCarouselPage
    case balanceCashInCarouselTitle
    case balanceCashInCarouselMessage
    
    case doItLater
    case cashInOptionsHeaderTitle
    case cashInOptionsHeaderMessage
    case creditCard
    case instantlyAvailable
    case addCard
    case addMoney

    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .onboarding
    }
}
