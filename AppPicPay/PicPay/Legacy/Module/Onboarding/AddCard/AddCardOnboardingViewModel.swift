import AnalyticsModule

protocol AddCardOnboardingViewModelInputs {
    func skipIfCreditCardRegistered()
    func didTapAddCardButton()
    func didTapNoCardButton()
    func didTapAddCardLaterButton()
    func didTapSkipButton()
}

protocol AddCardOnboardingViewModelOutputs: AnyObject {
    func perform(action: AddCardOnboardingAction)
}

protocol AddCardOnboardingViewModelType: AnyObject {
    var inputs: AddCardOnboardingViewModelInputs { get }
    var outputs: AddCardOnboardingViewModelOutputs? { get set }
}

final class AddCardOnboardingViewModel: AddCardOnboardingViewModelType, AddCardOnboardingViewModelInputs {
    var inputs: AddCardOnboardingViewModelInputs { return self }
    weak var outputs: AddCardOnboardingViewModelOutputs?

    private let service: AddCardOnboardingServiceProtocol

    init(service: AddCardOnboardingServiceProtocol) {
        self.service = service
    }
    
    func skipIfCreditCardRegistered() {
        guard service.isCreditCardRegistered else {
            return
        }
        outputs?.perform(action: .skip)
    }
    
    func didTapAddCardButton() {
        Analytics.shared.log(OnboardingEvent.creditCard(.registerCreditCard))
        outputs?.perform(action: .addCreditCard)
    }
    
    func didTapNoCardButton() {
        Analytics.shared.log(OnboardingEvent.creditCard(.doNotHaveACard))
        outputs?.perform(action: .skip)
    }
    
    func didTapAddCardLaterButton() {
        Analytics.shared.log(OnboardingEvent.creditCard(.registerLater))
        outputs?.perform(action: .skip)
    }
    
    func didTapSkipButton() {
        Analytics.shared.log(OnboardingEvent.creditCard(.skipped))
        outputs?.perform(action: .skip)
    }
}
