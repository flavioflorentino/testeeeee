import UI
import UIKit

protocol NoCardOrAddCardLaterViewDelegate: AnyObject {
    func didTapNoCardButton()
    func didTapAddCardLaterButton()
}

final class NoCardOrAddCardLaterView: UIView {
    enum Layout {
        static let font = UIFont.systemFont(ofSize: 14, weight: .medium)
        static let textColor = Palette.ppColorGrayscale400.color
    }
    
    weak var delegate: NoCardOrAddCardLaterViewDelegate?
    
    private lazy var noCardButton: UIButton = {
        let attributes: [NSAttributedString.Key: Any] = [.underlineStyle: 1, .font: Layout.font, .foregroundColor: Layout.textColor]
        let title = NSAttributedString(string: OnboardingLocalizable.noCard.text, attributes: attributes)
        let button = UIButton()
        button.setAttributedTitle(title, for: .normal)
        button.addTarget(self, action: #selector(tapNoCardButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var orLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.font
        label.text = OnboardingLocalizable.or.text
        label.textColor = Palette.ppColorGrayscale400.color
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var addCardLaterButton: UIButton = {
        let attributes: [NSAttributedString.Key: Any] = [.underlineStyle: 1, .font: Layout.font, .foregroundColor: Layout.textColor]
        let title = NSAttributedString(string: OnboardingLocalizable.addCardLater.text, attributes: attributes)
        let button = UIButton()
        button.setAttributedTitle(title, for: .normal)
        button.addTarget(self, action: #selector(tapAddCardLaterButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildViewHierarchy()
        setupConstraints()
    }
    
    @available(*, unavailable)
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildViewHierarchy() {
        stackView.addArrangedSubview(noCardButton)
        stackView.addArrangedSubview(orLabel)
        stackView.addArrangedSubview(addCardLaterButton)
        addSubview(stackView)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    @objc
    private func tapNoCardButton() {
        delegate?.didTapNoCardButton()
    }
    
    @objc
    private func tapAddCardLaterButton() {
        delegate?.didTapAddCardLaterButton()
    }
}
