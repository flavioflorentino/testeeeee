import UI
import UIKit

final class AddCardOnboardingViewController: LegacyViewController<AddCardOnboardingViewModelType, AddCardOnboardingCoordinating, BaseOnboardingView> {
    private lazy var addCardButton: UIPPButton = {
        let title = OnboardingLocalizable.addCreditCard.text
        let button = UIPPButton(title: title, target: self, action: #selector(tapAddCardButton), type: .cta)
        button.cornerRadius = 24
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var noCardOrAddCardLaterView: NoCardOrAddCardLaterView = {
        let view = NoCardOrAddCardLaterView()
        view.delegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.inputs.skipIfCreditCardRegistered()
    }
    
    override func configureViews() {
        rootView.image = #imageLiteral(resourceName: "credit_cards")
        rootView.title = OnboardingLocalizable.addCardOnboardTitle.text
        rootView.message = OnboardingLocalizable.addCardOnboardMessage.text
        rootView.addNewButtonView(addCardButton)
        rootView.addNewButtonView(noCardOrAddCardLaterView)
        rootView.delegate = self
    }
    
    @objc
    private func tapAddCardButton() {
        viewModel.inputs.didTapAddCardButton()
    }
}

extension AddCardOnboardingViewController: BaseOnboardingViewDelegate {
    func didTapSkipButton() {
        viewModel.inputs.didTapSkipButton()
    }
}

extension AddCardOnboardingViewController: NoCardOrAddCardLaterViewDelegate {
    func didTapNoCardButton() {
        viewModel.inputs.didTapNoCardButton()
    }
    
    func didTapAddCardLaterButton() {
        viewModel.inputs.didTapAddCardLaterButton()
    }
}

// MARK: View Model Outputs
extension AddCardOnboardingViewController: AddCardOnboardingViewModelOutputs {
    func perform(action: AddCardOnboardingAction) {
           coordinator.perform(action: action)
    }
}
