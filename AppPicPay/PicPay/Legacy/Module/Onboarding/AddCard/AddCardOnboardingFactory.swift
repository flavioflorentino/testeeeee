final class AddCardOnboardingFactory {
    static func make(with delegate: OnboardingAddCardDelegate) -> AddCardOnboardingViewController {
        let service: AddCardOnboardingServiceProtocol = AddCardOnboardingService(cardManager: CreditCardManager.shared)
        let viewModel: AddCardOnboardingViewModelType = AddCardOnboardingViewModel(service: service)
        var coordinator: AddCardOnboardingCoordinating = AddCardOnboardingCoordinator()
        let viewController = AddCardOnboardingViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        coordinator.delegate = delegate
        viewModel.outputs = viewController

        return viewController
    }
}
