enum AddCardOnboardingAction {
    case addCreditCard
    case skip
}

protocol OnboardingAddCardDelegate: AnyObject {
    func didFinishOnboardingAddCard()
}

protocol AddCardOnboardingCoordinating {
    var viewController: UIViewController? { get set }
    var delegate: OnboardingAddCardDelegate? { get set }
    func perform(action: AddCardOnboardingAction)
}

final class AddCardOnboardingCoordinator: AddCardOnboardingCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: OnboardingAddCardDelegate?
    
    func perform(action: AddCardOnboardingAction) {
        switch action {
        case .addCreditCard:
            guard let controller = ViewsManager.paymentMethodsStoryboardViewController(withIdentifier: "AddNewCreditCardViewController") as? AddNewCreditCardViewController else {
                return
            }
            controller.viewModel = AddNewCreditCardViewModel(origin: .onboarding)
            controller.hidesBottomBarWhenPushed = true
            controller.isCloseButton = false
            controller.isNavigationAllowed = true
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .skip:
            delegate?.didFinishOnboardingAddCard()
        }
    }
}
