protocol AddCardOnboardingServiceProtocol {
    var isCreditCardRegistered: Bool { get }
}

final class AddCardOnboardingService: AddCardOnboardingServiceProtocol {
    private let cardManager: CreditCardManagerContract
    
    var isCreditCardRegistered: Bool {
        return cardManager.defaultCreditCardId != 0
    }
    
    init(cardManager: CreditCardManagerContract) {
        self.cardManager = cardManager
    }
}
