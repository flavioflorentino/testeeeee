import Core
import UI
import FeatureFlag

final class OnboardingFlowCoordinator: Coordinator {
    typealias Dependencies = HasFeatureManager
    
    enum OnboardingStep {
        case social
        case card
        case firstAction
    }
    
    var currentCoordinator: Coordinator?
    private var childCoordinators: [Coordinating] = []
    
    public var viewController: UIViewController?
    private let navigationController: UINavigationController
    private let dependencies: Dependencies
    
    private lazy var onboardingSteps: [OnboardingStep] = {
        var steps = [OnboardingStep]()
        let featureManager = dependencies.featureManager
        
        guard featureManager.isActive(.newOnboarding) else {
            return steps
        }
        if featureManager.isActive(.onboardingContacts) {
            steps.append(.social)
        }
        if featureManager.isActive(.onboardingCreditCard) {
            steps.append(.card)
        }
        if featureManager.isActive(.onboardingFirstAction) {
            steps.append(.firstAction)
        }
        return steps
    }()
    
    public init(navigationController: UINavigationController, steps: [OnboardingStep]? = nil, dependencies: Dependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        if let steps = steps {
            self.onboardingSteps = steps
        }
    }
    
    public func start() {
        SessionManager.sharedInstance.currentCoordinator = self
        presentNextStep()
    }
    
    private func presentNextStep() {
        guard !onboardingSteps.isEmpty else {
            exitOnboarding()
            return
        }
        
        let nextStep = onboardingSteps.removeFirst()
        let controller = instatiateViewController(for: nextStep)
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func instatiateViewController(for step: OnboardingStep) -> UIViewController {
        switch step {
        case .social:
            return FindFriendsFactory.make(with: self)
        case .card:
            return AddCardOnboardingFactory.make(with: self)
        case .firstAction:
            return FirstActionOnboardingFactory.make(with: self)
        }
    }
}

extension OnboardingFlowCoordinator: OnboardingSocialDelegate {
    func didFinishOnboardingSocial() {
        presentNextStep()
    }
}

extension OnboardingFlowCoordinator: OnboardingAddCardDelegate {
    func didFinishOnboardingAddCard() {
        presentNextStep()
    }
}

extension OnboardingFlowCoordinator: OnboardingCashInDelegate {
    func didFinishOnboardingCashIn() {
        presentNextStep()
    }
}

extension OnboardingFlowCoordinator: OnboardingFirstActionDelegate {
    func exitOnboarding() {
        AppManager.shared.goToApp(fromContext: viewController)
        SessionManager.sharedInstance.currentCoordinator = nil
    }
}
