import AnalyticsModule

enum OnboardingEvent: AnalyticsKeyProtocol {
    case addressBookAuthorized
    case social(_ action: SocialOnboardingAction)
    case friends(_ action: SocialFindFriendsEvent)
    case cashIn(_ action: CashInOnboardingAction)
    case creditCard(_ action: CreditCardOnboardingAction)
    case firstAction(_ name: String)
    
    private var providers: [AnalyticsProvider] {
        return [.mixPanel, .firebase]
    }
        
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .addressBookAuthorized:
            return AnalyticsEvent("Permissao autorizada - Contatos", providers: providers)
        case .social(let action):
            return AnalyticsEvent("Onboarding social - \(action.description)", properties: action.properties, providers: providers)
        case .cashIn(let action):
            return AnalyticsEvent("Onboarding Cash-In - \(action.description)", properties: action.properties, providers: providers)
        case .creditCard(let action):
            let actionDescription = action.description.isEmpty ? "" : " - \(action.description)"
            return AnalyticsEvent("Onboarding cartão de crédito\(actionDescription)", properties: action.properties, providers: providers)
        case .firstAction(let name):
            let properties: [String: Any] = ["Primeira ação escolhida": name, "ID Consumidor": ConsumerManager.shared.consumer?.wsId ?? ""]
            return AnalyticsEvent("Onboarding de primeira ação", properties: properties, providers: providers)
        case .friends(let action):
            return AnalyticsEvent(action.description, properties: action.properties, providers: providers)
        }
    }
}

extension OnboardingEvent {
    enum SocialFindFriendsEvent {
        case userNavigated(_ toContacts: Bool)
        case recommendationAccepted(_ inContacts: Bool, _ proposed: Int, _ choosen: Int)
        case recommendationDropped(_ inContacts: Bool)
        
        var description: String {
            switch self {
            case .userNavigated:
                return "User Navigated"
            case .recommendationAccepted:
                return "Recommendation Accepted"
            case .recommendationDropped:
                return "Recommendation Dropped"
            }
        }
        
        var properties: [String: Any] {
            switch self {
            case let .userNavigated(toContacts):
                return [
                    "from": "permission_contact",
                    "to": toContacts ? "onboarding_friends_contacts" : "onboarding_friends_recommendation"
                ]
            case let .recommendationAccepted(inContacts, proposed, choosen):
                return [
                    "in": inContacts ? "onboarding_friends_contacts" : "onboarding_friends_recommendation" ,
                    "proposed_list": proposed,
                    "accepted_list": choosen
                ]
            case let .recommendationDropped(inContacts):
                return ["in": inContacts ? "onboarding_friends_contacts" : "onboarding_friends_recommendation"]
            }
        }
    }
}

extension OnboardingEvent {
    enum SocialOnboardingAction {
        case skipped
        case notNow
        case didFollow(followed: Int, found: Int)
        case didNotFollow
        case doNotHaveFriends
        case inviteFriends
        case skippedInviteFriends
        
        var description: String {
            switch self {
            case .skipped, .notNow:
                return "Exibiu invite de seguir"
            case .didFollow:
                return "Seguiu"
            case .didNotFollow:
                return "Cancelou"
            case .doNotHaveFriends:
                return "não tem amigos na agenda"
            case .inviteFriends:
                return "convidar amigos"
            case .skippedInviteFriends:
                return "pulou convidar amigos"
            }
        }
        
        var properties: [String: Any] {
            switch self {
            case .skipped:
                return ["Opção escolhida": "Pulou", "Procurou amigos": "Não"]
            case .notNow:
                return ["Opção escolhida": "Agora não", "Procurou amigos": "Não"]
            case .didFollow(let contactsFollowed, let contactsFound):
                return ["Pessoas seguidas": contactsFollowed, "Total de contatos encontrados": contactsFound]
            default:
                return [:]
            }
        }
    }
}

extension OnboardingEvent {
    enum CashInOnboardingAction {
        case carouselViewed(page: String, interactionType: CarouselInteractionType)
        case paymentMethodViewed(origin: PaymentMethodButton, page: String)
        case paymentMethodSelected(option: CashInOption.CashInType)
        case error
        
        var description: String {
            switch self {
            case .carouselViewed:
                return "Carousel Viewed"
            case .paymentMethodViewed:
                return "Payment Method Viewed"
            case .paymentMethodSelected:
                return "Payment Method Selected"
            case .error:
                return "Error"
            }
        }
        
        var properties: [String: Any] {
            switch self {
            case .carouselViewed(let page, let interactionType):
                return ["page": page, "interaction_type": interactionType.rawValue]
            case .paymentMethodViewed(let origin, let page):
                return ["origin": origin.rawValue, "page": page]
            case .paymentMethodSelected(let option):
                return ["option_selected": cashInTypeDescription(option)]
            case .error:
                return ["screen": "PAYMENT METHOD"]
            }
        }
        
        private func cashInTypeDescription(_ type: CashInOption.CashInType) -> String {
            switch type {
            case .credit:
                return "CARTÃO DE CRÉDITO"
            case .bill:
                return "BOLETO"
            case .bank, .original:
                return "TED/DOC"
            case .picpayAccount:
                return "PICPAY ACCOUNT"
            case .none:
                return "FAZER DEPOIS"
            }
        }
    }
    
    enum CarouselInteractionType: String {
        case swipe = "SWIPE"
        case buttonNext = "BOTÃO PRÓXIMO"
        case none = ""
    }
    
    enum PaymentMethodButton: String {
        case skipButton = "BOTÃO PULAR"
        case startButton = "BOTÃO COMEÇAR"
    }
}

extension OnboardingEvent {
    enum CreditCardOnboardingAction {
        case registerCreditCard
        case doNotHaveACard
        case registerLater
        case skipped
        
        var description: String {
            switch self {
            case .skipped:
                return "Pulou"
            default:
                return ""
            }
        }
        
        var properties: [String: Any] {
            switch self {
            case .registerCreditCard:
                return ["Opção escolhida": "Cadastro de cartão de crédito"]
            case .doNotHaveACard:
                return ["Opção escolhida": "Não tenho cartão"]
            case .registerLater:
                return ["Opção escolhida": "Cadastrar depois"]
            default:
                return [:]
            }
        }
    }
}
