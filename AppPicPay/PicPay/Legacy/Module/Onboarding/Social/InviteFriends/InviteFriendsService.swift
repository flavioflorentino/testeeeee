import Core

protocol InviteFriendsServiceProtocol {
    var mgmConfigsScreenTitle: String? { get }
    var mgmConfigsScreenMessage: String? { get }
    var mgmEnabled: Bool { get }
}

final class InviteFriendsService: InviteFriendsServiceProtocol {
    private let mgmConfigs: PPMgmConfigs?
    
    var mgmConfigsScreenTitle: String? {
        return mgmConfigs?.mgmScreenSubtitle2
    }
    
    var mgmConfigsScreenMessage: String? {
        return mgmConfigs?.text
    }
    
    var mgmEnabled: Bool {
        return mgmConfigs?.mgmEnabled ?? false
    }
    
    init(mgmConfigs: PPMgmConfigs?) {
        self.mgmConfigs = mgmConfigs
    }
}
