import CoreLegacy

final class InviteFriendsFactory {
    static func make(with delegate: OnboardingSocialDelegate?) -> InviteFriendsViewController {
        let service: InviteFriendsServiceProtocol = InviteFriendsService(mgmConfigs: AppParameters.global().mgmConfigs)
        let viewModel: InviteFriendsViewModelType = InviteFriendsViewModel(service: service)
        var coordinator: InviteFriendsCoordinating = InviteFriendsCoordinator()
        let viewController = InviteFriendsViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        coordinator.delegate = delegate
        viewModel.outputs = viewController
        
        return viewController
    }
}
