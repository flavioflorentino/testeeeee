import AnalyticsModule

protocol InviteFriendsViewModelInputs {
    func fetchAndUpdateScreenText()
    func didTapInviteFriendsButton()
    func didTapCancelButton()
    func skipIfPresentedSharedCodeScreen()
}

protocol InviteFriendsViewModelOutputs: AnyObject {
    func updateScreen(image: UIImage, title: String, message: String)
    func perform(action: InviteFriendsAction)
}

protocol InviteFriendsViewModelType: AnyObject {
    var inputs: InviteFriendsViewModelInputs { get }
    var outputs: InviteFriendsViewModelOutputs? { get set }
}

final class InviteFriendsViewModel: InviteFriendsViewModelType, InviteFriendsViewModelInputs {
    var inputs: InviteFriendsViewModelInputs { return self }
    weak var outputs: InviteFriendsViewModelOutputs?

    private let service: InviteFriendsServiceProtocol

    private var didPresentShareCode = false
    
    init(service: InviteFriendsServiceProtocol) {
        self.service = service
    }
    
    func fetchAndUpdateScreenText() {
        guard service.mgmEnabled, let title = service.mgmConfigsScreenTitle, let message = service.mgmConfigsScreenMessage else {
            let title = OnboardingLocalizable.inviteFriendsDefaultTitle.text
            let message = OnboardingLocalizable.inviteFriendsDefaultMessage.text
            outputs?.updateScreen(image: #imageLiteral(resourceName: "empty_contact_list_popup"), title: title, message: message)
            return
        }
        outputs?.updateScreen(image: #imageLiteral(resourceName: "ico_mgm_invite"), title: title, message: message)
    }
    
    func didTapInviteFriendsButton() {
        Analytics.shared.log(OnboardingEvent.social(.inviteFriends))
        outputs?.perform(action: .shareCode)
        didPresentShareCode = true
    }
    
    func didTapCancelButton() {
        Analytics.shared.log(OnboardingEvent.social(.skippedInviteFriends))
        outputs?.perform(action: .skip)
    }
    
    func skipIfPresentedSharedCodeScreen() {
        guard didPresentShareCode else {
            return
        }
        outputs?.perform(action: .skip)
    }
}
