import AnalyticsModule
import UI

final class InviteFriendsViewController: LegacyViewController<InviteFriendsViewModelType, InviteFriendsCoordinating, BaseOnboardingView> {
    private lazy var inviteFriendsButton: UIPPButton = {
        let title = OnboardingLocalizable.inviteFriends.text
        let button = UIPPButton(title: title, target: self, action: #selector(tapInviteFriendsButton), type: .cta)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var notNowButton: UIPPButton = {
        let title = OnboardingLocalizable.notNow.text
        let button = UIPPButton(title: title, target: self, action: #selector(tapCancelButton), type: .cleanUnderlined)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.inputs.fetchAndUpdateScreenText()
        Analytics.shared.log(OnboardingEvent.social(.doNotHaveFriends))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.inputs.skipIfPresentedSharedCodeScreen()
    }
    
    override func configureViews() {
        rootView.addNewButtonView(inviteFriendsButton)
        rootView.addNewButtonView(notNowButton)
        rootView.delegate = self
    }
    
    @objc
    private func tapInviteFriendsButton() {
        viewModel.inputs.didTapInviteFriendsButton()
    }
    
    @objc
    private func tapCancelButton() {
        viewModel.inputs.didTapCancelButton()
    }
}

extension InviteFriendsViewController: BaseOnboardingViewDelegate {
    func didTapSkipButton() {
        coordinator.perform(action: .skip)
    }
}

// MARK: View Model Outputs
extension InviteFriendsViewController: InviteFriendsViewModelOutputs {
    func updateScreen(image: UIImage, title: String, message: String) {
        rootView.image = image
        rootView.title = title
        rootView.message = message
    }
    
    func perform(action: InviteFriendsAction) {
        coordinator.perform(action: action)
    }
}
