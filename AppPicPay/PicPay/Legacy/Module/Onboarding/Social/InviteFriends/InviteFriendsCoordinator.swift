enum InviteFriendsAction {
    case shareCode
    case skip
}

protocol InviteFriendsCoordinating {
    var viewController: UIViewController? { get set }
    var delegate: OnboardingSocialDelegate? { get set }
    func perform(action: InviteFriendsAction)
}

final class InviteFriendsCoordinator: InviteFriendsCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: OnboardingSocialDelegate?
    
    func perform(action: InviteFriendsAction) {
        switch action {
        case .shareCode:
            guard let controller = ViewsManager.peopleSearchStoryboardViewController("SocialShareCode") as? SocialShareCodeViewController else {
                return
            }
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .skip:
            delegate?.didFinishOnboardingSocial()
        }
    }
}
