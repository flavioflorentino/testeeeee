final class FollowFriendsFactory {
    static func make(
        from originFlow: SocialOnboardingViewController.OriginFlow,
        with delegate: OnboardingSocialDelegate? = nil,
        didSkip: Bool = false
    ) -> SocialOnboardingViewController? {
        var coordinator: FollowFriendsCoordinating = FollowFriendsCoordinator()
        let container = DependencyContainer()
        guard let viewController = ViewsManager.socialStoryboardViewController(withIdentifier: "SocialOnboarding")  as? SocialOnboardingViewController else {
            return nil
        }
        
        let service: FindFriendsServiceProtocol = FindFriendsService(dependencies: container)
        viewController.setup(
            viewModel: SocialOnboardingViewModel(service: service, didSkip: didSkip, dependencies: container),
            originFlow: originFlow,
            coordinator: coordinator
        )
        
        coordinator.viewController = viewController
        coordinator.delegate = delegate

        return viewController
    }
}
