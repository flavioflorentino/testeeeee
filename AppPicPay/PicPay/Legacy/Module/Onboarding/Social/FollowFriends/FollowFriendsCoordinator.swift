enum FollowFriendsAction {
    case inviteFriends
    case skip
}

protocol FollowFriendsCoordinating {
    var viewController: UIViewController? { get set }
    var delegate: OnboardingSocialDelegate? { get set }
    func perform(action: FollowFriendsAction)
}

final class FollowFriendsCoordinator: FollowFriendsCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: OnboardingSocialDelegate?
    
    func perform(action: FollowFriendsAction) {
        switch action {
        case .inviteFriends:
            let controller = InviteFriendsFactory.make(with: delegate)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .skip:
            delegate?.didFinishOnboardingSocial()
        }
    }
}
