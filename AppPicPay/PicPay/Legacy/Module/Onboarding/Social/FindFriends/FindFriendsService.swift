import Core
import PermissionsKit

protocol FindFriendsServiceProtocol {
    var isAdressBookAuthorized: Bool { get }
    func requestAddressBookPermission(_ completion: @escaping Permission.Callback)
    func getFriendsRecommendations(_ phoneNumber: String, completion: @escaping (Result<[RecommendedFriendItem], Error>) -> Void)
    func getFriendsSuggestions(completion: @escaping (_ list:[PPContact]?, _ error:Error?) -> Void )
}

final class FindFriendsService: FindFriendsServiceProtocol {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    var isAdressBookAuthorized: Bool {
        return PPPermission.contacts().status == .authorized
    }
    
    func requestAddressBookPermission(_ completion: @escaping Permission.Callback) {
        PPPermission.requestContacts { status in
            completion(status)
        }
    }
    
    func getFriendsRecommendations(_ phoneNumber: String, completion: @escaping (Result<[RecommendedFriendItem], Error>) -> Void) {
        Api<[RecommendedFriendItem]>(endpoint: FindFriendsEndpoint.getFriendsRecommendations(phoneNumber)).execute { result in
            switch result {
            case let .success(response):
                self.dependencies.mainQueue.async {
                    completion(.success(response.model))
                }
            case let .failure(error):
                self.dependencies.mainQueue.async {
                    completion(.failure(error))
                }
            }
        }
    }
    
    func getFriendsSuggestions(completion: @escaping ([PPContact]?, Error?) -> Void) {
        WSSocial.followSuggestions { list, error in
            self.dependencies.mainQueue.async {
                completion(list, error)
            }
        }
    }
}
