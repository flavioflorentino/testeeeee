final class FindFriendsFactory {
    static func make(with delegate: OnboardingSocialDelegate) -> FindFriendsViewController {
        let container = DependencyContainer()
        let service: FindFriendsServiceProtocol = FindFriendsService(dependencies: container)
        let viewModel: FindFriendsViewModelType = FindFriendsViewModel(service: service, dependencies: container)
        var coordinator: FindFriendsCoordinating = FindFriendsCoordinator()
        let viewController = FindFriendsViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        coordinator.delegate = delegate
        viewModel.outputs = viewController

        return viewController
    }
}
