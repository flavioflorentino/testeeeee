import AnalyticsModule

protocol FindFriendsViewModelInputs {
    func didTapAuthorizeButton()
    func didTapNotNowButton()
    func didTapSkipButton()
}

protocol FindFriendsViewModelOutputs: AnyObject {
    func perform(action: FindFriendsAction)
}

protocol FindFriendsViewModelType: AnyObject {
    var inputs: FindFriendsViewModelInputs { get }
    var outputs: FindFriendsViewModelOutputs? { get set }
}

final class FindFriendsViewModel: FindFriendsViewModelType, FindFriendsViewModelInputs {
    typealias Dependencies = HasAnalytics
    var inputs: FindFriendsViewModelInputs { return self }
    weak var outputs: FindFriendsViewModelOutputs?
    
    private let service: FindFriendsServiceProtocol
    private let dependencies: Dependencies
    
    init(service: FindFriendsServiceProtocol, dependencies: Dependencies) {
        self.service = service
        self.dependencies = dependencies
    }
    
    func didTapAuthorizeButton() {
        dependencies.analytics.log(ContactListPermissionEvent.contactsPermission(.authorize, .onboarding))
        guard service.isAdressBookAuthorized else {
            requestPermissionToAccessAddressBook()
            return
        }
        showContactListToFollow()
    }
    
    private func requestPermissionToAccessAddressBook() {
        service.requestAddressBookPermission { [weak self] status in
            guard status == .authorized else {
                return
            }
            self?.showContactListToFollow()
        }
    }
    
    private func showContactListToFollow() {
        dependencies.analytics.log(OnboardingEvent.addressBookAuthorized)
        outputs?.perform(action: .showContactsToFollow)
    }
    
    func didTapNotNowButton() {
        dependencies.analytics.log(OnboardingEvent.social(.notNow))
        dependencies.analytics.log(ContactListPermissionEvent.contactsPermission(.notNow, .onboarding))
        outputs?.perform(action: .showRecommendedContactsToFollow)
    }
    
    func didTapSkipButton() {
        dependencies.analytics.log(OnboardingEvent.social(.skipped))
        outputs?.perform(action: .showRecommendedContactsToFollow)
    }
}
