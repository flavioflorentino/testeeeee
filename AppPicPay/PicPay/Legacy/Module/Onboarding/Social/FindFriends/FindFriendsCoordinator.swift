enum FindFriendsAction {
    case showContactsToFollow
    case showRecommendedContactsToFollow
}

protocol OnboardingSocialDelegate: AnyObject {
    func didFinishOnboardingSocial()
}

protocol FindFriendsCoordinating {
    var viewController: UIViewController? { get set }
    var delegate: OnboardingSocialDelegate? { get set }
    func perform(action: FindFriendsAction)
}

final class FindFriendsCoordinator: FindFriendsCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: OnboardingSocialDelegate?
    
    func perform(action: FindFriendsAction) {
        switch action {
        case .showContactsToFollow:
            guard let controller = FollowFriendsFactory.make(from: .registration, with: delegate) else {
                return
            }
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .showRecommendedContactsToFollow:
            guard let controller = FollowFriendsFactory.make(from: .registration, with: delegate, didSkip: true) else {
                return
            }
            viewController?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
