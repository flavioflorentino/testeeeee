import Core

enum FindFriendsEndpoint {
    case getFriendsRecommendations(_ phoneNumber: String)
}

extension FindFriendsEndpoint: ApiEndpointExposable {
    var path: String {
        if case let .getFriendsRecommendations(phoneNumber) = self {
            return "consumers/recommendation/\(phoneNumber)"
        }
        return ""
    }
    
    var isTokenNeeded: Bool {
        true
    }
}
