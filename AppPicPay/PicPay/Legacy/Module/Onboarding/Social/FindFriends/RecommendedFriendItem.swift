import Core

struct RecommendedFriendItem: Decodable {
    let id: String
    let name: String
    let username: String
    let imageUrlSmall: String?
    let imageUrlLarge: String?
}
