import UI

final class FindFriendsViewController: LegacyViewController<FindFriendsViewModelType, FindFriendsCoordinating, BaseOnboardingView> {
    private lazy var authorizeButton: UIPPButton = {
        let title = OnboardingLocalizable.authorizeAddressBook.text
        let button = UIPPButton(title: title, target: self, action: #selector(tapAuthorizeButton), type: .cta)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var notNowButton: UIPPButton = {
        let title = OnboardingLocalizable.notNow.text
        let button = UIPPButton(title: title, target: self, action: #selector(tapNotNowButton), type: .cleanUnderlined)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
 
    override func configureViews() {
        rootView.image = #imageLiteral(resourceName: "onboarding_social_popup")
        rootView.title = OnboardingLocalizable.onboardingSocialTitle.text
        rootView.message = OnboardingLocalizable.onboardingSocialMessage.text
        rootView.addNewButtonView(authorizeButton)
        rootView.addNewButtonView(notNowButton)
        rootView.delegate = self
    }
    
    @objc
    private func tapAuthorizeButton() {
        viewModel.inputs.didTapAuthorizeButton()
    }
    
    @objc
    private func tapNotNowButton() {
        viewModel.inputs.didTapNotNowButton()
    }
}

extension FindFriendsViewController: BaseOnboardingViewDelegate {
    func didTapSkipButton() {
        viewModel.inputs.didTapSkipButton()
    }
}

// MARK: View Model Outputs
extension FindFriendsViewController: FindFriendsViewModelOutputs {
    func perform(action: FindFriendsAction) {
        coordinator.perform(action: action)
    }
}
