protocol CashInCarouselPresenting: AnyObject {
    var viewController: CashInCarouselDisplay? { get set }
    func setupCarousel(with data: [CashInCarouselPageModel])
    func displayButtons(for pageModel: CashInCarouselPageModel)
    func displayNextPage()
    func displayLastPage()
    func perform(action: CashInCarouselAction)
}

final class CashInCarouselPresenter: CashInCarouselPresenting {
    private let coordinator: CashInCarouselCoordinating
    weak var viewController: CashInCarouselDisplay?

    init(coordinator: CashInCarouselCoordinating) {
        self.coordinator = coordinator
    }
    
    func setupCarousel(with data: [CashInCarouselPageModel]) {
        viewController?.setupCarousel(with: data)
    }
    
    func displayButtons(for pageModel: CashInCarouselPageModel) {
        viewController?.displayButtons(for: pageModel)
    }
    
    func displayNextPage() {
        viewController?.displayNextPage()
    }
    
    func displayLastPage() {
        viewController?.displayLastPage()
    }
    
    func perform(action: CashInCarouselAction) {
        coordinator.perform(action: action)
    }
}
