protocol CashInCarouselDisplay: AnyObject {
    func setupCarousel(with data: [CashInCarouselPageModel])
    func displayButtons(for pageModel: CashInCarouselPageModel)
    func displayNextPage()
    func displayLastPage()
}
