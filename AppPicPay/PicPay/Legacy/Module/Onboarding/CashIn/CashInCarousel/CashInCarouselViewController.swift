import UI
import UIKit

final class CashInCarouselViewController: ViewController<CashInCarouselViewModelInputs, UIView> {
    private enum Layout {
        static let skipButtonMargin: CGFloat = 16
        static let nextButtonMargin: CGFloat = 22
        static let skipButtonFont: UIFont = .systemFont(ofSize: 18, weight: .regular)
        static let nextButtonFont: UIFont = .systemFont(ofSize: 16, weight: .semibold)
    }
    
    private lazy var skipButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(tapSkipButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var carouselPageView: CarouselPageView<CashInCarouselPageModel, CashInCarouselPageView> = {
        let caroselView = CarouselPageView<CashInCarouselPageModel, CashInCarouselPageView>()
        caroselView.carouselPageDelegate = self
        return caroselView
    }()
    
    private lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = Palette.ppColorBranding300.color
        pageControl.pageIndicatorTintColor = Palette.ppColorGrayscale200.color
        pageControl.backgroundColor = .clear
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.isUserInteractionEnabled = false
        return pageControl
    }()
    
    private lazy var nextButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(tapNextButton), for: .touchUpInside)
        return button
    }()

    public override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    public override func buildViewHierarchy() {
        view.addSubview(skipButton)
        view.addSubview(carouselPageView)
        view.addSubview(pageControl)
        view.addSubview(nextButton)
    }
    
    public override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
    }
    
    public override func setupConstraints() {
        var topConstant: CGFloat = 20 // status bar height
        if #available(iOS 11, *) {
            topConstant = 0
        }
        
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [carouselPageView])
        
        NSLayoutConstraint.activate([
            skipButton.topAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.topAnchor, constant: Layout.skipButtonMargin + topConstant),
            skipButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Layout.skipButtonMargin),
            
            carouselPageView.topAnchor.constraint(equalTo: skipButton.bottomAnchor),
            carouselPageView.bottomAnchor.constraint(equalTo: pageControl.topAnchor),
            
            pageControl.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            pageControl.bottomAnchor.constraint(equalTo: nextButton.topAnchor),
            
            nextButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            nextButton.bottomAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.bottomAnchor, constant: -Layout.nextButtonMargin)
        ])
    }
    
    private func setButtonTitle(title: String?, attributes: [NSAttributedString.Key: Any], button: UIButton) {
        guard let title = title else {
            button.alpha = 0.0
            return
        }
        
        let attributedString = NSAttributedString(
            string: title,
            attributes: attributes
        )
        button.setAttributedTitle(attributedString, for: .normal)
        button.alpha = 1.0
    }
    
    @objc
    private func tapSkipButton() {
        viewModel.sendEventForSkipButtonTap(pageIndex: pageControl.currentPage)
        viewModel.didTapSkipButton()
    }
    
    @objc
    private func tapNextButton() {
        viewModel.sendEventForNextButonTap(overLastPage: carouselPageView.isOnLastPage)
        viewModel.didTapNextButton(overLastPage: carouselPageView.isOnLastPage)
    }
}

extension CashInCarouselViewController: CarouselPageViewDelegate {
    func didScrollToPage(at pageIndex: Int) {
        pageControl.currentPage = pageIndex
        viewModel.didScrollToPage(at: pageIndex)
    }
}

extension CashInCarouselViewController: CashInCarouselDisplay {
    func setupCarousel(with data: [CashInCarouselPageModel]) {
        pageControl.numberOfPages = data.count
        
        carouselPageView.setup(data: data) {  _, model, cell in
            cell.setup(model: model)
        }
    }
    
    func displayButtons(for pageModel: CashInCarouselPageModel) {
        setButtonTitle(
            title: pageModel.skipButtonTitle,
            attributes: [
                .foregroundColor: Palette.ppColorBranding300.color,
                .font: Layout.skipButtonFont
            ],
            button: skipButton
        )
        
        setButtonTitle(
            title: pageModel.nextButtonTitle,
            attributes: [
                .foregroundColor: Palette.ppColorBranding300.color,
                .font: Layout.nextButtonFont,
                .underlineStyle: NSUnderlineStyle.single.rawValue
            ],
             
            button: nextButton
        )
    }
    
    func displayNextPage() {
        carouselPageView.scrollToNextPage()
    }
    
    func displayLastPage() {
        carouselPageView.scrollToLastPage()
    }
}
