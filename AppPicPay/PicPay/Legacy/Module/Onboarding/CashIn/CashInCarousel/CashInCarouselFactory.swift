final class CashInCarouselFactory {
    static func make(with delegate: OnboardingCashInDelegate? = nil) -> CashInCarouselViewController {
        let coordinator: CashInCarouselCoordinating = CashInCarouselCoordinator()
        let presenter: CashInCarouselPresenting = CashInCarouselPresenter(coordinator: coordinator)
        let viewModel = CashInCarouselViewModel(presenter: presenter)
        let viewController = CashInCarouselViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
