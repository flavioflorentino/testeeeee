struct CashInCarouselPageModel {
    let page: String
    let image: UIImage?
    let title: String
    let message: String
    let skipButtonTitle: String?
    let nextButtonTitle: String?
    
    init(
        page: OnboardingLocalizable,
        image: UIImage?,
        title: OnboardingLocalizable,
        message: OnboardingLocalizable,
        skipButtonTitle: DefaultLocalizable?,
        nextButtonTitle: DefaultLocalizable
    ) {
        self.page = page.text
        self.image = image
        self.title = title.text
        self.message = message.text
        self.skipButtonTitle = skipButtonTitle?.text
        self.nextButtonTitle = nextButtonTitle.text
    }
}
