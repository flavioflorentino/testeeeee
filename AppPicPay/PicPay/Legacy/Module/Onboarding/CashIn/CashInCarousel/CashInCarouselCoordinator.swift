enum CashInCarouselAction {
    case cashInOptions
}

protocol CashInCarouselCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: OnboardingCashInDelegate? { get set }
    func perform(action: CashInCarouselAction)
}

final class CashInCarouselCoordinator: CashInCarouselCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: OnboardingCashInDelegate?
    
    func perform(action: CashInCarouselAction) {
        switch action {
        case .cashInOptions:
            let optionsController = CashInOptionsFactory.make(with: delegate)
            viewController?.navigationController?.pushViewController(optionsController, animated: true)
        }
    }
}
