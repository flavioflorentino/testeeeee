import UI

final class CashInCarouselPageView: UICollectionViewCell {
    private enum Layout {
        static let imageHeight: CGFloat = 220
        static let imageToTitleSpacing: CGFloat = 12
        static let titleToMessageSpacing: CGFloat = 20
        static let leadingTrailingSpacing: CGFloat = 20
    }
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var mainView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        buildViewHierarchy()
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(model: CashInCarouselPageModel) {
        imageView.image = model.image
        titleLabel.text = model.title
        setupMessageLabel(with: model.message)
    }
    
    func setupMessageLabel(with message: String) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineSpacing = 4
        
        let atributtedString = NSAttributedString(
            string: message,
            attributes: [
                .font: UIFont.systemFont(ofSize: 14, weight: .light),
                .foregroundColor: Palette.ppColorGrayscale500.color,
                .paragraphStyle: paragraphStyle
            ]
        )
        messageLabel.attributedText = atributtedString
    }
    
    private func buildViewHierarchy() {
        mainView.addSubview(imageView)
        mainView.addSubview(titleLabel)
        mainView.addSubview(messageLabel)
        addSubview(mainView)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(
            equalTo: self,
            for: [mainView, imageView, titleLabel, messageLabel],
            constant: Layout.leadingTrailingSpacing
        )
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: mainView.topAnchor),
            imageView.heightAnchor.constraint(equalToConstant: Layout.imageHeight),
            
            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: Layout.imageToTitleSpacing),
            
            messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Layout.titleToMessageSpacing),
            messageLabel.bottomAnchor.constraint(equalTo: mainView.bottomAnchor),
            
            mainView.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
}
