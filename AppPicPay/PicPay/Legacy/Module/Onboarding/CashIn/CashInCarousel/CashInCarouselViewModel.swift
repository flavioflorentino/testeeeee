import AnalyticsModule

protocol CashInCarouselViewModelInputs: AnyObject {
    func viewDidLoad()
    func didScrollToPage(at pageIndex: Int)
    func didTapNextButton(overLastPage: Bool)
    func didTapSkipButton()
    func sendEventForSkipButtonTap(pageIndex: Int)
    func sendEventForNextButonTap(overLastPage: Bool)
}

final class CashInCarouselViewModel {
    private let presenter: CashInCarouselPresenting
    private var scrollingAfterTapNextButton = false
    
    private let pages: [CashInCarouselPageModel] = [
        CashInCarouselPageModel(
            page: .beginCashInCarouselPage,
            image: #imageLiteral(resourceName: "ilu_cashin_pig"),
            title: .beginCashInCarouselTitle,
            message: .beginCashInCarouselMessage,
            skipButtonTitle: .skip,
            nextButtonTitle: .btNext
        ),
        CashInCarouselPageModel(
            page: .cardCashInCarouselPage,
            image: #imageLiteral(resourceName: "ilu_cashin_card"),
            title: .cardCashInCarouselTitle,
            message: .cardCashInCarouselMessage,
            skipButtonTitle: .skip,
            nextButtonTitle: .btNext
        ),
        CashInCarouselPageModel(
            page: .balanceCashInCarouselPage,
            image: #imageLiteral(resourceName: "ilu_cashin_money"),
            title: .balanceCashInCarouselTitle,
            message: .balanceCashInCarouselMessage,
            skipButtonTitle: nil,
            nextButtonTitle: .begin
        )
    ]

    init(presenter: CashInCarouselPresenting) {
        self.presenter = presenter
    }
}

extension CashInCarouselViewModel: CashInCarouselViewModelInputs {
    func viewDidLoad() {
        presenter.setupCarousel(with: pages)
        didScrollToPage(at: .zero)
        Analytics.shared.log(OnboardingEvent.cashIn(.carouselViewed(page: pages.first?.page ?? "", interactionType: .none)))
    }
    
    func didScrollToPage(at pageIndex: Int) {
        guard pageIndex < pages.count else {
            return
        }
        presenter.displayButtons(for: pages[pageIndex])
        
        let interactionType: OnboardingEvent.CarouselInteractionType = scrollingAfterTapNextButton ? .buttonNext : .swipe
        scrollingAfterTapNextButton = false
        Analytics.shared.log(OnboardingEvent.cashIn(.carouselViewed(page: pages[pageIndex].page, interactionType: interactionType)))
    }
    
    func didTapNextButton(overLastPage: Bool) {
        guard overLastPage else {
            presenter.displayNextPage()
            return
        }
        presenter.perform(action: .cashInOptions)
    }
    
    func didTapSkipButton() {
        presenter.perform(action: .cashInOptions)
    }
    
    func sendEventForSkipButtonTap(pageIndex: Int) {
        guard pageIndex < pages.count else {
            return
        }
        Analytics.shared.log(OnboardingEvent.cashIn(.paymentMethodViewed(origin: .skipButton, page: pages[pageIndex].page)))
    }
    
    func sendEventForNextButonTap(overLastPage: Bool) {
        guard overLastPage else {
            scrollingAfterTapNextButton = true
            return
        }
        Analytics.shared.log(OnboardingEvent.cashIn(.paymentMethodViewed(origin: .startButton, page: pages.last?.page ?? "")))
    }
}
