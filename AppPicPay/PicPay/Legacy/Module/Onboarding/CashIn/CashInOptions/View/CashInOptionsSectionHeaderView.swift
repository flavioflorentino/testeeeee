import UI
import UIKit

final class CashInOptionsSectionHeaderView: UIView, ViewConfiguration {
    private enum Layout {
        static let leadingSpacing: CGFloat = 32
        static let trailingSpacing: CGFloat = 16
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        return label
    }()
    
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(titleLabel)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Layout.leadingSpacing),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Layout.trailingSpacing),
            titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
}
