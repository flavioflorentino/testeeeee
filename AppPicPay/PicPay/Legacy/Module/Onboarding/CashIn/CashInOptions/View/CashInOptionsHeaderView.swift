import UI
import UIKit

final class CashInOptionsHeaderView: UIView, ViewConfiguration {
    private enum Layout {
        static let leadingTrailingMargin: CGFloat = 20
        static let topBottomMargin: CGFloat = 20
        static let titleToMessageSpacing: CGFloat = 8
    }
    
    var height: CGFloat {
        let estimatedTextWidth: CGFloat = UIScreen.main.bounds.width - (2 * Layout.leadingTrailingMargin)
        let estimatedTextSize = CGSize(width: estimatedTextWidth, height: .infinity)
        
        let titleHeight = titleLabel.sizeThatFits(estimatedTextSize).height
        let messageHeight = messageLabel.sizeThatFits(estimatedTextSize).height

        let totalSpaces = 2 * Layout.topBottomMargin + Layout.titleToMessageSpacing
        return titleHeight + messageHeight + totalSpaces
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale500.color
        label.text = OnboardingLocalizable.cashInOptionsHeaderTitle.text
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        label.textColor = Palette.ppColorGrayscale400.color
        label.text = OnboardingLocalizable.cashInOptionsHeaderMessage.text
        label.numberOfLines = 0
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(titleLabel)
        addSubview(messageLabel)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: self, for: [titleLabel, messageLabel], constant: Layout.leadingTrailingMargin)
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: Layout.topBottomMargin),
            messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Layout.titleToMessageSpacing),
            messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Layout.topBottomMargin)
        ])
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
}
