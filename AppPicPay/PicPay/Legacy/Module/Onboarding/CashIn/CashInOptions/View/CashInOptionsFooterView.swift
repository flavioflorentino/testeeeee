import UI
import UIKit

protocol CashInOptionsFooterViewDelegate: AnyObject {
    func didTapFooterButton()
}

final class CashInOptionsFooterView: UIView, ViewConfiguration {
    let height: CGFloat = 56
    
    private lazy var button: UIButton = {
        let button = UIButton()
        let title = NSAttributedString(
            string: OnboardingLocalizable.doItLater.text,
            attributes: [
                .underlineStyle: NSUnderlineStyle.single.rawValue,
                .font: UIFont.systemFont(ofSize: 14, weight: .semibold),
                .foregroundColor: Palette.ppColorGrayscale400.color
            ]
        )
        button.setAttributedTitle(title, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(tapFooterbutton), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: CashInOptionsFooterViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(button)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            button.centerXAnchor.constraint(equalTo: centerXAnchor),
            button.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -4)
        ])
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
    
    @objc
    func tapFooterbutton() {
        delegate?.didTapFooterButton()
    }
}
