import UI
import UIKit

final class CashInOptionsCell: UITableViewCell, ViewConfiguration {
    private enum Layout {
        static let imageSize: CGFloat = 48
        static let leadingTrailingExternalMargin: CGFloat = 16
        static let topBottonExternalMargin: CGFloat = 5
        static let internalMargin: CGFloat = 16
        static let internalSpacing: CGFloat = 8
        static let cornerRadius: CGFloat = 6
    }
    
    private lazy var rootView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale000.color
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale600.color
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.textColor = Palette.ppColorGrayscale400.color
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var titleDescriptionStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = Layout.internalSpacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var circularImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = Layout.imageSize / 2
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .clear
        return imageView
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = Layout.internalSpacing
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildViewHierarchy()
        configureViews()
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildViewHierarchy() {
        titleDescriptionStackView.addArrangedSubview(titleLabel)
        titleDescriptionStackView.addArrangedSubview(descriptionLabel)
        
        rootStackView.addArrangedSubview(titleDescriptionStackView)
        rootStackView.addArrangedSubview(circularImageView)
        
        rootView.addSubview(rootStackView)
        contentView.addSubview(rootView)
    }
    
    func configureViews() {
        backgroundColor = .clear
        selectionStyle = .none
        rootView.layer.cornerRadius = Layout.cornerRadius
        rootView.addShadow(with: Palette.black.color, alpha: 0.1, radius: 1, offset: CGSize(width: 0, height: 2))
    }
     
    func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: contentView, for: [rootView], constant: Layout.leadingTrailingExternalMargin)
        
        NSLayoutConstraint.activate([
            rootView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Layout.topBottonExternalMargin),
            rootView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Layout.topBottonExternalMargin),
            
            rootStackView.topAnchor.constraint(equalTo: rootView.topAnchor, constant: Layout.internalMargin),
            rootStackView.leadingAnchor.constraint(equalTo: rootView.leadingAnchor, constant: Layout.internalMargin),
            rootStackView.trailingAnchor.constraint(equalTo: rootView.trailingAnchor, constant: -Layout.internalMargin),
            rootStackView.bottomAnchor.constraint(equalTo: rootView.bottomAnchor, constant: -Layout.internalMargin),
            
            circularImageView.widthAnchor.constraint(equalToConstant: Layout.imageSize),
            circularImageView.heightAnchor.constraint(equalToConstant: Layout.imageSize)
        ])
    }
    
    func configure(with option: CashInOption) {
        titleLabel.text = option.name
        descriptionLabel.text = option.description
        circularImageView.image = option.image
        circularImageView.setImage(url: URL(string: option.imageUrl ?? ""))
    }
}
