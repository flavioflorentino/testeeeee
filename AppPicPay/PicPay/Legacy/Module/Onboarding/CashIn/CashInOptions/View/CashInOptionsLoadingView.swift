import UI
import UIKit

protocol CashInOptionsLoadingViewDelegate: AnyObject {
    func didTapBottomButton()
}

final class CashInOptionsLoadingView: UIView, ViewConfiguration {
    private enum Layout {
        static let margin: CGFloat = 36
    }
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView(style: .whiteLarge)
        activity.color = Palette.ppColorGrayscale400.color
        activity.translatesAutoresizingMaskIntoConstraints = false
        activity.startAnimating()
        return activity
    }()
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = true
        label.textColor = Palette.ppColorGrayscale400.color
        label.text = DefaultLocalizable.webviewLoading.text
        label.font = UIFont.systemFont(ofSize: 16)
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = 8.0
        return stackView
    }()
    
    private lazy var bottomButton: UIButton = {
        let button = UIButton()
        let attributedTitle = NSAttributedString(
            string: OnboardingLocalizable.doItLater.text,
            attributes: [
                .underlineStyle: NSUnderlineStyle.single.rawValue,
                .font: UIFont.systemFont(ofSize: 14, weight: .semibold),
                .foregroundColor: Palette.ppColorGrayscale400.color
            ]
        )
        button.setAttributedTitle(attributedTitle, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(didTapBottomButton), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: CashInOptionsLoadingViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        stackView.addArrangedSubview(activityIndicator)
        stackView.addArrangedSubview(textLabel)
        addSubview(stackView)
        addSubview(bottomButton)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            bottomButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            bottomButton.bottomAnchor.constraint(equalTo: compatibleSafeAreaLayoutGuide.bottomAnchor, constant: -Layout.margin)
        ])
    }
    
    func configureViews() {
        backgroundColor = Palette.ppColorGrayscale100.color
    }
    
    @objc
    private func didTapBottomButton() {
        delegate?.didTapBottomButton()
    }
}
