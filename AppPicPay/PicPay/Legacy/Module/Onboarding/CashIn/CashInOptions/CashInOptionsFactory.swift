final class CashInOptionsFactory {
    static func make(with delegate: OnboardingCashInDelegate? = nil) -> CashInOptionsViewController {
        let service: CashInOptionsServicing = CashInOptionsService()
        let coordinator: CashInOptionsCoordinating = CashInOptionsCoordinator()
        let presenter: CashInOptionsPresenting = CashInOptionsPresenter(coordinator: coordinator)
        let viewModel = CashInOptionsViewModel(service: service, presenter: presenter)
        let viewController = CashInOptionsViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
