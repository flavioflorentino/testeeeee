enum CashInOptionsAction {
    case skip
    case credit
    case bill(RechargeMethod)
    case bank(RechargeMethod)
    case original(RechargeMethod)
}

protocol OnboardingCashInDelegate: AnyObject {
    func didFinishOnboardingCashIn()
}

protocol CashInOptionsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: OnboardingCashInDelegate? { get set }
    func perform(action: CashInOptionsAction)
}

final class CashInOptionsCoordinator: CashInOptionsCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: OnboardingCashInDelegate?
    private var additionalMethod: RechargeMethod?
    
    func perform(action: CashInOptionsAction) {
        switch action {
        case .skip:
            delegate?.didFinishOnboardingCashIn()
            viewController?.navigationController?.dismiss(animated: true)
        case .credit:
            openAddCreditCardForm()
        case .bill(let method):
            openBillOrBankRecharge(with: method)
        case .bank(let method):
            openBillOrBankRecharge(with: method)
        case .original(let method):
            let controller = RechargeOriginalFactory.make()
            controller.delegate = self
            additionalMethod = method
            viewController?.navigationController?.present(PPNavigationController(rootViewController: controller), animated: true)
        }
    }
    
    private func openAddCreditCardForm() {
        guard let addCreditCardVC = ViewsManager.paymentMethodsStoryboardViewController(withIdentifier: "AddNewCreditCardViewController") as? AddNewCreditCardViewController else {
            return
        }
        let viewModel = AddNewCreditCardViewModel(origin: .payment)
        addCreditCardVC.viewModel = viewModel
        addCreditCardVC.delegate = self
        addCreditCardVC.isCloseButton = true
        let navController = UINavigationController(rootViewController: addCreditCardVC)
        navController.setNavigationBarHidden(true, animated: false)
        viewController?.navigationController?.present(navController, animated: true)
    }
    
    private func openBillOrBankRecharge(with method: RechargeMethod) {
        let rechargeViewController = RechargeValueFactory.make(model: method)
        let navController = DismissibleNavigationViewController(rootViewController: rechargeViewController)
        navController.dismissalDelegate = self
        viewController?.navigationController?.present(navController, animated: true)
    }
}

extension CashInOptionsCoordinator: DismissibleNavigationViewControllerDelegate {
    func didDismiss() {
        delegate?.didFinishOnboardingCashIn()
    }
}

extension CashInOptionsCoordinator: AddNewCreditCardDelegate {
    func creditCardDidInsert() {
        delegate?.didFinishOnboardingCashIn()
    }
}

extension CashInOptionsCoordinator: RechargeOriginalViewControllerDelegate {
    func originalOnConnect(controller: RechargeOriginalViewController) {
        controller.dismiss(animated: true) { [weak self] in
            guard let method = self?.additionalMethod else {
                return
            }
            self?.presentWebViewOriginal(method: method)
        }
    }
    
    private func presentWebViewOriginal(method: RechargeMethod) {
        guard
            let viewController = viewController,
            let urlString = method.authUrl,
            let url = URL(string: urlString)
            else {
                return
        }
        viewController.navigationController?.setNavigationBarHidden(false, animated: false)
        
        let webviewController = WebViewFactory.make(
            with: url,
            includeHeaders: false,
            customHeaders: method.authHeaders,
            completion: { [weak self] error in
                if let error = error {
                    AlertMessage.showAlert(withMessage: error.localizedDescription, controller: viewController)
                } else {
                    method.linked = true
                    self?.perform(action: .bank(method))
                }
            })
        webviewController.title = DefaultLocalizable.bankOriginal.text
        viewController.navigationController?.pushViewController(webviewController, animated: true)
        BreadcrumbManager.shared.addCrumb("WebView", typeOf: .webView, withProperties: ["url": url.absoluteString])
    }
}
