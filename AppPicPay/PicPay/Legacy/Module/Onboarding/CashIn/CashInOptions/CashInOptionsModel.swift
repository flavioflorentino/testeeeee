import UI

typealias CashInSections = [Section<CashInSectionHeader, CashInOption>]

struct CashInSectionHeader {
    let title: String
}

struct CashInOption {
    enum CashInType {
        case credit
        case bill
        case bank
        case original
        case picpayAccount
        case none
        
        init(rechargeType: RechargeMethod.RechargeType) {
            switch rechargeType {
            case .bill:
                self = .bill
            case .bank:
                self = .bank
            case .original:
                self = .original
            case .picpayAccount:
                self = .picpayAccount
            default:
                self = .none
            }
        }
    }
    
    let type: CashInType
    let name: String
    let description: String
    let imageUrl: String?
    let image: UIImage?
    let rechargeMethod: RechargeMethod?
    
    init?(rechargeMethod: RechargeMethod) {
        self.rechargeMethod = rechargeMethod
        type = CashInType(rechargeType: rechargeMethod.typeId)
        
        guard type != .none else {
            return nil
        }
        name = rechargeMethod.name
        description = rechargeMethod.desc
        imageUrl = rechargeMethod.imgUrl
        image = nil
    }
    
    init(type: CashInType, name: String, description: String, imageUrl: String? = nil, image: UIImage? = nil) {
        rechargeMethod = nil
        self.type = type
        self.name = name
        self.description = description
        self.imageUrl = imageUrl
        self.image = image
    }
}
