import AnalyticsModule
import UI

protocol CashInOptionsViewModelInputs: AnyObject {
    func fetchCashInOptions()
    func didTapSkipButton()
    func didSelectOption(type: CashInOption.CashInType, rechargeMethod: RechargeMethod?)
    func didSelectBank(with rechargeMethod: RechargeMethod)
}

final class CashInOptionsViewModel {
    private let service: CashInOptionsServicing
    private let presenter: CashInOptionsPresenting
    
    private let creditCardOption: CashInOption = {
        return CashInOption(
            type: .credit,
            name: OnboardingLocalizable.creditCard.text,
            description: OnboardingLocalizable.instantlyAvailable.text,
            image: #imageLiteral(resourceName: "ilu_credit_card_cash_in")
        )
    }()
    
    init(service: CashInOptionsServicing, presenter: CashInOptionsPresenting) {
        self.service = service
        self.presenter = presenter
    }
    
    private func buildTableStructure(with rechargeMethods: [RechargeMethod]) {
        let rechargeOptions = rechargeMethods.compactMap { CashInOption(rechargeMethod: $0) }
        
        let cashInSections: [Section<CashInSectionHeader, CashInOption>] = [
            Section(
                header: CashInSectionHeader(title: OnboardingLocalizable.addCard.text),
                items: [creditCardOption]
            ),
            Section(
                header: CashInSectionHeader(title: OnboardingLocalizable.addMoney.text),
                items: rechargeOptions
            )
        ]
        presenter.displayCashInSections(cashInSections)
    }
}

extension CashInOptionsViewModel: CashInOptionsViewModelInputs {
    func fetchCashInOptions() {
        presenter.displayLoading()
        service.getRechargeOptions { [weak self] result in
            self?.presenter.hideLoading()
            switch result {
            case .success(let rechargeMethods):
                self?.buildTableStructure(with: rechargeMethods)
            case .failure:
                self?.presenter.showErrorMessage()
                Analytics.shared.log(OnboardingEvent.cashIn(.error))
            }
        }
    }
        
    func didTapSkipButton() {
        presenter.didNextStep(action: .skip)
        Analytics.shared.log(OnboardingEvent.cashIn(.paymentMethodSelected(option: .none)))
    }
    
    func didSelectOption(type: CashInOption.CashInType, rechargeMethod: RechargeMethod?) {
        Analytics.shared.log(OnboardingEvent.cashIn(.paymentMethodSelected(option: type)))

        switch type {
        case .credit:
             presenter.didNextStep(action: .credit)
            
        case .bill:
            guard let rechargeMethod = rechargeMethod else {
                return
            }
            presenter.didNextStep(action: .bill(rechargeMethod))
            
        case .bank:
            guard let bankOptions = rechargeMethod?.options else {
                return
            }
            presenter.displayBankOptions(bankOptions)
            
        case .original:
            guard let rechargeMethod = rechargeMethod else {
                return
            }
            let isOriginalAccountLinked = rechargeMethod.linked ?? false
            presenter.didNextStep(action: isOriginalAccountLinked ? .bank(rechargeMethod) : .original(rechargeMethod))
            
        default:
            return
        }
    }
    
    func didSelectBank(with rechargeMethod: RechargeMethod) {
        presenter.didNextStep(action: .bank(rechargeMethod))
    }
}
