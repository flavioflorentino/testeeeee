protocol CashInOptionsPresenting: AnyObject {
    var viewController: CashInOptionsDisplay? { get set }
    func displayCashInSections(_ sessions: CashInSections)
    func showErrorMessage()
    func displayLoading()
    func hideLoading()
    func displayBankOptions(_ bankOptions: [RechargeMethod])
    func didNextStep(action: CashInOptionsAction)
}

final class CashInOptionsPresenter: CashInOptionsPresenting {
    weak var viewController: CashInOptionsDisplay?
    var coordinator: CashInOptionsCoordinating?
    
    init(coordinator: CashInOptionsCoordinating) {
        self.coordinator = coordinator
    }
    
    func displayCashInSections(_ sessions: CashInSections) {
        viewController?.displayCashInSections(sessions)
    }
    
    func showErrorMessage() {
        viewController?.displayError()
    }
    
    func displayLoading() {
        viewController?.displayLoading()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
    
    func didNextStep(action: CashInOptionsAction) {
        coordinator?.perform(action: action)
    }
    
    func displayBankOptions(_ bankOptions: [RechargeMethod]) {
        viewController?.displayBankOptions(bankOptions)
    }
}
