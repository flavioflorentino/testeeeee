protocol CashInOptionsDisplay: AnyObject {
    func displayCashInSections(_ sections: CashInSections)
    func displayError()
    func displayLoading()
    func hideLoading()
    func displayBankOptions(_ bankOptions: [RechargeMethod])
}
