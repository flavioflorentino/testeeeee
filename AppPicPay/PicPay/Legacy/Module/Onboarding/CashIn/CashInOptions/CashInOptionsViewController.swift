import UI
import UIKit

final class CashInOptionsViewController: ViewController<CashInOptionsViewModelInputs, UIView> {
    private enum Layout {
        static let heightForHeaderInSection: CGFloat = 40
        static let skeletonSectionsDristribution = [1, 3]
    }
    
    private lazy var headerView: UIView = {
        let headerView = CashInOptionsHeaderView()
        headerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: headerView.height)
        headerView.translatesAutoresizingMaskIntoConstraints = false
        return headerView
    }()
    
    private lazy var footerView: UIView = {
        let footerView = CashInOptionsFooterView()
        footerView.delegate = self
        footerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: footerView.height)
        return footerView
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.estimatedRowHeight = 90
        tableView.rowHeight = UITableView.automaticDimension
        tableView.showsVerticalScrollIndicator = false
        tableView.sectionHeaderHeight = Layout.heightForHeaderInSection
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = footerView
        tableView.delegate = tableViewHandler
        tableView.dataSource = tableViewHandler
        tableView.register(CashInOptionsCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        return tableView
    }()
    
    private lazy var loadingView: CashInOptionsLoadingView = {
        let view = CashInOptionsLoadingView()
        view.delegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var errorView: GenericErrorView = {
        let genericErrorView = GenericErrorView()
        genericErrorView.translatesAutoresizingMaskIntoConstraints = false
        genericErrorView.tryAgainTapped = { [weak self] in
            self?.retryLoading()
        }
        genericErrorView.setupBottomButton(title: OnboardingLocalizable.skip.text, isHidden: false) { [weak self] in
            self?.viewModel.didTapSkipButton()
        }
        genericErrorView.isHidden = true
        return genericErrorView
    }()
    
    private let cellReuseIdentifier = String(describing: CashInOptionsCell.self)
    private var tableViewHandler: TableViewHandler<CashInSectionHeader, CashInOption, CashInOptionsCell>?

    public override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchCashInOptions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
 
    override func buildViewHierarchy() {
        view.addSubview(tableView)
        view.addSubview(errorView)
        view.addSubview(loadingView)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.constraintAllEdges(from: tableView, to: view)
        NSLayoutConstraint.constraintAllEdges(from: errorView, to: view)
        NSLayoutConstraint.constraintAllEdges(from: loadingView, to: view)
        headerView.widthAnchor.constraint(equalTo: tableView.widthAnchor).isActive = true
    }
    
    private func retryLoading() {
        errorView.isHidden = true
        tableView.isHidden = false
        viewModel.fetchCashInOptions()
    }
    
    private func setupTableViewHandler(_ sections: CashInSections) {
        let handler = TableViewHandler(
            data: sections,
            cellType: CashInOptionsCell.self,
            configureCell: { _, item, cellView in
                cellView.configure(with: item)
            },
            configureSection: { _, sectionModel in
                let header = CashInOptionsSectionHeaderView()
                header.title = sectionModel.header?.title
                return Header.view(header)
            },
            
            configureDidSelectRow: { [weak self] _, cellModel in
                self?.viewModel.didSelectOption(type: cellModel.type, rechargeMethod: cellModel.rechargeMethod)
            }
        )

        tableViewHandler = handler
        tableView.delegate = handler
        tableView.dataSource = handler
        tableView.reloadData()
    }
}

extension CashInOptionsViewController: CashInOptionsFooterViewDelegate {
    func didTapFooterButton() {
        viewModel.didTapSkipButton()
    }
}

extension CashInOptionsViewController: CashInOptionsLoadingViewDelegate {
    func didTapBottomButton() {
        viewModel.didTapSkipButton()
    }
}

extension CashInOptionsViewController: CashInOptionsDisplay {
    func displayCashInSections(_ sections: CashInSections) {
        DispatchQueue.main.async { [weak self] in
            self?.setupTableViewHandler(sections)
        }
    }
    
    func displayError() {
        DispatchQueue.main.async { [weak self] in
            self?.loadingView.isHidden = true
            self?.tableView.isHidden = true
            self?.errorView.isHidden = false
        }
    }
    
    func displayLoading() {
        DispatchQueue.main.async { [weak self] in
            self?.loadingView.alpha = 1
            self?.loadingView.isHidden = false
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async { [weak self] in
            UIView.animate(withDuration: 0.3, animations: {
                self?.loadingView.alpha = 0
            }, completion: { _ in
                self?.loadingView.isHidden = true
            })
        }
    }
    
    func displayBankOptions(_ bankOptions: [RechargeMethod]) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for bank in bankOptions {
            let bankAction = UIAlertAction(title: bank.name, style: .default) { [weak self] _ in
                self?.viewModel.didSelectBank(with: bank)
            }
            actionSheet.addAction(bankAction)
        }
        let cancelAction = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel)
        actionSheet.addAction(cancelAction)
        present(actionSheet, animated: true)
    }
}
