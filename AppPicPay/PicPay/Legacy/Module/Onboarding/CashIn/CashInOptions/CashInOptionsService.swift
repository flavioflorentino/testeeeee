protocol CashInOptionsServicing {
    func getRechargeOptions(_ completion: @escaping ((Result<[RechargeMethod], PicPayError>) -> Void))
}

final class CashInOptionsService: CashInOptionsServicing {
    func getRechargeOptions(_ completion: @escaping ((Result<[RechargeMethod], PicPayError>) -> Void)) {
        WSConsumer.getLastRecharge { _, rechargeMethod, error in
            if let error = error {
                let picpayError = PicPayError(message: error.localizedDescription)
                completion(.failure(picpayError))
            } else {
                let rechargeOptions: [RechargeMethod] = rechargeMethod as? [RechargeMethod] ?? []
                completion(.success(rechargeOptions))
            }
        }
    }
}
