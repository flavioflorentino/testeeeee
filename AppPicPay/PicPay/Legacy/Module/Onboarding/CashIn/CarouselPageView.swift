protocol CarouselPageViewDelegate: AnyObject {
    func didScrollToPage(at pageIndex: Int)
}

final class CarouselPageView<T, Cell: UICollectionViewCell>: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    private var data = [T]()
    private var configureCellHandler: ((Int, T, Cell) -> Void)?
    private var supplementaryView: ((String, IndexPath) -> UICollectionReusableView)?
    private var lastPage: Int = .zero
    
    private var currentPage: Int {
        let index = round(contentOffset.x / frame.size.width)
        return max(Int(index), .zero)
    }
    
    public var isOnLastPage: Bool {
        return currentPage == data.count - 1
    }
    
    weak var carouselPageDelegate: CarouselPageViewDelegate?
    
    init() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        super.init(frame: .zero, collectionViewLayout: layout)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setup(data: [T], configureCell: @escaping (Int, T, Cell) -> Void) {
        self.data = data
        self.configureCellHandler = configureCell
        configureView()
        reloadData()
    }
    
    private func configureView() {
        showsHorizontalScrollIndicator = false
        isPagingEnabled = true
        dataSource = self
        delegate = self
        backgroundColor = .clear
        translatesAutoresizingMaskIntoConstraints = false
        register(Cell.self, forCellWithReuseIdentifier: String(describing: Cell.self))
    }
    
    public func scrollToNextPage() {
        scrollToPage(at: currentPage + 1)
    }
    
    public func scrollToLastPage() {
        scrollToPage(at: data.count - 1)
    }
    
    private func scrollToPage(at pageIndex: Int) {
        guard !isOnLastPage else {
            return
        }
        let nextPageIndexPath = IndexPath(item: pageIndex, section: .zero)
        scrollToItem(at: nextPageIndexPath, at: .right, animated: true)
    }
    
    // MARK: UICollectionViewDataSource

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: Cell.self), for: indexPath) as? Cell else {
            return UICollectionViewCell()
        }
        let model = data[indexPath.row]
        configureCellHandler?(indexPath.row, model, cell)
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentPageIndex = currentPage
        guard currentPageIndex != lastPage else {
            return
        }
        lastPage = currentPageIndex
        carouselPageDelegate?.didScrollToPage(at: currentPageIndex)
    }

    // MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return frame.size
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return .zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return .zero
    }
}
