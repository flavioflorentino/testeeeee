import Foundation

extension Notification.Name {
    enum CreditPicPay {
        static let reloadHomeCPCredit = Notification.Name("CPHomeReloadCreditHome")
        static let accountLoaded = Notification.Name(rawValue: "CPCreditAccountLoaded")
        static let requestPhysicalCard = Notification.Name(rawValue: "CPRequestPhysicalCard")
    }
}
