import Foundation

struct CreditPicPayEvent {
    struct Registration {
        // MARK: - Credit About
        static let registerButtonAbout = "CTA para contratar Crédito - Oferta Crédito"
        static let pageViewAbout = "Page view - Oferta Crédito"
        
        // MARK: - Pendding informations
        static let pageViewPendingInformations = "Page view - Credito - Infomações pendentes"
        
        // MARK: - Not approved
        static let pageViewNotApproved = "Page view - Credito - Cadastro negado"
        
        // MARK: - Home
        static let invoiceHome = "Credito - Home - Pagar fatura"
        
        // MARK: - Details Invoice
        static let invoicePayment = "Credito - fatura - Pagar fatura"
        
        // MARK: - Invoice Payment
        static let invoicePaymentView = "Page view - Credito - Pagar fatura"
        static let invoicePaymentPicPay = "Credito - fatura pagar c/ PicPay"
        static let invoicePaymentBill = "Credito - fatura pagar c/ Boleto"
        
        // MARK: - Payment
        static let paymentPicPayView = "Credito - Pagar fatura valor"
        static let paymentPicPayTotal = "Credito - fatura pagar - valor total"
        static let paymentPicPayInstallment = "Credito - fatura pagar - parcelar"
        static let paymentPicPayOther = "Credito - fatura pagar - outro valor"
        
        static let paymentCreditInstallment = "Credito - fatura pagar - parcelar pagamento"
        static let paymentCreditMethods = "Credito - fatura pagar - trocou meio de pagamento"
    }
}
