import Foundation
import Core
import FeatureFlag

final class CreditPicPayHelper {
    private let account: CPAccount?
    
    init(with account: CPAccount? = nil) {
        self.account = account
    }
    
    var isEnableCreditPicPay: Bool {
        return FeatureManager.isActive(.creditPicPay)
    }
    
    var hasPresentedAtm24OfferAlert: Bool {
        set(value) {
            KVStore().setBool(value, with: .atm24OfferAlert)
        }
        get {
            return KVStore().boolFor(.atm24OfferAlert)
        }
    }
    
    var isCardSettingsEnable: Bool {
        return account?.settings.cardSettingsEnabled ?? false
    }
    
    var isCardWalletEnable: Bool {
        return account?.settings.cardWalletEnabled ?? false
    }
    
    var isCreditActivatedOrBlocked: Bool {
        guard isEnableCreditPicPay, let account = self.account else {
            return false
        }
        return (account.creditStatus == .active) || (account.creditStatus == .blocked)
    }
    
    var isPhysicalCardEnable: Bool {
        return account?.settings.physicalCardConfigurationEnabled ?? false
    }
    
    var isVirtualCardEnable: Bool {
        guard let virtualCardAction = account?.settings.virtualCardButton.action else { return false }
        return virtualCardAction != .none
    }
    
    var isCardCreditSettingsEnabled: Bool {
        account?.settings.cardCreditSettingsEnabled ?? false
    }
    
    var shouldShowCreditPicPayOfferInPaymentMethodIntro: Bool {
        set (value) {
            KVStore().setBool(!value, with: .showCreditPaymentMethodIntro)
        }
        get {
            return !KVStore().boolFor(.showCreditPaymentMethodIntro)
        }
    }
}
