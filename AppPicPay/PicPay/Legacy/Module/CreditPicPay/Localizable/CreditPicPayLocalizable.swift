import Foundation

enum CreditPicPayLocalizable: String, Localizable {
    case featureName
    case payInvoice
    case invoices
    case invoiceDebitTotal
    case invoiceEarlyPayment
    case invoiceReceivedPayment
    case invoiceUnprocessedTransactionsSection
    case btnInvoiceSendToEmail
    case invoiceDetailNotransactions
    case invoiceSent
    case invoiceSentEmail
    case invoiceDetailMinimumPayment
    case invoiceDetailDueDate
    case invoiceDetailClosingDate
    case btInvoiceDetailEarlyPayment
    case dueDate
    case delayedInvoice
    case currentInvoice
    case closing
    case invoiceClosed
    case valueInvoice
    case cardBillTitle
    case cardBillDescription
    case cardPicPayTitle
    case cardPicPayDescription
    case balanceInWallet
    case valueOfPayment
    case totalValue
    case invoiceInstall
    case otherValue
    case installmentCountValue
    case installmentOptions
    case installmentMoreInformation
    case installmentHeader
    case invoicePayment
    case totalOfInvoice
    case whenYouWantToPay
    case minimumPayment
    case generateBill
    case copyBarCode
    case copiedBarCode
    case sendToEmail
    case sentBill
    case descriptionSentBill
    case billDaysPaymentDescription
    case billGeneratingTitle
    case billGeneratingDescription
    case billGeneratingNotificationDescription
    case billGeneratingNotificationTitle
    case billGeneratingPayWithPicPay
    case billGeneratingWantBeNotified
    case paymentTitle
    case billTotal
    case inconvenienceTax
    case paymentMethodTotal
    case paymentMethodInstallments
    case paymentMethodOther
    case installmentsPaymentTitle
    case installmentsPaymentDescription
    
    case alertTax
    case alertConvenienceTaxTitle
    case alertConvenienceTaxSubtitle
    case alertAcceptButton
    case alertDeclineButton
    
    case requiredDocuments
    case errorDocumentUpload
    case cameraAccessForDocuments
    case changeConfig
    
    case limitSuccessAlertTitle
    case limitSuccesAlertText
    case limitSuccessAlertButton
    case limitHavaliableValue
    case limitTitleView
    
    case configurateCard
    case digitalCard
    case virtualCard
    
    case analysisTitleLabel
    case analysisResumeButton
    
    case resumeTitle
    
    case hiringApprovedDescription
    
    case invoiceInstallmentValue
    case invoiceNewInstallmentValue
    case doYouHaveCreditBalance
    case blockedLimit
    case availableLimit
    case weWereUnableRetrieve
    case generalInstallmentConditions
    case contractingInstallment
    case contractedInstallmentsWillBeLaunched
    case interestRateContracted
    case paymentOccursInDifferentAmount
    case eventLatePayment
    case eventRepentance
    case generalConditions
    case incoming
    case weWereUnableVerifyYourInvoice
    case thereWasAnErrorLoading
    case noInvoice
    case yourInvoicesWillBeListedHere
    case limit
    case day
    case changeDay
    case selectDay
    case dueDateTitle
    case selectTheDay
    case changeDueDate
    case changeInvoiceDueDate
    case newDueDate
    case bestDayBuy
    case somethingWentWrong
    case confirmChange
    case doItLater
    case termsAndConditions
    case dueDateYourInvoice
    case bestDayToBuyResume
    case yourCreditLimit
    case importantProduct
    case iAgreeWithMembershipAgreement
    case adhesionContract
    case multiple
    case debit
    case birthplace
    case informCountryBirth
    case informStateBirth
    case informCityBirth
    case selectStateLoad
    case cardActivated
    case chooseTypeDocument
    case chooseIssuingOrgan
    case additionalData
    case orderSummary
    case correctInformation
    case personalData
    case address
    case identityDocument
    case fixRegistration
    case name
    case motherName
    case dateBirth
    case telephone
    case homeAddress
    case businessAddress
    case issuer
    case dateIssue
    case additionalDataIssue
    case monthlyIncome
    case patrimony
    case dataBelowCorrect
    
    case documentsScreenTitle
    case documentsTitle
    case documentsDescription
    case documentsButtonTitle
    case documentsAvailableTitle
    case documentsAvailableDescription
    case documentsAvailableButtonTitle
    case isReviewTitle
    case isReviewDescription
    case laterDismissTitle
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .creditPicPay
    }
}
