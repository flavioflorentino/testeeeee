import UI
import UIKit

protocol CreditBaseFormProtocol {
    var step: CPStep { get }
}

class CreditBaseFormController: BaseFormController {
    lazy var loadingView: UIView = {
        let view = UIView(frame: self.view.bounds)
        view.backgroundColor = Palette.ppColorGrayscale000.color
        let activity = UIActivityIndicatorView(style: .gray)
        activity.center = view.center
        activity.startAnimating()
        view.addSubview(activity)
        return view
    }()
    
    lazy var progressView: UIProgressView = {
        let progressView = UIProgressView()
        progressView.translatesAutoresizingMaskIntoConstraints = false
        progressView.progressTintColor = Palette.ppColorBranding300.color
        progressView.trackTintColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
        progressView.progress = 0.0
        return progressView
    }()
    
    var isEditingForm: Bool = false
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureProgressBar()
        hidesBottomBarWhenPushed = true
        navigationItem.backBarButtonItem?.title = ""
        
        /// Not add notification observer for keyboard with not seted scroll view
        if self.scrollView != nil {
            NotificationCenter.default.addObserver(self, selector: #selector(CreditBaseFormController.keyboardWillOpen), name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(CreditBaseFormController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if navigationController?.viewControllers.count == 1 {
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(closeForm))
        }
    }
    
    // MARK: - Configurations
    func configureProgressBar() {
        view.addSubview(progressView)
        NSLayoutConstraint.activate(
            [
                progressView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0),
                progressView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
                progressView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
                progressView.heightAnchor.constraint(equalToConstant: 3)
            ]
        )
    }
    
    func setStep(with progress: Float) {
        self.progressView.setProgress(progress, animated: false)
    }
    
    func isFormEdition() -> Bool {
        guard self.isEditingForm else {
    return false
}
        navigationController?.popViewController(animated: true)
        return true
    }
    
    override func startLoading() {
        view.addSubview(loadingView)
    }
    
    override func stopLoading() {
        loadingView.removeFromSuperview()
    }
    
    @objc
    private func closeForm() {
        CPRegistrationCoordinator.shared?.dispose()
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Text field delegate
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /// Remove field validation when writing
        if let textField = textField as? UIPPFloatingTextField, textField.validationRules != nil {
            let validatable = textField.validate()
            if !validatable.isValid {
                textField.errorMessage = nil
            }
        }

        return super.textField(textField, shouldChangeCharactersIn: range, replacementString: string)
    }

    @objc
    override func keyboardWillOpen(_ keyboardNotification: Notification) {
        /// Give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        guard let scrollView = self.scrollView else {
            return
        }
        guard let userInfo = keyboardNotification.userInfo else {
            return
        }
        guard let value = userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue else {
            return
        }
        
        var keyboardFrame: CGRect = value.cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset: UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
    }
    
    @objc(keyboardWillHide:)
    override func keyboardWillHide(_ keyboardNotification: Notification) {
        guard let scrollView = self.scrollView else {
            return
        }
        let contentInset: UIEdgeInsets = .zero
        scrollView.contentInset = contentInset
    }
}
