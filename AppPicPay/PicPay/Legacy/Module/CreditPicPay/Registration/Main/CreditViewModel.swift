import UIKit

final class CreditViewModel: NSObject {
    var creditForm: CreditForm
    var progress: Float = 0.0
    var account: CPAccount?
    let apiCredit = ApiCredit()
    
    @objc
    init(with account: CPAccount? = nil) {
        self.account = account
        self.creditForm = CreditForm()
        self.creditForm.birthplaceCountry = "Brasil"
    }
    
    func loadRegistrationStatus(_ completion: @escaping (Error?) -> Void) {
        let api = CreditPicpayApi()
        api.account(isLoanFlow: creditForm.isLoanFlow, onCacheLoaded: { _ in
        }, completion: { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            switch result {
            case .success(let account):
                strongSelf.account = account
                strongSelf.creditForm.account = account
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        })
    }
    
    func loadRegistrationData(_ completion: @escaping ((Error?) -> Void)) {
        apiCredit.getCreditData(credit: creditForm) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            DispatchQueue.main.async {
                switch result {
                case .success(let registration):
                    let isLoanFlow = strongSelf.creditForm.isLoanFlow
                    strongSelf.creditForm = registration
                    strongSelf.creditForm.isLoanFlow = isLoanFlow
                    strongSelf.creditForm.registrationStep = isLoanFlow && registration.registrationStep == .empty ? .personalData : registration.registrationStep
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }
}
