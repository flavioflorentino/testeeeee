import FeatureFlag
import UI
import UIKit
import Card

final class CreditViewController: CreditBaseFormController {
    private let viewModel: CreditViewModel
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    @objc
    init(with viewModel: CreditViewModel) {
        self.viewModel = viewModel
        self.dependencies = DependencyContainer()
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        startLoading()
        loadRegistrationStatus()
    }
    
    /// Load Registration Status for proceed
    private func loadRegistrationStatus() {
        viewModel.loadRegistrationStatus { [weak self] error in
            guard let strongSelf = self else {
                return
            }
            strongSelf.stopLoading()
            if let error = error {
                AlertMessage.showCustomAlertWithError(error, controller: strongSelf, completion: {
                    strongSelf.navigationController?.popToRootViewController(animated: true)
                })
            } else {
                strongSelf.stepPage()
            }
        }
    }

    /// Open Credit Main screen - The user has been activate credit and completed registration was reviewed
    func showCPHome() {
        guard let navigationController = self.navigationController else {
            return
        }
        let coordinator = CreditPicPayCoordinator(navigationController: navigationController)
        SessionManager.sharedInstance.currentCoordinator = coordinator
        coordinator.start()
    }
    
    /// Open Registraion is under review - The PicPay team is reviewing the data
    private func showRegistrationReview() {
        let controller = CreditAnalysisFactory.make(isLoan: viewModel.creditForm.isLoanFlow)
        controller.hidesBottomBarWhenPushed = true
        replaceControler(to: controller)
    }

    private func showRegistrationErrors() {
        if !viewModel.creditForm.isLoanFlow && FeatureManager.isActive(.isCardIdentityVerificationAvailable) {
            let controller = DocumentscopyFactory.make(creditForm: viewModel.creditForm, isReviewFlow: true)
            replaceControler(to: controller)
            return
        }
        let viewModel = CPPendingViewModel(with: self.viewModel.account)
        let controller = CPPendingViewController(with: viewModel)
        replaceControler(to: controller)
    }

    func stepPage() {
        let creditStatus = viewModel.creditForm.account?.creditStatus
        switch creditStatus {
        case .active?, .blocked?:
            showCPHome()
        case .onRegistration?:
            stepRegistrations() // Registration Status
        case .none:
            break
        }
    }
    
    func prepareForRegistration() {
        viewModel.loadRegistrationData { [weak self] error in
            guard let strongSelf = self else {
                return
            }
            if let error = error {
                AlertMessage.showCustomAlertWithError(error, controller: strongSelf, completion: {
                    strongSelf.navigationController?.popToRootViewController(animated: true)
                })
            } else {
                strongSelf.showRegistrationFlow()
            }
        }
    }
    
    func showRegistrationFlow() {
        guard let navigationController = self.navigationController else {
            return
        }
        let creditForm = viewModel.creditForm
        creditForm.account = viewModel.account
        let coordinator = CPRegistrationCoordinator(with: navigationController, creditForm: creditForm)
        CPRegistrationCoordinator.shared = coordinator
        CPRegistrationCoordinator.shared?.makeStepControllers()
        navigationController.setNavigationBarHidden(false, animated: false)
    }
    
    private func showApproved(with account: CPAccount?) {
        guard let navigationController = self.navigationController, let account = account else {
            return
        }
        let hiringFlow = HiringFlowCoordinator(
            isUpgradeDebitToCredit: account.settings.upgradeDebitToCredit,
            navigationController: navigationController
        )
        SessionManager.sharedInstance.currentCoordinating = hiringFlow
        hiringFlow.start()
    }

    private func showWaitingActivation(with account: CPAccount?) {
        guard let account = account else {
            return
        }
        let controller = HiringWaitingActivationFactory.make(isUpgrade: account.settings.upgradeDebitToCredit)
        controller.hidesBottomBarWhenPushed = true
        replaceControler(to: controller)
    }
    
    private func showDenied() {
        let controller = RejectedCardFactory.make()
        replaceControler(to: controller)
    }
 
    func stepRegistrations() {
        guard let status = viewModel.creditForm.account?.registrationStatus else {
            return
        }
        
        switch status {
        case .empty, .inProgress:
            prepareForRegistration()
        case .approved:
            showApproved(with: viewModel.creditForm.account)
        case .creditWaitingActivation:
            showWaitingActivation(with: viewModel.creditForm.account)
        case .completed:
            showRegistrationReview()
        case .reviewRequested:
            showRegistrationErrors()
        case .underAnalysis, .waitingExternalReview:
            showRegistrationReview()
        case .denied:
            showDenied()
        default:
            navigationController?.dismiss(animated: true)
        }
    }
    
    func replaceControler(to: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        transition.type = .fade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        navigationController?.replace(self, by: to)
    }
}
