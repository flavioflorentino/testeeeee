import Address
import Alamofire
import Core
import FeatureFlag
import SwiftyJSON
import UIKit

typealias BiometricProccess = (Swift.Result<CardBiometricProcess, ApiError>) -> Void

struct CardBiometricProcess: Codable {
    let identityAlreadyValidated: Bool
}

final class ApiCredit {
    typealias Dependencies = HasFeatureManager & HasMainQueue
    private let dependencies: Dependencies
    private var requestManager = RequestManager.shared
    
    final class CreditDocumentTypesResponse: BaseApiListResponse<CreditDocumentType> {}
    final class PatrimonyResponse: BaseApiListResponse<CreditPatrimony> {}
    
    lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func getCreditData(credit: CreditForm?, completion: @escaping ((PicPayResult<CreditForm>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            var headers:[String : String] = [:]
            
            if let credit = credit {
                headers["loan_flow"] = credit.isLoanFlow.description
            }
            
            requestManager
                .apiRequest(
                    endpoint: kWsUrlCreditPicpayRegistration,
                    method: .get,
                    headers: headers
                )
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func setCreditRegistration(
        _ credit: CreditForm,
        isCompleted: Bool = false,
        manualUpdated: Bool = false,
        _ completion: @escaping ((PicPayResult<BaseApiGenericResponse>) -> Void) = { _ in }
    ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            var params: [String: Any] = credit.getParamsToRequest()
            if isCompleted {
                params["completed"] = true
            }
            if var homeAddress = params["home_address"] as? [String: Any] {
                homeAddress["manual_updated"] = manualUpdated
                params["home_address"] = homeAddress
            }
            
            requestManager
                .apiRequest(endpoint: kWsUrlCreditPicpayRegistration,
                            method: .put,
                            parameters: params,
                            headers: ["loan_flow": credit.isLoanFlow.description])
                .responseApiObject { [weak self] result in
                    self?.dependencies.mainQueue.async {
                        completion(result)
                    }
                }
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func creditAddressCountries(_ completion: @escaping ((PicPayResult<BaseApiListResponse<AddressCountry>>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlAddressPicPayCountries, method: .get)
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func creditAddressStates(_ completion: @escaping ((PicPayResult<BaseApiListResponse<AddressStates>>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlAddressPicPayStates, method: .get)
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func creditAddressCities(from state: String, _ completion: @escaping ((PicPayResult<BaseApiListResponse<AddressCity>>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let endpoint = WebServiceInterface.apiEndpoint(kWsUrlAddressPicPayCities.replacingOccurrences(of: "{state}", with: state)) ?? ""
            requestManager
                .apiRequest(endpoint: endpoint, method: .get)
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func addressBy(_ cep: String, then completion: @escaping (Swift.Result<AddressSearch, ApiError>) -> Void) {
        let api = Api<AddressSearch>(endpoint: AddressEndpoint.searchBy(cep: cep))
        api.execute(jsonDecoder: decoder) { result in
            self.dependencies.mainQueue.async {
                completion(result.map { $0.model })
            }
        }
    }
    
    func documentTypeForUser(credit: CreditForm? = nil, _ completion: @escaping (PicPayResult<[CreditDocumentItem]>) -> Void) {
        guard dependencies.featureManager.isActive(.opsTransitionCreditApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWSUrlCreditPicpayDocumentsType, method: .get)
                .responseApiCodable { [weak self] result in
                    self?.dependencies.mainQueue.async {
                        completion(result)
                    }
                }
            return
        }
        
        let api = Api<[CreditDocumentItem]>(endpoint: CreditServiceEndpoint.documentTypeForUser(isLoanFlow: credit?.isLoanFlow ?? false))
        api.execute(jsonDecoder: decoder) { result in
            self.dependencies.mainQueue.async {
                switch result {
                case .success((let model, _)):
                    completion(.success(model))
                case .failure:
                    completion(.failure(.generalError))
                }
            }
        }
    }
    
    func patrimonies(_ completion: @escaping ((PicPayResult<PatrimonyResponse>) -> Void) ) {
        guard dependencies.featureManager.isActive(.opsTransitionCreditApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlCreditPicpayPatrimony, method: .get)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        let api = Api<[CreditPatrimony]>(endpoint: CreditServiceEndpoint.patrimony)
        api.execute(jsonDecoder: decoder) { result in
            self.dependencies.mainQueue.async {
                switch result {
                case .success((let model, _)):
                    completion(.success(PatrimonyResponse(list: model)))
                case .failure:
                    completion(.failure(.generalError))
                }
            }
        }
    }
    
    // MARK: - credit/upload
    func uploadDocument(imageData: Data, type: String, fileKey: String? = nil, uploadProgress: @escaping (Double) -> Void, completion: @escaping (PicPayResult<CreditFileDocument>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            var params: [String: Any] = [:]
            let files: [FileUpload] = [
                FileUpload(key: "file", data: imageData, fileName: "imagem.jpg", mimeType: "image/jpeg")
            ]
            if let fileKey = fileKey {
                params["file_key"] = fileKey
            }
            let endpoint = WebServiceInterface
                .apiEndpoint(kWsUrlCreditPicpayDocumentFile.replacingOccurrences(of: "{type}", with: type)) ?? ""
            requestManager
                .apiUploadCodable(endpoint, method: .post, files: files, parameters: params, uploadProgress: { [weak self] progress in
                    self?.dependencies.mainQueue.async {
                        uploadProgress(progress)
                    }
                }, completion: { [weak self] document in
                    self?.dependencies.mainQueue.async {
                        completion(document)
                    }
                })
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func documentTypes(_ completion: @escaping ((PicPayResult<CreditDocumentTypesResponse>) -> Void) ) {
        guard dependencies.featureManager.isActive(.opsTransitionCreditApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: WebServiceInterface.apiEndpoint(kWsUrlCreditPicpayDocument), method: .get)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        let api = Api<[CreditDocumentType]>(endpoint: CreditServiceEndpoint.documentTypes)
        api.execute(jsonDecoder: decoder) { result in
            self.dependencies.mainQueue.async {
                switch result {
                case .success((let model, _)):
                    completion(.success(CreditDocumentTypesResponse(list: model)))
                case .failure:
                    completion(.failure(.generalError))
                }
            }
        }
    }
    
    func biometricProcess(credit: CreditForm?, completion: @escaping BiometricProccess) {
        let api = Api<CardBiometricProcess>(endpoint: CreditServiceEndpoint.biometricProccess(isLoanFlow: credit?.isLoanFlow ?? false))
        api.execute(jsonDecoder: decoder) { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result
                    .mapError { $0 as ApiError }
                    .map { $0.model }
                
                completion(mappedResult)
            }
        }
    }
}
