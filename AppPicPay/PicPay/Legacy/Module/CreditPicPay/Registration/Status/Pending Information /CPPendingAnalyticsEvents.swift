import AnalyticsModule

struct CPPendingAnalyticsEvents {
    let analytics: AnalyticsProtocol
    
    private var name: String {
        "PicPay Card - Request - Sign Up - Info Pending"
    }
    
    private var providers: [AnalyticsProvider] {
        [.firebase, .mixPanel]
    }
    
    func sendTapSolveAfter() {
        sendEvent(action: "act-do-after")
    }
    
    func sendTapResolveProblems() {
        sendEvent(action: "act-fix-info-pending")
    }
    
    func sendEvent(action: String) {
        let event = AnalyticsEvent(name, properties: ["action": action], providers: providers)
        analytics.log(event)
    }
}
