import UIKit
import AnalyticsModule

final class CPPendingViewModel {
    typealias Dependencies = HasAnalytics
    private let account: CPAccount?
    var creditForm: CreditForm?
    let api = ApiCredit()
    private let analyticsContext: CPPendingAnalyticsEvents
    
    init(with account: CPAccount?, dependencies: Dependencies = DependencyContainer()) {
        self.account = account
        self.analyticsContext = CPPendingAnalyticsEvents(analytics: dependencies.analytics)
    }
    
    func trackScreenViewEvent() {
        PPAnalytics.trackEvent(CreditPicPayEvent.Registration.pageViewPendingInformations, properties: nil)
    }
    
    func trackResolveProblems() {
        analyticsContext.sendTapResolveProblems()
    }
    
    func trackSolveAfter() {
        analyticsContext.sendTapSolveAfter()
    }
    
    func loadRegistrationData(_ completion: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        api.getCreditData(credit: creditForm) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            DispatchQueue.main.async {
                switch result {
                case .success(let registration):
                    strongSelf.creditForm = registration
                    completion()
                case .failure(let error):
                    onError(error)
                }
            }
        }
    }
    
    func getErros() -> NSAttributedString? {
        guard let registrationErrors = account?.registrationErrors else {
            return nil
        }
        let errors = NSMutableAttributedString()
        let paragraph = NSMutableParagraphStyle()
        paragraph.headIndent = 15
        paragraph.paragraphSpacingBefore = 10
        let attributeds: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 15, weight: .bold),
            .foregroundColor: #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1),
            .paragraphStyle: paragraph
        ]
        for error in registrationErrors {
            errors.append(NSAttributedString(string: "\u{2022} \(error)\n", attributes: attributeds))
        }
        return errors
    }
}
