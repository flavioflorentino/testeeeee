import UI

final class CPPendingViewController: PPBaseViewController {
    private let viewModel: CPPendingViewModel

    @IBOutlet weak private var errorsList: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var correctButton: UIPPButton!
    @IBOutlet weak var laterButton: UIButton!
    
    init(with viewModel: CPPendingViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "CPPendingViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewConfiguration()
        loadRegistrationData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        viewModel.trackScreenViewEvent()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    @IBAction private func resolveProblemsAction(_ sender: Any) {
        guard let formModel = self.viewModel.creditForm else {
            return
        }
        viewModel.trackResolveProblems()
        let viewModel = CreditAttachDocViewModel(with: formModel, container: DependencyContainer(), isReview: true)
        let controller = CreditAttachDocViewController(with: viewModel)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction private func solveAfter(_ sender: Any) {
        viewModel.trackSolveAfter()
        dismiss(animated: true, completion: nil)
    }
    
    func populateErrors() {
        guard let errorAttributedString = viewModel.getErros() else {
            return
        }
        errorsList.attributedText = errorAttributedString
    }
    
    func loadRegistrationData() {
        startLoadingView()
        viewModel.loadRegistrationData({ [weak self] in
            self?.stopLoadingView()
            self?.populateErrors()
        }, onError: { [weak self] error in
            self?.stopLoadingView()
            AlertMessage.showCustomAlertWithError(error, controller: self)
        })
    }
    
    func viewConfiguration() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        titleLabel.textColor = Palette.ppColorGrayscale500.color
        descriptionLabel.textColor = Palette.ppColorGrayscale500.color
        errorsList.textColor =  Palette.ppColorGrayscale500.color
        correctButton.backgroundColor = Palette.ppColorBranding300.color
        laterButton.setTitleColor(Palette.ppColorGrayscale400.color, for: .normal)
    }
}
