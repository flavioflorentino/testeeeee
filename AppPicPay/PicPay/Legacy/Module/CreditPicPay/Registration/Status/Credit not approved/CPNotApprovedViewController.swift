import UIKit

final class CPNotApprovedViewController: PPBaseViewController {
    init() {
        super.init(nibName: "CPNotApprovedViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        trackScreenViewEvent()
    }

    @IBAction private func OkAction(_ sender: Any) {
        if navigationController?.presentingViewController != nil {
            navigationController?.dismiss(animated: true, completion: nil)
        } else {
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func trackScreenViewEvent() {
        PPAnalytics.trackEvent(CreditPicPayEvent.Registration.pageViewNotApproved, properties: nil)
    }
}
