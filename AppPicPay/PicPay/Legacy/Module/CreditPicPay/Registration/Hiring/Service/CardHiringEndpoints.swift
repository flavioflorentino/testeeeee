import Core
import Foundation

enum CardHiringEndpoints: ApiEndpointExposable {
    case dueDayList
    case saveDueDay(day: Int)
    
    var path: String {
        switch self {
        case .dueDayList, .saveDueDay:
            return "credit/account/dueday"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .dueDayList:
            return .get
        case .saveDueDay:
            return .put
        }
    }
    
    var body: Data? {
        guard case let .saveDueDay(day) = self else {
            return nil
        }
        return ["value": day].toData()
    }
    
    var customHeaders: [String : String] {
        return [:]
    }
}
