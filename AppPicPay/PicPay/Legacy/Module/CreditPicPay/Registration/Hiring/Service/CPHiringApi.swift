import Alamofire
import Core
import FeatureFlag
import SwiftyJSON
import UIKit

final class CPHiringApi {
    fileprivate let requestManager = RequestManager.shared
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    // MARK: - Due day methods
    
    func getDueDayList(_ completion: @escaping (PicPayResult<CPDueDayResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonCardHiringToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlCreditPicpayDueDay, method: .get)
                .responseApiObject(completionHandler: completion)
            return
        }

        // Core network version
        Api<Data>(endpoint: CardHiringEndpoints.dueDayList).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }

    func saveDueDay(for selectedDay: Int, _ completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonCardHiringToCore) else {
            let params: [String: Any] = ["value": selectedDay]
            requestManager
                .apiRequest(endpoint: kWsUrlCreditPicpayDueDay, method: .put, parameters: params)
                .responseApiObject(completionHandler: completion)
            return
        }

        // Core network version
        Api<Data>(endpoint: CardHiringEndpoints.saveDueDay(day: selectedDay)).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
}
