import Foundation

protocol CPDueDayCoordinatorProtocol {
    func onSaveDueDay(_ dueDay: Int, bestPurchaseDay: Int, limit: Float)
    func onChangeDueDay()
    func closeView()
}

final class CPDueDayCoordinator {
    private let account: CPAccount
    private var navigationController: UINavigationController?

    init(with navigationController: UINavigationController?, account: CPAccount) {
        self.navigationController = navigationController
        self.account = account
    }
    
    func start() {
        let dependencies = DependencyContainer()
        let viewModel = CPDueDayViewModel(account: account, coordinator: self, dependencies: dependencies)
        let controller = CPDueDayViewController(with: viewModel)
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension CPDueDayCoordinator: CPDueDayCoordinatorProtocol {
    func onSaveDueDay(_ dueDay: Int, bestPurchaseDay: Int, limit: Float) { }
    
    func onChangeDueDay() {
        guard let view = navigationController?.viewControllers.last?.view else {
            return
        }
        TaskCompleteAnimation.showSuccess(onView: view, completion: { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        })
    }
    
    func closeView() {
        navigationController?.popViewController(animated: true)
    }
}
