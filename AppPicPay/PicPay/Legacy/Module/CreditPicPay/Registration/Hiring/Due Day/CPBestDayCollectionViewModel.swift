final class CPBestDayCollectionViewModel {
    let dayLabel: String = CreditPicPayLocalizable.day.text
    let dayNumber: Int
    let bestPurchaseDay: Int?
    var isSelected: Bool = false
    
    init(dayNumber: Int, isSelected: Bool, bestPurchaseDay: Int?) {
        self.dayNumber = dayNumber
        self.bestPurchaseDay = bestPurchaseDay
        self.isSelected = isSelected
    }
}
