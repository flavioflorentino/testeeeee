import UI
import UIKit
import Card

final class CPDueDayViewController: PPBaseViewController {
    private let viewModel: CPDueDayViewModel
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 11.0, *) {
            return .default
        } else {
            return .lightContent
        }
    }
    
    @IBOutlet weak var headerDescriptionLabel: UILabel! {
        didSet {
            headerDescriptionLabel.textColor = Palette.ppColorGrayscale500.color
            headerDescriptionLabel.numberOfLines = 0
        }
    }
    @IBOutlet weak var bestPurchaseDayLabel: UILabel! {
        didSet {
            bestPurchaseDayLabel.attributedText = viewModel.bestPurchaseDay
            bestPurchaseDayLabel.textColor = Palette.ppColorGrayscale500.color
        }
    }
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var heightCollectionViewContraint: NSLayoutConstraint!
    @IBOutlet weak var nextButton: UIPPButton! {
        didSet {
            updateStateOfNextButton()
            nextButton.setTitle(viewModel.nextButtonTitle, for: .normal)
            nextButton.setBackgroundColor(Palette.ppColorBranding300.color, states: [.normal])
            nextButton.setBackgroundColor(Palette.ppColorGrayscale400.color, states: [.disabled])
        }
    }
    
    init(with viewModel: CPDueDayViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "CPDueDayViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = viewModel.titleView
        setUpView()
        setUpActionNextButton()
        setUpForPresented()
        collectionView.registerCell(type: CPBestDayCollectionViewCell.self)
        collectionView.backgroundColor = .clear
        setHeightCollectionView()
        loadData()
    }
    
    @IBAction private func nextButtonTapped(_ sender: Any) {
        viewModel.prepateToGoNextView()
    }
    
    private func setUpView() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
}

extension CPDueDayViewController: UICollectionViewDataSource {
    // MARK: UI Collection view data source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CPBestDayCollectionViewCell = collectionView.dequeueReusableCell(type: CPBestDayCollectionViewCell.self, forIndexPath: indexPath)
        cell.viewModel = viewModel.dueDayViewModel(for: indexPath.row)
        return cell
    }
}

extension CPDueDayViewController: UICollectionViewDelegate {
    // MARK: - UI Collection view delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.selectedDay(for: indexPath.row)
        updateStateOfNextButton()
        setUpBestPuchaseDay()
        collectionView.reloadData()
    }
}

extension CPDueDayViewController: UICollectionViewDelegateFlowLayout {
    // MARK: - UI Collection view delegate flow layout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       return sizeForItem()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}

extension CPDueDayViewController {
    private func setUpForPresented() {
        guard isModal && navigationController?.viewControllers.first == self else {
            let backButton = UIBarButtonItem(title: String(), style: .plain, target: nil, action: nil)
            navigationItem.backBarButtonItem = backButton
            
            return
        }
        let barCloseButtonItem = UIBarButtonItem(
            image: UI.Assets.icoCloseBigGreen.image,
            style: .plain,
            target: self,
            action: #selector(closeView)
        )
        
        navigationItem.rightBarButtonItem = barCloseButtonItem
    }
    
    private func setUpBestPuchaseDay() {
        bestPurchaseDayLabel.attributedText = viewModel.bestPurchaseDay
    }
    
    // MARK: Requests
    private func loadData() {
        startLoadingView()
        viewModel.loadData({ [weak self] in
            self?.stopLoadingView()
            self?.setUpViewAfterLoad()
        }, onError: { [weak self] error in
            self?.stopLoadingView()
            self?.showError(error)
        })
    }
    
    private func saveDueDayValue() {
        startLoadingView()
        nextButton.isEnabled = false
        viewModel.saveDueDay({ [weak self] in
            self?.stopLoadingView()
            self?.nextButton.isEnabled = true
        }, onError: { [weak self] error in
            self?.stopLoadingView()
            self?.nextButton.isEnabled = true
            self?.showError(error)
        })
    }
    
    // MARK: Other functions
    
    private func setUpViewAfterLoad() {
        updateStateOfNextButton()
        collectionView.reloadData()
        setUpBestPuchaseDay()
        headerDescriptionLabel.text = viewModel.headerDescriptionText
    }
    
    private func setUpActionNextButton() {
        viewModel.onTapNextButton = { [weak self] state in
            switch state {
            case .next:
                self?.saveDueDayValue()
            case .showAlertConfirmation:
                self?.showAlertConfirmationForChangeDueDay()
            }
        }
    }

    private func showAlertConfirmationForChangeDueDay() {
        guard let alert = viewModel.confirmationChangeDueDayAlert else {
            return
        }
        AlertMessage.showAlert(alert, controller: self) { [weak self] popUpController, button, _ in
            if button.action != .close {
                popUpController.dismiss(animated: true, completion: {
                    self?.saveDueDayValue()
                })
            }
        }
    }
    
    private func sizeForItem() -> CGSize {
        let numberOfItemsForLine: CGFloat = 4.0
        let insetMargins: CGFloat = 16.0 * 2
        let marginCollectionView: CGFloat = 4.0 * 2
        let spacingForCells: CGFloat = 10.0 * numberOfItemsForLine
        let width: CGFloat = (view.frame.width - (insetMargins + marginCollectionView + spacingForCells)) / numberOfItemsForLine
        let height: CGFloat = width
        return CGSize(width: width, height: height)
    }
    
    private func showError(_ error: Error) {
        let alert = viewModel.errorAlert(with: error)
        AlertMessage.showAlert(alert, controller: self) { [weak self] popUpController, _, _ in
            guard let strongSelf = self else {
            return
        }
            popUpController.dismiss(animated: true, completion: {
                if !strongSelf.viewModel.isLoadedData {
                    self?.viewModel.coordinator.closeView()
                }
            })
        }
    }
    
    private func setHeightCollectionView() {
        let height = (sizeForItem().height * 2) + (10 * 3)
        heightCollectionViewContraint.constant = height
        view.layoutIfNeeded()
    }

    private func updateStateOfNextButton() {
        nextButton.isEnabled = viewModel.isSelectedDueDay
    }
    
    @objc(closeView)
    private func closeView() {
        viewModel.coordinator.closeView()
    }
}

extension CPDueDayViewController: StyledNavigationDisplayable {}
