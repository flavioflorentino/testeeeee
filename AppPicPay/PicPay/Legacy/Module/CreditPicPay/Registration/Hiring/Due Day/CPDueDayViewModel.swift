import AnalyticsModule
import Card
import FeatureFlag
import UI

final class CPDueDayViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let api = CPHiringApi()
    private var dueDayViewModels: [CPBestDayCollectionViewModel] = []
    private var dueDayResponse: CPDueDayResponse?
    private let account: CPAccount
    
    var bestPurchaseDay: NSAttributedString? {
        guard let selectedModel = dueDayViewModels.first(where: { $0.isSelected }),
              let bestPurchaseDay = selectedModel.bestPurchaseDay  else {
            let text = String(format: CreditPicPayLocalizable.selectTheDay.text)
            return NSMutableAttributedString(string: text)
        }
        let text = String(format: CreditPicPayLocalizable.bestDayBuy.text, bestPurchaseDay)
        let content = NSMutableAttributedString(string: text)
        let location = 26
        let length = text.count - location
        let range = NSRange(location: location, length: length)
        content.addAttribute(.foregroundColor, value: Palette.ppColorBranding300.color, range: range)
        content.addAttribute(.font, value: UIFont.systemFont(ofSize: 15, weight: .bold), range: range)
        return content
    }
    var isLoadedData: Bool = false
    let titleView: String = CreditPicPayLocalizable.dueDateTitle.text
    
    lazy var nextButtonTitle: String = {
        CreditPicPayLocalizable.changeDueDate.text
    }()
    
    var confirmationChangeDueDayAlert: Alert? {
        guard let selectedDueDay = getSelectedDueDayValue() else {
            return nil
        }
        let title = NSAttributedString(string: CreditPicPayLocalizable.changeInvoiceDueDate.text)
        let dayText = String(format: CreditPicPayLocalizable.newDueDate.text, selectedDueDay)
        let content = NSMutableAttributedString(
            string: dayText,
            attributes: [
                .foregroundColor: Palette.ppColorGrayscale500.color,
                .font: UIFont.systemFont(ofSize: 14, weight: .regular)
            ]
        )
        let locationText = 31
        let lenghtBoldText = (dayText.count - 1) - locationText
        let range = NSRange(location: locationText, length: lenghtBoldText)
        content.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 14, weight: .bold), range: range)
        
        let alert = Alert(title: title, text: content)
        let confirmButton = Button(title: CreditPicPayLocalizable.confirmChange.text, type: .cta, action: .confirm)
        let cancelButton = Button(title: CreditPicPayLocalizable.doItLater.text, type: .clean, action: .close)
        alert.showCloseButton = true
        alert.buttons = [confirmButton, cancelButton]
        return alert
    }
    lazy var limit: Float = {
        return Float(account.availableLimit)
    }()
    var isHiddenInformationForSelectDay: Bool {
        return isSelectedDueDay
    }
    var isSelectedDueDay: Bool {
        return getSelectedDueDayValue() != nil
    }
    var headerDescriptionText: String {
        CreditPicPayLocalizable.changeDay.text
    }
    
    var onTapNextButton: (State) -> Void = { _ in }
    let coordinator: CPDueDayCoordinatorProtocol
    
    init(
        account: CPAccount,
        coordinator: CPDueDayCoordinatorProtocol,
        dependencies: Dependencies
    ) {
        self.account = account
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func numberOfRows() -> Int {
        return dueDayViewModels.count
    }
    
    func dueDayViewModel(for row: Int) -> CPBestDayCollectionViewModel {
        return dueDayViewModels[row]
    }
    
    func selectedDay(for row: Int) {
        /// deselect all models
        for model in dueDayViewModels where model.isSelected {
            model.isSelected = false
        }
        dependencies.analytics.log(CreditHiringEvent.dueDay(action: .selectDay))
        dueDayViewModels[row].isSelected = true
    }
    
    func loadData(_ onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        api.getDueDayList { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    strongSelf.setUpDueDay(for: response)
                    strongSelf.isLoadedData = true
                    onSuccess()
                case .failure(let error):
                    strongSelf.isLoadedData = false
                    onError(error)
                }
            }
        }
    }
    
    func saveDueDay(_ onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        guard let dayNumber: Int = getSelectedDueDayValue() else {
            onError(PicPayError(message: CreditPicPayLocalizable.selectTheDay.text))
            return
        }
        api.saveDueDay(for: dayNumber) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    onSuccess()
                    self?.goToNextView()
                case .failure(let error):
                    onError(error)
                }
            }
        }
    }
    
    func errorAlert(with error: Error) -> Alert {
        let title = NSAttributedString(string: CreditPicPayLocalizable.somethingWentWrong.text)
        let text = error.localizedDescription.html2Attributed(UIFont.systemFont(ofSize: 14))?.attributedPicpayFont() ?? NSAttributedString(string: error.localizedDescription)
        let alert = Alert(title: title, text: text)
        let button = Button(title: DefaultLocalizable.btOkUnderstood.text, type: Button.ButtonType.cta, action: Button.Action.close)
        alert.buttons = [button]
        alert.image = Alert.Image(with: #imageLiteral(resourceName: "icon-bad"))
        alert.dismissWithGesture = false
        alert.dismissOnTouchBackground = false
        return alert
    }
    
    func goToNextView() {
        coordinator.onChangeDueDay()
    }
    
    func prepateToGoNextView() {
        onTapNextButton(.showAlertConfirmation)
    }
}

extension CPDueDayViewModel {
    private func getSelectedDueDayValue() -> Int? {
        guard let selectedModel = dueDayViewModels.first(where: { $0.isSelected }) else {
            return nil
        }
        return selectedModel.dayNumber
    }
    
    private func setUpDueDay(for response: CPDueDayResponse) {
        dueDayResponse = response
        dueDayViewModels = response.validDueDays.map { dueDay in
            return CPBestDayCollectionViewModel(dayNumber: dueDay.dueDay,
                                                isSelected: false,
                                                bestPurchaseDay: dueDay.bestPurchaseDay)
        }
        let selectedInResponse = response.currentDueDay
        let dueDayViewModel = dueDayViewModels.last(where: { $0.dayNumber == selectedInResponse })
        dueDayViewModel?.isSelected = true
    }
}

extension CPDueDayViewModel {
    enum State {
        case showAlertConfirmation
        case next
    }
}
