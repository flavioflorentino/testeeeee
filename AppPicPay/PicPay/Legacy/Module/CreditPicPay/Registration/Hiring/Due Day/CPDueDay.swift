import SwiftyJSON

struct CPDueDayResponse: BaseApiResponse {
    let currentDueDay: Int?
    let validDueDays: [CPDueDay]
    
    init?(json: JSON) {
        var dueDays: [CPDueDay] = []
        if let list = json["valid_due_days"].array {
            for jsonDueDay in list {
                guard let dueDay = CPDueDay(json: jsonDueDay) else { continue }
                dueDays.append(dueDay)
            }
        }
        validDueDays = dueDays
        currentDueDay = json["current_due_day"].int
    }
}

struct CPDueDay: BaseApiResponse {
    let dueDay: Int
    let bestPurchaseDay: Int?
    
    init?(json: JSON) {
        guard let dueDay = json["due_day"].int else {
     return nil
}
        self.dueDay = dueDay
        bestPurchaseDay = json["best_purchase_day"].int
    }
}
