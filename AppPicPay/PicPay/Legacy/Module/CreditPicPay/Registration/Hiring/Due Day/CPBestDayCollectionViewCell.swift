import UI
import UIKit

final class CPBestDayCollectionViewCell: UICollectionViewCell {
    static let identifier: String = "bestDayIdentifier"
    var viewModel: CPBestDayCollectionViewModel? {
        didSet {
            guard let viewModel = self.viewModel else {
            return
        }
            diaLabel.text = viewModel.dayLabel
            dayNumber.text = "\(viewModel.dayNumber)"
            backgroundCircle.isSelected = viewModel.isSelected
            
            if viewModel.isSelected {
                diaLabel.textColor = Palette.white.color
                dayNumber.textColor = Palette.white.color
            } else {
                diaLabel.textColor = .lightGray
                dayNumber.textColor = .lightGray
            }
        }
    }
    
    @IBOutlet weak var backgroundCircle: BackgroundCircleValueView!
    @IBOutlet weak var diaLabel: UILabel!
    @IBOutlet weak var dayNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        backgroundCircle.backgroundColor = .clear
    }
}
