import Foundation
import SwiftyJSON

struct CreditDocument: BaseApiResponse {
    var issuerOfficeId: Int? = 0
    var number: String = ""
    var stateInitials: String = ""
    var typeId: Int?
    
    init() { }
    
    init?(json: JSON) { }

    init(json: [AnyHashable: Any]) {
        issuerOfficeId = json["issuer_office_id"] as? Int
        number = json["number"] as? String ?? ""
        stateInitials = json["state_initials"] as? String ?? ""
        typeId = json["type_id"] as? Int
    }
}
