import SwiftyJSON
import UIKit
import UI

public final class CreditForm: BaseApiResponse {
    //Flow
    var isLoanFlow: Bool = false
    
    // Status
    var account: CPAccount?
    
    // Registration Status
    var registrationStep: CPStep = .empty
    var registrationStatus: String = ""
    var creditGrantedByBank: Double = 0.00
    var creditRegistrationStatus: String = ""
    var reviewStatus: String = ""

    // Credit Preferences
    var dueDay: Int?
    var selectedCredit: Double?

    // CreditPersonalDataViewController
    var name: String = ""
    var email: String = ""
    var cpf: String = ""
    var mothersName: String = ""
    var ddd: String?
    var phone: String?
    var birthday: String = ""
    
    // CreditBirthplaceFormControllerViewController
    var birthplaceCountry: String = "Brasil"
    var birthplaceCountryAbbr: String = "BR"
    var birthplaceState: String = ""
    var birthplaceStateAbbr: String = ""
    var birthplaceCity: String = ""
    var birthplaceCityId: Int?
    
    // CreditAddressViewController
    var homeAddress: AddressSearch?

    // CreditRGViewController
    var docTypeId: Int?
    var docTypeName: String = ""
    var docExpeditionId: Int?
    var docExpeditionName: String = ""
    var docState: String?
    var docNumber: String = ""
    var emissionDate: String = ""

    var docType: CreditDocumentType?
    var docExpedition: CreditIssuerOffices?

    var occupation: CreditOccupation?
    var profession: CreditProfession?
    var income: Double?
    var patrimony: CreditPatrimony?
    var patrimonyId: Int?
    
    var userDocumentsFiles: [CreditFileDocument] = []

    // Signature
    var signature: UIImage?
    
    required init() {}

    required public init?(json: JSON) {
        registrationStep = CPStep(rawValue: json["registration_step"].string ?? "") ?? .empty
        registrationStatus = json["registration_status"].string ?? ""
        creditGrantedByBank = json["credit_granted_by_bank"].double ?? 0.00
        creditRegistrationStatus = json["credit_status"].string ?? ""
        reviewStatus = json["review_status"].string ?? ""
        dueDay = json["due_day"].int
        selectedCredit = json["selected_credit"].double
        name = json["name"].string ?? ""
        email = json["email"].string ?? ""
        cpf = formatCPFToDisplay(cpf: json["cpf"].string ?? "")
        mothersName = json["mothers_name"].string ?? ""
        birthday = formatDateToDisplay(date: json["birth_date"].string ?? "")
        income = json["monthly_income"].double
        ddd = json["ddd"].string
        phone = json["phone"].string
        birthplaceCountry = json["country_of_birth_name"].string ?? "Brasil"
        birthplaceState = json["state_of_birth_name"].string ?? ""
        birthplaceStateAbbr = json["state_of_birth_initials"].string ?? ""
        birthplaceCityId = Int(json["city_of_birth_id"].string ?? "")
        birthplaceCity = json["city_of_birth_name"].string ?? ""
        patrimony = CreditPatrimony(json: json["patrimony"])
        patrimonyId = patrimony?.id
        income = json["monthly_income"].double
        occupation = CreditOccupation(json: json["occupation"])
        profession = CreditProfession(json: json["profession"])
        
        parserDocuments(with: json)
        parserHomeAddress(with: json)
        parserPersonalDocuments(with: json)
    }
    
    func getParamsToRequest() -> [String: Any] {
        return creditRegistrationParams()
    }
 }

extension CreditForm {
    fileprivate func parserDocuments(with json: JSON) {
        if let jsonDocuments = json["user_documents"].array {
            for jsonDocument in jsonDocuments {
                guard let userDocument: CreditFileDocument = jsonDocument.decoded() else { continue }
                userDocumentsFiles.append(userDocument)
            }
        }
    }
    
    func parserHomeAddress(with json: JSON) {
        if let data = try? json["home_address"].rawData() {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            if let address = try? decoder.decode(AddressSearch.self, from: data) {
                homeAddress = address
            }
        }
    }
    
    fileprivate func parserPersonalDocuments(with json: JSON) {
        if let document = json["id_document"].dictionaryObject {
            self.docTypeId = document["type_id"] as? Int
            self.docTypeName = document["type_name"] as? String ?? ""
            self.docExpeditionId = document["issuer_office_id"] as? Int
            self.docExpeditionName = document["issuer_office_name"] as? String ?? ""
            self.docNumber = document["number"] as? String ?? ""
            self.emissionDate = formatDateToDisplay(date: document["emission_date"] as? String ?? "")
            
            if let docState = document["state"] as? String, !docState.isEmpty {
                self.docState = docState
            }
        }
    }
}

extension CreditForm {
    private func creditRegistrationParams() -> [String: Any] {
        var params: [String: Any] = [:]
        
        /// personal Data
        params["name"] = !self.name.isEmpty ? self.name : nil
        params["email"] = !self.email.isEmpty ? self.email : nil
        params["birth_date"] = getDateString(from: self.birthday)
        params["mothers_name"] = !self.mothersName.isEmpty ? self.mothersName : nil
        params["registration_step"] = self.registrationStep.rawValue
        params["cpf"] = self.cpf
        
        if let ddd = self.ddd, let phone = self.phone {
            params["ddd"] = ddd
            params["phone"] = phone
        }

        /// birthplace
        params["country_of_birth_initials"] = !self.birthplaceCountryAbbr.isEmpty ? self.birthplaceCountryAbbr : nil
        params["state_of_birth_initials"] = !self.birthplaceStateAbbr.isEmpty ? self.birthplaceStateAbbr: nil
        params["city_of_birth_id"] = self.birthplaceCityId
        params["city_of_birth_name"] = !self.birthplaceCity.isEmpty ? self.birthplaceCity : nil
        
        /// address step
        if let homeAddress: AddressSearch = self.homeAddress {
            params["home_address"] = convertAddressToParams(homeAddress)
        }
        
        if let documentParams = getDocumentParams() {
            params["id_document"] = documentParams
        }
        
        params["occupation"] = getOccupationParams()
        params["profession"] = getProfessionParams()
        
        params["monthly_income"] = self.income
        params["patrimony"] = self.patrimonyId != nil ? ["id": self.patrimonyId] : nil
        params["due_day"] = self.dueDay
        params["selected_credit"] = self.selectedCredit
        
        return params
    }
    
    private func getProfessionParams() -> [String: Any]? {
        guard let profession = self.profession else {
     return nil
}
        let professionParams: [String: Any] = [
            "id": profession.id,
            "name": profession.name
        ]
        return professionParams
    }
    
    private func getOccupationParams() -> [String: Any]? {
        guard let occupation = self.occupation else {
     return nil
}
        let params: [String: Any] = [
            "id": occupation.id,
            "name": occupation.name
        ]
        return params
    }

    func convertAddressToParams(_ address: AddressSearch) -> [String: Any] {
        var params: [String: Any] = [:]
        params["city_id"] = address.cityId
        params["address_complement"] = address.addressComplement
        params["neighborhood"] = address.neighborhood
        params["street_number"] = address.streetNumber.isEmpty ? "0" : address.streetNumber
        params["state"] = address.state
        params["zip_code"] = address.zipCode?.onlyNumbers
        params["street"] = address.street
        params["street_type"] = address.streetType
        return params
    }
    
    private func getDocumentParams() -> [String: Any]? {
        if let docType = self.docTypeId {
            var params: [String: Any] = [:]
            params["type_id"] = docType
            params["issuer_office_id"] = self.docExpeditionId
            params["state_initials"] = self.docState ?? ""
            params["state"] = self.docState ?? ""
            params["number"] = !self.docNumber.isEmpty ? self.docNumber : nil
            params["emission_date"] = getDateString(from: self.emissionDate)
            return params
        }
        return nil
    }
    
    private func getDateString(from string: String) -> String? {
        guard string.count == 10 else {
     return nil
}
        let brFormatter = DateFormatter.brazillianFormatter(dateFormat: "dd/MM/yyyy")
        let usFormatter = DateFormatter.brazillianFormatter(dateFormat: "yyyy-MM-dd")
        guard let date = brFormatter.date(from: string) else {
     return nil
}
        return usFormatter.string(from: date)
    }
    
    private func formatDateToDisplay(date: String?) -> String {
        guard let date = date else {
            return ""
        }
        guard let dateObject = DateFormatter.brazillianFormatter(dateFormat: "yyyy-MM-dd").date(from: date) else {
            return ""
        }
        let stringDate = DateFormatter.brazillianFormatter(dateFormat: "dd/MM/yyyy").string(from: dateObject)
        return stringDate
    }
    
    private func formatCPFToDisplay(cpf: String?) -> String {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        return mask.maskedText(from: cpf) ?? ""
    }
}
