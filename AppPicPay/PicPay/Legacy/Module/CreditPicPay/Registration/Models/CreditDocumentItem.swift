import Foundation

struct CreditDocumentItem: Codable, Equatable {
    let key: CreditImageDocumentType
    let description: String
    let dialog: Dialog
    let photo: Photo
    
    var documentFile: CreditFileDocument?
}

extension CreditDocumentItem {
    struct Dialog: Codable, Equatable {
        let icon: String
        let title: String
        let text: String
    }
    
    struct Photo: Codable, Equatable {
        let hint: String
        let confirmText: String
        let identityAlreadyValidated: Bool?
        let masked: Bool
        let selfie: Bool
        let gallery: Bool
    }
}
