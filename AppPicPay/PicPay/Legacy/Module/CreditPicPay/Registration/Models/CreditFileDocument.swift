import SwiftyJSON
import UIKit

struct CreditFileDocument: Codable, Equatable {
    let documentType: CreditImageDocumentType
    let fileKey: String
    let fileUrl: String
    let denied: Bool?
    let reviewMessage: String?
    let reviewed: Bool
}
