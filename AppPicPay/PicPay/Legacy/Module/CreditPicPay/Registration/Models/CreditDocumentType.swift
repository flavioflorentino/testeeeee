import Foundation
import SwiftyJSON

struct CreditIssuerOffices: BaseApiResponse, Codable {
    var id: Int?
    var name: String?
    let initials: String?
    var fullName: String?
    
    init?(json: JSON) {
        id = json["id"].int
        name = json["name"].string ?? ""
        initials = json["initials"].string ?? ""
        fullName = json["fullname"].string ?? ""
    }
}

struct CreditDocumentType: BaseApiResponse, Codable {
    let id: Int
    let name: String
    let needEmitterState: Bool
    var issuerOffices: [CreditIssuerOffices]
    
    init?(json: JSON) {
        guard let id = json["id"].int else {
            return nil
        }
        guard let name = json["name"].string else {
            return nil
        }
        self.id = id
        self.name = name
        self.needEmitterState = json["need_emitter_state"].boolValue
        
        var issuerOfficesAux: [CreditIssuerOffices] = []
        if let issuerOffices = json["issuer_offices"].array {
            for office in issuerOffices {
                guard let office = CreditIssuerOffices(json: office) else { continue }
                issuerOfficesAux.append(office)
            }
        }
        issuerOffices = issuerOfficesAux
    }
}
