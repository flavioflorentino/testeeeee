import Foundation

public enum CPStep: String {
    case personalData = "PERSONAL_DATA"
    case birthPlace = "BIRTH_PLACE"
    case address = "ADDRESS"
    case documentId = "DOCUMENT_ID"
    case documentFile = "DOCUMENT_FILE"
    case professionalData = "PROFESSIONAL_DATA"
    case yourCredit = "CREDIT_DATA"
    case confirmation = "CONFIRMATION"
    case empty = ""
}
