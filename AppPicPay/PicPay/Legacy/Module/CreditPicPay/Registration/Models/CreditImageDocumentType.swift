import Foundation

enum CreditImageDocumentType: String, Codable {
    case selfie = "SELFIE"
    case documentFront = "ID_FRONT"
    case documentBack = "ID_BACK"
    case proofOfAddress = "PROOF_OF_ADDRESS"
}
