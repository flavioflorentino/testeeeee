import SwiftyJSON
import UIKit

struct CreditOccupation: BaseApiResponse {
    let id: Int
    let name: String
    
    init?(json: JSON) {
        guard let id = json["id"].int else {
            return nil
        }
        guard let name = json["name"].string else {
            return nil
        }
        self.id = id
        self.name = name
    }
}
