import CoreLegacy
import SwiftyJSON

struct CreditPatrimony: BaseApiResponse, Codable {
    let id: Int
    let minValue: Double
    let maxValue: Double
    
    var minMaxValue: String {
        let minValueFormatted = CurrencyFormatter.brazillianRealString(from: NSNumber(value: minValue)) ?? ""
        let maxValueFormatted = CurrencyFormatter.brazillianRealString(from: NSNumber(value: maxValue)) ?? ""
        
        if maxValue == -1 {
            return "maior que \(minValueFormatted)"
        }
        return "\(minValueFormatted) à \(maxValueFormatted)"
    }
    
    init?(json: JSON) {
        guard let id = json["id"].int else {
            return nil
        }
        guard let minValue = json["min_value"].double else {
            return nil
        }
        guard let maxValue = json["max_value"].double else {
            return nil
        }
        self.id = id
        self.minValue = minValue
        self.maxValue = maxValue
    }
}
