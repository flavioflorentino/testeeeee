import AnalyticsModule
import Card
import FeatureFlag
import Foundation
import UIKit

public protocol CPRegistrationCoordinatorProtocol {
    func getStepProgressForController(with currentStep: CPStep) -> Float
    func pushController(with creditForm: CreditForm)
}

final class CPRegistrationCoordinator {
    static var shared: CPRegistrationCoordinator?
    private let navigationController: UINavigationController
    private var creditForm: CreditForm
    private var orderOfFlow: [CPStep] {
        if FeatureManager.isActive(.isCardOnboardingVisibilityActivated) {
            return [.empty, .personalData, .birthPlace, .address, .documentId, .documentFile, .professionalData, .confirmation]
        } else {
            Analytics.shared.log(CPRegistrationCoordinatorEvent.flowWithoutOnboarding)
            return [.personalData, .birthPlace, .address, .documentId, .documentFile, .professionalData, .confirmation]
        }
        
    }
    
    required init(with navigationController: UINavigationController, creditForm: CreditForm) {
        self.navigationController = navigationController
        self.creditForm = creditForm
    }
    
    /// De init singleton
    func dispose() {
        CPRegistrationCoordinator.shared = nil
        print("Disposed CPRegistrationCoordinator instance")
    }
    
    func didCompletedRegistration() {
        navigationController.pushViewController(
            viewController: CreditAnalysisFactory.make(),
            animated: true,
            completion: { [weak self] in
                self?.dispose()
            }
        )
    }

    func makeStepControllers() {
        let currentStep: CPStep = self.creditForm.registrationStep
        if currentStep != .empty {
            /// The user returned to continue registration
            var controllersFlow: [UIViewController] = []
            for step in orderOfFlow where currentStep != .empty {
                if currentStep == step {
                    /// if qual to current step break loop for not insert controller in navigation
                    break
                } else {
                    guard let controller = controller(for: step) else { continue }
                    controllersFlow.append(controller)
                }
            }
            
            /// insert the last view controller step
            guard let controller = controller(for: currentStep) else {
                return
            }
            
            controllersFlow.append(controller)
            
            navigationController.setViewControllers(controllersFlow, animated: false)
        } else {
            /// It's begin registration
            guard let firstStep = orderOfFlow.first,
                let controller = controller(for: firstStep),
                let currentViewController = navigationController.viewControllers.first
                else {
                    return
            }
            
            navigationController.replace(currentViewController, by: controller)
        }
    }
}

extension CPRegistrationCoordinator {
    private func getNextStep() -> CPStep? {
        let currentStep: CPStep = creditForm.registrationStep
        for (index, step) in orderOfFlow.enumerated() where step == currentStep && step != .confirmation {
            return orderOfFlow[index + 1]
        }
        return nil
    }

    private func controller(for step: CPStep) -> UIViewController? {
        let container = DependencyContainer()
        switch step {
        case .address:
            
            if FeatureManager.isActive(.isNewCardAddressScreenAvailable) {
                let controller: CreditAddressViewController = CreditAddressViewController()
                controller.viewModel = CreditAddressViewModel(with: creditForm, container: container)
                controller.setStep(with: getStepProgressForController(with: step))
                return controller
            } else {
                let controller: OldCreditAddressViewController = UIStoryboard.viewController(.credit)
                controller.viewModel = CreditAddressViewModel(with: creditForm, container: container)
                controller.setStep(with: getStepProgressForController(with: step))
                return controller
            }
        case .birthPlace:
            let viewModel = CreditBirthplaceViewModel(with: creditForm, container: container)
            let controller = CreditBirthplaceViewController(with: viewModel)
            return controller
        case .documentFile:
            if !creditForm.isLoanFlow && FeatureManager.isActive(.isCardIdentityVerificationAvailable) {
                return DocumentscopyFactory.make(creditForm: creditForm, isReviewFlow: false)
            }
            let viewModel = CreditAttachDocViewModel(with: creditForm, container: container)
            return CreditAttachDocViewController(with: viewModel)
        case .documentId:
            let controller: CreditRGViewController = UIStoryboard.viewController(.credit)
            controller.viewModel = CreditRGViewModel(with: creditForm, container: container)
            return controller
        case .personalData:
            let viewModel = CreditPersonalDataViewModel(with: creditForm, container: container)
            let controller = CreditPersonalDataViewController(with: viewModel)
            return controller
        case .professionalData:
            return ProfissionalInfoFactory.make(creditForm: creditForm)
        case .confirmation:
            let controller = CPRegistrationResumeViewController.instantiateFromNib()
            controller.viewModel = CPRegistrationResumeViewModel(with: creditForm, container: container)
            return controller
        case .empty:
            let upgrade = creditForm.account?.settings.upgradeDebitToCredit ?? false
            return DynamicOnboardingFactory.make(with: upgrade ? .debitToMultiple : .multiple, delegate: self)
        default:
            return nil
        }
    }
}

extension CPRegistrationCoordinator: DynamicOnboardingCoordinatorDelegate {
    func onRegistrationDidRequest() {
        CPRegistrationCoordinator.shared?.pushController(with: creditForm)
    }
    
    func onboardingDidClose() {
        navigationController.dismiss(animated: true) {
            self.updateParentViewControllerAppearance()
        }
    }
    
    private func updateParentViewControllerAppearance() {
        let topController = AppManager.shared.mainScreenCoordinator?.currentController()
        if let controller = topController {
            controller.beginAppearanceTransition(true, animated: false)
        }
    }
}

extension CPRegistrationCoordinator: CPRegistrationCoordinatorProtocol {
    func getStepProgressForController(with currentStep: CPStep) -> Float {
        guard let index = orderOfFlow.firstIndex(of: currentStep) else {
            return  0.0
        }
        return Float(index + 1) / Float(orderOfFlow.count)
    }
    
    func pushController(with creditForm: CreditForm) {
        self.creditForm = creditForm
        guard let nextStep = getNextStep(), let controller = controller(for: nextStep) else {
            return
        }
        navigationController.pushViewController(controller, animated: true)
    }
}
