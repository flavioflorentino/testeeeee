import AnalyticsModule

public enum CPRegistrationCoordinatorEvent: AnalyticsKeyProtocol {
    case flowWithoutOnboarding
    
    private var name: String {
        "Offer without Webview"
    }
    
    private var properties: [String: Any] {
        [:]
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay Card - Request - \(name)", properties: properties, providers: providers)
    }
}
