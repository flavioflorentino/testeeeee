import AnalyticsModule
import Card
import Foundation

final class CreditPersonalDataViewModel: CreditBaseFormProtocol {
    let creditForm: CreditForm 
    let apiCredit = ApiCredit()
    
    var step: CPStep = .personalData
    private let container: HasAnalytics?
    
    init(with model: CreditForm, container: DependencyContainer) {
        self.creditForm = model
        self.container = container
    }
    
    func syncData() {
        creditForm.registrationStep = step
        container?.analytics.log(RequestCardEvent.personalInfo(action: .nextStep, isLoan: creditForm.isLoanFlow))
        DispatchQueue.global(qos: .userInteractive).async {
            let api = ApiCredit()
            api.setCreditRegistration(self.creditForm) { _ in }
        }
    }
    
    func viewWillPop() {
        container?.analytics.log(RequestCardEvent.idDocument(action: .previousStep, isLoan: creditForm.isLoanFlow))
    }
    
    func trackScreenView() {
        container?.analytics.log(RequestCardEvent.didViewPersonalInfoScreen)
    }
}
