import Foundation
import Validator

enum PersonalDataValidationError: String, Error, CustomStringConvertible, ValidationError {
    case birthdayValidDate = "Insira um data válida dd/mm/aaaa"
    case defaultRequired = "Campo obrigatório"
    case phoneValid = "Telefone inválido"
    
    var description: String {
        return self.rawValue
    }
    
    var message: String {
        return self.rawValue
    }
}
