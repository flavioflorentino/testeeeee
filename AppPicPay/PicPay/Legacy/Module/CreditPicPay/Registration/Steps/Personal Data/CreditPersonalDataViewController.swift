import UI
import UIKit
import Validator

final class CreditPersonalDataViewController: CreditBaseFormController {
    //Constants
    private let nameFieldMaxLength = 100
    private let requiredFieldLength = 3
    private let birthDayMinLength = 10
    
    private let viewModel: CreditPersonalDataViewModel
    
    @IBOutlet private weak var nextBtn: UIPPButton!
    @IBOutlet private weak var nameTextField: UIPPFloatingTextField!
    @IBOutlet private weak var mothersName: UIPPFloatingTextField!
    @IBOutlet private weak var birthdayTextField: UIPPFloatingTextField!
    
    init(with viewModel: CreditPersonalDataViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "CreditPersonalDataViewController", bundle: nil)
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureForm()
        populateForm()
        
        title = Strings.Credit.CreditPersonalData.title
        
        if self.isEditingForm {
            nextBtn.setTitle(DefaultLocalizable.save.text, for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let coordinator = CPRegistrationCoordinator.shared {
            setStep(with: coordinator.getStepProgressForController(with: CPStep.personalData))
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.trackScreenView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if isMovingFromParent {
            viewModel.viewWillPop()
        }
    }
    
    @IBAction private func next(_ sender: Any) {
        guard validate().isValid else {
            return
        }
        saveFormData()
    }
    
    // MARK: - Configuration
    
    func configureForm() {
        addTextField(nameTextField)
        addTextField(mothersName)
        addTextField(birthdayTextField)
        configTextFieldsInpuAccessory()
        configureValidations()
    }
    
    private func configTextFieldsInpuAccessory() {
        let doneToolBar = DoneToolBar(doneText: "Ok")
        nameTextField.inputAccessoryView = doneToolBar
        mothersName.inputAccessoryView = doneToolBar
        birthdayTextField.inputAccessoryView = doneToolBar
    }
    
    func configureValidations() {
        var nameRules = ValidationRuleSet<String>()
        nameRules.add(rule: ValidationRuleLength(min: requiredFieldLength, error: PersonalDataValidationError.defaultRequired))
        
        nameTextField.validationRules = nameRules
        mothersName.validationRules = nameRules

        var birthdayRules = ValidationRuleSet<String>()
        birthdayRules.add(rule: ValidationRuleLength(min: birthDayMinLength, error: PersonalDataValidationError.birthdayValidDate))
        let conditionRule = ValidationRuleCondition<String>(error: PersonalDataValidationError.birthdayValidDate) { dateString -> Bool in
            guard let dateString = dateString else {
                return false
            }
            if let date = DateFormatter.brazillianFormatter(dateFormat: "dd/MM/yyyy").date(from: dateString) {
                return date.compare(Date()) == .orderedAscending
            }
            return false
        }
        birthdayRules.add(rule: conditionRule)
        
        birthdayTextField.validationRules = birthdayRules
        birthdayTextField.maskString = "00/00/0000"
    }
    
    // MARK: - Form Methods
    func populateForm() {
        nameTextField.text = viewModel.creditForm.name
        mothersName.text = viewModel.creditForm.mothersName

        /// If birtday of api is empty, fill text field with local consumer birtday
        if !viewModel.creditForm.birthday.isEmpty {
            birthdayTextField.text = viewModel.creditForm.birthday
        } else if let birtdayLocal = ConsumerManager.shared.consumer?.birth_date {
            birthdayTextField.text = birtdayLocal
        }
    }
    
    func saveFormData() {
        viewModel.creditForm.name = nameTextField.text ?? ""
        viewModel.creditForm.mothersName = mothersName.text ?? ""
        viewModel.creditForm.birthday = birthdayTextField.text ?? ""
        viewModel.syncData()

        guard !self.isFormEdition() else {
            return
        }
        
        CPRegistrationCoordinator.shared?.pushController(with: viewModel.creditForm)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTextField:
            mothersName.becomeFirstResponder()
        case mothersName:
            birthdayTextField.becomeFirstResponder()
        default:
            return false
        }
        return true
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.isEqual(birthdayTextField) {
            return validateBirthDayTextField(textField: textField, inRange: range, replacementeString: string)
        } else if textField.isEqual(nameTextField) || textField.isEqual(mothersName) {
            return validateNameTextField(textField: textField, inRange: range, replacementeString: string)
        }
        
        return true
    }
    
    private func validateBirthDayTextField(textField field: UITextField, inRange range: NSRange, replacementeString string: String) -> Bool {
        if (field.text?.count == 2) || (field.text?.count == 5) {
            // backspace
            if !string.isEmpty {
                field.text = (field.text)! + "/"
            }
        }
        // verify field length
        return !(field.text?.count > birthDayMinLength - 1 && (string.count) > range.length)
    }
    
    private func validateNameTextField(textField field: UITextField, inRange range: NSRange, replacementeString string: String) -> Bool {
        return !(field.text?.count > nameFieldMaxLength - 1 && (string.count) > range.length)
    }
}
