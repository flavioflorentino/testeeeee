import AnalyticsModule

struct CPRegistrationResumeAnalyticsEvents {
    let analytics: AnalyticsProtocol
    
    private var name: String {
        "PicPay Card - Request - Sign Up - Info Summary"
    }
    
    private var providers: [AnalyticsProvider] {
        [.firebase, .mixPanel]
    }
    
    func sendTapContinue() {
        sendEvent(action: "act-next-step", providers: [.firebase, .mixPanel, .appsFlyer])
    }

    func sendTapBackScreen() {
        sendEvent(action: "act-previous-step", providers: providers)
    }
    
    func sendTapFixInformations() {
        sendEvent(action: "act-fix-info", providers: providers)
    }
    
    func sendSelectedInformationToFix(informationType: RegistrationResumeOption) {
        switch informationType {
        case .personalInfo:
            sendEvent(action: "act-fix-personal-info", providers: providers)
        case .birthPlace:
            sendEvent(action: "act-fix-birthplace", providers: providers)
        case .personalAddress:
            sendEvent(action: "act-fix-personal-address", providers: providers)
        case .documentId:
            sendEvent(action: "act-fix-document-id", providers: providers)
        case .additionalInfo:
            sendEvent(action: "act-fix-additional-info", providers: providers)
        }
    }

    func sendEvent(action: String, providers: [AnalyticsProvider]) {
        let event = AnalyticsEvent(name, properties: ["action": action], providers: providers)
        analytics.log(event)
    }
}
