import AnalyticsModule
import CoreLegacy
import Card
import Foundation
import UI

enum RegistrationResumeOption {
    case personalInfo
    case birthPlace
    case personalAddress
    case documentId
    case additionalInfo
}

final class CPRegistrationResumeViewModel: CreditBaseFormProtocol {
    typealias Dependencies = HasAnalytics
    var step: CPStep = .confirmation
    let creditForm: CreditForm
    private let apiCredit = ApiCredit()
    private let analyticsContext: CPRegistrationResumeAnalyticsEvents
    
    init(with model: CreditForm, container: Dependencies) {
        self.creditForm = model
        self.analyticsContext = CPRegistrationResumeAnalyticsEvents(analytics: container.analytics)
    }
    
    func trackBackScreenEvent() {
        analyticsContext.sendTapBackScreen()
    }
    
    func trackNextStepEvent() {
        analyticsContext.sendTapContinue()
    }
    
    func trackFixInfoEvent() {
        analyticsContext.sendTapFixInformations()
    }
    
    func trackSelectedOption(type: RegistrationResumeOption) {
        analyticsContext.sendSelectedInformationToFix(informationType: type)
    }
    
    func syncData() {
        creditForm.registrationStep = step
        DispatchQueue.global(qos: .userInteractive).async { [weak self] in
            guard let self = self else {
                return
            }
            self.apiCredit.setCreditRegistration(self.creditForm) { _ in }
        }
    }
    
    func getResume() -> NSAttributedString {
        let resume = NSMutableAttributedString()
        let titleParagraph = NSMutableParagraphStyle()
        titleParagraph.paragraphSpacing = 18
        let subtitleParagraph = NSMutableParagraphStyle()
        subtitleParagraph.paragraphSpacing = 12
        let textParagraph = NSMutableParagraphStyle()
        textParagraph.paragraphSpacing = 8
        // Atributted
        let superTitle: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 20, weight: .bold),
            .foregroundColor: Palette.ppColorGrayscale500.color,
            .paragraphStyle: titleParagraph
        ]
        let subTitle: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 15, weight: .bold),
            .foregroundColor: Palette.ppColorGrayscale500.color,
            .paragraphStyle: subtitleParagraph
        ]
        let normalText: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 13, weight: .regular),
            .foregroundColor: Palette.hexColor(with: "#666666") ?? #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1),
            .paragraphStyle: textParagraph
        ]
        
        // Resume Title
        let title = NSAttributedString(string: CreditPicPayLocalizable.dataBelowCorrect.text, attributes: superTitle)

        resume.append(title)
        resume.append(makePersonalData(subtitleAttributes: subTitle, textAttributes: normalText))
        resume.append(makebirthLocal(subtitleAttributes: subTitle, textAttributes: normalText))
        resume.append(makeAddressLocal(subtitleAttributes: subTitle, textAttributes: normalText))
        resume.append(makeIdentityDocumentsLocal(subtitleAttributes: subTitle, textAttributes: normalText))
        resume.append(makeAditionalDataLocal(subtitleAttributes: subTitle, textAttributes: normalText))
        return resume
    }
    
    func saveRegistration(_ completion: @escaping (Error?) -> Void) {
        creditForm.registrationStep = step
        apiCredit.setCreditRegistration(creditForm, isCompleted: true) { result in
            switch result {
            case .failure(let error):
                completion(error)
            case .success:
                completion(nil)
            }
        }
    }
    
    /// Make apersonal data
    ///
    /// - Parameters:
    ///   - subtitleAttributes: attributes for subtile text
    ///   - textAttributes: attributes for normal text
    /// - Returns: complete NSattributedString
    fileprivate func makePersonalData(subtitleAttributes: [NSAttributedString.Key: Any], textAttributes: [NSAttributedString.Key: Any]) -> NSAttributedString {
        var birthdayString: String = ""
        if let birthdayDate = DateFormatter.brazillianFormatter().date(from: self.creditForm.birthday) {
            birthdayString = DateFormatter.brazillianFormatter(dateFormat: "dd/MM/yyyy").string(from: birthdayDate)
        }
        var phoneStirng: String = ""
        if let ddd = creditForm.ddd, let phone = creditForm.phone {
            phoneStirng = CustomStringMask(mask: "(00) 00000-0000").maskedText(from: "\(ddd)\(phone)") ?? ""
        }
        let content = NSMutableAttributedString()
        content.append(NSAttributedString(string: "\(CreditPicPayLocalizable.personalData.text)\n", attributes: subtitleAttributes))
        content.append(NSAttributedString(string: "\(CreditPicPayLocalizable.name.text) \(creditForm.name)\n", attributes: textAttributes))
        if !creditForm.cpf.isEmpty {
            let range = NSRange(location: 0, length: 7)
            let cpfMask: String = (creditForm.cpf as NSString).replacingCharacters(in: range, with: "###.###")
            content.append(NSAttributedString(string: "CPF: \(cpfMask)\n", attributes: textAttributes))
        }
        content.append(NSAttributedString(string: "\(CreditPicPayLocalizable.motherName.text) \(creditForm.mothersName)\n", attributes: textAttributes))
        content.append(NSAttributedString(string: "\(CreditPicPayLocalizable.dateBirth.text) \(birthdayString)\n", attributes: textAttributes))
        content.append(NSAttributedString(string: "Email: \(creditForm.email)\n", attributes: textAttributes))
        content.append(NSAttributedString(string: "\(CreditPicPayLocalizable.telephone.text) \(phoneStirng)\n", attributes: textAttributes))
        return content
    }
    
    /// Make bith informations
    ///
    /// - Parameters:
    ///   - subtitleAttributes: attributes for subtile text
    ///   - textAttributes: attributes for normal text
    /// - Returns: complete NSattributedString
    fileprivate func makebirthLocal(subtitleAttributes: [NSAttributedString.Key: Any], textAttributes: [NSAttributedString.Key: Any]) -> NSAttributedString {
        let content = NSMutableAttributedString()
        content.append(NSAttributedString(string: "\(CreditPicPayLocalizable.birthplace.text)\n", attributes: subtitleAttributes))
        content.append(NSAttributedString(string: "\(creditForm.birthplaceCity), \(creditForm.birthplaceState), \(creditForm.birthplaceCountry)\n\n", attributes: textAttributes))
        return content
    }
    
    /// Make home address and commercial address
    ///
    /// - Parameters:
    ///   - subtitleAttributes: attributes for subtile text
    ///   - textAttributes: attributes for normal text
    /// - Returns: complete NSattributedString
    fileprivate func makeAddressLocal(subtitleAttributes: [NSAttributedString.Key: Any], textAttributes: [NSAttributedString.Key: Any]) -> NSAttributedString {
        let content = NSMutableAttributedString()
        var homeAddressString: String = ""
        if let homeAddress = creditForm.homeAddress {
            homeAddressString.append(CreditPicPayLocalizable.homeAddress.text)
            homeAddressString.append("\(homeAddress.street ?? ""), ")
            if !homeAddress.streetNumber.isEmpty && homeAddress.streetNumber != "0" {
                homeAddressString.append("\(homeAddress.streetNumber ?? ""), ")
            }
            if let complement = homeAddress.addressComplement {
                homeAddressString.append("\(complement), ")
            }
            homeAddressString.append("\(homeAddress.state ?? ""). ")
            homeAddressString.append("CEP: \(homeAddress.zipCode ?? "")\n\n")
        }
        
        let subtile = NSAttributedString(string: "\(CreditPicPayLocalizable.address.text)\n", attributes: subtitleAttributes)
        let home = NSAttributedString(string: homeAddressString, attributes: textAttributes)
        
        content.append(subtile)
        content.append(home)
        return content
    }
    
    /// Make documents information
    ///
    /// - Parameters:
    ///   - subtitleAttributes: attributes for subtile text
    ///   - textAttributes: attributes for normal text
    /// - Returns: complete NSattributedString
    fileprivate func makeIdentityDocumentsLocal(subtitleAttributes: [NSAttributedString.Key: Any], textAttributes: [NSAttributedString.Key: Any]) -> NSAttributedString {
        let content = NSMutableAttributedString()
        content.append(NSAttributedString(string: "\(CreditPicPayLocalizable.identityDocument.text)\n", attributes: subtitleAttributes))
        content.append(NSAttributedString(string: "\(creditForm.docTypeName): \(creditForm.docNumber)\n", attributes: textAttributes))
        content.append(NSAttributedString(string: "\(CreditPicPayLocalizable.issuer.text) \(creditForm.docExpeditionName)\n", attributes: textAttributes))
        if let docState = creditForm.docState, !docState.isEmpty {
            content.append(NSAttributedString(string: "\(CreditPicPayLocalizable.informStateBirth.text): \(docState)\n", attributes: textAttributes))
        }
        content.append(NSAttributedString(string: "\(CreditPicPayLocalizable.dateIssue.text) \(creditForm.emissionDate)\n\n", attributes: textAttributes))
        return content
    }
    
    /// Make adicional date for user
    ///
    /// - Parameters:
    ///   - subtitleAttributes: attributes for subtile text
    ///   - textAttributes: attributes for normal text
    /// - Returns: complete NSattributedString
    fileprivate func makeAditionalDataLocal(subtitleAttributes: [NSAttributedString.Key: Any], textAttributes: [NSAttributedString.Key: Any]) -> NSAttributedString {
        let content = NSMutableAttributedString()
        content.append(NSAttributedString(string: CreditPicPayLocalizable.additionalDataIssue.text, attributes: subtitleAttributes))
        content.append(NSAttributedString(string: "\(CreditPicPayLocalizable.monthlyIncome.text) \(CurrencyFormatter.brazillianRealString(from: NSNumber(value: creditForm.income ?? 0)) ?? "")\n", attributes: textAttributes))
        if let patrimony = creditForm.patrimony {
            content.append(NSAttributedString(string: "\(CreditPicPayLocalizable.patrimony.text) \(patrimony.minMaxValue)\n", attributes: textAttributes))
        }
        return content
    }
}
