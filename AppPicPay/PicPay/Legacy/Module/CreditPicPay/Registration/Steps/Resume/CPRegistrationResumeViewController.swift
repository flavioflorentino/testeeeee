import FeatureFlag
import UI
import UIKit

final class CPRegistrationResumeViewController: CreditBaseFormController {
    var isShowButtons: Bool = true
    var viewModel: CPRegistrationResumeViewModel?
    let container = DependencyContainer()
    
    @IBOutlet weak private var resumeLabel: UILabel!
    @IBOutlet weak private var correctButton: UIPPButton!
    @IBOutlet weak private var finishButton: UIPPButton!
    @IBOutlet weak private var buttonsStackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareLayout()
        title = CreditPicPayLocalizable.orderSummary.text
        progressView.isHidden = true
        configureButtonsConstainer()
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureForm()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParent {
            viewModel?.trackBackScreenEvent()
        }
    }
    
    @IBAction private func nextStepAction(_ sender: Any) {
        viewModel?.trackNextStepEvent()
        finishButton.isEnabled = false
        viewModel?.saveRegistration { [weak self] error in
            guard let strongSelf = self else {
                return
            }
            strongSelf.finishButton.isEnabled = true
            strongSelf.stopLoading()
            if let error = error {
                AlertMessage.showCustomAlertWithError(error, controller: strongSelf)
            } else {
                CPRegistrationCoordinator.shared?.didCompletedRegistration()
            }
        }
    }
    
    @IBAction private func editRegistration(_ sender: Any) {
        viewModel?.trackFixInfoEvent()
        let optionsActionSheet = UIAlertController(title: CreditPicPayLocalizable.correctInformation.text, message: nil, preferredStyle: .actionSheet)
        optionsActionSheet.addAction(UIAlertAction(title: CreditPicPayLocalizable.personalData.text, style: .default, handler: { [weak self] _ in
            guard let strongSelf = self, let resumeViewModel = strongSelf.viewModel else {
                return
            }
            resumeViewModel.trackSelectedOption(type: .personalInfo)
            let viewModel = CreditPersonalDataViewModel(with: resumeViewModel.creditForm, container: strongSelf.container)
            let controller = CreditPersonalDataViewController(with: viewModel)
            controller.isEditingForm = true
            self?.navigationController?.pushViewController(controller, animated: true)
        }))
        
        optionsActionSheet.addAction(UIAlertAction(title: CreditPicPayLocalizable.birthplace.text, style: .default, handler: { [weak self] _ in
            guard let strongSelf = self, let resumeViewModel = strongSelf.viewModel else {
                return
            }
            resumeViewModel.trackSelectedOption(type: .birthPlace)
            let viewModel = CreditBirthplaceViewModel(with: resumeViewModel.creditForm, isEditingForm: true, container: strongSelf.container)
            let controller = CreditBirthplaceViewController(with: viewModel)
            strongSelf.navigationController?.pushViewController(controller, animated: true)
        }))
        optionsActionSheet.addAction(UIAlertAction(title: CreditPicPayLocalizable.address.text, style: .default, handler: { [weak self] _ in
            guard let strongSelf = self, let resumeViewModel = strongSelf.viewModel else {
                return
            }
            resumeViewModel.trackSelectedOption(type: .personalAddress)
            let controller: OldCreditAddressViewController = UIStoryboard.viewController(.credit)
            controller.viewModel = CreditAddressViewModel(with: resumeViewModel.creditForm, container: strongSelf.container)
            controller.isEditingForm = true
            self?.navigationController?.pushViewController(controller, animated: true)
        }))
        optionsActionSheet.addAction(UIAlertAction(title: CreditPicPayLocalizable.identityDocument.text, style: .default, handler: { [weak self] _ in
            guard let strongSelf = self, let resumeViewModel = strongSelf.viewModel else {
                return
            }
            resumeViewModel.trackSelectedOption(type: .documentId)
            let controller: CreditRGViewController = UIStoryboard.viewController(.credit)
            controller.viewModel = CreditRGViewModel(with: resumeViewModel.creditForm, container: strongSelf.container)
            controller.isEditingForm = true
            self?.navigationController?.pushViewController(controller, animated: true)
        }))
        optionsActionSheet.addAction(UIAlertAction(title: CreditPicPayLocalizable.additionalData.text, style: .default, handler: { [weak self] _ in
            guard let self = self, let resumeViewModel = self.viewModel else {
                return
            }
            resumeViewModel.trackSelectedOption(type: .additionalInfo)            
            let controller = ProfissionalInfoFactory.make(creditForm: resumeViewModel.creditForm, isEditingForm: true)
            self.navigationController?.pushViewController(controller, animated: true)
        }))
        optionsActionSheet.addAction(UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil))
        
        self.present(optionsActionSheet, animated: true, completion: nil)
    }
    
    func configureButtonsConstainer() {
        guard !isShowButtons else {
            return
        }
        buttonsStackView.isHidden = false
        correctButton.isHidden = true
        finishButton.isHidden = true
        view.setNeedsLayout()
        view.layoutIfNeeded()
    }
    
    private func prepareLayout() {
        correctButton.setTitle(CreditPicPayLocalizable.fixRegistration.text, for: .normal)
        correctButton.normalTitleColor = Palette.white.color
        correctButton.normalBackgrounColor = Palette.ppColorGrayscale300.color
        correctButton.highlightedTitleColor = Palette.white.color
        correctButton.highlightedBackgrounColor = .clear
    }
    
    func configureForm() {
        resumeLabel.attributedText = viewModel?.getResume()
    }
}

private func format(zipCode: String?) -> String {
    if let zip = zipCode {
        if zip.length == 8 {
            let prefix = zip.chopSuffix(3)
            let sufix = zip.chopPrefix(5)
            return "\(prefix)-\(sufix)"
        }
    }
    return ""
}
