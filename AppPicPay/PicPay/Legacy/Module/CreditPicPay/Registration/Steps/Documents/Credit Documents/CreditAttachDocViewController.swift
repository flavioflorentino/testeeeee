import Card
import IdentityValidation
import UI
import UIKit

final class CreditAttachDocViewController: CreditBaseFormController {
    private let viewModel: CreditAttachDocViewModel
    private var identityValidationCoordinator: IDValidationCoordinatorProtocol?
    
    @IBOutlet weak private var collectionView: UICollectionView!
    @IBOutlet weak private var messageLabel: UILabel!
    @IBOutlet weak private var sendButton: UIPPButton!
    
    init(with viewModel: CreditAttachDocViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "CreditAttachDocViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareLayout()
        if viewModel.isReview {
            sendButton.setTitle(DefaultLocalizable.save.text, for: .normal)
        }
        viewModel.selectPhoto = { [weak self] destination in
            guard let self = self else {
                return
            }
            switch destination {
            case .alert(let alert):
                AlertMessage.showAlert(alert, controller: self)
            case .biometric(let indexPath):
                self.openBiometricScreen(indexPath: indexPath)
            case .takePhoto(let item, let indexPath):
                self.takePhoto(item: item, for: indexPath)
            }
        }
        viewModel.goToSelectMethod = { [weak self] item, indexPath in
            self?.takePhoto(item: item, for: indexPath)
        }
        configureCollectionView()
        showNextButton()
        loadDocumentsData()
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
    }
    
    func openBiometricScreen(indexPath: IndexPath) {
        if viewModel.biometricIsValid == true {
            let navigation = UINavigationController(rootViewController: BiometricStateFactory.make())
            self.present(navigation, animated: true)
            return
        }
        identityValidationCoordinator = IDValidationFlowCoordinator(
            from: self,
            token: User.token ?? "",
            flow: viewModel.originFlow,
            completion: { _ in
                self.viewModel.checkBiometricProccess({ [weak self] in
                    self?.collectionView.reloadItems(at: [indexPath])
                    self?.showNextButton()
                }, onError: { [weak self] error in
                    AlertMessage.showCustomAlertWithError(error, controller: self)
                })
            }
        )
        
        identityValidationCoordinator?.start()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard !viewModel.isReview else {
            return
        }
        
        if let coordinator = CPRegistrationCoordinator.shared {
            setStep(with: coordinator.getStepProgressForController(with: CPStep.documentFile))
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParent {
            viewModel.trackBackScreenEvent()
        }
    }
    
    @IBAction private func next(_ sender: Any) {
        guard validate(), !super.isFormEdition() else {
            return
        }
        viewModel.trackContinueEvent()
        guard viewModel.isReview else {
            viewModel.syncData()
            CPRegistrationCoordinator.shared?.pushController(with: viewModel.creditForm)
            return
        }
        
        startLoading()
        viewModel.syncData(isBackground: false, onSuccess: { [weak self] in
            self?.stopLoading()
            guard let creditForm = self?.viewModel.creditForm else {
                return
            }
            let controller = CreditAnalysisFactory.make(isLoan: creditForm.isLoanFlow)
            self?.navigationController?.pushViewController(controller, animated: true)
        }, onError: { [weak self] error in
            self?.stopLoading()
            AlertMessage.showCustomAlertWithError(error, controller: self)
        })
    }
}

extension CreditAttachDocViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.documentList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CreditPhotoAttachCollectionViewCell = collectionView.dequeueReusableCell(type: CreditPhotoAttachCollectionViewCell.self, forIndexPath: indexPath)
        let item = viewModel.documentList[indexPath.row]
        cell.configureCell(item: item, isReview: viewModel.isReview, alreadyBiometricVerified: viewModel.biometricIsValid)
        return cell
    }
}

// MARK: - UICollectionViewDelegate

extension CreditAttachDocViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.tapSelectPhoto(at: indexPath)
    }
}

extension CreditAttachDocViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellsAcross: CGFloat = 2.0
        let spaceBetweenCells: CGFloat = 3.0
        let width: CGFloat = (CGFloat(collectionView.bounds.width) - (cellsAcross - 1.0) * spaceBetweenCells) / cellsAcross as CGFloat
        return CGSize(width: width, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
}

extension CreditAttachDocViewController: UINavigationControllerDelegate {}

extension CreditAttachDocViewController {
    private func prepareLayout() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        sendButton.disabledTitleColor = Palette.ppColorGrayscale000.color
        sendButton.disabledBorderColor = Palette.ppColorGrayscale300.color
        sendButton.disabledBackgrounColor = Palette.ppColorGrayscale300.color
        sendButton.isEnabled = false
    }
    
    private func configureCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.registerCell(type: CreditPhotoAttachCollectionViewCell.self)
    }
    
    private func takePhoto(item: CreditDocumentItem, for indexPath: IndexPath) {
        let takePhotoController = CreditTakePhotoViewController(with: item)
        takePhotoController.modalPresentationStyle = .fullScreen
        modalPresentationCapturesStatusBarAppearance = true
        present(takePhotoController, animated: true, completion: nil)
        
        takePhotoController.onComplete = { [weak self] image in
            self?.dismiss(animated: true, completion: nil)
            self?.collectionView.reloadItems(at: [indexPath])
            self?.upload(image, in: indexPath)
        }
    }

    private func upload(_ image: UIImage, in indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? CreditPhotoAttachCollectionViewCell else {
            return
        }
        cell.imageThumbView.image = image
        cell.hasUploaded = false
        cell.icon.isHidden = true
        
        viewModel.setDocumentsToUpload(at: indexPath, image: image)
        viewModel.uploadDocument(image, at: indexPath, onProgressUpdate: { progress in
            cell.setProgress(to: Float(progress))
        }, onSuccess: { [weak self] in
            self?.showNextButton()
            self?.collectionView.reloadData()
        }, onError: { [weak self] error in
            AlertMessage.showCustomAlertWithError(error, controller: self)
        })
    }
    
    private func loadDocumentsData() {
        startLoadingView()
        viewModel.loadDocuments({ [weak self] in
            self?.stopLoadingView()
            self?.collectionView.reloadData()
            self?.showNextButton()
        }, onError: { [weak self] error in
            AlertMessage.showCustomAlertWithError(error, controller: self, completion: {
                self?.navigationController?.popViewController(animated: true)
            })
        })
    }
    
    private func showNextButton() {
        sendButton.isEnabled = viewModel.isValidForm
    }
    
    private func validate() -> Bool {
        collectionView.reloadData()
        messageLabel.isHidden = viewModel.isValidForm
        messageLabel.text = CreditPicPayLocalizable.requiredDocuments.text
        return viewModel.isValidForm
    }
}
