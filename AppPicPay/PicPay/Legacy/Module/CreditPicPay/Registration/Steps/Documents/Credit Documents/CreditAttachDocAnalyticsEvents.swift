import AnalyticsModule

struct CreditAttachDocAnalyticsEvents {
    let analytics: AnalyticsProtocol
    
    private var name: String {
        "PicPay Card - Request - Sign Up - Document Photo"
    }
    
    private var providers: [AnalyticsProvider] {
        [.firebase, .mixPanel]
    }
    
    func sendTapBackScreen() {
        sendEvent(action: "act-previous-step")
    }
    
    func sendTapContinue() {
        sendEvent(action: "act-next-step")
    }
    
    func sendTapDocument(documentType: CreditImageDocumentType) {
        switch documentType {
        case .documentFront:
            sendEvent(action: "act-take-document-front")
        case .documentBack:
            sendEvent(action: "act-take-document-back")
        case .selfie:
            sendEvent(action: "act-take-selfie")
        case .proofOfAddress:
            sendEvent(action: "act-take-proof-address")
        }
    }
    
    func sendEvent(action: String) {
        let event = AnalyticsEvent(name, properties: ["action": action], providers: providers)
        analytics.log(event)
    }
}
