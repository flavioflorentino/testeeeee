import AnalyticsModule
import Card
import FeatureFlag
import Foundation
import UI

final class CreditAttachDocViewModel: CreditBaseFormProtocol {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies
    
    enum Destination {
        case alert(Alert)
        case biometric(index: IndexPath)
        case takePhoto(CreditDocumentItem, IndexPath)
    }
    
    let api = ApiCredit()
    let creditForm: CreditForm
    let isReview: Bool
    var biometricIsValid: Bool?
    
    var step: CPStep = .documentFile
    var documentList: [CreditDocumentItem] = []
    var documentsToUpload: [CreditImageDocumentType: UIImage] = [:]
    var selectedIndexPath: IndexPath?
    var isValidForm: Bool {
        if canShowBiometricScreen() {
            return validateBiometric()
        }
        for item in documentList {
            guard let file = item.documentFile else {
                return false
            }
            if file.denied == true {
                return false
            }
        }
        return true
    }
    
    var originFlow: String {
        creditForm.isLoanFlow ? "LOAN" : "CARD"
    }
    
    func validateBiometric() -> Bool {
        let list = documentList.filter { $0.key != .selfie }

        for item in list {
            guard let file = item.documentFile else {
                return false
            }
            if file.denied == true {
                return false
            }
        }
        guard let isValid = biometricIsValid else {
            return false
        }
        return isValid
    }
    
    var selectPhoto: (Destination) -> Void = { _ in }
    var goToSelectMethod: ((CreditDocumentItem, IndexPath) -> Void)?
    private let analyticsContext: CreditAttachDocAnalyticsEvents
    
    init(with model: CreditForm, container: Dependencies, isReview: Bool = false) {
        self.creditForm = model
        self.isReview = isReview
        self.analyticsContext = CreditAttachDocAnalyticsEvents(analytics: container.analytics)
        self.dependencies = container
    }
    
    func trackBackScreenEvent() {
        analyticsContext.sendTapBackScreen()
    }
    
    func trackContinueEvent() {
        analyticsContext.sendTapContinue()
    }
    
    func trackTapDocumentType(documentType: CreditImageDocumentType) {
        analyticsContext.sendTapDocument(documentType: documentType)
    }
    
    func canShowBiometricScreen() -> Bool {
        creditForm.isLoanFlow
            ? dependencies.featureManager.isActive(.releaseFeatureLoanSelfie)
            : dependencies.featureManager.isActive(.featureCardBiometric)
    }
    
    func loadDocuments(_ onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        api.documentTypeForUser(credit: creditForm) { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case .success(let documents):
                self.documentList = documents
                self.mergeWithExistItens()
                onSuccess()
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    func checkBiometricProccess(_ onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        api.biometricProcess(credit: creditForm) { [weak self] result in
            switch result {
            case .success(let value):
                self?.biometricIsValid = value.identityAlreadyValidated
                onSuccess()
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    func uploadDocument(
        _ image: UIImage,
        at indexPath: IndexPath,
        onProgressUpdate: @escaping (Double) -> Void,
        onSuccess: @escaping () -> Void,
        onError: @escaping (Error) -> Void
    ) {
        guard documentList.indices.contains(indexPath.row) else {
            onError(PicPayError(message: CreditPicPayLocalizable.errorDocumentUpload.text))
            return
        }
        
        let documentItem = documentList[indexPath.row]
        let documentType = documentItem.key
        guard let imageForUpload = image.resizeImage(targetSize: CGSize(width: 1200, height: 1200)),
            let imageData = imageForUpload.jpegData(compressionQuality: 0.8) else {
                onError(PicPayError(message: CreditPicPayLocalizable.errorDocumentUpload.text))
                return
        }
        
        api.uploadDocument(imageData: imageData, type: documentType.rawValue, uploadProgress: { progress in
            onProgressUpdate(progress)
        }, completion: { [weak self] result in
            switch result {
            case .success(let document):
                guard let strongSelf = self else {
                    return
                }
                let userDocumentFiles = strongSelf.creditForm.userDocumentsFiles
                let containsDocument = userDocumentFiles.contains(document)
                
                if containsDocument, let index = userDocumentFiles.firstIndex(of: document) {
                    strongSelf.creditForm.userDocumentsFiles[index] = document
                } else {
                    strongSelf.creditForm.userDocumentsFiles.append(document)
                }
                
                strongSelf.mergeWithExistItens()
                
                onSuccess()
            case .failure(let error):
                onError(error)
            }
        })
    }
    
    func syncData(isBackground: Bool = true, onSuccess: @escaping (() -> Void) = {}, onError: @escaping ((Error) -> Void) = { _ in }) {
        guard !isBackground else {
            creditForm.registrationStep = step
            DispatchQueue.global(qos: .userInteractive).async { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.api.setCreditRegistration(strongSelf.creditForm) { _ in }
            }
            return
        }
        
        api.setCreditRegistration(self.creditForm, isCompleted: true) { result in
            switch result {
            case .success:
                onSuccess()
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    func item(at indexPath: IndexPath) -> CreditDocumentItem? {
        guard documentList.indices.contains(indexPath.row) else {
            return nil
        }
        return documentList[indexPath.row]
    }
    
    func tapSelectPhoto(at indexPath: IndexPath) {
        guard documentList.indices.contains(indexPath.row) else {
            return
        }
        selectedIndexPath = indexPath
        let item = documentList[indexPath.row]
        trackTapDocumentType(documentType: item.key)
        
        if canShowBiometricScreen(), item.key == .selfie {
            selectPhoto(.biometric(index: indexPath))
            return
        }
        
        /// Alert setup
        let cancelButton = Button(title: DefaultLocalizable.doItLater.text, type: .clean) { popupController, _ in
            popupController.dismiss(animated: true)
        }
        
        let buttonText = item.photo.gallery ? DefaultLocalizable.selectPhoto.text : DefaultLocalizable.takePhoto.text
        let button = Button(title: buttonText, type: .cta) { [weak self] popupController, _ in
            popupController.dismiss(animated: true, completion: {
                self?.goToSelectMethod?(item, indexPath)
            })
        }
        
        let image = Alert.Image(name: item.dialog.icon, source: .url)
        let title = NSAttributedString(string: item.dialog.title)
        var text = NSMutableAttributedString(string: item.dialog.text)
        
        if let file = documentList[indexPath.row].documentFile, let denied = file.denied, denied {
            if let reviewText = file.reviewMessage {
                text = NSMutableAttributedString(string: reviewText)
                text.addAttributes([.foregroundColor: Palette.ppColorNegative300.color], range: NSRange(location: 0, length: reviewText.count))
            }
        }
        
        let alert = Alert(title: title, text: text)
        alert.buttons = [button, cancelButton]
        alert.image = image
        alert.showCloseButton = true
        selectPhoto(.alert(alert))
    }
    
    func indexPath(of documentItem: CreditDocumentItem) -> IndexPath? {
        guard let row = documentList.firstIndex(of: documentItem) else {
            return nil
        }
        return IndexPath(row: row, section: 0)
    }
    
    func setDocumentsToUpload(at indexPath: IndexPath, image: UIImage) {
        guard documentList.indices.contains(indexPath.row) else {
            return
        }
        let item = documentList[indexPath.row]
        documentsToUpload[item.key] = image
    }
}

extension CreditAttachDocViewModel {
    private func mergeWithExistItens() {
        guard !creditForm.userDocumentsFiles.isEmpty else {
            return
        }
        var documentsItems: [CreditDocumentItem] = []
        
        for item in documentList {
            var documentItem = item
            for file in creditForm.userDocumentsFiles where item.key == file.documentType {
                if canShowBiometricScreen(), documentItem.key == .selfie {
                    self.biometricIsValid = documentItem.photo.identityAlreadyValidated
                    documentItem.documentFile = nil
                } else {
                    documentItem.documentFile = file
                }
                
            }
            documentsItems.append(documentItem)
        }
        
        documentList = documentsItems
    }
}
