import UIKit

final class CreditTakePhotoPreviewViewController: PPBaseViewController {
    private let imagePreview: UIImage?
    private let documentItem: CreditDocumentItem
    private let onComplete: ((_ image: UIImage) -> Void)?
    
    @IBOutlet weak var containerButtonsView: UIView!
    @IBOutlet weak var shotAgainContainerView: UIView!
    @IBOutlet weak var linkedContainerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.isHidden = documentItem.photo.confirmText.isEmpty
            descriptionLabel.text = documentItem.photo.confirmText
        }
    }
    
    init(with documentItem: CreditDocumentItem, and imagePreview: UIImage?, onComplete: ((_ image: UIImage) -> Void)?) {
        self.documentItem = documentItem
        self.imagePreview = imagePreview
        self.onComplete = onComplete
        super.init(nibName: "CreditTakePhotoPreviewViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let preview = imagePreview {
            imageView.image = preview
        }
        configureBlurInContainerButtons()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    // MARK: - User Actions
    
    @IBAction private func discardPhoto(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func usePhoto(_ sender: Any) {
        if let image = imagePreview {
            onComplete?(image)
        }
    }
}

extension CreditTakePhotoPreviewViewController {
    fileprivate func configureBlurInContainerButtons() {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        containerButtonsView.insertSubview(blurEffectView, at: 0)
        NSLayoutConstraint.activate(
            [
                blurEffectView.topAnchor.constraint(equalTo: containerButtonsView.topAnchor),
                blurEffectView.trailingAnchor.constraint(equalTo: containerButtonsView.trailingAnchor),
                blurEffectView.bottomAnchor.constraint(equalTo: containerButtonsView.bottomAnchor),
                blurEffectView.leadingAnchor.constraint(equalTo: containerButtonsView.leadingAnchor)
            ]
        )
        
        if #available(iOS 11.0, *) {
            let blurEffectViewSafeArea = UIVisualEffectView(effect: blurEffect)
            blurEffectViewSafeArea.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(blurEffectViewSafeArea)
            blurEffectViewSafeArea.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            blurEffectViewSafeArea.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
            blurEffectViewSafeArea.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
            blurEffectViewSafeArea.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        }
    }
}
