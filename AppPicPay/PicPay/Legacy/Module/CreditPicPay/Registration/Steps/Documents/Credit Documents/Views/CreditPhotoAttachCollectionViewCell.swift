import UI
import UIKit

final class CreditPhotoAttachCollectionViewCell: UICollectionViewCell {
    var hasUploaded: Bool = false
    private var progress: Float = 0.0
    private var isReview: Bool = false
    private var isDenied: Bool = false
    private var isBiometricVerified: Bool = false
    private var hasError: Bool = false {
        didSet {
            updateStatusUI()
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            updateStatusUI()
        }
    }
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var imageThumbView: UIImageView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var loadingImage: UIActivityIndicatorView!
    @IBOutlet weak var cameraImageView: UIImageView!
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        prepareLayout()
    }
    
    private func prepareLayout() {
        containerView.layer.cornerRadius = 5
        containerView.layer.borderWidth = 0
        containerView.layer.borderColor = containerView.backgroundColor?.cgColor
        containerView.backgroundColor = Palette.ppColorGrayscale200.color
        label.textColor = Palette.ppColorGrayscale600.color
        loadingImage.color = Palette.ppColorGrayscale600.color
        progressView.isHidden = true
    }
    
    func setProgress(to progress: Float) {
        progressView.isHidden = false
        progressView.progress = progress
        self.progress = progress
        if progress == 1 {
            progressView.isHidden = true
        } else {
            icon.isHidden = true
        }
    }
    
    func configureCell(item: CreditDocumentItem, isReview: Bool, alreadyBiometricVerified: Bool?) {
        self.isReview = isReview
        isBiometricVerified = false
        loadingImage.stopAnimating()
        label.text = item.description
        isDenied = item.documentFile?.denied ?? false
        imageThumbView.image = nil
        imageThumbView.backgroundColor = nil
        
        if let verified = alreadyBiometricVerified, verified, item.key == .selfie {
            isBiometricVerified = true
            imageThumbView.backgroundColor = Colors.success050.color
            imageThumbView.image = Assets.CreditPicPay.icoBiometricCard.image
            icon.image = Assets.CreditPicPay.Registration.iconRadiobuttonCheck.image
            icon.isHidden = false
            return
        }
        
        if let file = item.documentFile, !file.fileUrl.isEmpty && !isDenied {
            hasUploaded = true
            cameraImageView.image = nil
            loadingImage.isHidden = false
            loadingImage.startAnimating()
            imageThumbView.setImage(url: URL(string: file.fileUrl))
            hasError = file.denied ?? false
        } else {
            cameraImageView.image = #imageLiteral(resourceName: "camera_document")
            imageThumbView.image = nil
            hasUploaded = false
            hasError = false
        }
        updateStatusUI()
    }
    
    func updateStatusUI() {
        if hasError {
            containerView.layer.borderColor = UIColor.red.cgColor
            containerView.layer.borderWidth = 2
        } else if isHighlighted {
            containerView.layer.borderColor = Palette.ppColorBranding300.cgColor
            containerView.layer.borderWidth = 2
        } else {
            containerView.layer.borderColor = containerView.backgroundColor?.cgColor
            containerView.layer.borderWidth = 0
        }
        if isDenied {
            icon.image = #imageLiteral(resourceName: "changePhoto")
        } else {
            icon.image = #imageLiteral(resourceName: "iconRadiobuttonCheck")
        }
        icon.isHidden = !(hasUploaded || isDenied)
        if isBiometricVerified {
            icon.image = Assets.CreditPicPay.Registration.iconRadiobuttonCheck.image
            icon.isHidden = false
        }
    }
}
