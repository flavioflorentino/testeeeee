import AVFoundation
import PermissionsKit
import UI
import UIKit

final class CreditTakePhotoViewController: PPBaseViewController {
    private let documentItem: CreditDocumentItem
    private var cameraController: CameraController?
    private lazy var permissionInfoView: PermissionInfoView = {
        let view = PermissionInfoView(frame: UIScreen.main.bounds)
        view.imageView.image = #imageLiteral(resourceName: "ilu_permission_contact_location.png")
        view.textLabel.text = CreditPicPayLocalizable.cameraAccessForDocuments.text
        view.button.setTitle(CreditPicPayLocalizable.changeConfig.text, for: .normal)
        view.button.addTarget(self, action: #selector(requestCameraPermission), for: .touchUpInside)
        return view
    }()
    var onComplete: (_ image: UIImage) -> Void = { _ in }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    private lazy var blurViewEffectViewSafeArea: UIVisualEffectView = {
        let blueEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        blueEffectView.translatesAutoresizingMaskIntoConstraints = false
        return blueEffectView
    }()
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var cameraViewPlaceholder: UIView!
    @IBOutlet weak var takePhotoButton: UIPPButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var maskPlaceholder: UIView!
    @IBOutlet weak var maskUtilPlaceholder: UIView!
    @IBOutlet weak var maskUtilPlaceholderTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerButtonView: UIView!
    
    init(with documentItem: CreditDocumentItem) {
        self.documentItem = documentItem
        super.init(nibName: "CreditTakePhotoViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureButtonsBackgroundBlur()
        headerLabel.text = documentItem.photo.hint
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraController?.captureSession.stopRunning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkCameraPermission()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard documentItem.photo.masked else {
            return
        }
        addDocumentMask()
    }
    
    // MARK: - User Action
    
    @IBAction private func takePhoto(_ sender: Any) {
        self.cameraController?.captureImage(completion: { [weak self] image, _ in
            guard let self = self else {
                return
            }
            let imagePreview: UIImage?
            if self.documentItem.photo.selfie {
                guard let takedImage = image, let cgImage = takedImage.cgImage else {
                    return
                }
                let imageFront = UIImage(cgImage: cgImage, scale: takedImage.scale, orientation: .leftMirrored)
                imagePreview = imageFront
            } else {
                imagePreview = image
            }
            
            let controller = CreditTakePhotoPreviewViewController(with: self.documentItem, and: imagePreview, onComplete: self.onComplete)
            self.cameraController?.captureSession.stopRunning()
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction private func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Configuration
    
    private func configureButtonsBackgroundBlur() {
        guard documentItem.photo.selfie else {
            return
        }
        configureBlurContainerButtonView()
        configureBlurCloseButton()
    }
    
    private func configureCameraController() {
        blurViewEffectViewSafeArea.isHidden = false
        takePhotoButton.isEnabled = true
        removePermissionPlaceholder()
        
        if let cameraController = self.cameraController {
            cameraController.captureSession.startRunning()
        } else {
            cameraController = CameraController()
            cameraController?.currentCameraDevice?.focusMode = .continuousAutoFocus
            cameraController?.prepare { [weak self] error in
                do {
                    guard let strongSelf = self else {
                        return
                    }
                    try strongSelf.cameraController?.displayPreview(on: strongSelf.cameraViewPlaceholder)
                    
                    guard strongSelf.documentItem.photo.selfie else {
                        return
                    }
                    try self?.cameraController?.switchToFrontCamera()
                } catch {
                    AlertMessage.showCustomAlertWithError(error, controller: self)
                }
            }
        }
    }
    
    // MARK: - Permission Manager
    
    private func checkCameraPermission() {
        let permissionContact: Permission = PPPermission.camera()
        
        if permissionContact.status != .authorized {
            takePhotoButton.isEnabled = false
            view.insertSubview(permissionInfoView, belowSubview: closeButton)
            blurViewEffectViewSafeArea.isHidden = true
            requestCameraPermission()
        } else {
            configureCameraController()
        }
    }
    
    @objc(requestCamerraPermission)
    private func requestCameraPermission() {
        PPPermission.requestCamera({ [weak self] status in
            if status == .authorized {
                self?.configureCameraController()
            }
        })
    }
    
    private func removePermissionPlaceholder() {
        permissionInfoView.removeFromSuperview()
    }
}

extension CreditTakePhotoViewController {
    private func addDocumentMask() {
        let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        blurView.isUserInteractionEnabled = false
        blurView.translatesAutoresizingMaskIntoConstraints = false
        maskPlaceholder.addSubview(blurView)
        NSLayoutConstraint.activate([
            blurView.topAnchor.constraint(equalTo: maskPlaceholder.topAnchor),
            blurView.trailingAnchor.constraint(equalTo: maskPlaceholder.trailingAnchor),
            blurView.bottomAnchor.constraint(equalTo: maskPlaceholder.bottomAnchor),
            blurView.leadingAnchor.constraint(equalTo: maskPlaceholder.leadingAnchor)
        ])
        maskPlaceholder.layoutIfNeeded()
        setUpBorder(in: blurView)
    }
    
    private func setUpBorder(in blurView: UIView) {
        let path = UIBezierPath(roundedRect: blurView.frame, cornerRadius: 0)
        let midX = maskUtilPlaceholder.frame.size.width / 2
        let midY = maskUtilPlaceholder.frame.size.height / 2 + maskUtilPlaceholderTopConstraint.constant
        var width = maskUtilPlaceholder.frame.size.width - 80 // screen width - margins
        var height = width / 0.65
        
        if height > maskUtilPlaceholder.frame.size.height {
            height = maskUtilPlaceholder.frame.size.height
            width = height * 0.65
        }
        
        let topY = midY - height / 2
        let bottonY = midY + height / 2
        let points = [
            CGPoint(x: midX - (width / 2), y: topY),
            CGPoint(x: midX + (width / 2), y: topY),
            CGPoint(x: midX + (width / 2), y: bottonY),
            CGPoint(x: midX - (width / 2), y: bottonY)
        ]
        
        let rectPath = UIBezierPath()
        rectPath.move(to: points[0])
        rectPath.addLine(to: points[1])
        rectPath.addLine(to: points[2])
        rectPath.addLine(to: points[3])
        rectPath.close()
        
        path.append(rectPath)
        path.usesEvenOddFillRule = true
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        maskLayer.fillRule = .evenOdd
        
        let borderLayer = CAShapeLayer()
        borderLayer.path = rectPath.cgPath
        
        blurView.layer.addSublayer(borderLayer)
        blurView.layer.mask = maskLayer
        
        // Corners
        let borderPath = borderPathConfig(withPoints: points)
        
        let corner = CAShapeLayer()
        corner.strokeColor = Palette.ppColorBranding300.cgColor
        corner.path = borderPath.cgPath
        corner.lineWidth = 10.0
        corner.fillColor = UIColor.clear.cgColor
        
        maskPlaceholder.layer.addSublayer(corner)
        maskPlaceholder.layoutIfNeeded()
    }
    
    private func borderPathConfig(withPoints points: [CGPoint] ) -> UIBezierPath {
        let borderPath = UIBezierPath()
        borderPath.move(to: CGPoint(x: points[0].x + 20.0, y: points[0].y))
        borderPath.addLine(to: CGPoint(x: points[0].x, y: points[0].y))
        borderPath.addLine(to: CGPoint(x: points[0].x, y: points[0].y + 25.0))
        
        borderPath.move(to: CGPoint(x: points[1].x - 20.0, y: points[1].y))
        borderPath.addLine(to: CGPoint(x: points[1].x, y: points[1].y))
        borderPath.addLine(to: CGPoint(x: points[1].x, y: points[1].y + 25.0))
        
        borderPath.move(to: CGPoint(x: points[2].x - 20.0, y: points[2].y))
        borderPath.addLine(to: CGPoint(x: points[2].x, y: points[2].y))
        borderPath.addLine(to: CGPoint(x: points[2].x, y: points[2].y - 25.0))
        
        borderPath.move(to: CGPoint(x: points[3].x + 20.0, y: points[3].y))
        borderPath.addLine(to: CGPoint(x: points[3].x, y: points[3].y))
        borderPath.addLine(to: CGPoint(x: points[3].x, y: points[3].y - 25.0))
        return borderPath
    }
    
    private func configureBlurContainerButtonView() {
        let blurViewEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        blurViewEffectView.translatesAutoresizingMaskIntoConstraints = false
        containerButtonView.insertSubview(blurViewEffectView, at: 0)
        NSLayoutConstraint.activate([
                blurViewEffectView.topAnchor.constraint(equalTo: containerButtonView.topAnchor),
                blurViewEffectView.trailingAnchor.constraint(equalTo: containerButtonView.trailingAnchor),
                blurViewEffectView.bottomAnchor.constraint(equalTo: containerButtonView.bottomAnchor),
                blurViewEffectView.leadingAnchor.constraint(equalTo: containerButtonView.leadingAnchor)
        ])
        
        if #available(iOS 11.0, *) {
            view.addSubview(blurViewEffectViewSafeArea)
            blurViewEffectViewSafeArea.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            blurViewEffectViewSafeArea.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
            blurViewEffectViewSafeArea.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
            blurViewEffectViewSafeArea.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        }
    }
    
    private func configureBlurCloseButton() {
        let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        blurView.layer.cornerRadius = 0.5 * closeButton.bounds.size.width
        blurView.frame = closeButton.bounds
        blurView.clipsToBounds = true
        blurView.isUserInteractionEnabled = false
        closeButton.insertSubview(blurView, at: 0)
        
        if let imageView = closeButton.imageView {
            closeButton.bringSubviewToFront(imageView)
        }
    }
}
