import AnalyticsModule
import Card
import Foundation

final class CreditRGViewModel: CreditBaseFormProtocol {
    let api = ApiCredit()
    var container: HasAnalytics?
    var step: CPStep = .documentId
    var formModel: CreditForm
    var stateList: [AddressStates] = []
    var documentTypeList: [CreditDocumentType] = []
    var issuerOfficeList: [CreditIssuerOffices] = []
    var documentTypeSelected: CreditDocumentType? {
        didSet {
           updateIssuerOfficeList(documentType: documentTypeSelected)
        }
    }
    
    init(with model: CreditForm, container: DependencyContainer) {
        self.container = container
        self.formModel = model
    }
    
    func loadDocumentList(_ completion: @escaping (Error?) -> Void) {
        if !documentTypeList.isEmpty {
            completion(nil)
            return
        }
        api.documentTypes { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                switch result {
                case .success(let documentResult):
                    strongSelf.documentTypeList = documentResult.list
                    self?.updateIssuerOfficeList(documentType: self?.documentTypeSelected)
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }
    
    func viewWillPop() {
        container?.analytics.log(RequestCardEvent.idDocument(action: .previousStep, isLoan: formModel.isLoanFlow))
    }
    
    func sendDocumentAnalytics(with type: CreditDocumentType) {
        guard let document = RequestCardEvent.IDDocumentAction(document: type.name) else {
            return
        }
        container?.analytics.log(RequestCardEvent.idDocument(action: document, isLoan: formModel.isLoanFlow))
    }
    
    func loadStates(_ completion: @escaping (([ListSelectorViewModel.ListSelectorItem], Error?) -> Void)) {
        if stateList.isEmpty {
            api.creditAddressStates { [weak self] result in
                guard let strongSelf = self else {
                    return
                }
                DispatchQueue.main.async {
                    switch result {
                    case .success(let statesResult):
                        strongSelf.stateList = statesResult.list
                        completion(strongSelf.convertStateItem(), nil)
                    case .failure(let error):
                        completion([], error)
                    }
                }
            }
        } else {
            completion(convertStateItem(), nil)
        }
    }
    
    func syncData() {
        formModel.registrationStep = step
        container?.analytics.log(RequestCardEvent.idDocument(action: .nextStep, isLoan: formModel.isLoanFlow))
        DispatchQueue.global(qos: .userInteractive).async {
            let api = ApiCredit()
            api.setCreditRegistration(self.formModel) { _ in }
        }
    }
    
    func setDocumentTypeSelected(of id: Int) {
        guard let document = documentTypeList.first(where: { $0.id == id }) else {
            return
        }
        documentTypeSelected = document
    }
    
    func getState(by id: Int) -> AddressStates? {
        return stateList.first { $0.id == id }
    }
    
    private func updateIssuerOfficeList(documentType: CreditDocumentType?) {
        guard let documentTypeId = documentType?.id,
            let issuerOffice = documentTypeList.first(where: { $0.id == documentTypeId }) else {
            return
        }
        issuerOfficeList = issuerOffice.issuerOffices
    }
}

extension CreditRGViewModel {
    private func convertStateItem() -> [ListSelectorViewModel.ListSelectorItem] {
        return stateList.map { state -> ListSelectorViewModel.ListSelectorItem in
            return ListSelectorViewModel.ListSelectorItem(with: "\(state.id)", and: state.name)
        }
    }
}
