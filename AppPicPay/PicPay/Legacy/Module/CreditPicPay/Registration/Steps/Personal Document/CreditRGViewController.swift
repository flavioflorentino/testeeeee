import UI
import UIKit
import Validator

final class CreditRGViewController: CreditBaseFormController {
    var viewModel: CreditRGViewModel?
    //Constants
    private let documentNumberMaxLength: Int = 20
    private let documentDateDayMinLength = 10
    
    @IBOutlet private weak var stateTextField: UIPPFloatingTextField!
    @IBOutlet private weak var docTypeTextField: UIPPFloatingTextField!
    @IBOutlet private weak var docNumberTextField: UIPPFloatingTextField!
    @IBOutlet private weak var expeditionTextField: UIPPFloatingTextField!
    @IBOutlet private weak var expeditionBtn: UIButton!
    @IBOutlet private weak var nextBtn: UIPPButton!
    @IBOutlet private weak var documentIssuerActivityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var emissionDateTextField: UIPPFloatingTextField!
    @IBOutlet private weak var stateContainerView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureForm()
        populateForm()
        if self.isEditingForm {
            nextBtn.setTitle(DefaultLocalizable.save.text, for: .normal)
        }
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let coordinator = CPRegistrationCoordinator.shared {
            setStep(with: coordinator.getStepProgressForController(with: CPStep.documentId))
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if isMovingFromParent {
            viewModel?.viewWillPop()
        }
    }
    
    @IBAction private func documentTypeAction(_ sender: Any) {
        selectDocumentType()
    }

    @IBAction private func documentIssuer(_ sender: Any) {
        showActionSheetForSelectionIssuerOfficer()
    }

    @IBAction private func stateAction(_ sender: Any) {
        pushListSelectorViewController()
    }
    
    @IBAction private func next(_ sender: Any) {
        guard validate().isValid else {
            return
        }
        saveFormData()
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.isEqual(docNumberTextField) {
            return validateDocumentNumberTextField(textField: textField, inRange: range, replacementeString: string)
        } else if textField.isEqual(emissionDateTextField) {
            return validateDocumentDate(textField: textField, inRange: range, replacementeString: string)
        } else {
            return true
        }
    }
    
    private func validateDocumentNumberTextField(textField field: UITextField, inRange range: NSRange, replacementeString string: String) -> Bool {
        let hasReachMaxCharacterCount = (field.text?.count > documentNumberMaxLength - 1)
        let isAddingCharacter = string.count > range.length
        let shouldChange = !(hasReachMaxCharacterCount && isAddingCharacter)
        return shouldChange
    }
    
    private func validateDocumentDate(textField field: UITextField, inRange range: NSRange, replacementeString string: String) -> Bool {
        if (field.text?.count == 2) || (field.text?.count == 5) {
            // backspace
            if !string.isEmpty {
                field.text = (field.text)! + "/"
            }
        }
        // verify field length
        return !(field.text?.count > documentDateDayMinLength - 1 && (string.count) > range.length)
    }
}

extension CreditRGViewController: ListSelectorTableViewDelegate {
    func selectedItem(_ id: String, description: String) {
        guard let state = viewModel?.getState(by: Int(id) ?? 0) else {
            return
        }
        stateTextField.text = state.initials
        stateTextField.errorMessage = nil
        viewModel?.formModel.docState = state.initials
    }
    
    func loadList(_ completion: @escaping ([ListSelectorViewModel.ListSelectorItem], Error?) -> Void) {
        viewModel?.loadStates(completion)
    }
}

extension CreditRGViewController {
    fileprivate enum ValidateError: String, Error, CustomStringConvertible, ValidationError {
        case stateRequired = "Estado é obrigatório"
        case docTypeRequired = "Tipo de Documento é obrigatório"
        case docNumberRequired = "Número do Documento é obrigatório"
        case expeditionRequired = "Órgão expeditor é obrigatório"
        case emissionDateRequired = "Data de emissão é obrigatório"
        case emissionDateValid = "Data inválida"
        
        var description: String {
            return self.rawValue
        }
        
        var message: String {
            return self.rawValue
        }
    }
    
    /// Request document type list
    fileprivate func selectDocumentType(showIssuerOffice: Bool = false) {
        showLoadingIssuerOfficers()
        
        viewModel?.loadDocumentList { [weak self] error in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideLoadingIssuerOfficers()
            
            if let error = error {
                AlertMessage.showCustomAlertWithError(error, controller: strongSelf)
            } else {
                if showIssuerOffice {
                    /// Set docyent type object for id
                    guard let documentTypeId = strongSelf.viewModel?.formModel.docTypeId else {
                        return
                    }
                    strongSelf.viewModel?.setDocumentTypeSelected(of: documentTypeId)
                    strongSelf.showActionSheetForSelectionIssuerOfficer()
                } else {
                    strongSelf.showActionSheetSelectDocumentType()
                }
            }
        }
    }
    
    private func showLoadingIssuerOfficers() {
        documentIssuerActivityIndicator.isHidden = false
        documentIssuerActivityIndicator.startAnimating()
        expeditionTextField.isEnabled = false
        expeditionBtn.isUserInteractionEnabled = false
    }
    
    private func hideLoadingIssuerOfficers() {
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.documentIssuerActivityIndicator.alpha = 0
        }, completion: { [weak self] _ in
            self?.documentIssuerActivityIndicator.isHidden = true
        })
        
        expeditionTextField.isEnabled = true
        expeditionBtn.isUserInteractionEnabled = true
    }
    
    /// Show action sheet with list of document types
    internal func showActionSheetSelectDocumentType() {
        let actionSheet = UIAlertController(title: CreditPicPayLocalizable.chooseTypeDocument.text, message: nil, preferredStyle: .actionSheet)
        if let documentTypeList = viewModel?.documentTypeList {
            for type in documentTypeList {
                let action = UIAlertAction(title: type.name, style: .default) { _ in
                    self.selectCreditDocument(with: type)
                    self.sendDocumentAnalytics(with: type)
                }
                actionSheet.addAction(action)
            }
        }
        let cancelAction = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil)
        actionSheet.addAction(cancelAction)
        present(actionSheet, animated: true, completion: nil)
    }
    
    private func sendDocumentAnalytics(with type: CreditDocumentType) {
        viewModel?.sendDocumentAnalytics(with: type)
    }
    
    /// SHow action sheet with list of issuer offices
    fileprivate func showActionSheetForSelectionIssuerOfficer() {
        guard viewModel?.documentTypeSelected != nil else {
            selectDocumentType(showIssuerOffice: true)
            return
        }
        let actionSheet = UIAlertController(title: CreditPicPayLocalizable.chooseIssuingOrgan.text, message: nil, preferredStyle: .actionSheet)
        if let issuerOfficeList = viewModel?.issuerOfficeList {
            for issuer in issuerOfficeList {
                let action = UIAlertAction(title: issuer.name, style: .default) { _ in
                    self.selectIssuerOfficer(with: issuer)
                }
                actionSheet.addAction(action)
            }
        }
        let cancelAction = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil)
        actionSheet.addAction(cancelAction)
        present(actionSheet, animated: true, completion: nil)
    }
    
    /// Select issuer office for register of credit
    ///
    /// - Parameter issuer: object issuer office
    internal func selectIssuerOfficer(with issuer: CreditIssuerOffices) {
        expeditionTextField.text = issuer.name
        expeditionTextField.errorMessage = nil
        viewModel?.formModel.docExpeditionId = issuer.id
        viewModel?.formModel.docExpeditionName = issuer.name ?? ""
    }
    
    /// select document type for register of credit and set list of issuer offices
    ///
    /// - Parameter type: type of document selected
    internal func selectCreditDocument(with type: CreditDocumentType) {
        if let docType = viewModel?.formModel.docTypeId, docType == type.id {
            configDocumentType(documentType: type)
            return
        }
        configDocumentType(documentType: type)
        expeditionTextField.text = ""
        expeditionTextField.errorMessage = nil
        viewModel?.formModel.docExpeditionId = nil
        viewModel?.formModel.docExpeditionName = ""
        
        configureStateField()
    }
    
    private func configDocumentType(documentType type: CreditDocumentType) {
        viewModel?.documentTypeSelected = type
        docTypeTextField.text = type.name
        docTypeTextField.errorMessage = nil
        expeditionBtn.isEnabled = true
        viewModel?.documentTypeSelected = type
        viewModel?.formModel.docTypeId = type.id
        viewModel?.formModel.docTypeName = type.name
        viewModel?.formModel.docType = type
    }
    
    // MARK: - Configuration
    
    fileprivate func configureForm() {
        addTextField(stateTextField)
        addTextField(docTypeTextField)
        addTextField(docNumberTextField)
        addTextField(expeditionTextField)
        addTextField(emissionDateTextField)
        configTextFieldsInpuAccessory()
        configureValidations()
        expeditionBtn.isEnabled = false
    }
    
    private func configTextFieldsInpuAccessory() {
        let doneToolBar = DoneToolBar(doneText: "Ok")
        docNumberTextField.inputAccessoryView = doneToolBar
        emissionDateTextField.inputAccessoryView = doneToolBar
    }
    
    fileprivate func configureValidations() {
        var docTypeRules = ValidationRuleSet<String>()
        docTypeRules.add(rule: ValidationRuleLength(min: 1, error: ValidateError.docTypeRequired))
        docTypeTextField.validationRules = docTypeRules
        
        var docNumberRules = ValidationRuleSet<String>()
        docNumberRules.add(rule: ValidationRuleLength(min: 3, error: ValidateError.docNumberRequired))
        docNumberTextField.validationRules = docNumberRules
        
        var expeditionRules = ValidationRuleSet<String>()
        expeditionRules.add(rule: ValidationRuleLength(min: 3, error: ValidateError.expeditionRequired))
        expeditionTextField.validationRules = expeditionRules
        
        var emissionDateRules = ValidationRuleSet<String>()
        emissionDateRules.add(rule: ValidationRuleLength(min: 10, error: ValidateError.emissionDateRequired))
        let conditionRule = ValidationRuleCondition<String>(error: ValidateError.emissionDateValid) { dateString -> Bool in
            guard let dateString = dateString else {
                return false
            }
            if let date = DateFormatter.brazillianFormatter(dateFormat: "dd/MM/yyyy").date(from: dateString) {
                guard date < Date() else {
                    return date.compare(Date()) == .orderedAscending
                }
                return true
            }
            return false
        }
        emissionDateRules.add(rule: conditionRule)
        emissionDateTextField.validationRules = emissionDateRules
        emissionDateTextField.maskString = "00/00/0000"
    }
    
    // MARK: - Form Methods
    
    fileprivate func populateForm() {
        if let docState = viewModel?.formModel.docState {
            stateContainerView.isHidden = false
            stateTextField.text = docState
        } else if viewModel?.formModel.docTypeId != nil {
            stateContainerView.isHidden = false
        } else {
            stateContainerView.isHidden = true
        }
        
        docTypeTextField.text = viewModel?.formModel.docTypeName
        docNumberTextField.text = viewModel?.formModel.docNumber
        expeditionTextField.text = viewModel?.formModel.docExpeditionName
        emissionDateTextField.text = viewModel?.formModel.emissionDate
        viewModel?.documentTypeSelected = viewModel?.formModel.docType
        
        expeditionBtn.isEnabled = viewModel?.formModel.docTypeId != nil
        
        showLoadingIssuerOfficers()
        viewModel?.loadDocumentList { [weak self] _ in
            self?.hideLoadingIssuerOfficers()
        }
    }
    
    fileprivate func saveFormData() {
        viewModel?.formModel.docState = stateTextField.text
        viewModel?.formModel.docNumber = docNumberTextField.text ?? ""
        viewModel?.formModel.docExpedition?.name = expeditionTextField.text ?? ""
        viewModel?.formModel.emissionDate = emissionDateTextField.text ?? ""
        viewModel?.syncData()
        
        guard !self.isFormEdition(), let formModel = viewModel?.formModel else {
            return
        }
        
        CPRegistrationCoordinator.shared?.pushController(with: formModel)
    }
    
    fileprivate func pushListSelectorViewController() {
        let controller = ListSelectorTableViewController.instantiate()
        controller.delegate = self
        navigationController?.pushViewController(controller, animated: true)
    }
    
    fileprivate func configureStateField() {
        guard let type = viewModel?.documentTypeSelected else {
            return
        }
        if type.needEmitterState {
            addTextField(stateTextField)
            stateContainerView.isHidden = false
            addStateValidationRules()
            viewModel?.formModel.docState = stateTextField.text
        } else {
            stateTextField.text = nil
            viewModel?.formModel.docState = nil
            removeTextField(stateTextField)
            stateContainerView.isHidden = true
            stateTextField.validationRules = nil
        }
    }
    
    fileprivate func addStateValidationRules() {
        var stateRules = ValidationRuleSet<String>()
        stateRules.add(rule: ValidationRuleLength(min: 2, error: ValidateError.stateRequired))
        stateTextField.validationRules = stateRules
    }
}
