import Core
import Foundation

protocol DocumentscopyServicing {
    func documentscopyProcess(completion: @escaping BiometricProccess)
    func setCreditRegistration(creditForm: CreditForm, isCompleted: Bool, completion: @escaping (Result<Void, Error>) -> Void)
}

final class DocumentscopyService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    private let api = ApiCredit()
    
    lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DocumentscopyServicing
extension DocumentscopyService: DocumentscopyServicing {
    func documentscopyProcess(completion: @escaping BiometricProccess) {
        let api = Api<CardBiometricProcess>(endpoint: CreditServiceEndpoint.biometricProccess(isLoanFlow: false))
        api.execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
    
    func setCreditRegistration(creditForm: CreditForm, isCompleted: Bool, completion: @escaping (Result<Void, Error>) -> Void) {
        api.setCreditRegistration(creditForm, isCompleted: isCompleted) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success:
                    completion(.success)
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
