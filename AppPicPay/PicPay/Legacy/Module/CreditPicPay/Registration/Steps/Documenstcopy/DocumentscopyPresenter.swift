import AssetsKit
import Foundation

protocol DocumentscopyPresenting: AnyObject {
    var viewController: DocumentscopyDisplaying? { get set }
    func presentScreen(proccessSuccess: Bool)
    func presentLoading(enable: Bool)
    func presentError(_ error: Error)
    func presentDocumentscopy()
    func didNextStep(action: DocumentscopyAction)
}

final class DocumentscopyPresenter {
    typealias Dependencies = HasCreditCoordinatorManager
    private let dependencies: Dependencies

    private let isReview: Bool
    private let coordinator: DocumentscopyCoordinating
    weak var viewController: DocumentscopyDisplaying?

    init(isReview: Bool, coordinator: DocumentscopyCoordinating, dependencies: Dependencies) {
        self.isReview = isReview
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - DocumentscopyPresenting
extension DocumentscopyPresenter: DocumentscopyPresenting {
    func presentScreen(proccessSuccess: Bool) {
        let step = dependencies.registrationCoordinator?.getStepProgressForController(with: .documentFile) ?? 0
        let buttonTitle = proccessSuccess ? CreditPicPayLocalizable.documentsAvailableButtonTitle.text:
            CreditPicPayLocalizable.documentsButtonTitle.text
        if isReview && !proccessSuccess {
            viewController?.displayScreen(image: Resources.Illustrations.iluPendingReview.image,
                                          title: CreditPicPayLocalizable.isReviewTitle.text,
                                          description: CreditPicPayLocalizable.isReviewDescription.text,
                                          buttonTitle: buttonTitle,
                                          step: step,
                                          hiddenLaterButton: proccessSuccess)
            return
        }
        let title = proccessSuccess ? CreditPicPayLocalizable.documentsAvailableTitle.text:
            CreditPicPayLocalizable.documentsTitle.text
        let description = proccessSuccess ? CreditPicPayLocalizable.documentsAvailableDescription.text:
            CreditPicPayLocalizable.documentsDescription.text
        viewController?.displayScreen(image: Resources.Illustrations.iluDocumentscopy.image,
                                      title: title,
                                      description: description,
                                      buttonTitle: buttonTitle,
                                      step: step,
                                      hiddenLaterButton: proccessSuccess)
    }
    
    func presentLoading(enable: Bool) {
        viewController?.displayLoading(enable: enable)
    }
    
    func presentError(_ error: Error) {
        viewController?.displayError(error)
    }
    
    func presentDocumentscopy() {
        viewController?.displayDocumentscopy()
    }
    
    func didNextStep(action: DocumentscopyAction) {
        coordinator.perform(action: action)
    }
}

