import Card
import UIKit
import UI

protocol DocumentscopyRootViewDelegate: AnyObject {
    func confirm()
    func dismiss()
}

private extension DocumentscopyRootView.Layout {
    enum Size {
        static let progressHeight: Double = 3
        static let dismissButtonHeight = Spacing.base03
    }
    enum Color {
        // todo O fluxo do crédito esta utilizando o estilo de cor antigo (Palette)
        static let progress = Colors.branding400.color
        static let background = Palette.ppColorGrayscale000.color
    }
}

final class DocumentscopyRootView: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    weak var delegate: DocumentscopyRootViewDelegate?
    
    private lazy var progressView: UIProgressView = {
        let progressView = UIProgressView()
        // todo: para atualizar essa cor deverá atualizar todo o fluxo do crédito
        progressView.progressTintColor = Layout.Color.progress
        progressView.trackTintColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
        progressView.progress = 0.0
        return progressView
    }()
    
    private lazy var infoImageview = InfoImageView()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(nextPressed), for: .touchUpInside)
        return button
    }()
    
    private lazy var laterDismissButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(dismissPressed), for: .touchUpInside)
        button.setTitle(CreditPicPayLocalizable.laterDismissTitle.text, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        return button
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base02
        stackView.distribution = .equalSpacing
        stackView.axis = .vertical
        stackView.addArrangedSubviews(confirmButton, laterDismissButton)
        return stackView
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal func buildViewHierarchy() {
        addSubviews(progressView, infoImageview, stackView)
    }
    
    internal func setupConstraints() {
        progressView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Size.progressHeight)
        }
        
        infoImageview.snp.makeConstraints {
            $0.top.equalTo(progressView.snp.bottom).offset(Spacing.base06)
            $0.centerX.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        stackView.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(infoImageview.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(compatibleSafeArea.bottom).inset(Spacing.base02)
        }
        
        laterDismissButton.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.dismissButtonHeight)
        }
    }
    
    internal func configureViews() {
        backgroundColor = Layout.Color.background
    }
    
    func displayScreen(image: UIImage, title: String, description: String, buttonTitle: String, step: Float, hiddenLaterButton: Bool) {
        progressView.setProgress(step, animated: false)
        infoImageview.setupView(image: image,
                                title: title,
                                description: description)
        confirmButton.setTitle(buttonTitle, for: .normal)
        laterDismissButton.isHidden = hiddenLaterButton
    }
    
    @objc
    private func nextPressed() {
        delegate?.confirm()
    }
    
    @objc
    private func dismissPressed() {
        delegate?.dismiss()
    }
}
