import AssetsKit
import Card
import IdentityValidation
import UI
import UIKit

protocol DocumentscopyDisplaying: AnyObject {
    func displayScreen(image: UIImage, title: String, description: String, buttonTitle: String, step: Float, hiddenLaterButton: Bool)
    func displayDocumentscopy()
    func displayLoading(enable: Bool)
    func displayError(_ error: Error)
}

final class DocumentscopyViewController: ViewController<DocumentscopyInteracting, DocumentscopyRootView> {
    private var identityValidationCoordinator: IDValidationFlowCoordinator?
    private let originFlow = "CARD"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.checkProcess()
    }

    override func configureViews() {
        title = CreditPicPayLocalizable.documentsScreenTitle.text
        rootView.delegate = self
    }
}

// MARK: - DocumentscopyRootViewDelegate
extension DocumentscopyViewController: DocumentscopyRootViewDelegate {
    func confirm() {
        interactor.confirm()
    }
    
    func dismiss() {
        interactor.dismiss()
    }
}

// MARK: - DocumentscopyDisplaying
extension DocumentscopyViewController: DocumentscopyDisplaying {
    func displayScreen(image: UIImage, title: String, description: String, buttonTitle: String, step: Float, hiddenLaterButton: Bool) {
        rootView.displayScreen(image: image,
                               title: title,
                               description: description,
                               buttonTitle: buttonTitle,
                               step: step,
                               hiddenLaterButton: hiddenLaterButton)
    }
    
    func displayLoading(enable: Bool) {
        enable ? beginState(): endState()
    }
    
    func displayError(_ error: Error) {
        endState()
        AlertMessage.showAlert(error, controller: self, completion: {
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    func displayDocumentscopy() {
        beginState()
        identityValidationCoordinator = IDValidationFlowCoordinator(
            from: self,
            token: User.token ?? "",
            flow: originFlow,
            completion: { [weak self] _ in
                self?.interactor.checkProcess()
            }
        )
        
        identityValidationCoordinator?.start()
    }
}

extension DocumentscopyViewController: StatefulProviding { }
