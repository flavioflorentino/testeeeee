import UIKit
import Card

enum DocumentscopyAction {
    case confirm(creditForm: CreditForm)
    case analysis(creditForm: CreditForm)
    case dismiss
}

protocol DocumentscopyCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: DocumentscopyAction)
}

final class DocumentscopyCoordinator {
    typealias Dependencies = HasCreditCoordinatorManager
    private let dependencies: Dependencies

    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DocumentscopyCoordinating
extension DocumentscopyCoordinator: DocumentscopyCoordinating {
    func perform(action: DocumentscopyAction) {
        switch action {
        case let .confirm(creditForm):
            dependencies.registrationCoordinator?.pushController(with: creditForm)
        case let .analysis(creditForm):
            let controller = CreditAnalysisFactory.make(isLoan: creditForm.isLoanFlow)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .dismiss:
            viewController?.navigationController?.dismiss(animated: true)
        }
    }
}
