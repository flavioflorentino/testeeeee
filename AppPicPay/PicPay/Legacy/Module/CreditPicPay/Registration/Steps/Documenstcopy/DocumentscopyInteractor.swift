import Foundation

protocol DocumentscopyInteracting: AnyObject {
    func confirm()
    func checkProcess()
    func dismiss()
}

final class DocumentscopyInteractor {
    private var creditForm: CreditForm
    private var identityAlreadyValidated: Bool?
    private let service: DocumentscopyServicing
    private let presenter: DocumentscopyPresenting
    var isReview: Bool

    init(service: DocumentscopyServicing, presenter: DocumentscopyPresenting, creditForm: CreditForm, isReview: Bool) {
        self.service = service
        self.presenter = presenter
        self.creditForm = creditForm
        self.isReview = isReview
    }
    
    private func syncData() {
        creditForm.registrationStep = .documentFile
        service.setCreditRegistration(creditForm: creditForm, isCompleted: false) { _ in }
    }
    
    private func pendingDocumentsSend() {
        presenter.presentLoading(enable: true)
        service.setCreditRegistration(creditForm: creditForm, isCompleted: true) { [weak self] result in
            guard let self = self else { return }
            self.presenter.presentLoading(enable: false)
            switch result {
            case .success:
                self.presenter.didNextStep(action: .analysis(creditForm: self.creditForm))
            case let .failure(error):
                self.presenter.presentError(error)
            }            
        }
    }
}

// MARK: - DocumentscopyInteracting
extension DocumentscopyInteractor: DocumentscopyInteracting {
    func confirm() {
        guard let alreadyValidation = identityAlreadyValidated, alreadyValidation else {
            presenter.presentDocumentscopy()
            return
        }
        if !isReview {
            syncData()
            presenter.didNextStep(action: .confirm(creditForm: creditForm))
        } else {
            pendingDocumentsSend()
        }
    }
    
    func checkProcess() {
        presenter.presentLoading(enable: true)
        service.documentscopyProcess { [weak self] result in
            switch result {
            case let .success(proccess):
                self?.identityAlreadyValidated = proccess.identityAlreadyValidated
                self?.presenter.presentLoading(enable: false)
                self?.presenter.presentScreen(proccessSuccess: proccess.identityAlreadyValidated)
            case let .failure(error):
                self?.presenter.presentError(error)
            }
        }
    }
    
    func dismiss() {
        presenter.didNextStep(action: .dismiss)
    }
}
