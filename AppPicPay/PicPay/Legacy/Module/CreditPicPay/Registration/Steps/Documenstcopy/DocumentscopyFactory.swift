import UIKit

enum DocumentscopyFactory {
    static func make(creditForm: CreditForm, isReviewFlow: Bool) -> DocumentscopyViewController {
        let container = DependencyContainer()
        let service: DocumentscopyServicing = DocumentscopyService(dependencies: container)
        let coordinator: DocumentscopyCoordinating = DocumentscopyCoordinator(dependencies: container)
        let presenter: DocumentscopyPresenting = DocumentscopyPresenter(isReview: isReviewFlow,
                                                                        coordinator: coordinator,
                                                                        dependencies: container)
        let interactor = DocumentscopyInteractor(service: service,
                                                 presenter: presenter,
                                                 creditForm: creditForm,
                                                 isReview: isReviewFlow)
        let viewController = DocumentscopyViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
