import Foundation

protocol CPRegistrationViewModel {
    var step: String { get }
    var creditForm: CreditForm { get set }
}
