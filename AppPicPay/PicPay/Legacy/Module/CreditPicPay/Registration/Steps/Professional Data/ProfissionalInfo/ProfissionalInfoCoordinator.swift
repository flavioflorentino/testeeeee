import UIKit

enum ProfissionalInfoAction {
    case nextStep(_ creditForm: CreditForm)
    case close
}

protocol ProfissionalInfoCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ProfissionalInfoAction)
}

final class ProfissionalInfoCoordinator {
    typealias Dependencies = HasCreditCoordinatorManager
    private let dependencies: Dependencies
    
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ProfissionalInfoCoordinating
extension ProfissionalInfoCoordinator: ProfissionalInfoCoordinating {
    func perform(action: ProfissionalInfoAction) {
        switch action {
        case .nextStep(let creditForm):
            dependencies.registrationCoordinator?.pushController(with: creditForm)
        case .close:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}
