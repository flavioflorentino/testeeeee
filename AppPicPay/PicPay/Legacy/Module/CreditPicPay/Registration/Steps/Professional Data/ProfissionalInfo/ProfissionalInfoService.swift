import Core
import Foundation

typealias CompletionGetProfessionalData = (Result<ApiCredit.PatrimonyResponse, Error>) -> Void

protocol ProfissionalInfoServicing {
    func loadListPatrimonies(completion: @escaping CompletionGetProfessionalData)
    func setCreditRegistration(creditForm: CreditForm)
}

final class ProfissionalInfoService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private let api = ApiCredit()
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ProfissionalInfoServicing
extension ProfissionalInfoService: ProfissionalInfoServicing {
    func loadListPatrimonies(completion: @escaping CompletionGetProfessionalData) {
        api.patrimonies { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result
                    .mapError { $0 as Error }
                    .map { $0 }
                completion(mappedResult)
            }
        }
    }
    
    func setCreditRegistration(creditForm: CreditForm) {
        api.setCreditRegistration(creditForm)
    }
}
