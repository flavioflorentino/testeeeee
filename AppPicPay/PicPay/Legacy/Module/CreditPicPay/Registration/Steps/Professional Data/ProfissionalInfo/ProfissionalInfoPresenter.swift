import Foundation

protocol ProfissionalInfoPresenting: AnyObject {
    var viewController: ProfissionalInfoDisplay? { get set }
    func setupProgressView(value: Float)
    func didNextStep(creditForm: CreditForm)
    func setupInitialForm(creditForm: CreditForm)
    func presentIncomeError(message: String?)
    func enableContinueButton(enable: Bool)
}

final class ProfissionalInfoPresenter {
    private let coordinator: ProfissionalInfoCoordinating
    weak var viewController: ProfissionalInfoDisplay?

    private let isEditingForm: Bool
    
    init(coordinator: ProfissionalInfoCoordinating, isEditingForm: Bool) {
        self.coordinator = coordinator
        self.isEditingForm = isEditingForm
    }
}

// MARK: - ProfissionalInfoPresenting
extension ProfissionalInfoPresenter: ProfissionalInfoPresenting {
    func setupProgressView(value: Float) {
        viewController?.updateProgressView(value: value)
    }
    
    func didNextStep(creditForm: CreditForm) {
        coordinator.perform(action: isEditingForm ? .close: .nextStep(creditForm))
    }
    
    func setupInitialForm(creditForm: CreditForm) {
        let buttonTittle = isEditingForm ? DefaultLocalizable.save.text: ProfissionalInfoLocalizable.tittleButton.text
        viewController?.displayInitialForm(creditForm: creditForm, buttonTitle: buttonTittle)
    }
    
    func presentIncomeError(message: String?) {
        viewController?.displayIncomeError(message: message)
    }
    
    func enableContinueButton(enable: Bool) {
        viewController?.enableContinueButton(enable: enable)
    }
}
