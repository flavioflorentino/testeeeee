import CoreLegacy
import SnapKit
import UI
import UIKit

protocol ProfissionalInfoDisplay: AnyObject {
    func updateProgressView(value: Float)
    func displayInitialForm(creditForm: CreditForm, buttonTitle: String)
    func displayIncomeError(message: String?)
    func enableContinueButton(enable: Bool)
}

extension ProfissionalInfoViewController.Layout {
    enum Size {
        static let progressHeight: Double = 3
        static let buttonHeight: Double = 44
    }
    enum Length {
        static let patrimonyFieldMaxLenght: Int = 17
    }
    enum CornerRadius {
        static let button: CGFloat = 22
    }
    enum Color {
        // todo O fluxo do crédito esta utilizando o estilo de cor antigo (Palette)
        static let enableButton = Colors.branding400.color
        static let disableButton = Palette.ppColorGrayscale300.color
        static let background = Palette.ppColorGrayscale000.color
        static let tittleButton = Palette.white.color
    }
}

final class ProfissionalInfoViewController: ViewController<ProfissionalInfoViewModelInputs, UIView> {    
    fileprivate enum Layout { }

    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = .clear
        scroll.alwaysBounceVertical = false
        return scroll
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var progressView: UIProgressView = {
        let progressView = UIProgressView()
        // todo: para atualizar essa cor deverá atualizar todo o fluxo do crédito
        progressView.progressTintColor = Layout.Color.enableButton
        progressView.trackTintColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
        progressView.progress = 0.0
        return progressView
    }()
    
    private lazy var formsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base07
        stackView.distribution = .equalSpacing
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var incomeTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.placeholder = ProfissionalInfoLocalizable.incomePlaceholder.text
        textfield.addTarget(self, action: #selector(incomeTextFieldDidChange), for: .editingChanged)
        textfield.keyboardType = .numberPad
        textfield.delegate = self
        let doneToolBar = DoneToolBar(doneText: DefaultLocalizable.btOk.text)
        textfield.inputAccessoryView = doneToolBar
        return textfield
    }()
    
    private lazy var patrimonyTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.placeholder = ProfissionalInfoLocalizable.patrimony.text
        textfield.addTarget(self, action: #selector(patrimonyTextFieldDidBegin), for: .editingDidBegin)
        return textfield
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = Layout.CornerRadius.button
        button.backgroundColor = Layout.Color.enableButton
        button.setTitleColor(Layout.Color.tittleButton, for: .normal)
        button.isEnabled = false
        button.addTarget(self, action: #selector(nextPressed), for: .touchUpInside)
        return button
    }()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.setupInitialForm()
        viewModel.getRegistrationStep()
        hideKeyboardWhenTappedAround()
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
        if isMovingFromParent {
            viewModel.backScreenEvent()
        }
    }
    
    public override func buildViewHierarchy() {
        contentView.addSubview(formsStackView)
        formsStackView.addArrangedSubview(incomeTextField)
        formsStackView.addArrangedSubview(patrimonyTextField)
        formsStackView.addArrangedSubview(confirmButton)
        
        view.addSubviews(progressView, scrollView)
        scrollView.addSubview(contentView)
    }
    
    public override func setupConstraints() {
        progressView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Size.progressHeight)
        }
        
        confirmButton.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.buttonHeight)
        }
        
        scrollView.snp.makeConstraints {
            $0.top.equalTo(progressView.snp.bottom)
            $0.bottom.equalToSuperview()
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalTo(scrollView)
        }
        
        formsStackView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
        }
    }

    public override func configureViews() {
        title = CreditPicPayLocalizable.additionalData.text
        view.backgroundColor = Layout.Color.background
    }
    
    @objc
    private func incomeTextFieldDidChange(sender: UITextField) {
        guard let value = sender.text else {
            return
        }
        
        let text = "R$ \(value.currencyInputFormatting())"
        sender.text = text
        viewModel.validateAndEnableContinue(text: text)
    }
    
    @objc
    private func patrimonyTextFieldDidBegin() {
        let controller = ListSelectorTableViewController.instantiate()
        controller.delegate = self
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc
    private func nextPressed() {
        viewModel.save(incomeText: incomeTextField.text)
    }
    
    @objc
    private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
}

// MARK: ProfissionalInfoDisplay
extension ProfissionalInfoViewController: ProfissionalInfoDisplay {
    func updateProgressView(value: Float) {
        progressView.setProgress(value, animated: false)
    }
    
    func displayInitialForm(creditForm: CreditForm, buttonTitle: String) {
        if let income = creditForm.income {
            incomeTextField.text = CurrencyFormatter.brazillianRealString(from: income as NSNumber)
        }
        if let patrimony = creditForm.patrimony {
            patrimonyTextField.text = patrimony.minMaxValue
        }
        confirmButton.setTitle(buttonTitle, for: .normal)
        viewModel.validateAndEnableContinue(text: incomeTextField.text)
    }
    
    func displayIncomeError(message: String?) {
        incomeTextField.errorMessage = message
    }
    
    func enableContinueButton(enable: Bool) {
        confirmButton.backgroundColor = enable ? Layout.Color.enableButton: Layout.Color.disableButton
        confirmButton.isEnabled = enable
    }
}

extension ProfissionalInfoViewController: ListSelectorTableViewDelegate {
    func selectedItem(_ id: String, description: String) {
        guard let id = Int(id) else {
            return
        }
        patrimonyTextField.text = description
        patrimonyTextField.errorMessage = nil
        viewModel.updatePatrimony(with: id)
        viewModel.validateAndEnableContinue(text: incomeTextField.text)
    }
    
    func loadList(_ completion: @escaping CompletionPatrimoniesResult) {
        viewModel.patrimonies(completion)
    }
}

extension ProfissionalInfoViewController: UITextFieldDelegate {
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text, !string.isEmpty else {
            return true
        }
        let newText = text + string
        if textField == incomeTextField {
              let hasValidSize = newText.count <= Layout.Length.patrimonyFieldMaxLenght
              return hasValidSize
        }
        return false
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let textField = textField as? UIPPFloatingTextField else {
            return
        }
        textField.errorMessage = nil
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == incomeTextField {
            viewModel.checkIncomeText(text: textField.text)
        }
    }
}
