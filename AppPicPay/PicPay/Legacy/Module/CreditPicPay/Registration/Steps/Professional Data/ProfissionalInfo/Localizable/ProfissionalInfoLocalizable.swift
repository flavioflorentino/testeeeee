enum ProfissionalInfoLocalizable: String, Localizable {
    case incomeErrorMessage
    case incomePlaceholder
    case patrimony
    case tittleButton
    case save
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .profissionalInfo
    }
}
