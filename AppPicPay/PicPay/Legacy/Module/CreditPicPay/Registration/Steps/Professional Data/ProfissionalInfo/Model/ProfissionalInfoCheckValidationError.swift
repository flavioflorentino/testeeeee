import Validator

enum ProfissionalInfoCheckValidationError: String, ValidationError {
    case incomeRequired
    
    var description: String {
        rawValue
    }
    
    var message: String {
        ProfissionalInfoLocalizable.incomeErrorMessage.text
    }
}
