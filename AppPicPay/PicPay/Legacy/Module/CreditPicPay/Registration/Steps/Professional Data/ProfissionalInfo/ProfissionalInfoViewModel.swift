import AnalyticsModule
import Card
import Foundation
import Validator

typealias CompletionPatrimoniesResult = ([ListSelectorViewModel.ListSelectorItem], Error?) -> Void

protocol ProfissionalInfoViewModelInputs: AnyObject {
    func setupInitialForm()
    func getRegistrationStep()
    func backScreenEvent()
    func patrimonies(_ completion: @escaping CompletionPatrimoniesResult)
    func updatePatrimony(with id: Int)
    func checkIncomeText(text: String?)
    func validateAndEnableContinue(text: String?)
    func save(incomeText: String?)
}

final class ProfissionalInfoViewModel {
    typealias Dependencies = HasAnalytics & HasCreditCoordinatorManager
    private let dependencies: Dependencies

    private let service: ProfissionalInfoServicing
    private let presenter: ProfissionalInfoPresenting
    
    private let creditForm: CreditForm
    private var patrimonyList: [CreditPatrimony] = []

    init(with model: CreditForm, service: ProfissionalInfoServicing, presenter: ProfissionalInfoPresenting, dependencies: Dependencies) {
        self.creditForm = model
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
    
    // MARK: Private Methods
    private func ruleIncome() -> ValidationRuleSet<String> {
        var rule = ValidationRuleSet<String>()
        rule.add(rule: ValidationRuleLength(min: 3, error: ProfissionalInfoCheckValidationError.incomeRequired))
        return rule
    }
    
    private func validate(incomeText: String) -> Bool {
        return incomeText.validate(rules: ruleIncome()).isValid && creditForm.patrimonyId != nil
    }
    
    private func getPatrimony(by id: Int) -> CreditPatrimony? {
        patrimonyList.first { $0.id == id }
    }
    
    private func mapList() -> [ListSelectorViewModel.ListSelectorItem] {
        patrimonyList.map { ListSelectorViewModel.ListSelectorItem(with: "\($0.id)", and: $0.minMaxValue)}
    }
    
    private func syncData() {
        creditForm.registrationStep = .professionalData
        service.setCreditRegistration(creditForm: creditForm)
    }
    
    private func formatIncomeValue(text: String) -> String {
        return text.replacingOccurrences(of: ".", with: "")
            .replacingOccurrences(of: ",", with: ".")
            .replacingOccurrences(of: "R$", with: "")
            .removingWhiteSpaces()
            .trim()
    }
    
    private func saveForm(with incomeNotFormattedValue: String) {
        let income = formatIncomeValue(text: incomeNotFormattedValue)
        creditForm.income = Double(income)
        syncData()
    }
    
    private func nextStepEvent() {
        dependencies.analytics.log(RequestCardEvent.additionalPersonalInfo(action: .nextStep, isLoan: creditForm.isLoanFlow))
    }
}

// MARK: - ProfissionalInfoViewModelInputs
extension ProfissionalInfoViewModel: ProfissionalInfoViewModelInputs {
    func setupInitialForm() {
        presenter.setupInitialForm(creditForm: creditForm)
    }
    
    func getRegistrationStep() {
        guard let coordinator = dependencies.registrationCoordinator else {
            return
        }
        let progressValue = coordinator.getStepProgressForController(with: .professionalData)
        presenter.setupProgressView(value: progressValue)
    }
    
    func backScreenEvent() {
        dependencies.analytics.log(RequestCardEvent.additionalPersonalInfo(action: .previousStep, isLoan: creditForm.isLoanFlow))
    }
    
    func updatePatrimony(with id: Int) {
        creditForm.patrimonyId = id
        creditForm.patrimony = getPatrimony(by: id)
    }
    
    func validateAndEnableContinue(text: String?) {
        guard let text = text else {
            return
        }
        let incomeText = formatIncomeValue(text: text)
        let isValid = validate(incomeText: incomeText)
        presenter.enableContinueButton(enable: isValid)
    }
    
    func checkIncomeText(text: String?) {
        guard let text = text else {
            return
        }
        
        let formatedValue = formatIncomeValue(text: text)
        let result = formatedValue.validate(rules: ruleIncome())
        var message: String?

        if case let .invalid(error) = result,
            let err = error.first {
            message = err.message
        }
        presenter.presentIncomeError(message: message)
    }
    
    func save(incomeText: String?) {
        guard let text = incomeText else {
            return
        }
        
        let incomeText = formatIncomeValue(text: text)
        if validate(incomeText: incomeText) {
            saveForm(with: text)
            nextStepEvent()
            presenter.didNextStep(creditForm: creditForm)
        }
    }
    
    func patrimonies(_ completion: @escaping CompletionPatrimoniesResult) {
        guard patrimonyList.isEmpty else {
            completion(mapList(), nil)
            return
        }
        service.loadListPatrimonies { result in
            switch result {
            case .success(let response):
                self.patrimonyList = response.list
                completion(self.mapList(), nil)
            case .failure(let error):
                completion([], error)
            }
        }
    }
}
