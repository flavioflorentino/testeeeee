import AnalyticsModule
import Foundation

final class ProfissionalInfoFactory {
    typealias Dependencies = HasAnalytics & HasCreditCoordinatorManager

    static func make(creditForm: CreditForm, isEditingForm: Bool = false) -> ProfissionalInfoViewController {
        let container = DependencyContainer()
        let service: ProfissionalInfoServicing = ProfissionalInfoService(dependencies: container)
        let coordinator: ProfissionalInfoCoordinating = ProfissionalInfoCoordinator(dependencies: container)
        let presenter: ProfissionalInfoPresenting = ProfissionalInfoPresenter(coordinator: coordinator, isEditingForm: isEditingForm)
        let viewModel = ProfissionalInfoViewModel(with: creditForm, service: service, presenter: presenter, dependencies: container)
        let viewController = ProfissionalInfoViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
