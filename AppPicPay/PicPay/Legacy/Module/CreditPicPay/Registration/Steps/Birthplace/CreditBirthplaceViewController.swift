import UIKit
import Validator
import UI

final class CreditBirthplaceViewController: CreditBaseFormController {
    private let viewModel: CreditBirthplaceViewModel
    
    @IBOutlet weak private var countryTextField: UIPPFloatingTextField!
    @IBOutlet weak private var countryButton: UIButton!
    @IBOutlet weak private var stateTextField: UIPPFloatingTextField!
    @IBOutlet weak private var cityTextField: UIPPFloatingTextField!
    @IBOutlet weak private var cityButton: UIButton!
    @IBOutlet weak private var nextBtn: UIPPButton!

    init(with viewModel: CreditBirthplaceViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "CreditBirthplaceViewController", bundle: nil)
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = CreditPicPayLocalizable.birthplace.text
        configureForm()
        populateForm()
        
        if self.isEditingForm {
            nextBtn.setTitle(DefaultLocalizable.save.text, for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let coordinator = CPRegistrationCoordinator.shared {
            setStep(with: coordinator.getStepProgressForController(with: CPStep.birthPlace))
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if isMovingFromParent {
            viewModel.viewWillPop()
        }
    }
    
    // MARK: - User Actions
    
    @IBAction private func next(_ sender: Any) {
        guard validate().isValid else {
            return
        }
        saveFormData()
    }
    
    @IBAction private func showStateOptions(_ sender: Any) {
        guard stateTextField.isEnabled else {
            return
        }
        viewModel.selectedList = .state
        pushListSelectorViewController()
    }

    @IBAction private func showCountryOptions(_ sender: Any) {
        viewModel.selectedList = .country
        pushListSelectorViewController()
    }
    
    @IBAction private func showCityOptions(_ sender: Any) {
        guard cityTextField.isEnabled else {
            return
        }
        viewModel.selectedList = .city
        pushListSelectorViewController()
    }
}

extension CreditBirthplaceViewController: ListSelectorTableViewDelegate {
    func selectedItem(_ id: String, description: String) {
        switch viewModel.selectedList {
        case .city:
            guard let id = Int(id) else {
            return
        }
            selectCity(by: id, and: description)
        case .country:
            guard let id = Int(id) else {
            return
        }
            selectCountry(by: id)
        case .state:
            guard let id = Int(id) else {
            return
        }
            selectState(by: id)
        default:
            break
        }
    }
    
    func loadList(_ completion: @escaping ([ListSelectorViewModel.ListSelectorItem], Error?) -> Void) {
        switch viewModel.selectedList {
        case .city:
            viewModel.loadCities(completion)
        case .country:
            viewModel.loadCountries(completion)
        case .state:
            viewModel.loadStates(completion)
        default:
            break
        }
    }
}

extension CreditBirthplaceViewController {
    fileprivate enum ValidateError: String, Error, CustomStringConvertible, ValidationError {
        case countryRequired = "Informe o país de nascimento"
        case stateRequired = "Informe o estado de nascimento"
        case cityRequired = "Informe a cidade de nascimento"
        
        var description: String {
            return self.rawValue
        }
        
        var message: String {
            return self.rawValue
        }
    }
    
    fileprivate func selectCity(by id: Int, and description: String) {
        cityTextField.text = description
        cityTextField.errorMessage = nil
        viewModel.creditForm.birthplaceCity = description
        viewModel.creditForm.birthplaceCityId = id
    }
    
    fileprivate func selectCountry(by id: Int) {
        guard let country = viewModel.getCountry(by: id) else {
            return
        }
        countryTextField.text = country.name
        viewModel.creditForm.birthplaceCountry = country.name
        viewModel.creditForm.birthplaceCountryAbbr = country.initials
        viewModel.selectedCountry = country
        
        // Clear State if Country was changed
        stateTextField.text = ""
        cityTextField.text = ""
        viewModel.creditForm.birthplaceState = ""
        viewModel.creditForm.birthplaceCityId = nil
        
        if country.initials == "BR" {
            stateTextField.isEnabled = true
            stateTextField.isHidden = false
            cityTextField.isHidden = false
            addTextField(stateTextField)
            addTextField(cityTextField)
        } else {
            stateTextField.isHidden = true
            cityTextField.isHidden = true
            removeTextField(stateTextField)
            removeTextField(cityTextField)
        }
    }
    
    fileprivate func selectState(by id: Int) {
        guard let state = viewModel.getState(by: id) else {
            return
        }
        viewModel.selectedState = state
        stateTextField.text = state.name
        stateTextField.errorMessage = nil
        viewModel.creditForm.birthplaceState = state.name
        viewModel.creditForm.birthplaceStateAbbr = state.initials
        cityTextField.isEnabled = true
        cityTextField.text = ""
        cityTextField.errorMessage = nil
        viewModel.creditForm.birthplaceCity = ""
        viewModel.creditForm.birthplaceCityId = nil
    }
    
    fileprivate func pushListSelectorViewController() {
        let controller = ListSelectorTableViewController.instantiate()
        controller.delegate = self
        switch viewModel.selectedList {
        case .city:
            controller.title = CreditPicPayLocalizable.informCityBirth.text
        case .state:
            controller.title = CreditPicPayLocalizable.informStateBirth.text
        case .country:
            controller.title = CreditPicPayLocalizable.informCountryBirth.text
        default:
            break
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - Configuration
    
    fileprivate func configureForm() {
        isEditingForm = viewModel.isEditingForm
        addTextField(countryTextField)
        addTextField(stateTextField)
        addTextField(cityTextField)
        stateTextField.isEnabled = false
        cityTextField.isEnabled = false
        configureValidations()
    }
    
    fileprivate func configureValidations() {
        var countryRules = ValidationRuleSet<String>()
        countryRules.add(rule: ValidationRuleRequired(error: ValidateError.countryRequired))
        countryTextField.validationRules = countryRules
        
        var stateRules = ValidationRuleSet<String>()
        stateRules.add(rule: ValidationRuleLength(min: 1, error: ValidateError.stateRequired))
        stateTextField.validationRules = stateRules
        
        var cityRules = ValidationRuleSet<String>()
        cityRules.add(rule: ValidationRuleLength(min: 1, error: ValidateError.cityRequired))
        cityTextField.validationRules = cityRules
    }
    
    // MARK: - Form Methods
    fileprivate func populateForm() {
        countryTextField.text = viewModel.creditForm.birthplaceCountry
        stateTextField.text = viewModel.creditForm.birthplaceState
        cityTextField.text = viewModel.creditForm.birthplaceCity
        
        if !viewModel.creditForm.birthplaceCountry.isEmpty {
            stateTextField.isEnabled = true
        }
        if !viewModel.creditForm.birthplaceState.isEmpty {
            cityTextField.isEnabled = true
        }
    }
    
    fileprivate func saveFormData() {
        guard !self.isFormEdition() else {
            return
        }
        
        viewModel.creditForm.birthplaceCountry = countryTextField.text ?? ""
        viewModel.creditForm.birthplaceState = stateTextField.text ?? ""
        viewModel.creditForm.birthplaceCity = cityTextField.text ?? ""
        viewModel.syncData()
        CPRegistrationCoordinator.shared?.pushController(with: viewModel.creditForm)
    }
}
