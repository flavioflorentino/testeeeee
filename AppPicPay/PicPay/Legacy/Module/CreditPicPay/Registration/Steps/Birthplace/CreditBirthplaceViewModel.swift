import AnalyticsModule
import Card
import Foundation

final class CreditBirthplaceViewModel: CreditBaseFormProtocol {
    private let container: HasAnalytics
    let creditForm: CreditForm
    let api = ApiCredit()
    let isEditingForm: Bool
    var step = CPStep.birthPlace
    var selectedList: ListSeleted = .unknown
    var countriesList: [AddressCountry] = []
    var citiesList: [AddressCity] = []
    var stateList: [AddressStates] = []
    var selectedCountry: AddressCountry?
    var selectedState: AddressStates? {
        willSet {
            if self.selectedState != nil {
                citiesList = []
            }
        }
    }

    init(with model: CreditForm, isEditingForm: Bool = false, container: DependencyContainer) {
        self.creditForm = model
        self.container = container
        self.isEditingForm = isEditingForm
        /// set up selected state if existing in form model. 
        if !model.birthplaceState.isEmpty && !model.birthplaceStateAbbr.isEmpty {
            selectedState = AddressStates(initials: model.birthplaceStateAbbr, name: model.birthplaceState)
        }
    }
    
    func syncData() {
        creditForm.registrationStep = step
        container.analytics.log(RequestCardEvent.birthplace(action: .nextStep, isLoan: creditForm.isLoanFlow))
        DispatchQueue.global(qos: .userInteractive).async {
            let api = ApiCredit()
            api.setCreditRegistration(self.creditForm) { _ in }
        }
    }
    
    func viewWillPop() {
        container.analytics.log(RequestCardEvent.birthplace(action: .previousStep, isLoan: creditForm.isLoanFlow))
    }
    
    func loadCountries(_ completion: @escaping ([ListSelectorViewModel.ListSelectorItem], Error?) -> Void) {
        if countriesList.isEmpty {
            api.creditAddressCountries { [weak self] result in
                guard let strongSelf = self else {
            return
        }
                DispatchQueue.main.async {
                    switch result {
                    case .success(let countriesResult):
                        strongSelf.countriesList = countriesResult.list
                        completion(strongSelf.convertCountryItem(), nil)
                    case .failure(let error):
                        completion([], error)
                    }
                }
            }
        } else {
            completion(convertCountryItem(), nil)
        }
    }
    
    func loadStates(_ completion: @escaping (([ListSelectorViewModel.ListSelectorItem], Error?) -> Void)) {
        if stateList.isEmpty {
            api.creditAddressStates { [weak self] result in
                guard let strongSelf = self else {
            return
        }
                DispatchQueue.main.async {
                    switch result {
                    case .success(let statesResult):
                        strongSelf.stateList = statesResult.list
                        strongSelf.citiesList = []
                        completion(strongSelf.convertStateItem(), nil)
                    case .failure(let error):
                        completion([], error)
                    }
                }
            }
        } else {
            completion(convertStateItem(), nil)
        }
    }
    
    func loadCities(_ completion: @escaping (([ListSelectorViewModel.ListSelectorItem], Error?) -> Void)) {
        if citiesList.isEmpty {
            guard let state = self.selectedState else {
                completion([], PicPayError(message: CreditPicPayLocalizable.selectStateLoad.text))
                return
            }
            api.creditAddressCities(from: state.initials) { [weak self] result in
                guard let strongSelf = self else {
            return
        }
                DispatchQueue.main.async {
                    switch result {
                    case .success(let citiesResult):
                        strongSelf.citiesList = citiesResult.list
                        completion(strongSelf.convertCityItem(), nil)
                    case .failure(let error):
                        completion([], error)
                    }
                }
            }
        } else {
            guard self.selectedState != nil else {
                completion([], PicPayError(message: CreditPicPayLocalizable.selectStateLoad.text))
                return
            }
            completion(convertCityItem(), nil)
        }
    }
    
    func getCity(by id: Int) -> AddressCity? {
        return citiesList.first(where: { $0.id == id })
    }
    
    func getState(by id: Int) -> AddressStates? {
        return stateList.first(where: { $0.id == id })
    }
    
    func getCountry(by id: Int) -> AddressCountry? {
        return countriesList.first(where: { $0.id == id })
    }
}

extension CreditBirthplaceViewModel {
    private func convertCountryItem() -> [ListSelectorViewModel.ListSelectorItem] {
        return countriesList.map({ country -> ListSelectorViewModel.ListSelectorItem in
            return ListSelectorViewModel.ListSelectorItem(with: "\(country.id)", and: country.name)
        })
    }
    
    private func convertCityItem() -> [ListSelectorViewModel.ListSelectorItem] {
        return citiesList.map({ city -> ListSelectorViewModel.ListSelectorItem in
            return ListSelectorViewModel.ListSelectorItem(with: "\(city.id)", and: city.name)
        })
    }
    
    private func convertStateItem() -> [ListSelectorViewModel.ListSelectorItem] {
        return stateList.map({ state -> ListSelectorViewModel.ListSelectorItem in
            return ListSelectorViewModel.ListSelectorItem(with: "\(state.id)", and: state.name)
        })
    }
}

extension CreditBirthplaceViewModel {
    enum ListSeleted {
        case country
        case state
        case city
        case unknown
    }
}
