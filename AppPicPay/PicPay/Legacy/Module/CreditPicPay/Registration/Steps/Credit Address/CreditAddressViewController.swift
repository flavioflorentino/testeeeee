import Address
import Card
import UI
import AssetsKit
import Validator
import SafariServices

final class CreditAddressViewController: CreditBaseFormController {
    // MARK: - State enum
    enum State {
        case blank
        case cpfValid
    }
    
    // MARK: - Properties
    var viewModel: CreditAddressViewModel?
    
    private var state: State = .blank {
        didSet {
            renderState()
            renderPrimaryButton()
            renderTextFields()
        }
    }
    
    private var safeAreaTopInset: CGFloat {
        guard #available(iOS 11.0, *) else { return 0 }
        return view.safeAreaInsets.top
    }
    
    private var currentTextField: UITextField?
    
    // MARK: - Constants
    private let cepTextLength = 8
    private let addressTextMaxLength = 30
    private let neighborhoodTextMaxLength = 30
    private let cityTextMinLength = 2
    private let cityTextMaxLength = 30
    private let stateTextMinLength = 2
    private let stateTextMaxLength = 30
    private let numberTextFieldMaxLength = 6
    private let complementTextMaxLength = 15
    
    // MARK: View Properties
    lazy var contentScrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.bounces = false
        scroll.showsVerticalScrollIndicator = false
        scroll.showsHorizontalScrollIndicator = false
        return scroll
    }()
    
    lazy var customContentView = UIView(frame: .zero)
    
    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "Precisamos da informação abaixo para localizarmos o seu endereço."
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale500.color)
        label.numberOfLines = 0
        label.minimumScaleFactor = 0.8
        return label
    }()

    lazy var addressStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    lazy var streetNumberStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .equalSpacing
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    lazy var cepTextField: ValidationTextField = {
        let cepTextLengthWithHyphen = cepTextLength + 1
        let textField = ValidationTextField(placeholder: "CEP", maxLength: cepTextLengthWithHyphen, hasCharacterLimitBehavior: true)
        textField.keyboardType = .numberPad
        return textField
    }()
    
    lazy var addressTextField: ValidationTextField = {
        let textField = ValidationTextField(placeholder: "Rua, avenida, alameda, etc", maxLength: addressTextMaxLength, hasCharacterLimitBehavior: true)
        textField.addTarget(self, action: #selector(streetDidChange(_:)), for: .editingChanged)
        return textField
    }()
    
    lazy var numberTextField: ValidationTextField = {
        let textField = ValidationTextField(placeholder: "Número", maxLength: numberTextFieldMaxLength, hasCharacterLimitBehavior: true)
        textField.keyboardType = .numberPad
        textField.addTarget(self, action: #selector(streetNumberDidChange(_:)), for: .editingChanged)
        return textField
    }()

    lazy var neighborhoodTextField: ValidationTextField = {
        let textField = ValidationTextField(placeholder: "Bairro", maxLength: neighborhoodTextMaxLength, hasCharacterLimitBehavior: true)
        textField.addTarget(self, action: #selector(neighborhoodDidChange(_:)), for: .editingChanged)
        return textField
    }()
    
    lazy var complementTextField: ValidationTextField = {
        let textField = ValidationTextField(placeholder: "Complemento", maxLength: complementTextMaxLength, hasCharacterLimitBehavior: true)
        textField.addTarget(self, action: #selector(complementDidChange(_:)), for: .editingChanged)
        return textField
    }()

    lazy var cityTextField: ValidationTextField = {
        let textField = ValidationTextField(placeholder: "Cidade", maxLength: cityTextMaxLength, hasCharacterLimitBehavior: true)
        textField.isEnabled = false
        return textField
    }()
    
    lazy var stateTextField: ValidationTextField = {
        let textField = ValidationTextField(placeholder: "Estado", maxLength: stateTextMaxLength, hasCharacterLimitBehavior: true)
        textField.isEnabled = false
        return textField
    }()
    
    lazy var continueButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(nextTapped), for: .touchUpInside)
        button.setTitle("Continuar", for: .normal)
        button.setBackgroundColor(Colors.branding400.color, for: .normal)
        return button
    }()
    
    private lazy var withoutNumberButton: UIButton = {
        let button = UIButton()
        let unselectedColorButton = Palette.ppColorGrayscale700.color
        let selectedColotButton = Palette.ppColorBranding300.color
        button.addTarget(self, action: #selector(self.withoutNumberButtonAction), for: .touchUpInside)
        button.setTitle("Sem número", for: .normal)
        button.setTitleColor(unselectedColorButton, for: .normal)
        button.setTitleColor(selectedColotButton, for: .selected)
        button.tintColor = button.titleColor(for: .normal)
        button.setImage(Assets.Icons.iconUnchecked.image.withRenderingMode(.alwaysTemplate), for: .normal)
        button.setImage(Assets.Icons.iconChecked.image.withRenderingMode(.alwaysOriginal), for: .selected)
        button.contentEdgeInsets = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
        button.contentHorizontalAlignment = .left
        button.imageEdgeInsets = UIEdgeInsets(top: 4, left: -4, bottom: 4, right: 4)
        button.imageView?.contentMode = .scaleAspectFit
        button.backgroundColor = view.backgroundColor
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.minimumScaleFactor = 0.5
        return button
    }()
    
    lazy var notMyCepButton: UIButton = {
        let button = UIButton()
        button.setTitle("Não é meu CEP", for: .normal)
        button.buttonStyle(LinkButtonStyle(size: .small))
        button.addTarget(self, action: #selector(didTapNotMyCep(sender:)), for: .touchUpInside)
        button.titleEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        button.alpha = 0.0
        return button
    }()
    
    lazy var dontKnowMyCepButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle(size: .default))
        button.setTitle("Não sei meu CEP", for: .normal)
        button.addTarget(self, action: #selector(didTapDontKnowMyCep(sender:)), for: .touchUpInside)
        return button
    }()
    
    lazy var greenCheckImageView: UIImageView = {
        let imageView = UIImageView(image: Resources.Icons.icoCheck.image)
        imageView.alpha = 0.0
        return imageView
    }()
    
    // MARK: Override
    override func viewDidLoad() {
        super.viewDidLoad()
        state = .blank
        title = DefaultLocalizable.deliveryAddress.text
        configureForm()
        buildLayout()
        state = determineState()
        if isEditingForm {
            continueButton.setTitle(DefaultLocalizable.save.text, for: .normal)
        }
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let coordinator = CPRegistrationCoordinator.shared {
            setStep(with: coordinator.getStepProgressForController(with: CPStep.address))
        }
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if isMovingFromParent {
            viewModel?.viewWillPop()
        }
        NotificationCenter.default.removeObserver(self)
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if [stateTextField, cityTextField].contains(textField) {
            return false
        }
        let shouldChange = super.textField(textField, shouldChangeCharactersIn: range, replacementString: string)
        if textField.tag == 1 {
            validateCepTextField(textfield: textField, inRange: range, replacementString: string, shouldChange: shouldChange)
        }
        return shouldChange
    }
    
    // MARK: - Text Field Listeners
    @objc
    private func streetDidChange(_ sender: UITextField) {
        guard let text: String = sender.text else {
            return
        }
        viewModel?.setStreet(text)
        renderTextField(addressTextField)
        renderPrimaryButton()
    }
    
    @objc
    private func streetNumberDidChange(_ sender: UITextField) {
        guard let text: String = sender.text else {
            return
        }
        viewModel?.setStreetNumber(text)
        renderTextField(numberTextField)
        renderPrimaryButton()
    }
    
    @objc
    private func complementDidChange(_ sender: UITextField) {
        guard let text: String = sender.text else {
            return
        }
        viewModel?.setComplement(text)
        renderTextField(complementTextField)
        renderPrimaryButton()
    }
    
    @objc
    private func neighborhoodDidChange(_ sender: UITextField) {
        guard let text: String = sender.text else {
            return
        }
        viewModel?.setNeighborhood(text)
        renderTextField(neighborhoodTextField)
        renderPrimaryButton()
    }
    
    // MARK: Objc functions
    @objc
    private func nextTapped(_ sender: Any) {
        guard isFormValid() else { return }
        saveFormData()
    }
    
    @objc
    private func noNumberHomeTapped(_ sender: UIButton) {
        sender.isSelected.toggle()
        numberTextField.isEnabled = !sender.isSelected
        if sender.isSelected {
            numberTextField.text = ""
            viewModel?.creditForm.homeAddress?.streetNumber = "0"
        }
        renderPrimaryButton()
    }
    
    // MARK: - Private Functions
    fileprivate func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func saveFormData() {
        guard let viewModel = viewModel, !self.isFormEdition() else { return }
        viewModel.syncData()
        if viewModel.wasAddressChanged {
            AlertMessage.showTwoButtonsCustomAlert(title: "Endereço de entrega",
                                         message: "Você alterou o endereço do CEP que está registrado no site dos Correios. A alteração está correta?",
                                         image: Resources.Illustrations.iluWarning.image,
                                         confirmationButtonTitle: "Sim, está correta",
                                         controller: self,
                                         completion: {
                                            CPRegistrationCoordinator.shared?.pushController(with: viewModel.creditForm)
                                         })
        } else {
            CPRegistrationCoordinator.shared?.pushController(with: viewModel.creditForm)
        }
    }
    
    private func isFormValid() -> Bool {
        let requiredTextFelds = [cepTextField, addressTextField, neighborhoodTextField, cityTextField, stateTextField]
        let isRequiredTextFeldsValid = requiredTextFelds.filter({ $0.textFieldState == .valid }).count == requiredTextFelds.count
        
        let isNumberFieldValid = numberTextField.textFieldState == .valid || withoutNumberButton.isSelected
        
        return state == .cpfValid && isRequiredTextFeldsValid && isNumberFieldValid
    }
    
    private func loadAddress(_ cep: String) {
        cepTextField.isEnabled = false
        cepTextField.isLoading = true
        
        viewModel?.loadAddress(by: cep.onlyNumbers) { [weak self] error in
            guard let self = self else { return }
            self.cepTextField.isEnabled = true
            self.cepTextField.isLoading = false
            if error != nil {
                self.state = .blank
                self.cepTextField.textFieldState = .invalid
                AlertMessage.showCustomAlertWithErrorWithImage(title: DefaultLocalizable.oops.text, errorMessage: DefaultLocalizable.anErrorHasOccurred.text, image: Resources.Illustrations.iluError.image, controller: self)
            } else {
                self.state = .cpfValid
            }
            self.form(isLocked: false)
        }
    }
    
    private func didPostalCodeError() {
        stateTextField.text = nil
        cityTextField.text = nil
        neighborhoodTextField.text = nil
        addressTextField.text = nil
        numberTextField.text = nil
    }
    
    private func validateCepTextField(textfield field: UITextField, inRange range: NSRange, replacementString string: String, shouldChange: Bool) {
        let oldString = field.text ?? ""
        var finalText = oldString
        if shouldChange {
            finalText = "\(oldString)\(string)"
        }
        if finalText.length == 9 {
            cepTextField.underlineErrorMessage = ""
            loadAddress(finalText.onlyNumbers)
        } else {
            if finalText.isEmpty {
                cepTextField.underlineErrorMessage = ""
                cepTextField.textFieldState = .blank
            } else {
                cepTextField.underlineErrorMessage = "CEP inválido"
                cepTextField.textFieldState = .invalid
            }
            if notMyCepButton.alpha == 1.0 {
                notMyCepButton.alpha = 0.0
            }
            emptyForm()
            form(isLocked: true)
        }
    }
    
    // MARK: - objc Selectors
    
    @objc
    func didTapNotMyCep(sender: UIButton) {
        emptyForm()
        cepTextField.text = ""
        cepTextField.textFieldState = .blank
        notMyCepButton.alpha = 0.0
        cepTextField.isEnabled = true
        viewModel?.trackNotMyPostalCodeAction()
    }
    
    @objc
    func didTapDontKnowMyCep(sender: UIButton) {
        if let url = viewModel?.cardAddressSearchPostalCodeUrl {
            let safariVC = SFSafariViewController(url: url)
            if #available(iOS 11.0, *) {
                safariVC.configuration.barCollapsingEnabled = true
                safariVC.configuration.entersReaderIfAvailable = true
            }
            present(safariVC, animated: true, completion: nil)
            viewModel?.trackOpenedPostOfficeAction()
        }
    }
    
    @objc
    private func withoutNumberButtonAction(sender: Any?) {
        withoutNumberButton.isSelected.toggle()
        viewModel?.trackNoNumber(checked: withoutNumberButton.isSelected)
        numberTextField.isEnabled = !withoutNumberButton.isSelected
        if withoutNumberButton.isSelected {
            numberTextField.text = ""
            numberTextField.textFieldState = .blank
            numberTextField.errorMessage = nil
        }
        renderPrimaryButton()
    }
}

// MARK: - Logic
extension CreditAddressViewController {
    private func determineState() -> State {
        guard let homeAddress = viewModel?.creditForm.homeAddress else { return .blank }
        if let cep = homeAddress.zipCode?.onlyNumbers, cep.count == 8 {
            return .cpfValid
        }
        return.blank
    }
    
    // MARK: - Animation
    private func startCheckmarkAnimation() {
        UIView.animate(
            withDuration: 0.5,
            delay: 0.0,
            options: .transitionCrossDissolve,
            animations: { [weak self] in
                self?.greenCheckImageView.alpha = 1.0
            }, completion: { [weak self] _ in
            UIView.animate(
                withDuration: 0.5,
                delay: 0.5,
                options: .transitionCrossDissolve,
                animations: { [weak self] in
                    self?.greenCheckImageView.alpha = 0.0
                    self?.notMyCepButton.alpha = 1.0
                    self?.populateForm()
                    self?.dontKnowMyCepButton.alpha = 0.0
                    self?.continueButton.alpha = 1.0
                })
           })
    }

    // MARK: Render Functions
    
    private func renderState() {
        switch state {
        case .cpfValid:
            startCheckmarkAnimation()
            cepTextField.isEnabled = false
            cepTextField.underlineErrorMessage = ""
        case .blank:
            emptyForm()
        }
    }
    
    private func renderPrimaryButton() {
        continueButton.isEnabled = isFormValid()
    }
    
    private func renderTextFields() {
        [cepTextField,
         addressTextField,
         neighborhoodTextField,
         numberTextField,
         complementTextField,
         stateTextField,
         cityTextField].forEach {
            renderTextField($0)
         }
    }
    
    private func renderTextField(_ textField: ValidationTextField) {
        guard let text = textField.text else { return }
        if text.count <= (textField.maxlength ?? 0) {
            textField.textFieldState = text.isEmpty ? .blank : .valid
        }
    }
    
    private func setTextFieldVisibility(to shouldShow: Bool) {
        [addressTextField,
         neighborhoodTextField,
         numberTextField,
         complementTextField,
         stateTextField,
         withoutNumberButton,
         cityTextField].forEach { $0.alpha = shouldShow ? 1.0 : 0.0 }
    }
    
    private func configureForm() {
        configTextFieldsInputAccessory()
        configureValidations()
        cepTextField.tag = 1
        cepTextField.delegate = self
    }
    
    private func configTextFieldsInputAccessory() {
        let doneToolBar = DoneToolBar(doneText: "Ok")
        cepTextField.inputAccessoryView = doneToolBar
        addressTextField.inputAccessoryView = doneToolBar
        numberTextField.inputAccessoryView = doneToolBar
        complementTextField.inputAccessoryView = doneToolBar
        neighborhoodTextField.inputAccessoryView = doneToolBar
        cityTextField.inputAccessoryView = doneToolBar
        stateTextField.inputAccessoryView = doneToolBar
    }
    
    private func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case addressTextField:
            neighborhoodTextField.becomeFirstResponder()
        case neighborhoodTextField:
            numberTextField.becomeFirstResponder()
        case numberTextField:
            complementTextField.becomeFirstResponder()
        case complementTextField:
            textField.resignFirstResponder()
        default:
            return false
        }
        return true
    }
    
    private func configureValidations() {
        var cepRules = ValidationRuleSet<String>()
        cepRules.add(rule: ValidationRuleLength(min: cepTextLength, error: CreditAddressViewModel.ValidateError.cepRequired))
        numberTextField.maxlength = numberTextFieldMaxLength
        addressTextField.maxlength = addressTextMaxLength
        neighborhoodTextField.maxlength = neighborhoodTextMaxLength
        complementTextField.maxlength = complementTextMaxLength
        cityTextField.maxlength = cityTextMaxLength
        stateTextField.maxlength = stateTextMaxLength
        complementTextField.validationRules = RegisterComplementAddressValidator.rules()
        cepTextField.validationRules = cepRules
        cepTextField.maskString = "00000-000"
        addressTextField.validationRules = RegisterStreetAddressValidator.rules()
        neighborhoodTextField.validationRules = RegisterNeighborhoodAddressValidator.rules()
        cityTextField.validationRules = RegisterCityAddressValidator.rules()
        stateTextField.validationRules = RegisterStateAddressValidator.rules()
    }
    
    // MARK: - Form Methods
    private func populateForm() {
        guard let homeAddress = viewModel?.creditForm.homeAddress else { return }
        let streetName: String
        if !homeAddress.streetType.isEmpty {
            streetName = "\(homeAddress.streetType ?? "") \(homeAddress.street ?? "")"
        } else {
            streetName = homeAddress.street ?? ""
        }
        cepTextField.text = format(zipCode: homeAddress.zipCode)
        addressTextField.text = streetName
        complementTextField.text = homeAddress.addressComplement
        neighborhoodTextField.text = homeAddress.neighborhood
        cityTextField.text = homeAddress.city
        stateTextField.text = homeAddress.state
        if let number = homeAddress.streetNumber, number != "0" {
            numberTextField.text = homeAddress.streetNumber
        }
        setTextFieldVisibility(to: true)
        renderTextFields()
        renderPrimaryButton()
    }
    
    private func emptyForm() {
        [addressTextField,
        neighborhoodTextField,
        complementTextField,
        numberTextField,
        cityTextField,
        stateTextField].forEach {
            $0.text = ""
            $0.errorMessage = nil
        }
        setTextFieldVisibility(to: false)
        dontKnowMyCepButton.alpha = 1.0
        continueButton.alpha = 0.0
    }
    
    private func form(isLocked: Bool) {
        [addressTextField, neighborhoodTextField, numberTextField, complementTextField].forEach {
            $0.isEnabled = !isLocked
        }
    }
    
    private func format(zipCode: String?) -> String {
        if let zip = zipCode {
            if zip.length == 8 {
                let prefix = zip.chopSuffix(3)
                let sufix = zip.chopPrefix(5)
                return "\(prefix)-\(sufix)"
            }
        }
        return ""
    }
    // swiftlint:disable:next file_length
}
// MARK: - View Configuration
extension CreditAddressViewController: ViewConfiguration {
    func buildViewHierarchy() {
        customContentView.addSubviews(descriptionLabel, addressStackView, continueButton, notMyCepButton, dontKnowMyCepButton, greenCheckImageView)
        contentScrollView.addSubview(customContentView)
        streetNumberStackView.addArrangedSubviews(numberTextField, withoutNumberButton)
        addressStackView.addArrangedSubviews(cepTextField, addressTextField, neighborhoodTextField, streetNumberStackView, complementTextField, stateTextField, cityTextField)
        view.addSubview(contentScrollView)
    }
    
    func setupConstraints() {
        contentScrollView.snp.makeConstraints { $0.edges.size.equalToSuperview() }
        
        continueButton.snp.makeConstraints {
            $0.height.equalTo(Sizing.base06)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base04)
        }
        
        dontKnowMyCepButton.snp.makeConstraints {
            $0.height.equalTo(Sizing.base06)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerX.equalToSuperview()
            $0.bottom.equalToSuperview().inset(Spacing.base04)
        }
        
        customContentView.snp.makeConstraints { $0.size.edges.equalToSuperview() }
    
        [cepTextField,
         addressTextField,
         neighborhoodTextField,
         numberTextField,
         complementTextField,
         stateTextField,
         cityTextField].forEach { $0.snp.makeConstraints { $0.height.equalTo(Sizing.base08) } }
        
        numberTextField.snp.makeConstraints { $0.width.equalToSuperview().multipliedBy(0.65) }
        
        notMyCepButton.snp.makeConstraints {
            $0.centerY.equalTo(cepTextField)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        greenCheckImageView.snp.makeConstraints {
            $0.centerY.equalTo(cepTextField)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.size.equalTo(20)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.height.equalTo(Sizing.base06)
        }
        addressStackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.bottom.equalTo(continueButton.snp.top).offset(-Spacing.base03)
        }
    }
}

extension CreditAddressViewController {
    // MARK: - Keyboard Handling
    @objc func keyboardWillShow(notification: Notification) {
                guard let keyboardSize = notification.keyboardSize else { return }
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        contentScrollView.contentInset = insets
        contentScrollView.scrollIndicatorInsets = insets
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        var rect = view.frame
        rect.size.height -= keyboardSize.height

        let activeField: UITextField? = [addressTextField, neighborhoodTextField, numberTextField, complementTextField, stateTextField, cityTextField].first { $0.isFirstResponder }
        if let activeField = activeField {
            if !rect.contains(activeField.frame.origin) {
                let scrollPoint = CGPoint(x: 0, y: activeField.frame.origin.y - keyboardSize.height)
                contentScrollView.setContentOffset(scrollPoint, animated: true)
                let bottomOffset = CGPoint(x: 0, y: contentScrollView.contentSize.height - contentScrollView.bounds.size.height + contentScrollView.contentInset.bottom)
                contentScrollView.setContentOffset(bottomOffset, animated: true)
            }
        }
    }

    @objc func keyboardWillHide(notification: Notification) {
        let contentInset: UIEdgeInsets = UIEdgeInsets.zero
        contentScrollView.contentInset = contentInset
    }
}

extension Notification {
    var keyboardSize: CGSize? {
        return (userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
    }
    var keyboardAnimationDuration: Double? {
        return userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double
    }
    var keyboardAnimationCurve: Double? {
        return userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? Double
    }
}


