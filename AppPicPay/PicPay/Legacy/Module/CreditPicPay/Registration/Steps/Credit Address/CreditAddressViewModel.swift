import AnalyticsModule
import Card
import Foundation
import SwiftyJSON
import Validator

final class CreditAddressViewModel: CreditBaseFormProtocol {
    let creditForm: CreditForm
    var container: HasAnalytics & HasFeatureManager
    let api = ApiCredit()
    
    var step: CPStep = .address
    
    var originalAddressSearch: AddressSearch?
    
    var wasAddressChanged: Bool {
        guard let originalAddressSearch = originalAddressSearch else { return false }
        return
            originalAddressSearch.neighborhood != creditForm.homeAddress?.neighborhood
            ||
            originalAddressSearch.street != creditForm.homeAddress?.street
    }
    
    var cardAddressSearchPostalCodeUrl: URL? {
        return URL(string: container.featureManager.text(.cardAddressSearchPostalCodeUrl))
    }
    
    init(with model: CreditForm, container: DependencyContainer) {
        self.creditForm = model
        self.container = container
        originalAddressSearch = creditForm.homeAddress
    }
    
    func loadAddress(by cep: String, _ completion: @escaping (Error?) -> Void) {
        api.addressBy(cep) { [weak self] result in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                switch result {
                case .success(let address):
                    self.originalAddressSearch = address
                    var address = address
                    address.addressComplement = ""
                    self.creditForm.homeAddress = address
                    completion(nil)
                    self.container.analytics.log(CardDeliveryAddressEvent.didGetPostalCode(result: true))
                case .failure(let error):
                    self.creditForm.homeAddress?.zipCode = cep
                    completion(error)
                    self.container.analytics.log(CardDeliveryAddressEvent.didGetPostalCode(result: false))
                }
            }
        }
    }
    
    func viewWillPop() {
        container.analytics.log(RequestCardEvent.address(action: .previousStep, isLoan: creditForm.isLoanFlow))
        container.analytics.log(CardDeliveryAddressEvent.didTapBack)
    }
    
    func comercialAddressTapped() {
        container.analytics.log(RequestCardEvent.address(action: .addComercialAddress, isLoan: creditForm.isLoanFlow))
    }
    
    func syncData() {
        creditForm.registrationStep = step
        container.analytics.log(RequestCardEvent.address(action: .nextStep, isLoan: creditForm.isLoanFlow))
        container.analytics.log(CardDeliveryAddressEvent.didTapContinue(editedAddress: wasAddressChanged))
        DispatchQueue.global(qos: .userInteractive).async {
            let api = ApiCredit()
            api.setCreditRegistration(self.creditForm, manualUpdated: self.wasAddressChanged) { _ in }
        }
    }
    
    func setStreetNumber(_ number: String) {
        creditForm.homeAddress?.streetNumber = number
    }
    
    func setComplement(_ complement: String) {
        creditForm.homeAddress?.addressComplement = complement
    }
    
    func setStreet(_ street: String) {
        let streetFinal: String = homeStreetNameHelper(for: street)
        creditForm.homeAddress?.street = streetFinal
    }
    
    func setNeighborhood(_ neighborhood: String) {
        creditForm.homeAddress?.neighborhood = neighborhood
    }
    
    func setCity(id: Int, name: String) {
        creditForm.homeAddress?.cityId = String(id)
        creditForm.homeAddress?.city = name
    }
    
    func setState(id: Int, name: String) {
        creditForm.homeAddress?.stateId = String(id)
        creditForm.homeAddress?.state = name
    }
    
    func trackOpenedPostOfficeAction() {
        container.analytics.log(CardDeliveryAddressEvent.didTapNotKnowMyPostalCode)
    }
    
    func trackNotMyPostalCodeAction() {
        container.analytics.log(CardDeliveryAddressEvent.didTapIsNotMyPostalCode)
    }
    
    func trackNoNumber(checked: Bool) {
        container.analytics.log(CardDeliveryAddressEvent.didTapNoNumber(checked: checked))
    }
}

extension CreditAddressViewModel {
    private func homeStreetNameHelper(for street: String) -> String {
        guard let homeAddress = self.creditForm.homeAddress else {
            return street
        }
        if street == homeAddress.streetName {
            return homeAddress.street ?? ""
        }
        return street
    }
}

extension CreditAddressViewModel {
    enum ValidateError: String, Error, CustomStringConvertible, ValidationError {
        case cepRequired = "CEP é obrigatório"
        
        var description: String {
            return self.rawValue
        }
        
        var message: String {
            return self.rawValue
        }
    }
}
