import Address
import Card
import UI
import UIKit
import Validator

final class OldCreditAddressViewController: CreditBaseFormController {
    var viewModel: CreditAddressViewModel?
    var isLoading: Bool = false
    var cep: String = ""
    
    //Constants
    private let cepTextLength = 8
    private let addressTextMaxLength = 30
    private let neighborhoodTextMaxLength = 30
    private let cityTextMinLength = 2
    private let cityTextMaxLength = 30
    private let stateTextMinLength = 2
    private let stateTextMaxLength = 30
    private let numberTextFieldMaxLength = 6
    private let complementTextMaxLength = 15
    
    @IBOutlet private weak var residentialTitleLabel: UILabel!
    @IBOutlet private weak var containerStackView: UIStackView!
    @IBOutlet private weak var cepTextField: UIPPFloatingTextField!
    @IBOutlet private weak var addressTextField: UIPPFloatingTextField!
    @IBOutlet private weak var numberTextField: UIPPFloatingTextField!
    @IBOutlet private weak var complementTextField: UIPPFloatingTextField!
    @IBOutlet private weak var neighborhoodTextField: UIPPFloatingTextField!
    @IBOutlet private weak var cityTextField: UIPPFloatingTextField!
    @IBOutlet private weak var stateTextField: UIPPFloatingTextField!
    @IBOutlet private weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var nextBtn: UIPPButton!
    @IBOutlet private weak var noNumberHomeAddressButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareLayout()
        configureForm()
        populateForm()
        containerStackView.isLayoutMarginsRelativeArrangement = true
        if self.isEditingForm {
            nextBtn.setTitle(DefaultLocalizable.save.text, for: .normal)
        }
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let coordinator = CPRegistrationCoordinator.shared {
            setStep(with: coordinator.getStepProgressForController(with: CPStep.address))
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if isMovingFromParent {
            viewModel?.viewWillPop()
        }
    }
    
    @IBAction private func next(_ sender: Any) {
        guard validate().isValid else {
            return
        }
        saveFormData()
    }
    
    // MARK: - Home address text did change
    
    @IBAction private func streetDidChange(_ sender: UITextField) {
        guard let text: String = sender.text else {
            return
        }
        viewModel?.setStreet(text)
    }
    
    @IBAction private func streetNumberDidChange(_ sender: UITextField) {
        guard let text: String = sender.text else {
            return
        }
        viewModel?.setStreetNumber(text)
    }
    
    @IBAction private func complementDidChange(_ sender: UITextField) {
        guard let text: String = sender.text else {
            return
        }
        viewModel?.setComplement(text)
    }
    
    @IBAction private func neighborhoodDidChange(_ sender: UITextField) {
        guard let text: String = sender.text else {
            return
        }
        viewModel?.setNeighborhood(text)
    }
    
    @IBAction private func noNumberHomeTapped(_ sender: UIButton) {
        sender.isSelected.toggle()
        numberTextField.isEnabled = !sender.isSelected
        if sender.isSelected {
            numberTextField.text = ""
            viewModel?.creditForm.homeAddress?.streetNumber = "0"
        }
        configureTextNumberValidation()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == stateTextField {
            textField.resignFirstResponder()
            selectState()
        }
        
        if textField == cityTextField {
            textField.resignFirstResponder()
            if let selectedState = stateTextField.text, selectedState.isNotEmpty {
                selectCity(uf: selectedState)
            } else {
                selectState { stateInitials in
                    DispatchQueue.main.async {
                        self.selectCity(uf: stateInitials)
                    }
                }
            }
        }
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if [stateTextField, cityTextField].contains(textField) {
            return false
        }
        
        let shouldChange = super.textField(textField, shouldChangeCharactersIn: range, replacementString: string)
        
        if textField.tag == 1 {
            validateCepTextField(textfield: textField, inRange: range, replacementString: string, shouldChange: shouldChange)
        } else if let length = (textField as? UIPPFloatingTextField)?.maxlength {
            return validateTextField(withMaxLength: length, textField: textField, inRange: range, replacementString: string)
        }
        
        return shouldChange
    }
    
    private func saveFormData() {
        guard let vModel = viewModel else {
            return
        }
        vModel.syncData()
        
        guard !self.isFormEdition() else {
            return
        }
        
        CPRegistrationCoordinator.shared?.pushController(with: vModel.creditForm)
    }
    
    private func loadAddress(_ cep: String) {
        let zipCode = cep.onlyNumbers
        self.cep = zipCode
        cepTextField.isEnabled = false
        
        loadingIndicator.isHidden = false
        
        viewModel?.loadAddress(by: self.cep) { [weak self] error in
            guard let strongSelf = self else {
                return
            }
            strongSelf.cepTextField.isEnabled = true
            strongSelf.loadingIndicator.isHidden = true
            
            strongSelf.addressTextField.becomeFirstResponder()
            if error != nil {
                strongSelf.didPostalCodeError()
            } else {
                strongSelf.populateForm()
            }
            
            strongSelf.form(isLock: false)
            strongSelf.validate()
        }
    }
    
    private func didPostalCodeError() {
        stateTextField.text = nil
        cityTextField.text = nil
        neighborhoodTextField.text = nil
        addressTextField.text = nil
        numberTextField.text = nil
        enableStateAndCityEdition()
    }
    
    private func validateCepTextField(textfield field: UITextField, inRange range: NSRange, replacementString string: String, shouldChange: Bool) {
        let oldString = field.text ?? ""
        var finalText = oldString
        
        if shouldChange {
            finalText = "\(oldString)\(string)"
        }
        
        if finalText.length == 9 {
            loadAddress(finalText.onlyNumbers)
        } else {
            emptyForm()
            form(isLock: true)
        }
    }
    
    private func validateTextField(withMaxLength length: Int, textField field: UITextField, inRange range: NSRange, replacementString string: String) -> Bool {
        return !(field.text?.count > length - 1 && (string.count) > range.length)
    }
}

extension OldCreditAddressViewController {
    private func prepareLayout() {
        residentialTitleLabel.textColor = Palette.ppColorGrayscale500.color
        cityTextField.isEnabled = false
        stateTextField.isEnabled = false
        loadingIndicator.isHidden = true
        noNumberHomeAddressButton.setImage(#imageLiteral(resourceName: "grayCheckmark"), for: .normal)
        noNumberHomeAddressButton.setImage(#imageLiteral(resourceName: "iconChecked"), for: .selected)
        noNumberHomeAddressButton.setTitleColor(Palette.hexColor(with: "#BBBBBB"), for: .normal)
        noNumberHomeAddressButton.setTitleColor(Palette.ppColorGrayscale600.color, for: .selected)
    }
    
    private func populateForm() {
        guard let homeAddress = viewModel?.creditForm.homeAddress else {
            return
        }
        let streetName: String
        if !homeAddress.streetType.isEmpty {
            streetName = "\(homeAddress.streetType ?? "") \(homeAddress.street ?? "")"
        } else {
            streetName = homeAddress.street ?? ""
        }
        cepTextField.text = format(zipCode: homeAddress.zipCode)
        addressTextField.text = streetName
        complementTextField.text = homeAddress.addressComplement
        neighborhoodTextField.text = homeAddress.neighborhood
        cityTextField.text = homeAddress.city
        stateTextField.text = homeAddress.state
        if let number = homeAddress.streetNumber, number.trim() == "0" {
            noNumberHomeAddressButton.isSelected = true
            numberTextField.text = ""
            numberTextField.isEnabled = false
        } else {
            numberTextField.text = homeAddress.streetNumber
        }
        configureTextNumberValidation()
        enableStateAndCityEdition()
    }
    
    private func enableStateAndCityEdition() {
        let state = stateTextField.text ?? ""
        let city = cityTextField.text ?? ""
        let canEditFields = state.isEmpty || city.isEmpty
        cityTextField.isEnabled = canEditFields
        stateTextField.isEnabled = canEditFields
    }
    
    private func selectState(completion: ((String) -> Void)? = nil) {
        let viewController = SelectRegionFactory.make(type: .state) { [weak self] state in
            guard let self = self else {
                return
            }
            self.cityTextField.text = nil
            self.stateTextField.text = state.getSelectionValue()
            self.viewModel?.setState(id: state.getId(), name: state.getSelectionValue())
            self.validate()
            completion?(state.getSelectionValue())
        }
        
        viewController.setCustomSearchBarColor(color: Palette.ppColorGrayscale100.color)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func selectCity(uf: String) {
        let viewController = SelectRegionFactory.make(type: .city(uf: uf)) { [weak self] city in
            guard let self = self else {
                return
            }
            
            self.cityTextField.text = city.getSelectionValue()
            self.viewModel?.setCity(id: city.getId(), name: city.getSelectionValue())
            self.validate()
        }
        
        viewController.setCustomSearchBarColor(color: Palette.ppColorGrayscale100.color)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func configureForm() {
        addTextField(cepTextField)
        addTextField(addressTextField)
        addTextField(numberTextField)
        addTextField(complementTextField)
        addTextField(neighborhoodTextField)
        addTextField(cityTextField)
        addTextField(stateTextField)
        configTextFieldsInpuAccessory()
        configureValidations()
        /// Configure CEP text fields for delegate
        cepTextField.tag = 1
        cepTextField.delegate = self
    }
    
    private func configTextFieldsInpuAccessory() {
        let doneToolBar = DoneToolBar(doneText: "Ok")
        cepTextField.inputAccessoryView = doneToolBar
        addressTextField.inputAccessoryView = doneToolBar
        numberTextField.inputAccessoryView = doneToolBar
        complementTextField.inputAccessoryView = doneToolBar
        neighborhoodTextField.inputAccessoryView = doneToolBar
        cityTextField.inputAccessoryView = doneToolBar
        stateTextField.inputAccessoryView = doneToolBar
    }
    
    private func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case addressTextField:
            numberTextField.becomeFirstResponder()
        case numberTextField:
            complementTextField.becomeFirstResponder()
        case complementTextField:
            neighborhoodTextField.becomeFirstResponder()
        default:
            return false
        }
        return true
    }
    
    private func validationTextRules(
        min: Int = 0, 
        emptyError: CreditAddressViewModel.ValidateError,
        invalidError: CreditAddressViewModel.ValidateError
    ) -> ValidationRuleSet<String> {
        var validationRules = ValidationRuleSet<String>()
        validationRules.add(rule: ValidationRuleLength(min: min, error: emptyError))
        let textPattern = "[A-zÀ-ú][A-zÀ-ú0-9,. ]+|^$"
        validationRules.add(rule: ValidationRulePattern(pattern: textPattern, error: invalidError))
        return validationRules
    }
    
    private func configureTextNumberValidation() {
        numberTextField.validationRules = RegisterNumberAddressValidator.numberRules(noNumberIsChecked: noNumberHomeAddressButton.isSelected)
    }
    
    private func configureValidations() {
        var cepRules = ValidationRuleSet<String>()
        cepRules.add(rule: ValidationRuleLength(min: cepTextLength, error: CreditAddressViewModel.ValidateError.cepRequired))
        
        numberTextField.maxlength = numberTextFieldMaxLength
        addressTextField.maxlength = addressTextMaxLength
        neighborhoodTextField.maxlength = neighborhoodTextMaxLength
        complementTextField.maxlength = complementTextMaxLength
        cityTextField.maxlength = cityTextMaxLength
        stateTextField.maxlength = stateTextMaxLength
        complementTextField.validationRules = RegisterComplementAddressValidator.rules()
        cepTextField.validationRules = cepRules
        cepTextField.maskString = "00000-000"
        addressTextField.validationRules = RegisterStreetAddressValidator.rules()
        neighborhoodTextField.validationRules = RegisterNeighborhoodAddressValidator.rules()
        cityTextField.validationRules = RegisterCityAddressValidator.rules()
        stateTextField.validationRules = RegisterStateAddressValidator.rules()
    }
    
    // MARK: - Form Methods
    private func emptyForm() {
        addressTextField.text = ""
        neighborhoodTextField.text = ""
        complementTextField.text = ""
        numberTextField.text = ""
        cityTextField.text = ""
        stateTextField.text = ""
        addressTextField.errorMessage = nil
        neighborhoodTextField.errorMessage = nil
        complementTextField.errorMessage = nil
        numberTextField.errorMessage = nil
        cityTextField.errorMessage = nil
        stateTextField.errorMessage = nil
    }
    
    private func form(isLock: Bool) {
        addressTextField.isEnabled = !isLock
        neighborhoodTextField.isEnabled = !isLock
        numberTextField.isEnabled = !isLock
        complementTextField.isEnabled = !isLock
    }
    
    private func format(zipCode: String?) -> String {
        if let zip = zipCode {
            if zip.length == 8 {
                let prefix = zip.chopSuffix(3)
                let sufix = zip.chopPrefix(5)
                return "\(prefix)-\(sufix)"
            }
        }
        return ""
    }
    // swiftlint:disable:next file_length
}
