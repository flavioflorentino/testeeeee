public enum CreditAboutAction {
    case wantToRegister
    case close
}

public protocol CreditAboutCoordinatorProtocol: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CreditAboutAction)
}

public final class CreditAboutCoordinator: CreditAboutCoordinatorProtocol {
    weak public var viewController: UIViewController?
    
    private let creditForm: CreditForm
    
    init(with creditForm: CreditForm) {
        self.creditForm = creditForm
    }

    public func perform(action: CreditAboutAction) {
        switch action {
        case .wantToRegister:
            CPRegistrationCoordinator.shared?.pushController(with: creditForm)

        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
