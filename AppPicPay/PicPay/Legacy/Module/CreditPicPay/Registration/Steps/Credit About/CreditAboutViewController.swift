import UI
import UIKit
import WebKit
import Core

public final class CreditAboutViewController: LegacyViewController<CreditAboutViewModelProtocol, CreditAboutCoordinatorProtocol, UIView> {
    private enum Layout {
        static let buttonMargins: CGFloat = 20
        static let buttonHeight: CGFloat = 44
        static let spacing: CGFloat = 10
        static let loadingLineHeight: CGFloat = 3
    }
    
    private lazy var webView: WKWebView = {
        let webview = WKWebView(frame: .zero)
        webview.translatesAutoresizingMaskIntoConstraints = false
        webview.isUserInteractionEnabled = true
        webview.isMultipleTouchEnabled = true
        webview.navigationDelegate = self
        return webview
    }()
    
    private lazy var loadingLineView: LoadingTableViewLine = {
        let loadingLine = LoadingTableViewLine(width: UIScreen.main.bounds.width)
        loadingLine.translatesAutoresizingMaskIntoConstraints = false
        loadingLine.isHidden = true
        return loadingLine
    }()
    
    private lazy var registerButton: UIPPButton = {
        let button = UIPPButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(wantToRegisterTapped), for: .touchUpInside)
        return button
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        title = CreditPicPayLocalizable.featureName.text
        viewModel.inputs.viewDidLoad()
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        PPAnalytics.trackEvent(CreditPicPayEvent.Registration.pageViewAbout, properties: nil)
    }
    
    override public func buildViewHierarchy() {
        view.addSubview(webView)
        view.addSubview(loadingLineView)
        view.addSubview(registerButton)
    }
    
    override public func configureViews() {
        if #available(iOS 13.0, *) {
            webView.scrollView.contentInsetAdjustmentBehavior = .never
        }
        
        view.backgroundColor = Palette.ppColorGrayscale000.color
        configureNavigationItem()
    }
    
    override public func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [loadingLineView])
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [registerButton], constant: Layout.buttonMargins)
        
        NSLayoutConstraint.activate([
            loadingLineView.topAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.topAnchor),
            loadingLineView.heightAnchor.constraint(equalToConstant: Layout.loadingLineHeight)
        ])
        
        NSLayoutConstraint.activate([
            registerButton.topAnchor.constraint(equalTo: webView.bottomAnchor, constant: Layout.spacing),
            registerButton.bottomAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.bottomAnchor),
            registerButton.heightAnchor.constraint(equalToConstant: Layout.buttonHeight)
        ])
        
        NSLayoutConstraint.activate([
            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            webView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            webView.topAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.topAnchor)
        ])
    }
    
    private func configureNavigationItem() {
        let leftItem = UIBarButtonItem(title: DefaultLocalizable.btClose.text, style: .plain, target: self, action: #selector(close))
        navigationItem.leftBarButtonItem = leftItem
    }
    
    private func startLoadingIndicator() {
        loadingLineView.isHidden = false
        loadingLineView.startAnimating()
    }
    
    private func stopLoadingIndicator() {
        loadingLineView.isHidden = true
    }
    
    @objc
    private func close() {
        coordinator.perform(action: .close)
    }
    
    @objc
    private func wantToRegisterTapped() {
        coordinator.perform(action: .wantToRegister)
        PPAnalytics.trackEvent(CreditPicPayEvent.Registration.registerButtonAbout, properties: nil)
    }
}

extension CreditAboutViewController: CreditAboutViewModelOutputs {
    public func setup(button: Button) {
        registerButton.configure(with: button)
    }
    
    public func load(request: URLRequest) {
        webView.load(request)
    }
}

extension CreditAboutViewController: WKNavigationDelegate {
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation?) {
        startLoadingIndicator()
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation?) {
        stopLoadingIndicator()
    }
    
    public func webView(
        _ webView: WKWebView,
        didReceive challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    ) {
        let pinningHandler = PinningHandler()
        pinningHandler.handle(challenge: challenge, completionHandler: completionHandler)
    }
}
