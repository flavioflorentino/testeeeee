import Foundation
import SwiftyJSON

public protocol CreditAboutViewModelInputs {
    func viewDidLoad()
}

public protocol CreditAboutViewModelOutputs: AnyObject {
    func setup(button: Button)
    func load(request: URLRequest)
}

public protocol CreditAboutViewModelProtocol: AnyObject {
    var inputs: CreditAboutViewModelInputs { get }
    var outputs: CreditAboutViewModelOutputs? { get set }
}

public final class CreditAboutViewModel: CreditAboutViewModelProtocol, CreditAboutViewModelInputs {
    public var inputs: CreditAboutViewModelInputs { return self }

    weak public var outputs: CreditAboutViewModelOutputs?
    private let service: CreditAboutServiceProtocol

    init(with service: CreditAboutServiceProtocol) {
        self.service = service
    }
}

extension CreditAboutViewModel {
    public func viewDidLoad() {
        let dict = service.featureConfigJson
        
        guard let urlString = dict?["url"] as? String,
            let url = URL(string: urlString),
            let dictButton: [String: Any] = dict?["button"] as? [String: Any],
            let button = Button(json: JSON(dictButton))
            else { return }
        
        outputs?.setup(button: button)
        outputs?.load(request: URLRequest(url: url))
    }
}
