import Foundation
import FeatureFlag

protocol CreditAboutServiceProtocol {
    var featureConfigJson: [String: Any]? { get }
}

final class CreditAboutService: CreditAboutServiceProtocol {
    var featureConfigJson: [String: Any]? {
        return FeatureManager.json(.creditpicpayOfferWebview)
    }
}
