import Foundation

public final class CreditAboutFactory {
    public static func make(with creditForm: CreditForm) -> CreditAboutViewController {
        let service: CreditAboutServiceProtocol = CreditAboutService()
        let viewModel: CreditAboutViewModelProtocol = CreditAboutViewModel(with: service)
        let coordinator: CreditAboutCoordinatorProtocol = CreditAboutCoordinator(with: creditForm)
        let viewController = CreditAboutViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
