import Core
import Foundation

enum CreditEndpoint {
    case disclaimer(invoiceId: String, paymentValue: Double)
}

extension CreditEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case let .disclaimer(invoiceId, _):
            return "credit/invoice/\(invoiceId)/payment-msg"
        }
    }
    
    var parameters: [String: Any] {
        switch self {
        case let .disclaimer(_, paymentValue):
            return [Parameters.value: paymentValue]
        }
    }
}

private extension CreditEndpoint {
    enum Parameters {
        static let value = "payment_value"
    }
}
