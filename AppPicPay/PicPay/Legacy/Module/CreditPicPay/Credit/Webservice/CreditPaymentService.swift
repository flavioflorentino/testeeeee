import Core
import FeatureFlag
import Foundation
import PCI

protocol CreditPaymentServicing {
    func payInvoice(
        password: String,
        invoiceId: String,
        cvv: String?,
        payload: InvoiceCardPayload,
        completion: @escaping (Result<CPPaymentResponse, PicPayError>) -> Void
    )
}

final class CreditPaymentService: BaseApi, CreditPaymentServicing {
    typealias Dependencies = HasMainQueue & HasFeatureManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func payInvoice(
        password: String,
        invoiceId: String,
        cvv: String?,
        payload: InvoiceCardPayload,
        completion: @escaping (Result<CPPaymentResponse, PicPayError>) -> Void
    ) {
        
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let url = kWsUrlCreditPicpayInvoicePayment.replacingOccurrences(of: "{id}", with: invoiceId)
            let parameters = payload.toDictionary() ?? [:]
            requestManager.apiRequest(endpoint: url, method: .post, parameters: parameters, pin: password).responseApiObject { [weak self] (result: PicPayResult<CPPaymentResponse>) in
                self?.dependencies.mainQueue.async {
                    switch result {
                    case .success(let value):
                        completion(.success(value))
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
            }
            return
        }
        
        // TODO: - adicionar helper para o core network
    }
}
