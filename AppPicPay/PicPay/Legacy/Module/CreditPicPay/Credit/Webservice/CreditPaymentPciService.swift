import Core
import Foundation
import SwiftyJSON
import PCI

final class CreditPaymentPciService: CreditPaymentServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    private let pciService: InvoiceCardServiceProtocol
    
    init(pciService: InvoiceCardServiceProtocol = InvoiceCardService(), dependencies: Dependencies = DependencyContainer()) {
        self.pciService = pciService
        self.dependencies = dependencies
    }
    
    func payInvoice(
        password: String,
        invoiceId: String,
        cvv: String?,
        payload: InvoiceCardPayload,
        completion: @escaping (Result<CPPaymentResponse, PicPayError>) -> Void
    ) {
        let cvv = CVVPayload(value: cvv)
        let paymentPayload = PaymentPayload<InvoiceCardPayload>(cvv: cvv, generic: payload)
        
        pciService.createTransaction(
            password: password,
            invoiceId: invoiceId,
            payload: paymentPayload,
            isNewArchitecture: false
        ) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let response):
                    let receipts = JSON(response.receipt)
                    let transaction = JSON(response.transaction)
                    let statusName = response.statusName
                    
                    guard let paymentResponse = CPPaymentResponse(statusName: statusName, receipts: receipts, transaction: transaction) else {
                        let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
                        completion(.failure(error))
                        return
                    }

                    completion(.success(paymentResponse))
                    
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
}
