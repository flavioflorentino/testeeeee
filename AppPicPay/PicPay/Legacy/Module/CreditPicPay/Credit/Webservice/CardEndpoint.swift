import Core

enum CreditServiceEndpoint {
    case biometricProccess(isLoanFlow: Bool)
    case patrimony
    case documentTypes
    case documentTypeForUser(isLoanFlow: Bool)
}

extension CreditServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .biometricProccess:
            return "credit/registration/biometric/check-process"
        case .patrimony:
            return "credit/domain/patrimony"
        case .documentTypes:
            return "credit/domain/document"
        case .documentTypeForUser:
            return "credit/registration/user-document-type"
        }
    }
    
    var customHeaders: [String: String] {
        switch self {
        case let .biometricProccess(isLoanFlow), let .documentTypeForUser(isLoanFlow: isLoanFlow):
            return ["loan_flow": isLoanFlow.description]
        default:
            return [:]
        }
    }
}
