import Core
import FeatureFlag
import UIKit
import SwiftyJSON

protocol CreditApiProtocol {
    var defauldCardId: String { get }
    var defauldCard: CardBank? { get }
    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool
    func saveCvvCard(id: String, value: String?)
    func cvvCard(id: String) -> String?
    func invoiceDetail(invoiceId: String, completion: @escaping (PicPayResult<CreditInvoiceDetail>) -> Void)
    func account(complete: Bool, isLoanFlow: Bool, onCacheLoaded: @escaping (CPAccount) -> Void, completion: @escaping (PicPayResult<CPAccount>) -> Void)
    func sendInvoiceEmail(invoiceId: String, completion: @escaping (PicPayResult<BaseApiEmptyResponse>) -> Void)
}

final class CreditPicpayApi: BaseApi, CreditApiProtocol {
    typealias Dependencies = HasFeatureManager & HasCreditCardManager & HasMainQueue & HasKeychainManager
    private let dependencies: Dependencies
    
    var defauldCardId: String {
        let card = dependencies.creditCardManager.defaultCreditCard
        return card?.id ?? "0"
    }
    
    var defauldCard: CardBank? {
        dependencies.creditCardManager.defaultCreditCard
    }
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool {
        CvvRegisterFlowCoordinator.cvvFlowEnable(cardBank: cardBank, cardValue: cardValue, paymentType: .invoice)
    }
    
    func saveCvvCard(id: String, value: String?) {
        dependencies.keychain.set(key: KeychainKeyPF.cvv(id), value: value)
    }
    
    func cvvCard(id: String) -> String? {
        return dependencies.keychain.getData(key: KeychainKeyPF.cvv(id))
    }
    
    func account(complete: Bool = false,
                 isLoanFlow: Bool = false,
                 onCacheLoaded: @escaping ((CPAccount) -> Void),
                 completion: @escaping ((PicPayResult<CPAccount>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let cacheKey = complete ? "credit_account_complete_true" : "credit_account_complete_false"
            let params: [String: Any] = ["complete": complete]
            let headers: [String: String] = ["loan_flow": isLoanFlow.description]
            requestManager.apiRequestWithCache(cacheKey, onCacheLoaded: { account in
                DispatchQueue.main.async {
                    onCacheLoaded(account)
                }
            }, endpoint: kWsUrlCreditPicpayAccount, method: .get, parameters: params, headers: headers)
            .responseApiObject { result in
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            return
        }
        
        // TODO: - adicionar helper para o core network
    }
    
    func timeline(page: Int, size: Int, _ completion: @escaping ((PicPayResult<[CPFeedItem]>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            var params: [String: Any] = [:]
            params["page"] = page
            params["size"] = size
            requestManager
                .apiRequest(endpoint: kWsUrlCreditPicpayTimeline, method: .get, parameters: params)
                .responseApiCodable(completionHandler: completion)
            return
        }
        
        // TODO: - adicionar helper para o core network
    }
    
    func invoices(completion: @escaping ((PicPayResult<[CreditInvoice]>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlCreditPicpayInvoice, method: .get)
                .responseApiCodable(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func invoiceDetail(invoiceId: String, completion: @escaping ((PicPayResult<CreditInvoiceDetail>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let url = kWsUrlCreditPicpayInvoiceDetail.replacingOccurrences(of: "{id}", with: invoiceId)
            requestManager
                .apiRequest(endpoint: url, method: .get)
                .responseApiCodable(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func invoicePayment(invoiceId: String, completion: @escaping ((PicPayResult<CreditInvoicePayment>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let url = kWsUrlCreditPicpayInvoicePayment.replacingOccurrences(of: "{id}", with: invoiceId)
            requestManager
                .apiRequest(endpoint: url, method: .get)
                .responseApiCodable(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func payInvoice(invoiceId: String, params: [String: Any], completion: @escaping (PicPayResult<CPPaymentResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let url = kWsUrlCreditPicpayInvoicePayment.replacingOccurrences(of: "{id}", with: invoiceId)
            requestManager
                .apiRequest(endpoint: url, method: .post, parameters: params)
                .responseApiObject { [weak self] result in
                    self?.dependencies.mainQueue.async {
                        completion(result)
                    }
                }
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func invoiceDownload(invoiceId: String, completion: @escaping (PicPayResult<Data>) -> Void) {
        let url = kWsUrlCreditPicpayInvoiceDonwload.replacingOccurrences(of: "{id}", with: invoiceId)
        guard let urlURL = URL(string: url) else {
            let result = PicPayResult<Data>.failure(PicPayError(message: "Ops! Ocorreu um erro. Tente novamente."))
            completion(result)
            return
        }
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: urlURL) {
                let result = PicPayResult<Data>.success(data)
                completion(result)
            } else {
                let result = PicPayResult<Data>.failure(PicPayError(message: "Ops! Ocorreu um erro. Tente novamente."))
                completion(result)
            }
        }
    }
    
    func sendInvoiceEmailBoleto(invoiceId: String, completion: @escaping (PicPayResult<BaseApiEmptyResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let url = kWsUrlCreditPicpayInvoiceEmailBoleto.replacingOccurrences(of: "{id}", with: invoiceId)
            requestManager
                .apiRequest(endpoint: url, method: .post)
                .responseApiObject { [weak self] result in
                    self?.dependencies.mainQueue.async {
                        completion(result)
                    }
                }
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func sendInvoiceEmail(invoiceId: String, completion: @escaping ((PicPayResult<BaseApiEmptyResponse>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let url = kWsUrlCreditPicpayInvoiceSendEmail.replacingOccurrences(of: "{id}", with: invoiceId)
            requestManager
                .apiRequest(endpoint: url, method: .post)
                .responseApiObject { [weak self] result in
                    self?.dependencies.mainQueue.async {
                        completion(result)
                    }
                }
            return
        }
        // TODO: - adicionar helper para o core network
    }
}

