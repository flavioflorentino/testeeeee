import Foundation
import UI

struct InvoiceDetailValueViewModel {
    let value: Double
    let dueDate: Date
    let closingDate: Date
    let minimumPayment: Double?
    let invoiceStatus: CreditInvoice.Status
    
    var valueFormatted: String {
        return value >= 0 ? value.stringAmount : 0.0.stringAmount
    }
    var hasClosingDate: Bool {
        return !hasMinimumPayment
    }
    var hasMinimumPayment: Bool {
        return invoiceStatus != .open && minimumPayment != nil && minimumPayment != 0
    }
    var closingDateFormatted: NSAttributedString {
        let text = String(format: CreditPicPayLocalizable.invoiceDetailClosingDate.text, closingDate.stringFormatted(with: .ddDeMMMM))
        return NSMutableAttributedString(string: text)
    }
    var dueDateFormatted: NSAttributedString {
        let color = invoiceStatus == .delayed ? Palette.ppColorNegative300.color : Palette.ppColorBranding300.color
        let text = String(format: CreditPicPayLocalizable.invoiceDetailDueDate.text, dueDate.stringFormatted(with: .ddDeMMMM))
        let location = 10
        let range = NSRange(location: location, length: text.count - location)
        let attributed = NSMutableAttributedString(string: text)
        attributed.addAttributes([NSAttributedString.Key.foregroundColor: color], range: range)
        return attributed
    }
    var minimumPaymentFormatted: NSAttributedString? {
        guard let minimumPayment = self.minimumPayment else {
     return nil
}
        let text = String(format: CreditPicPayLocalizable.invoiceDetailMinimumPayment.text, minimumPayment.stringAmount)
        return NSAttributedString(string: text)
    }
}
