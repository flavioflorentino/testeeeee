import Foundation

struct InvoiceDetailPaymentInformationViewModel {
    let receivedPayment: Double?
    let earlyPayment: Double?
    let totalDebit: Double?
}
