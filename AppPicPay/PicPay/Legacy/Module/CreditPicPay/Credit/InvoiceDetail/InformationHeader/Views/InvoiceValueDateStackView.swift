import UI
import UIKit

final class InvoiceValueDateStackView: UIStackView {
    private(set) lazy var bigTitle: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale600.color
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 30.0, weight: .medium)
        label.text = viewModel.valueFormatted
        return label
    }()
    private(set) lazy var dueDateLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        label.attributedText = viewModel.dueDateFormatted
        return label
    }()
    private(set) lazy var closingDateLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        label.attributedText = viewModel.closingDateFormatted
        return label
    }()
    private(set) lazy var minimumPaymentLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale400.color
        label.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        label.textAlignment = .center
        label.attributedText = viewModel.minimumPaymentFormatted
        return label
    }()
    
    private let viewModel: InvoiceDetailValueViewModel
    
    init(with viewModel: InvoiceDetailValueViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        layoutMargins = UIEdgeInsets(top: 19, left: 0, bottom: 9, right: 0)
        isLayoutMarginsRelativeArrangement = true
        spacing = 8.0
        distribution = .fill
        axis = .vertical
        addArrangedSubview(bigTitle)
        addArrangedSubview(dueDateLabel)
        if viewModel.hasClosingDate {
            addArrangedSubview(closingDateLabel)
        }
        if viewModel.hasMinimumPayment {
            addArrangedSubview(minimumPaymentLabel)
        }
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
