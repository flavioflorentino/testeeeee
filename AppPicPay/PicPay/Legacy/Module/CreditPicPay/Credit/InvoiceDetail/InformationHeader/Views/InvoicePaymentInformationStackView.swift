import UI
import UIKit

final class InvoicePaymentInformationStackView: UIStackView {
    private lazy var totalDebitLabelValue: LabelAndValueStackView = {
        let stack = LabelAndValueStackView()
        stack.left.textColor = Palette.ppColorGrayscale400.color
        stack.left.text = CreditPicPayLocalizable.invoiceDebitTotal.text
        stack.right.textColor = Palette.ppColorGrayscale400.color
        return stack
    }()
    private lazy var earlyPayment: LabelAndValueStackView = {
        let stack = LabelAndValueStackView()
        stack.left.textColor = Palette.ppColorGrayscale400.color
        stack.left.text = CreditPicPayLocalizable.invoiceEarlyPayment.text
        stack.right.textColor = Palette.ppColorBranding300.color
        stack.right.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        return stack
    }()
    private lazy var receivedPaymentLabelValue: LabelAndValueStackView = {
        let stack = LabelAndValueStackView()
        stack.left.textColor = Palette.ppColorBranding300.color
        stack.left.text = CreditPicPayLocalizable.invoiceReceivedPayment.text
        stack.left.font = UIFont.systemFont(ofSize: 13, weight: .bold)
        stack.right.textColor = Palette.ppColorBranding300.color
        stack.right.font = UIFont.systemFont(ofSize: 13, weight: .bold)
        return stack
    }()
    
    private let viewModel: InvoiceDetailPaymentInformationViewModel
    
    init(with viewModel: InvoiceDetailPaymentInformationViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        setupStackView()
        setupViewModel()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupStackView() {
        spacing = 8.0
        axis = .vertical
        distribution = .fill
        layoutMargins = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        isLayoutMarginsRelativeArrangement = true
    }
    
    private func setupViewModel() {
        if let value = viewModel.totalDebit {
            totalDebitLabelValue.right.text = value.stringAmount
            addArrangedSubview(totalDebitLabelValue)
        }
        if let value = viewModel.earlyPayment {
            earlyPayment.right.text = value.stringAmount
            addArrangedSubview(earlyPayment)
        }
        if let value = viewModel.receivedPayment {
            receivedPaymentLabelValue.right.text = value.stringAmount
            addArrangedSubview(receivedPaymentLabelValue)
        }
    }
}
