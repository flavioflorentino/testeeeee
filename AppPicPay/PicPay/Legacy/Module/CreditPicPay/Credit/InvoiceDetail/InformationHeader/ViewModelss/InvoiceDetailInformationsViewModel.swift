import Foundation
import UI

protocol InvoiceDetailInformationsDelegate: AnyObject {
    func sendInvoiceEmail()
    func pay()
}

final class InvoiceDetailInformationsViewModel {
    let invoice: CreditInvoice
    let delegate: InvoiceDetailInformationsDelegate
    
    var invoiceValueDateViewModel: InvoiceDetailValueViewModel {
        return InvoiceDetailValueViewModel(value: invoice.totalValue,
                                           dueDate: invoice.dueDate,
                                           closingDate: invoice.closingDate,
                                           minimumPayment: invoice.minimumPayment,
                                           invoiceStatus: invoice.status)
    }
    var invoicePaymentInformationViewModel: InvoiceDetailPaymentInformationViewModel? {
        guard invoice.earlyPayment != nil ||
            invoice.receivedPayment != nil ||
            invoice.totalDebit != nil else {
     return nil
}
        return InvoiceDetailPaymentInformationViewModel(receivedPayment: invoice.receivedPayment,
                                                        earlyPayment: invoice.earlyPayment,
                                                        totalDebit: invoice.totalDebit)
    }
    var hasActionButton: Bool {
        switch invoice.status {
        case .open, .openPayment:
            return invoice.payable
        case .closed, .delayed:
            return true
        }
    }
    var actionButtonConfig: (title: String, titleColor: UIColor, normalColor: UIColor, borderColor: UIColor) {
        guard invoice.payable else {
            return (CreditPicPayLocalizable.btnInvoiceSendToEmail.text, Palette.ppColorGrayscale400.color, .clear, Palette.ppColorGrayscale400.color)
        }
        var title = CreditPicPayLocalizable.payInvoice.text
        if invoice.status == .delayed {
            return (title, Palette.white.color, Palette.ppColorNegative300.color, .clear)
        }
        if invoice.status == .open {
            title = CreditPicPayLocalizable.btInvoiceDetailEarlyPayment.text
        }
        return (title, Palette.white.color, Palette.ppColorBranding300.color, .clear)
    }
    
    init(with invoice: CreditInvoice, delegate: InvoiceDetailInformationsDelegate) {
        self.invoice = invoice
        self.delegate = delegate
    }
    
    func onTappedInvoice() {
        if !invoice.payable {
            delegate.sendInvoiceEmail()
            return
        }
        delegate.pay()
    }
}
