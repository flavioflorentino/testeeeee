import UI
import UIKit

final class InvoiceDetailInformationsView: UIView {
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 15.0
        stackView.distribution = .fill
        return stackView
    }()
    private lazy var invoiceValueDateStackView: UIStackView = {
        return InvoiceValueDateStackView(with: viewModel.invoiceValueDateViewModel)
    }()
    private lazy var invoicePaymentInformationView: UIStackView? = {
        guard let viewModel = viewModel.invoicePaymentInformationViewModel else {
     return nil
}
        return InvoicePaymentInformationStackView(with: viewModel)
    }()
    private lazy var line1: UIStackView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale200.color
        view.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        let stackView = UIStackView(arrangedSubviews: [view])
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 11, bottom: 0, right: 11)
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    private lazy var line2: UIStackView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale200.color
        view.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        let stackView = UIStackView(arrangedSubviews: [view])
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 11, bottom: 0, right: 11)
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    private lazy var separatorView: UIStackView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale100.color
        view.heightAnchor.constraint(equalToConstant: 8).isActive = true
        
        let line = UIView()
        line.backgroundColor = Palette.ppColorGrayscale200.color
        line.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        let stackView = UIStackView(arrangedSubviews: [line, view])
        stackView.axis = .vertical
        return stackView
    }()
    private lazy var submitButton: UIView = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.borderColor = viewModel.actionButtonConfig.borderColor.cgColor
        button.backgroundColor = viewModel.actionButtonConfig.normalColor
        button.layer.borderWidth = 1.0
        button.layer.cornerRadius = 22
        button.setTitle(viewModel.actionButtonConfig.title, for: .normal)
        button.setTitleColor(viewModel.actionButtonConfig.titleColor, for: .normal)
        button.addTarget(self, action: #selector(actionButtonTapped), for: .touchUpInside)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        button.heightAnchor.constraint(equalToConstant: 44).isActive = true
        button.isEnabled = true
        
        let view = UIView()
        view.addSubview(button)
        NSLayoutConstraint.activate([button.topAnchor.constraint(equalTo: view.topAnchor, constant: 3),
                                     button.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24),
                                     button.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -3),
                                     button.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24)])
        return view
    }()
    private let viewModel: InvoiceDetailInformationsViewModel
    
    init(with viewModel: InvoiceDetailInformationsViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        isUserInteractionEnabled = true
        backgroundColor = Palette.ppColorGrayscale000.color
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addComponents() {
        addSubview(rootStackView)
        rootStackView.addArrangedSubview(invoiceValueDateStackView)
        if let informationValues = invoicePaymentInformationView {
            rootStackView.addArrangedSubview(line1)
            rootStackView.addArrangedSubview(informationValues)
        }
        if viewModel.hasActionButton {
            rootStackView.addArrangedSubview(line2)
            rootStackView.addArrangedSubview(submitButton)
        }
        rootStackView.addArrangedSubview(separatorView)
    }

    private func layoutComponents() {
        NSLayoutConstraint.activate([
            rootStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            rootStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            rootStackView.topAnchor.constraint(equalTo: topAnchor),
            rootStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            ])
    }
    
    @objc
    private func actionButtonTapped() {
        viewModel.onTappedInvoice()
    }
}
