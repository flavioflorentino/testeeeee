import UI
import UIKit

extension CreditInvoiceDetailViewController {
    final class InvoiceDetailSectionHeaderView: UIView {
        private lazy var button: UIButton = {
            let button = UIButton()
            button.addTarget(self, action: #selector(onTapButtonAction), for: .touchUpInside)
            button.setImage(#imageLiteral(resourceName: "credit_arrow_circle_green"), for: .normal)
            NSLayoutConstraint.activate([button.heightAnchor.constraint(equalToConstant: 18.0),
                                         button.widthAnchor.constraint(equalToConstant: 18.0)])
            return button
        }()
        private lazy var label: UILabel = {
            let label = UILabel()
            label.text = CreditPicPayLocalizable.invoiceUnprocessedTransactionsSection.text
            label.textColor = Palette.ppColorGrayscale400.color
            label.font = UIFont.systemFont(ofSize: 12.0, weight: .medium)
            return label
        }()
        private lazy var mainStackView: UIStackView = {
            let stackView = UIStackView()
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.axis = .horizontal
            stackView.alignment = .fill
            stackView.distribution = .fill
            return stackView
        }()
        private var isOpenSection: Bool = true {
            didSet {
                UIView.animate(withDuration: 0.25) {
                    if self.isOpenSection {
                        self.button.transform = CGAffineTransform.identity
                    } else {
                        self.button.transform = CGAffineTransform(rotationAngle: -CGFloat.pi)
                    }
                }
            }
        }
        var onTapButton: () -> Void = {}
        
        override init(frame: CGRect) {
            super.init(frame: .zero)
            isUserInteractionEnabled = true
            backgroundColor = Palette.ppColorGrayscale000.color
            addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTapButtonAction)))
            addComponents()
            addLayout()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func addComponents() {
            addSubview(mainStackView)
            mainStackView.addArrangedSubview(label)
            mainStackView.addArrangedSubview(button)
        }
        
        private func addLayout() {
            NSLayoutConstraint.activate([mainStackView.topAnchor.constraint(equalTo: topAnchor, constant: 19.0),
                                         mainStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15.0),
                                         mainStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15.0),
                                         mainStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15.0)])
        }
        
        @objc
        private func onTapButtonAction() {
            isOpenSection = !isOpenSection
            onTapButton()
        }
    }
}
