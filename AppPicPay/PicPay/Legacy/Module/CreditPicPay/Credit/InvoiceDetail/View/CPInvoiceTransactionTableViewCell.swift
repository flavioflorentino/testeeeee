import UI
import UIKit
import SkeletonView

final class CPInvoiceTransactionTableViewCell: UITableViewCell {
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.isSkeletonable = true
        }
    }
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.isSkeletonable = true
        }
    }
    @IBOutlet weak var valueLabel: UILabel! {
        didSet {
            valueLabel.isSkeletonable = true
        }
    }

    var transaction: CreditInvoiceDetail.Transaction? {
        didSet {
            guard let transaction = self.transaction else {
            return
        }
            hideAnimationsSkeleton()
            dateLabel.text = transaction.dateStr
            descriptionLabel.text = transaction.label.value
            descriptionLabel.textColor = Palette.hexColor(with: transaction.label.color)
            valueLabel.text = transaction.value.value
            valueLabel.textColor = Palette.hexColor(with: transaction.value.color)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        backgroundColor = Palette.ppColorGrayscale000.color
        showAnimationsSkeleton()
    }
}

extension CPInvoiceTransactionTableViewCell {
    func showAnimationsSkeleton() {
        let skeletonGradient = SkeletonGradient(baseColor: Palette.ppColorGrayscale300.color.withAlphaComponent(0.2), secondaryColor: Palette.ppColorGrayscale300.color.withAlphaComponent(0.4))
        dateLabel.showAnimatedGradientSkeleton(usingGradient: skeletonGradient)
        descriptionLabel.showAnimatedGradientSkeleton(usingGradient: skeletonGradient)
        valueLabel.showAnimatedGradientSkeleton(usingGradient: skeletonGradient)
    }
    
    func hideAnimationsSkeleton() {
        dateLabel.hideSkeleton()
        descriptionLabel.hideSkeleton()
        valueLabel.hideSkeleton()
    }
}

fileprivate extension Date {
    var transactionDate: String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "dd MMM"
        return formatter.string(from: self)
    }
}
