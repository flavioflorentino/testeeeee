import Foundation

protocol CreditInvoiceDetailCoordinatorDelegate: AnyObject {
    func pay(with invoiceId: String?, and account: CPAccount?)
    func closeIfPresentedController()
    func popDetailViewController()
    func showAlertError(_ error: Error, completion: (() -> Void)?)
    func showAlert(_ alert: Alert, completion: @escaping (AlertPopupViewController, Button, UIPPButton) -> Void)
}

final class CreditInvoiceDetailCoordinator: Coordinator {
    var currentCoordinator: Coordinator?
    private let navigationController: UINavigationController
    private let isPresentView: Bool
    private let invoiceDetail: CreditInvoiceDetail?
    private let account: CPAccount?
    private let invoiceId: String?
    weak var delegate: CreditInvoiceDetailCoordinatorDelegate?
    
    
    init(with navigationController: UINavigationController,
         account: CPAccount,
         invoiceDetail: CreditInvoiceDetail,
         isPresentView: Bool = false) {
        self.navigationController = navigationController
        self.invoiceDetail = invoiceDetail
        self.account = account
        self.isPresentView = isPresentView
        
        invoiceId = nil
    }
    
    init(with navigationController: UINavigationController,
         invoiceId: String,
         isPresentView: Bool = false) {
        self.navigationController = navigationController
        self.invoiceId = invoiceId
        self.isPresentView = isPresentView
        
        invoiceDetail = nil
        account = nil
    }
    
    func start() {
        guard let controller = createInvoiceDetailController() else {
            return
        }
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func createInvoiceDetailController() -> CreditInvoiceDetailViewController? {
        if let invoiceDetail = self.invoiceDetail, let account = self.account {
            let viewModel = CreditInvoiceDetailViewModel(with: invoiceDetail,
                                                         account: account,
                                                         coordinatorDelegate: delegate,
                                                         isPresented: isPresentView)
            return CreditInvoiceDetailViewController(with: viewModel)
        } else if let invoiceId = self.invoiceId {
            let viewModel = CreditInvoiceDetailViewModel(with: invoiceId,
                                                         coordinatorDelegate: delegate,
                                                         isPresented: isPresentView)
            return CreditInvoiceDetailViewController(with: viewModel)
        }
        return nil
    }
    
    @objc
    private func close() {
        navigationController.dismiss(animated: true, completion: nil)
    }
}
