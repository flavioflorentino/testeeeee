import UI
import UIKit
import SecurityModule
import SnapKit
import SkeletonView

final class CreditInvoiceDetailViewController: PPBaseViewController, Obfuscable {
    private let viewModel: CreditInvoiceDetailViewModel
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isSkeletonable = true
        tableView.registerCell(type: CPInvoiceTransactionTableViewCell.self)
        tableView.backgroundColor = Palette.ppColorGrayscale100.color
        tableView.estimatedRowHeight = 48
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.refreshControl = pushRefreshControl
        return tableView
    }()
    private var detailHeaderInformationView: InvoiceDetailInformationsView? {
        guard let invoice = viewModel.invoiceDetail?.invoice else {
     return nil
}
        let invoiceDetailViewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: viewModel)
        let view = InvoiceDetailInformationsView(with: invoiceDetailViewModel)
        return view
    }
    private lazy var pushRefreshControl: UIRefreshControl = {
        let pushRefreshControl = UIRefreshControl()
        pushRefreshControl.tintColor = Palette.ppColorGrayscale500.color
        pushRefreshControl.addTarget(self, action: #selector(reload), for: .valueChanged)
        return pushRefreshControl
    }()
    private lazy var emptyView: UIView = {
        let backgroundView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 120))
        backgroundView.backgroundColor = Palette.ppColorGrayscale000.color
        let titleLabel: UILabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = viewModel.emptyTextViewString
        titleLabel.textColor = Palette.ppColorGrayscale500.color
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        backgroundView.addSubview(titleLabel)
        NSLayoutConstraint.activate([titleLabel.centerYAnchor.constraint(equalTo: backgroundView.centerYAnchor),
                                     titleLabel.leadingAnchor.constraint(equalTo: backgroundView.leadingAnchor, constant: 8),
                                     titleLabel.trailingAnchor.constraint(equalTo: backgroundView.trailingAnchor, constant: -8)])
        return backgroundView
    }()
    
    //Screen obfuscation
    var appSwitcherObfuscator: AppSwitcherObfuscating?
 
    init(with viewModel: CreditInvoiceDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        setupTitles()
        loadDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObfuscationConfiguration()
        viewModel.trackScreenViewEvent()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deinitObfuscator()
    }
}

extension CreditInvoiceDetailViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(type: CPInvoiceTransactionTableViewCell.self, forIndexPath: indexPath)
        if let transaction = viewModel.getTransaction(in: indexPath) {
            cell.hideAnimationsSkeleton()
            cell.transaction = transaction
        } else {
            cell.showAnimationsSkeleton()
        }
        return cell
    }
}

extension CreditInvoiceDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let section = CreditInvoiceDetailViewModel.Section(rawValue: section) else {
             return nil
        }
        switch section {
        case .information:
            guard let header = detailHeaderInformationView else {
                 return nil
            }
            return header
        case .unprocessedTransactions:
            guard !viewModel.isListLoading && !viewModel.unprocessedTransaction.isEmpty else {
                 return nil
            }
            let sectionView = InvoiceDetailSectionHeaderView()
            sectionView.onTapButton = { [weak self] in
                self?.prepareUnprocessedSectionRows()
            }
            return sectionView
        case .processedTransactions:
            return nil
        }
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 1.0))
        view.backgroundColor = Palette.ppColorGrayscale300.color
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return viewModel.heightForFooter(in: section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return viewModel.heightForHeader(in: section)
    }
}

extension CreditInvoiceDetailViewController: SkeletonTableViewDataSource {
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return String(describing: CPInvoiceTransactionTableViewCell.self)
    }
}

extension CreditInvoiceDetailViewController {
    private func setup(){
        title = viewModel.titleView
        view.backgroundColor = Palette.ppColorGrayscale000.color
        setupTableView()
        setupForPresentedView()
        setupViewModel()
    }

    private func setupTableView() {
        view.addSubview(tableView)
        NSLayoutConstraint.activate([tableView.topAnchor.constraint(equalTo: view.topAnchor),
                                     tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                                     tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                                     tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor)])
    }
    
    private func loadDetails() {
        viewModel.loadData { [weak self] in
            self?.pushRefreshControl.endRefreshing()
            self?.setupTitles()
        }
    }
    
    private func prepareUnprocessedSectionRows() {
        let isCollapsed = viewModel.isCollapsedUnprocessedTransactions
        let indexPaths = viewModel.unprocessedIndexPaths
        tableView.beginUpdates()
        viewModel.isCollapsedUnprocessedTransactions = !isCollapsed
        if !isCollapsed {
            tableView.deleteRows(at: indexPaths, with: .top)
        } else {
            tableView.insertRows(at: indexPaths, with: .top)
        }
        tableView.endUpdates()
    }
    private func setupTitles() {
        guard let date = viewModel.invoiceDetail?.invoice?.dueDate else {
            return
        }
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "MMM yyyy"
        title = formatter.string(from: date).capitalizingFirstLetter()
        tableView.tableFooterView = viewModel.isEmptyView ? emptyView : nil
        tableView.reloadData()
    }
    
    private func setupViewModel() {
        viewModel.sendInvoiceAction = { [weak self] in
            self?.startLoadingView()
            self?.viewModel.sendInvoiceEmail {
                self?.stopLoadingView()
            }
        }
    }
    
    private func setupForPresentedView() {
        guard viewModel.isPresented else {
            return
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.btClose.text,
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(closePresentedView))
    }

    @objc
    private func reload() {
        loadDetails()
    }
    
    @objc
    private func closePresentedView() {
        viewModel.closeIfPresented()
    }
}
