import UIKit

final class CreditInvoiceDetailViewModel {
    private let creditApi: CreditApiProtocol
    private var invoiceId: String?
    private let coordinatorDelegate: CreditInvoiceDetailCoordinatorDelegate?
    let isPresented: Bool
    let emptyTextViewString = CreditPicPayLocalizable.invoiceDetailNotransactions.text
    let titleView = CreditPicPayLocalizable.invoices.text
    var isListLoading = true
    var account: CPAccount?
    var invoiceDetail: CreditInvoiceDetail?
    var isDeeplink = false
    var needLoadInvoiceDetail = true
    var processedTranctions: [CreditInvoiceDetail.Transaction] = []
    var unprocessedTransaction: [CreditInvoiceDetail.Transaction] = []
    var unprocessedIndexPaths: [IndexPath] {
        var indexPaths: [IndexPath] = []
        for i in 0..<unprocessedTransaction.count {
            indexPaths.append(IndexPath(row: i, section: Section.unprocessedTransactions.rawValue))
        }
        return indexPaths
    }
    var isEmptyView: Bool {
        return processedTranctions.isEmpty && unprocessedTransaction.isEmpty && !isListLoading
    }
    var isCollapsedUnprocessedTransactions: Bool = false
    var sendInvoiceAction: () -> Void = {}
    lazy var sentBoletoAlert: Alert = {
        return Alert(title: CreditPicPayLocalizable.invoiceSent.text, text: CreditPicPayLocalizable.invoiceSentEmail.text)
    }()
    
    init(with invoiceId: String,
         coordinatorDelegate: CreditInvoiceDetailCoordinatorDelegate?,
         isPresented: Bool = false,
         creditApi: CreditApiProtocol = CreditPicpayApi()) {
        isDeeplink = true
        self.isPresented = isPresented
        self.creditApi = creditApi
        self.invoiceId = invoiceId
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    init(with invoiceDetail: CreditInvoiceDetail,
         account: CPAccount,
         coordinatorDelegate: CreditInvoiceDetailCoordinatorDelegate?,
         isPresented: Bool,
         creditApi: CreditApiProtocol = CreditPicpayApi()) {
        self.isPresented = isPresented
        self.creditApi = creditApi
        self.invoiceDetail = invoiceDetail
        self.account = account
        self.coordinatorDelegate = coordinatorDelegate
        if let invoiceId = invoiceDetail.invoice?.id {
            self.invoiceId = invoiceId
        }
    }
    
    func numberOfSections() -> Int {
        return 3
    }
    
    func numberOfRows(in section: Int) -> Int {
        guard let section = Section(rawValue: section) else { return 0 }
        switch section {
        case .information:
            return 0
        case .unprocessedTransactions:
            return (isCollapsedUnprocessedTransactions || isListLoading) ? 0 : unprocessedTransaction.count
        case .processedTransactions:
            /// number of rows in skeleton loading
            let numberOfSkeletonRows = 5
            return isListLoading ? numberOfSkeletonRows : processedTranctions.count
        }
    }
    
    func getTransaction(in indexPath: IndexPath) -> CreditInvoiceDetail.Transaction? {
        guard let section = Section(rawValue: indexPath.section),
            section != .information else {
     return nil
}
        
        let transactions = (section == .unprocessedTransactions) ? unprocessedTransaction : processedTranctions
        guard transactions.indices.contains(indexPath.row) else {
     return nil
}
        return transactions[indexPath.row]
    }
    
    func title(for section: Int) -> String? {
        guard let section = Section(rawValue: section),
            section == .unprocessedTransactions,
            !unprocessedTransaction.isEmpty else {
     return nil
}
        return CreditPicPayLocalizable.invoiceUnprocessedTransactionsSection.text
    }
    
    func heightForHeader(in section: Int) -> CGFloat {
        guard let section = Section(rawValue: section) else { return .leastNonzeroMagnitude }
        switch section {
        case .information:
            return UITableView.automaticDimension
        case .unprocessedTransactions:
            return (isListLoading || unprocessedTransaction.isEmpty) ? CGFloat.leastNonzeroMagnitude : UITableView.automaticDimension
        case .processedTransactions:
            return (isListLoading || processedTranctions.isEmpty) ? CGFloat.leastNonzeroMagnitude : 8.0
        }
    }
    
    func heightForFooter(in section: Int) -> CGFloat {
        guard let section = Section(rawValue: section) else { return .leastNonzeroMagnitude }
        switch section {
        case .information:
            return .leastNonzeroMagnitude
        case .unprocessedTransactions:
            return (isListLoading || unprocessedTransaction.isEmpty) ? .leastNonzeroMagnitude : 1.0
        case .processedTransactions:
            return (isListLoading || processedTranctions.isEmpty) ? .leastNonzeroMagnitude : 1.0
        }
    }
    
    func loadData(_ completion: @escaping () -> Void) {
        guard isDeeplink else {
            loadDetails(completion)
            return
        }
        
        loadAccount { [weak self] (error) in
            guard let error = error else {
                self?.loadDetails(completion)
                return
            }
            self?.coordinatorDelegate?.showAlertError(error, completion: nil)
        }
    }
    
    func sendInvoiceEmail(_ completion: @escaping () -> Void) {
        guard let invoiceId = self.invoiceId else {
            let error = PicPayError(message: CreditPicPayLocalizable.weWereUnableVerifyYourInvoice.text)
            coordinatorDelegate?.showAlertError(error, completion: nil)
            return
        }
        creditApi.sendInvoiceEmail(invoiceId: invoiceId) { [weak self] result in
            switch result{
            case .success(_):
                guard let alert = self?.sentBoletoAlert else {
            return
        }
                self?.coordinatorDelegate?.showAlert(alert, completion: { (_, _, _) in })
                completion()
            case .failure(let error):
                self?.coordinatorDelegate?.showAlertError(error, completion: nil)
                completion()
            }
        }
    }
    
    func trackScreenViewEvent() {
        let eventName = "Page view - Credito - fatura"
        PPAnalytics.trackEvent(eventName, properties: nil)
    }
    
    func closeIfPresented() {
        coordinatorDelegate?.closeIfPresentedController()
    }
}

extension CreditInvoiceDetailViewModel: InvoiceDetailInformationsDelegate {
    func sendInvoiceEmail() {
        sendInvoiceAction()
    }
    
    func pay() {
        guard let invoice = invoiceDetail?.invoice,
            let account = self.account else {
            return
        }
        coordinatorDelegate?.pay(with: invoice.id, and: account)
        PPAnalytics.trackEvent(CreditPicPayEvent.Registration.invoicePayment, properties: nil)
    }
}

extension CreditInvoiceDetailViewModel {
    private func loadDetails(_ completion: @escaping () -> Void) {
        isListLoading = true
        guard let invoiceId = self.invoiceId else {
            let error = PicPayError(message: CreditPicPayLocalizable.thereWasAnErrorLoading.text)
            coordinatorDelegate?.showAlertError(error, completion: nil)
            return
        }
        creditApi.invoiceDetail(invoiceId: invoiceId) { [weak self] (result) in
            self?.isListLoading = false
            self?.needLoadInvoiceDetail = false
            switch result{
            case .success(let detail):
                self?.prepareTransactions(for: detail)
                completion()
            case .failure(let error):
                self?.coordinatorDelegate?.showAlertError(error, completion: {
                    self?.coordinatorDelegate?.popDetailViewController()
                })
                completion()
            }
        }
    }
    
    private func loadAccount(_ completion: @escaping (PicPayErrorDisplayable?) -> Void) {
        creditApi.account(complete: false, isLoanFlow: false, onCacheLoaded: { _ in
        }, completion: { [weak self] result in
            switch result {
            case .success(let account):
                self?.account = account
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        })
    }
    
    private func prepareTransactions(for invoiceDetail: CreditInvoiceDetail) {
        self.invoiceDetail = invoiceDetail
        processedTranctions = invoiceDetail.transactions.filter { $0.processed == true }
        unprocessedTransaction = invoiceDetail.transactions.filter { $0.processed == false }
    }
}

extension CreditInvoiceDetailViewModel {
    enum Section: Int {
        case information = 0
        case unprocessedTransactions = 1
        case processedTransactions = 2
    }
}
