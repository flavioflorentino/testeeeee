import UI
import UIKit

final class CreditPicPayInvoicePaymentTypeViewController: PPBaseViewController {
    private lazy var balanceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        label.attributedText = viewModel.balanceText
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        return label
    }()
    private lazy var line: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 1).isActive = true
        view.backgroundColor = Palette.ppColorGrayscale300.color
        return view
    }()
    private lazy var containerCenterLabels: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .center
        return stackView
    }()
    private lazy var labelsStackView: UIStackView = {
        let descriptionLabel = UILabel()
        descriptionLabel.text = CreditPicPayLocalizable.valueOfPayment.text
        descriptionLabel.textColor = Palette.ppColorGrayscale600.color
        descriptionLabel.font = UIFont.systemFont(ofSize: 24, weight: .semibold)
        descriptionLabel.textAlignment = .center
        
        let dueDateLabel = UILabel()
        dueDateLabel.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        dueDateLabel.textColor = Palette.ppColorGrayscale400.color
        dueDateLabel.textAlignment = .center
        dueDateLabel.attributedText = viewModel.dueDateText
        
        let stackView = UIStackView(arrangedSubviews: [descriptionLabel, dueDateLabel])
        stackView.axis = .vertical
        stackView.spacing = 8
        return stackView
    }()
    private lazy var containerCards: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()
    private lazy var valueCard: CardButtonView = {
        let card = CardButtonView()
        card.imageView.isHidden = true
        card.titleLabel.text = CreditPicPayLocalizable.totalValue.text
        card.descriptionLabel.text = viewModel.totalValueText
        card.descriptionLabel.textColor = Palette.ppColorBranding300.color
        card.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(totalValueTapped)))
        return card
    }()
    private lazy var installmentCard: CardButtonView = {
        let card = CardButtonView()
        card.imageView.isHidden = true
        card.titleLabel.text = CreditPicPayLocalizable.invoiceInstall.text
        card.descriptionLabel.text = viewModel.textInstallmentCard
        card.descriptionLabel.textColor = Palette.ppColorBranding300.color
        card.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(invoiceInstallTapped)))
        return card
    }()
    private lazy var otherValueCard: CardButtonView = {
        let card = CardButtonView()
        card.imageView.isHidden = true
        card.titleLabel.text = CreditPicPayLocalizable.otherValue.text
        card.descriptionLabel.isHidden = true
        card.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(otherValueTapped)))
        return card
    }()
    
    private let viewModel: CreditPicpayInvoicePaymentTypeViewModel
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    init(wit viewModel: CreditPicpayInvoicePaymentTypeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCards()
        setupView()
        addComponents()
        layoutComponents()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        PPAnalytics.trackEvent(CreditPicPayEvent.Registration.paymentPicPayView, properties: nil)
    }
    
    private func setupCards() {
        containerCards.addArrangedSubview(valueCard)
        if viewModel.hasIntallmentsOptions {
            containerCards.addArrangedSubview(installmentCard)
        }
        containerCards.addArrangedSubview(otherValueCard)
    }
    
    private func setupView() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
        title = CreditPicPayLocalizable.payInvoice.text
    }
    
    private func addComponents() {
        containerCenterLabels.addArrangedSubview(labelsStackView)
        view.addSubview(balanceLabel)
        view.addSubview(line)
        view.addSubview(containerCenterLabels)
        view.addSubview(containerCards)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            balanceLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 12),
            balanceLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -12),
            balanceLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 12),
            
            line.topAnchor.constraint(equalTo: balanceLabel.bottomAnchor, constant: 12),
            line.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            line.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            
            containerCenterLabels.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 12),
            containerCenterLabels.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -12),
            containerCenterLabels.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 12),
            
            containerCards.topAnchor.constraint(equalTo: containerCenterLabels.bottomAnchor, constant: 12),
            containerCards.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            containerCards.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
    
        let bottomSpacing: CGFloat = -20.0
        if #available(iOS 11.0, *) {
            containerCards.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: bottomSpacing).isActive = true
        } else {
            containerCards.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottomSpacing).isActive = true
        }
    }
}

extension CreditPicPayInvoicePaymentTypeViewController {
    @objc
    private func totalValueTapped() {
        viewModel.tapInTotalValue()
        PPAnalytics.trackEvent(CreditPicPayEvent.Registration.paymentPicPayTotal, properties: nil)
    }
    
    @objc
    private func invoiceInstallTapped() {
        viewModel.tapInInvoiceInstall()
        PPAnalytics.trackEvent(CreditPicPayEvent.Registration.paymentPicPayInstallment, properties: nil)
    }
    
    @objc
    private func otherValueTapped() {
        viewModel.tapInOtherValue()
        PPAnalytics.trackEvent(CreditPicPayEvent.Registration.paymentPicPayOther, properties: nil)
    }
}
