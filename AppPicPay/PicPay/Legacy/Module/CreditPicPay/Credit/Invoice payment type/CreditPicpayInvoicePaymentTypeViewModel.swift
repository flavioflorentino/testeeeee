import UI
import Foundation
import AnalyticsModule
final class CreditPicpayInvoicePaymentTypeViewModel {
    private let invoicePayment: CreditInvoicePayment
    private let coordinatorDelegate: CreditPicPayInvoicePaymentTypeCoordinatorDelegate?
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    var hasIntallmentsOptions: Bool {
        guard let installment = invoicePayment.installment else {
            return false
        }
        return installment.isValid && !installment.installmentOptions.isEmpty
    }
    var textInstallmentCard: String? {
        guard let installment = invoicePayment.installment else {
            return nil
        }
        guard let lastOption = installment.installmentOptions.last else {
            return nil
        }
        let count = lastOption.installmentCount
        let value = lastOption.openValue.stringAmount
        return String(format: CreditPicPayLocalizable.installmentCountValue.text, count, value)
    }
    var balanceText: NSAttributedString? {
        guard let balance = ConsumerManager.shared.consumer?.balance?.doubleValue.stringAmount else {
            return nil
        }
        let text = String(format: CreditPicPayLocalizable.balanceInWallet.text, balance)
        let range = NSRange(location: 19, length: balance.count)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttributes([.font: UIFont.systemFont(ofSize: 12, weight: .bold)], range: range)
        return attributedString
    }
    var dueDateText: NSAttributedString? {
        let text = String(format: CreditPicPayLocalizable.invoiceDetailDueDate.text, invoicePayment.invoice.dueDate.stringFormatted(with: .ddDeMMMM))
        let location = 10
        let range = NSRange(location: location, length: text.count - location)
        let attributed = NSMutableAttributedString(string: text)
        attributed.addAttributes([NSAttributedString.Key.foregroundColor: Palette.ppColorBranding300.color], range: range)
        return attributed
    }
    var totalValueText: String {
        return invoicePayment.invoice.totalValue.stringAmount
    }
    
    init(with invoicePayment: CreditInvoicePayment, coordinatorDelegate: CreditPicPayInvoicePaymentTypeCoordinatorDelegate?, dependencies: Dependencies = DependencyContainer()) {
        self.invoicePayment = invoicePayment
        self.coordinatorDelegate = coordinatorDelegate
        self.dependencies = dependencies
    }
    
    func tapInTotalValue() {
        dependencies.analytics.log(CardInvoiceEvent.paymentChoice(action: .payTotalAmount))
        coordinatorDelegate?.pay()
    }
    
    func tapInInvoiceInstall() {
        coordinatorDelegate?.payWithInstallment()
    }
    
    func tapInOtherValue() {
        dependencies.analytics.log(CardInvoiceEvent.paymentChoice(action: .payAnotherAmount))
        coordinatorDelegate?.payWithAnotherValue()
    }
}
