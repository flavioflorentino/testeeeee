import Foundation
import AnalyticsModule
protocol CreditPicPayInvoicePaymentTypeCoordinatorDelegate: AnyObject {
    func pay()
    func payWithInstallment()
    func payWithAnotherValue()
}

final class CreditPicPayInvoicePaymentTypeCoordinator: Coordinator {
    var currentCoordinator: Coordinator?
    private let navigationController: UINavigationController
    private let payment: CreditInvoicePayment
    weak var delegate: CreditPicPayInvoicePaymentTypeCoordinatorDelegate?
    
    init(with navigationController: UINavigationController, payment: CreditInvoicePayment) {
        self.navigationController = navigationController
        self.payment = payment
    }
    
    func start() {
        let viewModel = CreditPicpayInvoicePaymentTypeViewModel(with: payment, coordinatorDelegate: delegate, dependencies: DependencyContainer())
        let controller = CreditPicPayInvoicePaymentTypeViewController(wit: viewModel)
        navigationController.pushViewController(controller, animated: true)
    }
}
