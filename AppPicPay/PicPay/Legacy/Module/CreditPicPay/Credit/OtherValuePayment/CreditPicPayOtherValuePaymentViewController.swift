import UI
import UIKit
import SnapKit

final class CreditPicPayOtherValuePaymentViewController: PPBaseViewController {
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.text = CreditPicPayLocalizable.whenYouWantToPay.text
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .center
        return label
    }()
    private lazy var valueCurrencyStackView: ValueCurrencyStackView = {
        let stackView = ValueCurrencyStackView()
        stackView.onTextChange = { [weak self] value in
            self?.viewModel.value = value
            self?.setupNextButton()
        }
        return stackView
    }()
    private lazy var firstLine: UIView = {
        let view = UIView()
        view.heightAnchor.constraint(equalToConstant: 1).isActive = true
        view.backgroundColor = Palette.ppColorGrayscale300.color
        return view
    }()
    private lazy var secondLine: UIView = {
        let view = UIView()
        view.heightAnchor.constraint(equalToConstant: 1).isActive = true
        view.backgroundColor = Palette.ppColorGrayscale300.color
        return view
    }()
    private lazy var totalInvoice: LabelAndValueStackView = {
        let labelValue = LabelAndValueStackView()
        labelValue.left.text = CreditPicPayLocalizable.totalOfInvoice.text
        labelValue.left.textColor = Palette.ppColorGrayscale400.color
        labelValue.right.text = viewModel.totalInvoice
        labelValue.right.textColor = Palette.ppColorGrayscale400.color
        return labelValue
    }()
    private lazy var minimumPaymentInvoice: LabelAndValueStackView = {
        let labelValue = LabelAndValueStackView()
        labelValue.left.text = CreditPicPayLocalizable.minimumPayment.text
        labelValue.left.textColor = Palette.ppColorGrayscale400.color
        labelValue.right.text = viewModel.minimumPayment
        labelValue.right.textColor = Palette.ppColorGrayscale400.color
        return labelValue
    }()
    private lazy var receivedPaymentInvoice: LabelAndValueStackView = {
        let labelValue = LabelAndValueStackView()
        labelValue.left.text = CreditPicPayLocalizable.invoiceReceivedPayment.text
        labelValue.left.textColor = Palette.ppColorBranding300.color
        labelValue.left.font = UIFont.boldSystemFont(ofSize: 13)
        labelValue.right.text = viewModel.receivedPayment
        labelValue.right.textColor = Palette.ppColorBranding300.color
        labelValue.right.font = UIFont.boldSystemFont(ofSize: 13)
        return labelValue
    }()
    private lazy var labelsAndValuesStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.axis = .vertical
        stackView.spacing = 10
        return stackView
    }()
    private lazy var nextButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = 22
        button.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(nextButtonTapped)))
        button.setTitle(DefaultLocalizable.next.text, for: .normal)
        return button
    }()
    private lazy var mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.layoutMargins = UIEdgeInsets(top: 40, left: 0, bottom: 0, right: 0)
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.spacing = 8
        stackView.axis = .vertical
        return stackView
    }()
    private var nextButtonConstraintBottom: Constraint?
    private let viewModel: CreditPicPayOtherValuePaymentViewModel
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    init(with viewModel: CreditPicPayOtherValuePaymentViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupObservers()
        addComponents()
        layoutComponents()
        setupView()
        setupNextButton()
    }
    
    private func addComponents() {
        mainStackView.addArrangedSubview(descriptionLabel)
        mainStackView.addArrangedSubview(valueCurrencyStackView)
        mainStackView.addArrangedSubview(firstLine)
        mainStackView.addArrangedSubview(totalInvoice)
        
        if viewModel.hasMinimumPayment {
            mainStackView.addArrangedSubview(minimumPaymentInvoice)
        }
        if viewModel.hasReceivedPayment {
            mainStackView.addArrangedSubview(receivedPaymentInvoice)
        }
        
        mainStackView.addArrangedSubview(secondLine)
        view.addSubview(mainStackView)
        view.addSubview(nextButton)
    }
    
    private func layoutComponents() {
        mainStackView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(Spacing.base01)
        }
        nextButton.snp.makeConstraints { (make) in
            make.top.greaterThanOrEqualTo(mainStackView.snp.bottom).offset(Spacing.base02)
            make.leading.trailing.equalToSuperview().inset(Spacing.base02)
            nextButtonConstraintBottom = make.bottom.equalToSuperview().inset(Spacing.base04).constraint
            make.height.equalTo(Spacing.base06)
        }
    }
    
    private func setupView() {
        title = CreditPicPayLocalizable.invoicePayment.text
        view.backgroundColor = Palette.ppColorGrayscale000.color
        valueCurrencyStackView.valueTextField.becomeFirstResponder()
    }
    
    private func setupNextButton() {
        nextButton.setBackgroundColor(viewModel.buttonConfigurations.backgroundColor)
        nextButton.setTitleColor(viewModel.buttonConfigurations.titleColor)
        nextButton.borderColor = viewModel.buttonConfigurations.backgroundColor
        nextButton.isEnabled = viewModel.buttonConfigurations.isEnable
    }
    
    private func setupObservers() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDismiss), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc
    private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc
    private func nextButtonTapped() {
        viewModel.nextButtonTapped()
    }
    
    @objc
    private func keyboardWillDismiss(notification: Notification) {
        nextButtonConstraintBottom?.update(offset: -Spacing.base04).activate()
    }
    
    @objc
    private func keyboardWillShow(notification: Notification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        nextButtonConstraintBottom?.update(inset: keyboardSize.height + Spacing.base02).activate()
    }
}
