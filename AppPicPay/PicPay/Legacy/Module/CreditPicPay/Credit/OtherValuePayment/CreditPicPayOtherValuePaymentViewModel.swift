import CoreLegacy
import UI
import Foundation
import AnalyticsModule
final class CreditPicPayOtherValuePaymentViewModel {
    private let invoicePayment: CreditInvoicePayment
    private let delegate: CreditPicPayOtherValuePaymentCoordinatorDelegate?
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    var totalInvoice: String {
        return invoicePayment.invoice.totalValue.stringAmount
    }
    var minimumPayment: String? {
        guard let minimumPayment = invoicePayment.invoice.minimumPayment else {
     return nil
}
        return minimumPayment.stringAmount
    }
    var receivedPayment: String? {
        guard let receivedPayment = invoicePayment.invoice.receivedPayment else {
     return nil
}
        return receivedPayment.stringAmount
    }
    var valueText: String {
        return CurrencyFormatter.brazillianRealString(from: value as NSNumber)
    }
    var value: Double = 0.0
    var hasReceivedPayment: Bool {
        return invoicePayment.invoice.receivedPayment != nil
    }
    var hasMinimumPayment: Bool {
        return invoicePayment.invoice.minimumPayment != nil && invoicePayment.invoice.minimumPayment != 0
    }
    var buttonConfigurations: (backgroundColor: UIColor, titleColor: UIColor, isEnable: Bool) {
        if valueText.isEmpty || valueText == "R$ 0,00" {
            return (Palette.ppColorGrayscale300.color, Palette.ppColorGrayscale000.color, false)
        }
        return (Palette.ppColorBranding300.color, Palette.ppColorGrayscale000.color, true)
    }
    
    init(with invoicePayment: CreditInvoicePayment, coordinatorDelegate: CreditPicPayOtherValuePaymentCoordinatorDelegate?, dependencies: Dependencies = DependencyContainer()) {
        self.invoicePayment = invoicePayment
        self.delegate = coordinatorDelegate
        self.dependencies = dependencies
    }
    
    func nextButtonTapped() {
        dependencies.analytics.log(CardInvoiceEvent.payOtherAmount)
        delegate?.nextButtonTapped(value)
    }
}
