import Foundation

protocol CreditPicPayOtherValuePaymentCoordinatorDelegate: AnyObject {
    func nextButtonTapped(_ value: Double)
}

final class CreditPicPayOtherValuePaymentCoordinator: Coordinator {
    var currentCoordinator: Coordinator?
    private let navigationController: UINavigationController
    private let invoicePayment: CreditInvoicePayment
    weak var delegate: CreditPicPayOtherValuePaymentCoordinatorDelegate?
    
    init(with navigationController: UINavigationController, invoicePayment: CreditInvoicePayment) {
        self.navigationController = navigationController
        self.invoicePayment = invoicePayment
    }
    
    func start() {
        let viewModel = CreditPicPayOtherValuePaymentViewModel(with: invoicePayment, coordinatorDelegate: delegate, dependencies: DependencyContainer())
        let controller = CreditPicPayOtherValuePaymentViewController(with: viewModel)
        navigationController.pushViewController(controller, animated: true)
    }
}
 
