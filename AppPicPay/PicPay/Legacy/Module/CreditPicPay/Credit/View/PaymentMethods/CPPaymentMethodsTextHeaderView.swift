//
//  CPPaymentMethodsTextHeaderView.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 02/04/18.
//

import UIKit

final class CPPaymentMethodsTextHeaderView: NibView {
    private var gradientLayer: CAGradientLayer?
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var button: UIPPButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        if gradientLayer == nil {
            gradientLayer = view.applyGradient(withColours: PicPayStyle.gradientPicPayCredit, gradientOrientation: .vertical)
        }
    }
    
    func setup() {
        backgroundColor = view.backgroundColor
    }
}
