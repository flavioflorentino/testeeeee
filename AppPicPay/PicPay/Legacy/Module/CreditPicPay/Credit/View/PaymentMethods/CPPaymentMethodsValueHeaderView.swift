import UI
import UIKit

final class CPPaymentMethodsValueHeaderView: NibView {
    private var gradientLayer: CAGradientLayer?
    private var creditStatus: CPAccount.CreditStatusType?
    private let regularGradientColor = [Palette.ppColorBrandingPlus400.gradientColor.from, Palette.ppColorBrandingPlus400.gradientColor.to]
    private let blockedGradientColor = [Palette.ppColorGrayscalePlus.gradientColor.from, Palette.ppColorGrayscalePlus.gradientColor.to]
    
    var onTapButton: () -> Void = {}
    var onTapValue: () -> Void = {}
    
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            let featureName = CreditPicPayLocalizable.featureName.text
            descriptionLabel.text = "\(CreditPicPayLocalizable.limit.text) \(featureName)"
        }
    }
    @IBOutlet weak private var valueLabel: UILabel! {
        didSet {
            let gesture = UITapGestureRecognizer(target: self, action: #selector(onTapValueAction))
            valueLabel.isUserInteractionEnabled = true
            valueLabel.addGestureRecognizer(gesture)
        }
    }
    @IBOutlet weak private var button: UIPPButton!
    
    init(with value: String?, buttonTitle: String, frame: CGRect, creditStatus: CPAccount.CreditStatusType) {
        super.init(frame: frame)
        button.setTitle(buttonTitle, for: .normal)
        valueLabel.text = value
        self.creditStatus = creditStatus
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction private func tapButton(_ sender: Any) {
        let eventName = "Crédito - botão Ver detalhes"
        PPAnalytics.trackEvent(eventName, properties: nil)
        onTapButton()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        gradientLayer?.removeFromSuperlayer()
        let colors: [UIColor] = creditStatus == .blocked ? blockedGradientColor : regularGradientColor
        gradientLayer = view.applyGradient(withColours: colors, gradientOrientation: .vertical)
        backgroundColor = creditStatus == .blocked ? blockedGradientColor.first : regularGradientColor.first
    }
    
    @objc
    private func onTapValueAction() {
        onTapValue()
    }
}
