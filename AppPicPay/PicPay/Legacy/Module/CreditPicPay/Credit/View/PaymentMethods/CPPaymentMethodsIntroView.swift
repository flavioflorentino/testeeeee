//
//  CPPaymentMethodsIntroView.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 11/04/18.
//

import UIKit

final class CPPaymentMethodsIntroView: NibView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        isUserInteractionEnabled = true
        addGestureRecognizer(UITapGestureRecognizer( target: self, action: #selector(dismiss)))
    }
    
    @objc
    func dismiss() {
        UIView.animate(withDuration: 0.25, animations: {
                self.layer.opacity = 0.0
        }, completion: { completed in
            if completed {
                self.removeFromSuperview()
            }
        })
    }
}
