import UI
import UIKit
import Card

final class CreditLimitViewController: PPBaseViewController {
    private let viewModel: CreditLimitViewModel
    
    private lazy var limitValueCurrency: ValueCurrencyStackView = {
        let limitValueStackView = ValueCurrencyStackView()
        limitValueStackView.valueTextField.isEnabled = false
        return limitValueStackView
    }()
    
    private lazy var avaliableLimitLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    
    private lazy var slider: UISlider = {
        let slider = UISlider()
        slider.addTarget(self, action: #selector(limitSliderOnChange(_:)), for: .valueChanged)
        slider.minimumTrackTintColor = Colors.branding300.color
        slider.maximumTrackTintColor = Colors.grayscale300.color
        return slider
    }()
    
    private lazy var maxAndMinLabelsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.alignment = .lastBaseline
        return stackView
    }()
    
    private lazy var verticalStack: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var minLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.grayscale400.color
        label.font = UIFont.systemFont(ofSize: 9)
        return label
    }()
    
    private lazy var maxLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.grayscale400.color
        label.font = UIFont.systemFont(ofSize: 9)
        return label
    }()
    
    private lazy var sendButton: ConfirmButton = {
        let button = ConfirmButton()
        button.addTarget(self, action: #selector(sendButtonTapped), for: .touchUpInside)
        button.setTitle(DefaultLocalizable.adjust.text, for: .normal)
        return button
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 11.0, *) {
            return .default
        } else {
            return .lightContent
        }
    }
    
    init(with viewModel: CreditLimitViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewModel()
        addComponents()
        layoutComponents()
        setupView()
        fetchLimit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Workaround for iOS 13 modal gap below navigationbar
        if #available(iOS 13.0, *) {
            DispatchQueue.main.async {
                self.navigationController?.navigationBar.setNeedsLayout()
            }
        }
    }

    private func addComponents() {
        verticalStack.addArrangedSubviews(limitValueCurrency, avaliableLimitLabel, slider, maxAndMinLabelsStackView)
        maxAndMinLabelsStackView.addArrangedSubviews(minLabel,maxLabel)
        view.addSubview(verticalStack)
        view.addSubview(sendButton)
    }
    
    private func layoutComponents() {
        verticalStack.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(Spacing.base02)
            make.centerY.equalToSuperview()
        }
        sendButton.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(Spacing.base02)
            make.bottom.equalToSuperview().inset(Spacing.base04)
            make.height.equalTo(Spacing.base06)
        }
    }
    
    private func setupView() {
        title = CreditPicPayLocalizable.limitTitleView.text
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
    
    private func setupViewModel() {
        viewModel.onSuccess = { [weak self] in
            DispatchQueue.main.async {
                self?.completionRequestActions()
                self?.setupInformationView()
            }
        }
        viewModel.onError = { [weak self] in
            DispatchQueue.main.async {
                self?.completionRequestActions()
            }
        }
    }
    
    private func completionRequestActions() {
        stopLoadingView()
        sendButton.stopLoadingAnimating()
        slider.isEnabled = true
    }
    
    private func fetchLimit() {
        startLoadingView()
        viewModel.getLimit()
    }
    
    private func setupInformationView() {
        limitValueCurrency.value = viewModel.currentValue
        minLabel.text = viewModel.minValueFormatted
        maxLabel.text = viewModel.maxValueFormatted
        avaliableLimitLabel.attributedText = viewModel.avaliableLimit()
        slider.minimumValue = viewModel.minValue
        slider.maximumValue = viewModel.maxValue
        slider.setValue(Float(viewModel.currentValue), animated: true)
    }
    
    @objc
    private func sendButtonTapped() {
        sendButton.startLoadingAnimating()
        slider.isEnabled = false
        viewModel.save()
    }
    
    @objc
    private func limitSliderOnChange(_ sender: UISlider) {
        viewModel.valueOnChangeSlider(sender.value)
        avaliableLimitLabel.attributedText = viewModel.avaliableLimit()
        limitValueCurrency.value = viewModel.currentValue
    }
}

extension CreditLimitViewController: StyledNavigationDisplayable {}
