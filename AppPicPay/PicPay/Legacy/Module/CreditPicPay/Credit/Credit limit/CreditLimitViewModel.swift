import CoreLegacy
import UI
import UIKit

final class CreditLimitViewModel {
    private let worker: CreditLimitWorkerProtocol
    var creditLimit: CreditLimit?
    private(set) var currentValue: Double = 0.0

    weak var coordinatorDelegate: CreditLimitCoordinatorDelegate?
    
    var onSuccess: (() -> Void)?
    var onError: (() -> Void)?

    var secondaryValue: Float {
        guard let value = creditLimit?.balance else {
            return 0.0
        }
        return Float(value)
    }
    var secondaryColor: UIColor {
        let defaultSecondaryColor = Palette.ppColorBranding300.color.withAlphaComponent(0.5)
        let color = Palette.ppColorBranding300.color
        guard let invoiceValue = creditLimit?.balance else {
            return color
        }
        return (currentValue <= invoiceValue) ? color : defaultSecondaryColor
    }
    var minValueFormatted: String {
        guard let minValue = self.creditLimit?.minValue else {
            return "???"
        }
        return CurrencyFormatter.brazillianRealString(from: minValue as NSNumber)
    }
    var maxValueFormatted: String {
        guard let maxValue = self.creditLimit?.maxValue else {
            return "???"
        }
        return CurrencyFormatter.brazillianRealString(from: maxValue as NSNumber)
    }
    var minValue: Float {
        guard let minValue = self.creditLimit?.minValue else {
            return 0.0
        }
        return Float(minValue)
    }
    var maxValue: Float {
        guard let maxValue = self.creditLimit?.maxValue else {
            return 0.0
        }
        return Float(maxValue)
    }
    
    init(with worker: CreditLimitWorkerProtocol = CreditLimitWorker()) {
        self.worker = worker
        setCurrentValue()
    }
    
    func avaliableLimit() -> NSAttributedString {
        guard let creditLimit = self.creditLimit else {
            return NSAttributedString(string: "")
        }
        let value = currentValue - creditLimit.balance
        let color = colorForLimit(from: value)
        let valueText = value.getFormattedPrice()
        let text = String(format: CreditPicPayLocalizable.limitHavaliableValue.text, valueText)
        let attributedText = NSMutableAttributedString(string: text, attributes: [.foregroundColor: color, .font: UIFont.systemFont(ofSize: 13)])
        let boldRange = NSRange(location: 0, length: valueText.count)
        attributedText.addAttributes([.font: UIFont.boldSystemFont(ofSize: 13)], range: boldRange)
        return attributedText
    }
    
    private func setCurrentValue() {
        self.currentValue = creditLimit?.currentValue ?? 0.00
    }
    
    func getLimit() {
        worker.limit { [weak self] result in
            switch result {
            case .success(let creditLimit):
                self?.creditLimit = creditLimit
                self?.currentValue = creditLimit.currentValue
                self?.onSuccess?()
            case .failure(let error):
                self?.present(error) {
                    self?.closeView()
                }
            }
        }
    }
    
    func save() {
        worker.save(limit: currentValue) { [weak self] result in
            switch result {
            case .success:
                self?.onSuccess?()
                self?.coordinatorDelegate?.didUpdateLimit()
                self?.showSuccessAlert()
            case .failure(let error):
                self?.present(error)
                self?.setCurrentValue()
            }
        }
    }
    
    func closeView() {
        DispatchQueue.main.async { [weak self] in
            self?.coordinatorDelegate?.close()
        }
    }
    
    func valueOnChangeSlider(_ value: Float) {
        currentValue  = calculateCurrentValue(value: value)
    }
    
    func calculateCurrentValue(value: Float) -> Double {
        let isMinOrMaxValue = value == minValue || value == maxValue
        
        guard let creditLimit = creditLimit, !isMinOrMaxValue else {
            return Double(value)
        }
        let increment = Float(creditLimit.increment)
        let divider = (value / increment).rounded(.up).magnitude
        return Double(divider * increment)
    }
    
    private func showSuccessAlert() {
        let image = Alert.Image(with: #imageLiteral(resourceName: "ico-smile"))
        let title = CreditPicPayLocalizable.limitSuccessAlertTitle.text
        let currentValueFormatted = currentValue.getFormattedPrice()
        let text = String(format: CreditPicPayLocalizable.limitSuccesAlertText.text, currentValueFormatted)
        let button = Button(title: CreditPicPayLocalizable.limitSuccessAlertButton.text, type: .cta, action: .close)
        let alert = Alert(with: title, text: text, buttons: [button], image: image)
        coordinatorDelegate?.presentAlert(alert) { [weak self] in
            self?.closeView()
        }
    }
    
    private func colorForLimit(from value: Double) -> UIColor {
        return value > 0.0 ? Palette.ppColorBranding300.color : Palette.ppColorGrayscale400.color
    }
    
    private func present(_ error: Error, _ completion: (() -> Void)? = nil) {
        DispatchQueue.main.async { [weak self] in
            let alert = Alert(with: error, image: Alert.Image(with: #imageLiteral(resourceName: "sad-3")))
            self?.coordinatorDelegate?.presentAlert(alert, completion: completion)
        }
        onError?()
    }
}
