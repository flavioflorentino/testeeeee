import FeatureFlag
import Foundation
import Core

protocol CreditLimitWorkerProtocol {
    func limit(_ completion: @escaping (PicPayResult<CreditLimit>) -> Void)
    func save(limit: Double, _ completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void)
}

final class CreditLimitWorker: CreditLimitWorkerProtocol {
    private let requestManager = RequestManager.shared
    typealias Dependencies = HasFeatureManager & HasMainQueue
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func limit(_ completion: @escaping (PicPayResult<CreditLimit>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlCreditPicpayLimit, method: .get)
                .responseApiCodable { result in
                    completion(result)
                }
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func save(limit: Double, _ completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let params: [String: Any] = ["value": limit]
            requestManager
                .apiRequest(endpoint: kWsUrlCreditPicpayLimit, method: .put, parameters: params)
                .responseApiObject { [weak self] result in
                    self?.dependencies.mainQueue.async {
                        completion(result)
                    }
                }
            return
        }
        // TODO: - adicionar helper para o core network
    }
}
