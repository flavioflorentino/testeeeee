import Foundation

protocol CreditLimitCoordinatorDelegate: AnyObject {
    func close()
    func didUpdateLimit()
    func presentAlert(_ alert: Alert, completion: (() -> Void)?)
}

final class CreditLimitCoordinator: Coordinator {
    var currentCoordinator: Coordinator?
    var didUpdateCardLimit: (() -> Void)?
    
    private let navigationController: UINavigationController

    init(from viewController: UINavigationController) {
        navigationController = viewController
    }
    
    func start() {
        let viewModel = CreditLimitViewModel(with: CreditLimitWorker())
        let controller = CreditLimitViewController(with: viewModel)
        viewModel.coordinatorDelegate = self
        navigationController.pushViewController(controller, animated: true)
    }
}

extension CreditLimitCoordinator: CreditLimitCoordinatorDelegate {
    func close() {
        navigationController.popViewController(animated: true)
    }
    
    func didUpdateLimit() {
        didUpdateCardLimit?()
    }
    
    func presentAlert(_ alert: Alert, completion: (() -> Void)? = nil) {
        AlertMessage.showAlert(alert, controller: navigationController) { _, _, _ in
            completion?()
        }
    }
}
