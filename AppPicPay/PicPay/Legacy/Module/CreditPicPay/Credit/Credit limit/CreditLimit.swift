import Foundation

struct CreditLimit: Decodable {
    let maxValue: Double
    let minValue: Double
    let increment: Int
    let currentValue: Double
    let balance: Double
}
