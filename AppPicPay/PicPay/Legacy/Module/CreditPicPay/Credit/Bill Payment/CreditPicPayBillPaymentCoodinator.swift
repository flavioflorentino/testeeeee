import Foundation

protocol CreditPicPayBillPaymentCoordinatorDelegate: AnyObject {
    func showAlertError(_ error: Error, completion: (() -> Void)?)
    func showAlert(_ alert: Alert, completion: ((AlertPopupViewController, Button, UIPPButton) -> Void)?)
}

final class CreditPicPayBillPaymentCoordinator: Coordinator {
    var currentCoordinator: Coordinator?
    private let navigationController: UINavigationController
    private let invoicePayment: CreditInvoicePayment
    weak var delegate: CreditPicPayBillPaymentCoordinatorDelegate?
    
    init(with navigationController: UINavigationController, invoicePayment: CreditInvoicePayment) {
        self.navigationController = navigationController
        self.invoicePayment = invoicePayment
    }
    
    func start() {
        let viewModel = CreditPicPayBillPaymentViewModel(with: invoicePayment, coordinatorDelegate: delegate, dependencies: DependencyContainer())
        let controller = CreditPicPayBillPaymentViewController(with: viewModel)
        navigationController.pushViewController(controller, animated: true)
    }
}
