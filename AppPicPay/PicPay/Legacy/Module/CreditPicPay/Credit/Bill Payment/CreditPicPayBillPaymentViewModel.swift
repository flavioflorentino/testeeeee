import UI
import Foundation
import AnalyticsModule

final class CreditPicPayBillPaymentViewModel {
    private let invoicePayment: CreditInvoicePayment
    private let creditApi: CreditApiProtocol
    private var coordinatorDelegate: CreditPicPayBillPaymentCoordinatorDelegate?
    typealias Dependencies = HasAnalytics
    private var dependencies: Dependencies
    
    var value: Double {
        return invoicePayment.invoice.totalValue
    }
    var barCode: String {
        return invoicePayment.paymentLine
    }
    var minimumPaymentString: String? {
        return invoicePayment.minimumPayment?.stringAmount
    }
    
    var hasMinimumPayment: Bool {
        return invoicePayment.minimumPayment != nil && invoicePayment.minimumPayment != 0
    }
    
    var dueDateAttributed: NSAttributedString {
        let dueDate = invoicePayment.invoice.dueDate
        let dueDatetext = dueDate.stringFormatted(with: .ddDeMMMM)
        let text = CreditPicPayLocalizable.invoiceDetailDueDate.text
        let range = NSRange(location: 11, length: dueDatetext.count)
        let attributedString = NSMutableAttributedString(string: String(format: text, dueDatetext))
        attributedString.addAttributes([.foregroundColor: Palette.ppColorBranding300.color], range: range)
        return attributedString
    }
    
    init(with invoicePayment: CreditInvoicePayment, coordinatorDelegate: CreditPicPayBillPaymentCoordinatorDelegate?, creditApi: CreditApiProtocol = CreditPicpayApi(), dependencies: Dependencies = DependencyContainer()) {
        self.invoicePayment = invoicePayment
        self.coordinatorDelegate = coordinatorDelegate
        self.creditApi = creditApi
        self.dependencies = dependencies
    }
    
    func sendToEmail(_ completion: @escaping () -> Void) {
        dependencies.analytics.log(CardInvoiceEvent.billPayment(action: .billSendEmail))
        creditApi.sendInvoiceEmail(invoiceId: invoicePayment.invoice.id) { [weak self] result in
            switch result {
            case .success:
                self?.showAlertBillSent()
                completion()
            case .failure(let error):
                self?.coordinatorDelegate?.showAlertError(error, completion: nil)
            }
        }
    }
    
    func copyBarCode() {
        dependencies.analytics.log(CardInvoiceEvent.billPayment(action: .billCopyBarCode))
        let paymentLine = invoicePayment.paymentLine
        UIPasteboard.general.string = paymentLine.removingWhiteSpaces().replacingOccurrences(of: ".", with: "")
    }
    
    private func showAlertBillSent() {
        let consumerEmail = ConsumerManager.shared.consumer?.email ?? ""
        let description = String(format: CreditPicPayLocalizable.descriptionSentBill.text, consumerEmail)
        let alert = Alert(title: CreditPicPayLocalizable.sentBill.text, text: description)
        coordinatorDelegate?.showAlert(alert) { [weak self] (_, _, _) in
            self?.dependencies.analytics.log(CardInvoiceEvent.billPaymentAlertConfirmEmail)
        }
    }
}
