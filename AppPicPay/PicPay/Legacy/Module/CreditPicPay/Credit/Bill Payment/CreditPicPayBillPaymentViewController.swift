import UI
import UIKit

final class CreditPicPayBillPaymentViewController: PPBaseViewController {
    private lazy var topStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = UIEdgeInsets(top: 40, left: 0, bottom: 14, right: 0)
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 8
        return stackView
    }()
    private lazy var valueInvoiceLabel: UILabel = {
        let label = UILabel()
        label.text = CreditPicPayLocalizable.valueInvoice.text
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        return label
    }()
    private lazy var currencyValue: ValueCurrencyStackView = {
        let stackView = ValueCurrencyStackView()
        stackView.valueTextField.isEnabled = false
        stackView.value = viewModel.value
        return stackView
    }()
    private lazy var dueDateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        label.textColor = Palette.ppColorGrayscale400.color
        label.attributedText = viewModel.dueDateAttributed
        return label
    }()
    private lazy var firstLine: UIView = {
        let view = UIView()
        view.heightAnchor.constraint(equalToConstant: 1).isActive = true
        view.backgroundColor = Palette.ppColorGrayscale300.color
        return view
    }()
    private lazy var minimumPayment: LabelAndValueStackView = {
        let stackView = LabelAndValueStackView()
        stackView.left.textColor = Palette.ppColorGrayscale400.color
        stackView.left.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        stackView.left.text = CreditPicPayLocalizable.minimumPayment.text
        stackView.right.textColor = Palette.ppColorGrayscale400.color
        stackView.right.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        stackView.right.text = viewModel.minimumPaymentString
        return stackView
    }()
    private lazy var secondLine: UIView = {
        let view = UIView()
        view.heightAnchor.constraint(equalToConstant: 1).isActive = true
        view.backgroundColor = Palette.ppColorGrayscale300.color
        return view
    }()
    private lazy var minimumPaymentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 14
        stackView.axis = .vertical
        return stackView
    }()
    private lazy var centerBarCodeStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .center
        return stackView
    }()
    private lazy var barCodeStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 10
        return stackView
    }()
    private lazy var barCodeLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.text = viewModel.barCode
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    private lazy var copyCodeButton: UIButton = {
        let button = UIButton()
        button.setTitle(CreditPicPayLocalizable.copyBarCode.text, for: .normal)
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
        button.heightAnchor.constraint(equalToConstant: 44).isActive = true
        button.addTarget(self, action: #selector(copyButtonTapped), for: .touchUpInside)
        return button
    }()
    private lazy var sendEmailButton: UIPPButton = {
        let button = UIPPButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.borderColor = Palette.ppColorBranding300.color
        button.cornerRadius = 22
        button.normalTitleColor = Palette.ppColorBranding300.color
        button.normalBackgrounColor = Palette.ppColorGrayscale000.color
        button.highlightedTitleColor = Palette.ppColorGrayscale000.color
        button.highlightedBackgrounColor = Palette.ppColorBranding300.color
        button.loadingTitleColor = Palette.ppColorGrayscale000.color
        button.loadingBorderColor = Palette.ppColorBranding300.color
        button.setTitle(CreditPicPayLocalizable.sendToEmail.text, for: .normal)
        button.heightAnchor.constraint(equalToConstant: 44).isActive = true
        button.addTarget(self, action: #selector(sendToEmailButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private let viewModel: CreditPicPayBillPaymentViewModel
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    init(with viewModel: CreditPicPayBillPaymentViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = Palette.ppColorGrayscale000.color
        title = CreditPicPayLocalizable.generateBill.text
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
        layoutComponents()
        setupMinimumPayment()
    }
    
    private func addComponents() {
        topStackView.addArrangedSubview(valueInvoiceLabel)
        topStackView.addArrangedSubview(currencyValue)
        topStackView.addArrangedSubview(dueDateLabel)
        minimumPaymentStackView.addArrangedSubview(firstLine)
        minimumPaymentStackView.addArrangedSubview(minimumPayment)
        minimumPaymentStackView.addArrangedSubview(secondLine)
        barCodeStackView.addArrangedSubview(barCodeLabel)
        barCodeStackView.addArrangedSubview(copyCodeButton)
        centerBarCodeStackView.addArrangedSubview(barCodeStackView)
        view.addSubview(topStackView)
        view.addSubview(minimumPaymentStackView)
        view.addSubview(centerBarCodeStackView)
        view.addSubview(sendEmailButton)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            topStackView.topAnchor.constraint(equalTo: view.topAnchor),
            topStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8),
            topStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8),
            
            minimumPaymentStackView.topAnchor.constraint(equalTo: topStackView.bottomAnchor, constant: 8),
            minimumPaymentStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8),
            minimumPaymentStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8),
            
            centerBarCodeStackView.topAnchor.constraint(equalTo: minimumPaymentStackView.bottomAnchor, constant: 8),
            centerBarCodeStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8),
            centerBarCodeStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8),
            
            sendEmailButton.topAnchor.constraint(equalTo: centerBarCodeStackView.bottomAnchor, constant: 8),
            sendEmailButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            sendEmailButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16)
            ])
        
        let bottomButtonConstraint: CGFloat = -21
        if #available(iOS 11.0, *) {
            sendEmailButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: bottomButtonConstraint).isActive = true
        } else {
            sendEmailButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottomButtonConstraint).isActive = true
        }
    }
    
    private func setupMinimumPayment() {
        minimumPaymentStackView.isHidden = !viewModel.hasMinimumPayment
    }
    
    @objc
    private func copyButtonTapped() {
        viewModel.copyBarCode()
        copyCodeButton.setTitle(CreditPicPayLocalizable.copiedBarCode.text, for: .normal)
    }
    
    @objc
    func sendToEmailButtonTapped() {
        sendEmailButton.startLoadingAnimating(style: .gray, isEnabled: false)
        viewModel.sendToEmail { [weak self] in
            self?.sendEmailButton.stopLoadingAnimating(isEnabled: true)
        }
    }
}
