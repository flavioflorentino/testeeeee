import UI
import Foundation
import CoreLegacy
import AnalyticsModule
import Card

protocol CreditPicPayInvoicePayCoordinatorDelegate: AnyObject {
    func startPaymentSelectionType(_ invoicePayment: CreditInvoicePayment?)
    func payWithBill(_ invoicePayment: CreditInvoicePayment?)
    func showAlertError(_ error: Error, completion: (() -> Void)?)
    func showAlert(_ alert: Alert, completion: ((AlertPopupViewController, Button, UIPPButton) -> Void)?)
    func popViewController()
    func showPopup(_ popup: PopupViewController)
    func startBillNotificationScene()
}

final class CreditPicPayInvoicePayCoordinator: Coordinator {
    private let navigationController: UINavigationController
    private let account: CPAccount
    private let invoiceId: String
    private var invoicePayment: CreditInvoicePayment?
    var currentCoordinator: Coordinator?
    
    init(with navigationController: UINavigationController, invoiceId: String, account: CPAccount) {
        self.invoiceId = invoiceId
        self.account = account
        self.navigationController = navigationController
    }
    
    func start() {
        let viewModel = CreditPicPayInvoicePayViewModel(invoiceId: invoiceId, account: account, coordinatorDelegate: self, dependencies: DependencyContainer())
        let controller = CreditPicPayInvoicePayViewController(with: viewModel)
        currentCoordinator = self
        navigationController.pushViewController(controller, animated: true)
    }

    func showAlertError(_ error: Error, completion: (() -> Void)?) {
        AlertMessage.showCustomAlertWithError(error, controller: navigationController, completion: completion)
    }
    
    func showAlert(_ alert: Alert, completion: ((AlertPopupViewController, Button, UIPPButton) -> Void)?) {
        AlertMessage.showAlert(alert, controller: navigationController, action: completion)
    }
    
    func popViewController() {
        navigationController.popViewController(animated: true)
    }
}

extension CreditPicPayInvoicePayCoordinator: CreditPicPayInvoicePayCoordinatorDelegate {
    func startBillNotificationScene() {
        let viewController = BillNotificationFactory.make(invoiceId: invoiceId)
        viewController.modalPresentationStyle = .fullScreen
        navigationController.present(viewController, animated: true)
    }
    
    func startPaymentSelectionType(_ invoicePayment: CreditInvoicePayment?) {
        self.invoicePayment = invoicePayment
        startSelectPaymentType()
    }

    func payWithBill(_ invoicePayment: CreditInvoicePayment?) {
        self.invoicePayment = invoicePayment
        startBillCoordinator()
    }

    func showPopup(_ popup: PopupViewController) {
        navigationController.viewControllers.last?.showPopup(popup)
    }
}

extension CreditPicPayInvoicePayCoordinator: CreditPicPayInvoicePaymentTypeCoordinatorDelegate {
    private func startSelectPaymentType() {
        guard let invoicePayment = self.invoicePayment else {
            fatalError("there is not instantiated payment invoice")
        }
        let coordinator = CreditPicPayInvoicePaymentTypeCoordinator(with: navigationController, payment: invoicePayment)
        currentCoordinator = coordinator
        coordinator.delegate = self
        coordinator.start()
    }
    
    func pay() {
        guard let invoicePayment = self.invoicePayment else {
            fatalError("there is not instantiated payment invoice")
        }
        let coordinator = CreditPicPayPaymentCoordinator(with: navigationController, invoice: invoicePayment)
        currentCoordinator = coordinator
        coordinator.start()
    }
    
    func payWithInstallment() {
        startInstallmentCoordinator()
    }
    
    func payWithAnotherValue() {
        startOtherValuePaymentCoordinator()
    }
}

extension CreditPicPayInvoicePayCoordinator: CreditPicPayInstallmentCoordinatorDelegate {
    private func startInstallmentCoordinator() {
        guard let invoicePayment = self.invoicePayment else {
            fatalError("there is not instantiated payment invoice")
        }
        let coordinator = CreditPicPayInstallmentCoordinator(with: navigationController, invoicePayment: invoicePayment)
        currentCoordinator = coordinator
        coordinator.delegate = self
        coordinator.start()
    }
    
    func onTapInvoiceInstallment(_ invoice: CreditInvoicePayment.Installment.Option) {
        guard let invoicePayment = self.invoicePayment else {
            fatalError("there is not instantiated payment invoice")
        }
        let coordinator = CreditPicPayPaymentCoordinator(with: navigationController, invoice: invoicePayment)
        currentCoordinator = coordinator
        coordinator.start(with: invoice)
    }
    
    func onTapInformation() {
        let controller = CreditPicPayInformationInstallmentViewController()
        navigationController.pushViewController(controller, animated: true)
    }
}

extension CreditPicPayInvoicePayCoordinator: CreditPicPayOtherValuePaymentCoordinatorDelegate {
    private func startOtherValuePaymentCoordinator() {
        guard let invoicePayment = self.invoicePayment else {
            fatalError("there is not instantiated payment invoice")
        }
        let coordinator = CreditPicPayOtherValuePaymentCoordinator(with: navigationController, invoicePayment: invoicePayment)
        currentCoordinator = coordinator
        coordinator.delegate = self
        coordinator.start()
    }
    
    func nextButtonTapped(_ value: Double) {
        guard let invoicePayment = self.invoicePayment else {
            fatalError("there is not instantiated payment invoice")
        }
        let coordinator = CreditPicPayPaymentCoordinator(with: navigationController, invoice: invoicePayment)
        currentCoordinator = coordinator
        coordinator.startOtherValue(value)
    }
}

extension CreditPicPayInvoicePayCoordinator: CreditPicPayBillPaymentCoordinatorDelegate {
    private func startBillCoordinator() {
        guard let invoicePayment = self.invoicePayment else {
            fatalError("there is not instantiated payment invoice")
        }
        let coordinator = CreditPicPayBillPaymentCoordinator(with: navigationController, invoicePayment: invoicePayment)
        currentCoordinator = coordinator
        coordinator.delegate = self
        coordinator.start()
    }
}
