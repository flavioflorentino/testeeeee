import UI
import Foundation
import AnalyticsModule
import FeatureFlag

final class CreditPicPayInvoicePayViewModel {
    private let creditApi: CreditPicpayApi
    private let invoiceId: String
    private let account: CPAccount
    private let coordinatorDelegate: CreditPicPayInvoicePayCoordinatorDelegate?
    private var invoicePayment: CreditInvoicePayment? {
        didSet {
            guard let invoicePayment = invoicePayment else {
            return
        }
            valuePayment = invoicePayment.invoice.totalValue
        }
    }
    
    typealias Dependencies = HasAnalytics
    private var dependencies: Dependencies

    var dueDateFormatted: NSAttributedString? {
        guard let dueDate = invoicePayment?.invoice.dueDate else {
     return nil
}
        let dueDatetext = dueDate.stringFormatted(with: .ddDeMMMM)
        let text = CreditPicPayLocalizable.invoiceDetailDueDate.text
        let range = NSRange(location: 11, length: dueDatetext.count)
        let attributedString = NSMutableAttributedString(string: String(format: text, dueDatetext))
        attributedString.addAttributes([.foregroundColor: Palette.ppColorBranding300.color], range: range)
        return attributedString
    }
    
    var hasButtonPayment: Bool {
        return account.settings.appPaymentEnabled
    }
    
    var valuePayment: Double = 0.0
    
    init(invoiceId: String, account: CPAccount, coordinatorDelegate: CreditPicPayInvoicePayCoordinatorDelegate?, dependencies: Dependencies) {
        self.invoiceId = invoiceId
        self.account = account
        self.creditApi = CreditPicpayApi()
        self.coordinatorDelegate = coordinatorDelegate
        self.dependencies = dependencies
    }
    
    func loadDetails(_ completion: @escaping () -> Void) {
        creditApi.invoicePayment(invoiceId: invoiceId) { [weak self] result in
            switch result {
            case .success(let invoicePayment):
                self?.invoicePayment = invoicePayment
                completion()
            case .failure(let error):
                self?.coordinatorDelegate?.showAlertError(error, completion: {
                    self?.coordinatorDelegate?.popViewController()
                })
            }
        }
    }
    
    func payInvoiceWallet() {
        coordinatorDelegate?.startPaymentSelectionType(invoicePayment)
        dependencies.analytics.log(CardInvoiceEvent.methodsPayment(action: .payWithPicpay))
    }
    
    func payOnBill() {
        dependencies.analytics.log(CardInvoiceEvent.methodsPayment(action: .payWithBillet))
        if invoicePayment?.billEnabled == true {
            showBillAlertEnabled(for: invoicePayment)
        } else {
            FeatureManager.isActive(.isCardBilletNotificationAvailable) ?
                showBillNotificationAlertEnabled(for: invoicePayment) : showBillAlertDisabled(for: invoicePayment)
        }
    }

    private func showBillAlertEnabled(for invoicePayment: CreditInvoicePayment?) {
        let nextButton = Button(title: DefaultLocalizable.next.text, type: .cta) { [weak self] popupController, _ in
            popupController.dismiss(animated: true, completion: {
                self?.dependencies.analytics.log(CardInvoiceEvent.billPaymentConfirmAlert(action: .billContinue))
                self?.coordinatorDelegate?.payWithBill(invoicePayment)
            })
        }
        let clearButton = Button(title: DefaultLocalizable.notNow.text, type: .clean) { [weak self] popupController, _ in
            popupController.dismiss(animated: true) {
                self?.dependencies.analytics.log(CardInvoiceEvent.billPaymentConfirmAlert(action: .billAlertEnabledNotNow))
            }
        }
        let text = NSMutableAttributedString(string: CreditPicPayLocalizable.billDaysPaymentDescription.text,
                                             attributes: [.foregroundColor: Palette.ppColorGrayscale500.color])
        let range = NSRange(location: 31, length: 17)
        text.addAttributes([.font: UIFont.systemFont(ofSize: 13, weight: .bold)], range: range)
        let alert = Alert(with: nil, text: text, buttons: [nextButton, clearButton])
        alert.image = Alert.Image(with: Assets.CreditPicPay.calendarIcon.image)
        coordinatorDelegate?.showAlert(alert, completion: { _, _, _ in
        })
    }

    private func showBillNotificationAlertEnabled(for invoicePayment: CreditInvoicePayment?) {
        let popupViewController = PopupViewController(title: CreditPicPayLocalizable.billGeneratingNotificationTitle.text, description: CreditPicPayLocalizable.billGeneratingNotificationDescription.text, image: Assets.CreditPicPay.icoBillet.image)
        let payAction = PopupAction(title: CreditPicPayLocalizable.billGeneratingPayWithPicPay.text, style: .fill) { [weak self] in
            self?.dependencies.analytics.log(CardInvoiceEvent.billNotificationOptionsAlert(action: .payWithPicpay))
            self?.payInvoiceWallet()
        }
        let notificationAction = PopupAction(title: CreditPicPayLocalizable.billGeneratingWantBeNotified.text, style: .link) {
            [weak self] in
            self?.dependencies.analytics.log(CardInvoiceEvent.billNotificationOptionsAlert(action: .receiveNotification))
            self?.coordinatorDelegate?.startBillNotificationScene()
        }
        [payAction, notificationAction].forEach { popupViewController.addAction($0) }
        coordinatorDelegate?.showPopup(popupViewController)
    }
    
    private func showBillAlertDisabled(for invoicePayment: CreditInvoicePayment?) {
        let popupViewController = PopupViewController(
            title: CreditPicPayLocalizable.billGeneratingTitle.text,
            description: CreditPicPayLocalizable.billGeneratingDescription.text,
            preferredType: .image(Assets.Icons.icoAnalysisWarning.image)
        )
        
        let confirmAction = PopupAction(
            title: DefaultLocalizable.btOkUnderstood.text,
            style: .fill
        ) { [weak self] in
            self?.dependencies.analytics.log(CardInvoiceEvent.billPaymentInfoAlert)
        }
        popupViewController.addAction(confirmAction)
        
        coordinatorDelegate?.showPopup(popupViewController)
    }
}
