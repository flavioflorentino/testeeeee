import UI
import UIKit

final class CreditPicPayInvoicePayViewController: PPBaseViewController {
    private lazy var valueInformationView: ValueCurrencyInformationView = {
        let view = ValueCurrencyInformationView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isEnableValueText = false
        return view
    }()
    private lazy var containerValueInformation: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [valueInformationView])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .fill
        return stackView
    }()
    private lazy var firstCardButton: CardButtonView = {
        let button = CardButtonView()
        button.imageView.image = #imageLiteral(resourceName: "picpay-card-icon")
        button.titleLabel.text = CreditPicPayLocalizable.cardPicPayTitle.text
        button.descriptionLabel.text = CreditPicPayLocalizable.cardPicPayDescription.text
        button.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(payButtonTapped)))
        return button
    }()
    private lazy var secondCardButton: CardButtonView = {
        let button = CardButtonView()
        button.imageView.image = #imageLiteral(resourceName: "boleto-logo")
        button.titleLabel.text = CreditPicPayLocalizable.cardBillTitle.text
        button.descriptionLabel.text = CreditPicPayLocalizable.cardBillDescription.text
        button.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(billButtonTapped)))
        return button
    }()
    private lazy var cardStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()
    
    private let viewModel: CreditPicPayInvoicePayViewModel
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    init(with viewModel: CreditPicPayInvoicePayViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        addComponents()
        layoutComponents()
        load()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        PPAnalytics.trackEvent(CreditPicPayEvent.Registration.invoicePaymentView, properties: nil)
    }
    
    private func addComponents() {
        if viewModel.hasButtonPayment {
            cardStackView.addArrangedSubview(firstCardButton)
        }
        cardStackView.addArrangedSubview(secondCardButton)
        
        view.addSubview(containerValueInformation)
        view.addSubview(cardStackView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            containerValueInformation.topAnchor.constraint(equalTo: view.topAnchor, constant: 35),
            containerValueInformation.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            containerValueInformation.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            
            cardStackView.topAnchor.constraint(equalTo: containerValueInformation.bottomAnchor, constant: 35),
            cardStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            cardStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            cardStackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20),
            ])
        
        let bottomSpacing: CGFloat = 20.0
        if #available(iOS 11.0, *) {
            cardStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -bottomSpacing).isActive = true
        } else {
            cardStackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -bottomSpacing).isActive = true
        }
    }
}

extension CreditPicPayInvoicePayViewController {
    private func load() {
        startLoadingView()
        viewModel.loadDetails { [weak self] in
            self?.stopLoadingView()
            self?.setupInformationView()
        }
    }
    
    private func setupInformationView() {
        valueInformationView.changeValue(viewModel.valuePayment)
        valueInformationView.primaryLabel.text = CreditPicPayLocalizable.valueInvoice.text
        valueInformationView.secundaryLabel.attributedText = viewModel.dueDateFormatted
    }
    
    private func setupView() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
        title = CreditPicPayLocalizable.payInvoice.text
    }
    
    @objc
    private func payButtonTapped() {
        viewModel.payInvoiceWallet()
        PPAnalytics.trackEvent(CreditPicPayEvent.Registration.invoicePaymentPicPay, properties: nil)
    }
    
    @objc
    private func billButtonTapped() {
        viewModel.payOnBill()
        PPAnalytics.trackEvent(CreditPicPayEvent.Registration.invoicePaymentBill, properties: nil)
    }
}
