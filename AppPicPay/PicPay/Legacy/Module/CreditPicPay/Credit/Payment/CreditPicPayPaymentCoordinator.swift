import AnalyticsModule
import FeatureFlag
import UI

protocol CreditPicPayPaymentCoordinatorDelegate: AnyObject {
    func popViewController()
    func show(receipt: ReceiptWidgetViewModel)
    func showInstallments(manager: PPPaymentManager)
    func openCvv(completedCvv: @escaping (String) -> Void, completedWithoutCvv: @escaping () -> Void)
}

final class CreditPicPayPaymentCoordinator: Coordinator {
    typealias Dependencies = HasFeatureManager & HasAnalytics
    private let dependencies: Dependencies
    
    init(
        navigationController: UINavigationController,
        invoice: CreditInvoicePayment,
        dependencies: Dependencies = DependencyContainer()
    ) {
        self.navigationController = navigationController
        self.invoice = invoice
        self.dependencies = dependencies
    }
    
    private let navigationController: UINavigationController
    private var invoice: CreditInvoicePayment
    private var creditPaymentViewController: CreditPaymentViewController?
    private var coordinator: Coordinating?
    var currentCoordinator: Coordinator?
    var viewModel: CreditPaymentViewModel?
    
    init(
        with navigationController: UINavigationController,
        invoice: CreditInvoicePayment,
        dependencies: Dependencies = DependencyContainer()
    ) {
        self.navigationController = navigationController
        self.invoice = invoice
        self.dependencies = dependencies
    }
    
    func start() {
        invoice.updateValueToPay(invoice.total)
        var viewModel = CreditPaymentViewModel(invoice: invoice, servicePayment: createServicePayment(), dependencies: dependencies)
        viewModel.delegate = self
        self.viewModel = viewModel
        pushCreditPaymentController(with: viewModel)
    }
    
    func start(with option: CreditInvoicePayment.Installment.Option) {
        invoice.updateValueToPay(option.installmentsValue)
        var viewModel = CreditPaymentViewModel(invoice: invoice, paymentMethod: .installments, servicePayment: createServicePayment(), dependencies: dependencies)
        viewModel.delegate = self
        self.viewModel = viewModel
        pushCreditPaymentController(with: viewModel)
    }
    
    func startOtherValue(_ value: Double) {
        invoice.updateValueToPay(value)
        var viewModel = CreditPaymentViewModel(invoice: invoice, paymentMethod: .other, servicePayment: createServicePayment(), dependencies: dependencies)
        viewModel.delegate = self
        self.viewModel = viewModel
        pushCreditPaymentController(with: viewModel)
    }
    
    private func createServicePayment() -> CreditPaymentServicing {
        guard dependencies.featureManager.isActive(.pciInvoiceCard) else {
            return CreditPaymentService()
        }
        
        return CreditPaymentPciService()
    }
    
    private func pushCreditPaymentController(with viewModel: CreditPaymentViewModel) {
        currentCoordinator = self
        guard dependencies.featureManager.isActive(.newInvoiceCard) else {
            let controller = CreditPaymentViewController(viewModel: viewModel)
            self.creditPaymentViewController = controller
            navigationController.pushViewController(controller, animated: true)
            return
        }
        
        createNewInvoiceCardPayment(viewModel: viewModel)
    }
    
    private func createNewInvoiceCardPayment(viewModel: CreditPaymentViewModel) {
        guard let value = invoice.paymentValue else {
            return
        }
        
        let model = InvoiceCardPaymentModel(payeeId: invoice.invoice.id, title: viewModel.paymentType, value: value)
        let viewController = CreditPaymentLoadingFactory.make(model: model)
        navigationController.pushViewController(viewController, animated: true)
    }
}

extension CreditPicPayPaymentCoordinator: CreditPicPayPaymentCoordinatorDelegate {
    func popViewController() {
        navigationController.popViewController(animated: true)
    }
    
    func show(receipt: ReceiptWidgetViewModel) {
        guard let paymentViewController = creditPaymentViewController else {
            fatalError("We should have an CreditPaymentViewController instance")
        }
    
        DispatchQueue.main.async {
            TransactionReceipt.showReceiptSuccess(viewController: paymentViewController, receiptViewModel: receipt)
        }
    }
    
    func showInstallments(manager: PPPaymentManager) {
        let viewModel = InstallmentsSelectorViewModel(paymentManager: manager)
        let controller = InstallmentsSelectorViewController(with: viewModel)
        controller.statusbarStyle = .lightContent
        viewModel.delegate = self
        
        navigationController.pushViewController(controller, animated: true)
    }
    
    func openCvv(completedCvv: @escaping (String) -> Void, completedWithoutCvv: @escaping () -> Void) {
        let coordinator = CvvRegisterFlowCoordinator(
            navigationController: navigationController,
            paymentType: .invoice,
            finishedCvv: completedCvv,
            finishedWithoutCvv: completedWithoutCvv
        )
        coordinator.start()
        self.coordinator = coordinator
    }
}

extension CreditPicPayPaymentCoordinator: InstallmentsSelectorViewModelDelegate {
    func didSelectedInstallment(_ paymentManager: PPPaymentManager) {
        guard let paymentViewController = creditPaymentViewController else {
            fatalError("We should have an CreditPaymentViewController instance")
        }
        paymentViewController.updateMontlyPayments(paymentManager.selectedQuotaQuantity)
    }
}
