import AnalyticsModule

final class CreditPaymentViewController: PPBaseViewController {
    private var viewModel: CreditPaymentViewModel!
    
    lazy private var rootView: CreditPaymentView = {
        let rootView = CreditPaymentView()
        view.addSubview(rootView)
        rootView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            rootView.topAnchor.constraint(equalTo: view.topAnchor),
            rootView.bottomAnchor.constraint(equalTo: bottomLayoutGuide.topAnchor),
            rootView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            rootView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            ])
        rootView.paymentToolbar.trackingDelegate = self
        return rootView
    }()
    
    // MARK: Payment Toolbar stuff
    private var paymentManager = PPPaymentManager()
    private var toolBarController: PaymentToolbarController?
    private var auth: PPAuth?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    required init(viewModel: CreditPaymentViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let isNotPayingWithCard = (paymentManager?.cardTotal()?.doubleValue ?? 0) == 0
        rootView.hideInconvenienceTax(isNotPayingWithCard)
    }
    
    @available(*, deprecated, message: "use init:viewModel instead")
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    @available(*, deprecated, message: "use init:viewModel instead")
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
        setupToolbar()
        setupViewModel()
        addGestureRecognizer()
        
        viewModel.loadDisclaimer()
    }
    
    private func setupController() {
        title = CreditPicPayLocalizable.paymentTitle.text
        view.addSubview(rootView)
        if hasSafeAreaInsets {
            paintSafeAreaBottomInset(withColor: rootView.paymentToolbar.bottomBarBackgroundColor ?? .clear)
        }
    }
    
    private func setupViewModel() {
        setupViewModelLoading()
        setupViewModelOnErrorDisclaimer()
        setupViewModelDisclaimer()
        setupViewModelPayment()
        setupViewModeAlertTax()
    }
    
    private func setupViewModelLoading() {
        viewModel.isLoading = { [weak self] isLoading in
            DispatchQueue.main.async {
                guard let self = self else {
                    return
                }
                self.loading(isLoading, view: self.rootView.loadingView)
            }
        }
        
        viewModel.isLoadingPayment = { [weak self] isLoading in
            DispatchQueue.main.async {
                self?.rootView.loadingTransactionView.isHidden = !isLoading
            }
        }
    }
    
    private func loading(_ isLoading: Bool, view: UILoadView) {
        if isLoading {
            view.startLoading()
        } else {
            view.stopLoading()
            view.isHidden = true
        }
    }
    
    private func setupViewModelOnErrorDisclaimer() {
        viewModel.onErrorDisclaimer = { [weak self] error in
            DispatchQueue.main.async {
                AlertMessage.showAlert(error, controller: self, completion: {
                    self?.viewModel.goBack()
                })
            }
        }
    }
    
    private func setupViewModelDisclaimer() {
        viewModel.onDisclaimerLoad = { [weak self] disclaimer in
            DispatchQueue.main.async {
                self?.viewModel.disclaimerInfo = disclaimer
                if let text = disclaimer.text {
                    self?.rootView.showDisclaimerText(text)
                }
            
                self?.updateValues()
                self?.updateInterest(disclaimer.convenienceTax)
            }
        }
    }
    
    private func setupViewModelPayment() {
        viewModel.askForAuthentication = { [weak self] in
            self?.viewModel.tracking.usedFingerPrint(true)
            self?.auth = PPAuth.authenticate({ (password, biometry) in
                self?.checkoutProceed(password, isBiometry: biometry)
            }, canceledByUserBlock: {  [weak self] in
                self?.viewModel.tracking.alertCancel()
            })
        }
        viewModel.onErrorPayment = { [weak self] error in
            self?.viewModel.tracking.authenticationFailed()
            DispatchQueue.main.async {
                AlertMessage.showCustomAlertWithError(error, controller: self)
            }
        }
    }
    
    private func setupToolbar() {
        guard let manager = paymentManager else {
            return
        }
        updatePaymentToolbarWithTotal(viewModel.paymentValue)
        toolBarController = PaymentToolbarController(
            paymentManager: manager,
            parent: self,
            toolbar: rootView.paymentToolbar,
            privacyConfig: .Private,
            hasPrivacyView: false,
            pay: { [weak self] privacyConfig in
                self?.viewModel.userSelectedPrivacyConfig = privacyConfig
                self?.viewModel.setupAlertTax(with: manager)
                self?.checkout()
            },
            changeMethod: {
                PPAnalytics.trackEvent(CreditPicPayEvent.Registration.paymentCreditMethods, properties: nil)
        }
        )
    }
    
    private func setupViewModeAlertTax() {
        viewModel.showAlertTax = { [weak self] alert in
            guard let strongSelf = self else {
                return
            }
            let actionBlock: AlertMessage.ButtonActionBlock = { popupController, _, _ in
                popupController.dismiss(animated: true, completion: nil)
            }
            AlertMessage.showAlert(alert, controller: strongSelf, action: actionBlock)
        }
        
        viewModel.showAlertTaxOnPayment = { [weak self] alert in
            guard let strongSelf = self else {
                return
            }
            let actionBlock: AlertMessage.ButtonActionBlock = { popupController, button, _ in
                if button.action != .close {
                    popupController.dismiss(animated: true, completion: {
                        self?.viewModel.skipAlertTax = true
                        self?.acceptedInconvenienceTax()
                    })
                } else {
                    self?.viewModel.tracking.payLaterEvent()
                }
            }
            AlertMessage.showAlert(alert, controller: strongSelf, action: actionBlock)
        }
    }
    
    private func updateValues() {
        rootView.topInfo.topLabel.text = viewModel.paymentType
        rootView.topInfo.bottomLabel.text = viewModel.valueToPayDescription
        rootView.topInfo.updateInstallments(viewModel.montlyPayments)
        rootView.total.rightLabel.text = viewModel.total
    }
    
    private func updateInterest(_ interest: Double) {
        rootView.interest.rightLabel.attributedText = NSAttributedString.withImageOn(side: .left, image: #imageLiteral(resourceName: "credIconInfo"), text: "\(interest)%", font: rootView.interest.rightLabel.font)
    }
    
    private func checkout() {
        viewModel.checkout()
    }
    
    private func acceptedInconvenienceTax() {
        viewModel.tracking.acceptedTax()
        viewModel.checkout()
    }
    
    @objc
    private func tapInstallments() {
        viewModel.tapInstallments(with: paymentManager!)
        PPAnalytics.trackEvent(CreditPicPayEvent.Registration.paymentCreditInstallment, properties: nil)
    }
    
    private func checkoutProceed(_ password: String?, isBiometry: Bool) {
        viewModel.tracking.authenticationValidated()
        let credit = paymentManager?.balanceTotal()?.stringValue ?? "0"
        let total = paymentManager?.cardTotalWithoutInterest()?.stringValue ?? "0"
        let pin = password ?? ""
        viewModel.pay(pin, isBiometry: isBiometry, credit: credit, total: total)
    }
}

extension CreditPaymentViewController {
    private func addGestureRecognizer() {
        rootView.interest.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapInfoInterest)))
        
        rootView.topInfo.installments.addTarget(self, action: #selector(tapInstallments), for: .touchUpInside)
        rootView.topInfo.newInstallmentButton.addTarget(self, action: #selector(tapInstallments), for: .touchUpInside)
    }
    
    @objc
    private func tapInfoInterest() {
        viewModel.tapInfoInterest()
    }
}

extension CreditPaymentViewController {
    private func updatePaymentToolbarWithTotal(_ value: Decimal) {
        paymentManager?.subtotal = value as NSDecimalNumber
        toolBarController?.paymentManager = paymentManager
    }
}

extension CreditPaymentViewController {
    func updateMontlyPayments(_ montlyPayments: Int) {
        viewModel.update(montlyPayments: montlyPayments, manager: paymentManager)
        updateValues()
    }
}

extension CreditPaymentViewController: TrackingPaymentToolbarDelegate {
    func payPressInButton() {
        viewModel.tracking.cardPayment(paymentName: rootView.paymentToolbar.paymentMethodLabel.text)
    }
    
    func paymentMethodSelection() {
        viewModel.tracking.pressInMethodsPayment()
    }

}
