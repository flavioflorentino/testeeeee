import AnalyticsModule
import FeatureFlag
import PCI
import UI

struct CreditPaymentViewModel {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies
    private let paymentMethod: PaymentMethod
    private let invoice: CreditInvoicePayment
    private let servicePayment: CreditPaymentServicing
    private let service: CreditApiProtocol
    let tracking: CreditPaymentTrackingInvoice
    private(set) var montlyPayments: Int = 1
    var userSelectedPrivacyConfig: String?
    var disclaimerInfo: DisclaimerInfo?
    var skipAlertTax = false
    weak var delegate: CreditPicPayPaymentCoordinatorDelegate?
    
    private var isInvertedInstallmentActive: Bool {
        dependencies.featureManager.isActive(.experimentInvertedInstallment)
    }
    
    var paymentValue: Decimal {        
        return Decimal(floatLiteral: invoice.paymentValue ?? 0.0)
    }
    
    var valueToPayDescription: String {
        guard let paymentValue = invoice.paymentValue else { return "" }
        return paymentValue.getFormattedPrice()
    }
    
    var total: String {
        return invoice.total.getFormattedPrice()
    }
    
    var paymentType: String {
        return paymentMethod.description
    }
    
    var isLoading: (_ loading: Bool) -> Void = { _ in }
    var onErrorDisclaimer: (_ error: Error) -> Void = { _ in }
    var onDisclaimerLoad: (_ text: DisclaimerInfo) -> Void = { _ in }
    
    var isLoadingPayment: (_ loading: Bool) -> Void = { _ in }
    var onErrorPayment: (_ error: Error) -> Void = { _ in }
    
    var showAlertTaxOnPayment: (_ alert: Alert) -> Void = { _ in }
    var showAlertTax: (_ alert: Alert) -> Void = { _ in }
    
    var askForAuthentication: () -> Void = { }
    
    init(
        invoice: CreditInvoicePayment, 
        paymentMethod: PaymentMethod = .total,
        servicePayment: CreditPaymentServicing,
        service: CreditApiProtocol = CreditPicpayApi(),
        dependencies: Dependencies, tracking: CreditPaymentTrackingInvoice = CreditPaymentTrackingInvoice()
    ) {
        self.invoice = invoice
        self.paymentMethod = paymentMethod
        self.servicePayment = servicePayment
        self.service = service
        self.dependencies = dependencies
        self.tracking = tracking
    }
    
    func tapInfoInterest() {
        showAlertTax(createAlert())
    }
    
    func tapInstallments(with manager: PPPaymentManager) {
        delegate?.showInstallments(manager: manager)
        guard let value = (manager.subtotal as? Double)?.toString() else {
            return
        }
        dependencies.analytics.log(InstallmentButtonEvent.installmentAccessed(value, isInvertedInstallmentActive))
    }
    
    mutating func setupAlertTax(with paymentManager: PPPaymentManager) {
        let useBalance = paymentManager.usePicPayBalance()
        guard let balance = paymentManager.balance() else {
            return
        }
        skipAlertTax = (useBalance && paymentValue <= balance.decimalValue)
    }
    
    func checkout() {
        guard skipAlertTax else {
            showAlertTaxOnPayment(createAlert())
            return
        }
        askForAuthentication()
    }
    
    mutating func update(montlyPayments: Int, manager: PPPaymentManager?) {
        self.montlyPayments = montlyPayments
        guard
            let manager = manager,
            let value = (manager.subtotal as? Double)?.toString(),
            let installments = montlyPayments.toString() else {
            return
        }
        dependencies.analytics.log(InstallmentButtonEvent.installmentSelected(installments, value, isInvertedInstallmentActive))
    }
    
    func pay(_ password: String, isBiometry: Bool, credit: String, total: String) {
        let payload = createPayload(biometry: isBiometry, credit: credit, total: total)
        
        guard checkNeedCvv(cardValue: total) else {
            self.payInvoice(with: payload, cardValue: total, password: password)
            return
        }
        
        delegate?.openCvv(completedCvv: { cvv in
            self.payInvoice(with: payload, cardValue: total, password: password, informedCvv: cvv)
        }, completedWithoutCvv: {
            let error = PicPayError(message: DefaultLocalizable.cvvNotInformed.text)
            self.onErrorPayment(error)
        })
    }
    
    private func success(receipt: ReceiptWidgetViewModel) {
        delegate?.show(receipt: receipt)
    }
    
    func goBack() {
        delegate?.popViewController()
    }
}

extension CreditPaymentViewModel {
    enum PaymentMethod {
        case total
        case installments
        case other
        
        var description: String {
            switch self {
            case .total:
                return CreditPicPayLocalizable.paymentMethodTotal.text
            case .installments:
                return CreditPicPayLocalizable.paymentMethodInstallments.text
            case .other:
                return CreditPicPayLocalizable.paymentMethodOther.text
            }
        }
    }
}

extension CreditPaymentViewModel {
    private func createPayload(biometry: Bool, credit: String, total: String) -> InvoiceCardPayload {
        return InvoiceCardPayload(
            origin: "credit_invoice_payment",
            biometry: biometry,
            credit: credit,
            value: total,
            feedVisibility: userSelectedPrivacyConfig ?? FeedItemVisibility.Private.rawValue,
            cardId: CreditCardManager.shared.defaultCreditCardId,
            installments: montlyPayments
        )
    }
    
    private func payInvoice(with payload: InvoiceCardPayload, cardValue: String, password: String, informedCvv: String? = nil) {
        isLoadingPayment(true)
        let invoiceId = invoice.invoice.id
        let cvv = recoverCvv(cardValue: cardValue, informedCvv: informedCvv)
        servicePayment.payInvoice(password: password, invoiceId: invoiceId, cvv: cvv, payload: payload) { result in
            self.isLoadingPayment(false)
            switch result {
            case .success(let creditPayment):
                let receiptModel = ReceiptWidgetViewModel(type: .PAV)
                receiptModel.setReceiptWidgets(items: creditPayment.receipts.items)
                self.saveCvv(informedCvv: informedCvv)
                self.success(receipt: receiptModel)
            case .failure(let error):
                self.onErrorPayment(error)
            }
        }
    }
    
    private func checkNeedCvv(cardValue: String) -> Bool {
        guard
            let cardBank = service.defauldCard,
            let value = Double(cardValue)
            else {
                return false
        }
        
        return service.pciCvvIsEnable(cardBank: cardBank, cardValue: value)
    }
    
    private func saveCvv(informedCvv: String?) {
        guard let cvv = informedCvv else {
            return
        }
        
        let id = service.defauldCardId
        service.saveCvvCard(id: id, value: cvv)
    }
    
    private func recoverCvv(cardValue: String, informedCvv: String?) -> String? {
        guard let cvv = informedCvv else {
            return localStoreCvv(cardValue: cardValue)
        }
        
        return cvv
    }
    
    private func localStoreCvv(cardValue value: String) -> String? {
        let id = service.defauldCardId
        guard
            let cardValue = Double(value),
            cardValue != .zero,
            let cvv = service.cvvCard(id: id)
            else {
                return nil
        }
        
        return cvv
    }
}

extension CreditPaymentViewModel {
    struct DisclaimerInfo: Codable {
        let text: String?
        let paymentDisabled: Bool
        let convenienceTax: Double
    }
    
    func loadDisclaimer() {
        isLoading(true)
        requestDisclaimer(for: invoice) { (result) in
            self.isLoading(false)
            switch result {
            case .success(let disclaimer):
                self.onDisclaimerLoad(disclaimer)
            case .failure(let error):
                self.onErrorDisclaimer(error)
            }
        }
    }
    
    private func requestDisclaimer(for payment: CreditInvoicePayment, _ completion: @escaping (PicPayResult<DisclaimerInfo>) -> Void) {
        let endpoint = kWsUrlCreditPicpayInvoicePaymentMessage
            .replacingOccurrences(of: "{id}", with: payment.invoice.id)
            .replacingOccurrences(of: "{value}", with: "\(payment.paymentValue ?? 0.0)")
        RequestManager.shared.apiRequest(endpoint: endpoint, method: .get).responseApiCodable(completionHandler: { result in
            completion(result)
        })
    }
}

extension CreditPaymentViewModel {
    private func incovenienceTaxValue() -> String {
        guard let convenienceTax = disclaimerInfo?.convenienceTax,
            let balance = ConsumerManager.shared.consumer?.balance,
            let paymentValue = invoice.paymentValue else { return "" }
        
        let tax: Double = convenienceTax / 100
        let valueToPay = NSDecimalNumber(floatLiteral: paymentValue)
        let useBalance = ConsumerManager.useBalance()
        let isBigger = balance.compare(valueToPay) == .orderedDescending
        
        if !useBalance {
            return Double(paymentValue * tax).getFormattedPrice()
        } else if isBigger {
            let result = balance.subtracting(valueToPay).doubleValue * tax
            return result.getFormattedPrice()
        } else {
            let result = valueToPay.subtracting(balance).doubleValue * tax
            return result.getFormattedPrice()
        }
    }
    
    private func alertAttributedString(taxValue: Double) -> NSAttributedString {
        let tax = "\(CreditPicPayLocalizable.alertTax.text) \(taxValue)%: "
        let value = incovenienceTaxValue()
        
        let attributedString = NSMutableAttributedString()
        attributedString.appendString(CreditPicPayLocalizable.alertConvenienceTaxSubtitle.text)
        attributedString.appendString(tax)
        attributedString.appendString(value)
        
        let taxAndValueRange = (attributedString.string as NSString).range(of: "\(tax)\(value)")
        let valueRange = (attributedString.string as NSString).range(of: value)
        
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 14, weight: .semibold), range: taxAndValueRange)
        attributedString.addAttribute(.foregroundColor, value: Palette.ppColorBranding300.color, range: valueRange)
        
        return attributedString
    }
    
    private func createAlert() -> Alert {
        guard let convenienceTax = disclaimerInfo?.convenienceTax else {
            return Alert(title: "", text: "")
        }
        
        let popUp = Alert(title: NSAttributedString(string: CreditPicPayLocalizable.alertConvenienceTaxTitle.text), text: alertAttributedString(taxValue: convenienceTax))
        popUp.dismissOnTouchBackground = false
        popUp.dismissWithGesture = false
        popUp.showCloseButton = false
        popUp.buttons = [
            Button(title: CreditPicPayLocalizable.alertAcceptButton.text, type: .cta, action: .confirm),
            Button(title: CreditPicPayLocalizable.alertDeclineButton.text, type: .inline, action: .close)
        ]
        return popUp
    }
}
