import Foundation
import AnalyticsModule

final class CreditPaymentTrackingInvoice {
    typealias Dependencies = HasAnalytics
    private var dependencies: Dependencies = DependencyContainer()

    enum FormOfpayment: String {
        case balance = "Saldo"
        case card = ""
        case balanceAndCard = "+"
    }
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    private func paymentFormat(payment descripition: String) -> FormOfpayment {
        if descripition.contains(FormOfpayment.balance.rawValue) && !descripition.contains(FormOfpayment.balanceAndCard.rawValue){
            return .balance
        }
        else if descripition.contains(FormOfpayment.balanceAndCard.rawValue) {
            return .balanceAndCard
        }
        return .card
    }
    
    func cardPayment(paymentName: String?) {
        guard let paymentName = paymentName else { return }
        switch paymentFormat(payment: paymentName){
        case .balance:
            dependencies.analytics.log(CardInvoiceEvent.creditPayment(action: .balance))
        case .card:
            dependencies.analytics.log(CardInvoiceEvent.creditPayment(action: .card))
        case .balanceAndCard:
            dependencies.analytics.log(CardInvoiceEvent.creditPayment(action: .balanceAndCard))
        }
     }
    
    func pressInMethodsPayment() {
        dependencies.analytics.log(CardInvoiceEvent.cardInvoicePaymentMethod)
    }
    
    func alertCancel() {
        dependencies.analytics.log(CardInvoiceEvent.paymentInvoiceAuthAlert(action: .fingerPrintCancelAlert))
    }
    
    func usedFingerPrint(_ isBiometry: Bool) {
        dependencies.analytics.log(CardInvoiceEvent.paymentInvoiceAuthAlert(action: .fingerPrintUsed))
    }
    
    func authenticationValidated() {
        dependencies.analytics.log(CardInvoiceEvent.paymentInvoiceAuthAlert(action: .authenticationValidated))
    }
    
    func authenticationFailed() {
        dependencies.analytics.log(CardInvoiceEvent.paymentInvoiceAuthAlert(action: .authenticationValidated))
    }
    
    func acceptedTax() {
        dependencies.analytics.log(CardInvoiceEvent.additonalTaxAlert(action: .agree))
    }
    
    func payLaterEvent() {
        dependencies.analytics.log(CardInvoiceEvent.additonalTaxAlert(action: .notNow))
    }
}
