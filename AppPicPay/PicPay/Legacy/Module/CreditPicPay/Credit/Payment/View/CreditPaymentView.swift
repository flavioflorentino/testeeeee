import UI

final class CreditPaymentView: UIView {
    private var rootStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.axis = .vertical
        return stackView
    }()
    
    var topInfo: CreditTopInfoView = {
        let view = CreditTopInfoView()
        return view
    }()
    
    lazy var total: CreditTwoLabelsView = {
        let view = CreditTwoLabelsView()
        view.leftLabel.text = CreditPicPayLocalizable.billTotal.text
        return view
    }()
    
    lazy var interest: CreditTwoLabelsView = {
        let view = CreditTwoLabelsView()
        view.leftLabel.text = CreditPicPayLocalizable.inconvenienceTax.text
        return view
    }()
    
    lazy var disclaimer: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale200.color
        view.layer.cornerRadius = 6
        view.layer.masksToBounds = true
        
        return view
    }()
    
    lazy var disclaimerLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = Palette.ppColorGrayscale200.color
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.numberOfLines = 0
        
        disclaimer.addSubview(label)
        NSLayoutConstraint.leadingTrailing(equalTo: disclaimer, for: [label], constant: 16)
        label.topAnchor.constraint(equalTo: disclaimer.topAnchor, constant: 17).isActive = true
        label.bottomAnchor.constraint(equalTo: disclaimer.bottomAnchor, constant: -22).isActive = true
        
        return label
    }()
    
    let paymentToolbarHeight: CGFloat = 92
    lazy var paymentToolbar: PaymentToolbar = {
        let toolbar = PaymentToolbar(frame: .zero)
        toolbar.isUserInteractionEnabled = true
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        return toolbar
    }()
    
    lazy var loadingView: UILoadView = {
        let loadingView = UILoadView(superview: self, position: .center)
        addSubview(loadingView)
        return loadingView
    }()
    
    lazy var loadingTransactionView: UIView = {
        let loadingTransactionView = Loader.getLoadingView(DefaultLocalizable.completingTransaction.text) ?? UILoadView(superview: self, position: .center)
        addSubview(loadingTransactionView)
        return loadingTransactionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale000.color
        
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showDisclaimerText(_ text: String) {
        disclaimerLabel.text = text
    }
    
    func hideInconvenienceTax(_ hide: Bool) {
        interest.isHidden = hide
    }

    private func addComponents() {
        rootStackView.addArrangedSubview(topInfo)
        rootStackView.addArrangedSubview(total)
        rootStackView.addArrangedSubview(interest)
        rootStackView.addArrangedSubview(disclaimer)
        addSubview(rootStackView)
        addSubview(paymentToolbar)
        addCustomSpacing()
    }
    
    private func addCustomSpacing() {
        let lineColor = Palette.ppColorGrayscale400.color
        let lineView = rootStackView.insertLine(after: topInfo, color: lineColor)
        rootStackView.setSpacing(17, after: lineView)
        rootStackView.setSpacing(23, after: interest)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.leadingTrailing(equalTo: self, for: [rootStackView, topInfo, total, interest, paymentToolbar])
        NSLayoutConstraint.leadingTrailing(equalTo: self, for: [disclaimer], constant: 16)
        rootStackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        paymentToolbar.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        paymentToolbar.heightAnchor.constraint(equalToConstant: paymentToolbarHeight).isActive = true
    }
}
