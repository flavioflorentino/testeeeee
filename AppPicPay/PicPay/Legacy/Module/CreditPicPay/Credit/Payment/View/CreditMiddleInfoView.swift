import UI

final class CreditTwoLabelsView: UIView {
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.axis = .horizontal
        stackView.spacing = 6.0
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    lazy var leftLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13.0)
        label.textColor = Palette.ppColorGrayscale400.color
        return label
    }()
    
    lazy var rightLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = UIFont.systemFont(ofSize: 12.0, weight: .bold)
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()
    
    var imageRight: UIImage?
    
    convenience init(frame: CGRect, leftLabel: String, rightLabel: String, imageRightLabel: UIImage? = nil) {
        self.init(frame: frame)
        
        self.leftLabel.text = leftLabel
        self.rightLabel.text = rightLabel
        self.imageRight = imageRightLabel
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale000.color
        
        addComponents()
        contentHuggingPriority()
        layoutComponents()
        isUserInteractionEnabled = true
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        rootStackView.addArrangedSubview(leftLabel)
        rootStackView.addArrangedSubview(rightLabel)
        addSubview(rootStackView)
    }
    
    private func contentHuggingPriority() {
        leftLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 1000.0), for: .horizontal)
        leftLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000.0), for: .horizontal)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.constraintAllEdges(from: rootStackView, to: self)
        rootStackView.heightAnchor.constraint(greaterThanOrEqualToConstant: 28.0).isActive = true
    }
}
