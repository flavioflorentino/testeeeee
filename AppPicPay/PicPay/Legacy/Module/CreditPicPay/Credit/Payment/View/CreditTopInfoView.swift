import AnalyticsModule
import FeatureFlag
import UI

extension CreditTopInfoView.Layout {
    enum MarginLayout {
        static let rootStack = UIEdgeInsets(top: Spacing.none, left: Spacing.base02, bottom: Spacing.base02, right: Spacing.base02)
        static let installmentButton = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    enum CornerRadius {
        static let imageRadius = CGFloat(31)
    }
    
    enum FontSize {
        /// Default value 12.0
        static let size00 = UIFont.systemFont(ofSize: 12.0)
        
        /// Default value 15.0
        static let size01 = UIFont.systemFont(ofSize: 15.0)
        
        /// Default value 24.0
        static let size02 = UIFont.systemFont(ofSize: 24.0)
    }
    
    enum Sizes {
        static let imageWidth = CGFloat(62)
        static let imageHeight = CGFloat(62)
        static let installmentButtonWidth = CGFloat(30)
        static let installmentButtonHeight = CGFloat(30)
        
        /// Default value 11.0
        static let topSize = CGFloat(11)
    }
}

final class CreditTopInfoView: UIView {
    fileprivate enum Layout {}
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.layoutMargins = Layout.MarginLayout.rootStack
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var labelStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var infoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .leading
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = Layout.CornerRadius.imageRadius
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = Palette.ppColorGrayscale600.color
        imageView.image = #imageLiteral(resourceName: "creditreceipt")
        return imageView
    }()
    
    lazy var topLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.FontSize.size00
        label.textColor = Palette.ppColorGrayscale400.color
        return label
    }()
    
    lazy var bottomLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.FontSize.size02
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()
    
    lazy var installments: UIButton = {
        let button = UIButton()
        button.layer.borderWidth = Border.light
        button.layer.borderColor = Palette.ppColorBranding300.cgColor
        button.layer.cornerRadius = CornerRadius.medium
        button.setTitle(
            String(format: CreditPicPayLocalizable.invoiceInstallmentValue.text, 1),
            for: .normal
        )
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.titleLabel?.font = Layout.FontSize.size01
        return button
    }()
    
    lazy var newInstallmentButton: UIButton = {
        let button = UIButton()
        button.layer.borderWidth = Border.light
        button.layer.borderColor = Palette.ppColorBranding300.cgColor
        button.layer.cornerRadius = CornerRadius.medium
        button.setTitle(
            String(format: CreditPicPayLocalizable.invoiceNewInstallmentValue.text, 1),
            for: .normal
        )
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.titleLabel?.font = Layout.FontSize.size01
        button.contentEdgeInsets = Layout.MarginLayout.installmentButton
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale000.color
        
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateInstallments(_ numberOfMontlyPayments: Int) {
        if FeatureManager.isActive(.featureInstallmentNewButton) {
            newInstallmentButton.setTitle(
                String(format: CreditPicPayLocalizable.invoiceNewInstallmentValue.text, numberOfMontlyPayments),
                for: .normal
            )
        } else {
            installments.setTitle(
                String(format: CreditPicPayLocalizable.invoiceInstallmentValue.text, numberOfMontlyPayments),
                for: .normal
            )
        }
    }
    
    func addComponents() {
        addSubview(imageView)
        addSubview(infoStackView)
        addSubview(installments)
        
        infoStackView.addArrangedSubview(labelStackView)
        labelStackView.addArrangedSubview(topLabel)
        labelStackView.addArrangedSubview(bottomLabel)
    }
    
    func layoutComponents() {
        imageView.layout {
            $0.width == Layout.Sizes.imageWidth
            $0.height == Layout.Sizes.imageHeight
            $0.top == topAnchor + Layout.Sizes.topSize
            $0.leading == leadingAnchor + Spacing.base02
        }
        
        installments.layout {
            $0.top == imageView.topAnchor
            $0.trailing == trailingAnchor - Spacing.base02
            $0.width == Layout.Sizes.installmentButtonWidth
            $0.height == Layout.Sizes.installmentButtonHeight
        }
        
        infoStackView.layout {
            $0.top == imageView.topAnchor
            $0.leading == imageView.trailingAnchor + Spacing.base02
            $0.trailing == installments.leadingAnchor + Spacing.base01
        }
        
        if FeatureManager.isActive(.featureInstallmentNewButton) {
            installments.isHidden = true
            infoStackView.addArrangedSubview(newInstallmentButton)
            infoStackView.layout {
                $0.bottom == bottomAnchor - Spacing.base02
            }
        } else {
            imageView.layout {
                $0.bottom == bottomAnchor - Spacing.base02
            }
        }
    }
}
