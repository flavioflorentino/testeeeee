import UI
import Card
import Foundation
import FeatureFlag
import UIKit

final class CreditPicPayCoordinator: Coordinator {
    fileprivate var auth: PPAuth?
    typealias Dependencies = HasFeatureManager

    private var childCoordinators: [Coordinating] = []
    var currentCoordinator: Coordinator?
    private let dependencies: Dependencies = DependencyContainer()
    var navigationController: UINavigationController
    private var account: CPAccount?

    init(navigationController: UINavigationController = StyledNavigationController(), account: CPAccount? = nil) {
        navigationController.setNavigationBarHidden(false, animated: false)
        self.navigationController = navigationController
        self.account = account
    }

    func start() {
        startHomeCoordinator()
    }

    func startInListInvoices() {
        startInvoiceListCoordinator()
    }

    func startInInvoiceDetailWithList(with id: String) {
        startCreditInvoiceDetailWithList(for: id, isPresent: true)
    }

    func startInInvoiceDetail(with id: String) {
        startCreditInvoiceDetail(for: id, isPresent: false)
    }

    func startPayCoordinator(with invoiceId: String) {
        guard let account = self.account else {
            fatalError("the is not instantiated account object")
        }
        let invoicePayCoordinator = CreditPicPayInvoicePayCoordinator(with: navigationController, invoiceId: invoiceId, account: account)
        currentCoordinator = invoicePayCoordinator
        invoicePayCoordinator.start()
    }

    // MARK: - Functions for similar delegates

    func showAlert(_ alert: Alert, completion: @escaping (AlertPopupViewController, Button, UIPPButton) -> Void) {
        AlertMessage.showAlert(alert, controller: navigationController, action: completion)
    }

    func showAlertError(_ error: Error, completion: (() -> Void)?) {
        AlertMessage.showCustomAlertWithError(error, controller: navigationController, completion: completion)
    }

    func closeIfPresentedController() {
        navigationController.dismiss(animated: true, completion: nil)
    }
}

// MARK: Home coordinator delegate
extension CreditPicPayCoordinator: CreditPicPayHomeCoordinatorDelegate {
    private func startHomeCoordinator() {
        if dependencies.featureManager.isActive(.cardRedesignHome) {
            let homeCoordinator = CardHomeFlowCoordinator(navigationController: navigationController)
            homeCoordinator.start()
        } else {
            let homeCoordinator = CreditPicPayHomeCoordinator(with: navigationController,
                account: account)
            currentCoordinator = homeCoordinator
            homeCoordinator.delegate = self
            homeCoordinator.start()
        }
    }
    
    func startHomeCoordinatorByPresent() {
        let homeCoordinator = CreditPicPayHomeCoordinator(with: navigationController,
            account: account)
        currentCoordinator = homeCoordinator
        childCoordinators.append(homeCoordinator)
        homeCoordinator.delegate = self
        homeCoordinator.startByPresent()
    }

    func showInvoiceList(for account: CPAccount?) {
        self.account = account
        startInvoiceListCoordinator()
    }

    func showInvoiceDetailFromHome(with id: String?, and account: CPAccount?) {
        self.account = account
        guard let invoiceId = id else {
            return
        }
        startCreditInvoiceDetail(for: invoiceId)
    }

    func showConfigureCard() {
        let coordinator = CardSettingsFlowCoordinator(navigationController: navigationController)
        childCoordinators.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }

    func payInvoice(with invoiceId: String?, and account: CPAccount?) {
        guard let invoiceId = invoiceId else {
            fatalError("the is not instantiated invoice id")
        }
        startPayCoordinator(with: invoiceId)
    }

    func showVirtualCard(button: VirtualCardButton) {
        let input = VirtualCardFlowInput(button: button)

        let coordinator = VirtualCardFlowCoordinator(navigation: navigationController, input: input)
        childCoordinators.append(coordinator)
        coordinator.start()
    }
}

extension CreditPicPayCoordinator: CardSettingsFlowDelegate {
    func didOpenDueDayCard(from navigationController: UINavigationController) {
        let dueDayController = DueDayFactory.make(delegate: self)
        navigationController.pushViewController(dueDayController, animated: true)
    }

    func didOpenLimitCard(from navigationController: UINavigationController, didUpdateCardLimit: @escaping () -> Void) {
        guard dependencies.featureManager.isActive(.experimentCardCreditLimitPresentingBool) else {
            let coordinator = CreditLimitCoordinator(from: navigationController)
            coordinator.didUpdateCardLimit = didUpdateCardLimit
            currentCoordinator = coordinator
            coordinator.start()
            return
        }

        let coordinator = LimitAdjustmentFlowCoordinator(navigationController: navigationController)
        coordinator.start()
    }

    func didAskAuthentication(onSuccess: @escaping (String) -> Void) {
        auth = PPAuth.authenticate({ token, _ in
            guard let password = token else {
                return
            }
            onSuccess(password)
        }, canceledByUserBlock: { })
    }
}

// MARK: - Invoice list coordinator delegate
extension CreditPicPayCoordinator: CreditInvoiceListCoordinatorDelegate {
    private func startInvoiceListCoordinator() {
        guard let account = account else {
            return
        }
        let invoiceListCoordinator = CreditInvoiceListCoordinator(
            with: navigationController,
            account: account)
        invoiceListCoordinator.delegate = self
        currentCoordinator = invoiceListCoordinator
        invoiceListCoordinator.start()
    }

    func didSelectedInvoice(_ invoice: CreditInvoiceDetail) {
        startCreditInvoiceDetail(with: invoice)
    }

    func closeInvoiceList() {
        navigationController.popViewController(animated: true)
    }
}

// MARK: - Invoice detail coordinator delegate
extension CreditPicPayCoordinator: CreditInvoiceDetailCoordinatorDelegate {
    private func startCreditInvoiceDetail(with invoiceDetail: CreditInvoiceDetail) {
        guard let account = account else {
            return
        }
        let invoiceDetailCoordinator = CreditInvoiceDetailCoordinator(with: navigationController,
            account: account,
            invoiceDetail: invoiceDetail,
            isPresentView: false)
        currentCoordinator = invoiceDetailCoordinator
        invoiceDetailCoordinator.delegate = self
        invoiceDetailCoordinator.start()
    }

    func startCreditInvoiceDetailWithList(for id: String?, isPresent: Bool = false) {
        guard let invoiceId = id else {
            fatalError("the is not instantiated invoice id")
        }
        startInvoiceListCoordinator()
        startCreditInvoiceDetail(for: invoiceId, isPresent: isPresent)

    }

    func startCreditInvoiceDetail(for id: String, isPresent: Bool = false) {
        let invoiceDetailCoordinator = CreditInvoiceDetailCoordinator(with: navigationController,
            invoiceId: id,
            isPresentView: isPresent)
        currentCoordinator = invoiceDetailCoordinator
        invoiceDetailCoordinator.delegate = self
        invoiceDetailCoordinator.start()
    }

    func pay(with invoiceId: String?, and account: CPAccount?) {
        self.account = account
        guard let invoiceId = invoiceId else {
            fatalError("the is not instantiated invoice id")
        }
        startPayCoordinator(with: invoiceId)
    }

    func popDetailViewController() {
        navigationController.popViewController(animated: true)
    }
}


extension CreditPicPayCoordinator {
    enum Origin {
        case home
        case list
        case detail
    }
}

extension CreditPicPayCoordinator: VirtualCardFlowCoordinatorDelegate {
}

extension CreditPicPayCoordinator: DueDayCoordinatingDelegate {
    func didConfirmDueDay(cardForm: CardForm?, bestPurchaseDay: Int?) {
        navigationController.popViewController(animated: true)
    }
}
