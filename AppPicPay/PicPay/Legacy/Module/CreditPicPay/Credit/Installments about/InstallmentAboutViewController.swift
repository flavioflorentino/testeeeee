import UIKit

final class InstallmentAboutViewController: PPBaseViewController {
    @IBOutlet weak var abotTextLabel: UILabel! {
        didSet {
            abotTextLabel.attributedText = viewModel.aboutAttributedText
        }
    }
    private let viewModel = InstallmentAboutViewModel()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}
