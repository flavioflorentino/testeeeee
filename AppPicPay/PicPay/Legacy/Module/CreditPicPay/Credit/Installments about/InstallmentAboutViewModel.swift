import Foundation
import UI

final class InstallmentAboutViewModel {
    var aboutAttributedText: NSAttributedString {
        let contentText = NSMutableAttributedString()
        let titleParagraphStyle = NSMutableParagraphStyle()
        titleParagraphStyle.lineSpacing = 0
        let title = NSMutableAttributedString(string: CreditPicPayLocalizable.generalInstallmentConditions.text, attributes: [.paragraphStyle: titleParagraphStyle,
                                                                                                       .foregroundColor: Palette.ppColorGrayscale500.color,
                                                                                                       .font: UIFont.systemFont(ofSize: 20, weight: .bold)])
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1
        var content = "• \(CreditPicPayLocalizable.contractingInstallment.text)"
        content += "• \(CreditPicPayLocalizable.contractedInstallmentsWillBeLaunched.text)"
        content += "• \(CreditPicPayLocalizable.interestRateContracted.text)"
        content += "• \(CreditPicPayLocalizable.paymentOccursInDifferentAmount.text)"
        content += "• \(CreditPicPayLocalizable.eventLatePayment.text)"
        content += "• \(CreditPicPayLocalizable.eventRepentance.text)"
        content += "• \(CreditPicPayLocalizable.generalConditions.text)"
        let text = NSMutableAttributedString(string: content, attributes: [.paragraphStyle: paragraphStyle,
                                                                           .foregroundColor: Palette.ppColorGrayscale400.color,
                                                                           .font: UIFont.systemFont(ofSize: 13, weight: .regular)])
        contentText.append(title)
        contentText.append(text)
        return contentText
    }
}
