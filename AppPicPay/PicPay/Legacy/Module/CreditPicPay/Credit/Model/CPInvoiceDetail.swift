import Foundation

struct CreditInvoiceDetail: Codable {
    let invoice: CreditInvoice?
    let transactions: [Transaction]
    
    init(with invoice: CreditInvoice) {
        self.invoice = invoice
        self.transactions = []
    }
}

extension CreditInvoiceDetail {
    struct Label: Codable, Equatable {
        let value: String
        let color: String
    }
    
    struct Transaction: Codable, Equatable {
        let date: Date?
        let dateStr: String
        let processed: Bool
        let label: Label
        let value: Label
    }
}
