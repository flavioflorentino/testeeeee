import SwiftyJSON

final class CPPaymentResponse: BaseApiResponse {
    let receipts: CPreceipt
    let transaction: CPTransaction
    let statusName: String
    
    required init?(json: JSON) {
        guard
            let receipts = CPreceipt(json: json["receipt"]),
            let transaction = CPTransaction(json: json["Transaction"]),
            let statusName = json["status_name"].string
            else {
                return nil
        }
        
        self.receipts = receipts
        self.transaction = transaction
        self.statusName = statusName
    }
    
    required init?(statusName: String, receipts: JSON, transaction: JSON) {
        guard
            let receipts = CPreceipt(json: receipts),
            let transaction = CPTransaction(json: transaction)
            else {
                return nil
        }
        
        self.receipts = receipts
        self.transaction = transaction
        self.statusName = statusName
    }
}

extension CPPaymentResponse {
    final class CPreceipt {
        let items: [ReceiptWidgetItem]
        
        required init?(json: JSON) {
            items = WSReceipt.createReceiptWidgetItem(json)
        }
    }
}

extension CPPaymentResponse {
    final class CPTransaction {
        let date: Date
        let credit: Double
        let id: String
        let parcel: Int
        let totalValue: Double
        let siteValue: Double
        let parcelValue: Double
        
        required init?(json: JSON) {
            guard let date = DateFormatter.brazillianFormatter(dateFormat: "yyyy-MM-dd HH:mm:ss").date(from: json["transaction_date"].string ?? ""),
                let credit = json["credit"].double,
                let id = json["id"].string,
                let parcel = json["quant_parcelas"].int,
                let totalValue = json["total_value"].double,
                let siteValue = json["site_value"].double,
                let parcelValue = json["valor_parcela"].double else {
     return nil
}
            
            self.date = date
            self.credit = credit
            self.id = id
            self.parcel = parcel
            self.totalValue = totalValue
            self.siteValue = siteValue
            self.parcelValue = parcelValue
        }
    }
}
