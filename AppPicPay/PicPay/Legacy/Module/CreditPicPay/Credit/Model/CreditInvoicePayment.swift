struct CreditInvoicePayment: Codable {
    let invoice: CreditInvoice
    private(set) var paymentValue: Double?
    let paymentLine: String
    let dueDay: String
    let minimumPayment: Double?
    let total: Double
    let installment: Installment?
    let billEnabled: Bool?
    let billReleaseDate: String?
    
    var dateDueDay: Date {
        guard let date = DateFormatter.iso8601noTime.date(from: dueDay) else {
            fatalError("CreditInvoicePayment.dueDay is not on the right format")
        }
        return date
    }
    
    mutating func updateValueToPay(_ value: Double) {
        paymentValue = value
    }
}

extension CreditInvoicePayment {
    struct Installment: Codable {
        let value: Double
        let type: String
        let paymentLimitDate: String
        let installmentOptions: [Option]
        let auto: Bool
        
        var datePaymentLimit: Date {
            return DateFormatter.iso8601noTime.date(from: paymentLimitDate) ?? Date()
        }
        
        var isValid: Bool {
            let date = datePaymentLimit
            let currentDateString = DateFormatter.iso8601noTime.string(from: Date())
            let currentDate = DateFormatter.iso8601noTime.date(from: currentDateString)
            if currentDate?.compare(date) == .orderedAscending || currentDate?.compare(date) == .orderedSame {
                return true
            }
            return false
        }
        
        struct Option: Codable, Equatable {
            let openValue: Double
            let monthInterestRate: Double
            let installmentCount: Int
            let annualInterestRate: Double
            let installmentsValue: Double
            let monthTotalEffectiveCost: Double
            let annualTotalEffectiveCost: Double
        }
    }
}
