import Foundation

struct CPFeedItem: Codable {
    let transactionId: String
    let firstLabel: Label
    let secondLabel: Label
    let thirdLabel: Label
    let dateLabel: Label
    let background: String
    let cancelled: Bool
    let imageUrl: String?
}

extension CPFeedItem: Equatable {
    static func == (lhs: CPFeedItem, rhs: CPFeedItem) -> Bool {
        return lhs.transactionId == rhs.transactionId
    }
}

extension CPFeedItem {
    struct Label: Codable {
        let value: String
        let color: String
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.value = try container.decodeIfPresent(String.self, forKey: .value) ?? ""
            self.color = try container.decodeIfPresent(String.self, forKey: .color) ?? ""
        }
    }
}
