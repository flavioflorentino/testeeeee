import Card
import UIKit
import SwiftyJSON

final class CPAccount: NSObject, BaseApiResponse {
    var selectedCredit: Double = 0 /// Selected per user
    var availableCredit: Double = 0 /// Released by bank (always greater than or equal to selected)
    var availableLimit: Double = 0 /// Available for purchase
    var offerText: String = ""
    var dueDay: Int = 10
    var currentInvoice: CreditInvoice?
    var alert: Alert?
    
    let creditStatus: CPAccount.CreditStatusType
    let registrationStatus: CPAccount.RegistrationStatusType
    let registrationErrors: [String]?
    let offerEnabled: Bool
    
    let settings: CardSettings
    
    required init?(json: JSON) {
        guard let registrationStatusString = json["registration_status"].string, let registrationStatus = CPAccount.RegistrationStatusType(rawValue: registrationStatusString) else { return  nil }
        if let creditStatusString = json["credit_status"].string, let creditStatus = CPAccount.CreditStatusType(rawValue: creditStatusString) {
            self.creditStatus = creditStatus
        } else {
            self.creditStatus = .onRegistration
        }
        
        self.registrationStatus = registrationStatus
        
        if let selectedCredit = json["selected_limit"].double {
            self.selectedCredit = selectedCredit
        }
        if let availableCredit = json["available_credit"].double {
            self.availableCredit = availableCredit
        }
        if let availableLimit = json["available_limit"].double {
            self.availableLimit = availableLimit
        }

        if let dueDay = json["due_day"].int {
            self.dueDay = dueDay
        }
        if json["current_invoice"].exists(), let data = try? json["current_invoice"].rawData() {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            decoder.dateDecodingStrategy = .customISO8601
            if let invoice = try? decoder.decode(CreditInvoice.self, from: data) {
                self.currentInvoice = invoice
            }
        }
        if json["invoice_alert"].exists() {
            self.alert = Alert(json: json["invoice_alert"])
        }
        registrationErrors = json["registration_errors"].arrayObject as? [String]
        offerText = json["offer_text"].string ?? ""
        offerEnabled = json["offer_enabled"].bool ?? false
        
        let decoder = JSONDecoder()
        guard
            json["settings"].exists(),
            let data = try? json["settings"].rawData(),
            let settings = try? decoder.decode(CardSettings.self, from: data) else {
                self.settings = CardSettings()
                return
        }
        
        self.settings = settings
    }
}

extension CPAccount {
    enum CreditStatusType: String {
        case onRegistration = "ON_REGISTRATION"
        case active = "ACTIVE"
        case blocked = "BLOCKED"
    }

    enum PhysicalStatusType: String {
        case notExists = "NOT_EXISTS"
        case activated = "ACTIVATED"
        case waitingActivation = "WAITING_ACTIVATION"
        case blocked = "BLOCKED"
        case notRequested = "NOT_REQUESTED"
        case debitWaitingActivation = "DEBIT_WAITING_ACTIVATION"
        
        func active() -> Bool {
            return self != .notExists &&
                   self != .notRequested &&
                   self != .waitingActivation
        }
    }
    
    enum RegistrationStatusType: String {
        case empty = "EMPTY"
        case inProgress = "IN_PROGRESS"
        case underAnalysis = "UNDER_ANALYSIS"
        case reviewRequested = "REVIEW_REQUESTED"
        case completed = "COMPLETED"
        case approved = "APPROVED"
        case notExist = "NOT_EXISTS"
        case denied = "DENIED"
        case creditWaitingActivation = "CREDIT_WAITING_ACTIVATION"
        case creditActivated = "CREDIT_ACTIVATED"
        case waitingExternalReview = "WAITING_EXTERNAL_REVIEW"
    }
}

extension CPAccount {
    struct Alert {
        var title: String = ""
        var text: String = ""
        var buttonLabel: String = ""
        var dismissLabel: String = ""
        var action: AlertAction
        var invoice: CreditInvoice?
        
        init(json: JSON) {
            self.title = json["title"].string ?? ""
            self.text = json["text"].string ?? ""
            self.buttonLabel = json["button_label"].string ?? ""
            self.dismissLabel = json["dismiss_label"].string ?? ""
            
            if json["current_invoice"].exists(), let data = try? json["current_invoice"].rawData() {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                decoder.dateDecodingStrategy = .customISO8601
                if let invoice = try? decoder.decode(CreditInvoice.self, from: data) {
                    self.invoice = invoice
                }
            }
            
            if let actionString = json["action"].string, let actionType = Alert.AlertAction(rawValue: actionString) {
                self.action = actionType
            } else {
                self.action = .close
            }
        }
        
        public enum AlertAction: String, Codable {
            case close = "CLOSE"
            case pay = "PAY"
            
            public init(from decoder: Decoder) throws {
                self = try AlertAction(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .close
            }
        }
    }
}
