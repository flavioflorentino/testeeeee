struct CreditInvoice: Codable {
    let id: String
    let totalValue: Double
    let paidValue: Double?
    let closingDate: Date
    let dueDate: Date
    let paymentDate: Date?
    let minimumPayment: Double?
    let status: Status
    let payable: Bool
    let statusText: String
    let earlyPayment: Double?
    let receivedPayment: Double?
    let totalDebit: Double?
}

extension CreditInvoice {
    enum Status: String, Codable {
        case delayed = "DELAYED"
        case open = "OPEN"
        case closed = "CLOSED"
        case openPayment = "OPEN_PAYMENT"
    }
}
