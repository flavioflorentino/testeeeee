import UIKit

final class CreditPicPayInvoiceListViewModel {
    private let creditApi: CreditPicpayApi = CreditPicpayApi()
    private var isListLoading = false
    private var list: [CreditInvoice] = []
    var isEmpty: Bool {
        return list.isEmpty
    }
    var account: CPAccount
    private var coordinatorDelegate: CreditInvoiceListCoordinatorDelegate?
    
    init(with account: CPAccount, delegate: CreditInvoiceListCoordinatorDelegate?) {
        self.account = account
        self.coordinatorDelegate = delegate
    }
    
    func loadInvoices(_ completion: @escaping (PicPayErrorDisplayable?) -> Void) {
        isListLoading = true
        list = []
        creditApi.invoices { [weak self] (result) in
            guard let strongSelf = self else {
            return
        }
            strongSelf.isListLoading = false
            switch result{
            case .success(let response):
                strongSelf.list = response
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    func numberOfSections() -> Int {
        return list.isEmpty ? 0 : 1
    }

    func numberOfRows(in section: Int) -> Int {
        return isListLoading ? 3 : list.count
    }
    
    func didTapInvoiceItem(in indexPath: IndexPath) {
        trackEventName(CreditPicPayInvoiceListViewModel.EventName.invoiceTap)
        guard let invoice = getInvoice(in: indexPath) else {
            return
        }
        coordinatorDelegate?.didSelectedInvoice(CreditInvoiceDetail(with: invoice))
    }
    
    func getInvoice(in indexPath: IndexPath) -> CreditInvoice? {
        guard list.indices.contains(indexPath.row) else {
     return nil
}
        return list[indexPath.row]
    }
    
    func trackScreenViewEvent() {
        trackEventName(CreditPicPayInvoiceListViewModel.EventName.screen)
    }
    
    private func trackEventName(_ eventName: EventName) {
        PPAnalytics.trackEvent(eventName.rawValue, properties: nil)
    }
}

extension CreditPicPayInvoiceListViewModel {
    fileprivate enum EventName: String {
        case screen = "Page view - Credito - listar faturas"
        case invoiceTap = "Credito - tap fatura"
    }
}
