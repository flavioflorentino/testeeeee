//
//  CPInvoiceTableViewCell.swift
//  PicPay
//
//  ☝🏽☁️ Created by Luiz Henrique Guimarães on 26/03/18.
//

import CoreLegacy
import UI
import UIKit
import SkeletonView

final class CPInvoiceTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        showAnimationSkelecton()
    }
}

extension CPInvoiceTableViewCell {
    func configure(_ invoice: CreditInvoice) {
        hideAnimationSkelecton()
        selectionStyle = .none
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "pt_BR")
        formatter.dateFormat = "MMMM"

        let date = invoice.dueDate
        titleLabel.text = formatter.string(from: date).capitalizingFirstLetter()
        
        let value: Double = invoice.totalValue > 0 ? invoice.totalValue : 0.0
        valueLabel.text = CurrencyFormatter.brazillianRealString(from: NSNumber(value: value))
        
        statusLabel.text = invoice.statusText
        statusLabel.isHidden = false
        switch invoice.status {
        case .closed:
            changeLabelsColor(color: Palette.ppColorGrayscale500.color)
        case .openPayment:
            changeLabelsColor(color: Palette.ppColorBranding200.color)
        case .delayed:
            changeLabelsColor(color: Palette.ppColorNegative500.color)
        case .open:
            changeLabelsColor(color: Palette.ppColorNeutral200.color)
            statusLabel.isHidden = true
            titleLabel.text = invoice.statusText
        }
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    private func changeLabelsColor(color: UIColor) {
        titleLabel.textColor = color
        valueLabel.textColor = color
        statusLabel.textColor = color
    }
    
    private func showAnimationSkelecton() {
        titleLabel.showAnimatedGradientSkeleton()
        valueLabel.showAnimatedGradientSkeleton()
        statusLabel.showAnimatedGradientSkeleton()
    }
    
    private func hideAnimationSkelecton() {
        titleLabel.hideSkeleton()
        valueLabel.hideSkeleton()
        statusLabel.hideSkeleton()
    }
}
