import UIKit

protocol CreditInvoiceListCoordinatorDelegate: AnyObject {
    func didSelectedInvoice(_ invoice: CreditInvoiceDetail)
    func closeInvoiceList()
    func closeIfPresentedController()
}

final class CreditInvoiceListCoordinator: Coordinator {
    var currentCoordinator: Coordinator?
    private let navigationController: UINavigationController
    private let account: CPAccount
    weak var delegate: CreditInvoiceListCoordinatorDelegate?
    
    init(with navigationController: UINavigationController,
         account: CPAccount) {
        self.navigationController = navigationController
        self.account = account
    }
    
    func start() {
        let viewModel = CreditPicPayInvoiceListViewModel(with: account,
                                               delegate: delegate)
        let invoiceListController = CreditPicPayInvoiceListViewController(with: viewModel)
        navigationController.pushViewController(invoiceListController, animated: true)
    }
}
