import UI
import UIKit
import SkeletonView

protocol CreditPicPayInvoiceListViewControllerDelegate: AnyObject {
    func setCreditHomeNavigationBarStatusColor()
}

final class CreditPicPayInvoiceListViewController: PPBaseViewController {
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 40
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .singleLine
        tableView.tableFooterView = UIView()
        tableView.registerCell(type: CPInvoiceTableViewCell.self)
        tableView.refreshControl = pullRefreshControl
        tableView.backgroundColor = Palette.ppColorGrayscale000.color
        return tableView
    }()
    private lazy var pullRefreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(reload), for: .valueChanged)
        return refreshControl
    }()
    
    private let viewModel: CreditPicPayInvoiceListViewModel

    init(with viewModel: CreditPicPayInvoiceListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        title = CreditPicPayLocalizable.invoices.text
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.trackScreenViewEvent()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
        layoutComponents()
        loadInvoies()
    }
    
    private func addComponents() {
        view.addSubview(tableView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.constraintAllEdges(from: tableView, to: view)
    }
}

// MARK: - Table view data source
extension CreditPicPayInvoiceListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(type: CPInvoiceTableViewCell.self, forIndexPath: indexPath)
        if let invoice = viewModel.getInvoice(in: indexPath) {
            cell.configure(invoice)
        }
        return cell
    }
}

// MARK: - Table view delegate
extension CreditPicPayInvoiceListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.didTapInvoiceItem(in: indexPath)
    }
}

extension CreditPicPayInvoiceListViewController {
    private func loadInvoies() {
        startLoadingView()
        viewModel.loadInvoices { [weak self] (error) in
            self?.stopLoadingView()
            self?.tableView.tableFooterView = UIView(frame: CGRect.zero)
            if let error = error {
                self?.showListError(with: error)
            } else {
                self?.showOnboardingIfEmpty()
            }
            self?.pullRefreshControl.endRefreshing()
            self?.tableView.reloadData()
        }
    }
    
    private func showListError(with error: Error) {
        let connectionError = ConnectionErrorView(with: error, type: .error, frame: tableView.frame)
        connectionError.tryAgainTapped = { [weak self] in
            self?.reload()
        }
        tableView.tableFooterView = connectionError
    }
    
    private func showOnboardingIfEmpty() {
        if viewModel.isEmpty {
            let emptyList = BasicEmptyListView(frame: view.frame)
            emptyList.titleLabel.text = CreditPicPayLocalizable.noInvoice.text
            emptyList.textLabel.text =  CreditPicPayLocalizable.yourInvoicesWillBeListedHere.text
            emptyList.imageView.image = #imageLiteral(resourceName: "ilu_picpaycredit_invoice")
            tableView.tableFooterView = emptyList
        } else {
            tableView.tableFooterView = UIView()
        }
    }

    @objc
    private func reload() {
        loadInvoies()
    }
}

extension CreditPicPayInvoiceListViewController: StyledNavigationDisplayable {
    var navigationBarStyle: DefaultNavigationStyle {
        StandardNavigationStyle()
    }
}
