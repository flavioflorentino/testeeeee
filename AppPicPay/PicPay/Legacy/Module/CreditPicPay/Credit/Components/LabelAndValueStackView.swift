import UI
import UIKit

final class LabelAndValueStackView: UIStackView {
    private(set) lazy var left: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .left
        return label
    }()
    
    private(set) lazy var right: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .right
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutMargins = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 4)
        isLayoutMarginsRelativeArrangement = true
        distribution = .fill
        spacing = 8.0
        axis = .horizontal
        addArrangedSubview(left)
        addArrangedSubview(right)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
