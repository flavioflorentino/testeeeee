import UI
import UIKit

final class ValueCurrencyInformationView: UIStackView {
    lazy var primaryLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        return label
    }()
    lazy var secundaryLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        return label
    }()
    private lazy var valueStackView: ValueCurrencyStackView = {
        let valueStackView = ValueCurrencyStackView()
        return valueStackView
    }()
    
    var isEnableValueText: Bool = true {
        didSet {
            valueStackView.valueTextField.isEnabled = isEnableValueText
        }
    }
    
    init() {
        super.init(frame: .zero)
        axis = .vertical
        spacing = 4
        alignment = .center
        addComponents()
    }
    
    private func addComponents() {
        addArrangedSubview(primaryLabel)
        addArrangedSubview(valueStackView)
        addArrangedSubview(secundaryLabel)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func changeValue(_ value: Double) {
        valueStackView.value = value
    }
}

