import CoreLegacy
import UI
import UIKit

final class ValueCurrencyStackView: UIStackView {
    private lazy var currencyLabel: UILabel = {
        let label = UILabel()
        
        let labelFont = UIFont.systemFont(ofSize: 22, weight: .medium)
        let offset = (label.font.ascender - label.font.capHeight) - 9
        let attributes: [NSAttributedString.Key: Any] = [.font: labelFont, .baselineOffset: offset]
        
        label.font = labelFont
        label.attributedText = NSAttributedString(string: "R$", attributes: attributes)
            
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .right
        return label
    }()
    lazy var valueTextField: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.systemFont(ofSize: 52, weight: .light)
        textField.textColor = Palette.ppColorGrayscale500.color
        textField.textAlignment = .right
        textField.minimumFontSize = 14.0
        textField.keyboardType = .numberPad
        textField.addTarget(self, action: #selector(textDidChange(_:)), for: .editingChanged)
        textField.addTarget(self, action: #selector(becomeFirstResponseTextField(_:)), for: UIControl.Event.editingDidBegin)
        return textField
    }()
    private lazy var mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .top
        stackView.distribution = .fill
        stackView.spacing = 0
        return stackView
    }()
    
    var componentsWidth: CGFloat {
        return currencyLabel.bounds.width + valueTextField.bounds.width
    }
    
    var value: Double = 0.00 {
        didSet {
            let text = CurrencyFormatter.currencyInputBrazilianStringFromString(with: value.stringAmount)
            valueTextField.text = text
        }
    }
    var onTextChange: (Double) -> Void = { _ in }
    
    init() {
        super.init(frame: .zero)
        valueTextField.placeholder = "0,00"
        setupStackView()
        addComponents()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupStackView() {
        axis = .vertical
        alignment = .center
        currencyLabel.textColor = Palette.ppColorGrayscale300.color
    }
    
    private func addComponents() {
        mainStackView.addArrangedSubview(currencyLabel)
        mainStackView.addArrangedSubview(valueTextField)
        addArrangedSubview(mainStackView)
    }
    
    private func setupColorsForEmptyValue() {
        valueTextField.textColor = Palette.ppColorGrayscale300.color
        currencyLabel.textColor = Palette.ppColorGrayscale300.color
    }
    
    func setupColorsForPayValue() {
        valueTextField.textColor = Palette.ppColorBranding300.color
        currencyLabel.textColor = Palette.ppColorBranding300.color
    }
    
    @objc
    func textDidChange(_ textField: UITextField) {
        guard let text = textField.text, !text.isEmpty else {
            setupColorsForEmptyValue()
            return
        }
        let formattedString = CurrencyFormatter.currencyInputBrazilianStringFromString(with: text)
        if formattedString.isEmpty {
            setupColorsForEmptyValue()
        }
        valueTextField.text = formattedString
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.locale = Locale(identifier: "pt-br")
        
        guard let value = formatter.number(from: formattedString) else {
            onTextChange(0.0)
            return
        }
    
        onTextChange(value.doubleValue)
        setupColorsForPayValue()
        
        guard value == 0 else {
            return
        }
        setupColorsForEmptyValue()
    }

    @objc
    private func becomeFirstResponseTextField(_ textField: UITextField) {
        guard let text = textField.text, text.isEmpty else {
            return
        }
        setupColorsForEmptyValue()
    }
}
