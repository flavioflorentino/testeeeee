import UIKit

enum CreditPaymentLoadingAction {
    case openNewPayment(model: InvoiceCardPaymentModel, disclaimer: InvoiceCardDisclaimerInfo)
    case close
}

protocol CreditPaymentLoadingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CreditPaymentLoadingAction)
}

final class CreditPaymentLoadingCoordinator: CreditPaymentLoadingCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: CreditPaymentLoadingAction) {
        switch action {
        case let .openNewPayment(model, disclaimer):
            let orchestrator = InvoiceCardPaymentOrchestrator(model: model, disclaimer: disclaimer)
            viewController?.navigationController?.pushViewController(orchestrator.paymentViewController, animated: true)
            
        case .close:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}
