import UIKit

protocol CreditPaymentLoadingDisplay: AnyObject {
    func startLoad()
    func stopLoad()
    func didReceiveAnError(_ error: PicPayError)
}
