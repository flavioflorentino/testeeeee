import Core
import Foundation

protocol CreditPaymentLoadingServicing {
    typealias CompletionDisclaimer = (Result<InvoiceCardDisclaimerInfo, PicPayError>) -> Void
    func requestDisclaimer(invoiceId: String, paymentValue: Double, completion: @escaping CompletionDisclaimer)
}

final class CreditPaymentLoadingService: CreditPaymentLoadingServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func requestDisclaimer(invoiceId: String, paymentValue: Double, completion: @escaping CompletionDisclaimer) {
        let api = Api<InvoiceCardDisclaimerInfo>(
            endpoint: CreditEndpoint.disclaimer(invoiceId: invoiceId, paymentValue: paymentValue)
        )
        api.execute { [weak self] result in
            let mappedResult = result
                .mapError(\.picpayError)
                .map(\.model)
            
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
}
