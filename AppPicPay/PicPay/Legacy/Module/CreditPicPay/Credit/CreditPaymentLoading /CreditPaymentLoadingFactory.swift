import Foundation

final class CreditPaymentLoadingFactory {
    static func make(model: InvoiceCardPaymentModel) -> CreditPaymentLoadingViewController {
        let container = DependencyContainer()
        let service: CreditPaymentLoadingServicing = CreditPaymentLoadingService(dependencies: container)
        let coordinator: CreditPaymentLoadingCoordinating = CreditPaymentLoadingCoordinator()
        let presenter: CreditPaymentLoadingPresenting = CreditPaymentLoadingPresenter(coordinator: coordinator)
        let viewModel = CreditPaymentLoadingViewModel(model: model, service: service, presenter: presenter)
        let viewController = CreditPaymentLoadingViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
