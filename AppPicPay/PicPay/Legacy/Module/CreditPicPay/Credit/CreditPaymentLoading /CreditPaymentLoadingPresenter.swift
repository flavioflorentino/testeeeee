import Core
import Foundation

protocol CreditPaymentLoadingPresenting: AnyObject {
    var viewController: CreditPaymentLoadingDisplay? { get set }
    func startLoadDisclaimer()
    func errorLoadDisclaimer(_ error: PicPayError)
    func didNextStep(action: CreditPaymentLoadingAction)
}

final class CreditPaymentLoadingPresenter: CreditPaymentLoadingPresenting {
    private let coordinator: CreditPaymentLoadingCoordinating
    weak var viewController: CreditPaymentLoadingDisplay?

    init(coordinator: CreditPaymentLoadingCoordinating) {
        self.coordinator = coordinator
    }
    
    func startLoadDisclaimer() {
        viewController?.startLoad()
    }
    
    func errorLoadDisclaimer(_ error: PicPayError) {
        viewController?.stopLoad()
        viewController?.didReceiveAnError(error)
    }
    
    func didNextStep(action: CreditPaymentLoadingAction) {
        viewController?.stopLoad()
        coordinator.perform(action: action)
    }
}
