import Foundation

protocol CreditPaymentLoadingViewModelInputs: AnyObject {
    func updateView()
    func showError()
}

final class CreditPaymentLoadingViewModel {
    private let service: CreditPaymentLoadingServicing
    private let presenter: CreditPaymentLoadingPresenting
    private let model: InvoiceCardPaymentModel
    
    init(model: InvoiceCardPaymentModel, service: CreditPaymentLoadingServicing, presenter: CreditPaymentLoadingPresenting) {
        self.model = model
        self.service = service
        self.presenter = presenter
    }
}

extension CreditPaymentLoadingViewModel: CreditPaymentLoadingViewModelInputs {
    func updateView() {
        presenter.startLoadDisclaimer()
        service.requestDisclaimer(invoiceId: model.payeeId, paymentValue: model.value) { [weak self] result in
            switch result {
            case .success(let disclaimer):
                self?.successRequestDisclaimer(disclaimer: disclaimer)
                
            case .failure(let error):
                self?.presenter.errorLoadDisclaimer(error)
            }
        }
    }
    
    func showError() {
        presenter.didNextStep(action: .close)
    }
    
    private func successRequestDisclaimer(disclaimer: InvoiceCardDisclaimerInfo) {
        presenter.didNextStep(action: .openNewPayment(model: model, disclaimer: disclaimer))
    }
}
