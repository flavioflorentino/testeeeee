import UI
import UIKit
import SnapKit

final class CreditPaymentLoadingViewController: ViewController<CreditPaymentLoadingViewModelInputs, UIView> {
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.hidesWhenStopped = true
        
        return activityIndicator
    }()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.updateView()
    }
     
    public override func configureViews() {
        title = CreditPicPayLocalizable.paymentTitle.text
        view.backgroundColor = .backgroundPrimary()
    }
    
    public override func buildViewHierarchy() {
        view.addSubview(activityIndicator)
    }
    
    public override func setupConstraints() {
        activityIndicator.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
        }
    }
}

// MARK: View Model Outputs
extension CreditPaymentLoadingViewController: CreditPaymentLoadingDisplay {
    func startLoad() {
        activityIndicator.startAnimating()
    }
    
    func stopLoad() {
        activityIndicator.stopAnimating()
    }
    
    func didReceiveAnError(_ error: PicPayError) {
        AlertMessage.showCustomAlertWithErrorUI(error, controller: self) {
            self.viewModel.showError()
        }
    }
}
