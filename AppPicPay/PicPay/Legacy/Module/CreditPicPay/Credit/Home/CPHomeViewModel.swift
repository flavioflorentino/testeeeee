import AnalyticsModule
import FeatureFlag
import UI
import UIKit

final class CPHomeViewModel {
    private let creditApi = CreditPicpayApi()
    private var timelineList: [CPFeedItem] = []
    private var page: Int = 0
    private var size: Int = 20
    
    private weak var coordinatorDelegate: CreditPicPayHomeCoordinatorDelegate?

    typealias Dependencies = HasAnalytics & HasFeatureManager
    public var dependencies: Dependencies
    
    var isPaginateError: Bool = false
    var noNextPage: Bool = false
    var isViewWillApper: Bool = true
    var lastIndexPathBeforeFetchTimeLine: IndexPath?
    var account: CPAccount?
    var backgroundColorOfLastItem: UIColor {
        guard let lastItem = timelineList.last else {
            return Palette.ppColorGrayscale000.color
        }
        return Palette.hexColor(with: lastItem.background) ?? Palette.ppColorGrayscale000.color
    }
    var isFirstTimelineLoad: Bool = true
    var heightForFooterView: CGFloat {
        let minimumHeight: CGFloat = 44.0
        let maximumHeight: CGFloat = 280.0
        let minimumNumberOfRows: Int = 10
        return numberOfRows() >= minimumNumberOfRows ? minimumHeight : maximumHeight
    }
    var backgroundTableViewColor: UIColor {
        if let status = self.account?.creditStatus, status == .blocked {
            return Palette.ppColorGrayscalePlus.gradientColor.to
        } else {
            return Palette.ppColorBrandingPlus400.gradientColor.to
        }
    }
    var headerColor: UIColor {
        if let status = self.account?.creditStatus, status == .blocked {
            return Palette.ppColorGrayscalePlus.gradientColor.from
        } else {
            return Palette.ppColorBrandingPlus400.gradientColor.from
        }
    }
    var isRefreshData: Bool = false
    var updateBeginsRows: Bool = true
    var isLoadedTimeline: Bool = true {
        willSet {
            guard isLoadedAccount && newValue else {
                return
            }
            onCompletionLoads()
        }
    }
    var isLoadedAccount: Bool = true {
        willSet {
            guard isLoadedTimeline && newValue else {
                return
            }
            showAccountAlertIfExists()
            onCompletionLoads()
        }
    }
    
    var isPhysicalCardAvailable: Bool {
        return account?.settings.physicalCardConfigurationEnabled ?? false
    }
    
    var onCompletionLoads: () -> Void = {}
    
    var isVirtualCardAvailable: Bool {
        if dependencies.featureManager.isActive(.opsVirtualCard),
            let button = account?.settings.virtualCardButton, button.action != .none {
            return true
        } else {
            return false
        }
    }
    
    init(with account: CPAccount? = nil, delegate: CreditPicPayHomeCoordinatorDelegate, dependencies: Dependencies) {
        self.account = account
        self.coordinatorDelegate = delegate
        self.dependencies = dependencies
    }
    
    func showInvoices() {
        dependencies.analytics.log(CardEvent.didTapCardHomeMenu(action: .invoice))
        coordinatorDelegate?.showInvoiceList(for: account)
    }
    
    func showConfigureCard() {
        dependencies.analytics.log(CardEvent.didTapCardHomeMenu(action: .settings))
        coordinatorDelegate?.showConfigureCard()
    }

    func showVirtualCard() {
        if isVirtualCardAvailable {
            dependencies.analytics.log(CardEvent.didTapCardHomeMenu(action: .virtualCard))
            
            guard let virtualCardButton = account?.settings.virtualCardButton else {
                return
            }
            
            coordinatorDelegate?.showVirtualCard(button: virtualCardButton)
        }
    }
    
    func closeMenu() {
        dependencies.analytics.log(CardEvent.didTapCardHomeMenu(action: .cancel))
    }
    
    func showOptions() {
        dependencies.analytics.log(CardInvoiceEvent.homePicPayCard(action: .menu))
    }
    
    func prepareForLoadingTimeline() {
        isViewWillApper = true
        timelineList = []
        page = 0
    }
    
    func loadTimeline(_ completion: @escaping ([IndexPath], PicPayErrorDisplayable?) -> Void) {
        guard isLoadedTimeline && !noNextPage else {
            completion([], nil)
            return
        }
        isLoadedTimeline = false
        let page = isRefreshData ? 0 : self.page
        creditApi.timeline(page: page, size: size) { [weak self] result in
            self?.isLoadedTimeline = true
            switch result {
            case .success(let list):
                self?.isPaginateError = false
                self?.prepareIndexPathsForTableView(list: list, completion)
            case .failure(let error):
                guard let list = self?.timelineList else {
                    return
                }
                if !list.isEmpty {
                    self?.isPaginateError = true
                }
                completion([], error)
            }
        }
    }
    
    private func prepareIndexPathsForTableView(list: [CPFeedItem], _ completion: @escaping ([IndexPath], PicPayErrorDisplayable?) -> Void) {
        if let firstItemReponseList = list.first,
            let firstLocalItem = timelineList.first,
            firstLocalItem == firstItemReponseList {
            updateBeginsRows = false
        } else {
            updateBeginsRows = true
        }
        
        var totalElements: Int = (timelineList.isEmpty || isRefreshData) ? 0 : timelineList.count
        var indexPaths: [IndexPath] = []
        
        let rowFirstElement = (timelineList.isEmpty ? 0 : timelineList.count - 1)
        isFirstTimelineLoad = false
        lastIndexPathBeforeFetchTimeLine = IndexPath(row: rowFirstElement, section: 0)
        list.forEach { _ in
            indexPaths.append(IndexPath(row: totalElements, section: 0))
            totalElements += 1
        }
        
        /// check if list isnot empty and user not drag pull-refresh in table view
        if !list.isEmpty && !isRefreshData {
            page += 1
            timelineList += list
            noNextPage = list.count < size
        } else if list.isEmpty {
            noNextPage = true
        }
        isRefreshData = false
        completion(indexPaths, nil)
    }
    
    func loadAccount(onCacheLoaded: @escaping () -> Void, onError: @escaping (PicPayErrorDisplayable) -> Void) {
        isLoadedAccount = false
        creditApi.account(complete: true, onCacheLoaded: { [weak self] account in
            guard self?.account == nil else {
                return
            }
            self?.account = account
            onCacheLoaded()
        }, completion: { [weak self] result in
                switch result {
                case .success(let value):
                    self?.account = value
                case .failure(let error):
                    onError(error)
                }
                self?.isLoadedAccount = true
        })
    }
    func numberOfRows() -> Int {
        let rowsForSkeletonView: Int = 5
        return isViewWillApper ? rowsForSkeletonView : timelineList.count
    }
    
    func getTimeLineItem(in row: Int) -> CPFeedItem? {
        guard timelineList.indices.contains(row) else {
            return nil
        }
        return timelineList[row]
    }
    
    func didTapInFeedItem() {
        trackEvent(CPHomeViewModel.EventName.feedTap)
    }
    
    func trackScreenViewEvent() {
        trackEvent(CPHomeViewModel.EventName.screenName)
    }
    
    func showInvoiceDetail(with id: String) {
        coordinatorDelegate?.showInvoiceDetailFromHome(with: id, and: account)
    }
    
    func payInvoice() {
        let invoiceId = account?.currentInvoice?.id
        coordinatorDelegate?.payInvoice(with: invoiceId, and: account)
        dependencies.analytics.log(CardInvoiceEvent.homePicPayCard(action: .pay))
    }
    
    private func showAccountAlertIfExists() {
        guard let alertObj = account?.alert else {
            return
        }
        let alert = Alert(title: alertObj.title, text: alertObj.text)
        var buttons: [Button] = []
        
        if !alertObj.buttonLabel.isEmpty {
            let button = Button(title: alertObj.buttonLabel, type: .cta) { [weak self] alertPopupController, _ in
                if alertObj.action == .pay {
                    alertPopupController.dismiss(animated: true, completion: {
                        guard let invoice = alertObj.invoice, invoice.payable else {
                            return
                        }
                        self?.coordinatorDelegate?.showInvoiceDetailFromHome(with: invoice.id, and: self?.account)
                    })
                } else {
                    alertPopupController.dismiss(animated: true)
                }
            }
            buttons.append(button)
        }
        if !alertObj.dismissLabel.isEmpty {
            let button = Button(title: alertObj.dismissLabel, type: .clean, action: .close)
            buttons.append(button)
        }
        alert.buttons = buttons
        alert.showCloseButton = true
        
        coordinatorDelegate?.showAlert(alert, completion: {_, _, _ in
        })
    }
}

extension CPHomeViewModel {
    fileprivate enum EventName: String {
        case screenName = "Page view - Credito - home"
        case feedTap = "Credito - tap item feed"
    }
    
    private func trackEvent(_  eventName: EventName) {
        PPAnalytics.trackEvent(eventName.rawValue, properties: nil)
    }
}
