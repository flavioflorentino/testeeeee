import SkeletonView
import UI
import UIKit

final class CreditHomeFeedTableViewCell: UITableViewCell {
    var position: Position? {
        didSet {
            guard let position = self.position else {
            return
        }
            switch position {
            case .first:
                configureIfFirstCell()
            case .middle:
                lineTopView.isHidden = false
                lineBottomView.isHidden = false
            case .last:
                lineTopView.isHidden = false
                lineBottomView.isHidden = true
            }
        }
    }
    
    @IBOutlet weak var ilustrationView: UICircularImageView!
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.linesCornerRadius = 8
            dateLabel.lastLineFillPercent = 1
        }
    }
    @IBOutlet weak var firstLine: UILabel! {
        didSet {
            firstLine.linesCornerRadius = 8
            firstLine.lastLineFillPercent = 1
        }
    }
    @IBOutlet weak var secondLine: UILabel! {
        didSet {
            secondLine.linesCornerRadius = 8
            secondLine.lastLineFillPercent = 1
        }
    }
    @IBOutlet weak var thirdline: UILabel! {
        didSet {
            thirdline.linesCornerRadius = 8
            thirdline.lastLineFillPercent = 1
        }
    }
    @IBOutlet weak var backgroundColorView: UIView!
    @IBOutlet weak var lineTopView: UIView!
    @IBOutlet weak var lineBottomView: UIView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        backgroundColor = Palette.ppColorGrayscale000.color
        contentView.backgroundColor = Palette.ppColorGrayscale000.color
        firstLine.attributedText = nil
        firstLine.isHidden = true
        secondLine.attributedText = nil
        secondLine.isHidden = true
        thirdline.attributedText = nil
        thirdline.isHidden = true
        ilustrationView.cancelRequest()
        showSkeletonAnimation()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        showSkeletonAnimation()
    }
}

extension CreditHomeFeedTableViewCell {
    func configure(item: CPFeedItem) {
        hideSkeletonAnimation()
        selectionStyle = .none
        if let url = item.imageUrl {
            ilustrationView.setImage(url: URL(string: url))
            ilustrationView.isHidden = false
        } else {
            ilustrationView.isHidden = true
            ilustrationView.image = nil
        }
        
        let firstLineAttributedString = NSMutableAttributedString(string: item.firstLabel.value, attributes: [.foregroundColor: Palette.hexColor(with: item.firstLabel.color) ?? Palette.ppColorGrayscale600.color])
        let secondLineAttributedString = NSMutableAttributedString(string: item.secondLabel.value, attributes: [.foregroundColor: Palette.hexColor(with: item.secondLabel.color) ?? Palette.ppColorGrayscale600.color])
        let thirdLineAttributedString = NSMutableAttributedString(string: item.thirdLabel.value, attributes: [.foregroundColor: Palette.hexColor(with: item.thirdLabel.color) ?? Palette.ppColorGrayscale600.color])
        if item.cancelled {
            firstLineAttributedString.addAttribute(.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(0..<item.firstLabel.value.count))
            secondLineAttributedString.addAttribute(.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(0..<item.secondLabel.value.count))
            thirdLineAttributedString.addAttribute(.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(0..<item.thirdLabel.value.count))
        }
        
        firstLine.isHidden = item.firstLabel.value.isEmpty
        secondLine.isHidden = item.secondLabel.value.isEmpty
        thirdline.isHidden = item.thirdLabel.value.isEmpty
        
        dateLabel.text = item.dateLabel.value
        dateLabel.textColor = Palette.hexColor(with: item.dateLabel.color)
        
        backgroundColorView.layer.cornerRadius = 0
        backgroundColorView.backgroundColor = Palette.ppColorGrayscale000.color
        firstLine.attributedText = !firstLine.isHidden ? firstLineAttributedString : nil
        secondLine.attributedText = !secondLine.isHidden ? secondLineAttributedString : nil
        thirdline.attributedText = !thirdline.isHidden ? thirdLineAttributedString : nil
        backgroundColorView.layoutIfNeeded()
    }
    
    private func configureIfFirstCell() {
        /// set corner radius for first cell
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        backgroundColorView.layer.cornerRadius = 8.0
        backgroundColorView.clipsToBounds = true
        
        /// remove line of time line in top position
        lineTopView.isHidden = true
        lineBottomView.isHidden = false
        
        /// add view in the left bottom position in the cell for remove corner radius of cell
        let leftBottom = UIView()
        leftBottom.backgroundColor = backgroundColorView.backgroundColor
        leftBottom.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(leftBottom)
        
        NSLayoutConstraint.activate([
            leftBottom.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
            leftBottom.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            leftBottom.heightAnchor.constraint(equalToConstant: 8),
            leftBottom.widthAnchor.constraint(equalToConstant: 8)
        ])
        
        /// add view in the right bottom position in the cell for remove corner radius of cell
        let rightBottom = UIView()
        rightBottom.backgroundColor = backgroundColorView.backgroundColor
        rightBottom.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(rightBottom)
        
        NSLayoutConstraint.activate([
            rightBottom.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
            rightBottom.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
            rightBottom.heightAnchor.constraint(equalToConstant: 8),
            rightBottom.widthAnchor.constraint(equalToConstant: 8)
        ])
    }
    
    private func showSkeletonAnimation() {
        backgroundColorView.backgroundColor = Palette.ppColorGrayscale000.color
        ilustrationView.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: Palette.ppColorGrayscale200.color))
        dateLabel.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: Palette.ppColorGrayscale200.color))
        dateLabel.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: Palette.ppColorGrayscale200.color))
        firstLine.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: Palette.ppColorGrayscale200.color))
        secondLine.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: Palette.ppColorGrayscale200.color))
        thirdline.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: Palette.ppColorGrayscale200.color))
    }
    
    private func hideSkeletonAnimation() {
        ilustrationView.hideSkeleton()
        dateLabel.hideSkeleton()
        firstLine.hideSkeleton()
        secondLine.hideSkeleton()
        thirdline.hideSkeleton()
    }
}

extension CreditHomeFeedTableViewCell {
    enum Position {
        case first
        case middle
        case last
    }
}
