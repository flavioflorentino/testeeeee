import Foundation

final class CreditHomeHeaderViewModel {
    var account: CPAccount?
    var needUpdateAnimationOfLimitView: Bool = false
    var isPayableInvoice: Bool {
        if let invoiceEnabled = account?.settings.invoicePaymentEnabled {
            return invoiceEnabled
        }
        return false
    }
    var isCreditor: Bool {
        guard let invoice = self.account?.currentInvoice else {
            return false
        }
        return invoice.totalValue < 0
    }
    var isAnimatedCircleView: Bool = false
    
    init(with account: CPAccount?) {
        self.account = account
        self.isAnimatedCircleView = false
    }
    
    func update(account: CPAccount?) {
        self.account = account
        needUpdateAnimationOfLimitView = false
    }
}
