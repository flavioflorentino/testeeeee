//
//  CPHomeHeaderErrorView.swift
//  PicPay
//
//  ☝🏽☁️ Created by Luiz Henrique Guimarães on 22/03/18.
//

import UIKit

final class CPHomeHeaderErrorView: NibView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    init(frame: CGRect, error: PicPayErrorDisplayable) {
        super.init(frame: frame)
        view.backgroundColor = .clear
        titleLabel.text = error.title
        descriptionLabel.text = error.message
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
