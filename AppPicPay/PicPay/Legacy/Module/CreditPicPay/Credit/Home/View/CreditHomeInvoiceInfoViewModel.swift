import CoreLegacy
import Foundation

final class CreditHomeInvoiceInfoViewModel {
    private let invoice: CreditInvoice
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "pt_BR")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    var valueDescription: String = ""
    var valueLabelsIsHidden: Bool = false
    var dateLabelsIsHidden: Bool = false
    var dateDescription: String = ""
    var value: String {
        let value = invoice.totalValue > 0 ? invoice.totalValue : 0
        return CurrencyFormatter.brazillianRealString(from: NSNumber(value: value))
    }
    var date: String {
        switch invoice.status {
        case .delayed, .closed, .openPayment:
            let dueDate = invoice.dueDate
            return dateFormatter.string(from: dueDate)
        case .open:
            let closingDate = invoice.closingDate
            return dateFormatter.string(from: closingDate)
        }
    }
    
    init(with invoice: CreditInvoice) {
        self.invoice = invoice
        setup()
    }
    
    private func setup() {
        dateDescription = CreditPicPayLocalizable.dueDate.text
        switch invoice.status {
        case .delayed:
            valueDescription = CreditPicPayLocalizable.delayedInvoice.text
        case .open:
            valueDescription = CreditPicPayLocalizable.currentInvoice.text
            dateDescription = CreditPicPayLocalizable.closing.text
        case .closed:
            valueDescription = CreditPicPayLocalizable.invoiceClosed.text
        case .openPayment:
            valueDescription = CreditPicPayLocalizable.invoiceClosed.text
        }
    }
}
