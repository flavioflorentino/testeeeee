//
//  CreditHomeLimitView.swift
//  PicPay
//
//  ☝🏽☁️ Created by Luiz Henrique Guimarães on 20/03/18.
//

import CoreLegacy
import SnapKit
import UI
import UIKit

final class CreditHomeLimitView: NibView {
    // progress properties
    private var progress: CGFloat = 1.0
    
    fileprivate lazy var progressBackgroundLayer: CAShapeLayer = {
        let layer = CAShapeLayer()
        layer.opacity = 1
        layer.lineWidth = 2
        layer.strokeColor = Palette.ppColorGrayscale600.color.withAlphaComponent(0.2).cgColor
        layer.fillColor = UIColor.clear.cgColor
        return layer
    }()
    
    fileprivate var progressLayer: CAShapeLayer = {
        let layer = CAShapeLayer()
        layer.lineWidth = 3
        layer.strokeColor = Palette.ppColorBranding300.cgColor
        layer.fillColor = UIColor.clear.cgColor
        layer.strokeEnd = 1
        return layer
    }()
    
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.text = ""
        }
    }
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var currencyLabel: UILabel!

    // MARK: - Initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        backgroundColor = .clear
        progress = 1.0
        progressView.layer.addSublayer(progressBackgroundLayer)
        progressView.layer.addSublayer(progressLayer)
    }
    
    func configure(account: CPAccount?) {
        let avaliableCredit = account?.availableCredit ?? 0.0
        let selectedCredit = account?.selectedCredit ?? 0.0
        let value = CurrencyFormatter.brazillianRealString(from: NSNumber(value: avaliableCredit)).replacingOccurrences(of: "R$", with: "")
        valueLabel.text = value
        currencyLabel.isHidden = false
        let percentage = ceil(avaliableCredit * 100 / selectedCredit) / 100
        progress = avaliableCredit == 0.0 ? 0.0 : CGFloat(percentage)
        progressLayer.strokeEnd = progress
        setUpProgressLayer(for: account)
        setUpDescriptionLabel(for: account)
    }

    private func setUpProgressLayer(for account: CPAccount?) {
        progressLayer.strokeColor = account?.creditStatus == .blocked ? Palette.white.cgColor : Palette.ppColorBranding300.cgColor
    }
    
    private func setUpDescriptionLabel(for account: CPAccount?) {
        guard let account = account else {
            return
        }
        if account.creditStatus == .blocked {
            descriptionLabel.text = CreditPicPayLocalizable.blockedLimit.text
        } else {
            descriptionLabel.text = CreditPicPayLocalizable.availableLimit.text
        }
    }
    
    func startAnimate() {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 1
        animation.toValue = progress
        animation.duration = 0.8
        progressLayer.add(animation, forKey: nil)
    }
    
    // MARK: UI
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
         // update Progress circle layer
        let circlePath = UIBezierPath(ovalIn: CGRect(origin: CGPoint.zero, size: CGSize(width: progressView.frame.size.width, height: progressView.frame.size.height))).cgPath
       
        progressBackgroundLayer.frame = progressView.bounds
        progressBackgroundLayer.path = circlePath
        progressLayer.frame = progressView.bounds
        progressLayer.path = UIBezierPath(
            arcCenter: CGPoint(x: progressView.frame.size.width / 2, y: progressView.frame.size.height / 2),
            radius: progressView.frame.size.width / 2,
            startAngle: CGFloat(270 * Double.pi / 180.0),
            endAngle: CGFloat(270.01 * Double.pi / 180.0),
            clockwise: false
        ).cgPath
    }
}
