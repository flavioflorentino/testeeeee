import UI
import UIKit

final class PaginateTimelineErrorView: UIView {
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .medium)
        label.text = CreditPicPayLocalizable.weWereUnableRetrieve.text
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private lazy var errorImageView: UIImageView = {
        let widthImageView: CGFloat = 26
        let heightImageView: CGFloat = widthImageView
        let imageView = UIImageView(image: #imageLiteral(resourceName: "cancelled"))
        imageView.contentMode = .center
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant: widthImageView),
            imageView.widthAnchor.constraint(equalToConstant: heightImageView)
        ])
        return imageView
    }()
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [errorImageView, textLabel])
        stackView.alignment = .fill
        stackView.distribution = .fillProportionally
        stackView.axis = .horizontal
        stackView.spacing = 20
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    var onTap: (() -> Void) = {}
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
        setUpTapGesture()
    }
    
    private func setUpView() {
        addSubview(rootStackView)
        backgroundColor = Palette.ppColorGrayscale000.color
        NSLayoutConstraint.activate([
            rootStackView.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            rootStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            rootStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            rootStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20)
        ])
        layoutIfNeeded()
    }
    
    private func setUpTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        addGestureRecognizer(tap)
    }
    
    @objc
    private func tapAction() {
        onTap()
    }
}
