import SkeletonView
import UI
import UIKit


protocol CreditHomeInvoiceInfoViewDelegate: AnyObject {
    func didTouchInvoice()
}

final class CreditHomeInvoiceInfoView: NibView {
    private var viewModel: CreditHomeInvoiceInfoViewModel?
    private var isBeginStartAnimations: Bool = true
    @IBOutlet weak var invoiceButton: UIButton?
    var delegate: CreditHomeInvoiceInfoViewDelegate?
    @IBOutlet weak var valueDescriptionLabel: UILabel! {
        didSet {
            valueDescriptionLabel.linesCornerRadius = 8
            valueDescriptionLabel.lastLineFillPercent = 1
        }
    }
    
    @IBOutlet weak var valueLabel: UILabel! {
        didSet {
            valueLabel.linesCornerRadius = 8
            valueLabel.lastLineFillPercent = 1
        }
    }
    
    @IBOutlet weak var dateDescriptionLabel: UILabel! {
        didSet {
            dateDescriptionLabel.linesCornerRadius = 8
            dateDescriptionLabel.lastLineFillPercent = 1
        }
    }
    
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.linesCornerRadius = 8
            dateLabel.lastLineFillPercent = 1
        }
    }
    @IBOutlet weak var dateLabelsStackView: UIStackView!
    @IBOutlet weak var valueLabelsStackView: UIStackView!
    @IBOutlet weak var valueImageView: UIImageView!
    @IBOutlet weak var dateImageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        view.backgroundColor = .clear
        animations()
        addTargetsButton()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addTargetsButton() {
        invoiceButton?.addTarget(self, action: #selector(tapInInvoice), for: .touchUpInside)
    }
    
    func configure(invoice: CreditInvoice?) {
        guard let invoice = invoice else {
            animations()
            return
        }
        isBeginStartAnimations = false
        viewModel = CreditHomeInvoiceInfoViewModel(with: invoice)
        animations()
        
        if let viewModel = self.viewModel {
            valueDescriptionLabel.text = viewModel.valueDescription
            valueLabel.text = viewModel.value
            dateDescriptionLabel.text = viewModel.dateDescription
            dateLabel.text = viewModel.date
            valueLabelsStackView.isHidden = viewModel.valueLabelsIsHidden
            dateLabelsStackView.isHidden = viewModel.dateLabelsIsHidden
        }
    }
}

// MARK: CreditHomeInvoiceInfoView Button Actions
extension CreditHomeInvoiceInfoView {
    @objc func tapInInvoice() {
        delegate?.didTouchInvoice()
    }
}

// MARK: CreditHomeInvoiceInfoView animations
extension CreditHomeInvoiceInfoView {
    private func animations() {
        let isHiddenImageView: Bool = isBeginStartAnimations
        let labels = [dateLabel, valueLabel, dateDescriptionLabel, valueDescriptionLabel]
        let imagesView = [valueImageView, dateImageView]
        imagesView.forEach { imageView in
            imageView?.isHidden = isHiddenImageView
        }
        let baseColor = superview?.superview?.superview?.superview?.backgroundColor ?? PicPayStyle.picPayCreditGreen
        let secondaryColor = Palette.ppColorGrayscale000.color.withAlphaComponent(0.25)
        let gradientColor = SkeletonGradient(baseColor: baseColor.withAlphaComponent(0.15), secondaryColor: secondaryColor)
        labels.forEach { label in
            if isBeginStartAnimations {
                label?.showAnimatedGradientSkeleton(usingGradient: gradientColor, animation: nil)
            } else {
                label?.hideSkeleton()
            }
        }
    }
}
