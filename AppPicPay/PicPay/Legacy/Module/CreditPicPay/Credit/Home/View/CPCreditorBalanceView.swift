//
//  CPCreditorBalanceView.swift
//  PicPay
//
//  Created by Lucas Romano on 14/09/2018.
//

import UI
import UIKit

final class CPCreditorBalanceView: NibView {
    @IBOutlet weak var descriptionValueLabel: UILabel!
    
    public func setCreditorBalance(with account: CPAccount?) {
        guard let invoice = account?.currentInvoice else {
            return
        }
        let value = invoice.totalValue 
        guard let valueFormatted = CurrencyFormatter.brazillianRealString(from: NSNumber(value: -1 * value)) else {
            return
        }
        let labelAttributedString = NSMutableAttributedString(
            string: CreditPicPayLocalizable.doYouHaveCreditBalance.text,
            attributes: [
                .foregroundColor: Palette.white.color,
                .kern: -0.31
            ]
        )
        let valueAttributedString = NSAttributedString(string: valueFormatted, attributes: [.foregroundColor: Palette.ppColorBranding300.color])
        labelAttributedString.append(valueAttributedString)
        descriptionValueLabel.attributedText = labelAttributedString
    }
}
