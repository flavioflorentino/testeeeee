import UI
import UIKit

protocol CreditHomeHeaderViewDelegate: AnyObject {
    func didSetHeaderViewBackgroundColor()
}

final class CreditHomeHeaderView: UIView {
    lazy var limitView: CreditHomeLimitView = {
        let view = CreditHomeLimitView()
        return view
    }()
    lazy var invoiceDetailView: CreditHomeInvoiceInfoView = {
        let view = CreditHomeInvoiceInfoView()
        view.backgroundColor = .clear
        return view
    }()
    private lazy var creditorBalanceView: CPCreditorBalanceView = {
        let view = CPCreditorBalanceView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        var view = UIStackView()
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .fillProportionally
        view.spacing = Spacing.base01
        view.layoutMargins = UIEdgeInsets(top: Spacing.base01, left: Spacing.none, bottom: Spacing.base01, right: Spacing.none)
        return view
    }()
    
    private lazy var spaceBottonView: UIView = {
        let spaceViewBottom = UIView(frame: CGRect.zero)
        spaceViewBottom.backgroundColor = .clear
        return spaceViewBottom
    }()
    
    private let viewModel: CreditHomeHeaderViewModel

    private var headerView: UIView?
    private var gradientLayer: CAGradientLayer?
    
    private let regularGradientColor = [Palette.ppColorBrandingPlus400.gradientColor.from, Palette.ppColorBrandingPlus400.gradientColor.to]
    private let blockedGradientColor = [Palette.ppColorGrayscalePlus.gradientColor.from, Palette.ppColorGrayscalePlus.gradientColor.to]
    
    weak var delegate: CreditHomeHeaderViewDelegate?
    
    var openInvoiceAction: ((CreditInvoice) -> Void)?
    var errorTouchAction: (() -> Void)?
    var state: CreditHomeHeaderView.State = .loading {
        didSet {
            if case .error(let error) = self.state {
                configure(error: error)
            }
        }
    }

    // MARK: Initializer
    init(frame: CGRect, account: CPAccount?) {
        self.viewModel = CreditHomeHeaderViewModel(with: account)
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func animateCircleView() {
        guard !viewModel.isAnimatedCircleView else {
            return
        }
        limitView.startAnimate()
    }
    
    func configure(account: CPAccount?) {
        viewModel.update(account: account)
        limitView.configure(account: account)

        stackView.removeAllSubviews()
        stackView.addArrangedSubview(limitView)
        stackView.addArrangedSubview(invoiceDetailView)
        configureInvoiceDetailView()
        setUpEmptyInvoiceInformation()
        configureStackViewOnPayableInvoice()
        insertBottomSpaceInStackView()
        resizeAndApplyGradient()
        configureHeaderColor()
        animateCircleView()
    }
}

extension CreditHomeHeaderView {
    private func setup() {
       
        
        configure(account: viewModel.account)
        
        headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: 0))
        headerView?.addSubview(stackView)
        
        // add stack constraints
        stackView.snp.makeConstraints { make in
            make.width.equalTo(self.frame.size.width)
            make.top.equalTo(0)
            make.leading.equalTo(0)
        }
        
        // force stack size
        stackView.sizeToFit()
        stackView.layoutIfNeeded()
        
        headerView?.frame = CGRect(origin: CGPoint.zero, size: stackView.frame.size)
        
        if let headerView = headerView {
            self.frame = headerView.frame
            self.addSubview(headerView)
        }
        
        headerView?.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    private func configure(error: PicPayErrorDisplayable) {
        clearStackView()
        let connectionErrorView = CPHomeHeaderErrorView(frame: .zero, error: error)
        connectionErrorView.backgroundColor = .clear
        connectionErrorView.snp.makeConstraints { make in
            make.height.equalTo(220)
        }
        connectionErrorView.isUserInteractionEnabled = true
        connectionErrorView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(touchErrorView)))
        stackView.addArrangedSubview(connectionErrorView)
        resizeAndApplyGradient()
        configureHeaderColor()
    }
    
    /// Add bottom space
    private func insertBottomSpaceInStackView() {
        spaceBottonView.snp.makeConstraints { make in
            make.height.equalTo(50)
        }
        stackView.addArrangedSubview(spaceBottonView)
    }
    
    private func configureInvoiceDetailView() {
        invoiceDetailView.configure(invoice: viewModel.account?.currentInvoice)
    }
    
    private func configureStackViewOnPayableInvoice() {
        guard viewModel.account?.currentInvoice != nil else {
            return
        }
        if viewModel.isPayableInvoice {
            let invoiceButton = InvoiceButton(frame: self.frame)
            invoiceButton.setup(with: viewModel.account)
            invoiceButton.button.addTarget(self, action: #selector(openInvoice(_:)), for: .touchUpInside)
            stackView.addArrangedSubview(SpacerView(axis: .vertical, size: Spacing.base05))
            stackView.addArrangedSubview(invoiceButton)
        }
        
        if viewModel.isCreditor {
            creditorBalanceView.setCreditorBalance(with: viewModel.account)
            stackView.addArrangedSubview(creditorBalanceView)
        }
    }
    
    private func setUpEmptyInvoiceInformation() {
        guard case .success = self.state else {
            return
        }
        guard viewModel.account?.currentInvoice == nil else {
            return
        }
        let emptyInvoice = CreditHomeEmptyInvoice(frame: CGRect(x: 0,
                                                                y: 0,
                                                                width: self.frame.size.width,
                                                                height: 60))
        stackView.addArrangedSubview(emptyInvoice)
        invoiceDetailView.removeFromSuperview()
    }
    
    /// Force stack size
    private func resizeAndApplyGradient() {
        stackView.sizeToFit()
        stackView.setNeedsLayout()
        stackView.layoutIfNeeded()
        headerView?.frame = CGRect(origin: CGPoint.zero, size: stackView.frame.size)
        if let headerView = headerView {
            frame = headerView.frame
        }
    }
    
    private func clearStackView() {
        stackView.removeAllArrangedSubviews()
    }
    
    private func configureHeaderColor() {
        gradientLayer?.removeFromSuperlayer()
        let colors: [UIColor] = viewModel.account?.creditStatus == .blocked ? blockedGradientColor : regularGradientColor
        gradientLayer = stackView.applyGradient(withColours: colors, gradientOrientation: .vertical)
        delegate?.didSetHeaderViewBackgroundColor()
    }
    
    @objc(openInvoice:)
    private func openInvoice(_ sender: Any?) {
        guard let invoice = viewModel.account?.currentInvoice else {
            return
        }
        openInvoiceAction?(invoice)
    }
    
    @objc(touchErrorView)
    private func touchErrorView() {
        errorTouchAction?()
    }
}

extension CreditHomeHeaderView {
    // Extra classes
    enum State {
        case loading
        case success
        case error(PicPayErrorDisplayable)
    }
    
    fileprivate final class InvoiceButton: UIView {
        let button = UIPPButton(frame: CGRect.zero)
        
        override init(frame: CGRect) {
            super.init(frame: frame)
        }

        @available(*, unavailable)
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func setup(with account: CPAccount?) {
            guard let invoice = account?.currentInvoice else {
            return
        }
            backgroundColor = .clear
            button.normalTitleColor = Palette.white.color
            button.borderColor = UIColor.clear
            button.borderWidth = 0
            button.cornerRadius = 24
            button.highlightedBackgrounColor = account?.creditStatus == .blocked ? Palette.ppColorNegative200.color : Palette.ppColorBranding200.color
            button.normalBackgrounColor = account?.creditStatus == .blocked ? Palette.ppColorNegative300.color : Palette.ppColorBranding300.color
            button.highlightedTitleColor = Palette.white.color
            button.setTitle(CreditPicPayLocalizable.payInvoice.text, for: .normal)
            
            addSubview(button)
            button.snp.makeConstraints({ make in
                make.width.equalTo(self.frame.width - 48)
                make.height.equalTo(44)
                make.top.equalTo(10)
                make.centerX.equalToSuperview()
            })
            
            self.snp.makeConstraints { make in
                make.height.equalTo(60)
            }
            
            if invoice.status == .delayed {
                button.normalBackgrounColor = Palette.ppColorNegative300.color
            }
            setNeedsLayout()
        }
    }
}
