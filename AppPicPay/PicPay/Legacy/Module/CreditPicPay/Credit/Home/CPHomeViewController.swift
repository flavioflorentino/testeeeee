import Card
import CoreLegacy
import SecurityModule
import SkeletonView
import UI
import UIKit

//swiftlint:disable file_length
final class CPHomeViewController: PPBaseViewController, Obfuscable {
    private let viewModel: CPHomeViewModel
    private var navigationTitleView: NavigationTitle2LinesView?
    private var navigationTitleLabel: UILabel?
    
    private lazy var errorView: UIView = {
        let connectionError = ConnectionErrorView(frame: .zero)
        connectionError.translatesAutoresizingMaskIntoConstraints = false
        connectionError.tryAgainTapped = { [weak self] in
            self?.loadData()
        }
        
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.layer.cornerRadius = 8
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: self.view.frame.width).isActive = true
        view.heightAnchor.constraint(equalToConstant: self.view.frame.height).isActive = true
        view.addSubview(connectionError)
        
        connectionError.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
        connectionError.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        connectionError.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        return view
    }()
    
    private lazy var headerView: CreditHomeHeaderView = {
        let width: CGFloat = view.frame.size.width
        let headerView = CreditHomeHeaderView(frame: CGRect(x: 0, y: 0, width: width, height: 0), account: viewModel.account)
        headerView.delegate = self
        headerView.openInvoiceAction = { [weak self] invoice in
            self?.openInvoice(invoice: invoice)
            PPAnalytics.trackEvent(CreditPicPayEvent.Registration.invoiceHome, properties: nil)
        }
        headerView.errorTouchAction = { [weak self] in
            self?.setUpHeader()
            self?.loadAccount()
        }
        headerView.invoiceDetailView.delegate = self
        return headerView
    }()
    
    private lazy var paginateErrorView: PaginateTimelineErrorView = {
        let width: CGFloat = view.frame.width
        let height: CGFloat = 80.0
        let rect = CGRect(x: 0, y: 0, width: width, height: height)
        let errorView = PaginateTimelineErrorView(frame: rect)
        errorView.onTap = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            strongSelf.tableView.tableFooterView = strongSelf.activityIndicatorView
            strongSelf.loadTimeline()
        }
        return errorView
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.isSkeletonable = true
        tableView.estimatedRowHeight = 70
        tableView.tableFooterView = footerView
        tableView.backgroundColor = .clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.registerCell(type: CreditHomeFeedTableViewCell.self)
        tableView.backgroundView = headerView
        return tableView
    }()
    
    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.backgroundColor = viewModel.backgroundColorOfLastItem
        activityIndicatorView.startAnimating()
        activityIndicatorView.frame = CGRect(x: 0.0, y: 0.0, width: tableView.bounds.width, height: 44.0)
        return activityIndicatorView
    }()
    
    private var footerView: UIView {
        let width: CGFloat = view.frame.width
        let height: CGFloat = viewModel.heightForFooterView
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        footerView.backgroundColor = Palette.ppColorGrayscale000.color
        return footerView
    }
    
    private lazy var tableViewRefreshControl: UIRefreshControl = {
        let tableViewRefreshControl = UIRefreshControl()
        tableViewRefreshControl.tintColor = Colors.white.lightColor
        tableViewRefreshControl.addTarget(self, action: #selector(pullRefreshData), for: .valueChanged)
        return tableViewRefreshControl
    }()
    
    //Screen obfuscation
    var appSwitcherObfuscator: AppSwitcherObfuscating?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    init(with viewModel: CPHomeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        viewModel.isViewWillApper = true
        viewModel.prepareForLoadingTimeline()
        viewModel.onCompletionLoads = { [weak self] in
            self?.tableViewRefreshControl.endRefreshing()
            self?.headerView.state = CreditHomeHeaderView.State.success
            self?.setUpHeader()
        }
        tableView.refreshControl = tableViewRefreshControl
        tableView.isScrollEnabled = false
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObfuscationConfiguration()
        viewModel.trackScreenViewEvent()
        headerView.animateCircleView()
        loadData()
        configureNavigationController()
        // iOS 13 Bug - Verificar se nas atualizações futuras do iOS ainda é necessário manter esse fix.
        // https://forums.developer.apple.com/message/378841#378841
        if #available(iOS 13.0, *) {
            DispatchQueue.main.async {
                self.navigationController?.navigationBar.setNeedsLayout()
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        tableViewRefreshControl.endRefreshing()
        deinitObfuscator()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.isViewWillApper = false
    }
}

extension CPHomeViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(type: CreditHomeFeedTableViewCell.self, forIndexPath: indexPath)
        if let item = viewModel.getTimeLineItem(in: indexPath.row) {
            cell.configure(item: item)
        }
        if indexPath.row == 0 {
            cell.position = .first
        } else if indexPath.row == viewModel.numberOfRows() - 1 {
            cell.position = .last
        } else {
            cell.position = .middle
        }
        return cell
    }
}

extension CPHomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didTapInFeedItem()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        
        if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex && !viewModel.noNextPage {
            tableView.tableFooterView = activityIndicatorView
        } else {
            tableView.tableFooterView = footerView
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = Float(scrollView.contentOffset.y)
        navigationTitleLabel?.layer.opacity = 1.0 - Float.percentageInterval(min: -140, max: -110, value: y)
        navigationTitleView?.layer.opacity = Float.percentageInterval(min: -140, max: -90, value: y)
        navigationItem.titleView = y > -140 ? navigationTitleView : navigationTitleLabel
        
        if scrollView.contentOffset.y > -tableView.contentInset.top {
            tableView.backgroundColor = Palette.ppColorGrayscale000.color
        } else {
            tableView.backgroundColor = viewModel.backgroundTableViewColor
        }
        
        // infinity scroll
        guard !viewModel.isViewWillApper
            && viewModel.isLoadedTimeline
            && !viewModel.noNextPage
            && !viewModel.isPaginateError else {
                return
        }
        
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height {
            loadTimeline()
        }
    }
}

extension CPHomeViewController: SkeletonTableViewDataSource {
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return String(describing: CreditHomeFeedTableViewCell.self)
    }
}

extension CPHomeViewController {
    private func setup() {
        title = CreditPicPayLocalizable.featureName.text
        view.backgroundColor = Palette.ppColorGrayscale000.color
        NotificationCenter.default.addObserver(self, selector: #selector(loadData), name: Notification.Name.CreditPicPay.reloadHomeCPCredit, object: nil)
        setupTableView()
        setUpHeader()
        setupNavigationItens()
    }
    
    private func setupNavigationItens() {
        let moreInfoButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ico_followers_more"), style: .done, target: self, action: #selector(showMoreOptions))
        let closeLabelButton = UIBarButtonItem(title: DefaultLocalizable.btClose.text, style: .done, target: self, action: #selector(dismissController))
        
        if navigationController?.presentingViewController != nil {
            navigationItem.leftBarButtonItem = closeLabelButton
        }
        
        navigationItem.rightBarButtonItem = moreInfoButton
    }
    
    private func setupTableView() {
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
    }
    
    @objc private func dismissController() {
        navigationController?.dismiss(animated: true)
    }
    
    private func setUpHeader() {
        guard let account = viewModel.account,
            (!viewModel.isLoadedAccount && viewModel.isLoadedTimeline)
                || (viewModel.isLoadedAccount && viewModel.isLoadedTimeline) else {
                    return
        }
        headerView.configure(account: account)
        
        navigationTitleView?.titleLabel.text = CurrencyFormatter.brazillianRealString(from: NSNumber(value: viewModel.account?.availableCredit ?? 0))
        navigationTitleView?.layer.opacity = 0.0
        
        let cornerRadiusHeight: CGFloat = 8.0
        tableView.contentInset.top = headerView.frame.size.height - cornerRadiusHeight
        tableView.contentOffset.y = -tableView.contentInset.top
    }

    private func pushToDetailInvoice(with id: String) {
        viewModel.showInvoiceDetail(with: id)
    }
    
    private func showHeaderError(_ error: PicPayErrorDisplayable) {
        headerView.state = .error(error)
    }
    
    // MARK: - account methods
    private func loadAccount() {
        headerView.state = .loading
        viewModel.loadAccount(onCacheLoaded: { [weak self] in
            guard let viewModel = self?.viewModel,
                !viewModel.isRefreshData else {
                    return
            }
            self?.headerView.state = CreditHomeHeaderView.State.loading
            self?.setUpHeader()
        }, onError: { [weak self] error in
            self?.showHeaderError(error)
        })
    }
    
    // MARK: - Timeline methods
    @objc
    private func loadTimeline() {
        viewModel.loadTimeline { [weak self] indexPaths, error in
            if error != nil {
                self?.setUpErrorTimeline()
                return
            }
            self?.updateTableView(in: indexPaths)
        }
    }
    
    private func setUpErrorTimeline() {
        if !viewModel.isFirstTimelineLoad {
            tableView.tableFooterView = paginateErrorView
            return
        }
        setTimelineViewError()
    }
    
    private func updateTableView(in indexPaths: [IndexPath]) {
        tableView.isScrollEnabled = true
        tableView.tableFooterView = footerView
        tableView.reloadData()
        
        if viewModel.isViewWillApper {
            viewModel.isViewWillApper = false
        }
    }
    
    // MARK: - User Actions
    @objc
    func loadData() {
        loadAccount()
        loadTimeline()
    }
    
    @objc
    func pullRefreshData() {
        guard !viewModel.isRefreshData else {
            return
        }
        viewModel.isRefreshData = true
        loadData()
    }
    
    @objc
    func showInvoices() {
        viewModel.showInvoices()
    }
    
    @objc
    private func showMoreOptions() {
        viewModel.showOptions()
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let invoiceAction = UIAlertAction(title: CreditPicPayLocalizable.invoices.text, style: .default) { [weak self] _ in
            self?.showInvoices()
        }
        
        actionSheet.addAction(invoiceAction)
        
        if viewModel.isVirtualCardAvailable {
            let virtualCardAction = UIAlertAction(title: CreditPicPayLocalizable.virtualCard.text, style: .default) { [weak self] _ in
                self?.viewModel.showVirtualCard()
            }
            actionSheet.addAction(virtualCardAction)
        }
        
        if viewModel.isPhysicalCardAvailable {
            let configurateCardAction = UIAlertAction(title: CreditPicPayLocalizable.configurateCard.text, style: .default) { [weak self] _ in
                self?.viewModel.showConfigureCard()
            }
            actionSheet.addAction(configurateCardAction)
        }
        
        let cancelAction = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel) { [weak self] _ in
            self?.viewModel.closeMenu()
        }
        actionSheet.addAction(cancelAction)
        
        present(actionSheet, animated: true)
    }
    
    @objc
    private func configureNavigationController() {
        navigationController?.navigationBar.barTintColor = viewModel.headerColor
    }
    
    private func openInvoice(invoice: CreditInvoice) {
        viewModel.payInvoice()
    }
    
    private func setTimelineViewError() {
        tableView.tableFooterView = errorView
        tableView.reloadData()
    }
}

extension CPHomeViewController: CreditHomeHeaderViewDelegate {
    func didSetHeaderViewBackgroundColor() {
        configureNavigationController()
    }
}
extension CPHomeViewController: CreditHomeInvoiceInfoViewDelegate {
    func didTouchInvoice() {
        viewModel.showInvoiceDetail(with: viewModel.account?.currentInvoice?.id ?? "")
    }
}

extension CPHomeViewController: StyledNavigationDisplayable {
    var navigationBarStyle: DefaultNavigationStyle {
        CustomColorNavigationStyle(customColor: viewModel.headerColor)
    }
    
    var statusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .lightContent
        }
        return .default
    }
}
