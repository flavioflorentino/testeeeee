import Card
import Foundation
import UI

protocol CreditPicPayHomeCoordinatorDelegate: AnyObject {
    func showAlert(_ alert: Alert, completion: @escaping (AlertPopupViewController, Button, UIPPButton) -> Void)
    func showInvoiceList(for account: CPAccount?)
    func showInvoiceDetailFromHome(with id: String?, and account: CPAccount?)
    func payInvoice(with invoiceId: String?, and account: CPAccount?)
    func showConfigureCard()
    func showVirtualCard(button: VirtualCardButton)
}

final class CreditPicPayHomeCoordinator: Coordinator, Coordinating {
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    
    var currentCoordinator: Coordinator?
    private let navigationController: UINavigationController
    private let account: CPAccount?
    weak var delegate: CreditPicPayHomeCoordinatorDelegate?
    
    init(with navigationController: UINavigationController, account: CPAccount? = nil) {
        self.navigationController = navigationController
        self.account = account
    }
    
    func start() {
        guard let delegate = self.delegate else { return }
        let viewModel = CPHomeViewModel(with: account, delegate: delegate, dependencies: DependencyContainer())
        let controller = CPHomeViewController(with: viewModel)
        controller.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(controller, animated: true)
    }
    
    func startByPresent() {
        guard let delegate = self.delegate else { return }
        let viewModel = CPHomeViewModel(with: account, delegate: delegate, dependencies: DependencyContainer())
        let controller = CPHomeViewController(with: viewModel)
        navigationController.viewControllers = [controller]
        childViewController.append(controller)
    }
}
