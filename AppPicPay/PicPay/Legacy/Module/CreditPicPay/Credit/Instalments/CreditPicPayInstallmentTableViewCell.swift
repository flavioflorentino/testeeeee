import CoreLegacy
import UI
import UIKit

final class CreditPicPayInstallmentTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var bestOptionView: UIView! {
        didSet {
            bestOptionView.isHidden = true
        }
    }
    
    var isFirstCell: Bool = false {
        didSet {
            bestOptionView.isHidden = !isFirstCell
        }
    }
    var option: CreditInvoicePayment.Installment.Option? {
        didSet {
            guard let option = self.option else {
            return
        }
            titleLabel.attributedText = makeTitleAttributedText(with: option)
            descriptionLabel.text = makeDescriptionText(with: option)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bestOptionView.layer.cornerRadius = bestOptionView.frame.height / 2
        backgroundColor = Palette.ppColorGrayscale000.color
    }
}

extension CreditPicPayInstallmentTableViewCell {
    private func makeTitleAttributedText(with option: CreditInvoicePayment.Installment.Option) -> NSAttributedString {
        let valueString: String = CurrencyFormatter.brazillianRealString(from: option.openValue as NSNumber)
        let installmentValueString: String = CurrencyFormatter.brazillianRealString(from: option.installmentsValue as NSNumber)
        let installmentCount: Int = option.installmentCount - 1
        
        /// Set font size for different values, if is best option or not.
        let fontSizeForFirstAndValue: CGFloat = isFirstCell ? 22.0 : 14.0
        
        let initalText = NSAttributedString(string: CreditPicPayLocalizable.incoming.text, attributes: [
            .font: UIFont.systemFont(ofSize: fontSizeForFirstAndValue, weight: .bold),
            .foregroundColor: Palette.ppColorGrayscale500.color]
        )
        let valueText = NSAttributedString(string: valueString, attributes: [
            .font: UIFont.systemFont(ofSize: fontSizeForFirstAndValue, weight: .bold),
            .foregroundColor: Palette.ppColorBranding300.color]
        )
        let lastText = NSAttributedString(string: "\(isFirstCell ? "\n" : " ")+ \(installmentCount)x \(installmentValueString)", attributes: [
            .font: UIFont.systemFont(ofSize: 14, weight: .bold),
            .foregroundColor: Palette.ppColorGrayscale500.color]
        )
        
        /// Final text
        let attributedString: NSMutableAttributedString = NSMutableAttributedString()
        attributedString.append(initalText)
        attributedString.append(valueText)
        attributedString.append(lastText)
        
        return attributedString
    }
    
    /// Return description of cell
    ///
    /// - Parameter option: option installment
    /// - Returns: description text
    private func makeDescriptionText(with option: CreditInvoicePayment.Installment.Option) -> String {
        
        /// Convert values for currence format
        let monthTotalEffectiveCost: String = CurrencyFormatter.brazillianRealString(from: option.monthTotalEffectiveCost as NSNumber).replacingOccurrences(of: "R$ ", with: "")
        let annualTotalEffectiveCost: String = CurrencyFormatter.brazillianRealString(from: option.annualTotalEffectiveCost as NSNumber).replacingOccurrences(of: "R$ ", with:  "")
        
        /// Remove "." for "," in percentage value
        let monthInterestRate: String = "\(option.monthInterestRate)".replacingOccurrences(of: ".", with: ",")
        let annualInterestRate: String = "\(option.annualInterestRate)".replacingOccurrences(of: ".", with: ",")
        
        var descriptionText = "CET de \(monthTotalEffectiveCost) a.m. e \(annualTotalEffectiveCost) a.a.\n"
        descriptionText += "Juros de \(monthInterestRate)% a.m. / \(annualInterestRate)% a.a"
        return descriptionText
    }
}
