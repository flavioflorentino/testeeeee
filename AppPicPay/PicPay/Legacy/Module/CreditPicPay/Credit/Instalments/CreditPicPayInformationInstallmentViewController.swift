import UI
import UIKit

final class CreditPicPayInformationInstallmentViewController: PPBaseViewController {
    @IBOutlet weak var informationsView: UIView!
    @IBOutlet weak var informationsLabel: UILabel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    init() {
        super.init(nibName: "CreditPicPayInformationInstallmentViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = CreditPicPayLocalizable.installmentMoreInformation.text
        setupColors()
    }
    
    private func setupColors() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        informationsView.backgroundColor = Palette.ppColorGrayscale000.color
        informationsLabel.textColor = Palette.ppColorGrayscale500.color
    }
}
