import UI
import UIKit

final class CreditPicPayInstallmentViewController: PPBaseViewController {
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 44
        tableView.backgroundColor = Palette.ppColorGrayscale100.color
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.registerCell(type: CreditPicPayInstallmentTableViewCell.self)
        return tableView
    }()
    private lazy var headerView: UIView = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = Palette.ppColorGrayscale600.color
        label.attributedText = viewModel.headerText
        label.numberOfLines = 0
        
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.addSubview(label)
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 24),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24),
            label.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -24),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24)])
        return view
    }()
    
    private let viewModel: CreeditPicPayInstallmentViewModel
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    init(with viewModel: CreeditPicPayInstallmentViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        addComponents()
        layoutComponents()
        tableView.reloadData()
    }
    
    private func addComponents() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "iconInfo"), style: .plain, target: self, action: #selector(infoButtonTapped))
        view.addSubview(tableView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    
    private func setupView() {
        title = CreditPicPayLocalizable.installmentOptions.text
    }
}

extension CreditPicPayInstallmentViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(type: CreditPicPayInstallmentTableViewCell.self, forIndexPath: indexPath)
        cell.isFirstCell = indexPath.row == 0
        cell.option = viewModel.getOption(for: indexPath.row)
        return cell
    }
}

extension CreditPicPayInstallmentViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.tapInvoiceInstallment(in: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
}

extension CreditPicPayInstallmentViewController {
    @objc
    private func infoButtonTapped() {
        viewModel.tapInformationButton()
    }
}
