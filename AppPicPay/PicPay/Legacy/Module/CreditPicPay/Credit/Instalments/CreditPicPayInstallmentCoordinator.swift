import Foundation

protocol CreditPicPayInstallmentCoordinatorDelegate: AnyObject {
    func onTapInvoiceInstallment(_ invoice: CreditInvoicePayment.Installment.Option)
    func onTapInformation()
}

final class CreditPicPayInstallmentCoordinator: Coordinator {
    var currentCoordinator: Coordinator?
    private let navigationController: UINavigationController
    private let invoicePayment: CreditInvoicePayment
    weak var delegate: CreditPicPayInstallmentCoordinatorDelegate?
    
    init(with navigationController: UINavigationController, invoicePayment: CreditInvoicePayment) {
        self.navigationController = navigationController
        self.invoicePayment = invoicePayment
    }
    
    func start() {
        guard let invoiceInstallment = invoicePayment.installment else {
            return
        }
        let viewModel = CreeditPicPayInstallmentViewModel(with: invoiceInstallment,
                                                          invoicePayment: invoicePayment,
                                                          coordinatorDelegate: delegate)
        let controller = CreditPicPayInstallmentViewController(with: viewModel)
        navigationController.pushViewController(controller, animated: true)
    }
}
