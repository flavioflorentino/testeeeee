import Foundation
import UI

final class CreeditPicPayInstallmentViewModel {
    let invoicePayment: CreditInvoicePayment
    let installment: CreditInvoicePayment.Installment
    let coordinatorDelegate: CreditPicPayInstallmentCoordinatorDelegate?
    
    var headerText: NSAttributedString {
        let dateString = installment.datePaymentLimit.stringFormatted(with: .ddMMyyyy)        
        let text = String(format: CreditPicPayLocalizable.installmentHeader.text, dateString)
        let description: NSMutableAttributedString = NSMutableAttributedString(string: text)
        description.addAttributes([.font: UIFont.systemFont(ofSize: 14, weight: .regular),
                                   .foregroundColor: Palette.ppColorGrayscale500.color], range: NSRange(location: 0, length: 24))
        description.addAttributes([.font: UIFont.boldSystemFont(ofSize: 14),
                                   .foregroundColor: Palette.ppColorNegative300.color], range: NSRange(location: 24, length: 42))
        description.addAttributes([.font: UIFont.systemFont(ofSize: 14, weight: .regular),
                                   .foregroundColor: Palette.ppColorGrayscale500.color], range: NSRange(location: 66, length: 32))
        description.addAttributes([.font: UIFont.systemFont(ofSize: 14, weight: .bold),
                                   .foregroundColor: Palette.ppColorGrayscale500.color], range: NSRange(location: 97, length: 9))
        return description
    }
    
    init(with installment: CreditInvoicePayment.Installment,
         invoicePayment: CreditInvoicePayment,
         coordinatorDelegate: CreditPicPayInstallmentCoordinatorDelegate?) {
        self.installment = installment
        self.invoicePayment = invoicePayment
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRows(in section: Int) -> Int {
        return installment.installmentOptions.count
    }
    
    func getOption(for row: Int) -> CreditInvoicePayment.Installment.Option {
        return installment.installmentOptions[row]
    }
    
    func tapInvoiceInstallment(in indexPath: IndexPath) {
        let option = getOption(for: indexPath.row)
        coordinatorDelegate?.onTapInvoiceInstallment(option)
    }
    
    func tapInformationButton() {
        coordinatorDelegate?.onTapInformation()
    }
}
