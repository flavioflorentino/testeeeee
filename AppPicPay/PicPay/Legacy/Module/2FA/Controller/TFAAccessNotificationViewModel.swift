//
//  TFAAccessNotificationViewModel.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 12/12/18.
//

final class TFAAccessNotificationViewModel: NSObject {
    private let device: String
    private let date: String
    let changePasswordUrl: String
    private let email: String
    
    @objc
    init(device: String, date: String, email: String, changePasswordUrl: String) {
        self.device = device
        self.date = date
        self.email = email
        self.changePasswordUrl = changePasswordUrl
    }
    
    func textAttributedString() -> NSAttributedString {
        var text = "<font color='#3D4451'>"
        
        text += "<p>Identificamos uma tentativa de acesso à sua conta Picpay com seu login e senha a partir de um novo dispositivo ou instalação:</p>"
        
        text += "<p><b>Dispositivo:</b> \(device)<br>"
        text += "<b>Hora:</b> \(date)</p>"
        
        text += "<p>Se foi você, ignore esta mensagem e continue o processo de autenticação.</p>"
        text += "<p>Se <b>não</b> foi você, sua conta pode estar em risco e para proteger sua conta, recomendamos que:</p>"
        text += "<p><b>1)</b> Troque a senha do seu email \(email) (ela pode ter sido invadida).</p><p>Em seguida:</p>"
        text += "<p><b>2)</b> Crie uma nova senha</p></font><span></span>"
        
        let htmlAttributed = text.html2Attributed(UIFont.systemFont(ofSize: 16, weight: .light)) ?? NSAttributedString(string: text)
        
        return htmlAttributed
    }
}
