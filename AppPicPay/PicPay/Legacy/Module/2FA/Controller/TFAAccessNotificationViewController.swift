//
//  TFAAccessNotificationViewController.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 11/12/18.
//

import UIKit

final class TFAAccessNotificationViewController: PPBaseViewController {
    private var model: TFAAccessNotificationViewModel
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var actionButton: UIPPButton!
    
    // MARK: - Intializer
    
    @objc(initWithModel:)
    init(model: TFAAccessNotificationViewModel) {
        self.model = model
        super.init(nibName: "TFAAccessNotificationViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    // MARK: - User Actions
    
    @IBAction private func didTouchOnCreatePasswordButton(_ sender: Any) {
        showCreatePasswordController()
    }
    
    // MARK: - Internal Methods
    
    private func setupViews() {
        textLabel.attributedText = model.textAttributedString()
        
        let button = Button(title: TFALocalizable.createNewPasswordPicpay.text, type: .underlined)
        actionButton.configure(with: button, font: UIFont.systemFont(ofSize: 16, weight: .bold))
    }
    
    private func showCreatePasswordController() {
        ViewsManager.pushWebViewController(withUrl: model.changePasswordUrl, fromViewController: self, title: TFALocalizable.createNewPasswordLegacy.text, hideBottomBar: true)
    }
}
