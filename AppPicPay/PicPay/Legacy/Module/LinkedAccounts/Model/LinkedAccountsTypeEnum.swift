//
//  LinkedAccountsTypeEnum.swift
//  PicPay
//
//  Created by Lucas Romano on 18/09/2018.
//

import Foundation

enum LinkedAccountsTypeEnum: String {
    case streamlabs = "streamlabs"
}
