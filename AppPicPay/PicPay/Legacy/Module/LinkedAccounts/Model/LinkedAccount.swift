import Foundation
import SwiftyJSON

final class LinkedAccount {
    let id: String
    let type: LinkedAccountsTypeEnum
    let name: String
    let description: String
    let imgURL: URL?
    let user: String?
    var config: [String: Any]?
    
    init?(json: JSON) {
        guard let id = json["id"].string,
            let type = LinkedAccountsTypeEnum(rawValue: json["type"].string ?? ""),
            let name = json["name"].string,
            let description = json["description"].string
            else {
                return nil
        }
        
        self.id = id
        self.type = type
        self.name = name
        self.description = description
        self.imgURL = URL(string: json["img_url"].string ?? "")
        self.user = json["user"].string
        self.config = json["config"].dictionaryObject
    }
}

extension LinkedAccount: Equatable {
    static func == (lhs: LinkedAccount, rhs: LinkedAccount) -> Bool {
        return lhs.id == rhs.id
    }
}
