//
//  LinkedAccountsResponse.swift
//  PicPay
//
//  Created by Lucas Romano on 18/09/2018.
//

import Foundation
import SwiftyJSON

final class LinkedAccountsResponse: BaseApiResponse {
    
    let accounts: [LinkedAccount]
    let available: [AvailableLinkToAccount]
    
    required init?(json: JSON) {
        
        /// set accounts to list
        if let accountsJson = json["accounts"].array {
            var accountList: [LinkedAccount] = []
            for jsonAccount in accountsJson {
                guard let account = LinkedAccount(json: jsonAccount) else { continue }
                accountList.append(account)
            }
            self.accounts = accountList
        } else {
            self.accounts = []
        }
        
        /// set available accounts do link to user account
        if let availableJson = json["available"].array {
            var availableList: [AvailableLinkToAccount] = []
            for jsonAvailablet in availableJson {
                guard let account = AvailableLinkToAccount(with: jsonAvailablet) else { continue }
                availableList.append(account)
            }
            self.available = availableList
        } else {
            available = []
        }
    }
    
}
