import Foundation
import SwiftyJSON

final class AvailableLinkToAccount {
    let type: LinkedAccountsTypeEnum
    let name: String
    let description: String
    let imgURL: URL?
    
    init?(with json: JSON) {
        guard
            let type = LinkedAccountsTypeEnum(rawValue: json["type"].string ?? ""),
            let name = json["name"].string,
            let description = json["description"].string
            else {
                return nil
        }
        
        self.type = type
        self.name = name
        self.description = description
        self.imgURL = URL(string: json["img_url"].string ?? "")
    }
}
