//
//  AuthLinkForLinkAccount.swift
//  PicPay
//
//  Created by Lucas Romano on 20/09/2018.
//

import Foundation
import SwiftyJSON

final class AuthLinkForLinkAccount: BaseApiResponse {
    
    let link: URL
    
    required init?(json: JSON) {
        guard let linkURL: URL = URL(string: json["link"].string ?? "") else {
     return nil
}
        self.link = linkURL
    }
}
