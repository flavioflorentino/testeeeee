//
//  LinkedAccountsConfigProtocol.swift
//  PicPay
//
//  Created by Lucas Romano on 18/09/2018.
//

import Foundation
import SwiftyJSON

enum LinkedAccountConfigParams: String {
    case notificationForPaymentReceived = "notification_for_payment_received"
}
