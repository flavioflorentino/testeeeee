import Core
import FeatureFlag
import Foundation

final class LinkedAccountsApi: BaseApi {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func list(_ completion: @escaping (PicPayResult<LinkedAccountsResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlLinkedAccountsList, method: .get)
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func unlink(id: String,
                for type: LinkedAccountsTypeEnum,
                _ completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let endpoint = kWsUrlLinkedAccountsUnlink
                .replacingOccurrences(of: "{service_type}", with: type.rawValue)
                .replacingOccurrences(of: "{id}", with: id)
            requestManager
                .apiRequest(endpoint: endpoint, method: .delete)
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func update(for id: String,
                with params: [String: Any],
                _ completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let endpoint = kWsUrlLinkedAccountsUpdate.replacingOccurrences(of: "{id}", with: id)
            requestManager
                .apiRequest(endpoint: endpoint, method: .put, parameters: params)
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func authLink(for service: LinkedAccountsTypeEnum,
                  _ completion: @escaping (PicPayResult<AuthLinkForLinkAccount>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let endpoint: String = kWsUrlLinkedAccountsAuthLink.replacingOccurrences(of: "{service_type}", with: service.rawValue)
            requestManager
                .apiRequest(endpoint: endpoint, method: .get)
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
}
