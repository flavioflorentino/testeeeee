import UI
import UIKit

protocol ActionNotificationLinkedAccountDelegate: AnyObject {
    func onTapSwitchNotification(with status: Bool, _ completion: @escaping (Error?) -> Void)
}

final class ActionNotificationLinkedAccountTableViewCell: UITableViewCell {
    
    static let identifier: String = "actionNotificationAccount"
    
    @IBOutlet weak private var titleText: UILabel!
    @IBOutlet weak private var descriptionText: UILabel!
    @IBOutlet weak private var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak private var actionSwitch: UISwitch!
    
    weak var delegate: ActionNotificationLinkedAccountDelegate?
    var config: [String: Any]? {
        didSet {
            guard let config = self.config else {
                return
            }
            guard let notificationStatus: Bool = config[LinkedAccountConfigParams.notificationForPaymentReceived.rawValue] as? Bool else {
                return
            }
            actionSwitch.isOn = notificationStatus
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureLayoutStyle()
    }

    private func configureLayoutStyle(){
        descriptionText.textColor = Palette.ppColorGrayscale500.color
        titleText.textColor = Palette.ppColorGrayscale500.color
        self.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    @IBAction private func actionSwitchTapped(_ sender: UISwitch) {
        activityIndicator.isHidden = false
        actionSwitch.isHidden = true
        delegate?.onTapSwitchNotification(with: sender.isOn, { [weak self](error) in
            guard let strongSelf = self else {
            return
        }
            strongSelf.activityIndicator.isHidden = true
            strongSelf.actionSwitch.isHidden = false
            if error != nil {
                strongSelf.actionSwitch.isOn = !sender.isOn
            }
        })
    }
}
