import UI
import UIKit

final class LinkedAccountsViewCell: UITableViewCell {
    @IBOutlet weak var typeAccountImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var actionImageView: UIImageView!
    @IBOutlet weak var acitvityIndicator: UIActivityIndicatorView!
    
    static let identifier: String = "integratedServiceIdentifier"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        acitvityIndicator.isHidden = true
    }
    
    private func configureCell(title: String, description: String, actionIcon: UIImage, imageURL: URL?, isGrayscaleImage: Bool) {
        titleLabel.textColor = Palette.ppColorGrayscale500.color
        descriptionLabel.textColor = Palette.ppColorGrayscale500.color
        
        titleLabel.text = title
        descriptionLabel.text = description
        actionImageView.image = actionIcon
        
        guard let url = imageURL else {
            return
        }
        acitvityIndicator.isHidden = false
        typeAccountImageView.setImage(url: url) { [weak self] image in
            self?.acitvityIndicator.isHidden = true
            self?.setImage(image, grayscale: isGrayscaleImage)
        }
    }
    
    private func setImage(_ image: UIImage?, grayscale: Bool) {
        guard
            grayscale,
            let img = image
            else {
                typeAccountImageView.image = image
                return
        }
        typeAccountImageView.image = img.grayScaleImage()
    }
}

extension LinkedAccountsViewCell {
    func configureCell(for object: Any) {
        if let linkedAccount: LinkedAccount = object as? LinkedAccount {
            configureCell(
                title: linkedAccount.name,
                description: linkedAccount.description,
                actionIcon: Assets.LinkedAccounts.rightArrowlightGray.image,
                imageURL: linkedAccount.imgURL,
                isGrayscaleImage: false
            )
        } else if let availableAccount: AvailableLinkToAccount = object as? AvailableLinkToAccount {
            configureCell(
                title: availableAccount.name,
                description: availableAccount.description,
                actionIcon: Assets.LinkedAccounts.plusAddgreen.image,
                imageURL: availableAccount.imgURL,
                isGrayscaleImage: true
            )
        }
    }
}
