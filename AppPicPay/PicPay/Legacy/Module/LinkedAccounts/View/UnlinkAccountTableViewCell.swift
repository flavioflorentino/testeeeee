import UI
import UIKit

final class UnlinkAccountTableViewCell: UITableViewCell {

    static let identifier = "unlinkAccount"
    
    @IBOutlet weak var userLabel: UILabel!
    
    var account: LinkedAccount? {
        didSet {
            guard let account = self.account else {
                return
            }
            guard let user = account.user else {
                userLabel.isHidden = true
                return
            }
            userLabel.text = "\(LinkedAccountsLocalizable.youAreLogged.text) \(user)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureLayoutStyle()
    }
    
    private func configureLayoutStyle(){
        userLabel.textColor = Palette.ppColorGrayscale500.color
        self.backgroundColor = Palette.ppColorGrayscale000.color
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
