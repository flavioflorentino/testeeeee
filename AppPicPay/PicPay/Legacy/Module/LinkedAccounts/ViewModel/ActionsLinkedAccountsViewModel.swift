//
//  ActionsLinkedAccountsViewModel.swift
//  PicPay
//
//  Created by Lucas Romano on 18/09/2018.
//

import Foundation

final class ActionsLinkedAccountsViewModel {
    
    let api: LinkedAccountsApi = LinkedAccountsApi()
    let account: LinkedAccount
    
    init(with account: LinkedAccount) {
        self.account = account
    }
    
    func numberOfSections() -> Int {
        guard let config = account.config else { return 1 }
        return config.keys.count > 0 ? 2 : 1
    }
    
    func numberOfRows(in section: Int) -> Int {
        guard let config = account.config, numberOfSections() > 1 else {
            return 1
        }
        return section == 1 ? 1 : config.keys.count
    }
    
    func getTypeConfig(in row: Int) -> LinkedAccountConfigParams? {
        guard let config = account.config else {
     return nil
}
        let keys = Array(config.keys)
        guard let type: LinkedAccountConfigParams = LinkedAccountConfigParams(rawValue: keys[row]) else {
     return nil
}
        return type
    }
    
    // MARK: - Action of config
    
    func onSwitchActionNotification(isSwitted: Bool, _ completion: @escaping (Error?) -> Void) {
        guard let config: [String: Any] = account.config else {
            return
        }
        guard let notification: Bool = config[LinkedAccountConfigParams.notificationForPaymentReceived.rawValue] as? Bool else {
            return
        }
        account.config?[LinkedAccountConfigParams.notificationForPaymentReceived.rawValue] = !notification
        update(completion)
    }
    
    // MARK: - Requests
    
    private func update(_ completion: @escaping (Error?) -> Void) {
        guard let config = account.config else {
            return
        }
        var params: [String: Any] = [:]
        params["config"] = config
        api.update(for: account.id, with: params) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(_):
                    completion(nil)
                    break
                case .failure(let error):
                    completion(error)
                    break
                }
            }
        }
    }
    
    func unlink(_ completion: @escaping (Error?) -> Void) {
        api.unlink(id: account.id, for: account.type) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(_):
                    completion(nil)
                    break
                case .failure(let error):
                    completion(error)
                    break
                }
            }
        }
    }
    
}
