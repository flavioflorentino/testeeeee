//
//  AccountsListViewModel.swift
//  PicPay
//
//  Created by Lucas Romano on 14/09/2018.
//  Copyright © 2018 PicPay. All rights reserved.
//

import Foundation

internal final class AccountsListViewModel {
    
    let api: LinkedAccountsApi = LinkedAccountsApi()
    
    var accounts: [LinkedAccount] = []
    var available: [AvailableLinkToAccount] = []
    
    func numberOfSections() -> Int {
        if !accounts.isEmpty && !available.isEmpty {
            return 2
        } else if accounts.isEmpty && !available.isEmpty {
            return 1
        } else if !accounts.isEmpty && available.isEmpty {
            return 1
        }
        return 0
    }
    
    func numberOfRows(in section: Int) -> Int {
        if !accounts.isEmpty && !available.isEmpty {
            if section == 0 {
                return accounts.count
            } else {
                return available.count
            }
        } else if !accounts.isEmpty && available.isEmpty {
            return accounts.count
        } else if accounts.isEmpty && !available.isEmpty {
            return available.count
        }
        return 0
    }
    
    func object(in indexPath: IndexPath) -> Any? {
        if !accounts.isEmpty && !available.isEmpty {
            if indexPath.section == 0 {
                return accounts[indexPath.row]
            } else {
                return available[indexPath.row]
            }
        } else if !accounts.isEmpty && available.isEmpty {
            return accounts[indexPath.row]
        } else if accounts.isEmpty && !available.isEmpty {
            return available[indexPath.row]
        }
        return nil
    }
    
    func title(for section: Int) -> String {
        if !accounts.isEmpty && !available.isEmpty {
            if section == 0 {
                return LinkedAccountsLocalizable.withLinkedAccounts.text
            } else {
                return LinkedAccountsLocalizable.noLinkAccounts.text
            }
        } else if !accounts.isEmpty && available.isEmpty {
            return LinkedAccountsLocalizable.withLinkedAccounts.text
        } else if accounts.isEmpty && !available.isEmpty {
            return LinkedAccountsLocalizable.noLinkAccounts.text
        }
        return ""
    }
    
    func loadList(_ completion: @escaping (Error?) -> Void) {
        api.list { [weak self] result in
            guard let strongSelf = self else {
            return
        }
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    strongSelf.accounts = response.accounts
                    strongSelf.available = response.available
                    completion(nil)
                    break
                case .failure(let error):
                    completion(error)
                    break
                }
            }
        }
    }
    
    func linkAuth(for object: AvailableLinkToAccount, _ completion: @escaping (URL?, Error?) -> Void) {
        api.authLink(for: object.type) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let result):
                    completion(result.link, nil)
                    break
                case .failure(let error):
                    completion(nil, error)
                    break
                }
            }
        }
    }
}
