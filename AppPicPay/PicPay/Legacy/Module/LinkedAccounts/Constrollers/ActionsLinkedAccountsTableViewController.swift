import Foundation
import UI
import UIKit

final class ActionsLinkedAccountsTableViewController: PPBaseTableViewController {

    var viewModel: ActionsLinkedAccountsViewModel!
    
    lazy private var loadingView: UIView = {
        let view = UIView(frame: self.view.bounds)
        let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale100.color
        view.addSubview(activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        activityIndicator.startAnimating()
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLayout()
    }
    
    private func configureLayout() {
        tableView.backgroundColor = Palette.ppColorGrayscale100.color
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        title = viewModel.account.name
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(in: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.numberOfSections == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: UnlinkAccountTableViewCell.identifier, for: indexPath) as? UnlinkAccountTableViewCell else {
                fatalError("not set UnlinkAccountTableViewCell")
            }
            cell.account = viewModel.account
            return cell
        } else {
            if indexPath.section == 0 {
                guard let cell = typeConfigCell(in: indexPath) else {
                    fatalError("erro on set up type type cong cell in \(String(describing: self))")
                }
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: UnlinkAccountTableViewCell.identifier, for: indexPath) as? UnlinkAccountTableViewCell else {
                    fatalError("not set UnlinkAccountTableViewCell")
                }
                cell.account = viewModel.account
                return cell
            }
        }
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if viewModel.numberOfSections() == 1 || indexPath.section == 1 {
            unlinkAccount()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 32
    }
}

// MARK: - Action notification linked account delegate

extension ActionsLinkedAccountsTableViewController: ActionNotificationLinkedAccountDelegate {
    func onTapSwitchNotification(with status: Bool, _ completion: @escaping (Error?) -> Void) {
        viewModel.onSwitchActionNotification(isSwitted: status) { [weak self] (error) in
            guard let strongSelf = self else {
            return
        }
            if let error = error {
                AlertMessage.showCustomAlertWithError(error, controller: strongSelf)
            }
            completion(error)
        }
    }
}

extension ActionsLinkedAccountsTableViewController {
    
    private func typeConfigCell(in indexPath: IndexPath) -> UITableViewCell? {
        guard let type = viewModel.getTypeConfig(in: indexPath.row) else {
     return nil
}
        switch type {
        case .notificationForPaymentReceived:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ActionNotificationLinkedAccountTableViewCell.identifier, for: indexPath) as? ActionNotificationLinkedAccountTableViewCell else {
     return nil
}
            cell.delegate = self
            cell.config = viewModel.account.config
            return cell
        }
    }

    private func unlinkAccount() {
        let alert = Alert(title: LinkedAccountsLocalizable.unlinkYourStreamlabsAccount.text, text: nil)
        let confirmButton = Button(title: LinkedAccountsLocalizable.unlink.text, type: .cta) { [weak self] popupController, _ in
            guard let strongSelf = self else {
                return
            }
            popupController.dismiss(animated: true, completion: {
                strongSelf.processesUnlinkAccount()
            })
        }
        let cancelButton = Button(title: DefaultLocalizable.btCancel.text, type: .clean, action: .close)
        alert.buttons = [confirmButton, cancelButton]
        AlertMessage.showAlert(alert, controller: self)
    }
    
    private func processesUnlinkAccount() {
        view.addSubview(loadingView)
        viewModel.unlink { [weak self] error in
            guard let strongSelf = self else {
            return
        }
            strongSelf.loadingView.removeFromSuperview()
            if let error = error {
                AlertMessage.showCustomAlertWithError(error, controller: strongSelf.navigationController)
            } else {
                strongSelf.navigationController?.popViewController(animated: true)
            }
        }
    }
}
