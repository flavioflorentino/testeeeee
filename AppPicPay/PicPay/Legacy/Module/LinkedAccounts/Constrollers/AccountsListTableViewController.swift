import UI
import UIKit
import SafariServices

final class AccountsListTableViewController: PPBaseTableViewController {

    @IBOutlet weak private var messageText: UILabel!
    @IBOutlet weak private var messageView: UIView!
    
    private let viewModel: AccountsListViewModel = AccountsListViewModel()
    
    lazy private var loadingView: UIView = {
        let view = UIView(frame: self.view.bounds)
        var indicatorStyle: UIActivityIndicatorView.Style = .gray
        if #available(iOS 12.0, *), (self.traitCollection.userInterfaceStyle == .dark) {
          indicatorStyle = .white
        }
        let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(style: indicatorStyle)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale100.color
        view.addSubview(activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        activityIndicator.startAnimating()
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = Palette.ppColorGrayscale100.color
        tableView.tableFooterView = UIView()
        messageText.textColor = Palette.ppColorGrayscale500.color
        messageView.backgroundColor = Palette.ppColorGrayscale100.color
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        /// dinamic height for header view
        if let headerView = tableView.tableHeaderView {
            let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var headerFrame = headerView.frame
            
            /// Comparison necessary to avoid infinite loop
            if height != headerFrame.size.height {
                headerFrame.size.height = height
                headerView.frame = headerFrame
                tableView.tableHeaderView = headerView
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(in: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: LinkedAccountsViewCell = tableView.dequeueReusableCell(withIdentifier: LinkedAccountsViewCell.identifier, for: indexPath) as? LinkedAccountsViewCell else {
            fatalError("not instantiete LinkedAccountsViewCell")
        }
        guard let object = viewModel.object(in: indexPath) else {
            fatalError("not object for list in AccountsListTableViewController")
        }
        cell.configureCell(for: object)
        return cell
    }
    
    /// MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        /// segue for linked account action details
        if let linkedAccount = viewModel.object(in: indexPath) as? LinkedAccount {
            performSegue(withIdentifier: SegueIdentifier.detail.rawValue, sender: linkedAccount)
        }
        /// link availaible account for user account
        if let avalaibleAccount = viewModel.object(in: indexPath) as? AvailableLinkToAccount {
            openLinkAutentication(for: avalaibleAccount)
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView(for: section)
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 67
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 14
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.detail.rawValue {
            guard let linkedAccount = sender as? LinkedAccount else {
            return
        }
            guard let controller = segue.destination as? ActionsLinkedAccountsTableViewController else {
            return
        }
            controller.viewModel = ActionsLinkedAccountsViewModel(with: linkedAccount)
        }
    }
}

extension AccountsListTableViewController {
    
    @objc private func loadData() {
        tableView.tableHeaderView?.isHidden = true
        view.addSubview(loadingView)
        viewModel.loadList { [weak self] error in
            guard let strongSelf = self else {
            return
        }
            strongSelf.loadingView.removeFromSuperview()
            strongSelf.refreshControl?.endRefreshing()
            if let error = error {
                AlertMessage.showCustomAlertWithError(error, controller: strongSelf.navigationController, completion: {
                    strongSelf.navigationController?.popToRootViewController(animated: true)
                })
            } else {
                strongSelf.tableView.tableHeaderView?.isHidden = false
                strongSelf.tableView.reloadData()
            }
        }
    }
    
    private func openLinkAutentication(for object: AvailableLinkToAccount) {
        view.addSubview(loadingView)
        viewModel.linkAuth(for: object) { [weak self] (url, error) in
            guard let strongSelf = self else {
            return
        }
            strongSelf.loadingView.removeFromSuperview()
            if let error = error {
                AlertMessage.showCustomAlertWithError(error, controller: strongSelf)
            } else {
                guard let url = url else {
            return
        }
                strongSelf.openSafariWebView(wih: url)
            }
        }
    }
    
    private func openSafariWebView(wih url: URL) {
        let controller: SFSafariViewController = SFSafariViewController(url: url)
        self.present(controller, animated: true, completion: nil)
    }
    
    private func configPullRefresh() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.loadData), for: .valueChanged)
        // Add Refresh Control to Table View
        tableView.refreshControl = refreshControl
        self.refreshControl = refreshControl
    }
    
    private func headerView(for section: Int) -> UIView {
        let view = UIView()
        let label = UILabel()
        label.text = viewModel.title(for: section)
        label.textColor = Palette.ppColorGrayscale500.color 
        label.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        NSLayoutConstraint.activate(
            [
                label.topAnchor.constraint(equalTo: view.topAnchor, constant: 18),
                label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 16),
                label.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16),
                label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            ]
        )
        return view
    }
}

extension AccountsListTableViewController {
    private enum SegueIdentifier: String {
        case detail = "detailListService"
    }
}
