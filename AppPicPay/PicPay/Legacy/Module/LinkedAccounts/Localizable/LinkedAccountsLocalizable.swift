enum LinkedAccountsLocalizable: String, Localizable {
    case unlinkYourStreamlabsAccount
    case unlink
    case youAreLogged
    case withLinkedAccounts
    case noLinkAccounts
    
    var key: String {
        return self.rawValue
    }
    
    var file: LocalizableFile {
        return .linkedAccounts
    }
}
