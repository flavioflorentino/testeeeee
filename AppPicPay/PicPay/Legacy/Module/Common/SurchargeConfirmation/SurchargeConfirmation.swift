import UI
import UIKit
import SnapKit

private final class SurchargeFeeView: UIView, ViewConfiguration {
    // MARK: - SurchargeFeeView Outlets
    
    private lazy var containerView = UIView()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .right)
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .left)
            .with(\.textColor, .branding600())
        return label
    }()
    
    // MARK: - SurchargeFeeView Initialization
    
    init(fee: (surcharge: String, surchargeAmount: String?), frame: CGRect = .zero) {
        super.init(frame: frame)
        
        descriptionLabel.text = fee.surcharge
        valueLabel.text = fee.surchargeAmount
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - SurchargeFeeView ViewConfiguration
    
    func buildViewHierarchy() {
        addSubview(containerView)
        containerView.addSubview(descriptionLabel)
        containerView.addSubview(valueLabel)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.centerX.top.bottom.equalToSuperview()
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
        }
        valueLabel.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview()
            $0.leading.equalTo(descriptionLabel.snp.trailing).offset(Spacing.base01)
        }
    }
}

final class SurchargeConfirmation: UIViewController {
    // MARK: - Outlets
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle(cornerRadius: .strong))
            .with(\.backgroundColor, .groupedBackgroundSecondary())
        
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var surchargeStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 8
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle())
        button.setTitle(BilletLocalizable.notNow.text, for: .normal)
        button.addTarget(self, action: #selector(didTapCancel), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Properties
    
    var action: (()-> Void)?
    var close: (()-> Void)?

    var shouldShowCancel = false
    
    // MARK: - Initializer
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(
        title: String?,
        description: String?,
        fees: [(surcharge: String, surchargeAmount: String?)],
        shouldShowCancel: Bool
    ) {
        titleLabel.text = title
        messageLabel.text = description
        
        fees.forEach {
            let feeView = SurchargeFeeView(fee: $0)
            surchargeStackView.addArrangedSubview(feeView)
        }
        
        self.shouldShowCancel = shouldShowCancel
        
        let actionTitle = shouldShowCancel ? BilletLocalizable.acceptAdditionalFee.text : BilletLocalizable.gotIt.text
        actionButton.setTitle(actionTitle, for: .normal)
        
        buildLayout()
    }
}

// MARK: - Actions

@objc
private extension SurchargeConfirmation {
    func didTapAction() {
        dismiss(animated: true) { [weak self] in
            self?.action?()
        }
    }
    
    func didTapCancel() {
        dismiss(animated: true) { [weak self] in
            self?.close?()
        }
    }
}

// MARK: - ViewConfiguration

extension SurchargeConfirmation: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(containerView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(messageLabel)
        containerView.addSubview(surchargeStackView)
        containerView.addSubview(actionButton)
        
        guard shouldShowCancel else {
            return
        }
        
        containerView.addSubview(cancelButton)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base04)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        messageLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        surchargeStackView.snp.makeConstraints {
            $0.top.equalTo(messageLabel.snp.bottom).offset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
        
        actionButton.snp.makeConstraints {
            $0.top.equalTo(surchargeStackView.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            
            if !shouldShowCancel {
                $0.bottom.equalToSuperview().offset(-Spacing.base03)
            }
        }
        
        guard shouldShowCancel else {
            return
        }
        
        cancelButton.snp.makeConstraints {
            $0.top.equalTo(actionButton.snp.bottom).offset(Spacing.base00)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }
    }
}
