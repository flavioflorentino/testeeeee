import UIKit

final class UpdateAppViewController: PPBaseViewController {
    // MARK: - Initializer
    init() {
        super.init(nibName: UpdateAppViewController.nibName, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - User Actions
    @IBAction private func updateApp() {
        guard let url = URL(string: "itms-apps://itunes.apple.com/app/id561524792"),
            let urlWeb = URL(string: "https://itunes.apple.com/br/developer/picpay-apps/id949456209") else {
            return
        }
        
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.open(urlWeb)
        }
    }
}
