import Foundation
import SwiftyJSON

final class TEFError: NSObject {
    
    var transactionId: String
    var pin: NSNumber
    var value: String
    var expiry: NSNumber
    
    @objc init?(error: NSError) {
        // Needs to be a picpay error && be 833333 && have all necessary params
        guard let error = error as? PicPayErrorDisplayable,
            error.picpayCode == "833333",
            let data = (error.data as? JSON)?.dictionary,
            let transactionId = data["transaction_id"]?.string,
            let pin = data["pin"]?.number,
            let value = data["value"]?.string,
            let expiry = data["expiry"]?.number
            else {
                return nil
            }
        
        self.transactionId = transactionId
        self.pin = pin
        self.value = value
        self.expiry = expiry
        super.init()
    }
}
