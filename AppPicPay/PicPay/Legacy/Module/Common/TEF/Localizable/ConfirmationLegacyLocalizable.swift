import Foundation

enum ConfirmationLegacyLocalizable: String, Localizable {
    case paymentConfirmed
    case viewReceipt
    case yourCodeHasExpired
    case chargeWillBeCanceled
    case cancelPayment
    case pinCanceled
    
    var file: LocalizableFile {
        return .confirmationLegacy
    }
    
    var key: String {
        return self.rawValue
    }
}
