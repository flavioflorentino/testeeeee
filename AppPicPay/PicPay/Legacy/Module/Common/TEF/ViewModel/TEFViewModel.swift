//
//  TEFViewModel.swift
//  PicPay
//
//  Created by Marcos Timm on 27/12/2017.
//

import Foundation

final class TEFViewModel:NSObject {

    var apiTEF : ApiTEF
    var widgets: [ReceiptWidgetItem] = []

    var isComplete = false
    var isCanceled = false
    
    init(apiTef: ApiTEF = ApiTEF()){
        self.apiTEF = apiTef
    }
    
    func checkTransaction(transaction_id: String, _ completion: @escaping ((PicPayErrorDisplayable?) -> Void) ) {

        apiTEF.checkTransaction(transaction_id: transaction_id) { [weak self] (result) in
            guard let strongSelf = self else {
            return
        }
            
            DispatchQueue.main.async {
                switch result {
                case .success(let value):
                    strongSelf.widgets = value.widgets
                    strongSelf.isComplete = true
                    completion(nil)
                    break
                case .failure(let error):
                    completion(error)
                    break
                }
            }
        }
    }
    
    func cancelTefTransaction(transaction_id: String, _ completion: @escaping ((Error?) -> Void) ) {
        apiTEF.cancelTransaction(transaction_id: transaction_id) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(_):
                    completion(nil)
                    break
                case .failure(let error):
                    completion(error)
                    break
                }
            }
        }
    }

}
