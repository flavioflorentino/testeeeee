import Core
import FeatureFlag
import SwiftyJSON

final class ApiTEF: BaseApi {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies = DependencyContainer()
    
    func checkTransaction(transaction_id: String, _ completion: @escaping ((PicPayResult<TransactionResponse>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonTEFToCore) else {
            let params = ["transaction_id": transaction_id]
            let endpoint = WebServiceInterface.apiEndpoint(kWsUrlTEFCheckTransaction)
            requestManager
                .apiRequest(endpoint: endpoint ?? "", method: .post, parameters: params)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        let endpoint = TEFTransactionEndpoints.check(id: transaction_id)
        Api<Data>(endpoint: endpoint).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }

    func cancelTransaction(transaction_id: String, _ completion: @escaping ((PicPayResult<GenericResponse>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonTEFToCore) else {
            let params = ["transaction_id": transaction_id]
            let endpoint = WebServiceInterface.apiEndpoint(kWsUrlTEFCancelTransaction)
            requestManager
                .apiRequest(endpoint: endpoint ?? "", method: .post, parameters: params)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        let endpoint = TEFTransactionEndpoints.cancel(id: transaction_id)
        Api<Data>(endpoint: endpoint).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
}

extension ApiTEF {
    final class TransactionResponse: BaseApiResponse {
        var widgets: [ReceiptWidgetItem] = []

        required init? (json: JSON) {
            guard let dic = json.dictionary,
                let data = dic["data"]?.dictionary,
                let json = data["receipt"]
                else {
                 return nil
            }
            
            widgets.append(contentsOf: WSReceipt.createReceiptWidgetItem(json))
        }
    }
    
    final class GenericResponse: BaseApiResponse {
        required init(json: JSON) {}
    }

}
