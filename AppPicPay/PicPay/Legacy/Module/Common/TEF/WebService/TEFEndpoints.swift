import Core
import Foundation

enum TEFTransactionEndpoints: ApiEndpointExposable {
    case check(id: String)
    case cancel(id: String)
    
    var path: String {
        switch self {
        case .check:
            return "api/checkTransaction.json"
        case .cancel:
            return "api/cancelTransaction.json"
        }
    }
    
    var method: HTTPMethod {
        return .post
    }
    
    var body: Data? {
        switch self {
        case let .check(id):
            return ["transaction_id": id].toData()
        case let .cancel(id):
            return ["transaction_id": id].toData()
        }
    }
}
