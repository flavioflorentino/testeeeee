import Foundation
import UI
import UIKit

final class ConfirmationCodeViewController: UIViewController {
    @IBOutlet weak var confirmationCode: UILabel!
    @IBOutlet weak var countDownLabel: UILabel!
    @IBOutlet weak var attemptionView: GradientView!
    @IBOutlet weak var stackCountdown: UIStackView!
    @IBOutlet weak var storeLabel: UILabel!
    @IBOutlet weak var storeLogo: UICircularImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var iconDenied: UIImageView!
    @IBOutlet weak var iconSuccess: UIImageView!
    @IBOutlet weak var actionButton: UIPPButton!
    
    var model: TEFViewModel = TEFViewModel()
    
    private var tef: TEFError?
    private var store: PPStore?
    private var didUseCreditCard = false
    private var shouldShowReceiptOnComplete = true
    
    var seccondsLeft = 400 // 40 min default
    var totalOfTime = 400
    var timer: RepeatingTimer? = RepeatingTimer(timeInterval: 1)
    var startTime: Date?
    
    @objc var dismissBlock: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        attemptionView.startPoint = CGPoint(x: 0.5, y: 0.0)
        attemptionView.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        let width = min(UIScreen.main.bounds.size.width - 40, 360)
        self.view.frame.size.width = width
        
        descriptionLabel.isHidden = true
        
        if let expiry = tef?.expiry {
            totalOfTime = Int(truncating: expiry)
        }
        
        if let pin = tef?.pin {
            confirmationCode.text = "\(pin)"
        }
        if let value = tef?.value {
            valueLabel.text = value
        }
        if let name = store?.name {
            storeLabel.text = name
        }
        let url = URL(string: store?.img_url ?? "")
        storeLogo.setImage(url: url, placeholder: PPStore.photoPlaceholder())
        countDownLabel.text = "\(timeString(time: TimeInterval(totalOfTime)))"
        
        
        let backgroundView = UIView(frame: CGRect(x: 0, y: 32, width: view.frame.size.width, height: view.frame.size.height - 32))
        backgroundView.backgroundColor = Palette.ppColorGrayscale000.color
        backgroundView.layer.cornerRadius = 10
        backgroundView.clipsToBounds = true
        view.insertSubview(backgroundView, belowSubview: storeLabel)
        
        runCountDown()
        checkTransactionStatus()
    }
    
    @objc init(error: TEFError, andStore: PPStore, didUseCreditCard: Bool) {
        self.tef = error
        self.store = andStore
        self.didUseCreditCard = didUseCreditCard
        super.init(nibName: "ConfirmationCodeViewController", bundle: nil)
    }
    
    deinit {
        timer?.eventHandler = nil
        timer?.suspend()
        timer = nil
    }
    
    func runCountDown() {
        startTime = Date()
        timer?.eventHandler = { [weak self] in
            self?.updateTimer()
        }
        timer?.resume()
    }
    
    func updateTimer() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self, let beginDate = self.startTime else {
                return
            }
            
            let diferentTime = Int(Date().timeIntervalSince(beginDate))
            self.seccondsLeft = self.totalOfTime - diferentTime
            if self.seccondsLeft <= 0 {
                self.timer?.suspend()
                self.codeHasExpired()
                return
            }
            self.countDownLabel.text = "\(self.timeString(time: TimeInterval(self.seccondsLeft)))"
        }
    }
    
    func transactionComplete() {
        timer?.suspend()
        timer?.eventHandler = nil
        
        stackCountdown.isHidden = true
        confirmationCode.isHidden = true
        
        descriptionLabel.text = ConfirmationLegacyLocalizable.paymentConfirmed.text
        descriptionLabel.isHidden = false
        iconDenied.isHidden = true
        iconSuccess.isHidden = false
        
        actionButton.setTitle(ConfirmationLegacyLocalizable.viewReceipt.text, for: .normal)
    }
    
    func codeHasExpired() {
        
        stackCountdown.isHidden = true
        confirmationCode.isHidden = true
        attemptionView.animateTo(startColor: UIColor(red:1.00, green:0.33, blue:0.47, alpha:1.00), endColor: UIColor(red:0.89, green:0.09, blue:0.26, alpha:1.00))
        
        descriptionLabel.text = ConfirmationLegacyLocalizable.yourCodeHasExpired.text
        descriptionLabel.isHidden = false
        iconDenied.isHidden = false
        iconSuccess.isHidden = true
        
        actionButton.setTitle(DefaultLocalizable.btClose.text, for: .normal)
    }
    
    func codeHasCanceled(message: String) {
        
        stackCountdown.isHidden = true
        confirmationCode.isHidden = true
        attemptionView.animateTo(startColor: UIColor(red:1.00, green:0.33, blue:0.47, alpha:1.00), endColor: UIColor(red:0.89, green:0.09, blue:0.26, alpha:1.00))
        
        descriptionLabel.text = message
        descriptionLabel.isHidden = false
        iconDenied.isHidden = false
        iconSuccess.isHidden = true
        
        actionButton.setTitle(DefaultLocalizable.btClose.text, for: .normal)
    }
    
    func timeString(time:TimeInterval) -> String {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction private func buttonAction(_ sender: Any) {
        // Wainting for payment confirmation - Cancel payment action
        if seccondsLeft > 0 && !model.isComplete && !model.isCanceled {
            let message = didUseCreditCard ? ConfirmationLegacyLocalizable.chargeWillBeCanceled.text : ""
            let alert = Alert(title: ConfirmationLegacyLocalizable.cancelPayment.text, text: message)
            
            let buttonNo = Button(title: DefaultLocalizable.iWantContinue.text, type: .cta, action: .close)
            let buttonYes = Button(title: DefaultLocalizable.iWantCancel.text, type: .destructive) { popupController, _ in
                popupController.dismiss(animated: true, completion: { [weak self] in
                    guard let self = self, let transactionId = self.tef?.transactionId else {
                        return
                    }
                    self.cancelTefTransaction(transaction_id: transactionId)
                })
            }
            alert.buttons = [buttonYes, buttonNo]
            
            AlertMessage.showAlert(alert, controller: navigationController)
            return
        }
        // Payment success - Show receipt
        if model.isComplete {
            NotificationCenter.default.post(name: Notification.Name.Payment.new, object: nil, userInfo: ["type": "TEF"])
            
            guard let transactionId = self.tef?.transactionId else {
                return
            }
            
            showReceipt(transaction_id: transactionId, widgets: self.model.widgets)
            return
        }
        // Code expired - Close action
        sendUpdateFeedNotification()
        dismiss(animated: true) { [weak self] in
            self?.dismissBlock?()
        }
    }
    
    func checkTransactionStatus() {
        
        model.checkTransaction(transaction_id: (self.tef?.transactionId)!, { [weak self] (error) in
            guard let strongSelf = self else {
                return
            }
            
            if error != nil {
                if error?.picpayCode == "8333332" {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                        strongSelf.codeHasCanceled(message: error?.localizedDescription ?? ConfirmationLegacyLocalizable.pinCanceled.text)
                    })
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                        strongSelf.checkTransactionStatus()
                    })
                }
            } else {
                
                strongSelf.transactionComplete()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    strongSelf.showReceipt(transaction_id: (self?.tef?.transactionId)!, widgets: strongSelf.model.widgets)
                })
            }
        })
        
    }
    
    func showReceipt(transaction_id: String, widgets: [ReceiptWidgetItem]) {
        guard shouldShowReceiptOnComplete else {
            return
        }
        
        shouldShowReceiptOnComplete = false
        let receiptModel = ReceiptWidgetViewModel(transactionId: transaction_id, type: .PAV)
        
        if widgets.count > 0 {
            receiptModel.setReceiptWidgets(items: widgets)
        }
        
        guard let viewController = self.presentingViewController else {
            return
        }
        
        TransactionReceipt.showReceiptSuccess(viewController: viewController, receiptViewModel: receiptModel)
    }
    
    func cancelTefTransaction(transaction_id: String) {
        actionButton.startLoadingAnimating(style: .gray)
        model.cancelTefTransaction(transaction_id: transaction_id) { [weak self] (error) in
            self?.actionButton.stopLoadingAnimating()
            if error != nil {
                let alertView = UIAlertController(title: "", message: error?.localizedDescription, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertView.addAction(action)
                self?.present(alertView, animated: true, completion: nil)
            }
            self?.sendUpdateFeedNotification()
            self?.dismiss(animated: true, completion: {
                self?.dismissBlock?()
            })
        }
    }
    
    private func sendUpdateFeedNotification() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: FeedViewController.Observables.FeedNeedReload.rawValue), object: nil)
    }
    
}
