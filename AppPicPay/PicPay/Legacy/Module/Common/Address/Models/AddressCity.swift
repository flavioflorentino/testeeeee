//
//  AddressCity.swift
//  PicPay
//
//  Created by Marcos Timm on 11/05/2018.
//

import UIKit
import SwiftyJSON

struct AddressCity: BaseApiResponse {
    var id: Int
    var abbreviation: String
    var name: String
    var type: String
    
    init?(json:JSON) {
        guard let id = json["id"].int else {
     return nil
}
        guard let abbreviation = json["abbreviation"].string  else {
     return nil
}
        guard let name = json["name"].string else {
     return nil
}
        guard let type = json["type"].string else {
     return nil
}
        self.id = id
        self.abbreviation = abbreviation
        self.name = name
        self.type = type
    }
}
