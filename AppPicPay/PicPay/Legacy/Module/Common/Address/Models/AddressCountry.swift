//
//  AddressCountry.swift
//  PicPay
//
//  Created by Marcos Timm on 11/05/2018.
//

import UIKit
import SwiftyJSON

struct AddressCountry: BaseApiResponse {
    let id: Int
    let initials: String
    let name: String
    
    init?(json:JSON) {
        guard let id = json["id"].int else {
     return nil
}
        guard let initials = json["initials"].string else {
     return nil
}
        guard let name = json["name"].string else {
     return nil
}
        self.id = id
        self.initials = initials
        self.name = name
    }
}
