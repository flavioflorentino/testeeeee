import Foundation

struct AddressSearch: Codable, Equatable {
    var zipCode: String?
    var addressComplement: String?
    var city: String?
    var cityId: String?
    var neighborhood: String?
    var state: String?
    var stateId: String?
    var streetName: String?
    var streetNumber: String?
    var streetType: String?
    var street: String?
}
