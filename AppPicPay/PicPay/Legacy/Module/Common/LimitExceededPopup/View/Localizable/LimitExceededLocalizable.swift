import Foundation

enum LimitExceededLocalizable: String, Localizable {
    case payAdditionalFee
    case waitForRecipient
    case creditCardReceiptLimit
    case additionalFee
    case additionalFeeTitle
    case additionalFeeValue
    case additionalFeeCard
    case additionalFeeDetailValue 
    case additionalFeeDetailTax
    case additionalFeeDetailTotal
    
    var file: LocalizableFile {
        return .limitExceeded
    }
    
    var key: String {
        return self.rawValue
    }
}
