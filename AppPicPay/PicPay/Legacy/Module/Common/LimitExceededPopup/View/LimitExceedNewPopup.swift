import AssetsKit
import Foundation
import SnapKit
import UI
import UIKit

private extension LimitExceedNewPopup.Layout {
    enum Size {
        static let image: CGSize = CGSize(width: 136, height: 95)
    }
    
    enum AttributedText {
        static let lineHeight: CGFloat = 1.26
    }
}

@objc final class LimitExceedNewPopup: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Resources.Icons.icoPopupMoneyWallet.image
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
        label.text = LimitExceededLocalizable.additionalFeeTitle.text
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        return label
    }()
    
    private lazy var mainStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = .fillProportionally
        return view
    }()
    
    private lazy var titleStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .leading
        return view
    }()
    
    private lazy var valueStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .trailing
        return view
    }()
    
    private lazy var valueChangedTitle: UILabel = {
        let label = UILabel()
        label.text = LimitExceededLocalizable.additionalFeeDetailValue.text
        label.labelStyle(CaptionLabelStyle(type: .highlight))
        return label
    }()
    
    private lazy var addedTaxesTitle: UILabel = {
        let label = UILabel()
        label.text = LimitExceededLocalizable.additionalFeeDetailTax.text
        label.labelStyle(CaptionLabelStyle(type: .highlight))
        return label
    }()
    
    private lazy var totalValueTitle: UILabel = {
        let label = UILabel()
        label.text = LimitExceededLocalizable.additionalFeeDetailTotal.text
        label.labelStyle(CaptionLabelStyle(type: .highlight))
        return label
    }()
    
    private lazy var valueChangedText: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle(type: .highlight))
            .with(\.textColor, .branding600())
        return label
    }()
    
    private lazy var addedTaxesText: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle(type: .highlight))
            .with(\.textColor, .branding600())
        return label
    }()
    
    private lazy var totalValueText: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle(type: .highlight))
            .with(\.textColor, .branding600())
        return label
    }()
    
    @objc
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @objc
    convenience init(limitError: LimitExceededError) {
        self.init() 
        self.descriptionLabel.text = limitError.text
        self.valueChangedText.text = CurrencyFormatter.brazillianRealString(from: limitError.amountExceeded)
        self.addedTaxesText.text = CurrencyFormatter.brazillianRealString(from: limitError.surcharge)
        self.totalValueText.text = CurrencyFormatter.brazillianRealString(from: limitError.totalValue)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(imageView,
                    titleLabel,
                    descriptionLabel,
                    mainStackView)
        mainStackView.addArrangedSubviews(titleStackView,
                                          valueStackView)
        titleStackView.addArrangedSubviews(valueChangedTitle,
                                           addedTaxesTitle,
                                           totalValueTitle)
        valueStackView.addArrangedSubviews(valueChangedText,
                                           addedTaxesText,
                                           totalValueText)
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.image)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
        
        mainStackView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalTo(descriptionLabel)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }
    }
    
    func configureViews() {
        configureBoldTextForDescriptionLabel()
    }
    
    private func configureBoldTextForDescriptionLabel() {
        let text = NSString(string: descriptionLabel.text ?? "")
        let paragraphStyle = NSMutableParagraphStyle()
        
        paragraphStyle.alignment = .center
        paragraphStyle.lineHeightMultiple = Layout.AttributedText.lineHeight
        
        let attributedText = NSMutableAttributedString(
            string: text as String,
            attributes: [.paragraphStyle: paragraphStyle]
        )
        
        let highlightedRanges = [
            text.range(of: LimitExceededLocalizable.additionalFeeValue.text),
            text.range(of: LimitExceededLocalizable.additionalFeeCard.text)
        ]
        
        highlightedRanges.forEach {
            attributedText.addAttribute(.font, value: Typography.bodySecondary(.highlight).font(), range: $0)
        }
        
        descriptionLabel.attributedText = attributedText
    }
}
