import AnalyticsModule
import CoreLegacy
import UIKit

@objc final class LimitExceededPopup: PPBaseViewControllerObj {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var actionButton: UIPPButton!
    @IBOutlet weak var waitPicPayProButton: UIPPButton!
    @IBOutlet weak var exceededLabel: UILabel!
    @IBOutlet weak var surchargeLabel: UILabel!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var extraFeeLabel: UILabel!
    
    @objc var buttonTapAction: (() -> Void)?
    @objc var didClose: (() -> Void)?
    @objc var waitButtonTapAction: ((LimitExceededError) -> Void)?
    
    private var error: LimitExceededError
    private var dependencyContainer = DependencyContainer()
    private var buttonWasClicked: Bool = false
    
    override var shouldAddBreadcrumb: Bool {
        set(newValue) {
            self.shouldAddBreadcrumb = newValue
        }
        get {
            return false
        }
    }
    
    // MARK: - Initializer
    
    @objc init(error: LimitExceededError){
        self.error = error
        super.init(nibName: "LimitExceededPopup", bundle: Bundle.main)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let buttonTitle: String = LimitExceededLocalizable.payAdditionalFee.text
        let waitButtonTitle: String = LimitExceededLocalizable.waitForRecipient.text
        let exceeded = CurrencyFormatter.brazillianRealString(from: error.amountExceeded)
        let surcharge = CurrencyFormatter.brazillianRealString(from: error.surcharge)
        
        waitPicPayProButton.setTitle(waitButtonTitle, for: .normal)
        waitPicPayProButton.titleLabel?.numberOfLines = 0
        waitPicPayProButton.titleLabel?.lineBreakMode = .byWordWrapping
        waitPicPayProButton.titleLabel?.textAlignment = .center
        waitPicPayProButton.sizeToFit()
        
        if self.error.codeOfError == "1340" {
            titleLabel.text = LimitExceededLocalizable.creditCardReceiptLimit.text
        }
        
        textLabel.text = error.text
        exceededLabel.text = exceeded
        surchargeLabel.text = surcharge
        extraFeeLabel.text = "\(LimitExceededLocalizable.additionalFee.text) (\(error.extraFeeStr)%)"
        actionButton.setTitle(buttonTitle, for: .normal)
        container.layer.cornerRadius = 8
        
        /// show or not wait buttton
        waitPicPayProButton.isHidden = self.error.codeOfError != "1340"
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(checkButtonClickForTracking),
            name: UIApplication.willTerminateNotification,
            object: nil
        )
    }
    
    private func logTransactionExceededLimit(buttonClicked: Bool, selectedField: ExceededPopupField, action: ExceededPopupAction) {
        dependencyContainer.analytics.log(
            PaymentEvent.transactionExceededLimit(
                buttonClicked: buttonClicked,
                selectedField: selectedField,
                action: action
            )
        )
    }
    
    @objc
    private func checkButtonClickForTracking() {
        logTransactionExceededLimit(buttonClicked: false, selectedField: .none, action: .none)
    }
    
    // MARK: - User Actions
    
    @IBAction private func payAction(_ sender: Any) {
        logTransactionExceededLimit(buttonClicked: true, selectedField: .button, action: .acceptFee)
        buttonWasClicked = true
        buttonTapAction?()
    }
    
    @IBAction private func close(_ sender: Any) {
        logTransactionExceededLimit(buttonClicked: true, selectedField: .button, action: .close)
        buttonWasClicked = true
        dismiss(animated: false)
        didClose?()
    }
    
    @IBAction private func waitButtonTapped(_ sender: Any) {
        logTransactionExceededLimit(buttonClicked: true, selectedField: .button, action: .notNow)
        buttonWasClicked = true
        /// do not execute this block if code is different of 1340
        guard error.codeOfError == "1340" else {
            return
        }
        waitButtonTapAction?(self.error)
    }
}
