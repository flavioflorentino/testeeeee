import Foundation
import SwiftyJSON

@objc final class LimitExceededError: NSObject {
    
    @objc let amountExceeded: NSNumber
    @objc let surcharge: NSNumber
    @objc let totalValue: NSNumber
    @objc let extraFee: NSNumber
    @objc let extraFeeStr: String
    @objc let text: String
    @objc let codeOfError: String
    @objc var payload: [String: Any] = [:]
    
    @objc init?(error: NSError) {
        /// Needs to be a picpay error && be 1338 && have all necessary params
        guard let error = error as? PicPayErrorDisplayable, (error.picpayCode == "1338" || error.picpayCode == "1340") else {
            return nil
        }
        guard let wrapper = (error.data as? JSON)?["surcharge"].dictionary else {
            return nil
        }
        guard let amountExceeded = wrapper["amount_exceeded"]?.number else {
            return nil
        }
        guard let surcharge = wrapper["surcharge"]?.number else {
            return nil
        }
        guard let totalValue = wrapper["total_value"]?.number else {
            return nil
        }
        guard let extraFee = wrapper["variable_fee"]?.number else {
            return nil
        }
        guard let extraFeeStr = wrapper["variable_fee_str"]?.string else {
            return nil
        }
        
        if (error.picpayCode == "1340") {
            self.text = (error.data as? JSON)?["message"].string ?? error.message
        }
        else{
            self.text = error.message
        }
        /// payload for send notification for change to picpay pro
        if let payload = (error.data as? JSON)?["payload"].dictionaryObject {
            self.payload = payload
        }
        
        self.codeOfError = error.picpayCode
        self.amountExceeded = amountExceeded
        self.surcharge = surcharge
        self.totalValue = totalValue
        self.extraFee = extraFee
        self.extraFeeStr = extraFeeStr
        super.init()
    }
}
