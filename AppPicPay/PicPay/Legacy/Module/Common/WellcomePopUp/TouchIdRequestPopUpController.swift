import UI
import UIKit

final class TouchIdRequestPopUpController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var actionButton: UIPPButton!
    @IBOutlet weak var notNowButton: UIPPButton!
    @IBOutlet weak var imageAuthImageView: UIImageView!
    
    @objc var buttonTapAction:(()-> Void)?
    @objc var closeTapAction:(()-> Void)?
    
    private let biometric = BiometricTypeAuth()
    
    init() {
        super.init(nibName: "TouchIdRequestPopUp", bundle: Bundle.main)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupColors()
        
        switch biometric.biometricType() {
            case BiometricTypeAuth.BiometricType.touchID:
                titleLabel.text = Strings.WelcomePopUp.touchTitle
                textLabel.text = Strings.WelcomePopUp.touchDescription
                actionButton.setTitle(Strings.WelcomePopUp.touchConfirmation, for: .normal)
                imageAuthImageView.image = UIImage(named: "touchIdFinger.png")
                break
            
            case BiometricTypeAuth.BiometricType.faceID:
                titleLabel.text = Strings.WelcomePopUp.faceTitle
                textLabel.text = Strings.WelcomePopUp.faceDescription
                actionButton.setTitle(Strings.WelcomePopUp.faceConfirmation, for: .normal)
                imageAuthImageView.image = UIImage(named: "faceID")
                break
            
            default:
                break
        }
    }
    
    func setupColors() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        textLabel.textColor = Palette.ppColorGrayscale400.color
        notNowButton.setTitleColor(Palette.ppColorGrayscale400.color)
    }
    
    // MARK: - User Actions
    @IBAction private func enableTouchId(_ sender: Any) {
        buttonTapAction?()
    }
    
    @IBAction private func close(_ sender: Any) {
        closeTapAction?()
    }
}
