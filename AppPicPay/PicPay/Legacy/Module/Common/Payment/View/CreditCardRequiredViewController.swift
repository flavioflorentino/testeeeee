import UIKit
import FeatureFlag

protocol CreditCardRequiredDelegate: AnyObject {
    func openCreditCardForm();
    func dismissPopup();
}

final class CreditCardRequiredViewController: UIViewController {
    var creditCardDelegate: CreditCardRequiredDelegate?
    var shouldFocusParent: Bool = false
    
    @IBOutlet weak var textLabel: UILabel!
    var text: String?
    
    init(){
        super.init(nibName: "CreditCardRequiredViewController", bundle: Bundle.main)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textLabel.text = text ?? FeatureManager.text(.creditCardOBPopupInsertText)
    }
    
    // MARK: - User Actions
    @IBAction private func addCardAction(_ sender: Any) {
        parent?.view.endEditing(true)
        self.dismiss(animated: true) { [weak self] in
            self?.creditCardDelegate?.openCreditCardForm()
        }
    }
    
    @IBAction private func dismissAction(_ sender: Any) {
        shouldFocusParent = true
        self.dismiss(animated: true) { [weak self] in
            self?.creditCardDelegate?.dismissPopup()
        }
    }
}
