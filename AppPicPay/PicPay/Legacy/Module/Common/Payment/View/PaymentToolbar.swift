import UI
import UIKit

@objc protocol PaymentToolbarDelegate: AnyObject {
    @objc
    func paymentToolbarDidTouchInPayButton(toolbar: PaymentToolbar)
    @objc
    func paymentToolbarShowPaymentMethodSelection()
    @objc
    func paymentToolbarDidTouchInPrivacydButton(toolbar: PaymentToolbar)
}
protocol TrackingPaymentToolbarDelegate: AnyObject {
    func payPressInButton()
    func paymentMethodSelection()
}
final class PaymentToolbar: NibView {
    var bottomBarBackgroundColor: UIColor? {
        return self.bottomBar.backgroundColor
    }

    weak var delegate: PaymentToolbarDelegate?
    weak var trackingDelegate:TrackingPaymentToolbarDelegate?
    @IBOutlet weak var paymentMethodButton: UIButton!
    @IBOutlet weak var paymentButton: UIPPButton!
    @IBOutlet weak var privacyLabel: UILabel!
    @IBOutlet weak var privacyImage: UIImageView!
    @IBOutlet weak var paymentMethodLabel: UILabel!
    @IBOutlet weak var paymentMethodCenterYContraint: NSLayoutConstraint!
    @IBOutlet weak var secondaryImageHeight: NSLayoutConstraint!
    @IBOutlet weak var mainPaymentMethodImage: UIImageView!
    @IBOutlet weak var secondaryPaymentMethodImage: UIImageView!
    @IBOutlet weak var bottomBar: UIView!
    @IBOutlet weak var descriptionStackView: UIStackView!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelForPaymentMethodLabel: UILabel!

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    // MARK: - User Actions

    @IBAction private func pay(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name.Payment.tapPayButton, object: nil)
        delegate?.paymentToolbarDidTouchInPayButton(toolbar: self)
        trackingDelegate?.payPressInButton()
    }

    @IBAction private func changePaymentMethod(_ sender: Any) {
        delegate?.paymentToolbarShowPaymentMethodSelection()
        trackingDelegate?.paymentMethodSelection()
    }

    @objc
    func changePrivacy() {
        delegate?.paymentToolbarDidTouchInPrivacydButton(toolbar: self)
    }
    
    // MARK: - Public Methods
    
    func setPayButtonTitle(_ title: String) {
        paymentButton.setTitle(title, for: .normal)
    }

    // MARK: - Internal Methods

    fileprivate func setup() {
        bottomBar.backgroundColor = Palette.ppColorGrayscale100.color
        privacyLabel.isUserInteractionEnabled = true
        privacyLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(changePrivacy)))
        privacyImage.isUserInteractionEnabled = true
        privacyImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(changePrivacy)))

        view.backgroundColor = Palette.ppColorGrayscale000.color

        paymentMethodButton.layer.cornerRadius = paymentMethodButton.frame.size.height / 2
        secondaryPaymentMethodImage.layer.cornerRadius = secondaryImageHeight.constant / 2
        secondaryPaymentMethodImage.layer.borderColor = Palette.ppColorGrayscale000.cgColor
        secondaryPaymentMethodImage.layer.borderWidth = 2
        secondaryPaymentMethodImage.isHidden = true

        let startColor = Palette.ppColorBrandingPlus300.gradientColor.from
        let endColor = Palette.ppColorBrandingPlus300.gradientColor.to
        paymentButton.applyGradient(withColours: [startColor, endColor], gradientOrientation: .horizontal)
        paymentButton.clipsToBounds = true
    }

    func setPaymentMethod(mainImageUrl: String? = nil, mainImage: UIImage? = nil, secondaryImage: UIImage? = nil, text: String) {
        mainPaymentMethodImage.isHidden = false
        labelForPaymentMethodLabel.text = "Forma de pagamento"
        paymentMethodLabel.text = "Saldo PicPay"
        loadingActivityIndicator.isHidden = true
        paymentMethodButton.isEnabled = true

        // Reset images
        secondaryPaymentMethodImage.isHidden = true
        mainPaymentMethodImage.sd_cancelCurrentImageLoad()
        mainPaymentMethodImage.contentMode = .scaleAspectFit

        // Update text
        paymentMethodLabel.text = text

        // Update main image
        if let mainImage = mainImage {
            mainPaymentMethodImage.image = mainImage
        }

        if let mainImageUrl = mainImageUrl, let url = URL(string: mainImageUrl) {
            mainPaymentMethodImage.sd_setImage(with: url)
        }

        // Update secondary image
        if let secondaryImage = secondaryImage {
            secondaryPaymentMethodImage.isHidden = false
            paymentMethodCenterYContraint.constant = -5
            secondaryPaymentMethodImage.image = secondaryImage
            return
        }

        paymentMethodCenterYContraint.constant = 0
        secondaryPaymentMethodImage.image = nil
    }

    func setPaymentMethodForNotLoadedCardList() {
        mainPaymentMethodImage.isHidden = true
        labelForPaymentMethodLabel.text = "Carregando cartões ..."
        paymentMethodLabel.text = "-"
        loadingActivityIndicator.isHidden = false
        paymentMethodButton.isEnabled = false
    }
}
