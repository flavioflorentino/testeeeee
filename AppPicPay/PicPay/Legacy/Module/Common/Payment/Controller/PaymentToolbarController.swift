import UIKit
import AMPopTip
import UI

final class PaymentToolbarController: NSObject {
    
    var bottomBarBackgroundColor: UIColor? {
        get {
            return self.toolbar.bottomBarBackgroundColor
        }
    }
    
    // MARK: Private set attributes
    
    @objc fileprivate(set) var toolbar: PaymentToolbar
    fileprivate(set) var privacyConfig: FeedItemVisibility
    
    // MARK: Public attributes
    
    typealias PayActionBlock = ((_ privacyConfig: String) -> Void)
    typealias PayMethodBlock = (() -> Void)
    var changeMethod: PayMethodBlock?
    @objc var payAction: PayActionBlock?
    @objc var origin: String = ""
    @objc var shouldFocus: Bool = false
    @objc var canUseBalance: Bool = true
    @objc var paymentMethodHeaderText: String? = nil
    @objc weak var parent: UIViewController?
    @objc var paymentManager: PPPaymentManager? = nil {
        willSet {
            self.paymentManager?.removeObserver(self, forKeyPath: #keyPath(PPPaymentManager.subtotal))
        }
        didSet {
            // Add observer to checkvalue changes
            self.paymentManager?.addObserver(self, forKeyPath: #keyPath(PPPaymentManager.subtotal), options: [.new,.initial], context: nil)
            self.updateViews()
        }
    }

    private let paymentToolbarType: PaymentToolbarType

    let popupPresenter = PopupPresenter()
    // MARK: - Initializer
    
    @objc init(
        paymentManager: PPPaymentManager,
        parent: UIViewController,
        frame: CGRect = CGRect.zero,
        pay: PayActionBlock? = nil
    ) {
        self.payAction = pay
        self.privacyConfig = FeedItemVisibility(rawValue: "\(ConsumerManager.shared.privacyConfig)") ?? FeedItemVisibility.Friends
        self.toolbar = PaymentToolbar(frame: frame)
        self.parent = parent
        self.paymentToolbarType = .default

        super.init()
        
        defer {
            updatePaymentManger(paymentManager)
        }
        
        setup()
    }
    
    init(paymentManager: PPPaymentManager,
         parent: UIViewController,
         toolbar: PaymentToolbar,
         privacyConfig: FeedItemVisibility? = nil,
         hasPrivacyView: Bool = true,
         pay: PayActionBlock? = nil,
         changeMethod: PayMethodBlock? = nil
    ) {
        self.payAction = pay
        self.toolbar = toolbar
        self.parent = parent
        self.changeMethod = changeMethod
        self.paymentToolbarType = .default
        
        if let privacy = privacyConfig {
            self.privacyConfig = privacy
        } else {
            self.privacyConfig = FeedItemVisibility(rawValue: "\(ConsumerManager.shared.privacyConfig)") ?? FeedItemVisibility.Friends
        }
        toolbar.privacyImage.isHidden = !hasPrivacyView
        toolbar.privacyLabel.isHidden = !hasPrivacyView
        super.init()
        
        defer {
            updatePaymentManger(paymentManager)
        }
        
        setup()
    }
    
    init(parent: UIViewController, toolbar: PaymentToolbar, paymentToolbarType: PaymentToolbarType = .default) {
        self.toolbar = toolbar
        self.parent = parent
        self.privacyConfig = FeedItemVisibility(rawValue: "\(ConsumerManager.shared.privacyConfig)") ?? FeedItemVisibility.Friends
        self.paymentToolbarType = paymentToolbarType

        super.init()
        setup()
    }
    
    deinit {
        self.paymentManager?.removeObserver(self, forKeyPath: #keyPath(PPPaymentManager.subtotal))
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Setup
    
    fileprivate func setup(){
        updateViews()
        toolbar.delegate = self
        NotificationCenter.default.addObserver(forName: Notification.Name.Payment.methodChange, object: nil, queue: nil) { [weak self] (_) in
            DispatchQueue.main.async {
                self?.updateViews()
            }
        }
        //NotificationCenter.default.addObserver(self, selector: #selector(updateViews), name: NSNotification.Name("PaymentMethodChange"), object: nil)
        
        // Reload the user balance every time to avoid a balance inconsistent state. It was detected that
        // canceling a payment in progress (already being processed) and trying to pay it again can lead to this state.
        ConsumerManager.shared.loadConsumerBalance()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateViews), name: NSNotification.Name("ccListLoaded"), object: nil)
    }
    
    // MARK: - Views Updates
    
    fileprivate func updatePaymentManger(_ paymentManager: PPPaymentManager){
        self.paymentManager = paymentManager
    }
    
    fileprivate func getPaymentMethodText(usePPBalance: Bool, secondaryMethod: Bool) -> String {
        guard let defaultCreditCard: CardBank = CreditCardManager.shared.defaultCreditCard else {
            var text = ""
            if usePPBalance || secondaryMethod {
                text = PaymentLegacyLocalizable.balance.text
            }
            if usePPBalance && secondaryMethod {
                text += PaymentLegacyLocalizable.moreCard.text
            }
            if !usePPBalance{
                text = PaymentLegacyLocalizable.creditCard.text
            }
            
            return text
        }
        let lastNumbers: String = defaultCreditCard.lastDigits
        var text = ""
        
        if defaultCreditCard.creditCardBandeiraId == "\(CreditCardFlagTypeId.picpay.rawValue)" {
            text = CreditPicPayLocalizable.featureName.text
            
            if usePPBalance || secondaryMethod {
                text = PaymentLegacyLocalizable.balance.text
            }
            if secondaryMethod {
                text += "+ \(CreditPicPayLocalizable.featureName.text)"
            }
        } else {
            var cardName = ""
            if !defaultCreditCard.alias.isEmpty {
                /// set alias for description text in payment toolbar
                cardName = "\(defaultCreditCard.alias.capitalizingFirstLetter()) \(lastNumbers)"
            } else {
                cardName = "\(PaymentLegacyLocalizable.creditCard.text) \(lastNumbers)"
            }
            
            text = cardName
            
            if usePPBalance || secondaryMethod {
                text = PaymentLegacyLocalizable.balance.text
            }
            if secondaryMethod {
                text += "+ \(cardName)"
            }
        }
        
        return text
    }
    
    fileprivate func setCCImage(showSecondaryImage: Bool = false, usePPBalance: Bool) {
        
        if let defaultCreditCard: CardBank = CreditCardManager.shared.defaultCreditCard, !defaultCreditCard.image.isEmpty
            
        {
            if ((usePPBalance == true && showSecondaryImage == true) || (usePPBalance == false)) {
                let secondaryImage: UIImage? = showSecondaryImage ? UIImage(named: "ppBalanceIcon") : nil
                let text: String = getPaymentMethodText(usePPBalance: usePPBalance, secondaryMethod: showSecondaryImage)
                toolbar.setPaymentMethod(mainImageUrl: defaultCreditCard.image, secondaryImage: secondaryImage, text: text)
                return
            }
        }
        
        let mainImage = usePPBalance && !showSecondaryImage ? UIImage(named: "ppBalanceIcon") : #imageLiteral(resourceName: "ppCreditCardIcon")
        
        // Generic image
        let text: String = getPaymentMethodText(usePPBalance: usePPBalance, secondaryMethod: showSecondaryImage)
        self.toolbar.setPaymentMethod(mainImage: mainImage, secondaryImage: nil, text: text)
    }
    
    @objc fileprivate func updateViews() {
        guard let paymentManager = paymentManager, paymentManager.usePicPayBalance() == true else {
            updatePrivacyImageAndText()
            setCCImage(usePPBalance: self.paymentManager?.usePicPayBalance() ?? false)
            return
        }
        var showSecondaryImage = false
        var usingPPBalance = false
        
        if !(paymentManager.cardTotal() == NSDecimalNumber.zero) && !(paymentManager.balanceTotal() == NSDecimalNumber.zero) {
            usingPPBalance = true
            showSecondaryImage = true
        }
        else if (!(paymentManager.cardTotal() == NSDecimalNumber.zero) && (paymentManager.balanceTotal() == NSDecimalNumber.zero)) {
            
        }
        else if (paymentManager.cardTotal() == NSDecimalNumber.zero) && !(paymentManager.balanceTotal() == NSDecimalNumber.zero) {
            usingPPBalance = true
        }
        else if paymentManager.forceBalance {
            usingPPBalance = true
        }
        else {
            if !(paymentManager.balance() == NSDecimalNumber.zero) && paymentManager.usePicPayBalance() {
                usingPPBalance = true
            }
            else {
                
            }
        }
        if usingPPBalance {
            self.toolbar.setPaymentMethod(mainImage: UIImage(named: "ppBalanceIcon")!, text: getPaymentMethodText(usePPBalance: usingPPBalance, secondaryMethod: showSecondaryImage))
            setCCImage(showSecondaryImage: showSecondaryImage, usePPBalance: usingPPBalance)
            updatePrivacyImageAndText()
            return
        }
        
        setCCImage(showSecondaryImage: showSecondaryImage, usePPBalance: usingPPBalance)
        updatePrivacyImageAndText()
    }
    
    fileprivate func updatePrivacyImageAndText(){
        if case .pixReturn = paymentToolbarType {
            privacyConfig = .Private
        }

        if privacyConfig == FeedItemVisibility.Private {
            toolbar.privacyImage.image = UIImage(named: "icoPrivPrivado")
            toolbar.privacyLabel.text = DefaultLocalizable.privacyPrivate.text
        } else {
            toolbar.privacyImage.image = UIImage(named: "icoPrivAmigos")
            toolbar.privacyLabel.text = DefaultLocalizable.privacyPublic.text
        }
    }
    
    // MARK: - Internal Methods
    func showCreditCardFirstInsertPopup() {
        parent?.view.endEditing(true)
        guard let parentVC = parent else {
            return
        }
        let creditCardRequired = CreditCardRequiredViewController()
        creditCardRequired.creditCardDelegate = self
        popupPresenter.showPopup(creditCardRequired, fromViewController: parentVC)
    }
    
    // MARK: - User Actions
    
    func changePrivacy(){
        if case let .pixReturn(_, changePrivacyAlert) = paymentToolbarType {
            AlertMessage.showAlert(changePrivacyAlert, controller: parent)
            return
        }

        PPAnalytics.trackEvent(withCustomProperties: "Checkout Transaction Privacy Accessed", properties: [:])
        let alert = UIAlertController(title: PaymentLegacyLocalizable.selectWhoCanSeeThisPayment.text, message: PaymentLegacyLocalizable.transactionValuesVisibility.text, preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel)
        let friendsAction = UIAlertAction(title: PaymentLegacyLocalizable.publicFollowers.text, style: .default) { [weak self] _ in
            guard let strongSelf = self else {
                return
            }
            // Change local privacy as Public
            strongSelf.privacyConfig = FeedItemVisibility.Friends
            strongSelf.updatePrivacyImageAndText()
        }
        
        let privateAction = UIAlertAction(title: PaymentLegacyLocalizable.privateParticipants.text, style: .default ){ [weak self] _ in
            guard let strongSelf = self else {
                return
            }
            // Change local privacy as Private
            strongSelf.privacyConfig = FeedItemVisibility.Private
            strongSelf.updatePrivacyImageAndText()
        }
        
        alert.addAction(friendsAction)
        alert.addAction(privateAction)
        alert.addAction(cancelAction)
        
        parent?.present(alert, animated: true, completion: nil)
    }
    
    func pay() {
        if paymentManager?.cardTotal() != NSDecimalNumber.zero && CreditCardManager.shared.cardList.isEmpty {
            if CreditCardManager.shared.isOcurredErrorOnFirstLoad {
                
                /// If in first time not loaded card list, this load again this request for load card list
                /// in the credit card manager
                toolbar.setPaymentMethodForNotLoadedCardList()
                CreditCardManager.shared.loadCardList { [weak self] (error) in
                    DispatchQueue.main.async {
                        guard let strongSelf = self else {
                            return
                        }
                        if let error = error {
                            guard let parentController = strongSelf.parent else {
                                return
                            }
                            AlertMessage.showCustomAlertWithError(error, controller: parentController)
                        } else {
                            strongSelf.updateViews()
                            strongSelf.pay()
                        }
                    }
                }
            } else {
                showCreditCardFirstInsertPopup()
            }
        } else {
            payAction?(self.privacyConfig.rawValue)
        }
        
    }
    
    func changePaymentMethod() {
        switch paymentToolbarType {
        case let .pix(alert):
            AlertMessage.showAlert(alert, controller: parent)
            return
        case let .pixReturn(changePaymentAlert, _):
            AlertMessage.showAlert(changePaymentAlert, controller: parent)
            return
        default:
            break
        }

        if let ccSelection = ViewsManager.paymentMethodsStoryboardViewController(withIdentifier: "PaymentMethodsViewController") as? PaymentMethodsViewController {
            ccSelection.viewModel.shouldShowPicPayBalanceSwitch = canUseBalance
            ccSelection.headerText = paymentMethodHeaderText
            parent?.navigationController?.pushViewController(ccSelection, animated: true)
        }
    }
    
    
    // MARK: - Observable
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(PPPaymentManager.subtotal){
            updateViews()
        }
    }
}

// MARK: - PaymentToolbarDelegate
extension PaymentToolbarController: PaymentToolbarDelegate {
    func paymentToolbarDidTouchInPayButton(toolbar: PaymentToolbar) {
        pay()
    }
    
    func paymentToolbarShowPaymentMethodSelection() {
        changePaymentMethod()
        changeMethod?()
    }
    
    func paymentToolbarDidTouchInPrivacydButton(toolbar: PaymentToolbar) {
        changePrivacy()
    }
}

// MARK: - CreditCardRequiredDelegate
extension PaymentToolbarController: CreditCardRequiredDelegate {
    func openCreditCardForm() {
        guard let addCreditCardVC = ViewsManager.paymentMethodsStoryboardViewController(withIdentifier: "AddNewCreditCardViewController") as? AddNewCreditCardViewController else {
            return
        }
        let viewModel = AddNewCreditCardViewModel(origin: .payment)
        addCreditCardVC.viewModel = viewModel
        addCreditCardVC.hidesBottomBarWhenPushed = true
        addCreditCardVC.isCloseButton = true
        parent?.present(PPNavigationController(rootViewController:addCreditCardVC), animated: true, completion: nil)
    }
    
    func dismissPopup() {
         parent?.viewDidAppear(false)
    }
}
