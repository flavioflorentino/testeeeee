import Foundation

enum PaymentLegacyLocalizable: String, Localizable {
    case balance
    case moreCard
    case creditCard
    case selectWhoCanSeeThisPayment
    case transactionValuesVisibility
    case publicFollowers
    case privateParticipants
    case doYouWantChoosePrivateAsDefault
    case privatePaymentsCannotBeChanged
    
    var file: LocalizableFile {
        return .paymentLegacy
    }
    
    var key: String {
        return self.rawValue
    }
}
