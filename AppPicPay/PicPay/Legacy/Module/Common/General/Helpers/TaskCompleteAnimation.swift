import AVFoundation
import Lottie
import UI
import UIKit

final class TaskCompleteAnimation: NSObject {
    let animationWrapperView: UIView
    let lottieViewSize: CGSize
    let completion: () -> Void
    var lottieView = AnimationView(name: "LottieSucess")
    private var player: AVAudioPlayer?
    
    init(_ animationWrapperView: UIView, lottieViewSize: CGSize, completion: @escaping () -> Void) {
        self.animationWrapperView = animationWrapperView
        self.lottieViewSize = lottieViewSize
        self.completion = completion
    }
    
    @objc
    class func showSuccess(
        onView superview: UIView?,
        wrapperBounds: CGRect = UIScreen.main.bounds,
        animationSize: CGSize = .zero,
        completion: @escaping () -> Void
    ) {
        guard let view = superview else {
            return
        }
        let lottieViewSize = animationSize == .zero ? CGSize(width: 200, height: 200) : animationSize
        let animationWrapperView = UIView()
        animationWrapperView.alpha = 0
        animationWrapperView.backgroundColor = Palette.ppColorGrayscale000.color
        animationWrapperView.frame = wrapperBounds
        
        view.addSubview(animationWrapperView)
        
        let animation = TaskCompleteAnimation(animationWrapperView, lottieViewSize: lottieViewSize, completion: completion)
        UIView.animate(withDuration: 0.25, animations: { animationWrapperView.alpha = 1 }, completion: { done in
            if done {
                animation.animation()
            }
        })
    }
    
    @objc
    func myviewTapped(_ sender: UITapGestureRecognizer) {
        lottieView.pause()
        closeAnimation()
    }

    private func playCoinSound() {
        guard let url = Bundle.main.url(forResource: "coin", withExtension: "caf") else {
            return
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(.soloAmbient)
            try AVAudioSession.sharedInstance().setActive(true)

            player = try? AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.caf.rawValue)
            player?.play()
            
            try AVAudioSession.sharedInstance().setActive(false, options: .notifyOthersOnDeactivation)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func animation() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(myviewTapped(_:)))
        animationWrapperView.addGestureRecognizer(tapGesture)
        
        lottieView.frame = CGRect(origin: .zero, size: lottieViewSize)
        lottieView.center = animationWrapperView.center
        lottieView.contentMode = .scaleAspectFill
        lottieView.animationSpeed = 0.75
        animationWrapperView.addSubview(lottieView)
        playCoinSound()
        lottieView.play { _ in
            self.closeAnimation()
        }
    }
    
    private func closeAnimation() {
        UIView.animate(withDuration: 0.25, animations: { self.animationWrapperView.alpha = 0 }, completion: { _ in
            self.animationWrapperView.removeFromSuperview()
            self.completion()
        })
    }
}
