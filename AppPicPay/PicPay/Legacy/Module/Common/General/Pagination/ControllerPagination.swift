import UI
import UIKit

protocol PaginationChild {
    func childWillAppear()
    func childWillDisappear()
    func childDidAppear()
}

final class ControllerPagination: PPBaseViewController {
    // MARK: Enum
    enum Page: Int {
        case camera = 0
        case application = 1
    }
    
    // MARK: - StatusBar Methods
    override var childForStatusBarStyle: UIViewController?{
        if childrens.isEmpty || pageControl.currentPage >= childrens.count {
            return nil
        }
        return childrens[pageControl.currentPage].viewController
    }
    
    override var childForStatusBarHidden: UIViewController? {
        if childrens.isEmpty || pageControl.currentPage >= childrens.count {
            return nil
        }
        return childrens[pageControl.currentPage].viewController
    }
    
    // MARK: - Breadcrumb
    
    // Remove class from breadcrumb
    override var breadcrumbDescription: String {
        return ""
    }
    
    // MARK: - Internal Struct
    
    struct Child {
        let viewController: UIViewController
        var navigationController: UINavigationController?
        let isPaginationRootOnly: Bool
        
        init(viewController: UIViewController, navigationController: UINavigationController? = nil, isPaginationRootOnly: Bool = true) {
            self.viewController = viewController
            self.navigationController = navigationController
            self.isPaginationRootOnly = isPaginationRootOnly
            if navigationController == nil,
                let nav = viewController as? UINavigationController {
                self.navigationController = nav
            }
        }
    }
    
    // MARK: Private vars
    fileprivate var scrollView = UIScrollView()
    var childrens: [Child] = []
    fileprivate var pageControl = UIPageControl()
    fileprivate var didSetInitialPage = false
    
    // MARK: Public vars
    var initialPage: Page = .camera
    var currentPage: Int {
        return pageControl.currentPage
    }
    var didChange: ((Page) -> ())?
    var isVisible: (([Int]) -> ())?
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView = UIScrollView(frame: view.frame)
        scrollView.bounces = false
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        self.view.addSubview(scrollView)
        
        configurePageControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !didSetInitialPage {
            didSetInitialPage = true
            setPage(initialPage, animated: false)
        }
        
        callChildLifeCycle()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        callChildLifeCycle(previousPage: currentPage, ignoreAppear: true)
    }
    
    // MARK: Public functions
    func disable() {
        self.scrollView.isScrollEnabled = false
    }
    
    func clear() {
        childrens = []
        configurePageControl()
        scrollView.subviews.forEach { $0.removeFromSuperview() }
        scrollView.contentSize = .zero
    }
    
    func enable() {
        self.scrollView.isScrollEnabled = true
    }
    
    func setPage(_ page: Page, animated: Bool) {
        let previousPage = pageControl.currentPage
        let nextPageOffset = CGPoint(x: scrollView.frame.size.width * CGFloat(page.rawValue), y: 0)

        pageControl.currentPage = page.rawValue
        // didScroll would be called way too late so we call the handler with the future offset
        // so everything can be handled BEFORE we call the callChildLifeCycle method
        didScrollHandler(withContentOffset: nextPageOffset)
        callChildLifeCycle(previousPage: previousPage)
        scrollView.setContentOffset(nextPageOffset, animated: animated)
    }
    
    func add(child: UIViewController, withNavigationController nav: UINavigationController? = nil, isPaginationRootOnly: Bool = true, toIndex index: Int? = nil) {
        let child = Child(viewController: child, navigationController: nav, isPaginationRootOnly: isPaginationRootOnly)
        child.navigationController?.delegate = self
        
        if let index = index {
            childrens.insert(child, at: index)
        }else{
            childrens.append(child)
        }
        
        configurePageControl()
        
        if let _ = index {
            clear()
            childrens.forEach({ child in
                self.addChildToScrollView(child: child)
            })
        }else{
            addChildToScrollView(child: child)
        }
    }
    
    // MARK: Private functions
    fileprivate func callChildLifeCycle(previousPage: Int? = nil, ignoreAppear: Bool = false) {
        if let previousPage = previousPage,
            previousPage == currentPage,
            !ignoreAppear{
            return
        }
        
        guard !childrens.isEmpty, pageControl.currentPage < childrens.count else {
            return
        }
        
        // Let child know of appearing/disappearing
        if let child = childrens[pageControl.currentPage].viewController as? PaginationChild,
            !ignoreAppear {
            child.childWillAppear()
            child.childDidAppear()
        }
        if let previousPage = previousPage,
            let previousChild = childrens[previousPage].viewController as? PaginationChild {
            previousChild.childWillDisappear()
        }
        
        // change status bar style for current view controller
        let viewController: UIViewController = childrens[pageControl.currentPage].viewController
        viewController.setNeedsStatusBarAppearanceUpdate()
    }
    
    private func addChildToScrollView(child: Child) {
        let index = childrens.count - 1
        var childFrame = view.frame
        // Current page x offset
        childFrame.origin.x = self.scrollView.frame.size.width * CGFloat(index)
        childFrame.size = self.scrollView.frame.size
        
        let vc = childrens[index].viewController
        addChild(vc)
        
        vc.view.backgroundColor = .backgroundPrimary()
        self.scrollView.addSubview(vc.view)
        vc.view.snp.makeConstraints({ make in
            make.top.equalTo(self.view)
            make.bottom.equalTo(self.view)
            make.leading.equalTo(self.scrollView.frame.size.width * CGFloat(index))
            make.width.equalTo(self.scrollView.frame.size.width)
        })
        vc.didMove(toParent: self)
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width * CGFloat(childrens.count), height: self.scrollView.frame.size.height)
    }
    
    private func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        self.pageControl.numberOfPages = childrens.count
        self.pageControl.currentPage = 0
    }
}

// MARK: ScrollView Delegate
extension ControllerPagination: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        let previousPage = Int(currentPage)
        pageControl.currentPage = Int(pageNumber)
        callChildLifeCycle(previousPage: previousPage)
        
        if let page = Page(rawValue: pageControl.currentPage) {
            didChange?(page)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        didScrollHandler()
    }
    
    fileprivate func didScrollHandler(withContentOffset: CGPoint? = nil) {
        let contentOffset = withContentOffset ?? scrollView.contentOffset
        var result: [Int] = []
        var index = 0
        for child in childrens {
            let viewController = child.viewController
            let beginning = viewController.view.frame.origin.x
            let end = viewController.view.frame.origin.x + viewController.view.frame.width
            if contentOffset.x < end && contentOffset.x >= beginning{
                result.append(index)
            }
            viewController.view.endEditing(true)
            index += 1
        }
        isVisible?(result)
    }
}

extension ControllerPagination: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        guard let child = childrens.first(where: { $0.navigationController == navigationController }),
            viewController != navigationController.viewControllers[0] else {
                scrollView.isScrollEnabled = true
                return
        }
        scrollView.isScrollEnabled = !child.isPaginationRootOnly
    }
}
