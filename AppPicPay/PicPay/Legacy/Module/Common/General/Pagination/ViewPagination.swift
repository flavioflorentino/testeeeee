//
//  ViewPagination.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 13/11/17.
//

import Foundation
class ViewPagination: PPBaseViewController {
    
    // MARK: Private vars
    internal var scrollView = UIScrollView()
    fileprivate var childrens: [UIView] = []
    internal var pageControl = UIPageControl()
    var initialPage = 0
    var currentPage: CGFloat {
        return CGFloat(pageControl.currentPage)
    }
    var currentChild: UIView {
        return self.childrens[pageControl.currentPage]
    }
    
    var childrenCoud: Int {
        return self.childrens.count
    }
    
    var didMove: ((Int) -> ())?
    var isVisible: (([Int]) -> ())?
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView = UIScrollView(frame: view.frame)
        scrollView.bounces = false
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        self.view.addSubview(scrollView)
        
        configurePageControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setPage(page: initialPage, animated: false)
    }
    
    // MARK: Public functions
    func disable() {
        self.scrollView.isScrollEnabled = false
    }
    
    func enable() {
        self.scrollView.isScrollEnabled = true
    }
    
    func setPage(page: Int, animated: Bool) {
        pageControl.currentPage = page
        scrollView.setContentOffset(CGPoint(x: scrollView.frame.size.width * currentPage, y: 0), animated: animated)
    }
    
    func add(child: UIView) {
        childrens.append(child)
        addChildToScrollView(child: child)
        configurePageControl()
    }
    
    func remove(child: UIView) {
        childrens.removeAll { $0 == child }
        child.removeFromSuperview()
        pageControl.numberOfPages = childrenCoud
    }
    
    func clear() {
        childrens = []
        configurePageControl()
        scrollView.subviews.forEach { $0.removeFromSuperview() }
        scrollView.contentSize = .zero
    }
    
    // MARK: Private functions
    private func addChildToScrollView(child: UIView) {
        let index = childrens.count - 1
        var childFrame = view.frame
        // Current page x offset
        childFrame.origin.x = self.scrollView.frame.size.width * CGFloat(index)
        childFrame.size = self.scrollView.frame.size
        self.scrollView.addSubview(child)
        child.snp.makeConstraints({ make in
            make.top.equalTo(self.view)
            make.bottom.equalTo(self.view)
            make.leading.equalTo(self.scrollView.frame.size.width * CGFloat(index))
            make.width.equalTo(self.scrollView.frame.size.width)
        })
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width * CGFloat(childrens.count), height: self.scrollView.frame.size.height)
    }
    
    private func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        self.pageControl.numberOfPages = childrens.count
        self.pageControl.currentPage = 0
    }
}

// MARK: ScrollView Delegate
extension ViewPagination: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
        didMove?(pageControl.currentPage)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var result: [Int] = []
        var index = 0
        for child in childrens {
            let beginning = child.frame.origin.x
            let end = child.frame.origin.x + child.frame.width
            
            if scrollView.contentOffset.x < end && scrollView.contentOffset.x >= beginning{
                result.append(index)
            }
            index += 1
        }
        isVisible?(result)
    }
}
