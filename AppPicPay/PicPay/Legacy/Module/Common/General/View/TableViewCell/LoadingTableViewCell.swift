import Foundation
import Lottie
import UI

final class LoadingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    fileprivate let loadingAnimation = AnimationView(name: "load-animation")
    
    @objc func startAnimating() {
        guard loadingAnimation.superview == nil else {
            return
        }
        
        addSubview(loadingAnimation)
        loadingAnimation.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        loadingAnimation.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        loadingAnimation.loopMode = .loop
        loadingAnimation.play()
    }
}

final class LoadingTableViewHeader: UIView {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    fileprivate let loadingAnimation = AnimationView(name: "load-animation")
    
    @objc func startAnimating() {
        guard loadingAnimation.superview == nil else {
            return
        }
        
        addSubview(loadingAnimation)
        loadingAnimation.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        loadingAnimation.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        loadingAnimation.loopMode = .loop
        loadingAnimation.play()
    }
}

@objc
final class LoadingTableViewLine: UIView {
    private let loadingBar = UIView()
    private let loadingBarWidth: CGFloat

    @objc
    init(width: CGFloat) {
        loadingBarWidth = width * 3
        super.init(frame: CGRect(x: 0, y: 0, width: width, height: 3))
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        loadingBarWidth = 0.0
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        backgroundColor = Colors.backgroundPrimary.color
        loadingBar.backgroundColor = #colorLiteral(red: 0.3529411765, green: 0.4588235294, blue: 0.4235294118, alpha: 1)
        loadingBar.isHidden = true
        addSubview(loadingBar)
    }
    
    @objc
    func startAnimating() {
        loadingBar.isHidden = false
        loadingBar.layer.removeAllAnimations()
        loadingBar.frame = CGRect(x: -loadingBarWidth, y: 0, width: loadingBarWidth, height: frame.height)
        UIView.animate(withDuration: 1.0, delay: 0, options: [.repeat, .autoreverse], animations: {
            self.loadingBar.frame = CGRect(x: self.frame.width, y: 0.0, width: self.loadingBarWidth, height: self.frame.height)
        })
    }
}
