import Foundation
import SnapKit
import UI

enum CardStyle {
    case normal
    case extendedHorizontal
    case flat
}

class CardStyleTableViewCell: UITableViewCell {
    // MARK: Default/Constant values
    private let leftMargin = 12
    private let rightMargin = 12
    private let cornerRadius: CGFloat = 5
    
    // MARK: Private vars
    private var containerView: UIView?
    private var left: Constraint?
    private var right: Constraint?
    private var top: Constraint?
    private var bottom: Constraint?
    
    // MARK: Public vars
    var style: CardStyle = .normal {
        didSet {
            switch style {
            case .normal:
                left?.update(offset: leftMargin)
                right?.update(offset: rightMargin)
                containerView?.layer.cornerRadius = cornerRadius
            case .extendedHorizontal:
                left?.update(offset: 0)
                right?.update(offset: 0)
                containerView?.layer.cornerRadius = 0
            // Flat is basically a non card style (in a class named CardStyleTableViewCell, but whatever)
            case .flat:
                layer.shadowColor = UIColor.clear.cgColor
                layer.shadowOpacity = 0
                layer.shadowRadius = 0
                // Extend horizontally
                left?.update(offset: 0)
                right?.update(offset: 0)
                top?.update(offset: 0)
                bottom?.update(offset: 0)
                containerView?.layer.cornerRadius = 0
                
                containerView?.backgroundColor = .clear
            }
        }
    }
    
    private var cellBackgroundColor = Palette.ppColorGrayscale000.color
    
    var animateTouches = true
    
    func setBackground(color: UIColor) {
        cellBackgroundColor = color
        containerView?.backgroundColor = cellBackgroundColor
    }
    
    func setup(containerView: UIView, avoidShadow: Bool = false) {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        selectionStyle = .none
        
        // Setup view
        self.containerView = containerView
        containerView.layer.cornerRadius = cornerRadius
        if !avoidShadow {
            containerView.dropShadow()
        }
        containerView.backgroundColor = Palette.ppColorGrayscale000.color
        
        // Setup constraints (clearing constraints first)
        if let theSuperview = containerView.superview {
            containerView.removeFromSuperview()
            theSuperview.addSubview(containerView)
        }else{
            self.addSubview(containerView)
        }
        
        containerView.snp.remakeConstraints { make in
            self.top = make.top.equalToSuperview().offset(4).constraint
            self.left = make.left.equalToSuperview().offset(12).constraint
            self.right = make.right.equalToSuperview().offset(-12).constraint
            self.bottom = make.bottom.equalToSuperview().offset(-4).constraint
        }
        
        // Overriding isSelected/isHighlighted just doesn't work well
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(animateSelection))
        tap.cancelsTouchesInView = false
        self.addGestureRecognizer(tap)
    }
    
    @objc func animateSelection() {
        guard animateTouches else {
            return
        }
        UIView.animate(withDuration: 0.5) {
            self.containerView?.backgroundColor = Palette.ppColorGrayscale300.color
            UIView.animate(withDuration: 0.5) {
                self.containerView?.backgroundColor = self.cellBackgroundColor
            }
        }
    }
    
}
