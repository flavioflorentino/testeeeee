//
//  BaseListOnboardView.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 20/08/2018.
//

import UIKit

final class BaseListOnboardView: NibView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var buttonView: UIPPButton!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var buttonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleLeadingContraint: NSLayoutConstraint!
    @IBOutlet weak var titleTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var textLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var textTrailingContraint: NSLayoutConstraint!
    
    
    func hideButton(withBottomMargin bottomMargin: CGFloat = 0){
        buttonView.isHidden = true
        buttonHeightConstraint.constant = 0
        buttonTopConstraint.constant = bottomMargin
    }
    
    func fitToContent(margin: CGFloat = 0) {
        setNeedsLayout()
        layoutIfNeeded()
        contentView.sizeToFit()
        frame = CGRect(origin: frame.origin, size: CGSize(width: frame.size.width, height: contentView.frame.size.height + (margin * 2)))
    }
}
