//
//  SearchTextField.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 13/07/2018.
//

import UIKit

final class SearchTextField: UITextField {

    override func clearButtonRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.size.width - 35, y: (bounds.size.height / 2) - 15, width: 30, height: 30)
    }

}
