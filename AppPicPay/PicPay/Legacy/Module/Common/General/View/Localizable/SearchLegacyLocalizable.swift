import Foundation

enum SearchLegacyLocalizable: String, Localizable {
    case searchPeople
    case checkYourConnection
    case noResults
    case lookedEverywhere
    case recentSearches
    
    var file: LocalizableFile {
        return .searchLegacy
    }
    
    var key: String {
        return self.rawValue
    }
}
