//
//  GenericInfoTableViewCell.swift
//  PicPay
//
//  Created by Marcos Timm on 11/10/17.
//
//

import UIKit

final class GenericInfoTableViewCell: UITableViewCell {
    
    var cellInfo = UILabel()
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.clear
        self.selectedBackgroundView = bgColorView
        self.backgroundColor = UIColor.clear
        
    }
    
    func setText(info: String)
    {
        let screenSize = UIScreen.main.bounds
        cellInfo = UILabel(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: CGFloat.greatestFiniteMagnitude))
        cellInfo.text = info
        cellInfo.font = UIFont.systemFont(ofSize: 13.0)
        cellInfo.textColor = UIColor(red:0.40, green:0.40, blue:0.40, alpha:1.00)
        cellInfo.numberOfLines = 0;
//        cellInfo.lineBreakMode = NSLineBreakMode.byWordWrapping
        cellInfo.translatesAutoresizingMaskIntoConstraints = false
        cellInfo.sizeToFit()
        contentView.addSubview(cellInfo)
        
        let viewsDict = [
            "cellinfo" : cellInfo,
            ] as [String : Any]
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[cellinfo]", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[cellinfo]-16-|", options: [], metrics: nil, views: viewsDict))
        
    }
    
}
