import UI
import UIKit

final class ConnectionErrorView: UIView {
    private lazy var mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.alignment = .center
        return stackView
    }()
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        label.textColor = Palette.ppColorGrayscale500.color
        label.text = title
        label.textAlignment = .center
        label.numberOfLines = 0
        label.setContentHuggingPriority(UILayoutPriority(1000), for: .vertical)
        return label
    }()
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = subtitle
        label.setContentHuggingPriority(UILayoutPriority(750), for: .vertical)
        return label
    }()
    private lazy var tryAgainButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = 24
        button.configure(with: Button(title: DefaultLocalizable.tryAgain.text, type: .cta))
        button.addTarget(self, action: #selector(tryAgainButtonTapped), for: .touchUpInside)
        button.loadingTitleColor = Palette.ppColorGrayscale000.color
        button.loadingBackgrounColor = button.backgroundColor
        return button
    }()
    
    private let title: String
    private let subtitle: String
    private let type: TypeError
    
    @objc var tryAgainTapped: () -> Void = {}
    
    @objc
    init(with error: Error? = nil , type: TypeError = .connection, frame: CGRect) {
        if let error = error as? PicPayErrorDisplayable {
            self.title = error.title
            self.subtitle = error.description.isEmpty ? DefaultLocalizable.errorConnectionViewSubtitle.text : error.description
        } else {
            self.title = DefaultLocalizable.errorConnectionViewTitle.text
            self.subtitle = DefaultLocalizable.errorConnectionViewSubtitle.text
        }
        
        self.type = type
        super.init(frame: frame)
        
        switch self.type {
        case .connection:
            self.imageView.image = #imageLiteral(resourceName: "ilu_search_error_con")
        case .error:
            self.imageView.image = #imageLiteral(resourceName: "icon-bad")
        }
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        mainStackView.addArrangedSubview(imageView)
        mainStackView.addArrangedSubview(titleLabel)
        mainStackView.addArrangedSubview(subtitleLabel)
        mainStackView.addArrangedSubview(tryAgainButton)
        addSubview(mainStackView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            tryAgainButton.heightAnchor.constraint(equalToConstant: 48),
            tryAgainButton.trailingAnchor.constraint(equalTo: mainStackView.trailingAnchor),
            tryAgainButton.leadingAnchor.constraint(equalTo: mainStackView.leadingAnchor),
            imageView.heightAnchor.constraint(equalToConstant: 90),
            imageView.widthAnchor.constraint(equalToConstant: 90),
            
            mainStackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            mainStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            mainStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            ])
    }
    
    @objc
    private func tryAgainButtonTapped() {
        tryAgainButton.startLoadingAnimating()
        tryAgainTapped()
    }
}

extension ConnectionErrorView {
    @objc
    enum TypeError: Int {
        case error
        case connection
    }
}
