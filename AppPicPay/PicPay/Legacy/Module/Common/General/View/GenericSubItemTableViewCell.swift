//
//  GenericSubItemTableViewCell.swift
//  PicPay
//
//  Created by Marcos Timm on 10/10/17.
//
//

import UIKit

final class GenericSubItemTableViewCell: UITableViewCell {
    
    let imgArrow = UIImageView()
    let cellTitle = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.clear
        self.selectedBackgroundView = bgColorView
        
        imgArrow.frame = CGRect(x: 0, y: 0, width: 12, height: 16)
        imgArrow.image = #imageLiteral(resourceName: "UITableViewCellAccessoryDisclosureIndicator").withRenderingMode(.alwaysTemplate)
        imgArrow.contentMode = .scaleAspectFit
        
        cellTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        cellTitle.textColor = UIColor(red:0.13, green:0.76, blue:0.37, alpha:1.00)
        imgArrow.tintColor = UIColor(red:0.13, green:0.76, blue:0.37, alpha:1.00)
        
        imgArrow.translatesAutoresizingMaskIntoConstraints = false
        cellTitle.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(imgArrow)
        contentView.addSubview(cellTitle)
        
        let viewsDict = [
            "image" : imgArrow,
            "celltitle" : cellTitle,
            ] as [String : Any]
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[celltitle(35)]", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[image(35)]", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[celltitle]-[image(10)]-|", options: [], metrics: nil, views: viewsDict))
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
