import UI
import UIKit
import SnapKit

final class UILoadView: UIView {
    @objc enum Position: Int {
        case top
        case center
    }
    
    fileprivate var position: Position = .top
    
    fileprivate lazy var activity: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView(style: .gray)
        activity.center = CGPoint(x: (self.frame.size.width / 2) , y: self.position == .top ? 30 : (self.frame.size.height / 2))
        activity.startAnimating()
        return activity
    }()
    
    // MARK: - Initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @objc init(superview: UIView, position: Position = .top, activityStyle: UIActivityIndicatorView.Style = .gray){
        super.init(frame: CGRect(x: 0, y: 0, width: superview.frame.size.width, height: superview.frame.size.height))
        
        if #available(iOS 13.0, *) {
            activity.style = traitCollection.userInterfaceStyle == .dark ? .white : .gray
        } else {
            activity.style = activityStyle
        }
        
        self.position = position
        superview.addSubview(self)
        self.backgroundColor = UIColor.clear
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setup(){
        backgroundColor = Palette.ppColorGrayscale000.color
        
        self.addSubview(activity)
        
        snp.makeConstraints { (make) in
            make.size.equalToSuperview()
        }
        
        if position == .center{
            activity.snp.makeConstraints { (make) in
                make.center.equalToSuperview()
            }
        } else {
            activity.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.top.equalToSuperview().offset(32)
            }
        }
    }
    
    // MARK: - Public Methods
    
    @objc func startLoading() {
        self.layer.opacity = 1.0
        self.isHidden = false
        activity.startAnimating()
    }
    
    @objc func animatedRemoveFromSuperView(_ completion: ((Bool) -> Swift.Void)? = nil ){
        UIView.animate(withDuration: 0.25, animations: {
            self.layer.opacity = 0.0
        }) { (completed) in
            self.removeFromSuperview()
            completion?(completed)
        }
    }
    
    @objc func stopLoading(animated: Bool = true) {
        if animated {
            UIView.animate(withDuration: 0.25, animations: {
                self.layer.opacity = 0.0
            }) { (completed) in
                self.isHidden = true
            }
        }else{
            activity.stopAnimating()
            self.isHidden = true
        }
    }
}
