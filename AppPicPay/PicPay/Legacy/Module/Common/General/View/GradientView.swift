import UI
import UIKit

// If you need a quick-and-dirty gradient applied to a UIView check the View+Extensions file
// If you want more control over the gradient (eg. animations) this class is the way to go
@IBDesignable
open class GradientView: UIView {
    @IBInspectable
    
    public var startColor: UIColor = Palette.ppColorGrayscale000.color {
        didSet {
            gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
            setNeedsDisplay()
        }
    }
    @IBInspectable
    public var endColor: UIColor = Palette.ppColorGrayscale000.color {
        didSet {
            gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
            setNeedsDisplay()
        }
    }
    
    public var startPoint: CGPoint = CGPoint(x: 0, y: 0) {
        didSet {
            gradientLayer.startPoint = startPoint
            setNeedsDisplay()
        }
    }
    
    public var endPoint: CGPoint = CGPoint(x: 0, y: 1) {
        didSet {
            gradientLayer.endPoint = endPoint
            setNeedsDisplay()
        }
    }
    
    private lazy var gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [self.startColor.cgColor, self.endColor.cgColor]
        return gradientLayer
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    open override func layoutSubviews() {
        gradientLayer.frame = bounds
    }
    
    public func animateTo(startColor: UIColor, endColor: UIColor) {
        
        self.layer.removeAllAnimations()
        
        CATransaction.begin()
        let fromColors: [AnyObject] = [self.startColor, self.endColor]
        self.startColor = startColor
        self.endColor = endColor
        let toColors: [AnyObject] = [self.startColor, self.endColor]
        let animation : CABasicAnimation = CABasicAnimation(keyPath: "colors")
        
        animation.fromValue = fromColors
        animation.toValue = toColors
        animation.duration = 0.250
        animation.isRemovedOnCompletion = true
        animation.fillMode = .forwards
        animation.timingFunction = CAMediaTimingFunction(name: .linear)
        
        self.gradientLayer.add(animation, forKey: "animateGradient")
        
        CATransaction.commit()
    }
}
