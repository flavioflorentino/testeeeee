import Foundation

protocol CircleViewDataSource: AnyObject {
    // The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
}

final class CircleItemsView: UIView {
    private let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    fileprivate let numberOfCellsPerRow: CGFloat = 4
    fileprivate var totalCount: CGFloat = 0
    fileprivate let spaceBetweenCells: CGFloat = 7
    fileprivate var heightConstraint: NSLayoutConstraint?
    var totalHeight:CGFloat = 0
    
    // The collection view data source is set to this class, but we need to pass down some data source methods
    weak var dataSource: CircleViewDataSource?
    weak var delegate: UICollectionViewDelegate?
    
    fileprivate var itemSize: CGSize {
        let width: CGFloat = (CGFloat(self.bounds.width) - (numberOfCellsPerRow - 1.0) * spaceBetweenCells) / numberOfCellsPerRow as CGFloat

        return CGSize(width: width, height: width)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false

        self.addSubview(collectionView)
        
        collectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    func registerCell(name: String, cellIdentifier: String) {
        collectionView.register(UINib(nibName: name, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }

    func update(_ heightConstraint: NSLayoutConstraint? = nil, totalCount: Int) {
        self.totalCount = CGFloat(totalCount)
        
        // Adjust collectionView height
        let width = itemSize.width
        let rows = ceil(Double(totalCount) / 4.0)
        let height = (CGFloat(rows + 2) * spaceBetweenCells) + (CGFloat(rows) * width)
        self.heightConstraint = heightConstraint
        self.heightConstraint?.constant = height
        self.totalHeight = height
        
        collectionView.reloadData()
    }
}

extension CircleItemsView: UICollectionViewDelegateFlowLayout {
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return spaceBetweenCells
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spaceBetweenCells
    }
    
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.collectionView?(collectionView, didSelectItemAt: indexPath)
    }
}

// MARK: - UICollectionViewDataSource
extension CircleItemsView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Int(totalCount)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = dataSource?.collectionView(collectionView, cellForItemAt: indexPath) else { return UICollectionViewCell() }
        
        return cell
    }
}
