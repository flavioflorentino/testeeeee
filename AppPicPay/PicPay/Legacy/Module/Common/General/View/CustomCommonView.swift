//
//  CustomView.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 11/09/17.
//
//

import UIKit

@IBDesignable
final class CustomCommonView: UIView {

    @IBInspectable
    public var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
        }
    }

    @IBInspectable
    public var shadowColor: UIColor? = nil {
        didSet {
            self.layer.shadowColor = self.shadowColor?.cgColor
        }
    }
    
    @IBInspectable
    public var shadowOffset: CGSize = CGSize.zero {
        didSet {
            self.layer.shadowOffset = self.shadowOffset
        }
    }
    
    @IBInspectable
    public var shadowRadius: CGFloat = 0.0 {
        didSet {
            self.layer.shadowRadius = self.shadowRadius
        }
    }
    
    @IBInspectable
    public var shadowOpacity: CGFloat = 1 {
        didSet {
            self.layer.shadowOpacity = Float(self.shadowOpacity)
        }
    }
    
    @IBInspectable
    public var borderWidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = self.borderWidth
        }
    }
    
    @IBInspectable
    public var borderColor: UIColor? = nil {
        didSet {
            self.layer.borderColor = self.borderColor?.cgColor
        }
    }
}
