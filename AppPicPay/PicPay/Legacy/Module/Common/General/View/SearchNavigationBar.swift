import FeatureFlag
import Foundation
import SnapKit
import UI
import Store

final class SearchNavigationBar: UIView, SearchHeaderLegacyView {
    typealias Dependencies = HasFeatureManager
    private var dependencies: Dependencies
    // MARK: Constants
    private let searchIconSize = CGSize(width: 15, height: 15)
    private let searchIconLeftMargin: CGFloat = 15
    private let searchIconRightMargin: CGFloat = 8
    
    private let leftButtonSize = CGSize(width: 30, height: 30)
    private let rightButtonSize = CGSize(width: 30, height: 30)
    
    // MARK: Variable constraint
    // Distance from the content view to the container top
    private var contentTop: Constraint?
    private var leftButtonLeading: Constraint?
    private var rightButtonTrailing: Constraint?
    private var cancelRight: Constraint?
    private var searchFieldLeading: Constraint?
    
    private var searchToRightIcon: Constraint?
    private var searchToCancel: Constraint?
    
    // MARK: Private views
    private var contentView = UIView()
    private var searchIcon = UIImageView(image: #imageLiteral(resourceName: "search_placeholder.png"))
    private var searchField = SearchTextField()
    private var leftButtonView = UIButton()
    private var rightButtonView = UIButton()
    private var cancelButton = UIButton()

    private lazy var backButton: UIButton = {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        button.setImage(Assets.back.image.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Colors.branding600.color
        button.isHidden = true

        return button
    }()
    
    // MARK: Private vars
    private var defaultMargin: CGFloat = 15
    
    // isExpanded = true if the text field is focused (so we have the text field *expanded*)
    var isExpanded = false {
        didSet {
            update()
        }
    }

    var showsLeftAndRightButtons: Bool = true {
        didSet {
            update()
        }
    }

    var showsBackButton: Bool = false {
        didSet {
            update()
        }
    }
    
    fileprivate var animationsEnabled = true
    
    fileprivate var leftIcon: UIImage? {
        didSet {
            leftButtonView.setImage(
                leftIcon?.withRenderingMode(.alwaysTemplate)
                    .imageMaskedAndTinted(with: Colors.branding600.color),
                    for: .normal
            )
            update()
        }
    }
    
    fileprivate var rightIcon: UIImage? {
        didSet {
            rightButtonView.setImage(rightIcon, for: .normal)
            update()
        }
    }
    
    fileprivate var isInitialized = false
    
    // MARK: Public vars
    // ignoreStatusBar = true if we should give the contentView a top padding of 20
    // This is needed in case the self.top is set to the superview instead of the safe area
    var ignoreStatusBar = false {
        didSet {
            contentTop?.update(offset: ignoreStatusBar ? 20 : 0)
        }
    }
    
    var hasLeftIcon: Bool {
        return leftIcon != nil
    }
    
    var hasRightIcon: Bool {
        return rightIcon != nil
    }
    
    var leftButtonTapped: (() -> ())?
    var rightButtonTapped: (() -> ())?
    var cancelButtonTapped: (() -> ())?
    var searchTextChanged: ((String) -> ())?
    var searchTextDidBeginEditing: ((String) -> ())?
    var searchTextShouldBeginEditing: (() -> ())?
    var searchTextDidEndEditing: ((String) -> ())?
    var currentText: String {
        get {
            return searchField.text ?? ""
        }
        
        set(newValue) {
            searchField.text = newValue
        }
    }
    var shouldChageNormalStateWhenEndEdit: Bool = true
    
    // MARK: Public functions
    func setLeftButtonImage(image: UIImage, animated: Bool = true) {
        let previousAnimated = animationsEnabled
        animationsEnabled = animated
        leftIcon = image
        animationsEnabled = previousAnimated
    }
    
    func setPlaceholder(placeholder: String = SearchLegacyLocalizable.searchPeople.text) {
        searchField.placeholder = placeholder
        searchField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [.foregroundColor: Colors.grayscale700.color])
    }
    
    func setRightButtonImage(image: UIImage, animated: Bool = true) {
        let previousAnimated = animationsEnabled
        animationsEnabled = animated
        rightIcon = image
        animationsEnabled = previousAnimated
    }
    
    func setTwitterKeyboard() {
        searchField.keyboardType = .twitter
    }
    
    func setCompactMode() {
        self.defaultMargin = 8
        setupConstraints()
    }
    
    override func endEditing(_ force: Bool) -> Bool {
        searchField.endEditing(force)
        
        return super.endEditing(force)
    }
    
    // MARK: Helper functions
    private func animateUpdate(cancelVisible: Bool, rightVisible: Bool, leftVisible: Bool) {
        if !isInitialized {
            self.cancelButton.alpha = 1.0
            self.rightButtonView.alpha = 1.0
            self.leftButtonView.alpha = 1.0
            self.contentView.layoutIfNeeded()
        }
        
        // Animate any autolayout changes and set the correct alpha value for each element
        UIView.animate(withDuration: animationsEnabled ? 0.25 : 0) {
            self.cancelButton.alpha = cancelVisible ? 1.0 : 0.0
            self.rightButtonView.alpha = rightVisible ? 1.0 : 0.0
            self.leftButtonView.alpha = leftVisible ? 1.0 : 0.0
            self.contentView.layoutIfNeeded()
        }
    }
    
    private func leftIconLeading(visible: Bool = true) -> CGFloat {
        // We either give a left margin to the button or give a negative margin
        // equal to it's size so it's just at the edge outside the visible rect
        return visible ? defaultMargin : -leftButtonSize.width
    }
    
    private func rightIconTrailing(visible: Bool = true) -> CGFloat {
        // We either give a right margin to the button or give a negative margin
        // equal to it's size so it's just at the edge outside the visible rect
        return visible ? -defaultMargin : rightButtonSize.width
    }
    
    private func cancelTrailing(visible: Bool = true) -> CGFloat {
        // We either give a right margin to the button or give a negative margin
        // equal to it's size so it's just at the edge outside the visible rect
        return visible ?  -defaultMargin : cancelButton.intrinsicContentSize.width
    }
    
    private func update() {
        self.contentView.setNeedsLayout()
        self.contentView.layoutIfNeeded()
        // The left button is visible if the text field is not expanded and if we have a left icon
        let leftButtonVisible = !isExpanded && hasLeftIcon && !showsBackButton && showsLeftAndRightButtons
        // The right button is visible if the text field is not expanded and if we have a right icon
        let rightButtonVisible = !isExpanded && hasRightIcon && showsLeftAndRightButtons
        // The cancel button is visible if the text field is expanded
        let cancelButtonVisible = isExpanded && !showsBackButton
        
        leftButtonLeading?.update(offset: leftIconLeading(visible: leftButtonVisible))
        cancelRight?.update(offset: cancelTrailing(visible: cancelButtonVisible))
        rightButtonTrailing?.update(offset: rightIconTrailing(visible: rightButtonVisible))
        
        // We need to decide at which view the text field right constraint should be anchored at
        searchToCancel?.deactivate()
        searchToRightIcon?.deactivate()
        
        // If the text field is not expanded and we do have a right icon, then we should deactivate the
        // "cancel" button margin and activate the right button margin
        if !isExpanded && hasRightIcon {
            searchToCancel?.deactivate()
            searchToRightIcon?.activate()
        }else{
            searchToCancel?.activate()
            searchToRightIcon?.deactivate()
        }
        
        // If animations are not enabled, execute the layout changes and then call animateUpdate that will do some
        // layout changes, but won't actually update
        if !animationsEnabled || !isInitialized {
            self.contentView.setNeedsLayout()
            self.contentView.layoutIfNeeded()
        }
    
        animateUpdate(cancelVisible: cancelButtonVisible, rightVisible: rightButtonVisible, leftVisible: leftButtonVisible)

        if showsBackButton {
            backButton.isHidden = false

            searchFieldLeading?.deactivate()
            searchField.snp.makeConstraints {
                searchFieldLeading = $0.leading.equalTo(backButton.snp.trailing).offset(defaultMargin).constraint
            }
        } else {
            backButton.isHidden = true

            searchFieldLeading?.deactivate()
            searchField.snp.makeConstraints {
                searchFieldLeading = $0.leading.equalTo(leftButtonView.snp.trailing).offset(defaultMargin).constraint
            }
        }
    }

    // MARK: Life Cycle
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        isInitialized = true
    }
    
    override init(frame: CGRect) {
        self.dependencies = DependencyContainer()
        super.init(frame: frame)
        commonInit()
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
        super.init(frame: CGRect.zero)
        commonInit()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        self.dependencies = DependencyContainer()
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = Palette.ppColorGrayscale100.color
        setupViews()
        setupConstraints()
        
        isExpanded = false
    }
    
    private func setupViews() {
        // **** Container ***
        addSubview(contentView)
        
        // **** Search Field ***
        searchField.layer.cornerRadius = 36 / 2
        searchField.font = UIFont.systemFont(ofSize: 16)
        
        // Left padding view
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: searchIconSize.width + searchIconLeftMargin + searchIconRightMargin, height: 10))
        searchField.leftView = paddingView
        searchField.leftViewMode = .always
        searchField.delegate = self
        searchField.returnKeyType = .search
        searchField.clearButtonMode = .always
        searchField.autocapitalizationType = .none
        searchField.autocorrectionType = .no
        
        searchField.textColor = Colors.grayscale700.color
        
        searchField.backgroundColor = Colors.backgroundPrimary.color
        searchField.layer.borderWidth = 1
        searchField.layer.borderColor = Colors.grayscale200.color.cgColor

        contentView.addSubview(searchField)
        
        // **** Search Icon ***
        searchIcon.image = Assets.NewIconography.Search.newSearchIcon.image.withRenderingMode(.alwaysTemplate)
        searchIcon.tintColor = Colors.grayscale700.color
        searchField.addSubview(searchIcon)
        
        // **** Left Icon ***
        leftButtonView.setImage(leftIcon?.withRenderingMode(.alwaysTemplate).imageMaskedAndTinted(with: Palette.ppColorBranding300.color), for: .normal)
        leftButtonView.addTarget(self, action: #selector(didTapLeftButton), for: .touchUpInside)
        contentView.addSubview(leftButtonView)
        
        // **** Right Icon ***
        rightButtonView.setImage(rightIcon, for: .normal)
        rightButtonView.addTarget(self, action: #selector(didTapRightButton), for: .touchUpInside)
        contentView.addSubview(rightButtonView)
        
        // **** Cancel Button ***
        cancelButton.setTitle(DefaultLocalizable.btCancel.text, for: .normal)
        cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        cancelButton.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        cancelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        contentView.addSubview(cancelButton)

        contentView.addSubview(backButton)
    }
    
    private func setupConstraints() {
        // **** Container ***
        contentView.snp.remakeConstraints { make in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
            self.contentTop = make.top.equalTo(self.ignoreStatusBar ? 20 : 0).constraint
        }
        
        // **** Search Field ***
        searchField.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(4)
            make.bottom.equalToSuperview().offset(-4)
            searchFieldLeading = make.left.equalTo(leftButtonView.snp.right).offset(defaultMargin).constraint
            self.searchToCancel = make.right.equalTo(cancelButton.snp.left).offset(-defaultMargin).constraint
            self.searchToCancel?.deactivate()
            self.searchToRightIcon = make.right.equalTo(rightButtonView.snp.left).offset(-defaultMargin).constraint
        }
        
        // **** Search Icon ***
        searchIcon.snp.remakeConstraints { make in
            make.leading.equalTo(searchIconLeftMargin)
            make.size.equalTo(searchIconSize)
            make.centerY.equalToSuperview()
        }
        
        // **** Left Icon ***
        leftButtonView.snp.remakeConstraints { make in
            self.leftButtonLeading = make.leading.equalToSuperview().offset(defaultMargin).constraint
            make.centerY.equalToSuperview()
            make.size.equalTo(leftButtonSize)
        }
        
        // **** Right Icon ***
        rightButtonView.snp.remakeConstraints { make in
            self.rightButtonTrailing = make.right.equalToSuperview().offset(-defaultMargin).constraint
            make.centerY.equalToSuperview()
            make.size.equalTo(rightButtonSize)
        }

        // **** Cancel Button ***
        cancelButton.setContentHuggingPriority(UILayoutPriority(rawValue: 251), for: .horizontal)
        cancelButton.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 751), for: .horizontal)
        cancelButton.snp.remakeConstraints { make in
            self.cancelRight = make.right.equalToSuperview().offset(-defaultMargin).constraint
            make.centerY.equalToSuperview()
        }

        backButton.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(defaultMargin)
            $0.size.equalTo(leftButtonSize)
        }
    }
    
    // MARK: Actions
    func clearSearch() {
        if searchField.text != "" {
            searchTextChanged?("")
        }

        searchField.text = ""
        _ = self.endEditing(true)
    }

    @objc func cancel() {
        clearSearch()
        cancelButtonTapped?()
    }
    
    @objc private func didTapLeftButton() {
        leftButtonTapped?()
    }
    
    @objc private func didTapRightButton() {
        rightButtonTapped?()
    }
    
    @discardableResult
    override func resignFirstResponder() -> Bool {
        super.resignFirstResponder()
        searchField.resignFirstResponder()
        
        return true
    }

    func focusSearchField() {
        searchField.becomeFirstResponder()
    }

    func searchField(isHidden: Bool) {
        searchField.isHidden = isHidden
    }
}

extension SearchNavigationBar: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.isExpanded = true
        searchTextDidBeginEditing?(textField.text ?? "")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if shouldChageNormalStateWhenEndEdit {
            self.isExpanded = false
        }
        searchTextDidEndEditing?(textField.text ?? "")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text ?? "") as NSString
        
        searchTextChanged?(text.replacingCharacters(in: range, with: string))
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = ""
        searchTextChanged?("")
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        searchTextShouldBeginEditing?()
        return true
    }
}
