import UI
import UIKit

@IBDesignable
final class BackgroundCircleValueView: UIView {
    fileprivate var _selected: Bool = false

    var normalColor = Palette.ppColorGrayscale100.color
    var internalColor = Palette.ppColorGrayscale200.color
    
    var isSelected: Bool? {
        get {
            return self._selected
        }
        set (newVal) {
            self._selected = newVal!
            self.applySelectedStyle()
        }
    }
    
    fileprivate var circle1: CAShapeLayer?
    fileprivate var circle2: CAShapeLayer?
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.applySelectedStyle()
    }
    
    func applySelectedStyle(){
        //remove old layers
        circle1?.removeFromSuperlayer()
        circle2?.removeFromSuperlayer()
        
        // external circle
        let halfSize: CGFloat = min( bounds.size.width/2, bounds.size.height/2)
        let desiredLineWidth2: CGFloat = 1.0
        
        let circlePath2 = UIBezierPath(
            arcCenter: CGPoint(x: halfSize, y: halfSize),
            radius: CGFloat( halfSize - (desiredLineWidth2 / 2) ),
            startAngle: CGFloat(0),
            endAngle:CGFloat(Double.pi * 2),
            clockwise: true)
        
        let shapeLayer2 = CAShapeLayer()
        shapeLayer2.path = circlePath2.cgPath
        
        shapeLayer2.fillColor = UIColor.clear.cgColor
        shapeLayer2.lineWidth = desiredLineWidth2
        
        if _selected {
            shapeLayer2.strokeColor = Palette.ppColorBranding300.cgColor
        } else {
            shapeLayer2.strokeColor = normalColor.cgColor
        }
        
        circle2 = shapeLayer2
        layer.addSublayer(shapeLayer2)
        
        // internal circle
        let circlePath = UIBezierPath(
            arcCenter: CGPoint(x: halfSize,y: halfSize),
            radius: CGFloat(halfSize - 4.0),
            startAngle: CGFloat(0),
            endAngle:CGFloat(Double.pi * 2),
            clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = _selected ? Palette.ppColorBranding300.cgColor : internalColor.cgColor
        
        circle1 = shapeLayer
        layer.addSublayer(shapeLayer)
    }
}
