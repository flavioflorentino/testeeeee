//
//  SearchEmptyListView.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 22/05/17.
//
//

import UIKit

final class SearchEmptyListView: NibView {

    @IBOutlet weak var textLabel: UILabel!
    
}
