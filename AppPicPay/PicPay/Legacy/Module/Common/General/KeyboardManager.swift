//
//  KeyboardManager.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 11/10/17.
//
//

import Foundation

final class KeyboardManager: NSObject {
    final class KeyboardAwareConstraint: NSObject {
        var initialValue: CGFloat
        weak var constraint: NSLayoutConstraint?
        weak var parentView: UIView?
        
        init(constraint: NSLayoutConstraint, parentView: UIView) {
            self.constraint = constraint
            self.initialValue = constraint.constant
            self.parentView = parentView
            
            super.init()
        }
    }
    
    static let shared = KeyboardManager()
    
    private var keyboardAwareConstraints:[KeyboardAwareConstraint] = []
    var keyboardHeight: CGFloat = 0
    
    //MARK: Public functions
    
    func addConstraint(_ constraint: NSLayoutConstraint, parentView: UIView) {
        removeNilConstraints()
        if keyboardAwareConstraints.contains(where: {$0.parentView == parentView}) == false {
            keyboardAwareConstraints.append(KeyboardAwareConstraint(constraint: constraint, parentView: parentView))
        }
    }
    
    func removeConstraint(for view: UIView) {
        keyboardAwareConstraints.removeAll(where: { $0.parentView == view })
    }
    
    //MARK: Private functions
    
    private override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func removeNilConstraints() {
        keyboardAwareConstraints = keyboardAwareConstraints.filter { $0.constraint != nil && $0.parentView != nil }
    }
    
    private func setAllConstraintsToInitialValue() {
        keyboardAwareConstraints.forEach {
            $0.constraint?.constant = $0.initialValue
        }
    }
    
    private func addToAllConstraints(value: CGFloat) {
        keyboardAwareConstraints.forEach {
            if $0.constraint?.constant == $0.initialValue {
                $0.constraint?.constant += value
            }
        }
    }
    
    private func layoutAllParents() {
        self.keyboardAwareConstraints.forEach {
            $0.parentView?.layoutIfNeeded()
        }
    }
    
    //MARK: Keyboard observer responder
    
    @objc func keyboardWillShow(_ notification: Notification) {
        keyboardWillChange(notification, hide: false)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        keyboardWillChange(notification, hide: true)
    }
    
    func keyboardWillChange(_ notification: Notification, hide: Bool) {
        guard let userInfo = (notification as NSNotification).userInfo,
            let keyboardHeight =  (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)
            else {
            return
        }
        
        self.keyboardHeight = keyboardHeight.cgRectValue.height
        
        removeNilConstraints()
        
        let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
        let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
        let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
        let animationCurve = UIView.AnimationOptions(rawValue: animationCurveRaw)
        
        if hide {
            setAllConstraintsToInitialValue()
        } else {
            addToAllConstraints(value: keyboardHeight.cgRectValue.height)
        }
        
        UIView.animate(withDuration: duration, delay: TimeInterval(0), options: animationCurve, animations: {
            self.layoutAllParents()
        }, completion: nil)
    }
}
