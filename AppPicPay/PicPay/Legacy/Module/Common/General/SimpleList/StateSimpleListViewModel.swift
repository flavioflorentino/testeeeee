//
//  StateSimpleListViewModel.swift
//  PicPay
//
//  ☝🏽☁️ Created by Luiz Henrique Guimarães on 06/03/18.
//

import UIKit

final class SimpleStateModel : NSObject {
    var uf: String
    var name: String
    
    init(name: String, uf: String){
        self.name = name
        self.uf = uf
    }
}

final class StateSimpleListViewModel: SimpleListViewModel<SimpleStateModel> {
    
    override init() {
        super.init()
        
        self.title = DefaultLocalizable.state.text
        self.rawList = [
            SimpleStateModel(name: "Acre", uf: "AC"),
            SimpleStateModel(name: "Alagoas", uf: "AL"),
            SimpleStateModel(name: "Amapá", uf: "AP"),
            SimpleStateModel(name: "Amazonas", uf: "AM"),
            SimpleStateModel(name: "Bahia", uf: "BA"),
            SimpleStateModel(name: "Ceará", uf: "CE"),
            SimpleStateModel(name: "Distrito Federal", uf: "DF"),
            SimpleStateModel(name: "Espírito Santo", uf: "ES"),
            SimpleStateModel(name: "Goiás", uf: "GO"),
            SimpleStateModel(name: "Maranhão", uf: "MA"),
            SimpleStateModel(name: "Mato Grosso", uf: "MT"),
            SimpleStateModel(name: "Mato Grosso do Sul", uf: "MS"),
            SimpleStateModel(name: "Minas Gerais", uf: "MG"),
            SimpleStateModel(name: "Pará", uf: "PA"),
            SimpleStateModel(name: "Paraíba", uf: "PB"),
            SimpleStateModel(name: "Paraná", uf: "PR"),
            SimpleStateModel(name: "Pernambuco", uf: "PE"),
            SimpleStateModel(name: "Piauí", uf: "PI"),
            SimpleStateModel(name: "Rio de Janeiro", uf: "RJ"),
            SimpleStateModel(name: "Rio Grande do Norte", uf: "RN"),
            SimpleStateModel(name: "Rio Grande do Sul", uf: "RS"),
            SimpleStateModel(name: "Rondônia", uf: "RO"),
            SimpleStateModel(name: "Roraima", uf: "RR"),
            SimpleStateModel(name: "Santa Catarina", uf: "SC"),
            SimpleStateModel(name: "São Paulo", uf: "SP"),
            SimpleStateModel(name: "Sergipe", uf: "SE"),
            SimpleStateModel(name: "Tocantins", uf: "TO")
        ]
    }
    
    override func label(forItem item: SimpleStateModel) -> String {
        return "\(item.name) - (\(item.uf))"
    }
}
