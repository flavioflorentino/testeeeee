//
//  SimpleListViewController.swift
//  PicPay
//
//  ☝🏽☁️ Created by Luiz Henrique Guimarães on 16/05/17.
//
//

import UI
import UIKit

class SimpleListViewController<Item>: PPBaseViewController {
    
    let tableViewController:SimpleListTableViewController<Item>
    let searchController:UISearchController
    let model: SimpleListViewModel<Item>
    
    var loadingView: UIView?
    var viewBottomConstraint: NSLayoutConstraint?
    
    typealias didSelected = (SimpleListTableViewController<Item>, Item) -> Void
    
    // MARK: Initializer
    
    init(model: SimpleListViewModel<Item>, didSelected: @escaping didSelected) {
        
        self.model = model
        self.searchController = UISearchController(searchResultsController: nil)
        self.tableViewController = SimpleListTableViewController(model: model)
        self.tableViewController.didSelectedAction =  didSelected
        
        super.init(nibName: nil, bundle: nil)
        
        // Keyboard events observers
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillOpen), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        title = model.title
        navigationItem.backBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.back.text, style: .plain, target: nil, action: nil)
        loadList()
    }
    
    // MARK: Internal Methods
    
    func configureView() {
        
        // Search Bar
        searchController.searchResultsUpdater = tableViewController
        searchController.searchBar.barTintColor = Palette.ppColorBranding300.color
        searchController.searchBar.tintColor = Palette.ppColorGrayscale000.color
        searchController.searchBar.isTranslucent = false
        searchController.searchBar.backgroundImage = UIImage()
        searchController.searchBar.placeholder = DefaultLocalizable.searchState.text
        searchController.hidesNavigationBarDuringPresentation = true
        definesPresentationContext = true
        searchController.dimsBackgroundDuringPresentation = false
        
        let searchBar = searchController.searchBar
        searchBar.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: searchBar.frame.size.height)
        view.addSubview(searchBar)
        
        // Table View
        // Add the view controller
        addChild(tableViewController)
        tableViewController.view.frame = view.frame
        tableViewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableViewController.view)
        
        // Apply the contrants to present the corret size
        let tableView = tableViewController.view!
        
        // store bottom constraint to adjust keyboardsize
        viewBottomConstraint = NSLayoutConstraint(item: tableView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: tableView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 0.0),
            NSLayoutConstraint(item: tableView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: 0.0),
            NSLayoutConstraint(item: tableView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: searchBar.frame.size.height),
            viewBottomConstraint!
            ])
        
        // move controller
        tableViewController.didMove(toParent: self)
    }
    
    @objc func loadList() {
        showLoading()
        model.loadItems { [weak self] (list, error) in
            self?.hideLoading()
            self?.tableViewController.tableView.reloadData()
            self?.checkConnectionError(list: list, error: error)
        }
    }
    
    func checkConnectionError(list: [Item]?, error:Error?) {
        if list == nil || list!.count == 0 {
            let errorView = ConnectionErrorView(with: nil, type: .connection, frame: view.frame)
            errorView.tryAgainTapped = { [weak self] in
                self?.loadList()
            }
            DispatchQueue.main.async {
                self.tableViewController.tableView.tableHeaderView = errorView
            }
        }else{
            self.tableViewController.tableView.tableHeaderView = nil
        }
    }
    
    func showLoading() {
        loadingView?.removeFromSuperview()
        loadingView = UIView(frame: CGRect(x: 0, y: searchController.searchBar.frame.size.height, width: view.frame.size.width, height: view.frame.size.height))
        loadingView?.backgroundColor = Palette.ppColorGrayscale000.color
        
        let activity = UIActivityIndicatorView(style: .gray)
        activity.center = CGPoint(x: view.frame.size.width/2, y: 40)
        loadingView?.addSubview(activity)
        
        view.addSubview(loadingView!)
        
        activity.startAnimating()
    }
    
    func hideLoading() {
        if let loading = loadingView {
            UIView.animate(withDuration: 0.25, animations: {
                loading.layer.opacity = 0.0
            }, completion: { (finish) in
                loading.removeFromSuperview()
                self.loadingView = nil
            })
        }
    }
    
    // MARK: - Keyboard Events Handler
    
    @objc func keyboardWillOpen(_ keyboardNotification:Notification) {
        if  let animationDuration = ((keyboardNotification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey]) as? NSNumber)?.doubleValue,
            let keyboardRectValue = (keyboardNotification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        {
            
            // animate the view to adjust height with the opened keyboard
            self.view.layoutIfNeeded()
            self.viewBottomConstraint?.constant = keyboardRectValue.height
            
            UIView.animate(withDuration: animationDuration, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @objc func keyboardWillHide(_ keyboardNotification:Notification) {
        if  let animationDuration = ((keyboardNotification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey]) as? NSNumber)?.doubleValue
        {
            // animate the view to adjust height with the opened keyboard
            self.view.layoutIfNeeded()
            self.viewBottomConstraint?.constant = 0.0
            
            UIView.animate(withDuration: animationDuration, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
}


