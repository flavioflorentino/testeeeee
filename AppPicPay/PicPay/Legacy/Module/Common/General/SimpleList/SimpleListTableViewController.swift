//
//  SimpleListTableViewController.swift
//  PicPay
//
//  ☝🏽☁️ Created by Luiz Henrique Guimarães on 06/03/18.
//

import UIKit

final class SimpleListTableViewController<Item>: UITableViewController, UISearchResultsUpdating {
    
    typealias didSelected = (SimpleListTableViewController<Item>, Item) -> Void
    var didSelectedAction: didSelected?
    
    var model: SimpleListViewModel<Item>
    
    init(model: SimpleListViewModel<Item>) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.list.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard model.list.count > indexPath.row  else {
                return UITableViewCell()
            }
        
        let reusable = tableView.dequeueReusableCell(withIdentifier: "Cell")
        let cell = reusable ?? UITableViewCell(style: .default, reuseIdentifier: "Cell")
        
        cell.textLabel?.text = model.label(forRow: indexPath)
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard model.list.count > indexPath.row  else {
            return
        }
        
        let item = model.list[indexPath.row]
        self.didSelectedAction?(self, item)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        model.filter(searchController.searchBar.text ?? "")
        self.tableView.reloadData()
    }
}
