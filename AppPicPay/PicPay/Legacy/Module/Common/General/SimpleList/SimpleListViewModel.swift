//
//  SimpleListViewModel.swift
//  PicPay
//
//  ☝🏽☁️ Created by Luiz Henrique Guimarães on 06/03/18.
//

import UIKit

class SimpleListViewModel<Item>: NSObject {
    
    var title = DefaultLocalizable.select.text
    var list: [Item] {
        return isFiltered ? filteredList : rawList
    }
    
    var rawList: [Item] = []
    var filteredList: [Item] = []
    var isFiltered: Bool = false
    
    func loadItems(_ completion: @escaping (([Item]?, Error?) -> Void)) {
        completion(list, nil)
    }
    
    func label(forRow index: IndexPath) -> String {
        return label(forItem: list[index.row])
    }
    
    func label(forItem item: Item) -> String {
        guard let label = item as? String else { return "" }
        return label
    }
    
    func filter(_ filter:String){
        if filter != "" {
            filteredList = rawList.filter({ [weak self] (item) -> Bool in
                guard let strongSelf = self else {
                    return false
                }
                return strongSelf.filterItem(filter: filter, item: item)
            })
            isFiltered = true
        }else {
            filteredList.removeAll()
            isFiltered = false
        }
    }
    
    func filterItem(filter: String, item: Item) -> Bool {
        let itemLabel = label(forItem: item)
        let filterLower = filter.lowercased().folding(options: .diacriticInsensitive, locale: .current)
        let itemLabelLower = itemLabel.lowercased().folding(options: .diacriticInsensitive, locale: .current)
        return itemLabelLower.contains(filterLower)
    }
}

