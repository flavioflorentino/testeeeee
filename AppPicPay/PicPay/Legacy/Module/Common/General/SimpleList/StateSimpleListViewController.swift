//
//  StateSimpleListViewController.swift
//  PicPay
//
//  ☝🏽☁️ Created by Luiz Henrique Guimarães on 06/03/18.
//

import UIKit

@objc final class StateSimpleListViewControllerOBJC: NSObject  {
    var controller: UIViewController?
    typealias didSelected = (UIViewController?, SimpleStateModel) -> Void
    var didSelectedAction: didSelected
    
    init(_ didSelected: @escaping didSelected){
        self.didSelectedAction = didSelected
        super.init()
        
        controller = StateSimpleListViewController { [weak self] (controller, item) in
            self?.didSelectedAction(controller as UIViewController, item)
        }
    }
}

final class StateSimpleListViewController: SimpleListViewController<SimpleStateModel> {
    init(_ didSelected: @escaping didSelected){
        super.init(model: StateSimpleListViewModel(), didSelected: didSelected)
    }
}
