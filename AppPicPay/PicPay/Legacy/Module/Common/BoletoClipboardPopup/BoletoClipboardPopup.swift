import Foundation
import UI
import UIKit

final class BoletoClipboardPopup: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var textHeight: NSLayoutConstraint!
    @IBOutlet weak var actionButton: UIPPButton!
    
    var boletoNumber = ""
    
    var buttonTapAction:(()-> Void)?
    var closeTapAction:(()-> Void)?
    
    // MARK: - Initializer
    
    init(boletoNumber: String){
        super.init(nibName: "BoletoClipboardPopup", bundle: Bundle.main)
        self.boletoNumber = boletoNumber
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    // MARK: - View Setup
    private func setupView() {
        let mask = boletoNumber.boletoMask()
        textLabel.text = "\(BilletLocalizable.youHaveCopiedBoletoNumber.text)\n\n\(mask.maskedText(from: boletoNumber) ?? "")"
        
        view.backgroundColor = Palette.ppColorGrayscale000.color
        titleLabel.textColor = Palette.ppColorGrayscale600.color
        textLabel.textColor = Palette.ppColorGrayscale400.color
    }
    
    // MARK: - User Actions
    
    @IBAction private func enableTouchId(_ sender: Any) {
        buttonTapAction?()
    }
    
    @IBAction private func close(_ sender: Any){
        closeTapAction?()
    }
    
}

