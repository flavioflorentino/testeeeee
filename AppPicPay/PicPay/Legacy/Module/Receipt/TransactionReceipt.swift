import Foundation
import UIKit
import PIX

final class TransactionReceipt: NSObject {
    @objc
    static func showReceiptSuccess(viewController: UIViewController, receiptViewModel: ReceiptWidgetViewModel) {
        if receiptViewModel.needsDismissPresentingControllers {
            TransactionReceipt.showReceiptSuccess(
                superview: UIApplication.shared.keyWindow?.rootViewController,
                dismissVC: rootPresentingViewController(viewController),
                receiptViewModel: receiptViewModel
            )
        } else {
            TransactionReceipt.showReceiptSuccess(
                superview: viewController.presentingViewController,
                dismissVC: viewController,
                receiptViewModel: receiptViewModel
            )
        }
    }
    
    @objc
    static func showReceiptSuccess(
        superview: UIViewController?,
        dismissVC: UIViewController,
        receiptViewModel: ReceiptWidgetViewModel
    ) {
        guard let superview = superview else {
            return
        }
        guard let imageScreenshot = takeScreenshot() else {
            return
        }
        superview.view.addSubview(imageScreenshot)
        
        dismissVC.dismiss(animated: false) {
            TransactionReceipt.animationReceiptSuccess(
                viewController: superview,
                imageView: imageScreenshot,
                receiptViewModel: receiptViewModel
            )
        }
    }
    
    @objc
    static func showReceipt(
        viewController: UIViewController,
        receiptViewModel: ReceiptWidgetViewModel,
        feedItemId: String? = nil
    ) {
        let receiptWidgets = ReceiptWidgetViewController(viewModel: receiptViewModel)
        receiptWidgets.feedItemId = feedItemId
        viewController.present(receiptWidgets, animated: true, completion: nil)
    }
    
    private static func takeScreenshot() -> UIImageView? {
        guard let layer = UIApplication.shared.keyWindow?.layer else {
            return nil
        }
        let scale = UIScreen.main.scale
        
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        
        guard let context = UIGraphicsGetCurrentContext() else { return nil}
        layer.render(in: context)
        guard let screenshotImage = UIGraphicsGetImageFromCurrentImageContext() else { return nil}
        UIGraphicsEndImageContext()
        
        let imageView = UIImageView(image: screenshotImage)
        return imageView
    }
    
    private static func animationReceiptSuccess(
        viewController: UIViewController,
        imageView: UIImageView,
        receiptViewModel: ReceiptWidgetViewModel
    ) {
        TaskCompleteAnimation.showSuccess(onView: viewController.view) {
            let receiptViewController = ReceiptWidgetViewController(viewModel: receiptViewModel)
            imageView.removeFromSuperview()
            viewController.present(receiptViewController, animated: true, completion: nil)
        }
    }
    
    private static func rootPresentingViewController(_ controller: UIViewController) -> UIViewController {
        guard let presentingViewController = controller.presentingViewController else { return controller }
        return rootPresentingViewController(presentingViewController)
    }
}

// MARK: - TransactionReceipting
extension TransactionReceipt: TransactionReceipting {
    static func createReceipt(id: String, type: String) -> UIViewController {
        let type = ReceiptWidgetViewModel.ReceiptType(rawValue: type)
        let receiptViewModel = ReceiptWidgetViewModel(transactionId: id, type: type)
        return ReceiptWidgetViewController(viewModel: receiptViewModel)
    }
}
