final class ListReceiptWidgetItem: ReceiptWidgetItem {
    override var type: ReceiptWidgetType {
        return .list
    }
    
    var items: [ListItem]
    
    init?(jsonDict: [AnyHashable: Any]) {
        guard let list = jsonDict["itens"] as? NSArray else {
            return nil
        }
        
        self.items = []
        
        for item in list {
            if let listItem = ListItem(jsonDict: item as? [AnyHashable: Any]) {
                self.items.append(listItem)
            }
        }
        
        super.init()
        self.order = jsonDict["order"] as? NSInteger ?? 0
    }
}
