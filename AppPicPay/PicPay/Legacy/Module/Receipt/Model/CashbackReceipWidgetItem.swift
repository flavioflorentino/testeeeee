import Core
import UI

@objc
final class CashbackReceipWidgetItem: ReceiptWidgetItem {
    enum Action: String {
        case dismmis = "dismiss"
        case NotSpecified = ""
    }
    
    enum IconType: String {
        case asset
        case url
    }
    
    struct Icon {
        var type: IconType
        var value: String
        
        init?(jsonDict: [String: Any]?) {
            guard let type = IconType(rawValue: jsonDict?["type"] as? String ?? ""),
                let value = jsonDict?["value"] as? String else {
                return nil
            }
            self.type = type
            self.value = value
        }
    }
    
    struct ActionButton {
        var action: Action
        var title: String
        var backgroundColor: UIColor?
        var hexColor: String?
        var payload: [String: Any]?
        
        init?(jsonDict: [String: Any]?) {
            guard let title = jsonDict?["title"] as? String,
                let color = jsonDict?["color"] as? String else {
                return nil
            }
            
            self.action = Action(rawValue: jsonDict?["action"] as? String ?? "") ?? .NotSpecified
            self.hexColor = color
            self.backgroundColor = Palette.hexColor(with: color)
            self.title = title
            self.payload = jsonDict?["data"] as? [String: Any]
        }
    }
    
    override var type: ReceiptWidgetType {
        return .cashback
    }
    
    var icon: Icon?
    var title: String
    var titleColor: UIColor?
    var subtitle: String
    var subtitleColor: UIColor?
    var actionButton: ActionButton?
    var jsonDict: [String: Any] = [:]
    
    public init?(jsonDict: [AnyHashable: Any]) {
        guard let title = jsonDict["title"] as? NSDictionary,
            let subtitle = jsonDict["subtitle"] as? NSDictionary,
            let titleStr = title["value"] as? String,
            let subtitleStr = subtitle["value"] as? String else {
            return nil
        }
        
        self.jsonDict = jsonDict as? [String: Any] ?? [:]
        self.title = titleStr
        self.subtitle = subtitleStr
        self.titleColor = Palette.hexColor(with: title["color"] as? String ?? "#4a4a4a")
        self.subtitleColor = Palette.hexColor(with: subtitle["color"] as? String ?? "") ?? Palette.ppColorGrayscale500.color

        if let icon = Icon(jsonDict: jsonDict["icon"] as? [String: Any]) {
            self.icon = icon
        }
        if let button = ActionButton(jsonDict: jsonDict["button"] as? [String: Any]) {
            self.actionButton = button
        }
        
        super.init()
        self.order = jsonDict["order"] as? NSInteger ?? 0
    }
}

extension CashbackReceipWidgetItem {
    /// Open popup for last transaction with cashback
    @objc
    class func openCashbackPopup(in controller: UIViewController) {
        guard let dict = KVStore().dictionaryFor(.cashbackPopup),
            let cashBackWidgetItem = CashbackReceipWidgetItem(jsonDict: dict) else {
            return
        }
        
        let alert = Alert(title: cashBackWidgetItem.title, text: cashBackWidgetItem.subtitle)
        if let icon = cashBackWidgetItem.icon {
            let sourceType = Alert.ImageSourceType(rawValue: icon.type.rawValue) ?? .url
            let image = Alert.Image(name: icon.value, style: .square, source: sourceType)
            alert.image = image
        }
        
        var button = Button(title: ReceiptLocalizable.okayILoveIt.text)
        if let actionButton = cashBackWidgetItem.actionButton {
            button = Button(title: actionButton.title)
            button.backgroundColor = actionButton.hexColor
        }
        alert.buttons = [button]
        AlertMessage.showAlert(alert, controller: controller)
        
        PPAnalytics.trackEvent("Exibiu popup cashback", properties: nil)
        
        // Remove popup cashback after show
        KVStore().removeObjectFor(.cashbackPopup)
    }
    
    @objc
    func setCashbackPopupPedding() {
        KVStore().set(value: jsonDict, with: .cashbackPopup)
    }
}
