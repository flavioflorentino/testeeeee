import UI

final class SubscriptionReceiptWidgetItem: ReceiptWidgetItem {
    var title: String
    var descriptionList: [ListItem]
    var seeMoreButton: SeeDetailsButtonItem?
    
    override var type: ReceiptWidgetType {
        return .subscription
    }
    
    init?(jsonDict: [AnyHashable: Any]) {
        guard let list = jsonDict["list"] as? NSArray else {
            return nil
        }
        
        let titleFallback = ReceiptLocalizable.yourSignature.text
        if let title = jsonDict["title"] as? [AnyHashable: Any] {
            self.title = title["value"] as? String ?? titleFallback
        } else {
            self.title = titleFallback
        }
        
        self.descriptionList = []
        
        for item in list {
            if let listItem = ListItem(jsonDict: item as? [AnyHashable: Any]) {
                self.descriptionList.append(listItem)
            }
        }
        
        self.seeMoreButton = SeeDetailsButtonItem(jsonDict: jsonDict["button"] as? [AnyHashable: Any] ?? [:])
        
        super.init()
        self.order = jsonDict["order"] as? NSInteger ?? 0
    }
}

final class SeeDetailsButtonItem {
    var title: String
    var titleColor: UIColor?
    var subscription: ConsumerSubscription?
    var subscriptionId: String?
    
    init?(jsonDict: [AnyHashable: Any]?) {
        if let data = jsonDict?["data"] as? [AnyHashable: Any] {
            self.subscriptionId = data["subscription_id"] as? String
        } else {
            self.subscriptionId = nil
        }
        
        self.subscription = nil
        self.title = jsonDict?["title"] as? String ?? DefaultLocalizable.seeMoreDetails.text
        self.titleColor = Palette.hexColor(with: jsonDict?["color"] as? String ?? "")
    }
}
