final class TransactionReceiptWidgetItem: ReceiptWidgetItem {
    public enum PayeeType: String {
        case Store = "store"
        case Consumer = "consumer"
        case DigitalGoods = "digitalgoods"
        case Ecommerce = "ecommerce"
        case Default = "default"
    }
    
    struct Payee {
        var type: PayeeType
        var id: String
        var name: String
        var username: String
        var imgUrl: String
        var isVerified: Bool
        var isPro: Bool
        var email = ""
        
        init?(jsonDict: [AnyHashable: Any]?) {
            guard let name = jsonDict?["name"] as? String,
                let id = jsonDict?["id"] as? String else {
                    return nil
            }

            var type: PayeeType
            
            if let payeeType = PayeeType(rawValue: jsonDict?["type"] as? String ?? "") {
                type = payeeType
            } else {
                type = .Default
            }
            
            if type == .Consumer {
                guard let isVerified = jsonDict?["is_verified"] as? Bool,
                    let isPro = jsonDict?["is_pro"] as? Bool,
                    let username = jsonDict?["username"] as? String else {
                        return nil
                }
                self.isPro = isPro
                self.isVerified = isVerified
                self.username = username
            } else {
                self.isPro = false
                self.isVerified = jsonDict?["is_verified"] as? Bool ?? false
                self.username = ""
            }
            
            if let email = jsonDict?["email"] as? String {
                self.email = email
            }
            
            self.id = id
            self.type = type
            self.name = name
            self.imgUrl = jsonDict?["img_url"] as? String ?? ""
        }
    }
    
    struct Receiver {
        let name: String
        let cpf: String
        
        init?(jsonDict: [AnyHashable: Any]?) {
            guard
                let name = jsonDict?["name"] as? String,
                let cpf = jsonDict?["cpf"] as? String
                else {
                    return nil
            }
            self.name = name
            self.cpf = cpf
        }
    }
    
    var id: String
    var date: String
    var isCanceled: Bool
    var total: String
    var payee: Payee
    var list: [ListItem]
    var shareUrl: String?
    var shareText: String?
    var receiver: Receiver?
    
    override var type: ReceiptWidgetType {
        return .transaction
    }
    
    public init?(jsonDict: [AnyHashable: Any]) {
        guard let id = jsonDict["id"] as? String,
            let date = jsonDict["date"] as? String,
            let isCanceled = jsonDict["is_cancelled"] as? Bool,
            let total = jsonDict["total"] as? String,
            let payee = Payee(jsonDict: jsonDict["payee"] as? [AnyHashable: Any]),
            let list = jsonDict["list"] as? NSArray else {
                return nil
        }
        
        self.id = id
        self.date = date
        self.isCanceled = isCanceled
        self.total = total
        self.payee = payee
        self.list = []
        self.receiver = Receiver(jsonDict: jsonDict["receiver"] as? [AnyHashable: Any])
        
        if let share = jsonDict["share"] as? NSDictionary {
            self.shareUrl = share["url"] as? String
            self.shareText = share["text"] as? String
        }
        
        for item in list {
            if let listItem = ListItem(jsonDict: item as? [AnyHashable: Any]) {
                self.list.append(listItem)
            }
        }
        super.init()
        self.order = jsonDict["order"] as? NSInteger ?? 0
    }
}
