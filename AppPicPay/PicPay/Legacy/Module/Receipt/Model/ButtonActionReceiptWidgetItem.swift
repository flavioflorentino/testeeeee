enum ReceiptWidgetButtonActionType: String {
    case newBoleto = "pay_new_boleto"
}

final class ButtonActionReceiptWidgetItem: ReceiptWidgetItem {
    var title: String
    var actionType: ReceiptWidgetButtonActionType
    
    override public var type: ReceiptWidgetType {
        return .buttonAction
    }
    
    public init?(jsonDict: [AnyHashable: Any]) {
        guard let title = jsonDict["title"] as? String,
            let type = jsonDict["action"] as? String else {
                return nil
        }
        
        self.title = title
        
        if let actionType = ReceiptWidgetButtonActionType(rawValue: type) {
            self.actionType = actionType
        } else {
            return nil
        }
        
        super.init()
        self.order = jsonDict["order"] as? NSInteger ?? 0
    }
}
