import UI

final class TransactionStatusReceiptWidgetItem: ReceiptWidgetItem {
    enum Action: String {
        case accelerateAnalysis = "accelerate_analysis"
        case accelerateAnalysisPav = "accelerate_analysis_pav"
        case helpCenter = "helpcenter"
        case deeplink = "deeplink"
        case NotSpecified = ""
    }
    
    enum IconType: String {
        case asset
        case url
    }
    
    struct Icon {
        var type: IconType
        var value: String
        
        init?(jsonDict: [String: Any]?) {
            guard let type = IconType(rawValue: jsonDict?["type"] as? String ?? "") else {
                 return nil
            }
                        guard let value = jsonDict?["value"] as? String else {
                 return nil
            }
            
            self.type = type
            self.value = value
        }
    }
    
    struct ActionButton {
        var action: Action
        var title: String
        var backgroundColor: UIColor?
        var payload: [String: Any]?
        
        init?(jsonDict: [String: Any]?) {
            guard let title = jsonDict?["title"] as? String,
                let color = jsonDict?["color"] as? String,
                let action = Action(rawValue: jsonDict?["action"] as? String ?? "") else {
                    return nil
            }
            
            self.action = action
            self.backgroundColor = Palette.hexColor(with: color)
            self.title = title
            self.payload = jsonDict?["data"] as? [String: Any]
        }
    }
    
    struct ReadMoreButton {
        var action: Action
        var title: String
        var payload: [String: Any]?
        
        init?(jsonDict: [String: Any]?) {
            guard let title = jsonDict?["title"] as? String else {
                 return nil
            }
                        guard let action = Action(rawValue: jsonDict?["action"] as? String ?? "") else {
                 return nil
            }
            
            self.action = action
            self.title = title
            self.payload = jsonDict?["action_data"] as? [String: Any]
        }
    }
    
    override var type: ReceiptWidgetType {
        return .transactionStatus
    }
    
    var icon: Icon?
    var title: String
    var titleColor: UIColor?
    var subtitle: String
    var subtitleColor: UIColor?
    var actionButton: ActionButton?
    var readMoreButton: ReadMoreButton?
    
    public init?(jsonDict: [AnyHashable: Any]) {
        guard let title = jsonDict["title"] as? NSDictionary,
            let subtitle = jsonDict["subtitle"] as? NSDictionary else {
                return nil
        }
        
        guard let titleStr = title["value"] as? String,
            let titleColor = title["color"] as? String,
            let subtitleStr = subtitle["value"] as? String,
            let subtitleColor = subtitle["color"] as? String else {
                return nil
        }
        
        self.title = titleStr
        self.titleColor = Palette.hexColor(with: titleColor)
        self.subtitle = subtitleStr
        self.subtitleColor = Palette.hexColor(with: subtitleColor)
        
        if let button = ActionButton(jsonDict: jsonDict["button"] as? [String: Any]) {
            self.actionButton = button
        }
        if let button = ReadMoreButton(jsonDict: jsonDict["read_more_button"] as? [String: Any]) {
            self.readMoreButton = button
        }
        if let icon = Icon(jsonDict: jsonDict["icon"] as? [String: Any]) {
            self.icon = icon
        }
        
        super.init()
        self.order = jsonDict["order"] as? NSInteger ?? 0
    }
}
