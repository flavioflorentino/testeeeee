final class OpenUrlReceiptWidgetItem: ReceiptWidgetItem {
    var title: String
    var url: String
    
    override var type: ReceiptWidgetType {
        return .openUrl
    }
    
    init?(jsonDict: [AnyHashable: Any]?) {
        guard let title = jsonDict?["title"] as? String,
            let url = jsonDict?["url"] as? String else {
            return nil
        }
        
        self.title = title
        self.url = url
        
        super.init()
        self.order = jsonDict?["order"] as? NSInteger ?? 0
    }
}
