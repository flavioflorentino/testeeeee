import Foundation

final class PixTransactionReceiptWidgetItem: ReceiptWidgetItem {
    let header: Header
    let consumerId: Int
    let total: String
    let isCancelled: Bool
    let body: [[SectionInfo]]
    let list: [ListItem]
    
    override var type: ReceiptWidgetType {
        .pixTransaction
    }

    public init?(jsonDict: [AnyHashable: Any]) {
        guard
            let header = Header(jsonDict: jsonDict["header"] as? [AnyHashable: Any]),
            let consumerId = jsonDict["user_id"] as? Int,
            let total = jsonDict["total"] as? String,
            let isCancelled = jsonDict["is_cancelled"] as? Bool,
            let bodyJson = jsonDict["body"] as? [[[AnyHashable: Any]]],
            let listItems = jsonDict["list"] as? NSArray
            else {
                return nil
        }
        self.header = header
        self.consumerId = consumerId
        self.total = total
        self.isCancelled = isCancelled
        self.body = bodyJson.map({ $0.compactMap(PixTransactionReceiptWidgetItem.createSectionItems) })
        self.list = listItems.compactMap(PixTransactionReceiptWidgetItem.createListItem)
        super.init()
    }
    
    private static func createListItem(item: Any) -> ListItem? {
        ListItem(jsonDict: item as? [AnyHashable: Any])
    }
    
    private static func createSectionItems(item: [AnyHashable: Any]) -> SectionInfo? {
        SectionInfo(jsonDict: item)
    }
}

extension PixTransactionReceiptWidgetItem {
    struct Header {
        let imgUrl: String
        let title: String
        let id: String
        let date: String
        
        init?(jsonDict: [AnyHashable: Any]?) {
            guard
                let jsonDict = jsonDict,
                let imgUrl = jsonDict["img_url"] as? String,
                let title = jsonDict["title"] as? String,
                let id = jsonDict["id"] as? String,
                let date = jsonDict["date"] as? String
                else {
                    return nil
            }
            self.imgUrl = imgUrl
            self.title = title
            self.id = id
            self.date = date
        }
    }
    
    
    
    struct SectionInfo {
        let title: String
        let list: [String]
        
        init?(jsonDict: [AnyHashable: Any]?) {
            guard
                let jsonDict = jsonDict,
                let title = jsonDict["title"] as? String,
                let list = jsonDict["list"] as? [String]
                else {
                    return nil
            }
            self.title = title
            self.list = list
        }
    }
}
