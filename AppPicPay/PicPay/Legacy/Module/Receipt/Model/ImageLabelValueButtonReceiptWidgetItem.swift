import SwiftyJSON

final class ImageLabelValueButtonReceiptWidgetItem: ReceiptWidgetItem {
    let imageUrl: String?
    let title: Text
    let text: Text
    let button: Button?
    private let typeOfReceipt: ReceiptWidgetType
    
    override var type: ReceiptWidgetType {
        return typeOfReceipt
    }
    
    public init?(jsonDict: [AnyHashable: Any], type: ReceiptWidgetType) {
        guard let titleDic = jsonDict["title"],
            let title = Text(json: JSON(titleDic)),
            let textDict = jsonDict["text"],
            let text = Text(json: JSON(textDict)) else {
                return nil
        }
        
        imageUrl = jsonDict["img_url"] as? String
        typeOfReceipt = type
        self.title = title
        self.text = text
        
        if let buttonDict = jsonDict["button"] {
            self.button = Button(json: JSON(buttonDict))
        } else {
            self.button = nil
        }
        
        super.init()
        self.order = jsonDict["order"] as? NSInteger ?? 0
    }
}
