import UI
import UIKit

final class FaqReceiptWidgetItem: ReceiptWidgetItem {
    var label: String
    var labelColor: UIColor
    var details: String
    var detailsColor: UIColor
    var url: String
    
    override var type: ReceiptWidgetType {
        .faq
    }
    
    public init?(jsonDict: [AnyHashable: Any]) {
        guard
            let labelDict = jsonDict["label"] as? [String: String],
            let label = labelDict["value"],
            let labelColor = labelDict["color"],
            let detailsDict = jsonDict["value"] as? [String: String],
            let details = detailsDict["value"],
            let detailsColor = detailsDict["color"],
            let url = detailsDict["url"]
            else {
                return nil
        }
        
        self.label = label
        self.details = details
        self.labelColor = Palette.hexColor(with: labelColor) ?? Palette.ppColorGrayscale600.color
        self.detailsColor = Palette.hexColor(with: detailsColor) ?? Palette.ppColorGrayscale600.color
        self.url = url
        
        super.init()
        
        self.order = jsonDict["order"] as? NSInteger ?? 0
    }
}
