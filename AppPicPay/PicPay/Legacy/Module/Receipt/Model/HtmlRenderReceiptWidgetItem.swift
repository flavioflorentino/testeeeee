final class HtmlRenderReceiptWidgetItem: ReceiptWidgetItem {
    var content: String
    
    var isUrl: Bool {
        return content.hasPrefix("http")
    }
    
    override var type: ReceiptWidgetType {
        return .webview
    }
    
    init?(jsonDict: [AnyHashable: Any]) {
        self.content = jsonDict["content"] as? String ?? ""
        super.init()
        self.order = jsonDict["order"] as? NSInteger ?? 0
    }
}
