import UI

final class ExpiryAtReceiptWidgetItem: ReceiptWidgetItem {
    var title: String
    var titleColor: UIColor?
    var text: String
    var date: String
    var datetime: Int
    
    override var type: ReceiptWidgetType {
        return .expiryAt
    }
    
    init?(jsonDict: [AnyHashable: Any]) {
        guard let title = jsonDict["title"] as? NSDictionary,
            let titleStr = title["value"] as? String,
            let date = jsonDict["date"] as? String,
            let datetime = jsonDict["datetime"] as? Int,
            let text = jsonDict["text"] as? String else {
                return nil
        }
        
        self.title = titleStr
        self.text = text
        self.date = date
        self.datetime = datetime
        self.titleColor = Palette.hexColor(with: title["color"] as? String ?? "")
        
        super.init()
        self.order = jsonDict["order"] as? NSInteger ?? 0
    }
}
