import UI

final class VoucherReceiptWidgetItem: ReceiptWidgetItem {
    struct Action {
        var title: String
        var url: String
        var isAuthenticated: Bool
        
        init?(jsonDict: [AnyHashable: Any]?) {
            guard let title = jsonDict?["title"] as? String,
                let url = jsonDict?["action"] as? String else {
                    return nil
            }
            
            self.title = title
            self.url = url
            self.isAuthenticated = false
        }
    }
    
    override var type: ReceiptWidgetType {
        return .voucher
    }
    
    var title: String
    var titleColor: UIColor?
    var voucher: String
    var button: Action?
    var items: [ListItem]
    
    public init?(jsonDict: [AnyHashable: Any]) {
        guard let title = jsonDict["title"] as? NSDictionary,
            let voucher = jsonDict["voucher"] as? String else {
                return nil
        }
        
        guard let titleStr = title["value"] as? String else {
            return nil
        }
        
        self.title = titleStr
        self.titleColor = Palette.hexColor(with: title["color"] as? String ?? "")
        self.voucher = voucher
        self.button = Action(jsonDict: jsonDict["button"] as? [AnyHashable: Any])
        self.items = []
        
        super.init()
        self.order = jsonDict["order"] as? NSInteger ?? 0
    }
}
