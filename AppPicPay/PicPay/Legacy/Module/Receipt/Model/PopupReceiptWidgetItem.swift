import SwiftyJSON

final class PopupReceiptWidgetItem: ReceiptWidgetItem {
    enum PopupDisplayEvent: String {
        case onStart
    }
    let displayEvent: PopupDisplayEvent
    let alert: Alert?
    
    override var type: ReceiptWidgetType {
        return .popup
    }
    
    init?(jsonDict: [AnyHashable: Any]) {
        guard let displayEventString = jsonDict["display_event"] as? String,
            let displayEvent = PopupDisplayEvent(rawValue: displayEventString),
            let ui = jsonDict["_ui"] as? NSDictionary,
            let alertDict = ui["alert"] as? [String: Any],
            let alert = Alert(json: JSON(alertDict))
            else {
             return nil
        }
        self.displayEvent = displayEvent
        self.alert = alert
    }
}
