import UI
import UIKit

class InfoReceiptWidgetView: UIView, ReceiptPresentable {
    var viewDictionary = [String: UIView]()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Title"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        label.textAlignment = .center
        
        return label
    }()
    
    lazy var mainContentLabel: UILabel = {
        let label = UILabel()
        label.text = "Main Content"
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 17)
        label.textColor = #colorLiteral(red: 33/255, green: 194/255, blue: 94/255, alpha: 1)
        
        return label
    }()
    
    lazy var mainContentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var dashedLayer: CAShapeLayer = {
        let layer = CAShapeLayer()
        layer.strokeColor = #colorLiteral(red: 0.73, green: 0.73, blue: 0.73, alpha: 1.0).cgColor
        layer.fillColor = nil
        layer.lineDashPattern = [4, 4]
        return layer
    }()
    
    var title: String? {
        get { return titleLabel.text }
        set(title) { titleLabel.text = title }
    }
    
    var titleColor: UIColor {
        get { return titleLabel.textColor }
        set(color) { titleLabel.textColor = color }
    }
    
    var content: String? {
        get { return mainContentLabel.text }
        set(contentStr) { mainContentLabel.text = contentStr }
    }
    
    // MARK: ReceiptPresentable
    var contentHeight: CGFloat {
        var contentSize: CGFloat = 0
        contentSize += CGFloat.vMargin * 2
        contentSize += titleLabel.intrinsicContentSize.height
        contentSize += .vSpacing
        contentSize += .mainContentSize
        
        return contentSize
    }
    
    // MARK: UIView
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    // MARK: InfoReceiptWidgetView
    func initialize() {
        backgroundColor = Palette.ppColorGrayscale000.color
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(titleLabel)
        addSubview(mainContentView)
        
        executeMainConstraints()
        mainContentView.layer.addSublayer(dashedLayer)
        configureMainContentView()
        drawBottomLine(withThickness: 1, on: self)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateSizeAndDashedForm()
    }
    
    private func updateSizeAndDashedForm() {
        let cornerRadius = mainContentView.bounds.height / 2
        dashedLayer.path = UIBezierPath(roundedRect: mainContentView.bounds, cornerRadius: cornerRadius).cgPath
        dashedLayer.frame = mainContentView.bounds
        mainContentView.layer.cornerRadius = cornerRadius
    }
    
    func executeMainConstraints() {
        let constraintsMetrics: [String: CGFloat] = [
            "hMargin": .hMargin,
            "vMargin": .vMargin,
            "vSpacing": .vSpacing,
            "mainContentSize": .mainContentSize
        ]
        
        viewDictionary["mainContent"] = mainContentView
        viewDictionary["titleLabel"] = titleLabel
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: .vMargin)
        ])
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-hMargin-[titleLabel]-hMargin-|",
            options: NSLayoutConstraint.FormatOptions(rawValue: 0),
            metrics: constraintsMetrics,
            views: viewDictionary)
        )
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: mainContentView, attribute: .top, relatedBy: .equal, toItem: titleLabel, attribute: .bottom, multiplier: 1, constant: .vSpacing),
            NSLayoutConstraint(item: mainContentView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: .mainContentSize)
        ])
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-hMargin-[mainContent]-hMargin-|",
            options: NSLayoutConstraint.FormatOptions(rawValue: 0),
            metrics: constraintsMetrics,
            views: viewDictionary)
        )
    }
    
    func configureMainContentView() {
        viewDictionary["contentLabel"] = mainContentLabel
        mainContentView.addSubview(mainContentLabel)
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: mainContentLabel, attribute: .centerY, relatedBy: .equal, toItem: mainContentView, attribute: .centerY, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: mainContentLabel, attribute: .centerX, relatedBy: .equal, toItem: mainContentView, attribute: .centerX, multiplier: 1, constant: 0)
        ])
    }
    
    func contentView() -> UIView {
        return self
    }
}

private extension CGFloat {
    static let hMargin: CGFloat = 20
    static let vMargin: CGFloat = 18
    static let vSpacing: CGFloat = 14
    static let mainContentSize: CGFloat = 44
}
