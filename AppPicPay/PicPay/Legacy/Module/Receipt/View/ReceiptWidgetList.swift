import UI

public struct ReceiptWidgetListRowLabels {
    let title: UILabel
    let value: UILabel
    
    init() {
        title = UILabel()
        title.translatesAutoresizingMaskIntoConstraints = false
        title.textAlignment = .left
        
        value = UILabel()
        value.translatesAutoresizingMaskIntoConstraints = false
        value.textAlignment = .right
    }
}

public protocol ReceiptWidgetListDelegate: AnyObject {
    typealias RowLabels = ReceiptWidgetListRowLabels
    /**
     Permite a alteração da formatação padrão das labels
     */
    func configureRowLabels(_ labels: RowLabels, identifier: String?)
}

extension ReceiptWidgetListDelegate {
    /**
     Implementação padrão da formatação das labels
     */
    public func configureRowLabels(_ labels: RowLabels, identifier: String?) {
        var attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 13),
            .strikethroughColor: #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 0.5),
            .foregroundColor: #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1.0)
        ]
        
        labels.title.attributedText = NSAttributedString(string: labels.title.text ?? "", attributes: attributes)
        
        attributes[.font] = UIFont.systemFont(ofSize: 13, weight: .bold)
        //http://www.openradar.appspot.com/31034683
        //attributes[NSKernAttributeName] = -0.3
        
        labels.value.attributedText = NSAttributedString(string: labels.value.text ?? "", attributes: attributes)
    }
}

final class ReceiptWidgetList: UIView, ReceiptWidgetListDelegate {
    enum Metric: String {
        case hMargin
        case topMargin
        case bottomMargin
        case hRowSpacing
        case vRowSpacing
        case hContentExtraMargin
    }
    
    enum BorderOption: String {
        case top
        case bottom
    }
    
    weak var delegate: ReceiptWidgetListDelegate?
    
    private var constraintsMetrics: [String: Float] = [
        Metric.hMargin.rawValue: 0,
        Metric.topMargin.rawValue: 0,
        Metric.hContentExtraMargin.rawValue: 0,
        Metric.bottomMargin.rawValue: 0,
        Metric.hRowSpacing.rawValue: 10,
        Metric.vRowSpacing.rawValue: 10
    ]
    
    private var borders = [String: UIView]()
    private var viewDictionary = [String: UIView]()
    private var rows: [RowLabels] = []
    
    var identifier: String?
    
    var rowsCount: Int {
        return rows.count
    }
    
    var contentHeight: CGFloat {
        var height: Float = 0
        
        if let spacing = constraintsMetrics[Metric.vRowSpacing.rawValue], rows.count > 1 {
            height += spacing * Float(rowsCount - 1)
        }
        
        height += constraintsMetrics[Metric.topMargin.rawValue] ?? 0
        height += constraintsMetrics[Metric.bottomMargin.rawValue] ?? 0
        
        if let label = rows.first?.title {
            height += Float(label.intrinsicContentSize.height * CGFloat(rowsCount))
        }
        
        return CGFloat(height)
    }
    
    convenience init(metrics: [Metric: Float]) {
        self.init()
        for (metric, value) in metrics {
            constraintsMetrics[metric.rawValue] = value
        }
    }
    
    private func createRow(insideView container: UIView) -> RowLabels {
        let labels = ReceiptWidgetListRowLabels()
        
        container.addSubview(labels.title)
        container.addSubview(labels.value)
        
        return labels
    }
    
    private func executeConstraintsForRowLabels(_ labels: RowLabels, rowNumber: Int) {
        let rowTitle = "rowTitle_\(rowNumber)"
        let rowValue = "rowValue_\(rowNumber)"
        let visualVerticalConstraints: String
        
        if rowNumber > 1 {
            let previousRow = "rowTitle_\(rowNumber - 1)"
            visualVerticalConstraints = "V:[\(previousRow)]-vRowSpacing-[\(rowTitle)]"
        } else {
            visualVerticalConstraints = "V:|-topMargin-[\(rowTitle)]"
        }
        
        viewDictionary[rowTitle] = labels.title
        viewDictionary[rowValue] = labels.value
        
        let hMargin = (constraintsMetrics[Metric.hMargin.rawValue] ?? 0) + (constraintsMetrics[Metric.hContentExtraMargin.rawValue] ?? 0)
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-\(hMargin)-[\(rowTitle)]-hRowSpacing-[\(rowValue)]-\(hMargin)-|",
            options: NSLayoutConstraint.FormatOptions(rawValue: 0),
            metrics: constraintsMetrics,
            views: viewDictionary
        ))
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: labels.value, attribute: .centerY, relatedBy: .equal, toItem: labels.title, attribute: .centerY, multiplier: 1, constant: 0)
        ])
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(
            withVisualFormat: visualVerticalConstraints,
            options: NSLayoutConstraint.FormatOptions(rawValue: 0),
            metrics: constraintsMetrics,
            views: viewDictionary
        ))
    }
    
    func setBorderOptions(_ options: [BorderOption], withColor borderColor: UIColor, andThickness borderThickness: CGFloat) {
        let allOptions: [BorderOption] = [.top, .bottom]
        var borderConstraints: [NSLayoutConstraint] = []
        let constraintAttributeFor: [BorderOption: NSLayoutConstraint.Attribute] = [
            .top: .top,
            .bottom: .bottom
        ]
        
        for option in allOptions {
            if options.contains(option) && borders[option.rawValue] == nil {
                let border = UIView()
                let attribute = constraintAttributeFor[option] ?? .notAnAttribute
                border.translatesAutoresizingMaskIntoConstraints = false
                border.backgroundColor = borderColor
                border.layer.cornerRadius = borderThickness / 2
                addSubview(border)
                borders[option.rawValue] = border
                
                borderConstraints.append(contentsOf: [
                    NSLayoutConstraint(item: border, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: attribute, multiplier: 1, constant: 0),
                    NSLayoutConstraint(item: border, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: borderThickness)
                ])
                
                borderConstraints.append(contentsOf: NSLayoutConstraint.constraints(
                    withVisualFormat: "H:|-(hMargin)-[\(option.rawValue)]-(hMargin)-|",
                    options: [],
                    metrics: constraintsMetrics,
                    views: borders
                ))
                
                NSLayoutConstraint.activate(borderConstraints)
            } else {
                borders[option.rawValue]?.removeFromSuperview()
            }
        }
    }
    
    @discardableResult
    public func addRow(
            title: String?,
            value: String?,
            titleTextColor: UIColor? = nil,
            valueTextColor: UIColor? = nil
        ) -> RowLabels {
        let rowLabels = createRow(insideView: self)
        rows.append(rowLabels)
        
        rowLabels.title.text = title
        rowLabels.value.text = value
        
        //Se não houver alguma final classe que implemente o delegate
        if delegate == nil {
            //utiliza implementação padrão
            configureRowLabels(rowLabels, identifier: identifier)
        } else {
            delegate?.configureRowLabels(rowLabels, identifier: identifier)
        }
        rowLabels.title.textColor = Palette.ppColorGrayscale400.color
        rowLabels.value.textColor = Palette.ppColorGrayscale400.color
        
        if let titleTextColor = titleTextColor {
            rowLabels.title.textColor = titleTextColor
        }
        
        if let valueTextColor = valueTextColor {
            rowLabels.value.textColor = valueTextColor
        }
        
        executeConstraintsForRowLabels(rowLabels, rowNumber: rows.count)
        return rowLabels
    }
}
