import UI

extension ImageLabelValueButtonReceiptWidgetView.Layout {
    enum Size {
        static let imageView = CGSize(width: 137, height: 82)
        static let button = CGSize(width: 185, height: 48)
    }
    
    enum Font {
        static let title = UIFont.systemFont(ofSize: 22, weight: .bold)
        static let text = UIFont.systemFont(ofSize: 13)
        static let buttonItem = UIFont.systemFont(ofSize: 15.0, weight: .medium)
    }
}

final class ImageLabelValueButtonReceiptWidgetView: UIView {
    // MARK: - Layout
    fileprivate enum Layout {}
    
    // MARK: - Visual Components
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textAlignment = .center
        label.font = Layout.Font.title
        return label
    }()
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textAlignment = .center
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.font = Layout.Font.text
        return label
    }()
    
    private lazy var button: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = Layout.Size.button.height / 2
        button.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var stackViewContent: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var imageView = UIImageView()
    
    // MARK: - Variables
    private var item: ImageLabelValueButtonReceiptWidgetItem?
    /// parent is necessary to perform navigation
    private weak var parent: UIViewController?
    private lazy var viewSpaces: CGFloat = {
        var spaces = Spacing.base01
        if item?.imageUrl != nil {
            spaces += Spacing.base01
        }
        if item?.button != nil {
            spaces += Spacing.base02
        }
        return spaces
    }()
    
    // MARK: - Life Cycle
    init(_ item: ImageLabelValueButtonReceiptWidgetItem, parent: UIViewController) {
        super.init(frame: .zero)
        self.parent = parent
        self.item = item
        translatesAutoresizingMaskIntoConstraints = false
        buildLayout(item: item)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func buildLayout(item: ImageLabelValueButtonReceiptWidgetItem) {
        buildViewHierarchy(item: item)
        configureViews(item: item)
        setupConstraints()
    }
    
    private func buildViewHierarchy(item: ImageLabelValueButtonReceiptWidgetItem) {
        addSubview(stackViewContent)
        handleImage(imageUrl: item.imageUrl)
        stackViewContent.addArrangedSubview(titleLabel)
        stackViewContent.addArrangedSubview(textLabel)
        handleButton(buttonItem: item.button)
    }
    
    private func configureViews(item: ImageLabelValueButtonReceiptWidgetItem) {
        backgroundColor = Palette.ppColorGrayscale000.color
        drawBottomLine(withThickness: 1, on: self)
        
        titleLabel.text = item.title.text
        titleLabel.textColor = item.title.color
        
        textLabel.text = item.text.text
        textLabel.textColor = item.text.color
    }
    
    private func setupConstraints() {
        stackViewContent.setSpacing(Spacing.base01, after: titleLabel)
        
        stackViewContent.layout {
            $0.top == topAnchor + Spacing.base03
            $0.leading == leadingAnchor + Spacing.base03
            $0.trailing == trailingAnchor - Spacing.base03
        }
    }
    
    private func handleImage(imageUrl: String?) {
        guard
            let imageUrl = imageUrl,
            let url = URL(string: imageUrl)
            else {
                return
        }
        stackViewContent.addArrangedSubview(imageView)
        stackViewContent.setSpacing(Spacing.base01, after: imageView)
        imageView.layout {
            $0.width == Layout.Size.imageView.width
            $0.height == Layout.Size.imageView.height
        }
        imageView.setImage(url: url)
    }
    
    private func handleButton(buttonItem: Button?) {
        guard let buttonItem = buttonItem else {
            return
        }
        stackViewContent.addArrangedSubview(button)
        stackViewContent.setSpacing(Spacing.base02, after: textLabel)
        button.layout {
            $0.leading == stackViewContent.leadingAnchor
            $0.trailing == stackViewContent.trailingAnchor
            $0.height == Layout.Size.button.height
        }
        button.configure(with: buttonItem, font: Layout.Font.buttonItem)
    }
    
    // MARK: - Action
    @objc
    func buttonAction(sender: UIPPButton) {
        if item?.type == .mgmOffer {
            if let controller = ViewsManager.peopleSearchStoryboardViewController("SocialShareCode") as? SocialShareCodeViewController {
                controller.mgmType = .user
                if let button = item?.button, let actionData = button.actionData?["event_properties"] as? [AnyHashable: Any] {
                    PPAnalytics.trackEvent("MGM Recibo", properties: actionData)
                }
                parent?.present(DismissibleNavigationViewController(rootViewController: controller), animated: true)
            }
        }
        /// action for receipt credit invoice
        if let button = item?.button, button.action == .deeplink {
            if let actionData = button.actionData?["event_properties"] as? [AnyHashable: Any] {
                PPAnalytics.trackEvent("MGM Recibo", properties: actionData)
            }
            if let urlString: String = button.actionData?["url"] as? String, let url = URL(string: urlString) {
                parent?.dismiss(animated: true, completion: {
                    DeeplinkHelper.handleDeeplink(withUrl: url)
                })
            }
        }
    }
    
    
}

// MARK: - ReceiptPresentable
extension ImageLabelValueButtonReceiptWidgetView: ReceiptPresentable {
    var contentHeight: CGFloat {
        layoutIfNeeded()
        let verticalMargins = Spacing.base03 * 2
        return verticalMargins + stackViewContent.frame.height
    }
    
    func contentView() -> UIView {
        return self
    }
}
