import UI

final class LabelValueReceiptWidgetView: UIView {
    private let mainLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .bold)
        return label
    }()
    
    private let detailsLabel: UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 13)
        return label
    }()
    
    private var item: LabelValueReceiptWidgetItem?
    private let margin: CGFloat = 20
    
    convenience init(_ item: LabelValueReceiptWidgetItem) {
        self.init(frame: .zero)
        self.item = item
        
        translatesAutoresizingMaskIntoConstraints = false
        setupViews()
        setupConstraints()
    }
    
    private func setupViews() {
        drawBottomLine(withThickness: 1, on: self)
        backgroundColor = Palette.ppColorGrayscale000.color
        
        setupLabels(with: item)

        addSubview(mainLabel)
        addSubview(detailsLabel)
    }
    
    private func setupLabels(with item: LabelValueReceiptWidgetItem?) {
        guard let item = item else {
            return
        }
        let labelsWidth = UIScreen.main.bounds.width - margin * 2
        
        mainLabel.text = item.label
        mainLabel.textColor = item.labelColor
        mainLabel.preferredMaxLayoutWidth = labelsWidth
        
        detailsLabel.text = item.details
        detailsLabel.textColor = item.detailsColor
        detailsLabel.preferredMaxLayoutWidth = labelsWidth
    }
    
    private func setupConstraints() {
        mainLabel.snp.makeConstraints { make in
            make.left.equalTo(margin)
            make.top.equalTo(margin)
        }
        
        detailsLabel.snp.makeConstraints { make in
            make.left.equalTo(margin)
            make.top.equalTo(mainLabel.snp.bottom).offset(5)
        }
    }
}

extension LabelValueReceiptWidgetView: ReceiptPresentable {
    var contentHeight: CGFloat {
        var height = margin * 2
        height += mainLabel.intrinsicContentSize.height + detailsLabel.intrinsicContentSize.height
        
        return height
    }
    
    func contentView() -> UIView {
        return self
    }
}
