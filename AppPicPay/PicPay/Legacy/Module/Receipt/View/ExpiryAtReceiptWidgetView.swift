final class ExpiryAtReceiptWidgetView: InfoReceiptWidgetView {
    lazy private var subtitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = #colorLiteral(red: 0.29, green: 0.29, blue: 0.29, alpha: 1.0)
        label.textAlignment = .center
        label.text = "Subtitle"
        
        return label
    }()
    
    lazy private var iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    var subtitle: String? {
        get { return subtitleLabel.text }
        set(subtitle) { subtitleLabel.text = subtitle }
    }
    
    var icon: UIImage? {
        get { return iconView.image }
        set(icon) { iconView.image = icon }
    }
    
    override var contentHeight: CGFloat {
        var contentHeight = super.contentHeight
        contentHeight += subtitleLabel.intrinsicContentSize.height
        contentHeight += .subtitleSpacing
        
        return contentHeight
    }
    
    override func initialize() {
        addSubview(subtitleLabel)
        icon = #imageLiteral(resourceName: "iconGreenClock")
        super.initialize()
    }
    
    convenience init(_ item: ExpiryAtReceiptWidgetItem) {
        self.init(frame: .zero)
        title = item.title
        if let color = item.titleColor {
            titleColor = color
        }
        subtitle = item.text
        content = item.date
    }
    
    override func executeMainConstraints() {
        super.executeMainConstraints()
        viewDictionary["subtitleLabel"] = subtitleLabel
        
        let constraintsMetrics: [String: CGFloat] = [
            "hMargin": .hMargin,
            "vMargin": .vMargin,
            "vSpacing": .vSpacing,
            "mainContentSize": .mainContentSize,
            "subtitleSpacing": .subtitleSpacing,
            "iconSize": .iconSize,
            "hIconSpacing": .hIconSpacing
        ]
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: subtitleLabel, attribute: .top, relatedBy: .equal, toItem: viewDictionary["mainContent"], attribute: .bottom, multiplier: 1, constant: .subtitleSpacing),
            NSLayoutConstraint(item: subtitleLabel, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: .hMargin)
        ])
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-hMargin-[subtitleLabel]-hMargin-|",
            options: NSLayoutConstraint.FormatOptions(rawValue: 0),
            metrics: constraintsMetrics,
            views: viewDictionary
        ))
    }
    
    override func configureMainContentView() {
        super.configureMainContentView()
        viewDictionary["iconView"] = iconView
        mainContentView.addSubview(iconView)
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: iconView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: .iconSize),
            NSLayoutConstraint(item: iconView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: .iconSize),
            NSLayoutConstraint(item: iconView, attribute: .centerY, relatedBy: .equal, toItem: mainContentView, attribute: .centerY, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: mainContentLabel, attribute: .left, relatedBy: .equal, toItem: iconView, attribute: .right, multiplier: 1, constant: .hIconSpacing)
        ])
        
        if let constraint = mainContentView.constraints.first(where: { $0.firstAttribute == .centerX }) {
            constraint.constant = (CGFloat.iconSize + CGFloat.hIconSpacing) / CGFloat(2)
        }
    }
}

private extension CGFloat {
    static let subtitleSpacing: CGFloat = 16
    static let iconSize: CGFloat = 16
    static let hIconSpacing: CGFloat = 8
    static let hMargin: CGFloat = 20
    static let vMargin: CGFloat = 18
    static let vSpacing: CGFloat = 14
    static let mainContentSize: CGFloat = 44
}
