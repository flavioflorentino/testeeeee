import UI

final class StoreConfirmationReceiptWidgetView: UIView {
    private let profileImage = UIPPProfileImage()
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .title
        label.font = UIFont.boldSystemFont(ofSize: .titleFontSize)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        return label
    }()
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .description
        label.font = UIFont.systemFont(ofSize: .descriptionFontSize)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        return label
    }()
    
    private var store = PPStore()
    private var item: TransactionReceiptWidgetItem?
    private var dataView: ReceiptWidgetList?
    
    convenience init(_ item: TransactionReceiptWidgetItem) {
        self.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        store = PPStore()
        self.item = item
        store.img_url = item.payee.imgUrl

        setupViews()
        setupConstraints()
    }
    
    private func setupViews() {
        let labelsWidth = UIScreen.main.bounds.width - .horizontalMargin * 2
        
        profileImage.setStore(store)
        addSubview(profileImage)
        
        titleLabel.text = "\(ReceiptLocalizable.youBoughtIn.text) \(store.name ?? "Nerdstore")"
        titleLabel.preferredMaxLayoutWidth = labelsWidth
        addSubview(titleLabel)
        
        setupDescription()
        descriptionLabel.preferredMaxLayoutWidth = labelsWidth
        addSubview(descriptionLabel)
        
        setupDataView()
    }
    
    private func setupDescription() {
        let email = item?.payee.email ?? ""
        
        let mutableStr = NSMutableAttributedString(string: .descriptionStart + email + .descriptionEnd)
        mutableStr.addAttributes([
            .font: UIFont.boldSystemFont(ofSize: .descriptionFontSize),
            .foregroundColor: Palette.ppColorBranding300.color
        ], range: NSRange(location: String.descriptionStart.length, length: email.length))
        
        descriptionLabel.attributedText = mutableStr
    }
    
    private func setupDataView() {
        dataView = ReceiptWidgetList(metrics: [
            .hMargin: Float(.horizontalMargin - 10),
            .hContentExtraMargin: 1,
            .topMargin: 8,
            .bottomMargin: 8
        ])
        
        dataView?.setBorderOptions([.top], withColor: #colorLiteral(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0), andThickness: 1)
        
        if let item = item,
            let data = dataView {
            for itemList in item.list {
                data.addRow(title: itemList.label, value: itemList.value)
            }
            
            addSubview(data)
        }
    }
    
    private func setupConstraints() {
        profileImage.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(CGFloat.profileTop)
            make.size.equalTo(CGSize(width: .profileHeight, height: .profileHeight))
        }
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(profileImage.snp.bottom).offset(CGFloat.titleTop)
            make.left.equalToSuperview().offset(CGFloat.horizontalMargin)
            make.right.equalToSuperview().offset(-CGFloat.horizontalMargin)
        }
        
        descriptionLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(CGFloat.descriptionTop)
            make.left.equalToSuperview().offset(CGFloat.horizontalMargin)
            make.right.equalToSuperview().offset(-CGFloat.horizontalMargin)
        }
        
        dataView?.snp.makeConstraints({ make in
            make.top.equalTo(descriptionLabel.snp.bottom).offset(CGFloat.dataTop)
            make.left.equalToSuperview().offset(CGFloat.horizontalMargin)
            make.right.equalToSuperview().offset(-CGFloat.horizontalMargin)
        })
    }
}

extension StoreConfirmationReceiptWidgetView: ReceiptPresentable {
    var contentHeight: CGFloat {
        var height: CGFloat = .profileTop + .titleTop + .descriptionTop + .descriptionBottom + .profileHeight + 16
        height += titleLabel.intrinsicContentSize.height + descriptionLabel.intrinsicContentSize.height
        height += dataView?.contentHeight ?? 0.0
        return height
    }
    
    func contentView() -> UIView {
        return self
    }
}

private extension CGFloat {
    static let descriptionFontSize: CGFloat = 16
    static let titleFontSize: CGFloat = 20
    static let profileTop: CGFloat = 40
    static let titleTop: CGFloat = 21
    static let dataTop: CGFloat = 24
    static let descriptionTop: CGFloat = 10
    static let descriptionBottom: CGFloat = 16
    static let profileHeight: CGFloat = 100
    static let horizontalMargin: CGFloat = 20
}

private extension String {
    static let descriptionStart = ReceiptLocalizable.descriptionStart.text
    static let descriptionEnd = ReceiptLocalizable.descriptionEnd.text
}

private extension UIColor {
    static let title = Palette.hexColor(with: "4a4a4a") ?? UIColor()
    static let description = Palette.hexColor(with: "666666") ?? UIColor()
}
