import UI

final class ReceiverReceiptWidgetView: UIView {
    // MARK: - Visual Components
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = ExternalTransferLocalizable.receiverData.text
        return label
    }()
    
    private lazy var nameLabel = UILabel()
    
    private lazy var cpfLabel = UILabel()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    // MARK: - Life Cycle
    convenience init() {
        self.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        buildLayout()
    }
    
    // MARK: - Content
    func setContent(item: TransactionReceiptWidgetItem.Receiver) {
        nameLabel.text = item.name
        cpfLabel.text = String(format: ExternalTransferLocalizable.cpf.text, item.cpf)
    }
}

// MARK: - ViewConfiguration
extension ReceiverReceiptWidgetView: ViewConfiguration {
    func configureStyles() {
        titleLabel
            .labelStyle(CaptionLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        
        nameLabel
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale700())
        
        cpfLabel
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale700())
    }
    
    func setupConstraints() {
        contentStackView.snp.makeConstraints {
            $0.top.equalTo(Spacing.base02)
            $0.bottom.equalTo(-Spacing.base02)
            $0.left.equalTo(Spacing.base03)
            $0.right.equalTo(-Spacing.base03)
        }
    }
    
    func buildViewHierarchy() {
        addSubview(contentStackView)
        contentStackView.addArrangedSubview(titleLabel)
        contentStackView.addArrangedSubview(nameLabel)
        contentStackView.addArrangedSubview(cpfLabel)
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
}

// MARK: - ReceiptPresentable
extension ReceiverReceiptWidgetView: ReceiptPresentable {
    var contentHeight: CGFloat {
        layoutIfNeeded()
        let verticalMargins = Spacing.base02 * 2
        return contentStackView.bounds.height + verticalMargins
    }
    
    func contentView() -> UIView {
        return self
    }
}
