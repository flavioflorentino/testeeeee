import Foundation
import UI

final class LabelValueCentralizedReceiptWidgetView: UIView {
    private lazy var mainLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        label.textAlignment = .center
        return label
    }()
    
    private let detailsLabel: UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .justified
        label.font = UIFont.systemFont(ofSize: 13)
        return label
    }()
    
    private var item: LabelValueCentralizedReceiptWidgetItem?
    private let margin: CGFloat = 20
    
    convenience init(_ item: LabelValueCentralizedReceiptWidgetItem) {
        self.init(frame: .zero)
        self.item = item
        
        translatesAutoresizingMaskIntoConstraints = false
        setupViews()
        setupConstraints()
    }
    
    private func setupViews() {
        self.drawBottomLine(withThickness: 1, on: self)
        backgroundColor = Palette.ppColorGrayscale000.color
        
        mainLabel.text = item?.label
        detailsLabel.text = item?.details
        
        setupLabel(mainLabel, with: item)
        setupLabel(detailsLabel, with: item)
        
        addSubview(mainLabel)
        addSubview(detailsLabel)
    }
    
    private func setupLabel(_ label: UILabel, with item: LabelValueCentralizedReceiptWidgetItem?) {
        guard let item = item else {
            return
        }
        let labelsWidth = UIScreen.main.bounds.width - margin * 2
        
        label.textColor = item.labelColor
        label.preferredMaxLayoutWidth = labelsWidth
    }
    
    private func setupConstraints() {
        mainLabel.snp.makeConstraints { make in
            make.left.equalTo(margin)
            make.right.equalTo(-margin)
            make.top.equalTo(margin)
        }
        
        detailsLabel.snp.makeConstraints { make in
            make.left.equalTo(margin)
            make.top.equalTo(mainLabel.snp.bottom).offset(15)
        }
    }
}

extension LabelValueCentralizedReceiptWidgetView: ReceiptPresentable {
    var contentHeight: CGFloat {
        var height = margin + margin * 2
        height += mainLabel.intrinsicContentSize.height + detailsLabel.intrinsicContentSize.height
        
        return height
    }
    
    func contentView() -> UIView {
        return self
    }
}
