final class ConsumerReceiptWidgetView: DefaultReceiptWidgetView {
    override func initialize() {
        showSubtitle = true
        super.initialize()
    }
    
    convenience init(_ item: TransactionReceiptWidgetItem) {
        self.init(frame: .zero)
        let contact = PPContact()
        contact.imgUrl = item.payee.imgUrl
        contact.isVerified = item.payee.isVerified
        contact.businessAccount = item.payee.isPro
        profileView.setContact(contact)
        date = item.date
        title = item.payee.username
        subtitle = item.payee.name
        receiptTotal = item.total
        transaction = "\(ReceiptLocalizable.transaction.text) \(item.id)"
        isCanceled = item.isCanceled
        handleReceiver(data: item.receiver)
        for itemList in item.list {
            addDataRow(title: itemList.label, value: itemList.value)
        }
    }
}
