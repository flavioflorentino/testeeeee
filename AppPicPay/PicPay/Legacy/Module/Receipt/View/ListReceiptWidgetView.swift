import UI

final class ListReceiptWidgetView: UIView {
    lazy private var dataView: ReceiptWidgetList = {
        let list = ReceiptWidgetList(metrics: [
            .topMargin: 20,
            .bottomMargin: 20,
            .hMargin: 20,
            .hRowSpacing: 10,
            .vRowSpacing: 8
        ])
        list.translatesAutoresizingMaskIntoConstraints = false
        list.identifier = "dataList"
        
        return list
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    convenience init(_ item: ListReceiptWidgetItem) {
        self.init(frame: .zero)
        
        for itemList in item.items {
            addDataRow(title: itemList.label, value: itemList.value)
        }
    }
    
    func initialize() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = Palette.ppColorGrayscale000.color
        addSubview(dataView)
        executeMainConstraints()
        drawBottomLine(withThickness: 1, on: self)
    }
    
    private func executeMainConstraints() {
        dataView.snp.makeConstraints {
            $0.height.equalTo(dataView.contentHeight)
            $0.edges.equalToSuperview()
        }
    }
    
    func addDataRow(title: String, value: String, titleTextColor: UIColor? = nil, valueTextColor: UIColor? = nil) {
        dataView.addRow(title: title, value: value, titleTextColor: titleTextColor, valueTextColor: valueTextColor)
    }
}

extension ListReceiptWidgetView: ReceiptPresentable {
    var contentHeight: CGFloat {
        return dataView.contentHeight
    }
    
    func contentView() -> UIView {
        return self
    }
}
