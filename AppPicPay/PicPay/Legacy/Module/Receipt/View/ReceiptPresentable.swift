public protocol ReceiptPresentable: AnyObject {
    typealias RowLabels = ReceiptWidgetListRowLabels
    /**
     Returns: Altura total da view
     */
    var contentHeight: CGFloat { get }
    
    /**
     Returns: A `UIView` do recibo
     */
    func contentView() -> UIView
}

public extension ReceiptPresentable {
    /**
     Draws a divider line at the bottom of the container view
     */
    func drawBottomLine(withThickness thick: CGFloat, on superview: UIView) {
        let line = UIView()
        
        line.translatesAutoresizingMaskIntoConstraints = false
        line.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.09765625)
        superview.addSubview(line)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: line, attribute: .right, relatedBy: .equal, toItem: superview, attribute: .right, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: line, attribute: .left, relatedBy: .equal, toItem: superview, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: line, attribute: .bottom, relatedBy: .equal, toItem: superview, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: line, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: thick)
        ])
    }
}
