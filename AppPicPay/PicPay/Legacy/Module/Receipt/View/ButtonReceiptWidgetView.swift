import UI

protocol ButtonReceiptWidgetViewDelegate: AnyObject {
    func didTapButton(actionType: ReceiptWidgetButtonActionType)
}

final class ButtonReceiptWidgetView: UIView {
    private var item: ButtonActionReceiptWidgetItem?
    weak var delegate: ButtonReceiptWidgetViewDelegate?
    
    private let button: UIPPButton = {
        let button = UIPPButton()
        button.borderColor = Palette.ppColorBranding300.color
        button.titleLabel?.textColor = Palette.ppColorBranding300.color
        button.normalBackgrounColor = .clear
        button.cornerRadius = CGFloat.buttonHeight / 2
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        
        return button
    }()
    
    convenience init(_ item: ButtonActionReceiptWidgetItem) {
        self.init(frame: .zero)
        self.item = item
        
        translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(item.title, for: .normal)
        
        setupViews()
        setupConstraints()
    }
    
    private func setupViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
        drawBottomLine(withThickness: 1, on: self)
        addSubview(button)
    }
    
    private func setupConstraints() {
        button.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.height.equalTo(CGFloat.buttonHeight)
            make.left.equalTo(CGFloat.margin)
            make.right.equalTo(-CGFloat.margin)
        }
    }
    
    @objc
    func didTapButton() {
        guard let type = item?.actionType else {
            return
        }
        delegate?.didTapButton(actionType: type)
    }
}

extension ButtonReceiptWidgetView: ReceiptPresentable {
    var contentHeight: CGFloat {
        return .buttonHeight + .margin * 2
    }
    
    func contentView() -> UIView {
        return self
    }
}

private extension CGFloat {
    static let buttonHeight: CGFloat = 44
    static let margin: CGFloat = 20
}
