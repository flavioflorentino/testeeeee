import Foundation
import UI
import UIKit

protocol FaqReceiptWidgetViewDelegate: AnyObject {
    func didTouchFaq(link: String)
}

final class FaqReceiptWidgetView: UIView {
    private lazy var mainLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        label.textAlignment = .center
        return label
    }()
    
    private let detailsButton: UIButton = {
        let button = UIButton()
        button.contentHorizontalAlignment = .center
        button.addTarget(self, action: #selector(detailsButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private let legacySpacing: CGFloat = 20
    
    private var item: FaqReceiptWidgetItem?
    private weak var delegate: FaqReceiptWidgetViewDelegate?
    
    convenience init(_ item: FaqReceiptWidgetItem, delegate: FaqReceiptWidgetViewDelegate) {
        self.init(frame: .zero)
        
        self.item = item
        self.delegate = delegate
        
        setupElements()
        buildLayout()
    }
    
    private func setupElements() {
        mainLabel.text = item?.label
        mainLabel.textColor = item?.labelColor
        
        detailsButton.setTitle(item?.details, for: .normal)
        detailsButton.buttonStyle(LinkButtonStyle(size: .small))
        
        drawBottomLine(withThickness: 1, on: self)
        backgroundColor = Palette.ppColorGrayscale000.color
        
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    @objc
    private func detailsButtonTapped() {
        guard let url = item?.url else { return }
        delegate?.didTouchFaq(link: url)
    }
}

extension FaqReceiptWidgetView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(mainLabel)
        addSubview(detailsButton)
    }
    
    func setupConstraints() {
        mainLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(legacySpacing)
        }
        
        detailsButton.snp.makeConstraints {
            $0.top.equalTo(mainLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.bottom.equalToSuperview().inset(legacySpacing)
        }
    }
}

extension FaqReceiptWidgetView: ReceiptPresentable {
    var contentHeight: CGFloat {
        var height = legacySpacing + legacySpacing * 2
        height += mainLabel.intrinsicContentSize.height + detailsButton.intrinsicContentSize.height
        
        return height
    }
    
    func contentView() -> UIView {
        self
    }
}
