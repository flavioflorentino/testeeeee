import UI

protocol SubscriptionReceiptWidgetViewDelegate: AnyObject {
    func seeMoreDetailButtonFired(_ sender: UIButton?, buttonModel: SeeDetailsButtonItem?)
}

final class SubscriptionReceiptWidgetView: UIView {
    weak var delegate: SubscriptionReceiptWidgetViewDelegate?
    
    lazy private var dataView: ReceiptWidgetList = {
        let list = ReceiptWidgetList(metrics: [
            .topMargin: 8,
            .bottomMargin: 8,
            .hMargin: 20,
            .hRowSpacing: 10,
            .vRowSpacing: 8
        ])
        list.translatesAutoresizingMaskIntoConstraints = false
        
        return list
    }()
    
    lazy private var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = ReceiptLocalizable.yourSignature.text
        label.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        
        return label
    }()
    
    lazy private var iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "icon_supscription")
        
        return imageView
    }()
    
    lazy private var detailsButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(DefaultLocalizable.seeMoreDetails.text, for: .normal)
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        button.setTitleColor(#colorLiteral(red: 0.066666666666666666, green: 0.7803921568627451, blue: 0.43529411764705883, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        button.addTarget(self, action: #selector(SubscriptionReceiptWidgetView.detailsButtonAction), for: .touchUpInside)
        
        return button
    }()
    
    private var deatilsButtonModel: SeeDetailsButtonItem?
    
    // MARK: UIView
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    // MARK: SubscriptionReceiptWidgetView
    convenience init(_ item: SubscriptionReceiptWidgetItem) {
        self.init(frame: .zero)
        
        titleLabel.text = item.title
        
        for itemList in item.descriptionList {
            addDataRow(title: itemList.label, value: itemList.value)
        }
        
        if let button = item.seeMoreButton {
            deatilsButtonModel = button
            detailsButton.isEnabled = true
            detailsButton.alpha = 1
            detailsButton.setTitle(button.title, for: .normal)
            if let color = button.titleColor {
                detailsButton.setTitleColor(color, for: .normal)
            }
        } else {
            detailsButton.isEnabled = false
            detailsButton.alpha = 0.5
        }
    }
    
    // MARK: Methods
    func initialize() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = Palette.ppColorGrayscale000.color
        addSubview(iconView)
        addSubview(titleLabel)
        addSubview(dataView)
        addSubview(detailsButton)
        
        executeMainConstraints()
        drawBottomLine(withThickness: 1, on: self)
    }
    
    private func executeMainConstraints() {
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: .topMargin),
            NSLayoutConstraint(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: ((.iconSize + .hIconTitleSpacing) / 2))
        ])
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: iconView, attribute: .centerY, relatedBy: .equal, toItem: titleLabel, attribute: .centerY, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: iconView, attribute: .right, relatedBy: .equal, toItem: titleLabel, attribute: .left, multiplier: 1, constant: -.hIconTitleSpacing)
        ])
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: detailsButton, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -.bottomMargin),
            NSLayoutConstraint(item: detailsButton, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: detailsButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: .detailsButtonHeight)
        ])
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: dataView, attribute: .top, relatedBy: .equal, toItem: self.titleLabel, attribute: .bottom, multiplier: 1, constant: .vTitleListSpacing),
            NSLayoutConstraint(item: dataView, attribute: .bottom, relatedBy: .equal, toItem: self.detailsButton, attribute: .top, multiplier: 1, constant: .vListButtonSpacing),
            NSLayoutConstraint(item: dataView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: dataView, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 0)
        ])
    }
    
    @objc
    func detailsButtonAction(sender: UIButton) {
        self.delegate?.seeMoreDetailButtonFired(sender, buttonModel: self.deatilsButtonModel)
    }
    
    func addDataRow(title: String, value: String, titleTextColor: UIColor? = nil, valueTextColor: UIColor? = nil) {
        self.dataView.addRow(title: title, value: value, titleTextColor: titleTextColor, valueTextColor: valueTextColor)
    }
}

extension SubscriptionReceiptWidgetView: ReceiptPresentable {
    var contentHeight: CGFloat {
        var height: CGFloat = 0
        height += .topMargin
        height += titleLabel.intrinsicContentSize.height
        height += .vTitleListSpacing
        height += dataView.contentHeight
        height += .vListButtonSpacing
        height += detailsButton.intrinsicContentSize.height
        height += .bottomMargin
        
        return height
    }
    
    func contentView() -> UIView {
        return self
    }
}

private extension CGFloat {
    static let topMargin: CGFloat = 21
    static let bottomMargin: CGFloat = 14
    static let vTitleListSpacing: CGFloat = 8
    static let vListButtonSpacing: CGFloat = 8
    static let detailsButtonHeight: CGFloat = 24
    static let iconSize: CGFloat = 16
    static let hIconTitleSpacing: CGFloat = 8
}
