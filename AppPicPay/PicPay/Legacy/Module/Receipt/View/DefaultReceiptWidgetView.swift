import UI

// swiftlint:disable type_body_length
class DefaultReceiptWidgetView: UIView, ReceiptWidgetListDelegate {
    private var dataContentHeight: CGFloat {
        return dataView.contentHeight
    }
    
    private var receiverContentHeight: CGFloat {
        receiverView.contentHeight
    }
    
    private var headerContentHeight: CGFloat {
        var colOneHeight: CGFloat = 0
        var colTwoHeight: CGFloat = 0
        
        colOneHeight += .profileImageSize
        colOneHeight += .vMargin * 2
        
        colTwoHeight += .vMargin * 2
        colTwoHeight += titleLabel.intrinsicContentSize.height
        colTwoHeight += transactionLabel.intrinsicContentSize.height
        colTwoHeight += dateLabel.intrinsicContentSize.height
        colTwoHeight += .headerTransactionSpacing
        colTwoHeight += .headerDateSpacing
        
        if showSubtitle {
            colTwoHeight += subtitleLabel.intrinsicContentSize.height
            colTwoHeight += .headerSubtitleSpacing
        }
        
        return max(colOneHeight, colTwoHeight)
    }
    
    private var footerContentHeight: CGFloat {
        return footerView.contentHeight
    }
    
    var showSubtitle = false
    private var viewDictionary = [String: UIView]()
    
    var receiptTotal: String? {
        get { return receiptTotalRow?.value.text }
        
        set(valueStr) {
            let label = receiptTotalRow?.value
            if let attributes = label?.attributedText?.attributes(at: 0, effectiveRange: nil) {
                label?.attributedText = NSAttributedString(string: valueStr ?? "", attributes: attributes)
            } else {
                label?.text = valueStr
            }
        }
    }
    
    var title: String? {
        get { return titleLabel.text }
        set(str) { titleLabel.text = str }
    }
    
    var subtitle: String? {
        get { return subtitleLabel.text }
        set(str) { subtitleLabel.text = str }
    }
    
    var transaction: String? {
        get { return transactionLabel.text }
        set(str) { transactionLabel.text = str }
    }
    
    var date: String? {
        get { return dateLabel.text }
        set(str) { dateLabel.text = str }
    }
    
    // MARK: - Subviews
    private lazy var receiverView = ReceiverReceiptWidgetView()
    
    lazy var dataView: ReceiptWidgetList = {
        let vMargin = Float(CGFloat.vMargin)
        let list = ReceiptWidgetList(metrics: [
            .hMargin: Float(CGFloat.hMargin),
            .hContentExtraMargin: 1,
            .topMargin: vMargin,
            .bottomMargin: vMargin
        ])
        list.translatesAutoresizingMaskIntoConstraints = false
        list.setBorderOptions([.top], withColor: Palette.ppColorGrayscale500.color, andThickness: 1)
        return list
        }()
    
    lazy private var headerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var profileView: UIPPProfileImage = {
        let imageView = UIPPProfileImage()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
        }()
    
    lazy private var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.667
        label.textColor = Palette.ppColorGrayscale600.color
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = Palette.ppColorGrayscale000.color
        label.text = "TitleLabel"
        
        return label
    }()
    
    lazy private var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13)
        label.adjustsFontSizeToFitWidth = true
        label.textColor = Palette.ppColorGrayscale500.color
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = Palette.ppColorGrayscale000.color
        label.text = "SubtitleLabel"
        
        return label
    }()
    
    lazy private var transactionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = Palette.ppColorGrayscale400.color
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = Palette.ppColorGrayscale000.color
        label.text = "TransactionLabel"
        
        return label
    }()
    
    lazy private var dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = Palette.ppColorGrayscale400.color
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = Palette.ppColorGrayscale000.color
        label.text = "DateLabel"
        
        return label
    }()
    
    lazy private var footerView: ReceiptWidgetList = {
        let list = ReceiptWidgetList(metrics: [
            .topMargin: 14,
            .bottomMargin: 14,
            .hContentExtraMargin: 1,
            .hMargin: Float(CGFloat.hMargin)
        ])
        list.translatesAutoresizingMaskIntoConstraints = false
        
        list.setBorderOptions([.top], withColor: Palette.ppColorGrayscale500.color, andThickness: 1)
        list.identifier = "footer"
        
        return list
        }()
    
    private var receiptTotalRow: RowLabels?
    
    var isCanceled: Bool = false {
        didSet(oldValue) {
            if self.isCanceled != oldValue {
                self.updateLabelsStatus()
            }
        }
    }
    
    // MARK: UIView
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    convenience init(_ item: TransactionReceiptWidgetItem) {
        self.init(frame: .zero)
        let contact = PPContact()
        contact.imgUrl = item.payee.imgUrl
        profileView.setContact(contact)
        
        setupView(item: item)
    }
    
    convenience init(itemStore: TransactionReceiptWidgetItem) {
        self.init(frame: .zero)
        let store = PPStore()
        store.img_url = itemStore.payee.imgUrl
        store.isVerified = itemStore.payee.isVerified
        profileView.setStore(store)
        
        setupView(item: itemStore)
    }
    
    private func setupView(item: TransactionReceiptWidgetItem) {
        date = item.date
        title = item.payee.name
        
        isCanceled = item.isCanceled
        receiptTotal = item.total
        transaction = "\(ReceiptLocalizable.transaction.text) \(item.id)"
        updateLabelsStatus()
        handleReceiver(data: item.receiver)
        for itemList in item.list {
            addDataRow(title: itemList.label, value: itemList.value)
        }
    }
    
    // MARK: Private methods
    func initialize() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = Palette.ppColorGrayscale000.color
        addSubview(headerView)
        addSubview(receiverView)
        addSubview(dataView)
        addSubview(footerView)
        
        configureHeaderView()
        footerView.delegate = self
        receiptTotalRow = self.footerView.addRow(title: DefaultLocalizable.value.text, value: "R00,00")
        executeMainConstraints()
        drawBottomLine(withThickness: 1, on: self)
    }
    
    private func configureHeaderView() {
        let imageSize = CGFloat.profileImageSize
        let vMargin = CGFloat.vMargin
        let hMargin = CGFloat.hMargin
        let visualConstraints: String
        
        headerView.addSubview(profileView)
        viewDictionary["profileView"] = profileView
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: profileView, attribute: .top, relatedBy: .equal, toItem: headerView, attribute: .top, multiplier: 1, constant: vMargin),
            NSLayoutConstraint(item: profileView, attribute: .left, relatedBy: .equal, toItem: headerView, attribute: .left, multiplier: 1, constant: hMargin),
            NSLayoutConstraint(item: profileView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: imageSize),
            NSLayoutConstraint(item: profileView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: imageSize)
        ])
        
        var headerLabels: [String: UILabel] = [
            "titleLabel": titleLabel,
            "transactionLabel": transactionLabel,
            "dateLabel": dateLabel
        ]
        
        if self.showSubtitle {
            headerLabels["subtitleLabel"] = subtitleLabel
            visualConstraints = "V:|-vMargin-[titleLabel]-headerSubtitleSpacing-[subtitleLabel]-headerTransactionSpacing-[transactionLabel]-headerDateSpacing-[dateLabel]-vMargin-|"
        } else {
            visualConstraints = "V:|-vMargin-[titleLabel]-headerTransactionSpacing-[transactionLabel]-headerDateSpacing-[dateLabel]-vMargin-|"
        }
        
        for (id, label) in headerLabels {
            headerView.addSubview(label)
            viewDictionary[id] = label
            NSLayoutConstraint.activate([
                NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: profileView, attribute: .right, multiplier: 1, constant: hMargin),
                NSLayoutConstraint(item: label, attribute: .right, relatedBy: .equal, toItem: headerView, attribute: .right, multiplier: 1, constant: -hMargin)
            ])
        }
        
        let constraintsMetrics: [String: CGFloat] = [
            "hMargin": .hMargin,
            "vMargin": .vMargin,
            "hRowSpacing": .hRowSpacing,
            "vRowSpacing": .vRowSpacing,
            "profileImageSize": .profileImageSize,
            "headerSubtitleSpacing": .headerSubtitleSpacing,
            "headerTransactionSpacing": .headerTransactionSpacing,
            "headerDateSpacing": .headerDateSpacing
        ]
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(
            withVisualFormat: visualConstraints,
            options: NSLayoutConstraint.FormatOptions(rawValue: 0),
            metrics: constraintsMetrics,
            views: viewDictionary
        ))
    }
    
    private func executeMainConstraints() {
        viewDictionary["header"] = headerView
        viewDictionary["receiver"] = receiverView
        viewDictionary["data"] = dataView
        viewDictionary["footer"] = footerView
        
        headerView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.height.equalTo(headerContentHeight)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
        }
        
        receiverView.snp.makeConstraints {
            $0.top.equalTo(headerView.snp.bottom)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
        }
        
        dataView.snp.makeConstraints {
            $0.top.equalTo(receiverView.snp.bottom)
            $0.bottom.equalTo(footerView.snp.top)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
        }
        
        footerView.snp.makeConstraints {
            $0.bottom.equalToSuperview()
            $0.height.equalTo(footerContentHeight)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
        }
    }
    
    func handleReceiver(data: TransactionReceiptWidgetItem.Receiver?) {
        guard let receiverItem = data else {
            receiverView.snp.makeConstraints {
                $0.height.equalTo(0)
            }
            return
        }
        receiverView.setContent(item: receiverItem)
    }
    
    private func updateLabelsStatus(_ labels: [UILabel]? = nil) {
        var labelsToUpdate: [UILabel] = []
        
        if let items = labels {
            //Atualiza apenas as labels passadas por parametro
            labelsToUpdate.append(contentsOf: items)
        } else {
            //Atualiza todas as labels
            for view in dataView.subviews {
                if let label = view as? UILabel {
                    labelsToUpdate.append(label)
                }
            }
            
            for view in footerView.subviews {
                if let label = view as? UILabel {
                    labelsToUpdate.append(label)
                }
            }
        }
        
        if !labelsToUpdate.isEmpty {
            var attributes: [NSAttributedString.Key: Any]
            
            for label in labelsToUpdate {
                attributes = label.attributedText?.attributes(at: 0, effectiveRange: nil) ?? [:]
                attributes[.font] = label.font
                
                if isCanceled {
                    attributes[.foregroundColor] = label.textColor.withAlphaComponent(0.5)
                    attributes[.strikethroughStyle] = NSUnderlineStyle.single.rawValue
                    attributes[.strikethroughColor] = attributes[.foregroundColor]
                } else {
                    attributes[.foregroundColor] = Palette.ppColorGrayscale500.color
                    attributes[.strikethroughStyle] = nil
                    attributes[.strikethroughColor] = nil
                }
                
                label.attributedText = NSAttributedString(string: label.text ?? "", attributes: attributes)
            }
        }
    }
    
    func addDataRow(title: String, value: String, titleTextColor: UIColor? = nil, valueTextColor: UIColor? = nil) {
        let lastLabels = dataView.addRow(title: title, value: value, titleTextColor: titleTextColor, valueTextColor: valueTextColor)
        
        //Caso tenha que aplicar a formatação de cancelado
        updateLabelsStatus([lastLabels.title, lastLabels.value])
    }
    
    // MARK: ReceiptWidgetListItemDelegate
    func configureRowLabels(_ labels: RowLabels, identifier: String?) {
        if identifier == "footer" {
            let attributes: [NSAttributedString.Key: Any] = [
                .font: UIFont.systemFont(ofSize: 17, weight: .bold),
                .foregroundColor: Palette.ppColorGrayscale500.color
            ]
            labels.title.attributedText = NSAttributedString(string: labels.title.text ?? "", attributes: attributes)
            labels.value.attributedText = NSAttributedString(string: labels.value.text ?? "", attributes: attributes)
        }
    }
}

extension DefaultReceiptWidgetView: ReceiptPresentable {
    var contentHeight: CGFloat {
        return dataContentHeight + headerContentHeight + footerContentHeight + receiverContentHeight
    }
    
    func contentView() -> UIView {
        return self
    }
}

private extension CGFloat {
    static let hMargin: CGFloat = 20
    static let vMargin: CGFloat = 20
    static let hRowSpacing: CGFloat = 10
    static let vRowSpacing: CGFloat = 10
    static let profileImageSize: CGFloat = 64
    static let headerSubtitleSpacing: CGFloat = 0
    static let headerTransactionSpacing: CGFloat = 4
    static let headerDateSpacing: CGFloat = 0
}
