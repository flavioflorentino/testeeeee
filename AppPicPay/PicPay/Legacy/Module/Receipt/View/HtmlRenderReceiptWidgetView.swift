import UI
import WebKit

protocol HtmlRenderReceiptWidgetViewDelegate: AnyObject {
    func webviewDidLoad(widgetView: HtmlRenderReceiptWidgetView, isShowingContent: Bool)
}

final class HtmlRenderReceiptWidgetView: UIView {
    private static var webviewContext = "loadingWebviewContext"
    
    // MARK: HtmlRenderReceiptWidgetView
    weak var delegate: HtmlRenderReceiptWidgetViewDelegate?
    var initialViewHeight: CGFloat = 50
    var isRemoteRequest = false
    
    @objc lazy var content: WKWebView = {
        let webView = WKWebView()
        webView.scrollView.bounces = false
        webView.scrollView.showsVerticalScrollIndicator = false
        webView.scrollView.isScrollEnabled = false
        webView.isUserInteractionEnabled = false
        
        return webView
    }()
    
    lazy var loadingView: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .gray)
        indicator.hidesWhenStopped = true
        indicator.translatesAutoresizingMaskIntoConstraints = false
        
        return indicator
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    convenience init(_ widgetItem: HtmlRenderReceiptWidgetItem) {
        self.init(frame: .zero)
        initialize()
        loadHtml(with: widgetItem)
    }
    
    func initialize() {
        backgroundColor = Palette.ppColorGrayscale000.color
        translatesAutoresizingMaskIntoConstraints = false
        buildView()
    }
    
    deinit {
        if observationInfo != nil {
            removeObserver(self, forKeyPath: #keyPath(content.isLoading), context: &HtmlRenderReceiptWidgetView.webviewContext)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        content.frame = CGRect(origin: .zero, size: CGSize(width: frame.width, height: content.scrollView.contentSize.height))
    }
    
    func buildView() {
        addSubview(content)
        addSubview(loadingView)
        NSLayoutConstraint.activate(
            [
                loadingView.centerXAnchor.constraint(equalTo: centerXAnchor),
                loadingView.centerYAnchor.constraint(equalTo: centerYAnchor)
            ]
        )

        drawBottomLine(withThickness: 1, on: self)
    }
    
    func evaluateContentHeight() {
        content.evaluateJavaScript("document.body.scrollHeight") { [weak self] result, error in
            self?.evaluateJavaScriptHandler(result, error: error)
        }
    }
    
    private func evaluateJavaScriptHandler(_ result: Any?, error: Error?) {
        let hasContent: Bool
        
        if error == nil, let loadedHeight = result as? CGFloat {
            //Normalize
            if #available(iOS 11, *) {
                hasContent = loadedHeight > 0
            } else {
                //Para iOS <= 10 há um nimimo de 8 em tamanho, mesmo quando vazio
                hasContent = loadedHeight > 8
            }
            
            let newHeight = hasContent ? loadedHeight : 0
            content.scrollView.contentSize.height = newHeight
            
            let changeViewHeight:((_:UIView, _:CGFloat) -> Void) = { view, height in
                view.frame = CGRect(origin: view.frame.origin, size: CGSize(width: view.frame.width, height: height))
            }
            
            if isRemoteRequest {
                content.alpha = 0
                UIView.animate(withDuration: 0.25, animations: { [weak self] in
                    guard let strongSelf = self else {
                        return
                    }
                    
                    strongSelf.content.alpha = 1
                    changeViewHeight(strongSelf.content, newHeight)
                })
            } else {
                changeViewHeight(content, newHeight)
            }
        } else {
            hasContent = false
        }
        
        delegate?.webviewDidLoad(widgetView: self, isShowingContent: hasContent)
    }
    
    func loadHtml(with widgetItem: HtmlRenderReceiptWidgetItem) {
        loadingView.startAnimating()
        
        if widgetItem.isUrl {
            isRemoteRequest = true
            if let url = URL(string: widgetItem.content) {
                content.load(URLRequest(url: url))
            }
        } else {
            isRemoteRequest = false
            content.loadHTMLString(widgetItem.content, baseURL: nil)
        }
        addObserver(self, forKeyPath: #keyPath(content.isLoading), options: .new, context: &HtmlRenderReceiptWidgetView.webviewContext)
    }
    
    // MARK: NSObject
    // swiftlint:disable block_based_kvo
    // swiftlint:disable unused_optional_binding
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        guard let _ = object as? HtmlRenderReceiptWidgetView,
            let keyPath = keyPath,
            let change = change,
            context == &HtmlRenderReceiptWidgetView.webviewContext else {
                return
        }
        
        switch keyPath {
        case #keyPath(content.isLoading):
            if let isLoading = change[.newKey] as? Bool {
                if !isLoading {
                    loadingView.stopAnimating()
                    evaluateContentHeight()
                }
            }
        default:
            break
        }
    }
    // swiftlint:enable block_based_kvo
    // swiftlint:enable unused_optional_binding
}

extension HtmlRenderReceiptWidgetView: ReceiptPresentable {
    var contentHeight: CGFloat {
        return self.initialViewHeight
    }
    
    func contentView() -> UIView {
        return self
    }
}
