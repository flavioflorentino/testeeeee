import UI
import UIKit

private enum Layout {
    enum Size {
        static let lineHeight: CGFloat = 1
    }
}

final class PixReceiptLineView: UIView {
    // MARK: - Visual Components
    private let lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()
    
    // MARK: - Life Cycle
    convenience init() {
        self.init(frame: .zero)
        buildLayout()
    }
    
    func setContent(isLineHidden: Bool) {
        lineView.isHidden = isLineHidden
    }
}

// MARK: - ViewConfiguration
extension PixReceiptLineView: ViewConfiguration {
    func setupConstraints() {
        lineView.snp.makeConstraints {
            $0.bottom.top.equalToSuperview().inset(Spacing.base01)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Size.lineHeight)
        }
    }
    
    func buildViewHierarchy() {
        addSubview(lineView)
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
}
