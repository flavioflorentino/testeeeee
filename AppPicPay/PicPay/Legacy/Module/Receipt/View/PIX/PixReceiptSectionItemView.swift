import UI
import UIKit

final class PixReceiptSectionItemView: UIView {
    // MARK: - Visual Components
    private let contentLabel = UILabel()
    
    // MARK: - Life Cycle
    convenience init() {
        self.init(frame: .zero)
        buildLayout()
    }
    
    func setContent(text: String, styleType: Typography.Style.BodySecondary) {
        contentLabel.text = text
        contentLabel.labelStyle(BodySecondaryLabelStyle(type: styleType))
            .with(\.textColor, Colors.grayscale500.color)
    }
}

// MARK: - ViewConfiguration
extension PixReceiptSectionItemView: ViewConfiguration {
    func setupConstraints() {
        contentLabel.snp.makeConstraints {
            $0.bottom.top.equalToSuperview().inset(Spacing.base00)
            $0.leading.trailing.equalToSuperview()
        }
    }
    
    func buildViewHierarchy() {
        addSubview(contentLabel)
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
}
