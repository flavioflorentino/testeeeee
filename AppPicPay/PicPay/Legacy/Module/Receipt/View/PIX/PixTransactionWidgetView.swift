import UI
import UIKit

private extension PixTransactionWidget.Layout {
    enum Size {
        static let lineHeight: CGFloat = 1
    }
}

final class PixTransactionWidget: UIView {
    fileprivate enum Layout {}
    // MARK: - Visual Components
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var headerView = PixReceiptHeaderView()
    
    private lazy var transactionInfoView = PixTransactionInfoView()
    
    private lazy var listView: ReceiptWidgetList = {
        let list = ReceiptWidgetList(metrics: [
            .hMargin: Float(Spacing.base02),
            .topMargin: Float(Spacing.base02),
            .bottomMargin: Float(Spacing.base02)
        ])
        return list
    }()
    
    private lazy var valueListView: ReceiptWidgetList = {
        let list = ReceiptWidgetList(metrics: [
            .hMargin: Float(Spacing.base02),
            .topMargin: Float(Spacing.base02),
            .bottomMargin: Float(Spacing.base02),
        ])
        list.setBorderOptions([.top], withColor: Colors.grayscale100.color, andThickness: 1)
        return list
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()
    
    // MARK: - Life Cycle
    convenience init(item: PixTransactionReceiptWidgetItem) {
        self.init(frame: .zero)
        setContent(item: item)
        buildLayout()
    }
    
    // MARK: - Content
    private func createValueAttributedText(_ text: String) -> NSAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.title(.small).font(),
            .foregroundColor: Colors.grayscale700.color
        ]
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    private func setContent(item: PixTransactionReceiptWidgetItem) {
        headerView.setContent(item: item.header)
        transactionInfoView.setContent(body: item.body)
        setListData(item: item)
    }
    
    private func setListData(item: PixTransactionReceiptWidgetItem) {
        let color = Colors.grayscale500.color
        item.list.forEach { listView.addRow(title: $0.label, value: $0.value, titleTextColor: color, valueTextColor: color) }
        
        let totalValueRow = valueListView.addRow(title: nil, value: nil)
        totalValueRow.title.attributedText = createValueAttributedText(DefaultLocalizable.value.text)
        totalValueRow.value.attributedText = createValueAttributedText(item.total)
    }
}

// MARK: - ViewConfiguration
extension PixTransactionWidget: ViewConfiguration {
    func setupConstraints() {
        contentStackView.snp.makeConstraints {
            $0.width.equalTo(UIScreen.main.bounds.width)
            $0.edges.equalToSuperview()
        }
        
        listView.snp.makeConstraints {
            $0.height.equalTo(listView.contentHeight)
        }

        valueListView.snp.makeConstraints {
            $0.height.equalTo(valueListView.contentHeight)
        }
        
        lineView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.lineHeight)
            $0.bottom.leading.trailing.equalToSuperview()
        }
    }
    
    func buildViewHierarchy() {
        addSubview(contentStackView)
        addSubview(lineView)
        contentStackView.addArrangedSubview(headerView)
        contentStackView.addArrangedSubview(transactionInfoView)
        contentStackView.addArrangedSubview(listView)
        contentStackView.addArrangedSubview(valueListView)
    }
    
    func configureViews() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .clear
    }
}

// MARK: - ReceiptPresentable
extension PixTransactionWidget: ReceiptPresentable {
    var contentHeight: CGFloat {
        layoutIfNeeded()
        return bounds.height
    }
    
    func contentView() -> UIView {
        return self
    }
}
