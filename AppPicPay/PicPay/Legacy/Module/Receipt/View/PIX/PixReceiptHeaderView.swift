import UI
import UIKit

private extension PixReceiptHeaderView.Layout {
    enum Size {
        static let icon = CGSize(width: 60, height: 60)
    }
}

final class PixReceiptHeaderView: UIView {
    fileprivate enum Layout {}
    
    // MARK: - Visual Components
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var textsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var descriptionStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        return stackView
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = Layout.Size.icon.height / 2
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var transactionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale500.color)
        return label
    }()
    
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale500.color)
        return label
    }()
    
    // MARK: - Life Cycle
    convenience init() {
        self.init(frame: .zero)
        buildLayout()
    }
    
    // MARK: - Content
    func setContent(item: PixTransactionReceiptWidgetItem.Header) {
        iconImageView.setImage(url: URL(string: item.imgUrl))
        titleLabel.text = item.title
        transactionLabel.text = item.id
        dateLabel.text = item.date
    }
}

// MARK: - ViewConfiguration
extension PixReceiptHeaderView: ViewConfiguration {
    func setupConstraints() {
        contentStackView.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base03)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
        
        iconImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.icon)
        }
    }
    
    func buildViewHierarchy() {
        addSubview(contentStackView)
        contentStackView.addArrangedSubview(iconImageView)
        contentStackView.addArrangedSubview(textsStackView)
        textsStackView.addArrangedSubview(titleLabel)
        textsStackView.addArrangedSubview(descriptionStackView)
        descriptionStackView.addArrangedSubview(transactionLabel)
        descriptionStackView.addArrangedSubview(dateLabel)
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
}
