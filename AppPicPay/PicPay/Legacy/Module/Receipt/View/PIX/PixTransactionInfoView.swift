import UI
import UIKit

enum PixReceiptSectionItem {
    case content(text: String, styleType: Typography.Style.BodySecondary)
    case space(isLineHidden: Bool)
}

final class PixTransactionInfoView: UIView {
    fileprivate enum Layout {}
    // MARK: - Visual Components
    private lazy var sectionsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }()
    
    // MARK: - Life Cycle
    convenience init() {
        self.init(frame: .zero)
        buildLayout()
    }
    
    func setContent(body: [[PixTransactionReceiptWidgetItem.SectionInfo]]) {
        var items: [PixReceiptSectionItem] = []
        items.append(.space(isLineHidden: false))
        body.forEach { contentSection in
            contentSection.enumerated().forEach { index, sectionItem in
                items.append(.content(text: sectionItem.title, styleType: .highlight))
                sectionItem.list.forEach { text in
                    items.append(.content(text: text, styleType: .default))
                }
                if index != contentSection.count - 1 {
                    items.append(.space(isLineHidden: true))
                }
            }
            items.append(.space(isLineHidden: false))
        }
        items.forEach { sectionsStackView.addArrangedSubview(createSectionItemView(item: $0)) }
    }
    
    private func createSectionItemView(item: PixReceiptSectionItem) -> UIView {
        switch item {
        case let .space(isLineHidden):
            let view = PixReceiptLineView()
            view.setContent(isLineHidden: isLineHidden)
            return view
        case let .content(text, styleType):
            let view = PixReceiptSectionItemView()
            view.setContent(text: text, styleType: styleType)
            return view
        }
    }
}

// MARK: - ViewConfiguration
extension PixTransactionInfoView: ViewConfiguration {
    func setupConstraints() {
        sectionsStackView.snp.makeConstraints {
            $0.bottom.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func buildViewHierarchy() {
        addSubview(sectionsStackView)
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
}
