protocol VoucherReceiptWidgetViewDelegate: AnyObject {
    func voucherWidgetFiredAction(open url: String, needsAuth: Bool)
    func copyVoucherToClipboard(voucher: String, widget: VoucherReceiptWidgetView, enableTouch: @escaping (() -> Void))
}

final class VoucherReceiptWidgetView: InfoReceiptWidgetView {
    lazy private var actionView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    weak var delegate: VoucherReceiptWidgetViewDelegate?
    private var buttons: [UIButton] = []
    private var buttonActions: [Int: String] = [:]
    
    // MARK: ReceiptPresentable
    override public var contentHeight: CGFloat {
        var contentSize: CGFloat = super.contentHeight
        contentSize += .actionViewHeight
        contentSize += .vSpacing
        return contentSize
    }
    
    // MARK: VoucherReceiptWidgetView
    convenience init(_ item: VoucherReceiptWidgetItem) {
        self.init(frame: .zero)
        self.title = item.title
        if let color = item.titleColor {
            titleColor = color
        }
        
        content = item.voucher
        addVoucherAction()
        
        if let button = item.button {
            addActionButton(button)
        }
    }
    
    private func addVoucherAction() {
        mainContentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnVoucher)))
    }
    
    @objc
    private func didTapOnVoucher() {
        guard let content = content else {
            return
        }
        mainContentView.isUserInteractionEnabled = false
        delegate?.copyVoucherToClipboard(voucher: content, widget: self, enableTouch: { [weak self] in
            self?.mainContentView.isUserInteractionEnabled = true
        })
    }
    
    @objc
    func buttonDidTap(sender: UIButton) {
        if let url = buttonActions[sender.tag] {
            delegate?.voucherWidgetFiredAction(open: url, needsAuth: false)
        }
    }
    
    func addActionButton(_ action: VoucherReceiptWidgetItem.Action) {
        if !subviews.contains(actionView) {
            configureActionView()
        }
        
        if buttons.isEmpty {
            addButton(with: action.title, and: action.url)
        }
    }
    
    private func addButton(with title: String, and url: String) {
        let button = UIButton()
        buttons.append(button)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        button.setTitleColor(#colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1.0), for: .normal)
        button.addTarget(self, action: #selector(VoucherReceiptWidgetView.buttonDidTap), for: .touchUpInside)
        button.tag = buttonActions.count
        buttonActions[button.tag] = url
        actionView.addSubview(button)
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: actionView, attribute: .centerX, relatedBy: .equal, toItem: button, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: actionView, attribute: .centerY, relatedBy: .equal, toItem: button, attribute: .centerY, multiplier: 1, constant: 0)
        ])
    }
    
    private func configureActionView() {
        addSubview(actionView)
        viewDictionary["actionView"] = actionView
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: actionView, attribute: .top, relatedBy: .equal, toItem: mainContentView, attribute: .bottom, multiplier: 1, constant: .vSpacing),
            NSLayoutConstraint(item: actionView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: .actionViewHeight),
            NSLayoutConstraint(item: actionView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: .hMargin),
            NSLayoutConstraint(item: actionView, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -.hMargin)
        ])
    }
}

private extension CGFloat {
    static let actionViewHeight: CGFloat = 24
    static let hMargin: CGFloat = 20
    static let vMargin: CGFloat = 18
    static let vSpacing: CGFloat = 14
    static let mainContentSize: CGFloat = 44
}
