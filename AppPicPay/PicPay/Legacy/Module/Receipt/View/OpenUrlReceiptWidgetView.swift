import UI

protocol OpenUrlReceiptWidgetViewDelegate: AnyObject {
    func openUrlButtonFired(urlToOpen: String)
}

final class OpenUrlReceiptWidgetView: UIView {
    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(Palette.ppColorGrayscale000.color, for: .normal)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = CGFloat.buttonHeight / CGFloat(2)
        button.backgroundColor = Palette.ppColorBranding300.color
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        button.addTarget(self, action: #selector(OpenUrlReceiptWidgetView.buttonFired), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: OpenUrlReceiptWidgetViewDelegate?
    private var model: OpenUrlReceiptWidgetItem?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    convenience init(model: OpenUrlReceiptWidgetItem) {
        self.init(frame: .zero)
        self.model = model
        actionButton.setTitle(model.title, for: .normal)
    }
    
    func initialize() {
        backgroundColor = Palette.ppColorGrayscale000.color
        translatesAutoresizingMaskIntoConstraints = false
        buildView()
        drawBottomLine(withThickness: 1, on: self)
    }
    
    func buildView() {
        addSubview(actionButton)
        
        NSLayoutConstraint.activate([
            actionButton.topAnchor.constraint(equalTo: topAnchor, constant: .vMargin),
            actionButton.leftAnchor.constraint(equalTo: leftAnchor, constant: .hMargin),
            actionButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -.hMargin),
            actionButton.heightAnchor.constraint(equalToConstant: .buttonHeight)
        ])
    }
    
    @objc
    func buttonFired(sender: Any?) {
        delegate?.openUrlButtonFired(urlToOpen: model?.url ?? "")
    }
}

extension OpenUrlReceiptWidgetView: ReceiptPresentable {
    var contentHeight: CGFloat {
        var height: CGFloat = 0
        height += .buttonHeight
        height += .vMargin * 2
        return height
    }
    
    func contentView() -> UIView {
        return self
    }
}

private extension CGFloat {
    static let vMargin: CGFloat = 20
    static let hMargin: CGFloat = 18
    static let buttonHeight: CGFloat = 44
}
