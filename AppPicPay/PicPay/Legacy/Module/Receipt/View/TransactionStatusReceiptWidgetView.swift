import UI

protocol TransactionStatusActionDelegate: AnyObject {
    func transactionStatusWidgetFired(action: TransactionStatusReceiptWidgetItem.Action, with payload: [AnyHashable: Any]?)
}

final class TransactionStatusReceiptWidgetView: UIView {
    lazy var symbolView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        label.textAlignment = .center
        return label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        return label
    }()
    
    lazy var actionButton: UIButton = {
        let buttonHeight: CGFloat = 32
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        button.setTitleColor(Palette.ppColorGrayscale000.color, for: .normal)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = buttonHeight / 2
        button.addTarget(self, action: #selector(TransactionStatusReceiptWidgetView.buttonDidTap), for: .touchUpInside)
        return button
        }()
    
    lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var readMoreButton: UIPPButton = {
        var button = UIPPButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: .light)
        button.addTarget(self, action: #selector(TransactionStatusReceiptWidgetView.readMoreButtonTapped), for: .touchUpInside)
        return button
    }()
    
    var title: String? {
        get { return titleLabel.text }
        set(str) { titleLabel.text = str }
    }
    
    var subtitle: String? {
        get { return subtitleLabel.text }
        set(str) { subtitleLabel.text = str }
    }
    
    var titleTextColor: UIColor {
        get { return titleLabel.textColor }
        set(color) { titleLabel.textColor = color }
    }
    
    var subtitleTextColor: UIColor {
        get { return subtitleLabel.textColor }
        set(color) { subtitleLabel.textColor = color }
    }
    
    var showActionButton = false {
        didSet(oldValue) {
            if oldValue == showActionButton {
                return
            }
            
            if showActionButton {
                insertReadMoreButton()
                insertButton()
            } else {
                actionButton.removeFromSuperview()
                readMoreButton.removeFromSuperview()
            }
        }
    }
    
    private var action: TransactionStatusReceiptWidgetItem.Action = .NotSpecified
    private var readMoreAction: TransactionStatusReceiptWidgetItem.Action = .NotSpecified
    private var actionPayload: [String: Any]?
    private var readMoreActionPayload: [String: Any]?
    weak var delegate: TransactionStatusActionDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        basicSetup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        basicSetup()
    }
    
    convenience init(_ item: TransactionStatusReceiptWidgetItem) {
        self.init(frame: .zero)
        basicSetup()
        
        if let icon = item.icon {
            insertIcon()
            setIcon(icon)
        }
        
        initialize()
        
        title = item.title
        if let titleColor = item.titleColor {
            titleTextColor = titleColor
        }
        subtitle = item.subtitle
        if let subtitleColor = item.subtitleColor {
            subtitleTextColor = subtitleColor
        }
        
        if let actionButton = item.actionButton {
            insertButton()
            showActionButton = true
            setActionButton(actionButton)
        }
        
        if let readMoreButton = item.readMoreButton {
            insertReadMoreButton()
            setActionReadMore(readMoreButton)
        }
    }
    
    func initialize() {
        addSubview(symbolView)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        
        executeMainConstraints()
        drawBottomLine(withThickness: 1, on: self)
    }
    
    @objc
    func buttonDidTap() {
        delegate?.transactionStatusWidgetFired(action: action, with: actionPayload)
    }
    
    @objc
    func readMoreButtonTapped() {
        delegate?.transactionStatusWidgetFired(action: readMoreAction, with: readMoreActionPayload)
    }
    
    private func setIcon(_ icon: TransactionStatusReceiptWidgetItem.Icon) {
        switch icon.type {
        case .asset:
            iconImageView.image = UIImage(named: icon.value)
        case .url:
            iconImageView.setImage(url: URL(string: icon.value))
        }
    }
    
    func setActionButton(_ button: TransactionStatusReceiptWidgetItem.ActionButton) {
        action = button.action
        actionPayload = button.payload
        actionButton.setTitle(button.title, for: .normal)
        actionButton.backgroundColor = button.backgroundColor
        actionButton.sizeToFit()
    }
    
    func setActionReadMore(_ button: TransactionStatusReceiptWidgetItem.ReadMoreButton) {
        let auxButton = Button(title: button.title)
        readMoreAction = button.action
        readMoreActionPayload = button.payload
        auxButton.titleColor = PicPayStyle.mediumGreen1Hex
        auxButton.type = .underlined
        readMoreButton.configure(with: auxButton)
    }
    
    func basicSetup() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    func executeMainConstraints() {
        if subviews.contains(iconImageView) {
            titleLabel.topAnchor.constraint(equalTo: iconImageView.bottomAnchor, constant: .vTitleTopIconSpacing).isActive = true
        } else {
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: .vMargin).isActive = true
        }
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: titleLabel, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: .hMargin),
            NSLayoutConstraint(item: titleLabel, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -.hMargin)
        ])
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: subtitleLabel, attribute: .top, relatedBy: .equal, toItem: titleLabel, attribute: .bottom, multiplier: 1, constant: .vLabelSpacing),
            NSLayoutConstraint(item: subtitleLabel, attribute: .left, relatedBy: .equal, toItem: titleLabel, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: subtitleLabel, attribute: .right, relatedBy: .equal, toItem: titleLabel, attribute: .right, multiplier: 1, constant: 0)
        ])
    }
    
    private func insertButton() {
        addSubview(actionButton)
        
        NSLayoutConstraint.activate([
            actionButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            actionButton.topAnchor.constraint(equalTo: subtitleLabel.bottomAnchor, constant: .vButtonSpacing),
            actionButton.widthAnchor.constraint(lessThanOrEqualTo: widthAnchor, constant: -32),
            actionButton.heightAnchor.constraint(equalToConstant: .buttonHeight)
        ])
        
        actionButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }
    
    private func insertReadMoreButton() {
        addSubview(readMoreButton)
        let readMoreButtonTopAnchor = subviews.contains(actionButton) ? actionButton.bottomAnchor : subtitleLabel.bottomAnchor
        
        NSLayoutConstraint.activate([
            readMoreButton.topAnchor.constraint(equalTo: readMoreButtonTopAnchor, constant: .vButtonSpacing),
            readMoreButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            readMoreButton.widthAnchor.constraint(equalToConstant: .buttonWidth),
            readMoreButton.heightAnchor.constraint(equalToConstant: .buttonHeight)
        ])
    }
    
    private func insertIcon() {
        addSubview(iconImageView)
        let width: CGFloat = 28
        let height: CGFloat = 28
        
        NSLayoutConstraint.activate([
            iconImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            iconImageView.topAnchor.constraint(equalTo: topAnchor, constant: .vMargin),
            iconImageView.widthAnchor.constraint(equalToConstant: width),
            iconImageView.heightAnchor.constraint(equalToConstant: height)
        ])
    }
}

extension TransactionStatusReceiptWidgetView: ReceiptPresentable {
    var contentHeight: CGFloat {
        var height: CGFloat = 0
        
        height += .vMargin * 2
        height += .vLabelSpacing
        height += titleLabel.intrinsicContentSize.height
        height += subtitleLabel.intrinsicContentSize.height
        
        if showActionButton {
            height += actionButton.intrinsicContentSize.height
            height += .vButtonSpacing * 2 // top and bottom
        }
        
        if subviews.contains(readMoreButton) {
            height += readMoreButton.intrinsicContentSize.height
            height += .vButtonSpacing
        }
        
        if subviews.contains(iconImageView) {
            height += .iconHeight
            height += .vTitleTopIconSpacing
        }
        
        return height
    }
    
    func contentView() -> UIView {
        return self
    }
}

private extension CGFloat {
    static let hMargin: CGFloat = 20
    static let vMargin: CGFloat = 25
    static let vLabelSpacing: CGFloat = 4
    static let buttonHeight: CGFloat = 32
    static let buttonWidth: CGFloat = 140
    static let vButtonSpacing: CGFloat = 16
    static let iconWidth: CGFloat = 28
    static let iconHeight: CGFloat = 28
    static let vTitleTopIconSpacing: CGFloat = 8
}
