enum ReceiptLocalizable: String, Localizable {
    
    case receiptCouldNotBeDisplayed
    case receiptImageHasBeenSaved
    case picpayNotAllowedSaveYourPhotoLibrary
    case imageCannotBeSaved
    case speedUpAnalysis
    case subscriptionDetails
    case okayILoveIt
    case yourSignature
    case transaction
    case youBoughtIn
    case descriptionStart
    case descriptionEnd
    case documentText
    
    var key: String {
        return rawValue
    }
    var file: LocalizableFile {
        return .receiptLegacy
    }
}
