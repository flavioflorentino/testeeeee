import AnalyticsModule
import CustomerSupport
import FeatureFlag
import SwiftyJSON
import UI
import UIKit
import Billet

// swiftlint:disable type_body_length
final class ReceiptWidgetViewController: PPBaseViewController, TransactionStatusActionDelegate, SubscriptionReceiptWidgetViewDelegate, OpenUrlReceiptWidgetViewDelegate, HtmlRenderReceiptWidgetViewDelegate {
    enum Observables: String {
        case NeedReloadReceipt = "ReceiptWidgetViewControllerNeedReloadReceipt"
    }
    
    enum UserInfoKey {
        case TransactionId
    }
    
    private enum AccelerateActionType: String {
        case P2P = "openP2PTransactionVerification"
        case PAV = "openPAVTransactionVerification"
    }
    
    private let maxBlur: CGFloat = 0.8
    private var initialTouchPoint = CGPoint(x: 0, y: 0)
    
    private lazy var blurEffectView: UIView = {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.alpha = 0
        return blurEffectView
    }()
    private lazy var mainView: UIView = {
        let mainView = UIView()
        mainView.backgroundColor = .clear
        mainView.translatesAutoresizingMaskIntoConstraints = false
        return mainView
    }()
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.delegate = self
        scrollView.backgroundColor = Palette.ppColorGrayscale000.color
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    private lazy var headerNavigation: UIView = {
        let headerNavigation = UIView()
        headerNavigation.layer.cornerRadius = 9
        headerNavigation.backgroundColor = Palette.ppColorGrayscale100.color
        headerNavigation.translatesAutoresizingMaskIntoConstraints = false
        return headerNavigation
    }()
    private lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.text = DefaultLocalizable.receipt.text
        titleLabel.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        return titleLabel
    }()
    private lazy var closeButton: UIButton = {
        let closeButton = UIButton()
        closeButton.setTitle(DefaultLocalizable.btClose.text, for: .normal)
        closeButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        closeButton.setTitleColor(Palette.ppColorBranding400.color, for: .normal)
        closeButton.addTarget(self, action: #selector(self.dismissViewController), for: .touchUpInside)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        return closeButton
    }()
    private lazy var actionButton: UIButton = {
        let actionButton = UIButton()
        actionButton.setTitle(DefaultLocalizable.options.text, for: .normal)
        actionButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        actionButton.setTitleColor(Palette.ppColorBranding400.color, for: .normal)
        actionButton.addTarget(self, action: #selector(self.presentActionSheet), for: .touchUpInside)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        return actionButton
    }()
    
    private var logoViewBottomConstraint: NSLayoutConstraint?
    private var modalViewWidthConstraint: NSLayoutConstraint?
    private var modalViewHeightConstraint: NSLayoutConstraint?
    
    var viewModel: ReceiptWidgetViewModel
    var previousWidgetView: UIView!
    private var mustShowWidgetsOnViewDidAppear = false
    private var widgetsAddedAndConfigured = false
    
    private var recommendationIncentiveViewController: RecommendationIncentiveViewController?
    
    private let vViewSpacing: CGFloat = 14
    private let bottomViewHeight: CGFloat = 96.0
    
    @objc
    var feedItemId: String?
    
    private lazy var logoView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "logoPicPayReceiptFooter")
        imageView.contentMode = .center
        return imageView
    }()
    
    private lazy var bottomView: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .vertical
        view.alignment = .center
        view.spacing = 2.0
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addArrangedSubview(logoView)
        view.addArrangedSubview(SpacerView(size: Spacing.base01))
        view.addArrangedSubview(documentLabel)
        view.addArrangedSubview(SpacerView(size: Spacing.base02))
        return view
    }()
    
    private lazy var documentLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale400())
        label.text = ReceiptLocalizable.documentText.text
        return label
    }()
    
    lazy var activityView: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView()
        activityView.translatesAutoresizingMaskIntoConstraints = false
        activityView.hidesWhenStopped = true
        activityView.style = .gray
        return activityView
    }()
    
    lazy internal var modalView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()
    
    lazy internal var shareActionSheet: UIAlertController = {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let saveImageAction = UIAlertAction(title: DefaultLocalizable.saveImage.text, style: .default, handler: { [weak self] _ in
            guard let self = self else {
                return
            }
            if let image = self.receiptImage {
                UIImageWriteToSavedPhotosAlbum(image, self, #selector(ReceiptWidgetViewController.presentImageSavedAlert), nil)
            } else {
                self.presentImageSavedAlert(image: nil, didFinishSavingWithError: NSError(domain: "br.com.pipay::ReceiptWidgetViewController", code: 0, userInfo: nil), contextInfo: nil)
            }
        })
        
        let cancelAction = UIAlertAction.init(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil)
        
        actionSheet.addAction(saveImageAction)
        actionSheet.addAction(cancelAction)
        return actionSheet
    }()
    
    // 1- Update the scrollview's to be the same as the content offset (remember to save both the current content offset and current frame)
    // 2- Create a new view that will be the one saved to the user's photo library
    // 3- Take snapshot
    // 4- Revert step one
    private var receiptImage: UIImage? {
        var image: UIImage?
        var size = scrollView.contentSize
        
        //Não permitir valores com 0, o contexto (do screenshot) irá retornar nil se algum valor for 0
        size.height = size.height.isZero ? UIScreen.main.bounds.height : size.height
        size.width = size.width.isZero ? UIScreen.main.bounds.width : size.width
        
        // 1
        let savedContentOffset = scrollView.contentOffset
        let savedFrame = scrollView.frame
        
        scrollView.contentOffset = CGPoint.zero
        scrollView.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        
        // 2
        let snapshotView = UIView(frame: scrollView.frame)
        snapshotView.addSubview(scrollView.copyView())
        
        // 3
        //Handle WebViews
        for case let htmlWidget as HtmlRenderReceiptWidgetView in scrollView.subviews {
            //Para WKWebView, é necessário um metodo diferente para salvar o conteúdo em imagem
            if let webviewImage = htmlWidget.content.snapshot {
                //Sobrepõe as WKWebView com uma UIImageView
                let imageView = UIImageView(frame: htmlWidget.frame)
                imageView.image = webviewImage
                snapshotView.addSubview(imageView)
            }
        }
        
        // 5
        image = snapshotView.snapshot

        // 6
        scrollView.contentOffset = savedContentOffset
        scrollView.frame = savedFrame
        
        return image
    }
    
    private var shareReceiptImageAction: UIAlertAction? {
        let action = UIAlertAction(title: DefaultLocalizable.btShare.text, style: .default, handler: { [weak self] _ in
            guard let image = self?.receiptImage else {
                return
            }
            let controller = UIActivityViewController(activityItems: [image], applicationActivities: nil)
            self?.present(controller, animated: true)
        })
        return action
    }
    
    var isLoading: Bool = false {
        didSet(oldValue) {
            if isLoading == oldValue {
                return
            }
            
            modalView.isHidden = !isLoading
            actionButton.isEnabled = !isLoading
            
            if isLoading {
                activityView.startAnimating()
            } else {
                activityView.stopAnimating()
            }
        }
    }
    
    private var analytics: Analytics {
        Analytics.shared
    }
    
    @objc
    init(viewModel: ReceiptWidgetViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .overFullScreen
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        self.viewModel = ReceiptWidgetViewModel(transactionId: "", type: .PAV)
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewModelCallbacks()
        setHeaderNavigation()
        setScrollView()
        configureModalView()
        registerObserver()
        callAnalytics()
        presentRecommendationIncentiveIfNeeded()
        
        analytics.log(ReceiptEvent.screenViewed(viewModel.transactionId, viewModel.type.rawValue))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard !widgetsAddedAndConfigured else {
            return
        }
        
        if viewModel.widgets.isEmpty {
            getTransactionWidgets()
            actionButton.isEnabled = false
        } else {
            isLoading = true
            mustShowWidgetsOnViewDidAppear = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard !widgetsAddedAndConfigured else {
            return
        }
        
        configureLogoView()
        showBlur()
        if mustShowWidgetsOnViewDidAppear {
            isLoading = false
            mustShowWidgetsOnViewDidAppear = false
            configureWidgets()
            viewModel.showAlertIfNecessary()
        }
    }
    
    override func loadView() {
        let view = UIView()
        view.backgroundColor = .clear
        self.view = view
        
        view.addSubview(blurEffectView)
        view.addSubview(mainView)
        NSLayoutConstraint.constraintAllEdges(from: mainView, to: view)
    }
    
    override func viewWillLayoutSubviews() {
        //Atualizar o tamanho(width) do modal
        modalViewWidthConstraint?.constant = scrollView.bounds.size.width
        
        //Ajuste com Safe area
        let safeAreaInset: CGFloat
        if #available(iOS 11.0, *) {
            safeAreaInset = view.safeAreaInsets.bottom
        } else {
            safeAreaInset = 0
        }
        
        //Manter a logo ao final da scrollView e atualizar a altura(height) do modal
        if scrollView.contentSize.height <= scrollView.bounds.size.height {
            logoViewBottomConstraint?.constant = scrollView.bounds.size.height - safeAreaInset
            modalViewHeightConstraint?.constant = scrollView.bounds.size.height
        } else {
            logoViewBottomConstraint?.constant = scrollView.contentSize.height
            modalViewHeightConstraint?.constant = scrollView.contentSize.height
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setupDarkModeLogo()
    }
    
    private func showBlur() {
        UIView.animate(withDuration: 0.5) {
            self.blurEffectView.alpha = self.maxBlur
        }
    }
    
    // MARK: NavigationBar
    private func setScrollView() {
        createPanGestureRecognizer(targetView: scrollView)
        mainView.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: headerNavigation.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor)
        ])
    }
    
    private func setHeaderNavigation() {
        mainView.addSubview(headerNavigation)
        headerNavigation.addSubview(titleLabel)
        headerNavigation.addSubview(closeButton)
        headerNavigation.addSubview(actionButton)
        headerNavigation.addSubview(actionButton)
        setConstraintHeader()
    }
    
    private func setConstraintHeader() {
        setLeftBottomConstraint()
        setRightBottomConstraint()
        setHeaderNavigationConstraint()
        
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: headerNavigation.centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: headerNavigation.centerYAnchor)
        ])
        
        NSLayoutConstraint.activate([
            closeButton.topAnchor.constraint(equalTo: headerNavigation.topAnchor),
            closeButton.bottomAnchor.constraint(equalTo: headerNavigation.bottomAnchor),
            closeButton.leadingAnchor.constraint(equalTo: headerNavigation.leadingAnchor, constant: 14)
        ])
        
        NSLayoutConstraint.activate([
            actionButton.topAnchor.constraint(equalTo: headerNavigation.topAnchor),
            actionButton.bottomAnchor.constraint(equalTo: headerNavigation.bottomAnchor),
            actionButton.trailingAnchor.constraint(equalTo: headerNavigation.trailingAnchor, constant: -14)
        ])
    }
    
    private func setLeftBottomConstraint() {
        let leftBottomView: UIView = UIView()
        leftBottomView.translatesAutoresizingMaskIntoConstraints = false
        leftBottomView.backgroundColor = headerNavigation.backgroundColor
        mainView.addSubview(leftBottomView)
        
        NSLayoutConstraint.activate([
            leftBottomView.widthAnchor.constraint(equalToConstant: 9),
            leftBottomView.heightAnchor.constraint(equalToConstant: 9),
            leftBottomView.leadingAnchor.constraint(equalTo: headerNavigation.leadingAnchor),
            leftBottomView.bottomAnchor.constraint(equalTo: headerNavigation.bottomAnchor)
        ])
        
    }
    
    private func setRightBottomConstraint() {
        let rightBottomView: UIView = UIView()
        rightBottomView.translatesAutoresizingMaskIntoConstraints = false
        rightBottomView.backgroundColor = headerNavigation.backgroundColor
        mainView.addSubview(rightBottomView)
        
        NSLayoutConstraint.activate([
            rightBottomView.bottomAnchor.constraint(equalTo: headerNavigation.bottomAnchor),
            rightBottomView.widthAnchor.constraint(equalToConstant: 9),
            rightBottomView.heightAnchor.constraint(equalToConstant: 9),
            rightBottomView.trailingAnchor.constraint(equalTo: headerNavigation.trailingAnchor)
        ])
    }
    
    private func setHeaderNavigationConstraint() {
        let offsetTop: CGFloat = 8
        if #available(iOS 11.0, *) {
            let guide = view.safeAreaLayoutGuide
            NSLayoutConstraint.activate([
                headerNavigation.topAnchor.constraint(equalTo: guide.topAnchor, constant: offsetTop)
            ])
        } else {
            let heightStatusBar = UIApplication.shared.statusBarFrame.size.height
            NSLayoutConstraint.activate([
                headerNavigation.topAnchor.constraint(equalTo: mainView.topAnchor, constant: heightStatusBar + offsetTop)
            ])
        }
        
        NSLayoutConstraint.activate([
            headerNavigation.heightAnchor.constraint(equalToConstant: 48),
            headerNavigation.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
            headerNavigation.trailingAnchor.constraint(equalTo: mainView.trailingAnchor)
        ])
    }

    // MARK: ReceiptWidgetViewController
    func configureLogoView() {
        setupDarkModeLogo()
        
        scrollView.addSubview(bottomView)
        scrollView.contentSize.height += bottomViewHeight
        
        let bottomConstraint = bottomView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor)
        logoViewBottomConstraint = bottomConstraint
        
        NSLayoutConstraint.activate([
            bottomConstraint,
            bottomView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor)
        ])
    }
    
    private func setupDarkModeLogo() {
        let imageAssetName: String = {
            if #available(iOS 13, *), traitCollection.userInterfaceStyle == .dark {
                return "logoPicPayReceiptFooterWhite"
            }
            return "logoPicPayReceiptFooter"
        }()
        
        logoView.image = #imageLiteral(resourceName: imageAssetName)
    }
    
    func configureModalView() {
        scrollView.addSubview(modalView)
        let widthConstraint = NSLayoutConstraint.init(item: modalView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        let heightConstraint = NSLayoutConstraint.init(item: modalView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        
        modalViewWidthConstraint = widthConstraint
        modalViewHeightConstraint = heightConstraint
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: modalView, attribute: .top, relatedBy: .equal, toItem: scrollView, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: modalView, attribute: .left, relatedBy: .equal, toItem: scrollView, attribute: .left, multiplier: 1, constant: 0),
            widthConstraint,
            heightConstraint
        ])
        
        modalView.addSubview(activityView)
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: activityView, attribute: .centerX, relatedBy: .equal, toItem: scrollView, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: activityView, attribute: .top, relatedBy: .equal, toItem: scrollView, attribute: .top, multiplier: 1, constant: 40)
        ])
    }
    
    private func setupViewModelCallbacks() {
        viewModel.showAlert = { alert in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: { [weak self] in
                AlertMessage.showAlert(alert, controller: self)
            })
        }
    }
    
    func getTransactionWidgets() {
        isLoading = true
        
        viewModel.getReceipt(onSuccess: { [weak self] in
            DispatchQueue.main.async {
                self?.isLoading = false
                self?.configureWidgets()
                self?.viewModel.showAlertIfNecessary()
            }
        }, onError: { [weak self] error in
            DispatchQueue.main.async {
                self?.isLoading = false
                self?.presentErrorAlert(withMessage: error?.localizedDescription)
            }
        })
    }
    
    // swiftlint:disable cyclomatic_complexity
    // swiftlint:disable function_body_length
    private func configureWidgets() {
        let widgets = viewModel.widgets
        previousWidgetView = scrollView
        
        for widgetItem in widgets {
            switch widgetItem.type {
            case .transaction:
                if let receipt = widgetItem as? TransactionReceiptWidgetItem {
                    //Set once
                    if let shareAction = shareReceiptImageAction {
                        shareActionSheet.addAction(shareAction)
                    }
                    addWidget(getTransactionReceiptWidgetView(from: receipt))
                }
            case .transactionStatus:
                if let receipt = widgetItem as? TransactionStatusReceiptWidgetItem {
                    let receiptView = TransactionStatusReceiptWidgetView(receipt)
                    receiptView.delegate = self
                    addWidget(receiptView)
                }
            case .voucher:
                if let receipt = widgetItem as? VoucherReceiptWidgetItem {
                    let receiptView = VoucherReceiptWidgetView(receipt)
                    receiptView.delegate = self
                    addWidget(receiptView)
                }
            case .expiryAt:
                if let receipt = widgetItem as? ExpiryAtReceiptWidgetItem {
                    addWidget(ExpiryAtReceiptWidgetView(receipt))
                }
            case .list:
                if let receipt = widgetItem as? ListReceiptWidgetItem {
                    addWidget(ListReceiptWidgetView(receipt))
                }
            case .subscription:
                if let receipt = widgetItem as? SubscriptionReceiptWidgetItem {
                    let receiptView = SubscriptionReceiptWidgetView(receipt)
                    receiptView.delegate = self
                    addWidget(receiptView)
                }
            case .openUrl:
                if let receipt = widgetItem as? OpenUrlReceiptWidgetItem {
                    let receiptView = OpenUrlReceiptWidgetView(model: receipt)
                    receiptView.delegate = self
                    addWidget(receiptView)
                }
            case .buttonAction:
                if let receipt = widgetItem as? ButtonActionReceiptWidgetItem {
                    let receiptView = ButtonReceiptWidgetView(receipt)
                    receiptView.delegate = self
                    addWidget(receiptView)
                }
            case .labelValue:
                if let receipt = widgetItem as? LabelValueReceiptWidgetItem {
                    let receiptView = LabelValueReceiptWidgetView(receipt)
                    addWidget(receiptView)
                }
            case .centralizedLabelValue:
                if let receipt = widgetItem as? LabelValueCentralizedReceiptWidgetItem {
                    let receiptView = LabelValueCentralizedReceiptWidgetView(receipt)
                    addWidget(receiptView)
                }
            case .webview:
                if let receitpt = widgetItem as? HtmlRenderReceiptWidgetItem {
                    let receiptView = HtmlRenderReceiptWidgetView(receitpt)
                    receiptView.delegate = self
                    addWidget(receiptView)
                }
            case .mgmOffer, .imageLabelValueWithButton:
                if let receipt = widgetItem as? ImageLabelValueButtonReceiptWidgetItem {
                    let receiptView = ImageLabelValueButtonReceiptWidgetView(receipt, parent: self)
                    addWidget(receiptView)
                }
            case .incentiveTransaction:
                if let receipt = widgetItem as? IncentiveTransactionWidgetItem {
                    configureRecommendationIncentiveViewController(item: receipt)
                }
            case .faq:
                guard let faqItem = widgetItem as? FaqReceiptWidgetItem else { return }
                
                let faqWidget = FaqReceiptWidgetView(faqItem, delegate: self)
                addWidget(faqWidget)
            case .pixTransaction:
                guard let item = widgetItem as? PixTransactionReceiptWidgetItem else {
                    break
                }
                if let shareAction = shareReceiptImageAction {
                    shareActionSheet.addAction(shareAction)
                }
                let pixWidget = PixTransactionWidget(item: item)
                addWidget(pixWidget)
            case .unkown, .cashback, .popup:
                break
            }
        }
        widgetsAddedAndConfigured = true
    }
    // swiftlint:enable cyclomatic_complexity
    // swiftlint:enable function_body_length
    
    private func getTransactionReceiptWidgetView(from item: TransactionReceiptWidgetItem) -> ReceiptPresentable {
        switch item.payee.type {
        case .Consumer:
            return ConsumerReceiptWidgetView(item)
        case .Store:
            return StoreReceiptWidgetView(itemStore: item)
        case .DigitalGoods:
            return DigitalGoodsReceiptWidgetView(item)
        case .Ecommerce:
            return StoreConfirmationReceiptWidgetView(item)
        case .Default:
            return DefaultReceiptWidgetView(item)
        }
    }
    
    private func configureRecommendationIncentiveViewController(item: IncentiveTransactionWidgetItem) {
        recommendationIncentiveViewController = RecommendationIncentiveFactory.make(
            recommendationIncentiveData: RecommendationIncentiveData(
                referralCode: item.userInfo.code,
                cashBackValue: item.cashbackValue,
                shareMessage: item.incentiveTransactioSocialShare.whatsApp?.text
            )
        )
    }
    
    private func presentRecommendationIncentiveIfNeeded() {
        guard let controller = recommendationIncentiveViewController else {
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: { [weak self] in
            self?.present(controller, animated: true)
        })
    }
    
    private func addWidget(_ receiptWidgetView: ReceiptPresentable) {
        let subview = receiptWidgetView.contentView()
        let subviewHeight = receiptWidgetView.contentHeight
        subview.alpha = 0
        
        self.scrollView.addSubview(subview)
        
        let topConstraint: NSLayoutConstraint
        
        if self.scrollView == self.previousWidgetView {
            //Primeiro Widget
            topConstraint = NSLayoutConstraint(item: subview, attribute: .top, relatedBy: .equal, toItem: previousWidgetView, attribute: .top, multiplier: 1, constant: 0)
        } else {
            topConstraint = NSLayoutConstraint(item: subview, attribute: .top, relatedBy: .equal, toItem: previousWidgetView, attribute: .bottom, multiplier: 1, constant: vViewSpacing)
        }
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: subview, attribute: .left, relatedBy: .equal, toItem: scrollView, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: subview, attribute: .centerX, relatedBy: .equal, toItem: scrollView, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: subview, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: scrollView.frame.size.width),
            NSLayoutConstraint(item: subview, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: subviewHeight),
            topConstraint
        ])
        
        UIView.animate(withDuration: 0.5, animations: {
            subview.alpha = 1
        })
        
        scrollView.contentSize.height += subviewHeight
        scrollView.contentSize.height += vViewSpacing
        
        //Referencia do ultimo widget
        previousWidgetView = subview
    }
    
    private func registerObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadReceiptNotificationHandler), name: NSNotification.Name(rawValue: Observables.NeedReloadReceipt.rawValue), object: nil)
    }
    
    private func callAnalytics() {
        guard case .p2pExternalTransfer = viewModel.type else {
            return
        }
        analytics.log(ExternalTransferUserEvent.receiptViewed(origin: .detailOptions))
    }
    
    @objc
    private func dismissViewController() {
        analytics.log(ReceiptEvent.dissmissReceipt(.closeButton))
        animateGestureDismissModal(time: 0.4)
    }
    
    /// Apresenta um alert que, após a sua exibição o aplicativo retorna para tela anterior.
    /// Ao enviar `nil` como valor para o parametro `withMessage`, exibe uma mensagem genérica.
    ///
    /// - Parameter message: message error
    private func presentErrorAlert(withMessage message: String?) {
        let msg = message ?? ReceiptLocalizable.receiptCouldNotBeDisplayed.text
        let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { [weak self] (_) in
            self?.dismissViewController()
        })
        alert.addAction(action)
        self.present(alert, animated: true)
    }

    /// Observer para recarregar o recibo
    ///
    /// - Parameter notification: notification with notification center
    @objc
    func reloadReceiptNotificationHandler(_ notification: Notification) {
        guard let transacationId = notification.userInfo?[UserInfoKey.TransactionId] as? String  else {
            return
        }
        guard transacationId == viewModel.transactionId else {
            return
        }

        for view in scrollView.subviews where view is ReceiptPresentable {
            guard let receiptWidget = view as? ReceiptPresentable else { continue }
            scrollView.contentSize.height -= receiptWidget.contentHeight
            scrollView.contentSize.height -= vViewSpacing
            view.removeFromSuperview()
        }
        getTransactionWidgets()
    }
    
    /// Ação ao apertar o botão do lado direito da navigation bar
    @objc
    func presentActionSheet() {
        present(shareActionSheet, animated: true)
    }
    
    /// Ação executada após a tentativa de salvar a imagem na biblioteca de fotos
    ///
    /// - Parameters:
    ///   - image: image
    ///   - error: error
    ///   - contextInfo: info
    @objc
    func presentImageSavedAlert(image: UIImage?, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer?) {
        let alert: UIAlertController
        if error == nil {
            alert = UIAlertController(title: DefaultLocalizable.success.text, message: ReceiptLocalizable.receiptImageHasBeenSaved.text, preferredStyle: .alert)
        } else {
            let message: String
            if error?.code == -3310 {
                message = ReceiptLocalizable.picpayNotAllowedSaveYourPhotoLibrary.text
            } else if let description = error?.userInfo[NSLocalizedDescriptionKey] as? String {
                message = description
            } else {
                message = ReceiptLocalizable.imageCannotBeSaved.text
            }
            alert = UIAlertController(title: "Ops", message: message, preferredStyle: .alert)
        }
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    //MARK: TransactionStatusActionDelegate
    func transactionStatusWidgetFired(action: TransactionStatusReceiptWidgetItem.Action, with payload: [AnyHashable: Any]?) {
        switch action {
        case .accelerateAnalysis,
             .accelerateAnalysisPav:
            
            var actionType: String = AccelerateActionType.P2P.rawValue
            if(action == .accelerateAnalysisPav) {
                actionType = AccelerateActionType.PAV.rawValue;
            }
            
            if let analisysAcceleration = ViewsManager.antiFraudStoryboardViewController(withIdentifier: "AnalysisAccelerationViewController") as? AnalysisAccelerationViewController {
                analisysAcceleration.transactionId = viewModel.transactionId
                analisysAcceleration.feedItemId = feedItemId ?? ""
                analisysAcceleration.actionType = actionType
                analisysAcceleration.title = ReceiptLocalizable.speedUpAnalysis.text
                let navController = DismissibleNavigationViewController(rootViewController: analisysAcceleration)
                present(navController, animated: true)
            }
        case .helpCenter:
            if let query = payload?["query"] as? String {
                guard let option = HelpCenterOptions(rawValue: query) else { return }
                let faqOption = option.faqOption
                let faq = FAQFactory.make(option: faqOption)
                let navigation = UINavigationController(rootViewController: faq)
                present(navigation, animated: true)
            }
        case .deeplink:
            if let urlString = payload?["url"] as? String, let url = URL(string: urlString) {
                DeeplinkHelper.handleDeeplink(withUrl: url, from: self)
            }
        case .NotSpecified:
            break
        }
    }
    
    // MARK: SubscriptionReceiptWidgetViewDelegate\
    func seeMoreDetailButtonFired(_ sender: UIButton?, buttonModel: SeeDetailsButtonItem?) {
        let errorMessage: String = ReceiptLocalizable.subscriptionDetails.text

        if let subscriptionId = buttonModel?.subscriptionId {
            let showSubscriptionDetails: ((ConsumerSubscription) -> Void) = { [weak self] (subscription) in
                guard let self = self else {
                    return
                }
                let subscriptionViewController = SubscriptionViewController(model: SubscriptionViewModel(consumer: subscription))
                subscriptionViewController.headerView.optionsButtonIsHidden = true
                subscriptionViewController.showActionButton = false
                subscriptionViewController.onClose = { (sender) in
                    subscriptionViewController.dismiss(animated: true, completion: nil)
                }
                self.present(subscriptionViewController, animated: true, completion: nil)
            }
            
            if let subscription = buttonModel?.subscription {
                showSubscriptionDetails(subscription)
            } else {
                let buttonTitle: String? = sender?.currentTitle
                
                UIView.animate(withDuration: 0.1, animations: {
                    sender?.alpha = 0
                }, completion: { (_) in
                    sender?.setTitle(DefaultLocalizable.webviewLoading.text, for: sender?.state ?? .normal)
                    UIView.animate(withDuration: 0.1, animations: {
                        sender?.alpha = 0.5
                    })
                })
                
                SubscriptionViewModel.getSubscription(by: subscriptionId, { [weak self] (result, error) in
                    guard let strongSelf = self else {
                        return
                    }
                    if let subscription = result {
                        buttonModel?.subscription = subscription
                        showSubscriptionDetails(subscription)
                    } else {
                        AlertMessage.showAlert(withMessage: error?.localizedDescription ?? errorMessage, controller: strongSelf)
                    }
                    
                    sender?.setTitle(buttonTitle, for: sender?.state ?? .normal)
                    sender?.alpha = 1
                    sender?.isUserInteractionEnabled = true
                })
            }
        } else {
            AlertMessage.showAlert(withMessage: errorMessage, controller: self)
        }
    }
    
    
    //MARK: OpenWithReceiptWidgetViewDelegate
    
    func openUrlButtonFired(urlToOpen: String) {
        if let url = URL(string: urlToOpen) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    //MARK: HtmlRenderReceiptWidgetViewDelegate
    func webviewDidLoad(widgetView: HtmlRenderReceiptWidgetView, isShowingContent: Bool) {
        guard let heightConstraint = widgetView.constraints.first(where: {$0.firstAttribute == .height}) else {
            return
        }
        
        let diff = widgetView.content.scrollView.contentSize.height - heightConstraint.constant
        
        //Atualiza a constraint de altura do widget
        heightConstraint.constant = widgetView.content.scrollView.contentSize.height

        //Remove espaço acima do widget caso não exibir nenhum conteudo
        if !isShowingContent, let topSpacingConstraint = widgetView.superview?.constraints.first(where: { $0.firstItem === widgetView && $0.firstAttribute == .top }) {
            topSpacingConstraint.constant = 0
            self.scrollView.contentSize.height -= self.vViewSpacing
        }
        
        //Anima ao redimensionar a view
        UIView.animate(withDuration: 0.25, animations: {
            //Adiciona ou remove o espaço extra na scrollView
            self.scrollView.contentSize.height += diff
            
            self.view.layoutIfNeeded()
        }) { (_) in
            if !isShowingContent {
                //houve algum erro, remove as subviews
                widgetView.subviews.forEach({$0.removeFromSuperview()})
            }
        }
    }
    
    private func createPanGestureRecognizer(targetView: UIView) {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.panGestureRecognizerHandler(_:)))
        panGesture.delegate = self
        targetView.addGestureRecognizer(panGesture)
    }
    
    @objc(panGestureRecognizerHandler:)
    private func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        switch sender.state {
        case .began:
            gestureRecognizerStateBegan(touchPoint: touchPoint)
        case .changed:
            gestureRecognizerStateChanged(touchPoint: touchPoint)
        case .ended, .cancelled:
            let velocity = Double(sender.velocity(in: self.scrollView).y)
            gestureRecognizerStateEnded(touchPoint: touchPoint, velocity: velocity)
        default: break
        }
    }
    
    /// Calculates the initial touch point, considering the scroll offset so
    /// that the dismiss occurs when the scroll is at top
    ///
    /// - Parameters:
    ///     - touchPoint: Point the user clicked on the screen
    private func gestureRecognizerStateBegan(touchPoint: CGPoint) {
        let scrollOffset = scrollView.contentOffset.y
        initialTouchPoint.y = touchPoint.y + scrollOffset
    }
    
    /// If the scroll is at the top, slide the view down to decrease
    /// blur according gesture of the user
    ///
    /// - Parameters:
    ///     - touchPoint: Point the user clicked on the screen
    private func gestureRecognizerStateChanged(touchPoint: CGPoint) {
        if touchPoint.y - initialTouchPoint.y > 0 {
            blurEffectView.alpha = maxBlur - (touchPoint.y - initialTouchPoint.y) / mainView.frame.size.height
            mainView.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: mainView.frame.size.width, height: mainView.frame.size.height)
            scrollView.isScrollEnabled = false
        }
    }
    
    /// With the End gesture check:
    /// If modal passed 1/3 of screen, starts dismissal animation, informing the speed
    /// Else does animation that return the original parameters
    ///
    /// - Parameters:
    ///     - touchPoint: Point the user clicked on the screen
    ///     - velocity: Gesture speed
    private func gestureRecognizerStateEnded(touchPoint: CGPoint, velocity: Double) {
        if (touchPoint.y - initialTouchPoint.y) > (mainView.frame.size.height / 3) {
            let dist = Double(touchPoint.y - initialTouchPoint.y)
            var time = dist / velocity
            time = time > 1.0 ? 0.5 : time
            
            analytics.log(ReceiptEvent.dissmissReceipt(.swipeDown))
            animateGestureDismissModal(time: time)
        } else {
            animateGestureCancelModal()
        }
    }

    /// Start the animation of dismmis, push the view down and decrease the blur.
    /// At end of the animation view dismmis
    ///
    /// - Parameters:
    ///     - time: Duration of animation
    private func animateGestureDismissModal(time: TimeInterval) {
        UIView.animate(withDuration: time, animations: { [weak self] in
            guard let self = self else {
                return
            }
            self.blurEffectView.alpha = 0
            self.mainView.frame.origin.y = self.mainView.frame.size.height
        }) { [weak self] _ in
            guard let self = self else {
                return
            }
            self.sendUpdateFeedNotification()
            self.dismiss(animated: false, completion: {
                self.viewModel.checkReviewPrivacy()
            })
        }
    }
    
    /// Start the animation returns position of modal and the value
    /// blur to original value
    private func animateGestureCancelModal() {
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            guard let self = self else {
                return
            }
            self.blurEffectView.alpha = self.maxBlur
            self.mainView.frame = CGRect(x: 0, y: 0, width: self.mainView.frame.size.width, height: self.mainView.frame.size.height)
        }) { [weak self] _ in
            guard let self = self else {
                return
            }
            self.scrollView.isScrollEnabled = true
        }
    }
    
    private func sendUpdateFeedNotification() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: FeedViewController.Observables.FeedNeedReload.rawValue), object: nil)
    }
}

// MARK: - VoucherReceiptWidgetViewDelegate
extension ReceiptWidgetViewController: VoucherReceiptWidgetViewDelegate {
    func voucherWidgetFiredAction(open urlString: String, needsAuth: Bool) {
        guard let url = URL(string: urlString) else {
            return
        }
        
        let webview = WebViewFactory.make(with: url)
        present(UINavigationController(rootViewController: webview), animated: true)
    }
    
    func copyVoucherToClipboard(voucher: String, widget: VoucherReceiptWidgetView, enableTouch: @escaping (() -> Void)) {
        UIPasteboard.general.string = voucher
        widget.content = "Copiado!"
        // Wait two seconds and update the label back to the old value
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
            widget.content = voucher
            enableTouch()
        }
    }
}

// MARK: - ButtonReceiptWidgetViewDelegate
extension ReceiptWidgetViewController: ButtonReceiptWidgetViewDelegate {
    func didTapButton(actionType: ReceiptWidgetButtonActionType) {
        AppManager.shared.mainScreenCoordinator?.showPaymentSearchScreen(searchText: "", page: "", ignorePermissionPrompt: true)
        if let vc = AppManager.shared.mainScreenCoordinator?.paymentSearchNavigation() {
            self.dismiss(animated: true, completion: {
                // It's ok to pass a empty DGItem because the helper will pull data from remote config
                guard FeatureManager.isActive(.isNewBilletHubAvailable) else {
                    DGHelpers.openBoletoFlow(viewController: vc, origin: "receipt")
                    return
                }
                
                let hubViewController = BilletHubFactory.make(with: .receipt)
                let navigation = UINavigationController(rootViewController: hubViewController)
                self.present(navigation, animated: true)
            })
        }
    }
}

// MARK: - FaqReceiptWidgetViewDelegate
extension ReceiptWidgetViewController: FaqReceiptWidgetViewDelegate {
    func didTouchFaq(link: String) {
        guard let url = URL(string: link) else { return }
        DeeplinkHelper.handleDeeplink(withUrl: url, from: self)
    }
}

// MARK: - UIScrollViewDelegate
extension ReceiptWidgetViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.bounces = scrollView.contentOffset.y > 100
    }
}

// MARK: - UIGestureRecognizerDelegate
extension ReceiptWidgetViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
