import Core
import CorePayment
import FeatureFlag
import PIX
import Store
import Card

final class ReceiptWidgetViewModel: NSObject, SuccessPayment {
    @objc
    enum ReceiptType: Int, RawRepresentable {
        public typealias RawValue = String
        
        case PAV
        case debitReceipt
        case P2P
        case p2pExternalTransfer
        case store
        case pix
        case pixReceiver
        case p2pLending
        case unknown
        
        init(rawValue: String) {
            switch rawValue {
            case "pav":
                self = .PAV
            case "debitReceipt":
                self = .debitReceipt
            case "p2p":
                self = .P2P
            case "P2pExternalTransfer":
                self = .p2pExternalTransfer
            case "store":
                self = .store
            case "pix":
                self = .pix
            case "pixReceiver":
                self = .pixReceiver
            case "p2pLending":
                self = .p2pLending
            default:
                self = .unknown
            }
        }
        
        var rawValue: RawValue {
            switch self {
            case .PAV:
                return "pav"
            case .debitReceipt:
                return "debitReceipt"
            case .P2P:
                return "p2p"
            case .p2pExternalTransfer:
                return "P2pExternalTransfer"
            case .store:
                return "store"
            case .pix:
                return "pix"
            case .pixReceiver:
                return "pixReceiver"
            case .p2pLending:
                return "p2pLending"
            case .unknown:
                return "unknown"
            }
        }
    }
    @objc var transactionId: String
    var type: ReceiptType
    private let pixService = ReceiptService(dependencies: DependencyContainer())
    private let storeReceiptService = StoreReceiptService(dependencies: DependencyContainer())
    private let debitReceiptService = DebitReceiptService(dependencies: DependencyContainer())
    var widgets: [ReceiptWidgetItem] = []
    var alertPopupOnStart: Alert?
    var showAlert: ((_ alert: Alert) -> Void)?
    var needsDismissPresentingControllers: Bool = true
    
    convenience init(type: ReceiptType) {
        self.init(transactionId: "", type: type)
    }
    
    @objc
    init(transactionId: String, type: ReceiptType) {
        self.transactionId = transactionId
        self.type = type
        super.init()
    }
    
    func getReceipt(onSuccess: @escaping () -> Void, onError: @escaping (Error?) -> Void) {
        widgets = []
        switch type {
        case .pix:
            pixService.getReceiptPix(transactionId: transactionId) { [weak self] result in
                self?.handlePixReceiptResponse(onSuccess: onSuccess, onError: onError, result: result)
            }
        case .pixReceiver:
            pixService.getReceiverReceiptPixPF(transactionId: transactionId) { [weak self] result in
                self?.handlePixReceiptResponse(onSuccess: onSuccess, onError: onError, result: result)
            }
        case .store:
            handleStoreReceipt(onSuccess: onSuccess, onError: onError)
        case .debitReceipt:
            handleDebitReceipt(onSuccess: onSuccess, onError: onError)
        case .unknown:
            onError(nil)
        default:
            WSReceipt.getReceipt(transactionId: self.transactionId, type: self.type.rawValue, completion: { [weak self] widgets, error in
                self?.setReceiptWidgets(items: widgets, isLoading: true)
                
                guard widgets != nil else {
                    onError(error)
                    return
                }
                onSuccess()
            })
        }
    }
    
    private func handleStoreReceipt(onSuccess: @escaping () -> Void, onError: @escaping (Error?) -> Void) {
        storeReceiptService.getReceipt(transactionID: transactionId) { [weak self] result in
            switch result {
            case let .success(response):
                let widgets = response.data.receipt.compactMap(WSReceipt.createReceiptWidgetItem)
                self?.setReceiptWidgets(items: widgets.isEmpty ? nil : widgets, isLoading: true)
                onSuccess()
            case let .failure(error):
                onError(error)
            }
        }
    }

    private func handleDebitReceipt(onSuccess: @escaping () -> Void, onError: @escaping (Error?) -> Void) {
        debitReceiptService.getReceipt(transactionID: transactionId) { [weak self] result in
            switch result {
            case let .success(response):
                let widgets = response.data.receipt.compactMap(WSReceipt.createReceiptWidgetItem)
                self?.setReceiptWidgets(items: widgets.isEmpty ? nil : widgets, isLoading: true)
                onSuccess()
            case let .failure(error):
                onError(error)
            }
        }
    }
    
    private func handlePixReceiptResponse(onSuccess: @escaping () -> Void, onError: @escaping (Error?) -> Void, result: Result<(model: ReceiptResponse, data: Data?), ApiError>) {
        switch result {
        case let .success(response):
            let widgets = response.model.data.receipt.compactMap(WSReceipt.createReceiptWidgetItem)
            setReceiptWidgets(items: widgets.isEmpty ? nil : widgets, isLoading: true)
            onSuccess()
        case let .failure(error):
            onError(error)
        }
    }
    
    /// Set widget
    ///
    /// - Parameters:
    ///   - items: items
    ///   - isLoading: if loadding widget of api or other view controller
    @objc
    func setReceiptWidgets(items: [ReceiptWidgetItem]?, isLoading: Bool = false) {
        guard var widgetsItems = items else {
            return
        }
        widgetsItems = widgetsItems.sorted(by: { $0.order < $1.order })
        
        // Set cashback popup for open popup in feed view
        // only called for view controllers opened of ReceipViewController
        if !isLoading {
            for case let item as CashbackReceipWidgetItem in widgetsItems {
                item.setCashbackPopupPedding()
            }
        }
        
        for case let item in widgetsItems {
            if item.type == .popup {
                setAlertPopup(item)
            } else {
                widgets.append(item)
            }
        }
    }
    
    private func setAlertPopup(_ item: ReceiptWidgetItem) {
        guard let popupItem = item as? PopupReceiptWidgetItem, popupItem.displayEvent == .onStart else {
            return
        }
        alertPopupOnStart = popupItem.alert
    }
    
    func showAlertIfNecessary() {
        if let alertPopup = alertPopupOnStart {
            showAlert?(alertPopup)
            alertPopupOnStart = nil
        }
    }
    
    // MARK: métodos da revisão de configuração de privacidade
    func checkReviewPrivacy() {
        let updatePrivacyService = UpdatePrivacyService(dependencies: DependencyContainer())
        let helper = PrivacyConfigHelper(updatePrivacyService: updatePrivacyService)
        helper.checkReviewPrivacy(cameFromReceipt: true) { privacyResult in
            guard let privacyResult = privacyResult else { return }
            
            self.trackingAPIResult(with: privacyResult)
            
            guard privacyResult.shouldReview else { return }
            self.redirectToReviewPrivacy()
        }
    }
    
    private func redirectToReviewPrivacy() {
        let viewController = UpdatePrivacyMessageFactory.make(origin: .transaction)
        AppManager.shared.mainScreenCoordinator?.showHomeScreen()
        guard let navigationController = AppManager.shared.mainScreenCoordinator?.homeNavigation() else {
            return
        }
        
        viewController.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(viewController, animated: true)
    }
    
    private func trackingAPIResult(with privacyResult: PrivacyConfig) {
        let dependencies = DependencyContainer()
        let consumerId = dependencies.consumerManager.consumer?.wsId.toString()
        
        if privacyResult.apiResultIsSuccess {
            dependencies.analytics.log(
                UpdatePrivacyAPIEvent.calledApiFromReceiptSuccess(consumerId, isReviewed: privacyResult.isReviewed)
            )
        } else {
            dependencies.analytics.log(UpdatePrivacyAPIEvent.calledApiFromReceiptError(consumerId))
        }
    }
}
