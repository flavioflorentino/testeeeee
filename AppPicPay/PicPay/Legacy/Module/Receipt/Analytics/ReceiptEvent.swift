import Foundation
import AnalyticsModule

enum ReceiptDismissType {
    case closeButton
    case swipeDown
}

enum ReceiptEvent: AnalyticsKeyProtocol {
    case screenViewed(_ transactionId: String, _ transactionType: String)
    case dissmissReceipt(_ dissmissType: ReceiptDismissType)
    
    var name: String {
        switch self {
        case .screenViewed:
            return Constants.screenEventName.rawValue
        case .dissmissReceipt:
            return Constants.buttonClicked.rawValue
        }
    }
    
    var properties: [String: Any] {
        switch self {
        case let .screenViewed(transactionId, transactionType):
            return [
                Keys.transactionId.rawValue: transactionId,
                Keys.transactionType.rawValue: transactionType
            ]
        case let .dissmissReceipt(dissmissType):
            return [
                Keys.screenName.rawValue: Constants.receiptScreenName.rawValue,
                Keys.buttonName.rawValue: Constants.buttonName.rawValue,
                Keys.businessContext.rawValue: Constants.bussinessContextName.rawValue,
                Keys.methodCall.rawValue: getDismissType(dissmissType)
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker, .mixPanel])
    }
}

private extension ReceiptEvent {
    enum Keys: String {
        case transactionId = "transaction_id"
        case transactionType = "transaction_type"
        case businessContext = "business_context"
        case screenName = "screen_name"
        case buttonName = "button_name"
        case methodCall = "method_call"
    }
    enum Constants: String {
        case screenEventName = "Receipt Viewed"
        case buttonClicked = "Button Clicked"
        case receiptScreenName = "RECIBO"
        case buttonName = "FECHAR"
        case methodClick = "CLICK"
        case methodSwipeDown = "SWIPE_DOWN"
        case bussinessContextName = "TRANSACAO"
    }
    
    func getDismissType(_ type: ReceiptDismissType) -> String {
        type == .closeButton
            ? Constants.methodClick.rawValue
            : Constants.methodSwipeDown.rawValue
    }
}
