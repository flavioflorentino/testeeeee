import UI
import Foundation

protocol ProfileStudentAccountBarViewActionDelegate: AnyObject {
    func profileStudentAccountBarDidTapOffer(bar: ProfileStudentAccountBarView)
}

extension ProfileStudentAccountBarView.Layout {
    enum Size {
        static let heightLine: CGFloat = 1.0
    }
    
    enum Font {
        static let regular = UIFont.systemFont(ofSize: 14, weight: .regular)
        static let semibold = UIFont.systemFont(ofSize: 14, weight: .semibold)
    }
}

final class ProfileStudentAccountBarView: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .fillEqually
        stackView.spacing = Spacing.base00
        
        return stackView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.isHidden = true
        label.textAlignment = .center
        label.sizeToFit()
        label.font = Layout.Font.regular
        label.textColor = Palette.ppColorGrayscale600.color
        
        return label
    }()
    
    private lazy var offerButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.addTarget(self, action: #selector(profileStudentAccountBarDidTapOffer(_:)), for: .touchUpInside)
        button.updateColor(
            Palette.ppColorGrayscale600.color,
            highlightedColor: Palette.ppColorGrayscale600.color,
            font: Layout.Font.semibold
        )
        return button
    }()
    
    weak var delegate: ProfileStudentAccountBarViewActionDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = Palette.ppColorGrayscale100.color(withCustomDark: .ppColorGrayscale500)
        layer.cornerRadius = 4
        layer.masksToBounds = true
    }
    
    func buildViewHierarchy() {
        stackView.addArrangedSubview(descriptionLabel)
        stackView.addArrangedSubview(offerButton)
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.layout {
            $0.top == topAnchor + Spacing.base01
            $0.leading == leadingAnchor + Spacing.base01
            $0.trailing == trailingAnchor - Spacing.base01
            $0.bottom == bottomAnchor - Spacing.base00
        }
    }
    
    func update(with component: ProfileStudentAccountComponent) {
        updateButton(text: component.buttonText, color: component.buttonColor, darkColor: component.buttonDarkColor)
        updateBackground(color: component.backgroundColor, darkColor: component.backgroundDarkColor)
        
        if
            let description = component.description,
            let descriptionStringColor = component.descriptionColor,
            let descriptionStringDarkColor = component.descriptionDarkColor
        {
            updateDescription(text: description, color: descriptionStringColor, darkColor: descriptionStringDarkColor)
        }
    }
    
    private func updateButton(text: String, color: String, darkColor: String) {
        guard
            let buttonColor = Palette.hexColor(with: color, mapToDarkMode: false),
            let buttonDarkColor = Palette.hexColor(with: darkColor, mapToDarkMode: false)
            else {
                return
        }
        offerButton.setTitle(text, for: .normal)
        offerButton.updateColor(
            buttonColor.withCustomDarkColor(buttonDarkColor),
            highlightedColor: buttonColor.withCustomDarkColor(buttonDarkColor)
        )
    }
    
    private func updateDescription(text: String, color: String, darkColor: String) {
        guard
            let descriptionColor = Palette.hexColor(with: color, mapToDarkMode: false),
            let descriptionDarkColor = Palette.hexColor(with: darkColor, mapToDarkMode: false) else {
                return
        }
        descriptionLabel.isHidden = false
        descriptionLabel.textColor = descriptionColor.withCustomDarkColor(descriptionDarkColor)
        descriptionLabel.text = text
        stackView.layoutIfNeeded()
    }
    
    private func updateBackground(color: String, darkColor: String) {
        guard
            let backgroundComponentColor = Palette.hexColor(with: color, mapToDarkMode: false),
            let backgroundComponentDarkColor = Palette.hexColor(with: darkColor, mapToDarkMode: false)
            else {
                return
        }
        backgroundColor = backgroundComponentColor.withCustomDarkColor(backgroundComponentDarkColor)
    }
    
    @objc
    private func profileStudentAccountBarDidTapOffer(_ sender: Any) {
        delegate?.profileStudentAccountBarDidTapOffer(bar: self)
    }
}
