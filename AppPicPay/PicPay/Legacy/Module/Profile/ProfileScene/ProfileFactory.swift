import Foundation

final class ProfileFactory {
    static func make(profileId: String, placeHolderContact: PPContact? = nil) -> ConsumerProfileViewController {
        let container = DependencyContainer()
        let service: ProfileServicing = ProfileService(dependencies: container)
        let coordinator: ProfileCoordinating = ProfileCoordinator()
        let paymentRequestService = PaymentRequestSelectorService(dependencies: container)
        let presenter: ProfilePresenting = ProfilePresenter(coordinator: coordinator, dependencies: container)
        let viewModel = NewProfileViewModel(
            profileId: profileId,
            placeHolderContact: placeHolderContact,
            service: service,
            paymentRequestService: paymentRequestService,
            presenter: presenter,
            dependencies: container
        )
        let viewController = ConsumerProfileViewController(model: viewModel)
        
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
