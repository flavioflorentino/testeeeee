import Core
import UI
import AnalyticsModule
import DirectMessage
import DirectMessageSB

protocol ProfilePresenting: AnyObject {
    var viewController: ProfileDisplay? { get set }
    func presentStudentAccountView(with component: ProfileStudentAccountComponent)
    func presentStudentAccountOffer(url: URL)
    func presentPaymentRequestNotAllowedAlert(username: String)
    func presentPaymentRequestLimitExceeded()
    func presentPaymentRequestLimitExceededV2(title: String, message: String, buttonTitle: String)
    func presentPaymentRequestSuccess(profile: PPContact)
    func presentPaymentRequestError()
    func openDirectMessage(sender: DirectMessage.Contact, recipient: DirectMessage.Contact)
    func openDirectMessageSB(with user: ChatUser)
    func setMessageButtonVisibility(_ visibility: Bool)
}

final class ProfilePresenter: ProfilePresenting {
    typealias Dependencies = HasAnalytics
    
    private let dependencies: Dependencies
    private let coordinator: ProfileCoordinating
    weak var viewController: ProfileDisplay?
    
    init(coordinator: ProfileCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func presentStudentAccountView(with component: ProfileStudentAccountComponent) {
        viewController?.displayStudentAccountView(with: component)
    }
    
    func presentStudentAccountOffer(url: URL) {
        dependencies.analytics.log(StudentAccountEvent.view(screen: .offerWebview, origin: .profile))
        coordinator.perform(action: .openStudentWebview(url: url))
    }
    
    func presentPaymentRequestNotAllowedAlert(username: String) {
        let alertTitle = PaymentRequestLocalizable.paymentRequestNotAllowedTitle.text
        let alertMessage = String(format: PaymentRequestLocalizable.paymentRequestNotAllowedMessage.text, username)
        let alertButton = PaymentRequestLocalizable.privacyAlertButton.text
        viewController?.displayAlert(title: alertTitle, message: alertMessage, buttonTitle: alertButton)
    }
    
    func presentPaymentRequestLimitExceeded() {
        let text = PaymentRequestLocalizable.paymentRequestExceededAlertText.text
        let attributedText = createAttributedText(text)
        let alertData = StatusAlertAttributedData(
            icon: Assets.Emotions.iluSadFace.image,
            title: PaymentRequestLocalizable.paymentRequestExceededAlertTitle.text,
            text: attributedText,
            buttonTitle: PaymentRequestLocalizable.paymentRequestExceededAlertOtherButton.text,
            isCloseButtonHidden: false
        )
        viewController?.displayPaymentRequestLimitExceeded(with: alertData)
    }
    
    func presentPaymentRequestLimitExceededV2(title: String, message: String, buttonTitle: String) {
        let attributedText = createAttributedText(message)
        let alertData = StatusAlertAttributedData(
            icon: Assets.Emotions.iluSadFace.image,
            title: title,
            text: attributedText,
            buttonTitle: buttonTitle,
            isCloseButtonHidden: false
        )
        viewController?.displayPaymentRequestLimitExceeded(with: alertData)
    }
    
    private func createAttributedText(_ text: String) -> NSAttributedString {
        let normalAttributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: Palette.ppColorGrayscale500.color,
            .font: Typography.bodySecondary().font()
        ]
        return NSAttributedString(string: text, attributes: normalAttributes)
    }
    
    func presentPaymentRequestSuccess(profile: PPContact) {
        coordinator.perform(action: .paymentRequest(userProfile: profile))
        viewController?.displayPaymentRequestSuccess()
    }
    
    func presentPaymentRequestError() {
        let alertData = StatusAlertData(
            icon: Assets.Emotions.iluSadFace.image,
            title: PaymentRequestLocalizable.somethingWentWrong.text,
            text: PaymentRequestLocalizable.requestNotCompleted.text,
            buttonTitle: PaymentRequestLocalizable.tryAgain.text,
            isCloseButtonHidden: false
        )
        viewController?.displayPaymentRequestError(with: alertData)
    }

    func openDirectMessage(sender: Contact, recipient: Contact) {
        coordinator.perform(action: .directMessage(sender: sender, recipient: recipient))
    }
    
    func openDirectMessageSB(with user: ChatUser) {
        coordinator.perform(action: .directMessageSB(user: user))
    }

    func setMessageButtonVisibility(_ visibility: Bool) {
        viewController?.setMessageButtonVisibility(visibility)
    }
}
