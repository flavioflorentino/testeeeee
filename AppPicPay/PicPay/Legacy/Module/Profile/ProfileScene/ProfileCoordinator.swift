import FeatureFlag
import UIKit
import DirectMessage
import DirectMessageSB

enum ProfileAction {
    case paymentRequest(userProfile: PPContact)
    case openStudentWebview(url: URL)
    case directMessage(sender: Contact, recipient: Contact)
    case directMessageSB(user: ChatUser)
}

protocol ProfileCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ProfileAction)
}

final class ProfileCoordinator: ProfileCoordinating {
    typealias Dependencies = HasFeatureManager
    
    let dependencies: Dependencies
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }

    func perform(action: ProfileAction) {
        switch action {
        case .openStudentWebview(url: let url):
            let webview = WebViewFactory.make(with: url)
            viewController?.present(UINavigationController(rootViewController: webview), animated: true)
        case let .paymentRequest(userProfile):
            goToPaymentRequestWithUserProfile(userProfile)
        case let .directMessage(sender, recipient):
            openDirectMessage(sender: sender, recipient: recipient)
        case let .directMessageSB(userId):
            openDirectMessageSB(with: userId)
        }
    }
    
    private func goToPaymentRequestWithUserProfile(_ userProfile: PPContact) {
        if dependencies.featureManager.isActive(.paymentRequestV2) {
            guard let username = userProfile.username else {
                return
            }
            
            let imgUrl = URL(string: userProfile.imgUrl ?? String())
            
            let origin = PaymentRequestUserEvent.PaymentRequestDetailOrigin.userProfile.rawValue
            let selectedUser = (id: String(userProfile.wsId), name: username, url: imgUrl)
            let controller = PaymentRequestDetailFactory.make(origin: origin, users: [selectedUser])
            
            let navigationController = UINavigationController(rootViewController: controller)
            viewController?.present(navigationController, animated: true)
        } else {
            let controller = PaymentRequestCheckoutFactory.make(contact: userProfile, backButtonClose: true)
            let navigationController = UINavigationController(rootViewController: controller)
            viewController?.present(navigationController, animated: true)
            return
        }
    }

    private func openDirectMessage(sender: Contact, recipient: Contact) {
        let controller = DMChatFactory.make(sender: sender, recipient: recipient)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func openDirectMessageSB(with user: ChatUser) {
        let controller = ChatSetupFactory.make(for: .distinctUser(user))
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
