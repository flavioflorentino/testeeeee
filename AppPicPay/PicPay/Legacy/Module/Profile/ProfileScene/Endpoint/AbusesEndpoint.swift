import Core
import Foundation

enum AbusesType: String {
    case consumer
}

enum AbusesAction: String {
    case block
    case unblock
}

enum AbusesEndpoint {
    case abuses(reportedId: String, type: AbusesType, action: AbusesAction)
}

extension AbusesEndpoint: ApiEndpointExposable {
    var path: String {
        return "api/abuses"
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var body: Data? {
        switch self {
        case let .abuses(reportedId, type, action):
            return [
                "reported_id": reportedId,
                "type": type.rawValue.uppercased(),
                "action": action.rawValue.uppercased()
            ].toData()
        }
    }
}
