import Core
import DirectMessageSB
import FeatureFlag
import Foundation

protocol ProfileServicing {
    var hasStudentComponentFeature: Bool { get }
    var studentComponent: ProfileStudentAccountComponent? { get }
    var studentOfferWebview: String { get }
    func loadStudentStatus(consumerId: String, _ completion: @escaping (PicPayResult<StatusPayload>) -> Void)
    func abusesUser(consumerId: String, action: AbusesAction, _ completion: @escaping (Result<Void, Error>) -> Void)
    func canReach(user: ChatUser, completion: @escaping (Result<Bool, MessagingClientError>) -> Void)
}

final class ProfileService: ProfileServicing {
    typealias Dependencies = HasStudentWorker & HasFeatureManager & HasMainQueue & HasChatApiManager
    private let dependencies: Dependencies
    
    var hasStudentComponentFeature: Bool {
        return dependencies.featureManager.isActive(.featureStudentAccountProfileComponentEnabled)
    }
    
    var studentComponent: ProfileStudentAccountComponent? {
        return dependencies.featureManager.object(.featureStudentAccountProfileComponent, type: ProfileStudentAccountComponent.self)
    }
    
    var studentOfferWebview: String {
        return studentComponent?.buttonLink ?? dependencies.featureManager.text(.universityAccountRegisterWebview)
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func loadStudentStatus(consumerId: String, _ completion: @escaping (PicPayResult<StatusPayload>) -> Void) {
        dependencies.studentWorker.loadStatus(consumerId: consumerId) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result)
            }
        }
    }
    
    func abusesUser(consumerId: String, action: AbusesAction, _ completion: @escaping (Result<Void, Error>) -> Void) {
        let request = Api<NoContent>(endpoint: AbusesEndpoint.abuses(reportedId: consumerId, type: .consumer, action: action))
        
        request.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success:
                    completion(.success(()))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }

    func canReach(user: ChatUser, completion: @escaping (Result<Bool, MessagingClientError>) -> Void) {
        dependencies.chatApiManager.canReach(user: user) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result)
            }
        }
    }
}
