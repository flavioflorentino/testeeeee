import Foundation

struct ProfileStudentAccountComponent: Decodable {
    let description: String?
    let descriptionColor: String?
    let descriptionDarkColor: String?
    let backgroundColor: String
    let backgroundDarkColor: String
    let buttonLink: String
    let buttonText: String
    let buttonColor: String
    let buttonDarkColor: String
}
