import UI

protocol ProfileDisplay: AnyObject {
    func displayStudentAccountView(with component: ProfileStudentAccountComponent)
    func displayAlert(title: String, message: String, buttonTitle: String)
    func displayPaymentRequestLimitExceeded(with alertData: StatusAlertAttributedData)
    func displayPaymentRequestSuccess()
    func displayPaymentRequestError(with alertData: StatusAlertData)
    func setMessageButtonVisibility(_ visibility: Bool)
}
