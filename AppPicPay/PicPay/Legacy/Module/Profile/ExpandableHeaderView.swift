import UI
import UIKit

final class ExpandableHeaderView:UIView {
    private var heightConstraint:NSLayoutConstraint? = nil
    private var titleCenterYConstraint:NSLayoutConstraint? = nil
    private var buttonTopAlignment:NSLayoutConstraint? = nil
    
    private(set) var currentHeight:CGFloat = 0 {
        didSet {
            if let _ = self.heightConstraint {
                self.setNeedsUpdateConstraints()
            }
        }
    }
    
    private(set) var currentTitleCenterY:CGFloat = 0 {
        didSet {
            if let _ = self.titleCenterYConstraint {
                self.setNeedsUpdateConstraints()
            }
        }
    }
    
    lazy private var backgroundImageView: UIImageView = {
        let _view = UIImageView()
        _view.contentMode = .scaleAspectFit
        _view.clipsToBounds = true
        _view.translatesAutoresizingMaskIntoConstraints = false
        
        return _view
    }()
    
    lazy private var overlay: UIView = {
        let _view = UIView()
        _view.translatesAutoresizingMaskIntoConstraints = false
        
        return _view
    }()
    
    lazy private var leftButton: UIButton = {
        let _view = UIButton()
        _view.translatesAutoresizingMaskIntoConstraints = false
        _view.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        
        return _view
    }()
    
    lazy private var rightButton: UIButton = {
        let _view = UIButton()
        _view.translatesAutoresizingMaskIntoConstraints = false
        _view.setImage(#imageLiteral(resourceName: "moreIcon"), for: .normal)
        
        return _view
    }()
        
    lazy private var titleLabel: UILabel = {
        let _view = UILabel()
        _view.translatesAutoresizingMaskIntoConstraints = false
        _view.textAlignment = .center
        _view.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold)
        _view.textColor = Colors.white.lightColor
        _view.text = " "
        
        return _view
    }()
    
    //Public Properties
    public var initialHeight:CGFloat = 110 {
        didSet {
            if let _ = self.heightConstraint {
                self.heightConstraint?.isActive = false
                self.heightConstraint = nil
            }
            self.currentHeight = self.initialHeight
            self.heightConstraint = self.heightAnchor.constraint(equalToConstant: self.initialHeight)
            self.heightConstraint?.isActive = true
        }
    }
    
    public var topInset:CGFloat = 0 {
        didSet {
            self.setNeedsUpdateConstraints()
        }
    }
    
    public var isSingleNavigation:Bool = false {
        didSet {
            if self.isSingleNavigation == oldValue {
                return
            }
            if self.isSingleNavigation {
                self.leftButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
            } else {
                self.leftButton.setImage(#imageLiteral(resourceName: "iconClose"), for: .normal)
            }
        }
    }
    
    public var isRightButtonHidden:Bool = false {
        didSet {
            self.rightButton.isHidden = self.isRightButtonHidden
        }
    }
    
    public var isLeftButtonHidden:Bool = false {
        didSet {
            self.leftButton.isHidden = self.isLeftButtonHidden
        }
    }
    
    public var isTitleScrollable:Bool = true
    
    public var minHeight:CGFloat = 40 {
        didSet {
            if self.minHeight > self.maxHeight {
                self.maxHeight = self.minHeight
            }
        }
    }
    
    public var maxHeight:CGFloat = CGFloat.greatestFiniteMagnitude {
        didSet {
            if self.maxHeight < self.minHeight {
                self.minHeight = self.maxHeight
            }
        }
    }
    
    public var backgroundImage:UIImage? = nil {
        didSet {
            if let newImage = self.backgroundImageView.image {
                CATransaction.begin()
                CATransaction.setAnimationDuration(0.125)
                
                let fade = CATransition()
                fade.type = .fade
                
                self.backgroundImageView.layer.add(fade, forKey: kCATransition)
                self.backgroundImageView.image = newImage
                
                CATransaction.commit()
            } else {
                self.backgroundImageView.image = self.backgroundImage
            }
        }
    }
    
    public var profile: PPContact? = nil {
        didSet {
            let url = URL(string: self.profile?.imgUrlLarge ?? "")
            self.backgroundImageView.setImage(url: url) { [weak self] (image) in
                if let newImage = image {
                    CATransaction.begin()
                    CATransaction.setAnimationDuration(0.125)

                    let fade = CATransition()
                    fade.type = .fade

                    self?.backgroundImageView.layer.add(fade, forKey: kCATransition)
                    self?.backgroundImageView.image = newImage

                    CATransaction.commit()
                }
            }
        }
    }
    
    public var overlayColor:UIColor? {
        get {
            return self.overlay.backgroundColor
        }
        
        set {
            self.overlay.backgroundColor = newValue
        }
    }
    
    public var title:String? {
        get {
            return self.titleLabel.text
        }
        set {
            self.titleLabel.text = newValue
        }
    }
    
    public var titleOffset:CGFloat = 0 {
        didSet {
            if let _ = self.titleCenterYConstraint {
                self.setNeedsUpdateConstraints()
            }
        }
    }
    
    var leftButtonAction:((Any?) -> Void)? = nil
    var rightButtonAction:((Any?) -> Void)? = nil
    
    //UIView
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }
    
    override func updateConstraints() {
        let verticalMargin:CGFloat = 10
        let buttonTopAlignmentConstant:CGFloat = self.topInset + verticalMargin - self.leftButton.contentEdgeInsets.top
        
        if self.currentHeight != self.heightConstraint?.constant {
            self.heightConstraint?.constant = self.currentHeight
        }
        
        if self.currentTitleCenterY != self.titleCenterYConstraint?.constant {
            self.titleCenterYConstraint?.constant = self.currentTitleCenterY
        }
        
        if let constraint = self.buttonTopAlignment, constraint.constant != (buttonTopAlignmentConstant) {
            self.buttonTopAlignment?.constant = buttonTopAlignmentConstant
        }
        
        super.updateConstraints()
    }
    
    //Private Methods
    private func initialize() {
        self.buildSubviews()
        self.isSingleNavigation = true
        self.registerActions()
    }
    
    private func buildSubviews() {
        let buttonSize:CGSize = CGSize(width: 38, height: 38)
        let horizontalMargin:CGFloat = 10
        let buttonInset:UIEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        self.addSubview(self.backgroundImageView)
        self.addSubview(self.overlay)
        self.addSubview(self.leftButton)
        self.addSubview(self.rightButton)
        self.addSubview(self.titleLabel)
        
        //Overlay
        NSLayoutConstraint.activate([
            self.overlay.topAnchor.constraint(equalTo: self.topAnchor),
            self.overlay.leftAnchor.constraint(equalTo: self.leftAnchor),
            self.overlay.rightAnchor.constraint(equalTo: self.rightAnchor),
            self.overlay.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
        
        self.leftButton.contentEdgeInsets = buttonInset
        let topAlignment = self.leftButton.topAnchor.constraint(equalTo: self.topAnchor, constant: buttonInset.top)
        self.buttonTopAlignment = topAlignment
        NSLayoutConstraint.activate([
            self.leftButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: horizontalMargin - buttonInset.left),
            topAlignment,
            self.leftButton.widthAnchor.constraint(equalToConstant: buttonSize.width),
            self.leftButton.heightAnchor.constraint(equalToConstant: buttonSize.height),
        ])
        
        self.rightButton.contentEdgeInsets = buttonInset
        NSLayoutConstraint.activate([
            self.rightButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -(horizontalMargin - buttonInset.left)),
            self.rightButton.topAnchor.constraint(equalTo: self.leftButton.topAnchor),
            self.rightButton.widthAnchor.constraint(equalToConstant: buttonSize.width),
            self.rightButton.heightAnchor.constraint(equalToConstant: buttonSize.height),
        ])
        
        //ImageView
        self.clipsToBounds = true
        NSLayoutConstraint.activate([
            self.backgroundImageView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            self.backgroundImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            self.backgroundImageView.widthAnchor.constraint(equalTo: self.widthAnchor),
        ])
        
        //TitleLabel
        let centerYTitleLabelConstraint = self.titleLabel.centerYAnchor.constraint(equalTo: self.leftButton.centerYAnchor)
        self.titleCenterYConstraint = centerYTitleLabelConstraint
        NSLayoutConstraint.activate([
            self.titleLabel.leftAnchor.constraint(equalTo: self.leftButton.rightAnchor, constant: 8),
            self.titleLabel.rightAnchor.constraint(equalTo: self.rightButton.leftAnchor, constant: -8),
            centerYTitleLabelConstraint
        ])
    }
    
    private func registerActions() {
        self.rightButton.addTarget(self, action: #selector(rightButtonActionHandler), for: .touchUpInside)
        self.leftButton.addTarget(self, action: #selector(leftButtonActionHandler), for: .touchUpInside)
    }
    
    @objc private func rightButtonActionHandler(sender: Any?) {
        self.rightButtonAction?(sender)
    }
    
    @objc private func leftButtonActionHandler(sender: Any?) {
        self.leftButtonAction?(sender)
    }
    
    //Public Methods
    func updateHeight(offset:CGPoint) {
        let minHeightWithOffset:CGFloat = self.minHeight + (self.buttonTopAlignment?.constant ?? 0)
        let offsetY = offset.y + self.topInset
        
        if offsetY <= 0 {
            //Expand
            let expandFactor:CGFloat = 1.3
            self.currentHeight = min((self.initialHeight + self.topInset) - (offsetY * expandFactor), self.maxHeight)
        } else {
            let shrinkFactor:CGFloat = 1.7
            self.currentHeight = max((self.initialHeight + self.topInset) - (offsetY * shrinkFactor), minHeightWithOffset)
        }
        
        if self.isTitleScrollable {
            self.currentTitleCenterY = max(self.titleOffset - offsetY, 0)
        }
        
        if self.currentHeight <= minHeightWithOffset {
            self.layer.zPosition = 100
        } else {
            self.layer.zPosition = 0
        }
        
        self.overlay.alpha = min(max(1 - (self.currentHeight / self.maxHeight), 0), 0.4)
    }
}
