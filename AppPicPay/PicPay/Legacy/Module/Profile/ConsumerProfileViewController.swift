import AnalyticsModule
import AsyncDisplayKit
import FeatureFlag
import Feed
import SnapKit
import UI
import UIKit

extension ConsumerProfileViewController.Layout {
    enum Size {
        static let horizontalMargin: CGFloat = 16
        static let headerMinHeight: CGFloat = 40
        static let headerMaxHeight: CGFloat = UIScreen.main.bounds.height >= 667 ? 350 : 300
        static let usernameLabelProfileImageOffset: CGFloat = 16
        static let feedHeaderHeight: CGFloat = 50
        static let profileImageSize = CGSize(width: 88, height: 88)
        static let paymentButtonSize = CGSize(width: 128, height: 48)
        static let socialBarSize = CGSize(width: 300, height: 60)
    }
    enum Offset {
        static let socialViewTop: CGFloat = 8
        static let socialBarTop: CGFloat = 20
    }
}

final class ConsumerProfileViewController: PPBaseViewController, UIScrollViewDelegate {
    fileprivate enum Layout {}
    
    // MARK: - Visual Components
    private lazy var contentView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    private lazy var headerView: ExpandableHeaderView = {
        let headerView = ExpandableHeaderView()
        headerView.backgroundColor = Palette.ppColorBranding300.color
        headerView.overlayColor = Colors.black.lightColor
        headerView.translatesAutoresizingMaskIntoConstraints = false
        return headerView
    }()
    
    private lazy var profileImageView: UIPPProfileImage = {
        let profileImage = UIPPProfileImage()
        profileImage.imageView?.borderWidth = 2
        profileImage.imageView?.borderColor = Palette.ppColorGrayscale000.color
        profileImage.setContact(PPContact())
        profileImage.translatesAutoresizingMaskIntoConstraints = false
        return profileImage
    }()
    
    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold)
        label.textColor = Palette.ppColorGrayscale600.color
        label.textAlignment = .center
        label.text = " "
        return label
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = UIColor(white: 156.0/255.0, alpha: 1)
        label.textAlignment = .center
        label.text = " "
        return label
    }()

    private lazy var socialButtonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 10
        return stackView
    }()
    
    private lazy var favoriteButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(Assets.Favorite.favoriteButtonFavorite.image, for: .normal)
        button.setImage(Assets.Favorite.favoriteButtonUnFavorite.image, for: .selected)
        button.addTarget(self, action: #selector(favoritePress), for: .touchUpInside)
        return button
    }()

    private lazy var messageButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(Assets.Profile.messages.image, for: .normal)
        button.addTarget(self, action: #selector(messagePress), for: .touchUpInside)
        button.isHidden = !shouldShowMessageButton
        return button
    }()
    
    private lazy var studentAccountBar: ProfileStudentAccountBarView = {
        let studentAccountView = ProfileStudentAccountBarView()
        studentAccountView.translatesAutoresizingMaskIntoConstraints = false
        studentAccountView.delegate = self
        return studentAccountView
    }()
    
    private lazy var socialBar: ProfileSocialBarView = {
        let socialVarView = ProfileSocialBarView()
        socialVarView.translatesAutoresizingMaskIntoConstraints = false
        return socialVarView
    }()
    
    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 10
        return stackView
    }()
    
    private lazy var payButton: UIPPButton = {
        let button = UIPPButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isEnabled = false
        button.setTitle(ProfileLocalizable.pay.text, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        button.setImage(#imageLiteral(resourceName: "coinIcon"), for: .normal)
        button.normalBackgrounColor = Palette.ppColorBranding300.color
        button.normalTitleColor = Palette.white.color
        button.disabledBackgrounColor = Palette.ppColorGrayscale300.color
        button.disabledBorderColor = Palette.ppColorGrayscale300.color
        button.disabledTitleColor = Palette.ppColorGrayscale000.color
        button.leftIconPadding = 20
        button.tintImage = true
        return button
    }()
    
    private lazy var paymentRequestButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(ProfileLocalizable.paymentRequest.text, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        button.setImage(Assets.P2P.paymentRequest.image, for: .normal)
        button.normalBackgrounColor = .clear
        button.normalTitleColor = Palette.ppColorBranding300.color
        button.disabledBackgrounColor = Palette.ppColorGrayscale300.color
        button.disabledBorderColor = Palette.ppColorGrayscale300.color
        button.disabledTitleColor = Palette.ppColorGrayscale000.color
        button.leftIconPadding = 20
        button.tintImage = true
        return button
    }()
    
    private lazy var loadingActivityView: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView.init(style: .gray)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        return activityIndicator
    }()
    
    //  - Feed
    private lazy var feedContent: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var feedHeader: LegacyHomeFeedHeaderView = {
        let feedHeader = LegacyHomeFeedHeaderView()
        feedHeader.toolbarView.isHidden = true
        feedHeader.translatesAutoresizingMaskIntoConstraints = false
        return feedHeader
    }()
    
    //Properties
    private var socialTopConstraintWithoutStudentView: NSLayoutConstraint?
    
    private var lastOffsetPosition: CGPoint? = nil
    
    private var feedAllViewController: ProfileFeedTransactionsViewController? = nil
    
    private lazy var feedFlowCoordinator = MainFeedFlowCoordinator(viewController: self)
    
    private lazy var feedViewController: Feed.FeedViewController = {
        let isCurrentUser = model?.isCurrentUser() ?? false
        let isOpenFollowersAndFollowings = model?.isOpenFollowersAndFollowings() ?? false
        
        let viewController = feedFlowCoordinator.startFeed(
            options: .profile(profileId: isCurrentUser ? nil : model?.profileId,
                              isOpenProfile: isCurrentUser ? true : isOpenFollowersAndFollowings),
            hasRefresh: false
        )
        return viewController
    }()
    
    private var model:NewProfileViewModel? = nil
    
    private var feedContentHeightConstraint:NSLayoutConstraint? = nil
    
    private var profileImageCenterYConstraint:NSLayoutConstraint? = nil
    
    private var isProfileLoaded:Bool = false
    
    private let isNewFeedEnabled = FeatureManager.isActive(.featureNewFeed)
    
    public var isPreviewMode:Bool = false
    
    public var isLoading:Bool = false {
        didSet(oldValue) {
            if oldValue == self.isLoading {
                return
            }
            if isLoading {
                loadingActivityView.startAnimating()
            } else {
                loadingActivityView.stopAnimating()
            }
        }
    }

    private lazy var shouldFilterProfileForDM = FeatureManager.isActive(.directMessageSBUserQueryEnabled)

    private lazy var shouldShowMessageButton = FeatureManager.isActive(.directMessageSBEnabled) && !shouldFilterProfileForDM
    
    private var showPaymentButtons:Bool = true {
        didSet {
            guard
                let payButtonHeightConstraint = payButton.constraints.first(where: { $0.firstAttribute == .height }),
                let paymentRequestButtonHeightConstraint = paymentRequestButton.constraints.first(where: { $0.firstAttribute == .height })
                else {
                    return
            }
            let buttonHeight: CGFloat = showOptionsButton ? 48.0 : 0.0
            payButtonHeightConstraint.constant = buttonHeight
            paymentRequestButtonHeightConstraint.constant = buttonHeight
        }
    }
    
    public var showOptionsButton:Bool = true {
        didSet(oldValue) {
            if oldValue == self.showOptionsButton {
                return
            }
            self.headerView.isRightButtonHidden = !self.showOptionsButton
        }
    }

    private var shouldSetupSocialButtons: Bool {
        guard let consumer = ConsumerManager.shared.consumer, let model = model else {
            return false
        }
        return consumer.wsId.description != model.profileId
    }
    
    //UIViewController
    init(model:NewProfileViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.model = model
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = Colors.backgroundPrimary.color
        buildSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.updateHeight(offset: CGPoint(x: 0, y: -headerView.topInset))
        
        registerActions()
        
        if let model = self.model, let placeholderProfile = model.userProfile {
            if model.isCurrentUser() || isPreviewMode {
                showOptionsButton = false
                headerView.isLeftButtonHidden = isPreviewMode
                payButton.isHidden = true
                paymentRequestButton.isHidden = true
            }
            update(using: placeholderProfile)
        }
        
        socialBar.actionButton.changeButtonWithStatus(FollowerStatus.loading, consumerStatus: FollowerStatus.loading)
        isLoading = true
        if usernameLabel.text != " " {
            loadingActivityView.stopAnimating()
        }
        
        model?.loadUser(completed: { [weak self] (profile) in
            guard let self = self else { return }
            
            self.isLoading = false
            self.isProfileLoaded = true
            self.socialBar.actionButton.isLoading = false
            self.payButton.stopLoadingAnimating()
            self.update(using: profile)
            
            self.model?.checkConsumerStudentStatus()
            self.checkIfProfileCanOpenDirectMessageSB()
            if profile.isBlocked {
                self.isProfileLoaded = false
                
                self.updateSceneForBlockedUser()
                
                if !self.isNewFeedEnabled {
                    self.feedAllViewController?.profile = profile
                    self.feedAllViewController?.tableNode.backgroundColor = Colors.backgroundPrimary.color
                    self.feedAllViewController?.showEmptyView()
                }
            } else if !self.isPreviewMode {
                self.loadFeed()
            }
            
            if self.isNewFeedEnabled && !self.isPreviewMode {
                self.setupFeedControllers()
            }
        })
        
        if !isNewFeedEnabled && !isPreviewMode {
            setupFeedControllers()
        }
        
        setupSocialButtons()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        model?.userDidOpen()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        model?.checkUserIsFavorite { [weak self] in
            self?.favoriteButton.isSelected = self?.model?.isFavorite ?? false
        }
        
        //Workaround - Ajustar o tamanho da view que exibe o feed
        DispatchQueue.main.async { [weak self] in
            self?.view.setNeedsUpdateConstraints()
            self?.contentView.delegate = self
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    //PPBaseViewController
    override func navigationBarIsTransparent() -> Bool {
        return true
    }
    
    //Private Methods
    private func update(using profile:PPContact) {
        if let username = profile.username {
            usernameLabel.text = "@\(username)"
        } else {
            usernameLabel.text = " "
        }
        nameLabel.text = profile.onlineName ?? " "
        if isProfileLoaded {
            headerView.profile = profile
        }
        headerView.title = usernameLabel.text
        profileImageView.setContact(profile)
        
        if let model = model,
            let followerStatus = model.followerStatus,
            let consumerStatus = model.consumerStatus {
            
            if model.isCurrentUser() {
                socialBar.showActionButton(show: false, animated: true)
                payButton.isEnabled = false
            } else {
                socialBar.showActionButton(show: true, animated: true)
                socialBar.actionButton.changeButtonWithStatus(followerStatus, consumerStatus: consumerStatus)
                payButton.isEnabled = true
            }
        } else {
            socialBar.showActionButton(show: false, animated: true)
        }
        socialBar.followers = "\(profile.followers)"
        socialBar.following = "\(profile.following)"
        socialBar.canAccessSocialData = profile.isOpenFollowersAndFollowings
    }
    
    private func buildSubviews() {
        let headerInitialHeight:CGFloat = isPreviewMode ? 100 : 150
        
        view.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: view.topAnchor),
            contentView.leftAnchor.constraint(equalTo: view.leftAnchor),
            contentView.rightAnchor.constraint(equalTo: view.rightAnchor),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
        
        contentView.addSubview(headerView)
        contentView.addSubview(profileImageView)
        contentView.addSubview(usernameLabel)
        contentView.addSubview(nameLabel)
        contentView.addSubview(socialBar)
        contentView.addSubview(buttonsStackView)
        buttonsStackView.addArrangedSubview(paymentRequestButton)
        buttonsStackView.addArrangedSubview(payButton)
        contentView.addSubview(feedContent)
        
        contentView.bringSubviewToFront(headerView)
        contentView.bringSubviewToFront(profileImageView)
        contentView.isUserInteractionEnabled = true
        
        //Header
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: view.topAnchor),
            headerView.leftAnchor.constraint(equalTo: view.leftAnchor),
            headerView.widthAnchor.constraint(equalTo: view.widthAnchor),
        ])
        headerView.initialHeight = headerInitialHeight
        headerView.minHeight = Layout.Size.headerMinHeight
        headerView.maxHeight = Layout.Size.headerMaxHeight
        let halfHeightUsernameLabel:CGFloat = usernameLabel.sizeThatFits(CGSize(width: UIScreen.main.bounds.width, height: 300)).height / 2
        headerView.titleOffset = (Layout.Size.profileImageSize.height / 2) + Layout.Size.usernameLabelProfileImageOffset + 150 - halfHeightUsernameLabel
        if !isPreviewMode {
            if #available(iOS 11.0, *) {
                headerView.topInset = UIApplication.shared.statusBarFrame.height + view.safeAreaInsets.top
            } else {
                headerView.topInset = UIApplication.shared.statusBarFrame.height
            }
        }
        
        //ProfileImage
        let profileImageCenterY:NSLayoutConstraint = profileImageView.centerYAnchor.constraint(equalTo: contentView.topAnchor, constant: headerInitialHeight)
        profileImageCenterYConstraint = profileImageCenterY
        NSLayoutConstraint.activate([
            profileImageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            profileImageCenterY,
            profileImageView.widthAnchor.constraint(equalToConstant: Layout.Size.profileImageSize.width),
            profileImageView.heightAnchor.constraint(equalToConstant: Layout.Size.profileImageSize.height)
        ])
        profileImageView.layer.cornerRadius = Layout.Size.profileImageSize.height / 2
        
        //UsernameLabel
        NSLayoutConstraint.activate([
            usernameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: Layout.Size.horizontalMargin),
            usernameLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -(2 * Layout.Size.horizontalMargin)),
            usernameLabel.topAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: Layout.Size.usernameLabelProfileImageOffset)
        ])
        
        //NameLabel
        NSLayoutConstraint.activate([
            nameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: Layout.Size.horizontalMargin),
            nameLabel.widthAnchor.constraint(equalTo: usernameLabel.widthAnchor),
            nameLabel.topAnchor.constraint(equalTo: usernameLabel.bottomAnchor, constant: 4),
        ])
        
        //Social
        let socialTopConstraint = socialBar.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 20)
        socialTopConstraint.priority = .defaultLow
        
        socialTopConstraintWithoutStudentView = socialTopConstraint
        NSLayoutConstraint.activate([
            socialTopConstraint,
            socialBar.heightAnchor.constraint(equalToConstant: Layout.Size.socialBarSize.height),
            socialBar.widthAnchor.constraint(equalToConstant: Layout.Size.socialBarSize.width),
            socialBar.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
        ])
        
        //ActivityView
        let activityViewContainer = UIView()
        activityViewContainer.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(activityViewContainer)
        NSLayoutConstraint.activate([
            activityViewContainer.topAnchor.constraint(equalTo: profileImageView.bottomAnchor),
            activityViewContainer.bottomAnchor.constraint(equalTo: socialBar.topAnchor),
            activityViewContainer.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            activityViewContainer.widthAnchor.constraint(equalTo: contentView.widthAnchor)
        ])
        
        activityViewContainer.addSubview(loadingActivityView)
        NSLayoutConstraint.activate([
            loadingActivityView.centerXAnchor.constraint(equalTo: activityViewContainer.centerXAnchor),
            loadingActivityView.centerYAnchor.constraint(equalTo: activityViewContainer.centerYAnchor),
        ])
        
        //Payment Buttons
        payButton.cornerRadius = (Layout.Size.paymentButtonSize.height / 2)
        paymentRequestButton.cornerRadius = Layout.Size.paymentButtonSize.height / 2
        NSLayoutConstraint.activate([
            buttonsStackView.topAnchor.constraint(equalTo: socialBar.bottomAnchor, constant: 20),
            buttonsStackView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            payButton.widthAnchor.constraint(equalToConstant: Layout.Size.paymentButtonSize.width),
            payButton.heightAnchor.constraint(equalToConstant: Layout.Size.paymentButtonSize.height),
        ])
        
        paymentRequestButton.layout {
            $0.width == Layout.Size.paymentButtonSize.width
            $0.height == Layout.Size.paymentButtonSize.height
        }
        
        //FeedContainer
        let feedContentHeight:NSLayoutConstraint = feedContent.heightAnchor.constraint(equalToConstant: 0)
        feedContentHeightConstraint = feedContentHeight
        NSLayoutConstraint.activate([
            feedContent.topAnchor.constraint(equalTo: buttonsStackView.bottomAnchor, constant: 20),
            feedContent.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            feedContent.widthAnchor.constraint(equalTo: contentView.widthAnchor),
            feedContentHeight
        ])
        
        //Isso permite que tenha scroll na view
        contentView.contentSize.height = (2 * UIScreen.main.bounds.height)
    }
    
    private func registerActions() {
        profileImageView.isUserInteractionEnabled = true
        profileImageView.addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(toogleProfileOverlay))
        )
        
        payButton.addTarget(self, action: #selector(payButtonDidTap), for: .touchUpInside)
        paymentRequestButton.addTarget(self, action: #selector(paymentRequestDidTap), for: .touchUpInside)
        
        if FeatureManager.isActive(.featureReportProfile) {
            
            headerView.isRightButtonHidden = false
            headerView.rightButtonAction = { [weak self] (_) in
                self?.rightHeaderButtonDidTap()
            }
        } else {
            headerView.isRightButtonHidden = true
        }
        
        headerView.leftButtonAction = { [weak self] (_) in
            self?.leftHeaderButtonDidTap()
        }
        
        socialBar.followersAction = { [weak self] in
            self?.socialBarFollowersDidTap()
        }
        
        socialBar.followingAction = { [weak self] in
            self?.socialBarFollowingDidTap()
        }
        
        socialBar.buttonAction = { [weak self] (button) in
            self?.socialBarActionButtonDidTap(button: button)
        }
        
        socialBar.unblockUserAction = { [weak self] in
            self?.showUnblockUserAlert()
        }
    }

    private func setupConstraints(for socialView: UIView) {
        socialView.snp.makeConstraints {
            $0.top.equalTo(nameLabel.snp.bottom).offset(Layout.Offset.socialViewTop)
            $0.centerX.equalToSuperview()
        }

        socialBar.snp.makeConstraints {
            $0.top.equalTo(socialView.snp.bottom).offset(Layout.Offset.socialBarTop).priority(.high)
        }
    }

    private func setupSocialButtonsStackView() {
        contentView.addSubview(socialButtonsStackView)
        socialButtonsStackView.addArrangedSubview(favoriteButton)
        socialButtonsStackView.addArrangedSubview(messageButton)
    }
    
    private func setupSocialButtons() {
        guard shouldSetupSocialButtons else { return }
        setupSocialButtonsStackView()
        setupConstraints(for: socialButtonsStackView)
    }
    
    private func setupFeedControllers() {
        guard isNewFeedEnabled else {
            configureLegacyFeedViewController()
            return
        }
        
        configureNewFeedViewController()
    }
    
    private func configureNewFeedViewController() {
        feedContent.addSubview(feedViewController.view)
        addChild(feedViewController)
        feedViewController.didMove(toParent: self)
        
        feedViewController.view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        (feedViewController as FeedComponentAdaptering).scrollView?.isScrollEnabled = false
    }
    
    private func configureLegacyFeedViewController() {
        let tableNodeAll = ASTableNode(style: .plain)
        tableNodeAll.view.isScrollEnabled = false
        tableNodeAll.view.bounces = true
        let feedViewController = ProfileFeedTransactionsViewController(tableView: tableNodeAll, mode: .profile)
        feedViewController.isScrollViewEnable = false
        feedViewController.delegate = self
        feedViewController.view.backgroundColor = Colors.backgroundPrimary.color

        if isProfileLoaded {
            loadFeed()
        }

        feedAllViewController = feedViewController

        addChild(feedViewController)

        feedContent.addSubview(feedHeader)
        feedContent.addSubview(feedViewController.view)
        NSLayoutConstraint.activate([
            feedHeader.topAnchor.constraint(equalTo: feedContent.topAnchor),
            feedHeader.leftAnchor.constraint(equalTo: feedContent.leftAnchor),
            feedHeader.rightAnchor.constraint(equalTo: feedContent.rightAnchor),
            feedHeader.heightAnchor.constraint(equalToConstant: Layout.Size.feedHeaderHeight)
        ])
        feedViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            feedViewController.view.topAnchor.constraint(equalTo: feedHeader.bottomAnchor),
            feedViewController.view.leftAnchor.constraint(equalTo: feedContent.leftAnchor),
            feedViewController.view.rightAnchor.constraint(equalTo: feedContent.rightAnchor),
            feedViewController.view.bottomAnchor.constraint(equalTo: feedContent.bottomAnchor),
        ])

        feedViewController.didMove(toParent: self)
    }
    
    
    private func loadFeed() {
        guard let profile = model?.userProfile, !isNewFeedEnabled else {
            return
        }
        
        feedAllViewController?.profile = profile
        feedAllViewController?.configureModel()
        feedAllViewController?.configureTableView()
        feedAllViewController?.changeFilter(Int(FeedItemVisibility.Friends.rawValue) ?? 3)
        feedAllViewController?.tableNode.backgroundColor = Colors.backgroundPrimary.color
    }

    private func checkIfProfileCanOpenDirectMessageSB() {
        if shouldSetupSocialButtons, shouldFilterProfileForDM {
            model?.checkIfProfileCanOpenDirectMessageSB()
        }
    }
    
    @objc private func toogleProfileOverlay() {
        let overlayViewTag = 100
        let imageViewTag = 101
        let presentAnimationDuration:TimeInterval = 0.35
        let dismissAnimationDuration:TimeInterval = 0.25
        let imageViewFrame:CGRect = profileImageView.superview?.convert(profileImageView.frame, to: nil) ?? .zero
        
        if let overlay = view.viewWithTag(overlayViewTag) {
            //Remove o overlay e reduz o tamanho da imagem
            overlay.isUserInteractionEnabled = false
            UIView.animate(withDuration: dismissAnimationDuration, animations: {
                overlay.backgroundColor = .clear
                if let imageView = overlay.viewWithTag(imageViewTag) as? UIImageView {
                    imageView.frame = imageViewFrame
                    imageView.layer.cornerRadius = self.profileImageView.layer.cornerRadius
                }
            }, completion: { (_) in
                overlay.removeFromSuperview()
            })
        } else if let image = profileImageView.imageView?.image {
            //Se houver imagem de profile,
            //Coloca uma overlay e aumenta a imagem do profile para ocupar todo width
            let overlay = UIView(frame: view.frame)
            let imageView = UIImageView(image: image)
            imageView.tag = imageViewTag
            overlay.tag = overlayViewTag
            
            imageView.frame = imageViewFrame
            imageView.layer.masksToBounds = true
            imageView.layer.cornerRadius = profileImageView.layer.cornerRadius
            imageView.contentMode = .scaleAspectFit
            
            overlay.addSubview(imageView)
            overlay.backgroundColor = .clear
            overlay.isUserInteractionEnabled = true
            
            view.addSubview(overlay)
            //Executa a animação para aumentar e centralizar a imagem do profile
            UIView.animate(withDuration: presentAnimationDuration, animations: {
                let ratio = image.size.width / image.size.height
                imageView.frame = CGRect(x: 0, y: 0, width: overlay.frame.width, height: overlay.frame.width * ratio)
                imageView.center = overlay.center
                imageView.layer.cornerRadius = 0
                overlay.backgroundColor = Palette.ppColorGrayscale600.color.withAlphaComponent(0.5)
            }, completion: { (_) in
                overlay.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.toogleProfileOverlay)))
            })
        }
    }
    
    @objc
    private func payButtonDidTap() {
        guard let newTransactionViewController = ViewsManager.peerToPeerStoryboardFirtViewController() as? NewTransactionViewController,
            let userProfile = model?.userProfile else {
                return
        }
        
        newTransactionViewController.preselectedContact = userProfile
        newTransactionViewController.touchOrigin = "Profile"
        
        present(PPNavigationController(rootViewController: newTransactionViewController), animated: true)
    }
    
    @objc
    private func paymentRequestDidTap() {
        beginState()
        model?.tryPaymentRequest()
    }
    
    func socialBarFollowersDidTap() {
        guard let profile = model?.userProfile else {
            return
        }
        if !profile.isOpenFollowersAndFollowings && !(model?.isCurrentUser() ?? false) {
            //TODO: Show alert
            return
        }
        
        let nextScreenModel = FollowersContactListViewModel.init(profileId: String(profile.wsId))
        let nextScreen = FollowersTableViewController.init(model: nextScreenModel)
        
        navigationController?.pushViewController(nextScreen, animated: true)
    }
    
    func socialBarFollowingDidTap() {
        guard let profile = model?.userProfile else {
            return
        }
        if !profile.isOpenFollowersAndFollowings && !(model?.isCurrentUser() ?? false) {
            //TODO: Show alert
            return
        }
        
        let nextScreenModel = FollowingContactListViewModel.init(profileId: String(profile.wsId))
        guard let navigation = ViewsManager.socialStoryboardViewController(withIdentifier: "ContactNavigationViewController") as? UINavigationController,
            navigation.viewControllers.count > 0,
            let controller = navigation.viewControllers[0] as? ContactListTableViewController else {
                return
        }
        
        controller.model = nextScreenModel
        
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc
    private func favoritePress() {
        guard let model = model else { return }
        
        model.setFavorite(id: model.profileId)
        favoriteButton.isSelected = model.isFavorite
    }

    @objc
    private func messagePress() {
        model?.openDirectMessageSB()
    }
    
    private func socialBarActionButtonDidTap(button:UIPPFollowButton) {
        model?.follow(sender: button, completion: { error in
            guard let error = error else {
                return
            }
            AlertMessage.showAlertWithError(error, controller: self)
        })
    }
    
    private func rightHeaderButtonDidTap() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let blockAction = UIAlertAction(title: ProfileLocalizable.block.text, style: .destructive) { [weak self] _ in
            self?.showBlockUserAlert()
        }
        
        let abuseAction = UIAlertAction(title: ProfileLocalizable.report.text, style: .default) { [weak self] _ in
            guard
                let self = self,
                let profileId = self.model?.profileId
            else {
                return
            }
            
            ReportManager.startReportFlow(from: self, type: .consumer, referedId: profileId) { error, _ in
                self.navigationController?.popToViewController(self, animated: true)
            }
        }
        
        let cancelAction = UIAlertAction(title: ProfileLocalizable.cancel.text, style: .cancel)
        
        alertController.addAction(blockAction)
        alertController.addAction(abuseAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true)
    }
    
    private func leftHeaderButtonDidTap() {
        if let navigation = navigationController  {
            navigation.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    //UIScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView == contentView else {
            return
        }
        
        var lastOffsetPosition = CGPoint.zero
        if let offset = self.lastOffsetPosition {
            lastOffsetPosition = offset
        } else {
            lastOffsetPosition.y = -headerView.topInset
            self.lastOffsetPosition = lastOffsetPosition
        }
        
        let topInset: CGFloat
        if #available(iOS 11.0, *) {
            topInset = headerView.topInset + view.safeAreaInsets.top
        } else {
            topInset = headerView.topInset
        }
        let isScrollingUp = scrollView.contentOffset.y < lastOffsetPosition.y
        let feedContentSize = isNewFeedEnabled ?
            (feedViewController as FeedComponentAdaptering).scrollView?.contentSize.height ?? .zero :
            feedAllViewController?.tableNode.view.contentSize.height ?? 0
        
        if  (feedContent.frame.height - (headerView.currentHeight + topInset)) > feedContentSize && !isScrollingUp {
            scrollView.contentOffset.y = lastOffsetPosition.y
            return
        }
        
        //Altera o tamanho da headerView
        headerView.updateHeight(offset: scrollView.contentOffset)
        
        //Altera a transparencia da imagem do profile
        let offset = headerView.currentHeight - (headerView.initialHeight + headerView.topInset)
        if offset > 0 {
            let maxOffsetUntilAlphaZero: CGFloat = 60
            profileImageView.alpha = max(1 - (offset / maxOffsetUntilAlphaZero), 0)
            if headerView.currentHeight == headerView.maxHeight {
                contentView.contentOffset = lastOffsetPosition
            }
        } else {
            profileImageView.alpha = 1
        }
        
        // -- Controle do scroll do feed --
        let feedContentOffset = isNewFeedEnabled ?
            (feedViewController as FeedComponentAdaptering).scrollView?.contentOffset ?? .zero :
            feedAllViewController?.tableNode.contentOffset ?? .zero
        
        let mustScrollFeedContent = scrollView.contentOffset.y > feedContent.frame.origin.y - headerView.currentHeight
        
        if mustScrollFeedContent || (isScrollingUp && feedContentOffset.y > 0) {
            let translatedOffsetForFeedContent = scrollView.contentOffset.y - (feedContent.frame.origin.y - headerView.currentHeight)
            let feedNewOffset = max(feedContentOffset.y + translatedOffsetForFeedContent, 0)
            let feedContentSize = isNewFeedEnabled ?
                (feedViewController as FeedComponentAdaptering).scrollView?.contentSize.height ?? .zero :
                feedAllViewController?.tableNode.view.contentSize.height ?? 0
            //Rola o scroll do feed apenas se houver conteudo
            let contentOffset = min(feedNewOffset, (feedContentSize - feedContent.frame.size.height + headerView.currentHeight))
            
            if isNewFeedEnabled {
                (feedViewController as FeedComponentAdaptering).scrollView?.contentOffset.y = contentOffset
            } else {
                feedAllViewController?.tableNode.contentOffset.y = contentOffset
            }
            
            //trava para não rolar a scrollView principal
            scrollView.contentOffset.y = feedContent.frame.origin.y - headerView.currentHeight
        } else {
            //Altera o tamanho da view que exibe o feed
            let heightToFillScreen = UIScreen.main.bounds.height - feedContent.frame.origin.y
            
            let newHeight = scrollView.contentOffset.y - headerView.currentHeight + heightToFillScreen
            if newHeight != feedContentHeightConstraint?.constant && feedContent.frame.origin.y != 0 {
                feedContentHeightConstraint?.constant = scrollView.contentOffset.y + heightToFillScreen
            }
        }
        
        //Armazena qual foi o ultimo offset para ajudar a determinar o sentido  e travar a rolagem
        //da scrollView principal
        self.lastOffsetPosition = scrollView.contentOffset
    }
    
    override func updateViewConstraints() {
        if feedContent.frame.origin.y != 0 && feedContentHeightConstraint?.constant == 0 && !isPreviewMode {
            let heightToFillScreen:CGFloat
            if #available(iOS 11.0, *) {
                heightToFillScreen = UIScreen.main.bounds.height - feedContent.frame.origin.y - view.safeAreaInsets.top
            } else {
                heightToFillScreen = UIScreen.main.bounds.height - feedContent.frame.origin.y
            }
            
            feedContentHeightConstraint?.constant = heightToFillScreen
        }
        super.updateViewConstraints()
    }
    
    func removePreviewMode() {
        isPreviewMode = false
        if let model = model, !model.isCurrentUser() {
            showOptionsButton = true
            payButton.isHidden = false
            paymentRequestButton.isHidden = false
        } else {
            showPaymentButtons = false
        }
        
        headerView.isLeftButtonHidden = false
        headerView.initialHeight = 150
        profileImageCenterYConstraint?.constant = headerView.initialHeight
        if #available(iOS 11.0, *) {
            headerView.topInset = UIApplication.shared.statusBarFrame.height + view.safeAreaInsets.top
        } else {
            headerView.topInset = UIApplication.shared.statusBarFrame.height
        }
        headerView.updateHeight(offset: CGPoint(x: 0, y: -headerView.topInset))
        view.setNeedsUpdateConstraints()
        view.setNeedsLayout()
        
        setupFeedControllers()
        loadFeed()
    }
}

// MARK: View Model Outputs
extension ConsumerProfileViewController: ProfileDisplay {
    func displayStudentAccountView(with component: ProfileStudentAccountComponent) {
        profileImageView.setStudentAccountBadge()
        
        contentView.addSubview(studentAccountBar)
        
        studentAccountBar.layout {
            $0.top == nameLabel.bottomAnchor + Spacing.base02
            $0.width == Layout.Size.socialBarSize.width
            $0.centerX == contentView.centerXAnchor
        }
        
        socialTopConstraintWithoutStudentView?.isActive = false
        
        socialBar.layout {
            $0.top == studentAccountBar.bottomAnchor + Spacing.base02
        }
        
        studentAccountBar.update(with: component)
    }
    func displayAlert(title: String, message: String, buttonTitle: String) {
        endState()
        let button = Button(title: buttonTitle, type: .cta)
        let alert = Alert(
            with: title,
            text: message,
            buttons: [button]
        )
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayPaymentRequestLimitExceeded(with alertData: StatusAlertAttributedData) {
        endState()
        let alert = StatusAlertView.show(on: view, data: alertData)
        alert.statusAlertDelegate = self
    }
    
    func displayPaymentRequestSuccess() {
        endState()
    }
    
    func displayPaymentRequestError(with alertData: StatusAlertData) {
        endState()
        StatusAlertView.show(on: view, data: alertData)
    }

    func setMessageButtonVisibility(_ visibility: Bool) {
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
            self.messageButton.isHidden = !visibility
        }
    }
}

extension ConsumerProfileViewController: ProfileStudentAccountBarViewActionDelegate {
    func profileStudentAccountBarDidTapOffer(bar: ProfileStudentAccountBarView) {
        model?.openStudentAccountOffer()
    }
}

extension ConsumerProfileViewController: FeedViewControllerDelegate {
    func feedScrollViewDidScroll(_ scrollView: UIScrollView) {}
    func feedWillReload() { }
    func feedDidReload() { }
    func feedDidLoadNewPage() { }
}

extension ConsumerProfileViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return (otherGestureRecognizer is UIScreenEdgePanGestureRecognizer)
    }
}

// MARK: - StatusAlertViewDelegate
extension ConsumerProfileViewController: StatusAlertViewDelegate {
    func didTouchOnButton() {
        let controller = PaymentRequestSelectorFactory().make(origin: .profile)
        present(controller, animated: true)
    }
}

// MARK: - StatefulProviding
extension ConsumerProfileViewController: StatefulProviding { }

// MARK: - Blocked And Unblock User Private Methods
extension ConsumerProfileViewController {
    private func unblockedUserAction() {
        model?.abusesUser(.unblock) { [weak self] result in
            guard let self = self else { return}
            switch result {
            case .success:
                self.updateSceneForUnblockedUser()
                self.checkIfProfileCanOpenDirectMessageSB()
            case .failure:
                self.showUnblockErrorAlert()
            }
        }
    }
    
    private func updateSceneForUnblockedUser() {
        showOptionsButton = true
        payButton.isHidden = false
        favoriteButton.isHidden = false
        messageButton.isHidden = !shouldShowMessageButton
        paymentRequestButton.isHidden = false
        socialBar.canAccessSocialData = true
        socialBar.actionButton.isHidden = false
        socialBar.unblockUserButton.isHidden = true
        
        if !isNewFeedEnabled {
            feedAllViewController?.showServerLoading()
        }
        
        if !isPreviewMode {
            loadFeed()
        }
    }
    
    private func showUnblockErrorAlert() {
        let retryButton = Button(title: ProfileLocalizable.tryAgain.text, type: .cta) { [weak self] popupViewController, _ in
            self?.unblockedUserAction()
            popupViewController.dismiss(animated: true)
        }

        let cancelButton = Button(title: ProfileLocalizable.cancel.text, type: .inline)

        let alert = Alert(
            with: ProfileLocalizable.somethingWentWrong.text,
            text: ProfileLocalizable.unableToUnblockThisUser.text,
            buttons: [retryButton, cancelButton],
            image: Alert.Image(with: Assets.CreditPicPay.Registration.sad3.image)
        )

        AlertMessage.showAlert(alert, controller: self)
    }
    
    private func showUnblockUserAlert() {
        let alertController = UIAlertController(
            title: ProfileLocalizable.unblockUser.text,
            message: ProfileLocalizable.nowThatUserCanFollowYou.text,
            preferredStyle: .alert
        )

        let unblockAction = UIAlertAction(title: ProfileLocalizable.unblock.text, style: .cancel) { [weak self] _ in
            self?.unblockedUserAction()
        }

        let cancelAction = UIAlertAction(title: ProfileLocalizable.cancel.text, style: .default)

        alertController.addAction(unblockAction)
        alertController.addAction(cancelAction)

        present(alertController, animated: true)
    }
    
    private func blockedUserAction() {
        model?.abusesUser(.block) { [weak self] result in
            switch result {
            case .success:
                self?.updateSceneForBlockedUser()
                self?.showUserBlockedAlert()
            case .failure:
                self?.showBlockErrorAlert()
            }
        }
    }
    
    private func showBlockErrorAlert() {
        let retryButton = Button(title: ProfileLocalizable.tryAgain.text, type: .cta) { [weak self] popupViewController, _ in
            self?.blockedUserAction()
            popupViewController.dismiss(animated: true)
        }

        let cancelButton = Button(title: ProfileLocalizable.cancel.text, type: .inline)

        let alert = Alert(
            with: ProfileLocalizable.somethingWentWrong.text,
            text: ProfileLocalizable.unableToBlockThisUser.text,
            buttons: [retryButton, cancelButton],
            image: Alert.Image(with: Assets.CreditPicPay.Registration.sad3.image)
        )

        AlertMessage.showAlert(alert, controller: self)
    }
    
    private func showBlockUserAlert() {
        let alertController = UIAlertController(
            title: ProfileLocalizable.blockUser.text,
            message: ProfileLocalizable.thisUserWillNoLongerBeAbleToFollowYou.text,
            preferredStyle: .alert
        )

        let blockAction = UIAlertAction(title: ProfileLocalizable.block.text, style: .cancel) { [weak self] _ in
            self?.blockedUserAction()
        }

        let cancelAction = UIAlertAction(title: ProfileLocalizable.cancel.text, style: .default)

        alertController.addAction(blockAction)
        alertController.addAction(cancelAction)

        present(alertController, animated: true)
    }
    
    
    private func showUserBlockedAlert() {
            let alertController = UIAlertController(
                title: ProfileLocalizable.blockedUser.text,
                message: ProfileLocalizable.youCanUnblock.text,
                preferredStyle: .alert
            )

            let okAction = UIAlertAction(title: ProfileLocalizable.OkIgotIt.text, style: .cancel)

            alertController.addAction(okAction)

            present(alertController, animated: true)
        }
    private func updateSceneForBlockedUser() {
        payButton.isHidden = true
        paymentRequestButton.isHidden = true
        favoriteButton.isHidden = true
        messageButton.isHidden = true
        showOptionsButton = false
        socialBar.canAccessSocialData = false
        socialBar.actionButton.isHidden = true
        socialBar.unblockUserButton.isHidden = false
        
        if !isNewFeedEnabled {
            feedAllViewController?.showEmptyView()
        }
    }
}
