//
//  ProfileFeedTransactionsViewModel.swift
//  PicPay
//
//  Created by Vagner Orlandi on 18/07/2018.
//

import UIKit

final class ProfileFeedTransactionsViewModel: FeedViewModel {
    
    var profile:PPContact
    
    init(profile: PPContact) {
        self.profile = profile
        super.init()
    }
    
    override func loadMoreFeed(_ reload: Bool, onCacheLoaded: ((_ newItems:[FeedItem], _ hasNext:Bool) -> Void)?, callback: @escaping ([FeedItem], Bool, Error?) -> Void, blockRequest: @escaping () -> Void) {
        let requestId = Int(currentRequestId + 1)
        currentRequestId = currentRequestId + 1
        
        // get last fetched feed
        var lastId = ""
        if let lastFeedItem = self.items.last,
            !reload && self.items.count > 0 {
            
            lastId = lastFeedItem.id
        }
        
        WSNewFeed.profileFeeds(profileId: "\(self.profile.wsId)", last: lastId, visibility: visibility.rawValue, feedItemId: nil) { [weak self] (list, hasNext, error) in
            if self?.currentRequestId == requestId {
                if let items = list {
                    callback(items, hasNext, error)
                }else{
                    callback([], hasNext, error)
                }
            }else{
                blockRequest()
            }
        }
    }
}
