import UI
import UIKit

final class ProfileSocialBarView: UIView {
    //DRY
    final class VerticalSocialInfo: UIView {
        //Subviews
        lazy private var valueLabel: UILabel = {
            let _view = UILabel()
            _view.translatesAutoresizingMaskIntoConstraints = false
            _view.textColor = Palette.ppColorBranding300.color
            _view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
            _view.textAlignment = .center
            _view.text = " "
            
            return _view
        }()
        
        lazy private var titleLabel: UILabel = {
            let _view = UILabel()
            _view.translatesAutoresizingMaskIntoConstraints = false
            _view.textColor = UIColor(red: 156.0/255.0, green: 156.0/255.0, blue: 156.0/255.0, alpha: 1)
            _view.font = UIFont.systemFont(ofSize: 11, weight: UIFont.Weight.medium)
            _view.textAlignment = .center
            _view.text = " "
            
            return _view
        }()
        
        //Properties
        public var title:String? {
            get {
                return self.titleLabel.text
            }
            set {
                UIView.transition(with: self.titleLabel, duration: 0.25, options: .transitionCrossDissolve, animations: {
                    self.titleLabel.text = newValue
                }, completion: nil)
            }
        }
        
        public var value:String? {
            get {
                return self.valueLabel.text
            }
            set {
                UIView.transition(with: self.valueLabel, duration: 0.25, options: .transitionCrossDissolve, animations: {
                    self.valueLabel.text = newValue
                }, completion: nil)
            }
        }
        
        public var isActive:Bool = true {
            didSet(oldValue) {
                if self.isActive == oldValue {
                    return
                }
                let activeColor:UIColor = Palette.ppColorBranding300.color
                let deactiveColor:UIColor = UIColor.init(white: 156.0/255.0, alpha: 1)
                
                UIView.transition(with: self.valueLabel, duration: 0.25, options: .transitionCrossDissolve, animations: {
                    self.valueLabel.textColor = self.isActive ? activeColor : deactiveColor
                }, completion: nil)
            }
        }
        
        //UIView
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.initialize()
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            self.initialize()
        }
        
        //Private Methods
        private func initialize() {
            self.buildSubviews()
        }
        
        private func buildSubviews() {
            self.addSubview(self.valueLabel)
            self.addSubview(self.titleLabel)
            
            NSLayoutConstraint.activate([
                self.valueLabel.topAnchor.constraint(equalTo: self.topAnchor),
                self.valueLabel.leftAnchor.constraint(equalTo: self.leftAnchor),
                self.valueLabel.rightAnchor.constraint(equalTo: self.rightAnchor),
                ])
            
            NSLayoutConstraint.activate([
                self.titleLabel.topAnchor.constraint(equalTo: self.valueLabel.bottomAnchor, constant: 3),
                self.titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor),
                self.titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor),
                self.titleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor),
                ])
        }
    }
    
    //Actions
    var followersAction:(() -> Void)? = nil
    
    var followingAction:(() -> Void)? = nil
    
    var buttonAction:((UIPPFollowButton) -> Void)? = nil
    var unblockUserAction: (() -> Void)?
    
    //Subviews
    lazy private var topBorder: UIView = {
        let _view = UIView()
        _view.translatesAutoresizingMaskIntoConstraints = false
        _view.backgroundColor = UIColor(white: 221.0/255.0, alpha: 1)
        
        return _view
    }()
    
    lazy private var bottomBorder: UIView = {
        let _view = UIView()
        _view.translatesAutoresizingMaskIntoConstraints = false
        _view.backgroundColor = UIColor(white: 221.0/255.0, alpha: 1)
        
        return _view
    }()
    
    lazy private var followingInfo: VerticalSocialInfo = {
        let _view = VerticalSocialInfo()
        _view.translatesAutoresizingMaskIntoConstraints = false
        _view.title = ProfileLocalizable.following.text
        
        return _view
    }()
    
    lazy private var followerInfo: VerticalSocialInfo = {
        let _view = VerticalSocialInfo()
        _view.translatesAutoresizingMaskIntoConstraints = false
        _view.title = ProfileLocalizable.followers.text
        
        return _view
    }()
    
    lazy var actionButton: UIPPFollowButton = {
        let _view = UIPPFollowButton()
        _view.translatesAutoresizingMaskIntoConstraints = false
        _view.setImage(UIImage.init(named: "ico_nav_friends.png"), for: .normal)
        _view.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        _view.leftIconPadding = 17
        _view.tintImage = true
        _view.disabledTitleColor = Palette.ppColorGrayscale000.color
        _view.disabledBackgrounColor = UIColor(white: 170.0/255.0, alpha: 1)
        _view.disabledBorderColor = UIColor(white: 170.0/255.0, alpha: 1)
        _view.setup()
        _view.setupTintImage()


        _view.centerLoading = true

        return _view
    }()
    
    lazy var unblockUserButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(ProfileLocalizable.unblock.text, for: .normal)
        button.buttonStyle(PrimaryButtonStyle(size: .small, icon: (name: .usersAlt, alignment: .left)))
        button.isHidden = true
        return button
    }()
    
    
    //Properties
    private var leftConstraint:NSLayoutConstraint? = nil
    
    private var rightConstraint:NSLayoutConstraint? = nil
    
    private var containerRightConstraint:NSLayoutConstraint? = nil
    
    public var followers:String? {
        get {
            return self.followerInfo.value
        }
        
        set {
            self.followerInfo.value = newValue
        }
    }
    
    public var following:String? {
        get {
            return self.followingInfo.value
        }
        
        set {
            self.followingInfo.value = newValue
        }
    }
    
    public var canAccessSocialData:Bool = true {
        didSet(oldValue) {
            if self.canAccessSocialData == oldValue {
                return
            }
            self.followerInfo.isActive = self.canAccessSocialData
            self.followingInfo.isActive = self.canAccessSocialData
        }
    }
    
    //UIView
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }
    
    //Private Methods
    private func initialize() {
        self.buildSubviews()
        self.registerActions()
    }
    
    private func buildSubviews() {
        let borderWidth: CGFloat = 1
        let actionButtonHeight:CGFloat = 36
        let followInfoContainer = UIStackView()
        followInfoContainer.translatesAutoresizingMaskIntoConstraints = false
        followInfoContainer.axis = .horizontal
        followInfoContainer.alignment = .center
        followInfoContainer.distribution = .fillEqually
        followInfoContainer.spacing = 3
        
        self.addSubview(self.topBorder)
        self.addSubview(followInfoContainer)
        followInfoContainer.addArrangedSubview(self.followerInfo)
        followInfoContainer.addArrangedSubview(self.followingInfo)
        self.addSubview(self.actionButton)
        addSubview(unblockUserButton)
        self.addSubview(self.bottomBorder)
        
        //TopBorder
        let rightConstraint = self.topBorder.rightAnchor.constraint(equalTo: self.rightAnchor)
        let leftConstraint = self.topBorder.leftAnchor.constraint(equalTo: self.leftAnchor)
        self.rightConstraint = rightConstraint
        self.leftConstraint = leftConstraint
        NSLayoutConstraint.activate([
            self.topBorder.topAnchor.constraint(equalTo: self.topAnchor),
            leftConstraint,
            rightConstraint,
            self.topBorder.heightAnchor.constraint(equalToConstant: borderWidth)
            ])
        
        //BottomBorder
        NSLayoutConstraint.activate([
            self.bottomBorder.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            self.bottomBorder.leftAnchor.constraint(equalTo: self.topBorder.leftAnchor),
            self.bottomBorder.rightAnchor.constraint(equalTo: self.topBorder.rightAnchor),
            self.bottomBorder.heightAnchor.constraint(equalToConstant: borderWidth)
            ])
        
        //Container
        let containerRightConstraint = followInfoContainer.rightAnchor.constraint(equalTo: self.topBorder.rightAnchor, constant: -128)
        self.containerRightConstraint = containerRightConstraint
        NSLayoutConstraint.activate([
            followInfoContainer.topAnchor.constraint(equalTo: self.topAnchor),
            followInfoContainer.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            followInfoContainer.leftAnchor.constraint(equalTo: self.topBorder.leftAnchor),
            containerRightConstraint
            ])
        
        //ActionButton
        NSLayoutConstraint.activate([
            actionButton.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            actionButton.trailingAnchor.constraint(equalTo: self.topBorder.trailingAnchor),
            actionButton.heightAnchor.constraint(equalToConstant: actionButtonHeight),
            actionButton.widthAnchor.constraint(equalToConstant: 120)
        ])

        actionButton.cornerRadius = (actionButtonHeight / 2)
        
        NSLayoutConstraint.activate([
            unblockUserButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            unblockUserButton.leadingAnchor.constraint(equalTo: followInfoContainer.trailingAnchor, constant: Spacing.base01),
            unblockUserButton.trailingAnchor.constraint(equalTo: topBorder.trailingAnchor)
        ])
    }
    
    private func registerActions() {
        followerInfo.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(followerInfoDidTap(tapRecognizer:))))
        followingInfo.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(followingInfoDidTap(tapRecognizer:))))
        actionButton.addTarget(self, action: #selector(actionButtonHandler), for: .touchUpInside)
        unblockUserButton.addTarget(self, action: #selector(unblockUserButtonHandler), for: .touchUpInside)
    }
    
    @objc
    private func followerInfoDidTap(tapRecognizer: Any?) {
        if canAccessSocialData {
            followersAction?()
        }
    }
    
    @objc
    private func followingInfoDidTap(tapRecognizer: Any?) {
        if canAccessSocialData {
            followingAction?()
        }
    }
    
    @objc
    private func actionButtonHandler(sender: Any?) {
        buttonAction?(self.actionButton)
    }
    
    @objc
    private func unblockUserButtonHandler() {
        unblockUserAction?()
    }
    
    public func showActionButton(show:Bool = true, animated:Bool = true) {
        let extraSpacing:CGFloat = 128
        
        if let leftConstraint = self.leftConstraint,
            let rightConstraint = self.rightConstraint,
            let containerRightConstraint = self.containerRightConstraint {
            let newWidth:CGFloat = show ? -extraSpacing : 0
            if containerRightConstraint.constant == newWidth {
                //Não houve alteração
                return
            } else {
                containerRightConstraint.constant = newWidth
            }
            
            let alpha:CGFloat
            let halfSpacing:CGFloat = (extraSpacing / 2)
            if show {
                leftConstraint.constant -= halfSpacing
                rightConstraint.constant += halfSpacing
                alpha = 1
            } else {
                leftConstraint.constant += halfSpacing
                rightConstraint.constant -= halfSpacing
                alpha = 0
            }
            
            if animated {
                UIView.animate(withDuration: 0.25) {
                    self.layoutIfNeeded()
                    self.actionButton.alpha = alpha
                }
            } else {
                self.actionButton.alpha = alpha
            }
        }
    }
}
