//
//  ProfileFeedTransactionsViewController.swift
//  PicPay
//
//  Created by Vagner Orlandi on 18/07/2018.
//

import UIKit

final class ProfileFeedTransactionsViewController: FeedViewController {
    
    var profile:PPContact? = nil
    
    lazy private var emptyViewController: EmptyMessageViewController = {
        let _vc = ViewsManager.emptyMessageStoryboardFirtViewController() as! EmptyMessageViewController
        
        return _vc
    }()
    
    override func configureModel() {
        if let consumerProfile = profile {
            self.model = ProfileFeedTransactionsViewModel(profile: consumerProfile)
        }
    }
    
    override func showEmptyView() {
        if let consumerProfile = self.profile {
            DispatchQueue.main.async {
                
                if consumerProfile.isOpenFollowersAndFollowings {
                    self.emptyViewController.updateType(messageType: .noTransactions, username: consumerProfile.username ?? "")
                } else {
                    self.emptyViewController.updateType(messageType: .cantSee)
                }
                
                self.emptyViewController.view.frame = self.view.frame
                self.tableNode.view.tableHeaderView = self.emptyViewController.view
            }
        }
    }
}
