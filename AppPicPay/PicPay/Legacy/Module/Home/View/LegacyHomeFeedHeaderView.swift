//
//  HomeFeedHeaderView.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 28/05/2018.
//

import FeatureFlag
import UI
import UIKit

protocol LegacyHomeFeedHeaderDelegate: AnyObject {
    func feedHeaderDidChangeVisibility(visibility: FeedItemVisibility)
}

final class LegacyHomeFeedHeaderView: NibView {
    typealias Dependencies = HasFeatureManager
    private var dependencies: Dependencies
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var myButton: UIButton!
    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var activityLabel: UILabel! {
        didSet {
            activityLabel.textColor = Palette.ppColorGrayscale600.color
        }
    }
    
    fileprivate var selectionMarkerView: UIView?
    
    weak var delegate: LegacyHomeFeedHeaderDelegate?
    
    override init(frame: CGRect) {
        self.dependencies = DependencyContainer()
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(dependencies: Dependencies) {
        self.init(frame: .zero)
        self.dependencies = dependencies
    }
    
    func setup(){
        allButton.setTitleColor(Palette.ppColorBranding300.color, for: .disabled)
        myButton.setTitleColor(Palette.ppColorBranding300.color, for: .disabled)
        allButton.setTitleColor(Palette.ppColorGrayscale400.color, for: .normal)
        myButton.setTitleColor(Palette.ppColorGrayscale400.color, for: .normal)
        
        allButton.addTarget(self, action: #selector(selectFriend), for: .touchUpInside)
        myButton.addTarget(self, action: #selector(selectPrivate), for: .touchUpInside)
        
        selectionMarkerView = UIView(frame: CGRect(x: 0, y: 40, width: 30, height: 2))
        selectionMarkerView?.backgroundColor = Palette.ppColorBranding300.color
        toolbarView.addSubview(selectionMarkerView!)
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    func selectVisibilty(_ visibility: FeedItemVisibility){
        UIView.performWithoutAnimation {
            allButton.isEnabled = visibility != .Friends
            myButton.isEnabled = visibility != .Private
        }
        
        UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseInOut], animations: {
            if visibility == .Friends {
                self.selectionMarkerView?.frame.size.width = self.allButton.frame.size.width - 10
                self.selectionMarkerView?.center = CGPoint(x: self.allButton.center.x, y: self.allButton.center.y + 12)
            }else{
                self.selectionMarkerView?.frame.size.width = self.myButton.frame.size.width - 10
                self.selectionMarkerView?.center = CGPoint(x: self.myButton.center.x, y: self.myButton.center.y + 12)
            }
        }, completion: nil)
    }
    
    @objc func selectFriend() {
        delegate?.feedHeaderDidChangeVisibility(visibility: .Friends)
    }
    
    @objc func selectPrivate() {
        delegate?.feedHeaderDidChangeVisibility(visibility: .Private)
    }
    
}
