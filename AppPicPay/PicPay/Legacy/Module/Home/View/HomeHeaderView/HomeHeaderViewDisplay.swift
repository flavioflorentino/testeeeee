import UIKit

public protocol HomeHeaderViewDisplay: AnyObject {
    func presentLoadingErrorView(forSegment index: Int)
    func dismissLoadingErrorView(forSegment index: Int)
}
