import Advertising
import Core
import FeatureFlag

protocol HomeHeaderViewInputs: AnyObject {
    var favoritesConnectionResult: Result<Void, Error> { get set }
    var suggestionConnectionResult: Result<Void, Error> { get set }
    
    func checkConnectionResult(forIndex index: Int)
    func refreshPinnedBanners(containerPinnedBanners: ContainerPinnedBannersViewRefreshing)
}

final class HomeHeaderViewModel {
    typealias Dependencies = HasFeatureManager
    public private(set) var presenter: HomeHeaderViewPresenting
    
    private let dependencies: Dependencies
    
    var favoritesConnectionResult: Result<Void, Error> = .success
    var suggestionConnectionResult: Result<Void, Error> = .success
    
    public init(presenter: HomeHeaderViewPresenting, dependencies: Dependencies = DependencyContainer()) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension HomeHeaderViewModel: HomeHeaderViewInputs {
    func checkConnectionResult(forIndex index: Int) {
        let result = index == 0 ? suggestionConnectionResult : favoritesConnectionResult
        switch result {
        case .success:
            presenter.hideHeaderViewLoadingError(forSegment: index)
        case .failure:
            presenter.showHeaderViewLoadingError(forSegment: index)
        }
    }
    
    func refreshPinnedBanners(containerPinnedBanners: ContainerPinnedBannersViewRefreshing) {
        guard dependencies.featureManager.isActive(.opsCardPinnedBannersHomeBool) else {
            return
        }
        
        containerPinnedBanners.refresh()
    }
}
