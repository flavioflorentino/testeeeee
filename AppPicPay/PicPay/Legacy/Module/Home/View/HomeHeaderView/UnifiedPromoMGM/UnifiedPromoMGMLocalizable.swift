enum UnifiedPromoMGMLocalizable: String {
    case viewTitle
    case viewDescription
}

// MARK: - Localizable
extension UnifiedPromoMGMLocalizable: Localizable {
    var key: String {
        rawValue
    }

    var file: LocalizableFile {
        .unifiedPromoMGM
    }
}
