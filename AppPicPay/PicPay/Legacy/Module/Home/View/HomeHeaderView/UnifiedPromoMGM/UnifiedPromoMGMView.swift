import AssetsKit
import Core
import Foundation
import SnapKit
import UI
import UIKit

final class UnifiedPromoMGMView: UIView, ViewConfiguration {
    private lazy var promotionsImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Resources.Icons.icoPromo.image
        return imageView
    }()
    
    private lazy var labelStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .leading
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
        label.text = UnifiedPromoMGMLocalizable.viewTitle.text
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        label.text = UnifiedPromoMGMLocalizable.viewDescription.text
        return label
    }()
    
    private lazy var chevronLabel: UILabel = {
       let chevron = UILabel()
        chevron.labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, Colors.branding600.color)
        chevron.text = Iconography.angleRightB.rawValue
        return chevron
    }()
    
    var onViewTap: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapUnifiedView))
        addGestureRecognizer(tapGesture)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(promotionsImageView,
                    labelStackView,
                    chevronLabel)
        
        labelStackView.addArrangedSubviews(titleLabel,
                                           descriptionLabel)
    }
    
    func setupConstraints() {
        promotionsImageView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        labelStackView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(promotionsImageView.snp.trailing).offset(Spacing.base01)
        }
        
        chevronLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
    }
    
    func configureViews() {
        updateStyle()
        promotionsImageView.tintColor = Colors.branding600.color
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateStyle()
    }
    
    private func updateStyle() {
        self.viewStyle(CardViewStyle())
            .with(\.cornerRadius, .medium)
            .with(\.backgroundColor, .backgroundPrimary())
    }
    
    @objc
    private func didTapUnifiedView() {
        onViewTap?()
    }
}
