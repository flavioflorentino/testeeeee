import Core

protocol HomeHeaderViewPresenting: AnyObject {
    var viewController: HomeHeaderViewDisplay? { get set }
    func showHeaderViewLoadingError(forSegment index: Int)
    func hideHeaderViewLoadingError(forSegment index: Int)
}

final class HomeHeaderViewPresenter: HomeHeaderViewPresenting {
    weak var viewController: HomeHeaderViewDisplay?
    
    func showHeaderViewLoadingError(forSegment index: Int) {
        viewController?.presentLoadingErrorView(forSegment: index)
    }
    
    func hideHeaderViewLoadingError(forSegment index: Int) {
        viewController?.dismissLoadingErrorView(forSegment: index)
    }
}
