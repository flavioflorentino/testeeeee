import AnalyticsModule
import UI
import UIKit
import SnapKit
import FeatureFlag
import Advertising

enum HomeHeaderViewTab {
    case suggestions, favorites
}

protocol HomeHeaderViewDelegate: AnyObject {
    func reloadFavoritesCollectionData()
    func didSelectedTab(_ tab: HomeHeaderViewTab)
    func openUnifiedPromotions()
}

extension HomeHeaderView.Layout {
    enum SegmentedControl {
        static let height: CGFloat = 25
        static let leading: CGFloat = Spacing.base02
    }
    
    enum FavoriteLoadingErrorView {
        static let top: CGFloat = 15
    }
    
    enum NavigationBar {
        static let height: CGFloat = 44
    }
    
    enum SuggestionsLineView {
        static let height: CGFloat = 2
    }
    
    enum TitleLabel {
        static let fontSize: CGFloat = 16
    }
}

final class HomeHeaderView: UIView {
    fileprivate enum Layout { }
    
    typealias Dependencies = HasFeatureManager
    private var dependencies: Dependencies
    
    var navigationBar = LegacyHomeNavigationBar(frame: .zero)
    
    lazy var suggestionsView: UIView = UIView(frame: .zero)
    lazy var suggestionLimiterView = UIView()
    
    lazy var suggestionsLoaderView: UIActivityIndicatorView = {
        var indicatorStyle: UIActivityIndicatorView.Style = .gray
        if #available(iOS 12.0, *), (self.traitCollection.userInterfaceStyle == .dark) {
          indicatorStyle = .white
        }
        
        let suggestionsLoaderView = UIActivityIndicatorView(style: indicatorStyle)
        suggestionsLoaderView.startAnimating()
        return suggestionsLoaderView
    }()
    
    private lazy var favoriteLoadingErrorView: FavoritesOnboardingLoadingErrorView = {
        let view = FavoritesOnboardingLoadingErrorView()
        view.isHidden = true
        view.delegate = self
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let titleLabel = UILabel(frame: .zero)
        titleLabel.font = UIFont.boldSystemFont(ofSize: Layout.TitleLabel.fontSize)
        titleLabel.textColor = Palette.white.color
        titleLabel.text = FavoriteLocalizable.favoriteSegmentedControlNoOpt.text
        return titleLabel
    }()
    
    private lazy var segmentedControl: CustomSegmentedControl = {
        let view = CustomSegmentedControl(
            buttonTitles: [
                FavoriteLocalizable.favoriteSegmentedControlOpt1.text,
                FavoriteLocalizable.favoriteSegmentedControlOpt2.text
        ])
        view.selectorBottomMargin = .zero
        view.textColor = Colors.grayscale700.color
        view.selectorViewColor = Colors.grayscale700.color
        view.selectorTextColor = Colors.grayscale700.color
        view.delegate = self
        view.backgroundColor = .clear
        return view
    }()
    
    let suggestionCollectionView = RecentsCollectionView()
    let favoriteCarouselCollectionView = FavoriteCarouselCollectionView()
    var viewModel: HomeHeaderViewModel?
    var onRefreshPinnedBannerHeight: ((CGFloat) -> Void)?
    weak var homeHeaderDelegate: HomeHeaderViewDelegate?
    
    private let containerPinnedBanners = ContainerPinnedBannersFactory.make(source: .home)
    
    private lazy var unifiedPromoMgmView: UnifiedPromoMGMView = UnifiedPromoMGMView()
    
    override init(frame: CGRect) {
        self.dependencies = DependencyContainer()
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(viewModel: HomeHeaderViewModel, dependencies: Dependencies) {
        self.init(frame: .zero)
        self.viewModel = viewModel
        self.viewModel?.presenter.viewController = self
        self.dependencies = dependencies
        setup()
    }
    
    fileprivate func setup() {
        self.isUserInteractionEnabled = true
        buildLayout()
        setupHeaderControl()
        setupContainerPinnedBanners()
    }
    
    private func setupContainerPinnedBanners() {
        containerPinnedBanners.onRefresh = { [weak self] height in
            self?.onRefreshPinnedBannerHeight?(height)
        }
    }
    
    private func setupSuggestionsSegmentedControl() {
        suggestionsView.addSubview(segmentedControl)
        segmentedControl.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Layout.SegmentedControl.leading)
            $0.height.equalTo(Layout.SegmentedControl.height)
        }
    }
    
    private func setupSuggestionsCollectionView(withTopView view: UIView) {
        suggestionsView.addSubview(suggestionCollectionView)
        if dependencies.featureManager.isActive(.experimentNewAccessPromotionsMgmBool) {
            suggestionCollectionView.snp.makeConstraints {
                $0.top.equalTo(segmentedControl.snp.bottom)
                $0.leading.trailing.equalToSuperview()
            }
            setupUnifiedPromoView()
        } else {
            suggestionCollectionView.snp.makeConstraints {
                $0.top.equalTo(segmentedControl.snp.bottom)
                $0.leading.trailing.bottom.equalToSuperview()
            }
        }
    }
    
    private func setupFavoritesCarouselCollectionView() {
        suggestionsView.addSubview(favoriteCarouselCollectionView)
        favoriteCarouselCollectionView.snp.makeConstraints {
            $0.top.equalTo(segmentedControl.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(suggestionCollectionView)
        }
    }
    
    private func setupFavoritesLoadingErrorView() {
        suggestionsView.addSubview(favoriteLoadingErrorView)
        favoriteLoadingErrorView.snp.makeConstraints {
            $0.top.equalTo(segmentedControl.snp.bottom).offset(-Layout.FavoriteLoadingErrorView.top)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func setupUnifiedPromoView() {
        suggestionsView.addSubview(unifiedPromoMgmView)
        unifiedPromoMgmView.snp.makeConstraints {
            $0.top.equalTo(suggestionCollectionView.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview()
            $0.height.equalTo(Spacing.base08)
        }
        
        unifiedPromoMgmView.onViewTap = { [weak self] in
            self?.homeHeaderDelegate?.openUnifiedPromotions()
        }
    }
    
    private func setupOriginalHeaderCollectionView() {
        suggestionsView.addSubview(titleLabel)
        titleLabel.layout {
            $0.top == suggestionsView.topAnchor + Spacing.base02
            $0.leading == leadingAnchor + Spacing.base02
            $0.trailing == trailingAnchor
        }
        
        setupSuggestionsCollectionView(withTopView: titleLabel)
    }
    
    func refreshPinnedBanners() {
        viewModel?.refreshPinnedBanners(containerPinnedBanners: containerPinnedBanners)
    }
    
    func setResultForSuggestions(_ result: Result<Void, Error>) {
        viewModel?.suggestionConnectionResult = result
    }
    
    func setResultForFavorite(_ result: Result<Void, Error>) {
        viewModel?.favoritesConnectionResult = result
    }
    
    func setupHeaderControl() {
        favoriteCarouselCollectionView.isHidden = true
        setupSuggestionsSegmentedControl()
        setupSuggestionsCollectionView(withTopView: segmentedControl)
        setupFavoritesCarouselCollectionView()
        setupFavoritesLoadingErrorView()
    }
    
    func startLoading() {
        DispatchQueue.main.async {
            self.setupSuggestionsLoaderView()
        }
    }
    
    func stopLoading() {
        DispatchQueue.main.async {
            self.suggestionsLoaderView.removeFromSuperview()
            self.viewModel?.checkConnectionResult(forIndex: self.segmentedControl.selectedIndex)
        }
    }
    
    private func setupSuggestionsLoaderView() {
        suggestionsView.addSubview(suggestionsLoaderView)
        suggestionsLoaderView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
}

extension HomeHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(
            navigationBar,
            suggestionsView,
            suggestionLimiterView,
            containerPinnedBanners
        )
    }
    
    func setupConstraints() {
        navigationBar.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.NavigationBar.height)
        }
        
        suggestionsView.snp.makeConstraints {
            $0.top.equalTo(navigationBar.snp.bottom)
            $0.leading.trailing.equalToSuperview()
        }
        
        suggestionLimiterView.snp.makeConstraints {
            $0.top.equalTo(suggestionsView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.SuggestionsLineView.height)
        }
        
        containerPinnedBanners.snp.makeConstraints {
            $0.top.equalTo(suggestionsView.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
            $0.bottom.equalToSuperview().offset(-Spacing.base00)
        }
    }
    
    func configureViews() {
        suggestionsView.backgroundColor = Colors.backgroundPrimary.color
        navigationBar.backgroundColor = Colors.backgroundPrimary.color
        suggestionLimiterView.backgroundColor = Colors.backgroundSecondary.color
        suggestionLimiterView.isHidden = dependencies.featureManager.isActive(.experimentNewAccessPromotionsMgmBool)
    }
}

extension HomeHeaderView: CustomSegmentedControlDelegate {
    func changeToIndex(index: Int) {
        if index == 1 {
            Analytics.shared.log(HomeEvent.segmentSelection(.favorites))
            homeHeaderDelegate?.didSelectedTab(.favorites)
            suggestionCollectionView.isHidden = true
            favoriteCarouselCollectionView.isHidden = false
        } else {
            Analytics.shared.log(HomeEvent.segmentSelection(.suggestions))
            homeHeaderDelegate?.didSelectedTab(.suggestions)
            suggestionCollectionView.isHidden = false
            favoriteCarouselCollectionView.isHidden = true
            favoriteLoadingErrorView.isHidden = true
        }
        viewModel?.checkConnectionResult(forIndex: index)
    }
}

extension HomeHeaderView: FavoritesOnboardingLoadingErrorViewDelegate {
    func reloadFavoritesCollection() {
        favoriteLoadingErrorView.isHidden = true
        homeHeaderDelegate?.reloadFavoritesCollectionData()
    }
}

extension HomeHeaderView: HomeHeaderViewDisplay {
    func dismissLoadingErrorView(forSegment index: Int) {
        if index == 0 {
            suggestionsLoaderView.removeFromSuperview()
        } else {
            favoriteLoadingErrorView.isHidden = true
        }
    }
    
    func presentLoadingErrorView(forSegment index: Int) {
        if index == 0 {
            setupSuggestionsLoaderView()
        } else {
            suggestionsLoaderView.removeFromSuperview()
            favoriteCarouselCollectionView.reset()
            favoriteLoadingErrorView.isHidden = false
        }
    }
}
