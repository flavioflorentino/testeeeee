import UI
import UIKit

final class BannerMGMView: UIView {
    var onTapCloseButton: (() -> Void)?
    var onTapView: (() -> Void)?
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = HomeLegacyLocalizable.doYouHaveReferralCode.text
        label.textColor = Palette.ppColorGrayscale000.color
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.medium)
        label.isUserInteractionEnabled = true
        label.numberOfLines = 3
        return label
    }()
    
    private lazy var popupButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        let icon: UIImage = #imageLiteral(resourceName: "iconClose").withRenderingMode(.alwaysTemplate)
        button.setImage(icon, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets.init(top: 6, left: 6, bottom: 6, right: 6)
        button.imageView?.tintColor = #colorLiteral(red:0.08, green:0.36, blue:0.48, alpha:1.00)
        button.addTarget(self, action: #selector(onTapCloseButtonAction), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setUpView()
        setGesture()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUpView() {
        backgroundColor = #colorLiteral(red: 0, green: 175.0/255.0, blue: 238.0/255.0, alpha: 1)
        layer.cornerRadius = 5
        layer.masksToBounds = true
        isUserInteractionEnabled = true
        
        addSubview(textLabel)
        addSubview(popupButton)
        NSLayoutConstraint.activate([textLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
                                     textLabel.trailingAnchor.constraint(equalTo: popupButton.leadingAnchor, constant: -8),
                                     textLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16),
                                     textLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
                                     textLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
                                     // popup button
                                     popupButton.widthAnchor.constraint(equalToConstant: 24),
                                     popupButton.heightAnchor.constraint(equalToConstant: 24),
                                     popupButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
                                     popupButton.centerYAnchor.constraint(equalTo: centerYAnchor)])
    }
    
    private func setGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(onTapViewAction))
        addGestureRecognizer(gesture)
    }
    
    @objc
    private func onTapViewAction(_ sender: UITapGestureRecognizer) {
         onTapView?()
    }
    
    @objc
    private func onTapCloseButtonAction() {
        onTapCloseButton?()
    }
}
