import AnalyticsModule
import Core
import DirectMessageSB
import SnapKit
import UI
import UIKit
import FeatureFlag

protocol LegacyHomeNavigationBarDelegate: AnyObject {
    func homeNavigationBarDidTapScanner(_ navigationBar: LegacyHomeNavigationBar)
    func homeNavigationBarDidTapSettings()
    func homeNavigationBarDidTapInvite(_ navigationBar: LegacyHomeNavigationBar)
    func homeNavigationBarDidTapMessagesButton(messagesStatus: TrackedMessagesStatus)
    func homeNavigationBarDidTapBalance()
    func homeNavigationBarDidTapAtPromotions()
}

private extension LegacyHomeNavigationBar.Layout {
    enum NewIndicator {
        static let size: CGFloat = 8.0
        static let cornerRadius: CGFloat = size / 2.0
        static let marginTop: CGFloat = 2.0
        static let marginTrailing: CGFloat = 3.0
    }
    
    enum Button {
        static let size: CGFloat = 32.0
        static let imageSize = CGSize(width: 22.0, height: 22.0)
    }
    
    enum Margin {
        static let size000: CGFloat = 4.0
        static let size100: CGFloat = 8.0
    }
    
    enum FontSize {
        static let title: CGFloat = 12.0
        static let balance: CGFloat = 18.0
    }
}

final class LegacyHomeNavigationBar: UIView {
    fileprivate enum Layout { }
    typealias Dependencies = HasFeatureManager & HasAnalytics & HasConsumerManager
    private var dependencies: Dependencies
    
    private var isNewNavigationBarActive: Bool {
        dependencies.featureManager.isActive(.experimentNewAccessSettings)
    }
    
    private var isCenteredHeaderExperimentActive: Bool {
        dependencies.featureManager.isActive(.experimentNewFeedCenteredHeader)
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        if isNewNavigationBarActive {
            label.labelStyle(BodySecondaryLabelStyle())
                .with(\.text, String(format: HomeLegacyLocalizable.newNavigationTitlePrefix.text, (dependencies.consumerManager.consumer?.username ?? "")))
                .with(\.textColor, Colors.grayscale750.color)
                .with(\.isHidden, true)
                .with(\.textAlignment, .left)
        } else {
            label.labelStyle(CaptionLabelStyle())
                .with(\.text, HomeLegacyLocalizable.myBalance.text)
                .with(\.textColor, Colors.grayscale700.color)
                .with(\.isHidden, true)
                .with(\.textAlignment, .center)
        }

        return label
    }()
    
    lazy var balanceLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, isNewNavigationBarActive ? Colors.branding600.color: Colors.black.color)
            .with(\.text, "")
            .with(\.textAlignment, isNewNavigationBarActive ? .left : .center)
            .with(\.isHidden, true)
        
        return label
    }()
    
    private lazy var visibilityOffImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "visibilityOff")
        imageView.tintColor = Palette.ppColorGrayscale400.color
        imageView.isHidden = true
        imageView.contentMode = .center
        
        return imageView
    }()
    
    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.startAnimating()
        
        return activityIndicatorView
    }()
    
    private lazy var leftStackContainerView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = Layout.Margin.size100
        
        return stackView
    }()
    
    private lazy var scannerButton: UIButton = {
        let button = UIButton(type: .custom)
        button.adjustsImageWhenHighlighted = false
        button.accessibilityIdentifier = "scannerButton"
        let image = Assets.NewIconography.Navigationbar.newIconQrcode.image
        button.setImage(image, for: .normal)
        button.tintColor = Colors.branding600.color
        button.addTarget(self, action: #selector(self.openScanner(_:)), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var settingsButton: UIButton = {
        let button = UIButton(type: .custom)
        button.adjustsImageWhenHighlighted = false
        button.accessibilityIdentifier = "settingsButton"
        button.accessibilityLabel = DefaultLocalizable.settings.text
        let image = Assets.NewIconography.Navigationbar.newIconSettings.image
        button.setImage(image, for: .normal)
        button.tintColor = Colors.branding600.color
        button.addTarget(self, action: #selector(self.openSettings(_:)), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var rightStackContainerView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = Layout.Margin.size100
        
        return stackView
    }()
    
    private lazy var inviteButton: UIButton = {
        let button = UIButton(type: .custom)
        button.adjustsImageWhenDisabled = false
        button.accessibilityIdentifier = "inviteButton"
        let image = Assets.NewIconography.Navigationbar.newIconRecommendation.image
        button.setImage(image, for: .normal)
        button.tintColor = Colors.branding600.color
        button.addTarget(self, action: #selector(self.openInvitePerson(_:)), for: .touchDown)
        
        return button
    }()
    
    private lazy var promotionsButton: UIButton = {
        let button = UIButton(type: .custom)
        button.adjustsImageWhenDisabled = false
        button.accessibilityIdentifier = "promotionsButton"
        
        let image = Assets.NewIconography.Navigationbar.newIconPromotions.image
        button.setImage(image, for: .normal)
        button.tintColor = Colors.branding600.color
        button.addTarget(self, action: #selector(didTapAtPromotionsButton), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var messagesButton: MessageButton = {
        let button = MessageButton()
        button.addTarget(self, action: #selector(openMessages), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var newIndicatorForPromotion: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorNegative400.color
        view.layer.cornerRadius = Layout.NewIndicator.cornerRadius
        
        return view
    }()
    
    private lazy var balanceView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        view.isHidden = true
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapBalance)))
        return view
    }()
    
    // MARK: - NewNavigationBar elements
    private lazy var profileImageView: UIPPProfileImage = {
        let imageView = UIPPProfileImage()
        guard let consumer = dependencies.consumerManager.consumer else {
            imageView.setContact(PPContact())
            return imageView
        }
        imageView.setContact(PPContact(consumer))
        imageView.isHomeNavigationBar = true
        imageView.delegate = self
        return imageView
    }()
    
    weak var delegate: LegacyHomeNavigationBarDelegate?
    
    override init(frame: CGRect) {
        self.dependencies = DependencyContainer()
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(dependencies: Dependencies) {
        self.init(frame: .zero)
        self.dependencies = dependencies
        buildLayout()
    }
    
    private func canAddPromotionsButton() -> Bool {
        FeatureManager.isActive(.userPromotionsList)
            && !dependencies.featureManager.isActive(.experimentNewAccessPromotionsMgmBool)
            && !FeatureManager.isActive(.directMessageSBEnabled)
    }
 
    func changeUnreadMessagesCount(to count: Int) {
        messagesButton.changeUnreadMessagesCount(to: count)
    }
    
    func stopLoading() {
        titleLabel.isHidden = false
        balanceView.isHidden = false
        activityIndicatorView.stopAnimating()
    }
    
    func setupBalanceVisibility(_ isHidden: Bool) {
        balanceLabel.isHidden = isHidden
        visibilityOffImage.isHidden = !isHidden
    }
}

extension LegacyHomeNavigationBar: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(leftStackContainerView)
        addSubview(rightStackContainerView)
        addSubview(activityIndicatorView)
        
        balanceView.addSubview(titleLabel)
        balanceView.addSubview(balanceLabel)
        balanceView.addSubview(visibilityOffImage)
        addSubview(balanceView)
        
        leftStackContainerView.addArrangedSubview(scannerButton)
        
        if dependencies.featureManager.isActive(.experimentNewAccessSettings) {
            leftStackContainerView.addArrangedSubview(profileImageView)
        } else {
            dependencies.analytics.log(SettingsEvent.settingsHomeButtonViewed)
            leftStackContainerView.addArrangedSubview(settingsButton)
        }
        
        rightStackContainerView.addArrangedSubview(inviteButton)
        
        if dependencies.featureManager.isActive(.directMessageSBEnabled) {
            rightStackContainerView.addArrangedSubview(messagesButton)
        }
        
        guard canAddPromotionsButton() else { return }
        
        dependencies.analytics.log(PromotionsEvent.promotionsHomeButtonViewed)
        rightStackContainerView.addArrangedSubview(promotionsButton)
        
        guard !KVStore().getFirstTimeOnlyEvent(PromotionsUserDefaultKey.alreadyOpened.rawValue) else { return }
        promotionsButton.addSubview(newIndicatorForPromotion)
    }
    
    func setupConstraints() {
        leftStackContainerView.snp.makeConstraints {
            $0.leading.equalTo(snp.leading).offset(Layout.Margin.size100)
            $0.centerY.equalTo(snp.centerY)
        }
        
        scannerButton.snp.makeConstraints {
            $0.width.equalTo(Layout.Button.size)
            $0.height.equalTo(Layout.Button.size)
        }
        
        if dependencies.featureManager.isActive(.experimentNewAccessSettings) {
            profileImageView.snp.makeConstraints {
                $0.width.equalTo(Layout.Button.size)
                $0.height.equalTo(Layout.Button.size)
            }
        } else {
            settingsButton.snp.makeConstraints {
                $0.width.equalTo(Layout.Button.size)
                $0.height.equalTo(Layout.Button.size)
            }
        }
        
        rightStackContainerView.snp.makeConstraints {
            $0.trailing.equalTo(snp.trailing).offset(-Layout.Margin.size100)
            $0.centerY.equalTo(snp.centerY)
        }
        
        inviteButton.snp.makeConstraints {
            $0.width.equalTo(Layout.Button.size)
            $0.height.equalTo(Layout.Button.size)
        }
        
        messagesButton.snp.makeConstraints {
            $0.width.equalTo(Layout.Button.size)
            $0.height.equalTo(Layout.Button.size)
        }
        
        visibilityOffImage.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom)
            $0.bottom.equalTo(balanceView.snp.bottom)
            $0.leading.equalTo(balanceView.snp.leading)
            $0.trailing.equalTo(balanceView.snp.trailing)
        }
        
        
        balanceView.snp.makeConstraints {
            $0.bottom.equalTo(snp.bottom).offset(-Layout.Margin.size000)
            guard isNewNavigationBarActive else {
                $0.top.equalTo(snp.top).offset(Layout.Margin.size000)
                $0.centerX.equalTo(snp.centerX)
                $0.leading.greaterThanOrEqualTo(leftStackContainerView.snp.trailing)
                $0.trailing.lessThanOrEqualTo(rightStackContainerView.snp.leading)
                return
            }
            
            guard isCenteredHeaderExperimentActive else {
                $0.top.equalTo(snp.top).offset(Layout.Margin.size000)
                $0.leading.equalTo(leftStackContainerView.snp.trailing).offset(Spacing.base01)
                $0.centerY.equalTo(leftStackContainerView.snp.centerY)
                return
            }
            $0.top.equalTo(snp.top)
            $0.center.equalToSuperview()
            $0.width.equalTo(titleLabel.snp.width)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            guard isNewNavigationBarActive,
                  isCenteredHeaderExperimentActive else {
                $0.leading.equalTo(balanceView.snp.leading)
                $0.trailing.equalTo(balanceView.snp.trailing)
                return
            }
            $0.centerX.equalToSuperview()
        }
        
        balanceLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom)
            $0.bottom.equalTo(balanceView.snp.bottom)
            guard isNewNavigationBarActive,
                  isCenteredHeaderExperimentActive else {
                $0.leading.equalTo(balanceView.snp.leading)
                $0.trailing.equalTo(balanceView.snp.trailing)
                return
            }
            $0.centerX.equalToSuperview()
        }
        
        activityIndicatorView.snp.makeConstraints {
            $0.centerX.equalTo(snp.centerX)
            $0.centerY.equalTo(snp.centerY)
        }
        
        guard canAddPromotionsButton() && promotionsButton.superview != nil else { return }
        
        promotionsButton.snp.makeConstraints {
            $0.width.equalTo(Layout.Button.size)
            $0.height.equalTo(Layout.Button.size)
        }
        
        guard
            KVStore().getFirstTimeOnlyEvent(PromotionsUserDefaultKey.alreadyOpened.rawValue) == false,
            let imageView = promotionsButton.imageView
        else {
            return
        }
        
        newIndicatorForPromotion.snp.makeConstraints {
            $0.width.equalTo(Layout.NewIndicator.size)
            $0.height.equalTo(Layout.NewIndicator.size)
            $0.top.equalTo(imageView.snp.top).offset(-Layout.NewIndicator.marginTop)
            $0.trailing.equalTo(imageView.snp.trailing).offset(Layout.NewIndicator.marginTrailing)
        }
    }
    
    func configureViews() {
        
    }
}

// MARK: - Objective-C
extension LegacyHomeNavigationBar {
    @objc
    private func openScanner(_ sender: Any? = nil) {
        dependencies.analytics.log(ScannerEvent.qrcodeAccessed(.home))
        delegate?.homeNavigationBarDidTapScanner(self)
    }
    
    @objc
    private func openSettings(_ sender: Any? = nil) {
        delegate?.homeNavigationBarDidTapSettings()
    }
    
    @objc
    private func openInvitePerson(_ sender: Any? = nil) {
        delegate?.homeNavigationBarDidTapInvite(self)
    }
    
    @objc
    private func didTapAtPromotionsButton() {
        delegate?.homeNavigationBarDidTapAtPromotions()
        
        KVStore().setFirstTimeOnlyEvent(PromotionsUserDefaultKey.alreadyOpened.rawValue)
        newIndicatorForPromotion.isHidden = true
    }
    
    @objc
    private func openMessages() {
        delegate?.homeNavigationBarDidTapMessagesButton(messagesStatus: messagesButton.messagesStatus)
    }
    
    @objc
    private func didTapBalance() {
        delegate?.homeNavigationBarDidTapBalance()
    }
}

extension LegacyHomeNavigationBar: UIPPProfileImageDelegate {
    func profileImageViewDidTap() {
        delegate?.homeNavigationBarDidTapSettings()
    }
}
