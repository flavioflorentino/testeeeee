import Core
import DirectMessageSB
import UIKit
import FeatureFlag

protocol LegacyHomeViewModelOutputs: AnyObject {
    var shouldDisplayPixKeyRegistrationPopup: Bool { get }
    func presentFavoritesCarouselLoadingErrorView()
}

final class LegacyHomeViewModel {
    typealias Dependencies = HasLocationManager
        & HasMainQueue
        & HasKVStore
        & HasFeatureManager
        & HasChatApiManager
    
    private var dependencies: Dependencies
    
    var lastAvailableLocation: CLLocation? {
        return dependencies.locationManager.lastAvailableLocation
    }
    
    var isLoaddingTimeLine: Bool = false
    fileprivate let api = SearchApi()
    private let service: FavoritesServicing = FavoritesService()
    private lazy var mgmIncentiveWorker: MGMIncentiveVerifiable = {
        let service = MGMIncentiveService(dependencies: dependencies)
        return MGMIncentiveWorker(service: service)
    }()
    
    weak var outputs: LegacyHomeViewModelOutputs?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    var shouldDisplayPixKeyRegistrationPopup: Bool {
        dependencies.featureManager.isActive(.releasePixKeyRegistrationHomePopupBool)
            && !dependencies.kvStore.boolFor(KVKey.isPixModalHomeVisualized)
    }
    
    private lazy var updatePrivacyService = UpdatePrivacyService(dependencies: dependencies)
    
    func loadSuggestions(onCacheLoaded: @escaping (LegacySearchResultSection?) -> Void, completion: @escaping (LegacySearchResultSection?) -> Void) {
        
        let request = SearchRequest()
        request.group = "SUGGESTIONS"
        request.term = nil
        
        if let coordinate = lastAvailableLocation?.coordinate {
            request.latitude = coordinate.latitude
            request.longitude = coordinate.longitude
        }
        
        request.page = 0
        
        _ = api.search(request: request, originFlow: .feed, cacheKey: "home-suggestions-list", onCacheLoaded: { [weak self] searchResponse in
            let sugestions = self?.extractSugestionSection(searchResponse)
            DispatchQueue.main.async {
                onCacheLoaded(sugestions)
            }
        }, completion: { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let searchResponse):
                    let sugestions = self?.extractSugestionSection(searchResponse)
                    completion(sugestions)
                case .failure:
                    completion(nil)
                }
            }
        })
    }
    
    func loadStudentAccountStatus(completion: @escaping (_ showPopUp: Bool) -> Void) {
        StudentWorker().loadStatus { result in
            switch result {
            case .success(let payload) where payload.status == .active:
                KVStore().setBool(true, with: .hasStudentAccountActive)
                guard KVStore().boolFor(.studentAccountActivePopup) else {
                    return
                }
                completion(true)
                KVStore().setBool(false, with: .studentAccountActivePopup)
            default:
                KVStore().setBool(false, with: .hasStudentAccountActive)
                completion(false)
            }
        }
    }
    
    func getHomeFavorites(completion: @escaping ([HomeFavorites.Item]) -> Void) {
        service.homeFavorites { result in
            switch result {
            case let .success(favorites):
                completion(
                    favorites
                        .flatMap { $0.items }
                        .filter {
                            guard case HomeFavorites.Item.unknown = $0 else {
                                return true
                            }

                            return false
                        }
                )
            case .failure:
                completion([])
            }
        }
    }

    func canShowIncentiveModal(completion: @escaping (Bool) -> Void) {
        mgmIncentiveWorker.checkCanShowIncentive(completion: completion)
    }
    
    func updateHomeAccessCount() {
        mgmIncentiveWorker.updateHomeAccessCount()
    }
    
    func setPaymentInPrivate() {
        let helper = PrivacyConfigHelper(updatePrivacyService: updatePrivacyService)
        helper.setPaymentInPrivateConfig()
    }
    
    func checkReviewPrivacy(cameFromReceipt: Bool, completion: @escaping (Bool) -> Void) {
        let helper = PrivacyConfigHelper(updatePrivacyService: updatePrivacyService)
        helper.checkReviewPrivacy(cameFromReceipt: cameFromReceipt) { privacyResult in
            guard let privacyResult = privacyResult else { return }
            self.trackingAPIResult(with: privacyResult)
            completion(privacyResult.shouldReview)
        }
    }
    
    private func trackingAPIResult(with privacyResult: PrivacyConfig) {
        let dependencies = DependencyContainer()
        let consumerId = dependencies.consumerManager.consumer?.wsId.toString()
        
        if privacyResult.apiResultIsSuccess {
            dependencies.analytics.log(
                UpdatePrivacyAPIEvent.calledApiFromHomeSuccess(consumerId,
                                                               isReviewed: privacyResult.isReviewed,
                                                               hasTransactions: privacyResult.hasTransactions)
            )
        } else {
            dependencies.analytics.log(UpdatePrivacyAPIEvent.calledApiFromHomeError(consumerId))
        }
    }

    private func extractSugestionSection(_ searchResponse: SearchApi.SearchApiResponse) -> LegacySearchResultSection? {
        guard let section = searchResponse.list.first else {
            return nil
        }
        
        return section
    }
}
