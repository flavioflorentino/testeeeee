import AnalyticsModule
import CustomerSupport
import DirectMessageSB
import UIKit
import SnapKit
import AsyncDisplayKit
import Core
import CoreLegacy
import UI
import Store
import FeatureFlag
import PermissionsKit
import PIX
import ReceiptKit

final class LegacyHomeViewController: PPBaseViewController {
    private let isNewParking = 2

    override func navigationBarIsTransparent() -> Bool {
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    
    private let defaultbackgroundColor: UIColor = Palette.ppColorGrayscale100.color
    private lazy var headerView: HomeHeaderView = {
        let viewModel = HomeHeaderViewModel(presenter: HomeHeaderViewPresenter())
        let view = HomeHeaderView(viewModel: viewModel, dependencies: self.dependencies)
        view.navigationBar.delegate = self
        view.suggestionCollectionView.recentsDelegate = self
        view.favoriteCarouselCollectionView.favoritesDelegate = self
        view.backgroundColor = Colors.backgroundPrimary.color
        view.homeHeaderDelegate = self
        return view
    }()
    
    private lazy var feedFixedHeaderContainer:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var feedFloatHeaderContainer:UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = true
        return view
    }()
    
    private lazy var statusBarView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var rootScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.bounces = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.isScrollEnabled = true
        scrollView.delegate = self
        scrollView.delaysContentTouches = true
        scrollView.canCancelContentTouches = false
        scrollView.backgroundColor = Colors.backgroundPrimary.color
        scrollView.contentInset = .zero
        scrollView.scrollIndicatorInsets = .zero
        scrollView.contentOffset = CGPoint(x: 0, y: 0)
        scrollView.bounces = true
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    private lazy var feedView: UIView = {
        let view = UIView()
        view.backgroundColor = self.defaultbackgroundColor
        return view
    }()
    private lazy var feedVisibilitySelector: LegacyHomeFeedHeaderView = {
        let view = LegacyHomeFeedHeaderView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        return view
    }()
    private lazy var fixedHeaderView: UIView = {
        let fixedHeader = UIView()
        fixedHeader.translatesAutoresizingMaskIntoConstraints = false
        fixedHeader.backgroundColor = self.defaultbackgroundColor
        return fixedHeader
    }()
    private lazy var feedHeader: UIView = {
        let feedHeader = UIView()
        return feedHeader
    }()
    private lazy var bannerMGMView: BannerMGMView = {
        let banner: BannerMGMView = BannerMGMView()
        banner.translatesAutoresizingMaskIntoConstraints = false
        banner.onTapView = { [weak self] in
            self?.bannerMgmCodeOpenPromoCode()
        }
        banner.onTapCloseButton = { [weak self] in
            self?.bannerMgmCodeOpenPopup()
        }
        
        return banner
    }()
    private lazy var customRefreshControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.translatesAutoresizingMaskIntoConstraints = false
        refresh.addTarget(self, action: #selector(reloadPullRefresh), for: .valueChanged)
        return refresh
    }()
    private var feedAllViewController: FeedViewController?
    private var feedPrivateViewController: FeedViewController?
    private var model = LegacyHomeViewModel(dependencies: DependencyContainer())
    private var headerHeight: CGFloat = FeatureManager.shared.isActive(.experimentNewAccessPromotionsMgmBool) ? 285.0 : 205.0
    private var bannerMGMCodeViewHeight: CGFloat = 62
    private var pinnedBannerHeight: CGFloat = 0 {
        didSet {
            headerHeight -= oldValue
            headerHeight += pinnedBannerHeight
            
            updateFeedHeight()
        }
    }
    private var completeHeaderHeight: CGFloat {
        get {
            return self.feedHeaderHeight + self.headerHeight
        }
    }
    private var sugestionsContainerHeight: CGFloat = 149.0
    private var feedFixedHeaderContainerHeight: CGFloat = 50 {
        didSet {
            self.view.setNeedsUpdateConstraints()
        }
    }
    private var feedFloatHeaderContainerHeight: CGFloat = 0 {
        didSet {
            self.view.setNeedsUpdateConstraints()
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
    }
    private var feedHeaderHeight:CGFloat {
        get {
            return self.feedFixedHeaderContainerHeight + self.feedFloatHeaderContainerHeight
        }
    }
    private var pageViewController: UIPageViewController!
    private var activeVisibility: FeedItemVisibility = .Friends {
        didSet {
            self.feedVisibilitySelector.selectVisibilty(activeVisibility)
        }
    }
    private var isNoSugestions: Bool = false {
        didSet(oldValue) {
            if self.isNoSugestions == oldValue {
                return
            }
            if self.isNoSugestions {
                headerHeight -= sugestionsContainerHeight
            } else {
                headerHeight += sugestionsContainerHeight
            }

            updateFeedHeight()
        }
    }
    
    private func updateFeedHeight() {
        UIView.animate(withDuration: 0.25, animations: {
            self.headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.headerHeight)
            self.feedHeader.frame = CGRect(x: 0, y: self.headerHeight, width: self.view.frame.width, height: self.feedHeaderHeight)
            self.feedView.frame = CGRect(x: 0, y: self.completeHeaderHeight, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: { completed in
            if completed {
                /// remove fresh controll for reset constraints
                self.customRefreshControl.removeFromSuperview()
                /// inserting in the first position so as not to be present at the top of the other views
                self.rootScrollView.insertSubview(self.customRefreshControl, at: 0)
                NSLayoutConstraint.activate([self.customRefreshControl.topAnchor.constraint(equalTo: self.rootScrollView.topAnchor, constant: self.headerHeight),
                                             self.customRefreshControl.centerXAnchor.constraint(equalTo: self.rootScrollView.centerXAnchor, constant: 0),
                                             self.customRefreshControl.widthAnchor.constraint(equalToConstant: 70),
                                             self.customRefreshControl.heightAnchor.constraint(equalToConstant: 70)])
            }
        })
    }
    
    private var itemSelected: Int?
    private var hasAtLeastOneFeedOnce = true
    private var favoritesFlowCoordinator: FavoriteFlowCoordinator?
    private lazy var auth: PPAuth = {
        return PPAuth()
    }()

    private var childCoordinator: Coordinating?
    typealias Dependencies = HasFeatureManager & HasAnalytics & HasUserManager & HasConnectionStateManufacturing
    private var dependencies: Dependencies
    lazy var connectionStateInteractor = dependencies
        .connectionStateFactory
        .makeInteractor(delegate: self, display: self, screen: .home)
    
    // MARK: - Initializer
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func scrollTop(){
        let desiredOffset = CGPoint(x: 0, y: -self.rootScrollView.contentInset.top)
        self.rootScrollView.setContentOffset(desiredOffset, animated: true)
    }
    
    // MARK: - View Lifecyle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        view.backgroundColor = Colors.backgroundPrimary.color
        
        
        self.hasAtLeastOneFeedOnce = AppParameters.global().hasAtLeastOneFeedOnce()
        AppParameters.global().setNotFirstTimeOnApp()
        
        if let token = dependencies.userManager.token {
            CustomerSupportManager.shared.registerUserLoggedCredentials(accessToken: token)
            CustomerSupportManager.shared.register(notificationToken: token)
        }
        
        setup()
        
        checkStudentStatus()
        model.setPaymentInPrivate()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(loadFavorites),
                                               name: NSNotification.Name.reloadFavoriteSection,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(shouldOpenPrivacyReview),
                                               name: NSNotification.PrivacyReview.shouldOpen.name,
                                               object: nil)
        addChatUnreadCountObserver()
        
        if #available(iOS 13.0, *) {
            dependencies.analytics.log(UserEvent.darkModeOn(traitCollection.userInterfaceStyle == .dark))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // prevent the refresh from appearing
        if !model.isLoaddingTimeLine && rootScrollView.contentOffset.y <= 0  {
            customRefreshControl.endRefreshing()
            rootScrollView.contentOffset.y = 0.0
        }
        
        dependencies.analytics.log(HomeEvent.screenViewed)

        headerView.refreshPinnedBanners()

        ConsumerManager.shared.loadConsumerBalance()
        loadSuggestions()
        headerView.navigationBar.setupBalanceVisibility(ConsumerManager.shared.isBalanceHidden)
        

        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForegroung(_:)), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        displayPixPopupOnFirstTimeHomeVisualized()
        displayReviewPrivacyFlow()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        PaginationCoordinator.shared.configurePagination()
        connectionStateInteractor.didCancelAction()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
        PaginationCoordinator.shared.configurePagination()
        
        PPAnalytics.trackFirstTimeOnlyEvent("FT Inicio", properties: ["breadcrumb": BreadcrumbManager.shared.breadcrumb])
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            CashbackReceipWidgetItem.openCashbackPopup(in: strongSelf.navigationController?.tabBarController ?? strongSelf)
        }
        
        //MGM referfalCode
        if let dismissDate = AppParameters.global().referalCodeBannerDismissDate, dismissDate > Date() {
            self.showMgmBanner()
        } else {
            self.dismissMgmBanner()
        }
        
        model.updateHomeAccessCount()
        model.canShowIncentiveModal { [weak self] canShowModal in
            if canShowModal {
                self?.showPayWithPicpayAlert()
            }
        }
        
        // Social PopUp Onboard
        if !dependencies.featureManager.isActive(.newOnboarding), AppParameters.global().getOnboardSocial() {
            showSocialOnboarding()
        } else if AppParameters.global().displayCreditCardOnboardDelay > 0 && CreditCardManager.shared.cardList.isEmpty {
            // Credit card onboard
            let delay = AppParameters.global().displayCreditCardOnboardDelay
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delay)) { [weak self] in
                self?.showCreditCardOnboard()
            }
        }
        
        // Check Notification Permition
        if !AppParameters.global().getOnboardSocial() {
            PPPermission.requestNotifications()
        }
    }
    
    override func updateViewConstraints() {
        if let floatHeightConstraint = self.feedFloatHeaderContainer.constraints.first(where: {$0.firstAttribute == .height}), floatHeightConstraint.constant != self.feedFloatHeaderContainerHeight {
            floatHeightConstraint.constant = self.feedFloatHeaderContainerHeight
        }
        if let fixedHeightConstraint = self.feedFixedHeaderContainer.constraints.first(where: {$0.firstAttribute == .height}), fixedHeightConstraint.constant != self.feedFixedHeaderContainerHeight {
            fixedHeightConstraint.constant = self.feedFixedHeaderContainerHeight
        }
        super.updateViewConstraints()
    }
    
    // MARK: - Configure View
    
    private func setup() {
        setupViews()
        setupFeedControllers()
        selectVisibility(activeVisibility)
        setupNotificationsHandler()
        headerView.onRefreshPinnedBannerHeight = { [weak self] height in
            self?.pinnedBannerHeight = height
        }
    }
    
    // MARK: - Internal Methods
    
    /// Load the suggestions on header view
    private func loadSuggestions() {
        headerView.startLoading()
        model.loadSuggestions(onCacheLoaded: { [weak self] suggestionSection in
            self?.setupSuggestions(suggestionSection)
        }, completion: { [weak self] suggestionSection in
            self?.setupSuggestions(suggestionSection)
        })
    }
    
    private func setupSuggestions(_ suggestionSection: LegacySearchResultSection?) {
        headerView.stopLoading()
        guard let section = suggestionSection else {
            headerView.setResultForSuggestions(.failure(ApiError.unknown(nil)))
            return
        }
        
        headerView.setResultForSuggestions(.success)
        section.style = .home
        headerView.suggestionCollectionView.setup(results: section, imageDelegate: self)
        isNoSugestions = section.rows.isEmpty
    }
    
    @objc
    private func loadFavorites() {
        headerView.startLoading()
        model.getHomeFavorites { [weak self] homeHeaderModel in
            guard homeHeaderModel.isNotEmpty else {
                self?.headerView.setResultForFavorite(.failure(ApiError.unknown(nil)))
                self?.headerView.stopLoading()
                return
            }
            self?.headerView.setResultForFavorite(.success)
            self?.setupCarouselCollectionView(model: homeHeaderModel)
        }
    }
    
    private func setupCarouselCollectionView(model: [HomeFavorites.Item]) {
        headerView.favoriteCarouselCollectionView.setup(result: model) { [weak self] in
            self?.headerView.stopLoading()
        }
    }
    
    /// Show
    private func showSocialOnboarding() {
        let onboardingAutorizeButton = Button(title: HomeLegacyLocalizable.authorizeContacts.text, type: .cta) { [weak self] popupController, _ in
            popupController.dismiss(animated: true, completion: {
                self?.findFriends()
                AppParameters.global().setOnboardSocial(false)
            })
        }
        let onboardingSkipButton = Button(title: DefaultLocalizable.notNow.text, type: .inline) { [weak self] popupController, _ in
            popupController.dismiss(animated: true, completion: {
                self?.trackEvent(false)
                AppParameters.global().setOnboardSocial(false)
            })
        }
        
        let alert = Alert(
            with: HomeLegacyLocalizable.findYourFriends.text,
            text: HomeLegacyLocalizable.toFindOut.text,
            buttons: [onboardingAutorizeButton, onboardingSkipButton]
        )
        
        alert.image = Alert.Image(name: "onboarding_social_popup")
        
        AlertMessage.showAlert(alert, controller: self)
    }
    
    private func showCreditCardOnboard() {
        if navigationController?.viewControllers.last == self {
            AppParameters.global().displayCreditCardOnboardDelay = 0
            CreditCardOnboardingPopUpViewController.showCreditCardOnboarding(self)
        }
    }
    
    func showPayWithPicpayAlert() {
        let mainButton = Button(
            title: MGMIncentiveModalLocalizable.buttonTitle.text,
            type: .cta
        ) { alertController, _ in
            Analytics.shared.log(IncentiveModalEvent.pay)
            
            alertController.dismiss(animated: true) {
                AppManager.shared.mainScreenCoordinator?.showPaymentScreen()
            }
        }
        
        let notNowButton = Button(
            title: MGMIncentiveModalLocalizable.notNow.text,
            type: .cleanUnderlined
        ) { alertController, _ in
            Analytics.shared.log(IncentiveModalEvent.notNow)
            
            alertController.dismiss(animated: true)
        }
        
        let alert = Alert(
            with: MGMIncentiveModalLocalizable.title.text,
            text: MGMIncentiveModalLocalizable.subtitle.text,
            buttons: [
                mainButton,
                notNowButton
            ],
            image: Alert.Image(with: Assets.Icons.icoCoin.image)
        )
        
        AlertMessage.showAlert(alert, controller: self)
    }
    
    @objc private func willEnterForegroung(_ notification: Notification) {
        guard User.isAuthenticated else {
            return
        }
        loadSuggestions()
    }
    
    // MARK: - User Actions
    
    func selectVisibility(_ visibility: FeedItemVisibility) {
        if visibility == .Friends {
            guard let feedVC = feedAllViewController else {
                return
            }
            pageViewController.setViewControllers([feedVC], direction: .reverse, animated: true, completion: { [weak self] _ in
                self?.playEmptyViewAnimation(feedVC)
            })
        } else {
            guard let feedPrivateVC = feedPrivateViewController else {
                return
            }
            pageViewController.setViewControllers([feedPrivateVC], direction: .forward, animated: true, completion: { [weak self] _ in
                self?.playEmptyViewAnimation(feedPrivateVC)
            })
        }
        activeVisibility = visibility
        BreadcrumbManager.shared.addCrumb(visibility == .Friends ? HomeLegacyLocalizable.all.text : HomeLegacyLocalizable.you.text, typeOf: .tab, withProperties: nil)
    }
    
    func playEmptyViewAnimation(_ feedViewController: FeedViewController) {
        if let headerTable = feedViewController.tableNode.view.tableHeaderView as? FeedEmptyListView {
            DispatchQueue.main.async {
                headerTable.playAnimation()
            }
        }
    }
    
    func openScanner() {
        PaginationCoordinator.shared.showScanner()
    }
    
    func openSettings() {
        guard let navigationController = navigationController else { return }
        let settingsCoordinator = SettingsCoordinator(navigationController: navigationController)
        settingsCoordinator.start()
        Analytics.shared.log(HomeEvent.settingsAccessed)
    }
    
    func openInvite() {
        if dependencies.featureManager.isActive(.featureMgmIndicationCenter) {
            let controller = ReferralPaginationFactory.make()
            let navigationController = PPNavigationController(rootViewController: controller)
            navigationController.modalPresentationStyle = .fullScreen
            present(navigationController, animated: true)
        } else {
            guard let viewController = ViewsManager.peopleSearchStoryboardViewController("SocialShareCode") as? SocialShareCodeViewController else {
                return
            }
            Analytics.shared.log(HomeEvent.invitePeople)
            viewController.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(viewController, animated: true)
        }
    }

    func openPromotions() {
        guard let navigationController = navigationController else {
            return
        }
        
        if FeatureManager.isActive(.releaseChatPocPresentingBool) {
            let chatListViewController = ChatListFactory.make()
            navigationController.pushViewController(chatListViewController, animated: true)
            return
        }

        let coordinator = PromotionsCoordinator(
            navigationController: navigationController,
            controllerFactory: PromotionsFactory(),
            coordinatorFactory: PromotionsFactory()
        )

        coordinator.didFinishFlow = { [weak self] in
            self?.childCoordinator = nil
        }

        coordinator.start()
        childCoordinator = coordinator

        Analytics.shared.log(PromotionsEvent.promotionListAccessed(origin: .home))
    }
    
    func openMessages() {
        let chatListViewController = ChatListFactory.make()
        navigationController?.pushViewController(chatListViewController, animated: true)
    }
    
    func reloadFeed() {
        feedAllViewController?.reloadFeed()
        feedPrivateViewController?.reloadFeed()
    }
}

// MARK: - Setup
extension LegacyHomeViewController {
    
    private func setupViews() {
        statusBarView.translatesAutoresizingMaskIntoConstraints = false
        
        statusBarView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 20)
        rootScrollView.frame = CGRect(x: 0, y: 20, width: view.frame.width, height: view.frame.height)
        headerView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: headerHeight)
        feedHeader.frame = CGRect(x: 0, y: headerHeight, width: view.frame.width, height: feedHeaderHeight)
        feedView.frame = CGRect(x: 0, y: completeHeaderHeight, width: view.frame.width, height: view.frame.height)
        customRefreshControl.translatesAutoresizingMaskIntoConstraints = false
        
        feedHeader.addSubview(feedFixedHeaderContainer)
        feedHeader.addSubview(feedFloatHeaderContainer)
        NSLayoutConstraint.activate([feedFixedHeaderContainer.topAnchor.constraint(equalTo: feedHeader.topAnchor),
                                     feedFixedHeaderContainer.leftAnchor.constraint(equalTo: feedHeader.leftAnchor),
                                     feedFixedHeaderContainer.rightAnchor.constraint(equalTo: feedHeader.rightAnchor),
                                     feedFixedHeaderContainer.heightAnchor.constraint(equalToConstant: feedFixedHeaderContainerHeight),
                                     // feed float
            feedFloatHeaderContainer.heightAnchor.constraint(equalToConstant: feedFloatHeaderContainerHeight),
            feedFloatHeaderContainer.leftAnchor.constraint(equalTo: feedHeader.leftAnchor),
            feedFloatHeaderContainer.rightAnchor.constraint(equalTo: feedHeader.rightAnchor),
            feedFloatHeaderContainer.bottomAnchor.constraint(equalTo: feedHeader.bottomAnchor)])
        
        feedFixedHeaderContainer.addSubview(self.feedVisibilitySelector)
        NSLayoutConstraint.activate([feedVisibilitySelector.topAnchor.constraint(equalTo: feedFixedHeaderContainer.topAnchor),
                                     feedVisibilitySelector.leftAnchor.constraint(equalTo: feedFixedHeaderContainer.leftAnchor),
                                     feedVisibilitySelector.rightAnchor.constraint(equalTo: feedFixedHeaderContainer.rightAnchor),
                                     feedVisibilitySelector.heightAnchor.constraint(equalToConstant: 50)])
        
        view.addSubview(statusBarView)
        view.addSubview(rootScrollView)
        rootScrollView.addSubview(customRefreshControl)
        rootScrollView.addSubview(headerView)
        rootScrollView.addSubview(feedHeader)
        rootScrollView.addSubview(feedView)
        rootScrollView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height + self.completeHeaderHeight)
        
        // status bar
        if #available(iOS 11.0, *) {
            statusBarView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            statusBarView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
            statusBarView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        }
        statusBarView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        statusBarView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        // scroll view
        NSLayoutConstraint.activate([rootScrollView.topAnchor.constraint(equalTo: statusBarView.bottomAnchor, constant: 0),
                                     rootScrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
                                     rootScrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
                                     rootScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
                                     // set layout for refresh control
            customRefreshControl.topAnchor.constraint(equalTo: feedFloatHeaderContainer.topAnchor),
            customRefreshControl.centerXAnchor.constraint(equalTo: rootScrollView.centerXAnchor),
            customRefreshControl.widthAnchor.constraint(equalToConstant: 50),
            customRefreshControl.heightAnchor.constraint(equalToConstant: 50)])
        
        // Create view for feed header at the top of the screen
        fixedHeaderView.isHidden = true
        view.addSubview(fixedHeaderView)
        fixedHeaderView.snp.makeConstraints { (make) in
            make.top.equalTo(statusBarView.snp.bottom)
            make.right.left.equalToSuperview()
            make.height.equalTo(feedFixedHeaderContainerHeight)
        }
    }
    
    private func setupFeedControllers() {
        // All Feed
        let tableNodeAll = ASTableNode(style: .plain)
        tableNodeAll.frame = CGRect(x: 0, y: 0, width: feedView.frame.size.width, height: feedView.frame.size.height)
        tableNodeAll.view.isScrollEnabled = false
        tableNodeAll.view.bounces = true
        feedAllViewController = FeedViewController(tableView: tableNodeAll)
        feedAllViewController?.isScrollViewEnable = false
        feedAllViewController?.delegate = self
        feedAllViewController?.changeFilter(Int(FeedItemVisibility.Friends.rawValue) ?? 3)
        feedAllViewController?.configureModel()
        feedAllViewController?.configureTableView()
        feedAllViewController?.tableNode.backgroundColor = Colors.backgroundPrimary.color
        
        
        // private feed
        let tableNodePrivate = ASTableNode(style: .plain)
        tableNodePrivate.frame = CGRect(x: 0, y: 0, width: feedView.frame.size.width, height: feedView.frame.size.height)
        tableNodePrivate.view.isScrollEnabled = false
        tableNodePrivate.view.bounces = false
        feedPrivateViewController = FeedViewController(tableView: tableNodePrivate)
        feedPrivateViewController?.isScrollViewEnable = false
        feedPrivateViewController?.delegate = self
        feedPrivateViewController?.changeFilter(Int(FeedItemVisibility.Private.rawValue) ?? 3)
        feedPrivateViewController?.configureModel()
        feedPrivateViewController?.configureTableView()
        feedPrivateViewController?.tableNode.backgroundColor = Colors.backgroundPrimary.color
        
        
        // page control
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.setViewControllers([feedAllViewController!], direction: .forward, animated: true, completion: nil)
        pageViewController.dataSource = self
        pageViewController.delegate = self
        
        /// Set page view controller for not scroll on drasg view in horizontal
        for case let scrollView as UIScrollView in pageViewController.view.subviews {
            scrollView.isScrollEnabled = false
        }
        
        feedView.clipsToBounds = false
        addChild(pageViewController)
        pageViewController.didMove(toParent: self)
        pageViewController.view.frame = feedView.bounds
        feedView.embed(pageViewController.view)
    }
    
    private func setupNotificationsHandler() {
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "PushReceived"), object: nil, queue: nil) { (notification) in
            DispatchQueue.main.async {
                self.pushReceivedHandler(notification)
            }
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "ContactsInvited"), object: nil, queue: nil) { (notification) in
            DispatchQueue.main.async {
                self.pushReceivedHandler(notification)
            }
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "FeedNeedReload"), object: nil, queue: nil) { (_) in
            DispatchQueue.main.async {
                self.reloadFeed()
            }
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.Payment.new, object: nil, queue: nil) { (notification) in
            DispatchQueue.main.async {
                self.paymentNewHandler(notification)
            }
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "balanceChanged"), object: nil, queue: nil) { (_) in
            DispatchQueue.main.async {
                self.updateBalanceView()
            }
        }
    }
    
    // MARK: - Notifications Handler
    
    private func pushReceivedHandler(_ notification: Notification){
        updateBalanceView()
        reloadFeed()
    }
    
    private func paymentNewHandler(_ notification: Notification){
        selectVisibility(.Friends)
    }
    
    // MARK: - Views Update
    
    private func updateBalanceView() {
        guard let consumer = ConsumerManager.shared.consumer else {
            return
        }
        headerView.navigationBar.stopLoading()
        headerView.navigationBar.balanceLabel.text = CurrencyFormatter.brazillianRealString(from: consumer.balance)
    }
    
    // utils
    @objc private func updateFeed() {
        if activeVisibility == .Friends {
            guard let controller = feedAllViewController else {
                return
            }
            controller.reloadFeed()
        } else {
            guard let controller = feedPrivateViewController else {
                return
            }
            controller.reloadFeed()
        }
    }
}

// MARK: - UIScrollViewDelegate

extension LegacyHomeViewController: UIScrollViewDelegate {
    
    @objc
    private func reloadPullRefresh() {
        model.isLoaddingTimeLine = true
        HapticFeedback.impactFeedbackMedium()
        updateFeed()
        loadSuggestions()
        ConsumerManager.shared.loadConsumerBalance()
        headerView.refreshPinnedBanners()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= 0 {
            addFeedHeaderBellowSugestionsView()
            let contentOffsetY = scrollView.contentOffset.y
            headerView.frame = CGRect(x: 0, y: contentOffsetY, width: view.frame.width, height: headerHeight)
            feedHeader.frame = CGRect(x: 0, y: headerHeight + contentOffsetY, width: view.frame.width, height: feedFixedHeaderContainerHeight + feedFloatHeaderContainerHeight)
            feedView.frame = CGRect(x: 0, y: completeHeaderHeight - contentOffsetY, width: view.frame.width, height: view.frame.height + completeHeaderHeight)
            
        } else if scrollView.contentOffset.y >= headerHeight  {
            addFeedHeaderOnTop()
            if (scrollView.contentOffset.y > headerHeight + feedFloatHeaderContainerHeight) {
                feedView.frame.origin = CGPoint(x: 0, y: scrollView.contentOffset.y + feedFixedHeaderContainerHeight)
            } else {
                adjustFeedSize()
            }
            ajustFeedContentOffset(with: scrollView)
            
        } else if scrollView.contentOffset.y > 0 && scrollView.contentOffset.y <= headerHeight {
            addFeedHeaderBellowSugestionsView()
            adjustFeedSize()
        }
    }
    
    // Move feed header to below sugestions
    fileprivate func addFeedHeaderBellowSugestionsView() {
        if feedFixedHeaderContainer.superview != feedHeader {
            feedHeader.addSubview(feedFixedHeaderContainer)
            NSLayoutConstraint.activate([feedFixedHeaderContainer.topAnchor.constraint(equalTo: feedHeader.topAnchor),
                                         feedFixedHeaderContainer.leftAnchor.constraint(equalTo: feedHeader.leftAnchor),
                                         feedFixedHeaderContainer.rightAnchor.constraint(equalTo: feedHeader.rightAnchor),
                                         feedFixedHeaderContainer.heightAnchor.constraint(equalToConstant: feedFixedHeaderContainerHeight)])
        }
        fixedHeaderView.isHidden = true
    }
    
    // Move feed header to the top of the screen
    fileprivate func addFeedHeaderOnTop() {
        if feedFixedHeaderContainer.superview != fixedHeaderView {
            feedFixedHeaderContainer.removeFromSuperview()
            fixedHeaderView.addSubview(feedFixedHeaderContainer)
            
            NSLayoutConstraint.activate([feedFixedHeaderContainer.topAnchor.constraint(equalTo: fixedHeaderView.topAnchor),
                                         feedFixedHeaderContainer.leftAnchor.constraint(equalTo: fixedHeaderView.leftAnchor),
                                         feedFixedHeaderContainer.rightAnchor.constraint(equalTo: fixedHeaderView.rightAnchor),
                                         feedFixedHeaderContainer.bottomAnchor.constraint(equalTo: fixedHeaderView.bottomAnchor)])
        }
        fixedHeaderView.isHidden = false
    }
    // Adjust feed position and size based on the completeHeaderHeight value
    fileprivate func adjustFeedSize() {
        guard let tableNodeAll = feedAllViewController?.tableNode else {
            return
        }
        guard let tableNodePrivate = feedPrivateViewController?.tableNode else {
            return
        }
        tableNodeAll.contentOffset.y = 0
        tableNodePrivate.contentOffset.y = 0
        feedView.frame.origin = CGPoint(x: 0, y: completeHeaderHeight)
    }
    
    // Controll feed view position
    func ajustFeedContentOffset(with scrollView: UIScrollView) {
        guard let tabHeight = tabBarController?.tabBar.frame.height else {
            return
        }
        let feedViewController = activeVisibility == .Friends ? feedAllViewController : feedPrivateViewController
        guard let tableNode = feedViewController?.tableNode else {
            return
        }
        let defaultContentHeight = view.frame.height + completeHeaderHeight + tabHeight
        if tableNode.view.contentSize.height < defaultContentHeight {
            tableNode.view.contentSize.height = defaultContentHeight
        }
        scrollView.contentSize.height = tableNode.view.contentSize.height + completeHeaderHeight
        tableNode.contentOffset.y = scrollView.contentOffset.y - headerHeight
    }
}

// MARK: - FeedViewControllerDelegate

extension LegacyHomeViewController: FeedViewControllerDelegate {
    func feedDidLoadNewPage() {
    }
    
    func feedScrollViewDidScroll(_ scrollView: UIScrollView) {
    }
    
    func feedWillReload() {
    }
    
    func feedDidReload() {
        customRefreshControl.endRefreshing()
        
        //Ainda não foi exibido nenhum item do feed público
        if !self.hasAtLeastOneFeedOnce && (self.feedAllViewController?.model?.isLoading == false) {
            //Controla para que o seletor só esteja visível quando houver algum item no feed
            let newAlpha:CGFloat
            if let count = self.feedAllViewController?.model?.items.count, count > 0 {
                newAlpha = 1
                AppParameters.global().setHasAtLeastOneFeedOnce(true)
                self.hasAtLeastOneFeedOnce = true
            } else {
                newAlpha = 0
            }
            
            UIView.animate(withDuration: 0.25) {
                self.feedVisibilitySelector.toolbarView.alpha = newAlpha
            }
        }
        model.isLoaddingTimeLine = false
    }
}

// MARK: - UIPageViewControllerDataSource

extension LegacyHomeViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let feedVC = feedAllViewController else { return nil}
        guard let vc = viewController as? FeedViewController else {
            return nil
        }
        guard let feedViewModel = vc.model else {
            return nil
        }
        if feedViewModel.visibility == .personal {
            activeVisibility = .Private
            return feedVC
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let privateFeedVC = feedPrivateViewController else {
            return nil
        }
        guard let vc = viewController as? FeedViewController else {
            return nil
        }
        guard let feedViewModel = vc.model else {
            return nil
        }
        if feedViewModel.visibility == .followers {
            activeVisibility = .Friends
            return privateFeedVC
        }
        return nil
    }
}

// MARK: - UIPageViewControllerDelegate

extension LegacyHomeViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let vc = previousViewControllers.first as? FeedViewController else {
            return
        }
        guard let feedViewModel = vc.model else {
            return
        }
        if completed {
            if feedViewModel.visibility == .personal {
                activeVisibility = .Friends
            } else {
                activeVisibility = .Private
            }
        }
    }
}

// MARK: - RecentsCollectionViewDelegate

extension LegacyHomeViewController: RecentsCollectionViewDelegate {
    func didSelectCell(at indexPath: IndexPath) {
        itemSelected = indexPath.item
    }
    
    func didSelectRow(section: LegacySearchResultSection, row: LegacySearchResultRow){
        if case .person = row.type {
            Analytics.shared.log(ProfileEvent.touchPicture(from: .suggestions))
        }
        SearchHelper.openPayment(withSection: section, row: row, from: self, origin: .homeSugestions)
        
        guard let currentItem = itemSelected else {
            return
        }
        
        Analytics.shared.log(
            PaymentEvent.itemAccessed(
                type: row.type.rawValue,
                id: row.basicData.id ?? String(),
                name: row.basicData.title ?? String(),
                section: (name: section.title ?? String(), position: currentItem)
            )
        )
    }
}

// MARK: - FavoritesCollectionViewDelegate
extension LegacyHomeViewController: FavoriteCarouselCollectionViewDelegate {
    func didSelect(item: HomeFavorites.Item) {
        switch item {
        case let .action(value):
            didSelect(item: value)
        case let .consumer(value):
            didSelect(item: value)
        case let .digitalGood(value):
            didSelect(item: value)
        case let .store(value):
            didSelect(item: value)
        default:
            break
        }
    }

    private func didSelect(item: HomeFavorites.ActionItem) {
        guard let navController = self.navigationController else {
            return
        }

        let coordinator = FavoriteFlowCoordinator(navigationController: navController)
        favoritesFlowCoordinator = coordinator

        switch item.action {
        case .favoriteOnboarding:
            Analytics.shared.log(FavoritesEvent.favoritesOnboardingAccessed(.home))
            coordinator.goToOnboarding()
        case .allFavorites:
            Analytics.shared.log(FavoritesEvent.favoritesListAccessed(.home))
            coordinator.goToFavoritesList()
        default:
            break
        }
    }

    private func didSelect(item: HomeFavorites.ConsumerItem) {
        guard let controller = NewTransactionViewController.fromStoryboard() else {
            return
        }

        Analytics.shared.log(ProfileEvent.touchPicture(from: .favorites))
        Analytics.shared.log(CheckoutEvent.checkoutScreenViewed(CheckoutEvent.CheckoutEventOrigin.p2p))
        controller.touchOrigin = title
        controller.showCancel = true
        controller.loadConsumerInfo(item.id)
        present(PPNavigationController(rootViewController: controller), animated: true, completion: nil)
    }

    private func didSelect(item: HomeFavorites.DigitalGoodItem) {
        let digitalGood = dgItem(from: item)
        DGHelpers.openDigitalGoodFlow(for: digitalGood, viewController: self, origin: nil)
    }

    private func didSelect(item: HomeFavorites.StoreItem) {
        guard item.store.isParking != isNewParking else {
            return openNewParkingPayment(item: item)
        }

        guard
            let store = PPStore(profileDictionary: storeDictionary(from: item)),
            let controller = ViewsManager.paymentStoryboardFirstViewController() as? PaymentViewController else {
                return
        }

        store.isParkingPayment = Int32(item.store.isParking)
        Analytics.shared.log(CheckoutEvent.checkoutScreenViewed(store.isParkingPayment == 1 ?
                                                                    CheckoutEvent.CheckoutEventOrigin.parking :
                                                                    CheckoutEvent.CheckoutEventOrigin.p2m)
        )
        controller.store = store
        controller.touchOrigin = title
        present(PPNavigationController(rootViewController: controller), animated: true, completion: nil)
    }

    private func openNewParkingPayment(item: HomeFavorites.StoreItem) {
        let parkingViewController = ParkingCoordinator().start(sellerId: item.seller.id, storeId: item.store.id)
        Analytics.shared.log(CheckoutEvent.checkoutScreenViewed(CheckoutEvent.CheckoutEventOrigin.parking))
        present(parkingViewController, animated: true, completion: nil)
    }

    private func storeDictionary(from item: HomeFavorites.StoreItem) -> [String: Any] {
        [
            "id": item.store.id,
            "seller_id": item.seller.id,
            "name": item.store.name,
            "address": item.store.address,
            "img_url": item.store.imageUrl?.absoluteString ?? "",
            "is_parking": "\(item.store.isParking)"
        ]
    }

    private func dgItem(from item: HomeFavorites.DigitalGoodItem) -> DGItem {
        let digitalGood = DGItem(id: item.id, service: DGService(rawValue: item.service))
        digitalGood.name = item.name
        digitalGood.itemDescription = item.description
        digitalGood.sellerId = "\(item.sellerId)"
        digitalGood.displayType = DGDisplayType(rawValue: item.selectionType) ?? .grid
        digitalGood.imgUrl = item.imageUrl?.absoluteString
        digitalGood.infoUrl = item.infoUrl?.absoluteString
        digitalGood.longDescriptionMarkdown = item.descriptionLargeMarkdown
        digitalGood.bannerImgUrl = item.bannerImageUrl?.absoluteString
        digitalGood.isStudentAccount = item.studentAccount
        digitalGood.screenId = item.screenId ?? ""

        return digitalGood
    }
}

// MARK: - LegacyHomeFeedHeaderDelegate

extension LegacyHomeViewController: LegacyHomeFeedHeaderDelegate {
    func feedHeaderDidChangeVisibility(visibility: FeedItemVisibility) {
        selectVisibility(visibility)
        Analytics.shared.log(HomeEvent.tabActivitiesSelected(type: visibility == .Friends ? .all : .me))
    }
}

// MARK: - LegacyHomeNavigationBarDelegate

extension LegacyHomeViewController: LegacyHomeNavigationBarDelegate {
    func homeNavigationBarDidTapSettings() {
        openSettings()
    }
    
    func homeNavigationBarDidTapScanner(_ navigationBar: LegacyHomeNavigationBar) {
        openScanner()
    }
    
    func homeNavigationBarDidTapInvite(_ navigationBar: LegacyHomeNavigationBar) {
        openInvite()
    }

    func homeNavigationBarDidTapAtPromotions() {
        openPromotions()
    }
    
    func homeNavigationBarDidTapMessagesButton(messagesStatus: TrackedMessagesStatus) {
        connectionStateInteractor.setConnectionActions()
        dependencies.analytics.log(.button(.clicked, .openChats(status: messagesStatus)))
    }
    
    func homeNavigationBarDidTapBalance() {
        dependencies.analytics.log(ExperimentEvent.walletAccessedByBalance)
        AppManager.shared.mainScreenCoordinator?.showWalletScreen()
    }
}

// MARK: - UIPPProfileImageDelegate

extension LegacyHomeViewController: UIPPProfileImageDelegate {
}

// MARK: - HomeHeaderViewDelegate
extension LegacyHomeViewController: HomeHeaderViewDelegate {
    func reloadFavoritesCollectionData() {
        loadFavorites()
    }
    
    func didSelectedTab(_ tab: HomeHeaderViewTab) {
        switch tab {
        case .suggestions:
            if headerView.suggestionCollectionView.result == nil {
                loadSuggestions()
            }
        case .favorites:
            guard headerView.favoriteCarouselCollectionView.items.isEmpty else {
                return
            }
            loadFavorites()
        }
    }
    
    func openUnifiedPromotions() {
        KVStore().setFirstTimeOnlyEvent(PromotionsUserDefaultKey.alreadyOpened.rawValue)
        openPromotions()
    }
}

// MARK: - MGM Banner
extension LegacyHomeViewController {
    @objc
    func bannerMgmCodeOpenPromoCode() {
        let viewController = PromoCodeViewController(from: .home)
        viewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc
    func bannerMgmCodeOpenPopup() {
        let okButton = Button(title: DefaultLocalizable.btOkUnderstood.text, type: .cta){ [weak self] popupController, _ in
            popupController.dismiss(animated: true, completion: {
                self?.dismissMgmBanner()
            })
        }
        let codeButton = Button(title: HomeLegacyLocalizable.iHaveCode.text, type: .clean) { [weak self] popupController, _ in
            popupController.dismiss(animated: true, completion: {
                self?.bannerMgmCodeOpenPromoCode()
            })
        }
        let alert = Alert(with: HomeLegacyLocalizable.promotionalCodes.text, text: HomeLegacyLocalizable.whenYouNeed.text, buttons: [okButton, codeButton])
        
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func showMgmBanner() {
        if self.bannerMGMView.superview == nil {
            let margin: CGFloat = 4
            feedFloatHeaderContainer.addSubview(self.bannerMGMView)
            NSLayoutConstraint.activate([bannerMGMView.bottomAnchor.constraint(equalTo: feedFloatHeaderContainer.bottomAnchor, constant: -margin),
                                         bannerMGMView.topAnchor.constraint(equalTo: feedFloatHeaderContainer.topAnchor, constant: margin),
                                         bannerMGMView.heightAnchor.constraint(equalToConstant: bannerMGMCodeViewHeight),
                                         bannerMGMView.leftAnchor.constraint(equalTo: feedFloatHeaderContainer.leftAnchor, constant: 8),
                                         bannerMGMView.rightAnchor.constraint(equalTo: feedFloatHeaderContainer.rightAnchor, constant: -8)])
            feedFloatHeaderContainer.setNeedsLayout()
            feedFloatHeaderContainer.layoutIfNeeded()
            bannerMGMCodeViewHeight = bannerMGMView.frame.size.height + (2 * margin)
            feedFloatHeaderContainerHeight += bannerMGMCodeViewHeight
            
            UIView.animate(withDuration: 0.25, animations: {
                let newHeight:CGFloat = self.feedHeader.frame.height + self.bannerMGMCodeViewHeight
                self.bannerMGMView.alpha = 1
                self.feedHeader.layoutIfNeeded()
                self.feedView.frame = CGRect(x: 0, y: self.completeHeaderHeight, width: self.view.frame.width, height: self.view.frame.height)
                self.feedHeader.frame = CGRect(origin: self.feedHeader.frame.origin, size: CGSize(width: self.feedHeader.frame.width, height: newHeight))
            })
        }
    }
    
    func dismissMgmBanner() {
        if self.bannerMGMView.superview != nil {
            AppParameters.global().referalCodeBannerDismissDate = nil
            self.feedFloatHeaderContainerHeight -= self.bannerMGMCodeViewHeight
            UIView.animate(withDuration: 0.25, animations: {
                let newHeight:CGFloat = self.feedHeader.frame.height + self.bannerMGMCodeViewHeight
                self.bannerMGMView.alpha = 0
                self.feedFloatHeaderContainer.layoutIfNeeded()
                self.feedView.frame = CGRect(x: 0, y: self.completeHeaderHeight, width: self.view.frame.width, height: self.view.frame.height)
                self.feedHeader.frame = CGRect(origin: self.feedHeader.frame.origin, size: CGSize(width: self.feedHeader.frame.width, height: newHeight))
            }) { (_) in
                self.bannerMGMView.removeFromSuperview()
            }
        }
    }
}

extension LegacyHomeViewController: CreditCardOnboardingPopUpDelegate{
    func creditCardOnboardingPopUpAddCreditCard() {
    }
    
    func creditCardOnboardingPopUpDidAddCreditCard() {
    }
}

extension LegacyHomeViewController {
    func findFriends() {
        let permissionContact: Permission = PPPermission.contacts()
        if ( permissionContact.status != PermissionStatus.authorized ){
            PPPermission.requestContacts { [weak self] (status) in
                if status == .authorized {
                    self?.showFollowSuggestions()
                }
            }
        } else {
            self.showFollowSuggestions()
        }
    }
    
    func showFollowSuggestions() {
        trackEvent(true)
        guard let controller = FollowFriendsFactory.make(from: .home) else {
            return
        }
        present(UINavigationController(rootViewController: controller), animated: true)
    }
    
    func trackEvent(_ accept: Bool) {
        let properties = ["Procurou amigos" : accept ? "Sim" : "Não"]
        PPAnalytics.trackEvent("Onboarding social - Exibiu invite de seguir", properties: properties)
    }
}

extension LegacyHomeViewController {
    private func checkStudentStatus() {
        model.loadStudentAccountStatus { [weak self] showPopup in
            if showPopup {
                self?.showStudentActivePopup()
            }
        }
    }
    
    private func showStudentActivePopup() {
        let title = StudentLocalizable.activePopupTitle.text
        let message = String(format: StudentLocalizable.activePopupDescription.text, StudentLocalizable.activePopupBenefitsCashback.text)
        
        let range = (message as NSString).range(of: StudentLocalizable.activePopupBenefitsCashback.text)
        
        let benefitsAttributes = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .bold),
            NSAttributedString.Key.foregroundColor: Palette.ppColorBranding300.color
        ]
        let regularAttributes = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .regular),
            NSAttributedString.Key.foregroundColor: Palette.ppColorGrayscale500.color
        ]
        
        let attributedString = NSMutableAttributedString(string: message, attributes: regularAttributes)
        attributedString.addAttributes(benefitsAttributes, range: range)
        
        let image = #imageLiteral(resourceName: "icon-universitario")
        let popup = PopupViewController(title: title, description: message, preferredType: .image(image))
        Analytics.shared.log(StudentAccountEvent.activatedAccountPopup)
        popup.setAttributeDescription(attributedString)
    
        let confirmAction = PopupAction(title: StudentLocalizable.activePopupButton.text, style: .fill) { [weak self] in
            self?.openStudentBenefits()
            Analytics.shared.log(StudentAccountEvent.benefits(from: .popup))
            popup.dismiss(animated: true)
        }
        
        let cancelAction = PopupAction(title: DefaultLocalizable.notNow.text, style: .default) {
            Analytics.shared.log(StudentAccountEvent.activatedAccountPopupDismiss)
            popup.dismiss(animated: true)
        }
        
        popup.didCloseDismiss = {
            Analytics.shared.log(StudentAccountEvent.activatedAccountPopupClose)
        }
        
        popup.addAction(confirmAction)
        popup.addAction(cancelAction)
        showPopup(popup)
    }
    
    private func openStudentBenefits() {
        let urlString = dependencies.featureManager.text(.universityAccountBenefitsWebview)
        guard let url = URL(string: urlString) else {
            return
        }
        let webViewController = WebViewFactory.make(with: url)
        webViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(webViewController, animated: true)
        navigationController?.isNavigationBarHidden = false
    }
}

private extension LegacyHomeViewController {
    func displayPixPopupOnFirstTimeHomeVisualized() {
        guard model.shouldDisplayPixKeyRegistrationPopup else {
                return
        }
        
        let modal = ModalRegistrationFactory.make(delegate: self)
        showPopup(modal)
    }
    
    @objc
    func shouldOpenPrivacyReview() {
        displayReviewPrivacyFlow(cameFromReceipt: true)
    }
    
    func displayReviewPrivacyFlow(cameFromReceipt: Bool = false) {
        model.checkReviewPrivacy(cameFromReceipt: cameFromReceipt) { [weak self] shouldReview in
            guard shouldReview else { return }            
            let controller = UpdatePrivacyMessageFactory.make(origin: .home)
            self?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

extension LegacyHomeViewController: ModalRegistrationDelegate {
    func didTapRegisterKeyOnPix() {
        PIXConsumerFlowCoordinator(originViewController: self, originFlow: .deeplink, destination: .keyManagement).start()
    }
}

fileprivate extension UIRefreshControl {
    func programaticallyBeginRefreshing(in scrollVuew: UIScrollView) {
        beginRefreshing()
        let offsetPoint = CGPoint.init(x: 0, y: -frame.size.height)
        scrollVuew.setContentOffset(offsetPoint, animated: true)
    }
}

fileprivate extension LegacyHomeViewController {
    func addChatUnreadCountObserver() {
        guard dependencies.featureManager.isActive(.directMessageSBEnabled) else { return }
        NotificationCenter.default.addObserver(
            forName: ChatNotifier.NotificationName.didUpdateUnreadMessagesCount.notificationName,
            object: nil,
            queue: .main
        ) { [weak self] notification in
            self?.updateChatUnreadCount(with: notification)
        }
    }
    
    func updateChatUnreadCount(with notification: Notification) {
        guard let unreadCount = notification.userInfo?[ChatNotifier.NotificationUserInfoKey.totalCount] as? Int
        else { return }
        
        headerView.navigationBar.changeUnreadMessagesCount(to: unreadCount)
    }
}

extension LegacyHomeViewController: ConnectionStateSuccessDelegate {
    func didSucceedConnection() {
        openMessages()
    }
}

extension LegacyHomeViewController: ApolloLoadable, ConnectionStateDisplaying, ConnectionStateSceneStandardFlow {}
