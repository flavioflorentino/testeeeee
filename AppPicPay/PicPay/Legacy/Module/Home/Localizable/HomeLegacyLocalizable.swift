enum HomeLegacyLocalizable: String, Localizable {
    case authorizeContacts
    case findYourFriends
    case toFindOut
    case all
    case you
    case iHaveCode
    case promotionalCodes
    case whenYouNeed
    case doYouHaveReferralCode
    case myBalance
    case newNavigationTitlePrefix
    
    var key: String {
        return self.rawValue
    }
    
    var file: LocalizableFile {
        return .homeLegacy
    }
}
