import UIKit

final class HistoricFeedDetailViewController: FeedDetailViewController {
    override func configureNavigationBar(){
        self.navigationItem.title = SubscribeLocalizable.transaction.text
    }
    
    override func feedDidTapOnProfile(_ contact: PPContact?) {
        // Evitar navegação
    }
}
