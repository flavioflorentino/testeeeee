import UI
import UIKit
import AnalyticsModule

fileprivate final class ProducerLoadingTableViewCell: UITableViewCell {
    var loader:UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView(style: .gray)
        loader.hidesWhenStopped = true
        loader.translatesAutoresizingMaskIntoConstraints = false
        return loader
    }()
    
    var isAnimating:Bool = false {
        didSet {
            if isAnimating {
                self.loader.startAnimating()
            } else {
                self.loader.stopAnimating()
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.initialize()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        //Remove separator line
        let inset = self.frame.width / 2
        separatorInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
    }
    
    internal func initialize() {
        self.buildView()
    }
    
    internal func buildView() {
        self.addSubview(self.loader)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: loader, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: loader, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        ])
    }
}

final class ProducerPlansViewController: PPBaseViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, ExpandableTableViewCellDelegate {
    lazy var headerView:ProducerProfileHeaderView = {
        let header = ProducerProfileHeaderView()
        header.translatesAutoresizingMaskIntoConstraints = false
        return header
    }()
    
    lazy var footerView:UIView = { [unowned self] in
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale600.color
        view.alpha = 0
        
        let barHeight:CGFloat = 52
        let buttonHeight:CGFloat = 38
        let buttonVMargin:CGFloat = (barHeight - buttonHeight) / 2
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(ProducerPlansViewController.nextButtonHandler), for: .touchUpInside)
        button.setTitle(DefaultLocalizable.next.text, for: .normal)
        button.backgroundColor = Palette.ppColorGrayscale600.color
        button.setTitleColor(Palette.ppColorGrayscale000.color, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = buttonHeight / 2
        
        let subscriptionLabel = UILabel()
        subscriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        subscriptionLabel.text = SubscribeLocalizable.selectedPlan.text
        subscriptionLabel.textColor = Palette.ppColorGrayscale300.color
        subscriptionLabel.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.medium)
        
        let valueLabel = UILabel()
        valueLabel.translatesAutoresizingMaskIntoConstraints = false
        valueLabel.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        valueLabel.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .horizontal)
        valueLabel.text = "R$ 0"
        valueLabel.tag = ProducerPlansViewController.valueLabelFooterViewTag
        valueLabel.textColor = Palette.ppColorGrayscale300.color
        
        let periodLabel = UILabel()
        periodLabel.translatesAutoresizingMaskIntoConstraints = false
        periodLabel.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        periodLabel.text = SubscribeLocalizable.perMonth.text
        periodLabel.tag = ProducerPlansViewController.periodLabelFooterViewTag
        periodLabel.textColor = Palette.ppColorGrayscale300.color
        
        view.addSubview(button)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: button, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: buttonVMargin),
            NSLayoutConstraint(item: button, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -buttonVMargin),
            NSLayoutConstraint(item: button, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 110),
            NSLayoutConstraint(item: button, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: -12),
            NSLayoutConstraint(item: button, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: buttonHeight)
            ])
        
        view.addSubview(subscriptionLabel)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: subscriptionLabel, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 9),
            NSLayoutConstraint(item: subscriptionLabel, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 16),
            NSLayoutConstraint(item: subscriptionLabel, attribute: .right, relatedBy: .equal, toItem: button, attribute: .left, multiplier: 1, constant: -8),
        ])
        
        view.addSubview(valueLabel)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: valueLabel, attribute: .top, relatedBy: .equal, toItem: subscriptionLabel, attribute: .bottom, multiplier: 1, constant: 2),
            NSLayoutConstraint(item: valueLabel, attribute: .left, relatedBy: .equal, toItem: subscriptionLabel, attribute: .left, multiplier: 1, constant: 0),
        ])
        
        view.addSubview(periodLabel)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: periodLabel, attribute: .firstBaseline, relatedBy: .equal, toItem: valueLabel, attribute: .firstBaseline, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: periodLabel, attribute: .left, relatedBy: .equal, toItem: valueLabel, attribute: .right, multiplier: 1, constant: 2),
            NSLayoutConstraint(item: periodLabel, attribute: .right, relatedBy: .equal, toItem: subscriptionLabel, attribute: .right, multiplier: 1, constant: 0)
        ])
        
        return view
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = Palette.ppColorGrayscale000.color
        tableView.showsVerticalScrollIndicator = false
        tableView.alwaysBounceVertical = false
        tableView.bounces = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 100
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
    lazy var headerSectionView: UIView = {
        let headerSectionTitle:String = SubscribeLocalizable.chooseYourSubscription.text
        let view = UIView()
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        label.text = headerSectionTitle
        label.numberOfLines = 0
        view.addSubview(label)
        
        let button = UIButton()
        let buttonHeight:CGFloat = 28
        button.tag = ProducerPlansViewController.seeMoreButtonHeaderSectionViewTag
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(ProducerPlansViewController.seeMoreAboutProducer), for: .touchUpInside)
        button.setTitle(DefaultLocalizable.learnMore.text, for: .normal)
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        button.layer.borderColor = Palette.ppColorBranding300.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = buttonHeight / 2
        button.layer.masksToBounds = true
        view.addSubview(button)
        
        let cardsStackView = UIStackView()
        cardsStackView.tag = ProducerPlansViewController.cardsStackHeaderSectionViewTag
        cardsStackView.distribution = .equalSpacing
        cardsStackView.alignment = .top
        cardsStackView.axis = .vertical
        cardsStackView.translatesAutoresizingMaskIntoConstraints = false
        cardsStackView.spacing = 12
        view.addSubview(cardsStackView)
        
        // Cards
        NSLayoutConstraint.activate([
            cardsStackView.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
            cardsStackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 12),
            cardsStackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12),
            cardsStackView.bottomAnchor.constraint(equalTo: label.topAnchor, constant: 0),
        ])
        
        // Label
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 16),
            NSLayoutConstraint(item: label, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -20),
        ])
        
        // Button
        NSLayoutConstraint.activate([
            button.widthAnchor.constraint(equalToConstant: 94),
            button.heightAnchor.constraint(equalToConstant: buttonHeight),
            button.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
            button.leftAnchor.constraint(equalTo: label.rightAnchor, constant: 50),
            button.centerYAnchor.constraint(equalTo: label.centerYAnchor)
        ])
        
        return view
    }()
    
    private static var valueLabelFooterViewTag = 100
    private static var periodLabelFooterViewTag = 101
    private static var seeMoreButtonHeaderSectionViewTag = 102
    private static var cardsStackHeaderSectionViewTag = 103
    private var sizeSafeAreaBotton: CGFloat {
        get {
            if #available(iOS 11.0, *) {
                return self.view.safeAreaInsets.bottom
            } else {
                return 0
            }
        }
    }
    private lazy var addressListViewModel: AddressListViewModel = {
        return AddressListViewModel()
    }()
    
    private var address:(
        isLoading: Bool,
        didFinishLoad:((Error?) -> Void)?
        ) = (false, nil)
    
    private var askNewAddressReasonText: String = SubscribeLocalizable.subscriptionYouChoseNeeds.text
    
    internal lazy var headerViewSize:CGFloat = { [unowned self] in
        var size:CGFloat = 180
        if #available(iOS 11.0, *) {
            size += UIApplication.shared.statusBarFrame.height + self.view.safeAreaInsets.top
        } else {
            size += UIApplication.shared.statusBarFrame.height
        }
        return size
    }()
    
    internal var tableViewBottomConstraint:NSLayoutConstraint? = nil
    internal var dataSource:[SubscriptionDataCell] = []
    internal var lastItem:SubscriptionDataCell? = nil
    internal var currentItem:SubscriptionDataCell? = nil

    public var isLoading:Bool = false
    public var onClose:(() -> Void)? = nil
    public var isProducerProfileLoaded:Bool = true
    
    private var origin: SubscriptionProcucerPlanOrigin
    var model: ProducerCategoriesViewModel {
        didSet {
            self.headerView.fill(with: self.model.producer)
            self.showSeeMoreButton(self.model.producer.fullProfileDescriptionMarkdown != nil)
            self.addCardsIfNeeded()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    //MARK: UIViewController
    
    init(model: ProducerCategoriesViewModel, origin: SubscriptionProcucerPlanOrigin) {
        self.model = model
        self.origin = origin
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        self.model = ProducerCategoriesViewModel(producer: ProducerProfileItem())
        self.origin = .deeplink
        super.init(coder: aDecoder)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.shared.log(SubscriptionEvent.plansViewed(from: origin))
        view.backgroundColor = Palette.ppColorGrayscale000.color
        buildView()
        if isProducerProfileLoaded {
            self.headerView.fill(with: self.model.producer)
            self.showSeeMoreButton(self.model.producer.fullProfileDescriptionMarkdown != nil)
        } else {
            let producerModel = ProducerViewModel(producerId: self.model.producer.id)
            producerModel.loadProducerProfile({ [weak self] error in
                guard error == nil else {
                    return
                }
                self?.model.producer.update(using: producerModel.producer)
                self?.headerView.fill(with: producerModel.producer)
                self?.showSeeMoreButton(producerModel.producer.fullProfileDescriptionMarkdown != nil)
            })
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ProducerPlanTableViewCell.self, forCellReuseIdentifier: "subscriptionCell")
        tableView.register(ProducerLoadingTableViewCell.self, forCellReuseIdentifier: "loadingCell")
        tableView.tableHeaderView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 1, height: self.headerViewSize)))
        
        loadData()
        
        headerView.closeButtonAction = { [weak self] button in
            self?.dismissViewControllerHandler(sender: button)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        if let navigation = self.navigationController, navigation.viewControllers.first !== self {
            self.headerView.showBackButton = true
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showFooterView(footerView.alpha == 1, animated: false)
    }
    
    //MARK: SubscriptionViewController
    
    private func showSeeMoreButton(_ show:Bool = true) {
        if let button = self.headerSectionView.viewWithTag(ProducerPlansViewController.seeMoreButtonHeaderSectionViewTag) {
            button.isHidden = !show
        }
    }
    
    internal func setFooterViewValue(_ newValue:Double?) {
        if let label = self.view.viewWithTag(ProducerPlansViewController.valueLabelFooterViewTag) as? UILabel {
            let formatter = NumberFormatter()
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 2
            let number = formatter.string(from: NSNumber(value: newValue ?? 0)) ?? "0,00"
            label.text = "R$\(number)".replacingOccurrences(of: ".", with: ",")
        }
    }
    
    internal func setFooterViewPeriod(_ newPeriod:String?) {
        if let label = self.view.viewWithTag(ProducerPlansViewController.periodLabelFooterViewTag) as? UILabel {
            if let text = newPeriod {
                label.text = text.isEmpty ? "" : "/ \(text)"
            } else {
                label.text = ""
            }
        }
    }
    
    internal func buildView() {
        
        if #available(iOS 13.0, *) {
            headerView.topInset = 4
        } else {
            if #available(iOS 11.0, *) {
                headerView.topInset = UIApplication.shared.statusBarFrame.height + view.safeAreaInsets.top
            } else {
                headerView.topInset = UIApplication.shared.statusBarFrame.height
            }
        }
        
        let tableBottomConstraint = tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        tableBottomConstraint.priority = UILayoutPriority(100)
        headerView.minHeight = 44 + headerView.topInset
        tableViewBottomConstraint = tableBottomConstraint
        
        view.addSubview(tableView)
        view.addSubview(footerView)
        view.addSubview(headerView)
        
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [tableView, headerView, footerView])
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableBottomConstraint,
            headerView.topAnchor.constraint(equalTo: view.topAnchor),
            headerView.heightAnchor.constraint(equalToConstant: headerViewSize),
            footerView.topAnchor.constraint(equalTo: tableView.bottomAnchor)
        ])
        
    }
    
    private func goAddressListScreen() {
        if let option = self.currentItem {
            //Cria a viewController com a lista de endereços
            addressListViewModel.displayContextHeader = true
            let addressListVC = AddressListViewController(viewModel: addressListViewModel)
            addressListVC.addressContextTitle = DefaultLocalizable.deliveryAddress.text
            addressListVC.addressContextText = SubscribeLocalizable.enterAddress.text
            
            addressListVC.onAddressSelected = { [weak self] address in
                guard let strongSelf = self else {
                    return
                }
                Analytics.shared.log(SubscriptionEvent.addressSelected)
                strongSelf.goPaymentScreen(subscription: option.subscription, producer: strongSelf.model.producer, address: address)
            }
            
            if address.isLoading {
                // Evita que a tela addressListVC realize uma nova chamada e atribui o status isLoading.
                addressListViewModel.isLoading = true
                address.didFinishLoad = { [weak self] error in
                    // Atualiza a tela addressListVC ao receber resposta do servidor
                    self?.addressListViewModel.isLoading = false
                    addressListVC.updateTableHeader(withError: error)
                }
            }
            
            if addressListViewModel.isAddressListLoaded() && addressListViewModel.getAddressListSize() == 0 {
                // A lista de endereços já foi carregada.
                // O consumer não tem endereço cadastrado.
                // Exibe o popup solicitando o cadastro do endereço!
                let askNewAddress = AskNewAddressViewController()
                askNewAddress.text = askNewAddressReasonText
                
                askNewAddress.confirmButtonAction = {  [weak self] _ in
                    let addNewAddress = AddNewAddressViewController()
                    addNewAddress.onAddressSaved = { newAddress in //TODO: Salvar novo endereco
                        guard let strongSelf = self else {
                            return
                        }
                        // Adiciona na stack de navegação a lista de endereços já com o modelo com os endereços
                        strongSelf.navigationController?.viewControllers.append(addressListVC)
                        strongSelf.goPaymentScreen(subscription: option.subscription, producer: strongSelf.model.producer, address: newAddress)
                    }
                    self?.navigationController?.present(addNewAddress, animated: true, completion: nil)
                }
                showPopup(askNewAddress)
            } else {
                // O consumer tem endereço cadastrado, ou não foram carregados, exibe a tela de seleção de endereço
                navigationController?.pushViewController(addressListVC, animated: true)
            }
        }
    }
    
    private func loadAddresses() {
        if self.address.isLoading {
            return
        }
        address.isLoading = true
        addressListViewModel.getAddressList { [weak self] error in
            DispatchQueue.main.async {
                self?.address.isLoading = false
                self?.address.didFinishLoad?(error)
                self?.address.didFinishLoad = nil
            }
        }
    }
    
    internal func showFooterView(_ show:Bool, animated:Bool = true) {
        let newValues:(color: UIColor?, alpha: CGFloat, bottomConstant: CGFloat)
        if show {
            newValues = (color: footerView.backgroundColor, alpha: CGFloat(1), bottomConstant: -(footerView.bounds.height))
        } else {
            newValues = (color: tableView.backgroundColor,  alpha: CGFloat(0), bottomConstant: 0)
        }
        
        if self.tableViewBottomConstraint?.constant != newValues.bottomConstant {
            let applyChanges:((UIColor?, CGFloat, CGFloat)) -> Void = { [weak self] arg in
                let (color, alpha, _) = arg
                self?.scrollToContent()
                self?.footerView.alpha = alpha
                self?.paintSafeAreaBottomInset(withColor: color)
            }
            
            tableViewBottomConstraint?.constant = newValues.bottomConstant -  sizeSafeAreaBotton
            if animated {
                UIView.animate(withDuration: 0.25, animations: { [weak self] in
                    self?.view.layoutIfNeeded()
                    applyChanges(newValues)
                })
            } else {
                applyChanges(newValues)
            }
        }
    }
    
    internal func showErrorAlert(with message:String? = nil) {
        let fallbackMessage = SubscribeLocalizable.notPossibleDisplay.text
        let alertMessage:String
        if let str = message {
            alertMessage = str.isEmpty ? fallbackMessage : str
        } else {
            alertMessage = fallbackMessage
        }
        
        let alert = UIAlertController(
            title: "Ops",
            message: alertMessage,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(
            title: "OK",
            style: .default,
            handler: self.dismissViewControllerHandler
        ))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func loadData() {
        self.startLoading()
        self.model.getCategoriesPlans { [weak self] (error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.stopLoading()
            if let error = error {
                self?.showErrorAlert(with: error.localizedDescription)
            } else if let category = strongSelf.model.categories.first {
                var needLoadAddresses = false
                strongSelf.addCardsIfNeeded()
                
                for plan in category.plans {
                    if plan.isAddressNeeded {
                        needLoadAddresses = true
                    }
                    let data = SubscriptionDataCell(subscription: plan)
                    // Seleciona o item previamente indicado
                    if plan.id == strongSelf.currentItem?.subscription.id {
                        data.isSelected = true
                        strongSelf.currentItem = nil
                        strongSelf.itemSelectedHandler(data)
                    }
                    strongSelf.dataSource.append(data)
                }
                if needLoadAddresses {
                    self?.loadAddresses()
                }
                strongSelf.tableView.reloadData()
            } else {
                self?.showErrorAlert()
            }
        }
    }
    
    func scrollToContent(animated:Bool = false) {
        if self.tableView.contentSize.height - 40 > self.tableView.bounds.height {
            let maxOffset = self.tableView.contentSize.height - self.tableView.bounds.height - 40
            if animated {
                UIView.animate(withDuration: 0.25, animations: {
                    self.tableView.contentOffset.y = min(self.tableView.contentOffset.y, maxOffset)
                })
            } else {
                self.tableView.contentOffset.y = min(self.tableView.contentOffset.y, maxOffset)
            }
        }
    }
    
    func addCardsIfNeeded() {
        guard let stackView = self.headerSectionView.viewWithTag(ProducerPlansViewController.cardsStackHeaderSectionViewTag) as? UIStackView else {
            return
        }
        let bottomConstraint = self.headerSectionView.constraints.first(where: {$0.firstItem === stackView && $0.firstAttribute == .bottom})
        if let category = self.model.categories.first, !category.cards.isEmpty {
            bottomConstraint?.constant = -30
            for card in category.cards {
                let cardWidgetStackView = WidgetsVerticalStackView(frame: .zero, model: card)
                cardWidgetStackView.translatesAutoresizingMaskIntoConstraints = false
                stackView.addArrangedSubview(cardWidgetStackView)
                cardWidgetStackView.widthAnchor.constraint(equalTo: stackView.widthAnchor).isActive = true
                cardWidgetStackView.didTapOnButton = { [weak self] (uiButton, button, widgetStack) in
                    self?.widgetStackButtonActionHandler(uiButton: uiButton, buttonModel: button, stackModel: widgetStack)
                }
            }
        } else {
            bottomConstraint?.constant = 0
        }
    }
    
    //MARK:Handlers
    
    internal func itemSelectedHandler(_ dataCell: SubscriptionDataCell?) -> Void {
        guard let item = dataCell else {
            HapticFeedback.selectionFeedback()
            return
        }
        
        if !item.subscription.isCurrentSubscription {
            HapticFeedback.selectionFeedback()
        }
        
        if item.isSelected || item.subscription.isCurrentSubscription {
            lastItem = currentItem
            currentItem = item.subscription.isCurrentSubscription ? nil : item
            lastItem?.isSelected = false
            showFooterView(!item.subscription.isCurrentSubscription)
            setFooterViewValue(currentItem?.subscription.price)
            setFooterViewPeriod(currentItem?.subscription.periodicityDescription)
            
            guard let lastItem = self.lastItem else {
                return
            }
            if let index = self.dataSource.firstIndex(of: lastItem) {
                if let lastItemCellView = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ProducerPlanTableViewCell {
                    lastItemCellView.updateMarkButtonView()
                }
            }
        } else {
            lastItem = nil
            currentItem = nil
            showFooterView(false)
        }
    }
    
    internal func dismissViewControllerHandler(sender: Any?) {
        self.onClose?()
    }
    
    @objc
    func nextButtonHandler(sender: UIButton) {
        guard let category = model.categories.first, let item = currentItem else {
            return
        }
        
        let planSelectedEvent = SubscriptionEvent.planSelected(withPrice: item.subscription.price,
                                                               fromSellerID: model.producer.id)
        Analytics.shared.log(planSelectedEvent)
        
        if let additionalOptIn = category.additionalOptIn, additionalOptIn.type == .alert, let alert = additionalOptIn.data as? Alert {
            AlertMessage.showAlert(alert, controller: navigationController ?? self) { [weak self] (popup, button, buttonView) in
                let action: SubscriptionEvent.TermsAndConditionsAction = button.action == .close ? .cancel : .accept
                Analytics.shared.log(SubscriptionEvent.termsAndConditionsInteracted(action: action))
                if button.action != .close {
                    popup.dismiss(animated: true, completion: {
                        self?.goToNextScreen()
                    })
                }
            }
        } else {
            goToNextScreen()
        }
    }
    
    private func goToNextScreen(){
        if let option = self.currentItem {
            if option.subscription.isAddressNeeded {
                self.goAddressListScreen()
            } else {
                self.goPaymentScreen(subscription: option.subscription, producer: self.model.producer)
            }
        }
    }
    
    private func goPaymentScreen(subscription:ProducerPlanItem, producer:ProducerProfileItem, address:ConsumerAddressItem? = nil) {
        let paymentModel = ProducerPlanViewModel(subscriptionPlan: subscription, producer: producer, address: address)
        let paymentViewController = SubscriptionPaymentViewController(model: paymentModel)
        self.navigationController?.pushViewController(paymentViewController, animated: true)
    }
    
    @objc
    func seeMoreAboutProducer() {
        Analytics.shared.log(SubscriptionEvent.learnMoreSelected)
        let moreProducer = AboutProducerViewController(model: ProducerViewModel(producer: self.model.producer))
        self.navigationController?.pushViewController(moreProducer, animated: true)
    }
    
    internal func widgetStackButtonActionHandler(uiButton: UIPPButton, buttonModel:Button, stackModel:WidgetsStack) {
        switch buttonModel.action {
        case .mbsSubscriberSubscriptions:
            let controller = MySubscriptionViewController()
            navigationController?.pushViewController(controller, animated: true)
        default:
            break
        }
    }
    
    // MARK: - Loading Methods
    
    func startLoading() {
        isLoading = true
        tableView.reloadData()
    }
    
    func stopLoading() {
        isLoading = false
        tableView.reloadData()
    }
    
    //MARK: UIScrollViewDelegate
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView === self.tableView {
            // Não permitir scroll pelo inset bottom
            scrollToContent()
            headerView.resize(offset: scrollView.contentOffset)
        }
    }
    
    //MARK: UITableViewDelegate
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isLoading {
            return 1
        }
        return self.dataSource.count
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return self.headerSectionView
        }
        return nil
    }
    
    //MARK: UITableViewDataSource
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isLoading {
            if let cell = self.tableView.dequeueReusableCell(withIdentifier: "loadingCell") as? ProducerLoadingTableViewCell {
                cell.isAnimating = true
                return cell
            } else {
                return UITableViewCell()
            }
        }
        if let cell = self.tableView.dequeueReusableCell(withIdentifier: "subscriptionCell") as? ProducerPlanTableViewCell {
            cell.attach(data: self.dataSource[indexPath.row])
            cell.delegate = self
            if cell.onItemSelected == nil {
                cell.onItemSelected = self.itemSelectedHandler
            }
            return cell
        }
        return UITableViewCell()
    }
    
    //MARK: ExpandableTableViewCellDelegate
    
    func cellHeightChanged(cell: UITableViewCell, diff:CGFloat, isExpanding:Bool) -> Void {
        // Altera o tamanho da celula e corrige glitch no scroll na atualização da tableView
        // iOS < 11 Possui um wrapperView entre a tableView e a cell
        if cell.superview === self.tableView || cell.superview?.superview === self.tableView {
            // Evita que bottom da tableView se ajuste automaticamente com a bottom da tela após o endUpdates
            tableView.contentInset.bottom = UIScreen.main.bounds.height - self.headerView.frame.height
            tableView.beginUpdates()
            tableView.endUpdates()
            // Remove o inset bottom
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                self?.tableView.contentInset.bottom = 0
            }, completion: { [weak self] _ in
                // Em alguns casos, pode aparecer um espaço vazio ao final da tableView após a animação
                self?.scrollToContent(animated: true)
            })
        }
    }
}
