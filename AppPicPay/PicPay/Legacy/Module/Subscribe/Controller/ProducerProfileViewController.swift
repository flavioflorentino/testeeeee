import UI
import UIKit

final class ProducerProfileViewController: UIViewController {
    lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.layer.cornerRadius = 8
        view.layer.masksToBounds = true
        return view
    }()
    lazy var closeButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "iconClose"), for: .normal)
        button.addTarget(self, action: #selector(ProducerProfileViewController.close), for: .touchUpInside)
        button.imageEdgeInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        return button
    }()
    lazy var profileImageView: UIPPProfileImage = {
        let profileImage = UIPPProfileImage()
        profileImage.setProducer(ProducerProfileItem())
        profileImage.translatesAutoresizingMaskIntoConstraints = false
        profileImage.imageView?.borderColor = Palette.ppColorGrayscale000.color
        profileImage.imageView?.borderWidth = 3
        return profileImage
    }()
    lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor(red: 16/255.0, green: 214/255.0, blue: 118/255.0, alpha: 1)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    lazy var backgroundImageMaskView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale600.color
        view.alpha = 0.75
        return view
    }()
    lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.monospacedDigitSystemFont(ofSize: 17, weight: UIFont.Weight.semibold)
        label.textAlignment = .center
        label.text = ""
        return label
    }()
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font =  UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor.init(white: 0.6117, alpha: 1)
        label.textAlignment = .center
        label.text = ""
        return label
    }()
    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = ""
        return label
    }()
    lazy var subscriptionButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(SubscribeLocalizable.seePlans.text, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.medium)
        button.setTitleColor(Palette.ppColorGrayscale000.color, for: .normal)
        button.backgroundColor = UIColor(red: 17/255.0, green: 199/255.0, blue: 111/255.0, alpha: 1)
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(ProducerProfileViewController.subscriptionButtonActionHandler), for: .touchUpInside)
        return button
    }()
    private lazy var loaderView:UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .gray)
        activityView.hidesWhenStopped = true
        activityView.translatesAutoresizingMaskIntoConstraints = false
        return activityView
    }()
    
    internal let profileImageSize:CGFloat = 88
    internal let subscriptionButtonHeight:CGFloat = 48
    
    private var previousViewController:UIViewController? = nil
    private var profileViewLoaded:Bool = false
    private var isSubscribeButtonActive:Bool = true {
        didSet {
            self.subscriptionButton.isEnabled = self.isSubscribeButtonActive
            self.subscriptionButton.alpha = self.isSubscribeButtonActive ? 1 : 0.5
        }
    }
    private var origin: SubscriptionProcucerPlanOrigin
    
    public var model:ProducerViewModel
    
    init(parentViewController:UIViewController, model:ProducerViewModel, origin: SubscriptionProcucerPlanOrigin) {
        self.previousViewController = parentViewController
        self.model = model
        self.origin = origin
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildView()
        
        if model.profileLoaded {
            fill(with: self.model.producer)
        } else {
            isSubscribeButtonActive = false
            startLoad()
            model.loadProducerProfile { [weak self] error in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.stopLoad()
                if let error = error {
                    AlertMessage.showCustomAlertWithError(error, controller: strongSelf) {
                        self?.close()
                    }
                } else {
                    strongSelf.fill(with: strongSelf.model.producer, animated: !strongSelf.profileViewLoaded)
                    strongSelf.isSubscribeButtonActive = true
                    strongSelf.profileViewLoaded = true
                }
            }
        }
    }
    
    //MARK: SubscriptionProfileViewController
    internal func buildView() {
        view.addSubview(contentView)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: contentView, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 12),
            NSLayoutConstraint(item: contentView, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: -12),
            NSLayoutConstraint(item: contentView, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: contentView, attribute: .height, relatedBy: .lessThanOrEqual, toItem: view, attribute: .height, multiplier: 1, constant: -150),
        ])
        
        contentView.addSubview(backgroundImageView)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: backgroundImageView, attribute: .left, relatedBy: .equal, toItem: contentView, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: backgroundImageView, attribute: .right, relatedBy: .equal, toItem: contentView, attribute: .right, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: backgroundImageView, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: backgroundImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 90)
        ])
        
        contentView.addSubview(backgroundImageMaskView)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: backgroundImageMaskView, attribute: .top, relatedBy: .equal, toItem: backgroundImageView, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: backgroundImageMaskView, attribute: .bottom, relatedBy: .equal, toItem: backgroundImageView, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: backgroundImageMaskView, attribute: .left, relatedBy: .equal, toItem: backgroundImageView, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: backgroundImageMaskView, attribute: .right, relatedBy: .equal, toItem: backgroundImageView, attribute: .right, multiplier: 1, constant: 0)
        ])
        
        contentView.addSubview(closeButton)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: closeButton, attribute: .right, relatedBy: .equal, toItem: contentView, attribute: .right, multiplier: 1, constant: -8),
            NSLayoutConstraint(item: closeButton, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: 8),
            NSLayoutConstraint(item: closeButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 28),
            NSLayoutConstraint(item: closeButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 28)
        ])
        
        contentView.addSubview(profileImageView)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: profileImageView, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: 45),
            NSLayoutConstraint(item: profileImageView, attribute: .centerX, relatedBy: .equal, toItem: contentView, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: profileImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: profileImageSize),
            NSLayoutConstraint(item: profileImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 88)
        ])
        
        profileImageView.layer.cornerRadius = profileImageSize / 2
        contentView.addSubview(usernameLabel)
        usernameLabel.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: usernameLabel, attribute: .top, relatedBy: .equal, toItem: profileImageView, attribute: .bottom, multiplier: 1, constant: 16),
            NSLayoutConstraint(item: usernameLabel, attribute: .left, relatedBy: .equal, toItem: contentView, attribute: .left, multiplier: 1, constant: 18),
            NSLayoutConstraint(item: usernameLabel, attribute: .right, relatedBy: .equal, toItem: contentView, attribute: .right, multiplier: 1, constant: -18),
        ])
        
        contentView.addSubview(nameLabel)
        nameLabel.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: nameLabel, attribute: .top, relatedBy: .equal, toItem: usernameLabel, attribute: .bottom, multiplier: 1, constant: 2),
            NSLayoutConstraint(item: nameLabel, attribute: .left, relatedBy: .equal, toItem: contentView, attribute: .left, multiplier: 1, constant: 18),
            NSLayoutConstraint(item: nameLabel, attribute: .right, relatedBy: .equal, toItem: contentView, attribute: .right, multiplier: 1, constant: -18),
        ])
        
        contentView.addSubview(descriptionLabel)
        descriptionLabel.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: descriptionLabel, attribute: .top, relatedBy: .equal, toItem: nameLabel, attribute: .bottom, multiplier: 1, constant: 12),
            NSLayoutConstraint(item: descriptionLabel, attribute: .left, relatedBy: .equal, toItem: contentView, attribute: .left, multiplier: 1, constant: 18),
            NSLayoutConstraint(item: descriptionLabel, attribute: .right, relatedBy: .equal, toItem: contentView, attribute: .right, multiplier: 1, constant: -18),
        ])
        
        contentView.addSubview(subscriptionButton)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: subscriptionButton, attribute: .top, relatedBy: .equal, toItem: descriptionLabel, attribute: .bottom, multiplier: 1, constant: 20),
            NSLayoutConstraint(item: subscriptionButton, attribute: .centerX, relatedBy: .equal, toItem: contentView, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: subscriptionButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 184),
            NSLayoutConstraint(item: subscriptionButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 48),
            NSLayoutConstraint(item: subscriptionButton, attribute: .bottom, relatedBy: .equal, toItem: contentView, attribute: .bottom, multiplier: 1, constant: -32),
            ])
        subscriptionButton.layer.cornerRadius = subscriptionButtonHeight / 2
    }
    
    internal func showSubscriptionOptions() {
        let subscriptionsModel = ProducerCategoriesViewModel(producer: model.producer)
        let subscriptionOptions = ProducerPlansViewController(model: subscriptionsModel, origin: origin)
        let paymentNavigation = PPNavigationController(rootViewController: subscriptionOptions)
        
        subscriptionOptions.onClose = {
            subscriptionOptions.dismiss(animated: true, completion: nil)
        }
        dismiss(animated: true) { [weak self] in
          self?.previousViewController?.present(paymentNavigation, animated: true, completion: nil)
        }
    }
    
    internal func fill(with producer:ProducerProfileItem, animated:Bool = true) {
        profileImageView.setProducer(producer)
        usernameLabel.text = "@\(producer.username)"
        nameLabel.text = producer.name
        descriptionLabel.text = producer.profileDescription
        backgroundImageView.setImage(url: URL(string: producer.backgroundImageUrl ?? ""))
        
        if animated {
            descriptionLabel.alpha = 0
            usernameLabel.alpha = 0
            nameLabel.alpha = 0
            UIView.animate(withDuration: 0.25) { [weak self] in
                self?.descriptionLabel.alpha = 1
                self?.usernameLabel.alpha = 1
                self?.nameLabel.alpha = 1
                self?.contentView.layoutIfNeeded()
            }
        }
    }
    
    //MARK: Loader
    
    internal func startLoad() {
        loaderView.startAnimating()
        contentView.addSubview(loaderView)
        NSLayoutConstraint.activate([
            loaderView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            loaderView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: 25),
        ])
    }
    
    internal func stopLoad() {
        loaderView.stopAnimating()
        loaderView.removeFromSuperview()
    }
    
    //MARK:Handlers
    @objc func close() {
        self.dismiss(animated: true)
    }
    
    @objc func subscriptionButtonActionHandler() {
        self.showSubscriptionOptions()
    }
}
