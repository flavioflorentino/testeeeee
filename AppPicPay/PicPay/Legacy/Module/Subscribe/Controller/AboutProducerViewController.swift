import UI
import UIKit

class AboutProducerViewController: PPBaseViewController {
    private lazy var headerView: ProducerNavigationBarView = {
        let view = ProducerNavigationBarView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private lazy var profileImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = ProducerProfileItem.photoPlaceholder()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.textColor = Palette.ppColorGrayscale200.color
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale300.color
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private lazy var descriptionMarkdownView: MarkdownView = {
        let label = MarkdownView()
        label.isScrollEnabled = false
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = Palette.ppColorGrayscale000.color
        scrollView.showsVerticalScrollIndicator = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    private lazy var scrollViewContentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    private lazy var loaderView: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .gray)
        activityView.hidesWhenStopped = true
        activityView.translatesAutoresizingMaskIntoConstraints = false
        return activityView
    }()
    
    private var profileLoaded: Bool = false
    var model: ProducerViewModel
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    init(model: ProducerViewModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        buildView()
        configureDescriptionMarkdownView()
        headerView.title = DefaultLocalizable.learnMore.text
        headerView.closeButtonAction = { [weak self] (_) in
            self?.done()
        }

        if model.profileLoaded {
            fill()
        } else {
            loadFromServer()
        }
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    internal func buildView() {
        let headerViewHeight:CGFloat = 64
        let imageProfileSize:CGFloat = 108
        let descriptionHMargin:CGFloat = 24
        
        // Header
        view.addSubview(headerView)
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: view.topAnchor),
            headerView.leftAnchor.constraint(equalTo: view.leftAnchor),
            headerView.widthAnchor.constraint(equalTo: view.widthAnchor),
            headerView.heightAnchor.constraint(equalToConstant: headerViewHeight)
        ])
        
        // ScrollView
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scrollView.leftAnchor.constraint(equalTo: view.leftAnchor),
            scrollView.rightAnchor.constraint(equalTo: view.rightAnchor),
        ])
       
        scrollView.addSubview(scrollViewContentView)
        NSLayoutConstraint.activate([
            scrollViewContentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            scrollViewContentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            scrollViewContentView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
            scrollViewContentView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
            scrollViewContentView.widthAnchor.constraint(equalToConstant: view.frame.width),
        ])

        // ProfileImage
        scrollViewContentView.addSubview(profileImage)
        NSLayoutConstraint.activate([
            profileImage.widthAnchor.constraint(equalToConstant: imageProfileSize),
            profileImage.heightAnchor.constraint(equalToConstant: imageProfileSize),
            profileImage.topAnchor.constraint(equalTo: scrollViewContentView.topAnchor, constant: 25),
            profileImage.centerXAnchor.constraint(equalTo: scrollViewContentView.centerXAnchor),
        ])
        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = imageProfileSize / 2
    
        // Username
        scrollViewContentView.addSubview(usernameLabel)
        NSLayoutConstraint.activate([
            usernameLabel.centerXAnchor.constraint(equalTo: profileImage.centerXAnchor),
            usernameLabel.topAnchor.constraint(equalTo: profileImage.bottomAnchor, constant: 8),
        ])

        // Name
        scrollViewContentView.addSubview(nameLabel)
        NSLayoutConstraint.activate([
            nameLabel.centerXAnchor.constraint(equalTo: usernameLabel.centerXAnchor),
            nameLabel.topAnchor.constraint(equalTo: usernameLabel.bottomAnchor, constant: 4),
        ])

        // Description
        scrollViewContentView.addSubview(descriptionMarkdownView)
        NSLayoutConstraint.activate([
            descriptionMarkdownView.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 20),
            descriptionMarkdownView.leftAnchor.constraint(equalTo: scrollViewContentView.leftAnchor, constant: descriptionHMargin),
            descriptionMarkdownView.rightAnchor.constraint(equalTo: scrollViewContentView.rightAnchor, constant: -descriptionHMargin),
            descriptionMarkdownView.bottomAnchor.constraint(equalTo: scrollViewContentView.bottomAnchor, constant: -24),
        ])
    }

    private func loadFromServer() {
        startLoad()
        model.loadProducerProfile { [weak self] (error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.stopLoad()
            if error == nil {
                strongSelf.fill(animated: !strongSelf.profileLoaded)
            } else if !strongSelf.profileLoaded {
                // Apenas apresenta erro caso não houver dados no cache
                strongSelf.presentAlertError(with: error?.localizedDescription)
            }
        }
    }
    
    private func configureDescriptionMarkdownView() {
        self.descriptionMarkdownView.onRendered = {[weak self] (height) in
            guard let strongSelf = self else {
                return
            }
            if let heightConstraint = strongSelf.descriptionMarkdownView.constraints.first(where: {$0.firstAttribute == .height}) {
                heightConstraint.constant = height
            } else {
                strongSelf.descriptionMarkdownView.heightAnchor.constraint(equalToConstant: height).isActive = true
            }
            strongSelf.descriptionMarkdownView.alpha = 0
            UIView.animate(withDuration: 0.25, animations: {
                strongSelf.descriptionMarkdownView.alpha = 1
            })
        }
        
        self.descriptionMarkdownView.onTouchLink = {[weak self] (request) in
            guard let url = request.url, let strongSelf = self else {
                    return false
            }
            ViewsManager.presentSafariViewController(url, from: strongSelf)
            return false
        }
    }
    
    internal func fill(animated:Bool = false) {
        guard let fullProfileDescription = self.model.producer.fullProfileDescriptionMarkdown else {
            presentAlertError()
            return
        }
        
        headerView.fill(with: model.producer)
        usernameLabel.text = "@\(model.producer.username)"
        nameLabel.text = model.producer.name
        descriptionMarkdownView.load(markdown: fullProfileDescription, enableImage: true)

        let url = URL(string: model.producer.profileImageUrl ?? "")
        profileImage.setImage(url: url, placeholder: ProducerProfileItem.photoPlaceholder())

        if animated {
            let viewsToFadeIn:[UIView] = [usernameLabel, nameLabel, descriptionMarkdownView]
            viewsToFadeIn.forEach({$0.alpha = 0})
            UIView.animate(withDuration: 0.25, animations: { [weak self] in
                viewsToFadeIn.forEach({$0.alpha = 1})
                self?.view.layoutSubviews()
            })
        }
        
        profileLoaded = true
    }
    
    private func presentAlertError(with message:String? = nil) {
        let alertMessage = message ?? SubscribeLocalizable.notPossibleProvide.text
        let alertView = UIAlertController(title: nil, message: alertMessage, preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] _ in
            self?.done()
        }))
        present(alertView, animated: true)
    }
    
    /**
     * Dismiss ViewController
     */
    func done() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Loader
    
    internal func startLoad() {
        loaderView.startAnimating()
        scrollViewContentView.addSubview(loaderView)
        NSLayoutConstraint.activate([
            loaderView.centerXAnchor.constraint(equalTo: scrollViewContentView.centerXAnchor),
            loaderView.centerYAnchor.constraint(equalTo: scrollViewContentView.centerYAnchor, constant: 150),
        ])
    }
    
    internal func stopLoad() {
        loaderView.stopAnimating()
        loaderView.removeFromSuperview()
    }
}

