import AnalyticsModule
import SnapKit
import SwiftyJSON
import UI
import UIKit

final class SubscriptionPaymentViewController: PPBaseViewController {
    internal var paymentManager: PPPaymentManager = PPPaymentManager()
    internal var model: PayableSubscriptionViewModel
    internal var toolbarController: PaymentToolbarController? = nil
    
    var hasAnyPaymentError: Bool = false
    
    private var consumerAddress: ConsumerAddressItem?
    
    fileprivate let loader = Loader.getLoadingView("Aguarde")
    fileprivate var auth: PPAuth? = nil
    private var coordinator: Coordinating?
    
    override var shouldAddBreadcrumb: Bool {
        return false
    }

    private lazy var payToolbarContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private lazy var headerView: SubscriptionPaymentHeaderInfoView = {
        let view = SubscriptionPaymentHeaderInfoView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private lazy var dividerLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale100.color
        return view
    }()
    private lazy var descriptionView: PaymentDetailsContainer = {
        let scrollView = PaymentDetailsContainer()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()

    //MARK: UIViewController
    
    init(model:PayableSubscriptionViewModel, nibName: String? = nil, bundle: Bundle? = nil) {
        self.model = model
        self.consumerAddress = self.model.addressItem()
        super.init(nibName: nibName, bundle: bundle)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: UIView
    
    override func viewDidLoad() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        if let topItem = self.navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
        
        toolbarController = PaymentToolbarController(paymentManager: self.paymentManager, parent: self, pay: executePayment)
        buildView()
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Nova Assinatura"
        navigationController?.setNavigationBarHidden(false, animated: true)
        if navigationController?.viewControllers.count == 1 {
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(SubscriptionPaymentViewController.close))
        }
    }

    @objc
    func close() {
        model.changeAddress(self.consumerAddress)
        dismiss(animated: true)
    }
    
    func configureView() {
        paymentManager.subtotal = NSDecimalNumber(value: model.planItem().price)
        headerView.fill(with: model.producerItem(), and: model.planItem())
        let isRootViewController = navigationController?.viewControllers.first == self
        let planDetail = SubscriptionPaymentDetailsView()
        planDetail.setSubscriptionPlan(self.model.planItem())
        if !isRootViewController {
            planDetail.onChangeButtonTap = { [weak self] _ in
                guard let producerPlanVC = self?.navigationController?.viewControllers.first(where: { $0 is ProducerPlansViewController}) as? ProducerPlansViewController else {
                    return
                }
                Analytics.shared.log(SubscriptionEvent.plansViewed(from: .changePlan))
                self?.navigationController?.popToViewController(producerPlanVC, animated: true)
            }
        }
        descriptionView.addPaymentDetails(planDetail)
      
        guard let addressItem = model.addressItem() else {
            return
        }
        let addressDetail = AddressSubscriptionDetailsView()
        addressDetail.setAddress(addressItem)
        addressDetail.onChangeButtonTap = { [weak self] _, address in
            //Procura pela tela com a lista de endereços na stack
            if let addressListVC = self?.navigationController?.viewControllers.first(where: {$0 is AddressListViewController}) as? AddressListViewController {
                //addressListVC.selectedAddress = self?.model.addressItem() //TODO: como passar endereco selecionado
                self?.navigationController?.popToViewController(addressListVC, animated: true)
            } else {
                //Se por algum motivo não encontrar a lista de endereço na navigation stack cria uma nova lista de endereços
                guard let paymentModel = self?.model else {
                    return
                }
                let addressListVM = AddressListViewModel(selectedAddress: paymentModel.addressItem(), displayContextHeader: true)
                let addressListVC = AddressListViewController(viewModel: addressListVM)
                addressListVC.addressContextTitle = "Endereço de entrega"
                addressListVC.addressContextText = "Informe o endereço para onde será enviado os produtos que você está comprando."
                if isRootViewController {
                    addressListVC.onAddressSelected = { (addressSelected) in
                        paymentModel.changeAddress(addressSelected)
                        addressDetail.setAddress(addressSelected)
                        addressListVC.navigationController?.popViewController(animated: true)
                    }
                    self?.navigationController?.pushViewController(addressListVC, animated: true)
                } else {
                    addressListVC.onAddressSelected = { (addressSelected) in
                        paymentModel.changeAddress(addressSelected)
                        addressDetail.setAddress(addressSelected)
                        let paymentViewController = SubscriptionPaymentViewController(model: paymentModel)
                        addressListVC.navigationController?.pushViewController(paymentViewController, animated: true)
                    }
                    let index = (self?.navigationController?.viewControllers.count ?? 1) - 1
                    self?.navigationController?.viewControllers.insert(addressListVC, at: index)
                    self?.navigationController?.popToViewController(addressListVC, animated: true)
                }
            }
        }
        self.descriptionView.addWidget(addressDetail)
    }
    
    func executePayment(privacyConfig: String)  {
        Analytics.shared.log(SubscriptionEvent.subscriptionConfirmed(withPrice: model.planItem().price,
                                                                     fromSellerID: model.producerItem().id))
        auth = PPAuth.authenticate({ [weak self] (authToken, biometry) in
            guard let authToken = authToken,
                let strongSelf = self else {
                    return
            }
            
            DispatchQueue.main.async {
                strongSelf.loader?.isHidden = false
            }
            
            strongSelf.createTransaction(authToken: authToken, biometry: biometry, privacyConfig: privacyConfig)
        }, canceledByUserBlock: {})
    }
    
    func createTransaction(authToken: String, biometry: Bool, privacyConfig: String) {
        guard model.checkNeedCvv(paymentManager: paymentManager) else {
            notifyOfPayment(authToken: authToken, biometry: biometry, privacyConfig: privacyConfig)
            return
        }
        
        createCvvFlowCoordinator(completedCvv: { [weak self] cvv in
            self?.notifyOfPayment(authToken: authToken, biometry: biometry, privacyConfig: privacyConfig, informedCvv: cvv)
        }, completedNoCvv: { [weak self] in
            self?.showCvvError()
        })
    }
    
    private func showCvvError() {
        loader?.isHidden = true
        let error = PicPayError(message: DefaultLocalizable.cvvNotInformed.text)
        AlertMessage.showCustomAlertWithError(error, controller: self)
    }
    
    func notifyOfPayment(authToken: String, biometry: Bool, privacyConfig: String, informedCvv: String? = nil) {
        model.requestPayment(pin: authToken, paymentManager: paymentManager, privacySettings: privacyConfig, biometry: biometry, hasPreviousErrors: self.hasAnyPaymentError, informedCvv: informedCvv) {  [weak self] (result, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.loader?.isHidden = true
            if let result = result {
                
                guard let strongSelf = self else {
                    return
                }
                
                Analytics.shared.log(PaymentEvent.transaction(.membership,
                                                       id: result.transactionId,
                                                       value: result.transactionValue.decimalValue))
                
                let receiptModel = ReceiptWidgetViewModel(transactionId: result.transactionId, type: .PAV)
                if !result.receipts.isEmpty {
                    receiptModel.setReceiptWidgets(items: result.receipts)
                }
                
                NotificationCenter.default.post(name: Notification.Name.Payment.new, object: nil, userInfo: ["type": "Assinatura"])
                
                TransactionReceipt.showReceiptSuccess(viewController: strongSelf, receiptViewModel: receiptModel)
                
            } else {
                strongSelf.hasAnyPaymentError = true //Duplicate
                strongSelf.auth?.handleError(error, callback: { (authError) in
                    AlertMessage.showAlert(withMessage: authError?.localizedDescription, controller: strongSelf)
                })
            }
        }
        
    }

    func buildView() {
        view.addSubview(self.headerView)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: headerView, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: headerView, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: headerView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .topMargin, multiplier: 1, constant: 0),
        ])
        
        view.addSubview(dividerLineView)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: dividerLineView, attribute: .top, relatedBy: .equal, toItem: headerView, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: dividerLineView, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: dividerLineView, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: dividerLineView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 1)
        ])
        
        view.addSubview(descriptionView)
        view.addSubview(payToolbarContainer)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: descriptionView, attribute: .top, relatedBy: .equal, toItem: dividerLineView, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: descriptionView, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: descriptionView, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: descriptionView, attribute: .bottom, relatedBy: .equal, toItem: payToolbarContainer, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: descriptionView, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 1, constant: 0)
        ])

        let payToolbarBottomConstraint:NSLayoutConstraint
        if hasSafeAreaInsets, #available(iOS 11.0, *) {
            payToolbarBottomConstraint = payToolbarContainer.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
            paintSafeAreaBottomInset(withColor: toolbarController?.bottomBarBackgroundColor ?? .clear)
        } else {
            payToolbarBottomConstraint = NSLayoutConstraint(item: payToolbarContainer, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        }
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: payToolbarContainer, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: payToolbarContainer, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: 0),
            payToolbarBottomConstraint,
            NSLayoutConstraint(item: payToolbarContainer, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 92)
        ])
        
        if let loaderView = loader {
            let topView: UIView = navigationController?.view ?? view
            topView.addSubview(loaderView)
        }
        
        if let toolbar = self.toolbarController?.toolbar {
            payToolbarContainer.addSubview(toolbar)
            toolbarController?.toolbar.snp.makeConstraints({ make in
                make.edges.equalToSuperview()
            })
        }
    }
    
    private func createCvvFlowCoordinator(completedCvv: @escaping (String) -> Void, completedNoCvv: @escaping () -> Void) {
        guard let navigationController = navigationController else {
            return
        }
        
        let coordinator = CvvRegisterFlowCoordinator(
            navigationController: navigationController,
            paymentType: .subscription,
            finishedCvv: completedCvv,
            finishedWithoutCvv: completedNoCvv
        )
        coordinator.start()
       
        self.coordinator = coordinator
    }
}
