import UI
import UIKit

final class ProducerPlanChangesViewController: PPBaseViewController {
    private lazy var headerView: ProducerNavigationBarView = {
        let view = ProducerNavigationBarView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private lazy var loaderView:UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .gray)
        activityView.hidesWhenStopped = true
        activityView.translatesAutoresizingMaskIntoConstraints = false
        return activityView
    }()
    private lazy var cardsStackView: UIStackView = {
        let stack = UIStackView()
        stack.distribution = .equalSpacing
        stack.alignment = .top
        stack.axis = .vertical
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.spacing = 12
        return stack
    }()
    
    private lazy var cardsScrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        scroll.showsVerticalScrollIndicator = false
        return scroll
    }()

    var model: ProducerPlanViewModel
    var subscriptionId: String?
    var isProducerProfileLoaded = true
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        buildView()
        headerView.title = SubscribeLocalizable.changeHistory.text
        
        if isProducerProfileLoaded {
            headerView.fill(with: self.model.producerItem())
        } else {
            guard let subscriptionId = self.subscriptionId else {
                return
            }
            SubscriptionViewModel.getSubscription(by: subscriptionId, { [weak self] consumerSubscription, _ in
                guard let subscription = consumerSubscription else {
                    return
                }
                self?.headerView.fill(with: subscription.producer)
            })
        }
        
        headerView.closeButtonAction = { [weak self] (_) in
            self?.done()
        }
        
        if model.plan.changesCards == nil {
            getPlanUpdates()
        } else {
            fillStackView()
        }
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK: ProducerPlanChangesViewController
    
    init(model: ProducerPlanViewModel, subscriptionId:String? = nil) {
        self.model = model
        self.subscriptionId = subscriptionId
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func getPlanUpdates() {
        if self.loaderView.isAnimating {
            return
        }
        
        let errorViewTag:Int = 1000
        let errorView:UIView? = self.view.subviews.first(where: {$0.tag == errorViewTag})
        
        loaderView.startAnimating()
        errorView?.removeFromSuperview()
        
        self.model.getProducerPlanUpdates(after: self.subscriptionId) { [weak self] (error) in
            self?.loaderView.stopAnimating()
            if error == nil {
                self?.fillStackView()
            } else {
                guard let strongSelf = self else {
                    return
                }
                let errorView:UIView
                if ((error as NSError?)?.isConnectionError() ?? false) {
                    errorView = ConnectionErrorView(frame: .zero)
                } else {
                    errorView = BasicEmptyListView(frame: .zero)
                    (errorView as? BasicEmptyListView)?.imageView.image = #imageLiteral(resourceName: "ico_subscribe_large")
                    (errorView as? BasicEmptyListView)?.titleLabel.text = ""
                    if let errorText = error?.localizedDescription, !errorText.isEmpty {
                        (errorView as? BasicEmptyListView)?.textLabel.text = errorText
                    } else {
                        (errorView as? BasicEmptyListView)?.textLabel.text = SubscribeLocalizable.planChangesCouldNotBeRetrieved.text
                    }
                }
                errorView.translatesAutoresizingMaskIntoConstraints = false
                errorView.tag = errorViewTag
                errorView.addGestureRecognizer(UITapGestureRecognizer(target: strongSelf, action:#selector(ProducerPlanChangesViewController.getPlanUpdates)))
                strongSelf.view.addSubview(errorView)
                NSLayoutConstraint.activate([
                    errorView.topAnchor.constraint(equalTo: strongSelf.headerView.bottomAnchor),
                    errorView.leftAnchor.constraint(equalTo: strongSelf.view.leftAnchor),
                    errorView.rightAnchor.constraint(equalTo: strongSelf.view.rightAnchor),
                    errorView.bottomAnchor.constraint(equalTo: strongSelf.view.bottomAnchor)
                ])
            }
        }
    }

    private func buildView() {
        let headerViewHeight:CGFloat = 64
        // Header
        self.view.addSubview(self.headerView)
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: view.topAnchor),
            headerView.leftAnchor.constraint(equalTo: view.leftAnchor),
            headerView.widthAnchor.constraint(equalTo: view.widthAnchor),
            headerView.heightAnchor.constraint(equalToConstant: headerViewHeight)
        ])
        
        // Loader
        view.addSubview(loaderView)
        NSLayoutConstraint.activate([
            loaderView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loaderView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -(headerViewHeight / 2))
        ])
        
        // ScrollView
        view.addSubview(cardsScrollView)
        NSLayoutConstraint.activate([
            cardsScrollView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
            cardsScrollView.leftAnchor.constraint(equalTo: view.leftAnchor),
            cardsScrollView.rightAnchor.constraint(equalTo: view.rightAnchor),
            cardsScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])

        // StackView
        cardsScrollView.addSubview(cardsStackView)
        NSLayoutConstraint.activate([
            cardsStackView.topAnchor.constraint(equalTo: cardsScrollView.topAnchor, constant: 12),
            cardsStackView.leftAnchor.constraint(equalTo: cardsScrollView.leftAnchor, constant: 12),
            cardsStackView.widthAnchor.constraint(equalTo: cardsScrollView.widthAnchor, constant: -24),
            cardsStackView.bottomAnchor.constraint(equalTo: cardsScrollView.bottomAnchor, constant: -12),
        ])
    }
    
    private func fillStackView() {
        self.cardsStackView.removeAllArrangedSubviews()
        
        if let cards = self.model.plan.changesCards, !cards.isEmpty {
            for card in cards {
                let cardWidgetStackView = WidgetsVerticalStackView(frame: .zero, model: card)
                cardWidgetStackView.translatesAutoresizingMaskIntoConstraints = false
                cardsStackView.addArrangedSubview(cardWidgetStackView)
                cardWidgetStackView.widthAnchor.constraint(equalTo: cardsStackView.widthAnchor).isActive = true
            }
        } else {
            let emptyView = BasicEmptyListView(frame: .zero)
            emptyView.titleLabel.text = ""
            emptyView.textLabel.text = SubscribeLocalizable.hasNotChangedSince.text
            emptyView.imageView.image = #imageLiteral(resourceName: "ico_subscribe_large")
            emptyView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(emptyView)
            NSLayoutConstraint.activate([
                emptyView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
                emptyView.leftAnchor.constraint(equalTo: view.leftAnchor),
                emptyView.rightAnchor.constraint(equalTo: view.rightAnchor),
                emptyView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
        }
    }
    
    func done() {
        self.navigationController?.popViewController(animated: true)
    }
}
