import UIKit

final class ConsumerSubscriptionsViewModel: NSObject {
    var apiSubscription: ApiSubscription
    var items: [ConsumerSubscription] = []
    var emptyTitleText: String =  ""
    var subscriptonType: SubscriptionType = .active

    enum SubscriptionType {
        case active
        case cancelled
    }
    
    init(apiSubscription: ApiSubscription = ApiSubscription()) {
        self.apiSubscription = apiSubscription
    }
    
    func getConsumerSubscriptions(_ completion: @escaping ((Error?) ->Void) ) {
        apiSubscription.subscriptions(isActive: self.subscriptonType == .active) { [weak self] (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let result):
                    self?.items = result.list
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }
}
