import UI

final class SubscriptionsTableViewController: UIViewController {
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 157
        tableView.separatorStyle = .none
        tableView.backgroundColor = Palette.ppColorGrayscale100.color
        tableView.registerCell(type: SubscriptionTableViewCell.self)
        tableView.registerCell(type: SubscriptionPaymentTableViewCell.self)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.refreshControl = refreshControl
        return tableView
    }()
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = Palette.ppColorGrayscale600.color
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return refreshControl
    }()
    
    var model: ConsumerSubscriptionsViewModel
    private var loadingView: UILoadView?
    // MARK: - Initializer
    
    init(model: ConsumerSubscriptionsViewModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        registerObsevers()
        loadItems()
    }

    private func setupViews() {
        addComponents()
        layoutComponents()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *), previousTraitCollection?.userInterfaceStyle != self.traitCollection.userInterfaceStyle {
            tableView.reloadData()
        }
    }
    
    // MARK: - Setup
    
    private func registerObsevers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(refresh),
                                               name: Notification.Name.Subscriptions.reload,
                                               object: nil)
    }

    // MARK: - Internal methods
    
    private func loadItems(showLoading: Bool =  true){
        if showLoading {
            startLoading()
        }
        model.getConsumerSubscriptions { [weak self] (error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.checkLoadedItems(error:error)
        }
    }
    
    private func checkLoadedItems(error: Error?){
        if let error = error as NSError?{
            if error.isConnectionError() {
                let errorView = ConnectionErrorView(frame: view.frame)
                errorView.tryAgainTapped = { [weak self] in
                    self?.retryLoad()
                }
                tableView.tableHeaderView = errorView
            } else {
                AlertMessage.showAlert(withMessage: error.localizedDescription, controller: self)
            }
        } else if model.items.isEmpty {
            let emptyView = BasicEmptyListView(frame:view.frame)
            emptyView.titleLabel.text = model.emptyTitleText
            emptyView.textLabel.text = ""
            emptyView.imageView.image = #imageLiteral(resourceName: "ico_subscribe_large")
            tableView.tableHeaderView = emptyView
        }else{
            tableView.tableHeaderView = nil
        }
        
        stopLoading()
        refreshControl.endRefreshing()
        tableView.reloadData()
    }
    
    private func addComponents() {
        view.addSubview(tableView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        if #available(iOS 11.0, *) {
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
    }
    
    // MARK: - Loading Methods
    
    private func startLoading() {
        loadingView?.removeFromSuperview()
        loadingView = UILoadView(superview: view)
        loadingView?.startLoading()
    }
    
    private func stopLoading() {
        loadingView?.animatedRemoveFromSuperView()
    }
    
    // MARK: - User Actions
    
    private func openSubscription(_ subscription: ConsumerSubscription) {
        let subscriptionDetails = SubscriptionViewController(model: SubscriptionViewModel(consumer: subscription))
        subscriptionDetails.hidesBottomBarWhenPushed = true
        subscriptionDetails.onClose = { (sender) in
            subscriptionDetails.dismiss(animated: true)
        }
        let navigationController = UINavigationController(rootViewController: subscriptionDetails)
        present(navigationController, animated: true)
    }
    
    private func paySubscription(_ subscription: ConsumerSubscription) {
        let subscriptionPayment = SubscriptionPaymentViewController(model: SubscriptionViewModel(consumer: subscription))
        let paymentNavigation = PPNavigationController(rootViewController: subscriptionPayment)
        self.present(paymentNavigation, animated: true, completion: nil)
    }
    
    @objc
    private func refresh() {
        loadItems(showLoading: false)
    }
    
    @objc
    private func retryLoad() {
        loadItems(showLoading: true)
    }
}

// MARK: - Table view data source
extension SubscriptionsTableViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard model.items.count > indexPath.row else {
            return UITableViewCell()
        }
        let subscription = model.items[indexPath.row]
        switch subscription.status {
        case .waitingPayment:
            let cell = tableView.dequeueReusableCell(type: SubscriptionPaymentTableViewCell.self, forIndexPath: indexPath)
            cell.configure(subscription)
            cell.payAction = paySubscription
            return cell
        default:
            let cell = tableView.dequeueReusableCell(type: SubscriptionTableViewCell.self, forIndexPath: indexPath)
           cell.configure(subscription)
            return cell
        }
    }
}

// MARK: - Table view delegate
extension SubscriptionsTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard model.items.count > indexPath.row
            else {
            return
        }
        
        let subscription = model.items[indexPath.row]
        openSubscription(subscription)
    }
}
