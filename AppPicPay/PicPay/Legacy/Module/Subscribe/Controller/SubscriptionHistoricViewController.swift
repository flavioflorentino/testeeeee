import UI
import UIKit
import AsyncDisplayKit

final class SubscriptionHistoricViewController: FeedViewController {
    
    override func viewDidLoad() {
        self.title = SubscribeLocalizable.historic.text
        self.view.backgroundColor = Palette.ppColorGrayscale000.color
        self.node.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 44)
        super.viewDidLoad()
    }
    
    override func registerObservers() {
        //Não registrar observers
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.reloadFeed()
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func showEmptyView(){
        let views = Bundle.main.loadNibNamed("FeedEmptyListView", owner: nil, options: nil)
        if let emptyView = views?.first as? FeedEmptyListView {
            emptyView.frame = self.view.frame
            
            emptyView.titleLabel.text = SubscribeLocalizable.couldntLoadHistory.text
            emptyView.textLabel.text = SubscribeLocalizable.pleaseTryAgainLater.text
            emptyView.animationView.isHidden = true
            
            DispatchQueue.main.async {
                self.tableNode.view.tableHeaderView = emptyView
            }
        }
    }
    
    override func openFeedDetail(_ feedItem:DefaultFeedItem, openKeyboard:Bool = false){
        let feedDetailModel = FeedDetailViewModel(feedItem: feedItem)
        let controller = HistoricFeedDetailViewController(model: feedDetailModel)
        controller.appearWithOpenKeyboard = openKeyboard
        controller.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
