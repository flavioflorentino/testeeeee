import UIKit

final class ProducerCategoriesViewModel: NSObject {
    var apiSubscription:ApiSubscription
    var producer:ProducerProfileItem
    var categories:[ProducerCategoryItem] = []
    
    init(producer: ProducerProfileItem, apiSubscription: ApiSubscription = ApiSubscription()) {
        self.apiSubscription = apiSubscription
        self.producer = producer
    }
    
    func getCategoriesPlans(_ completion: @escaping ((Error?) ->Void) ) {
        apiSubscription.categoriesPlans(by: self.producer.id) { [weak self] (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let result):
                    self?.categories = result.list
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }       
}
