import UI
import UIKit
import AnalyticsModule

final class SubscriptionViewController: PPBaseViewController, UIScrollViewDelegate {
    typealias Dependencies = HasAnalytics
    
    private let dependencies: Dependencies = DependencyContainer()
    
    @objc lazy var headerView: ProducerProfileHeaderView = {
        let header = ProducerProfileHeaderView()
        header.minHeight = 64
        header.translatesAutoresizingMaskIntoConstraints = false
        header.optionsButtonIsHidden = false
        return header
    }()
    @objc lazy var contentView: ProducerPlanView = {
        let activePlan = ProducerPlanView()
        activePlan.translatesAutoresizingMaskIntoConstraints = false
        activePlan.backgroundColor = Palette.ppColorGrayscale000.color
        activePlan.bounces = false
        return activePlan
    }()
    @objc lazy var optionsSheet: UIAlertController = { [unowned self] in
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        sheet.addAction(UIAlertAction(
            title: "Cancelar",
            style: .cancel,
            handler: nil
        ))
        return sheet
    }()
    @objc lazy var cancelSubscriptionAlert: UIAlertController = { [unowned self] in
        let alert = UIAlertController(
            title: "Tem certeza que deseja cancelar sua assinatura?",
            message: "Ao cancelar, sua próxima mensalidade não será renovada",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(
            title: "Sim, quero cancelar",
            style: .destructive,
            handler: { [weak self] (action) in self?.cancelSubscriptionAction(sender: action)}
        ))
        alert.addAction(UIAlertAction(
            title: "Não, não quero cancelar",
            style: .cancel,
            handler: { [weak self] _ in self?.trackCancelSubscription(with: .no) }
        ))
        return alert
    }()
    @objc let loader = Loader.getLoadingView("Aguarde")
    
    lazy private var contentLoaderView:UIView = {
        let view = UIView()
        let activityView = UIActivityIndicatorView(style: .gray)
        
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.isHidden = true
        
        activityView.translatesAutoresizingMaskIntoConstraints = false
        activityView.hidesWhenStopped = true
        activityView.startAnimating()
        
        view.addSubview(activityView)
        NSLayoutConstraint.activate([
            view.centerXAnchor.constraint(equalTo: activityView.centerXAnchor),
            view.centerYAnchor.constraint(equalTo: activityView.centerYAnchor)
        ])
        return view
    }()

    internal lazy var initialHeaderViewSize: CGFloat = { [weak self] in
        guard let strongSelf = self else {
            return 0.0
        }
        var size:CGFloat = 180
        if #available(iOS 11.0, *) {
            size += UIApplication.shared.statusBarFrame.height + strongSelf.view.safeAreaInsets.top
        } else {
            size += UIApplication.shared.statusBarFrame.height
        }
        return size
    }()
    
    @objc public var onClose:((_ sender:Any?) -> Void)? = nil
    @objc public var showActionButton:Bool = true
    @objc private var model:SubscriptionViewModel?
    
    private var viewTopConstraint:NSLayoutConstraint? = nil
    private var auth:PPAuth? = nil
    private var subscriptionId:String? = nil
    
    // MARK: UIViewController
    
    @objc init(model subscription: SubscriptionViewModel?, nibName nibNameOrNil: String? = nil, bundle nibBundleOrNil: Bundle? = nil) {
        self.model = subscription
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        self.model = nil
        super.init(coder: aDecoder)
    }
    
    //MARK: UIView
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationController?.setNavigationBarHidden(true, animated: true)
        if let navigation = navigationController, navigation.viewControllers.first !== self {
            headerView.showBackButton = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentView.delegate = self
        view.backgroundColor = Palette.ppColorGrayscale000.color
        buildView()
        
        if model != nil {
            configureView()
        } else {
            loadModel()
        }
        
        // Register Actions
        headerView.optionsButtonAction = { [weak self] (_) in
            if let strongSelf = self {
                strongSelf.present(strongSelf.optionsSheet, animated: true, completion: nil)
            }
        }
        headerView.closeButtonAction = { [weak self] (sender) in
            if let strongSelf = self {
                strongSelf.onClose?(sender)
            }
        }
    }

    internal func configureView() {
        self.headerView.fill(with: self.model?.subscription.producer)
        if navigationController != nil {
            contentView.onSubscriptionWidgetChangeButtonTap = { [weak self] (widget) in
                switch widget {
                case let address as ConsumerAddressItem:
                    self?.changeSubscriptionAddress(currentAddress: address)
                    self?.trackAddressChanged()
                default:
                    break
                }
            }
        }
        contentView.fill(with: self.model?.subscription)
        contentView.onCardButtonTap = { [weak self] (button, cardButtonModel, widgetsStackModel) in
            guard let strongSelf = self else {return}
            strongSelf.executeCardAction(cardButtonModel: cardButtonModel, widgetModel: widgetsStackModel, button: button)
        }
        
        contentView.onActionButtonTap = nil
        if let status = self.model?.subscription.status {
            self.configureOptionsSheet(for: status)
            dependencies.analytics.log(SubscriptionEvent.subscriptionViewed(status: status))
        }
    }
    
    private func showProducerPlanChanges() {
        guard let plan = model?.planItem(), let producer = model?.producerItem(), let status = model?.subscription.status else { return }
        dependencies.analytics.log(SubscriptionEvent.planChangesHistoryViewed(status: status))
        let producerModel = ProducerPlanViewModel(subscriptionPlan: plan, producer: producer)
        let controller = ProducerPlanChangesViewController(model: producerModel, subscriptionId: model?.subscription.id)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func askForPassword(then callback: @escaping ((String) -> Void))  {
        self.auth = PPAuth.authenticate({ (authToken, biometry) in
            guard let authToken = authToken else {
                return
            }
            callback(authToken)
        }, canceledByUserBlock: {})
    }
    
    internal func updateSubscriptionAddress(_ address: ConsumerAddressItem) {
        let loader = Loader.getLoadingView("Aguarde") ?? UIView()
        guard let actualViewController = self.navigationController?.viewControllers.last else {
            return
        }
        actualViewController.view.addSubview(loader)
        loader.isHidden = false
        
        self.model?.updateSubscriptionAddress(new: address, { [weak self] (error) in
            guard let strongSelf = self, let subscriptionModel = strongSelf.model else {
                return
            }
            loader.isHidden = true
            loader.removeFromSuperview()
            if error == nil {
                strongSelf.contentView.updateDetailsWidgets(consumerSubscrition: subscriptionModel.subscription)
                if strongSelf.navigationController?.viewControllers.last !== strongSelf {
                    strongSelf.navigationController?.popToViewController(strongSelf, animated: true)
                }
            } else {
                AlertMessage.showAlert(error, controller: self)
            }
        })
    }
    
    internal func changeSubscriptionAddress(currentAddress: ConsumerAddressItem) {
        let addressListVM = AddressListViewModel(selectedAddress: currentAddress, displayContextHeader: true)
        let consumerAddressList = AddressListViewController(viewModel: addressListVM)
        consumerAddressList.addressContextTitle = "Endereço de entrega"
        consumerAddressList.addressContextText = "Informe o endereço para onde será enviado os produtos que você está comprando."
        consumerAddressList.onAddressSelected = { [weak self] (address) in
            guard let newAddress = address else {
                return
            }
            if newAddress != currentAddress {
                let alert = UIAlertController(title: "Deseja alterar o endereço?", message: "Qualquer tipo de entrega contemplada pelo seu plano passará a ser enviada para o novo endereço selecionado.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { (_) in
                    self?.updateSubscriptionAddress(newAddress)
                }))
                alert.addAction(UIAlertAction(title: "Não", style: .cancel, handler: nil))
                consumerAddressList.present(alert, animated: true, completion: nil)
            }
        }
        self.navigationController?.pushViewController(consumerAddressList, animated: true)
    }
    
    private func trackAddressChanged() {
        let event = SubscriptionEvent.subscriptionAddressChanged(status: model?.subscription.status ?? .unknown)
        dependencies.analytics.log(event)
    }
    
    private func trackCancelSubscription(with action: SubscriptionEvent.CancelSubscriptionAction) {
        let event = SubscriptionEvent.cancelSubscriptionInteracted(action: action)
        dependencies.analytics.log(event)
    }
    
    internal func removeCard(widgetModel:WidgetsStack) {
        if let widgetStackIndex = self.model?.subscription.cards.firstIndex(where: {$0 === widgetModel}) {
            self.model?.subscription.cards.remove(at: widgetStackIndex)
            self.contentView.removeWidgetStackView(stackModel: widgetModel)
        }
    }
    
    internal func executeCardAction(cardButtonModel:Button, widgetModel: WidgetsStack, button:UIButton) {
        let cardSubscriptionId = cardButtonModel.actionData?["subscription_id"] as? String ?? ""
        let cardPlanId = cardButtonModel.actionData?["plan_id"] as? String ?? ""
        let cardProducerId = cardButtonModel.actionData?["producer_id"] as? String ?? ""
        let subscriptionViewModel:SubscriptionViewModel = {
            if let subscriptionViewModel = self.model, subscriptionViewModel.subscription.id == cardSubscriptionId {
                return subscriptionViewModel
            } else {
                let subscription = ConsumerSubscription()
                subscription.id = cardSubscriptionId
                return SubscriptionViewModel(consumer: subscription)
            }
        }()
        
        switch cardButtonModel.action {
        //*Plano passa a pedir endereço*:
        case .mbsInsertAddress:
            let addressListVM = AddressListViewModel(displayContextHeader: true)
            let controller = AddressListViewController(viewModel: addressListVM)
            controller.addressContextTitle = "Endereço de entrega"
            controller.addressContextText = "Informe o endereço para onde será enviado os produtos que você está comprando."
            controller.onAddressSelected = { [weak self] (address) in
                guard let consumerAddress = address else {
                    return
                }
                self?.updateSubscriptionAddressCardAction(consumerAddress, subscriptionViewModel: subscriptionViewModel, cardWidgetModel: widgetModel)
            }
            self.navigationController?.pushViewController(controller, animated: true)
            
        //*Pagamento atrasado*:
        case .mbsPaySubscription:
            if let subscriptionModel = self.model, cardSubscriptionId == subscriptionModel.subscription.id {
                self.showPaymentScreen(subscriptionViewModel: subscriptionModel)
            } else {
                (button as? UIPPButton)?.startLoadingAnimating()
                SubscriptionViewModel.getSubscription(by: cardSubscriptionId, { [weak self] (consumerSubscription, error) in
                    (button as? UIPPButton)?.stopLoadingAnimating()
                    if let subscription = consumerSubscription {
                        let model = SubscriptionViewModel(consumer: subscription)
                        self?.showPaymentScreen(subscriptionViewModel: model)
                    } else {
                        AlertMessage.showAlert(error, controller: self)
                    }
                })
            }
            
        //*Cancelamento solicitado*:
        case .mbsResubscribe:
            dependencies.analytics.log(SubscriptionEvent.subscriptionReactivated)
            self.askForPassword(then: { (pin) in
                (button as? UIPPButton)?.startLoadingAnimating()
                subscriptionViewModel.reSubscribe(pin: pin, { [weak self] (error) in
                    (button as? UIPPButton)?.stopLoadingAnimating()
                    if error == nil {
                        NotificationCenter.default.post(name: Notification.Name.Subscriptions.reload, object: nil)
                        AlertMessage.showAlert(withMessage: "Sua assinatura foi reativada.", controller: self, completion: {
                            self?.navigationController?.popViewController(animated: true)
                        })
                    } else {
                        self?.auth?.handleError(error, callback: { (authError) in
                            AlertMessage.showAlert(authError, controller: self)
                        })
                    }
                })
            })
            
        //*Plano alterado*:
        case .mbsPlanUpdateHistory:
            self.showSubscriptionUpdatesCardAction(cardSubscriptionId, planId: cardPlanId)
            
        //*Plano alterado*: - Ok, entendi
        case .mbsDismissPlanUpdate:
            (button as? UIPPButton)?.startLoadingAnimating()
            subscriptionViewModel.lastSubscriptionUpdatesSeen({ [weak self] (error) in
                (button as? UIPPButton)?.stopLoadingAnimating()
                if error == nil {
                    self?.removeCard(widgetModel: widgetModel)
                } else {
                    AlertMessage.showAlert(error, controller: self)
                }
            })
            
        //*Assinatura cancelada*:
        case .mbsNewSubscription:
            self.newSubscriptionCardAction(cardProducerId)
            
        //Perfil já assinado:
        case .mbsSubscriberSubscriptions:
            let controller = MySubscriptionViewController()
            self.navigationController?.pushViewController(controller, animated: true)
            
        default:
            break
        }
    }
    
    internal func buildView() {
        view.addSubview(contentView)
        NSLayoutConstraint.constraintAllEdges(from: contentView, to: view)
        contentView.contentWidth = view.bounds.width
        contentView.paddingTop = initialHeaderViewSize
        
        view.addSubview(headerView)
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [headerView])
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: view.topAnchor),
            headerView.heightAnchor.constraint(equalToConstant: initialHeaderViewSize)
        ])

        if #available(iOS 13.0, *) {
            self.headerView.topInset = 0
        } else {
            if #available(iOS 11.0, *) {
                self.headerView.topInset = UIApplication.shared.statusBarFrame.height + self.view.safeAreaInsets.top
            } else {
                self.headerView.topInset = UIApplication.shared.statusBarFrame.height
            }
        }
        
        if let loaderView = self.loader {
            self.view.addSubview(loaderView)
        }
    }
    
    func configureOptionsSheet(`for` status: ConsumerSubscription.Status) {
        if let _ = self.model?.producerItem().fullProfileDescriptionMarkdown {
            self.optionsSheet.addAction(createAction(type: .aboutProducer))
        }
        self.optionsSheet.addAction(createAction(type: .seePlanChanges))
        switch status {
        case .active:
            optionsSheet.addAction(createAction(type: .history))
            optionsSheet.addAction(createAction(type: .cancel))
        case .activeCancelled:
            optionsSheet.addAction(createAction(type: .history))
            optionsSheet.addAction(createAction(type: .reSubscribe))
        case .cancelled:
            optionsSheet.addAction(createAction(type: .history))
            optionsSheet.addAction(createAction(type: .reSubscribe))
            optionsSheet.addAction(createAction(type: .removeFromList))
        case .waitingPayment:
            optionsSheet.addAction(createAction(type: .history))
            optionsSheet.addAction(createAction(type: .subscribeOtherPlan))
            optionsSheet.addAction(createAction(type: .paySubscription))
        default:
            break
        }
    }
    
    fileprivate enum ActionType {
        case history
        case cancel
        case reSubscribe
        case removeFromList
        case paySubscription
        case subscribeOtherPlan
        case aboutProducer
        case seePlanChanges
    }
    
    fileprivate func createAction(type: ActionType) -> UIAlertAction  {
        switch type {
        case .history:
            return UIAlertAction(
                title: "Ver histórico de pagamentos",
                style: .default,
                handler: { [weak self] (_) in
                    if let subscription = self?.model?.subscription {
                        self?.dependencies.analytics.log(SubscriptionEvent.paymentHistoryViewed(status: subscription.status))
                        let historic = SubscriptionHistoricViewController()
                        let feedModel = FeedViewModel()
                        feedModel.visibility = FeedViewModel.Visibility.personal
                        feedModel.targetType = "subscription"
                        feedModel.targetId = subscription.id
                        historic.model = feedModel
                        self?.navigationController?.pushViewController(historic, animated: true)
                    } else {
                        AlertMessage.showAlert(withMessage: "Não foi possível exibir o histórico.\nPor favor tente mais tarde.", controller: self)
                    }
                }
            )
        case .cancel:
            return UIAlertAction(
                title: "Cancelar assinatura",
                style: .destructive,
                handler: { [weak self] (action) in self?.showCancelSubscriptionAlert(sender: action)}
            )
        case .reSubscribe:
            return UIAlertAction(
                title: "Reassinar",
                style: .default,
                handler: { [weak self] (action) in self?.reSubscribeAction(sender: action)}
            )
        case .removeFromList:
            return UIAlertAction(
                title: "Remover da lista",
                style: .default,
                handler: { [weak self] (_) in
                    self?.deleteCanceledSubscription()
                }
            )
        case .paySubscription:
            return UIAlertAction(
                title: "Pagar assinatura",
                style: .default,
                handler: { [weak self] (action) in self?.payNowAction(sender: action)}
            )
        case .subscribeOtherPlan:
            return UIAlertAction(
                title: "Assinar outro plano",
                style: .default,
                handler: { [weak self] (_) in self?.showSubscriptionPlans() }
            )
            
        case .aboutProducer:
            return UIAlertAction(
                title: "Sobre @\(self.model?.producerItem().username ?? "")",
                style: .default,
                handler: { [weak self] (_) in
                    if let model = self?.model {
                        self?.dependencies.analytics.log(SubscriptionEvent.aboutViewed(status: model.subscription.status))
                        let aboutProducer = AboutProducerViewController(model: ProducerViewModel(producer: model.producerItem()))
                        self?.navigationController?.pushViewController(aboutProducer, animated: true)
                    } else {
                        AlertMessage.showAlert(withMessage: "Não foi possível exibir o mais sobre o produtor.\nPor favor tente mais tarde.", controller: self)
                    }
                }
            )
            
        case .seePlanChanges:
            return UIAlertAction(
                title: "Ver histórico de alterações do plano",
                style: .default,
                handler: { [weak self] (_) in
                    self?.showProducerPlanChanges()
                }
            )
        }
    }
    
    internal func deleteCanceledSubscription() {
        self.loader?.isHidden = false
        self.model?.deleteSubscription({ [weak self] (error) in
            guard let strongSelf = self else {
                return
            }
            self?.loader?.isHidden = true
            if error == nil {
                NotificationCenter.default.post(name: NSNotification.Name.Subscriptions.reload, object: nil)
                strongSelf.onClose?(nil)
            } else {
                AlertMessage.showAlert(error, controller: self)
            }
        })
    }
    
    private func loadModel() {
        self.view.addSubview(self.contentLoaderView)
        self.contentLoaderView.frame = CGRect(
            origin: CGPoint(x: 0, y: self.initialHeaderViewSize),
            size: CGSize(width: self.view.frame.width, height: self.view.frame.height - self.initialHeaderViewSize)
        )
        
        self.contentLoaderView.isHidden = false
        
        SubscriptionViewModel.getSubscription(by: self.subscriptionId ?? "", { [weak self] (response, error) in
            if let subscription = response, let strongSelf = self {
                strongSelf.contentLoaderView.subviews.forEach({ ($0 as? UIActivityIndicatorView)?.stopAnimating() })
                
                UIView.animate(withDuration: 0.25, animations: {
                    self?.contentLoaderView.backgroundColor = .clear
                }, completion: { (done) in
                    self?.contentLoaderView.isHidden = true
                    self?.contentLoaderView.removeFromSuperview()
                })
                
                strongSelf.model = SubscriptionViewModel(consumer: subscription)
                strongSelf.configureView()
            } else {
                let alertMessage:String = error?.localizedDescription ?? "Não foi possível exibir os detalhes do plano.\nPor favor tente novamente mais tarde."
                let alert = UIAlertController(title: nil, message: alertMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] (action) in
                    self?.loader?.isHidden = true
                    self?.onClose?(nil)
                }))
                self?.present(alert, animated: true, completion: nil)
            }
        })
    }
    
    @objc func load(subscriptionId: String) {
        self.subscriptionId = subscriptionId
    }
    
    //MARK: Handlers
    
    internal func reSubscribeAction(sender: Any?) {
        dependencies.analytics.log(SubscriptionEvent.reSubscribeSelected)
        guard let plan = self.model?.subscription.subscriptionPlan else {
            return
        }
        self.showSubscriptionPlans(currentPlanItem: SubscriptionDataCell(subscription: plan))
    }
    
    internal func cancelSubscriptionAction(sender: Any?) {
        trackCancelSubscription(with: .yes)
        self.loader?.isHidden = false
        self.model?.cancelSubscription({ [weak self] (error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.loader?.isHidden = true
            if error == nil {
                NotificationCenter.default.post(name: NSNotification.Name.Subscriptions.reload, object: nil)
                strongSelf.onClose?(nil)
            } else {
                AlertMessage.showAlert(error, controller: self)
            }
        })
    }
    
    internal func payNowAction(sender: Any?){
        guard let subscription = self.model else {
            return
        }
        self.showPaymentScreen(subscriptionViewModel: subscription)
    }
    
    internal func showSubscriptionPlans(currentPlanItem: SubscriptionDataCell? = nil) {
        guard let producer = self.model?.subscription.producer else {
            return
        }
        
        let subscriptionModel = ProducerCategoriesViewModel(producer: producer)
        let subscriptionPlansVC = ProducerPlansViewController(model: subscriptionModel, origin: .newSubscription)
        subscriptionPlansVC.onClose = {
            subscriptionPlansVC.dismiss(animated: true, completion: nil)
        }
        subscriptionPlansVC.currentItem = currentPlanItem
        let paymentNavigation = PPNavigationController(rootViewController: subscriptionPlansVC)
        self.present(paymentNavigation, animated: true, completion: nil)
    }
    
    internal func showCancelSubscriptionAlert(sender: Any?) {
        dependencies.analytics.log(SubscriptionEvent.cancelSubscriptionSelected)
        self.present(self.cancelSubscriptionAlert, animated: true, completion: nil)
    }
    
    internal func showPaymentScreen(subscriptionViewModel: SubscriptionViewModel) {
        let subscriptionPayment = SubscriptionPaymentViewController(model: subscriptionViewModel)
        
        let navigation = PPNavigationController(rootViewController: subscriptionPayment)
        self.present(navigation, animated: true, completion: nil)
    }
    
    //MARK: CardActionsFunctions
    
    internal func newSubscriptionCardAction(_ cardProducerId: String) {
        let producerPlansVC: ProducerPlansViewController
        if let producer = self.model?.producerItem(), producer.id == cardProducerId {
            producerPlansVC = ProducerPlansViewController(model: ProducerCategoriesViewModel(producer: producer), origin: .newSubscription)
        } else {
            let producer = ProducerProfileItem()
            producer.id = cardProducerId
            producerPlansVC = ProducerPlansViewController(model: ProducerCategoriesViewModel(producer: producer), origin: .newSubscription)
            producerPlansVC.isProducerProfileLoaded = false
        }
        
        let paymentNavigation = PPNavigationController(rootViewController: producerPlansVC)
        
        producerPlansVC.onClose = {
            paymentNavigation.dismiss(animated: true, completion: nil)
        }
        
        self.present(paymentNavigation, animated: true, completion: nil)
    }
    
    internal func showSubscriptionUpdatesCardAction(_ cardSubscriptionId:String, planId:String) {
        if let subscriptionModel = self.model, subscriptionModel.subscription.id == cardSubscriptionId {
            let controller = ProducerPlanChangesViewController(model: ProducerPlanViewModel(subscriptionPlan: subscriptionModel.planItem(), producer: subscriptionModel.producerItem()), subscriptionId: cardSubscriptionId)
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            let plan = ProducerPlanItem()
            plan.id = planId
            let controller = ProducerPlanChangesViewController(model: ProducerPlanViewModel(subscriptionPlan: plan, producer: ProducerProfileItem()), subscriptionId: cardSubscriptionId)
            controller.isProducerProfileLoaded = false
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    internal func updateSubscriptionAddressCardAction(_ address: ConsumerAddressItem, subscriptionViewModel:SubscriptionViewModel, cardWidgetModel:WidgetsStack) {
        let loader = Loader.getLoadingView("Aguarde") ?? UIView()
        guard let actualViewController = self.navigationController?.viewControllers.last else {
            return
        }
        actualViewController.view.addSubview(loader)
        loader.isHidden = false
        
        subscriptionViewModel.updateSubscriptionAddress(new: address) { [weak self] (error) in
            guard let strongSelf = self else {
                return
            }
            loader.isHidden = true
            loader.removeFromSuperview()
            if error == nil {
                if self?.model?.subscription.id == subscriptionViewModel.subscription.id {
                    strongSelf.contentView.updateDetailsWidgets(consumerSubscrition: subscriptionViewModel.subscription)
                }
                strongSelf.removeCard(widgetModel: cardWidgetModel)
                if strongSelf.navigationController?.viewControllers.last !== strongSelf {
                    strongSelf.navigationController?.popToViewController(strongSelf, animated: true)
                }
            } else {
                AlertMessage.showAlert(error, controller: self)
            }
        }
    }
    
    // MARK: UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView === self.contentView {
            self.headerView.resize(offset: scrollView.contentOffset)
        }
    }
}

extension SubscriptionViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return (otherGestureRecognizer is UIScreenEdgePanGestureRecognizer)
    }
}
