import CarbonKit
import UI
import UIKit

final class MySubscriptionViewController: UIViewController {
    private var carbonTabNavigation: CarbonTabSwipeNavigation?
    private lazy var canceledSubscriptionController: SubscriptionsTableViewController = {
        let modelCanceled = ConsumerSubscriptionsViewModel()
        modelCanceled.emptyTitleText = SubscribeLocalizable.canceledSubscriptions.text
        modelCanceled.subscriptonType = .cancelled
        return SubscriptionsTableViewController(model: modelCanceled)
    }()
    private lazy var activeSubscriptionController: SubscriptionsTableViewController = {
        let model = ConsumerSubscriptionsViewModel()
        model.emptyTitleText = SubscribeLocalizable.activeSubscriptions.text
        
        model.subscriptonType = .active
        return SubscriptionsTableViewController(model: model)
    }()
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupCarbonNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.isNavigationBarHidden = false
    }
    
    // MARK: - Configure
    
    func setupView() {
        title = SubscribeLocalizable.mySubscriptions.text
        view.backgroundColor = Palette.ppColorGrayscale100.color
        paintSafeAreaBottomInset(withColor: Palette.ppColorGrayscale100.color)
    }
    
    func setupCarbonNavigation() {
        carbonTabNavigation = CarbonTabSwipeNavigation(items: ["Ativas", "Canceladas"], delegate: self)
        carbonTabNavigation?.toolbar.isTranslucent = false
        carbonTabNavigation?.setIndicatorColor(Palette.ppColorBranding300.color)
        carbonTabNavigation?.setNormalColor(Palette.ppColorGrayscale400.color)
        carbonTabNavigation?.setSelectedColor(Palette.ppColorBranding300.color)
        carbonTabNavigation?.carbonSegmentedControl?.backgroundColor = Palette.ppColorGrayscale100.color
        carbonTabNavigation?.setTabBarHeight(44.0)
        carbonTabNavigation?.setIndicatorHeight(2.0)
        carbonTabNavigation?.carbonTabSwipeScrollView.scrollsToTop = false
        carbonTabNavigation?.insert(intoRootViewController: self)
        
        let width = view.frame.width / 2
        carbonTabNavigation?.carbonSegmentedControl?.setWidth(width, forSegmentAt: 0)
        carbonTabNavigation?.carbonSegmentedControl?.setWidth(width, forSegmentAt: 1)
    }
}

// MARK: - CarbonTabSwipeNavigationDelegate
extension MySubscriptionViewController: CarbonTabSwipeNavigationDelegate {
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        if index == 0 {
            return activeSubscriptionController
        }
        return canceledSubscriptionController
    }
}
