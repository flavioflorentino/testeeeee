import UI
import UIKit

protocol ItemDetailsWidget {
    func view() -> UIView
    func viewMustUpdate() -> Void
}

extension ItemDetailsWidget where Self: UIView {
    func view() -> UIView {
        return self
    }
}

class ItemDetailsWidgetContainer: UIScrollView {
    var lastView: UIView? = nil
    
    private func dividerView() -> UIView {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    internal func addWidget(_ details: ItemDetailsWidget, isDividerRequired:Bool = true) {
        let view = details.view()
        view.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
        if let topView = self.lastView {
            if let lasViewBottomConstraint = self.constraints.first(where: { $0.firstItem === self.lastView && $0.firstAttribute == .bottom }) {
                lasViewBottomConstraint.isActive = false
            }
            NSLayoutConstraint.activate([
                view.topAnchor.constraint(equalTo: topView.bottomAnchor),
                view.widthAnchor.constraint(equalTo: widthAnchor),
                view.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
        } else {
            NSLayoutConstraint.activate([
                view.topAnchor.constraint(equalTo: topAnchor),
                view.widthAnchor.constraint(equalTo: widthAnchor),
                view.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
        }
        
        if isDividerRequired {
            let divider = dividerView()
            let dividerLeftInset: CGFloat = 16
            let dividerHeight: CGFloat = 1
            addSubview(divider)
            NSLayoutConstraint.activate([
                divider.leftAnchor.constraint(equalTo: leftAnchor, constant: dividerLeftInset),
                divider.widthAnchor.constraint(equalTo: widthAnchor, constant: -dividerLeftInset),
                divider.heightAnchor.constraint(equalToConstant: dividerHeight),
                divider.centerYAnchor.constraint(equalTo: view.bottomAnchor)
            ])
        }
        
        self.lastView = view
    }
}

