import UI
import UIKit

protocol SubscriptionDetailsWidget:ItemDetailsWidget { }

protocol SubscriptionDetails: AnyObject {
    func getSubscriptionDetailInfo() -> String
    func getSubscriptionDetailTitle() -> String?
    func getSubscriptionDetailSubtitle() -> String
}

final class SubscriptionDetailsView<T: SubscriptionDetails>: UIView {
    enum Metrics {
        case topMargin
        case leftMargin
        case rightMargin
        case bottomMargin
    }
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = Palette.ppColorGrayscale400.color
        label.text = " "
        return label
    }()
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
        label.textColor = Palette.ppColorGrayscale400.color
        label.text = " "
        return label
    }()
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale400.color
        label.numberOfLines = 0
        label.text = " "
        return label
    }()
    private lazy var changeButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(DefaultLocalizable.change.text, for: .normal)
        button.backgroundColor = Palette.ppColorGrayscale000.color
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.contentEdgeInsets = UIEdgeInsets.init(top: 8, left: 16, bottom: 8, right: 16)
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(SubscriptionDetailsView.changeButtonFired), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private var shouldLoadContraints:Bool = true
    
    var model: T?
    
    internal var metrics: [Metrics: CGFloat] = [
        .topMargin : 16,
        .leftMargin : 20,
        .rightMargin : 8,
        .bottomMargin : 16
    ]

    public var onChangeButtonTap:((Any?, T?) -> Void)? = nil {
        didSet {
            self.changeButton.isHidden = (self.onChangeButtonTap == nil)
        }
    }

    override func updateConstraints() {
        if shouldLoadContraints {
            loadConstraints()
            shouldLoadContraints = false
        }
        super.updateConstraints()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async {
            self.changeButton.layer.cornerRadius = self.changeButton.frame.height / 2
        }
    }
    
    private func loadConstraints() {
        let topMargin = metrics[.topMargin] ?? 0
        let leftMargin = metrics[.leftMargin] ?? 0
        let rightMargin = metrics[.rightMargin] ?? 0
        let bottomMargin = metrics[.bottomMargin] ?? 0
        
        addSubview(infoLabel)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addSubview(changeButton)

        // Info Label
        NSLayoutConstraint.activate([
            infoLabel.topAnchor.constraint(equalTo: topAnchor, constant: topMargin),
            infoLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: leftMargin),
            infoLabel.rightAnchor.constraint(equalTo: changeButton.leftAnchor, constant: -16)
        ])
        
        // ChangeButton
        changeButton.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .horizontal)
        NSLayoutConstraint.activate([
            changeButton.topAnchor.constraint(equalTo: infoLabel.topAnchor),
            changeButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -rightMargin)
        ])
        
        // Title Label
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: infoLabel.bottomAnchor, constant: 8),
            titleLabel.leftAnchor.constraint(equalTo: infoLabel.leftAnchor),
            titleLabel.rightAnchor.constraint(equalTo: infoLabel.rightAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: subtitleLabel.topAnchor, constant: -8),
        ])
        
        // Subtitle Label
        NSLayoutConstraint.activate([
            subtitleLabel.leftAnchor.constraint(equalTo: titleLabel.leftAnchor),
            subtitleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -rightMargin),
            subtitleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -bottomMargin)
        ])
    }
    
    private func update() {
        infoLabel.text = model?.getSubscriptionDetailInfo()
        titleLabel.text = model?.getSubscriptionDetailTitle()
        subtitleLabel.text = model?.getSubscriptionDetailSubtitle()
    }

    @objc
    private func changeButtonFired(sender: Any?) {
        onChangeButtonTap?(sender, model)
    }

    func viewMustUpdate() {
        update()
    }
    
    func setModel(_ model:T?) {
        self.model = model
        self.update()
    }
}
