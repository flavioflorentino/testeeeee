import UI

protocol PaymentDetailsWidget:ItemDetailsWidget {}

protocol PaymentDetails: AnyObject {
    func getPaymentDetailInfo() -> String
    func getPaymentDetailTitle() -> String
}

class PaymentDetailsView<T: PaymentDetails>: UIView {
    enum Metrics {
        case topMargin
        case leftMargin
        case rightMargin
        case bottomMargin
    }
    
    internal lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
        label.textColor = Palette.ppColorGrayscale500.color
        label.text = " "
        return label
    }()
    internal var metrics: [Metrics: CGFloat] = [
        .topMargin : 16,
        .leftMargin : 20,
        .rightMargin : 8,
        .bottomMargin : 16
    ]
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = Palette.ppColorGrayscale500.color
        label.text = " "
        return label
    }()
    private lazy var changeButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(DefaultLocalizable.change.text, for: .normal)
        button.backgroundColor = Palette.ppColorGrayscale100.color
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.contentEdgeInsets = UIEdgeInsets.init(top: 8, left: 16, bottom: 8, right: 16)
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(PaymentDetailsView.changeButtonFired), for: .touchUpInside)
        return button
    }()
    private lazy var buttonContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = true
        return view
    }()
    
    private var shouldLoadContraints: Bool = true
    private var containerChangeButtonWidthConstraint: NSLayoutConstraint? = nil
    
    var isChangeButtonHidden: Bool = false {
        didSet {
            self.containerChangeButtonWidthConstraint?.constant = self.isChangeButtonHidden ? 0 : 75
            UIView.animate(withDuration: 0.25, animations: {
                self.layoutIfNeeded()
            })
        }
    }
    var model: T? = nil
    var onChangeButtonTap: ((Any?) -> Void)? = nil

    override func updateConstraints() {
        if shouldLoadContraints {
            loadConstraints()
            shouldLoadContraints = false
        }
        super.updateConstraints()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        //Trick - UIView Lifecycle doesnt have a method with its size on AutoLayout
        DispatchQueue.main.async {
            self.changeButton.layer.cornerRadius = self.changeButton.frame.height / 2
        }
    }
    
    //MARK: Private Methods
    internal func loadConstraints() {
        let topMargin = self.metrics[.topMargin] ?? 0
        let leftMargin = self.metrics[.leftMargin] ?? 0
        let rightMargin = self.metrics[.rightMargin] ?? 0
        let bottomMargin = self.metrics[.bottomMargin] ?? 0
        
        addSubview(infoLabel)
        addSubview(titleLabel)
        addSubview(buttonContainerView)
        buttonContainerView.addSubview(self.changeButton)
        
        // Info Label
        NSLayoutConstraint.activate([
            infoLabel.topAnchor.constraint(equalTo: topAnchor, constant: topMargin),
            infoLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: leftMargin),
            infoLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -rightMargin)
        ])
        
        // Title Label
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: infoLabel.bottomAnchor, constant: 6),
            titleLabel.leftAnchor.constraint(equalTo: infoLabel.leftAnchor),
            titleLabel.rightAnchor.constraint(equalTo: buttonContainerView.leftAnchor, constant: -12),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -bottomMargin),
            ])
        
        // Container
        let containerViewWidthConstraint = buttonContainerView.widthAnchor.constraint(equalToConstant: isChangeButtonHidden ? 0 : 75)
        containerChangeButtonWidthConstraint = containerViewWidthConstraint
        NSLayoutConstraint.activate([
            containerViewWidthConstraint,
            buttonContainerView.topAnchor.constraint(equalTo: topAnchor, constant: topMargin),
            buttonContainerView.rightAnchor.constraint(equalTo: rightAnchor, constant: -rightMargin),
            buttonContainerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -bottomMargin),
            ])
        
        // ChangeButton
        NSLayoutConstraint.activate([
            changeButton.centerXAnchor.constraint(equalTo: buttonContainerView.centerXAnchor),
            changeButton.centerYAnchor.constraint(equalTo: buttonContainerView.centerYAnchor)
        ])
    }
    
    internal func update() {
        infoLabel.text = model?.getPaymentDetailInfo()
        titleLabel.text = model?.getPaymentDetailTitle()
    }
    
    //MARK: Handlers
    
    @objc private func changeButtonFired(sender: Any?) {
        self.onChangeButtonTap?(sender)
    }
    
    // MARK: Public methods
    
    func viewMustUpdate() {
        update()
    }
    
    func setModel(_ model:T?) {
        self.model = model
        self.update()
    }
}
