import UI

protocol PaymentDetailsWithSubtitle:PaymentDetails {
    func getPaymentDetailSubtitle() -> String
}

final class PaymentDetailsWithSubtitleView<T: PaymentDetailsWithSubtitle>: PaymentDetailsView<T> {
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale300.color
        label.numberOfLines = 2
        label.text = " "
        return label
    }()
    
    override func loadConstraints() {
        super.loadConstraints()
        let bottomMargin = metrics[.bottomMargin] ?? 0
        if let titleBottomConstraint = self.constraints.first(where: { $0.firstItem === self.titleLabel && $0.firstAttribute == .bottom}) {
            titleBottomConstraint.isActive = false
        }
        
        addSubview(subtitleLabel)
        NSLayoutConstraint.activate([
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8),
            subtitleLabel.leftAnchor.constraint(equalTo: titleLabel.leftAnchor),
            subtitleLabel.rightAnchor.constraint(equalTo: titleLabel.rightAnchor),
            subtitleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -bottomMargin)
        ])
    }
    
    override internal func update() {
        super.update()
        subtitleLabel.text = model?.getPaymentDetailSubtitle()
    }
}
