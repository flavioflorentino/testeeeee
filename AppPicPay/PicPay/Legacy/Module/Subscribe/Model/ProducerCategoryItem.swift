//
//  ProducerCategoryItem.swift
//  PicPay
//
//  Created by Vagner Orlandi on 19/12/17.
//

import UIKit
import SwiftyJSON

final class ProducerCategoryItem: NSObject, BaseApiResponse {
    enum CategoryStatus:String {
        case open = "open"
        case closed = "closed"
        case unknown = ""
    }
    
    var id:String = ""
    var name:String = ""
    var categoryDescription:String = ""
    var isFixedValue:Bool = true
    var accountId:String = ""
    var status:CategoryStatus = .unknown
    var plans:[ProducerPlanItem] = []
    var cards:[WidgetsStack] = []
    var additionalOptIn: ProducerPlanItem.AdditionalOptIn? = nil
    
    required init?(json: JSON) {
        self.id = json["id"].string ?? ""
        self.name = json["name"].string ?? ""
        self.categoryDescription = json["description"].string ?? ""
        self.isFixedValue = json["is_fixed_price"].bool ?? true
        self.accountId = json["account_id"].string ?? ""
        self.status = CategoryStatus(rawValue: json["status"].string ?? "") ?? .unknown
        if let plans = json["plans"].array {
            for jsonPlan in plans {
                if let plan = ProducerPlanItem(json: jsonPlan) {
                    self.plans.append(plan)
                }
            }
        }
        
        if let cardListJson = json["_ui"]["cards"].array {
            for cardJson in cardListJson {
                if let card = WidgetsStack(json: cardJson["panel"]) {
                    cards.append(card)
                }
            }
        }
        
        if json["_ui"]["additional_opt_in"].exists(), let optIn = ProducerPlanItem.AdditionalOptIn(json: json["_ui"]["additional_opt_in"]) {
            additionalOptIn = optIn
        }
    }
}
