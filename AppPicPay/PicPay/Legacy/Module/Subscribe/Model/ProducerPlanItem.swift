//
//  ProducerPlanItem.swift
//  PicPay
//
//  Created by Vagner Orlandi on 08/12/17.
//

import SwiftyJSON

final class ProducerPlanItem: NSObject, BaseApiResponse {
    
    enum PlanStatus:String {
        case open = "open"
        case closed = "closed"
        case unkown = ""
    }
    
    var id:String = ""
    var categoryId:String = ""
    var name:String = ""
    var planDescription:String = ""
    var paymentDetails:String = ""
    var price:Double = 0.0
    var periodicityDescription:String = ""
    var isFixedPrice:Bool = true
    var status:PlanStatus = .open
    var isAddressNeeded:Bool = false
    var isCurrentSubscription:Bool = false
    var changesCards:[WidgetsStack]? = nil
    
    enum AdditionalOptInType: String{
        case alert = "alert"
    }
    
    struct AdditionalOptIn {
        var type: AdditionalOptInType
        var data: Any
        
        init?(json: JSON){
            guard let type = AdditionalOptInType(rawValue: json["type"].string ?? "") else { return nil}
            
            switch type {
            case .alert:
                guard let alert = Alert(json: json["data"]) else { return nil}
                self.data = alert
                break
                
            }
            
            self.type = type
        }
    }
    
    var additionalOptIn: AdditionalOptIn? = nil
    
    override init() {
        super.init()
    }
    
    required init?(json: JSON) {
        self.id = json["id"].string ?? ""
        self.categoryId = json["category_id"].string ?? ""
        self.name = json["name"].string ?? ""
        self.price = json["value"].double ?? 0.0
        self.planDescription = json["description"].string ?? ""
        self.paymentDetails = json["billing_description"].string ?? ""
        self.periodicityDescription = json["periodicity_description"].string ?? ""
        self.isFixedPrice = json["is_fixed_price"].bool ?? true
        self.status = PlanStatus(rawValue: json["status"].string ?? "") ?? .unkown
        self.isAddressNeeded = json["address_required"].bool ?? false
        self.isCurrentSubscription = false
    }
}

