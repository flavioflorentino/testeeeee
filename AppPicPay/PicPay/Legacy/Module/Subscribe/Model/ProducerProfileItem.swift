//
//  ProducerProfileItem.swift
//  PicPay
//
//  Created by Vagner Orlandi on 08/12/17.
//

import UIKit
import SwiftyJSON

final class ProducerProfileItem: NSObject, BaseApiResponse {
    var id:String = ""
    var name:String = ""
    var username:String = ""
    var profileImageUrl:String? = ""
    var backgroundImageUrl:String? = ""
    var profileDescription:String = ""
    var fullProfileDescription:String? = nil
    var fullProfileDescriptionMarkdown:String? = nil
    
    override init() {
        super.init()
    }
    
    required init?(json: JSON) {
        self.id = json["id"].string ?? ""
        self.name = json["name"].string ?? ""
        self.username = json["username"].string ?? ""
        self.backgroundImageUrl = json["img_url"].string
        self.profileImageUrl = json["img_url_small"].string
        self.profileDescription = json["bio"].string ?? ""
        
        self.fullProfileDescription = {$0.isEmpty ? nil : $0}(json["description"].string ?? "")
        self.fullProfileDescriptionMarkdown = {$0.isEmpty ? nil : $0}(json["description_markdown"].string ?? "")
    }
    
    func update(using producerProfile:ProducerProfileItem) {
        self.name = producerProfile.name
        self.username = producerProfile.username
        self.backgroundImageUrl = producerProfile.backgroundImageUrl
        self.profileImageUrl = producerProfile.profileImageUrl
        self.profileDescription = producerProfile.profileDescription
        self.fullProfileDescription = producerProfile.fullProfileDescription
        self.fullProfileDescriptionMarkdown = producerProfile.fullProfileDescriptionMarkdown
    }
    
    static func photoPlaceholder() -> UIImage {
        return #imageLiteral(resourceName: "avatar_producer")
    }
}

