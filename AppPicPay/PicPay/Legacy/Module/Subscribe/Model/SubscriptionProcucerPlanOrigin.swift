import Foundation

enum SubscriptionProcucerPlanOrigin: String {
    case feed
    case search = "busca"
    case newSubscription = "fazer nova assinatura"
    case changePlan = "alterar plano"
    case deeplink
}
