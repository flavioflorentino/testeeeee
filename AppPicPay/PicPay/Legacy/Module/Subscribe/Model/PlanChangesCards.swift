import Foundation
import SwiftyJSON

final class PlanChangesCards: BaseApiResponse {
    var list: [WidgetsStack] = []
    
    init() { }
    
    required init?(json: JSON) {
        if let listObject = json.array {
            for j in listObject {
                if let o = WidgetsStack(json: j["panel"]) {
                    list.append(o)
                }
            }
        }
    }
}
