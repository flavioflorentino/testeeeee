import Foundation
import SwiftyJSON

final class PlanSubscriptionTransaction: BaseApiResponse {
    var subscription: ProducerPlanItem?
    var price: Double = 0.0
    var transactionId: String = ""
    var transactionDate: String = ""
    var transactionValue: Double = 0.0
    var receipts: [ReceiptWidgetItem] = []
    var visibility: String = ""
    var creditcardId: String = ""
    var recurringStatus: String = ""
    var status: String = ""
    var subscriptionId: Int = 0
    
    required init?(json: JSON) {
        if json.isEmpty {
            return nil
        }
        self.subscription = ProducerPlanItem(json: json)
        self.price = json["payment"]["value"].double ?? 0.0
        self.transactionId = json["payment"]["transaction"]["id"].string ?? ""
        self.transactionDate = json["payment"]["transaction"]["date"].string ?? ""
        self.transactionValue = json["payment"]["transaction"]["value"].double ?? 0.0
        self.subscriptionId = json["id"].int ?? 0
        for widget in json["payment"]["receipt"].arrayValue {
            if let widgetItem = WSReceipt.createReceiptWidgetItem(jsonDict: widget.dictionaryObject ?? [:]) {
                self.receipts.append(widgetItem)
            }
        }
    }
}
