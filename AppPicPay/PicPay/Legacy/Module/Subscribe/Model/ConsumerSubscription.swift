//
//  ConsumerSubscription.swift
//  PicPay
//
//  ☝🏽☁️ Created by Luiz Henrique Guimarães on 06/12/17.
//

import UIKit
import SwiftyJSON

final class ConsumerSubscription: NSObject, BaseApiResponse {
    
    enum Status : String {
        case active =  "active" // ativo
        case cancelled = "cancelled" // cancelado e dasativado
        case deleted = "deleted"
        case activeCancelled = "pending_cancellation" // cancelado mas ativo até o final do contrato
        case waitingPayment = "payment_delayed" // vencido aguardando pagamento
        case waitingAnalysis = "under_analysis"
        case unknown = "unknown"
    }
    
    var id:String = ""
    var subscriberId:String = ""
    var producerId:String = ""
    var planId:String = ""
    var price:Double = 0.00
    var producer:ProducerProfileItem
    var subscriptionPlan:ProducerPlanItem
    var consumerAddress:ConsumerAddressItem?
    var billingDescription:String = ""
    var lastBillingText:String = ""
    var isAddressMissing:Bool = false
    var extraText: String = "<font color=\"ED1846\">A &uacute;ltima tentativa de cobran&ccedil;a no dia 88/88/88 &agrave;s 88:88 falhou. Certifique-se de que voc&ecirc; tem saldo ou algum m&eacute;todo de pagamento v&aacute;lido cadastrado e tente fazer o pagamento novamente. <br><br><b>Caso voc&ecirc; n&atilde;o consiga pagar at&eacute; o dia 88/88/88, sua assinatura ser&aacute; cancelada.</b></font>"
    var status: Status = .unknown
    var cards: [WidgetsStack] = []
    
    override init() {
        self.producer = ProducerProfileItem()
        self.subscriptionPlan = ProducerPlanItem()
        self.consumerAddress = nil
        super.init()
    }
    
    required init?(json:JSON) {
        guard let id = json["id"].string,
            let plan = ProducerPlanItem(json: json["plan"]),
            let producer = ProducerProfileItem(json: json["producer"])
            else {
            return nil
        }
        
        self.id = id
        self.subscriptionPlan = plan
        self.producer = producer
        
        self.price = json["value"].double ?? 0.00
        self.subscriberId = json["subscriber_id"].string ?? ""
        self.producerId = json["producer_id"].string ?? ""
        self.planId = json["plan_id"].string ?? ""
        self.billingDescription = json["billing_date_description"].string ?? ""
        self.extraText = json["warning_text"].string ?? ""
        self.lastBillingText = json["last_billing_date_description"].string ?? ""
        self.isAddressMissing = json["missing_address"].bool ?? false
        self.consumerAddress = json["address"].decoded()
        self.status = Status(rawValue: json["status"].string ?? "") ?? .unknown
        
        if let cardListJson = json["_ui"]["cards"].array {
            for cardJson in cardListJson {
                if let card = WidgetsStack(json: cardJson["panel"]) {
                    cards.append(card)
                }
            }
        }
    }
}
