enum SubscribeLocalizable: String, Localizable {
    case transaction
    case canceledSubscriptions
    case activeSubscriptions
    case mySubscriptions
    case changeHistory
    case notPossibleProvide
    case planChangesCouldNotBeRetrieved
    case hasNotChangedSince
    case selectedPlan
    case perMonth
    case chooseYourSubscription
    case subscriptionYouChoseNeeds
    case enterAddress
    case notPossibleDisplay
    case seePlans
    case historic
    case couldntLoadHistory
    case pleaseTryAgainLater
    case newSubscription
    case noInternetConnection
    case sureYouWantCancelSubscription
    case yourNextMonthlyFee
    case wantCancel
    case iDontWantCancel
    case youWantChangeAddress
    case deliveryContemplatedByYourPlan
    case viewPaymentHistory
    case cancelSubscription
    case resign
    case removeFromList
    case subscriptionReactivated
    case paySubscription
    case signAnotherPlan
    case couldntaShowMore
    case viewPlanChangeHistory
    case planDetailsCouldNotBeDisplayed
    case planSigned
    case currentlySubscribe
    case chargedEveryDay
    case lastCharged
    case signatureChosen
    case level1
    case historyCouldNot
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .subscribe
    }
    
}
