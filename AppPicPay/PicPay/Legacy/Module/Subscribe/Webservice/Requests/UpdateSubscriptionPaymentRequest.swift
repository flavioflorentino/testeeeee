import Foundation
import SwiftyJSON

final class UpdateSubscriptionPaymentRequest: SubscriptionPaymentRequest {
    var subscription: ConsumerSubscription
    
    init(subscription: ConsumerSubscription,
         paymentManager: PPPaymentManager,
         privacySettings: String,
         biometry: Bool,
         hasAnyPreviousErrors: Bool) {
        self.subscription = subscription
        super.init(paymentManager: paymentManager,
                   privacySettings: privacySettings,
                   biometry: biometry,
                   hasAnyPreviousErrors: hasAnyPreviousErrors)
    }
    
    override func toParams() -> [String: Any] {
        if let consumerAddress = self.subscription.consumerAddress, let addressId = consumerAddress.id {
            var params = super.toParams()
            params["address_id"] = addressId
            return params
        } else {
            return super.toParams()
        }
    }
}
