import Foundation
import SwiftyJSON

final class SubscribePlanPaymentRequest: SubscriptionPaymentRequest {
    var plan: ProducerPlanItem
    var address: ConsumerAddressItem?
    
    init(plan: ProducerPlanItem,
         address: ConsumerAddressItem? = nil,
         paymentManager: PPPaymentManager,
         privacySettings: String,
         biometry: Bool,
         hasAnyPreviousErrors: Bool) {
        self.plan = plan
        self.address = address
        super.init(paymentManager: paymentManager,
                   privacySettings: privacySettings,
                   biometry: biometry,
                   hasAnyPreviousErrors: hasAnyPreviousErrors)
    }
    
    override func toParams() -> [String: Any] {
        if let consumerAddress = self.address, let addressId = consumerAddress.id {
            var params = super.toParams()
            params["address_id"] = addressId
            return params
        } else {
            return super.toParams()
        }
    }
}
