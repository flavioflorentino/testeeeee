import Foundation
import SwiftyJSON

class SubscriptionPaymentRequest: BaseApiRequest {
    var paymentManager: PPPaymentManager
    var privacySettings: String
    var biometry: Bool
    var hasAnyPreviousErrors: Bool
    
    init(paymentManager: PPPaymentManager, privacySettings: String, biometry: Bool, hasAnyPreviousErrors: Bool) {
        self.paymentManager = paymentManager
        self.privacySettings = privacySettings
        self.biometry = biometry
        self.hasAnyPreviousErrors = hasAnyPreviousErrors
    }
    
    func toParams() -> [String: Any] {
        return [
            "total_value": paymentManager.subtotal.stringValue,
            "credit": paymentManager.balanceTotal().stringValue,
            "consumer_credit_card_id": String(CreditCardManager.shared.defaultCreditCardId),
            "feed_visibility": privacySettings,
            "duplicated": hasAnyPreviousErrors ? 1 : 0,
            "biometry": self.biometry
        ]
    }
}
