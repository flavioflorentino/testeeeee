import Core
import Foundation

enum SubscriptionEndpoints: ApiEndpointExposable {
    case subscriptionsByStatus(status: String)
    case subscriptions(isActive: Bool)
    case delete(id: String)
    case categories(producerId: String)
    case subscribePlan(request: SubscribePlanPaymentRequest, password: String)
    case cancel(id: String)
    case markSeenUpdates(id: String)
    case resubscribe(id: String, password: String)
    case updatePayment(request: UpdateSubscriptionPaymentRequest, password: String)
    case updateAddress(subscriptionId: String, addressId: String)
    case subscription(id: String)
    case producerProfile(id: String)
    case producerPlanUpdates(id: String, subscriptionId: String?)
        
    var path: String {
        switch self {
        case .subscriptionsByStatus, .subscriptions:
            return "membership/subscribers/subscriptions"
        case let .delete(id):
            return "membership/subscribers/subscriptions/\(id)"
        case let .categories(producerId):
            return "membership/producers/\(producerId)/categories"
        case let .subscribePlan(request, _):
            return "membership/subscribers/plans/\(request.plan.id)/subscription"
        case let .cancel(id):
            return "membership/subscribers/subscriptions/\(id)/cancel"
        case let .markSeenUpdates(id):
            return "membership/subscribers/subscriptions/\(id)/plan_last_update_seen"
        case let .resubscribe(id, _):
            return "membership/subscribers/subscriptions/\(id)/resubscribe"
        case let .updatePayment(request, _):
            return "membership/subscribers/subscriptions/\(request.subscription.id)/payment"
        case let .updateAddress(subscriptionId, _):
            return "membership/subscribers/subscriptions/\(subscriptionId)/address"
        case let .subscription(id):
            return "membership/subscribers/subscriptions/\(id)"
        case let .producerProfile(id):
            return "membership/producers/\(id)"
        case let .producerPlanUpdates(id, _):
            return "membership/plans/\(id)/update_history"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .subscriptionsByStatus, .subscriptions, .categories, .subscription, .producerProfile, .producerPlanUpdates:
            return .get
        case .delete:
            return .delete
        case .subscribePlan, .updatePayment:
            return .post
        case .cancel, .markSeenUpdates, .resubscribe, .updateAddress:
            return .patch
        }
    }
    
    var body: Data? {
        switch self {
        case let .subscribePlan(request, _):
            return request.toParams().toData()
        case let .updatePayment(request, _):
            return request.toParams().toData()
        case let .updateAddress(_ , addressId):
            return ["address_id": addressId].toData()
        default:
            return nil
        }
    }
    
    var shouldAppendBody: Bool {
        body != nil && method != .get
    }
    
    var parameters: [String : Any] {
        switch self {
        case let .subscriptionsByStatus(status):
            guard !status.isEmpty else {
                return [:]
            }
            return ["status": status]
        case let .subscriptions(isActive):
            return ["is_active" : (isActive ? "true" : "false")]
        case let .producerPlanUpdates(_ , subscriptionId):
            guard let subscriptionId = subscriptionId else {
                return [:]
            }
            return ["subscription_id": subscriptionId]
        default:
            return [:]
        }
    }
    
    var contentType: ContentType {
        (method == .get || method == .patch) ? .textPlain : .applicationJson
    }
    
    var customHeaders: [String : String] {
        switch self {
        case .categories:
            return ["X-Api-Version": "mobile.v2"]
        case let .subscribePlan(_, password),
             let .resubscribe(_, password),
             let .updatePayment(_, password):
            return ["password": password]
        default:
            return [:]
        }
    }
}
