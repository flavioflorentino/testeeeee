import Core
import FeatureFlag
import Foundation
import SwiftyJSON

final class ApiSubscription: BaseApi {
    typealias SubscriptionsResponse = BaseApiListResponse<ConsumerSubscription>
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    var defauldCard: CardBank? {
        return CreditCardManager.shared.defaultCreditCard
    }
    
    func subscriptions(by status: String = "", _ completion: @escaping ((PicPayResult<SubscriptionsResponse>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonSubscriptionsToCore) else {
            var subscriptionsEndpoint: String = WebServiceInterface.apiEndpoint(kWsUrlMembershipSubscriberSubscriptions)
            if !status.isEmpty {
                subscriptionsEndpoint.append("?status=\(status)")
            }
            requestManager
                .apiRequest(endpoint: subscriptionsEndpoint, method: .get)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        Api<Data>(endpoint: SubscriptionEndpoints.subscriptionsByStatus(status: status)).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func subscriptions(isActive: Bool, _ completion: @escaping ((PicPayResult<SubscriptionsResponse>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonSubscriptionsToCore) else {
            var subscriptionsEndpoint: String = WebServiceInterface.apiEndpoint(kWsUrlMembershipSubscriberSubscriptions)
            subscriptionsEndpoint.append("?is_active=\(isActive ? "true" : "false")")
            requestManager
                .apiRequest(endpoint: subscriptionsEndpoint, method: .get)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        Api<Data>(endpoint: SubscriptionEndpoints.subscriptions(isActive: isActive)).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func categoriesPlans(by producerId: String, _ completion: @escaping ((PicPayResult<CategoryPlans>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonSubscriptionsToCore) else {
            let api = kWsUrlMembershipCategoriesByProducerId.replacingOccurrences(of: "{id}", with: producerId)
            let endpoint = WebServiceInterface.apiEndpoint(api) ?? ""
            let headers: [String: String] = [
                "X-Api-Version": "mobile.v2"
            ]
            requestManager
                .apiRequest(endpoint: endpoint, method: .get, headers: headers)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        Api<Data>(endpoint: SubscriptionEndpoints.categories(producerId: producerId)).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func subscribePlan(_ request: SubscribePlanPaymentRequest,
                       pin: String,
                       _ completion: @escaping ((PicPayResult<PlanSubscriptionTransaction>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonSubscriptionsToCore) else {
            let api = kWsUrlMembershipSubscribeByPlanId.replacingOccurrences(of: "{id}", with: "\(request.plan.id)")
            let endpoint = WebServiceInterface.apiEndpoint(api) ?? ""
            requestManager
                .apiRequest(endpoint: endpoint, method: .post, parameters: request.toParams(), pin: pin)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        Api<Data>(endpoint: SubscriptionEndpoints.subscribePlan(request: request, password: pin)).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func cancelSubscription(by id: String, _ completion: @escaping ((PicPayResult<BaseApiEmptyResponse>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonSubscriptionsToCore) else {
            let subscriptionsEndpoint = WebServiceInterface
                .apiEndpoint(kWsUrlMembershipCancelBySubscriptionId)
                .replacingOccurrences(of: "{id}", with: id)
            requestManager
                .apiRequest(endpoint: subscriptionsEndpoint, method: .patch)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        Api<Data>(endpoint: SubscriptionEndpoints.cancel(id: id)).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func deleteSubscription(by id: String, _ completion: @escaping ((PicPayResult<BaseApiGenericResponse>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonSubscriptionsToCore) else {
            var subscriptionsEndpoint: String = WebServiceInterface.apiEndpoint(kWsUrlMembershipSubscriberSubscriptions)
            subscriptionsEndpoint.append("/\(id)")
            requestManager
                .apiRequest(endpoint: subscriptionsEndpoint, method: .delete)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        Api<Data>(endpoint: SubscriptionEndpoints.delete(id: id)).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func markSeenSubscriptionUpdates(by id: String, _ completion: @escaping ((PicPayResult<BaseApiEmptyResponse>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonSubscriptionsToCore) else {
            let subscriptionsEndpoint = WebServiceInterface
                .apiEndpoint(kWsUrlMembershipMarkSeenUpdatesBySubscriptionId)
                .replacingOccurrences(of: "{id}", with: id)
            requestManager
                .apiRequest(endpoint: subscriptionsEndpoint, method: .patch)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        Api<Data>(endpoint: SubscriptionEndpoints.markSeenUpdates(id: id)).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func reSubscribe(by id: String, pin: String, _ completion: @escaping ((PicPayResult<BaseApiEmptyResponse>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonSubscriptionsToCore) else {
            let subscriptionsEndpoint = WebServiceInterface
                .apiEndpoint(kWsUrlMembershipReSubscribeBySubscriptionId)
                .replacingOccurrences(of: "{id}", with: id)
            requestManager
                .apiRequest(endpoint: subscriptionsEndpoint, method: .patch, pin: pin)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        Api<Data>(endpoint: SubscriptionEndpoints.resubscribe(id: id, password: pin)).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func updateSubscriptionPayment(request: UpdateSubscriptionPaymentRequest,
                                   pin: String,
                                   _ completion: @escaping ((PicPayResult<PlanSubscriptionTransaction>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonSubscriptionsToCore) else {
            let api = kWsUrlMembershipUpdatePaymentBySubscriptionId
                .replacingOccurrences(of: "{id}", with: "\(request.subscription.id)")
            let endpoint = WebServiceInterface.apiEndpoint(api) ?? ""
            requestManager
                .apiRequest(endpoint: endpoint, method: .post, parameters: request.toParams(), pin: pin)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        Api<Data>(endpoint: SubscriptionEndpoints.updatePayment(request: request, password: pin)).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func updateConsumerAddressSubscription(_ subscriptionId: String,
                                           newAddressId: String,
                                           _ completion: @escaping ((PicPayResult<ConsumerSubscription>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonSubscriptionsToCore) else {
            let api = kWsUrlMembershipUpdateAddressBySubscriptionId.replacingOccurrences(of: "{id}", with: subscriptionId)
            let endpoint = WebServiceInterface.apiEndpoint(api) ?? ""
            let params: [String: Any] = ["address_id": newAddressId]
            requestManager
                .apiRequest(endpoint: endpoint, method: .patch, parameters: params)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        let endpoint = SubscriptionEndpoints.updateAddress(subscriptionId: subscriptionId, addressId: newAddressId)
        Api<Data>(endpoint: endpoint).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func getSubscription(by id: String, _ completion: @escaping ((PicPayResult<ConsumerSubscription>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonSubscriptionsToCore) else {
            let api = kWsUrlMembershipSubscriptionById.replacingOccurrences(of: "{id}", with: id)
            let endpoint = WebServiceInterface.apiEndpoint(api) ?? ""
            requestManager
                .apiRequest(endpoint: endpoint, method: .get)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        Api<Data>(endpoint: SubscriptionEndpoints.subscription(id: id)).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    // WARNING: Atenção que tem que fazer chamada com o cache para funcionar
    func getProducerProfile(by id: String, _ completion: @escaping ((PicPayResult<ProducerProfileItem>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonSubscriptionsToCore) else {
            let api = kWsUrlMembershipProducerById.replacingOccurrences(of: "{id}", with: id)
            let endpoint = WebServiceInterface.apiEndpoint(api) ?? ""
            requestManager
                .apiRequestWithCache(onCacheLoaded: { completion(PicPayResult.success($0)) }, endpoint: endpoint, method: .get)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        Api<Data>(endpoint: SubscriptionEndpoints.producerProfile(id: id)).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func getProducerPlanUpdates(by id: String,
                                subscriptionId: String? = nil,
                                _ completion: @escaping ((PicPayResult<PlanChangesCards>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonSubscriptionsToCore) else {
            let api = kWsUrlMembershipPlanHistoryById.replacingOccurrences(of: "{id}", with: id)
            var endpoint = WebServiceInterface.apiEndpoint(api) ?? ""
            if let subscription = subscriptionId, !subscription.isEmpty {
                endpoint.append("?subscription_id=\(subscription)")
            }
            requestManager
                .apiRequest(endpoint: endpoint, method: .get)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        Api<Data>(endpoint: SubscriptionEndpoints.producerPlanUpdates(id: id, subscriptionId: subscriptionId)).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool {
        return CvvRegisterFlowCoordinator.cvvFlowEnable(cardBank: cardBank, cardValue: cardValue, paymentType: .subscription)
    }
}
