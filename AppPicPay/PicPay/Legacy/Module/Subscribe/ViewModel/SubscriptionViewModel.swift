import UIKit
import Core
import FeatureFlag
import PCI
import SwiftyJSON

final class SubscriptionViewModel: NSObject, PayableSubscriptionViewModel {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    
    var subscription:ConsumerSubscription
    var api:ApiSubscription
    
    private let service: SubscriptionServiceProtocol
    
    init(apiSubscription: ApiSubscription = ApiSubscription(), consumer subscription:ConsumerSubscription, service: SubscriptionServiceProtocol = SubscriptionService()) {
        self.subscription = subscription
        self.api = apiSubscription
        self.service = service
        super.init()
    }
    
    func cancelSubscription(_ completion: @escaping ((Error?) ->Void) ) {
        self.api.cancelSubscription(by: "\(self.subscription.id)") { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success:
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }

    func deleteSubscription(_ completion: @escaping ((Error?) ->Void) ) {
        self.api.deleteSubscription(by: "\(self.subscription.id)") { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success:
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }
    
    func reSubscribe(pin:String, _ completion: @escaping ((Error?) ->Void) ) {
        self.api.reSubscribe(by: "\(self.subscription.id)", pin: pin) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success:
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }
    
    func lastSubscriptionUpdatesSeen(_ completion: @escaping ((Error?) ->Void) ) {
        self.api.markSeenSubscriptionUpdates(by: "\(self.subscription.id)") { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success:
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }
    
    func updateSubscriptionAddress(new consumerAddress:ConsumerAddressItem, _ completion: @escaping ((Error?) ->Void) ) {
        self.api.updateConsumerAddressSubscription("\(self.subscription.id)", newAddressId: "\(consumerAddress.id ?? -1)") { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success:
                    self.subscription.consumerAddress = consumerAddress
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }

    static func getSubscription(by id:String, _ completion: @escaping ((ConsumerSubscription?, Error?) -> Void), api:ApiSubscription = ApiSubscription()) {
        api.getSubscription(by: id) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let result):
                    completion(result, nil)
                case .failure(let error):
                    completion(nil, error)
                }
            }
        }
    }
    
    func getSubscritpionProducerPlanUpdates(_ completion: @escaping ((Error?) ->Void) ) {
        self.api.getProducerPlanUpdates(by: "\(self.planItem().id)", subscriptionId: self.subscription.id) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let result):
                    self.planItem().changesCards = result.list
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }
    
    //MARK: PayableSubscriptionViewModel
    
    func planItem() -> ProducerPlanItem {
        return self.subscription.subscriptionPlan
    }
    
    func producerItem() -> ProducerProfileItem {
        return self.subscription.producer
    }
    
    func addressItem() -> ConsumerAddressItem? {
        return self.subscription.consumerAddress
    }
    
    func changeAddress(_ address: ConsumerAddressItem?) {
        self.subscription.consumerAddress = address
    }
    
    func checkNeedCvv(paymentManager: PPPaymentManager) -> Bool {
        guard let cardBank = api.defauldCard, let cardValue = paymentManager.cardTotal()?.doubleValue else {
            return false
        }
        
        return api.pciCvvIsEnable(cardBank: cardBank, cardValue: cardValue)
    }
    
    func requestPayment(
        pin: String,
        paymentManager: PPPaymentManager,
        privacySettings: String,
        biometry: Bool,
        hasPreviousErrors: Bool = false,
        informedCvv: String?,
        _ completion: @escaping ((PlanSubscriptionTransaction?, Error?) -> Void)
    ) {
        if FeatureManager.isActive(.pciSubscription) {
            subscriptionPayPCI(
                subscriptionId: subscription.id,
                pin: pin,
                paymentManager: paymentManager,
                privacySettings: privacySettings,
                biometry: biometry,
                hasPreviousErrors: hasPreviousErrors,
                informedCvv: informedCvv,
                completion: completion
            )
            return
        }
        
        let request = UpdateSubscriptionPaymentRequest(subscription: self.subscription, paymentManager: paymentManager, privacySettings: privacySettings, biometry: biometry, hasAnyPreviousErrors: hasPreviousErrors)
        api.updateSubscriptionPayment(request: request, pin: pin) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let result):
                    completion(result, nil)
                case .failure(let error):
                    completion(nil, error)
                }
            }
        }
    }
    
    private func saveCvv(informedCvv: String?) {
        guard let cvv = informedCvv else {
            return
        }
        
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        dependencies.keychain.set(key: KeychainKeyPF.cvv(id), value: cvv)
    }
    
    private func subscriptionPayPCI(
        subscriptionId: String,
        pin: String,
        paymentManager: PPPaymentManager,
        privacySettings: String,
        biometry: Bool,
        hasPreviousErrors: Bool = false,
        informedCvv: String?,
        completion: @escaping (PlanSubscriptionTransaction?, Error?) -> Void
    ) {
        let payload = createPayload(
            pin: pin,
            paymentManager: paymentManager,
            privacySettings: privacySettings,
            biometry: biometry,
            hasPreviousErrors: hasPreviousErrors,
            informedCvv: informedCvv
        )
        
        service.subscriptionPay(
            password: pin,
            subscriptionId: subscriptionId,
            payload: payload,
            isNewArchitecture: false
        ) { [weak self] result in
            switch result {
            case .success(let data):
                guard let transaction = self?.parseTransaction(with: data) else {
                    return
                }
                
                DispatchQueue.main.async {
                    self?.saveCvv(informedCvv: informedCvv)
                    completion(transaction, nil)
                }
            case .failure(let apiError):
                self?.handleFailure(apiError, completion: completion)
            }
        }
    }
    
    private func createPayload(
        pin: String,
        paymentManager: PPPaymentManager,
        privacySettings: String,
        biometry: Bool,
        hasPreviousErrors: Bool = false,
        informedCvv: String?
    ) -> PaymentPayload<SubscriptionPaymentPayload> {
        let subscriptionPayload = SubscriptionPaymentPayload(
            biometry: biometry,
            duplicated: hasPreviousErrors ? 1 : 0,
            total: paymentManager.subtotal.stringValue,
            credit: paymentManager.balanceTotal().stringValue,
            feedVisibility: privacySettings,
            creditCardId: String(CreditCardManager.shared.defaultCreditCardId)
        )
        let cvv = cvvPayload(paymentManager: paymentManager, informedCvv: informedCvv)
        return PaymentPayload(cvv: cvv, generic: subscriptionPayload)
    }
    
    private func cvvPayload(paymentManager: PPPaymentManager, informedCvv: String?) -> CVVPayload? {
        guard let cvv = informedCvv else {
            return createLocalStoreCVVPayload(paymentManager: paymentManager)
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func createLocalStoreCVVPayload(paymentManager: PPPaymentManager) -> CVVPayload? {
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        guard
            let cvv = dependencies.keychain.getData(key: KeychainKeyPF.cvv(id)),
            let cardValue = paymentManager.cardTotal()?.doubleValue,
            !cardValue.isZero
            else {
                return nil
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func parseTransaction(with data: Data?) -> PlanSubscriptionTransaction? {
        guard let jsonData = data, let json = try? JSON(data: jsonData) else {
            return nil
        }
        
        return PlanSubscriptionTransaction(json: json)
    }
    
    private func handleFailure(_ apiError: ApiError, completion: @escaping (PlanSubscriptionTransaction?, Error?) -> Void) {
        DispatchQueue.main.async {
            completion(nil, apiError.picpayError)
        }
    }
}
