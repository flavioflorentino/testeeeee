import Core
import FeatureFlag
import PCI
import SwiftyJSON
import UIKit


protocol PayableSubscriptionViewModel: AnyObject {
    func planItem() -> ProducerPlanItem
    func checkNeedCvv(paymentManager: PPPaymentManager) -> Bool
    func producerItem() -> ProducerProfileItem
    func addressItem() -> ConsumerAddressItem?
    func changeAddress(_ address:ConsumerAddressItem?) -> Void
    func requestPayment(
        pin: String,
        paymentManager: PPPaymentManager,
        privacySettings: String,
        biometry: Bool,
        hasPreviousErrors: Bool,
        informedCvv: String?,
        _ completion: @escaping ((PlanSubscriptionTransaction?, Error?) -> Void)
    ) -> Void
}

final class ProducerPlanViewModel: NSObject, PayableSubscriptionViewModel {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    
    var api:ApiSubscription
    var plan:ProducerPlanItem
    var producer:ProducerProfileItem
    var address:ConsumerAddressItem?
    var informedCvv: String?
    
    private let service: SubscriptionServiceProtocol
    
    init(apiSubscription:ApiSubscription = ApiSubscription(), subscriptionPlan: ProducerPlanItem, producer: ProducerProfileItem, address: ConsumerAddressItem? = nil, service: SubscriptionServiceProtocol = SubscriptionService()) {
        self.api = apiSubscription
        self.plan = subscriptionPlan
        self.producer = producer
        self.address = address
        self.service = service
        
        super.init()
    }

    func getProducerPlanUpdates(after subscriptionId:String? = nil, _ completion: @escaping ((Error?) ->Void) ) {
        self.api.getProducerPlanUpdates(by: "\(self.plan.id)", subscriptionId: subscriptionId) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let result):
                    self.plan.changesCards = result.list
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }
    
    //MARK: PayableSubscriptionViewModel
    
    func planItem() -> ProducerPlanItem {
        return self.plan
    }
    
    func producerItem() -> ProducerProfileItem {
        return self.producer
    }

    func addressItem() -> ConsumerAddressItem? {
        return self.address
    }
    
    func changeAddress(_ address: ConsumerAddressItem?) {
        self.address = address
    }
    
    private func saveCvv(informedCvv: String?) {
        guard let cvv = informedCvv else {
            return
        }
        
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        dependencies.keychain.set(key: KeychainKeyPF.cvv(id), value: cvv)
    }
    
    func checkNeedCvv(paymentManager: PPPaymentManager) -> Bool {
        guard let cardBank = api.defauldCard, let cardValue = paymentManager.cardTotal()?.doubleValue else {
            return false
        }
        
        return api.pciCvvIsEnable(cardBank: cardBank, cardValue: cardValue)
    }
    
    func requestPayment(
        pin: String,
        paymentManager: PPPaymentManager,
        privacySettings: String,
        biometry: Bool,
        hasPreviousErrors: Bool = false,
        informedCvv: String?,
        _ completion: @escaping ((PlanSubscriptionTransaction?, Error?) -> Void)
    ) {
        if FeatureManager.isActive(.pciSubscription) {
            subscribePlanPCI(
                password: pin,
                paymentManager: paymentManager,
                privacySettings: privacySettings,
                biometry: biometry,
                hasPreviousErrors: hasPreviousErrors,
                informedCvv: informedCvv,
                completion: completion
            )
            return
        }
        
        let request = SubscribePlanPaymentRequest(plan: self.plan, address: self.address, paymentManager: paymentManager, privacySettings: privacySettings, biometry: biometry, hasAnyPreviousErrors: hasPreviousErrors)
        api.subscribePlan(request, pin: pin) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let response):
                    completion(response, nil)
                case .failure(let error):
                    completion(nil, error)
                }
            }
        }
    }
    
    private func subscribePlanPCI(
        password: String,
        paymentManager: PPPaymentManager,
        privacySettings: String,
        biometry: Bool,
        hasPreviousErrors: Bool = false,
        informedCvv: String?,
        completion: @escaping (PlanSubscriptionTransaction?, Error?) -> Void
    ) {
        let payload = createPayload(paymentManager: paymentManager, privacySettings: privacySettings, biometry: biometry, hasPreviousErrors: hasPreviousErrors, informedCvv: informedCvv)

        service.subscribe(
            password: password,
            planId: plan.id,
            payload: payload,
            isNewArchitecture: false
        ) { [weak self] result in
            switch result {
            case .success(let data):
                guard let transaction = self?.parseTransaction(with: data) else {
                    return
                }
                
                DispatchQueue.main.async {
                    self?.saveCvv(informedCvv: informedCvv)
                    completion(transaction, nil)
                }
            case .failure(let apiError):
                self?.handleFailure(apiError, completion: completion)
            }
        }
    }
    
    private func createPayload(paymentManager: PPPaymentManager, privacySettings: String, biometry: Bool, hasPreviousErrors: Bool = false, informedCvv: String?) -> PaymentPayload<SubscribePayload> {
        let subscribePayload = SubscribePayload(
            addressId: address?.id ?? 0,
            biometry: biometry,
            creditCardId: String(CreditCardManager.shared.defaultCreditCardId),
            credit: paymentManager.balanceTotal().stringValue,
            duplicated: hasPreviousErrors ? 1 : 0,
            feedVisibility: privacySettings,
            total: paymentManager.subtotal.stringValue
        )
        let cvv = cvvPayload(paymentManager: paymentManager, informedCvv: informedCvv)
        return PaymentPayload(cvv: cvv, generic: subscribePayload)
    }
    
    private func cvvPayload(paymentManager: PPPaymentManager, informedCvv: String?) -> CVVPayload? {
        guard let cvv = informedCvv else {
            return createLocalStoreCVVPayload(paymentManager: paymentManager)
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func createLocalStoreCVVPayload(paymentManager: PPPaymentManager) -> CVVPayload? {
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        guard
            let cvv = dependencies.keychain.getData(key: KeychainKeyPF.cvv(id)),
            let cardValue = paymentManager.cardTotal()?.doubleValue,
            !cardValue.isZero
            else {
                return nil
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func parseTransaction(with data: Data?) -> PlanSubscriptionTransaction? {
        guard let jsonData = data, let json = try? JSON(data: jsonData) else {
            return nil
        }
        
        return PlanSubscriptionTransaction(json: json)
    }
    
    private func handleFailure(_ apiError: ApiError, completion: @escaping (PlanSubscriptionTransaction?, Error?) -> Void) {
        DispatchQueue.main.async {
            completion(nil, apiError.requestError)
        }
    }
}
