//
//  ProducerViewModel.swift
//  PicPay
//
//  Created by Vagner Orlandi on 24/01/18.
//

import UIKit

final class ProducerViewModel: NSObject {
    var producer:ProducerProfileItem
    var apiSubscription:ApiSubscription
    private var producerId:String? = nil
    var profileLoaded:Bool
    
    init(producer:ProducerProfileItem, api:ApiSubscription = ApiSubscription()) {
        self.producer = producer
        self.apiSubscription = api
        self.profileLoaded = true
        super.init()
    }
    
    convenience init(producerId:String, api:ApiSubscription = ApiSubscription()) {
        self.init(producer: ProducerProfileItem(), api: api)
        self.producerId = producerId
        self.profileLoaded = false
    }
    
    func loadProducerProfile(_ completion: @escaping ((Error?) ->Void) ) {
        apiSubscription.getProducerProfile(by: self.producerId ?? "") { [weak self] (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let result):
                    self?.producer = result
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }
}
