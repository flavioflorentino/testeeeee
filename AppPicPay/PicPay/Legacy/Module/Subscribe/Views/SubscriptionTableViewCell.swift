import UI

class SubscriptionTableViewCell: UITableViewCell {
    @IBOutlet weak var profileView: UIPPProfileImage!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.textColor = Palette.ppColorGrayscale600.color
        }
    }
    @IBOutlet weak var containerView: CustomCommonView! {
        didSet {
            containerView.backgroundColor = Palette.ppColorGrayscale000.color
            containerView.shadowColor = Palette.ppColorGrayscale300.color(withCustomDark: .ppColorGrayscale100)
            containerView.shadowRadius = 1.0
            containerView.shadowOffset = CGSize(width: 0, height: 2)
            containerView.shadowOpacity = 0.5
        }
    }
    @IBOutlet weak var separatorView: UIView! {
        didSet {
            separatorView.backgroundColor = Palette.ppColorGrayscale200.color
        }
    }
    
    var subscription: ConsumerSubscription?
    
    func configure(_ model: ConsumerSubscription) {
        selectionStyle = .none
        subscription = model
        profileView.setProducer(model.producer)
        usernameLabel.text = model.producer.username
        nameLabel.text = model.producer.name
        
        let price = model.price.getFormattedPrice()
        valueLabel.text = "\(price) \(model.subscriptionPlan.periodicityDescription)"
        valueLabel.textColor = Palette.ppColorGrayscale600.color
        
        switch model.status {
        case .cancelled:
            valueLabel.text = DefaultLocalizable.canceled.text
            descriptionLabel.text = ""
            descriptionLabel.attributedText = model.lastBillingText.htmlStringToAttributedString()?.attributedPicpayFont()
            valueLabel.textColor = Palette.ppColorGrayscale400.color
            descriptionLabel.textColor = Palette.ppColorGrayscale400.color
            
        case .active:
            descriptionLabel.text = ""
            descriptionLabel.attributedText = model.billingDescription.htmlStringToAttributedString()?.attributedPicpayFont()
            descriptionLabel.textColor = Palette.ppColorGrayscale600.color
            
        default:
            descriptionLabel.text = ""
            descriptionLabel.attributedText = model.extraText.htmlStringToAttributedString()?.attributedPicpayFont()
            descriptionLabel.textColor = Palette.ppColorGrayscale600.color
        }
    }
    
}
