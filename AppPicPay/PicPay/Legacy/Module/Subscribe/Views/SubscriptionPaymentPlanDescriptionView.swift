import UI
import UIKit
import AMPopTip

final class SubscriptionPaymentPlanDescriptionView:UIScrollView {
    internal lazy var popOver: PopTip = { [unowned self] in
        let tip = PopTip()
        tip.bubbleColor = UIColor(white: 0.2901, alpha: 1)
        tip.textColor = Palette.ppColorGrayscale000.color
        tip.textAlignment = .left
        tip.shouldDismissOnTap = true
        tip.shouldDismissOnTapOutside = true
        tip.shouldDismissOnTap = true
        tip.offset = 5
        tip.edgeMargin = (self.horizontalMargin)
        tip.font = UIFont.boldSystemFont(ofSize: 11)
        tip.edgeInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
        return tip
    }()
    internal lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.semibold)
        label.text = "R$5"
        return label
    }()
    internal lazy var periodLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.medium)
        label.textColor = UIColor.init(white: 0.6078, alpha: 1)
        label.text = SubscribeLocalizable.perMonth.text
        return label
    }()
    internal lazy var levelLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
        label.textColor = UIColor(white: 0.2901, alpha: 1)
        label.text = SubscribeLocalizable.level1.text
        return label
    }()
    internal lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor(white: 0.46667, alpha: 1)
        label.numberOfLines = 0
        label.text = "Description"
        return label
    }()
    internal lazy var moreInfoButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(SubscriptionPaymentPlanDescriptionView.moreInfoButtonHandler), for: .touchUpInside)
        button.setImage(UIImage(named: "iconInfo"), for: .normal)
        return button
    }()
    internal lazy var contentView: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var widthConstraint: NSLayoutConstraint?
    private var producerPlanPaymentDetails: String?
    internal var horizontalMargin: CGFloat = 20
    

    public var contentWidth:CGFloat {
        get {
            return self.widthConstraint?.constant ?? 0
        }
        set(width) {
            self.widthConstraint?.constant = width
        }
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    internal func initialize() {
        buildView()
    }

    internal func buildView() {
        addSubview(contentView)
        let contentWidthConstraint = NSLayoutConstraint(item: contentView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 320)
        widthConstraint = contentWidthConstraint
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: contentView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: contentView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: contentView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0),
            contentWidthConstraint
            ])
        
        contentView.addSubview(valueLabel)
        valueLabel.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .vertical)
        valueLabel.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .horizontal)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: valueLabel, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: 20),
            NSLayoutConstraint(item: valueLabel, attribute: .left, relatedBy: .equal, toItem: contentView, attribute: .left, multiplier: 1, constant: self.horizontalMargin)
        ])
        
        contentView.addSubview(periodLabel)
        periodLabel.setContentHuggingPriority(UILayoutPriority.defaultLow, for: .horizontal)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: periodLabel, attribute: .left, relatedBy: .equal, toItem: valueLabel, attribute: .right, multiplier: 1, constant: 5),
            NSLayoutConstraint(item: periodLabel, attribute: .firstBaseline, relatedBy: .equal, toItem: valueLabel, attribute: .firstBaseline, multiplier: 1, constant: 0),
        ])
        
        contentView.addSubview(self.moreInfoButton)
        let infoImageSize:CGFloat = 20
        let infoInsetSize:CGFloat = 10
        let infoButtonSize:CGFloat = infoImageSize + (infoInsetSize * 2)
        moreInfoButton.imageEdgeInsets = UIEdgeInsets(top: infoInsetSize, left: infoInsetSize, bottom: infoInsetSize, right: infoInsetSize)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: moreInfoButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: infoButtonSize),
            NSLayoutConstraint(item: moreInfoButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: infoButtonSize),
            NSLayoutConstraint(item: moreInfoButton, attribute: .left, relatedBy: .equal, toItem: periodLabel, attribute: .right, multiplier: 1, constant: 8),
            NSLayoutConstraint(item: moreInfoButton, attribute: .right, relatedBy: .equal, toItem: contentView, attribute: .right, multiplier: 1, constant: -(self.horizontalMargin - infoInsetSize)),
            NSLayoutConstraint(item: moreInfoButton, attribute: .centerY, relatedBy: .equal, toItem: valueLabel, attribute: .centerY, multiplier: 1, constant: 0),
        ])
        
        contentView.addSubview(levelLabel)
        levelLabel.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .vertical)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: levelLabel, attribute: .left, relatedBy: .equal, toItem: contentView, attribute: .left, multiplier: 1, constant: self.horizontalMargin),
            NSLayoutConstraint(item: levelLabel, attribute: .right, relatedBy: .equal, toItem: contentView, attribute: .right, multiplier: 1, constant: -self.horizontalMargin),
            NSLayoutConstraint(item: levelLabel, attribute: .top, relatedBy: .equal, toItem: valueLabel, attribute: .bottom, multiplier: 1, constant: 12),
            ])
        
        contentView.addSubview(descriptionLabel)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: descriptionLabel, attribute: .left, relatedBy: .equal, toItem: contentView, attribute: .left, multiplier: 1, constant: self.horizontalMargin),
            NSLayoutConstraint(item: descriptionLabel, attribute: .right, relatedBy: .equal, toItem: contentView, attribute: .right, multiplier: 1, constant: -self.horizontalMargin),
            NSLayoutConstraint(item: descriptionLabel, attribute: .top, relatedBy: .equal, toItem: levelLabel, attribute: .bottom, multiplier: 1, constant: 11),
            NSLayoutConstraint(item: descriptionLabel, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: self, attribute: .bottom, multiplier: 1, constant: -20)
        ])
    }
    
    //MARK: Public Methods
    
    public func fill(with producerPlanItem:ProducerPlanItem) {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        let number = formatter.string(from: NSNumber(value: producerPlanItem.price)) ?? "0,00"
        valueLabel.text = "R$ \(number)".replacingOccurrences(of: ".", with: ",")
        periodLabel.text = producerPlanItem.periodicityDescription.isEmpty ? "" : "/ \(producerPlanItem.periodicityDescription)"
        levelLabel.text = producerPlanItem.name
        descriptionLabel.text = producerPlanItem.planDescription

        if !producerPlanItem.paymentDetails.isEmpty {
            producerPlanPaymentDetails = producerPlanItem.paymentDetails
            moreInfoButton.isHidden = false
        } else {
            moreInfoButton.isHidden = true
        }
    }
    
    //MARK: Handlers
    
    @objc func moreInfoButtonHandler() {
        if !(self.popOver.isVisible || self.popOver.isAnimating) {
            let width = self.bounds.width - ((2 * self.horizontalMargin) + (2 * 12) + (2 * 5)) //Margin + edgeInsets + offset
            if let infoText = producerPlanPaymentDetails {
                popOver.show(text: infoText, direction: .down, maxWidth: width, in: self, from: self.moreInfoButton.imageRect(forContentRect: self.moreInfoButton.frame))
            }
        } else {
            popOver.hide(forced: true)
        }
    }
}
