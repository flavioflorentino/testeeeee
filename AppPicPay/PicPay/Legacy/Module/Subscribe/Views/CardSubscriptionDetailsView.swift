import UIKit

final class CardSubscriptionDetailsView: UIView {
    var widgetsStackView: WidgetsVerticalStackView
    private var shouldLoadConstraints = true

    init(frame: CGRect, model: WidgetsStack) {
        widgetsStackView = WidgetsVerticalStackView(frame: CGRect.zero, model: model)
        widgetsStackView.translatesAutoresizingMaskIntoConstraints = false
        super.init(frame: frame)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    override func updateConstraints() {
        if shouldLoadConstraints {
            self.loadConstraints()
            self.shouldLoadConstraints = false
        }
        super.updateConstraints()
    }
    
    private func loadConstraints() {
        setup()
    }
    
    func setup(){
        backgroundColor = .clear
        addSubview(widgetsStackView)
        NSLayoutConstraint.leadingTrailing(equalTo: self, for: [widgetsStackView], constant: 12)
        NSLayoutConstraint.activate([
            widgetsStackView.topAnchor.constraint(equalTo: topAnchor),
            widgetsStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15)
        ])
    }

}

extension CardSubscriptionDetailsView : SubscriptionDetailsWidget {
    func viewMustUpdate() {
        setNeedsLayout()
        layoutIfNeeded()
        widgetsStackView.setNeedsLayout()
        widgetsStackView.layoutIfNeeded()
    }
}
