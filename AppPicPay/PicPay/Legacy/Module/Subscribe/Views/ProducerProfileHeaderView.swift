import UI
import UIKit

final class ProducerProfileHeaderView: UIView {
    // MARK: Subviews
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(ProducerProfileHeaderView.closeButtonFired), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "icon_round_close"), for: .normal)
        return button
    }()
    
    private lazy var optionsButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(ProducerProfileHeaderView.optionsButtonFired), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "moreIcon"), for: .normal)
        return button
    }()
    
    public lazy var profileBackgroundImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.alpha = 0.25
        imageView.backgroundColor = Palette.ppColorBranding300.color
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.white.color
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
        return label
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.white.color
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.medium)
        return label
    }()
    
    private lazy var profileImage: UIPPProfileImage = {
        let imageView = UIPPProfileImage()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.imageView?.borderColor = Palette.ppColorGrayscale000.color
        imageView.imageView?.borderWidth = 3
        imageView.imageView?.image = ProducerProfileItem.photoPlaceholder()
        return imageView
    }()
    
    // MARK: - Properties
    
    private weak var heightConstraint: NSLayoutConstraint? = nil
    private weak var centerYUsernameLabelConstraint: NSLayoutConstraint? = nil
    private var originalHeight: CGFloat = 0
    private var startCenterYUsernameLabelConstant: CGFloat = 0
    private var buttonTopAlignment: NSLayoutConstraint? = nil
    
    // MARK: Public Properties
    
    public var minHeight: CGFloat = 0
    public var showBackButton: Bool = false {
        didSet(oldValue) {
            if oldValue == self.showBackButton {
                return
            }
            if self.showBackButton {
                self.closeButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
            } else {
                self.closeButton.setImage(#imageLiteral(resourceName: "iconClose"), for: .normal)
            }
        }
    }
    
    @objc public var topInset: CGFloat = 0 {
        didSet {
            self.setNeedsUpdateConstraints()
        }
    }
    @objc public var optionsButtonIsHidden: Bool = true {
        didSet {
            self.optionsButton.isHidden = self.optionsButtonIsHidden
        }
    }
    @objc public var closeButtonAction: ((UIButton) -> Void)? = nil
    @objc public var optionsButtonAction: ((UIButton) -> Void)? = nil
    
    // MARK: - ProducerProfileHeaderView
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }
    
    override func updateConstraints() {
        let buttonSize:CGFloat = self.closeButton.constraints.first(where: {$0.firstAttribute == .height})?.constant ?? 40
        let buttonTopAlignmentConstant:CGFloat = self.topInset + ((self.minHeight - self.topInset - buttonSize) / 2)
        if let constraint = self.buttonTopAlignment, constraint.constant != (buttonTopAlignmentConstant) {
            self.buttonTopAlignment?.constant = buttonTopAlignmentConstant
        }
        
        super.updateConstraints()
    }
    
    // MARK: Methods
    
    private func initialize() {
        self.buildViews()
        self.backgroundColor = Palette.ppColorGrayscale600.color(withCustomDark: .ppColorGrayscale600)
        self.optionsButton.isHidden = self.optionsButtonIsHidden
    }
    
    private func buildViews() {
        let buttonSize:CGFloat = 40
        let buttonInset:UIEdgeInsets = UIEdgeInsets(top: 9, left: 9, bottom: 9, right: 9)
        let buttonHorizontalMargin:CGFloat = 16 - buttonInset.left
        
        addSubview(self.profileBackgroundImage)
        addSubview(self.closeButton)
        addSubview(self.optionsButton)
        
        NSLayoutConstraint.constraintAllEdges(from: profileBackgroundImage, to: self)

        // CloseButton
        let topAlignment = self.closeButton.topAnchor.constraint(equalTo: self.topAnchor, constant: buttonInset.top)
        buttonTopAlignment = topAlignment
        closeButton.contentEdgeInsets = buttonInset
        
        NSLayoutConstraint.activate([
            topAlignment,
            self.closeButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: buttonHorizontalMargin),
            self.closeButton.widthAnchor.constraint(equalToConstant: buttonSize),
            self.closeButton.heightAnchor.constraint(equalToConstant: buttonSize)
        ])
        
        // OptionsButton
        self.optionsButton.contentEdgeInsets = buttonInset
        NSLayoutConstraint.activate([
            self.optionsButton.topAnchor.constraint(equalTo: self.closeButton.topAnchor),
            self.optionsButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -buttonHorizontalMargin),
            self.optionsButton.widthAnchor.constraint(equalToConstant: buttonSize),
            self.optionsButton.heightAnchor.constraint(equalToConstant: buttonSize),
        ])
        
        // UsernameLabel
        self.addSubview(self.usernameLabel)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: self.usernameLabel, attribute: .centerY, relatedBy: .equal, toItem: self.closeButton, attribute: .centerY, multiplier: 1, constant: 110),
            NSLayoutConstraint(item: self.usernameLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0),
        ])
        
        //ContactProfile
        self.addSubview(self.profileImage)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: self.profileImage, attribute: .bottom, relatedBy: .equal, toItem: self.usernameLabel, attribute: .top, multiplier: 1, constant: -6),
            NSLayoutConstraint(item: self.profileImage, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.profileImage, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60),
            NSLayoutConstraint(item: self.profileImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60)
        ])
        
        //NameLabel
        self.addSubview(self.nameLabel)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: self.nameLabel, attribute: .top, relatedBy: .equal, toItem: self.usernameLabel, attribute: .bottom, multiplier: 1, constant: 2),
            NSLayoutConstraint(item: self.nameLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0),
        ])
    }
    
    // MARK: Public Methods
    
    @objc public func resize(offset: CGPoint) {
        if self.heightConstraint == nil {
            if let selfHeightconstraint = self.constraints.first(where: {$0.firstItem === self && $0.firstAttribute == .height}) {
                self.originalHeight = selfHeightconstraint.constant
                self.heightConstraint = selfHeightconstraint
            }
        }
        
        if self.centerYUsernameLabelConstraint == nil {
            if let constraint = self.constraints.first(where: {$0.firstItem === self.usernameLabel && $0.firstAttribute == .centerY}) {
                self.centerYUsernameLabelConstraint = constraint
                self.startCenterYUsernameLabelConstant = constraint.constant
            }
        }
        
        let newHeight = self.originalHeight - offset.y
        let newCenterY = self.startCenterYUsernameLabelConstant - offset.y
        if newHeight > self.minHeight {
            if newHeight < self.originalHeight {
                self.heightConstraint?.constant = newHeight > self.minHeight ? newHeight : self.minHeight
                self.centerYUsernameLabelConstraint?.constant = newCenterY > 0 ? newCenterY : 0
                let factor:CGFloat = (self.centerYUsernameLabelConstraint?.constant ?? 0) / self.startCenterYUsernameLabelConstant
                self.nameLabel.alpha = factor
                self.profileImage.alpha = factor
            } else if self.heightConstraint?.constant != self.originalHeight {
                self.heightConstraint?.constant = self.originalHeight
                self.centerYUsernameLabelConstraint?.constant = self.startCenterYUsernameLabelConstant
                self.nameLabel.alpha = 1
                self.profileImage.alpha = 1
            }
        } else {
            self.heightConstraint?.constant = self.minHeight
            self.centerYUsernameLabelConstraint?.constant = 0
            self.nameLabel.alpha = 0
            self.profileImage.alpha = 0
        }
    }
    
    public func fill(with producer:ProducerProfileItem?) {
        if let profile = producer {
            self.profileImage.setProducer(profile)
            self.profileBackgroundImage.setImage(url: URL(string: profile.backgroundImageUrl ?? ""))
            self.usernameLabel.text = profile.username
            self.nameLabel.text = profile.name
        } else {
            self.profileImage.setProducer(ProducerProfileItem())
            self.profileBackgroundImage.image = nil
            self.usernameLabel.text = "@username"
            self.nameLabel.text = "name"
        }
    }
    
    // MARK: Handlers
    
    @objc func closeButtonFired(sender: UIButton) {
        self.closeButtonAction?(sender)
    }
    
    @objc func optionsButtonFired(sender: UIButton) {
        self.optionsButtonAction?(sender)
    }
}

