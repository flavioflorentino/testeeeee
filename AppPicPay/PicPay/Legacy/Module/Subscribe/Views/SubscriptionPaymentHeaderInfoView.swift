import UI
import UIKit
import AMPopTip
import AnalyticsModule

final class SubscriptionPaymentHeaderInfoView: UIView {
    private lazy var profileImage: UIPPProfileImage = {
        let profileImage = UIPPProfileImage()
        profileImage.setStore(PPStore())
        profileImage.translatesAutoresizingMaskIntoConstraints = false
        profileImage.layer.masksToBounds = true
        return profileImage
    }()
    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "@username"
        return label
    }()
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = Palette.ppColorGrayscale400.color
        label.text = "name"
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 26)
        label.textColor = Palette.ppColorGrayscale400.color
        label.text = "R$0,00"
        return label
    }()
    private lazy var periodicityLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        label.textColor = Palette.ppColorGrayscale400.color
        label.text = "/ "
        return label
    }()
    
    internal lazy var moreInfoButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(SubscriptionPaymentHeaderInfoView.moreInfoButtonHandler), for: .touchUpInside)
        button.setImage(UIImage(named: "iconInfo"), for: .normal)
        return button
    }()
    internal lazy var popOver: PopTip = { [unowned self] in
        let tip = PopTip()
        tip.isOpaque = true
        tip.bubbleColor = Palette.ppColorGrayscale400.color
        tip.textColor = Palette.ppColorGrayscale000.color
        tip.textAlignment = .left
        tip.shouldDismissOnTap = true
        tip.shouldDismissOnTapOutside = true
        tip.shouldDismissOnTap = true
        tip.offset = 5
        tip.edgeMargin = 8
        tip.font = UIFont.boldSystemFont(ofSize: 11)
        tip.edgeInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
        return tip
    }()
    
    private var popTipInfoText:String? = nil
    
    //MARK: Public properties
    
    public var username:String? {
        set(newUsername) {
            self.usernameLabel.text = newUsername
        }
        get {
            return self.usernameLabel.text
        }
    }
    
    public var name:String?  {
        set(newName) {
            self.nameLabel.text = newName
        }
        get {
            return self.nameLabel.text
        }
    }
    
    public var value:String?  {
        set(newValue) {
            self.valueLabel.text = newValue
        }
        get {
            return self.valueLabel.text
        }
    }
    
    public var periodicity:String?  {
        set(newValue) {
            self.periodicityLabel.text = newValue
        }
        get {
            return self.periodicityLabel.text
        }
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }

    internal func initialize() {
        self.buildView()
    }
    
    internal func buildView() {
        self.addSubview(self.profileImage)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: profileImage, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 15),
            NSLayoutConstraint(item: profileImage, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 10),
            NSLayoutConstraint(item: profileImage, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60),
            NSLayoutConstraint(item: profileImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60),
            NSLayoutConstraint(item: profileImage, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -10),
        ])
        
        addSubview(usernameLabel)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: usernameLabel, attribute: .top, relatedBy: .equal, toItem: profileImage, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: usernameLabel, attribute: .left, relatedBy: .equal, toItem: profileImage, attribute: .right, multiplier: 1, constant: 11),
        ])
        
        addSubview(nameLabel)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: nameLabel, attribute: .top, relatedBy: .equal, toItem: usernameLabel, attribute: .bottom, multiplier: 1, constant: 2),
            NSLayoutConstraint(item: nameLabel, attribute: .left, relatedBy: .equal, toItem: usernameLabel, attribute: .left, multiplier: 1, constant: 0),
        ])
        
        addSubview(valueLabel)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: valueLabel, attribute: .top, relatedBy: .equal, toItem: nameLabel, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: valueLabel, attribute: .left, relatedBy: .equal, toItem: nameLabel, attribute: .left, multiplier: 1, constant: 0),
        ])
        
        addSubview(moreInfoButton)
        NSLayoutConstraint.activate([
            moreInfoButton.heightAnchor.constraint(equalToConstant: 20),
            moreInfoButton.widthAnchor.constraint(equalToConstant: 20),
            moreInfoButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            moreInfoButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
        ])
        
        addSubview(periodicityLabel)
        NSLayoutConstraint.activate([
            periodicityLabel.firstBaselineAnchor.constraint(equalTo: valueLabel.firstBaselineAnchor),
            periodicityLabel.leftAnchor.constraint(equalTo: valueLabel.rightAnchor, constant: 4)
        ])
    }

    public func setProducerProfileImage(_ producer:ProducerProfileItem) {
        profileImage.setProducer(producer)
    }
    
    public func fill(with producer:ProducerProfileItem, and producerPlanItem:ProducerPlanItem) {
        value = String(format: "R$%0.2f", producerPlanItem.price).replacingOccurrences(of: ".", with: ",")
        username = producer.username
        name = producer.name
        profileImage.setProducer(producer)
        periodicity = "/ \(producerPlanItem.periodicityDescription)"
        if !producerPlanItem.paymentDetails.isEmpty {
            popTipInfoText = producerPlanItem.paymentDetails
            moreInfoButton.isHidden = false
        } else {
            moreInfoButton.isHidden = true
        }
    }

    @objc
    func moreInfoButtonHandler() {
        if !(self.popOver.isVisible || self.popOver.isAnimating) {
            let margin: CGFloat = 16
            let edgeInsets: CGFloat = 24
            let offset: CGFloat = 10
            let total = margin + edgeInsets + offset
            let width = self.bounds.width - total
            if let infoText = popTipInfoText {
                Analytics.shared.log(SubscriptionEvent.paymentInfoViewed)
                popOver.show(text: infoText, direction: .down, maxWidth: width, in: self.superview ?? self, from: self.moreInfoButton.imageRect(forContentRect: self.moreInfoButton.frame))
            }
        } else {
            popOver.hide(forced: true)
        }
    }
}

