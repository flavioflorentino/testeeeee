//
//  PaymentDetailsContainer.swift
//  PicPay
//
//  Created by Vagner Orlandi on 23/04/18.
//

import UIKit

final class PaymentDetailsContainer: ItemDetailsWidgetContainer {
    //MARK:Public Methods
    func addPaymentDetails(_ details: PaymentDetailsWidget) {
        self.addWidget(details)
    }
}
