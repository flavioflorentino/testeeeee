//
//  ProducerNavigationBarView.swift
//  PicPay
//
//  Created by Vagner Orlandi on 08/02/18.
//

import UI
import UIKit

final class ProducerNavigationBarView: UIView {

    // MARK: Subviews
    
    private lazy var closeButton:UIButton = {
        let _button = UIButton()
        _button.translatesAutoresizingMaskIntoConstraints = false
        _button.addTarget(self, action: #selector(ProducerNavigationBarView.closeButtonFired), for: .touchUpInside)
        _button.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        
        return _button
    }()
    
    public lazy var profileBackgroundImage:UIImageView = {
        let _imageView = UIImageView()
        _imageView.translatesAutoresizingMaskIntoConstraints = false
        _imageView.alpha = 0.25
        _imageView.backgroundColor = Palette.ppColorBranding300.color
        _imageView.contentMode = .scaleAspectFill
        _imageView.layer.masksToBounds = true
        
        return _imageView
    }()
    
    private lazy var titleLabel:UILabel = {
        let _label = UILabel()
        _label.translatesAutoresizingMaskIntoConstraints = false
        _label.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold)
        _label.textColor = Palette.ppColorGrayscale000.color
        _label.textAlignment = .center
        
        return _label
    }()

    //MARK: Properties
    
    public var closeButtonAction:((UIButton) -> Void)? = nil
    
    public var title:String? {
        get {
            return self.titleLabel.text
        }
        set(newTitle) {
            self.titleLabel.text = newTitle
        }
    }

    //MARK: ProducerNavigationBarView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }
    
    internal func initialize() {
        self.backgroundColor = Palette.ppColorGrayscale600.color
        self.buildView()
    }
    
    internal func buildView() {
        //BackgroundImage
        self.addSubview(self.profileBackgroundImage)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: self.profileBackgroundImage, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.profileBackgroundImage, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.profileBackgroundImage, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.profileBackgroundImage, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 0),
        ])
        
        //CloseButton
        self.addSubview(self.closeButton)
        let buttonSize:CGFloat = 40
        let buttonImageSize:CGFloat = 22
        let buttonPadding:CGFloat = (buttonSize - buttonImageSize) / 2
        let buttonTopMargin:CGFloat = 31 - buttonPadding
        let buttonHorizontalMargin:CGFloat = 13 - buttonPadding
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: self.closeButton, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: buttonTopMargin),
            NSLayoutConstraint(item: self.closeButton, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: buttonHorizontalMargin),
            NSLayoutConstraint(item: self.closeButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: buttonSize),
            NSLayoutConstraint(item: self.closeButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: buttonSize)
            ])
        self.closeButton.contentEdgeInsets = UIEdgeInsets(top: buttonPadding, left: buttonPadding, bottom: buttonPadding, right: buttonPadding)
        
        //TitleLabel
        self.addSubview(self.titleLabel)
        NSLayoutConstraint.activate([
            self.titleLabel.centerYAnchor.constraint(equalTo: self.closeButton.centerYAnchor),
            self.titleLabel.leftAnchor.constraint(equalTo: self.closeButton.rightAnchor, constant: 13),
            self.titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -48)
        ])
        
    }

    func fill(with producer:ProducerProfileItem) {
        if let urlString = producer.backgroundImageUrl {
            let url = URL(string: urlString)
            self.profileBackgroundImage.setImage(url: url)
        }
    }

    // MARK: Handlers
    
    @objc func closeButtonFired(sender: UIButton) {
        self.closeButtonAction?(sender)
    }
}
