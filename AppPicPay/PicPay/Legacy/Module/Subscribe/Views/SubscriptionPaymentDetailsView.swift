import UIKit

final class SubscriptionPaymentDetailsView: UIView, PaymentDetailsWidget {

    //MARK: Subviews
    lazy var details: PaymentDetailsWithSubtitleView<ProducerPlanItem> = {
        let _view = PaymentDetailsWithSubtitleView<ProducerPlanItem>()
        _view.translatesAutoresizingMaskIntoConstraints = false
        _view.isChangeButtonHidden = true
        
        return _view
    }()

    //MARK: Properties
    private var shouldLoadConstraints:Bool = true
    
    public var onChangeButtonTap:((Any?) -> Void)? {
        get {
            return self.details.onChangeButtonTap
        }
        set {
            self.details.onChangeButtonTap = newValue
            self.details.isChangeButtonHidden = newValue == nil
        }
    }
    
    //MARK: Private methods
    private func loadConstraints() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(self.details)
        
        NSLayoutConstraint.activate([
            self.details.topAnchor.constraint(equalTo: self.topAnchor),
            self.details.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            self.details.leftAnchor.constraint(equalTo: self.leftAnchor),
            self.details.rightAnchor.constraint(equalTo: self.rightAnchor)
        ])
    }
    
    //MARK: Public methods
    func setSubscriptionPlan(_ plan:ProducerPlanItem?) {
        guard let item = plan else {
            return
        }
        self.details.setModel(item)
    }
    
    func viewMustUpdate() {
        self.details.viewMustUpdate()
    }
    
    //MARK: UIView
    override func updateConstraints() {
        if shouldLoadConstraints {
            self.loadConstraints()
            self.shouldLoadConstraints = false
        }
        super.updateConstraints()
    }
}

extension ProducerPlanItem:PaymentDetailsWithSubtitle {
    func getPaymentDetailInfo() -> String {
        return SubscribeLocalizable.signatureChosen.text
    }
    
    func getPaymentDetailTitle() -> String {
        return self.name
    }
    
    func getPaymentDetailSubtitle() -> String {
        return self.planDescription
    }
}
