import UI
import UIKit
import FeatureFlag

final class ProducerPlanView:UIScrollView {
    private lazy var contentView: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 22, weight: UIFont.Weight.semibold)
        label.text = "R$ 0"
        return label
    }()
    lazy var periodLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)
        label.textColor = Palette.ppColorGrayscale600.color
        label.text = SubscribeLocalizable.perMonth.text
        return label
    }()
    lazy var payDayLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = SubscribeLocalizable.chargedEveryDay.text
        return label
    }()
    lazy var lastPayLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textColor = Palette.ppColorGrayscale600.color
        return label
    }()
    lazy var warningLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.text = ""
        return label
    }()
    lazy var actionButton: UIButton = { [unowned self] in
        let buttonColor = Palette.ppColorNegative300.color
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(ProducerPlanView.actionButtonActionHandler), for: .touchUpInside)
        button.setTitle(SubscribeLocalizable.cancelSubscription.text, for: .normal)
        button.setTitleColor(buttonColor, for: .normal)
        button.layer.borderColor = buttonColor.cgColor
        button.layer.borderWidth = 1
        button.layer.masksToBounds = true
        button.isUserInteractionEnabled = true
        button.backgroundColor = Palette.ppColorGrayscale000.color
        return button
    }()
    lazy var payNowButton: UIButton = { [unowned self] in
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(ProducerPlanView.payNowButtonActionHandler), for: .touchUpInside)
        button.setTitleColor(Palette.ppColorGrayscale000.color, for: .normal)
        button.setTitle(SubscribeLocalizable.paySubscription.text, for: .normal)
        button.backgroundColor = Palette.ppColorNegative300.color
        button.isHidden = true
        button.layer.masksToBounds = true
        return button
    }()
    lazy var subscriptionDetailsContainer: SubscriptionDetailsContainer = {
        let container = SubscriptionDetailsContainer()
        container.translatesAutoresizingMaskIntoConstraints = false
        return container
    }()
    
    //MARK: Properties
    
    private var buttonBottomConstraint: NSLayoutConstraint? = nil
    private var widthConstraint: NSLayoutConstraint? = nil
    private var paddingConstraint: NSLayoutConstraint? = nil
    private var payNowButtonHeightConstraint: NSLayoutConstraint? = nil
    private var payNowButtonSpacingConstraint: NSLayoutConstraint? = nil
    
    //MARK: Public Properties
    
    public var contentWidth:CGFloat {
        get {
            return self.widthConstraint?.constant ?? 0
        }
        set(width) {
            self.widthConstraint?.constant = width
        }
    }
    
    public var onCardButtonTap: ((UIButton, Button, WidgetsStack) -> Void)?
    public var onActionButtonTap: ((_ sender: UIButton) -> Void)? = nil {
        didSet {
            if self.onActionButtonTap != nil {
                self.actionButton.isHidden = false
            } else {
                self.actionButton.isHidden = true
            }
        }
    }
    public var onPayNowButtonTap: ((_ sender: UIButton) -> Void)? = nil {
        didSet {
            if self.onPayNowButtonTap != nil {
                let buttonHeight:CGFloat = 36
                payNowButtonHeightConstraint?.constant = buttonHeight
                payNowButtonSpacingConstraint?.constant = 12
                payNowButton.isHidden = false
                payNowButton.layer.cornerRadius = buttonHeight / 2
            } else {
                payNowButtonHeightConstraint?.constant = 0
                payNowButtonSpacingConstraint?.constant = 4
                payNowButton.isHidden = true
            }
        }
    }
    
    public var onSubscriptionWidgetChangeButtonTap: ((SubscriptionDetails?) -> Void)? = nil
    public var paddingTop: CGFloat = 0 {
        didSet {
            self.paddingConstraint?.constant = self.paddingTop
        }
    }

    //MARK: ProducerPlanView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }
    
    //MARK: Methods
    
    internal func initialize() {
        self.showsVerticalScrollIndicator = false
        self.buildView()
    }
    
    internal func buildView() {
        let buttonHeight:CGFloat = 36
        let buttonWidth:CGFloat = 170
        
        addSubview(self.contentView)
        let contentWidthConstraint = NSLayoutConstraint(item: contentView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 320)
        widthConstraint = contentWidthConstraint
        let contentTopConstraint = NSLayoutConstraint(item: contentView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        paddingConstraint = contentTopConstraint
        
        NSLayoutConstraint.activate([
            contentTopConstraint,
            NSLayoutConstraint(item: contentView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: contentView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0),
            contentWidthConstraint
        ])
        
        contentView.addSubview(valueLabel)
        valueLabel.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .vertical)
        valueLabel.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .horizontal)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: valueLabel, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: 20),
            NSLayoutConstraint(item: valueLabel, attribute: .left, relatedBy: .equal, toItem: contentView, attribute: .left, multiplier: 1, constant: 20)
            ])
        
        contentView.addSubview(periodLabel)
        periodLabel.setContentHuggingPriority(UILayoutPriority.defaultLow, for: .horizontal)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: periodLabel, attribute: .firstBaseline, relatedBy: .equal, toItem: valueLabel, attribute: .firstBaseline, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: periodLabel, attribute: .left, relatedBy: .equal, toItem: valueLabel, attribute: .right, multiplier: 1, constant: 3),
            NSLayoutConstraint(item: periodLabel, attribute: .right, relatedBy: .equal, toItem: contentView, attribute: .right, multiplier: 1, constant: -20)
        ])
        
        
        contentView.addSubview(payDayLabel)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: payDayLabel, attribute: .top, relatedBy: .equal, toItem: valueLabel, attribute: .bottom, multiplier: 1, constant: 8),
            NSLayoutConstraint(item: payDayLabel, attribute: .left, relatedBy: .equal, toItem: valueLabel, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: payDayLabel, attribute: .right, relatedBy: .equal, toItem: periodLabel, attribute: .right, multiplier: 1, constant: 0),
        ])
        
        contentView.addSubview(lastPayLabel)
        lastPayLabel.setContentHuggingPriority(UILayoutPriority.defaultLow, for: .horizontal)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: lastPayLabel, attribute: .top, relatedBy: .equal, toItem: payDayLabel, attribute: .bottom, multiplier: 1, constant: 8),
            NSLayoutConstraint(item: lastPayLabel, attribute: .left, relatedBy: .equal, toItem: payDayLabel, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: lastPayLabel, attribute: .right, relatedBy: .equal, toItem: payDayLabel, attribute: .right, multiplier: 1, constant: 0),
        ])
        
        contentView.addSubview(warningLabel)
        warningLabel.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .vertical)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: warningLabel, attribute: .top, relatedBy: .equal, toItem: lastPayLabel, attribute: .bottom, multiplier: 1, constant: 8),
            NSLayoutConstraint(item: warningLabel, attribute: .left, relatedBy: .equal, toItem: lastPayLabel, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: warningLabel, attribute: .right, relatedBy: .equal, toItem: lastPayLabel, attribute: .right, multiplier: 1, constant: 0),
        ])
        
        contentView.addSubview(payNowButton)
        let payNowButtonHeight = NSLayoutConstraint(item: payNowButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        let payNowButtonSpacing = NSLayoutConstraint(item: payNowButton, attribute: .top, relatedBy: .equal, toItem: warningLabel, attribute: .bottom, multiplier: 1, constant: 4)
        payNowButtonHeightConstraint = payNowButtonHeight
        payNowButtonSpacingConstraint = payNowButtonSpacing
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: payNowButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: buttonWidth),
            payNowButtonHeight,
            payNowButtonSpacing,
            NSLayoutConstraint(item: payNowButton, attribute: .left, relatedBy: .equal, toItem: lastPayLabel, attribute: .left, multiplier: 1, constant: 0),
        ])

        contentView.addSubview(subscriptionDetailsContainer)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: subscriptionDetailsContainer, attribute: .top, relatedBy: .equal, toItem: payNowButton, attribute: .bottom, multiplier: 1, constant: 13),
            NSLayoutConstraint(item: subscriptionDetailsContainer, attribute: .left, relatedBy: .equal, toItem: contentView, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: subscriptionDetailsContainer, attribute: .right, relatedBy: .equal, toItem: contentView, attribute: .right, multiplier: 1, constant: 0)
        ])
        
        contentView.addSubview(actionButton)
        let bottomConstraint = NSLayoutConstraint(item: actionButton, attribute: .bottom, relatedBy: .equal, toItem: contentView, attribute: .bottom, multiplier: 1, constant: -30)
        buttonBottomConstraint = bottomConstraint
        actionButton.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .vertical)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: actionButton, attribute: .top, relatedBy: .equal, toItem: subscriptionDetailsContainer, attribute: .bottom, multiplier: 1, constant: 30),
            NSLayoutConstraint(item: actionButton, attribute: .left, relatedBy: .equal, toItem: payNowButton, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: actionButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: buttonHeight),
            NSLayoutConstraint(item: actionButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: buttonWidth),
            bottomConstraint
        ])
        actionButton.layer.cornerRadius = buttonHeight / 2
        actionButton.layer.masksToBounds = true
    }
    
    private func configureActionButton(with status: ConsumerSubscription.Status) {
        switch status {
        case .activeCancelled, .cancelled:
            let buttonColor = Palette.ppColorGrayscale600.color
            actionButton.setTitle(SubscribeLocalizable.resign.text, for: .normal)
            actionButton.setTitleColor(buttonColor, for: .normal)
            actionButton.layer.borderColor = buttonColor.cgColor
            buttonBottomConstraint?.constant = -30

        default:
            actionButton.isHidden = true
            buttonBottomConstraint?.constant = 0
        }
    }
    
    private func configureDetailsContainer(with subscription: ConsumerSubscription) {
        if FeatureManager.isActive(.subscriptionCard) && !subscription.cards.isEmpty {
            for card in subscription.cards {
                let cardWidgetStackView = CardSubscriptionDetailsView(frame: CGRect.zero, model: card)
                cardWidgetStackView.widgetsStackView.didTapOnButton = { [weak self] (buttonView, buttonModel, stackModel) in
                    self?.onCardButtonTap?(buttonView, buttonModel, stackModel)
                }
                subscriptionDetailsContainer.addSubscriptiontDetails(cardWidgetStackView, isDividerRequired: false)
            }
        }
        
        if let consumerAddress = subscription.consumerAddress {
            let addressWidget = AddressSubscriptionDetailsView()
            addressWidget.setAddress(consumerAddress)
            
            if subscription.status != .cancelled && onSubscriptionWidgetChangeButtonTap != nil{
                addressWidget.onChangeButtonTap = {[weak self] (_, address) in
                    self?.onSubscriptionWidgetChangeButtonTap?(address)
                }
            }
            subscriptionDetailsContainer.addSubscriptiontDetails(addressWidget)
        }
        
        let planWidget = ProducerPlanSubscriptionDetailsView()
        planWidget.setPlan(subscription.subscriptionPlan)
        subscriptionDetailsContainer.addSubscriptiontDetails(planWidget)
        subscriptionDetailsContainer.forceFitAllContent()
    }
    
    //MARK: Public methods
    
    public func fill(with consumersubscription: ConsumerSubscription?) {
        if let subscription = consumersubscription {
            configureActionButton(with: subscription.status)
            valueLabel.text = subscription.price.stringAmount
            periodLabel.text = "/ \(subscription.subscriptionPlan.periodicityDescription)"
            payDayLabel.text = ""
            
            if !(subscription.status == .cancelled || subscription.status == .activeCancelled) {
                payDayLabel.attributedText = subscription.billingDescription.htmlStringToAttributedString()?.attributedPicpayFont(12)
                payDayLabel.textColor = Palette.ppColorGrayscale500.color
            }
            if subscription.status != .waitingPayment && subscription.status != .waitingAnalysis {
                lastPayLabel.text = ""
                lastPayLabel.attributedText = subscription.lastBillingText.htmlStringToAttributedString()?.attributedPicpayFont(12)
            }
            if !FeatureManager.isActive(.subscriptionCard) && !subscription.extraText.isEmpty {
                warningLabel.text = ""
                warningLabel.attributedText = subscription.extraText.htmlStringToAttributedString()?.attributedPicpayFont(12)
                warningLabel.textColor = Palette.ppColorNegative300.color
            }

            configureDetailsContainer(with: subscription)
            lastPayLabel.textColor = Palette.ppColorGrayscale600.color
        } else {
            actionButton.isHidden = true
            valueLabel.text = "R$ 0,00"
            periodLabel.text = SubscribeLocalizable.perMonth.text
            payDayLabel.text = SubscribeLocalizable.chargedEveryDay.text
            lastPayLabel.text = "\(SubscribeLocalizable.lastCharged.text) 00/00/00 às 00:00"
        }
        self.layoutIfNeeded()
    }
    
    func updateDetailsWidgets(consumerSubscrition: ConsumerSubscription?) {
        var addressFound:Bool = false
        for case let detail as SubscriptionDetailsWidget in self.subscriptionDetailsContainer.subviews {
            switch detail {
            case let detail as AddressSubscriptionDetailsView:
                addressFound = true
                detail.setAddress(consumerSubscrition?.consumerAddress)
                
            case let detail as ProducerPlanSubscriptionDetailsView:
                detail.setPlan(consumerSubscrition?.subscriptionPlan)
                
            default:
                break
            }
        }
        
        if let subscription = consumerSubscrition, let consumerAddress = subscription.consumerAddress, !addressFound {
            let addressWidget = AddressSubscriptionDetailsView()
            addressWidget.setAddress(consumerAddress)
            if subscription.status != .cancelled {
                addressWidget.onChangeButtonTap = {[weak self] (_, address) in
                    self?.onSubscriptionWidgetChangeButtonTap?(address)
                }
            }
            self.subscriptionDetailsContainer.addSubscriptiontDetails(addressWidget)
        }
        self.subscriptionDetailsContainer.forceFitAllContent()
    }
    
    @discardableResult
    func removeWidgetStackView(stackModel: WidgetsStack) -> CGFloat {
        for case let card as CardSubscriptionDetailsView in self.subscriptionDetailsContainer.subviews where card.widgetsStackView.model === stackModel {
            let cardHeight = card.frame.height
            if let constraint = card.constraints.first(where: { $0.firstAttribute == .height }) {
                constraint.constant = 0
            } else {
                card.heightAnchor.constraint(equalToConstant: 0).isActive = true
            }
            self.subscriptionDetailsContainer.forceFitAllContent()
            UIView.animate(withDuration: 0.5, animations: {
                card.alpha = 0
                self.subscriptionDetailsContainer.layoutIfNeeded()
            })
            return cardHeight
        }
        return 0
    }

    //MARK: Handlers
    
    @objc func actionButtonActionHandler(sender: UIButton) {
        onActionButtonTap?(sender)
    }
    
    @objc func payNowButtonActionHandler(sender: UIButton) {
        onPayNowButtonTap?(sender)
    }
}
