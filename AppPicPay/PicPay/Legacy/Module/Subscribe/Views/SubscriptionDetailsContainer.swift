import UIKit

final class SubscriptionDetailsContainer: ItemDetailsWidgetContainer {
    func addSubscriptiontDetails(_ details: SubscriptionDetailsWidget, isDividerRequired:Bool = true) {
        addWidget(details, isDividerRequired: isDividerRequired)
    }
    
    func forceFitAllContent() {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else {
            return
        }
            
            strongSelf.contentSize = strongSelf.subviews.reduce(CGRect(), { $1 is SubscriptionDetailsWidget ? $0.union($1.frame) : $0 }).size
            if let constraint = self?.constraints.first(where: {$0.firstAttribute == .height}) {
                constraint.constant = strongSelf.contentSize.height
            } else {
                strongSelf.heightAnchor.constraint(equalToConstant: strongSelf.contentSize.height).isActive = true
            }
        }
    }
}
