import UI
import UIKit

/**
 * Classe utilizada para guardar os dados e o estado de cada celula
 */
final class SubscriptionDataCell:NSObject {
    public var isExpanded:Bool = false
    public var isSelected:Bool = false
    public var subscription:ProducerPlanItem
    
    init(subscription item:ProducerPlanItem) {
        self.subscription = item
        super.init()
    }
}

protocol ExpandableTableViewCellDelegate: AnyObject {
    func cellHeightChanged(cell: UITableViewCell, diff:CGFloat, isExpanding:Bool) -> Void
}

extension ExpandableTableViewCellDelegate {
    func cellHeightChanged(cell: UITableViewCell, diff:CGFloat, isExpanding:Bool) {
        if let tableView = cell.superview as? UITableView {
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
}

final class ProducerPlanTableViewCell: UITableViewCell {
    
    //MARK: Properties
    
    internal var dataItem:SubscriptionDataCell? = nil
    internal var descriptionHeightConstraint:NSLayoutConstraint? = nil
    
    var isExpanded:Bool = false
    
    //MARK: Public properties
    
    public var onItemSelected:((SubscriptionDataCell?) -> Void)? = nil
    public var currentSubscriptionText:String =  SubscribeLocalizable.currentlySubscribe.text
    public var minDescriptionHeight:CGFloat = 90
    public var isCurrentOption:Bool = false {
        didSet(oldValue) {
            if self.isCurrentOption == oldValue {
                return
            }
            self.setCurrentOption(self.isCurrentOption)
        }
    }
    public var valor:Double? = nil {
        didSet {
            let formatter = NumberFormatter()
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 2
            let number = formatter.string(from: NSNumber(value: self.valor ?? 0)) ?? "0,00"
            self.valueLabel.text = "R$ \(number)".replacingOccurrences(of: ".", with: ",")
            let needAddTrailingZero = { $0?.count == 2 ? $0?[1].count == 1 : false }(self.valueLabel.text?.split(separator: ","))
            if needAddTrailingZero {
                self.valueLabel.text?.append("0")
            }
        }
    }
    public var periodo:String? = nil {
        didSet {
            if let period = self.periodo {
                if !period.isEmpty {
                    self.periodLabel.text = "/ \(period)"
                }
            } else {
                self.periodLabel.text = ""
            }
        }
    }
    public var nivel:String? = nil {
        didSet {
            self.levelLabel.text = self.nivel
        }
    }
    public var descricao:String? = nil {
        didSet {
            self.descriptionLabel.text = self.descricao
        }
    }
    
    public weak var delegate:ExpandableTableViewCellDelegate? = nil
    
    //MARK: SubViews
    
    lazy internal var markButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(ProducerPlanTableViewCell.itemSelected), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "iconUnchecked").withRenderingMode(.alwaysTemplate), for: .normal)
        button.setImage(#imageLiteral(resourceName: "iconChecked").withRenderingMode(.alwaysTemplate), for: .selected)
        button.tintColor = Palette.ppColorBranding300.color
        return button
    }()
    lazy internal var valueLabel: UILabel = {
        let label = UILabel()
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ProducerPlanTableViewCell.itemSelected)))
        label.isUserInteractionEnabled = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        return label
    }()
    lazy internal var periodLabel: UILabel = {
        let label = UILabel()
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ProducerPlanTableViewCell.itemSelected)))
        label.isUserInteractionEnabled = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.medium)
        label.textColor = Palette.ppColorGrayscale400.color
        return label
    }()
    lazy private var currentSubscriptionLabel:UILabelInsets = {
        let label = UILabelInsets()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()
    
    lazy private var levelLabel: UILabelInsets = {
        let label = UILabelInsets()
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ProducerPlanTableViewCell.itemSelected)))
        label.isUserInteractionEnabled = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()
    lazy internal var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale400.color
        label.text = "description"
        return label
    }()
    lazy internal var descriptionContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.layer.masksToBounds = true
        return view
    }()
    lazy internal var descriptionMask: CAGradientLayer = {
        let layer = CAGradientLayer()
        layer.colors = [Palette.ppColorGrayscale000.cgColor, UIColor.clear.cgColor]
        layer.locations = [0.7, 1]
        return layer
    }()
    lazy internal var seeMoreButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(ProducerPlanTableViewCell.expandDescription), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "icoButtonArrow"), for: .normal)
        button.setTitle(DefaultLocalizable.viewMore.text, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
        button.setTitleColor(Palette.ppColorGrayscale300.color, for: .normal)
        button.semanticContentAttribute = .forceRightToLeft
        return button
    }()
    
    //MARK: - ProducerPlanTableViewCell
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.initialize()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }
    
    //MARK: Internal Methods
    
    internal func initialize() {
        self.contentView.backgroundColor = Palette.ppColorGrayscale000.color
        self.buildView()
        self.descriptionContainerView.layer.mask = self.descriptionMask
    }
    
    internal func updateDescriptionHeightContraint() {
        if self.descriptionLabel.intrinsicContentSize.height > self.minDescriptionHeight {
            self.seeMoreButton.isHidden = false
            self.descriptionHeightConstraint?.constant = self.isExpanded ? self.descriptionLabel.intrinsicContentSize.height : self.minDescriptionHeight
        } else {
            self.seeMoreButton.isHidden = true
            self.descriptionHeightConstraint?.constant = self.descriptionLabel.intrinsicContentSize.height
        }
    }
    
    internal func updateDescriptionMask() {
        if (self.descriptionMask.colors?.count ?? 0) >= 2 {
            let isFadeNeeded:Bool = self.descriptionLabel.intrinsicContentSize.height > self.minDescriptionHeight && !self.isExpanded
            self.descriptionMask.colors?[1] = isFadeNeeded ? UIColor.clear.cgColor : Palette.ppColorGrayscale000.cgColor
        }
        
        DispatchQueue.main.async {
            self.descriptionMask.frame = self.descriptionContainerView.bounds
        }
    }
    
    internal func buildView() {
        self.contentView.addSubview(self.markButton)
        
        let leftMargin:CGFloat = 20
        let rightMargin:CGFloat = 12

        let buttonPadding:CGFloat = 10
        let buttonImageSize:CGFloat = 24
        let buttonSize:CGFloat = buttonImageSize + (2 * buttonPadding)
        let buttonValueSpacing:CGFloat = 12 - buttonPadding
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: self.markButton, attribute: .left, relatedBy: .equal, toItem: self.contentView, attribute: .left, multiplier: 1, constant: 20 - buttonPadding),
            NSLayoutConstraint(item: self.markButton, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 20 - buttonPadding),
            NSLayoutConstraint(item: self.markButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: buttonSize),
            NSLayoutConstraint(item: self.markButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: buttonSize),
        ])
        
        self.markButton.contentEdgeInsets = UIEdgeInsets(top: buttonPadding, left: buttonPadding, bottom: buttonPadding, right: buttonPadding)
        
        self.contentView.addSubview(self.periodLabel)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: self.periodLabel, attribute: .right, relatedBy: .equal, toItem: self.contentView, attribute: .right, multiplier: 1, constant: -rightMargin)
            ])
        
        self.contentView.addSubview(self.valueLabel)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: self.valueLabel, attribute: .right, relatedBy: .equal, toItem: self.periodLabel, attribute: .left, multiplier: 1, constant: -6),
            NSLayoutConstraint(item: self.valueLabel, attribute: .left, relatedBy: .equal, toItem: self.markButton, attribute: .right, multiplier: 1, constant: buttonValueSpacing),
            NSLayoutConstraint(item: self.valueLabel, attribute: .centerY, relatedBy: .equal, toItem: self.markButton, attribute: .centerY, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.valueLabel, attribute: .firstBaseline, relatedBy: .equal, toItem: self.periodLabel, attribute: .firstBaseline, multiplier: 1, constant: 0)
            ])
        
        self.contentView.addSubview(self.currentSubscriptionLabel)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: self.currentSubscriptionLabel, attribute: .left, relatedBy: .equal, toItem: self.valueLabel, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.currentSubscriptionLabel, attribute: .top, relatedBy: .equal, toItem: self.valueLabel, attribute: .bottom, multiplier: 1, constant: 8),
            NSLayoutConstraint(item: self.currentSubscriptionLabel, attribute: .right, relatedBy: .equal, toItem: self.periodLabel, attribute: .right, multiplier: 1, constant: 0),
        ])
        self.currentSubscriptionLabel.contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
        
        self.contentView.addSubview(self.levelLabel)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: self.levelLabel, attribute: .left, relatedBy: .equal, toItem: self.valueLabel, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.levelLabel, attribute: .top, relatedBy: .equal, toItem: self.currentSubscriptionLabel, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.levelLabel, attribute: .right, relatedBy: .equal, toItem: self.periodLabel, attribute: .right, multiplier: 1, constant: 0),
        ])
        self.levelLabel.contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
        
        self.contentView.addSubview(self.seeMoreButton)
        let imageTextSpacing:CGFloat = 5
        let seeMoreButtonInsetSize:CGFloat = 10
        self.seeMoreButton.contentEdgeInsets = UIEdgeInsets(top: seeMoreButtonInsetSize, left: seeMoreButtonInsetSize, bottom: seeMoreButtonInsetSize, right: seeMoreButtonInsetSize + imageTextSpacing)
        self.seeMoreButton.imageEdgeInsets.right = -imageTextSpacing
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: self.seeMoreButton, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -10),
            NSLayoutConstraint(item: self.seeMoreButton, attribute: .right, relatedBy: .equal, toItem: self.contentView, attribute: .right, multiplier: 1, constant: -6),
        ])
        
        self.contentView.addSubview(self.descriptionContainerView)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: self.descriptionContainerView, attribute: .top, relatedBy: .equal, toItem: self.levelLabel, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.descriptionContainerView, attribute: .left, relatedBy: .equal, toItem: self.valueLabel, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.descriptionContainerView, attribute: .right, relatedBy: .equal, toItem: self.periodLabel, attribute: .right, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.descriptionContainerView, attribute: .bottom, relatedBy: .equal, toItem: self.seeMoreButton, attribute: .top, multiplier: 1, constant: 0),
        ])
        self.descriptionHeightConstraint = NSLayoutConstraint(item: descriptionContainerView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 10)
        self.descriptionHeightConstraint?.isActive = true
        
        self.descriptionContainerView.addSubview(self.descriptionLabel)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: self.descriptionLabel, attribute: .left, relatedBy: .equal, toItem: self.descriptionContainerView, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.descriptionLabel, attribute: .top, relatedBy: .equal, toItem: self.descriptionContainerView, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.descriptionLabel, attribute: .right, relatedBy: .equal, toItem: self.descriptionContainerView, attribute: .right, multiplier: 1, constant: 0),
        ])
        self.descriptionLabel.preferredMaxLayoutWidth = UIScreen.main.bounds.width - (leftMargin + buttonSize + buttonValueSpacing + rightMargin)
    }
    
    internal func updateSeeMoreButton() {
        let degreeAngle:Double = self.isExpanded ? 180 : 0
        let buttonTitle:String = self.isExpanded ? DefaultLocalizable.viewLess.text : DefaultLocalizable.viewMore.text
        self.seeMoreButton.imageView?.transform = CGAffineTransform(rotationAngle: CGFloat(degreeAngle * (Double.pi / 180)))
        self.seeMoreButton.setTitle(buttonTitle, for: .normal)
    }
    
    internal func setCurrentOption(_ isCurrent:Bool) {
        if isCurrent {
            self.currentSubscriptionLabel.text = self.currentSubscriptionText
            self.markButton.tintColor = Palette.ppColorGrayscale200.color
            self.valueLabel.alpha = 0.5
            self.periodLabel.alpha = 0.5
            self.levelLabel.alpha = 0.5
            self.descriptionLabel.alpha = 0.5
            self.markButton.isSelected = true
        } else {
            self.currentSubscriptionLabel.text = ""
            self.markButton.tintColor = Palette.ppColorGrayscale500.color
            self.valueLabel.alpha = 1
            self.periodLabel.alpha = 1
            self.levelLabel.alpha = 1
            self.descriptionLabel.alpha = 1
            self.markButton.isSelected = false
        }
    }
    
    //MARK:Handlers
    
    @objc func expandDescription(sender: UIButton) {
        self.isExpanded = !self.isExpanded
        self.dataItem?.isExpanded = self.isExpanded
        let tableUpdateAnimationTime:TimeInterval = 0.3
        
        let cellDiff = (self.descriptionLabel.intrinsicContentSize.height - self.minDescriptionHeight)
        
        UIView.animate(withDuration: tableUpdateAnimationTime) { [weak self] in
            if let strongSelf = self {
                strongSelf.updateSeeMoreButton()
                strongSelf.updateDescriptionHeightContraint()
                strongSelf.updateDescriptionMask()
                strongSelf.contentView.layoutIfNeeded()
            }
        }
        
        self.delegate?.cellHeightChanged(cell: self, diff: cellDiff, isExpanding: self.isExpanded)
    }
    
    @objc func itemSelected(sender: Any?) {
        if let item = self.dataItem {
            //Atualiza o estado do markButton apenas se não for a opção ativa
            if !item.subscription.isCurrentSubscription {
                item.isSelected = !self.markButton.isSelected
                self.updateMarkButtonView()
            }
        } else {
            self.updateMarkButtonView(to: !self.markButton.isSelected)
        }
        self.onItemSelected?(self.dataItem)
    }
    
    //MARK: Public Methods
    
    public func attach(data:SubscriptionDataCell) {
        self.dataItem = data
        self.isExpanded = data.isExpanded
        self.markButton.isSelected = data.isSelected
        self.descricao = data.subscription.planDescription
        self.valor = data.subscription.price
        self.nivel = data.subscription.name
        self.periodo = data.subscription.periodicityDescription
        self.isCurrentOption = data.subscription.isCurrentSubscription
        self.updateDescriptionHeightContraint()
        self.updateDescriptionMask()
        self.updateSeeMoreButton()
    }
    
    public func updateMarkButtonView() {
        guard let newState = self.dataItem?.isSelected else {
            return
        }
        self.updateMarkButtonView(to: newState)
    }
    
    public func updateMarkButtonView(to newState:Bool) {
        let checkButton = self.markButton
        
        if let item = self.dataItem {
            if item.subscription.isCurrentSubscription {
                return
            }
        }
        
        if checkButton.isSelected {
            UIView.animate(withDuration: 0.1, animations: {
                checkButton.alpha = 0.5
            }, completion: { (success) in
                checkButton.alpha = 1
                checkButton.isSelected = newState
            })
        } else {
            checkButton.alpha = 0.5
            UIView.animate(withDuration: 0.1) {
                checkButton.alpha = 1
            }
            checkButton.isSelected = newState
        }
    }
}

fileprivate final class UILabelInsets:UILabel {
    var contentInsets: UIEdgeInsets = .zero {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    // MARK: UILabel
    override func drawText(in rect: CGRect) {
        let newRect = rect.inset(by: contentInsets)
        super.drawText(in: newRect)
    }
    
    // MARK: UIView
    override func layoutSubviews() {
        super.layoutSubviews()
        preferredMaxLayoutWidth = frame.width - (contentInsets.left + contentInsets.right)
    }
    
    override var intrinsicContentSize : CGSize {
        let size = super.intrinsicContentSize
        let width = size.width + contentInsets.left + contentInsets.right
        let height = size.height + contentInsets.top + contentInsets.bottom
        return CGSize(width: width, height: height)
    }
}

