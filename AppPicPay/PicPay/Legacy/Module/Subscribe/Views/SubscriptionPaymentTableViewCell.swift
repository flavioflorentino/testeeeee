import UI

final class SubscriptionPaymentTableViewCell: SubscriptionTableViewCell {
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var paymentButton: UIPPButton!
    @IBOutlet override weak var containerView: CustomCommonView! {
        didSet {
            containerView.backgroundColor = Palette.ppColorGrayscale000.color
            containerView.shadowColor = Palette.ppColorGrayscale300.color(withCustomDark: .ppColorGrayscale100)
            containerView.shadowRadius = 1.0
            containerView.shadowOffset = CGSize(width: 0, height: 2)
            containerView.shadowOpacity = 0.5
        }
    }
    @IBOutlet override weak var separatorView: UIView! {
        didSet {
            separatorView.backgroundColor = Palette.ppColorGrayscale200.color
        }
    }
    
    var payAction: ((ConsumerSubscription) -> Void)?
    
    override func configure(_ model: ConsumerSubscription) {
        super.configure(model)
        let color = Palette.ppColorNegative300.color

        descriptionLabel.text = ""
        paymentLabel.text = ""
        paymentLabel.attributedText = model.extraText.html2Attributed(font: paymentLabel.font,
                                                                      color: Palette.ppColorGrayscale400.color)
        paymentButton.setBackgroundColor(color, states: [.normal, .disabled])
        paymentButton.setBackgroundColor(color.withAlphaComponent(0.7), states: [.highlighted])
    }
    
    @IBAction private func pay(_ sender: Any){
        guard let subscription = subscription else {
            return
        }
        payAction?(subscription)
    }
    
}
