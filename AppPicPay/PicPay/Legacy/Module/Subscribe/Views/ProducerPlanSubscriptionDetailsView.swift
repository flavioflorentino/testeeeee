import UIKit

final class ProducerPlanSubscriptionDetailsView: UIView, SubscriptionDetailsWidget {
    lazy var details: SubscriptionDetailsView<ProducerPlanItem> = {
        let view = SubscriptionDetailsView<ProducerPlanItem>()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var shouldLoadConstraints:Bool = true
    
    public var onChangeButtonTap: ((Any?, ProducerPlanItem?) -> Void)? {
        get {
            return details.onChangeButtonTap
        }
        set {
            self.details.onChangeButtonTap = newValue
        }
    }
    
    override func updateConstraints() {
        if shouldLoadConstraints {
            loadConstraints()
            shouldLoadConstraints = false
        }
        super.updateConstraints()
    }
    
    private func loadConstraints() {
        addSubview(self.details)
        NSLayoutConstraint.activate([
            details.topAnchor.constraint(equalTo: topAnchor),
            details.bottomAnchor.constraint(equalTo: bottomAnchor),
            details.leftAnchor.constraint(equalTo: leftAnchor),
            details.rightAnchor.constraint(equalTo: rightAnchor)
        ])
    }
    
    func setPlan(_ plan:ProducerPlanItem?) {
        guard let item = plan else {
            return
        }
        details.setModel(item)
    }
    
    func viewMustUpdate() {
        details.viewMustUpdate()
    }
}

extension ProducerPlanItem: SubscriptionDetails {
    func getSubscriptionDetailInfo() -> String {
        return SubscribeLocalizable.planSigned.text
    }
    
    func getSubscriptionDetailTitle() -> String? {
        return self.name
    }
    
    func getSubscriptionDetailSubtitle() -> String {
        return self.planDescription
    }
}
