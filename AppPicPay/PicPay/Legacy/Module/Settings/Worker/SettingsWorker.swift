import Core
import Foundation
import FeatureFlag
import SecurityModule

typealias LoanInfoSettingsCompletionBlock = (Result<LoanSettingsResponse, ApiError>) -> Void
typealias RegistrationRenewalSettingsCompletionBlock = (Result<RegistrationRenewalSettings, PicPayError>) -> Void
typealias SettingsCompletionBlock<M: Decodable, E: Error> = (Result<M, E>) -> Void

protocol SettingsServicing {
    var isRegistrationRenewalEnabled: Bool { get }
    var isLoanSettingsEnabled: Bool { get }
    func appVersion() -> String?
    func biometricType() -> BiometricTypeAuth.BiometricType
    func shouldShowAppProtectionRow() -> Bool
    func isEnableCreditPicPay() -> Bool
    func isCardSettingsEnable(for account: CPAccount?) -> Bool
    func isPhysicalCardEnable(for account: CPAccount) -> Bool
    func isVirtualCardEnable(for account: CPAccount) -> Bool
    func isCardCreditSettingsEnabled(for account: CPAccount?) -> Bool
    func checkStudentAccountStatus() -> Bool
    func statusUpgradeAccount(onCacheLoaded: @escaping ((UpgradeAccountStatus) -> Void), completion: @escaping ((PicPayResult<UpgradeAccountStatus>) -> Void))
    func loadLoanInfoSettings(_ completion: @escaping LoanInfoSettingsCompletionBlock)
    func loadRegistrationRenewalSettings(_ completion: @escaping RegistrationRenewalSettingsCompletionBlock)
    func fetchP2PLendingSectionSettings(completion: @escaping SettingsCompletionBlock<P2PLendingSectionSettings, ApiError>)
}

final class SettingsWorker: BaseApi, SettingsServicing {
    typealias Dependencies = HasKVStore & HasFeatureManager
    private let dependencies: Dependencies

    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }

    var isLoanSettingsEnabled: Bool {
        FeatureManager.isActive(.loanSettings)
    }
    
    var isRegistrationRenewalEnabled: Bool {
        FeatureManager.isActive(.renewalSettings)
    }
    
    func isEnableCreditPicPay() -> Bool {
        return CreditPicPayHelper().isEnableCreditPicPay
    }
    
    func isPhysicalCardEnable(for account: CPAccount) -> Bool {
        return CreditPicPayHelper(with: account).isPhysicalCardEnable
    }
    
    func isVirtualCardEnable(for account: CPAccount) -> Bool {
        CreditPicPayHelper(with: account).isVirtualCardEnable
    }
    
    func isCardSettingsEnable(for account: CPAccount?) -> Bool {
        return CreditPicPayHelper(with: account).isCardSettingsEnable
    }
    
    func isCardCreditSettingsEnabled(for account: CPAccount?) -> Bool {
        CreditPicPayHelper(with: account).isCardCreditSettingsEnabled
    }
    
    func appVersion() -> String? {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    func biometricType() -> BiometricTypeAuth.BiometricType {
        return BiometricTypeAuth().biometricType()
    }

    func shouldShowAppProtectionRow() -> Bool {
        guard dependencies.featureManager.isActive(.releaseAppProtectionShowBool) else {
            return false
        }

        let valueForAppProtection = dependencies.kvStore.boolFor(AppProtectionKey.userDeviceProtectedValue)
        return DeviceSecurity.canEvaluate() || valueForAppProtection
    }
    
    func statusUpgradeAccount(onCacheLoaded: @escaping ((UpgradeAccountStatus) -> Void), completion: @escaping ((PicPayResult<UpgradeAccountStatus>) -> Void)) {
        requestManager.apiRequestWithCacheCodable(onCacheLoaded: { status in
            DispatchQueue.main.async {
                onCacheLoaded(status)
            }
        }, endpoint: kWsUrlUpgradeStatus, method: .get).responseApiCodable(strategy: .useDefaultKeys, completionHandler: completion)
    }
    
    func checkStudentAccountStatus() -> Bool {
        return KVStore().boolFor(.hasStudentAccountActive)
    }
    
    func loadLoanInfoSettings(_ completion: @escaping LoanInfoSettingsCompletionBlock) {
        let api = Api<LoanSettingsResponse>(endpoint: LoanSettingsEndpoint.settingsSection)
        api.execute { result in
            DispatchQueue.main.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func loadRegistrationRenewalSettings(_ completion: @escaping RegistrationRenewalSettingsCompletionBlock) {
        let api = Api<RegistrationRenewalSettings>(endpoint: SettingsEndpoint.registrationRenewal)
        api.execute { result in
            let mappedResult = result
                .mapError(\.picpayError)
                .map(\.model)
            
            DispatchQueue.main.async {
                completion(mappedResult)
            }
        }
    }
    
    func fetchP2PLendingSectionSettings(completion: @escaping SettingsCompletionBlock<P2PLendingSectionSettings, ApiError>) {
        let api = Api<P2PLendingSectionSettings>(endpoint: SettingsEndpoint.p2plendingSectionSettings)
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
            DispatchQueue.main.async {
                completion(result.map(\.model))
            }
        }
    }
}
