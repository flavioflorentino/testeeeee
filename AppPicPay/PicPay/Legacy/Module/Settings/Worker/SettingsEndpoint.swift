import Core
import Foundation

enum SettingsEndpoint {
    case registrationRenewal
    case p2plendingSectionSettings
}

extension SettingsEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .registrationRenewal:
            return "registration-manager/register/update/menu/settings"
        case .p2plendingSectionSettings:
            return "p2plending/configs/menu"
        }
    }
}
