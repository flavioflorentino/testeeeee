import Foundation

protocol ConsumerWorkerProtocol {
    var consumer: MBConsumer? {get}
}

final class ConsumerWorker: ConsumerWorkerProtocol {
    var consumer: MBConsumer? {
        return ConsumerManager.shared.consumer
    }
}
