//
//  LoadingHeaderView.swift
//  PicPay
//
//  Created by Marcos Timm on 08/03/2018.
//

import UIKit

final class LoadingHeaderView: UIView {
    
    var activityIndicator:UIActivityIndicatorView
    
    override init(frame: CGRect) {
        activityIndicator = UIActivityIndicatorView(style: .gray)
        
        super.init(frame: frame)
        
        var f = activityIndicator.frame
        f.origin.x = (frame.size.width / 2) - (f.size.width / 2)
        f.origin.y = (frame.size.height / 2) - (f.size.height / 2)
        activityIndicator.frame = f
        
        self.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

