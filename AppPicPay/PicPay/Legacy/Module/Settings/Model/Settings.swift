import Foundation

final class SettingsSection {
    let title: SettingsStrings
    var rows: [SettingsRow]
    
    init(title: SettingsStrings, rows: [SettingsRow]) {
        self.title = title
        self.rows = rows
    }
}

final class SettingsRow {
    let title: SettingsStrings
    let subtitle: String
    let cell: TypeCell
    let action: Action
    
    init(title: SettingsStrings, subtitle: String? = nil, cell: TypeCell = .normal, action: Action) {
        self.title = title
        self.subtitle = subtitle ?? ""
        self.cell = cell
        self.action = action
    }
}

extension SettingsRow {
    enum TypeCell: String, Decodable {
        case cellSwitch
        case pending
        case analysis
        case normal
        case alert
        case logout
        case new
    }
    
    enum Action {
        case open(TypeAction)
        case switchButton(TypeAction)
        case actionSheet(TypeAction)
    }
    
    enum TypeAction {        
        case myPicPay
        case myNumber
        case myNumberCheck
        case myEmail
        case personalData
        case bankAccount
        case incomeStatements
        case bankAccountCheck(PPBankAccount)
        case taxAndLimits
        case myAddress
        case linkedAccounts
        case myFavorites
        case identityVerification
        case upgradeAccount
        case registrationRenewal

        case myPixKeys
        case myPixLimit
        
        case creditHome(CPAccount)
        case creditCardDigital
        case creditInvoices(CPAccount)
        case creditLimit
        case creditBestDay(CPAccount)
        case configureCard
        case virtualCard(CPAccount)
        
        case benefits
        case studentAccount
        case promotionalCode
        case invateYourFriends
        case recommendStores
        
        case mySignatures
        
        case noFreeParcelling
        case leavePicPayPro
        
        case picpayPRO
        case forEstablishment
        case saleSubscriptions
                
        case biometric
        case changePassword
        case privacity
        case notifications
        case analytics
        
        case beta
        case help
        case about
        case deactivateAccount
        
        case loanInfo
        case loanAgreements
        case loanAgreementsTrack
        
        case p2plendingProposals
        case p2plendingInvestiments
        
        case logout
        case featureFlagControl
        
        case protectAccess
    }
}
