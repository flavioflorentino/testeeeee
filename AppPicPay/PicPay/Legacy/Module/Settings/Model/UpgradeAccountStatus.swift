import Foundation

final class UpgradeAccountStatus: Codable {
    let enabled: Bool
    let status: UpgradeStatus
    let label: String?
}

extension UpgradeAccountStatus {
    enum UpgradeStatus: String, Codable {
        case notStarted = "NOT_STARTED"
        case pending = "PENDING"
        case analysis = "ANALYSIS"
        case rejected = "REJECTED"
        case approved = "APPROVED"
    }
}
