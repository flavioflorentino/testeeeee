import Foundation

struct SettingsBankInfo: Decodable {
    let agency: String
    let account: String
    let bankCode: String
    let bankName: String
    let ispb: String?
}
