struct P2PLendingSectionSettings: Decodable {
    let enable: Bool
    let options: P2PLendingRowsSettings
}

struct P2PLendingRowsSettings: Decodable {
    let myOffers: Bool
    let myInvestments: Bool
    let help: Bool
}
