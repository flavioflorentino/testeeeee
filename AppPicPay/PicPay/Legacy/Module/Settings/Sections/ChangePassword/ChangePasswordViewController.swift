import UIKit
import UI
import Validator

final class ChangePasswordViewController: BaseFormController {
    private let viewModel: ChangePasswordViewModel
    
    @IBOutlet weak private var currentPasswordField: UIPPFloatingTextField!
    @IBOutlet weak private var newPasswordField: UIPPFloatingTextField!
    @IBOutlet weak private var repeatPasswordField: UIPPFloatingTextField!
    
    init() {
        viewModel = ChangePasswordViewModel()
        super.init(nibName: "ChangePasswordViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        configureValidations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        currentPasswordField.becomeFirstResponder()
    }
    
    @IBAction private func save(_ sender: Any) {
        if validate().isValid {
            sendForm()
        }
    }
}

extension ChangePasswordViewController {
    private func setupViews() {
        title = SettingsLocalizable.changePassword.text
        addTextField(currentPasswordField)
        addTextField(newPasswordField)
        addTextField(repeatPasswordField)
        
        if ConsumerManager.shared.consumer?.numericKeyboardForPassword == true {
            currentPasswordField.keyboardType = .numberPad
        }
    }
    
    private func configureValidations() {
        var fieldRules = ValidationRuleSet<String>()
        fieldRules.add(rule: ValidationRuleLength(min: 1, error: ValidateErrors.fieldRequired))
        
        currentPasswordField.validationRules = fieldRules
        newPasswordField.validationRules = fieldRules
        repeatPasswordField.validationRules = fieldRules
    }
    
    private func sendForm() {
        guard let currentPassword = currentPasswordField.text else {
            return
        }
        guard let newPassword = newPasswordField.text else {
            return
        }
        guard let repeatPassword = repeatPasswordField.text else {
            return
        }
        
        if viewModel.verifyPassword(newPassword: newPassword, repeatPassword: repeatPassword) {
           savePassword(currentPassword: currentPassword, newPassword: newPassword)
        } else {
            self.showAlert(message: SettingsLocalizable.newPasswordsAreDifferent.text, dismiss: false)
        }
    }
    
    private func savePassword(currentPassword: String, newPassword: String) {
        startLoading()
        viewModel.saveChangePassword(currentPassword: currentPassword, newPassword: newPassword) { [weak self] error in
            guard let strongSelf = self else {
            return
        }
            strongSelf.stopLoading()
            if let erro = error {
                AlertMessage.showCustomAlertWithError(erro, controller: strongSelf)
            } else {
                strongSelf.showAlert(message: SettingsLocalizable.passwordChangedSuccessfully.text, dismiss: true)
            }
        }
    }
    
    private func showAlert(message: String, dismiss: Bool) {
        let buttons = [Button(title: "Ok")]
        let alert = Alert(with: "", text: message, buttons: buttons)
        AlertMessage.showAlert(alert, controller: self) { [weak self] _, _, _ in
            if dismiss {
                self?.navigationController?.popViewController(animated: true)
            }
        }
    }
}

extension ChangePasswordViewController {
    enum ValidateErrors: String, Error, CustomStringConvertible, ValidationError {
        case fieldRequired = "Campo obrigatório"
        
        var description: String {
            return self.rawValue
        }
        
        var message: String {
            return self.rawValue
        }
    }
}
