import Foundation

final class ChangePasswordViewModel {
    private let api: ChangePasswordApi
    
    init(api: ChangePasswordApi = ServiceChangePasswordApi()) {
        self.api = api
    }
    
    func saveChangePassword(currentPassword: String, newPassword: String, completion: @escaping (Error?) -> Void) {
        api.passwordChangeRequest(currentPassword: currentPassword, newPassword: newPassword) { sucess, error in
            if sucess {
                completion(nil)
            } else {
                completion(error)
            }
        }
    }
    
    func verifyPassword(newPassword: String, repeatPassword: String) -> Bool {
        if newPassword == repeatPassword {
            return true
        } else {
            return false
        }
    }
}
