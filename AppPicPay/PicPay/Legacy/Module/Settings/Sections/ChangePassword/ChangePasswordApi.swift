import Foundation

protocol ChangePasswordApi {
    func passwordChangeRequest(currentPassword: String, newPassword: String, completion: @escaping (Bool, Error?) -> Void)
}

final class ServiceChangePasswordApi: ChangePasswordApi {
    func passwordChangeRequest(currentPassword: String, newPassword: String, completion: @escaping (Bool, Error?) -> Void) {
        WSPasswordChangeRequest.changePassword(currentPassword, newPassword: newPassword) { sucess, error in
            DispatchQueue.main.async {
                completion(sucess, error)
            }
        }
    }
}
