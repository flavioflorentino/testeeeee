import UI
import UIKit

protocol DisclosureSettingsCellDelegate: AnyObject {
    func didTapChevronButton(settingKey: ToggleSettingsKey, indexPath: IndexPath)
}

private extension DisclosureSettingsCell.Layout {
    enum Size {
        static let button: CGFloat = 24
    }
}

final class DisclosureSettingsCell: UITableViewCell, ViewConfiguration {
    fileprivate enum Layout { }

    weak var delegate: DisclosureSettingsCellDelegate?
    private var settingKey: ToggleSettingsKey?
    private var indexPath: IndexPath?

    private lazy var titleLabel = UILabel()

    private lazy var chevronButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.Icons.rightArrow.image, for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        return button
    }()

    private lazy var permissionStatusLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildViewHierarchy() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(chevronButton)
        contentView.addSubview(permissionStatusLabel)
    }

    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }

        chevronButton.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.size.equalTo(Layout.Size.button)
        }

        permissionStatusLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.equalTo(titleLabel.snp.leading)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
        }
    }

    func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
        textLabel?.textColor = Palette.ppColorGrayscale600.color
    }

    func setup(model: ToggleSettingsCellModel, indexPath: IndexPath) {
        self.indexPath = indexPath
        settingKey = model.type
        titleLabel.text = model.label
        permissionStatusLabel.text = model.detailLabel
        permissionStatusLabel.textColor = model.detailLabelColor
        selectionStyle = .none
    }
}

@objc
private extension DisclosureSettingsCell {
    func buttonAction() {
        guard let key = settingKey, let index = indexPath else {
            return
        }
        delegate?.didTapChevronButton(settingKey: key, indexPath: index)
    }
}
