import UI
import UIKit

final class ToggleSettingsViewController: PPBaseViewController {
    typealias Localizable = Strings.UpdatePrivacy
    private var viewModel: ToggleSettingsViewModelProtocol
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = Palette.ppColorGrayscale100.color
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    init() {
        viewModel = NotificationSettingsViewModel()
        super.init(nibName: nil, bundle: nil)
        viewModel.delegate = self
    }
    
    init(viewModel: ToggleSettingsViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        loadSettings()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(loadSettings),
            name: UIApplication.didBecomeActiveNotification,
            object: nil
        )
    }
    
    private func setupSubviews() {
        title = viewModel.title
        
        tableView.register(ToggleSettingsCell.self, forCellReuseIdentifier: ToggleSettingsCell.identifier)
        tableView.register(DisclosureSettingsCell.self, forCellReuseIdentifier: DisclosureSettingsCell.identifier)
        
        view.addSubview(tableView)
        NSLayoutConstraint.constraintAllEdges(from: tableView, to: view)
    }

    @objc
    private func loadSettings() {
        startLoadingView()
        viewModel.loadSettings(onSuccess: { [weak self] in
            self?.stopLoadingView()
            self?.tableView.reloadData()
        }, onError: { [weak self] error in
            self?.stopLoadingView()
            AlertMessage.showAlert(withMessage: error.localizedDescription, controller: self)
            self?.navigationController?.popViewController(animated: true)
        })
    }
}

extension ToggleSettingsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsAtSection(section)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.titleForHeaderAtSection(section)
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return viewModel.titleForFooterInSection(section)
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = viewModel.cellModelFor(section: indexPath.section, row: indexPath.row) else {
            return UITableViewCell()
        }

        if model.type == .contactsPermission || model.type == .locationPermission {
            guard
                let disclosureCell = dequeueCell(from: tableView, at: indexPath, type: DisclosureSettingsCell.self)
                else {
                    return UITableViewCell()
            }
            disclosureCell.delegate = viewModel
            disclosureCell.setup(model: model, indexPath: indexPath)
            return disclosureCell
        }

        guard
            let toggleCell = dequeueCell(from: tableView, at: indexPath, type: ToggleSettingsCell.self)
            else {
                return UITableViewCell()
        }

        toggleCell.delegate = viewModel
        toggleCell.setup(model: model, indexPath: indexPath)
        return toggleCell
    }

    private func dequeueCell<T: UITableViewCell>(
        from tableView: UITableView,
        at indexPath: IndexPath,
        type: T.Type
    ) -> T? {
        return tableView.dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as? T
    }
}

extension ToggleSettingsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as? UITableViewHeaderFooterView
        header?.textLabel?.textColor = Palette.ppColorGrayscale500.color
    }

    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        let footer = view as? UITableViewHeaderFooterView
        footer?.textLabel?.textColor = Palette.ppColorGrayscale500.color
    }
}

extension ToggleSettingsViewController: ToggleSettingsViewControllerDelegate {
    func reloadTableViewCell(at indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.tableView.reloadRows(at: [indexPath], with: .none)
        }
    }
    
    func showAlert(withMessage message: String) {
        AlertMessage.showAlert(withMessage: message, controller: self)
    }
    
    func showConfirmationPopup(with model: PrivacySettingsPopupViewModel) {
        let confirmationPopup = PrivacySettingsPopupViewController(viewModel: model)
        present(confirmationPopup, animated: true)
    }
    
    func showProfileConfirmationPopup(_ yesCompletion: @escaping () -> Void, _ noCompletion: @escaping () -> Void) {
        let primaryButtonAction = ApolloAlertAction(title: Localizable.updatePrivacyPopupProfileNo,
                                                    completion: noCompletion)
        let linkButtonAction = ApolloAlertAction(title: Localizable.updatePrivacyPopupProfileYes,
                                                 completion: yesCompletion)
        
        showApolloAlert(title: Localizable.updatePrivacyPopupProfileTitle,
                        subtitle: Localizable.updatePrivacyPopupProfileDescription,
                        primaryButtonAction: primaryButtonAction,
                        linkButtonAction: linkButtonAction,
                        dismissOnTouchOutside: false)
    }
}
