import UI
import UIKit

protocol ToggleSettingsCellDelegate: AnyObject {
    func switchDidChange(to isOn: Bool, indexPath: IndexPath, settingKey: ToggleSettingsKey)
}

final class ToggleSettingsCell: UITableViewCell {
    weak var delegate: ToggleSettingsCellDelegate?
    private var indexPath: IndexPath?
    private var settingKey: ToggleSettingsKey?
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .gray)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.color = Colors.brandingBase.color
        return spinner
    }()
    
    private lazy var switchButton: UISwitch = {
        let switchButton = UISwitch()
        switchButton.translatesAutoresizingMaskIntoConstraints = false
        switchButton.addTarget(self, action: #selector(switchDidChange), for: .valueChanged)
        return switchButton
    }()
    
    private var isLoading: Bool = false {
        didSet {
            activityIndicator.isHidden = !isLoading
            switchButton.isHidden = isLoading
            isLoading ? activityIndicator.startAnimating() : activityIndicator.stopAnimating()
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupSubviews()
    }
    
    func setup(model: ToggleSettingsCellModel, indexPath: IndexPath) {
        self.indexPath = indexPath
        settingKey = model.type
        isLoading = model.isLoading
        textLabel?.text = model.label
        switchButton.setOn(model.isOn, animated: false)
        selectionStyle = .none
    }

    private func setupSubviews() {
        backgroundColor = Palette.ppColorGrayscale000.color
        textLabel?.textColor = Palette.ppColorGrayscale600.color

        addSubview(activityIndicator)
        addSubview(switchButton)
        NSLayoutConstraint.activate([
            switchButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            switchButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor),
            activityIndicator.centerXAnchor.constraint(equalTo: switchButton.centerXAnchor)
        ])
    }
    
    @objc
    private func switchDidChange() {
        guard let key = settingKey, let index = indexPath else {
            return
        }
        delegate?.switchDidChange(to: switchButton.isOn, indexPath: index, settingKey: key)
    }
}
