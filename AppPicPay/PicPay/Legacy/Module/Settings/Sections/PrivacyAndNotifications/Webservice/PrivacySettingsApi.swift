protocol PrivacySettingsApiProtocol {
    func getPrivacySettings(completion: @escaping (PicPayResult<PrivacySettingsModel>) -> Void)
    func setPrivacySettings(configs: [String: Bool], completion: @escaping (PicPayResult<BaseCodableEmptyResponse>) -> Void)
}

final class PrivacySettingsApi: BaseApi, PrivacySettingsApiProtocol {
    func getPrivacySettings(completion: @escaping (PicPayResult<PrivacySettingsModel>) -> Void) {
        requestManager.apiRequest(endpoint: kWsUrlConsumerConfig, method: .get).responseApiCodable(completionHandler: { result in
            completion(result)
        })
    }
    
    func setPrivacySettings(configs: [String: Bool], completion: @escaping (PicPayResult<BaseCodableEmptyResponse>) -> Void) {
        requestManager.apiRequest(endpoint: kWsUrlConsumerConfig, method: .patch, parameters: configs).responseApiCodable(completionHandler: { result in
            completion(result)
        })
    }
}
