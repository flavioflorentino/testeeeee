protocol NotificationSettingsApiProtocol {
    func getNotificationSettings(completion: @escaping (PicPayResult<NotificationSettingsModel>) -> Void)
    func setNotificationSettings(configs: [String: Bool], completion: @escaping (PicPayResult<BaseCodableEmptyResponse>) -> Void)
}

final class NotificationSettingsApi: BaseApi, NotificationSettingsApiProtocol {
    func getNotificationSettings(completion: @escaping (PicPayResult<NotificationSettingsModel>) -> Void) {
        requestManager.apiRequest(endpoint: kWsUrlNotificationConfig, method: .get).responseApiCodable(completionHandler: { result in
            completion(result)
        })
    }
    
    func setNotificationSettings(configs: [String: Bool], completion: @escaping (PicPayResult<BaseCodableEmptyResponse>) -> Void) {
        requestManager.apiRequest(endpoint: kWsUrlNotificationConfig, method: .patch, parameters: configs).responseApiCodable(completionHandler: { result in
            completion(result)
        })
    }
}
