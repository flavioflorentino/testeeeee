struct NotificationSettingsModel: Codable {
    let likePush: Bool
    let commentPush: Bool
    let followerPush: Bool
    let paymentPush: Bool
    let birthdayPush: Bool
    let newsPush: Bool
    let paymentEmail: Bool
    let sendChargeToConsumerPush: Bool?
}
