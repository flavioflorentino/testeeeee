struct ToggleSettingsSessionModel {
    var header: String
    var footer: String
    var rows: [ToggleSettingsCellModel]
    
    init(header: SettingsLocalizable? = nil, footer: SettingsLocalizable? = nil, rows: [ToggleSettingsCellModel]) {
        self.header = header?.text ?? ""
        self.footer = footer?.text ?? ""
        self.rows = rows
    }
}
