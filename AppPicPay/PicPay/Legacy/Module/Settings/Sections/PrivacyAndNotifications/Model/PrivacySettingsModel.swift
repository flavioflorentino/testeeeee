struct PrivacySettingsModel: Codable {
    let closedAccount: Bool
    let receiveInPrivate: Bool
    let paymentInPrivate: Bool?
    let notifyBirthday: Bool
    let paymentRequest: Bool?
    let reviewed: Bool
}
