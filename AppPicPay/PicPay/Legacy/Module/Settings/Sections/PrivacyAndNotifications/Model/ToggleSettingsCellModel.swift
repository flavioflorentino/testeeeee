struct ToggleSettingsCellModel {
    let type: ToggleSettingsKey
    let label: String
    let detailLabel: String?
    let detailLabelColor: UIColor?
    var isLoading: Bool
    var isOn: Bool
    
    init(
        type: ToggleSettingsKey,
        label: SettingsLocalizable,
        detailLabel: SettingsLocalizable? = nil,
        detailLabelColor: UIColor? = nil,
        isLoading: Bool = false,
        isOn: Bool
    ) {
        self.type = type
        self.label = label.text
        self.detailLabel = detailLabel?.text
        self.detailLabelColor = detailLabelColor
        self.isOn = isOn
        self.isLoading = isLoading
    }
}

enum ToggleSettingsKey: String {
    // Notification keys
    case likePush
    case commentPush
    case followerPush
    case paymentPush
    case birthdayPush
    case newsPush
    case paymentEmail
    case sendChargeToConsumerPush
    case directMessageSendBirdPush
    
    // Privacy keys
    case closedAccount
    case paymentInPrivate
    case receiveInPrivate
    case notifyBirthday
    case paymentRequest
    case contactsPermission
    case locationPermission
}
