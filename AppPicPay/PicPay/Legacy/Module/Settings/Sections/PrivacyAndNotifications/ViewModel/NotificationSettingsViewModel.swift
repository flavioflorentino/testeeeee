import AnalyticsModule
import DirectMessageSB

final class NotificationSettingsViewModel: ToggleSettingsViewModelProtocol {
    // MARK: - Variables
    private var api: NotificationSettingsApiProtocol
    private var sections = [ToggleSettingsSessionModel]()
    private var notificationSettings: NotificationSettingsModel?
    var title = DefaultLocalizable.notifications.text
    weak var delegate: ToggleSettingsViewControllerDelegate?
    private let analytics: AnalyticsProtocol
    private let application: UIApplicationContract
    private let featureManager: FeatureManagerContract
    private let chatApiManager: ChatApiManaging
    
    // MARK: - Life Cycle
    init(
        api: NotificationSettingsApiProtocol = NotificationSettingsApi(),
        analytics: AnalyticsProtocol = Analytics.shared,
        application: UIApplicationContract = UIApplication.shared,
        featureManager: FeatureManagerContract = FeatureManager.shared,
        chatApiManager: ChatApiManaging = ChatApiManager.shared
    ) {
        self.api = api
        self.analytics = analytics
        self.application = application
        self.featureManager = featureManager
        self.chatApiManager = chatApiManager
    }
    
    // MARK: - Settings Initializer
    func loadSettings(onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        api.getNotificationSettings { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                self.notificationSettings = response
                self.configureTableView(response)
                self.loadDirectMessageSBCellModel()
                onSuccess()
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    func configureTableView(_ settings: NotificationSettingsModel) {
        var newSections = [ToggleSettingsSessionModel]()
        var rows = [
            ToggleSettingsCellModel(
                type: .likePush,
                label: .newLikes,
                isOn: settings.likePush
            ),
            ToggleSettingsCellModel(
                type: .commentPush,
                label: .newComments,
                isOn: settings.commentPush
            ),
            ToggleSettingsCellModel(
                type: .followerPush,
                label: .newFollowers,
                isOn: settings.followerPush
            ),
            ToggleSettingsCellModel(
                type: .paymentPush,
                label: .newPayments,
                isOn: settings.paymentPush
            ),
            ToggleSettingsCellModel(
                type: .birthdayPush,
                label: .birthdaysFromWhoIFollow,
                isOn: settings.birthdayPush
            ),
            ToggleSettingsCellModel(
                type: .newsPush,
                label: .newsAndPromotions,
                isOn: settings.newsPush
            )
        ]
        
        if let sendChargeToConsumerPush = settings.sendChargeToConsumerPush {
            rows.append(
                ToggleSettingsCellModel(
                    type: .sendChargeToConsumerPush,
                    label: .sendChargeToConsumerPush,
                    isOn: sendChargeToConsumerPush
                )
            )
        }
        
        if featureManager.isActive(.directMessageSBEnabled) {
            rows.append(
                ToggleSettingsCellModel(
                    type: .directMessageSendBirdPush,
                    label: .directMessageSendBirdPush,
                    isLoading: true,
                    isOn: true
                )
            )
        }
        
        newSections.append(ToggleSettingsSessionModel(header: .receiveNotificationsOnPhone, rows: rows))
        
        rows = [
            ToggleSettingsCellModel(
                type: .paymentEmail,
                label: .newPayments,
                isOn: settings.paymentEmail
            )
        ]
        newSections.append(ToggleSettingsSessionModel(header: .receiveNotificationsByEmail, rows: rows))
        sections = newSections
    }
    
    // MARK: - Settings Update
    func updateSetting(
        at index: IndexPath,
        isOn: Bool,
        settingName: String,
        settingKey: ToggleSettingsKey,
        onSuccess: @escaping () -> Void,
        onError: @escaping (Error) -> Void
    ) {
        api.setNotificationSettings(configs: [settingName: isOn]) { [weak self] result in
            switch result {
            case .success:
                self?.callUpdateSettingAnalytics(settingKey: settingKey, isOn: isOn)
                self?.updateCellInfoState(at: index, isLoading: false, isOn: isOn)
                onSuccess()
            case .failure(let error):
                self?.updateCellInfoState(at: index, isLoading: false, isOn: !isOn)
                onError(error)
            }
        }
    }
    
    // MARK: - TableView Build
    func updateCellInfoState(at index: IndexPath, isLoading: Bool, isOn: Bool) {
        guard
            sections.indices.contains(index.section),
            sections[index.section].rows.indices.contains(index.row)
            else {
                return
        }
        sections[index.section].rows[index.row].isLoading = isLoading
        sections[index.section].rows[index.row].isOn = isOn
    }
    
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func numberOfRowsAtSection(_ section: Int) -> Int {
        return sections.indices.contains(section) ? sections[section].rows.count : 0
    }
    
    func cellModelFor(section: Int, row: Int) -> ToggleSettingsCellModel? {
        guard
            sections.indices.contains(section),
            sections[section].rows.indices.contains(row)
            else {
                return nil
        }
        return sections[section].rows[row]
    }
    
    func titleForHeaderAtSection(_ section: Int) -> String {
        return sections.indices.contains(section) ? sections[section].header : ""
    }
    
    func titleForFooterInSection(_ section: Int) -> String {
        return sections.indices.contains(section) ? sections[section].footer : ""
    }
    
    private func callUpdateSettingAnalytics(settingKey: ToggleSettingsKey, isOn: Bool) {
        guard case .sendChargeToConsumerPush = settingKey else {
            return
        }
        analytics.log(PaymentRequestUserEvent.notificationChange(origin: .adjustments, isOn: isOn))
    }
}
// MARK: - ToggleSettingsCellDelegate & DisclosureSettingsCellDelegate
extension NotificationSettingsViewModel: ToggleSettingsCellDelegate {
    func switchDidChange(to isOn: Bool, indexPath: IndexPath, settingKey: ToggleSettingsKey) {
        if settingKey == .directMessageSendBirdPush {
            updateDirectMessageSBPushOption(to: isOn)
            return
        }
        
        guard let settingName = settingKey.rawValue.snakeCased() else {
            return
        }
        
        updateCellInfoState(at: indexPath, isLoading: true, isOn: isOn)
        delegate?.reloadTableViewCell(at: indexPath)
        
        updateSetting(at: indexPath, isOn: isOn, settingName: settingName, settingKey: settingKey, onSuccess: { [weak self] in
            self?.delegate?.reloadTableViewCell(at: indexPath)
            }, onError: { [weak self] error in
                self?.delegate?.showAlert(withMessage: error.localizedDescription)
                self?.delegate?.reloadTableViewCell(at: indexPath)
        })
    }

    func didTapChevronButton(settingKey: ToggleSettingsKey, indexPath: IndexPath) {
        guard
            let url = URL(string: UIApplication.openSettingsURLString),
            application.canOpenURL(url)
            else {
                return
        }
        application.open(url)
    }
}

// MARK: - Direct Message
extension NotificationSettingsViewModel {
    var directMessagePushIndexPath: IndexPath? {
        guard featureManager.isActive(.directMessageSBEnabled),
              let sendBirdCellIndex = sections.first?.rows.enumerated()
                .first(where: { $0.element.type == .directMessageSendBirdPush })?
                .offset
        else { return nil }
        
        return IndexPath(row: sendBirdCellIndex, section: 0)
    }
    
    func loadDirectMessageSBCellModel(completion: ((Bool) -> Void)? = nil) { 
        guard let directMessageIndexPath = directMessagePushIndexPath else { return }
        
        chatApiManager.getPushNotificationOption { [weak self] result in
            switch result {
            case let .success(option):
                self?.updateCellState(at: directMessageIndexPath, isLoading: false, isOn: option == .all)
                completion?(true)
            case .failure:
                self?.updateCellState(at: directMessageIndexPath, isLoading: true, isOn: true)
                completion?(false)
            }
        }
    }
    
    func updateDirectMessageSBPushOption(to isOn: Bool, completion: ((Bool) -> Void)? = nil) {
        guard let directMessageIndexPath = directMessagePushIndexPath else { return }

        analytics.log(.button(.toggled, .generalNotificationSettings(status: isOn ? .turnOn : .turnOff)))
        
        updateCellState(at: directMessageIndexPath, isLoading: true, isOn: isOn)
        
        chatApiManager.setPushNotificationOption(isOn ? .all: .off) { [weak self] result in
            switch result {
            case .success:
                self?.updateCellState(at: directMessageIndexPath, isLoading: false, isOn: isOn)
                completion?(true)
            case .failure:
                self?.handleError(isOn: isOn)
                self?.updateCellState(at: directMessageIndexPath, isLoading: false, isOn: !isOn)
                completion?(false)
            }
        }
    }
    
    private func updateCellState(at indexPath: IndexPath, isLoading: Bool, isOn: Bool) {
        self.updateCellInfoState(at: indexPath,
                                 isLoading: isLoading,
                                 isOn: isOn)
        self.delegate?.reloadTableViewCell(at: indexPath)
    }
    
    private func handleError(isOn: Bool) {
        let message = isOn ?
            SettingsLocalizable.directMessageSendBirdPushTurningOnError.text :
            SettingsLocalizable.directMessageSendBirdPushTurningOffError.text
        self.delegate?.showAlert(withMessage: message)
    }
}
