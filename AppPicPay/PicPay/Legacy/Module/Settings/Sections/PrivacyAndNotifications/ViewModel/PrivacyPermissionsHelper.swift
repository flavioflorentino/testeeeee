import Foundation
import PermissionsKit
import UI

enum PrivacyPermissionState {
    case activated
    case deactivated
    case notDetermined

    var text: SettingsLocalizable {
        switch self {
        case .activated:
            return .permissionActivated
        default:
            return .permissionDeactivated
        }
    }

    var color: UIColor {
        switch self {
        case .activated:
            return Colors.success900.color
        default:
            return Colors.critical900.color
        }
    }
}

protocol PrivacyPermissionsHelperProtocol {
    var contactsStateText: SettingsLocalizable { get }
    var contactsStateColor: UIColor { get }
    var shouldShowContactSettings: Bool { get }
    var locationStateText: SettingsLocalizable { get }
    var locationStateColor: UIColor { get }
    var shouldShowLocationSettings: Bool { get }
    func updateContactsState()
    func updateLocationState()
    func askForContactsPermission(completion: @escaping () -> Void)
    func askForLocationWhenInUsePermission(completion: @escaping () -> Void)

}

final class PrivacyPermissionsHelper: PrivacyPermissionsHelperProtocol {
    private var contactsState: PrivacyPermissionState = .notDetermined
    private var locationState: PrivacyPermissionState = .notDetermined

    var contactStatus = Permission.contacts.status
    var locationAlwaysStatus = Permission.locationAlways.status
    var locationWhenInUseStatus = Permission.locationWhenInUse.status

    var contactsStateText: SettingsLocalizable { contactsState.text }
    var contactsStateColor: UIColor { contactsState.color }
    var shouldShowContactSettings: Bool {
        contactStatus != .notDetermined
    }

    var locationStateText: SettingsLocalizable { locationState.text }
    var locationStateColor: UIColor { locationState.color }
    var shouldShowLocationSettings: Bool {
        locationAlwaysStatus != .notDetermined || locationWhenInUseStatus != .notDetermined
    }

    func updateContactsState() {
        switch contactStatus {
        case .authorized:
            contactsState = .activated
        case .notDetermined:
            contactsState = .notDetermined
        default:
            contactsState = .deactivated
        }
    }

    func updateLocationState() {
        if locationAlwaysStatus == .authorized || locationWhenInUseStatus == .authorized {
            locationState = .activated
        } else if locationAlwaysStatus == .notDetermined && locationWhenInUseStatus == .notDetermined {
            locationState = .notDetermined
        } else {
            locationState = .deactivated
        }
    }

    func askForContactsPermission(completion: @escaping () -> Void) {
        Permission.contacts.request { [weak self] status in
            self?.contactStatus = status
            self?.updateContactsState()
            completion()
        }
    }

    func askForLocationWhenInUsePermission(completion: @escaping () -> Void) {
        Permission.locationWhenInUse.request { [weak self] status in
            self?.locationWhenInUseStatus = status
            self?.updateLocationState()
            completion()
        }
    }
}
