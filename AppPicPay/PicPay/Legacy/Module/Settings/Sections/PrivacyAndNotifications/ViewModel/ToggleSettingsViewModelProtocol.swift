protocol ToggleSettingsViewModelProtocol: ToggleSettingsCellDelegate, DisclosureSettingsCellDelegate {
    var title: String { get }
    var delegate: ToggleSettingsViewControllerDelegate? { get set }
    func loadSettings(onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void)
    func updateSetting(at index: IndexPath, isOn: Bool, settingName: String, settingKey: ToggleSettingsKey, onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void)
    func updateCellInfoState(at index: IndexPath, isLoading: Bool, isOn: Bool)
    func numberOfSections() -> Int
    func numberOfRowsAtSection(_ section: Int) -> Int
    func cellModelFor(section: Int, row: Int) -> ToggleSettingsCellModel?
    func titleForHeaderAtSection(_ section: Int) -> String
    func titleForFooterInSection(_ section: Int) -> String
}

protocol ToggleSettingsViewControllerDelegate: AnyObject {
    func reloadTableViewCell(at indexPath: IndexPath)
    func showAlert(withMessage message: String)
    func showConfirmationPopup(with model: PrivacySettingsPopupViewModel)
    func showProfileConfirmationPopup(_ yesCompletion: @escaping () -> Void, _ noCompletion: @escaping () -> Void)
}
