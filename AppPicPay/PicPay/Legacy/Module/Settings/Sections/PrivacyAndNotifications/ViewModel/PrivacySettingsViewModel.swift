import AnalyticsModule
import Foundation
import UIKit.UIApplication

final class PrivacySettingsViewModel: ToggleSettingsViewModelProtocol {
    private typealias Localizable = Strings.UpdatePrivacy
    typealias Dependencies = HasFeatureManager & HasConsumerManager & HasAnalytics
    
    // MARK: - Variables
    private let dependencies: Dependencies
    private var api: PrivacySettingsApiProtocol
    private var sections = [ToggleSettingsSessionModel]()
    private var privacySettings: PrivacySettingsModel?
    var title = SettingsLocalizable.privacySettingsTitle.text
    weak var delegate: ToggleSettingsViewControllerDelegate?
    private let application: UIApplicationContract
    private let permissionsHelper: PrivacyPermissionsHelperProtocol
    
    // As 3 variáveis abaixo foram criadas para ajudar na revisão de atividades passadas
    private var settingName: String = ""
    private var settingKey: ToggleSettingsKey?
    private var selectedIndex: IndexPath?
    
    // MARK: - Life Cycle
    init(
        api: PrivacySettingsApiProtocol = PrivacySettingsApi(),
        application: UIApplicationContract = UIApplication.shared,
        permissionsHelper: PrivacyPermissionsHelperProtocol = PrivacyPermissionsHelper(),
        dependencies: Dependencies
    ) {
        self.api = api
        self.application = application
        self.permissionsHelper = permissionsHelper
        self.dependencies = dependencies
    }
    
    // MARK: - Settings Initializer
    func loadSettings(onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        api.getPrivacySettings { [weak self] result in
            switch result {
            case .success(let response):
                self?.privacySettings = response
                self?.configureTableView(response)
                onSuccess()
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    func configureTableView(_ settings: PrivacySettingsModel) {
        var newSections = [ToggleSettingsSessionModel]()
        
        let closedAccountRow = ToggleSettingsCellModel(
            type: .closedAccount,
            label: .openedAccount,
            isOn: !settings.closedAccount
        )
        let closedAccountSessionModel = ToggleSettingsSessionModel(
            footer: .openedAccountDescription,
            rows: [closedAccountRow]
        )
        newSections.append(closedAccountSessionModel)
        
        let receiveInPrivateRow = ToggleSettingsCellModel(
            type: .receiveInPrivate,
            label: .receiveInPublic,
            isOn: !settings.receiveInPrivate
        )
        let receiveInPrivateSessionModel = ToggleSettingsSessionModel(
            footer: .receiveInPublicDescription,
            rows: [receiveInPrivateRow]
        )
        newSections.append(receiveInPrivateSessionModel)
        
        // TODO - esse bloco abaixo só será mantido enquanto o serviço não adicionar a propriedade payment_in_ptivate no retorno
        let paymentInPrivate: Bool

        if let payInPrivate = settings.paymentInPrivate {
            paymentInPrivate = !payInPrivate
        } else {
            paymentInPrivate = !(ConsumerManager.shared.privacyConfig == FeedItemVisibilityInt.private.rawValue)
        }
        
        let payInPrivateRow = ToggleSettingsCellModel(
            type: .paymentInPrivate,
            label: .payInPublic,
            isOn: paymentInPrivate
        )
        let payInPrivateSessionModel = ToggleSettingsSessionModel(
            footer: .payInPublicDescription,
            rows: [payInPrivateRow]
        )
        newSections.append(payInPrivateSessionModel)
        
        let notifyBirthdayRow = ToggleSettingsCellModel(
            type: .notifyBirthday,
            label: .notifyBirthday,
            isOn: settings.notifyBirthday
        )
        let notifyBirthdaySessionModel = ToggleSettingsSessionModel(
            footer: .notifyBirthdayDescription,
            rows: [notifyBirthdayRow]
        )
        newSections.append(notifyBirthdaySessionModel)
        
        if let paymentRequest = settings.paymentRequest {
            let paymentRequestRow = ToggleSettingsCellModel(
                type: .paymentRequest,
                label: .paymentRequest,
                isOn: paymentRequest
            )
            let paymentRequestSessionModel = ToggleSettingsSessionModel(
                footer: .paymentRequestDescription,
                rows: [paymentRequestRow]
            )
            newSections.append(paymentRequestSessionModel)
        }

        permissionsHelper.updateContactsState()
        let contactsPermissionRow = ToggleSettingsCellModel(
            type: .contactsPermission,
            label: .contactsPermission,
            detailLabel: permissionsHelper.contactsStateText,
            detailLabelColor: permissionsHelper.contactsStateColor,
            isOn: false
        )
        let contactsPermissionSessionModel = ToggleSettingsSessionModel(
            footer: .contactsPermissionDescription,
            rows: [contactsPermissionRow]
        )
        newSections.append(contactsPermissionSessionModel)

        permissionsHelper.updateLocationState()
        let locationPermissionRow = ToggleSettingsCellModel(
            type: .locationPermission,
            label: .locationPermission,
            detailLabel: permissionsHelper.locationStateText,
            detailLabelColor: permissionsHelper.locationStateColor,
            isOn: false
        )
        let locationPermissionSessionModel = ToggleSettingsSessionModel(
            footer: .locationPermissionDescription,
            rows: [locationPermissionRow]
        )
        newSections.append(locationPermissionSessionModel)

        self.sections = newSections
    }
    
    // MARK: - Settings Update
    func updateSetting(
        at index: IndexPath,
        isOn: Bool,
        settingName: String,
        settingKey: ToggleSettingsKey,
        onSuccess: @escaping () -> Void,
        onError: @escaping (Error) -> Void
    ) {
        self.settingName = settingName
        self.settingKey = settingKey
        self.selectedIndex = index

        var swicthIsOn = isOn
        
        if settingKey == .paymentInPrivate || settingKey == .closedAccount || settingKey == .receiveInPrivate {
            swicthIsOn = !isOn
        }
        
        // Special case for the privatePay cell
        if settingKey == .paymentInPrivate {
            trackEventPayInPrivateChanged(switchIsOn: swicthIsOn)
            ConsumerManager.shared.privacyConfig = swicthIsOn ? FeedItemVisibilityInt.private.rawValue : FeedItemVisibilityInt.friends.rawValue
            
            updateCellInfoState(at: index, isLoading: false, isOn: isOn)
        }
                
        if !isOn {
            if (settingKey == .paymentInPrivate || settingKey == .receiveInPrivate) {
                loadPopupConfirmation()
                return
            }
            
            if settingKey == .closedAccount {
                loadProfilePopupConfirmation()
                return
            }
        }
        
        api.setPrivacySettings(configs: [settingName: swicthIsOn]) { [weak self] result in
            switch result {
            case .success:
                self?.callUpdateSettingAnalytics(settingKey: settingKey, isOn: swicthIsOn)
                self?.updateCellInfoState(at: index, isLoading: false, isOn: isOn)
                onSuccess()
            case .failure(let error):
                self?.updateCellInfoState(at: index, isLoading: false, isOn: !isOn)
                onError(error)
            }
        }
    }
    
    private func loadProfilePopupConfirmation() {
        trackDialogViewed()
        
        delegate?.showProfileConfirmationPopup({
            self.saveConfigs([self.settingName: true])
        }, {
            guard let index = self.selectedIndex else { return }
            self.updateCellInfoState(at: index, isLoading: false, isOn: true)
            self.delegate?.reloadTableViewCell(at: index)
        })
    }
    
    private func saveConfigs(_ configs: [String: Bool]) {
        api.setPrivacySettings(configs: configs) { [weak self] result in
            guard let index = self?.selectedIndex, let key = self?.settingKey else { return }
            switch result {
            case .success:
                self?.callUpdateSettingAnalytics(settingKey: key, isOn: true)
                self?.updateCellInfoState(at: index, isLoading: false, isOn: false)
            case .failure(let error):
                self?.updateCellInfoState(at: index, isLoading: false, isOn: true)
                self?.delegate?.showAlert(withMessage: error.localizedDescription)
            }
            
            self?.delegate?.reloadTableViewCell(at: index)
        }
    }
    
    private func loadPopupConfirmation() {
        let options = [
            PopupSettingsOptionData(.hideFromNow, Localizable.updatePrivacyPopupSecondOptionTitle),
            PopupSettingsOptionData(.hidePastActivities, Localizable.updatePrivacyPopupFirstOptionTitle)
        ]
        
        let model = PrivacySettingsPopupViewModel(title: Localizable.updatePrivacyPopupTitle,
                                                  primaryButtonTitle: Localizable.updatePrivacyPopupButtonTitle,
                                                  options: options,
                                                  primaryButtonAction: privacySettingsPopupCallback)
        trackDialogViewed()
        delegate?.showConfirmationPopup(with: model)
    }
    
    private func trackDialogViewed() {
        guard let key = settingKey else { return }
        
        let consumerId = dependencies.consumerManager.consumer?.wsId.toString()
        let origin = DialogOrigin.reviewPrivacy
        
        switch key {
        case .paymentInPrivate:
            dependencies.analytics.log(UpdatePrivacyEvent.paymentDialogViewed(consumerId, origin))
        case .receiveInPrivate:
            dependencies.analytics.log(UpdatePrivacyEvent.receiveDialogViewed(consumerId, origin))
        case .closedAccount:
            dependencies.analytics.log(UpdatePrivacyEvent.accountDialogViewed(consumerId, origin))
        default:
            return
        }
    }
    
    private func privacySettingsPopupCallback(data: PopupSettingsOption) {
        let value = data == .hidePastActivities
        let hidePropertyName: String = settingName == "payment_in_private" ? "hide_past_payments" : "hide_past_receives"
        
        let configs = [settingName: true, hidePropertyName: value]

        saveConfigs(configs)
    }
    
    private func trackEventPayInPrivateChanged(switchIsOn: Bool) {
        let properties: [String: Any] = [
            "Privacidade": (switchIsOn ? "Privado" : "Público"),
            "Origem": "Ajustes",
            "Alterou padrão": true
        ]
        PPAnalytics.trackEvent("Privacidade Alterada", properties: properties)
    }
    
    // MARK: - TableView Build
    func updateCellInfoState(at index: IndexPath, isLoading: Bool, isOn: Bool) {
        guard
            sections.indices.contains(index.section),
            sections[index.section].rows.indices.contains(index.row)
            else {
                return
        }
        sections[index.section].rows[index.row].isLoading = isLoading
        sections[index.section].rows[index.row].isOn = isOn
    }
    
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func numberOfRowsAtSection(_ section: Int) -> Int {
        return sections.indices.contains(section) ? sections[section].rows.count : 0
    }

    func cellModelFor(section: Int, row: Int) -> ToggleSettingsCellModel? {
        guard
            sections.indices.contains(section),
            sections[section].rows.indices.contains(row)
            else {
                return nil
        }
        return sections[section].rows[row]
    }
    
    func titleForHeaderAtSection(_ section: Int) -> String {
        return sections.indices.contains(section) ? sections[section].header : ""
    }
    
    func titleForFooterInSection(_ section: Int) -> String {
        return sections.indices.contains(section) ? sections[section].footer : ""
    }
    
    private func callUpdateSettingAnalytics(settingKey: ToggleSettingsKey, isOn: Bool) {
        guard case .paymentRequest = settingKey else {
            return
        }
        dependencies.analytics.log(PaymentRequestUserEvent.privacyChange(origin: .adjustments, isOn: isOn))
    }
}
// MARK: - ToggleSettingsCellDelegate & DisclosureSettingsCellDelegate
extension PrivacySettingsViewModel {
    func switchDidChange(to isOn: Bool, indexPath: IndexPath, settingKey: ToggleSettingsKey) {
        guard let settingName = settingKey.rawValue.snakeCased() else {
            return
        }
        
        updateCellInfoState(at: indexPath, isLoading: true, isOn: isOn)
        delegate?.reloadTableViewCell(at: indexPath)
        
        updateSetting(at: indexPath, isOn: isOn, settingName: settingName, settingKey: settingKey, onSuccess: { [weak self] in
            self?.delegate?.reloadTableViewCell(at: indexPath)
            }, onError: { [weak self] error in
                self?.delegate?.showAlert(withMessage: error.localizedDescription)
                self?.delegate?.reloadTableViewCell(at: indexPath)
        })
    }

    func didTapChevronButton(settingKey: ToggleSettingsKey, indexPath: IndexPath) {
        switch settingKey {
        case .contactsPermission:
            guard permissionsHelper.shouldShowContactSettings else {
                permissionsHelper.askForContactsPermission { [weak self] in
                    self?.reloadCell(at: indexPath)
                }
                return
            }

        case .locationPermission:
            guard permissionsHelper.shouldShowLocationSettings else {
                permissionsHelper.askForLocationWhenInUsePermission { [weak self] in
                    self?.reloadCell(at: indexPath)
                }
                return
            }

        default:
            return
        }

        openSettings()
    }

    private func reloadCell(at indexPath: IndexPath) {
        if let settings = privacySettings {
            configureTableView(settings)
        }
        delegate?.reloadTableViewCell(at: indexPath)
    }

    private func openSettings() {
        guard
            let url = URL(string: UIApplication.openSettingsURLString),
            application.canOpenURL(url)
            else {
                return
        }
        application.open(url)
    }
}
