import CoreLegacy
import UI
import UIKit

final class PromoCodeViewController: PPSlidableViewController {
    private typealias Attrs = (key: NSAttributedString.Key, value: Any)
    private let viewModel = RegistrationViewModel()
    private let origin: PromoCodeAnalytics.Origin
    private var profileInviterViewController: ProfilePromoCodeViewController?
    
    @IBOutlet weak private var promoCodeTextField: UIPPFloatingTextField!
    @IBOutlet weak private var activateCodeButton: UIPPButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var regulationButton: UIButton!
    
    init(from origin: PromoCodeAnalytics.Origin) {
        self.origin = origin
        super.init(nibName: PromoCodeViewController.nibName, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        promoCodeTextField.becomeFirstResponder()        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        PromoCodeAnalytics.send(.actionInUse(origin))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Immediately close the keyboard when presenting this controller from the wallet screen
        if navigationController?.viewControllers.first == self {
            view.endEditing(true)
        }
    }
    
    @IBAction private func didTapActivateCodeButton(_ sender: Any) {
        if let code = promoCodeTextField.text, !code.isEmpty {
            validatePromoCode(code)
            PromoCodeAnalytics.send(.actionActivate(code))
        }
    }
    
    @IBAction private func didTapTermsButton(_ sender: Any) {
        let url = WebServiceInterface.apiEndpoint(WsWebViewURL.mgmTerms.endpoint)
        ViewsManager.pushWebViewController(withUrl: url, fromViewController: self, title: SettingsLocalizable.termsAndConditions.text)
    }

    private func showInvalidPromoCode(_ error: Error) {
        AlertMessage.showCustomAlertWithError(error, controller: self, completion: { [weak self] in
            self?.promoCodeTextField.becomeFirstResponder()
            })
    }
    
    private func validatePromoCode(_ code: String) {
        activateCodeButton.startLoadingAnimating()
        view.endEditing(true)
        
        PromoCodeWorker.validate(code) { [weak self] result in
            DispatchQueue.main.async {
                self?.activateCodeButton.stopLoadingAnimating()
                self?.promoCodeTextField.text = ""
                
                switch result {
                case .success(let promoCode):
                    AppParameters.global().referalCodeBannerDismissDate = nil
                    self?.showPromoCodeInfo(promoCode.data)
                case .failure(let error):
                    PromoCodeAnalytics.send(.actionError(error.description))
                    self?.showInvalidPromoCode(error)
                }
            }
        }
    }
    
    private func startStudentAccount() {
        StudentAccountCoordinator(from: self).start(from: .settings)
    }
    
    private func showPromoCodeInfo(_ promoData: PPPromoCode.PromoCodeData) {
        switch promoData {
        case .inviterReward(let code):
            showActivatedPromoCode(rewardPromoCode: code)
        case .couponWeb(let cupom):
            showCouponWebview(coupon: cupom)
        case .studentAccount:
            startStudentAccount()
        case .unknown:
            AlertMessage.showAlert(withMessage: SettingsLocalizable.codeUsedWithSuccess.text, controller: self)
        }
    }
    
    private func showActivatedPromoCode(rewardPromoCode: PPRegisterRewardPromoCode) {
        if let url = rewardPromoCode.webviewUrl {
            present(url: url)
            return
        }
        
        if let contact = rewardPromoCode.inviter {
            let popup = ProfilePromoCodeFactory.make(contact: contact, wsId: ConsumerManager.shared.consumer?.wsId)
            popup.modalPresentationStyle = .overCurrentContext
            #if swift(>=5.1)
            if #available(iOS 13.0, *) {
                popup.isModalInPresentation = true
            }
            #endif
            present(popup, animated: true)
        }
    }
    
    private func showCouponWebview(coupon: CouponWebPromoCodeData) {
        present(url: coupon.url)
    }

    private func present(url: String) {
        guard isModal else {
            ViewsManager.presentWebViewController(withUrl: url, fromViewController: self)
            navigationController?.popViewController(animated: true)
            return
        }
        let presentingViewController = self.presentingViewController
        dismiss(animated: true) {
            guard let presentingViewController = presentingViewController else {  return }
            ViewsManager.presentWebViewController(withUrl: url, fromViewController: presentingViewController)
        }
    }
    
    // MARK: View Setup
    
    private func setupView() {
        setupColors()
        setRegulationButtonLabel()
    }
    
    private func setupColors() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        descriptionLabel.textColor = Palette.ppColorGrayscale500.color
        promoCodeTextField.textColor = Palette.ppColorGrayscale500.color
    }
    
    private func setRegulationButtonLabel() {
        let regulationButtonLabel = SettingsLocalizable.regulation.text
        
        let attributes = getRegulationButtonLabelAttrs(regulationButtonLabel)
        let regulationAttrString = NSAttributedString(string: regulationButtonLabel, attributes: attributes)
        regulationButton.setAttributedTitle(regulationAttrString, for: .normal)
    }
    
    private func getRegulationButtonLabelAttrs(_ regulationButtonLabel: String) -> [NSAttributedString.Key: Any] {
        let color: Attrs = (.foregroundColor, Palette.ppColorGrayscale500.color)
        let font: Attrs = (.font, UIFont(name: "Helvetica Neue", size: 13.0) ?? UIFont.systemFont(ofSize: 13.0))
        let underline: Attrs = (.underlineStyle, NSUnderlineStyle.single.rawValue)
        
        return [color.key: color.value, font.key: font.value, underline.key: underline.value]
    }
}
