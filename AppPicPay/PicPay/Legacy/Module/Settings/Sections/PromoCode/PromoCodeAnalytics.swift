final class PromoCodeAnalytics {
    enum Origin: String {
        case deeplink = "Deeplink"
        case home = "Início"
        case settings = "Ajustes"
        case signUp = "Cadastro"
        case wallet = "Carteira"
    }
    
    enum Analytics {
        case actionActivate(_ code: String)
        case actionError(_ error: String)
        case actionInUse(_ origin: Origin)
        
        var name: String {
            switch self {
            case .actionActivate:
                return "Inseriu código promocional"
            case .actionError:
                return "Inseriu código promocional - erro"
            case .actionInUse:
                return "Tocou em código promocional"
            }
        }
        
        var properties: [String: String] {
            switch self {
            case .actionActivate(let code):
                return ["Código": "\(code)"]
            case .actionError(let error):
                return ["Motivo": "\(error)"]
            case .actionInUse(let origin):
                return ["Canal": "\(origin.rawValue)"]
            }
        }
    }
    
    static func send(_ event: Analytics) {        
        PPAnalytics.trackEvent(event.name, properties: event.properties)
    }
}
