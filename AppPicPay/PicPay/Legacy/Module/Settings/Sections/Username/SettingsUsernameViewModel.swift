import Foundation

final class SettingsUsernameViewModel {
    private let consumerWorker: ConsumerWorkerProtocol
    private let worker: SettingsUsernameWorker
    
    var username: String {
        return consumerWorker.consumer?.username ?? ""
    }
    
    init(consumerWorker: ConsumerWorkerProtocol = ConsumerWorker(), worker: SettingsUsernameWorker = ServiceSettingsUsername()) {
        self.consumerWorker = consumerWorker
        self.worker = worker
    }
    
    func sanitizedUsername(_ name: String) -> String {
        return name.replacingOccurrences(of: "[^a-z0-9_.]", with: "", options: .regularExpression)
    }
    
    func saveUsername(username: String, onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        worker.updateUsername(username: username) { [weak self] error in
            if let erro = error {
                onError(erro)
            } else {
                self?.consumerWorker.consumer?.username = username
                onSuccess()
            }
        }
    }
}
