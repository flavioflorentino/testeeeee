import Foundation

protocol SettingsUsernameWorker {
    func updateUsername(username: String, completion: @escaping (Error?) -> Void)
}

final class ServiceSettingsUsername: SettingsUsernameWorker {
    func updateUsername(username: String, completion: @escaping (Error?) -> Void) {
        WSConsumer.createUsername(username) { error in
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
}
