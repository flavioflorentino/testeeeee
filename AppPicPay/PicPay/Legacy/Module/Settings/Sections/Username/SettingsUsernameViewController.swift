import UI
import UIKit

protocol SettingsUsernameViewControllerDelegate: AnyObject {
    func usernameUpdateDidFinish()
}

final class SettingsUsernameViewController: UIViewController {
    private let viewModel: SettingsUsernameViewModel
    
    weak var delegate: SettingsUsernameViewControllerDelegate?
    
    @IBOutlet weak private var usernameTextField: UITextField!
    @IBOutlet weak private var button: UIPPButton!
    @IBOutlet weak private var checkImage: UIImageView!
    
    init(with viewModel: SettingsUsernameViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "SettingsUsernameViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    @IBAction private func save(_ sender: Any) {
        usernameTextField.resignFirstResponder()
        confirmAction()
    }
    
    private func setupView() {
        title = SettingsLocalizable.yourPicPay.text
        usernameTextField.text = viewModel.username
        usernameTextField.placeholder = viewModel.username
        checkImage.isHidden = true
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    private func confirmAction() {
        let change = Button(title: SettingsLocalizable.changeUsername.text, type: .cta) { [weak self] popupController, _ in
            self?.saveUsername()
            popupController.dismiss(animated: true)
        }
        let cancel = Button(title: DefaultLocalizable.btCancel.text, type: .clean, action: .close)
        
        let alert = Alert(with: SettingsLocalizable.changeUsername.text, text: SettingsLocalizable.doYouReallyWantChangeUsername.text, buttons: [change, cancel])
        AlertMessage.showAlert(alert, controller: nil)
    }
    
    private func saveUsername() {
        guard let username = usernameTextField.text else {
            return
        }
        button.startLoadingAnimating()
        viewModel.saveUsername(username: username, onSuccess: { [weak self] in
            self?.button.stopLoadingAnimating()
            self?.finishWithSuccesAnimation()
            }, onError: { [weak self] erro in
            guard let strongSelf = self else {
            return
        }
            strongSelf.button.stopLoadingAnimating()
            AlertMessage.showCustomAlertWithError(erro, controller: strongSelf)
        })
    }
    
    private func finishWithSuccesAnimation() {
        checkImage.alpha = 0
        checkImage.isHidden = false
        
        UIView.animate(withDuration: 0.5, animations: { [weak self] in
            self?.checkImage.alpha = 1
        }, completion: { [weak self] complete in
            if complete {
                self?.delegate?.usernameUpdateDidFinish()
            }
        })
    }
}

extension SettingsUsernameViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let username = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) else {
    return false
}
        let filter = viewModel.sanitizedUsername(username)
        
        return username == filter
    }
}
