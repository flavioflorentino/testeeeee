import Foundation

final class MailFactory: NSObject {
    @objc
    static func make() -> UIViewController {
        let service: MailServicing = MailService(ConsumerManager.shared)
        let viewModel: MailViewModelType = MailViewModel(service: service)
        var coordinator: MailCoordinating = MailCoordinator()
        let viewController = MailViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
