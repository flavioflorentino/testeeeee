import Foundation

protocol MailViewModelInputs {
    func viewDidLoad()
    func newEmailTyped(_ newEmail: String)
    func willChangeEmail(_ newEmail: String)
    func saveNewEmail(_ email: String)
    func resendEmail()
}

protocol MailViewModelOutputs: AnyObject {
    func currentEmail(_ email: String)
    func changeViewToEmailVerified()
    func changeViewToEmailUnverified()
    func changeViewToEmailChanged()
    func changeViewToEmailPending()
    func changeSaveButtonStatus(_ enable: Bool)
    func presentErrorAlert(_ message: String)
    func presentAlert(_ title: String, message: String)
    func startLoadingView()
    func stopLoadingView()
    func askForChangeConfirmation(from previousEmail: String, to newEmail: String)
}

protocol MailViewModelType: AnyObject {
    var inputs: MailViewModelInputs { get }
    var outputs: MailViewModelOutputs? { get set }
}

final class MailViewModel: MailViewModelType, MailViewModelInputs {
    typealias AlertDetails = (title: String, message: String)
    
    private enum ControlViewStatus {
        case verified, changed, pending, unverified
    }

    private enum SimpleAlertMessage {
        case emailAuth, verifyEmail, resendEmail
    }

    private let service: MailServicing
    private let emailCheckManager = EmailStatusCheckManager.shared
    private var currentEmail: String?
    private var emailVerified: Bool
    private var pendingVerification: Bool
    private var currentStatus: ControlViewStatus?
    
    var inputs: MailViewModelInputs { return self }
    weak var outputs: MailViewModelOutputs?
    
    init(service: MailServicing) {
        self.service = service
        self.emailVerified = false
        self.pendingVerification = false
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func viewDidLoad() {
        let selector = #selector(MailViewModel.mailStatusDidUpdate)
        let notificationName = Notification.Name(EmailStatusCheckManager.Observables.EmailStatusUpdated.rawValue)
        NotificationCenter.default.addObserver(self, selector: selector, name: notificationName, object: nil)
        emailCheckManager.updateNow()
        updateEmail()
    }
    
    func newEmailTyped(_ newEmail: String) {
        guard newEmail != currentEmail else {
            updateViewStatus()
            return
        }
        
        updateView(with: .changed)
        outputs?.changeSaveButtonStatus(isValidEmail(newEmail))
    }
    
    func saveNewEmail(_ email: String) {
        service.updateUsersEmail(email) { [weak self] result in
            switch result {
            case .failure(.userCancel):
                self?.outputs?.stopLoadingView()
            case .failure(let error):
                self?.outputs?.presentErrorAlert(error.localizedDescription)
            case .success:
                self?.emailUpdated(email)
            }
        }
    }
    
    private func emailUpdated(_ email: String) {
        guard emailVerified else {
            showSimpleAlert(for: .verifyEmail, with: email)
            return
        }
        
        showSimpleAlert(for: .emailAuth, with: currentEmail ?? "")
    }
    
    func resendEmail() {
        service.resendConfirmationEmail { [weak self] result in
            switch result {
            case .failure(let error):
                self?.outputs?.presentErrorAlert(error.localizedDescription)
            case .success:
                self?.showSimpleAlert(for: .resendEmail, with: self?.currentEmail ?? "")
            }
        }
    }
    
    func willChangeEmail(_ newEmail: String) {
        outputs?.askForChangeConfirmation(from: currentEmail ?? "", to: newEmail)
    }
}

extension MailViewModel {
    private func updateEmail() {
        getEmailStatus()
        
        if let email = currentEmail {
            outputs?.currentEmail(email)
            updateViewStatus()
        }
    }
    
    private func getEmailStatus() {
        currentEmail = service.getConsumerEmail()
        emailVerified = service.getConsumerEmailVerified()
        pendingVerification = service.getConsumerPendingVerification()
    }
    
    private func updateViewStatus() {
        guard emailVerified else {
            updateView(with: .unverified)
            return
        }
        
        guard pendingVerification else {
            updateView(with: .verified)
            return
        }
        
        updateView(with: .pending)
    }
    
    private func updateView(with status: ControlViewStatus) {
        guard status != currentStatus else {
            return
        }
        
        currentStatus = status
        
        switch status {
        case .pending:
            outputs?.changeViewToEmailPending()
        case .verified:
            outputs?.changeViewToEmailVerified()
        case .unverified:
            outputs?.changeViewToEmailUnverified()
        case .changed:
            outputs?.changeViewToEmailChanged()
        }
    }
    
    private func showSimpleAlert(for message: SimpleAlertMessage, with email: String) {
        var alertDetails = AlertDetails(title: "", message: "")
        
        switch message {
        case .emailAuth:
            alertDetails.title = MailLocalizable.receiveAuthorizationMessage.text
            alertDetails.message = String(format: MailLocalizable.continueAuthorization.text, email)
        case .verifyEmail:
            alertDetails.title = MailLocalizable.receiveVerificationMail.text
            alertDetails.message = String(format: MailLocalizable.accessSentLink.text, email)
        case .resendEmail:
            alertDetails.title = MailLocalizable.newVerificationMail.text
            alertDetails.message = String(format: MailLocalizable.accessSentLink.text, email)
        }
        
        outputs?.presentAlert(alertDetails.title, message: alertDetails.message)
    }
    
    private func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegEx).evaluate(with: email)
    }
    
    @objc
    private func mailStatusDidUpdate(notification: NSNotification) {
        currentEmail = service.getConsumerEmail()
        updateEmail()
    }
}
