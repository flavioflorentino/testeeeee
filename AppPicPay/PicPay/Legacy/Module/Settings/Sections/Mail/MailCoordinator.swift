import UIKit

enum MailAction {
}

protocol MailCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: MailAction)
}

final class MailCoordinator: MailCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: MailAction) {
    }
}
