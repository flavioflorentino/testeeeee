import Foundation

enum MailError: Error {
    case userCancel, generic(_: Error)
}

protocol MailServicing {
    typealias CompletionRequestSuccess = (Result<Void, MailError>) -> Void
    func updateUsersEmail(_ email: String, completion: @escaping CompletionRequestSuccess)
    func resendConfirmationEmail(_ completion: @escaping CompletionRequestSuccess)
    func getConsumerEmail() -> String
    func getConsumerEmailVerified() -> Bool
    func getConsumerPendingVerification() -> Bool
}

final class MailService: MailServicing {
    private var ppauth: PPAuth?
    private let consumerManager: ConsumerManagerContract
    
    init(_ consumer: ConsumerManagerContract) {
        consumerManager = consumer
    }
    
    func updateUsersEmail(_ email: String, completion: @escaping CompletionRequestSuccess) {
        ppauth = PPAuth.authenticate({ [weak self] pin, _ in
            self?.updateEmail(email, pin: pin, completion: completion)
        }, canceledByUserBlock: {
            completion(.failure(.userCancel))
        })
    }
    
    private func updateEmail(_ email: String, pin: String?, completion: @escaping CompletionRequestSuccess) {
        WSConsumer.updateEmail(email, pin: pin) { [weak self] success, error in
            DispatchQueue.main.async {
                if let error = error {
                    self?.handlePPAuthError(error, completion: completion)
                    return
                }
                
                completion(.success(()))
            }
        }
    }
    
    private func handlePPAuthError(_ error: Error, completion: @escaping CompletionRequestSuccess) {
        ppauth?.handleError(error, callback: { error in
            guard let error = error else {
                return
            }
            completion(.failure(.generic(error)))
        })
    }
    
    func resendConfirmationEmail(_ completion: @escaping CompletionRequestSuccess) {
        WSConsumer.resendEmailConfirmation { success, error in
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(.generic(error)))
                    return
                }
                completion(.success(()))
            }
        }
    }
    
    func getConsumerEmail() -> String {
        return consumerManager.consumer?.email ?? ""
    }
    
    func getConsumerEmailVerified() -> Bool {
        return consumerManager.consumer?.emailConfirmed ?? false
    }
    
    func getConsumerPendingVerification() -> Bool {
        return consumerManager.consumer?.emailPending ?? false
    }
}
