import SecurityModule
import UI
import UIKit

final class MailViewController: LegacyViewController<MailViewModelType, MailCoordinating, UIView>, Obfuscable {
    private enum Layout {
        static let textBig = UIFont.systemFont(ofSize: 16)
        static let textBold = UIFont.systemFont(ofSize: 14, weight: .bold)
        static let textSmall = UIFont.systemFont(ofSize: 13)
        static let buttonSize: CGFloat = 44.0
    }
    
    private enum ViewLoading {
        case start, stop
    }
    
    private lazy var headerLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = Layout.textSmall
        label.text = MailLocalizable.headerMessage.text
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var emailTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.font = Layout.textBig
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.selectedLineColor = Palette.ppColorBranding300.color
        textField.placeholderColor = Palette.ppColorGrayscale400.color
        textField.selectedTitleColor = Palette.ppColorBranding300.color
        textField.errorColor = Palette.ppColorNegative300.color
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        textField.spellCheckingType = .no
        textField.keyboardType = .emailAddress
        textField.returnKeyType = .done
        textField.addTarget(self, action: #selector(emailChanged), for: .editingChanged)
        return textField
    }()
    
    private lazy var emailVerifiedLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale400.color
        label.font = Layout.textSmall
        label.text = MailLocalizable.emailVerificated.text
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var pendingMessageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = Layout.textSmall
        label.text = MailLocalizable.pendingMessage.text
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var saveButton: UIPPButton = {
        let saveButton = UIPPButton()
        let button = Button(title: MailLocalizable.saveButtonTitle.text, type: .cta)
        saveButton.translatesAutoresizingMaskIntoConstraints = false
        saveButton.configure(with: button, font: Layout.textBold)
        saveButton.disabledBorderColor = Palette.ppColorBranding300.color
        saveButton.disabledBackgrounColor = Palette.ppColorBranding300.color
        saveButton.cornerRadius = Layout.buttonSize / 2
        saveButton.isEnabled = true
        saveButton.addTarget(self, action: #selector(didTapSaveButton), for: .touchUpInside)
        return saveButton
    }()
    
    private lazy var controlView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(emailVerifiedLabel)
        view.addSubview(pendingMessageLabel)
        view.addSubview(saveButton)
        return view
    }()
    
    private lazy var warningLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorNegative200.color
        label.font = Layout.textSmall
        label.text = MailLocalizable.warningMessage.text
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var resendConfirmationButton: UIPPButton = {
        let resendButton = UIPPButton()
        let button = Button(title: MailLocalizable.resendButtonTitle.text, type: .ghost)
        resendButton.translatesAutoresizingMaskIntoConstraints = false
        resendButton.configure(with: button, font: Layout.textBold)
        resendButton.cornerRadius = Layout.buttonSize / 2
        resendButton.isEnabled = true
        resendButton.addTarget(self, action: #selector(didTapResendEmailButton), for: .touchUpInside)
        return resendButton
    }()
    
    private lazy var resendEmailView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(warningLabel)
        view.addSubview(resendConfirmationButton)
        return view
    }()
    
    //Screen obfuscation
    var appSwitcherObfuscator: AppSwitcherObfuscating?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buildViewHierarchy()
        configureViews()
        setupConstraints()
        viewModel.inputs.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deinitObfuscator()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObfuscationConfiguration()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(headerLabel)
        view.addSubview(emailTextField)
        view.addSubview(controlView)
        view.addSubview(resendEmailView)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        title = MailLocalizable.title.text
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: view,
                                           for: [headerLabel, emailTextField, controlView, resendEmailView],
                                           constant: 16)
        
        NSLayoutConstraint.leadingTrailing(equalTo: controlView,
                                           for: [emailVerifiedLabel, pendingMessageLabel, saveButton])
        
        NSLayoutConstraint.leadingTrailing(equalTo: resendEmailView,
                                           for: [warningLabel, resendConfirmationButton])
        
        NSLayoutConstraint.activate([
            headerLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            headerLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 26)
        ])
        
        NSLayoutConstraint.activate([
            emailTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            emailTextField.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: 25)
        ])
        
        NSLayoutConstraint.activate([
            controlView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            emailVerifiedLabel.topAnchor.constraint(equalTo: controlView.topAnchor, constant: 8)
        ])
        
        NSLayoutConstraint.activate([
            pendingMessageLabel.topAnchor.constraint(equalTo: emailVerifiedLabel.topAnchor, constant: 13),
            pendingMessageLabel.bottomAnchor.constraint(equalTo: controlView.bottomAnchor, constant: 20)
        ])
        
        NSLayoutConstraint.activate([
            saveButton.topAnchor.constraint(equalTo: controlView.topAnchor, constant: 28),
            saveButton.heightAnchor.constraint(equalToConstant: Layout.buttonSize),
            saveButton.bottomAnchor.constraint(equalTo: controlView.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            resendEmailView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            warningLabel.topAnchor.constraint(equalTo: resendEmailView.topAnchor, constant: 8)
        ])
        
        NSLayoutConstraint.activate([
            resendConfirmationButton.topAnchor.constraint(equalTo: warningLabel.bottomAnchor, constant: 23),
            resendConfirmationButton.bottomAnchor.constraint(equalTo: resendEmailView.bottomAnchor),
            resendConfirmationButton.heightAnchor.constraint(equalToConstant: Layout.buttonSize)
        ])
    }
    
    @objc
    private func didTapSaveButton() {
        guard let email = emailTextField.text else {
            return
        }
        viewModel.inputs.willChangeEmail(email)
    }
    
    @objc
    private func didTapResendEmailButton() {
        animateView(.start)
        viewModel.inputs.resendEmail()
    }
    
    @objc
    private func emailChanged() {
        viewModel.inputs.newEmailTyped(emailTextField.text ?? "")
    }
    
    private func animateView(_ loading: ViewLoading) {
        switch loading {
        case .start:
            saveButton.startLoadingAnimating()
            emailTextField.isEnabled = false
        case .stop:
            saveButton.stopLoadingAnimating()
            emailTextField.isEnabled = true
        }
    }
}

// MARK: View Model Outputs
extension MailViewController: MailViewModelOutputs {
    func currentEmail(_ email: String) {
        emailTextField.text = email
    }
    
    func changeViewToEmailVerified() {
        controlView.alpha = 0
        controlView.isHidden = false
        emailVerifiedLabel.isHidden = false
        pendingMessageLabel.isHidden = true
        saveButton.isHidden = true
        resendEmailView.isHidden = true
        emailTextField.errorMessage = ""
        UIView.animate(withDuration: 0.25, animations: { [weak self] in
            self?.controlView.alpha = 1
        })
    }
    
    func changeViewToEmailUnverified() {
        resendEmailView.alpha = 0
        controlView.isHidden = true
        resendEmailView.isHidden = false
        emailTextField.errorMessage = MailLocalizable.textFieldTitle.text
        UIView.animate(withDuration: 0.25, animations: { [weak self] in
            self?.resendEmailView.alpha = 1
        })
    }
    
    func changeViewToEmailChanged() {
        controlView.isHidden = false
        emailVerifiedLabel.isHidden = true
        pendingMessageLabel.isHidden = true
        saveButton.isHidden = false
        resendEmailView.isHidden = true
        emailTextField.errorMessage = ""
    }
    
    func changeViewToEmailPending() {
        controlView.alpha = 0
        controlView.isHidden = false
        emailVerifiedLabel.isHidden = false
        pendingMessageLabel.isHidden = false
        resendEmailView.isHidden = true
        saveButton.isHidden = true
        emailTextField.errorMessage = ""
        UIView.animate(withDuration: 0.25, animations: { [weak self] in
            self?.controlView.alpha = 1
        })
    }
    
    func changeSaveButtonStatus(_ enable: Bool) {
        saveButton.isEnabled = enable
        saveButton.alpha = enable ? 1.0 : 0.5
    }
    
    func presentErrorAlert(_ message: String) {
        animateView(.stop)
        
        AlertMessage.showAlert(withMessage: message, controller: self)
    }
    
    func presentAlert(_ title: String, message: String) {
        animateView(.stop)
        AlertMessage.showCustomAlert(title: title, message: message, controller: self)
    }
    
    func startLoadingView() {
        animateView(.start)
    }
    
    func stopLoadingView() {
        animateView(.stop)
    }
    
    func askForChangeConfirmation(from previousEmail: String, to newEmail: String) {
        let title = MailLocalizable.changeEmailTitle.text
        let description = MailLocalizable.changeEmailDescription.text
        let message = String(format: description, previousEmail, newEmail)
        
        AlertMessage.showCustomAlert(title: title, message: message, controller: self) { [weak self] in
            self?.animateView(.start)
            self?.viewModel.inputs.saveNewEmail(newEmail)
        }
    }
}
