import Foundation

enum MailLocalizable: String, Localizable {
    case title
    case headerMessage
    case saveButtonTitle
    case resendButtonTitle
    case emailVerificated
    case warningMessage
    case pendingMessage
    case receiveAuthorizationMessage
    case continueAuthorization
    case receiveVerificationMail
    case accessSentLink
    case newVerificationMail
    case textFieldTitle
    case changeEmailTitle
    case changeEmailDescription
    case confirmChange
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .mail
    }
}
