import Foundation

final class FeesAndLimits: Codable {
    let accountPersonal: Fees?
    let accountPro: Fees?
    let accountLimits: Limits?
    
    private enum CodingKeys: String, CodingKey {
        case accountPersonal = "taxes_personal"
        case accountPro = "taxes_pro"
        case accountLimits = "limits"
    }
}

final class Fees: Codable {
    let title: String
    let header: FeeAdvice?
    let rows: [Row]
    let footer: FeeAdvice?
    
    private enum CodingKeys: String, CodingKey {
        case title
        case header
        case rows
        case footer
    }
}

final class Limits: Codable {
    let header: FeeAdvice?
    let limitsTitle: LimitsText
    let limitAccount: AccountLimit?
    let footer: FeeAdvice?
    
    private enum CodingKeys: String, CodingKey {
        case header
        case limitsTitle = "text"
        case limitAccount = "payment_account_limit"
        case footer
    }
}

final class FeeAdvice: Codable {
    let description: String
    let highlight: String?
    
    private enum CodingKeys: String, CodingKey {
        case description
        case highlight = "description_highlight"
    }
}

final class Row: Codable {
    let title: String
    let description: String
    
    private enum CodingKeys: String, CodingKey {
        case title
        case description
    }
}

extension Limits {
    final class LimitsText: Codable {
        let balance: String
        let deposit: String
        let progress: String
        
        private enum CodingKeys: String, CodingKey {
            case balance = "title_balance"
            case deposit = "title_deposit"
            case progress = "progress"
        }
    }
    
    final class AccountLimit: Codable {
        let hasLimit: Bool
        let balance: AccounPayment
        let deposit: AccounPayment
        
        private enum CodingKeys: String, CodingKey {
            case hasLimit = "has_limit"
            case balance
            case deposit
        }
    }
    
    final class AccounPayment: Codable {
        let available: Double
        let provisioned: Double
        let total: Double
        
        private enum CodingKeys: String, CodingKey {
            case available
            case provisioned
            case total
        }
    }
}
