import UI
import UIKit

protocol FeesAndLimitsViewControllerDelegate: AnyObject {
    func didTapHeaderPro()
    func didTapFooterLimit()
    func didTapHelp()
    func close()
}

final class FeesAndLimitsViewController: PPBaseViewController {
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var feesAndLimitsTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    private lazy var helpButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: #imageLiteral(resourceName: "green_help_button"), style: .plain, target: self, action: #selector(openHelpCenter))
        return button
    }()
    
    private let viewModel: FeesAndLimitsViewModel
    weak var delegate: FeesAndLimitsViewControllerDelegate?
    
    init(with: FeesAndLimitsViewModel) {
        viewModel = with
        super.init(nibName: "FeesAndLimitsViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startLoadingView()
        setupColors()
        
        viewModel.loadFeesAndLimits(onSuccess: { [weak self] in
            self?.stopLoadingView()
            self?.setupSection()
            self?.setupHelp()
        }, onError: { [weak self] (error) in
            self?.stopLoadingView()
            self?.showError(error: error)
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale000.color
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale100.color
    }
    
    private func showError(error: Error) {
        AlertMessage.showCustomAlertWithError(error, controller: self) { [weak self] in
            self?.delegate?.close()
        }
    }
    
    private func setupColors() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        feesAndLimitsTitleLabel.textColor = Palette.ppColorGrayscale600.color
        descriptionLabel.textColor = Palette.ppColorGrayscale500.color
    }
    
    private func setupHelp() {
        if viewModel.showSection(section: .limit) {
            navigationItem.rightBarButtonItem = helpButton
        }
    }
    
    private func setupSection() {
        let isPro = viewModel.userIsPro()
        if viewModel.showSection(section: .personal) {
             createSection(type: .personal, isSelected: !isPro)
        }
        
        if viewModel.showSection(section: .pro) {
            createSection(type: .pro, isSelected: isPro)
        }
        
        if viewModel.showSection(section: .limit) {
            createSection(type: .limit, isSelected: false)
        }
    }
    
    private func createSection(type: FeesAndLimitsViewModel.Section, isSelected: Bool) {
        let title = viewModel.title(section: type)
        
        let section = FeesAndLimitsSectionView(frame: .zero)
        section.setupTitle(title: title, isSelected: isSelected)
        
        addHeader(to: section, ofType: type)
        addFooter(to: section, ofType: type)
        addRows(to: section, ofType: type)
       
        stackView.addArrangedSubview(section)
    }
    
    private func addRows(to section: FeesAndLimitsSectionView, ofType: FeesAndLimitsViewModel.Section) {
        let number = viewModel.numberOfRows(section: ofType)
        for i in 0..<number {
            let row = viewModel.row(section: ofType, row: i)
            
            if viewModel.rowIsSelectable(section: ofType, row: i) {
                let view = createLimitsView()
                section.addRowsSelectable(row: row, view: view)
            } else {
                section.addRows(row: row)
            }
        }
    }
    
    private func addHeader(to section: FeesAndLimitsSectionView, ofType: FeesAndLimitsViewModel.Section) {
        if !viewModel.showHeader(section: ofType) { return }
        
        switch ofType {
        case .personal, .limit:
            let text = viewModel.headerTitle(section: ofType)
            let header = FeeAndLimitDetailsView(frame: .zero, text: text, type: .normal)
            section.setupHeader(view: header)
        case .pro:
            let view = createProHeader()
            section.setupHeader(view: view)
        }
    }
    
    private func addFooter(to section: FeesAndLimitsSectionView, ofType: FeesAndLimitsViewModel.Section) {
        if !viewModel.showFooter(section: ofType) { return }
        
        switch ofType {
        case .personal, .pro:
            let text = viewModel.footerTitle(section: ofType)
            let footer = FeeAndLimitDetailsView(frame: .zero, text: text, type: .description)
            section.setupFooter(view: footer)
        case .limit:
            let view = createLimitFooter()
            section.setupFooter(view: view)
        }
    }
    
    private func createLimitsView() -> LimitsView {
        let text = viewModel.limitDepositText()
        let highlight = viewModel.limitDepositHighlight()
        let limitView = LimitsView(frame: .zero, text: text, highlight: highlight)
        limitView.progressValue = viewModel.progressDeposit() ?? 0
        
        return limitView
    }
    
    private func createProHeader() -> FeeAndLimitDetailsView {
        let text = viewModel.headerTitle(section: .pro)
        let highlight = viewModel.headerHighlight(section: .pro)
        let header = FeeAndLimitDetailsView(frame: .zero, text: text, highlight: highlight, type: .title)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapHeaderPro(_:)))
        header.addGestureRecognizer(tapGesture)
        
        return header
    }
    
    private func createLimitFooter() -> FeeAndLimitDetailsButtonView {
        let text = viewModel.footerTitle(section: .limit)
        let highlight = viewModel.footerHighlight(section: .limit)
        let footer = FeeAndLimitDetailsButtonView(frame: .zero, text: text, highlight: highlight)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapFooterLimit(_:)))
        footer.addGestureRecognizer(tapGesture)
        
        return footer
    }
    
    @objc
    private func handleTapHeaderPro(_ sender: UITapGestureRecognizer) {
        delegate?.didTapHeaderPro()
    }
    
    @objc
    private func handleTapFooterLimit(_ sender: UITapGestureRecognizer) {
        PPAnalytics.trackEvent(UpgradeChecklistEvent.checklistTitle, properties: [UpgradeChecklistEvent.checklistAttribute: UpgradeChecklistEvent.feesAndLimits])
        delegate?.didTapFooterLimit()
    }
    
    @objc
    private func openHelpCenter() {
        delegate?.didTapHelp()
    }
}
