import UIKit

final class FeesAndLimitsViewModel {
    private var model: FeesAndLimits?
    private let worker: FeesAndLimitsWorker
    
    init(api: FeesAndLimitsWorker = FeesAndLimitsService()) {
        self.worker = api
    }
    
    func showSection(section: Section) -> Bool {
        switch section {
        case .personal:
            return model?.accountPersonal != nil && worker.showSectionPersonal()
        case .pro:
            return model?.accountPro != nil && worker.showSectionPro()
        case .limit:
            return  model?.accountLimits != nil && worker.showSectionLimits()
        }
    }
    
    func showHeader(section: Section) -> Bool {
        switch section {
        case .personal:
            return model?.accountPersonal?.header != nil
        case .pro:
            return userIsPro() ? false : model?.accountPro?.header != nil
        case .limit:
            return model?.accountLimits?.header != nil
        }
    }
    
    func showFooter(section: Section) -> Bool {
        switch section {
        case .personal:
            return model?.accountPersonal?.footer != nil
        case .pro:
            return model?.accountPro?.footer != nil
        case .limit:
            return model?.accountLimits?.footer != nil
        }
    }
    
    func title(section: Section) -> String {
        switch section {
        case .personal:
            return model?.accountPersonal?.title ?? ""
        case .pro:
            return model?.accountPro?.title ?? ""
        case .limit:
            return SettingsLocalizable.accountLimitsTitle.text
        }
    }
    
    func headerTitle(section: Section) -> String {
        switch section {
        case .personal:
            return model?.accountPersonal?.header?.description ?? ""
        case .pro:
            return model?.accountPro?.header?.description ?? ""
        case .limit:
            return model?.accountLimits?.header?.description ?? ""
        }
    }
    
    func footerTitle(section: Section) -> String {
        switch section {
        case .personal:
            return model?.accountPersonal?.footer?.description ?? ""
        case .pro:
            return model?.accountPro?.footer?.description ?? ""
        case .limit:
            let text = model?.accountLimits?.footer?.description ?? ""
            let value = model?.accountLimits?.limitAccount?.balance.total.getFormattedPrice() ?? ""
            return text.replacingOccurrences(of: "%s", with: value)
        }
    }
    
    func headerHighlight(section: Section) -> String {
        switch section {
        case .personal:
            return model?.accountPersonal?.header?.highlight ?? ""
        case .pro:
            return model?.accountPro?.header?.highlight ?? ""
        case .limit:
            return model?.accountLimits?.header?.highlight ?? ""
        }
    }
    
    func footerHighlight(section: Section) -> String {
        switch section {
        case .personal:
            return model?.accountPersonal?.footer?.highlight ?? ""
        case .pro:
            return model?.accountPro?.footer?.highlight ?? ""
        case .limit:
            return model?.accountLimits?.footer?.highlight ?? ""
        }
    }
    
    func row(section: Section, row: Int) -> (String, String) {
        switch section {
        case .personal:
            let left = model?.accountPersonal?.rows[row].title ?? ""
            let right = model?.accountPersonal?.rows[row].description ?? ""
            return (left, right)
            
        case .pro:
            let left = model?.accountPro?.rows[row].title ?? ""
            let right = model?.accountPro?.rows[row].description ?? ""
            return (left, right)
          
        case .limit:
            return rowLimit(row: row)
        }
    }
    
    private func rowLimit(row: Int) -> (String, String) {
        guard let accountLimits = model?.accountLimits, let limitAccount = accountLimits.limitAccount else {
            return ("", "")
        }
        let rightValue = (row == 0) ? limitAccount.balance : limitAccount.deposit
        let rightText = rightValue.total.getFormattedPrice()
        let leftText = (row == 0) ? accountLimits.limitsTitle.balance : accountLimits.limitsTitle.deposit
        
        return (leftText, rightText)
    }
    
    func rowIsSelectable(section: Section, row: Int) -> Bool {
        if section == .limit && row == 1 {
            return model?.accountLimits?.limitAccount?.hasLimit ?? false
        }
        
        return false
    }
    
    func progressDeposit() -> Float? {
        guard let deposit = model?.accountLimits?.limitAccount?.deposit else {
            return nil
        }
        return Float((deposit.total - deposit.available) / deposit.total)
    }
    
    func limitDepositText() -> String {
        guard let accountLimits = model?.accountLimits, let deposit = accountLimits.limitAccount?.deposit else {
            return ""
        }
        let text = accountLimits.limitsTitle.progress
        let value = deposit.total - deposit.available
        return text.replacingOccurrences(of: "%s", with: value.getFormattedPrice())
    }
    
    func limitDepositHighlight() -> String {
        guard let deposit = model?.accountLimits?.limitAccount?.deposit else {
            return ""
        }
        let value = deposit.total - deposit.available
        return value.getFormattedPrice()
    }
    
    func userIsPro() -> Bool {
        return worker.userIsPro()
    }
    
    func numberOfRows(section: Section) -> Int {
        switch section {
        case .personal:
            return model?.accountPersonal?.rows.count ?? 0
        case .pro:
            return model?.accountPro?.rows.count ?? 0
        case .limit:
            return model?.accountLimits != nil ? 2 : 0
        }
    }
    
    func loadFeesAndLimits(onSuccess: @escaping () -> Void, onError: @escaping (PicPayErrorDisplayable) -> Void) {
        worker.getFeesAndLimits { [weak self] (result) in
            switch result {
            case .success(let detail):
                self?.model = detail
                onSuccess()
            case .failure(let error):
                onError(error)
            }
        }
    }
}

extension FeesAndLimitsViewModel {
    enum Section {
        case personal
        case pro
        case limit
    }
}
