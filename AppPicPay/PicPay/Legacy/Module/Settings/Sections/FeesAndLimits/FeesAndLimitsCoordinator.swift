import Foundation

final class FeesAndLimitsCoordinator: Coordinator {
    var currentCoordinator: Coordinator?
    private let navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let viewModel = FeesAndLimitsViewModel()
        let controller = FeesAndLimitsViewController(with: viewModel)
        controller.delegate = self
        
        controller.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(controller, animated: true)
    }
}

extension FeesAndLimitsCoordinator: FeesAndLimitsViewControllerDelegate {
    func didTapHeaderPro() {
        WebViewPro.openWebView(type: .pro, navigationController: navigationController)
    }
    
    func didTapFooterLimit() {
        let coordinador = UpgradeCoordinator(navigationController: navigationController)
        coordinador.start()
        currentCoordinator = coordinador
    }
    
    func didTapHelp() {
        let url = UpgradeChecklistDeeplink.fees
        DeeplinkHelper.handleDeeplink(withUrl: URL(string: url), from: navigationController)
    }
    
    func close() {
        navigationController.popViewController(animated: true)
    }
}
