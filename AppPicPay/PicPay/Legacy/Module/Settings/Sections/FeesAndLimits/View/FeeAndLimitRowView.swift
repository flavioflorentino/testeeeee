import UI
import UIKit

final class FeeAndLimitRowView: UIView {
    private lazy var moreStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = 0.0
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var buttonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = 0.0
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.distribution = .fillEqually
        stackView.axis = .horizontal
        stackView.spacing = 16.0
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var leftLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14.0, weight: .light)
        label.textColor = Palette.ppColorGrayscale600.color
        label.numberOfLines = 5
        return label
    }()
    
    private lazy var rightLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14.0, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 5
        return label
    }()
    
    private lazy var button: UIButton = {
        let button = UIButton()
        button.isUserInteractionEnabled = false
        button.setTitle(SettingsLocalizable.seeDetails.text, for: .normal)
        button.setTitleColor(Palette.ppColorNeutral300.color, for: .normal)
        button.setImage(#imageLiteral(resourceName: "ic_chevron_blue_down"), for: .normal)
        button.semanticContentAttribute = .forceRightToLeft
        button.imageEdgeInsets = UIEdgeInsets(top: 4, left: 8, bottom: 0, right: 8)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12.0, weight: .bold)
        button.contentHorizontalAlignment = .left
        
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale100.color
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var moreView: UIView?
    private var isSelectable = false
    
    convenience init(frame: CGRect, leftLabel: String, rightLabel: String, moreView: UIView? = nil) {
        self.init(frame: frame)
        
        self.isSelectable = moreView != nil
        self.moreView = moreView
        self.moreView?.isHidden = true
        self.leftLabel.text = leftLabel
        self.rightLabel.text = rightLabel
        
        addComponents()
        layoutComponents()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        if let view = moreView, isSelectable {
            buttonStackView.addArrangedSubview(rightLabel)
            buttonStackView.addArrangedSubview(button)
            
            stackView.addArrangedSubview(leftLabel)
            stackView.addArrangedSubview(buttonStackView)
            
            moreStackView.addArrangedSubview(stackView)
            moreStackView.addArrangedSubview(view)
            
            addGesture()
        } else {
            stackView.addArrangedSubview(leftLabel)
            stackView.addArrangedSubview(rightLabel)
            moreStackView.addArrangedSubview(stackView)
        }
        
        addSubview(moreStackView)
        addSubview(lineView)
    }
    
    private func layoutComponents() {
        stackView.layoutMargins = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        
        NSLayoutConstraint.activate([
            lineView.bottomAnchor.constraint(equalTo: bottomAnchor),
            lineView.leadingAnchor.constraint(equalTo: leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: trailingAnchor),
            lineView.heightAnchor.constraint(equalToConstant: 1)
        ])
        
        NSLayoutConstraint.activate([
            moreStackView.topAnchor.constraint(equalTo: topAnchor),
            moreStackView.bottomAnchor.constraint(equalTo: lineView.topAnchor),
            moreStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            moreStackView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
    private func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapRow))
        addGestureRecognizer(tap)
    }
    
    @objc
    private func tapRow() {
        UIView.animate(withDuration: 0.3) { [weak self] in
            let status = self?.moreView?.isHidden ?? true
            let image = status ? #imageLiteral(resourceName: "ic_chevron_blue_up") : #imageLiteral(resourceName: "ic_chevron_blue_down")
            self?.button.setImage(image, for: .normal)
            self?.moreView?.alpha = status == true ? 1 : 0
            self?.moreView?.isHidden = !status
            self?.layoutIfNeeded()
        }
    }
}
