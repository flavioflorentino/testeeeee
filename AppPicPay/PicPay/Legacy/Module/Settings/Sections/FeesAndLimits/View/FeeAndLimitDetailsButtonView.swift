import UIKit

final class FeeAndLimitDetailsButtonView: UIView {
    private lazy var button: UIPPButton = {
        let optionsButton = Button(title: SettingsLocalizable.buttonTitleUpgrade.text, type: .ghost)
        let button = UIPPButton()
        button.isUserInteractionEnabled = false
        button.cornerRadius = 20
        button.configure(with: optionsButton)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private var detailView: FeeAndLimitDetailsView?
    
    init(frame: CGRect, text: String, highlight: String = "") {
        super.init(frame: frame)
        
        detailView = FeeAndLimitDetailsView(frame: .zero, text: text, highlight: highlight, type: .title)
        detailView?.translatesAutoresizingMaskIntoConstraints = false
        
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        guard let detailView = detailView else {
            return
        }
        addSubview(detailView)
        addSubview(button)
    }
    
    private func layoutComponents() {
        guard let detailView = detailView else {
            return
        }
        NSLayoutConstraint.activate([
            detailView.topAnchor.constraint(equalTo: topAnchor),
            detailView.leadingAnchor.constraint(equalTo: leadingAnchor),
            detailView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: detailView.bottomAnchor, constant: 20),
            button.bottomAnchor.constraint(equalTo: bottomAnchor),
            button.leadingAnchor.constraint(equalTo: leadingAnchor),
            button.heightAnchor.constraint(equalToConstant: 40),
            button.widthAnchor.constraint(equalToConstant: 150)
        ])
    }
}
