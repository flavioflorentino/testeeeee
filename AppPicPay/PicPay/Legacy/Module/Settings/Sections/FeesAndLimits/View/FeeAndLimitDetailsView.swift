import UI
import UIKit

final class FeeAndLimitDetailsView: UIView {
    private var titleAttributed: [NSAttributedString.Key: Any]  = {
        let font = UIFont.systemFont(ofSize: 14.0, weight: .bold)
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: Palette.ppColorGrayscale600.color]
        
        return attributes
    }()
    
    private var highlightAttributed: [NSAttributedString.Key: Any] = {
        let font = UIFont.systemFont(ofSize: 14.0, weight: .bold)
        let color = Palette.ppColorBranding300.color
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: color]
        
        return attributes
    }()
    
    private var descAttributed: [NSAttributedString.Key: Any]  = {
        let font = UIFont.systemFont(ofSize: 12.0, weight: .light)
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: Palette.ppColorGrayscale600.color]
        
        return attributes
    }()
    
    private var normalAttributed: [NSAttributedString.Key: Any]  = {
        let font = UIFont.systemFont(ofSize: 16, weight: .light)
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: Palette.ppColorGrayscale600.color]
        
        return attributes
    }()
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 20
        
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    convenience init(frame: CGRect, text: String, highlight: String = "", type: TypeDetails) {
        self.init(frame: frame)
        var attributedTex: NSMutableAttributedString
        switch type {
        case .description:
            attributedTex = NSMutableAttributedString(string: text, attributes: descAttributed)
        case .title:
            let range = NSString(string: text).range(of: highlight)
            attributedTex = NSMutableAttributedString(string: text, attributes: titleAttributed)
            attributedTex.addAttributes(highlightAttributed, range: range)
        case .normal:
            attributedTex = NSMutableAttributedString(string: text, attributes: normalAttributed)
        }
        
        self.textLabel.attributedText = attributedTex
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale000.color
        
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        addSubview(textLabel)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            textLabel.topAnchor.constraint(equalTo: topAnchor),
            textLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            textLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            textLabel.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
}

extension FeeAndLimitDetailsView {
    enum TypeDetails {
        case normal
        case title
        case description
    }
}
