import UI
import UIKit

final class LimitsView: UIView {
    private var titleAttributed: [NSAttributedString.Key: Any]  = {
        let font = UIFont.systemFont(ofSize: 14.0, weight: .semibold)
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: Palette.ppColorGrayscale600.color]
        
        return attributes
    }()
    
    private var highlightAttributed: [NSAttributedString.Key: Any] = {
        let font = UIFont.systemFont(ofSize: 14.0, weight: .semibold)
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: Palette.ppColorNeutral300.color]
        
        return attributes
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 4
        
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var progressBar: UIProgressView = {
        let progressBar = UIProgressView()
        progressBar.progressTintColor = Palette.ppColorNeutral300.color
        progressBar.trackTintColor = Palette.ppColorGrayscale300.color
        progressBar.applyRadiusCorners(radius: 4)
        progressBar.translatesAutoresizingMaskIntoConstraints = false
        return progressBar
    }()
    
    var progressValue: Float = 0 {
        didSet {
            progressBar.progress = progressValue
        }
    }
    
    convenience init(frame: CGRect, text: String, highlight: String = "") {
        self.init(frame: frame)
        
        let range = NSString(string: text).range(of: highlight)
        let attributedTex = NSMutableAttributedString(string: text, attributes: titleAttributed)
        attributedTex.addAttributes(highlightAttributed, range: range)
        
        self.titleLabel.attributedText = attributedTex
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale100.color
        applyRadiusCorners(radius: 4)
        
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        addSubview(titleLabel)
        addSubview(progressBar)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            titleLabel.bottomAnchor.constraint(equalTo: progressBar.topAnchor, constant: -20),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20)
        ])
        
        let constrainBottom =  progressBar.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -25)
        constrainBottom.priority = UILayoutPriority(rawValue: 255)
        
        NSLayoutConstraint.activate([
            progressBar.heightAnchor.constraint(equalToConstant: 8),
            constrainBottom,
            progressBar.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            progressBar.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor)
        ])
    }
}
