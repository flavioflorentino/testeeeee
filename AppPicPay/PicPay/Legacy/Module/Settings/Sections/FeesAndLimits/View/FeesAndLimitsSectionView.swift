import UI
import UIKit

final class FeesAndLimitsSectionView: UIView {
    private var headerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private var footerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private var titleAttributed: [NSAttributedString.Key: Any]  = {
        let font = UIFont.systemFont(ofSize: 20.0, weight: .bold)
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: Palette.ppColorGrayscale600.color]
        
        return attributes
    }()
    
    private var selectAttributed: [NSAttributedString.Key: Any] = {
        let font = UIFont.systemFont(ofSize: 20.0, weight: .regular)
        let color = Palette.ppColorBranding300.color
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: color]
        
        return attributes
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.axis = .vertical
        
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale000.color
        
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        addSubview(headerView)
        addSubview(titleLabel)
        addSubview(stackView)
        addSubview(footerView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: topAnchor),
            headerView.bottomAnchor.constraint(equalTo: titleLabel.topAnchor),
            headerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            headerView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            footerView.topAnchor.constraint(equalTo: stackView.bottomAnchor),
            footerView.bottomAnchor.constraint(equalTo: bottomAnchor),
            footerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            footerView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
    func setupTitle(title: String, isSelected: Bool = false) {
        let attributedTex = NSMutableAttributedString(string: title, attributes: titleAttributed)
        
        if isSelected {
            attributedTex.append(NSAttributedString(string: SettingsLocalizable.yourAccount.text, attributes: selectAttributed))
        }
        self.titleLabel.attributedText = attributedTex
    }
    
    func addRows(row: (String, String)) {
        let rowView = FeeAndLimitRowView(frame: .zero, leftLabel: row.0, rightLabel: row.1)
        stackView.addArrangedSubview(rowView)
    }
    
    func addRowsSelectable(row: (String, String), view: UIView) {
        let rowView = FeeAndLimitRowView(frame: .zero, leftLabel: row.0, rightLabel: row.1, moreView: view)
        stackView.addArrangedSubview(rowView)
    }
    
    func setupHeader(view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        let constrainBottom = view.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -25)
        constrainBottom.priority = UILayoutPriority(rawValue: 255)
        
        headerView.addSubview(view)
        
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: headerView.topAnchor),
            constrainBottom,
            view.leadingAnchor.constraint(equalTo: headerView.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: headerView.trailingAnchor)
        ])
        
        layoutIfNeeded()
    }
    
    func setupFooter(view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        let constrainTop = view.topAnchor.constraint(equalTo: footerView.topAnchor, constant: 25)
        constrainTop.priority = UILayoutPriority(rawValue: 260)
        
        footerView.addSubview(view)
        
        NSLayoutConstraint.activate([
            constrainTop,
            view.bottomAnchor.constraint(equalTo: footerView.bottomAnchor),
            view.leadingAnchor.constraint(equalTo: footerView.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: footerView.trailingAnchor)
        ])
        
        layoutIfNeeded()
    }
}
