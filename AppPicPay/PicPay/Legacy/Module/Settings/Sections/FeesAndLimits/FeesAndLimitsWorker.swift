import Foundation
import FeatureFlag

protocol FeesAndLimitsWorker {
    func getFeesAndLimits(completion: @escaping (PicPayResult<FeesAndLimits>) -> Void)
    func userIsPro() -> Bool
    func showSectionPro() -> Bool
    func showSectionLimits() -> Bool
    func showSectionPersonal() -> Bool
}

final class FeesAndLimitsService: BaseApi, FeesAndLimitsWorker {
    var consumer: MBConsumer? {
        return ConsumerManager.shared.consumer
    }
    
    func getFeesAndLimits(completion: @escaping ((PicPayResult<FeesAndLimits>) -> Void)) {
        requestManager.apiRequest(endpoint: kWsUrlTaxAndLimits, method: .get).responseApiCodable(strategy: .useDefaultKeys, completionHandler: completion)
    }

    func userIsPro() -> Bool {
        return consumer?.businessAccount ?? false
    }
    
    func showSectionPersonal() -> Bool {
        FeatureManager.isActive(.featureSectionPersonal)
    }
    
    func showSectionPro() -> Bool {
        FeatureManager.isActive(.featureSectionPro)
    }
    
    func showSectionLimits() -> Bool {
        FeatureManager.isActive(.featureSectionLimits)
    }
}
