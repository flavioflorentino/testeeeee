import Foundation
import Validator

struct PersonalDetailsTextfieldSettings {
    let name: TextfieldSetting
    let document: TextfieldSetting
    let birthdate: TextfieldSetting
    let mothersName: TextfieldSetting
}

struct TextfieldSetting {
    let placeholder: String
    let text: String?
    let rule: ValidationRuleSet<String>?
    let mask: String?
}
