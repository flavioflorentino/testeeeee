import Foundation
import Validator

enum PersonalDetailsLocalizable: String, Error, CustomStringConvertible, Localizable, ValidationError {
    case title
    case ButtonSaveTitle
    case TextfieldPlaceholderName
    case TextfieldPlaceholderDocument
    case TextfieldPlaceholderBirthdate
    case TextfieldPlaceholderMothersName

    case ErrorNameRequired = "Nome obrigatório"
    case ErrorBirthdateRequired = "Data de Nascimento obrigatória"
    case ErrorBirthdateInvalid = "Data de Nascimento inválida"
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .personalDetails
    }
    
    var description: String {
        return self.rawValue
    }
    
    var message: String {
        return self.rawValue
    }
}
