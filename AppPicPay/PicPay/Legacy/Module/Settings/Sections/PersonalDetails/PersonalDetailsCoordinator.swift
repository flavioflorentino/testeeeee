import UIKit

enum PersonalDetailsAction {
}

protocol PersonalDetailsCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: PersonalDetailsAction)
}

final class PersonalDetailsCoordinator: PersonalDetailsCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: PersonalDetailsAction) {
    }
}
