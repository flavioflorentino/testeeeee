import Foundation

final class PersonalDetailsFactory: NSObject {
    @objc
    static func make() -> UIViewController {
        let consumer = ConsumerManager.shared.consumer
        let service: PersonalDetailsServicing = PersonalDetailsService()
        let viewModel: PersonalDetailsViewModelType = PersonalDetailsViewModel(service: service, consumer: consumer)
        var coordinator: PersonalDetailsCoordinating = PersonalDetailsCoordinator()
        let viewController = PersonalDetailsViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
