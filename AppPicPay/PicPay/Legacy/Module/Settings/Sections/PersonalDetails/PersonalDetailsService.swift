import Foundation

protocol PersonalDetailsServicing {
    func updatePersonalData(model: PersonalDetail, completion: @escaping (Result<Bool, Error>) -> Void)
}

final class PersonalDetailsService: PersonalDetailsServicing {
    func updatePersonalData(model: PersonalDetail, completion: @escaping (Result<Bool, Error>) -> Void) {
        WSConsumer.updatePersonalData(model.name, cpf: model.document, birth_date: model.birthdate, name_mother: model.mothersName) { suc, error in
            
            if let err = error {
                completion(.failure(err))
            } else {
                completion(.success(suc))
            }
        }
    }
}
