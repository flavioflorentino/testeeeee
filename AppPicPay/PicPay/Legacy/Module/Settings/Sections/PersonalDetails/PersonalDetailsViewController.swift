import SecurityModule
import UI
import UIKit

final class PersonalDetailsViewController: LegacyViewController<PersonalDetailsViewModelType, PersonalDetailsCoordinating, UIView>, Obfuscable {
    struct Layout {
        static let topOffset: CGFloat = 20
        static let topOffsetButton: CGFloat = 32
        static let heightButton: CGFloat = 44
    }
    
    private lazy var textfieldName: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.addTarget(self, action: #selector(checkTextfieldName), for: .editingChanged)
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var textfieldDocument: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.keyboardType = .numberPad
        textfield.isEnabled = false
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var textfieldBirthdate: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.addTarget(self, action: #selector(checkTexfieldBirthdate), for: .editingChanged)
        textfield.keyboardType = .numberPad
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var textfieldMothersName: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.addTarget(self, action: #selector(checkTextfieldMotherName), for: .editingChanged)
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var buttonSave: UIPPButton = {
        let button = UIPPButton()
        button.addTarget(self, action: #selector(didTapButtonSave), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.normalBackgrounColor = Palette.ppColorBranding300.color
        button.normalTitleColor = Palette.ppColorGrayscale000.color
        button.setTitleColor(Palette.ppColorGrayscale000.color)
        button.disabledTitleColor = Palette.ppColorGrayscale000.color
        button.disabledBackgrounColor = Palette.ppColorBranding300.color.withAlphaComponent(0.5)
        button.setTitle(PersonalDetailsLocalizable.ButtonSaveTitle.text, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15.0)
        button.cornerRadius = Layout.heightButton / 2
        return button
    }()
    
    //Screen obfuscation
    var appSwitcherObfuscator: AppSwitcherObfuscating?
    
    override func buildViewHierarchy() {
        view.addSubview(textfieldName)
        view.addSubview(textfieldDocument)
        view.addSubview(textfieldBirthdate)
        view.addSubview(textfieldMothersName)
        view.addSubview(buttonSave)
    }
    
    override func configureViews() {
        title = PersonalDetailsLocalizable.title.text
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func setupConstraints() {
        let elements = [textfieldName, textfieldDocument, textfieldBirthdate, textfieldMothersName, buttonSave]
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: elements, constant: Layout.topOffset)
        
        NSLayoutConstraint.activate([
            textfieldName.topAnchor.constraint(equalTo: view.topAnchor, constant: Layout.topOffset),
            textfieldDocument.topAnchor.constraint(equalTo: textfieldName.bottomAnchor, constant: Layout.topOffset),
            textfieldBirthdate.topAnchor.constraint(equalTo: textfieldDocument.bottomAnchor, constant: Layout.topOffset),
            textfieldMothersName.topAnchor.constraint(equalTo: textfieldBirthdate.bottomAnchor, constant: Layout.topOffset),
            buttonSave.topAnchor.constraint(equalTo: textfieldMothersName.bottomAnchor, constant: Layout.topOffsetButton),
            buttonSave.heightAnchor.constraint(equalToConstant: Layout.heightButton)
        ])
    }
}

extension PersonalDetailsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.inputs.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObfuscationConfiguration()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deinitObfuscator()
    }
    
    @objc
    private func didTapButtonSave() {
        view?.endEditing(true)
        setLoadingState(loading: true)
        viewModel.inputs.actionSave(name: textfieldName, birthdate: textfieldBirthdate, mothersName: textfieldMothersName)
    }
    
    @objc
    func checkTextfieldName() {
        viewModel.inputs.checkTextfield(textfield: textfieldName)
    }
    
    @objc
    func checkTexfieldBirthdate() {
        viewModel.inputs.checkTextfield(textfield: textfieldBirthdate)
    }
    
    @objc
    func checkTextfieldMotherName() {
        viewModel.inputs.checkMothersName(text: textfieldMothersName.text)
    }
    
    private func setLoadingState(loading: Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.textfieldName.isEnabled = !loading
            self?.textfieldBirthdate.isEnabled = !loading
            self?.textfieldMothersName.isEnabled = !loading
            
            if loading {
                self?.buttonSave.startLoadingAnimating()
            } else {
                self?.buttonSave.stopLoadingAnimating()
            }
        }
    }
}

// MARK: View Model Outputs
extension PersonalDetailsViewController: PersonalDetailsViewModelOutputs {
    func setTextfields(settings: PersonalDetailsTextfieldSettings) {
        textfieldName.placeholder = settings.name.placeholder
        textfieldName.text = settings.name.text
        textfieldName.validationRules = settings.name.rule
        
        textfieldDocument.placeholder = settings.document.placeholder
        textfieldDocument.text = settings.document.text
        
        textfieldBirthdate.placeholder = settings.birthdate.placeholder
        textfieldBirthdate.text = settings.birthdate.text
        textfieldBirthdate.validationRules = settings.birthdate.rule
        textfieldBirthdate.maskString = settings.birthdate.mask
        
        textfieldMothersName.placeholder = settings.mothersName.placeholder
        textfieldMothersName.text = settings.mothersName.text
        
        viewModel.inputs.checkTextfieldsAndEnableSave(name: textfieldName, birthdate: textfieldBirthdate, mothersName: textfieldMothersName)
    }
    
    func updateTextfieldMothersName(text: String) {
        textfieldMothersName.text = text
    }
    
    func updateTextfieldErrorMessage(textfield: UIPPFloatingTextField, message: String) {
        textfield.errorMessage = message
        viewModel.inputs.checkTextfieldsAndEnableSave(name: textfieldName, birthdate: textfieldBirthdate, mothersName: textfieldMothersName)
    }
    
    func enableButtonSave(enable: Bool) {
        buttonSave.isEnabled = enable
    }
    
    func setSuccessState() {
        setLoadingState(loading: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }
    }
    
    func setErrorState(error: Error) {
        setLoadingState(loading: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            AlertMessage.showCustomAlertWithError(error, controller: self)
        }
    }
}

extension PersonalDetailsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldBirthdate {
            guard let ppTextField = textField as? MaskTextField, let textt = textField.text else {
                return true
            }
            
            let newText = (textt as NSString).replacingCharacters(in: range, with: string)
            
            // Mask
            if let maskString = ppTextField.maskString {
                let mask = CustomStringMask(mask: maskString)
                textField.text = mask.maskedText(from: newText)
                checkTexfieldBirthdate()
                return false
            }
            
            if let maskABlock = ppTextField.maskblock {
                textField.text = maskABlock(textField.text ?? "", newText)
                return false
            }
        }
        
        return true
    }
}
