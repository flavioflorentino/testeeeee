import Foundation

struct PersonalDetail {
    var name: String
    var document: String
    var birthdate: String
    var mothersName: String
}
