import Foundation
import Validator
import UI

protocol PersonalDetailsViewModelInputs {
    func viewDidLoad()
    func checkTextfieldsAndEnableSave(name: UIPPFloatingTextField, birthdate: UIPPFloatingTextField, mothersName: UIPPFloatingTextField)
    func actionSave(name: UIPPFloatingTextField, birthdate: UIPPFloatingTextField, mothersName: UIPPFloatingTextField)
    func checkMothersName(text: String?)
    func checkTextfield(textfield: UIPPFloatingTextField)
}

protocol PersonalDetailsViewModelOutputs: AnyObject {
    func setTextfields(settings: PersonalDetailsTextfieldSettings)
    func updateTextfieldMothersName(text: String)
    func updateTextfieldErrorMessage(textfield: UIPPFloatingTextField, message: String)
    func enableButtonSave(enable: Bool)
    func setSuccessState()
    func setErrorState(error: Error)
}

protocol PersonalDetailsViewModelType: AnyObject {
    var inputs: PersonalDetailsViewModelInputs { get }
    var outputs: PersonalDetailsViewModelOutputs? { get set }
}

final class PersonalDetailsViewModel: PersonalDetailsViewModelType, PersonalDetailsViewModelInputs {
    var inputs: PersonalDetailsViewModelInputs { return self }
    weak var outputs: PersonalDetailsViewModelOutputs?

    private let service: PersonalDetailsServicing
    private var model: PersonalDetail
    
    init(service: PersonalDetailsServicing, consumer: MBConsumer?) {
        self.service = service
        
        let name = consumer?.name ?? ""
        let document = consumer?.cpf ?? ""
        let birthdate = consumer?.birth_date ?? ""
        let mothersName = consumer?.name_mother ?? ""
        
        model = PersonalDetail(name: name, document: document, birthdate: birthdate, mothersName: mothersName)
    }

    func viewDidLoad() {
        buildTextfieldSettings()
    }
    
    func actionSave(name: UIPPFloatingTextField, birthdate: UIPPFloatingTextField, mothersName: UIPPFloatingTextField) {
        let modelUpdated = buildModelForUpdate(name: name, birthdate: birthdate, mothersName: mothersName)
        
        service.updatePersonalData(model: modelUpdated) { [weak self] result in
            switch result {
            case .failure(let error):
                self?.outputs?.setErrorState(error: error)
            case .success:
                self?.outputs?.setSuccessState()
            }
        }
    }
}

// MARK: Builders
extension PersonalDetailsViewModel {
    func buildModelForUpdate(name: UIPPFloatingTextField, birthdate: UIPPFloatingTextField, mothersName: UIPPFloatingTextField) -> PersonalDetail {        
        var modelForUpdate = model
        
        modelForUpdate.name = name.text ?? model.name
        modelForUpdate.birthdate = birthdate.text ?? model.birthdate
        modelForUpdate.mothersName = mothersName.text ?? model.mothersName
        
        return modelForUpdate
    }
    
    func buildTextfieldSettings() {
        let set = PersonalDetailsTextfieldSettings(name: buildTextfieldSettingName(),
                                                   document: buildTextfieldSettingDocument(),
                                                   birthdate: buildTextfieldSettingBirthdate(),
                                                   mothersName: buildTextfieldSettingMothersName())
        
        outputs?.setTextfields(settings: set)
    }
    
    func buildTextfieldSettingName() -> TextfieldSetting {
        var rule = ValidationRuleSet<String>()
        rule.add(rule: ValidationRuleLength(min: 1, error: PersonalDetailsLocalizable.ErrorNameRequired))
        
        let placeholder = PersonalDetailsLocalizable.TextfieldPlaceholderName.text
        
        return TextfieldSetting(placeholder: placeholder, text: model.name, rule: rule, mask: nil)
    }
    
    func buildTextfieldSettingDocument() -> TextfieldSetting {
        let placeholder = PersonalDetailsLocalizable.TextfieldPlaceholderDocument.text
        return TextfieldSetting(placeholder: placeholder, text: model.document, rule: nil, mask: nil)
    }
    
    func buildTextfieldSettingBirthdate() -> TextfieldSetting {
        var rule = ValidationRuleSet<String>()
        rule.add(rule: ValidationRuleLength(min: 1, error: PersonalDetailsLocalizable.ErrorBirthdateRequired))
        rule.add(rule: ValidationRuleLength(min: 10, error: PersonalDetailsLocalizable.ErrorBirthdateInvalid))
        
        let placeholder = PersonalDetailsLocalizable.TextfieldPlaceholderBirthdate.text
        
        return TextfieldSetting(placeholder: placeholder, text: model.birthdate, rule: rule, mask: "00/00/0000")
    }
    
    func buildTextfieldSettingMothersName() -> TextfieldSetting {
        let placeholder = PersonalDetailsLocalizable.TextfieldPlaceholderMothersName.text
        return TextfieldSetting(placeholder: placeholder, text: model.mothersName, rule: nil, mask: nil)
    }
}

// MARK: Checkers
extension PersonalDetailsViewModel {
    func checkMothersName(text: String?) {
        guard let unvalidText = text else {
            return
        }
        
        let validText = unvalidText.replacingOccurrences(of: "[^a-zA-Z\u{00C0}-\u{00FF} ]", with: "", options: .regularExpression)
        
        outputs?.updateTextfieldMothersName(text: validText)
    }
    
    func checkTextfield(textfield: UIPPFloatingTextField) {
        let result = textfield.validate()
        
        switch result {
        case .invalid(let error):
            guard let err = error.first else {
                return
            }
            
            let message = String(describing: err)
            outputs?.updateTextfieldErrorMessage(textfield: textfield, message: message)
        default:
            outputs?.updateTextfieldErrorMessage(textfield: textfield, message: "")
        }
    }
    
    func checkTextfieldsAndEnableSave(name: UIPPFloatingTextField, birthdate: UIPPFloatingTextField, mothersName: UIPPFloatingTextField) {
        let textfields = [name, birthdate, mothersName]
        let enable = textfields.allSatisfy { $0.validate().isValid }
        outputs?.enableButtonSave(enable: enable)
    }
}
