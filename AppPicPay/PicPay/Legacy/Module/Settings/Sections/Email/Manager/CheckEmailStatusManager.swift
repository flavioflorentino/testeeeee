//
//  CheckEmailStatusManager.swift
//  PicPay
//
//  Created by Vagner Orlandi on 19/10/17.
//
//

import Foundation

/**
 * Classe para checar o status do email do consumer nos casos de:
 *   - Toda vez que retornar ao Foreground
 *
 * Utilizado Singleton, mas com a diferença que a referencia da intancia é `weak`.
 * Singleton para evitar que multiplas instancias executem requisições desnecessárias
 * `weak` para apenas viver enquanto alguma classe necessitar do manager
 */

final class EmailStatusCheckManager:NSObject {
    
    internal enum Observables:String {
        case EmailStatusUpdated = "EmailStatusCheckManagerEmailStatusUpdated"
    }
    
    private var isLoading:Bool = false
    
    private static weak var weakInstance:EmailStatusCheckManager?
    
    static var shared:EmailStatusCheckManager {
        get {
            if let instance = weakInstance {
                return instance
            } else {
                let newInstance = EmailStatusCheckManager()
                EmailStatusCheckManager.weakInstance = newInstance
                return newInstance
            }
        }
    }
    
    private override init() {
        super.init()
        self.listenWhenAppReturnToForeground()
    }
    
    deinit {
        self.unregisterObserver()
    }
    
    private func listenWhenAppReturnToForeground() {
        NotificationCenter.default.addObserver(self, selector: #selector(appWillReturnToForegroundHandler), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    private func unregisterObserver() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func appWillReturnToForegroundHandler(notification:NSNotification) {
        self.checkEmailStatus()
    }
    
    private func checkEmailStatus() {
        //Evitar que seja realizada mais uma requisição quando já houver 
        //uma em andamento
        if self.isLoading {
            return
        }
        
        self.isLoading = true
        WSConsumer.checkEmailStatus { [weak self] (success, error) in
            if success {
                self?.isLoading = false
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Observables.EmailStatusUpdated.rawValue), object: nil, userInfo: nil)
                }
            }
        }
    }
    
    func updateNow() {
        self.checkEmailStatus()
    }
}
