//
//  ProDisableViewModel.swift
//  PicPay
//
//  ☝🏽☁️ Created by Luiz Henrique Guimarães on 30/01/18.
//

import UIKit

final class LeaveProViewModel: NSObject {
    
    func disablePro(_ completion: @escaping ((String?, Error?) -> Void)) {
        WSConsumer.disableBusinessAccount { (message, error) in
            DispatchQueue.main.async {
                completion(message, error)
            }
        }
    }
    
}
