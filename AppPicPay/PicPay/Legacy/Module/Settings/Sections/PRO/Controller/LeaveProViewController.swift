import UIKit
import AnalyticsModule
import FeatureFlag

final class LeaveProViewController: PPBaseViewController {
    var model: LeaveProViewModel!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var button: UIPPButton!
    
    // MARK: - Initializer
    
    init(model : LeaveProViewModel = LeaveProViewModel()) {
        self.model = model
        super.init(nibName: "LeaveProViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.shared.log(ProEvent.leaveViewed)
        setupViews()
    }
    
    // MARK: - Configure Views
    
    func setupViews(){
        title = SettingsLocalizable.stopBeingPicPayPro.text
        titleLabel.text = FeatureManager.text(.leavePicPayProTitle)
        textLabel.text = FeatureManager.text(.leavePicPayProText)
        button.setTitle(FeatureManager.text(.leavePicPayProCTA), for: .normal)
        button.setTitle("", for: .disabled)
    }
    
    // MARK: - User Actions
    
    @IBAction private func disablePro(_ sender: Any) {
        Analytics.shared.log(ProEvent.leaveSelected)
        guard (FeatureManager.text(.leavePicPayProAlertTitle) != "" || FeatureManager.text(.leavePicPayProAlertMessage) != "") else {
            
            self.performDisablePro()
            return
        }
        
        let alert = UIAlertController(title:FeatureManager.text(.leavePicPayProAlertTitle),
                                      message: FeatureManager.text(.leavePicPayProAlertMessage),
                                      preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: FeatureManager.text(.leavePicPayProAlertCTA), style: .destructive, handler: { [weak self] (_) in
            Analytics.shared.log(ProEvent.leaveConfirmationSelected)
            self?.performDisablePro()
        }))
        
        alert.addAction(UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    func performDisablePro() {
        button.isEnabled = false
        button.startLoadingAnimating()
        model.disablePro { [weak self] (msg, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.button.stopLoadingAnimating()
            
            if let message = msg, error == nil {
                ConsumerManager.shared.consumer?.businessAccount = false
                AlertMessage.showAlert(withMessage: message, controller: strongSelf, completion: {
                    self?.navigationController?.popViewController(animated: true)
                })
                return
            }
            
            if let error = error {
                AlertMessage.showAlertWithError(error, controller: strongSelf)
            }
        }
    }
}
