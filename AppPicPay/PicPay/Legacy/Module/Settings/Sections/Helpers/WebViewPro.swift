import UIKit

final class WebViewPro: NSObject {
    class func openWebView(type: Account, navigationController: UINavigationController?, completion: (() -> Void)? = nil) {
        let consumerIsPro = ConsumerManager.shared.consumer?.businessAccount ?? false
        
        let webViewType: WsWebViewURL = type == .pro ? .proAboutWebview : .bizAboutWebview
        guard let url = URL(string: webViewType.endpoint) else {
            return
        }

        let webViewController = WebViewFactory.make(with: url, includeHeaders: true, completion: { _ in
            DispatchQueue.global().async {
                WSConsumer.amIPro({ isPro, _ in
                    ConsumerManager.shared.consumer?.businessAccount = isPro
                    if isPro && !consumerIsPro {
                        PPAnalytics.trackEvent("Virou PRO", properties: nil)
                    }
                    completion?()
                })
            }
        })

        navigationController?.pushViewController(webViewController, animated: true)
    }
}

extension WebViewPro {
    enum Account {
        case pro
        case business
    }
}
