import Core

enum LoanSettingsEndpoint {
    case settingsSection
}

extension LoanSettingsEndpoint: ApiEndpointExposable {
    var path: String {
        "personal-credit/configs"
    }
}
