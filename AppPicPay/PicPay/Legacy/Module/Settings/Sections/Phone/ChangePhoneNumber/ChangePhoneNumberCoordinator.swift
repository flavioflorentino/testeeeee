enum ChangePhoneNumberAction {
    case verifyCode
}

protocol ChangePhoneNumberCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: ChangePhoneNumberAction)
}

final class ChangePhoneNumberCoordinator: ChangePhoneNumberCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: ChangePhoneNumberAction) {
        guard action == .verifyCode else {
            return
        }
        let verificationCodeController = PhoneVerificationCodeFactory.make()
        viewController?.navigationController?.pushViewController(verificationCodeController, animated: true)
    }
}
