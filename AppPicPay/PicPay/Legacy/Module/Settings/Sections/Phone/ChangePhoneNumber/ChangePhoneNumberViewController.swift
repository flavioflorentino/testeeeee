import AnalyticsModule
import SecurityModule
import UI
import UIKit

final class ChangePhoneNumberViewController: LegacyViewController<ChangePhoneNumberViewModelType, ChangePhoneNumberCoordinating, UIView>, LoadingViewProtocol, Obfuscable {
    struct Layout {
        static let topOffset: CGFloat = 20
        static let spaceBetweenSubviews: CGFloat = 20
        static let leadingTrailingMargin: CGFloat = 20
        static let trailingMargin: CGFloat = -20
        static let buttonHeight: CGFloat = 48
    }
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.text = PhoneLocalizable.changeNumberScreenMessage.text
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var dddTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = PhoneLocalizable.ddd.text
        textField.tag = 1
        textField.font = UIFont.systemFont(ofSize: 17)
        textField.lineColor = Palette.ppColorBranding400.color
        textField.selectedLineColor = Palette.ppColorBranding300.color
        textField.selectedTitleColor = Palette.ppColorGrayscale400.color
        textField.textColor = Palette.ppColorGrayscale600.color
        textField.errorColor = Palette.ppColorNegative200.color
        textField.keyboardType = .numberPad
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.delegate = self
        return textField
    }()
    
    private lazy var phoneNumberTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = PhoneLocalizable.phoneNumber.text
        textField.tag = 2
        textField.font = UIFont.systemFont(ofSize: 17)
        textField.lineColor = Palette.ppColorBranding400.color
        textField.selectedLineColor = Palette.ppColorBranding300.color
        textField.selectedTitleColor = Palette.ppColorGrayscale400.color
        textField.textColor = Palette.ppColorGrayscale600.color
        textField.errorColor = Palette.ppColorNegative200.color
        textField.keyboardType = .numberPad
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.delegate = self
        return textField
    }()
    
    private lazy var phoneNumberStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.addArrangedSubview(dddTextField)
        stackView.addArrangedSubview(phoneNumberTextField)
        stackView.axis = .horizontal
        stackView.spacing = 20.0
        stackView.isUserInteractionEnabled = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var sendSMSButton: UIPPButton = {
        let button = UIPPButton(title: PhoneLocalizable.sendSMS.text, target: self, action: #selector(tapSentSMSButton), type: .cta)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var alreadyHaveACodeButton: UIButton = {
        let button = UIButton()
        button.setTitle(PhoneLocalizable.alreadyHaveACode.text, for: .normal)
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        button.addTarget(self, action: #selector(tapAlreadyHaveACodeButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    var loadingView = LoadingView()
    
    //Screen obfuscation
    var appSwitcherObfuscator: AppSwitcherObfuscating?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObfuscationConfiguration()
        dddTextField.becomeFirstResponder()
        viewModel.inputs.updateAlreadyHaveACodeButtonState()
        viewModel.inputs.startTimerToUpdateSMSButtonState()
        Analytics.shared.log(PhoneNumberEvent.change)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.inputs.clearTimer()
        deinitObfuscator()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(messageLabel)
        view.addSubview(phoneNumberStackView)
        view.addSubview(sendSMSButton)
        view.addSubview(alreadyHaveACodeButton)
    }
    
    override func configureViews() {
        title = PhoneLocalizable.changeNumberScreenTitle.text
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }

    override func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [messageLabel, phoneNumberStackView, sendSMSButton], constant: Layout.leadingTrailingMargin)
        
        NSLayoutConstraint.activate([
            messageLabel.topAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.topAnchor, constant: Layout.topOffset),
            
            dddTextField.widthAnchor.constraint(equalToConstant: 80),
            
            phoneNumberStackView.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: Layout.spaceBetweenSubviews),
            
            sendSMSButton.topAnchor.constraint(equalTo: phoneNumberStackView.bottomAnchor, constant: Layout.spaceBetweenSubviews),
            sendSMSButton.heightAnchor.constraint(equalToConstant: Layout.buttonHeight),
            
            alreadyHaveACodeButton.topAnchor.constraint(equalTo: sendSMSButton.bottomAnchor, constant: Layout.spaceBetweenSubviews),
            alreadyHaveACodeButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            alreadyHaveACodeButton.widthAnchor.constraint(equalToConstant: 200.0),
            alreadyHaveACodeButton.heightAnchor.constraint(equalToConstant: 32.0)
        ])
    }
    
    @objc
    private func tapSentSMSButton() {
        viewModel.inputs.didTapSentSMSButton(withDDD: dddTextField.text, phoneNumber: phoneNumberTextField.text)
    }
    
    @objc
    private func tapAlreadyHaveACodeButton() {
        presentVerifyCodeStep()
    }
    
    private func sendSMS() {
        startLoadingView()
        viewModel.inputs.sendSMSVerification()
    }
}

// MARK: View Model Outputs
extension ChangePhoneNumberViewController: ChangePhoneNumberViewModelOutputs {
    func hideAlredyHaveACodeButton() {
        alreadyHaveACodeButton.isHidden = true
    }
    
    func updateSendSMSButton(isEnabled: Bool, title: String) {
        sendSMSButton.isEnabled = isEnabled
        let attributedString = NSAttributedString(string: title)
        sendSMSButton.setAttributedTitle(attributedString, for: isEnabled ? .normal : .disabled)
    }
    
    func updateErrorDDDField(errorMessage: String?) {
        dddTextField.errorMessage = errorMessage
    }
    
    func updateErrorPhoneNumberField(errorMessage: String?) {
        phoneNumberTextField.errorMessage = errorMessage
    }
    
    func presentNumberConfirmationAlert(ddd: String, phoneNumber: String) {
        let message = String(format: PhoneLocalizable.isNumberCorrect.text, ddd, phoneNumber)
        let alert = Alert(title: PhoneLocalizable.confirmNumber.text, text: message)
        
        let editButton = Button(title: DefaultLocalizable.btEdit.text, type: .clean) { [weak self] popupController, _ in
            popupController.dismiss(animated: true) {
                self?.dddTextField.becomeFirstResponder()
            }
        }
        
        let okButton = Button(title: DefaultLocalizable.btYes.text, type: .cta) { [weak self] popupController, _ in
            popupController.dismiss(animated: true) {
                self?.dddTextField.resignFirstResponder()
                self?.phoneNumberTextField.resignFirstResponder()
                self?.sendSMS()
            }
        }
        
        alert.buttons = [okButton, editButton]
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func presentSMSVerificationError(_ error: Error) {
        DispatchQueue.main.async { [weak self] in
            self?.stopLoadingView()
            AlertMessage.showAlert(error, controller: self)
            self?.dddTextField.text = ""
            self?.phoneNumberTextField.text = ""
            self?.dddTextField.becomeFirstResponder()
        }
    }
    
    func presentVerifyCodeStep() {
        DispatchQueue.main.async { [weak self] in
            self?.stopLoadingView()
            self?.coordinator.perform(action: .verifyCode)
        }
    }
}

extension ChangePhoneNumberViewController: UITextFieldDelegate {
    func textField( _ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == dddTextField {
            dddTextField.errorMessage = nil
            return formatDDDTextfield(charactersIn: range, replacementString: string)
        } else {
            phoneNumberTextField.errorMessage = nil
            return formatPhoneTextField(charactersIn: range, replacementString: string)
        }
    }
    
    private func formatDDDTextfield(charactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldValue = dddTextField.text as NSString?
        let newDDD = textFieldValue?.replacingCharacters(in: range, with: string).onlyNumbers ?? ""
        
        if newDDD.isEmpty && !string.isEmpty {
            return false
        }
        
        if string == UIPasteboard.general.string ?? "" && newDDD.count < 2 {
            dddTextField.text = newDDD
            return false
        }
        
        // jump to the next field when the ddd is completed
        if newDDD.count >= 2 && !string.isEmpty {
            let range: Range<String.Index> = newDDD.startIndex..<newDDD.index(newDDD.startIndex, offsetBy: 2)
            dddTextField.text = String(newDDD[range])
            phoneNumberTextField.becomeFirstResponder()
            return false
        }

        return true
    }
    
    private func formatPhoneTextField(charactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldValue = phoneNumberTextField.text as NSString?
        // back to ddd textfield
        if string.isEmpty && phoneNumberTextField.text?.count == range.length {
            phoneNumberTextField.text = ""
            dddTextField.becomeFirstResponder()
            return false
        }
        
        //format phone
        var newPhone = textFieldValue?.replacingCharacters(in: range, with: string) ?? ""
        newPhone = newPhone.replacingOccurrences(of: "-", with: "").onlyNumbers
        
        if newPhone.isEmpty && !string.isEmpty {
            return false
        }
        
        if string == UIPasteboard.general.string ?? "" && newPhone.count <= 4 {
            phoneNumberTextField.text = newPhone
            return false
        }
        
        if newPhone.count > 4 {
            phoneNumberTextField.text = newPhone.formatAsPhoneNumberWithHyphen()
            return false
        }
        
        return true
    }
}
