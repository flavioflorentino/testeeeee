protocol ChangePhoneNumberViewModelInputs {
    func updateAlreadyHaveACodeButtonState()
    func didTapSentSMSButton(withDDD ddd: String?, phoneNumber: String?)
    func startTimerToUpdateSMSButtonState()
    func clearTimer()
    func sendSMSVerification()
}

protocol ChangePhoneNumberViewModelOutputs: AnyObject {
    func updateSendSMSButton(isEnabled: Bool, title: String)
    func updateErrorDDDField(errorMessage: String?)
    func updateErrorPhoneNumberField(errorMessage: String?)
    func presentNumberConfirmationAlert(ddd: String, phoneNumber: String)
    func hideAlredyHaveACodeButton()
    func presentSMSVerificationError(_ error: Error)
    func presentVerifyCodeStep()
}

protocol ChangePhoneNumberViewModelType: AnyObject {
    var inputs: ChangePhoneNumberViewModelInputs { get }
    var outputs: ChangePhoneNumberViewModelOutputs? { get set }
}

final class ChangePhoneNumberViewModel: ChangePhoneNumberViewModelType, ChangePhoneNumberViewModelInputs {
    var inputs: ChangePhoneNumberViewModelInputs { return self }
    weak var outputs: ChangePhoneNumberViewModelOutputs?

    private let service: ChangePhoneNumberServicing
    
    private var ddd: String?
    private var phoneNumber: String?
    
    private var timer: Timer?
    private var isLoading = false
    
    private var isValidDDD: Bool {
        return ddd?.count == 2
    }
    
    private var isValidPhoneNumber: Bool {
        guard let phoneNumber = phoneNumber else {
            return false
        }
        return phoneNumber.count >= 8
    }
    
    private var elapsedSecondsSinceLastVerification: TimeInterval {
        return -service.lastPhoneVerificationDate.timeIntervalSinceNow
    }
    
    private var smsDelayTime: String? {
        let interval = service.delayToMakeVerificationThroughSMS - elapsedSecondsSinceLastVerification
        return interval > 0 ? formattedTime(interval) : nil
    }
    
    init(service: ChangePhoneNumberServicing) {
        self.service = service
    }
    
    func updateAlreadyHaveACodeButtonState() {
        guard service.phoneHasNoVerificationNumber else {
            return
        }
        outputs?.hideAlredyHaveACodeButton()
    }
    
    private func validate(ddd: String?, phoneNumber: String?) {
        self.ddd = ddd
        self.phoneNumber = phoneNumber
        
        let dddFieldErrorMessage = isValidDDD ? nil : PhoneLocalizable.invalid.text
        let phoneFieldErrorMessage = isValidPhoneNumber ? nil : PhoneLocalizable.invalidPhoneNumber.text
        
        outputs?.updateErrorDDDField(errorMessage: dddFieldErrorMessage)
        outputs?.updateErrorPhoneNumberField(errorMessage: phoneFieldErrorMessage)
    }
    
    func didTapSentSMSButton(withDDD ddd: String?, phoneNumber: String?) {
        validate(ddd: ddd, phoneNumber: phoneNumber)
        guard isValidDDD, isValidPhoneNumber, let ddd = ddd, let phoneNumber = phoneNumber else {
            return
        }
        
        outputs?.presentNumberConfirmationAlert(ddd: ddd, phoneNumber: phoneNumber)
    }
    
    func startTimerToUpdateSMSButtonState() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateSendSMSButtonState), userInfo: nil, repeats: true)
        timer?.fire()
        updateSendSMSButtonState()
    }
    
    func clearTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    @objc
    private func updateSendSMSButtonState() {
        var enabled = isLoading ? false : true
        var title = PhoneLocalizable.sendSMS.text
        if let delay = smsDelayTime {
            title = "\(title) \(delay)"
            enabled = false
        }
        outputs?.updateSendSMSButton(isEnabled: enabled, title: title)
    }
    
    func sendSMSVerification() {
        isLoading = true
        service.sendSMSVerification(ddd: ddd, phoneNumber: phoneNumber) { [weak self] result in
            self?.isLoading = false
            
            switch result {
            case .success:
                self?.outputs?.presentVerifyCodeStep()
            case .failure(let error):
                self?.outputs?.presentSMSVerificationError(error)
            }
        }
    }
    
    private func formattedTime(_ time: TimeInterval) -> String? {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .positional
        formatter.allowedUnits = time >= 3600 ? [.hour, .minute, .second] : [.minute, .second]
        formatter.zeroFormattingBehavior = .pad
        return formatter.string(from: time)
    }
}
