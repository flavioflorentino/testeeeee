protocol ChangePhoneNumberServicing {
    var phoneHasNoVerificationNumber: Bool { get }
    var delayToMakeVerificationThroughSMS: TimeInterval { get }
    var lastPhoneVerificationDate: Date { get }
    func sendSMSVerification(ddd: String?, phoneNumber: String?, completion: @escaping (Result<Void, Error>) -> Void)
}

final class ChangePhoneNumberService: ChangePhoneNumberServicing {
    let parametersProvider: PhoneVerificationParametersProvider
    
    var phoneHasNoVerificationNumber: Bool {
        return parametersProvider.getPhoneVerificationNumber() == nil
    }
    
    var delayToMakeVerificationThroughSMS: TimeInterval {
        let phoneVerificationSMSDelay = parametersProvider.getPhoneVerificationSMSDelay()
        return TimeInterval(exactly: phoneVerificationSMSDelay) ?? TimeInterval()
    }
    
    var lastPhoneVerificationDate: Date {
        if let date = parametersProvider.getPhoneVerificationSentDate() as Date? {
            return date
        }
        return Date()
    }
    
    init(phoneVerificationParametersProvider: PhoneVerificationParametersProvider) {
        parametersProvider = phoneVerificationParametersProvider
    }
    
    func sendSMSVerification(ddd: String?, phoneNumber: String?, completion: @escaping (Result<Void, Error>) -> Void) {
        let parameters = parametersProvider
        WSConsumer.sendSmsVerification(ddd, phone: phoneNumber) { delaySMS, isCallEnabled, delayCall, error in
            parameters.setEnabledPhoneVerificationCall(isCallEnabled)
            parameters.setPhoneVerificationCallDelay(delayCall)
            parameters.setPhoneVerificationSMSDelay(delaySMS)
            parameters.setPhoneVerificationSentDate(Date())
            
            if let error = error {
                completion(.failure(error))
                return
            }
            
            parameters.setPhoneVerificationDDD(ddd)
            parameters.setPhoneVerificationNumber(phoneNumber)
            completion(.success(()))
        }
    }
}
