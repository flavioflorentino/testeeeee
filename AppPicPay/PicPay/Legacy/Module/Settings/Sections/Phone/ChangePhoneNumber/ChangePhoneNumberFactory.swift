import CoreLegacy

final class ChangePhoneNumberFactoryObjc: NSObject {
    @objc
    static func make() -> UIViewController {
        return ChangePhoneNumberFactory.make()
    }
}

final class ChangePhoneNumberFactory {
    static func make() -> ChangePhoneNumberViewController {
        let service: ChangePhoneNumberServicing = ChangePhoneNumberService(phoneVerificationParametersProvider: AppParameters.global())
        let viewModel: ChangePhoneNumberViewModelType = ChangePhoneNumberViewModel(service: service)
        var coordinator: ChangePhoneNumberCoordinating = ChangePhoneNumberCoordinator()
        let viewController = ChangePhoneNumberViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
