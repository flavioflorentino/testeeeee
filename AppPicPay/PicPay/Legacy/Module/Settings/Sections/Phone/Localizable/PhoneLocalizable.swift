enum PhoneLocalizable: String, Localizable {
    case myPhoneNumberScreenTitle
    case myPhoneNumberScreenMessage
    case changeMyPhoneNumber
    case atention
    case verifyNumberDescription
    
    case changeNumberScreenTitle
    case changeNumberScreenMessage
    case ddd
    case phoneNumber
    case sendSMS
    case alreadyHaveACode
    case isNumberCorrect
    case confirmNumber
    case invalid
    case invalidPhoneNumber
    
    case verificationCodeScreenMessage
    case invalidCodeMessage
    case isNumberAboveCorrect
    case youWillReceiveACall
    case callMe
    case resendSMSWithCode
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .phone
    }
}
