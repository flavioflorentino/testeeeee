enum MyNumberAction {
    case changeNumber
}

protocol MyPhoneNumberCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: MyNumberAction)
}

final class MyPhoneNumberCoordinator: MyPhoneNumberCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: MyNumberAction) {
        guard action == .changeNumber else {
            return
        }
        let changeNumberController = ChangePhoneNumberFactory.make()
        viewController?.navigationController?.pushViewController(changeNumberController, animated: true)
    }
}
