import SecurityModule
import UI
import UIKit

final class MyPhoneNumberViewController: LegacyViewController<MyPhoneNumberViewModelType, MyPhoneNumberCoordinating, UIView>, Obfuscable {
    struct Layout {
        static let topOffset: CGFloat = 20
        static let spaceBetweenSubviews: CGFloat = 20
        static let leadingTrailingMargin: CGFloat = 20
        static let trailingMargin: CGFloat = -20
        static let buttonHeight: CGFloat = 48
    }
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.text = PhoneLocalizable.myPhoneNumberScreenMessage.text
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var phoneNumberLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17)
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var changeNumberButton: UIPPButton = {
        let button = UIPPButton(title: PhoneLocalizable.changeMyPhoneNumber.text, target: self, action: #selector(tapChangeNumberButton), type: .cta)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    //Screen obfuscation
    var appSwitcherObfuscator: AppSwitcherObfuscating?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.inputs.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObfuscationConfiguration()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deinitObfuscator()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(messageLabel)
        view.addSubview(phoneNumberLabel)
        view.addSubview(changeNumberButton)
    }
    
    override func configureViews() {
        title = PhoneLocalizable.myPhoneNumberScreenTitle.text
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [messageLabel, phoneNumberLabel, changeNumberButton], constant: Layout.leadingTrailingMargin)
        
        NSLayoutConstraint.activate([
            messageLabel.topAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.topAnchor, constant: Layout.topOffset),
            
            phoneNumberLabel.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: Layout.spaceBetweenSubviews),
            
            changeNumberButton.topAnchor.constraint(equalTo: phoneNumberLabel.bottomAnchor, constant: Layout.spaceBetweenSubviews),
            changeNumberButton.heightAnchor.constraint(equalToConstant: Layout.buttonHeight)
        ])
    }
    
    @objc
    private func tapChangeNumberButton() {
        let alert = Alert(with: PhoneLocalizable.atention.text, text: PhoneLocalizable.verifyNumberDescription.text)
      
        let okButton = Button(title: DefaultLocalizable.btOk.text, type: .cta) { [weak self] popupController, _ in
            popupController.dismiss(animated: true) {
                self?.coordinator.perform(action: .changeNumber)
            }
        }
        
        let cancelButton = Button(title: DefaultLocalizable.btCancel.text, type: .clean, action: .close)
        alert.buttons = [okButton, cancelButton]
        AlertMessage.showAlert(alert, controller: self)
    }
}

// MARK: View Model Outputs
extension MyPhoneNumberViewController: MyPhoneNumberViewModelOutputs {
    func updatePhoneNumber(withValue value: String) {
        phoneNumberLabel.text = value
    }
}
