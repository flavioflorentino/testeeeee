protocol MyPhoneNumberServicing {
    var phoneNumber: String? { get }
}

final class MyPhoneNumberService: MyPhoneNumberServicing {
    var phoneNumber: String? {
        return ConsumerManager.shared.consumer?.verifiedPhoneNumber
    }
}
