import UI

protocol MyPhoneNumberViewModelInputs {
    func viewDidLoad()
}

protocol MyPhoneNumberViewModelOutputs: AnyObject {
    func updatePhoneNumber(withValue value: String)
}

protocol MyPhoneNumberViewModelType: AnyObject {
    var inputs: MyPhoneNumberViewModelInputs { get }
    var outputs: MyPhoneNumberViewModelOutputs? { get set }
}

final class MyPhoneNumberViewModel: MyPhoneNumberViewModelType, MyPhoneNumberViewModelInputs {
    var inputs: MyPhoneNumberViewModelInputs { return self }
    weak var outputs: MyPhoneNumberViewModelOutputs?

    private let service: MyPhoneNumberServicing

    init(service: MyPhoneNumberServicing) {
        self.service = service
    }

    func viewDidLoad() {
        guard let phoneNumber = service.phoneNumber,
            let maskedPhoneNumber = applyMaskToPhoneNumberString(phoneNumber) else {
            return
        }
        outputs?.updatePhoneNumber(withValue: maskedPhoneNumber)
    }
    
    private func applyMaskToPhoneNumberString(_ phoneNumber: String) -> String? {
        switch phoneNumber.count {
        case 11:
            let mask = CustomStringMask(mask: "(00) 00000-0000")
            return mask.maskedText(from: phoneNumber)
        case 10:
            let mask = CustomStringMask(mask: "(00) 0000-0000")
            return mask.maskedText(from: phoneNumber)
        default:
            return phoneNumber
        }
    }
}
