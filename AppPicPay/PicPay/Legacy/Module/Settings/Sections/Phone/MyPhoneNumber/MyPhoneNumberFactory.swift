final class MyPhoneNumberFactory {
    static func make() -> MyPhoneNumberViewController {
        let service: MyPhoneNumberServicing = MyPhoneNumberService()
        let viewModel: MyPhoneNumberViewModelType = MyPhoneNumberViewModel(service: service)
        var coordinator: MyPhoneNumberCoordinating = MyPhoneNumberCoordinator()
        let viewController = MyPhoneNumberViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
