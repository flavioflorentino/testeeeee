enum PhoneVerificationCodeAction {
    case verified
    case close
}

protocol PhoneVerificationCodeCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: PhoneVerificationCodeAction)
}

final class PhoneVerificationCodeCoordinator: PhoneVerificationCodeCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: PhoneVerificationCodeAction) {
        switch action {
        case .verified:
            viewController?.navigationController?.popToRootViewController(animated: true)
        case .close:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}
