import AnalyticsModule
import UI
import UIKit

final class PhoneVerificationCodeViewController: LegacyViewController<PhoneVerificationCodeViewModelType, PhoneVerificationCodeCoordinating, UIView> {
    struct Layout {
        static let topOffset: CGFloat = 40
        static let leadingTrailingMargin: CGFloat = 20
        static let trailingMargin: CGFloat = -20
        static let spaceBetweenSubviews: CGFloat = 12
        static let spaceBetweenButtons: CGFloat = 8
    }
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.text = PhoneLocalizable.verificationCodeScreenMessage.text
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var codeTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.font = UIFont.systemFont(ofSize: 17)
        textField.lineColor = Palette.ppColorBranding400.color
        textField.selectedLineColor = Palette.ppColorBranding300.color
        textField.selectedTitleColor = Palette.ppColorGrayscale300.color
        textField.textColor = Palette.ppColorGrayscale600.color
        textField.errorColor = Palette.ppColorNegative200.color
        textField.keyboardType = .numberPad
        textField.textAlignment = .center
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.delegate = self
        return textField
    }()
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .gray)
        view.hidesWhenStopped = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = Palette.ppColorNegative200.color
        label.textAlignment = .center
        label.numberOfLines = 0
        label.isHidden = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var callButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.NewGeneration.iconPhone.image, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -8, bottom: 0, right: 0)
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        button.addTarget(self, action: #selector(tapCallButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var sendSMSButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.NewGeneration.iconSms.image, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -8, bottom: 0, right: 0)
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        button.addTarget(self, action: #selector(tapSendSMSButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 8
        stackView.alignment = .center
        stackView.isUserInteractionEnabled = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        codeTextField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.inputs.startTimerToUpdateSMSAndCallButtonsState()
        Analytics.shared.log(PhoneNumberEvent.change)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.inputs.clearTimer()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(messageLabel)
        view.addSubview(codeTextField)
        view.addSubview(errorLabel)
        view.addSubview(activityIndicator)
        
        buttonsStackView.addArrangedSubview(callButton)
        buttonsStackView.addArrangedSubview(sendSMSButton)
        view.addSubview(buttonsStackView)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [messageLabel, errorLabel], constant: Layout.leadingTrailingMargin)
        
        NSLayoutConstraint.activate([
            messageLabel.topAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.topAnchor, constant: Layout.topOffset),
            
            codeTextField.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: Layout.spaceBetweenSubviews),
            codeTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            codeTextField.widthAnchor.constraint(equalToConstant: 80),
            
            activityIndicator.topAnchor.constraint(equalTo: codeTextField.bottomAnchor, constant: Layout.spaceBetweenSubviews),
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            errorLabel.topAnchor.constraint(equalTo: activityIndicator.topAnchor),
            
            buttonsStackView.topAnchor.constraint(equalTo: errorLabel.bottomAnchor, constant: 40),
            buttonsStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    private func finishStep() {
        guard let code = self.codeTextField.text else {
            return
        }
        viewModel.inputs.validateCode(code: code) 
    }
    
    @objc
    private func tapCallButton() {
        viewModel.inputs.callUserPhone()
    }
    
    @objc
    private func tapSendSMSButton() {
        viewModel.inputs.confirmSendSMSCode()
    }
    
    private func close() {
        coordinator.perform(action: .close)
    }
}

// MARK: View Model Outputs
extension PhoneVerificationCodeViewController: PhoneVerificationCodeViewModelOutputs {
    func updateCallButton(isEnabled: Bool, title: String) {
        DispatchQueue.main.async { [weak self] in
            self?.callButton.isEnabled = isEnabled
            self?.callButton.setTitle(title, for: isEnabled ? .normal : .disabled)
        }
    }
    
    func updateSendSMSButton(isEnabled: Bool, title: String) {
        DispatchQueue.main.async { [weak self] in
            self?.sendSMSButton.isEnabled = isEnabled
            self?.sendSMSButton.setTitle(title, for: isEnabled ? .normal : .disabled)
        }
    }
    
    func disableSendSMSButton() {
        sendSMSButton.isEnabled = false
    }
    
    func disableCallButton() {
        callButton.isEnabled = false
    }
    
    func hideCallButton() {
        DispatchQueue.main.async { [weak self] in
            self?.callButton.isHidden = true
        }
    }
    
    func showErrorMessage(labelMessage: String?, textFieldMessage: String?) {
        DispatchQueue.main.async { [weak self] in
            self?.errorLabel.text = labelMessage
            self?.errorLabel.isHidden = false
            self?.codeTextField.errorMessage = textFieldMessage
        }
    }
    
    func hideErrorMessage() {
        errorLabel.text = nil
        errorLabel.isHidden = true
    }
    
    func startLoading() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator.startAnimating()
        }
    }
    
    func stopLoading() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator.stopAnimating()
        }
    }
    
    func phoneNumberVerifiedWithSuccess() {
        DispatchQueue.main.async { [weak self] in
            self?.coordinator.perform(action: .verified)
        }
    }
    
    func showConfirmationAlertWith(ddd: String, phoneNumber: String) {
        let text = String(format: PhoneLocalizable.isNumberAboveCorrect.text, ddd, phoneNumber)
        let alert = Alert(title: PhoneLocalizable.confirmNumber.text, text: text)
        let okButton = Button(title: DefaultLocalizable.btOk.text, type: .cta) { [weak self] popupController, _ in
            popupController.dismiss(animated: true) {
                self?.viewModel.inputs.sendSMSCodeRequest()
            }
        }
        let cancelButton = Button(title: DefaultLocalizable.btEdit.text, type: .clean) { [weak self] popupController, _ in
            popupController.dismiss(animated: true) {
                self?.close()
            }
        }
        alert.buttons = [okButton, cancelButton]
        
        DispatchQueue.main.async { [weak self] in
            AlertMessage.showAlert(alert, controller: self)
        }
    }
    
    func showYouWillReceiveACallAlert() {
        let alert = Alert(title: nil, text: PhoneLocalizable.youWillReceiveACall.text)
        
        DispatchQueue.main.async { [weak self] in
            AlertMessage.showAlert(alert, controller: self)
        }
    }
}

extension PhoneVerificationCodeViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == codeTextField {
            finishStep()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard textField == codeTextField, let textFieldText = textField.text else {
            return true
        }
        
        let newCode = (textFieldText as NSString).replacingCharacters(in: range, with: string)
        if newCode.count > 4 {
            return false
        }
        if newCode.count == 4 {
            let range: Range<String.Index> = newCode.startIndex ..< newCode.index(newCode.startIndex, offsetBy: 4)
            codeTextField.text = String(newCode[range])
            finishStep()
            return false
        }
        
        return true
    }
}
