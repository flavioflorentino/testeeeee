protocol PhoneVerificationCodeViewModelInputs {
    func startTimerToUpdateSMSAndCallButtonsState()
    func clearTimer()
    func confirmSendSMSCode()
    func sendSMSCodeRequest()
    func callUserPhone()
    func validateCode(code: String)
}

protocol PhoneVerificationCodeViewModelOutputs: AnyObject {
    func updateCallButton(isEnabled: Bool, title: String)
    func updateSendSMSButton(isEnabled: Bool, title: String)
    func disableSendSMSButton()
    func disableCallButton()
    func hideCallButton()
    func showErrorMessage(labelMessage: String?, textFieldMessage: String?)
    func hideErrorMessage()
    func startLoading()
    func stopLoading()
    func phoneNumberVerifiedWithSuccess()
    func showConfirmationAlertWith(ddd: String, phoneNumber: String)
    func showYouWillReceiveACallAlert()
}

protocol PhoneVerificationCodeViewModelType: AnyObject {
    var inputs: PhoneVerificationCodeViewModelInputs { get }
    var outputs: PhoneVerificationCodeViewModelOutputs? { get set }
}

final class PhoneVerificationCodeViewModel: PhoneVerificationCodeViewModelType, PhoneVerificationCodeViewModelInputs {
    var inputs: PhoneVerificationCodeViewModelInputs { return self }
    weak var outputs: PhoneVerificationCodeViewModelOutputs?

    private let service: PhoneVerificationCodeServicing
    
    private let ddd: String?
    private let phoneNumber: String?
    
    private var timer: Timer?
    private var isLoading = false
    
    private var elapsedSecondsSinceLastVerification: TimeInterval {
        return -service.lastPhoneVerificationDate.timeIntervalSinceNow
    }
    
    private var callDelayTime: String? {
        let interval = service.delayToMakeVerificationThroughCall - elapsedSecondsSinceLastVerification
        return interval > 0 ? formattedTime(interval) : nil
    }
    
    private var smsDelayTime: String? {
        let interval = service.delayToMakeVerificationThroughSMS - elapsedSecondsSinceLastVerification
        return interval > 0 ? formattedTime(interval) : nil
    }
    
    init(service: PhoneVerificationCodeServicing) {
        self.service = service
        ddd = service.phoneVerificationDDD
        phoneNumber = service.phoneVerificationNumber
    }
    
    func validateCode(code: String) {
        guard code.count == 4 else {
            outputs?.showErrorMessage(labelMessage: PhoneLocalizable.invalidCodeMessage.text, textFieldMessage: PhoneLocalizable.invalid.text)
            return
        }
        outputs?.hideErrorMessage()
        verifyPhoneNumber(code)
    }
    
    func startTimerToUpdateSMSAndCallButtonsState() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateSMSAndCallButtonState), userInfo: nil, repeats: true)
        timer?.fire()
        updateSMSAndCallButtonState()
    }
    
    func clearTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    func confirmSendSMSCode() {
        guard let ddd = ddd, let phoneNumber = phoneNumber else {
            return
        }
        outputs?.showConfirmationAlertWith(ddd: ddd, phoneNumber: phoneNumber)
    }
    
    func sendSMSCodeRequest() {
        outputs?.disableSendSMSButton()
        isLoading = true
        service.finishPhoneStep(ddd: ddd, phoneNumber: phoneNumber) { [weak self] result in
            DispatchQueue.main.async {
                self?.isLoading = false
                self?.updateSMSAndCallButtonState()
                
                switch result {
                case .success:
                    break
                case .failure(let error):
                    self?.outputs?.showErrorMessage(labelMessage: error.localizedDescription, textFieldMessage: nil)
                }
            }
        }
    }
    
    func callUserPhone() {
        guard let ddd = ddd, let phoneNumber = phoneNumber else {
            return
        }
        outputs?.disableCallButton()
        isLoading = true
        service.requestCallToPhone(ddd: ddd, phoneNumber: phoneNumber) { [weak self] result in
            self?.isLoading = false
            self?.updateSMSAndCallButtonState()
            
            switch result {
            case .success:
                self?.outputs?.showYouWillReceiveACallAlert()
            case .failure(let error):
                self?.outputs?.showErrorMessage(labelMessage: error.localizedDescription, textFieldMessage: nil)
            }
        }
    }
    
    @objc
    private func updateSMSAndCallButtonState() {
        updateCallButtonState()
        updateSendSMSButtonState()
    }

    private func updateCallButtonState() {
        guard service.isCallVerificationEnabled else {
            outputs?.hideCallButton()
            return
        }
        var enabled = isLoading ? false : true
        var title = PhoneLocalizable.callMe.text
        if let delay = callDelayTime {
            title = "\(title) \(delay)"
            enabled = false
        }
        outputs?.updateCallButton(isEnabled: enabled, title: title)
    }
    
    private func updateSendSMSButtonState() {
        var enabled = isLoading ? false : true
        var title = PhoneLocalizable.resendSMSWithCode.text
        if let delay = smsDelayTime {
            title = "\(title) \(delay)"
            enabled = false
        }
        outputs?.updateSendSMSButton(isEnabled: enabled, title: title)
    }
    
    private func formattedTime(_ time: TimeInterval) -> String? {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .positional
        formatter.allowedUnits = time >= 3600 ? [.hour, .minute, .second] : [.minute, .second]
        formatter.zeroFormattingBehavior = .pad
        return formatter.string(from: time)
    }
    
    private func verifyPhoneNumber(_ code: String) {
        outputs?.startLoading()
        isLoading = true
        service.verifyPhoneNumber(code: code) { [weak self] result in
            self?.isLoading = false
            self?.outputs?.stopLoading()
            
            switch result {
            case .success:
                self?.outputs?.phoneNumberVerifiedWithSuccess()
            case .failure(let error):
                self?.outputs?.showErrorMessage(labelMessage: error.localizedDescription, textFieldMessage: nil)
            }
        }
    }
}
