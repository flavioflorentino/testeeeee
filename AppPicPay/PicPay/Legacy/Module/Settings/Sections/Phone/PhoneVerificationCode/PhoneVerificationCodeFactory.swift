import CoreLegacy

final class PhoneVerificationCodeFactory {
    static func make() -> PhoneVerificationCodeViewController {
        let service: PhoneVerificationCodeServicing = PhoneVerificationCodeService(phoneVerificationParametersProvider: AppParameters.global())
        let viewModel: PhoneVerificationCodeViewModelType = PhoneVerificationCodeViewModel(service: service)
        var coordinator: PhoneVerificationCodeCoordinating = PhoneVerificationCodeCoordinator()
        let viewController = PhoneVerificationCodeViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
