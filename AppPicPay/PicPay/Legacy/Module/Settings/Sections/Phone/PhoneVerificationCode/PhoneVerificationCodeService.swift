protocol PhoneVerificationCodeServicing {
    var phoneVerificationNumber: String? { get }
    var phoneVerificationDDD: String? { get }
    var delayToMakeVerificationThroughCall: TimeInterval { get }
    var delayToMakeVerificationThroughSMS: TimeInterval { get }
    var lastPhoneVerificationDate: Date { get }
    var isCallVerificationEnabled: Bool { get }
    func finishPhoneStep(ddd: String?, phoneNumber: String?, completion: @escaping (Result<Void, Error>) -> Void)
    func requestCallToPhone(ddd: String?, phoneNumber: String?, completion: @escaping (Result<Void, Error>) -> Void)
    func verifyPhoneNumber(code: String, completion: @escaping (Result<Void, Error>) -> Void)
}

final class PhoneVerificationCodeService: PhoneVerificationCodeServicing {
    let parametersProvider: PhoneVerificationParametersProvider
    let consumerWorker: ConsumerWorkerProtocol
    
    var phoneVerificationNumber: String? {
        return parametersProvider.getPhoneVerificationNumber()
    }
    
    var phoneVerificationDDD: String? {
        return parametersProvider.getPhoneVerificationDDD()
    }
    
    var delayToMakeVerificationThroughCall: TimeInterval {
        let phoneVerificationCallDelay = parametersProvider.getPhoneVerificationCallDelay()
        return TimeInterval(exactly: phoneVerificationCallDelay) ?? TimeInterval()
    }
    
    var delayToMakeVerificationThroughSMS: TimeInterval {
        let phoneVerificationSMSDelay = parametersProvider.getPhoneVerificationSMSDelay()
        return TimeInterval(exactly: phoneVerificationSMSDelay) ?? TimeInterval()
    }
    
    var lastPhoneVerificationDate: Date {
        if let date = parametersProvider.getPhoneVerificationSentDate() as Date? {
            return date
        }
        return Date()
    }
    
    var isCallVerificationEnabled: Bool {
        return parametersProvider.isEnabledPhoneVerificationCall()
    }
    
    init(phoneVerificationParametersProvider: PhoneVerificationParametersProvider, consumerWorker: ConsumerWorkerProtocol = ConsumerWorker()) {
        parametersProvider = phoneVerificationParametersProvider
        self.consumerWorker = consumerWorker
    }
    
    func finishPhoneStep(ddd: String?, phoneNumber: String?, completion: @escaping (Result<Void, Error>) -> Void) {
        WSConsumer.sendSmsVerification(ddd, phone: phoneNumber) { [weak self] smsDelay, isCallEnabled, callDelay, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            self?.updateGlobalPhoneParameters(smsDelay: smsDelay, isCallEnabled: isCallEnabled, callDelay: callDelay)
            completion(.success(()))
        }
    }
    
    func requestCallToPhone(ddd: String?, phoneNumber: String?, completion: @escaping (Result<Void, Error>) -> Void) {
        WSConsumer.callVerification(ddd, phone: phoneNumber) { [weak self] smsDelay, isCallEnabled, callDelay, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            self?.updateGlobalPhoneParameters(smsDelay: smsDelay, isCallEnabled: isCallEnabled, callDelay: callDelay)
            completion(.success(()))
        }
    }
    
    private func updateGlobalPhoneParameters(smsDelay: Int, isCallEnabled: Bool, callDelay: Int) {
        parametersProvider.setPhoneVerificationSentDate(Date())
        parametersProvider.setPhoneVerificationSMSDelay(smsDelay)
        parametersProvider.setPhoneVerificationCallDelay(callDelay)
        parametersProvider.setEnabledPhoneVerificationCall(isCallEnabled)
    }
    
    func verifyPhoneNumber(code: String, completion: @escaping (Result<Void, Error>) -> Void) {
        WSConsumer.verifyPhoneNumber(code) { [weak self] ddd, phoneNumber, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            self?.updateLocalConsumerPhone(ddd: ddd, phoneNumber: phoneNumber)
            completion(.success(()))
        }
    }
    
    private func updateLocalConsumerPhone(ddd: String?, phoneNumber: String?) {
        consumerWorker.consumer?.verifiedPhoneNumber = "\(ddd ?? "")\(phoneNumber ?? "")"
    }
}
