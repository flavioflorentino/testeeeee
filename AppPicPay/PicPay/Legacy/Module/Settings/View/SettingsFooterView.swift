import UIKit

final class SettingsFooterView: UIView {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 11.0)
        label.textColor = .lightGray
        label.textAlignment = .center
        return label
    }()
    
    convenience init(frame: CGRect, titleLabel: String) {
        self.init(frame: frame)
        self.titleLabel.text = titleLabel
    }
    
    override init(frame: CGRect) {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: 40))
        super.init(frame: view.frame)
        backgroundColor = .clear
        
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        addSubview(titleLabel)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10)
        ])
    }
}
