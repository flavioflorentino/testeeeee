import UI
import UIKit
import SnapKit

enum SettingsHeaderBankInfoState {
    case loading
    case tryAgain
    case loaded(info: String)
}

extension SettingsHeaderBankInfoState: Equatable {
    static func == (lhs: SettingsHeaderBankInfoState, rhs: SettingsHeaderBankInfoState) -> Bool {
        switch (lhs, rhs) {
        case (.loading, .loading):
            return true
        case (.tryAgain, .tryAgain):
            return true
        case (.loaded, .loaded):
            return true
        default:
            return false
        }
    }
}

protocol SettingsHeaderBankInfoDelegate: AnyObject {
    func tryAgain()
}

final class SettingsHeaderBankInfoView: UIView, ViewConfiguration {
    weak var delegate: SettingsHeaderBankInfoDelegate?
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.hidesWhenStopped = true
        return indicator
    }()
    
    private lazy var bankInfoLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var tryAgainButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.numberOfLines = 0
        button.setTitle(Strings.Legacy.Settings.TryAgain.bankInfo,
                        for: .normal)
        button.buttonStyle(SecondaryButtonStyle(size: .small,
                                                icon: (name: Iconography.redo,
                                                       alignment: IconAlignment.left)))
        button.addTarget(self, action: #selector(tryAgain), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        buildLayout()
    }
    
    func buildViewHierarchy() {
        addSubview(activityIndicator)
        addSubview(tryAgainButton)
        addSubview(bankInfoLabel)
    }
    
    func setupConstraints() {
        bankInfoLabel.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        tryAgainButton.snp.makeConstraints {
            $0.width.equalTo(size.width / 1.5)
            $0.center.equalToSuperview()
        }
        
        activityIndicator.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    func configureViews() {
        isHidden = true
        backgroundColor = Colors.groupedBackgroundPrimary.color
    }
    
    func display(state: SettingsHeaderBankInfoState) {
        UIView.animate(withDuration: 0.3) {
            self.bankInfoLabel.isHidden = state == .tryAgain || state == .loading
            self.tryAgainButton.isHidden = state != .tryAgain
            state == .loading
                ? self.activityIndicator.startAnimating()
                : self.activityIndicator.stopAnimating()
            
            if case let .loaded(info) = state {
                self.bankInfoLabel.text = info
            }
        }
    }
}

private extension SettingsHeaderBankInfoView {
    @objc
    func tryAgain() {
        delegate?.tryAgain()
    }
}
