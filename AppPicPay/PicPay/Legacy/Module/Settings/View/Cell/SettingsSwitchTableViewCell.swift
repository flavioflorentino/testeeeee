import UI
import UIKit

final class SettingsSwitchTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var statusSwitch: UISwitch!
    
    private var action: ((Bool) -> Void)?
    
    // MARK: - Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    // MARK: - Private Methods
    
    private func setupView() {
        backgroundColor = Colors.groupedBackgroundSecondary.color
        statusSwitch.addTarget(self, action: #selector(self.changeSwitch), for: .valueChanged)
    }
    
    @objc
    private func changeSwitch(_ sender: UISwitch) {
        action?(sender.isOn)
    }
    
    // MARK: - Public Methods
    
    func configureCell(title: String, isActivated: Bool, action: @escaping (Bool) -> Void) {
        titleLabel.text = title
        statusSwitch.isOn = isActivated
        self.action = action
    }
}
