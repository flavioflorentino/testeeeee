import UI
import UIKit

final class SettingsTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var pendingImage: UIImageView!
    @IBOutlet weak var alertImage: UIImageView!
    @IBOutlet weak var analysisImage: UIImageView!
    
    private lazy var badgeNew: UILabel = {
        let label = UILabel()
        label.isHidden = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale000.color
        label.backgroundColor = UIColor(red: 0, green: 0.69, blue: 0.93, alpha: 1)
        label.layer.cornerRadius = 2
        label.layer.masksToBounds = true
        label.text = SettingsLocalizable.new.text
        label.textAlignment = .center
        contentView.addSubview(label)
        
        label.widthAnchor.constraint(equalToConstant: 43).isActive = true
        label.heightAnchor.constraint(equalToConstant: 22).isActive = true
        label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -17).isActive = true
        
        return label
    }()
    
    // MARK: - Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = Colors.groupedBackgroundSecondary.color
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        badgeNew.isHidden = true
        alertImage.isHidden = true
        pendingImage.isHidden = true
        analysisImage.isHidden = true
    }
    
    // MARK: - Public Methods
    
    func configureCell(title: String, desc: String, type: SettingsRow.TypeCell) {
        titleLabel.text = title
        descLabel.text = desc
        titleLabel.textColor = Palette.ppColorGrayscale600.color
        descLabel.textColor = Palette.ppColorGrayscale400.color
        
        switch type {
        case .pending:
            pendingImage.isHidden = false
            descLabel.textColor = Palette.ppColorAttentive400.color
        case .analysis:
            analysisImage.isHidden = false
        case .logout:
            titleLabel.textColor = .red
        case .alert:
            alertImage.isHidden = false
            descLabel.textColor = .red
        case .new:
            badgeNew.isHidden = false
        default: ()
        }
    }
}
