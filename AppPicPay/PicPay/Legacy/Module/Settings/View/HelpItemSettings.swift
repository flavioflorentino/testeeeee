import UI
import UIKit

final class HelpItemSettings: UIButton {
    private lazy var helpNotificationBadge: UINotificationBadge = {
        let offesetY: CGFloat = -5
        let offsetX: CGFloat = frame.width * 0.87
        let button = UINotificationBadge(frame: CGRect(x: offsetX, y: offesetY, width: 18, height: 18))
        button.isHidden = true
        return button
    }()
    
    override init(frame: CGRect) {
        let frame = CGRect(x: 0, y: 0, width: 65, height: 20)
        super.init(frame: frame)
        setupView()
        addComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateNotificationBadge(number: Int) {
        if number > 0 {
            helpNotificationBadge.isHidden = false
            helpNotificationBadge.text = "\(Int(number))"
        } else {
            helpNotificationBadge.isHidden = true
        }
    }
    
    private func setupView() {
        contentHorizontalAlignment = .right
        backgroundColor = .clear
        setTitle(DefaultLocalizable.help.text, for: .normal)
        clipsToBounds = false
        setTitleColor(Palette.ppColorBranding300.color, for: .normal)
    }
    
    private func addComponents() {
        addSubview(helpNotificationBadge)
    }
}
