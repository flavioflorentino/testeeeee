import Core
import FeatureFlag
import Foundation

protocol SettingsHeaderViewModelProtocol {
    var username: String { get }
    var fullname: String { get }
    var contact: PPContact { get }
    var shouldDisplayBankInfo: Bool { get }
    func fetchBankInfo(completion: @escaping (Result<String, ApiError>) -> Void)
    func resizeImage(image: UIImage) -> Data?
    func uploadProfileImage(imageData: Data, uploadProgress: @escaping (CGFloat) -> Void, onSuccess: @escaping (String?) -> Void, onError: @escaping (Error) -> Void)
}

final class SettingsHeaderViewModel {
    typealias Dependencies = HasMainQueue & HasFeatureManager
    private let dependencies: Dependencies
    
    private let api: ConsumerApiProtocol
    private let worker: ConsumerWorkerProtocol
    
    init(dependencies: Dependencies, api: ConsumerApiProtocol = ConsumerApi(), worker: ConsumerWorkerProtocol = ConsumerWorker()) {
        self.dependencies = dependencies
        self.api = api
        self.worker = worker
    }
}

extension SettingsHeaderViewModel: SettingsHeaderViewModelProtocol {
    var username: String {
        "@" + (worker.consumer?.username ?? "")
    }
    
    var fullname: String {
        return worker.consumer?.name ?? ""
    }
    
    var contact: PPContact {
        guard let consumer = worker.consumer else {
            return PPContact()
        }
        return PPContact(consumer)
    }
    
    var shouldDisplayBankInfo: Bool {
        dependencies.featureManager.isActive(.isCashinAccountAdjustmentsAvailable)
    }
    
    func fetchBankInfo(completion: @escaping (Result<String, ApiError>) -> Void) {
        api.fetchBankInfo { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case let .success(bankInfo):
                    let infoString = Strings.Legacy.Settings.bankInfo(bankInfo.agency,
                                                                      bankInfo.account,
                                                                      bankInfo.bankCode,
                                                                      bankInfo.bankName)
                    completion(.success(infoString))
                    
                case let .failure(error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    func uploadProfileImage(imageData: Data, uploadProgress: @escaping (CGFloat) -> Void, onSuccess: @escaping (String?) -> Void, onError: @escaping (Error) -> Void) {
        api.uploadProfileImage(imageData: imageData, uploadProgress: { progress in
            DispatchQueue.main.async {
                uploadProgress(CGFloat(progress) * 100)
            }
        }, completion: { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let imageUpload):
                    onSuccess(imageUpload.imgUrl)
                case .failure(let error):
                    onError(error)
                }
            }
        })
    }
    
    func resizeImage(image: UIImage) -> Data? {
        guard let resizeImage = image.resizeImage(targetSize: CGSize(width: 1200, height: 1200)) else {
            return nil
        }
        let imageData = resizeImage.jpegData(compressionQuality: 0.8)
        return imageData
    }
}

struct ProfileImageUpload: Codable {
    enum CodingKeys: String, CodingKey {
        case imgUrl
        case data
    }
    
    let imgUrl: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        imgUrl = try data.decodeIfPresent(String.self, forKey: .imgUrl)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        var data = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        try data.encode(imgUrl, forKey: .imgUrl)
    }
}
