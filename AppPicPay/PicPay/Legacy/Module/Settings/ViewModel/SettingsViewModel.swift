import AnalyticsModule
import Core
import FeatureFlag
import UI
import UIKit

// swiftlint:disable:next type_body_length
final class SettingsViewModel {
    typealias Dependencies = HasFeatureManager & HasAnalytics
    private var dependencies: Dependencies
    
    private var model: [SettingsSection] = []
    private let worker: ConsumerWorkerProtocol
    private let settingsService: SettingsServicing
    
    private let identityWorker: IdentityValidationStatusServicing
    private var identityVerificationStatus: IVIdentityValidationStatus?
    
    private var upgradeAccountStatus: UpgradeAccountStatus?
    private var registrationRenewalSettings: RegistrationRenewalSettings?
    
    var accountOfCreditPicPay: CPAccount?
    
    var isLoanSettingsActive: Bool = false
    var isStudentAccount: Bool = false
    var p2plendingSectionSettings: P2PLendingSectionSettings?
    
    var creditPicPayEnabled: Bool {
        return settingsService.isEnableCreditPicPay()
    }
    
    var identityVerificationEnabled: Bool {
        dependencies.featureManager.isActive(.identityVerification)
    }

    var isPasswordResetActive: Bool {
        dependencies.featureManager.isActive(.isPasswordResetLoggedinAvailable)
    }
    
    var upgradeAccountEnabled: Bool {
        return upgradeAccountStatus?.enabled ?? false
    }
    
    var appVersion: String {
        return settingsService.appVersion() ?? ""
    }
    
    init(worker: ConsumerWorkerProtocol = ConsumerWorker(), settingsService: SettingsServicing = SettingsWorker(), identityWorker: IdentityValidationStatusServicing = IdentityValidationStatusService(), dependencies: Dependencies = DependencyContainer()) {
        self.identityWorker = identityWorker
        self.worker = worker
        self.settingsService = settingsService
        self.dependencies = dependencies
    }
    
    func title(section: Int) -> String {
        return model[section].title.text
    }
    
    func title(section: Int, index: Int) -> String {
        return model[section].rows[index].title.text
    }
    
    func subtitle(section: Int, index: Int) -> String {
        return model[section].rows[index].subtitle
    }
    
    func type(section: Int, index: Int) -> SettingsRow.TypeCell {
        return model[section].rows[index].cell
    }
    
    func action(section: Int, index: Int) -> SettingsRow.Action {
        return model[section].rows[index].action
    }
    
    func instantiateSections() {
        var sections: [SettingsSection] = []
        guard let consumer = worker.consumer else {
            return
        }
        
        if isStudentAccount {
            sections.append(SettingsSection(title: .sectionStudentAccount, rows: instantiateStudentAccount()))
        }
        sections.append(SettingsSection(title: .sectionAccount, rows: instantiateAccount()))
        
        if let settings = p2plendingSectionSettings, settings.enable {
            let rows = p2plendingRows(rowSettings: settings.options)
            if !rows.isEmpty {
                sections.append(SettingsSection(title: .sectionP2PLending, rows: rows))
            }
        }
        
        if isLoanSettingsActive {
            sections.append(SettingsSection(title: .sectionLoan, rows: instantiateLoan()))
        }

        if let account = accountOfCreditPicPay, settingsService.isCardSettingsEnable(for: account) {
            let rows = instantiateCreditPicpay(account: account)
            sections.append(SettingsSection(title: .sectionCreditPicpay, rows: rows))
        }

        if dependencies.featureManager.isActive(.releasePixKeyManagementOnSettingsBool) {
            sections.append(SettingsSection(title: .sectionPix, rows: instantiatePix()))
        }
        
        sections.append(SettingsSection(title: .sectionPromotion, rows: instantiatePromotion()))
        
        sections.append(SettingsSection(title: .sectionSecurity, rows: instantiateSecurity()))
        
        sections.append(SettingsSection(title: .sectionSubscriptions, rows: instantiateSubscriptions()))
        
        if consumer.businessAccount {
            sections.append(SettingsSection(title: .sectionPro, rows: instantiatePRO()))
        }
        
        sections.append(SettingsSection(title: .sectionBusiness, rows: instantiateBussiness()))
                
        sections.append(SettingsSection(title: .sectionSettings, rows: instantiateSettings()))
        sections.append(SettingsSection(title: .sectionGeneral, rows: instantiateGeneral()))
        
        #if DEBUG
        if FeatureFlagControlHelper.isEnabled {
            sections.append(SettingsSection(title: .sectionLogout, rows: instantiateFeatureFlagControl()))
        }
        #endif
        
        sections.append(SettingsSection(title: .sectionLogout, rows: instantiateLogout()))
        
        
        model = sections
    }
    
    func setupContactAttributes(in popupViewController: PopupViewController) {
        //TODO: Colocar o numero correto do original
        let phoneUrl = "tel://+55XXXXXXXX"
        let phoneText = String(format: SettingsLocalizable.alertMessage.text, "XXXXXXXXX")
        let range = NSString(string: phoneText).range(of: phoneUrl)

        let attributedText = phoneText.attributedStringWith(
            normalFont: .systemFont(ofSize: 14, weight: .regular),
            highlightFont: .systemFont(ofSize: 16, weight: .bold),
            normalColor: Palette.ppColorGrayscale500.color,
            highlightColor: Palette.ppColorGrayscale500.color,
            textAlignment: .center,
            underline: false,
            lineHeight: 24
        )

        if let url = URL(string: phoneUrl),
            UIApplication.shared.canOpenURL(url) {
            let linkAttributes: [NSAttributedString.Key: Any] = [
                .link: url,
                .font: UIFont.boldSystemFont(ofSize: 16),
                .underlineStyle: NSUnderlineStyle.single.rawValue,
                .foregroundColor: Palette.ppColorBranding300.color
            ]
            
            attributedText.addAttributes(linkAttributes, range: range)
            
            popupViewController.setAttributeDescription(attributedText, linkAttributes: linkAttributes)
        } else {
            popupViewController.setAttributeDescription(attributedText)
        }
    }
    
    private func instantiateStudentAccount() -> [SettingsRow] {
        var rows: [SettingsRow] = []
        let seemOnce = KVStore().boolFor(.sawBenefits)
        let type: SettingsRow.TypeCell = seemOnce ? .normal : .new
        rows.append(SettingsRow(title: .rowBenefits, subtitle: nil, cell: type, action: .open(.benefits)))
        return rows
    }
    
    // MARK: Section Account
    private func instantiateAccount() -> [SettingsRow] {
        var rows: [SettingsRow] = []
        guard let consumer = worker.consumer else {
            return rows
        }
        let email: String?
        let phoneVerificationSubtitle: String?
        if dependencies.featureManager.isActive(.featureAppSecObfuscationMask) {
            email = StringObfuscationMasker.mask(email: consumer.email)
            phoneVerificationSubtitle = StringObfuscationMasker.mask(phone: consumer.verifiedPhoneNumber)
        } else {
            phoneVerificationSubtitle = consumer.verifiedPhoneNumber?.cellphoneFormatting()
            email = consumer.email
        }

        let emailConfirmed: Bool = consumer.emailConfirmed
        
        var username = ""
        if let user: String = consumer.username {
            username = "@\(user)"
        }
        
        rows.append(SettingsRow(title: .rowMyPicPay, subtitle: username, action: .open(.myPicPay)))
        
        if consumer.verifiedPhoneNumber != nil {
            rows.append(SettingsRow(title: .rowMyNumber, subtitle: phoneVerificationSubtitle, action: .open(.myNumberCheck)))
        } else {
            rows.append(SettingsRow(title: .rowMyNumber, subtitle: phoneVerificationSubtitle, cell: .alert, action: .open(.myNumber)))
        }
        
        rows.append(SettingsRow(title: .rowMyEmail, subtitle: email, cell: (emailConfirmed ? .normal : .alert), action: .open(.myEmail)))
        rows.append(SettingsRow(title: .rowPersonalData, action: .open(.personalData)))
        
        if let bankAccount = consumer.bankAccount {
            rows.append(SettingsRow(title: .rowBankAccount, action: .open(.bankAccountCheck(bankAccount))))
        } else {
            rows.append(SettingsRow(title: .rowBankAccount, action: .open(.bankAccount)))
        }
        
        if dependencies.featureManager.isActive(.isIncomeStatementsAvailable) {
            rows.append(SettingsRow(title: .incomeStatements, cell: .new, action: .open(.incomeStatements)))
        }
       
        rows.append(SettingsRow(title: .rowTaxAndLimits, action: .open(.taxAndLimits)))
        
        if dependencies.featureManager.isActive(.address) {
            rows.append(SettingsRow(title: .rowMyAddress, action: .open(.myAddress)))
        }
        
        rows.append(SettingsRow(title: .rowMyFavorites, action: .open(.myFavorites)))
        
        if upgradeAccountEnabled, let row = instantiateUpgradeAccount() {
            rows.append(row)
        }
    
        if let row = instantiateRegistrationRenewal() {
            rows.append(row)
        }
        
        return rows
    }
    
    private func instantiateIdentityVerification() -> SettingsRow? {
        guard let status = identityVerificationStatus?.status else {
            return nil
        }
        
        var cell: SettingsRow.TypeCell = .normal
        switch status {
        case .pending:
             cell = .pending
        case .toVerify:
            cell = .analysis
        case .verified:
            cell = .normal
        case .notCreated, .rejected, .undefined, .notVerified:
            cell = .alert
        case .disabled:
            return nil
        }
        return SettingsRow(title: .rowIdentity, subtitle: identityVerificationStatus?.label, cell: cell, action: .open(.identityVerification))
    }
    
    private func instantiateUpgradeAccount() -> SettingsRow? {
        guard let status = upgradeAccountStatus?.status else {
            return nil
        }
        
        var cell: SettingsRow.TypeCell = .normal
        let subtitle = upgradeAccountStatus?.label ?? ""
        
        switch status {
        case .pending:
            cell = .pending
        case .analysis:
            cell = .analysis
        case .rejected:
            cell = .alert
        case .approved, .notStarted:
            cell = .normal
        }
        return SettingsRow(title: .rowUpgrade, subtitle: subtitle, cell: cell, action: .open(.upgradeAccount))
    }
    
    private func instantiateRegistrationRenewal() -> SettingsRow? {
        guard
            let status = registrationRenewalSettings,
            settingsService.isRegistrationRenewalEnabled
            else {
                return nil
        }
        
        return SettingsRow(
            title: .rowRegistrationRenewal,
            subtitle: status.subtitle,
            cell: status.type,
            action: .open(.registrationRenewal)
        )
    }
    
    // MARK: Section Credit Picpay
    private func instantiateCreditPicpay(account: CPAccount) -> [SettingsRow] {
        var rows: [SettingsRow] = []

        if settingsService.isCardCreditSettingsEnabled(for: account) {
            rows.append(SettingsRow(title: .rowCreditHome, action: .open(.creditHome(account))))
            rows.append(SettingsRow(title: .rowCreditInvoice, action: .open(.creditInvoices(account))))
            
            rows.append(SettingsRow(title: .rowCreditLimit, subtitle: account.selectedCredit.stringAmount, action: .open(.creditLimit)))
            rows.append(SettingsRow(title: .rowCreditBestDay, subtitle: "Dia \(account.dueDay)", action: .open(.creditBestDay(account))))
        }
        if settingsService.isPhysicalCardEnable(for: account) {
            rows.append(SettingsRow(title: .rowConfigureCard, action: .open(.configureCard)))
        }
        
        if settingsService.isVirtualCardEnable(for: account) && dependencies.featureManager.isActive(.opsVirtualCard) {
            rows.append(SettingsRow(title: .virtualCard, action: .open(.virtualCard(account))))
        }
        
        return rows
    }

    // MARK: Section Pix
    private func instantiatePix() -> [SettingsRow] {
        var rows: [SettingsRow] = []

        if dependencies.featureManager.isActive(.releasePixKeyManagementOnSettingsBool) {
            rows.append(SettingsRow(title: .rowMyKeys, action: .open(.myPixKeys)))
        }
        if dependencies.featureManager.isActive(.isPixDailyLimitAvailable) {
            rows.append(SettingsRow(title: .rowMyDailyLimit, action: .open(.myPixLimit)))
        }
        return rows
    }
    
    // MARK: Section Promotion
    private func instantiatePromotion() -> [SettingsRow] {
        var rows: [SettingsRow] = []
                
        if dependencies.featureManager.isActive(.featureShowStudentAccountPromotion) {
            rows.append(SettingsRow(title: .rowStudentAccount, action: .open(.studentAccount)))
        }
        rows.append(SettingsRow(title: .rowPromotionalCode, action: .open(.promotionalCode)))
        rows.append(SettingsRow(title: .rowInvateYourFriends, action: .open(.invateYourFriends)))
        
        if dependencies.featureManager.isActive(.mGB) {
            rows.append(SettingsRow(title: .rowRecommendStores, action: .open(.recommendStores)))
        }
        
        return rows
    }
    
    // MARK: Section Security
    private func instantiateSecurity() -> [SettingsRow] {
        var rows: [SettingsRow] = []
        
        if identityVerificationEnabled, let row = instantiateIdentityVerification() {
           rows.append(row)
        }
        
        rows.append(SettingsRow(title: .rowChangePassword, action: .actionSheet(.changePassword)))
        
        if settingsService.shouldShowAppProtectionRow() {
            rows.append(SettingsRow(title: .rowProtectAccess, cell: .normal, action: .open(.protectAccess)))
        }
        return rows
    }
    
    // MARK: Section Subscriptions
    private func instantiateSubscriptions() -> [SettingsRow] {
        var rows: [SettingsRow] = []
        rows.append(SettingsRow(title: .rowMySignatures, action: .open(.mySignatures)))
        
        return rows
    }
    
    // MARK: Section PRO
    private func instantiatePRO() -> [SettingsRow] {
        var rows: [SettingsRow] = []
        rows.append(SettingsRow(title: .rowFreeParcelling, action: .open(.noFreeParcelling)))
        rows.append(SettingsRow(title: .rowLeavePicPayPro, action: .open(.leavePicPayPro)))
        
        return rows
    }
    
    // MARK: Section Business
    private func instantiateBussiness() -> [SettingsRow] {
        var rows: [SettingsRow] = []
        guard let consumer = worker.consumer else {
            return rows
        }
        
        if !consumer.businessAccount {
            rows.append(SettingsRow(title: .rowPicPayPro, action: .open(.picpayPRO)))
        }
        
        rows.append(SettingsRow(title: .rowForEstablishment, action: .open(.forEstablishment)))
        
        // Subscriptions information for user
        if dependencies.featureManager.isActive(.subscriptionInformationsInSettings) {
            rows.append(SettingsRow(title: .rowSaleSubscriptions, action: .open(.saleSubscriptions)))
        }
        
        return rows
    }
        
    // MARK: Section Settings
    private func instantiateSettings() -> [SettingsRow] {
        var rows: [SettingsRow] = []
        let typeBiometric = settingsService.biometricType()
        
        if typeBiometric != .none {
            let text: SettingsStrings = typeBiometric == .touchID ? .rowTouchId : .rowFaceId
            rows.append(SettingsRow(title: text, cell: .cellSwitch, action: .switchButton(.biometric)))
        }
        
        rows.append(SettingsRow(title: .rowPrivacity, action: .open(.privacity)))
        rows.append(SettingsRow(title: .rowNotifications, action: .open(.notifications)))
        
        if dependencies.featureManager.isActive(.tPS) {
            rows.append(SettingsRow(title: .rowLinkedAccounts, action: .open(.linkedAccounts)))
        }
        
        if FeatureFlagControlHelper.isEnabled {
            rows.append(SettingsRow(title: .rowAnalytics, cell: .cellSwitch, action: .switchButton(.analytics)))
        }
        
        return rows
    }
    
    // MARK: Section General
    private func instantiateGeneral() -> [SettingsRow] {
        var rows: [SettingsRow] = []
        
        if dependencies.featureManager.isActive(.featureJoinBeta) {
            rows.append(SettingsRow(title: .rowBeta, action: .open(.beta)))
        }
        
        rows.append(SettingsRow(title: .rowHelp, action: .open(.help)))
        rows.append(SettingsRow(title: .rowAbout, action: .actionSheet(.about)))
        rows.append(SettingsRow(title: .rowDeactivateAccount, action: .open(.deactivateAccount)))
        
        return rows
    }
    
    // MARK: Section Logout
    private func instantiateLogout() -> [SettingsRow] {
        var rows: [SettingsRow] = []
        rows.append(SettingsRow(title: .rowLogout, cell: .logout, action: .actionSheet(.logout)))
        
        return rows
    }
    
    // MARK: Section FeatureFlagControl
    #if DEBUG
    private func instantiateFeatureFlagControl() -> [SettingsRow] {
        [SettingsRow(title: .featureFlagControl, cell: .logout, action: .open(.featureFlagControl))]
    }
    #endif

    // MARK: Section Loan
    private func instantiateLoan() -> [SettingsRow] {
        var rows: [SettingsRow] = []
        
        if dependencies.featureManager.isActive(.opsLoanInstallmentsStatus) {
            rows.append(SettingsRow(title: .rowLoanAgreements, action: .open(.loanAgreements)))
        }
        
        if dependencies.featureManager.isActive(.featureLoanConstracts) {
            rows.append(SettingsRow(title: .rowLoanContracts, action: .open(.loanAgreementsTrack)))
        }
        
        rows.append(SettingsRow(title: .rowOriginalInfo, action: .open(.loanInfo)))
        
        return rows
    }
    
    private func p2plendingRows(rowSettings: P2PLendingRowsSettings) -> [SettingsRow] {
        var rows: [SettingsRow] = []
        if rowSettings.myOffers {
            rows.append(SettingsRow(title: .rowMyProposals, action: .open(.p2plendingProposals)))
        }
        if rowSettings.myInvestments {
            rows.append(SettingsRow(title: .rowMyInvestments, action: .open(.p2plendingInvestiments)))
        }
        return rows
    }
    
    func loadAccountOfCreditPicpay(onCacheLoaded: @escaping () -> Void, completion: @escaping () -> Void) {
        let api = CreditPicpayApi()
        api.account(onCacheLoaded: { [weak self] result in
            self?.accountOfCreditPicPay = result
            onCacheLoaded()
        }, completion: { [weak self] result in
            switch result {
            case .success(let account):
                self?.accountOfCreditPicPay = account
                completion()
            case .failure:
                completion()
            }
        })
    }
    
    func loadIdentityVerificationStatus(_ completion: @escaping ((Error?) -> Void)) {
        identityWorker.status { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let status):
                    self?.identityVerificationStatus = status
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }
    
    func loadUpgradeAccountStatus(onCacheLoaded: @escaping () -> Void, onSucess: @escaping () -> Void, onError: ((PicPayErrorDisplayable) -> Void)? = nil) {
        settingsService.statusUpgradeAccount(onCacheLoaded: { [weak self] status in
            self?.upgradeAccountStatus = status
            onCacheLoaded()
        }, completion: { [weak self] result in
            switch result {
            case .success(let status):
                self?.upgradeAccountStatus = status
                onSucess()
            case .failure(let error):
                onError?(error)
            }
        })
    }
    
    func loadLoanSettings(loanLoaded: @escaping () -> Void) {
        settingsService.loadLoanInfoSettings { [weak self] result in
            guard
                let self = self,
                self.settingsService.isLoanSettingsEnabled,
                case let .success(loanSettings) = result,
                loanSettings.showMenu
                else {
                    return
            }
            self.isLoanSettingsActive = true
            loanLoaded()
        }
    }
    
    func loadRegistrationRenewalStatus(completion: @escaping () -> Void) {
        settingsService.loadRegistrationRenewalSettings { [weak self] result in
            if case let .success(status) = result {
                self?.registrationRenewalSettings = status
            }
            completion()
        }
    }
    
    func fetchP2PLendingSectionSettings(completion: @escaping () -> Void) {
        guard dependencies.featureManager.isActive(.releaseP2PLending) else {
            return
        }
        settingsService.fetchP2PLendingSectionSettings { [weak self] result in
            guard case let .success(settings) = result else {
                return
            }
            self?.p2plendingSectionSettings = settings
            completion()
        }
    }
    
    func checkStudentAccountStatus() {
        isStudentAccount = settingsService.checkStudentAccountStatus()
    }
    
    func numberOfSections() -> Int {
        return model.count
    }
    
    func  numberOfRows(section: Int) -> Int {
        return model[section].rows.count
    }
    
    func trackingSelectedItem(action: SettingsRow.TypeAction) {
        switch action {
        case .creditHome:
            dependencies.analytics.log(CardEvent.didTapAppSettingsCardSession(action: .home))
        case .configureCard:
            dependencies.analytics.log(CardEvent.didTapAppSettingsCardSession(action: .settings))
        default:
            return
        }
    }
}
