import Foundation

enum SettingsStrings: String {
    // MARK: Section
    case sectionStudentAccount = "CONTA UNIVERSITÁRIA"
    case sectionAccount = "MINHA CONTA"
    case sectionCreditPicpay
    case sectionPromotion = "PROMOÇÕES"
    case sectionSubscriptions = "MINHAS ASSINATURAS"
    case sectionPro = "PICPAY PRO"
    case sectionBusiness = "PARA O MEU NEGÓCIO"
    case sectionSettings = "CONFIGURAÇÕES"
    case sectionGeneral = "GERAL"
    case sectionLogout = ""
    case sectionLoan = "EMPRÉSTIMO PESSOAL"
    case sectionP2PLending = "EMPRÉSTIMO ENTRE PESSOAS"
    case sectionSecurity = "SEGURANÇA"
    case sectionPix = "PIX"
    
    // MARK: Section StudentAccount
    case rowBenefits = "Benefícios"
    
    // MARK: Section Account
    case rowMyPicPay = "Meu PicPay"
    case rowMyNumber = "Meu número"
    case rowMyEmail = "Meu e-mail"
    case rowPersonalData = "Dados pessoais"
    case rowBankAccount = "Conta bancária"
    case incomeStatements = "Informe de Rendimentos"
    case rowTaxAndLimits = "Tarifas e taxas"
    case rowMyAddress = "Meus endereços"
    case rowLinkedAccounts = "Contas Vinculadas"
    case rowMyFavorites = "Meus Favoritos"
    case rowUpgrade = "Upgrade de conta"
    case rowRegistrationRenewal = "Atualizar cadastro"
    case rowMyKeys = "Minhas chaves"
    case rowMyDailyLimit = "Meu limite diário"
    
    // MARK: Section Credit Picpay
    case rowCreditHome
    case rowCreditCardDigital = "Cartão digital"
    case rowCreditInvoice = "Minhas faturas"
    case rowCreditLimit = "Meu limite"
    case rowCreditBestDay = "Vencimento da fatura"
    case rowConfigureCard = "Configuração do cartão"
    case virtualCard = "Meu cartão virtual"
    
    // MARK: Section Promotion
    case rowStudentAccount = "Conta universitária"
    case rowPromotionalCode = "Usar código promocional"
    case rowInvateYourFriends = "Convide seus amigos"
    case rowRecommendStores = "Ganhe dinheiro indicando lojas"
    
    // MARK: Section Subscriptions
    case rowMySignatures = "Ver minhas assinaturas"
    
    // MARK: Section PRO
    case rowFreeParcelling = "Parcelamento de vendas"
    case rowLeavePicPayPro = "Deixar de ser PicPay Pro"
    
    // MARK: Section Business
    case rowPicPayPro = "PicPay PRO"
    case rowForEstablishment = "Para estabelecimentos"
    case rowSaleSubscriptions = "Venda por assinaturas"
        
    // MARK: Section Settings
    case rowTouchId = "Usar Touch ID para pagar"
    case rowFaceId = "Usar Face ID para pagar"
    case rowPrivacity = "Privacidade"
    case rowNotifications = "Notificações"
    case rowAnalytics = "Tracking de Eventos"
    
    // MARK: Section General
    case rowBeta = "Seja um Beta Tester"
    case rowHelp = "Precisa de ajuda?"
    case rowAbout = "Sobre o PicPay"
    case rowDeactivateAccount = "Desativar minha conta"
    
    // MARK: Section Logout
    case rowLogout = "Sair da conta"
    
    // MARK: Master Switch - Feature Flags
    case featureFlagControl = "🛠 Feature Flag Control 🛠"
    
    // MARK: Section Loan
    case rowOriginalInfo = "Ajuda"
    case rowLoanAgreements = "Meus Empréstimos"
    case rowLoanConditions = "Condições"
    case rowLoanContracts = "Contratos"
    
    // MARK: P2P Lending Section
    case rowMyProposals = "Meus pedidos"
    case rowMyInvestments = "Meus investimentos"
    
    // MARK: Section Security
    case rowProtectAccess = "Proteger acesso"
    case rowIdentity = "Validar identidade"
    case rowChangePassword = "Alterar senha"
    
    var text: String {
        switch self {
        case .sectionCreditPicpay:
            return CreditPicPayLocalizable.featureName.text
        case .rowCreditHome:
            return "Meu \(CreditPicPayLocalizable.featureName.text)"
        default:
            return self.rawValue
        }
    }
}
