import UIKit
import UI
import CropViewController

protocol SettingsHeaderViewControllerDelegate: AnyObject {
    func didTapProfile()
}

final class SettingsHeaderViewController: UIViewController {
    private lazy var studentBadge: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isHidden = true
        imageView.image = #imageLiteral(resourceName: "icon-cap-green")
        view.addSubview(imageView)
        imageView.widthAnchor.constraint(equalToConstant: 26).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 26).isActive = true
        imageView.trailingAnchor.constraint(equalTo: profileImage.trailingAnchor, constant: -7).isActive = true
        imageView.bottomAnchor.constraint(equalTo: profileImage.bottomAnchor, constant: 6).isActive = true
        return imageView
    }()
    
    private let viewModel: SettingsHeaderViewModelProtocol
    weak var delegate: SettingsHeaderViewControllerDelegate?
    
    @IBOutlet weak private var usernameLabel: UILabel!
    @IBOutlet weak private var fullnameLabel: UILabel!
    @IBOutlet weak private var profileImage: UIPPProfileImage!
    @IBOutlet weak private var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak private var perfilView: UIView!
    @IBOutlet weak private var bankInfoView: SettingsHeaderBankInfoView?
    
    init(with viewModel: SettingsHeaderViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: "SettingsHeaderViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bankInfoView?.delegate = self
        setupView()
        setupGesture()
        setupBankInfo()
    }
    
    func reloadHeader(_ studentAccount: Bool = false) {
        setupLabel()
        studentBadge.isHidden = !studentAccount
    }
    
    private func setupView() {
        setupLabel()
        profileImage.setContact(viewModel.contact)
        
        view.backgroundColor = Colors.groupedBackgroundPrimary.color
        activityIndicatorView.tintColor = Colors.brandingBase.color
        activityIndicatorView.color = Colors.brandingBase.color
        
        if #available(iOS 13.0, *) {
            activityIndicatorView.style = .large
        } else {
            activityIndicatorView.style = .gray
        }
        
        activityIndicatorView.isHidden = true
    }
    
    private func setupLabel() {
        usernameLabel.text = viewModel.username
        fullnameLabel.text = viewModel.fullname
    }
    
    private func setupGesture() {
        let gesturePerfil = UITapGestureRecognizer(target: self, action: #selector(tapProfile))
        let gestureImage = UITapGestureRecognizer(target: self, action: #selector(tapImage))
        
        perfilView.addGestureRecognizer(gesturePerfil)
        profileImage.addGestureRecognizer(gestureImage)
    }
    
    private func setupBankInfo() {
        guard viewModel.shouldDisplayBankInfo else { return }
        bankInfoView?.isHidden = false
        loadBankInfo()
    }
    
    private func loadBankInfo() {
        bankInfoView?.display(state: .loading)
        viewModel.fetchBankInfo() { [weak self] result in
            switch result {
            case let .success(bankInfo):
                self?.bankInfoView?.display(state: .loaded(info: bankInfo))
                
            case .failure:
                self?.bankInfoView?.display(state: .tryAgain)
            }
        }
    }
    
    private func changePhoto() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let library = UIAlertAction(title: SettingsLocalizable.chooseFromLibrary.text, style: .default, handler: { _ in
            self.openImagePicker(sourceType: .photoLibrary)
        })
        
        let takePicture = UIAlertAction(title: SettingsLocalizable.takePicture.text, style: .default, handler: { _ in
            self.openImagePicker(sourceType: .camera)
        })
        
        let cancel = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil)
        
        alert.addAction(library)
        alert.addAction(takePicture)
        alert.addAction(cancel)
        
        present(alert, animated: true)
    }
    
    private func openImagePicker(sourceType: UIImagePickerController.SourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        if sourceType == .camera && UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.cameraDevice = .front
            imagePicker.checkCameraPermission(cameraActionDescription: SettingsLocalizable.takePictures.text) { [weak self] () -> Void in
                self?.present(imagePicker, animated: true)
            }
        } else {
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true)
        }
    }
    
    @objc
    private func tapProfile(_ sender: UITapGestureRecognizer) {
        delegate?.didTapProfile()
    }
    
    @objc
    private func tapImage(_ sender: UITapGestureRecognizer) {
        changePhoto()
    }
}

extension SettingsHeaderViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        guard let originalImage = info[.originalImage] as? UIImage else {
            return
        }
        
        let cropStyle = TOCropViewCroppingStyle.circular
        let cropViewController = TOCropViewController(croppingStyle: cropStyle, image: originalImage)
        cropViewController.delegate = self
        picker.present(cropViewController, animated: true)
    }
    
    private func setProfileImageUploading(_ newProfileImage: UIImage) {
        profileImage.imageView?.alpha = 0.5
        profileImage.imageView?.image = newProfileImage
    }
    
    private func setNewProfileImage(_ newProfileImage: UIImage) {
        profileImage.imageView?.alpha = 1
        profileImage.imageView?.image = newProfileImage
    }
    
    private func startLoading() {
        activityIndicatorView.isHidden = false
        activityIndicatorView.startAnimating()
    }
    
    private func endLoading() {
        activityIndicatorView.isHidden = true
    }
}

extension SettingsHeaderViewController: TOCropViewControllerDelegate {
    func cropViewController(
        _ cropViewController: TOCropViewController,
        didCropTo image: UIImage,
        with cropRect: CGRect,
        angle: Int
    ) {
        dismiss(animated: true, completion: nil)
        
        guard
            let croppedImage = image.cropSquare(),
            let imageData = viewModel.resizeImage(image: croppedImage) else {
            return
        }
        
        startLoading()
        setProfileImageUploading(image)
        
        viewModel.uploadProfileImage(imageData: imageData, uploadProgress: { [weak self] progress in
            self?.profileImage.imageView?.alpha = (progress / 200) + 0.5
        }, onSuccess: { [weak self] _ in
            self?.activityIndicatorView.stopAnimating()
            self?.profileImage.imageView?.alpha = 1
        }, onError: { [weak self] error in
            guard let strongSelf = self else {
                return
            }

            self?.endLoading()
            
            AlertMessage.showAlert(error, controller: strongSelf)
            strongSelf.setNewProfileImage(image)
        })
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
       dismiss(animated: true, completion: nil)
    }
}

extension SettingsHeaderViewController: SettingsHeaderBankInfoDelegate {
    func tryAgain() {
        loadBankInfo()
    }
}
