import SecurityModule
import UI
import UIKit

protocol SettingsViewControllerDelegate: AnyObject {
    func didTapOpen(action: SettingsRow.TypeAction, account: CPAccount?)
    func didTapForgotPassword()
    func didTapChangePassword()
    func didTapAboutPolicies()
    func didTapAboutTerms()
    func didTapHelp()
    func didTaplogout()
    func didTapOmbudsman()
    func didTapProfile()
}

final class SettingsViewController: PPBaseViewController {
    private enum CellIdentifier: String {
        case normal = "SettingsTableViewCell"
        case cellSwitch = "SettingsSwitchTableViewCell"
    }

    private lazy var footerView: SettingsFooterView = {
        let text = viewModel.appVersion
        return SettingsFooterView(frame: .zero, titleLabel: text)
    }()
    
    private lazy var helpButton: HelpItemSettings = {
        let button = HelpItemSettings(frame: .zero)
        return button
    }()
    
    private lazy var headerViewController: SettingsHeaderViewController = {
        let viewModel = SettingsHeaderViewModel(dependencies: DependencyContainer())
        let vc = SettingsHeaderViewController(with: viewModel)
        vc.delegate = self
        return vc
    }()
    
    private let viewModel: SettingsViewModel
    private let auth = PPAuth()
    var delegate: SettingsViewControllerDelegate?
    
    var helpshitUnreadNotificationObserver: NSObjectProtocol?

    @IBOutlet weak private var tableView: UITableView!
    
    init(with: SettingsViewModel) {
        viewModel = with
        super.init(nibName: SettingsViewController.nibName, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        if let notificationObserver = helpshitUnreadNotificationObserver {
            NotificationCenter.default.removeObserver(notificationObserver)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        updateSettings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(false, animated: false)
        
        let bar = self.navigationController?.navigationBar
        bar?.tintColor = Colors.grayscale700.color
        bar?.barTintColor = Colors.grayscale050.color
        bar?.titleTextAttributes = [.foregroundColor: Colors.grayscale700.color]
        if #available(iOS 11, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        PPAnalytics.trackFirstTimeOnlyEvent("FT Ajustes", properties: ["breadcrumb": BreadcrumbManager.shared.breadcrumb])
        loadUpgradeAccount()
        loadLoanSettings()
        loadRenewalStatus()
        fetchP2PLendingSectionSettings()
        if viewModel.creditPicPayEnabled {
            loadCreditPicpay()
        }
        
        if viewModel.identityVerificationEnabled {
            loadIdentityVerification()
        }
        
        checkStudentAccountStatus()
    }
    
    public func loadCreditPicpay() {
        viewModel.loadAccountOfCreditPicpay(onCacheLoaded: { [weak self] in
            self?.updateSettings()
        }, completion: { [weak self] in
            self?.updateSettings()
        })
    }
    
    private func loadIdentityVerification() {
        viewModel.loadIdentityVerificationStatus { [weak self] _ in
            self?.updateSettings()
        }
    }
    
    private func fetchP2PLendingSectionSettings() {
        viewModel.fetchP2PLendingSectionSettings(completion: updateSettings)
    }
    
    private func checkStudentAccountStatus() {
        viewModel.checkStudentAccountStatus()
        updateSettings()
    }
    
    private func loadUpgradeAccount() {
        viewModel.loadUpgradeAccountStatus(onCacheLoaded: { [weak self] in
            self?.updateSettings()
        }, onSucess: { [weak self] in
            self?.updateSettings()
        })
    }
    
    private func loadLoanSettings() {
        viewModel.loadLoanSettings { [weak self] in
            self?.updateSettings()
        }
    }
    
    private func loadRenewalStatus() {
        viewModel.loadRegistrationRenewalStatus { [weak self] in
            self?.updateSettings()
        }
    }
    
    private func setupView() {
        configureView()
        configureTableView()
        registerObserver()
        createHeader()
        createFooter()
    }
    
    // MARK: - Configurations
    
    private func configureView() {
        title = SettingsLocalizable.title.text
        auth.delegate = self
        configureHelpButton()
    }
    
    private func configureHelpButton() {
        helpButton.addTarget(self, action: #selector(tapHelp), for: .touchUpInside)
        let barButtonItem = UIBarButtonItem(customView: helpButton)
        navigationItem.rightBarButtonItem = barButtonItem
    }
    
    private func configureTableView() {
        tableView.accessibilityIdentifier = "configTableView"
        tableView.backgroundColor = Colors.groupedBackgroundPrimary.color
        tableView.register(SettingsTableViewCell.nib, forCellReuseIdentifier: CellIdentifier.normal.rawValue)
        tableView.register(SettingsSwitchTableViewCell.nib, forCellReuseIdentifier: CellIdentifier.cellSwitch.rawValue)
    }
    
    private func createHeader() {
        let headerView = UIView(frame: headerViewController.view.frame)
        
        addChild(headerViewController)
        headerView.addSubview(headerViewController.view)
        headerViewController.didMove(toParent: self)
        
        tableView.tableHeaderView = headerView
    }
    
    private func createFooter() {
        tableView.tableFooterView = footerView
    }
    
    private func registerObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateEmailStatus), name: Notification.Name.EmailStatus.updated, object: nil)
    }
    
    private func updateSettings() {
        headerViewController.reloadHeader(viewModel.isStudentAccount)
        viewModel.instantiateSections()
        tableView.reloadData()
    }
    
    @objc
    private func updateEmailStatus() {
        updateSettings()
    }
    
    @objc
    private func tapHelp() {
        delegate?.didTapHelp()
    }
    
    // MARK: - ActionSheet
    
    private func openActionSheet(action: SettingsRow.TypeAction) {
        switch action {
        case .logout:
            logout()
        case .changePassword:
            changePassword()
        case .about:
            aboutPicPay()
        default: ()
        }
    }
    
    private func logout() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil)
        let logout = UIAlertAction(title: SettingsLocalizable.logout.text, style: .destructive) { _ in
            self.delegate?.didTaplogout()
        }
        
        alert.addAction(cancel)
        alert.addAction(logout)
        
        present(alert, animated: true)
    }
    
    private func changePassword() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil)
        alert.addAction(cancel)

        if viewModel.isPasswordResetActive {
            let forgot = UIAlertAction(title: SettingsLocalizable.forgotPassword.text, style: .default) { _ in
                self.delegate?.didTapForgotPassword()
            }
            alert.addAction(forgot)
        }

        let change = UIAlertAction(title: SettingsLocalizable.redefinePassword.text, style: .default, handler: { _ in
            self.delegate?.didTapChangePassword()
        })
        alert.addAction(change)

        present(alert, animated: true)
    }
    
    private func aboutPicPay() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil)
        let politics = UIAlertAction(title: SettingsLocalizable.privacyPolicy.text, style: .default, handler: { _ in
            self.delegate?.didTapAboutPolicies()
        })
        let terms = UIAlertAction(title: SettingsLocalizable.termsOfUse.text, style: .default, handler: { _ in
            self.delegate?.didTapAboutTerms()
        })
        let ombudsman = UIAlertAction(title: SettingsLocalizable.ombudsman.text, style: .default, handler: { _ in
            self.delegate?.didTapOmbudsman()
        })
        
        alert.addAction(cancel)
        alert.addAction(politics)
        alert.addAction(terms)
        alert.addAction(ombudsman)
        
        present(alert, animated: true)
    }
}

extension SettingsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.title(section: section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        
        let title = viewModel.title(section: section, index: row)
        let desc = viewModel.subtitle(section: section, index: row)
        let type = viewModel.type(section: section, index: row)
        
        switch type {
        case .normal, .pending, .logout, .alert, .analysis, .new:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.normal.rawValue, for: indexPath) as? SettingsTableViewCell else {
                return UITableViewCell()
            }
            cell.accessibilityLabel = title
            cell.configureCell(title: title, desc: desc, type: type)
            return cell
        case .cellSwitch:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.cellSwitch.rawValue, for: indexPath) as? SettingsSwitchTableViewCell else {
                return UITableViewCell()
            }
            return setupCellSwitch(cell: cell, indexPath: indexPath, title: title)
        }
    }
    
    private func setupCellSwitch(cell: SettingsSwitchTableViewCell, indexPath: IndexPath, title: String) -> SettingsSwitchTableViewCell {
        cell.configureCell(title: title, isActivated: cellSwitchIsActivated(from: indexPath)) { [weak self] isActivated in
            self?.didChangeSwitchCell(indexPath: indexPath, isActivated: isActivated)
        }
        return cell
    }
    
    private func cellSwitchIsActivated(from indexPath: IndexPath) -> Bool {
        guard case .switchButton(let action) = viewModel.action(section: indexPath.section, index: indexPath.row) else {
            return false
        }

        switch action {
        case .biometric:
            return PPAuth.useTouchId()
        case .analytics:
            return (tabBarController as? MainTabBarController)?.isPanelAnalyticsVisible ?? false
        default:
            return false
        }
    }
    
    private func didChangeSwitchCell(indexPath: IndexPath, isActivated: Bool) {
        guard case .switchButton(let action) = viewModel.action(section: indexPath.section, index: indexPath.row) else {
            return
        }

        switch action {
        case .biometric:
            if isActivated {
                auth.enableBiometricAuthentication()
            } else {
                auth.disableBiometricAuthentication()
            }
        case .analytics:
            (tabBarController as? MainTabBarController)?.updatePanelAnalytics(isActivated)
        default:
            break
        }
    }
}

extension SettingsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        let row = indexPath.row
        let action = viewModel.action(section: section, index: row)
        
        switch action {
        case .open(let screen):
            viewModel.trackingSelectedItem(action: screen)
            delegate?.didTapOpen(action: screen, account: viewModel.accountOfCreditPicPay)
        case .actionSheet(let action):
            openActionSheet(action: action)
        default: ()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension SettingsViewController: PPAuthDelegate {
    func biometricAuthenticationStatus(_ enabled: Bool) {
        updateSettings()
    }
}

extension SettingsViewController: SettingsHeaderViewControllerDelegate {
    func didTapProfile() {
        delegate?.didTapProfile()
    }
}

extension SettingsViewController: StyledNavigationDisplayable {
    var navigationBarStyle: DefaultNavigationStyle {
        StandardNavigationStyle()
    }
}
