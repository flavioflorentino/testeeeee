import UI
import WebKit
import Core

final class SettingsWebViewController: PPBaseViewController {
    var rightButtonAction: () -> Void = { }
    
    private lazy var webView: WKWebView = {
        let webView = WKWebView()
        webView.navigationDelegate = self
        webView.translatesAutoresizingMaskIntoConstraints = false
        return webView
    }()
    
    private lazy var loadingLineView: LoadingTableViewLine = {
        let loadingLine = LoadingTableViewLine(width: view.frame.width)
        loadingLine.translatesAutoresizingMaskIntoConstraints = false
        return loadingLine
    }()
    
    private let titleWeb: String
    private let url: URL
    
    var urlLoaded: String {
        return url.absoluteString
    }
    
    init?(url: String, title: String) {
        guard let url = URL(string: url) else {
            return nil
        }
        self.url = url
        self.titleWeb = title
        
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        addComponents()
        layoutComponents()
        loadWebView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale100.color
    }
    
    private func setupView() {
        title = titleWeb
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale000.color
    }
    
    private func loadWebView() {
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        startLoadingIndicator()
    }
    
    private func addComponents() {
        view.addSubview(webView)
        view.addSubview(loadingLineView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.constraintAllEdges(from: webView, to: view)
        
        NSLayoutConstraint.activate([
            loadingLineView.topAnchor.constraint(equalTo: view.topAnchor),
            loadingLineView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            loadingLineView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            loadingLineView.heightAnchor.constraint(equalToConstant: 3)
        ])
    }
    
    private func startLoadingIndicator() {
        loadingLineView.startAnimating()
    }
    
    private func stopLoadingIndicator() {
        loadingLineView.isHidden = true
    }
    
    func setupRightBarButton(_ title: String) {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: title,
            style: .plain,
            target: self,
            action: #selector(rightBarButtonAction)
        )
    }
    
    @objc
    private func rightBarButtonAction() {
        rightButtonAction()
    }
}

extension SettingsWebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) { // swiftlint:disable:this implicitly_unwrapped_optional
        stopLoadingIndicator()
    }
    
    func webView(
        _ webView: WKWebView,
        decidePolicyFor navigationAction: WKNavigationAction,
        decisionHandler: @escaping (WKNavigationActionPolicy) -> Void
    ) {
        guard let url = navigationAction.request.url, DeeplinkHelper.handleDeeplink(withUrl: url, from: self) else {
            decisionHandler(.allow)
            return
        }
        decisionHandler(.cancel)
    }
        
    func webView(
        _ webView: WKWebView,
        didReceive challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    ) {
        let pinningHandler = PinningHandler()
        pinningHandler.handle(challenge: challenge, completionHandler: completionHandler)
    }
}
