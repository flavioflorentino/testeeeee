import Foundation

enum SettingsLocalizable: String, Localizable {
    // NotificationSettings
    case newLikes
    case accountLimitsTitle
    case newComments
    case newFollowers
    case newPayments
    case birthdaysFromWhoIFollow
    case newsAndPromotions
    case receiveNotificationsOnPhone
    case receiveNotificationsByEmail
    case buttonTitleUpgrade
    case feesAndLimitsTitle
    case sendChargeToConsumerPush
    case directMessageSendBirdPush
    case directMessageSendBirdPushTurningOnError
    case directMessageSendBirdPushTurningOffError
    
    // PromoCode
    case termsAndConditions
    case codeUsedWithSuccess
    
    // PrivacySettings
    case privacySettingsTitle
    case openedAccount
    case openedAccountDescription
    case receiveInPublic
    case receiveInPublicDescription
    case payInPublic
    case payInPublicDescription
    case notifyBirthday
    case notifyBirthdayDescription
    case paymentRequest
    case paymentRequestDescription
    case contactsPermission
    case contactsPermissionDescription
    case locationPermission
    case locationPermissionDescription
    case permissionActivated
    case permissionDeactivated
    
    // Settings
    case title
    case logout
    case forgotPassword
    case redefinePassword
    case privacyPolicy
    case termsOfUse
    case ombudsman
    case takePicture
    case chooseFromLibrary
    case takePictures
    case changePassword
    case newPasswordsAreDifferent
    case passwordChangedSuccessfully
    case seeDetails
    case yourAccount
    case stopBeingPicPayPro
    case regulation
    case yourPicPay
    case changeUsername
    case doYouReallyWantChangeUsername
    case new
    case day
    
    case needHelp
    case phoneUrl
    case alertMessage
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .settings
    }
}
