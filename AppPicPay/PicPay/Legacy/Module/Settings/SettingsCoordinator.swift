import AnalyticsModule
import Card
import Core
import CustomerSupport
import FeatureFlag
import IncomeTaxReturn
import Loan
import PIX
import UI
import UIKit
import Registration
import PasswordReset
import P2PLending

final class SettingsCoordinator: Coordinator {
    typealias Dependencies = HasAnalytics & HasFeatureManager & HasConsumerManager
    
    private var navigationController: UINavigationController
    private let dependencies: Dependencies
    var currentCoordinator: Coordinator?
    
    fileprivate var auth: PPAuth?
    private var childCoordinators: [Coordinating] = []
    private var account: CPAccount?
    weak var settingsViewController: SettingsViewController?
    
    init(navigationController: UINavigationController = StyledNavigationController(),
         dependencies: Dependencies = DependencyContainer()) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }
    
    public var rootViewController: UINavigationController {
        return navigationController
    }
    
    func start() {
        let viewModel = SettingsViewModel()
        let vc = SettingsViewController(with: viewModel)
        settingsViewController = vc
        vc.delegate = self
        settingsViewController?.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(vc, animated: true)
    }
    
    func open(type: SettingsRow.TypeAction, account: CPAccount? = nil) {
        switch type {
        case .creditHome(let account):
            let coordinator = CreditPicPayCoordinator(navigationController: navigationController, account: account)
            currentCoordinator = coordinator
            coordinator.start()
        case .creditInvoices(let account):
            let creditCoordinator = CreditPicPayCoordinator(navigationController: navigationController, account: account)
            creditCoordinator.startInListInvoices()
        case let .creditBestDay(account):
            guard dependencies.featureManager.isActive(.experimentCardDueDayPresentingBool) else {
                let coordinator = CPDueDayCoordinator(with: navigationController, account: account)
                coordinator.start()
                return
            }
            let dueDayController = DueDayFactory.make(delegate: self)
            navigationController.pushViewController(dueDayController, animated: true)
        case .identityVerification:
            let coordinator = IdentityValidationFlowCoordinator(from: navigationController, originFlow: .settings)
            currentCoordinator = coordinator
            coordinator.start()
        case .upgradeAccount:
            let coordinator = UpgradeCoordinator(navigationController: rootViewController)
            coordinator.start()
            currentCoordinator = coordinator
        case .registrationRenewal:
            let dependencies = RegistrationRenewalDependencies(
                addressSelector: AddressListRenewalProxy(),
                authManager: AuthProxy(),
                origin: .settings
            )
            let coordinator = RegistrationRenewalFlowCoordinator(with: navigationController, dependencies: dependencies)
            coordinator.start()
            childCoordinators = [coordinator]
        case .taxAndLimits:
         openTaxAndLimits()
        case .myFavorites:
            let coordinator = FavoriteFlowCoordinator(navigationController: navigationController)
            childCoordinators.append(coordinator)
            coordinator.goToFavoritesList()
            dependencies.analytics.log(FavoritesEvent.favoritesListAccessed(.settings))
        case .help:
            let helpCenterController = FeatureManager.isActive(.featureHelpCenterNewExperienceBool) ? HelpCenterFactory.make() : FAQFactory.make()
            let navigationHelpCenterController = UINavigationController(rootViewController: helpCenterController)
            navigationController.present(navigationHelpCenterController, animated: true)

        case .creditLimit:
            didOpenLimitCard(from: navigationController) { [weak self] in
                self?.settingsViewController?.loadCreditPicpay()
            }
        case .virtualCard(let account):
            let picpayCardType: PicPayCardType = account.creditStatus == .active ? .multiple : .debit
            let settingsButtons = CardSettingsButtons(
                cardReplacement: account.settings.replacementButton,
                virtualCard: account.settings.virtualCardButton
            )
            let settingsData = CardSettingsData(cardType: picpayCardType, buttons: settingsButtons)
            didOpenVirtualCard(from: navigationController, settingsData: settingsData)
        case .configureCard:
            self.account = account
            let coordinator = CardSettingsFlowCoordinator(navigationController: navigationController)
            childCoordinators.append(coordinator)
            coordinator.delegate = self
            coordinator.start()
        case .loanAgreements:
            startLoanAgreements()
            
        case .loanAgreementsTrack:
            startLoanAgreementsTrack()
            
        case .loanInfo:
            startLoanFAQ()
        case .invateYourFriends:
            if dependencies.featureManager.isActive(.featureMgmIndicationCenter) {
                let controller = ReferralCodeFactory.make()
                navigationController.present(UINavigationController(rootViewController: controller), animated: true)
            } else {
                guard let controller = ViewsManager.peopleSearchStoryboardViewController("SocialShareCode") else {
                    return
                }
                
                pushViewController(controller: controller)
            }
        case .p2plendingProposals:
            let controller = LendingProposalListFactory.make(for: .borrower)
            navigationController.show(controller, sender: nil)
        case .p2plendingInvestiments:
            let controller = LendingProposalListFactory.make(for: .investor)
            navigationController.show(controller, sender: nil)
        case .myPixKeys:
            PIXConsumerFlowCoordinator(
                originViewController: navigationController,
                originFlow: .settings,
                destination: .keyManagement
            ).start(isModalPresentation: false)
        case .myPixLimit:
            let controller = DailyLimitFactory.makePF()
            pushViewController(controller: controller)
        case .protectAccess:
            let controller = AppProtectionFactory.make()
            pushViewController(controller: controller)

        default:
            guard let controller = instantiateViewController(transition: type) else {
                return
            }
            pushViewController(controller: controller)
        }
    }
    
    func instantiateViewController(transition: SettingsRow.TypeAction) -> UIViewController? {
        switch transition {
        case .myPicPay:
            let viewModel = SettingsUsernameViewModel()
            let controller = SettingsUsernameViewController(with: viewModel)
            controller.delegate = self
            return controller
            
        case .myNumber:
            return ChangePhoneNumberFactory.make()
            
        case .myNumberCheck:
            return MyPhoneNumberFactory.make()
            
        case .myEmail:
            return MailFactory.make()
            
        case .personalData:
            return PersonalDetailsFactory.make()
            
        case .bankAccount:
            return RegisterBankAccountSplashFactory.make(hidesBottomBarWhenPushed: true)
            
        case .incomeStatements:
            dependencies.analytics.log(IncomeStatementsExternalEvent.openingButtonClicked)
            return IncomeStatementsFactory.make()
        
        case .bankAccountCheck(let bankAccount):
            return BankAccountFactory.make(with: [bankAccount], hidesBottomBarWhenPushed: true)
            
        case .myAddress:
            let controller = AddressListViewController()
            return controller
            
        case .linkedAccounts:
            let controller = AccountsListTableViewController.controllerFromStoryboard(.linkedAccounts)
            return controller
            
        case .benefits:
            let coordinator = StudentAccountCoordinator(from: navigationController)
            let benefits = coordinator.benefits()
            let rules = coordinator.rules()
            benefits.setupRightBarButton(StudentLocalizable.titleRules.text)
            benefits.rightButtonAction = { [weak self] in
                self?.dependencies.analytics.log(StudentAccountEvent.rules)
                self?.pushViewController(controller: rules)
            }
            
            KVStore().setBool(true, with: .sawBenefits)
            dependencies.analytics.log(StudentAccountEvent.benefits(from: .settings))
            return benefits
        
        case .promotionalCode:
            return PromoCodeViewController(from: .settings)
            
        case .studentAccount:
            dependencies.analytics.log(SettingsEvent.itemSelected(.studentAccount))
            let urlString = dependencies.featureManager.text(.universityAccountRegisterWebview)
            guard dependencies.featureManager.isActive(.settingsWithUniversityAccountWebview), let url = URL(string: urlString) else {
                return PromoCodeViewController(from: .settings)
            }
            
            dependencies.analytics.log(StudentAccountEvent.view(screen: .offerWebview, origin: .settings))
            return WebViewFactory.make(with: url)
        case .recommendStores:
            guard let controller = ViewsManager.peopleSearchStoryboardViewController("SocialShareCode") as? SocialShareCodeViewController else {
                return nil
            }
            controller.setTypeString(type: "business")
            return controller
            
        case .mySignatures:
            dependencies.analytics.log(SettingsEvent.mySubscriptions)
            let controller = MySubscriptionViewController()
            return controller
            
        case .noFreeParcelling:
            guard let controller = ViewsManager.settingsStoryboardViewController(withIdentifier: "AccountInstallmentsConfig") else {
                return nil
            }
            dependencies.analytics.log(ProEvent.installmentsConfigurationSelected)
            return controller

        case .leavePicPayPro:
            let controller = LeaveProViewController(model: LeaveProViewModel())
            return controller

        case .picpayPRO:
            return BusinessTypeFactory.make()
            
        case .forEstablishment:
            guard let url = URL(string: WebServiceInterface.apiEndpoint(WsWebViewURL.bizAboutWebview.endpoint)) else {
                return nil
            }
            return WebViewFactory.make(with: url, includeHeaders: true)
        
        case .saleSubscriptions:
            guard let url = URL(string: WebServiceInterface.apiEndpoint(WsWebViewURL.aboutSubscriptions.endpoint)) else {
                return nil
            }
            return WebViewFactory.make(with: url, includeHeaders: true)

        case .privacity:
            let vm = PrivacySettingsViewModel(dependencies: dependencies)
            return ToggleSettingsViewController(viewModel: vm)

        case .notifications:
            let vm = NotificationSettingsViewModel()
            return ToggleSettingsViewController(viewModel: vm)
        
        case .beta:
            let link = dependencies.featureManager.text(.featureBetaLink)
            guard let url = URL(string: WebServiceInterface.apiEndpoint(link)) else {
                return nil
            }
            return WebViewFactory.make(with: url, includeHeaders: true)
            
        case .deactivateAccount:
            return DeactivateAccountFactory.make()
            
        case .featureFlagControl:
            return FeatureFlagControlHelper.isEnabled ? FeatureFlagControl.shared.controller() : nil
                        
        default: return nil
        }
    }
    
    func instantiateDelegateViewController(transition: TypeScreen) -> UIViewController? {
        switch transition {
        case .aboutPolicies:
            guard let urlString = WebServiceInterface.apiEndpoint(WsWebViewURL.termsOfUse.endpoint), let url = URL(string: urlString) else {
                return nil
            }
            
            let controller = WebViewFactory.make(with: url)
            controller.title = SettingsLocalizable.termsOfUse.text

            return controller
            
        case .aboutTerms:
            guard let urlString = WebServiceInterface.apiEndpoint(WsWebViewURL.privacyPolicy.endpoint), let url = URL(string: urlString) else {
                return nil
            }
            
            let controller = WebViewFactory.make(with: url)
            controller.title = SettingsLocalizable.privacyPolicy.text

            return controller
            
        case .changePassword:
            return ResetPasswordFactory.make()

        case .profile:
            dependencies.analytics.log(SettingsEvent.itemSelected(.profile))
            guard let consumer = ConsumerManager.shared.consumer else {
                return nil
            }
            let controller = NewProfileProxy.newProfile(consumer: consumer, showRightButton: false)
            return controller
            
        case .ombudsman:
            let url = dependencies.featureManager.text(.ombudsmanUrl)
            guard let controller = SettingsWebViewController(url: url, title: "") else {
                return nil
            }
            return controller
        }
    }
    
    private func pushViewController(controller: UIViewController) {
        controller.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func creditPicpayNavigationController(with viewController: UIViewController) -> PPNavigationController {
        let creditNavigationController = PPNavigationController(rootViewController: viewController)
        creditNavigationController.setUpToCreditPicPay()
        return creditNavigationController
    }
    
    private func didAskForAuthentication(onSuccess: @escaping (String) -> Void) {
        auth = PPAuth.authenticate({ token, _ in
            guard let password = token else {
                return
            }
            onSuccess(password)
        }, canceledByUserBlock: { })
    }
    
    private func startLoanAgreements() {
        let coordinator = LoanCoordinator(with: navigationController, from: .settingsMenu)
        SessionManager.sharedInstance.currentCoordinating = coordinator
        coordinator.start(flow: .loanAgreements) {
            SessionManager.sharedInstance.currentCoordinating = nil
        }
    }
    
    private func startLoanAgreementsTrack() {
        let coordinator = LoanCoordinator(with: navigationController, from: .settingsMenu)
        SessionManager.sharedInstance.currentCoordinating = coordinator
        coordinator.start(flow: .loanAgreementsTrack) {
            SessionManager.sharedInstance.currentCoordinating = nil
        }
    }
    
    private func startLoanFAQ() {
        let urlString = WsWebViewURL.loanFAQ.rawValue
        guard let url = URL(string: urlString) else {
            return
        }
        
        DeeplinkHelper.handleDeeplink(withUrl: url)
    }
    
    private func openTaxAndLimits() {
        if dependencies.featureManager.isActive(.taxAndLimitsWebView) {
            guard let url = URL(string: WsWebViewURL.taxAndLimits.endpoint) else {
                return
            }
            
            let controller = WebViewFactory.make(with: url)
            pushViewController(controller: controller)
        } else {
            let coordinador = FeesAndLimitsCoordinator(navigationController: rootViewController)
            coordinador.start()
            currentCoordinator = coordinador
        }
    }
}

extension SettingsCoordinator: SettingsViewControllerDelegate {
    func didTaplogout() {
        AuthManager.shared.logout(fromContext: rootViewController)
    }
    
    func didTapHelp() {
       open(type: .help)
    }
    
    func didTapOmbudsman() {
        guard let controller = instantiateDelegateViewController(transition: .ombudsman) else {
            return
        }
        pushViewController(controller: controller)
    }
    
    func didTapProfile() {
        guard let controller = instantiateDelegateViewController(transition: .profile) else {
            return
        }
        pushViewController(controller: controller)
    }
    
    func didTapForgotPassword() {
        let passwordResetCoordinator = PasswordResetCoordinator(presenterController: navigationController)
        childCoordinators.append(passwordResetCoordinator)
        passwordResetCoordinator.start()
    }
    
    func didTapChangePassword() {
        guard let controller = instantiateDelegateViewController(transition: .changePassword) else {
            return
        }
        pushViewController(controller: controller)
    }
    
    func didTapAboutPolicies() {
        guard let controller = instantiateDelegateViewController(transition: .aboutTerms) else {
            return
        }
        pushViewController(controller: controller)
    }
    
    func didTapAboutTerms() {
        guard let controller = instantiateDelegateViewController(transition: .aboutPolicies) else {
            return
        }
        pushViewController(controller: controller)
    }
    
    func didTapOpen(action: SettingsRow.TypeAction, account: CPAccount?) {
        open(type: action, account: account)
    }
}

extension SettingsCoordinator: SettingsUsernameViewControllerDelegate {
    func usernameUpdateDidFinish() {
        navigationController.popViewController(animated: true)
    }
}

extension SettingsCoordinator: CardSettingsFlowDelegate {
    func didOpenDueDayCard(from navigationController: UINavigationController) {
        guard dependencies.featureManager.isActive(.experimentCardDueDayPresentingBool) else {
            guard let account = account else { return }
            let coordinator = CPDueDayCoordinator(with: navigationController, account: account)
            coordinator.start()
            return
        }
        let dueDayController = DueDayFactory.make(delegate: self)
        navigationController.pushViewController(dueDayController, animated: true)
    }
    
    func didOpenLimitCard(from navigationController: UINavigationController, didUpdateCardLimit: @escaping () -> Void) {
        guard dependencies.featureManager.isActive(.experimentCardCreditLimitPresentingBool) else {
            let coordinator = CreditLimitCoordinator(from: navigationController)
            currentCoordinator = coordinator
            coordinator.didUpdateCardLimit = didUpdateCardLimit
            coordinator.start()
            return
        }
        let coordinator = LimitAdjustmentFlowCoordinator(navigationController: navigationController)
        coordinator.start()
    }
    
    func didOpenVirtualCard(from navigationController: UINavigationController, settingsData: CardSettingsData) {
        let input = VirtualCardFlowInput(
            button: settingsData.buttons.virtualCard
        )
        
        let coordinator = VirtualCardFlowCoordinator(navigation: navigationController, input: input)
        childCoordinators.append(coordinator)
        coordinator.start()
    }
    
    func didAskAuthentication(onSuccess: @escaping (String) -> Void) {
        didAskForAuthentication(onSuccess: onSuccess)
    }
}

extension SettingsCoordinator: DueDayCoordinatingDelegate {
    public func didConfirmDueDay(cardForm: CardForm?, bestPurchaseDay: Int?) {
        navigationController.popViewController(animated: true)
    }
}

extension SettingsCoordinator {
    enum TypeScreen {
        case profile
        case changePassword
        case aboutPolicies
        case aboutTerms
        case ombudsman
    }
}
