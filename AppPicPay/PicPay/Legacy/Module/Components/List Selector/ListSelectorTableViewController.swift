//
//  ListSelectorTableViewController.swift
//  PicPay
//
//  Created by Lucas Romano on 27/09/18.
//

import UI
import UIKit

protocol ListSelectorTableViewDelegate: AnyObject {
    func selectedItem(_ id: String, description: String)
    func loadList(_ completion: @escaping ([ListSelectorViewModel.ListSelectorItem], Error?) -> Void)
}

final class ListSelectorTableViewController: PPBaseTableViewController {

    class func instantiate() -> ListSelectorTableViewController {
        let storyboard = UIStoryboard(name: "ListSelector", bundle: Bundle.main)
        guard let controller = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as? ListSelectorTableViewController else {
            fatalError("not instantiate \(String(describing: self))")
        }
        return controller
    }
    
    lazy var loadingView: UIView = {
        let view = UIView(frame: self.view.bounds)
        view.backgroundColor = Palette.ppColorGrayscale000.color
        let activity = UIActivityIndicatorView(style: .gray)
        activity.center = view.center
        activity.startAnimating()
        view.addSubview(activity)
        return view
    }()
    
    weak var delegate: ListSelectorTableViewDelegate?
    var viewModel: ListSelectorViewModel = ListSelectorViewModel()
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupSerachController()
        loadList()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(in: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ListSelectorTableViewCell.identifier, for: indexPath) as? ListSelectorTableViewCell else {
            fatalError("not set up ListSelectorTableViewCell in \(String(describing: self))")
        }
        cell.item = viewModel.getItem(in: indexPath)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.didSelectItem(in: indexPath)
        guard let selectedItem = viewModel.selectedItem else {
            return
        }
        delegate?.selectedItem(selectedItem.id, description: selectedItem.description)
        if self.isBeingPresented {
            dismiss(animated: true, completion: nil)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
}

extension ListSelectorTableViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else {
            return
        }
        viewModel.filterInList(with: text)
        tableView.reloadData()
    }
}

extension ListSelectorTableViewController {
    private func setupTableView() {
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
    }
    
    private func setupSerachController() {
        searchController.searchResultsUpdater = self
        searchController.searchBar.tintColor = Palette.ppColorBranding300.color
        searchController.searchBar.backgroundColor = Palette.ppColorGrayscale100.color
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        
        definesPresentationContext = true
        extendedLayoutIncludesOpaqueBars = true
        
        if #available(iOS 11.0, *) {
            searchController.obscuresBackgroundDuringPresentation = false
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            searchController.searchBar.sizeToFit()
            tableView.tableHeaderView = searchController.searchBar
        }
    }
    
    private func loadList() {
        view.addSubview(loadingView)
        delegate?.loadList({ [weak self] (list, error) in
            guard let strongSelf = self else {
            return
        }
            strongSelf.loadingView.removeFromSuperview()
            if let error = error {
                AlertMessage.showCustomAlertWithError(error, controller: strongSelf, completion: { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                })
            } else {
                strongSelf.viewModel.list = list
                strongSelf.tableView.reloadData()
            }
        })
    }
}
