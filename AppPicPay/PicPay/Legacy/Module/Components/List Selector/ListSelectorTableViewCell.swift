//
//  ListSelectorTableViewCell.swift
//  PicPay
//
//  Created by Lucas Romano on 27/09/18.
//

import UIKit

final class ListSelectorTableViewCell: UITableViewCell {

    static let identifier = "listSelectorCellIdentifier"
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var item: ListSelectorViewModel.ListSelectorItem? {
        didSet {
            guard let item = self.item else {
            return
        }
            descriptionLabel.text = item.description
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
