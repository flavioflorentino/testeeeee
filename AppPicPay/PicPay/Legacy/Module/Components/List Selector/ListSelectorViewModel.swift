//
//  ListSelectorViewModel.swift
//  PicPay
//
//  Created by Lucas Romano on 27/09/18.
//

import Foundation

public final class ListSelectorViewModel {
    
    var list: [ListSelectorItem] = []
    var filteredList: [ListSelectorItem] = []
    var selectedItem: ListSelectorItem?
    var noResultsInFilteredList: Bool = false
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRows(in section: Int) -> Int {
        return (filteredList.isEmpty && !noResultsInFilteredList) ? list.count : filteredList.count
    }
    
    func getItem(in indexPath: IndexPath) -> ListSelectorItem {
        return (filteredList.isEmpty && !noResultsInFilteredList) ? list[indexPath.row] : filteredList[indexPath.row]
    }
    
    func didSelectItem(in indexPath: IndexPath) {
        selectedItem = (filteredList.isEmpty && !noResultsInFilteredList) ? list[indexPath.row] : filteredList[indexPath.row]
    }
    
    func filterInList(with text: String) {
        if text.isEmpty {
            filteredList = []
            noResultsInFilteredList = false
        } else {
            filteredList = list.filter({ $0.description.lowercased().contains(text.lowercased().trim()) })
            noResultsInFilteredList = filteredList.isEmpty
        }
    }
}

public extension ListSelectorViewModel {
 
    struct ListSelectorItem {
        let id: String
        let description: String
        
        init(with id: String, and description: String) {
            self.id = id
            self.description = description
        }
    }
    
}
