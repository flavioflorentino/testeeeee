//
//  CompletionInformationViewController.swift
//  PicPay
//
//  Created by Lucas Romano on 30/08/2018.
//

import UI
import UIKit

final class CompletionInformationViewController: PPBaseViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var closeButon: UIButton!
    
    private var viewModel: CompletionInformationViewModel
    public var onCloseAction: (() -> Void)?
    public var autoDismissAction: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        configureElements()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard viewModel.isAutoDismiss else {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.autoDismissAction?()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    init(with title: String? = nil, image: UIImage? = nil, description: String? = nil, showCloseButton: Bool = false, isAutoDismiss: Bool = true) {
        viewModel = CompletionInformationViewModel(with: title, image: image, description: description, showCloseButton: showCloseButton, isAutoDismiss: isAutoDismiss)
        super.init(nibName: "CompletionInformationViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction private func closeButtonTapped(_ sender: Any) {
        onCloseAction?()
    }
}

extension CompletionInformationViewController {
    
    func configureElements() {
        closeButon.tintColor = Palette.ppColorBranding300.color
        let image =  #imageLiteral(resourceName: "iconClose").withRenderingMode(.alwaysTemplate)
        closeButon.setImage(image, for: .normal)
        closeButon.isUserInteractionEnabled = true
        closeButon.imageView?.tintColor = Palette.ppColorBranding300.color
        
        if let title = viewModel.title {
            titleLabel.isHidden = false
            titleLabel.text = title
        } else {
            titleLabel.isHidden = true
        }
        
        if let image = viewModel.image {
            imageView.isHidden = false
            imageView.image = image
        } else {
            imageView.isHidden = true
        }
        
        if let description = viewModel.description {
            descriptionLabel.isHidden = false
            descriptionLabel.text = description
        } else {
            descriptionLabel.isHidden = true
        }
        closeButon.isHidden = !viewModel.showCloseButton
    }
}
