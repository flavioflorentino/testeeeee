//
//  CompletionInformationViewModel.swift
//  PicPay
//
//  Created by Lucas Romano on 30/08/2018.
//

import Foundation

final class CompletionInformationViewModel {
    
    let title: String?
    let image: UIImage?
    let description: String?
    let showCloseButton: Bool
    let isAutoDismiss: Bool
    
    init(with title: String? = nil, image: UIImage? = nil, description: String? = nil, showCloseButton: Bool = false, isAutoDismiss: Bool = true) {
        self.title = title
        self.image = image
        self.description = description
        self.showCloseButton = showCloseButton
        self.isAutoDismiss = isAutoDismiss
    }
    
}
