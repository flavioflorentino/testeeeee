import UI
import UIKit
import SwiftyJSON

final class Text : WidgetStackble, BaseApiResponse {
    static var widgetTypeDescription = "text"
    var widgetType: String = Text.widgetTypeDescription
    
    enum TextType: String {
        case plain = "plain"
        case html = "html"
    }
    
    var text: String?
    var color: UIColor?
    var textType: TextType = .plain
    
    required init?(json: JSON) {
        
        if let text = json["value"].string {
            self.text = text
        }
        
        if let color = json["color"].string {
            self.color = Palette.hexColor(with: color) ?? Palette.ppColorGrayscale600.color
        }
        
        if let textType = TextType(rawValue: json["type"].string ?? "") {
            self.textType = textType
        }
    }
}
