import SwiftyJSON

public final class Button: WidgetStackble {
    static var widgetTypeDescription = "button"
    var widgetType: String = Button.widgetTypeDescription
    
    enum ButtonType: String {
        case cta
        case ctaYellow
        case ghost
        case ghostYellow
        case ghostGray
        case destructive
        case clean
        case inline
        case underlined
        case cleanUnderlined
        case custom
        case white
        case cleanDestructive
    }
    
    enum Action: RawRepresentable {
        case handler
        case finish
        case confirm
        case refund
        case mbsInsertAddress
        case mbsPaySubscription
        case mbsResubscribe
        case mbsPlanUpdateHistory
        case mbsDismissPlanUpdate
        case mbsNewSubscription
        case mbsSubscriberSubscriptions
        case showReceipt
        case cancelBillsAnalysis
        case backToScanner
        case callback
        case sendLink
        
        // Automatically handed actions by AlertPopup
        case close
        case deeplink
        
        var rawValue: String {
            return String(describing: self)
        }
        
        init?(rawValue: String) {
            switch rawValue {
            case Action.close.rawValue:
                self = .close
            case Action.deeplink.rawValue:
                self = .deeplink
            case Action.finish.rawValue:
                self = .finish
            case Action.refund.rawValue:
                self = .refund
            case Action.sendLink.rawValue:
                self = .sendLink
            case "mbs_insert_address":
                self = .mbsInsertAddress
            case "mbs_pay_subscription":
                self = .mbsPaySubscription
            case "mbs_resubscribe":
                self = .mbsResubscribe
            case "mbs_plan_update_history":
                self = .mbsPlanUpdateHistory
            case "mbs_dismiss_plan_update":
                self = .mbsDismissPlanUpdate
            case "mbs_new_subscription":
                self = .mbsNewSubscription
            case "mbs_subscriber_subscriptions":
                self = .mbsSubscriberSubscriptions
            case "show_receipt":
                self = .showReceipt
            case "cancel_bills_analysis":
                self = .cancelBillsAnalysis
            case "back_to_scanner":
                self = .backToScanner
            case Action.callback.rawValue:
                self = .callback
            default:
                self = .handler
            }
        }
    }
    
    enum Alignment: String {
        case left = "left"
        case center = "center"
        case right = "right"
    }
    
    var type: ButtonType = .cta
    var action: Action = .close
    var actionData: [AnyHashable: Any]? = nil
    var title: String
    var alignment: Alignment = .left
    var backgroundColor: String?
    var titleColor: String?
    var borderColor: String?
    var handler: ((AlertPopupViewController, UIPPButton) -> Void)?
    
    init(title: String) {
        self.title = title
    }
    
    init(title: String, type: ButtonType, action: Action? = nil) {
        self.title = title
        self.type = type
        if let action = action {
            self.action = action
        }
    }
    
    convenience init (title: String, type: ButtonType, handler: @escaping (AlertPopupViewController, UIPPButton) -> Void) {
        self.init(title: title, type: type, action: .handler)
        self.handler = handler
    }
    
    init?(json: JSON) {
        guard let title = json["title"].string else {
            return nil
        }
        self.title = title
        
        if let actionRaw = json["action"].string, let action = Action(rawValue: actionRaw) {
            self.action = action
        }
        
        if let actionData = json["action_data"].dictionaryObject {
            self.actionData = actionData
        }
        
        if let typeRaw = json["type"].string, let type = ButtonType(rawValue: typeRaw) {
            self.type = type
        }
        
        if let alignmentRaw = json["alignment"].string, let alignment = Button.Alignment(rawValue: alignmentRaw) {
            self.alignment = alignment
        }
        
        titleColor = json["title_color"].string ?? ""
        backgroundColor = json["background_color"].string ?? ""
    }
    
    var actionDataUrl: URL? {
        if let urlString = actionData?["url"] as? String {
            return URL(string: urlString)
        }
        return nil
    }
    
    var actionDataMessage: String? {
       actionData?["message"] as? String
    }
}
