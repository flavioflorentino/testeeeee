import UI
import UIKit

@IBDesignable
class UIPPButton: UIButton {
    @IBInspectable var type: NSInteger = 0
    @IBInspectable var borderColor = Palette.ppColorBranding300.color
    @IBInspectable var borderWidth = AppStyles.buttonBorderWidth()
    @IBInspectable var cornerRadius = AppStyles.buttonCornerRadius()

    @IBInspectable var letIconPosition = true
    @IBInspectable var leftIconPadding: CGFloat = 20.0

    @IBInspectable var tintImage = false

    @IBInspectable var normalTitleColor = Palette.ppColorBranding300.color
    @IBInspectable var normalBackgrounColor = Palette.ppColorGrayscale000.color

    @IBInspectable var disabledTitleColor = Palette.ppColorBranding300.color
    @IBInspectable var disabledBackgrounColor = Palette.ppColorGrayscale000.color
    @IBInspectable var disabledBorderColor = Palette.ppColorGrayscale000.color

    @IBInspectable var highlightedImage: String = ""
    @IBInspectable var highlightedTitleColor = Palette.ppColorGrayscale000.color
    @IBInspectable var highlightedBackgrounColor = Palette.ppColorBranding300.color
    @IBInspectable var highlightedBorderColor : UIColor?
    
    @IBInspectable var loadingTitleColor: UIColor?
    @IBInspectable var loadingBackgrounColor: UIColor?
    @IBInspectable var loadingBorderColor: UIColor?
    
    var model: Button?
    var isLoading = false
    var activityIndicatorViewStyle: UIActivityIndicatorView.Style?
    
    // MARK: - Private Properties
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView(style: .gray)
        activity.translatesAutoresizingMaskIntoConstraints = false
        activity.isHidden = true
        activity.hidesWhenStopped = true
        activity.tintColor = normalBackgrounColor
        return activity
    }()
    private var tempTitle: String?
    private var tempAttributedTitle: NSAttributedString?
    private var tempImage: UIImage?
    private var tempState: UIControl.State?
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        commonInit()
    }
    
    convenience init(title: String, target: Any?, action: Selector, type: Button.ButtonType = .cta) {
        self.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        cornerRadius = 22
        configure(with: Button(title: title, type: type))
        addTarget(target, action: action, for: .touchUpInside)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        addSubview(activityIndicator)
        centerActivityIndicator()
    }

    // MARK: Public Methods
    @objc
    func startLoadingAnimating(style: UIActivityIndicatorView.Style = .white, isEnabled: Bool = false) {
        isLoading = true
        self.isEnabled = isEnabled
        startActivityIndicatorWith(style)
        updateTempVarsWith(isEnabled ? .normal : .disabled)
        clearButtonForLoading()
    }
    
    @objc
    func stopLoadingAnimating(isEnabled: Bool = true) {
        activityIndicator.stopAnimating()
        isLoading = false
        checkAndUpdateTitleAndImagesWithTemp()
        self.isEnabled = isEnabled
    }

    // MARK: Layout Methods
    @objc func setupTintImage() {
        if tintImage {
            adjustsImageWhenHighlighted = false
            if let image = imageView?.image {
                setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
            }
        }
    }

    // MARK: - Layout Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        setupTintImage()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius

        if letIconPosition,
            var frame = imageView?.frame {
            frame.origin.x = leftIconPadding
            imageView?.frame = frame
        }

        if type == 0 {
            configureForTypeZero()
        }

        if type == 1 {
            configureForTypeOne()
        }
    }
    
    private func configureForTypeZero() {
        switch state {
        case .normal:
            if buttonType == .custom {
                titleLabel?.textColor = normalTitleColor
            } else {
                tintColor = normalTitleColor
            }
            backgroundColor = normalBackgrounColor
            
            if highlightedBorderColor?.cgColor != nil {
                layer.borderColor = borderColor.cgColor
            }
            if tintImage {
                imageView?.tintColor = normalTitleColor
            }
        case .highlighted:
            if highlightedImage != "" {
                setImage(UIImage(named: highlightedImage), for: .highlighted)
            }
            titleLabel?.textColor = highlightedTitleColor
            backgroundColor = highlightedBackgrounColor
            
            if let color = highlightedBorderColor?.cgColor {
                layer.borderColor = color
            }
            
            if tintImage {
                imageView?.tintColor = highlightedTitleColor
            }
        case .disabled:
            titleLabel?.textColor = isLoading && loadingTitleColor != nil ? loadingTitleColor : disabledTitleColor
            layer.borderColor = isLoading && loadingBorderColor != nil ? loadingBorderColor?.cgColor : disabledBorderColor.cgColor
            backgroundColor = isLoading && loadingBackgrounColor != nil ? loadingBackgrounColor : disabledBackgrounColor
            if tintImage {
                imageView?.tintColor = loadingTitleColor ?? disabledTitleColor
            }
        default:
            break
        }
    }
    
    private func configureForTypeOne() {
        switch state {
        case .normal:
            if highlightedImage != "" {
                setImage(UIImage(named: highlightedImage), for: .highlighted)
            }
            titleLabel?.textColor = highlightedTitleColor
            backgroundColor = highlightedBackgrounColor
            
            if let color = highlightedBorderColor?.cgColor {
                layer.borderColor = color
            }
            
            if tintImage {
                imageView?.tintColor = highlightedTitleColor
            }
        case .highlighted:
            titleLabel?.textColor = normalTitleColor
            backgroundColor = normalBackgrounColor
            
            if highlightedBorderColor?.cgColor != nil {
                layer.borderColor = borderColor.cgColor
            }
            
            if tintImage {
                imageView?.tintColor = normalTitleColor
            }
        case .disabled:
            titleLabel?.textColor = disabledTitleColor
            layer.borderColor = disabledBorderColor.cgColor
            backgroundColor = disabledBackgrounColor
            if tintImage {
                imageView?.tintColor = disabledTitleColor
            }
        default:
            break
        }
    }
    
    func configure(with button: Button, font buttonFont: UIFont = UIFont.systemFont(ofSize: 15, weight: .semibold)) {
        model = button
        setTitle(button.title, for: .normal)
        
        switch button.type {
        case .cta:
            configureButtonCta()
            
        case .ctaYellow:
            configureButtonCtaYellow()
            
        case .ghost:
            configureButtonGhost()
            
        case .destructive:
            configureButtonDestructive()
            
        case .cleanDestructive:
            configureButtonCleanDestructive()
            
        case .clean, .inline, .cleanUnderlined:
            configureButtonClean()
            
        case .underlined:
            configureButtonUnderlined()
            
        case .white:
            configureButtonWhite()
            
        default:
            break
        }
        
        // Custom config
        var titleAttributes: [NSAttributedString.Key : Any] = [.font: buttonFont]
        if let titleColor = button.titleColor, !titleColor.isEmpty, let color = Palette.hexColor(with: titleColor) {
            titleAttributes[.foregroundColor] = color
            normalTitleColor = color
            highlightedTitleColor = color.withAlphaComponent(0.75)
            disabledTitleColor = color
        }
        
        if button.type == .underlined || button.type == .cleanUnderlined {
            titleAttributes[.underlineStyle] = 1
        }
        
        setAttributedTitle(NSAttributedString(string: button.title, attributes: titleAttributes), for: .normal)
        
        if let backgroundColor = button.backgroundColor, !backgroundColor.isEmpty, let color = Palette.hexColor(with: backgroundColor) {
            normalBackgrounColor = color
            highlightedBackgrounColor = color.withAlphaComponent(0.75)
            disabledBackgrounColor = color
        }
    }
    
    
    func configureButtonCta() {
        normalTitleColor = Palette.white.color
        normalBackgrounColor = Palette.ppColorBranding300.color
        borderWidth = 0
        borderColor = Palette.ppColorBranding300.color
        
        loadingTitleColor = .clear
        loadingBackgrounColor = Palette.ppColorBranding300.color.withAlphaComponent(0.65)
        loadingBorderColor = .clear
        
        highlightedTitleColor = Palette.white.color
        highlightedBackgrounColor = Palette.ppColorBranding300.color.withAlphaComponent(0.5)
        
        disabledTitleColor = Palette.white.color
        disabledBackgrounColor = Palette.ppColorGrayscale300.color
        disabledBorderColor = .clear
    }
    
    func configureButtonCtaYellow() {
        normalTitleColor = .black
        normalBackgrounColor = Palette.ppColorAttentive300.color
        borderWidth = 0
        borderColor = Palette.ppColorBranding300.color
        
        loadingTitleColor = .clear
        loadingBackgrounColor = Palette.ppColorAttentive300.color.withAlphaComponent(0.65)
        loadingBorderColor = .clear
        
        highlightedTitleColor = Palette.ppColorGrayscale000.color
        highlightedBackgrounColor = Palette.ppColorAttentive300.color.withAlphaComponent(0.5)
        
        disabledTitleColor = Palette.ppColorGrayscale000.color
        disabledBackgrounColor = Palette.ppColorGrayscale300.color
        disabledBorderColor = .clear
    }
    
    func configureButtonGhost() {
        normalTitleColor = Palette.ppColorBranding300.color
        normalBackgrounColor = .clear
        borderWidth = 1
        borderColor = Palette.ppColorBranding300.color
        
        loadingTitleColor = Palette.ppColorBranding300.color.withAlphaComponent(0.5)
        loadingBackgrounColor = .clear
        loadingBorderColor = Palette.ppColorBranding300.color
        
        highlightedTitleColor = Palette.ppColorBranding300.color.withAlphaComponent(0.5)
        highlightedBackgrounColor = .clear
        
        disabledTitleColor = Palette.ppColorBranding300.color.withAlphaComponent(0.65)
        disabledBackgrounColor = .clear
        disabledBorderColor = Palette.ppColorBranding300.color.withAlphaComponent(0.65)
    }
    
    func configureButtonDestructive() {
        normalTitleColor = Palette.white.color
        normalBackgrounColor = Palette.ppColorNegative300.color
        borderWidth = 0
        borderColor = Palette.ppColorNegative300.color
        
        loadingTitleColor = .clear
        loadingBackgrounColor = Palette.ppColorNegative300.color.withAlphaComponent(0.65)
        loadingBorderColor = .clear
        
        highlightedTitleColor = Palette.ppColorGrayscale000.color
        highlightedBackgrounColor = Palette.ppColorNegative300.color.withAlphaComponent(0.5)
        
        disabledTitleColor = Palette.ppColorGrayscale000.color
        disabledBackgrounColor = Palette.ppColorNegative300.color
        disabledBorderColor = .clear
    }
    
    
    func configureButtonCleanDestructive() {
        normalTitleColor = Palette.ppColorNegative300.color
        normalBackgrounColor = .clear
        borderWidth = 0
        
        loadingTitleColor = .clear
        loadingBackgrounColor = .clear
        loadingBorderColor = .clear
        
        highlightedTitleColor = Palette.ppColorNegative300.color.withAlphaComponent(0.65)
        highlightedBackgrounColor = .clear
        
        disabledTitleColor = Palette.ppColorNegative300.color.withAlphaComponent(0.5)
        disabledBackgrounColor = .clear
        disabledBorderColor = .clear
    }
    
    func configureButtonWhite() {
        normalTitleColor = Palette.ppColorGrayscale000.color
        normalBackgrounColor = .clear
        borderWidth = 1
        borderColor = Palette.ppColorGrayscale000.color
        
        loadingTitleColor = .clear
        loadingBackgrounColor = .clear
        loadingBorderColor = Palette.ppColorGrayscale000.color
        
        highlightedTitleColor = Palette.ppColorGrayscale000.color
        highlightedBackgrounColor = .clear
        
        disabledTitleColor = Palette.ppColorGrayscale000.color
        disabledBackgrounColor = Palette.ppColorGrayscale300.color
        disabledBorderColor = .clear
        
        activityIndicatorViewStyle = .white
    }
    
    
    func configureButtonClean() {
        normalTitleColor = Palette.ppColorGrayscale400.color
        normalBackgrounColor = .clear
        borderWidth = 0
        borderColor = .clear
        
        loadingTitleColor = .clear
        loadingBackgrounColor = .clear
        loadingBorderColor = .clear
        
        highlightedTitleColor = Palette.ppColorGrayscale300.color
        highlightedBackgrounColor = .clear
        
        disabledTitleColor = Palette.ppColorGrayscale400.color.withAlphaComponent(0.6)
        disabledBackgrounColor = Palette.ppColorGrayscale300.color.withAlphaComponent(0.2)
        disabledBorderColor = .clear
    
        activityIndicatorViewStyle = .gray
    }
    
    func configureButtonUnderlined() {
        normalTitleColor = Palette.ppColorGrayscale400.color
        normalBackgrounColor = .clear
        borderWidth = 0
        borderColor = .clear
        
        loadingTitleColor = .clear
        loadingBackgrounColor = .clear
        loadingBorderColor = .clear
        
        highlightedTitleColor = Palette.ppColorGrayscale300.color
        highlightedBackgrounColor = .clear
        
        disabledTitleColor = Palette.ppColorGrayscale400.color.withAlphaComponent(0.6)
        disabledBackgrounColor = Palette.ppColorGrayscale300.color.withAlphaComponent(0.2)
        disabledBorderColor = .clear
        
        normalTitleColor = PicPayStyle.mediumGreen1
        
        activityIndicatorViewStyle = .gray
    }
}

// MARK: Loading Configuration
extension UIPPButton {
    private func clearButtonForLoading() {
        setTitle("", for: state)
        setAttributedTitle(nil, for: state)
        setImage(nil, for: state)
    }
    
    private func startActivityIndicatorWith(_ style: UIActivityIndicatorView.Style) {
        activityIndicator.style = style
        activityIndicator.startAnimating()
    }
    
    private func centerActivityIndicator() {
        activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    private func updateTempVarsWith(_ state: UIControl.State) {
        tempTitle = title(for: state)
        tempAttributedTitle = attributedTitle(for: state)
        tempImage = image(for: state)
        tempState = state
    }
    
    private func checkAndUpdateTitleAndImagesWithTemp() {
        if let state = tempState {
            setTitle(tempTitle, for: state)
            setAttributedTitle(tempAttributedTitle, for: state)
            setImage(tempImage, for: state)
        }
    }
}

//Mark - Helpers
extension UIPPButton {
    func setTitleColor(_ color: UIColor, states: [UIControl.State] = [.normal, .disabled, .highlighted]) {
        if states.contains(.normal) {
            normalTitleColor = color
        }
        if states.contains(.disabled) {
            disabledTitleColor = color
        }
        if states.contains(.highlighted) {
            highlightedTitleColor = color
        }
    }
    
    func setBackgroundColor(_ color: UIColor, states: [UIControl.State] = [.normal, .disabled, .highlighted]) {
        if states.contains(.normal) {
            normalBackgrounColor = color
        }
        if states.contains(.disabled) {
            disabledBackgrounColor = color
        }
        if states.contains(.highlighted) {
            highlightedBackgrounColor = color
        }
    }
    
    func setBorderColor(_ color: UIColor, states: [UIControl.State] = [.normal, .disabled, .highlighted]) {
        if states.contains(.normal) {
            borderColor = color
        }
        if states.contains(.disabled) {
            disabledBorderColor = color
        }
        if states.contains(.highlighted) {
            borderColor = color
        }
    }
}
