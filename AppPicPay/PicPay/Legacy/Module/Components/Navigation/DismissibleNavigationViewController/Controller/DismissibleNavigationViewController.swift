protocol DismissibleNavigationViewControllerDelegate: AnyObject {
    func didDismiss()
}

final class DismissibleNavigationViewController: PPNavigationController {
    private var rootViewController: UIViewController?
    public weak var dismissalDelegate: DismissibleNavigationViewControllerDelegate?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        
        self.rootViewController = rootViewController
        rootViewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(close))
    }
    
    @objc
    private func close() {
        self.rootViewController?.dismiss(animated: true) { [weak self] in
            self?.dismissalDelegate?.didDismiss()
        }
    }
}
