import UI
import UIKit

final class NavigationTitle2LinesView: UIView {
    
    lazy var descriptionLabel: UILabel = {
        return UILabel(frame: self.frame)
    }()
    lazy var titleLabel: UILabel = {
        return UILabel(frame: self.frame)
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        addSubview(descriptionLabel)
        descriptionLabel.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(0)
        }
        descriptionLabel.font = UIFont.systemFont(ofSize: 11)
        descriptionLabel.textColor = Palette.ppColorGrayscale000.color
        descriptionLabel.textAlignment = .center
        
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.bottom.leading.trailing.equalTo(0)
            make.top.equalTo(descriptionLabel.snp.bottom)
        }
        titleLabel.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold)
        titleLabel.textColor = Palette.ppColorGrayscale000.color
        titleLabel.textAlignment = .center
    }
    
}

