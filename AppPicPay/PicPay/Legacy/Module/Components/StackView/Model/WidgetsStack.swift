//
//  WidgetsStack.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 27/04/18.
//

import UIKit
import SwiftyJSON

protocol WidgetStackble {
    var widgetType: String { get }
}

final class WidgetsStack : BaseApiResponse {
    var background: String?
    var widgets:[WidgetStackble] = []
    
    required init?(json: JSON) {
        if let background = json["background_color"].string{
            self.background = background
        }
        
        if let widgetsJson = json["widgets"].array{
            for w in widgetsJson {
                guard let type = w["type"].string else { continue }
                let data = w["data"]
                
                switch type {
                
                case TitleStackWidget.widgetTypeDescription:
                    if let title = TitleStackWidget(json: data) {
                        widgets.append(title)
                    }
                    
                case Button.widgetTypeDescription:
                    if let button = Button(json: data) {
                        widgets.append(button)
                    }
                    
                case Text.widgetTypeDescription:
                    if let text = Text(json: data) {
                        widgets.append(text)
                    }
                    
                case LabeledTextStackWidget.widgetTypeDescription:
                    if let labeledText = LabeledTextStackWidget(json: data){
                        widgets.append(labeledText)
                    }
                    
                case SeparatorStackWidget.widgetTypeDescription:
                    if let separator = SeparatorStackWidget(json: data){
                        widgets.append(separator)
                    }
                    
                default:
                    continue
                }
            }
        }
    }
}
