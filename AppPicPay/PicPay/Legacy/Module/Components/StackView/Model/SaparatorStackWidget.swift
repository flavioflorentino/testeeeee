//
//  TitleStackWidget.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 04/05/18.
//

import UIKit
import SwiftyJSON

final class SeparatorStackWidget: WidgetStackble, BaseApiResponse {
    
    static var widgetTypeDescription = "separator"
    var widgetType: String = SeparatorStackWidget.widgetTypeDescription
    
    var color: String?
    
    required init?(json: JSON) {
        color = json["color"].string ?? nil
    }
    
}

