//
//  LabeledStackWidget.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 11/05/18.
//

import SwiftyJSON

final class LabeledTextStackWidget: WidgetStackble, BaseApiResponse {
    
    static var widgetTypeDescription = "labeled_text"
    var widgetType: String = LabeledTextStackWidget.widgetTypeDescription
    
    var text: Text?
    var label: Text?
    
    required init?(json: JSON) {
        label = Text(json: json["label"])
        text = Text(json: json["text"])
    }
    
}
