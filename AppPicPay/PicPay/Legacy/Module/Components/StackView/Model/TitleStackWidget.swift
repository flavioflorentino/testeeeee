//
//  TitleStackWidget.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 04/05/18.
//

import UIKit
import SwiftyJSON

final class TitleStackWidget: WidgetStackble, BaseApiResponse {
    
    static var widgetTypeDescription = "title"
    var widgetType: String = TitleStackWidget.widgetTypeDescription
    
    var text: String?
    var textColor: String?
    var backgroundColor: String?
    var icon: String?
    
    required init?(json: JSON) {
        
        if let text = json["value"].string {
            self.text = text
        }
        
        if let textColor = json["text_color"].string {
            self.textColor = textColor
        }
        
        if let backgroundColor = json["background_color"].string {
            self.backgroundColor = backgroundColor
        }
        
        if let icon = json["icon"].string {
            self.icon = icon
        }
    }
    
}
