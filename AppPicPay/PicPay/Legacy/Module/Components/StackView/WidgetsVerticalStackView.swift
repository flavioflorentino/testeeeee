import UI
import UIKit

final class WidgetsVerticalStackView: UIView {
    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.backgroundColor = UIColor.clear
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.spacing = spacing
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        return stackView
    }()
    
    var model: WidgetsStack
    private let padding: CGFloat = 16
    private let spacing: CGFloat = 10
    
    public var didTapOnButton: ((UIPPButton, Button, WidgetsStack) -> Void)?
    
    init(frame: CGRect, model: WidgetsStack) {
        self.model = model
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        backgroundColor = .clear
        if let background = model.background, let backgroundColor = Palette.hexColor(with: background) {
            self.backgroundColor = backgroundColor
        }
        
        clipsToBounds = true
        layer.cornerRadius = 5.0
        addSubview(stackView)
        NSLayoutConstraint.constraintAllEdges(from: stackView, to: self)

        for widget in model.widgets {
            addWidgetView(with: widget)
        }
    }
    
    func addWidgetView(with widgetModel: WidgetStackble) {
        switch widgetModel {
        case let model as LabeledTextStackWidget:
            addLabeledText(model)
        case let model as TitleStackWidget:
            addTitle(model)
        case let text as Text:
            addText(text)
        case let button as Button:
            addButton(button)
        case let separator as SeparatorStackWidget:
            addSeparator(separator)
        default:
            return
        }
    }
    
    @objc
    func didTapOnButtonHandler(_ sender: UIPPButton){
        guard let model = sender.model else {
            return
        }
        didTapOnButton?(sender, model, self.model)
    }
    
    func addTitle(_ titleStackWidget: TitleStackWidget){
        let titleView = TitleStackWidgetView(frame: CGRect.zero)
        titleView.configure(with: titleStackWidget)
        stackView.addArrangedSubview(titleView)
    }
    
    func addText(_ text: Text){
        let label = TextStackWidget(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = Palette.ppColorGrayscale400.color
        label.configure(with: text)
        label.backgroundColor = .clear
        
        let content = UIView(frame: CGRect.zero)
        content.addSubview(label)
        NSLayoutConstraint.leadingTrailingCenterX(equalTo: content, for: [label], constant: 16)
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: content.topAnchor, constant: padding),
            label.bottomAnchor.constraint(equalTo: content.bottomAnchor, constant: -padding)
        ])
        stackView.addArrangedSubview(content)
    }
    
    func addLabeledText(_ labeledText: LabeledTextStackWidget){
        let view = LabeledTextStackWidgetView(frame: .zero)
        view.configure(with: labeledText)
        stackView.addArrangedSubview(view)
    }
    
    func addButton(_ button: Button){
        let buttonView = UIPPButton(frame: CGRect.zero)
        buttonView.translatesAutoresizingMaskIntoConstraints = false
        buttonView.contentEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        buttonView.cornerRadius = 18
        buttonView.backgroundColor = .clear
        buttonView.addTarget(self, action: #selector(didTapOnButtonHandler), for: .touchUpInside)
        buttonView.configure(with: button, font: UIFont.boldSystemFont(ofSize: 13.5))
        
        let buttonContent = UIView(frame: CGRect.zero)
        buttonContent.addSubview(buttonView)
        buttonView.heightAnchor.constraint(equalToConstant: 36).isActive = true
        buttonView.topAnchor.constraint(equalTo: buttonContent.topAnchor).isActive = true
        buttonView.bottomAnchor.constraint(equalTo: buttonContent.bottomAnchor).isActive = true
        
        switch button.alignment {
        case .right:
            buttonView.trailingAnchor.constraint(equalTo: buttonContent.trailingAnchor, constant: -padding).isActive = true
        case .left:
            buttonView.leadingAnchor.constraint(equalTo: buttonContent.leadingAnchor, constant: padding).isActive = true
        case .center:
            buttonView.centerXAnchor.constraint(equalTo: buttonContent.centerXAnchor).isActive = true
        }
        
        stackView.addArrangedSubview(buttonContent)
    }
    
    func addSeparator(_ separator: SeparatorStackWidget){
        let separatorView = SeparatorStackWidgetView(frame: .zero)
        stackView.addArrangedSubview(separatorView)
    }
}


