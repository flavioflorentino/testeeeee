import UI
import UIKit

final class TextStackWidget: UILabel {
    func configure(with text: Text) {
        guard text.textType == .html else {
            self.text = text.text
            return
        }
        
        var html = text.text ?? ""
        if !html.contains("<span>"){
            html = "<span>\(html)</span>"
        }
        self.text = ""
        attributedText = html.html2Attributed(UIFont.systemFont(ofSize: 13))?.attributedPicpayFont()
        if !html.contains("#") {
            textColor = Palette.ppColorGrayscale600.color
        }
        setNeedsLayout()
        layoutIfNeeded()
        sizeToFit()
    }

}
