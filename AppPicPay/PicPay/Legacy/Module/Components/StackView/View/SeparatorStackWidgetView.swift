import UI
import UIKit
import SnapKit

final class SeparatorStackWidgetView: UIView {
    
    var separatorView: UIView!
    
    // MARK: - Initialize
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup(){
        // title
        separatorView = UIView(frame: .zero)
        separatorView.backgroundColor = UIColor(red: 218/255, green: 225/255, blue: 240/255, alpha: 1)
        
        addSubview(separatorView)
        
        // Constraints
        separatorView.snp.makeConstraints { (make) in
            make.top.equalTo(5)
            make.bottom.equalTo(-5)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(1 / UIScreen.main.scale)
        }
        
    }
    
    // MARK: - Configure
    
    func configure(with model: SeparatorStackWidget){
        if let backgroundColor = model.color, let color = Palette.hexColor(with: backgroundColor) {
            separatorView.backgroundColor = color
        }
        
    }
    
}

