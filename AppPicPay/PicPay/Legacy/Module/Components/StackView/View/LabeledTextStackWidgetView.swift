//
//  TitleStackWidget.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 04/05/18.
//

import UIKit
import SnapKit

final class LabeledTextStackWidgetView: UIView {
    
    var titleLabel: UILabel!
    var textLabel: UILabel!
    
    // MARK: - Initialize
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup(){
        backgroundColor = .clear
        
        // title
        titleLabel = UILabel(frame: frame)
        titleLabel.text = ""
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1)
        titleLabel.font = UIFont.systemFont(ofSize: 13, weight: .bold)
        addSubview(titleLabel)
        
        // Constraints
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(5)
            make.left.equalTo(34)
            make.right.equalTo(-34)
        }
        
        // title
        textLabel = UILabel(frame: frame)
        textLabel.text = ""
        textLabel.numberOfLines = 0
        textLabel.lineBreakMode = .byWordWrapping
        textLabel.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        textLabel.textColor = UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1)
        addSubview(textLabel)
        
        // Constraints
        textLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(4)
            make.left.equalTo(34)
            make.right.equalTo(-34)
            make.bottom.equalTo(-5)
        }
        
    }
    
    // MARK: - Configure
    
    func configure(with model: LabeledTextStackWidget){
        titleLabel.text = model.label?.text ?? ""
        textLabel.text = model.text?.text ?? ""
    }
    
}

