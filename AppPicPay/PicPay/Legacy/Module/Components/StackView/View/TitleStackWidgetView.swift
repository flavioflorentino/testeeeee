import UI
import UIKit
import SnapKit

final class TitleStackWidgetView: UIView {
    
    var titleLabel: UILabel!
    var iconImageView: UIImageView!
    
    // MARK: - Initialize
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup(){
        
        // icon
        iconImageView = UIImageView(frame: frame)
        iconImageView.contentMode = .scaleAspectFit
        iconImageView.image = #imageLiteral(resourceName: "icon_supscription")
        addSubview(iconImageView)
        
        // title
        titleLabel = UILabel(frame: frame)
        titleLabel.text = ""
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        addSubview(titleLabel)
        
        // Constraints
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.bottom.equalTo(-10)
            make.left.equalTo(iconImageView.snp.right).offset(10)
            make.right.equalTo(-16)
        }
        
        iconImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(titleLabel)
            make.width.height.equalTo(16)
            make.left.equalTo(16)
        }
        
    }
    
    // MARK: - Configure
    
    func configure(with model: TitleStackWidget){
        // title
        titleLabel.text = model.text
        titleLabel.textColor = Palette.hexColor(with: model.textColor ?? "#000000")
        
        // background
        backgroundColor = model.backgroundColor != nil ? Palette.hexColor(with: model.backgroundColor!) : UIColor.clear
        
        // icon
        if let iconString = model.icon {
            var icon = #imageLiteral(resourceName: "ico_card_edit")
            switch iconString {
            case "write":
                icon = #imageLiteral(resourceName: "ico_card_edit")
                break
            case "delete":
                icon = #imageLiteral(resourceName: "ico_card_excluded")
                break
            case "money":
                icon = #imageLiteral(resourceName: "ico_card_money")
                break
            case "clock":
                icon = #imageLiteral(resourceName: "ico_card_clock")
                break
            default: break
            }
            
            iconImageView.image = icon.withRenderingMode(.alwaysTemplate)
            iconImageView.tintColor = titleLabel.textColor
            
        }
    }

}
