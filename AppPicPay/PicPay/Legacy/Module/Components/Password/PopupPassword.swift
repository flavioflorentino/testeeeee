import Foundation
import UI

@objc final class PopupPassword: NSObject {
    
    let popupPresenter = PopupPresenter()
    
    @objc func showAlert(isBiometricActive: Bool, onPay: @escaping (String, Bool, PasswordPopupViewController, UIPPButton) -> Void, onClose: @escaping () -> Void) {
        let senhaPopUp = PasswordPopupViewController(biometricActive: isBiometricActive, onPay: onPay, onClose: onClose)
        popupPresenter.showPopup(senhaPopUp)
    }
}
