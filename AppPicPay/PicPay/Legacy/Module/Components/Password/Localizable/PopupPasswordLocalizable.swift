import Foundation

enum PopupPasswordLocalizable: String, Localizable {
    case typeYourPassword
    case useTouchID
    case useFaceID
    
    var file: LocalizableFile {
        return .popupLegacy
    }
    
    var key: String {
        return self.rawValue
    }
}
