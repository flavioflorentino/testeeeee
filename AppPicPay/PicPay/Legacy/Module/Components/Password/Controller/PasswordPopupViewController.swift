import UI
import UIKit

@objc
final class PasswordPopupViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var passwordTextField: UIPPFloatingTextField!
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var containerBiometricView: UIView!
    @IBOutlet weak var textAuthLabel: UILabel!
    
    private let popupWidth = min(UIScreen.main.bounds.width - (2 * 12), 300)
    private let popupHeight: CGFloat = 262.5
    
    private var titlePopup: String = PopupPasswordLocalizable.typeYourPassword.text
    private var myButton: [Button] = []
    private var buttons: [UIPPButton] = []
    private var isBiometricActive: Bool = true
    private var isSmallScreen: Bool = false
    private var shouldUseExtraHeight: Bool = false
    
    private let onPay: (String, Bool, PasswordPopupViewController, UIPPButton) -> Void
    private let onClose: () -> Void
    
    var isChecked: Bool = false {
        didSet {
            checkImageView.image = isChecked ? UIImage(named: "check_full") : UIImage(named: "check_empty")
        }
    }
    
    init(biometricActive biometric: Bool, onPay: @escaping (String, Bool, PasswordPopupViewController, UIPPButton) -> Void, onClose: @escaping () -> Void) {
        self.onPay = onPay
        self.onClose = onClose
        self.isBiometricActive = biometric
        
        let pagarButton = Button(title: DefaultLocalizable.next.text, type: .cta, action: .confirm)
        let cancelButton = Button(title: DefaultLocalizable.btCancel.text, type: .clean, action: .close)
        myButton = [pagarButton, cancelButton]
        
        super.init(nibName: "PasswordPopupViewController", bundle: nil)
        isSmallScreen = UIScreen.main.bounds.height <= 568
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.endEditing(true)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTouchUpCheck(_:)))
        containerBiometricView.addGestureRecognizer(tapGestureRecognizer)
        
        shouldUseExtraHeight = BiometricTypeAuth().biometricType() != .none && isBiometricActive
        let extraHeight: CGFloat = isSmallScreen ? 44 : 60
        
        view.frame.size = CGSize(width: popupWidth, height: shouldUseExtraHeight ? popupHeight + extraHeight : popupHeight)
        // Verificar se a senha e Numero ou caracter
        if let keyboardType = ConsumerManager.shared.consumer?.numericKeyboardForPassword {
            if keyboardType {
                passwordTextField.keyboardType = UIKeyboardType.numberPad
            }
        }
        passwordTextField.textAlignment = .center
        passwordTextField.accessibilityLabel = titlePopup
        passwordTextField.delegate = self
        titleLabel.text = titlePopup
        setupButtons()
        setupTextAuth()
        setupColors()
        
        if let button = buttons.first {
            button.isEnabled = false
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         passwordTextField.becomeFirstResponder()
    }
    
    private func setupColors() {
        view.backgroundColor = Palette.ppColorGrayscale200.color
        containerBiometricView.backgroundColor = Palette.ppColorGrayscale200.color
        textAuthLabel.textColor = Palette.ppColorGrayscale400.color
    }
    
    private func setupTextAuth() {
        containerBiometricView.isHidden = !isBiometricActive
        let auth = BiometricTypeAuth()
        
        switch auth.biometricType() {
            case BiometricTypeAuth.BiometricType.none:
                containerBiometricView.isHidden = true
            
            case BiometricTypeAuth.BiometricType.touchID:
                textAuthLabel.text = PopupPasswordLocalizable.useTouchID.text
            
            case BiometricTypeAuth.BiometricType.faceID:
                textAuthLabel.text = PopupPasswordLocalizable.useFaceID.text
        }
    }
    
    private func setupButtons() {
        for (index, button) in myButton.enumerated() {
            let buttonView = UIPPButton(frame: CGRect.zero)
            buttonView.setTitle(button.title, for: .normal)
            buttonView.cornerRadius = (shouldUseExtraHeight && isSmallScreen) ? 20 : 22
            buttonView.translatesAutoresizingMaskIntoConstraints = false
            buttonView.heightAnchor.constraint(equalToConstant: (shouldUseExtraHeight && isSmallScreen) ? 40 : 44).isActive = true
            buttonView.addTarget(self, action: #selector(didTouchUpInsideButton(_:)), for: .touchUpInside)
            buttonView.tag = index
            buttonView.configure(with: button, font: UIFont.boldSystemFont(ofSize: 16))
            buttonView.titleLabel?.numberOfLines = 0
            buttonView.titleLabel?.lineBreakMode = .byWordWrapping
            buttonView.titleLabel?.textAlignment = .center
            buttonView.sizeToFit()
            if button.type == .cta {
                buttonView.setBackgroundColor(Palette.ppColorGrayscale400.color, states: [.disabled])
            }
            buttonStackView.addArrangedSubview(buttonView)
            buttons.append(buttonView)
        }
    }
    
    @objc private func didTouchUpCheck(_ sender: UIView) {
        isChecked = !isChecked
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        let screenSizeAvailable = UIScreen.main.bounds.height - endFrame.size.height
        view.frame.origin.y = (screenSizeAvailable / 2) - (view.frame.size.height / 2)
    }
    
    @objc
    private func keyboardWillHide(notification: NSNotification) {
        view.frame.origin.y = (UIScreen.main.bounds.height / 2) - (view.frame.size.height / 2)
    }
    
    @objc private func didTouchUpInsideButton(_ sender: UIPPButton) {
        let button = myButton[sender.tag]
        
        if button.action == .confirm {
            passwordTextField.resignFirstResponder()
            pay(button: sender)
        }
        
        if button.action == .close {
            dismiss(animated: true) { [weak self] in
                self?.onClose()
            }
        }
    }
    
    @objc func disableAllButtons(){
        for button in buttons{
            button.isEnabled = false
        }
    }
    
    @objc func enableAllButtons(){
        for button in buttons{
            button.isEnabled = true
        }
    }
    
    func pay(button: UIPPButton?){
        
        let payButton = button ?? buttons.first
        
        guard let senha = passwordTextField.text,
            let payBtn = payButton else {
            return
        }
        onPay(senha, isChecked, self, payBtn)
    }
}

extension PasswordPopupViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let button = buttons.first else { return true }
        guard let text = textField.text else { return true }
        
        if text.count == 1 && string.isEmpty {
            button.isEnabled = false
        } else {
            button.isEnabled = true
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        pay(button: nil)
        textField.resignFirstResponder()
        view.endEditing(true)
        dismiss(animated: true, completion: nil)
        return true
    }
}
