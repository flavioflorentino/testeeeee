import UIKit

final class AlertPopupViewController: UIViewController {
    typealias ButtonActionBlock = (AlertPopupViewController, Button, UIPPButton) -> Void
    
    // public properties
    let alert: Alert
    var buttonAction: ButtonActionBlock?
    var presenterViewController: UIViewController?
    let popupView: AlertPopupView
    
    init(alert: Alert, action: ButtonActionBlock? = nil, controller: UIViewController?) {
        self.alert = alert
        self.buttonAction = action
        self.presenterViewController = controller
        self.popupView = AlertPopupView(withAlert: alert)
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = popupView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popupView.delegate = self
    }
    
    @objc(disableAllButtons)
    func disableAllButtons() {
        for button in popupView.buttons {
            button.isEnabled = false
        }
    }
    
    @objc(enableAllButtons)
    func enableAllButtons() {
        for button in popupView.buttons {
            button.isEnabled = true
        }
    }
}

extension AlertPopupViewController: AlertPopupViewDelegate {
    func close(alertPopupView: AlertPopupView) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func didTouchUpInsideButton(alertPopupView: AlertPopupView, sender: UIPPButton) {
        let button = alert.buttons[sender.tag]
        
        switch button.action {
        case .close:
            self.dismiss(animated: true, completion: nil)
        case .deeplink:
            self.dismiss(animated: true) { [weak self] in
                guard let presenterVC = self?.presenterViewController else {
                    return
                }
                DeeplinkHelper.handleDeeplink(withUrl: button.actionDataUrl, from: presenterVC)
            }
        case .handler:
            button.handler?(self, sender)
        default:
            break
        }
        
        buttonAction?(self, button, sender)
    }
    
    func linkPushed(alertPopupView: AlertPopupView, url: URL) {
        let webviewController = WebViewFactory.make(with: url)
        self.dismiss(animated: true) { [weak self] in
            self?.presenterViewController?.present(DismissibleNavigationViewController(rootViewController: webviewController), animated: true, completion: nil)
        }
    }
}
