import UI
import UIKit

protocol AlertPopupViewDelegate: AnyObject {
    func didTouchUpInsideButton(alertPopupView: AlertPopupView, sender: UIPPButton)
    func close(alertPopupView: AlertPopupView)
    func linkPushed(alertPopupView: AlertPopupView, url: URL)
}

final class AlertPopupView: UIView {
    private let alert: Alert
    weak var delegate: AlertPopupViewDelegate?
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = Palette.ppColorGrayscale500.color(withCustomDark: .ppColorGrayscale000)
        return label
    }()
    
    private var textContentLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = Palette.ppColorGrayscale400.color
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    private lazy var attributedTextView: TextView = {
        let textView = TextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.backgroundColor = .clear
        return textView
    }()
    
    private var buttonsStack: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.alignment = .fill
        stack.distribution = .fill
        stack.axis = .vertical
        stack.spacing = 12
        return stack
    }()
    
    private var closeButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(#imageLiteral(resourceName: "close_popup"), for: .normal)
        button.addTarget(self, action: #selector(close), for: .touchUpInside)
        return button
    }()
    
    private var imageView: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = .clear
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    private let popupWidth = min(UIScreen.main.bounds.width - (2 * 12), 300)
    
    var buttons: [UIPPButton] = []
    
    //calculation helpers
    private let buttonDefaultHeight: CGFloat = 44
    private let buttonDefaultSpace: CGFloat = 12
    private var allSpacesWithImage: CGFloat = 112
    private var allSpacesWithoutImage: CGFloat = 102
    private let horizontalMarginsSpace: CGFloat = 56
    private let imageDefaultHeight: CGFloat = 100
    
    //constraints
    private var imageViewTitleTopConstraint = NSLayoutConstraint()
    private var imageViewWidthConstraint = NSLayoutConstraint()
    private var imageViewHeightConstraint = NSLayoutConstraint()
    private var imageViewBottomConstraint = NSLayoutConstraint()
    private var titleLabelBottomConstraint = NSLayoutConstraint()
    private var buttonsStackBottomConstraint = NSLayoutConstraint()
    
    init(withAlert alert: Alert) {
        self.alert = alert
        super.init(frame: .zero)
        configureViews()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureViews() {
        addComponents()
        layoutComponents()
        backgroundColor = Palette.ppColorGrayscale000.color(withCustomDark: .ppColorGrayscale500)
        setupCloseButton()
        setupImage()
        setupTitle()
        setupText()
        setupButtons()
    }
    
    private func addComponents() {
        addSubview(closeButton)
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(textContentLabel)
        addSubview(buttonsStack)
    }
    
    private func layoutComponents() {
        closeButton.topAnchor.constraint(equalTo: topAnchor).isActive = true
        closeButton.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        closeButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
        closeButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        imageViewTitleTopConstraint = imageView.topAnchor.constraint(equalTo: topAnchor, constant: 40)
        imageViewTitleTopConstraint.isActive = true
        imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        imageViewWidthConstraint = imageView.widthAnchor.constraint(equalToConstant: 150)
        imageViewWidthConstraint.isActive = true
        imageViewHeightConstraint = imageView.heightAnchor.constraint(equalToConstant: 100)
        imageViewHeightConstraint.isActive = true
        
        imageViewBottomConstraint = imageView.bottomAnchor.constraint(equalTo: titleLabel.topAnchor, constant: -8)
        imageViewBottomConstraint.isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 22).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -22).isActive = true
        
        titleLabelBottomConstraint = titleLabel.bottomAnchor.constraint(equalTo: textContentLabel.topAnchor, constant: -8)
        titleLabelBottomConstraint.isActive = true
        textContentLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24).isActive = true
        textContentLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24).isActive = true
        
        textContentLabel.bottomAnchor.constraint(equalTo: buttonsStack.topAnchor, constant: -24).isActive = true
        
        buttonsStack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24).isActive = true
        buttonsStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24).isActive = true
        buttonsStackBottomConstraint = buttonsStack.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -24)
        buttonsStackBottomConstraint.isActive = true
    }
    
    @objc
    private func touchButton(_ sender: UIPPButton) {
        delegate?.didTouchUpInsideButton(alertPopupView: self, sender: sender)
    }
    
    @objc
    private func close() {
        delegate?.close(alertPopupView: self)
    }
}

// Setups
extension AlertPopupView {
    func setupCloseButton() {
        closeButton.isHidden = !alert.showCloseButton
        if !closeButton.isHidden {
            allSpacesWithoutImage = 120
        }
    }
    
    func setupImage() {
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFit
        
        if let imageObject = alert.image {
            if let image = imageObject.image {
                imageView.image = image
            } else if imageObject.sourceType == .url {
                imageView.showActivityIndicator = true
                let url = URL(string: imageObject.value)
                imageView.setImage(url: url)
            } else {
                imageView.image = UIImage(named: imageObject.value)
            }
            
            if imageObject.styleType == .round {
                imageViewWidthConstraint.constant = imageDefaultHeight
                imageView.layer.cornerRadius = imageView.frame.size.height / 2
            } else if imageObject.styleType == .square {
                imageViewWidthConstraint.constant = imageDefaultHeight
            }
        } else {
            imageViewHeightConstraint.constant = 0
            imageView.isHidden = true
        }
    }
    
    func setupTitle() {
        // Title
        if let alertTitle = alert.title, !alertTitle.string.isEmpty {
            titleLabel.attributedText = alertTitle
            if #available(iOS 11.0, *) {
                titleLabel.preferredMaxLayoutWidth = titleLabel.frame.size.width
            }
        } else {
            titleLabel.isHidden = true
        }
        
        if !alert.showCloseButton && alert.image == nil {
            imageViewTitleTopConstraint.constant = 24
        }
    }
    
    func setupText() {
        if alert.textType == .html {
            setupTextForHtml()
        } else {
            setupPlainText()
        }
    }
    
    private func setupPlainText() {
        let attr = NSMutableAttributedString()
        let paragraphStyle = NSMutableParagraphStyle()
        attr.append(alert.text ?? NSAttributedString(string: ""))
        paragraphStyle.lineSpacing = 4
        paragraphStyle.alignment = .center
        attr.addAttribute(kCTParagraphStyleAttributeName as NSAttributedString.Key, value: paragraphStyle, range: NSRange(location: 0, length: attr.length))
        textContentLabel.attributedText = attr
        textContentLabel.accessibilityLabel = attr.string
        textContentLabel.textColor = Colors.grayscale900.color
        attributedTextView.isHidden = true
    }
    
    private func height(withConstrainedWidth width: CGFloat, text: NSAttributedString) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            context: nil)

        return ceil(boundingBox.height)
    }
    
    private func setupTextForHtml() {
        let unicodeObjectPlaceHolder = "\\ufffc"
        var text = alert.text?.string.replacingOccurrences(of: unicodeObjectPlaceHolder, with: "") ?? ""
        
        if !text.contains("<style") {
            text = "<style type='text/css'>a{color: '#37BF66' !important; font-weight: bold;}</style><center><font color='#6c7073'>\(text)</font></center>"
        }
        
        let font = UIFont.systemFont(ofSize: 14)
        let textAttributedString = text.html2Attributed(font: font)?.attributedPicpayFont() ?? NSAttributedString(string: text)
        let mutableAttributedText = NSMutableAttributedString(attributedString: textAttributedString)
        mutableAttributedText.searchAndAddAttributesOnLinks()
        attributedTextView.attributedText = mutableAttributedText
        let width = popupWidth - horizontalMarginsSpace
        let size = CGSize(width: width,
                          height: height(withConstrainedWidth: width, text: textAttributedString))
        attributedTextView.frame.size = size
        
        textContentLabel.removeConstraints(textContentLabel.constraints)
        textContentLabel.removeFromSuperview()
        
        addSubview(attributedTextView)
        NSLayoutConstraint.activate([
            titleLabel.bottomAnchor.constraint(equalTo: attributedTextView.topAnchor, constant: -16),
            attributedTextView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 28),
            attributedTextView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -28),
            attributedTextView.bottomAnchor.constraint(equalTo: buttonsStack.topAnchor, constant: -24),
            attributedTextView.heightAnchor.constraint(equalToConstant: size.height)
        ])
            
        allSpacesWithoutImage += 16
        imageViewBottomConstraint.constant -= 8
    }
    
    func setupButtons() {
        if alert.buttons.isEmpty {
            alert.buttons.append(Button(title: "Ok"))
        }
        
        for (index, button) in alert.buttons.enumerated() {
            let buttonView = UIPPButton(frame: CGRect.zero)
            buttonView.setTitle(button.title, for: .normal)
            buttonView.accessibilityLabel = button.title
            buttonView.cornerRadius = 22
            buttonView.addTarget(self, action: #selector(touchButton(_:)), for: .touchUpInside)
            buttonView.tag = index
            buttonView.configure(with: button, font: UIFont.boldSystemFont(ofSize: 16))
            buttonView.titleLabel?.numberOfLines = 0
            buttonView.titleLabel?.lineBreakMode = .byWordWrapping
            buttonView.titleLabel?.textAlignment = .center
            buttonView.sizeToFit()
            buttonsStack.addArrangedSubview(buttonView)
            
            buttonView.heightAnchor.constraint(equalToConstant: 44).isActive = true
            buttonView.centerXAnchor.constraint(equalTo: buttonsStack.centerXAnchor).isActive = true
            buttons.append(buttonView)
        }
        
        if let lastBtn = alert.buttons.last, lastBtn.type != .cleanUnderlined || lastBtn.type != .clean {
            buttonsStackBottomConstraint.constant = -32
            allSpacesWithImage += 8
            allSpacesWithoutImage += 8
        }
    }
}

// MARK: - UITextViewDelegate
extension AlertPopupView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        delegate?.linkPushed(alertPopupView: self, url: URL)
        return false
    }
}
