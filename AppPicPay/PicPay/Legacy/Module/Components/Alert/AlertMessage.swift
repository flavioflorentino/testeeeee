import UI
import UIKit

@objc
final class AlertMessage: NSObject {
    // Mark - Public methods
    typealias ButtonActionBlock = (AlertPopupViewController, Button, UIPPButton) -> Void
    
    static let popupPresenter = PopupPresenter()
    
    @discardableResult
    class func showAlert(_ alert: Alert, controller: UIViewController?, action: ButtonActionBlock? = nil ) -> AlertPopupViewController {
        let alertPopup = AlertPopupViewController(alert: alert, action: action, controller: controller)
        if let presenterController = controller {
            popupPresenter.showPopup(alertPopup, fromViewController: presenterController)
        } else {
            popupPresenter.showPopup(alertPopup)
        }
        return alertPopup
    }
    
    class func showAlert(_ error: Error?, controller: UIViewController?, completion: (() -> Void)? = nil, alertButton: AlertPopupViewController.ButtonActionBlock?) {
        guard let picpayError = error as? PicPayErrorDisplayable, let alert = picpayError.alert else {
            AlertMessage.showAlert(error, controller: controller, completion: completion)
            return
        }
        
        AlertMessage.showAlert(alert, controller: controller) { alert, model, button in
            completion?()
            alertButton?(alert, model, button)
        }
    }
    
    class func showCustomAlert(title: String, message: String, controller: UIViewController, completion: (() -> Void)? = nil) {
        let alert = Alert(title: title, text: message)
        alert.buttons = [Button(title: DefaultLocalizable.btOk.text, type: .cta, action: .close)]
        
        AlertMessage.showAlert(alert, controller: controller) { _, _, _ in
            completion?()
        }
    }
    
    class func showTwoButtonsCustomAlert(title: String, message: String, image: UIImage, confirmationButtonTitle: String = DefaultLocalizable.btOk.text, controller: UIViewController, completion: (() -> Void)? = nil) {
        let buttons = [
            Button(title: confirmationButtonTitle, type: .cta, action: .finish),
            Button(title: DefaultLocalizable.back.text, type: .cleanUnderlined, action: .close)
        ]
        
        let alert = Alert(with: title, text: message, buttons: buttons, image: Alert.Image(with: image))
//
        AlertMessage.showAlert(alert, controller: controller) { alert, button, _ in
            guard button.action == .finish else { return }
            alert.dismiss(animated: true)
            completion?()
        }
    }
    
    @objc
    class func showCustomAlertWithError(_ error: Error?, controller: UIViewController?, completion: (() -> Void)? = nil) {
        let errorImage = Alert.Image(with: #imageLiteral(resourceName: "sad-3"))
        let alert = Alert(with: error, image: errorImage)
        alert.buttons = [Button(title: DefaultLocalizable.btOk.text, type: .cta, action: .close)]
        
        AlertMessage.showAlert(alert, controller: controller) { alertController, button, _ in
            if button.action == .close {
                alertController.dismiss(animated: true, completion: {
                    completion?()
                })
            }
        }
    }
    
    @objc
    class func showCustomAlertWithErrorWithImage(title: String, errorMessage: String, image: UIImage, controller: UIViewController?, completion: (() -> Void)? = nil) {
        let errorImage = Alert.Image(with: image)
        let alert = Alert(with: title, text: errorMessage, buttons: nil, image: .init(with: image))
        alert.buttons = [Button(title: DefaultLocalizable.btOk.text, type: .cta, action: .close)]
        
        AlertMessage.showAlert(alert, controller: controller) { alertController, button, _ in
            if button.action == .close {
                alertController.dismiss(animated: true, completion: {
                    completion?()
                })
            }
        }
    }
    
    //Suportar _ui em objective C
    @objc
    class func showCustomAlertWithErrorUI(_ error: Error?, controller: UIViewController?, completion: (() -> Void)? = nil) {
        if let picpayError = error as? PicPayErrorDisplayable, let alert = picpayError.alert {
            showAlert(alert, controller: controller) { _, _, _ in
                completion?()
            }
        } else {
            showCustomAlertWithError(error, controller: controller, completion: completion)
        }
    }
    
    @objc
    class func showAlertWithError(_ error: Error?, controller: UIViewController?) {
        showAlert(error, controller: controller)
    }
    
    class func showAlert(_ error: Error?, controller: UIViewController?, completion: (() -> Void)? = nil) {
        privateShowAlert(message: error?.localizedDescription, controller: controller, completion: completion)
    }
    
    @objc
    class func showAlert(withMessage message: String?, controller: UIViewController?) {
        privateShowAlert(message: message, controller: controller)
    }
    
    @objc
    class func showAlert(withMessage message: String?, controller: UIViewController?, completion: (() -> Void)? = nil) {
        privateShowAlert(message: message, controller: controller, completion: completion)
    }
    
    @objc
    class func showAlert(withTitle title: String?, message: String?, controller: UIViewController?, completion: (() -> Void)? = nil) {
        privateShowAlert(withTitle: title, message: message, controller: controller, completion: completion)
    }
}

extension AlertMessage {
    // Mark - Private methods
    
    private class func privateShowAlert(withTitle title: String? = nil, message: String?, controller: UIViewController?, completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: { _ in
            completion?()
        })
        alert.addAction(action)
        controller?.present(alert, animated: true)
    }
}
