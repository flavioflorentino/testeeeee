import SwiftyJSON
import UIKit

public final class Alert: BaseApiResponse {
    enum TextType: String {
        case plain
        case html
    }
    
    enum ImageStyleType: String {
        case round
        case square
        case standard
    }
    
    enum ImageSourceType: String {
        case asset
        case url
    }
    
    struct Image {
        var value: String = ""
        var styleType: ImageStyleType = .standard
        var sourceType: ImageSourceType = .url
        
        init(name: String, style: ImageStyleType = .standard, source: ImageSourceType = .asset) {
            styleType = style
            sourceType = source
            value = name
        }
        
        var image: UIImage?
        
        init?(json: JSON) {
            guard let value = json["url"].string ?? json["asset"].string else {
                return nil
            }
            self.value = value
            
            if let style = json["type"].string, let styleType = ImageStyleType(rawValue: style) {
                self.styleType = styleType
            }
            
            if let source = json["sourceType"].string, let sourceType = ImageSourceType(rawValue: source) {
                self.sourceType = sourceType
            }
        }
        
        init(with image: UIImage, styleType: ImageStyleType = .standard) {
            self.image = image
            self.styleType = styleType
        }
    }
    
    var title: NSAttributedString?
    var text: NSAttributedString?
    var textType: TextType = .plain
    var image: Image?
    var buttons: [Button] = []
    var showCloseButton: Bool = false
    var dismissWithGesture: Bool = true
    var dismissOnTouchBackground: Bool = true
    var overlayMaskAlpha: CGFloat?
    
    init(title: String?, text: String?) {
        self.title = NSAttributedString(string: title ?? "")
        self.text = NSAttributedString(string: text ?? "")
    }
    
    convenience init(with error: Error?, dismissOnTouch: Bool = false, dismissWithGesture: Bool = false, image: Image? = nil) {
        if let error = error as? PicPayErrorDisplayable {
            self.init(title: error.title, text: error.description)
        } else {
            self.init(title: "Ops", text: error?.localizedDescription)
        }
        self.image = image
        self.dismissOnTouchBackground = dismissOnTouch
        self.dismissWithGesture = dismissWithGesture
    }
    
    init(title: NSAttributedString?, text: NSAttributedString) {
        self.title = title
        self.text = text
    }
    
    init(with title: NSAttributedString?, text: NSAttributedString, buttons: [Button]? = nil) {
        self.title = title
        self.text = text
        self.textType = .plain
        guard let buttonsArray = buttons else {
            return
        }
        self.buttons = buttonsArray
    }
    
    required public init?(json: JSON) {
        if let title = json["title"].string {
            self.title = NSAttributedString(string: title)
        }
        
        if let text = json["text"]["value"].string {
            self.text = NSAttributedString(string: text)
        }
        
        self.image = Image(json: json["image"])
        
        if let textType = TextType(rawValue: json["text"]["type"].string ?? "") {
            self.textType = textType
        }
        
        if let buttons = json["buttons"].array?.compactMap({ Button(json: $0) }) {
            self.buttons = buttons
        }
        
        if let dismissWithGesture = json["dismiss_with_gesture"].bool {
            self.dismissWithGesture = dismissWithGesture
        }
        
        if let showCloseButton = json["show_close_button"].bool {
            self.showCloseButton = showCloseButton
        }
        
        if let dismissOnTouchBackground = json["dismiss_on_touch_background"].bool {
            self.dismissOnTouchBackground = dismissOnTouchBackground
        }
    }
    
    required init(with title: String, text: String, buttons: [Button]? = nil, image: Image? = nil) {
        self.title = NSAttributedString(string: title)
        self.text = NSAttributedString(string: text)
        self.textType = .plain
        if let buttonsArray = buttons {
            self.buttons = buttonsArray
        }
        if let image = image {
            self.image = image
        }
    }
    
    convenience init?(dictionary: [String: Any]) {
        let json = JSON(dictionary)
        self.init(json: json)
    }
}
