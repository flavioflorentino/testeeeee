import Foundation

enum CardVerificationLocalizable: String, Localizable {
    case verificationSuccessCompletion
    case cancelVerification
    case confirmValue
    case cancelVerificationPopUpTitle
    case cancelVerificationPopUpDesc
    
    var file: LocalizableFile {
        return .cardVerification
    }
    
    var key: String {
        return self.rawValue
    }
}
