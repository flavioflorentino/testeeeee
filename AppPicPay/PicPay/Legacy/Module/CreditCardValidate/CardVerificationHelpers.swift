import UIKit
import FeatureFlag

final class CardVerificationHelpers: NSObject {
    static func controller(withModel viewModel: CardVerificationViewModel, forStatus: CardVerificationStatus? = nil) -> UIViewController {
        
        // Start Flow
        guard let status = forStatus else {
            let controller = CardVerificationViewController(viewModel: viewModel)
            return controller
        }
        
        switch status.status {
            
        case .verifiable:
            let controller = CardVerificationIntroViewController.controllerFromStoryboard()
            controller.viewModel = viewModel
            return controller
            
        case .waitingFillValue:
            let controller = CardVerificationFormController.controllerFromStoryboard()
            controller.viewModel = viewModel
            return controller
            
        default:
            break
        }
        
        // Fallback
        let controller = CardVerificationViewController(viewModel: viewModel)
        return controller
    }
    
    static func show(from: UIViewController, withModel viewModel: CardVerificationViewModel, forStatus: CardVerificationStatus? = nil){
        if viewModel.card.verifyStatus == .notAvailable {
            guard let cardVerification = FeatureManager.object(.cardVerification, type: CardVerificationRemoteConfig.self) else {
                Log.debug(Log.Domain.view, "Decodable Parse Error: Não foi possível converter json em objeto")
                return
            }
            
            let alert = Alert(title: cardVerification.failure.title, text: cardVerification.failure.description)
            alert.image = Alert.Image(with: #imageLiteral(resourceName: "creditCardValidationFail"), styleType: .square)
            alert.buttons.append(Button(title: cardVerification.failure.primaryButtonText ?? DefaultLocalizable.btOkUnderstood.text, type: .cta, action: .close))
            AlertMessage.showAlert(alert, controller: from.tabBarController ?? from) { (popup, buttonModel, button) in
                popup.dismiss(animated: true, completion: nil)
            }
        } else {
            CardVerificationHelpers.showPopupForVerificationStatus(from: from.tabBarController ?? from) {
                let controller = CardVerificationHelpers.controller(withModel: viewModel, forStatus: forStatus)
                controller.hidesBottomBarWhenPushed = true
                if let nav = from as? UINavigationController {
                    nav.pushViewController(controller, animated: true)
                }
            }
        }
    }
    
    private static func showPopupForVerificationStatus(from parentController: UIViewController, onTappedInVerify: @escaping () -> Void) {
        let alert: Alert = Alert(
            title: DefaultLocalizable.checkYourCard.text,
            text: DefaultLocalizable.paymentsWithUnverifiedCards.text
        )

        let verifyNowButton = Button(title: DefaultLocalizable.checkNow.text, type: .cta) { popupController, _ in
            PPAnalytics.trackEvent("Onboarding de Verificação de Cartão ACEITOU", properties: nil)
            popupController.dismiss(animated: true, completion: {
                onTappedInVerify()
            })
        }
        let verifyLaterButton = Button(title: DefaultLocalizable.checkLater.text, type: .clean) { popupController, _ in
            PPAnalytics.trackEvent("Onboarding de Verificação de Cartão RECUSOU", properties: nil)
            popupController.dismiss(animated: true, completion: nil)
        }
        
        alert.image = Alert.Image(name: "creditCardValidationOnboarding", style: .standard, source: .asset)
        alert.buttons = [verifyNowButton, verifyLaterButton]
        AlertMessage.showAlert(alert, controller: parentController)
    }
}
