import UI
import CoreLegacy
import UIKit

final class CardVerificationFormController: BaseFormController {
    var viewModel: CardVerificationViewModel?
    
    @IBOutlet weak var topTextLabel: UILabel!
    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var confirmationButton: UIPPButton!
    @IBOutlet weak var bottomTextView: UITextView!
    @IBOutlet weak var bottomTextLabel: UILabel!
    @IBOutlet weak var floatCoinLabel: UILabel!
    @IBOutlet weak var lineOfValueView: UIView!
    @IBOutlet weak var containerValue: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    static func controllerFromStoryboard() -> CardVerificationFormController {
        return CardVerificationFormController.controllerFromStoryboard(.creditCardVerification)
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurationValuesForLabels()
        configureView()
    }
    
    @IBAction private func confirmationButtonTapped(_ sender: Any) {
        sendVerification()
    }
    
    @IBAction private func valueTextChange(_ sender: UITextField) {
        guard let text = sender.text, !text.isEmpty, text != "0,0" else {
            UIView.animate(withDuration: 0.25, animations: {
                self.errorLabel.isHidden = false
                self.containerValue.layoutIfNeeded()
            })
            confirmationButton.isEnabled = false
            lineOfValueView.backgroundColor = Palette.ppColorNegative300.color
            floatCoinLabel.textColor = Palette.ppColorNegative300.color
            sender.text = nil
            return
        }
        UIView.animate(withDuration: 0.25) {
            if !self.errorLabel.isHidden {
                self.errorLabel.isHidden = true
                self.containerValue.layoutIfNeeded()
            }
        }
        confirmationButton.isEnabled = true
        lineOfValueView.backgroundColor = Palette.ppColorBranding300.color
        floatCoinLabel.textColor = Palette.ppColorBranding300.color
        sender.text = CurrencyFormatter.currencyInputBrazilianStringFromString(with: text).replacingOccurrences(of: "R$ ", with: "")
    }
    
    // MARK: - TextfieldDelegate
    
    internal func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        if URL.absoluteString == ":cancel" {
            dismissKeyboard()
            showPopupForProccessCanceled()
        }
        return false
    }
    
    // MARK: - Keyboard Events Handler
}

extension CardVerificationFormController {
    /// Send value with value for card verification
    private func sendVerification() {
        guard let valueString = valueTextField.text?.replacingOccurrences(of: ".", with: "").replacingOccurrences(of: ",", with: ".") else {
            return
        }
        guard let value = Double(valueString.trim()) else {
            return
        }
        startLoadingView()
        viewModel?.sendVerfication(with: value) { [weak self] error in
            guard let strongSelf = self else {
                return
            }
            strongSelf.stopLoadingView()
            if let error = error as? PicPayErrorDisplayable {
                if error.picpayCode == "30091" {
                    PPAnalytics.trackEvent("Verificação de cartão CONFIRMOU", properties: ["Status": "Falhou todas as tentativas"])
                    strongSelf.showErrorForManyAttempts(with: error)
                } else {
                    AlertMessage.showCustomAlertWithError(error, controller: strongSelf)
                    PPAnalytics.trackEvent("Verificação de cartão CONFIRMOU", properties: ["Status": "Valor incorreto"])
                }
            } else {
                PPAnalytics.trackEvent("Verificação de cartão CONFIRMOU", properties: ["Status": "Sucesso"])
                strongSelf.completedVerificationOfCard()
            }
        }
    }
    
    /// Configure behavior for card verification completed
    private func completedVerificationOfCard() {
        guard showCompletionViewController() else {
            return
        }
        
        let completionController = CompletionInformationViewController(
            with: CardVerificationLocalizable.verificationSuccessCompletion.text,
            image: #imageLiteral(resourceName: "cardStatusVerified")
        )
        
        completionController.onCloseAction = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            strongSelf.navigationController?.popToRootViewController(animated: true)
        }
        completionController.autoDismissAction = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            strongSelf.navigationController?.popToRootViewController(animated: true)
        }
        navigationController?.fadeTo(completionController)
    }
    
    /// Cofigure behavior for many attemps of card verifycation
    ///
    /// - Parameter error: picpay error
    private func showErrorForManyAttempts(with error: PicPayErrorDisplayable) {
        guard showCompletionViewController() else {
            return
        }
        
        let controller = CompletionInformationViewController(
            with: error.title,
            image: #imageLiteral(resourceName: "creditCardValidationFail"),
            description: error.description,
            showCloseButton: true,
            isAutoDismiss: false
        )
        controller.onCloseAction = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            strongSelf.navigationController?.popToRootViewController(animated: true)
        }
        navigationController?.fadeTo(controller)
    }
    
    private func showCompletionViewController() -> Bool {
        if let willFinishCallback = viewModel?.willFinishVerification, let nav = navigationController {
            let showCompletion = willFinishCallback(nav)
            return showCompletion
        }
        return true
    }
    
    /// Configure view with verification step for hide or not visual elements
    private func configureView() {
        valueTextField.keyboardType = .numberPad
        
        bottomTextLabel.textColor = Palette.ppColorGrayscale400.color
        
        bottomTextView.textContainer.lineFragmentPadding = 0
        bottomTextView.textContainerInset = .zero
        bottomTextView.textColor = Palette.ppColorGrayscale400.color
        
        confirmationButton.isEnabled = false
        
        containerValue.isHidden = false
        containerValue.backgroundColor = Palette.ppColorGrayscale000.color
        
        containerView.backgroundColor = Palette.ppColorGrayscale000.color
        
        configureActionButtons()
    }
    
    private func configurationValuesForLabels() {
        guard let cardVerification = viewModel?.verificationStatus else {
            return
        }
        let text = "<span>\(cardVerification.text)</span>"
        topTextLabel.attributedText = text.html2Attributed(
            font: UIFont.systemFont(ofSize: 13),
            color: Palette.ppColorGrayscale500.color
        )
        containerValue.isHidden = viewModel?.isHiddenValueTextField ?? false
        configureBottomText()
        configureActionButtons()
    }
    
    private func configureBottomText() {
        guard let cardVerification = viewModel?.verificationStatus else {
            return
        }
        if cardVerification.status == .waitingFillValue {
            let attributedString = NSMutableAttributedString(
                string: CardVerificationLocalizable.cancelVerification.text,
                attributes: [
                    .font: UIFont.systemFont(ofSize: 12.0, weight: .regular),
                    .foregroundColor: Palette.ppColorGrayscale400.color
                ]
            )
            
            /// mount link dor cancel action in text
            attributedString.addAttributes(
                [
                    .link: ":cancel",
                    .font: UIFont.systemFont(ofSize: 12.0, weight: .bold),
                    .foregroundColor: Palette.ppColorBranding300.color
                ],
                range: NSRange(location: 46, length: 11)
            )
            bottomTextView.attributedText = attributedString
            bottomTextView.backgroundColor = Palette.ppColorGrayscale000.color
            bottomTextView.isHidden = false
            bottomTextView.textColor = Palette.ppColorGrayscale400.color
            bottomTextView.isUserInteractionEnabled = true
            bottomTextLabel.textColor = Palette.ppColorGrayscale400.color
            
            if let bottomTextAttributed = cardVerification.infoText.html2Attributed(UIFont.systemFont(ofSize: 12)) {
                let attributedStringBottomText = NSMutableAttributedString()
                attributedStringBottomText.append(bottomTextAttributed)
                attributedStringBottomText.addAttributes(
                    [
                        .font: UIFont.systemFont(ofSize: 12.0, weight: .regular),
                        .foregroundColor: Palette.ppColorGrayscale400.color
                    ],
                    range: NSRange(location: 0, length: bottomTextAttributed.string.count)
                )
                
                bottomTextLabel.attributedText = attributedStringBottomText
            }
        } else {
            bottomTextView.isHidden = true
            bottomTextLabel.attributedText = cardVerification.infoText.html2Attributed(
                font: UIFont.systemFont(ofSize: 12),
                color: Palette.ppColorGrayscale500.color
            )
        }
    }
    
    private func configureActionButtons() {
        confirmationButton.setTitle(CardVerificationLocalizable.confirmValue.text, for: .normal)
        confirmationButton.isEnabled = false
    }
    
    /// Show popup fo cancel action
    ///
    /// this popup is mounted in api return
    @objc
    private func showPopupForProccessCanceled() {
        let alert = Alert(title: CardVerificationLocalizable.cancelVerificationPopUpTitle.text, text: CardVerificationLocalizable.cancelVerificationPopUpDesc.text)
        let cancelButton = Button(title: DefaultLocalizable.btYes.text, type: .destructive) { [weak self] popupController, _ in
            popupController.dismiss(animated: true, completion: {
                self?.cancelVerify()
            })
        }
        let closeButton = Button(title: DefaultLocalizable.btNo.text, type: .cta, action: .close)
        alert.buttons = [cancelButton, closeButton]
        AlertMessage.showAlert(alert, controller: self)
    }
    
    private func cancelVerify() {
        startLoadingView()
        viewModel?.cancelVerify { [weak self] alert, error in
            guard let strongSelf = self else {
                return
            }
            strongSelf.stopLoadingView()
            if let error = error {
                AlertMessage.showCustomAlertWithError(error, controller: strongSelf)
                return
            } else if let alert = alert {
                PPAnalytics.trackEvent("Verificação de cartão CANCELADA", properties: nil)
                alert.dismissWithGesture = false
                alert.showCloseButton = false
                alert.dismissOnTouchBackground = false
                AlertMessage.showAlert(alert, controller: strongSelf.tabBarController, action: { popupController, _, _ in
                    popupController.dismiss(animated: true, completion: {
                        strongSelf.navigationController?.popToRootViewController(animated: true)
                    })
                })
            }
        }
    }
}
