import UI
import UIKit

final class CardVerificationViewController: PPBaseViewController {
    var viewModel: CardVerificationViewModel
    
    // MARK: - View Properties
    
    lazy var loadingView: UILoadView = {
        return UILoadView(superview: self.view)
    }()
    
    // MARK: - Initializer
    
    init(viewModel: CardVerificationViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Lifecyle
    
    override func loadView() {
        super.loadView()
        self.view = UIView(frame: UIScreen.main.bounds)
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadStatus()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    // MARK: - Iternal Methods
    
    fileprivate func setup() {
        navigationItem.title = DefaultLocalizable.checkCard.text
    }
    
    private func loadStatus() {
        loadingView.startLoading()
        viewModel.verificationStatus { [weak self] error in
            guard let strongSelf = self else {
            return
        }
            strongSelf.loadingView.animatedRemoveFromSuperView()
            if let error = error {
                AlertMessage.showAlert(error, controller: strongSelf)
                let errorView = ConnectionErrorView(frame: strongSelf.view.frame)
                strongSelf.view.addSubview(errorView)
            } else {
                self?.showCurrentStatusViewController()
            }
        }
    }
    
    private func showCurrentStatusViewController() {
        guard let status = viewModel.verificationStatus else {
            return
        }
        
        let controller = CardVerificationHelpers.controller(withModel: viewModel, forStatus: status)
        navigationController?.fadeReplace(self, by: controller)
    }
}
