import UI
import UIKit

final class CardVerificationIntroViewController: PPBaseViewController {
    private var coordinator: Coordinating?
    var viewModel: CardVerificationViewModel?
    
    @IBOutlet weak var topTextLabel: UILabel!
    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var actionButton: UIPPButton!
    @IBOutlet weak var bottomTextView: UITextView!
    @IBOutlet weak var bottomTextLabel: UILabel!
    @IBOutlet weak var floatCoinLabel: UILabel!
    @IBOutlet weak var lineOfValueView: UIView!
    @IBOutlet weak var containerValue: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    static func controllerFromStoryboard() -> CardVerificationIntroViewController {
        return CardVerificationIntroViewController.controllerFromStoryboard(.creditCardVerification)
    }
    
    // MARK: - View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: - User Actions
    
    @IBAction private func buttonAction(_ sender: Any) {
        startVerification()
    }
    
    // MARK: - Internal Methods
    
    private func setup() {
        viewModel?.viewController = self
        setupColors()
        configureTopText()
        configureBottomText()
    }
    
    private func setupColors() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        containerView.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    private func configureTopText() {
        guard let cardVerification = viewModel?.verificationStatus else {
            return
        }
        let text = "<span>\(cardVerification.text)</span>"
        topTextLabel.attributedText = text.html2Attributed(
            font: UIFont.systemFont(ofSize: 13),
            color: Palette.ppColorGrayscale600.color
        )
    }
    
    private func configureBottomText() {
        bottomTextView.isHidden = true
        guard let cardVerification = viewModel?.verificationStatus else {
            return
        }
        bottomTextLabel.attributedText = cardVerification.infoText.html2Attributed(
            font: UIFont.systemFont(ofSize: 12),
            color: Palette.ppColorGrayscale400.color
        )
    }
    
    private func startVerification() {
        actionButton.startLoadingAnimating()
        viewModel?.startVerification(onSuccess: { [weak self] in
            self?.actionButton.stopLoadingAnimating()
            PPAnalytics.trackEvent("Verificação de Cartão INICIOU", properties: nil)
            self?.goToNextStep()
        }, onError: { [weak self] error in
            self?.actionButton.stopLoadingAnimating()
            AlertMessage.showAlert(error, controller: self)
        })
    }
    
    private func goToNextStep() {
        guard let model = viewModel else {
            return
        }
        let controller = CardVerificationHelpers.controller(withModel: model, forStatus: model.verificationStatus)
        navigationController?.fadeReplace(self, by: controller)
    }
}


extension CardVerificationIntroViewController: CardVerificationViewModelDisplay {
    func cvvFlowCoordinator(completedCvv: @escaping (String) -> Void, completedNoCvv: @escaping () -> Void) {
        guard let navigationController = navigationController else {
            return
        }
        
        let coordinator = CvvRegisterFlowCoordinator(
            navigationController: navigationController,
            paymentType: .cardVerification,
            finishedCvv: completedCvv,
            finishedWithoutCvv: completedNoCvv
        )
        coordinator.start()
        
        self.coordinator = coordinator
    }
}

extension CardVerificationIntroViewController: StyledNavigationDisplayable {}
