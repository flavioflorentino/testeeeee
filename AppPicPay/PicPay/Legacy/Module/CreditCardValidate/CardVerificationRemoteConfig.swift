import Foundation

struct CardVerificationRemoteConfig: Decodable {
    let start: CardVerificationScreen
    let success: CardVerificationScreen
    let failure: CardVerificationScreen
    
    enum CodingKeys: String, CodingKey {
        case start = "start_screen"
        case success = "success_screen"
        case failure = "failure_screen"
    }
}

struct CardVerificationScreen: Decodable {
    let title: String
    let description: String
    let primaryButtonText: String?
    let secondButtonText: String?
    let helpCenterUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case title
        case description
        case primaryButtonText = "primary_button_text"
        case secondButtonText = "secondary_button_text"
        case helpCenterUrl = "helpcenter_url"
    }
}
