import AnalyticsModule
import UI

final class PaginationCoordinator: NSObject {
    // MARK: Private vars
    fileprivate let paginator = ControllerPagination()
    fileprivate var mainTabBarController: MainTabBarController? {
        didSet {
            setupPages()
            setup()
        }
    }
    fileprivate var scannerVc: ScannerBaseViewController?
    private static var privateShared: PaginationCoordinator?
    
    // MARK: Public vars
    @objc
    static var shared: PaginationCoordinator {
        guard let auxShared = self.privateShared else {
            privateShared = PaginationCoordinator()
            return privateShared!
        }
        return auxShared
    }
    
    /// Destroy singleton of final class
    class func destroy() {
        self.privateShared = nil
    }
    
    // MARK: Lifecycle
    override init(){
        super.init()
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: NotificationName.paymentNew), object: nil, queue: nil) { [weak self] (_) in
            DispatchQueue.main.async {
                // If paid, move to the tabbar page
                self?.paginator.setPage(.application, animated: false)
            }
        }
    }
    
    // MARK: Public funcs
    @objc
    func setTabController(mainTab: MainTabBarController){
        paginator.clear()
        self.mainTabBarController = mainTab
        PaginationCoordinator.shared.showHome(animated: false)
    }
    
    @objc
    func mainScreen() -> UIViewController {
        return paginator
    }
    
    @objc
    func showHome(animated: Bool = true) {
        paginator.setPage(.application, animated: animated)
    }
    
    @objc
    func showScanner() {
        paginator.setPage(.camera, animated: true)
    }
    
    func showMyQrCode() {
        paginator.setPage(.camera, animated: false)
        guard let scannerBaseController = paginator.childrens.first?.viewController as? ScannerBaseViewControllerProtocol else {
            return
        }
        scannerBaseController.maximizeMyCodeView()
    }
    
    // MARK: Private funcs
    private func setupPages() {
        scannerVc = ScannerBaseViewController()
        guard let scannerVc = scannerVc else {
            return
        }
        paginator.add(child: scannerVc)
        
        paginator.didChange = { page in
            if case .camera = page {
                Analytics.shared.log(UserEvent.qrCodeAccessed(type: .swipe, origin: .home))
            }
        }

        if let vc = mainTabBarController {
            paginator.add(child: vc, withNavigationController: mainTabBarController?.homeNavigation())
        }
    }
    
    private func setup() {
        NotificationCenter.default.addObserver(self, selector: #selector(didChangeTabs(notification:)), name: NSNotification.Name(MainTabBarController.TabNotifications.TabChanged.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(initialTab(notification:)), name: NSNotification.Name(MainTabBarController.TabNotifications.InitialTab.rawValue), object: nil)
        
        paginator.initialPage = .application

        paginator.isVisible = { [weak self] pages in
            self?.scannerVc?.view.backgroundColor = Palette.ppColorGrayscale600.color
        }
    }
    
    // MARK: MainTabbar notifications
    @objc
    private func didChangeTabs(notification: Notification) {
        guard
            let viewController = notification.userInfo?["viewController"] as? UIViewController,
            let tabBarController = notification.userInfo?["tabBarController"] as? MainTabBarController else {
            return
        }
        checkEnablePaginatorForTabController(tabBarController, viewController: viewController)
    }
    
    @objc
    private func initialTab(notification: Notification) {
        guard
            let viewController = notification.userInfo?["viewController"] as? UIViewController,
            let tabBarController = notification.userInfo?["tabBarController"] as? MainTabBarController else {
            return
        }
        checkEnablePaginatorForTabController(tabBarController, viewController: viewController)
    }
    
    private func checkEnablePaginatorForTabController(_ tabBarController: MainTabBarController, viewController: UIViewController) {
        configurePagination()
    }
    
    func configurePagination() {
        let tabIndex = mainTabBarController?.selectedIndex
        let targetIndexes: [MainTabBarController.Tab] = [.home, .paymentSearch]
        let isHomeOrPaymentTab = targetIndexes
            .map { $0.rawValue }
            .contains(tabIndex)
        
        let isRoot = AppManager.shared.mainScreenCoordinator?
            .currentController()
            .navigationController?
            .viewControllers.count == 1
        let enabled = isHomeOrPaymentTab && isRoot
        enabled ? paginator.enable() : paginator.disable()
    }
}

