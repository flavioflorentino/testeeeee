#import "PPNavigationController.h"
#import "PicPay-Swift.h"

@implementation PPNavigationController

- (instancetype)init {
    self = [super init];
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    if (self.visibleViewController != nil) {
        return self.visibleViewController.preferredStatusBarStyle;
    }

    return UIApplication.sharedApplication.statusBarStyle;
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if ([[self.viewControllers lastObject] conformsToProtocol:@protocol(PPNavigationBarProtocol)]) {
        UIViewController <PPNavigationBarProtocol> *currentNavigable = (UIViewController <PPNavigationBarProtocol> *) [self.viewControllers lastObject];
        
        if ([viewController conformsToProtocol:@protocol(PPNavigationBarProtocol)]) {
            UIViewController <PPNavigationBarProtocol> *nextNavigable = (UIViewController <PPNavigationBarProtocol> *) viewController;
            
            if ([currentNavigable navigationBarIsTransparent] && ![nextNavigable navigationBarIsTransparent]) {
                [currentNavigable setShouldShowBarWhenDisappear:YES];
            }
        }
        
    }

    [super pushViewController:viewController animated:animated];
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated {
    NSInteger count = [self.viewControllers count];
    if ( count > 1 && [[self.viewControllers lastObject] conformsToProtocol:@protocol(PPNavigationBarProtocol)]) {
        UIViewController <PPNavigationBarProtocol> *currentNavigable = (UIViewController <PPNavigationBarProtocol> *) [self.viewControllers lastObject];
        
        UIViewController *viewController = [self.viewControllers objectAtIndex:count-2];
        if ([viewController conformsToProtocol:@protocol(PPNavigationBarProtocol)]) {
            UIViewController <PPNavigationBarProtocol> *nextNavigable = (UIViewController <PPNavigationBarProtocol> *) viewController;
            
            if ([currentNavigable navigationBarIsTransparent] && ![nextNavigable navigationBarIsTransparent]) {
                [currentNavigable setShouldShowBarWhenDisappear:YES];
            }
        }
        
    }
    return [super popViewControllerAnimated: animated];
}

@end
