#import <UIKit/UIKit.h>

@interface PPNavigationController: UINavigationController
@end

@protocol PPNavigationBarProtocol
- (BOOL)navigationBarIsTransparent;
- (void)setShouldShowBarWhenDisappear:(BOOL) showBar;
- (BOOL)shouldShowBarWhenDisappear;

@end
