//
//  PPBaseViewControllerObj.m
//  PicPay
//
//  Created by Marcos Timm on 01/02/2018.
//

#import "PPBaseViewControllerObj.h"
#import "PicPay-Swift.h"

@interface PPBaseViewControllerObj()
    @property BOOL shouldShowBarWillDisappear;
@end

@implementation PPBaseViewControllerObj

#pragma mark - StatusBar
- (UIStatusBarStyle)preferredStatusBarStyle {
    return [AppStyles statusBarStyle];
}

#pragma mark - PPNavigationBarProtocol

- (BOOL) navigationBarIsTransparent {
    return NO;
}
- (void) setShouldShowBarWhenDisappear:(BOOL) showBar{
    self.shouldShowBarWillDisappear = showBar;
}
- (BOOL) shouldShowBarWhenDisappear{
    return self.shouldShowBarWillDisappear;
}

#pragma mark - Breadcrumb

@dynamic breadcrumbProperties, breadcrumbDescription, breadcrumbType, shouldAddBreadcrumb;

- (BOOL) shouldAddBreadcrumb {
    if (self.navigationController != nil || self.tabBarController != nil || self.presentingViewController != nil){
        return YES;
    }
    return NO;
}

- (CrumbType) breadcrumbType {
    return CrumbTypeScreen;
}

- (NSString *) breadcrumbDescription {
    NSString * description = [[[[self class] description] componentsSeparatedByString:@"."] lastObject];
    return  description !=nil ? description : [[self class] description];
}

- (NSDictionary *) breadcrumbProperties {
    return nil;
}

#pragma mark - Orientations Support

- (BOOL)shouldAutorotate{
    return false;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - View Life Cycle

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // register only the main viewController
    if ([self shouldAddBreadcrumb]) {
        [[BreadcrumbManager shared] addCrumb:self];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.shouldShowBarWillDisappear) {
        [self.navigationController setNavigationBarHidden:NO animated:animated];
        [self setShouldShowBarWhenDisappear:NO];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.navigationBarIsTransparent){
        [self.navigationController setNavigationBarHidden:YES animated:animated];
    }
    
    // iOS 13 Bug - Verificar se nas atualizações futuras do iOS ainda é necessário manter esse fix.
    // https://forums.developer.apple.com/message/378841#378841
    if (@available(iOS 13, *)) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController.navigationBar setNeedsLayout];
        });
    }
}

@end

