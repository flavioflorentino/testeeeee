//
//  PPBaseTableViewController.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 25/07/2018.
//

import UIKit

class PPBaseTableViewController: UITableViewController, PPNavigationBarProtocol, Breadcrumbable {
    
    // MARK: - Breadcrumb properties
    var shouldAddBreadcrumb: Bool {
        if (parent is UINavigationController || parent is UITabBarController || self.presentingViewController != nil){
            return true
        }
        return false
    }
    
    var breadcrumbType: BreadcrumbManager.CrumbType {
        return .screen
    }
    
    var breadcrumbDescription: String {
        return String(describing: type(of: self)).components(separatedBy: ".").last ?? String(describing: type(of: self))
    }
    
    var breadcrumbProperties: [String : String]? {
        return nil
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return AppStyles.statusBarStyle()
    }
    
    // MARK: - PPNavigationBarProtocol
    func navigationBarIsTransparent() -> Bool {
        return false
    }
    
    var _shouldShowBarWhenDisappear = false;
    func setShouldShowBarWhenDisappear(_ showBar: Bool) {
        _shouldShowBarWhenDisappear = showBar;
    }
    
    func shouldShowBarWhenDisappear() -> Bool {
        return _shouldShowBarWhenDisappear
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // register only the main viewController
        if self.shouldAddBreadcrumb {
            BreadcrumbManager.shared.addCrumb(self)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if shouldShowBarWhenDisappear() {
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
            setShouldShowBarWhenDisappear(false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if navigationBarIsTransparent() {
            self.navigationController?.setNavigationBarHidden(true, animated: animated)
        }
    }

}
