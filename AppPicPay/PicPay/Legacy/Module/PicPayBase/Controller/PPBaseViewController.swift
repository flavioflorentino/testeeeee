//
//  PPBaseViewController.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 01/08/16.
//
//
import Foundation
import UI
import UIKit

class PPBaseViewController: UIViewController, Breadcrumbable, PPNavigationBarProtocol {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return AppStyles.statusBarStyle()
    }
    
    // MARK: - PPNavigationBarProtocol
    func navigationBarIsTransparent() -> Bool {
        return false
    }
    
    var _shouldShowBarWhenDisappear = false;
    func setShouldShowBarWhenDisappear(_ showBar: Bool) {
        _shouldShowBarWhenDisappear = showBar;
    }
    
    func shouldShowBarWhenDisappear() -> Bool {
        return _shouldShowBarWhenDisappear
    }
    
    // MARK: - Breadcrumb properties
    
    var shouldAddBreadcrumb: Bool {
        if (parent is UINavigationController || parent is UITabBarController || self.presentingViewController != nil){
            return true
        }
        return false
    }
    
    var breadcrumbType: BreadcrumbManager.CrumbType {
        return .screen
    }
    
    var breadcrumbDescription: String {
        return String(describing: type(of: self)).components(separatedBy: ".").last ?? String(describing: type(of: self))
    }
    
    var breadcrumbProperties: [String : String]? {
        return nil
    }
    
    fileprivate lazy var loadingView: LoadingView = {
        let view = LoadingView()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: - UI SDK properties
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    // MARK: - View Life
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // register only the main viewController
        if self.shouldAddBreadcrumb {
            BreadcrumbManager.shared.addCrumb(self)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if shouldShowBarWhenDisappear() {
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
            setShouldShowBarWhenDisappear(false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if navigationBarIsTransparent() {
            self.navigationController?.setNavigationBarHidden(true, animated: animated)
        }
    }
    
    func configureLoadingView(text: String? = nil, textColor: UIColor? = nil, activityIndicatorStyle: UIActivityIndicatorView.Style? = nil, backgroundColor: UIColor? = nil) {
        loadingView.text = text
        loadingView.setTextColor(textColor)
        loadingView.setActivityIndicatorStyle(activityIndicatorStyle ?? .gray)
        loadingView.backgroundColor = backgroundColor ?? Palette.ppColorGrayscale000.color
    }
    
    func startLoadingView() {
        loadingView.alpha = 1
        view.addSubview(loadingView)
        NSLayoutConstraint.constraintAllEdges(from: loadingView, to: view)
    }
    
    func stopLoadingView() {
        UIView.animate(withDuration: 0.3, animations: {
            self.loadingView.alpha = 0
        }, completion: { completed in
            guard completed else {
                return
            }
            self.loadingView.removeFromSuperview()
        })
    }
}
