import AnalyticsModule
import Core
import CoreLegacy
import DirectMessage
import DirectMessageSB
import FeatureFlag
import PermissionsKit
import UI
import UIKit
import Store

enum MainTabIdentifier: String {
    case pay
}

enum PaymentTabIdentifier: String {
    case main = "Principais"
    case place = "Locais"
    case store = "Store"
    
    var tabIndex: UInt {
        switch self {
        case .main:
            return 0
        case .place:
            return 1
        case .store:
            return 2
        }
    }
}

@objc protocol MainScreenCoordinator : AnyObject {
    func currentController() -> UIViewController
    func mainScreen() -> UIViewController
    func homeNavigation() -> UINavigationController
    func notificationsNavigation() -> UINavigationController
    func paymentSearchNavigation() -> UINavigationController
    func settingsNavigation() -> UINavigationController
    func walletNavigation() -> UINavigationController?
    
    func showHomeScreen()
    func showPaymentSearchScreen(searchText: String?, page: String?, ignorePermissionPrompt: Bool)
    func showSearchScreen(searchText: String)
    func showPaymentScreen()
    func showNotificationsScreen()
    func showSettingsScreen()
    func showWalletScreen()
    func showStoreScreen()

    func showLocalsScreen()

    func handle(deeplink: PPDeeplink)
}

final class MainTabBarController: UITabBarController, MainScreenCoordinator {
    private var middleNavigationController: UINavigationController?
    private var middleViewController: SearchWrapper?
    private let settingsCoordinator = SettingsCoordinator()
    private var permissionsCoordinator: PermissionsCoordinating?
    private var shouldStartPermissionsCoordinator: Bool = true
    private let container = DependencyContainer()

    private var homeCoordinator: HomeCoordinator?
    private var transactionsCoordinator: TransactionsCoordinator?

    var previousIndex: Int?
    
    enum Tab: Int {
        case home = 0
        case wallet = 1
        case paymentSearch = 2
        case notifications = 3
        case store = 4
        
        var eventType: UserEvent.TabItemType {
            switch self {
            case .home:
                return .home
            case .wallet:
                return .wallet
            case .paymentSearch:
                return .payment
            case .notifications:
                return .notifications
            case .store:
                return .store
            }
        }
    }
    
    enum TabNotifications: String {
        case TabChanged = "TabChanged"
        case InitialTab = "InitialTab"
    }
    
    // MARK: - Public Properties
    
    var initialTab: Tab = .home
    
    public var isPanelAnalyticsVisible: Bool {
        return tabBar.superview?.subviews.contains(where: { $0 === loggerBadgeButton }) ?? false
    }
    
    // MARK: - Private Properties
    
    private var firstAppear: Bool = true
    private let emailCheckManager:EmailStatusCheckManager? = EmailStatusCheckManager.shared
    private var isShowingPopupToEnableBiometricAuthentication = false
    private lazy var auth = PPAuth()
    
    private lazy var loggerBadgeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("👀", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 24)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(tapLogger), for: .touchUpInside)
        button.backgroundColor = Palette.ppColorBranding300.color
        button.layer.shadowColor = Palette.ppColorGrayscale600.cgColor
        button.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        button.layer.shadowOpacity = 0.2
        button.layer.shadowRadius = 4.0
        button.layer.cornerRadius = 32
        return button
    }()
    
    private lazy var storeTooltip: TooltipView = {
        let tooltip = TooltipView(verticalOrientation: .bottom, horizontalOrientation: .right, tip: .store)
         return tooltip
    }()
    
    private lazy var settingsTooltip: TooltipView = {
         let tooltip = TooltipView(verticalOrientation: .top, horizontalOrientation: .left, tip: .settings)
         return tooltip
    }()
    
    // MARK: - StatusBar Methods
    
    override var childForStatusBarStyle: UIViewController? {
        return selectedViewController
    }
    
    override var childForStatusBarHidden: UIViewController? {
        return selectedViewController
    }
    
    // MARK: - Tab Controllers

//    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
//        super.traitCollectionDidChange(previousTraitCollection)
//
//        if #available(iOS 13.0, *) {
//            if traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
//                print("ok")
//            }
//        } else {
//            // Fallback on earlier versions
//        }
//    }
    
    fileprivate lazy var _paymentSearchNavigation: UINavigationController = {
        let navigationController = UINavigationController()
        let tabBarTitle = container.featureManager.isActive(.experimentNewLabelPayButtonTabBarBool) ?
            DefaultLocalizable.pay.text :
            "   "
        navigationController.tabBarItem = UITabBarItem(
            title: tabBarTitle,
            image: nil,
            tag: Tab.paymentSearch.rawValue
        )

        guard container.featureManager.isActive(.experimentNewTransactionScreenBool) else {
            let paymentViewController = SearchWrapper(
                ignorePermissionPrompt: false,
                dependencies: container,
                shouldShowSearchBar: !container.featureManager.isActive(.experimentSearchHomeBool)
            )
            paymentViewController.origin = .pay

            navigationController.viewControllers = [paymentViewController]

            middleViewController = paymentViewController
            middleNavigationController = navigationController

            return navigationController
        }

        transactionsCoordinator = TransactionsCoordinator(
            navigationController: navigationController,
            factory: TransactionsFactory(),
            dependencies: DependencyContainer(),
            billetPasteboardInteractor: BilletPasteboardInteractor(),
            pixPasteboardInteractor: PixPasteboardInteractor()
        )

        transactionsCoordinator?.start()

        middleNavigationController = navigationController

        return navigationController
    }()
    
    private lazy var _notificationsNavigation: UINavigationController = {
        let notificationNavigationController = ViewsManager.walletStoryboardViewController(withIdentifier: "NotificationsNavigationController") as! UINavigationController
        notificationNavigationController.tabBarItem = UITabBarItem(
            title: DefaultLocalizable.notifications.text,
            image: Assets.NewIconography.Tabbar.newIconNotification.image,
            tag: Tab.notifications.rawValue)
        notificationNavigationController.tabBarItem.selectedImage = Assets.NewIconography.Tabbar.newIconNotificationActive.image
        return notificationNavigationController
    }()
    
    private lazy var _homeNavigation: UINavigationController = {
        // Home
        let homeNavigationController = StyledNavigationController()
        homeNavigationController.tabBarItem = UITabBarItem(
            title: DefaultLocalizable.start.text,
            image: Assets.NewIconography.Tabbar.newIconHome.image,
            tag: Tab.home.rawValue)
        homeNavigationController.tabBarItem.selectedImage = Assets.NewIconography.Tabbar.newIconHomeActive.image

        guard container.featureManager.isActive(.experimentSearchHomeBool) else {
            let homeViewController = LegacyHomeViewController(dependencies: container)
            homeNavigationController.viewControllers = [homeViewController]

            return homeNavigationController
        }

        let homeCoordinator = HomeCoordinator(
            navigationController: homeNavigationController,
            factory: HomeFactory(),
            dependencies: container
        )
        homeCoordinator.start()

        self.homeCoordinator = homeCoordinator

        return homeNavigationController
    }()
    
    private lazy var _walletNavigation: UINavigationController? = {
        // Wallet
        let walletViewController = ViewsManager.paymentMethodsStoryboardViewController(withIdentifier: "PaymentMethodsViewController")
        guard let paymentMethodsVC = walletViewController as? PaymentMethodsViewController else {
            return nil
        }
        paymentMethodsVC.viewModel.flowContext = .wallet
        
        let walletNavigationController = StyledNavigationController(rootViewController: paymentMethodsVC)
        walletNavigationController.tabBarItem = UITabBarItem(
            title: DefaultLocalizable.wallet.text,
            image: Assets.NewIconography.Tabbar.newIconWallet.image,
            tag: Tab.wallet.rawValue)
        walletNavigationController.tabBarItem.selectedImage = Assets.NewIconography.Tabbar.newIconWalletActive.image

        return walletNavigationController
    }()
    
    private lazy var _storeNavigation: UINavigationController = {
        
        let searchViewController: SearchGenericViewController = SearchGenericViewController(dependencies: DependencyContainer())
        
        searchViewController.tabType = SearchWrapperTabType.store.searchTab
        searchViewController.isSearch = true
        searchViewController.shouldDisplayTypeIcons = true
        searchViewController.shouldSaveRecents = false
        searchViewController.isPersonRequestingPayment = false
        
        let searchNavigationBar: SearchNavigationBar = SearchNavigationBar()
        searchNavigationBar.ignoreStatusBar = false
        searchNavigationBar.setPlaceholder(placeholder: DefaultLocalizable.storeSearchPlaceholder.text)
        searchNavigationBar.setLeftButtonImage(image: Assets.NewIconography.Navigationbar.newIconQrcode.image)
        searchNavigationBar.setTwitterKeyboard()
        searchNavigationBar.leftButtonTapped = {
            PaginationCoordinator.shared.showScanner()
            Analytics.shared.log(ScannerEvent.qrcodeAccessed(.pay))
        }
        searchNavigationBar.searchTextShouldBeginEditing = {
            Analytics.shared.log(
                SearchEvent.searchStarted(id: UUID(), tabName: PaymentTabIdentifier.store.rawValue, origin: .store)
            )
        }
        let homeScreenId = FeatureManager.text(.featureStoreHomeScreenId)
        let storeViewController: UIViewController = StoreHomeFactory.make(homeId: homeScreenId, legacyStoreHandler: DGHelpers(), searchHeader: searchNavigationBar, searchResult: searchViewController)
        
        let storeNavigationController = UINavigationController(rootViewController: storeViewController)
        storeNavigationController.tabBarItem = UITabBarItem(
            title: DefaultLocalizable.store.text,
            image: Assets.NewIconography.Tabbar.newIconStore.image,
            tag: Tab.paymentSearch.rawValue)
        storeNavigationController.tabBarItem.selectedImage = Assets.NewIconography.Tabbar.newIconStoreActive.image
        return storeNavigationController
    }()
    
    
    
    // MARK: - Initializer
    
    @objc class func controllerFromStoryboard() -> MainTabBarController {
        let controller: MainTabBarController = UIStoryboard.viewController(.mainTabBar)
        AppManager.shared.mainScreenCoordinator = controller
        return controller
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        configureTabBar()
        setupObservers()
        connectDirectMessageBroker()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if shouldStartPermissionsCoordinator, let selectedViewController = selectedViewController {
            shouldStartPermissionsCoordinator.toggle()

            permissionsCoordinator = PermissionsFactory(dependencies: DependencyContainer())
                .makePermissionsCoordinator(mainController: selectedViewController)
            permissionsCoordinator?.didFinishFlow = { [weak self] in
                self?.permissionsCoordinator = nil
            }

            permissionsCoordinator?.start()
        }

        presentTooltips()
        guard
            FeatureManager.isActive(.appTour),
            KVStore().boolFor(.showAppTour)
            else {
                askForBiometricAutentication()
                return
        }
        
        presentTutorial()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateBadges()
        
        if firstAppear {
            firstAppear = false
            if let viewController = self.viewControllers?[initialTab.rawValue] {
                fireInitialTabNotification(viewController: viewController)
            }
        }
    }
    
    private func presentTutorial() {
        KVStore().setBool(false, with: .showAppTour)
        
        let controller = AppTourFactory.make(delegate: self)
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        present(controller, animated: true)
    }
    
    func presentTooltips() {

        let storeCountedTimes = KVStore().intFor(KVKey.featureShowStoreTooltipQuantity)
        let storeTarget = FeatureManager.int(.storeTooltipShowCount)
        
        if  storeCountedTimes < storeTarget {
            if storeCountedTimes > 0 {
                setupStoreTooltipView()
                Analytics.shared.log(StoreTooltipEvent.didViewStoreTooltip(count: storeCountedTimes))
            }
            KVStore().setInt(storeCountedTimes + 1, with:KVKey.featureShowStoreTooltipQuantity)
        }

        let settingsCountedTimes = KVStore().intFor(KVKey.featureShowSettingsTooltipQuantity)
        let settingsTarget = FeatureManager.int(.storeTooltipShowCount)
        
        if settingsCountedTimes < settingsTarget {
            if settingsCountedTimes > 0 {
                setupSettingsTooltipView()
                Analytics.shared.log(StoreTooltipEvent.didViewSettingsTooltip(count: settingsCountedTimes))
            }
            KVStore().setInt(settingsCountedTimes + 1, with:KVKey.featureShowSettingsTooltipQuantity)
        }
    }
    
    func setupStoreTooltipView() {
        guard let superView = tabBar.superview else { return }
        defer { storeTooltip.startApearrence() }
        
        superView.addSubview(storeTooltip)
        storeTooltip.snp.makeConstraints { (make) in
            make.bottom.equalTo(view.compatibleSafeArea.bottom).inset(Spacing.base06 + Spacing.base01)
            make.trailing.equalToSuperview().inset(Spacing.base02 + Spacing.base00)
        }
    }
    
    func setupSettingsTooltipView() {
        guard let superView = tabBar.superview else { return }
        defer { settingsTooltip.startApearrence() }
        
        superView.addSubview(settingsTooltip)
        settingsTooltip.snp.makeConstraints { (make) in
            make.top.equalTo(view.compatibleSafeArea.top).inset(Spacing.base05)
            make.leading.equalToSuperview().inset(Spacing.base05 + Spacing.base01)
        }
    }
    
    private func executeSavedDeeplink() {
        let homeNavigationController = homeNavigation()
        if let deepLink = DynamicLinkHelper.getDeepLinkToRegistration() {
            PPAnalytics.trackEvent("DynamicLinkFirebase", properties: ["deepLink": deepLink.absoluteString], includeMixpanel: true)
            DeeplinkHelper.handleDeeplink(withUrl: deepLink, from: homeNavigationController)
            DynamicLinkHelper.removeDeepLinkToRegistration()
        } else if let deeplink = DeeplinkHelper.retrieveSavedDeepLink() {
            DeeplinkHelper.handleDeeplink(withUrl: deeplink, from: homeNavigationController)
        }
    }
    
    private func askForBiometricAutentication() {
        let askForBiometricAuthentication = PPAuth.shouldAskForBiometricAuthentication()
        let hasOnboardSocial = AppParameters.global().getOnboardSocial()
        guard
            !isShowingPopupToEnableBiometricAuthentication,
            currentController().presentingViewController == nil,
            askForBiometricAuthentication, !hasOnboardSocial
            else {
                executeSavedDeeplink()
                return
        }
        isShowingPopupToEnableBiometricAuthentication = true
        auth.biometricAuthenticationWelcomeAlert(for: navigationController?.tabBarController ?? self) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.executeSavedDeeplink()
            }
        }
    }
    
    // MARK: - Public Methods
    public func updatePanelAnalytics(_ isEnabled: Bool) {
        guard let superView = tabBar.superview, isEnabled else {
            return loggerBadgeButton.removeFromSuperview()
        }
        
        superView.addSubview(loggerBadgeButton)
        
        NSLayoutConstraint.activate([
            loggerBadgeButton.bottomAnchor.constraint(equalTo: superView.compatibleSafeAreaLayoutGuide.bottomAnchor, constant: -89),
            loggerBadgeButton.trailingAnchor.constraint(equalTo: superView.trailingAnchor, constant: -16),
            loggerBadgeButton.widthAnchor.constraint(equalToConstant: 64),
            loggerBadgeButton.heightAnchor.constraint(equalToConstant: 64)
        ])
    }
    
    private func updateBadges() {
        updateNotificationsBadge()
        updateWalletNotificationsBadge()
    }
    
    // MARK: - Internal Methods
    
    private func configureTabBar(){
        // tabbar style
        tabBar.unselectedItemTintColor = Colors.grayscale600.color
        tabBar.tintColor = Colors.branding900.color.withCustomDarkColor(.white)
        tabBar.barTintColor = Palette.ppColorGrayscale000.color(withCustomDark: .tabBar)
        tabBar.isTranslucent = false
        tabBar.isOpaque = true
        
        guard let wallet = _walletNavigation else {
            return
        }
        // config tab bar controllers
        self.viewControllers = [_homeNavigation, wallet,  _paymentSearchNavigation, _notificationsNavigation, _storeNavigation]
        self.setSelectedIndex(initialTab.rawValue)
        
        // set the middle tabBarItem
        if let mainTabBar = tabBar as? MainTabBar {
            mainTabBar.middleTabBarItem = _paymentSearchNavigation.tabBarItem
            mainTabBar.middleButtonDelegate = self
            mainTabBar.updateMiddleIcon()
        }
        
    }
    
    private func setupObservers() {
        
        // check for change notifications changes
        NotificationCenter.default.addObserver(
            forName: AppManager.SessionData.unreadNotificationsChanged,
            object: nil,
            queue: nil
        ) { [weak self] (_) in
            DispatchQueue.main.async {
                self?.updateNotificationsBadge()
            }
        }

        // check for change wallet notifications changes
        NotificationCenter.default.addObserver(
            forName: AppManager.SessionData.unreadWalletNotificationsChanged,
            object: nil,
            queue: nil
        ) { [weak self] (_) in
            DispatchQueue.main.async {
                self?.updateWalletNotificationsBadge()
            }
        }
        
        // check for new payments
        NotificationCenter.default.addObserver(
            forName: Notification.Name.Payment.new,
            object: nil,
            queue: nil
        ) { [weak self] (_) in
            DispatchQueue.main.async {
                self?.homeNavigation().popToRootViewController(animated: false)
                self?.showHomeScreen()
            }
        }
    }
    
    private func connectDirectMessageBroker() {
        guard container.featureManager.isActive(.featureDirectMessageEnabled) else { return }
        let dmSocket = DMSocketFactory.make()
        container.socketManager.connect(dmSocket)
    }
    
    @objc
    private func tapLogger() {
        guard let currentViewController = AppManager.shared.mainScreenCoordinator?.currentController() else {
            return
        }
        
        let navigation = UINavigationController(rootViewController: AnalyticsTrackingFactory.make())
        currentViewController.present(navigation, animated: true)
    }
    
    // MARK: - UI Updates
    
    private func updateNotificationsBadge() {
        if viewControllers?.count > Tab.notifications.rawValue, let notificationsController = viewControllers?[Tab.notifications.rawValue] {
            let count = AppManager.shared.sessionData.unreadNotificationsCount
            if count > 0 {
                notificationsController.tabBarItem.badgeValue = String(format:"%d", count)
            }else{
                notificationsController.tabBarItem.badgeValue = nil
            }
            
        }
    }

    private func updateWalletNotificationsBadge() {
        if viewControllers?.count > Tab.wallet.rawValue, let walletController = viewControllers?[Tab.wallet.rawValue] {
            let badgeValue = AppManager.shared.sessionData.unreadWalletNotificationsLabel
            walletController.tabBarItem.badgeValue = badgeValue.isEmpty ? nil : badgeValue
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle
                && interfaceStylesAreNotUndefined(previousTraitCollection: previousTraitCollection)
                && UIApplication.shared.applicationState != .background {
                Analytics.shared.log(UserEvent.darkModeOn(traitCollection.userInterfaceStyle == .dark))
            }
        }
    }
    
    private func interfaceStylesAreNotUndefined(previousTraitCollection: UITraitCollection?) -> Bool {
        if #available(iOS 13.0, *) {
            return traitCollection.userInterfaceStyle != .unspecified && previousTraitCollection?.userInterfaceStyle != .unspecified
        }
        return false
    }
    
    // MARK: - MainScreenCoordinator
    
    // When we set selectedIndex = index the delegate for didSelectViewController: is not called
    // This functions encapsulates the asignment itself and calls the delegate function
    private func setSelectedIndex(_ index: Int) {
        selectedIndex = index
        
        if let viewController = self.viewControllers?[index] {
            didChangeTab(viewController: viewController)
        }
    }
    
    func currentController() -> UIViewController {
        guard let selectedViewController = selectedViewController else { return self }
        let topMostPresentedController = topMostPresentedViewController(controller: selectedViewController)
        guard let navigation = topMostPresentedController as? UINavigationController else {
            return topMostPresentedController ?? self
        }
        return navigation.visibleViewController ?? self
    }
    
    func mainScreen() -> UIViewController{
        return self
    }
    
    func notificationsNavigation() -> UINavigationController {
        return _notificationsNavigation
    }
    
    func walletNavigation() -> UINavigationController? {
        return _walletNavigation
    }
    
    func homeNavigation() -> UINavigationController {
        return _homeNavigation
    }
    
    func homeViewController() -> LegacyHomeViewController{
        return _homeNavigation.viewControllers[0] as? LegacyHomeViewController ?? LegacyHomeViewController(dependencies: container)
    }
    
    func paymentSearchNavigation() ->UINavigationController {
        return _paymentSearchNavigation
    }
    
    func settingsNavigation() -> UINavigationController {
        _homeNavigation
    }
    
    func showHomeScreen() {
        setSelectedIndex(Tab.home.rawValue)
    }
    
    func showSettingsScreen() {
        setSelectedIndex(Tab.home.rawValue)
    }

    func showStoreScreen() {
        setSelectedIndex(Tab.store.rawValue)
    }
    
    func showWalletScreen() {
        setSelectedIndex(Tab.wallet.rawValue)
    }
    
    func showPaymentScreen() {
        middleViewController?.showContainerSearch = false
        setSelectedIndex(Tab.paymentSearch.rawValue)
    }
    
    func showSearchScreen(searchText: String) {
        middleViewController?.showContainerSearch = true
        middleViewController?.initialSearch = searchText
        
        setSelectedIndex(Tab.paymentSearch.rawValue)
    }
    
    func showPaymentSearchScreen(searchText: String? = nil, page: String? = nil, ignorePermissionPrompt: Bool = false) {
        middleViewController?.initialSearch = searchText
        middleViewController?.initialPage = page
        middleViewController?.ignorePermissionPrompt = ignorePermissionPrompt
        if middleNavigationController?.viewControllers.last != middleViewController {
            middleNavigationController?.popToRootViewController(animated: true)
        }
        
        previousIndex = selectedIndex
        setSelectedIndex(Tab.paymentSearch.rawValue)

        transactionsCoordinator?.resume()
    }
    
    func showNotificationsScreen() {
        setSelectedIndex(Tab.notifications.rawValue)
        _notificationsNavigation.popToRootViewController(animated: false)
    }

    func showLocalsScreen() {
        showPaymentScreen()

        guard container.featureManager.isActive(.experimentNewTransactionScreenBool) else {
            if let presentedViewController = paymentSearchNavigation().topViewController?.presentedViewController {
                presentedViewController.dismiss(animated: true)
            }

            let viewController = paymentSearchNavigation().viewControllers.first as? SearchWrapper
            viewController?.selectSearchTabBar(tab: .places)

            return
        }

        transactionsCoordinator?.open(deeplink: .local)
    }

    func handle(deeplink: PPDeeplink) {
        switch deeplink.type {
        case .payPeople:
            showPaymentScreen()
            transactionsCoordinator?.open(deeplink: .payConsumer)
        default:
            return
        }
    }
    
    private func topMostPresentedViewController(controller: UIViewController) -> UIViewController? {
        guard let presentedController = controller.presentedViewController else {
            return controller
        }
        return topMostPresentedViewController(controller: presentedController)
    }
    
    private func fireTabChangeNotification(viewController: UIViewController) {
        NotificationCenter.default.post(
            name: NSNotification.Name(rawValue: MainTabBarController.TabNotifications.TabChanged.rawValue),
            object: nil,
            userInfo: [
                "tabBarController": self,
                "viewController": viewController
            ]
        )
    }
    
    private func fireInitialTabNotification(viewController: UIViewController) {
        NotificationCenter.default.post(
            name: NSNotification.Name(rawValue: MainTabBarController.TabNotifications.InitialTab.rawValue),
            object: nil,
            userInfo: [
                "tabBarController": self,
                "viewController": viewController
            ]
        )
    }
    
    private func didChangeTab(viewController: UIViewController) {
        self.updateBadges()
        
        fireTabChangeNotification(viewController: viewController)
        
        if selectedIndex == Tab.home.rawValue && previousIndex == Tab.home.rawValue {
            homeViewController().scrollTop()
        }
    }
}

extension MainTabBarController: AppTourCoordinatorDelegate {
    func didFinishTutorial() {
        askForBiometricAutentication()
    }
}

extension MainTabBarController : MainTabBarDelegate {
    func tabBarDidTapOnMiddleItem() {
        showPaymentSearchScreen()
        middleViewController?.selectMainTabBar(idTab: "main")
        Analytics.shared.log(UserEvent.tabSelected(type: .payment))
    }
}

extension MainTabBarController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        previousIndex = tabBarController.selectedIndex
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        didChangeTab(viewController: viewController)
        
        guard var eventType = Tab(rawValue: selectedIndex)?.eventType else {
            return
        }

        Analytics.shared.log(UserEvent.tabSelected(type: eventType))
    }
}

extension MainTabBarController: PaginationChild {
    func childWillAppear() {
        if let viewController = self.viewControllers?[selectedIndex] {
            viewController.viewWillAppear(true)
        }
    }
    
    func childDidAppear() {
        if let viewController = self.viewControllers?[selectedIndex] {
            viewController.viewDidAppear(true)
        }
    }
    
    func childWillDisappear() {
        if let viewController = self.viewControllers?[selectedIndex] {
            viewController.viewWillDisappear(true)
        }
    }
}

extension MainTabBarController {
    @objc
    func switchToPaymentTabIfNecessary() {
        guard
            let flagDict = FeatureManager.json(.featureOpenAppTabV2),
            let rawTab = flagDict["tab"] as? String,
            let _ = MainTabIdentifier(rawValue: rawTab)
            else {
                return
        }
        
        _homeNavigation.tabBarItem.title = DefaultLocalizable.activities.text
        showPaymentScreen()
    }
}
