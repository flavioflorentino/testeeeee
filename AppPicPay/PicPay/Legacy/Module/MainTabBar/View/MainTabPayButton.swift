import AnalyticsModule
import FeatureFlag
import Foundation
import UI
import UIKit

private extension MainTabPayButton.Layout {
    enum Image {
        static let size = CGSize(width: 16.0, height: 26.0)
        static let newSize = CGSize(width: 12.0, height: 20.0)

        static let origin = CGPoint(x: .zero, y: 6.0)
    }

    enum Label {
        static let font: UIFont = .systemFont(ofSize: 10.0, weight: .medium)
    }
}

final class MainTabPayButton: UIControl {
    typealias Dependencies = HasFeatureManager & HasAnalytics

    fileprivate enum Layout { }

    // MARK: - Properties
    private let dependencies: Dependencies

    private let selectedBackgroundColor: UIColor = Colors.branding900.change(.dark, to: .white).color
    private let unselectedBackgroundColor: UIColor = Colors.branding600.color
    
    private let selectedTintColor: UIColor = Colors.white.color
    private let unselectedTintColor: UIColor = Colors.white.change(.dark, to: Colors.white.lightColor).color

    private lazy var circleView: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .full))
            .with(\.backgroundColor, unselectedBackgroundColor)

        view.addSubviews(imageView, label)

        return view
    }()
    
    private lazy var imageView: UIImageView = {
        let image = Assets.NewIconography.Tabbar.newIconPayment.image.imageMaskedAndTinted(with: Colors.white.color)
        let imageView = UIImageView(image: image)
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFit
        
        return imageView
    }()
    
    private lazy var label: UILabel = {
        let label = UILabel()
        label.backgroundColor = .clear
        label.font = Layout.Label.font
        label.text = DefaultLocalizable.pay.text
        label.textColor = unselectedTintColor
        label.textAlignment = .center

        label.isHidden = dependencies.featureManager.isActive(.experimentNewLabelPayButtonTabBarBool)

        return label
    }()
    
    override var isSelected: Bool {
        didSet {
            updateSelected()
        }
    }
    
    // MARK: - Initialization
    required init(dependencies: Dependencies) {
        self.dependencies = dependencies

        super.init(frame: .zero)
        buildLayout()
    }
    
    override init(frame: CGRect) {
        self.dependencies = DependencyContainer()

        super.init(frame:frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Layout
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        updateSelected()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        updateLayout()
    }
}

// MARK: - ViewConfiguration
extension MainTabPayButton: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(circleView)
    }

    func setupConstraints() {
        circleView.snp.makeConstraints {
            if dependencies.featureManager.isActive(.experimentNewLabelPayButtonTabBarBool) {
                $0.edges.equalToSuperview().inset(Spacing.base00)
            } else {
                $0.edges.equalToSuperview()
            }
        }

        imageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()

            if dependencies.featureManager.isActive(.experimentNewLabelPayButtonTabBarBool) {
                $0.centerY.equalToSuperview()
                $0.size.equalTo(Layout.Image.newSize)
            } else {
                $0.size.equalTo(Layout.Image.size)
                $0.top.equalToSuperview().offset(Layout.Image.origin.y)
            }
        }

        label.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom)
            $0.centerX.equalToSuperview()
            $0.leading.greaterThanOrEqualToSuperview()
            $0.trailing.bottom.lessThanOrEqualToSuperview()
        }
    }

    func configureViews() {
        backgroundColor = .clear
    }
}

// MARK: - Private
private extension MainTabPayButton {
    func updateSelected() {
        let backgroundColor = isSelected ? selectedBackgroundColor : unselectedBackgroundColor
        circleView.backgroundColor = backgroundColor

        let tintColor = isSelected ? selectedTintColor : unselectedTintColor
        imageView.image = Assets.NewIconography.Tabbar.newIconPayment.image.imageMaskedAndTinted(with: tintColor)
        label.textColor = tintColor
    }

    func updateLayout() {
        circleView.layer.cornerRadius = circleView.bounds.height / 2.0
    }
}
