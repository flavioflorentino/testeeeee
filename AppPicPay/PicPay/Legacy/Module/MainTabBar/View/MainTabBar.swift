import FeatureFlag
import Foundation
import UIKit

protocol MainTabBarDelegate : AnyObject {
    func tabBarDidTapOnMiddleItem()
}

private extension MainTabBar.Layout {
    enum MiddleButton {
        static let size = CGSize(width: 54.0, height: 54.0)
        static let newSize = CGSize(width: 48.0, height: 48.0)
        static let centerY: CGFloat = 18.0
        static let newCenterY: CGFloat = 8.0
    }
}

final class MainTabBar: UITabBar {
    typealias Dependencies = HasFeatureManager

    fileprivate enum Layout { }
    
    var middleTabBarItem: UITabBarItem?
    weak var middleButtonDelegate: MainTabBarDelegate?

    private lazy var dependencies: Dependencies = DependencyContainer()
    
    lazy private var middleButton : UIControl =  {
        let size: CGSize = dependencies.featureManager.isActive(.experimentNewLabelPayButtonTabBarBool) ?
            Layout.MiddleButton.newSize :
            Layout.MiddleButton.size

        let centerY: CGFloat = dependencies.featureManager.isActive(.experimentNewLabelPayButtonTabBarBool) ?
            Layout.MiddleButton.newCenterY :
            Layout.MiddleButton.centerY

        let _button = MainTabPayButton(frame: CGRect(origin: .zero, size: size))
        _button.center = CGPoint(x: UIScreen.main.bounds.width / 2, y: centerY)
        _button.addTarget(self, action: #selector(middleButtonAction), for: .touchUpInside)
        _button.accessibilityIdentifier = "middleButton"
        
        return _button
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupMiddleButton()
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if isHidden {
            return nil
        }

        let from = point
        let to = middleButton.center
        
        return sqrt((from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y)) <= 39 ? middleButton : super.hitTest(point, with: event)
    }
    
    func setupMiddleButton() {
        addSubview(middleButton)
        middleButton.layer.zPosition = 10000
    }
    
    @objc func middleButtonAction() {
        middleButtonDelegate?.tabBarDidTapOnMiddleItem()
    }
    
    override var selectedItem: UITabBarItem? {
        didSet {
            updateMiddleIcon()
        }
    }
    
    override var isHidden: Bool {
        didSet {
            middleButton.isHidden = isHidden
            middleButton.isEnabled = !isHidden
        }
    }
    
    public func updateMiddleIcon() {
        middleButton.isSelected = middleTabBarItem == selectedItem
    }
}
