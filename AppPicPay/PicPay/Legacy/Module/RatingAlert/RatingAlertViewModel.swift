import Core

protocol RatingAlertViewModelInputs {
    func updateRateNumber(_ rate: Int)
    func createAndShowThankYouPopup()
}

protocol RatingAlertViewModelOutputs: AnyObject {
    func enableSendButton()
    func disableSendButton()
    func showThankYouPopup(storeReviewURL: URL?)
}

protocol RatingAlertViewModelType: AnyObject {
    var inputs: RatingAlertViewModelInputs { get }
    var outputs: RatingAlertViewModelOutputs? { get set }
}

final class RatingAlertViewModel: RatingAlertViewModelType, RatingAlertViewModelInputs {
    var inputs: RatingAlertViewModelInputs { return self }
    weak var outputs: RatingAlertViewModelOutputs?
    
    private var rate = 0
    
    private lazy var appStoreReviewUrl: URL? = {
        guard let appStoreId = Environment.appStoreId else {
            return nil
        }
        let urlString = WebServiceInterface.apiEndpoint(KWsUrlAppStoreWriteReviewLink.replacingOccurrences(of: "{appStoreId}", with: appStoreId)) ?? ""
        return URL(string: urlString)
    }()
    
    private var shouldShowStoreReview: Bool {
        return rate == 5
    }
    
    func updateRateNumber(_ rate: Int) {
        self.rate = rate
        rate > 0 ? outputs?.enableSendButton() : outputs?.disableSendButton()
    }
    
    func createAndShowThankYouPopup() {
        let storeReviewUrl = shouldShowStoreReview ? appStoreReviewUrl : nil
        outputs?.showThankYouPopup(storeReviewURL: storeReviewUrl)
    }
}
