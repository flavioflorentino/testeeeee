import UI
import UIKit

final class RatingAlertViewController: LegacyViewController<RatingAlertViewModelType, RatingAlertCoordinating, UIView> {
    struct Layout {
        static let margin: CGFloat = 20
        static let buttonHeight: CGFloat = 48
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = RatingAlertLocalizable.ratingAlertTitle.text
        label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.text = RatingAlertLocalizable.ratingAlertMessage.text
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = Palette.ppColorGrayscale400.color
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var starsRatingView: StarsRatingView = {
        let view = StarsRatingView()
        view.delegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var sendButton: UIPPButton = {
        let button = UIPPButton()
        button.configure(with: Button(title: DefaultLocalizable.btSend.text, type: .cta))
        button.cornerRadius = Layout.buttonHeight / 2
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(didTapSendButton), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    private lazy var cancelButton: UIPPButton = {
        let button = UIPPButton()
        button.configure(with: Button(title: RatingAlertLocalizable.notNow.text, type: .cleanUnderlined))
        button.cornerRadius = Layout.buttonHeight / 2
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(didTapCancelButton), for: .touchUpInside)
        return button
    }()
 
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(messageLabel)
        view.addSubview(starsRatingView)
        view.addSubview(sendButton)
        view.addSubview(cancelButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 24),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.margin),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Layout.margin),
            
            messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8),
            messageLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.margin),
            messageLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Layout.margin),
            
            starsRatingView.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 16),
            starsRatingView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.margin + 20),
            starsRatingView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Layout.margin - 20),
            
            sendButton.topAnchor.constraint(equalTo: starsRatingView.bottomAnchor, constant: 24),
            sendButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.margin),
            sendButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Layout.margin),
            sendButton.heightAnchor.constraint(equalToConstant: Layout.buttonHeight),
            
            cancelButton.topAnchor.constraint(equalTo: sendButton.bottomAnchor, constant: 12),
            cancelButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.margin),
            cancelButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Layout.margin),
            cancelButton.heightAnchor.constraint(equalToConstant: Layout.buttonHeight),
            cancelButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20)
        ])
    }
    
    private func showThankYouMessage() {
        viewModel.inputs.createAndShowThankYouPopup()
    }
    
    @objc
    private func didTapSendButton() {
        showThankYouMessage()
    }
    
    @objc
    private func didTapCancelButton() {
        coordinator.perform(action: .close)
    }
}

extension RatingAlertViewController: StarsRatingViewDelegate {
    func didTapStar(rating: Int) {
        viewModel.inputs.updateRateNumber(rating)
    }
}

extension RatingAlertViewController: RatingAlertViewModelOutputs {
    func enableSendButton() {
        sendButton.isEnabled = true
    }
    
    func disableSendButton() {
        sendButton.isEnabled = false
    }
    
    func showThankYouPopup(storeReviewURL: URL?) {
        coordinator.perform(action: .showThankYouPopup(storeReviewURL))
    }
}
