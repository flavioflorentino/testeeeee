final class RatingAlertFactory {
    static func make() -> RatingAlertViewController {
        let viewModel: RatingAlertViewModelType = RatingAlertViewModel()
        var coordinator: RatingAlertCoordinating = RatingAlertCoordinator()
        let viewController = RatingAlertViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController
        
        return viewController
    }
}
