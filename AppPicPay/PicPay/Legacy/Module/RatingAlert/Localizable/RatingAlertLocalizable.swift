import Foundation

enum RatingAlertLocalizable: String, Localizable {
    case ratingAlertTitle
    case ratingAlertMessage
    case notNow
    case thankYouAlertTitle
    case thankYouAlertMessage
    case thankYouAlertExtraMessage
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .ratingAlert
    }
}
