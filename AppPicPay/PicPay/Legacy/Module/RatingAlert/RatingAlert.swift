final class RatingAlert {
    typealias Seconds = Int
    
    static private var wasAskedForReview = false
    
    @objc
    static func presentRatingPopup(withDelay delay: Seconds) {
        guard !RatingAlert.wasAskedForReview else {
            return
        }
        RatingAlert.wasAskedForReview = true
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(delay)) {
            let popupController = RatingAlertFactory.make()
            let topViewController = AppManager.shared.mainScreenCoordinator?.currentController()
            topViewController?.showPopup(popupController)
        }
    }
}
