enum RatingAlertAction {
    case close
    case showThankYouPopup(URL?)
}

protocol RatingAlertCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: RatingAlertAction)
}

final class RatingAlertCoordinator: RatingAlertCoordinating {
    weak var viewController: UIViewController?

    func perform(action: RatingAlertAction) {
        switch action {
        case .close:
            viewController?.dismiss(animated: true)
            
        case .showThankYouPopup(let url):
            dismissAndShowAlert(storeReviewURL: url)
        }
    }
    
    private func thankYouAlert(shouldShowStoreReview: Bool) -> Alert {
        let buttonOk = Button(title: DefaultLocalizable.btOk.text, type: .cta, action: .close)
        var message = RatingAlertLocalizable.thankYouAlertMessage.text
        if shouldShowStoreReview {
            message += RatingAlertLocalizable.thankYouAlertExtraMessage.text
        }
        let alert = Alert(with: RatingAlertLocalizable.thankYouAlertTitle.text, text: message, buttons: [buttonOk])
        return alert
    }
    
    private func dismissAndShowAlert(storeReviewURL: URL?) {
        viewController?.dismiss(animated: true) {
            let topViewController = AppManager.shared.mainScreenCoordinator?.currentController()
            let alert = self.thankYouAlert(shouldShowStoreReview: storeReviewURL != nil)
            
            AlertMessage.showAlert(alert, controller: topViewController) { _, _, _ in
                guard let url = storeReviewURL else {
                    return
                }
                self.openStoreReview(url: url)
            }
        }
    }
    
    private func openStoreReview(url writeReviewURL: URL) {
        UIApplication.shared.open(writeReviewURL)
    }
}
