import AnalyticsModule
import Core
import FeatureFlag
import Lottie
import PermissionsKit
import SnapKit
import Store
import SwiftyJSON
import UI
import UIKit
import SearchKit

protocol SearchGenericViewControllerDelegate: AnyObject {
    func getAnalyticsID() -> UUID?
}

final class SearchGenericViewController: PPBaseViewController {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private var dependencies: Dependencies

    weak var delegate: SearchGenericViewControllerDelegate?

    private enum TabType: String {
        case main
        case places
    }

    static let NotificationChangePermissionStatus = "PermissionChangeStatus"

    // MARK: Outlets
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundSecondary.color
        return view
    }()
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0.01))
        tableView.backgroundColor = Colors.backgroundPrimary.color
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        
        // Cells
        let nibStore = UINib(nibName: "SearchListCell", bundle: Bundle.main)
        tableView.register(nibStore, forCellReuseIdentifier: "SearchListCell")
        
        tableView.register(LoadingTableViewCell.self, forCellReuseIdentifier: "LoadingCell")
        tableView.register(StoresMapTableViewCell.self, forCellReuseIdentifier: "StoresMapTableViewCell")
        tableView.register(SearchHightlightTableViewCell.self, forCellReuseIdentifier: "HightlightTableViewCell")
        tableView.register(SearchRecentTableViewCell.self, forCellReuseIdentifier: "SearchRecentTableViewCell")
        return tableView
    }()
    fileprivate var keyboardHeight: CGFloat {
        return KeyboardManager.shared.keyboardHeight
    }
    
    // MARK: Private vars
    var viewModel = SearchViewModel(dependencies: DependencyContainer())
    fileprivate var profileViewController: UIViewController?
    fileprivate let refreshControl = UIRefreshControl()
    fileprivate var legacyErrorView: EmptySearchView?
    fileprivate var legacyNoResultsView: EmptySearchView?
    fileprivate var errorView: SearchErrorView?
    fileprivate var mapCell: StoresMapTableViewCell?

    private var establishmentDetailsCoordinator: EstablishmentDetailsCoordinating?
    
    private var itemSelected: Int?
    private lazy var locationPermission: Permission = .locationWhenInUse
    private lazy var contactsPermission: Permission = .contacts
    
    var shouldDisplayTypeIcons: Bool = false
    
    // MARK: Public vars
    var didAskForPermissions: (() -> ())?
    var didSearchFinished: ((String, Bool) -> Void)?
    var shouldAskPermission: (() -> (Bool))?
    var isSearch: Bool = false
    var shouldSaveRecents: Bool = false
    var shouldSetupModelWhenLoaded = true
    var isPersonRequestingPayment = false {
        didSet {
            viewModel.isPersonRequestingPayment = isPersonRequestingPayment
        }
    }
    private var transitionDelegate: BottomSheetTransitioningDelegate?
    private var bottomSheetCellIndexPath: IndexPath?
    
    var tabType: SearchTab? {
        didSet {
            guard let tab = tabType else {
                return
            }
            viewModel.group = tab.group
        }
    }
    
    var term: String {
        return viewModel.term
    }
    
    // When the user taps "Ver todos" we pass this additional param to update the model accordingly
    var linkedFrom: String? {
        didSet {
            viewModel.group = linkedFrom
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    
    // MARK: Factory
    static func controllerFromStoryboard() -> SearchGenericViewController {
        return SearchGenericViewController(dependencies: DependencyContainer())
    }
    
    // If the server didn't return any results AND the user didn't grant all permissions
    // should we show the "no permissions" placeholder or "no results"? For digital goods for example
    // it's better to show "no results", but for places it's better to ask for permissions in case
    // the user didn't grant location access
    var arePermissionsImportant = true
    
    lazy var permissionLocationView: BaseListOnboardView = {
        let locationFooter = BaseListOnboardView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0.0))
        locationFooter.imageView.image = UIImage(named: "ilu-location-auth")
        locationFooter.titleLabel.text = SearchLocalizable.establishmentAlertTitle.text
        locationFooter.titleLabel.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        locationFooter.titleLabel.textColor = Colors.black.color
        locationFooter.titleLeadingContraint.constant = 24
        locationFooter.titleTrailingConstraint.constant = 24
        
        locationFooter.textLabel.text = SearchLocalizable.establishmentAlertText.text
        locationFooter.textLabel.font = UIFont.systemFont(ofSize: 13)
        locationFooter.textLabel.textColor = Colors.grayscale800.color
        locationFooter.textLeadingConstraint.constant = 24
        locationFooter.textTrailingContraint.constant = 24
        
        locationFooter.buttonView.setTitle(SearchLocalizable.authorize.text, for: .normal)
        locationFooter.buttonLeadingConstraint.constant = 40
        locationFooter.buttonTrailingConstraint.constant = 40
        locationFooter.buttonView.addTarget(self, action: #selector(self.requestLocationPermission), for: .touchUpInside)
        return locationFooter
    }()
    
    lazy var contactsFooterView: ContactsPermissionFooterView = {
        let contactsFooter = ContactsPermissionFooterView()
        contactsFooter.authorizeAction = { [weak self] in
            self?.requestContactPermision()
        }

        return contactsFooter
    }()
    
    // MARK: Lifecycle
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
        viewModel.isSearch = self.isSearch
        
        let dismissKeyboardGesture = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        dismissKeyboardGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(dismissKeyboardGesture)
        
        automaticallyAdjustsScrollViewInsets = false
        
        // Register observer for permission status change
        NotificationCenter.default.addObserver(self, selector: #selector(permissionChanged), name: NSNotification.Name(rawValue: SearchGenericViewController.NotificationChangePermissionStatus), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkPermission), name: NSNotification.Name.Permissions.didChange, object: nil)
        
        setupViews()
        setupCallbacks()
        
        if shouldSetupModelWhenLoaded {
            viewModel.setup()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkPermission()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !self.isSearch {
            if let name = self.tabType?.name {
                DispatchQueue.main.asyncAfter(wallDeadline: .now() + 0.25) {
                    
                    PPAnalytics.trackFirstTimeOnlyEvent("FT Pagar - " + name, properties: ["breadcrumb": BreadcrumbManager.shared.breadcrumb])
                }
            }
        }
        
        lineView.isHidden = false
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        setupTableFooterView()
    }
    
    private func setupViews() {
        // Self.view
        view.backgroundColor = Colors.backgroundPrimary.color
        view.setNeedsLayout()
        view.layoutIfNeeded()
        
        // Refresh control
        refreshControl.tintColor = Palette.ppColorGrayscale300.color
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
        
        // Error view
        legacyErrorView = EmptySearchView(frame: view.frame)
        legacyErrorView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(errorRefresh)))
        legacyErrorView?.setup(style: .error)

        // Error view
        legacyNoResultsView = EmptySearchView(frame: view.frame)
        legacyNoResultsView?.setup(style: .noResult)

        errorView = SearchErrorView()
        errorView?.frame = view.frame
        errorView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        errorView?.style = .generalError

        // Map
        mapCell = StoresMapTableViewCell(style: .default, reuseIdentifier: "map")
    }
    
    private func setupCallbacks() {
        viewModel.didUpdate = { [weak self] (isMapUpdate) in
            DispatchQueue.main.async { [weak self] in
                if isMapUpdate {
                    // We don't upload the map in "cellForRowAtIndex" because it could cause a lot of unecessary updates
                    // We'll try to find the map section and update it manually
                    guard let section = self?.viewModel.sections.first(where: { $0.viewType == .map }) else {
                        return
                    }
                    
                    self?.mapCell?.configure(withData: section.rows)
                    return
                }
                
                self?.refreshControl.endRefreshing()
                self?.updateViews()
            }
        }
        
        viewModel.didFail = { [weak self] in
            DispatchQueue.main.async {
                self?.updateViews()
                self?.trackErrorEvent()
            }
        }
        
        viewModel.loadingUpdate = { [weak self] (isLoading) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.updateViews()
                isLoading ? self.showLoadingView() : self.hideLoadingView()
            }
        }
        
        viewModel.mapLoadingUpdate = { [weak self] (isLoading) in
            DispatchQueue.main.async { [weak self] in
                self?.mapCell?.isLoading = isLoading
            }
        }
        
        viewModel.didSearch = { [weak self] word, isSearchEmpty in
            if isSearchEmpty {
                DispatchQueue.main.async { [weak self] in
                    self?.updateViews()
                }
            }
            self?.didSearchFinished?(word, isSearchEmpty)
        }
    }
    
    // MARK: Public functions called by the parent
    // When the user navigates through tabs this is used to set the initial search term
    func setInitialTerm(term: String) {
        viewModel.term = term
    }
    
    // Called by the parent to update about text changes
    func searchTextChanged(text: String) {
        viewModel.term = text
    }
    
    // MARK: Actions
    @objc fileprivate func dismissKeyboard() {
        self.view.endEditing(true)
        UIApplication.shared.keyWindow?.endEditing(true)
        UIApplication.shared.sendAction(#selector(resignFirstResponder), to:nil, from:nil, for:nil)
    }
    
    @objc private func errorRefresh() {
        viewModel.refresh(clearBefore: true)
    }
    
    @objc func refresh(ignoreMapLoading: Bool = false){
        // If we are ignoring the map loading then we should also not show the refresh control
        if !ignoreMapLoading {
            refreshControl.beginRefreshing()
        }
        viewModel.refresh()
    }
    
    @objc private func permissionChanged() {
        tableView.contentOffset = CGPoint(x: 0, y: -self.refreshControl.frame.size.height)
        refreshControl.beginRefreshing()

        checkPermission()
    }
    
    // MARK: View update

    func showHeader(headerView: UIView?) {
        guard !viewModel.isLoading else {
            return
        }
        headerView?.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height - 44 - keyboardHeight)
        tableView.tableHeaderView = headerView
    }

    func showErrorView(style: SearchErrorView.Style) {
        guard let errorView = errorView, !viewModel.isLoading else { return }
        errorView.frame = view.frame
        tableView.tableHeaderView = errorView
        tableView.isScrollEnabled = false
        errorView.style = style
        errorView.tryAgainAction = { [weak self] in
            self?.trackErrorEvent(isUserReloading: true)
            self?.errorRefresh()
        }
    }

    func showLoadingView() {
        guard isSearch else { return }
        let loadingView = LegacySearchLoadingSkeletonView()
        loadingView.frame = view.frame
        tableView.tableHeaderView = loadingView
        tableView.isScrollEnabled = false
    }

    func showWaitingInputView() {
        guard isSearch, !isPersonRequestingPayment else { return }
        let waitingView = SearchErrorView()
        waitingView.style = .waitingInput
        waitingView.frame = view.frame
        tableView.tableHeaderView = waitingView
        tableView.isScrollEnabled = false
    }

    func hideLoadingView() {
        guard isSearch, tableView.tableHeaderView is LegacySearchLoadingSkeletonView else { return }
        hideHeaderView()
    }
    
    func hideHeaderView() {
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0.01))
        tableView.isScrollEnabled = true
    }
    
    func isHeaderHidden() -> Bool {
        if let header = tableView.tableHeaderView {
            let value = header.frame.height
            return value <= 0.02
        }
        return true
    }
    
    private func updateViews() {
        hideHeaderView()
        refreshControl.endRefreshing()
        
        tableView.reloadData()
        
        if viewModel.lastCallFailed {
            if isSearch {
                let style: SearchErrorView.Style = viewModel.lastCallError == .noConnection ? .noConnection : .generalError
                showErrorView(style: style)
            } else {
                showHeader(headerView: legacyErrorView)
            }
            tableView.backgroundView = nil
            tableView.tableFooterView = nil
            return
        }

        checkPermission()
        
        guard
            viewModel.sections.isEmpty,
            !viewModel.isLoading
            else {
                return
        }
        if isSearch && viewModel.term.count < 3 {
            showWaitingInputView()
        } else if tableView.tableFooterView == nil || tabType?.shouldAskPermissions == false || tableView.backgroundView == nil {
            // If there was no result, the user gave the necessary permissions and the tab should ask for permissions
            if isSearch {
                showErrorView(style: .emptyState(viewModel: nil))
            } else {
                showHeader(headerView: legacyNoResultsView)
            }
        }
    }
    
    // MARK: Permissions
    @objc
    private func checkPermission() {
        tableView.tableFooterView = nil
        tableView.backgroundView = nil
        
        let isContactNotDetermined: Bool = contactsPermission.status == .notDetermined
        let isLocationNotDetermined: Bool = locationPermission.status == .notDetermined
        
        if let should = shouldAskPermission?(), should && dependencies.featureManager.isActive(.featureInitialPermissionsAfterLogin) == false {
            
            if isContactNotDetermined && isLocationNotDetermined {
                if let controller = self.tabBarController {
                    openPermissionContactAndLocationPopup(from: controller)
                } else {
                    // For some reason self.tabBarController is nil on viewWillAppear, but if we wait just a little bit, it's there :thinking:
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.15) { [weak self] in
                        guard let strongSelf = self else {
                            return
                        }
                        strongSelf.openPermissionContactAndLocationPopup(from: strongSelf.tabBarController ?? strongSelf)
                    }
                }
                return
            }
            
            if isLocationNotDetermined {
                if let controller = self.tabBarController {
                    openPermissionLocationPopup(from: controller)
                } else {
                    // For some reason self.tabBarController is nil on viewWillAppear, but if we wait just a little bit, it's there :thinking:
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.15) { [weak self] in
                        guard let strongSelf = self else {
                            return
                        }
                        strongSelf.openPermissionLocationPopup(from: strongSelf.tabBarController ?? strongSelf)
                    }
                }
                return
            }
        }
        
        let isContactAuthorized: Bool = contactsPermission.status == .authorized
        let isLocationAuthorized: Bool = locationPermission.status == .authorized
        let tabId: String? = self.tabType?.id
        
        if !isSearch {
            
            if tabId == "places" && !isLocationAuthorized {
                showLocationPermissionView()
                return
            }
            
            if tabId == "main" && !isContactAuthorized {
                showContactsPermissionFooter()
                return
            }
        }
        
    }
    
    // On first time accessing the pay "$" button, without any authorization
    @objc private func openPermissionContactAndLocationPopup(from: UIViewController) {
        
        self.didAskForPermissions?() // Disable new authorization popups
        
        let alert = Alert(
            title: SearchLocalizable.contactAndLocationPermissonTitle.text,
            text: SearchLocalizable.contactAndLocationPermissionText.text
        )
        let buttonYes = Button(title: SearchLocalizable.authorize.text, type: .cta) { [weak self] popupController, _ in
            popupController.dismiss(animated: true, completion: {
                PPPermission.requestContacts { _ in
                    PPPermission.requestLocation { _ in
                        self?.didChangePermissionStatus()
                    }
                }
            })
        }
        let buttonNo = Button(title: DefaultLocalizable.notNow.text, type: .clean, action: .close)
        alert.buttons = [buttonYes, buttonNo]
        alert.image = Alert.Image(name: "ilu-contact-location-auth")
        
        AlertMessage.showAlert(alert, controller: from)
    }
    
    // On fist time accessing the pay "$" button, with contacts already authorized
    @objc private func openPermissionLocationPopup(from: UIViewController) {
        
        self.didAskForPermissions?() // Disable new authorization popups
        
        let alert = Alert(
            title: SearchLocalizable.locationPermissionTitle.text,
            text: SearchLocalizable.locationPermissionText.text
        )
        let buttonYes = Button(title: SearchLocalizable.authorize.text, type: .cta) { [weak self] popupController, _ in
            popupController.dismiss(animated: true, completion: {
                PPPermission.requestLocation { _ in
                    self?.didChangePermissionStatus()
                }
            })
        }
        let buttonNo = Button(title: DefaultLocalizable.notNow.text, type: .clean, action: .close)
        alert.buttons = [buttonYes, buttonNo]
        alert.image = Alert.Image(name: "ilu-location-auth")
        
        AlertMessage.showAlert(alert, controller: from)
    }

    private func setupTableFooterView() {
        if let footerView = tableView.tableFooterView {
            updateTableHeaderOrFooterHeight(footerView)
            tableView.tableFooterView = footerView
        }

        tableView.layoutIfNeeded()
    }

    private func updateTableHeaderOrFooterHeight(_ headerOrFooter: UIView) {
        let targetSize = CGSize(width: headerOrFooter.bounds.width, height: UIView.layoutFittingCompressedSize.height)
        let size = headerOrFooter.systemLayoutSizeFitting(targetSize)

        guard headerOrFooter.frame.height != size.height else {
            return
        }

        headerOrFooter.frame.size.height = size.height
    }
    
    private func didChangePermissionStatus(){
        NotificationCenter.default.post(name: Notification.Name(rawValue: SearchGenericViewController.NotificationChangePermissionStatus), object: nil)
    }
    
    private func showContactsPermissionFooter() {
        tableView.tableFooterView = tableFooterViewToShow()
        setupTableFooterView()
    }

    private func tableFooterViewToShow() -> UIView? {
        guard arePermissionsImportant
            && isHeaderHidden()
            && viewModel.hasContacts == false
            && dependencies.featureManager.isActive(.experimentPayPersonBool) == false else {
                return nil
        }

        return contactsFooterView
    }
    
    private func showLocationPermissionView() {
        guard arePermissionsImportant else { return }

        tableView.backgroundView = permissionLocationView
    }
    
    // On click over the contact footer view
    @objc private func requestContactPermision(){
        Analytics.shared.log(PermissionsEvent.cardContactsPermission(origin: .pay))
        PPPermission.requestContacts { [weak self] _ in
            self?.checkPermission()
        }
    }
    
    // On click over the button "Autorizar agora" from the location footer
    @objc private func requestLocationPermission(){
        Analytics.shared.log(LocationPermissionEvent.locationPermission("Autorizar", "Locais"))
        PPPermission.requestLocation { [weak self] _ in
            self?.didChangePermissionStatus()
        }
    }
    
    // MARK: Profile View controller
    
    
    // MARK: - Table view action
    func didSelectResult(section: LegacySearchResultSection, row: LegacySearchResultRow, origin: SearchHelper.Origin? = nil, indexPath: IndexPath? = nil) {
        if let origin = section.title {
            BreadcrumbManager.shared.addCrumb(origin, typeOf: .listSection)
        }
        
        dismissKeyboard()
        if row.type == .map {
            guard mapCell?.isLoading == false else {
                return
            }
            let map = StoresMapViewController.init(with: section.rows)
            map.title = tabType?.name
            self.navigationController?.pushViewController(map, animated: true)
        }
        
        var options = SearchHelper.Options()
        options.term = viewModel.term
        SearchHelper.openPayment(withSection: section, row: row, from: self, options: options, origin: origin)
        
        let shouldSaveRecentRow: Bool

        switch row.type {
        case .person, .store, .digitalGood, .financial, .subscription, .pix, .feature, .p2m, .p2p:
            shouldSaveRecentRow = true
        default:
            shouldSaveRecentRow = false
        }
        
        if shouldSaveRecents, shouldSaveRecentRow, let tab = tabType {
            viewModel.saveRecentSearch(row: row, tab: tab, section: section.title ?? "")
        }
        
        guard let tabName = tabType?.name, let title = section.title else {
            return
        }

        guard !isPersonRequestingPayment,
            let rowNumber = indexPath?.getAbsoluteRowNumber(for: self.tableView),
            let id = delegate?.getAnalyticsID() else {
            return
        }
        viewModel.trackSelectionEvents(row: row, tab: tabName, section: title, position: rowNumber, id: id, type: .titleTouch)
    }
    
    private func selectResult() {
        guard
            let section = bottomSheetCellIndexPath?.section,
            let row = bottomSheetCellIndexPath?.row
            else {
                return
        }
        
        let customSection = viewModel.sections[section]
        let customRow = customSection.rows[row]
        
        let origin: SearchHelper.Origin
        if isPersonRequestingPayment {
            origin = .requestPaymentSearch
        } else if customSection.type == .contact {
            origin = .contacts
        } else {
            origin = isSearch ? .paySearch : .payStore
        }
        
        didSelectResult(section: customSection, row: customRow, origin: origin)
    }
}

// MARK: UITableViewDelegate
extension SearchGenericViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // This *should* never be false, *but* we were having problems with this when injecting sections locally
        // Better safe than sorry anyway
        let isAllowedToSelect = viewModel.isCellSelectable(at: indexPath, isPersonRequestingPayment: isPersonRequestingPayment)
        guard
            viewModel.sections.count > indexPath.section,
            viewModel.sections[indexPath.section].rows.count > indexPath.row,
            isAllowedToSelect
            else {
                return
        }
        let section = viewModel.sections[indexPath.section]
        let row = section.rows[indexPath.row]
        itemSelected = indexPath.item
        
        let origin: SearchHelper.Origin
        if isPersonRequestingPayment {
            origin = .requestPaymentSearch
        } else if section.type == .contact {
            origin = .contacts
        } else {
            origin = isSearch ? .paySearch : .payStore
        }

        didSelectResult(section: section, row: row, origin: origin, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if viewModel.isLoading {
            return 40
        }
        
        guard viewModel.sections.count > indexPath.section else {
            return 0
        }
        
        let viewType = viewModel.sections[indexPath.section].viewType
        switch viewType {
        case .list:
            if !dependencies.featureManager.isActive(.searchCardStyle) {
                // Flat style should have smaller cells
                return 70
            }
            return 90
        case .highlight:
            
            let height = (view.frame.size.width - 52) / 2.0
            
            return height
        case .carousel:
            return 110
        case .map:
            return 210
        }
    }
    
    // Hide the keyboard on scroll
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        // The search bar is several parents above us so we need to send an action for the whole app to dismiss the keyboard
        UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil)
    }
    
    // Ask for more data when showing the last cell
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard indexPath.section == viewModel.sections.count - 1 && indexPath.row + 6 > viewModel.sections.last?.rows.count else {
            return
        }
        // Load more (pagination)
        viewModel.loadMore()
    }
}

// MARK: - UITableViewDataSource
extension SearchGenericViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewModel.isLoading && !isSearch {
            let loadingCell = tableView.dequeueReusableCell(withIdentifier: "LoadingCell") as! LoadingTableViewCell
            loadingCell.startAnimating()
            loadingCell.backgroundColor = .clear
            return loadingCell
        }
        
        guard viewModel.sections.count > indexPath.section else {
            tableView.reloadData()
            return UITableViewCell()
        }
        
        let section = viewModel.sections[indexPath.section]
        
        switch section.viewType {
        case .list:
            guard section.rows.count > indexPath.row else {
                return UITableViewCell()
            }
            let row = section.rows[indexPath.row]
            let listCell = tableView.dequeueReusableCell(withIdentifier: "SearchListCell") as! SearchListCell
            let shouldPassthroughImageTap = row.type == .digitalGood || row.type == .subscription
            listCell.setup(data: row.basicData, fullObject: row.fullData ?? NSObject(), profileImageDelegate: shouldPassthroughImageTap ? nil : self, origin: section.title, shouldDisplayIcon: self.shouldDisplayTypeIcons, isSearch: self.isSearch)
            listCell.bottomSheetDelegate = self
            listCell.didTapBottomSheetButton = { [weak self] in
                self?.bottomSheetCellIndexPath = indexPath
            }
            
            if viewModel.isCellSelectable(at: indexPath, isPersonRequestingPayment: isPersonRequestingPayment) {
                return listCell
            }
            
            listCell.disableCell(text: SearchLocalizable.paymentRequestDisabled.text)
            return listCell
        case .highlight:
            // Sometimes self.frame.height for the cell will return a placeholder so we call the delegate to get it correctly
            let cellHeight = tableView.delegate?.tableView?(tableView, heightForRowAt: indexPath) ?? 0
            let listCell = tableView.dequeueReusableCell(withIdentifier: "HightlightTableViewCell") as! SearchHightlightTableViewCell
            listCell.didTapInfoButton = {[weak self] (section, row) in
                guard let data = row.basicData as? SearchResultBanner, let info = data.info, let self = self else {
                    return
                }
                let popup = SearchBannerInfoPopUpController(info: info)
                self.showPopup(popup)
            }
            listCell.setup(data: section, height: cellHeight, selection: { [weak self] (section, row) in
                self?.didSelectResult(section: section, row: row)
            })
            
            return listCell
        case .carousel:
            let carouselCell = tableView.dequeueReusableCell(withIdentifier: "SearchRecentTableViewCell") as! SearchRecentTableViewCell
            
            carouselCell.setup(data: section, selection: { [weak self] section, row, indexPath in
                self?.itemSelected = indexPath.item
                self?.didSelectResult(section: section, row: row, origin: .payMain)
            })
            
            return carouselCell
        case .map:
            return mapCell ?? UITableViewCell()
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if viewModel.isLoading{
            return 1
        }
        
        if viewModel.lastCallFailed {
            return 0
        }

        return viewModel.sections.count
    }

    private func canNotShowItemsForPlacesTab() -> Bool {
        isSearch == false && tabType?.id == TabType.places.rawValue && locationPermission.status != .authorized
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.isLoading {
            return 1
        }
        
        guard viewModel.sections.count > section else {
            tableView.reloadData()
            return 0
        }
        
        let section = viewModel.sections[section]
        
        // If the section type is carousel then we "compress" all rows into one
        if section.viewType == .carousel {
            return 1
        }
        
        // If the section type is highlight then we "compress" all rows into one
        if section.viewType == .highlight {
            return 1
        }

        if canNotShowItemsForPlacesTab() {
            return 0
        }
        
        // If the section type is map we show rows as pins
        if section.viewType == .map {
            return 1
        }
        
        return section.rows.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard viewModel.sections.count > section else {
            return nil
        }
        let sectionPosition = section
        let section = viewModel.sections[sectionPosition]
        
        guard let title = section.title,
            title != "",
            !viewModel.isLoading else {
                return nil
        }
        
        let header: SearchSectionHeader = SearchSectionHeader.fromNib()
        
        header.titleLabel.text = section.title
        header.showAll = section.hasMore
        
        if section.style == .recentSearch {
            header.titleLabel.textColor = Palette.ppColorGrayscale500.color
        }
        
        header.seeAll = { [weak self] in
            Analytics.shared.log(StoreEvent.didTapSeeAllOnCategory(name: title, id: section.id ?? "", position: sectionPosition))
            let all = SearchGenericViewController.controllerFromStoryboard()
            all.linkedFrom = section.linksTo
            all.arePermissionsImportant = false
            all.title = section.title
            all.hidesBottomBarWhenPushed = true
            self?.navigationController?.pushViewController(all, animated: true)
        }
        
        //if !FeatureManager.isActive(FeatureConstant.SearchCardStyle) {
        //    header.left.constant = 12
        //}
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard viewModel.sections.count > section else { return 0 }
        
        let section = viewModel.sections[section]
        
        guard section.viewType != .map else { return 0 }
        
        guard let title = section.title,
            title != "",
            !viewModel.isLoading else { return 12 }
        
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard viewModel.sections.count > section else { return 0 }
        
        let section = viewModel.sections[section]
        if section.viewType == .highlight || section.viewType == .map {
            return 0
        }
        
        return 25
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard viewModel.sections.count > section else {
            return nil
        }
        
        let sectionIndex = section
        let section = viewModel.sections[section]
        if section.viewType == .highlight || section.viewType == .map || (viewModel.sections.count == sectionIndex + 1) {
            return nil
        }
        
        let footerView: SearchSectionFooter = SearchSectionFooter.fromNib()
        return footerView
    }
    
}

// MARK: - UIPPProfileImageDelegate
extension SearchGenericViewController: UIPPProfileImageDelegate {
    func profileImageViewDidTap(_ profileImage: UIPPProfileImage, contact: PPContact) {
        dismissKeyboard()

        if let origin = profileImage.origin {
            BreadcrumbManager.shared.addCrumb(origin, typeOf: .listSection)
        }
        
        if isPersonRequestingPayment {
            Analytics.shared.log(PaymentRequestUserEvent.didSelectUserAvatar)
        }
        
        Analytics.shared.log(ProfileEvent.touchPicture(from: .pay))
        profileViewController = NewProfileProxy.openProfile(contact: contact, parent: self, originView: profileImage)
        guard let indexPath = profileImage.tapGesture?.getIndexPath(in: self.tableView) else { return }
        trackInteraction(for: indexPath, type: .pictureTouch)
    }
    
    func profileImageViewDidTap(_ profileImage: UIPPProfileImage, store: PPStore) {
        dismissKeyboard()
        
        if let origin = profileImage.origin {
            BreadcrumbManager.shared.addCrumb(origin, typeOf: .listSection)
        }

        Analytics.shared.log(PaymentEvent.itemPopupAccessed(type: .biz, origin: .photo))

        if let indexPath = profileImage.tapGesture?.getIndexPath(in: self.tableView) {
            trackInteraction(for: indexPath, type: .pictureTouch)
        }

        establishmentDetailsCoordinator = EstablishmentCoordinatorFactory().makeDetailsCoordinator(
            store: store,
            navigationController: navigationController,
            fromViewController: self
        )

        establishmentDetailsCoordinator?.didFinishFlow = { [weak self] in
            self?.establishmentDetailsCoordinator = nil
        }

        establishmentDetailsCoordinator?.start()
    }

    private func trackInteraction(for indexPath: IndexPath, type: SearchEvent.InteractionType) {
        let section = viewModel.sections[indexPath.section]
        let row = section.rows[indexPath.row]
        let rowNumber = indexPath.getAbsoluteRowNumber(for: tableView)

        guard let analyticsId = delegate?.getAnalyticsID(),
            let tabName = tabType?.name,
            let sectionName = section.title else {
                return
        }
        viewModel.trackSelectionEvents(row: row, tab: tabName, section: sectionName, position: rowNumber, id: analyticsId, type: .pictureTouch)
    }

    private func trackErrorEvent(isUserReloading: Bool = false) {
        guard isSearch, let tabName = tabType?.name, let analyticsId = self.delegate?.getAnalyticsID() else { return }
        let event: SearchEvent

        if isUserReloading {
            event = .errorReload(id: analyticsId, tab: tabName, query: term, type: viewModel.lastCallError)
        } else {
            event = .resultError(id: analyticsId, tab: tabName, query: term, type: viewModel.lastCallError)
        }

        dependencies.analytics.log(event)
    }
}

// MARK: - ViewConfiguration
extension SearchGenericViewController: ViewConfiguration {
    func setupConstraints() {
        lineView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(2)
        }
        tableView.snp.makeConstraints {
            $0.top.equalTo(lineView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func buildViewHierarchy() {
        view.addSubviews(lineView, tableView)
    }
}

extension SearchGenericViewController: SearchListCellDelegate {
    func openSearchBottomSheet(
        sender: UITapGestureRecognizer,
        withData data: SearchResultBasics,
        cellObject object: NSObject,
        andProfileImageDelegate delegate: UIPPProfileImageDelegate?
    ) {
        if let indexPath = sender.getIndexPath(in: self.tableView) {
            trackInteraction(for: indexPath, type: .optionsTouch)
        }

        let controller = SearchBottomSheetFactory.make(
            data: data,
            object: object,
            uippdelegate: delegate,
            delegates: (
                bsDelegate: self,
                coordinatorDelegate: self
            ),
            tabId: tabType?.id
        )
        transitionDelegate = BottomSheetTransitioningDelegate()
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = transitionDelegate
        present(controller, animated: true)
    }
}

extension SearchGenericViewController: SearchBottomSheetDelegate {
    func handleDeeplink(deeplink: String) {
        guard let url = URL(string: deeplink) else {
            return
        }
        AppManager.shared.mainScreenCoordinator?.showHomeScreen()
        DeeplinkHelper.handleDeeplink(withUrl: url)
    }
}

extension SearchGenericViewController: SearchBottomSheetCoordinatorDelegate {
    func presentPaymentRequestValueFlow() {
        isPersonRequestingPayment = true
        selectResult()
    }
    
    func displayContactProfile(_ image: UIPPProfileImage, _ contact: PPContact) {
        profileViewController = NewProfileProxy.openProfile(contact: contact, parent: self, originView: image)
    }
    
    func displayStoreProfile(_ image: UIPPProfileImage, _ store: PPStore) {
        establishmentDetailsCoordinator = EstablishmentCoordinatorFactory().makeDetailsCoordinator(
            store: store,
            navigationController: navigationController,
            fromViewController: self
        )

        establishmentDetailsCoordinator?.didFinishFlow = { [weak self] in
            self?.establishmentDetailsCoordinator = nil
        }

        establishmentDetailsCoordinator?.start()
    }
    
    func displayDefaultCellAction() {
        selectResult()
    }
}

private extension IndexPath {
    func getAbsoluteRowNumber(for tableView: UITableView) -> Int {
        (0..<section).reduce(row) { $0 + tableView.numberOfRows(inSection: $1) }
    }
}

private extension UITapGestureRecognizer {
    func getIndexPath(in tableView: UITableView) -> IndexPath? {
        let location = self.location(in: tableView)
        guard let indexPath = tableView.indexPathForRow(at: location) else {
            return nil
        }

        return indexPath
    }
}

extension SearchGenericViewController: SearchResultLegacyViewController {}
