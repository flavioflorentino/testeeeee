import AnalyticsModule
import Billet
import CarbonKit
import Core
import FeatureFlag
import Foundation
import SnapKit
import UI
import UIKit

enum SearchWrapperTabType {
    case main
    case consumers
    case places
    case store

    var searchTab: SearchTab {
        switch self {
        case .main:
            return SearchTab(
                name: SearchLocalizable.all.text,
                group: "MAIN",
                id: "main",
                shouldAskPermissions: false,
                autoRefresh: false
            )
        case .consumers:
            return SearchTab(
                name: SearchLocalizable.people.text,
                group: "CONSUMERS",
                id: "consumers",
                shouldAskPermissions: false,
                autoRefresh: false
            )
        case .places:
            return SearchTab(
                name: SearchLocalizable.places.text,
                group: "PLACES",
                id: "places",
                shouldAskPermissions: false,
                autoRefresh: false
            )
        case .store:
            return SearchTab(
                name: SearchLocalizable.store.text,
                group: "STORE",
                id: "store",
                shouldAskPermissions: false,
                autoRefresh: false
            )
        }
    }
}

// Search wrapper that encapsulates the search tab controller and search bar
final class SearchWrapper: PPBaseViewController {
    typealias Dependencies = HasFeatureManager & HasAnalytics
    private var dependencies: Dependencies
    // MARK: - Layout
    enum Layout {
        static let searchBarHeight: CGFloat = 44
    }
    // MARK: IBOutlets
    private lazy var searchBar: SearchNavigationBar = {
        let bar = SearchNavigationBar()
        bar.translatesAutoresizingMaskIntoConstraints = false
        bar.backgroundColor = Colors.backgroundPrimary.color
        return bar
    }()
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    // MARK: - Variables
    private var shouldShowSearchBar: Bool
    private var searchViewContainer: UIView!
    private var searchResultTabControllers: SearchTabsController!
    private var initialTabController: SearchTabsController!
    
    private var recentSearchViewController: SearchGenericViewController?
    private var recentSearchViewContainer: UIView?
    private var recentSearchViewLeftConstraint: Constraint?
    
    // Push notification related
    public var initialSearch: String? {
        didSet {
            guard let initialSearch = initialSearch,
                let searchViewContainer = searchViewContainer else {
                    return
            }
            searchBar.currentText = initialSearch
            searchBar.searchTextChanged?(initialSearch)
            if showContainerSearch {
                searchViewContainer.isHidden = false
            } else {
                dismissSearch()
            }
        }
    }
    
    public var showContainerSearch: Bool = false {
        didSet {
            guard let searchViewContainer = searchViewContainer else {
                return
            }
            if showContainerSearch {
                searchViewContainer.isHidden = false
            } else {
                dismissSearch()
            }
        }
    }

    public var initialPage: String?
    public var ignorePermissionPrompt = false
    public var isPersonRequestingPayment = false
    public var shouldShowBackButton: Bool = false
    public var shouldFocusWhenAppear: Bool = false
    public var shouldHideTabsForSingleTab: Bool = false
    public var showsSearchBarButtons: Bool = true
    public var shouldClearOnChangeTab: Bool = true
    public var origin: SearchEvent.SearchOrigin?

    private var didAppear: Bool = false
    private lazy var searchPlaceHolder: String = dependencies.featureManager.text(.searchBarLabel)
    private var tabs: [SearchWrapperTabType] = [.main, .consumers, .places, .store]
    private var initialTabs: [SearchTab]?
    private var isOnlySearchEnabled: Bool = false
        
    // MARK: - Lifecycle
    init(ignorePermissionPrompt: Bool, dependencies: Dependencies, shouldShowSearchBar: Bool = true) {
        self.dependencies = dependencies
        self.ignorePermissionPrompt = ignorePermissionPrompt
        self.shouldShowSearchBar = shouldShowSearchBar

        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createInitialTab()
        buildLayout()
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(dismissKeyboard)
            )
        )
        
        paintSafeAreaTopInset(
            withColor: Colors.backgroundPrimary.color
        )
        
        setupInitialTabController()
        setupSearchResult()
        setupSearchBar()
        setupRecentSearch()
        
        // Payment notification
        NotificationCenter.default.addObserver(forName: Notification.Name.Payment.new, object: nil, queue: nil) { [weak self] _ in
            // clear search bar on payment
            DispatchQueue.main.async {
                self?.searchBar.currentText = ""
                self?.searchBar.resignFirstResponder()
            }
        }
        searchViewContainer.isHidden = !isOnlySearchEnabled
        searchBar.isExpanded = isOnlySearchEnabled
        searchBar.showsLeftAndRightButtons = showsSearchBarButtons
        
        if isPersonRequestingPayment {
            Analytics.shared.log(PaymentRequestUserEvent.didOpenSearchScreen)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        if let initialSearch = initialSearch {
            searchBar.currentText = initialSearch
            searchBar.searchTextChanged?(initialSearch)
            if showContainerSearch {
                searchViewContainer.isHidden = false
            } else {
                dismissSearch()
            }
        }
        
        searchResultTabControllers.initialPage = initialPage
        initialTabController.initialPage = initialPage
        
        initialSearch = nil
        initialPage = nil
        ignorePermissionPrompt = false
        
        if didAppear {
            if viewIfLoaded?.window != nil {
                BreadcrumbManager.shared.addCrumb(self)
            }
        }

        searchBar.searchField(isHidden: !shouldShowSearchBar)
        searchBar.showsBackButton = shouldShowBackButton
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        didAppear = true

        if shouldFocusWhenAppear {
            searchBar.focusSearchField()
        }
        
        PaginationCoordinator.shared.configurePagination()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        PaginationCoordinator.shared.configurePagination()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (navigationController?.viewControllers.last as? SearchWrapper) == nil {
            navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
    
    private func createInitialTab() {
        initialTabController = SearchTabsController(
            ignorePermissionPrompt: ignorePermissionPrompt,
            isPersonRequestingPayment: isPersonRequestingPayment,
            tabs: initialTabs
        )
        initialTabController.shouldHideToolbarForSingleTab = shouldHideTabsForSingleTab

        addChild(initialTabController)
        containerView.addSubview(initialTabController.view)
        initialTabController.view.translatesAutoresizingMaskIntoConstraints = false
        initialTabController.didMove(toParent: self)
    }
    
    // MARK: - Search Type
    public func setTabs(tabs: [SearchWrapperTabType], isOnlySearchEnabled: Bool) {
        self.tabs = tabs
        self.isOnlySearchEnabled = isOnlySearchEnabled
    }

    public func setInitialTabs(tabs: [SearchTab]?) {
        self.initialTabs = tabs
    }
    
    public func setSearchPlaceHolder(_ text: String) {
        searchPlaceHolder = text
    }
    
    // MARK: - Selection
    public func selectMainTabBar(tab: SearchWrapperTabType) {
        selectMainTabBar(idTab: tab.searchTab.id)
    }

    public func selectMainTabBar(idTab: String) {
        if initialTabController == nil {
            initialPage = idTab
        } else {
            initialTabController.initialPage = idTab
        }
    }
    
    public func selectSearchTabBar(idTab: String) {
        if searchResultTabControllers == nil {
            initialPage = idTab
        } else {
            searchResultTabControllers.initialPage = idTab
        }
    }

    public func selectSearchTabBar(tab: SearchWrapperTabType) {
        selectSearchTabBar(idTab: tab.searchTab.id)
    }
    
    // MARK: - Setup
    private func setupSearchBar() {
        searchBar.ignoreStatusBar = false
        searchBar.setPlaceholder(placeholder: searchPlaceHolder)
        searchBar.setLeftButtonImage(image: Assets.NewIconography.Navigationbar.newIconQrcode.image)
        searchBar.setTwitterKeyboard()
        searchBar.leftButtonTapped = { [weak self] in
            guard let self = self, !self.isPersonRequestingPayment else {
                return
            }
            
            PaginationCoordinator.shared.showScanner()
            Analytics.shared.log(UserEvent.qrCodeAccessed(type: .click, origin: .payment))
        }
        
        searchBar.shouldChageNormalStateWhenEndEdit = false
        searchBar.searchTextChanged = { [weak self] text in
            if text.length < 3 {
                self?.showRecentsSearch()
            } else {
                self?.hideRecentsSearch()
                self?.searchResultTabControllers.broadcastSearchTextChange(text: text)
            }
        }

        searchBar.searchTextShouldBeginEditing = { [weak self] in
            guard let self = self, !self.isPersonRequestingPayment, let origin = self.origin else { return }
            self.searchResultTabControllers.userDidBeginSearching(origin: origin)
        }
        
        searchBar.searchTextDidBeginEditing = { [weak self] text in
            guard let self = self else {
                return
            }
            
            if self.isPersonRequestingPayment {
                self.dependencies.analytics.log(PaymentRequestUserEvent.didTapSearchField)
            }

            if text.length < 2 {
                self.showRecentsSearch(animated: true)
            }
            
            self.searchViewContainer.isHidden = false
        }
        
        searchBar.searchTextDidEndEditing = { [weak self] text in
            if text.length >= 3 {
                self?.hideRecentsSearch()
            } else {
                self?.searchViewContainer.isHidden = false
            }
        }
        
        searchBar.cancelButtonTapped = { [weak self] in
            guard self?.isOnlySearchEnabled == true else {
                self?.dismissSearch()
                return
            }

            self?.dismissController()
        }
    }
    
    private func setupSearchResult() {
        searchResultTabControllers = SearchTabsController(
            ignorePermissionPrompt: ignorePermissionPrompt,
            isPersonRequestingPayment: isPersonRequestingPayment
        )

        searchResultTabControllers.tabs = tabs.map(\.searchTab)

        searchResultTabControllers.isSearch = true
        searchResultTabControllers.shouldHideToolbarForSingleTab = shouldHideTabsForSingleTab
        
        searchResultTabControllers.getCurrentTerm = { [weak self] in
            return self?.searchBar.currentText ?? ""
        }
        searchResultTabControllers.navigationWillMoveAt = { [weak self] index in
            guard let self = self else {
                return
            }
            
            if (index != 0 && self.recentSearchViewLeftConstraint?.layoutConstraints.first?.constant == 0)
                || (self.recentSearchViewLeftConstraint?.layoutConstraints.first?.constant < 0 && index == 0) {
                let left = index != 0 ? self.view.frame.size.width * -1 : 0
                
                UIView.animate(withDuration: 0.25, animations: {
                    self.recentSearchViewLeftConstraint?.layoutConstraints.first?.constant = left
                    self.recentSearchViewContainer?.setNeedsLayout()
                    self.recentSearchViewContainer?.layoutIfNeeded()
                })
            }
        }
        
        searchViewContainer = UIView(frame: CGRect(x: 10, y: 44, width: 200, height: 300))
        view.addSubview(searchViewContainer)
        searchViewContainer.snp.makeConstraints { make in
            make.edges.equalTo(containerView)
        }
        
        addChild(searchResultTabControllers)
        searchResultTabControllers.willMove(toParent: self)
        
        searchViewContainer.embed(searchResultTabControllers.view)
        searchResultTabControllers.didMove(toParent: self)
        
        searchViewContainer.isHidden = true
        searchResultTabControllers.navigationDidMoveAt = { [weak self] index in
            guard
                let self = self,
                !self.isPersonRequestingPayment
                else {
                    return
            }
            Analytics.shared.log(SearchEvent.tabSelected(tab: self.searchResultTabControllers.getTabName(at: index) ?? String()))
        }
    }
    
    private func setupInitialTabController() {
        initialTabController.isPersonRequestingPayment = isPersonRequestingPayment
        initialTabController.navigationWillMoveAt = { [weak self] index in
            self?.hideRecentsSearch()

            guard self?.shouldClearOnChangeTab == true else { return }
            self?.searchBar.clearSearch()
        }
        
        initialTabController.navigationDidMoveAt = { [weak self] index in
            guard
                let self = self,
                !self.isPersonRequestingPayment
                else {
                    return
            }
            let tabName = self.initialTabController.getTabName(at: index) ?? String()
            Analytics.shared.log(SearchEvent.tabSelected(tab: tabName))
        }
        
        initialTabController.getCurrentTerm = {
            return ""
        }
    }
    
    private func setupRecentSearch() {
        let recentControlller = SearchGenericViewController(dependencies: DependencyContainer())
        recentControlller.tabType = SearchTab(name: SearchLocalizable.recentSearch.text, group: "recent_search", id: "recent_search", shouldAskPermissions: false, autoRefresh: false)
        recentControlller.isSearch = true
        recentControlller.shouldDisplayTypeIcons = true
        recentControlller.shouldSaveRecents = false
        recentControlller.viewModel = SearchRecentsViewModel(dependencies: DependencyContainer())
        recentControlller.isPersonRequestingPayment = isPersonRequestingPayment
        recentControlller.delegate = searchResultTabControllers

        var top: CGFloat = 124
        if #available(iOS 11.0, *) {
            let safeAreaInsetTop = (UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0.0)
            top = safeAreaInsetTop == 0 ? 124 : 104 + safeAreaInsetTop
        }
        recentSearchViewContainer = UIView(frame: CGRect(x: 10, y: 50, width: 200, height: 300))
        view.addSubview(recentSearchViewContainer!)
        recentSearchViewContainer?.snp.makeConstraints { make in
            make.width.equalTo(self.view.frame.size.width)
            self.recentSearchViewLeftConstraint = make.left.equalTo(0).constraint
            make.top.equalTo(top)
            if let bottomConstraint = make.bottom.equalTo(0).constraint.layoutConstraints.first {
                KeyboardManager.shared.addConstraint(bottomConstraint, parentView: self.view)
            }
        }
        
        addChild(recentControlller)
        recentControlller.willMove(toParent: self)
        
        recentSearchViewContainer?.embed(recentControlller.view)
        recentControlller.didMove(toParent: self)
        
        recentSearchViewController = recentControlller
        recentSearchViewContainer?.isHidden = true
    }
    
    private func showRecentsSearch(animated: Bool = false) {
        guard tabs.contains(.main) else {
            return
        }
        
        if recentSearchViewController == nil {
            setupRecentSearch()
        }

        if recentSearchViewContainer?.isHidden == true {
            self.recentSearchViewContainer?.isHidden = false
            recentSearchViewController?.viewModel.loadRecentsSearch { [weak self] recents in
                guard let recents = recents, !recents.isEmpty else {
                    DispatchQueue.main.async {
                        self?.recentSearchViewContainer?.isHidden = true
                    }
                    return
                }
                
                if let model = self?.recentSearchViewController?.viewModel as? SearchRecentsViewModel {
                    model.loadCachedRows(recents: recents)
                }
            }
        }
    }
    
    // MARK: - Hide n dismiss
    private func hideRecentsSearch() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(10)) {
            self.recentSearchViewContainer?.isHidden = true
        }
    }
    
    private func dismissSearch() {
        searchBar.isExpanded = false
        searchViewContainer.isHidden = true
        searchResultTabControllers.broadcastSearchTextChange(text: "")
        hideRecentsSearch()
    }
    
    private func dismissController() {
        if let navController = navigationController {
            navController.popViewController(animated: true)
        } else {
            dismiss(animated: true)
        }
    }
    
    @objc
    private func dismissKeyboard() {
        view.endEditing(true)
    }
}

// MARK: - ViewConfiguration
extension SearchWrapper: ViewConfiguration {
    func setupConstraints() {
        searchBar.layout {
            var statusBarSpacing: CGFloat = UIApplication.shared.statusBarFrame.height
            if #available(iOS 11, *) {
                statusBarSpacing = 0
            }
            $0.top == view.compatibleSafeAreaLayoutGuide.topAnchor + statusBarSpacing
            $0.leading == view.compatibleSafeAreaLayoutGuide.leadingAnchor
            $0.trailing == view.compatibleSafeAreaLayoutGuide.trailingAnchor
            $0.height == Layout.searchBarHeight
        }
        
        containerView.layout {
            $0.top == searchBar.bottomAnchor
            $0.leading == view.compatibleSafeAreaLayoutGuide.leadingAnchor
            $0.trailing == view.compatibleSafeAreaLayoutGuide.trailingAnchor
            $0.bottom == view.bottomAnchor
        }
        
        initialTabController.view.layout {
            $0.top == containerView.topAnchor
            $0.bottom == containerView.bottomAnchor
            $0.leading == containerView.leadingAnchor
            $0.trailing == containerView.trailingAnchor
        }
    }
    
    func buildViewHierarchy() {
        view.addSubviews(searchBar, containerView)
    }
}
