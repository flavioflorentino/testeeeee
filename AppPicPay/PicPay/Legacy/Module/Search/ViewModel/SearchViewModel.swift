import Alamofire
import AnalyticsModule
import Core
import FeatureFlag
import Foundation
import PermissionsKit

class SearchViewModel: NSObject {
    typealias Dependencies = HasFeatureManager & HasAnalytics

    enum ErrorType: Equatable {
        case noConnection
        case generalError(code: String)

        static func == (lhs: Self, rhs: Self) -> Bool {
            switch (lhs, rhs) {
            case (.noConnection, .noConnection):
                return true
            case (.generalError, .generalError):
                return true
            default:
                return false
            }
        }
    }

    private var dependencies: Dependencies
    // MARK: Public vars
    
    // The view controller's sections. This is used to populate the table view
    var sections: [LegacySearchResultSection] = [] {
        didSet {
            updateMapSection()
        }
    }
    
    private var mapRows: [LegacySearchResultRow] = []

    var isPersonRequestingPayment = false

    var isLoading = true {
        didSet {
            loadingUpdate?(isLoading)
        }
    }
    
    private(set) var isMapLoading = true {
        didSet {
            mapLoadingUpdate?(isMapLoading)
        }
    }
    
    // Important to know if the last call failed to show the error screen even if sections is not empty
    // (in case we have something cached or whatever)
    private(set) var lastCallFailed = false
    private(set) var lastCallError: ErrorType = .noConnection


    // Model's type that is sent to the server
    var group: String? {
        didSet {
            // Cancel everything as this param is essential to the request
            cancelPendingRequest()
            searchParamsChanged()
        }
    }
    
    var term = "" {
        didSet {
            guard term != oldValue else {
                return
            }
            searchParamsChanged()
        }
    }
    
    var didUpdate: ((Bool) -> ())? {
        didSet {
            didUpdate?(false)
        }
    }
    
    var didFail: (() -> ())?
    var didSearch: ((String, Bool) -> Void)?
    var loadingUpdate: ((Bool) -> ())?
    var mapLoadingUpdate: ((Bool) -> ())?
    
    var isSearch:Bool = true

    var hasContacts: Bool {
        sections.contains(where: { $0.type == .contact })
    }
    
    // MARK: Private vars
    fileprivate var currentPage = 0 {
        didSet {
            if currentPage == 0 {
                hasMore = true
            }
        }
    }
    // Local loading is when we are trying to retrieve the cache or doing the local filter
    // This is important to know if this process that isn't necessary is taking to long so we can stop it and show the loading wheel
    fileprivate var isLocalLoading = false
    fileprivate let api = SearchApi()
    var hasMore = true
    fileprivate var ongoingRequest: DataRequest?
    // This is necessary so the local filter can update the `sections` var and keep filtering on the latest api result
    fileprivate var lastApiResult:[LegacySearchResultSection] = []
    private lazy var searchRecentsManager = SearchRecentsManager()
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    // MARK: Lifecycle
    func setup() {
        cancelPendingRequest()
        retrieveLocallyOrLoad()
        search()
    }
    
    // MARK: Public functions
    func refresh(clearBefore: Bool = false) {
        if clearBefore {
            sections = []
            retrieveLocallyOrLoad()
        }
        search()
    }
    
    // Cache only works for first page for now. If we improve it to work for all pages, we'd need to check for duplicates
    func loadMore() {
        guard hasMore && (ongoingRequest?.progress.isFinished == true || ongoingRequest == nil || ongoingRequest?.progress.isCancelled == true) else {
            return
        }

        search(page: currentPage + 1)
    }
    
    // MARK: Search execution
    private func searchParamsChanged() {
        
        // cancel the previos request to prevent load while user is typing
        cancelPendingRequest()
        
        retrieveLocallyOrLoad()
        
        // Debounce
        self.perform(#selector(search), with: nil, afterDelay: term == "" ? 0.0 : 0.5)
    }
    
    @objc func search(page: Int = 0) {
        
        if isSearch {
            if term.count < 3 {
                isLoading = false
                sections = []
                return
            }
        }

        let request = SearchRequest()
        request.group = group
        request.term = term != "" ? term : nil
        request.longitude = LocationManager.shared.authorizedLocation?.coordinate.longitude
        request.latitude = LocationManager.shared.authorizedLocation?.coordinate.latitude
        request.page = page
        
        if let term = request.term, term != "" {
            BreadcrumbManager.shared.addCrumb("Search", typeOf: .search, withProperties: ["term":term])
        }
        
        cancelPendingRequest()

        let flow = getRequestOriginFlow()
        let key = cacheKey(forPage: page)

        ongoingRequest = api.search(request: request, originFlow: flow, cacheKey: key) { [weak self] response in
            self?.currentPage = page
            self?.onApiLoaded(response: response)
        }
    }
    
    private func onApiLoaded(response: PicPayResult<SearchApi.SearchApiResponse>?) {
        switch response {
        case .success(let result):
            lastCallFailed = false
            hasMore = needsToFetchMorePages(sections: result.list)
            var finalResult = result
            if currentPage > 0 {
                let newResponse = SearchApi.SearchApiResponse()
                newResponse.list = mergeNewPage(result.list)
                finalResult = newResponse
            }
            didSearch?(term, finalResult.list.isEmpty)
            updateSections(raw: finalResult)
        case .failure(let error):
            print(error)
            // If the code is -999 than it's a canceled and should just be discarded
            guard error.picpayCode != "-999" else {
                return
            }
            lastCallFailed = true
            lastCallError = error.isConnectionError() ? .noConnection : .generalError(code: error.picpayCode)
            hasMore = false
            isLoading = false
            didFail?()
        case .none:
            let finalResult = SearchApi.SearchApiResponse()
            didSearch?(term, finalResult.list.isEmpty)
            updateSections(raw: finalResult)
        }
    }

    private func needsToFetchMorePages(sections: [LegacySearchResultSection]) -> Bool {
        guard sections.isNotEmpty else { return false }
        return sections.allSatisfy { $0.hasMore }
    }
    
    // MARK: Local cache / filter
    // Tries to retrieve from cache and do the local filter, if it takes too long, show the loading wheel
    private func retrieveLocallyOrLoad(forPage page: Int = 0) {
        if isSearch {
            if term.count < 3 {
                isLoading = false
                sections = []
                return
            } else {
                isLoading = true
            }
        }
        
        isLocalLoading = true
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(1000)) { [weak self] in
            if self?.isLocalLoading == true {
                self?.isLoading = true
            }
        }
        
        CacheManagerOBJC.shared.getApiResponseFromCache(forkey: cacheKey(forPage: page), onCacheLoaded: cacheLoaded, onNotFound: { [weak self] in
            // If there's no cache, try to work with what we have locally, filtering the results
            self?.loacalFilter({ filtered in
                self?.isLocalLoading = false
                self?.isLoading = filtered.isEmpty
                if !filtered.isEmpty {
                    self?.updateSections(raw: nil, parsed: filtered)
                }
            })
        })
    }
    
    private func cacheLoaded(result: SearchApi.SearchApiResponse) {
        isLocalLoading = false
        guard result.list.count > 0 else {
            isLoading = true
            return
        }
        
        updateSections(raw: result)
    }
    
    private func loacalFilter(_ completed: @escaping ([LegacySearchResultSection]) -> Void) {
        if !FeatureManager.isActive(.searchLocalFilter) {
            completed([])
            return
        }
        
        guard term != "" else {
            completed(lastApiResult)
            return
        }
        
        // Start on a bg thread
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let strongSelf = self else {
                return
            }
            // Filter every section
            let filteredSections = strongSelf.lastApiResult.filter { section -> Bool in
                // Filter every row
                section.rows = section.rows.filter({ row -> Bool in
                    // Remove diaereses and make the filter case insensitive
                    let title = (row.basicData.title ?? "").removeDiacriticsAndUppercase()
                    let desc = (row.basicData.descriptionText ?? "").removeDiacriticsAndUppercase()
                    let term = strongSelf.term.removeDiacriticsAndUppercase()
                    
                    // All sections should be of type list after the search started, but if we are doing local filter
                    // on the main page we need to force this
                    section.viewType = .list
                    
                    // Search on the title and description
                    return title.contains(term) || desc.contains(term)
                })
                
                return section.rows.count > 0
            }
            DispatchQueue.main.async {
                completed(filteredSections)
            }
        }
    }
    
    
    // MARK: Helpers
    // We need to ensure that the cacheKey is unique across the system
    func cacheKey(forPage page: Int) -> String {
        
        let coord = LocationManager.shared.authorizedLocation?.coordinate
        let lat = coord?.latitude.truncate(places: 4)
        let lng = coord?.longitude.truncate(places: 4)
        var key = "search_\(term)_\(group ?? "")_\(page)"
        
        if let lat = lat, let lng = lng {
            key += "_\(lat),\(lng)"
        }
        
        return key
    }
    
    private func cancelPendingRequest() {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        ongoingRequest?.cancel()
        ongoingRequest = nil
    }
    
    func updateMapSection(isMapUpdate: Bool = false) {
        // Set map as the first section if:
        // 1- The group is places
        // 2- The term is ""
        // 3- There are other results
        // 4- Isn't loading
        let sectionsWithoutMap = sections.filter { $0.viewType != .highlight }
        if  group == "PLACES" && term == "" && !sectionsWithoutMap.isEmpty && !isLoading {
            if sections.first?.viewType != .map {
                sections.insert(LegacySearchResultSection(viewType: .map, rows: mapRows), at: 0)
            }
        }else{
            // If it's not remove if it exists
            if let index = sections.firstIndex(where: { $0.viewType == .map }) {
                sections.remove(at: index)
            }
        }
        
        // Update the map rows if there's a map section
        if let section = sections.first(where: { $0.viewType == .map }) {
            section.rows = mapRows
        }

        if sections.contains(where: { $0.type == .recents }) {
            sections.removeAll(where: { $0.type == .recents })
        }
        
        // Call the block to update the vc about the sections update
        didUpdate?(isMapUpdate)
    }
    
    private func mergeNewPage(_ newSections: [LegacySearchResultSection]) -> [LegacySearchResultSection] {
        var newData = sections
        for newSection in newSections {
            // If the old section contains the new section
            if let oldSection = newData.first(where: { oldSection -> Bool in
                return oldSection.title == newSection.title
            }) {
                // Append the content at the end
                oldSection.rows.append(contentsOf: newSection.rows)
            }else{
                // Append the new section at the end
                newData.append(newSection)
            }
        }
        
        return newData
    }
    
    private func updateSections(raw: SearchApi.SearchApiResponse?, parsed: [LegacySearchResultSection]? = nil) {
        let filteredSections = filter(sections: raw?.list ?? parsed ?? [])
        sections = filteredSections

        if let _ = raw {
            lastApiResult = filteredSections
        }

        isLoading = false
        
        guard LocationManager.shared.authorizedLocation != nil else {
            return
        }
        
        loadMapDataWithSearchData()
    }

    private func filter(sections: [LegacySearchResultSection]) -> [LegacySearchResultSection] {
        var filteredSections = removeEmpty(sections: sections)

        if FeatureManager.isActive(.searchHighlight) == false {
            filteredSections = remove(sections: filteredSections, viewType: .highlight)
        }

        if Permission.contacts.status != .authorized || dependencies.featureManager.isActive(.experimentPayPersonBool) {
            filteredSections = remove(sections: filteredSections, type: .contact)
        }

        if isSearch == false && dependencies.featureManager.isActive(.experimentPayPersonBool) == false {
            filteredSections = removeRows(from: filteredSections, type: .p2p)
        }

        filteredSections = removeRows(from: filteredSections, type: .unknown)

        return filteredSections
    }

    private func removeRows(from sections: [LegacySearchResultSection], type: SearchResultType) -> [LegacySearchResultSection] {
        guard sections.lazy.flatMap(\.rows).contains(where: { $0.type == type }) else { return sections }

        return sections.lazy.map { section in
            section.rows.removeAll(where: { $0.type == type })
            return section
        }
    }

    private func remove(sections: [LegacySearchResultSection], viewType: SearchResultViewType) -> [LegacySearchResultSection] {
        sections.filter { $0.viewType != viewType }
    }

    private func remove(sections: [LegacySearchResultSection], type: LegacySearchResultSectionType) -> [LegacySearchResultSection] {
        sections.filter { $0.type != type }
    }

    private func removeEmpty(sections: [LegacySearchResultSection]) -> [LegacySearchResultSection] {
        sections.filter(\.rows.isNotEmpty)
    }
    
    // MARK: Map
    private func loadMapDataWithSearchData() {
        guard let listRows = sections.last?.rows else { return }

        isMapLoading = false
        mapRows = listRows
            .lazy
            .compactMap { $0.fullData as? PPStore }
            .map { LegacySearchResultRow(type: .map, fullData: $0) }

        updateMapSection(isMapUpdate: true)
    }
    
    func loadRecentsSearch(_ completion: @escaping (([SearchRecentCacheRow]?) -> Void)) {
        searchRecentsManager.loadRecentsSearch(completion)
    }
    
    func saveRecentSearch(row: LegacySearchResultRow, tab: SearchTab, section: String) {
        searchRecentsManager.saveRecentSearch(row: row, tab: tab, section: section)
    }
    
    func isCellSelectable(at indexPath: IndexPath, isPersonRequestingPayment: Bool) -> Bool {
        guard isPersonRequestingPayment else {
            return true
        }
        let section = sections[indexPath.section]
        let row = section.rows[indexPath.row]
        return row.basicData.canReceivePaymentRequest ?? false
    }

    func userDidBeginSearching(id: UUID, tabName: String, origin: SearchEvent.SearchOrigin) {
        let event = SearchEvent.searchStarted(id: id, tabName: tabName, origin: origin)
        dependencies.analytics.log(event)
    }

    func trackSelectionEvents(row: LegacySearchResultRow,
                              tab: String,
                              section: String,
                              position: Int,
                              id: UUID,
                              type: SearchEvent.InteractionType) {
        let touchPictureEvent = ProfileEvent.touchPicture(from: .paySuggestions)
        dependencies.analytics.log(touchPictureEvent)

        let itemAccessedEvent = PaymentEvent.itemAccessed(
            type: row.type.rawValue,
            id: row.basicData.id,
            name: itemAccessedName(for: row),
            section: (name: itemAccessedSection(for: row, tab: tab, section: section), position: position)
        )

        let resultInteractedEvent = SearchEvent.resultInteracted(id: id,
                                                                 query: term,
                                                                 tab: tab,
                                                                 section: section,
                                                                 pos: position,
                                                                 itemId: row.basicData.id,
                                                                 type: type)
        dependencies.analytics.log(itemAccessedEvent)
        dependencies.analytics.log(resultInteractedEvent)
    }

    private func itemAccessedSection(for row: LegacySearchResultRow, tab: String, section: String) -> String {
        guard isSearch == false else {
            return "pagar - search"
        }

        switch row.type {
        case .person:
            return "pagar principais - contatos"
        case .p2p:
            return "pagar principais - serviços"
        default:
            return section.isEmpty ? tab.lowercased() : "\(tab) - \(section)".lowercased()
        }
    }

    private func itemAccessedName(for row: LegacySearchResultRow) -> String? {
        guard case .p2p = row.type else {
            return row.basicData.title
        }
        
        return "pagar pessoas"
    }

    private func getRequestOriginFlow() -> SearchRequest.OriginFlow {
        if group == "PLACES" {
            return isSearch ? .searchResultsPlacesTab : .placesTab
        } else if isPersonRequestingPayment {
            return .p2pRequestPayment
        } else {
            return isSearch ? .searchResults : .standard
        }
    }
}
