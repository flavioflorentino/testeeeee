final class SearchRecentsViewModel: SearchViewModel {
    override func setup() {
        isSearch = true
        isLoading = false
        loadRecentsSearch { [weak self] recents in
            guard let recents = recents else {
                return
            }
            self?.loadCachedRows(recents: recents)
        }
    }
    
    func loadCachedRows(recents: [SearchRecentCacheRow]) {
        let rows = recents.map { cacheRow -> LegacySearchResultRow in
            // remove distance from row
            if cacheRow.row.type == .store {
                cacheRow.row.basicData.titleDetail = ""
            }
            return cacheRow.row
        }
        
        let section = LegacySearchResultSection(viewType: .list, rows: rows)
        section.title = rows.isEmpty ? nil : SearchLegacyLocalizable.recentSearches.text
        section.style = .recentSearch
        sections = [section]
        hasMore = false
    }
    
    override func search(page: Int) {
        return
    }
    
    override func refresh(clearBefore: Bool) {
        updateMapSection()
    }
}
