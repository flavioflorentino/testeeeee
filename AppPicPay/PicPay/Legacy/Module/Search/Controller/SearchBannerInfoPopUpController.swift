import UIKit

final class SearchBannerInfoPopUpController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var button: UIPPButton!
    
    // public properties
    let info: SearchResultBanner.Info
    private let biometric = BiometricTypeAuth()
    // MARK: - Initializer
    
    init(info: SearchResultBanner.Info){
        self.info = info
        super.init(nibName: "SearchBannerInfoPopUpController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text =  info.title
        textLabel.text = info.text
        button.setTitle(info.button, for: .normal)
    }
    
    @IBAction private func close() {
        dismiss(animated: true)
    }

}
