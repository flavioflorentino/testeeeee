import AnalyticsModule
import CarbonKit
import Foundation
import SwiftyJSON
import UI
import FeatureFlag

final class SearchTabsController: PPBaseViewController {
    // MARK: Private vars
    fileprivate var carbonTabSwipeNavigation: CarbonTabSwipeNavigation?
    var viewControllers: [SearchGenericViewController]?
    fileprivate var currentIndex = 0
    fileprivate var isFirstLoad = true
    // Same across tabs, that's why this isn't in SearchGenericViewController
    fileprivate var didAskForPermissionsOnce = false
    
    fileprivate var didViewAppear: Bool = false
    
    // This should probably come from the server, but ...
    fileprivate let notdisplayTypeIconsInGroup = [
        "store"
    ]

    private var analyticsSearchSessionID: UUID?
    private var indicatorHeight: CGFloat = 3.0

    // MARK: Public vars
    var tabs: [SearchTab]?
    var ignorePermissionPrompt = false
    var isSearch: Bool = false
    var isPersonRequestingPayment = false
    var shouldHideToolbarForSingleTab = false {
        didSet {
            carbonTabSwipeNavigation?.setTabBarHeight(shouldHideToolbarForSingleTab ? 0.0 : toolBarHeight)
        }
    }

    var toolBarHeight: CGFloat = 50
    
    // Needs to be setted by the parent to have access to the current search bar term
    var getCurrentTerm: (() -> (String))?
    
    var clearSearchBar: (() -> Void)?
    var navigationWillMoveAt: ((UInt) -> Void)?
    var navigationDidMoveAt: ((UInt) -> Void)?
    
    // Push notification related
    var initialPage: String? {
        didSet {
            // Tries to find a tab that matches the push notification ID and set it as active
            guard let page = initialPage,
                let tabs = tabs else {
                    return
            }
            
            var pageIndex = 0
            // If we don't find the tab with the given type, just show the first tab
            if let foundIndex = tabs.firstIndex(where: { $0.id == page }) {
                pageIndex = tabs.startIndex.distance(to: foundIndex)
            }
            
            currentIndex = pageIndex
            carbonTabSwipeNavigation?.setCurrentTabIndex(UInt(pageIndex), withAnimation: true)
            initialPage = nil
        }
    }
    
    var segmentedControlIsEnabled: Bool = true {
        didSet {
            carbonTabSwipeNavigation?.carbonSegmentedControl?.isEnabled = segmentedControlIsEnabled
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    
    init(ignorePermissionPrompt: Bool, isPersonRequestingPayment: Bool, tabs: [SearchTab]? = nil) {
        self.isPersonRequestingPayment = isPersonRequestingPayment
        self.ignorePermissionPrompt = ignorePermissionPrompt
        self.tabs = tabs

        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        // Register observer for permition status change
        registerObservers()
        setupTabs()
        setupCarbon()
        switchSearchTabIfNecessary()
    }
    
    private func registerObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(permissionChanged), name: NSNotification.Name(rawValue: SearchGenericViewController.NotificationChangePermissionStatus), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(resetToFirstTabOnPayment), name: NSNotification.Name(rawValue: NotificationName.paymentNew), object: nil)
    }
    
    @objc
    private func resetToFirstTabOnPayment() {
        DispatchQueue.main.async { [weak self] in
            self?.currentIndex = 0
            // Carbon kit goes crazy if we call `self?.carbonTabSwipeNavigation?.setCurrentTabIndex(0, withAnimation: true)` here
            // Instead we instantiate it again
            self?.setupCarbon()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
        didViewAppear = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if didViewAppear {
            let tabName = tabs?[currentIndex].name ?? ""
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                if self.viewIfLoaded?.window != nil {
                    BreadcrumbManager.shared.addCrumb(tabName, typeOf: .tab)
                }
            }
        }
    }
    
    // Broadcast text change to all children
    func broadcastSearchTextChange(text: String) {
        viewControllers?[currentIndex].searchTextChanged(text: text)
    }
    
    // MARK: Setup functions
    private func setupCarbon() {
        let items = tabs?.map { $0.name }
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(
            items: items,
            delegate: self
        )
        carbonTabSwipeNavigation?.insert(intoRootViewController: self)
        
        // style
        carbonTabSwipeNavigation?.toolbar.isTranslucent = false
        carbonTabSwipeNavigation?.toolbar.clipsToBounds = true
        carbonTabSwipeNavigation?.setTabBarHeight(shouldHideToolbarForSingleTab ? 0.0 : toolBarHeight)
        carbonTabSwipeNavigation?.setIndicatorHeight(indicatorHeight)
        carbonTabSwipeNavigation?.carbonTabSwipeScrollView.isScrollEnabled = false
        
        carbonTabSwipeNavigation?.carbonSegmentedControl?.backgroundColor = Colors.backgroundPrimary.color
        carbonTabSwipeNavigation?.view.backgroundColor = Colors.backgroundPrimary.color
        carbonTabSwipeNavigation?.setSelectedColor(
            Colors.grayscale800.color,
            font: Typography.bodyPrimary(.highlight).font()
        )
        carbonTabSwipeNavigation?.setNormalColor(
            Colors.grayscale500.color,
            font: Typography.bodyPrimary(.highlight).font()
        )
        carbonTabSwipeNavigation?.setIndicatorColor(Colors.branding600.color)
        
        guard let count = tabs?.count else {
            return
        }
        let segmentWidth = ceil(view.frame.size.width / CGFloat(count))
        for i in 0..<count {
            carbonTabSwipeNavigation?.carbonSegmentedControl?.setWidth(segmentWidth, forSegmentAt: i)
        }
    }
    
    func setupTabs() {
        if tabs == nil {
            tabs = SearchTab.parse(json: FeatureManager.text(.tabList))
        }
        
        viewControllers = tabs?.map({ tab -> SearchGenericViewController in
            let tabVc = SearchGenericViewController(dependencies: DependencyContainer())
            tabVc.tabType = tab
            tabVc.isSearch = self.isSearch
            tabVc.shouldSaveRecents = self.isSearch
            tabVc.shouldSetupModelWhenLoaded = !self.isSearch
            tabVc.shouldDisplayTypeIcons = true
            tabVc.viewModel.isSearch = self.isSearch
            tabVc.isPersonRequestingPayment = isPersonRequestingPayment
            tabVc.delegate = self
            
            if let typeName = tabVc.tabType?.id, !self.isSearch {
                if notdisplayTypeIconsInGroup.contains(typeName) {
                    tabVc.shouldDisplayTypeIcons = false
                }
            }
            
            tabVc.didSearchFinished = { [weak self] word, isSearchEmpty in
                guard
                    let self = self,
                    word.isNotEmpty,
                    !self.isPersonRequestingPayment,
                    let analyticsID = self.analyticsSearchSessionID
                    else {
                        return
                }
                Analytics.shared.log(SearchEvent.searchResultViewed(isEmpty: isSearchEmpty, query: word, tab: tab.name))
                Analytics.shared.log(SearchEvent.resultsViewed(id: analyticsID,
                                                               query: word,
                                                               isEmpty: isSearchEmpty,
                                                               tabName: tab.name))
            }
            
            tabVc.didAskForPermissions = { [weak self] in
                self?.didAskForPermissionsOnce = true
            }
            
            tabVc.shouldAskPermission = { [weak self] in
                guard let strongSelf = self else {
                    return false
                }
                return !strongSelf.didAskForPermissionsOnce && !strongSelf.ignorePermissionPrompt && tab.shouldAskPermissions
            }
            
            tabVc.arePermissionsImportant = tab.shouldAskPermissions
            return tabVc
        })
    }
    
    // MARK: - Public Methods
    public func getTabName(at index: UInt) -> String? {
        let index = Int(index)
        
        guard let tabs = tabs, tabs.indices.contains(index) else {
            return nil
        }
        
        return tabs[index].name
    }
    
    // MARK: Helpers
    fileprivate func getStoreTabImage(name: String, active: Bool) -> UIImage {
        return tintAndEmbedTextOn(image: #imageLiteral(resourceName: "ico_tab_picpay_store"), isActive: active, embedText: name)
    }
    
    fileprivate func getPlaceTabImage(name: String, active: Bool) -> UIImage {
        return tintAndEmbedTextOn(image: #imageLiteral(resourceName: "stablishment.png"), isActive: active, embedText: name)
    }
    
    private func tintAndEmbedTextOn(image: UIImage, isActive: Bool, embedText: String) -> UIImage {
        let color: UIColor = isActive ? Palette.ppColorBranding300.color : Palette.ppColorGrayscale400.color
        let tintedImage = image.withRenderingMode(.alwaysTemplate).imageMaskedAndTinted(with: color) ?? image
        let segFont = UIFont.systemFont(ofSize: 14, weight: .medium)
        let embedImage = UIImage.textEmbededImage(image: tintedImage,
                                                  string: embedText,
                                                  color: color,
                                                  segFont: segFont,
                                                  opacity: 1.0)
        return embedImage
    }
    
    // MARK: Actions
    @objc
    private func permissionChanged() {
        // Wait a bit for a location update or whatever and refresh the model
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) { [weak self] in
            self?.viewControllers?.forEach({ vc in
                vc.refresh()
            })
        }
    }
}

extension SearchTabsController: CarbonTabSwipeNavigationDelegate {
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        guard let searchViewController = viewControllers?[Int(index)] else {
            return UIViewController()
        }
        return searchViewController
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
        currentIndex = Int(index)
        
        let currentVc = viewControllers?[currentIndex]
        let term = getCurrentTerm?() ?? ""
        
        currentVc?.setInitialTerm(term: term)
        
        if let shouldRefresh = tabs?[currentIndex].autoRefresh,
            term.isEmpty && shouldRefresh && !isFirstLoad {
            currentVc?.refresh(ignoreMapLoading: true)
        }
        
        let tabName = tabs?[currentIndex].name ?? ""
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
            if self.viewIfLoaded?.window != nil {
                BreadcrumbManager.shared.addCrumb(tabName, typeOf: .tab)
            }
        }
        
        isFirstLoad = false
        navigationWillMoveAt?(index)
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {
        navigationDidMoveAt?(index)
    }
}

extension SearchTabsController {
    func switchSearchTabIfNecessary() {
        guard
            let flagDict = FeatureManager.json(.featureOpenAppTabV2),
            let rawTab = flagDict["tab"] as? String,
            let _ = MainTabIdentifier(rawValue: rawTab),
            let subTab = flagDict["sub_tab"] as? String,
            let index = PaymentTabIdentifier(rawValue: subTab)?.tabIndex
            else {
                return
        }
        
        carbonTabSwipeNavigation?.setCurrentTabIndex(index, withAnimation: true)
        Analytics.shared.log(SearchEvent.firstScreenViewed(PaymentTabIdentifier(rawValue: subTab)))
    }

    func userDidBeginSearching(origin: SearchEvent.SearchOrigin) {
        guard
            let selectedTabIndex = carbonTabSwipeNavigation?.currentTabIndex,
            let viewControllers = self.viewControllers,
            let tabs = self.tabs else {
                return
        }
        let newSearchSessionID = UUID()
        let selectedViewController = viewControllers[Int(selectedTabIndex)]
        let selectedTab = tabs[Int(selectedTabIndex)]

        selectedViewController.viewModel.userDidBeginSearching(id: newSearchSessionID, tabName: selectedTab.name, origin: origin)
        analyticsSearchSessionID = newSearchSessionID
    }
}

extension SearchTabsController: SearchGenericViewControllerDelegate {
    func getAnalyticsID() -> UUID? {
        analyticsSearchSessionID
    }
}
