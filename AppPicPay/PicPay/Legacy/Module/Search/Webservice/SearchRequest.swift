//
//  SearchRequest.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 05/01/18.
//

import Foundation
import SwiftyJSON
import Alamofire

final class SearchRequest: BaseApiRequest {
    //MARK: Variable params
    var term: String?
    var group: String?
    var longitude: Double?
    var latitude: Double?
    var page: Int?
    
    var paramsToBeMerged: [String: Any]?
    
    // All params are obligatory
    func obligatoryParams(params: [String: Any?]) throws -> [String: Any] {
        let nonNil = params.nilsRemoved
        
        if nonNil.keys.count < params.keys.count {
            throw PicPayError(message: "Missing params")
        }
        
        return nonNil
    }
    
    func mergeToParams(params: [String: Any]) {
        self.paramsToBeMerged = params
    }
    
    func toParams() throws -> [String: Any] {
        var params = [
            "group": group
            ] as [String : Any?]
        
        if let lng = longitude {
            params["longitude"] = lng
        }
        
        if let lat = latitude {
            params["latitude"] = lat
        }
        
        if let term = term {
            params["term"] = term
        }
        
        if let page = page {
            params["page"] = page
        }
        
        return try obligatoryParams(params: params)
    }

    enum OriginFlow {
        case standard
        case searchResults
        case searchResultsPlacesTab
        case placesTab
        case feed
        case p2pRequestPayment

        var endpoint: String {
            switch self {
            case .standard, .searchResults, .p2pRequestPayment: return "search"
            case .placesTab, .searchResultsPlacesTab: return "v1/places"
            case .feed: return "suggestions"
            }
        }
    }
}
