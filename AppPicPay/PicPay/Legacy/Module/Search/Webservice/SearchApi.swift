import Core
import FeatureFlag
import Foundation
import Alamofire

final class SearchApi: BaseApi {
    typealias SearchApiResponse = BaseApiListResponse<LegacySearchResultSection>
    private let featureManager: FeatureManagerContract

    init(featureManager: FeatureManagerContract = FeatureManager.shared) {
        self.featureManager = featureManager
    }

    func search(request: SearchRequest,
                originFlow: SearchRequest.OriginFlow = .standard,
                cacheKey: String,
                onCacheLoaded: @escaping ((SearchApiResponse) -> Void) = { _ in },
                completion: @escaping ((PicPayResult<SearchApiResponse>) -> Void)) -> DataRequest? {
        guard !shouldUseNewSearchEngine(originFlow: originFlow, tabName: request.group) else {
            return newSearch(request: request, cacheKey: cacheKey, onCacheLoaded: onCacheLoaded, completion: completion)
        }

        do {
            let expiryTime = FeatureManager.number(.searchResultCache)
            if FeatureManager.isActive(.experimentNewSuggestionRouteBool) {
                let parameters = originFlow == .feed ? nil : try request.toParams()
                requestManager.apiRequestWithCache(cacheKey,
                                                   expiryTime: expiryTime?.doubleValue,
                                                   onCacheLoaded: onCacheLoaded,
                                                   endpoint: originFlow.endpoint,
                                                   method: .get,
                                                   parameters: parameters).responseApiObject(completionHandler: completion)
            } else {
                let parameters = try request.toParams()
                requestManager.apiRequestWithCache(cacheKey,
                                                   expiryTime: expiryTime?.doubleValue,
                                                   onCacheLoaded: onCacheLoaded,
                                                   endpoint: originFlow.endpoint,
                                                   method: .get,
                                                   parameters: parameters).responseApiObject(completionHandler: completion)
            }
        } catch {
            completion(.failure(.generalError))
        }

        return nil
    }

    func setNewsWasViewed(forNews newsId: String) {
        Api<NoContent>(endpoint: NewsEndpoint.viewed(id: newsId)).execute { _ in }
    }
    
    private func shouldUseNewSearchEngine(originFlow: SearchRequest.OriginFlow, tabName: String?) -> Bool {
        let experiment = SearchExperiment(tabName: tabName ?? "")
        return (originFlow == .searchResults || originFlow == .searchResultsPlacesTab) && (experiment == .jarbas)
    }
}

enum SearchExperiment: String {
    case legacy
    case legacyControl = "legacy_control"
    case jarbas
    
    init(string: String) {
        self = SearchExperiment(rawValue: string) ?? .legacy
    }

    init(tabName: String) {
        switch tabName.lowercased() {
        case "todos", "main":
            self = SearchExperiment(string: FeatureManager.text(.newSearchEngineMain))
        case "pessoas", "consumers":
            self = SearchExperiment(string: FeatureManager.text(.newSearchEngineConsumer))
        case "locais", "places":
            self = SearchExperiment(string: FeatureManager.text(.newSearchEnginePlaces))
        case "store":
            self = SearchExperiment(string: FeatureManager.text(.newSearchEngineStore))
        default:
            self = .legacy
        }
    }
}
