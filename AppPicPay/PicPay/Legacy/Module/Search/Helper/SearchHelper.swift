import AnalyticsModule
import Core
import FeatureFlag
import PIX
import UI
import UIKit
import Card
import Billet
import IncomeTaxReturn

final class SearchHelper {
    struct Options {
        var term: String?
    }
    
    enum Origin: String {
        case homeSugestions = "Seção Início"
        case homeFavorites = "Seção Favoritos"
        case payMain = "Pagar Principais"
        case payStore = "Pagar Store"
        case paySearch = "Pagar Busca"
        case requestPaymentSearch = "Cobrar Busca"
        case myCode = "Meu Código"
        case contacts = "from_contacts"
        case search = "Search"
    }

    private static var childCoordinator: Coordinating?
    
    static let popupPresenter = PopupPresenter()
    
    static func openPayment(
        withSection section: LegacySearchResultSection,
        row: LegacySearchResultRow,
        from: UIViewController,
        options: Options? = nil,
        origin: Origin? = nil
    ) {
        if let origin = section.title {
            BreadcrumbManager.shared.addCrumb(origin, typeOf: .listSection)
        }
        
        switch row.type {
        case .person where origin == .requestPaymentSearch:
            showPersonChargeViewController(data: row.fullData, from: from)
        case .person:
            showPersonViewController(data: row.fullData, title: section.title ?? "", from: from, options: options)
        case .store:
            showStoreViewController(data: row.fullData, title: section.title ?? "", from: from, options: options)
        case .digitalGood, .financial, .externalTransfer:
            guard let digitalGood = row.fullData as? DGItem else {
                return
            }
            
            if case .boleto = digitalGood.service {
                let dependencies = DependencyContainer()
                guard dependencies.featureManager.isActive(.isNewBilletHubAvailable) else {
                    DGHelpers.openBoletoFlow(viewController: from, origin: Origin.search.rawValue)
                    return
                }
                
                let hubViewController = BilletHubFactory.make(with: .search)
                let navigation = UINavigationController(rootViewController: hubViewController)
                from.present(navigation, animated: true)
                
            } else {
                DGHelpers.openDigitalGoodFlow(for: digitalGood, viewController: from, origin: origin?.rawValue ?? section.title)
            }
            
        case .map:
            // OPEN ONLY IN SEARCH VIEW
            break
        case .subscription:
            // Subscription is a popup so we need to dismiss the keyboard
            //dismissKeyboard()
            guard let producer = row.fullData as? ProducerProfileItem else {
                return
            }
            let popup = ProducerProfileViewController(parentViewController: from, model: ProducerViewModel(producer: producer), origin: .search)
            popupPresenter.showPopup(popup, fromViewController: from)
        case .banner:
            break
        case .webview:
            
            if let webviewUrl = row.basicData.id, let url = URL(string: webviewUrl) {
                let controller = WebViewFactory.make(with: url)
                controller.hidesBottomBarWhenPushed = true
                from.navigationController?.pushViewController(controller, animated: true)
            }
            break
        case .linkGroup:
            let all = SearchGenericViewController.controllerFromStoryboard()
            all.linkedFrom = row.basicData.id
            all.arePermissionsImportant = false
            all.title = row.basicData.id
            all.hidesBottomBarWhenPushed = true
            from.navigationController?.pushViewController(all, animated: true)
        case .unknown:
            break
        case .p2m:
            guard
                let data = row.fullData as? P2MItem,
                data.service == .recommendation else {
                P2MHelpers.presentScanner(from: from)
                return
            }
            
            showIndicatePlace(from: from)
        case .p2p:
            let title = (row.fullData as? P2PItem)?.name
            startPayPeopleFlow(title: title, from: from)
        case .news:
            guard
                let news = row.fullData as? News,
                let url = URL(string: news.url) else {
                return
            }
            SearchApi().setNewsWasViewed(forNews: news.id)

            let controller = WebViewFactory.make(with: url)
            from.present(UINavigationController(rootViewController: controller), animated: true)
        case .pix:
            guard let pixItem = row.fullData as? PixSearchItem else {
                return
            }
            presentPIXFlow(pixItem: pixItem, origin: origin, from: from)
        case .feature:
            guard let featureItem = row.fullData as? SearchFeatureItem,
                  let navigationVC = from.navigationController else {
                return
            }
            presentFeatureFlow(item: featureItem, from: navigationVC)

        case .deeplink:
            guard let deeplinkItem = row.fullData as? DeeplinkSearchResultItem else { return }
            open(deeplink: deeplinkItem.deeplink)
        }
    }

    private static func open(deeplink: URL?) {
        guard let deeplink = deeplink, UIApplication.shared.canOpenURL(deeplink) else { return }
        UIApplication.shared.open(deeplink)
    }

    private static func presentFeatureFlow(item: SearchFeatureItem, from navigationVC: UINavigationController) {
        switch item.service {
        case .picPayCard:
            presentPicPayCardFlow(from: navigationVC)
        case .mgmCode:
            presentMGMCodeFlow(from: navigationVC)
        case .wallet:
            presentWalletFlow()
        case .reportIncome:
            presentReportIncomeFlow(from: navigationVC)
        }
    }

    private static func presentPicPayCardFlow(from navigationVC: UINavigationController) {
        let coordinator = HybridDeeplinkFlowCoordinator(navigationController: navigationVC)
        coordinator.start()
        childCoordinator = coordinator
    }

    private static func presentMGMCodeFlow(from navigationVC: UINavigationController) {
        let storyboardVC = ViewsManager.peopleSearchStoryboardViewController("SocialShareCode")
        guard let viewController = storyboardVC as? SocialShareCodeViewController else { return }
        if #available(iOS 11, *) {
            navigationVC.setLargeTitle(shouldBeLarge: false)
        }
        navigationVC.pushViewController(viewController, animated: true)
    }

    private static func presentWalletFlow() {
        AppManager.shared.mainScreenCoordinator?.showWalletScreen()
    }

    private static func presentReportIncomeFlow(from navigationVC: UINavigationController) {
        let viewController = IncomeStatementsFactory.make()
        if #available(iOS 11, *) {
            navigationVC.setLargeTitle(shouldBeLarge: false)
        }
        navigationVC.pushViewController(viewController, animated: true)
    }

    private static func presentPIXFlow(pixItem: PixSearchItem, origin: Origin?, from viewController: UIViewController) {
        let flow: PIXConsumerFlow
        switch pixItem.service {
        case .keyManager:
            flow = .keyManagement
        case .hub:
            flow = .hub(origin: pixHubOrigin(origin), paymentOpening: PaymentPIXProxy(), bankSelectorType: BanksFactory.self, qrCodeScannerType: ScannerBaseViewController.self)
        }
        PIXConsumerFlowCoordinator(originViewController: viewController, originFlow: .suggestions, destination: flow).start()
    }
    
    private static func pixHubOrigin(_ origin: Origin?) -> String {
        guard let origin = origin else {
            return ""
        }
        switch origin {
        case .payMain:
            return "from_services"
        case .homeSugestions:
            return "from_suggestions"
        default:
            return origin.rawValue
        }
    }
    
    fileprivate static func showPersonViewController(data: NSObject?, title: String, from: UIViewController, options: Options? = nil) {
        guard let contact = data as? PPContact else {
            return
        }
        
        if FeatureManager.isActive(.newP2P) {
            presentNewP2PPayment(contact: contact, from: from, options: options)
            return
        }
        
        presentLegacyP2PPayment(contact: contact, title: title, from: from, options: options)
    }
    
    private static func presentNewP2PPayment(contact: PPContact, from: UIViewController, options: Options? = nil) {
        let usedSearch = !(options?.term?.isEmpty ?? true)
        let paidUsername = options?.term?.contains("@") == true
        let orchestrator = P2PPaymentOrchestratorModel(origin: .homeSugestions, usedSearch: usedSearch, paidUsername: paidUsername, extra: nil)
        let paymentOrchestrator = P2PPaymentOrchestrator(contact: contact, orchestrator: orchestrator)
        
        guard let viewController = paymentOrchestrator.paymentViewController else {
            return
        }
        
        let navigation = UINavigationController(rootViewController: viewController)        
        from.present(navigation, animated: true)
    }
    
    private static func presentLegacyP2PPayment(contact: PPContact, title: String, from: UIViewController, options: Options? = nil) {
        guard let newTransactionViewController = ViewsManager.peerToPeerStoryboardFirtViewController() as? NewTransactionViewController else {
            return
        }
        
        newTransactionViewController.preselectedContact = contact
        newTransactionViewController.touchOrigin = title
        newTransactionViewController.preselectedContactFromSearch = !(options?.term?.isEmpty ?? true)
        newTransactionViewController.preselectedContactFromSearchWithUsername = options?.term?.contains("@") == true
        
        from.present(PPNavigationController(rootViewController: newTransactionViewController), animated: true)
    }
    
    private static func showPersonChargeViewController(data: NSObject?, from viewController: UIViewController) {
        
        guard let contact = data as? PPContact else {
            return
        }
        
        Analytics.shared.log(PaymentRequestUserEvent.didSelectUser(contact.wsId))
        
        let controller = PaymentRequestCheckoutFactory.make(contact: contact, backButtonClose: false)
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    fileprivate static func showStoreViewController(data: NSObject?, title: String, from: UIViewController, options: Options? = nil) {
        guard let store = data as? PPStore else {
            return
        }
        
        if store.isCielo {
            let scanner = P2MScannerViewController.controllerFromStoryboard()
            let scannerNavigationController = UINavigationController(rootViewController: scanner)
            from.present(scannerNavigationController, animated: true)
            return
        }
        
        if store.isParkingPayment == IsNewParking, let sellerId = store.sellerId, let storeId = store.storeId {
            let parkingViewController = ParkingCoordinator().start(sellerId: sellerId, storeId: storeId)
            from.present(parkingViewController, animated: true, completion: nil)
            Analytics.shared.log(CheckoutEvent.checkoutScreenViewed(CheckoutEvent.CheckoutEventOrigin.parking))
            return
        }
        
        let paymentDetailsNavigationController = ViewsManager.paymentStoryboardFirtNavigationController()
        guard let paymentViewController = paymentDetailsNavigationController?.topViewController as? PaymentViewController else {
            return
        }
        paymentViewController.store = store
        paymentViewController.from_explore = true
        paymentViewController.touchOrigin = title
        paymentViewController.showCancel = true
        
        if let location = LocationManager.shared.authorizedLocation {
            paymentViewController.bestGpsLat = CGFloat(location.coordinate.latitude)
            paymentViewController.bestGpsLng = CGFloat(location.coordinate.longitude)
            paymentViewController.bestGpsAcc = CGFloat(location.horizontalAccuracy)
        }
        
        paymentViewController.navigationLevel = 10
        
        Analytics.shared.log(CheckoutEvent.checkoutScreenViewed(paymentViewController.store?.isParkingPayment == 1 ?
                                                                    CheckoutEvent.CheckoutEventOrigin.parking :
                                                                    CheckoutEvent.CheckoutEventOrigin.p2m))
        from.present(PPNavigationController(rootViewController:paymentViewController), animated: true, completion: nil)
    }
    
    private static func showIndicatePlace(from: UIViewController) {
        let placeIndicationController = PlaceIndicationFactory.make()
        let navigation = PPNavigationController(rootViewController: placeIndicationController)
        from.present(navigation, animated: true)
    }
}

private extension SearchHelper {
    static func startPayPeopleFlow(title: String?, from viewController: UIViewController) {
        guard let navigationController = viewController.navigationController else { return }

        let coordinator = PayPeopleFactory().makePayPeopleCoordinator(title: title, navigationController: navigationController)
        coordinator.didFinishFlow = {
            childCoordinator = nil
        }

        coordinator.start()
        childCoordinator = coordinator
    }
}
