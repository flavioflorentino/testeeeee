import Cache
import SwiftyJSON

protocol SearchRecentsManageable {
    func loadRecentsSearch(_ completion: @escaping (([SearchRecentCacheRow]?) -> Void))
    func saveRecentSearch(row: LegacySearchResultRow, tab: SearchTab, section: String)
}

final class SearchRecentsManager: SearchRecentsManageable {
    private static let cacheKey = "search-recent-rows"
    private static let maxItems = 5
    
    // MARK: - Recents Search Cache
    
    func loadRecentsSearch(_ completion: @escaping (([SearchRecentCacheRow]?) -> Void)) {
        CacheManagerOBJC.shared.data(for: SearchRecentsManager.cacheKey, onCacheLoaded: { data in
            guard let json = try? JSON(data: data) else {
                return
            }
            
            let recents = json.array?.compactMap { SearchRecentCacheRow(json: $0) } ?? []
            completion(recents)
        }, onNotFound: {
            completion(nil)
        })
    }
    
    func saveRecentSearch(row: LegacySearchResultRow, tab: SearchTab, section: String) {
        let resultCache = SearchRecentCacheRow(row: row, section: section, tab: tab)
        
        loadRecentsSearch { loadedRecents in
            var recents = loadedRecents ?? [SearchRecentCacheRow]()
            
            // remove it the item already exists
            if let index = recents.firstIndex(of: resultCache) {
                recents.remove(at: index)
            }
            
            // insert on list
            recents.insert(resultCache, at: 0)
            
            if recents.count > SearchRecentsManager.maxItems {
                recents.removeLast(recents.count - SearchRecentsManager.maxItems)
            }
            
            self.save(recents)
        }
    }
    
    /// Save the recentes list at local cache
    private func save(_ recents: [SearchRecentCacheRow]) {
        let json = JSON(recents.map({ $0.toJson() }))
        
        // save cache
        guard let data = try? json.rawData() else {
            return
        }
        CacheManagerOBJC.shared.setApiResponseFromCache(with: data, forKey: SearchRecentsManager.cacheKey)
    }
}
