import Foundation
import SwiftyJSON

extension LegacySearchResultRow {
    func toJson() -> JSON? {
        var searchableJSON: JSON

        switch fullData {
        case let contact as PPContact:
            searchableJSON = contact.toJson()
        case let store as PPStore:
            searchableJSON = store.toJson()
        case let digitalGood as DGItem:
            searchableJSON = digitalGood.toJson()
        case let susbscription as ProducerProfileItem:
            searchableJSON = susbscription.toJson()
        case let pix as PixSearchItem:
            searchableJSON = pix.toJson()
        case let p2m as P2MItem:
            searchableJSON = p2m.toJson()
        case let p2p as P2PItem:
            searchableJSON = p2p.toJson()
        case let feature as SearchFeatureItem:
            searchableJSON = feature.toJson()
        default:
            searchableJSON = JSON()
        }

        var dict: [String: Any] = [:]
        dict["data"] = searchableJSON.dictionaryObject
        dict["type"] = self.type.rawValue
        return JSON(dict)
    }
}

private extension PPContact {
    func toJson() -> JSON {
        var dict: [String: Any] = [:]
        dict["image_url_large"] = self.imgUrlLarge
        dict["image_url_small"] = self.imgUrl
        dict["verified"] = self.isVerified
        dict["pro"] = self.businessAccount
        dict["name"] = self.onlineName
        dict["id"] = self.wsId
        dict["username"] = self.username

        return JSON(dict)
    }
}

private extension PPStore {
    func toJson() -> JSON {
        var dict: [String: Any] = [:]
        var sellerDict: [String: Any] = [:]
        sellerDict["name"] = self.name
        sellerDict["img_url"] = self.img_url
        sellerDict["id"] = self.wsId

        var storeDict: [String: Any] = [:]
        storeDict["seller_id"] = self.sellerId
        storeDict["is_cielo"] = self.isCielo
        storeDict["name"] = self.name
        storeDict["img_url"] = self.img_url
        storeDict["is_parking"] = self.isParkingPayment
        storeDict["distance"] = self.distance
        storeDict["address"] = self.address
        storeDict["id"] = self.wsId

        dict["Seller"] = sellerDict
        dict["Store"] = storeDict

        return JSON(dict)
    }
}

private extension DGItem {
    func toJson() -> JSON {
        var dict: [String: Any] = [:]

        dict["description"] = self.itemDescription
        dict["image_url"] = self.imgUrl
        dict["service"] = self.service?.rawValue
        dict["name"] = self.name
        dict["_id"] = self.id
        dict["student_account"] = self.isStudentAccount

        return JSON(dict)
    }
}

private extension ProducerProfileItem {
    func toJson() -> JSON {
        var dict: [String: Any] = [:]

        dict["name"] = self.name
        dict["username"] = self.username
        dict["bio"] = self.profileDescription
        dict["id"] = self.id
        dict["img_url_small"] = self.profileImageUrl

        return JSON(dict)
    }
}

private extension PixSearchItem {
    func toJson() -> JSON {
        var dict: [String: Any] = [:]

        dict["name"] = self.name
        dict["service"] = self.service.rawValue
        dict["description"] = self.descriptionText
        dict["image_url"] = self.imageUrl

        return JSON(dict)
    }
}

private extension P2PItem {
    func toJson() -> JSON {
        var dict: [String: Any] = [:]

        dict["name"] = self.name
        dict["service"] = self.service.rawValue
        dict["description"] = self.descriptionText
        dict["image_url"] = self.imageUrl

        return JSON(dict)
    }
}

private extension P2MItem {
    func toJson() -> JSON {
        var dict: [String: Any] = [:]

        dict["name"] = self.name
        dict["service"] = self.service.rawValue
        dict["description"] = self.descriptionText
        dict["image_url"] = self.imageUrl

        return JSON(dict)
    }
}

private extension SearchFeatureItem {
    func toJson() -> JSON {
        var dict: [String: Any] = [:]

        dict["name"] = self.name
        dict["service"] = self.service.rawValue
        dict["description"] = self.descriptionText
        dict["image_url"] = self.imageUrl

        return JSON(dict)
    }
}
