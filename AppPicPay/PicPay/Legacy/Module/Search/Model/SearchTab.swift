import FeatureFlag
import Foundation
import SwiftyJSON

final class SearchTab: NSObject {
    var name: String
    var group: String
    var id: String
    var shouldAskPermissions: Bool
    var autoRefresh: Bool
    
    init(name: String, group: String, id: String, shouldAskPermissions: Bool, autoRefresh: Bool) {
        self.name = name
        self.group = group
        self.id = id
        self.shouldAskPermissions = shouldAskPermissions
        self.autoRefresh = autoRefresh
    }
    
    init?(json: JSON) {
        guard
            let name = json["name"].string,
            let group = json["group"].string,
            let id = json["id"].string,
            let shouldAskPermission = json["ask_permission"].bool,
            let autoRefresh = json["auto_refresh"].bool
            else {
                SearchTab.notifyNewRelic(.invalidJSON)
                return nil
        }
        
        self.name = name.lowercased().capitalizingFirstLetter()
        self.group = group
        self.id = id
        self.shouldAskPermissions = shouldAskPermission
        self.autoRefresh = autoRefresh
    }
    
    func toJson() -> JSON {
        var dict: [String: Any] = [:]
        dict["name"] = self.name
        dict["group"] = self.group
        dict["id"] = self.id
        dict["ask_permission"] = self.shouldAskPermissions
        dict["auto_refresh"] = self.autoRefresh
        
        return JSON(dict)
    }
    
    static func parse(json stringJson: String) -> [SearchTab] {
        let json = JSON(parseJSON: stringJson)
        guard
            let searchTabs = json["tabs"].array?.map({ SearchTab(json: $0) }) as? [SearchTab],
            searchTabs.isNotEmpty else {
            notifyNewRelic(.invalidJSON)
            return defaultSearchTabs()
        }
        return searchTabs
    }
    
    private static func defaultSearchTabs() -> [SearchTab] {
        let key = FeatureConfig.tabList.rawValue
        let dict = FeatureFlagControlHelper.dictFromFeatureTogglesPlist()
        guard !dict.isEmpty, let stringJson = dict[key] as? String else {
            notifyNewRelic(.invalidJSONInTogglesPlistSomeoneDisabledTheUnitTestThatPrevetentedThis)
            return []
        }
        return parse(json: stringJson)
    }
}

private extension SearchTab {
    enum SearchTabParseError: Error {
        case invalidJSON
        case invalidJSONInTogglesPlistSomeoneDisabledTheUnitTestThatPrevetentedThis
    }
    
    // TODO: update this to use the new Module for Alerts on NewRelic
    // TODO: add unit testing to check if the notification sent is correct
    private static func notifyNewRelic(_ searchTabParseError: SearchTabParseError) {
        let className = "SearchTab"
        let event = "RemoteConfig"
        NewRelic.recordError(searchTabParseError, attributes: ["class": className, "event": event])
        NewRelic.recordError(searchTabParseError)
    }
}
