import SwiftyJSON
import FeatureFlag

enum SearchResultType: String {
    case person = "consumer"
    case store
    case digitalGood = "digital_good"
    case financial = "financial_service"
    case subscription = "membership"
    case p2m
    case p2p
    case map
    case banner = "highlight"
    case webview = "web_view"
    case linkGroup = "link"
    case news
    case externalTransfer = "external_transfer"
    case pix
    case unknown
    case feature
    case deeplink
    
    var description: String {
        switch self {
        case .person:
            return ""
        case .store:
            return "Local"
        case .digitalGood:
            return "Store"
        case .subscription:
            return "Assinatura"
        case .news:
            return "Novidades!"
        default:
            return ""
        }
    }
}

class SearchResultBasics: NSObject {
    var type: SearchResultType?
    var id: String?
    var sellerId: String?
    var imageUrl: String?
    var largeImageUrl: String?
    var title: String?
    var titleDetail: String?
    var descriptionText: String?

    // User only, but well...
    var isPro: Bool?
    var isVerified: Bool?
    var canReceivePaymentRequest: Bool?
}

final class SearchResultBanner: SearchResultBasics {
    var info: Info?
    var name: String?
    
    final class Info: NSObject{
        var title: String
        var text: String
        var button: String
        
        init?(json: JSON) {
            guard let title = json["title"].string,
                let text = json["description"].string,
                let button = json["button"].string else {
                return nil
            }
            
            self.title = title
            self.text = text
            self.button = button
            super.init()
        }
    }
}

final class LegacySearchResultRow: NSObject, BaseApiResponse {
    var type: SearchResultType
    var basicData = SearchResultBasics()
    var fullData: NSObject?
    var json: JSON?
    
    init(type: SearchResultType, fullData: NSObject) {
        self.type = type
        self.fullData = fullData
    }

    required init?(json: JSON) {
        self.json = json
        
        type = SearchResultType(rawValue: json["type"].string ?? "consumer") ?? .unknown
        if json["type"].string == "contact" {
            type = .person
        }
        basicData.type = type
        
        let data = json["data"]
        
        switch type {
        // Done locally for now
        case .banner:
            let banner = SearchResultBanner()
            banner.id = data["id"].string ?? ""
            banner.largeImageUrl = data["image_url"].string ?? ""
            banner.info = SearchResultBanner.Info(json: data["info"])
            banner.name = data["name"].string
            
            basicData = banner
            fullData = LegacySearchResultRow(json:data)
        case .map:
            break
        case .person:
            let contact = PPContact(profileDictionary: data.dictionaryObject)
            basicData.id = String(describing: contact.wsId)
            basicData.imageUrl = contact.imgUrl
            basicData.largeImageUrl = contact.imgUrlLarge
            basicData.title = "@\(contact.username ?? "")"
            basicData.descriptionText = contact.onlineName
            basicData.isPro = contact.businessAccount
            basicData.isVerified = contact.isVerified
            basicData.canReceivePaymentRequest = contact.canReceivePaymentRequest
            
            fullData = contact
        case .store:
            let storeData = data["Store"]
            guard let store = PPStore(profileDictionary: storeData.dictionaryObject) else {
                return nil
            }
            let seller = data["Seller"].dictionary
            store.img_url = seller?["img_url"]?.string
            store.sellerId = seller?["id"]?.string
            store.storeId = storeData["id"].string
            store.isParkingPayment = Int32(storeData["is_parking"].string ?? "0") ?? 3
            if let distanceString = storeData.dictionary?["distance"]?.string,
                let distance = Double(distanceString){
                store.distance = Int32(distance)
                basicData.titleDetail = store.distanceAsString()
            }
            basicData.id = String(describing: store.wsId)
            basicData.sellerId = store.sellerId
            basicData.imageUrl = store.img_url
            basicData.largeImageUrl = store.large_img
            basicData.title = store.name
            basicData.descriptionText = store.address
            basicData.isVerified = store.isVerified
            
            fullData = store
        case .digitalGood, .financial, .externalTransfer:
            guard let digitalGood = DGItem(json: data) else {
                return nil
            }
            basicData.id = digitalGood.id
            basicData.imageUrl = digitalGood.imgUrl
            basicData.largeImageUrl = digitalGood.bannerImgUrl ?? digitalGood.imgUrl
            basicData.title = digitalGood.name
            basicData.descriptionText = digitalGood.itemDescription
            
            fullData = digitalGood
            
        case .p2m:
            guard let p2m = P2MItem(json: data) else {
                return nil
            }
            basicData.id = data["id"].string ?? ""
            basicData.imageUrl = p2m.imageUrl
            basicData.largeImageUrl = p2m.imageUrl
            basicData.title = p2m.name
            basicData.descriptionText = p2m.descriptionText
            
            fullData = p2m

        case .p2p:
            guard let p2p = P2PItem(json: data) else {
                return nil
            }

            basicData.imageUrl = p2p.imageUrl
            basicData.largeImageUrl = p2p.imageUrl
            basicData.title = p2p.name
            basicData.descriptionText = p2p.descriptionText

            fullData = p2p
            
        case .subscription:
            if FeatureManager.isActive(.subscription),
                let producer = ProducerProfileItem(json: data) {
                basicData.id = producer.id
                basicData.imageUrl = producer.profileImageUrl
                basicData.largeImageUrl = producer.backgroundImageUrl
                basicData.title = "@\(producer.username)"
                basicData.descriptionText = producer.name
                
                fullData = producer
            } else {
                return nil
            }
        case .webview:
            basicData.id = data["url"].string ?? ""

        case .linkGroup:
            basicData.id = data["link_to"].string ?? ""

        case .news:
            guard let news = News(json: data, type: type) else {
                return nil
            }
            
            basicData.id = news.id
            basicData.imageUrl = news.imageURL
            basicData.title = news.name
            basicData.descriptionText = news.descriptionText
            
            fullData = news
        case .pix:
            guard let pixItem = PixSearchItem(json: data) else {
                return nil
            }
            basicData.id = pixItem.name
            basicData.imageUrl = pixItem.imageUrl
            basicData.title = pixItem.name
            basicData.descriptionText = pixItem.descriptionText
            
            fullData = pixItem
        case .feature:
            guard let featureItem = SearchFeatureItem(json: data) else {
                return nil
            }

            basicData.id = featureItem.name
            basicData.imageUrl = featureItem.imageUrl
            basicData.title = featureItem.name
            basicData.descriptionText = featureItem.descriptionText

            fullData = featureItem

        case .deeplink:
            guard let deeplinkItem = DeeplinkSearchResultItem(json: data) else { return nil }

            basicData.title = deeplinkItem.name
            basicData.imageUrl = deeplinkItem.imageURL?.absoluteString
            basicData.descriptionText = deeplinkItem.descriptionText

            fullData = deeplinkItem
        case .unknown:
            break
        }
    }
}
