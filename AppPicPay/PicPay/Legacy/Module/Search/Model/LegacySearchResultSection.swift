//
//  SearchResult.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 21/12/17.
//

import Foundation
import SwiftyJSON

enum SearchResultViewType: String {
    case highlight = "highlight"
    case list = "list"
    case carousel = "carousel"
    case map = "map"
}

enum LegacySearchResultSectionType: String {
    case recents
    case contact
}

final class LegacySearchResultSection: BaseApiResponse {
    
    enum Style {
        case normal
        case home
        case recentSearch
    }
    
    var id: String?
    var title: String?
    var linksTo: String?
    var type: LegacySearchResultSectionType?
    var viewType: SearchResultViewType
    var rows: [LegacySearchResultRow]
    var hasMore: Bool
    var style: Style = .normal
    
    init(viewType: SearchResultViewType, rows: [LegacySearchResultRow]) {
        self.viewType = viewType
        self.rows = rows
        self.hasMore = false
    }
    
    required init?(json: JSON) {
        id = json["id"].string
        title = json["title"].string
        linksTo = json["link_to"].string
        
        type = LegacySearchResultSectionType(rawValue: json["type"].string ?? "")
        viewType = SearchResultViewType(rawValue: json["view"].string ?? "list") ?? .list

        rows = []
        for row in json["rows"].array ?? [] {
            if let rowData = LegacySearchResultRow(json: row) {
                rows.append(rowData)
            }
        }
        
        hasMore = rows.count < (json["total_count"].number?.intValue ?? -1)
    }
}
