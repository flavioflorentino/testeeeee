import Foundation
import SwiftyJSON

final class DeeplinkSearchResultItem: NSObject {
    // MARK: - Properties
    let name: String
    let descriptionText: String
    let imageURL: URL?
    let deeplink: URL?

    // MARK: - Initialization
    required init(name: String, descriptionText: String, imageURL: URL?, deeplink: URL?) {
        self.name = name
        self.descriptionText = descriptionText
        self.imageURL = imageURL
        self.deeplink = deeplink
    }
}

// MARK: - SwiftyJSON
extension DeeplinkSearchResultItem {
    private enum JSONKeys {
        static let name = "name"
        static let description = "description"
        static let imageURL = "image_url"
        static let deeplink = "deeplink"
    }

    convenience init?(json: JSON) {
        guard
            let name = json[JSONKeys.name].string,
            let descriptionText = json[JSONKeys.description].string
        else {
            return nil
        }

        let imageURL = json[JSONKeys.imageURL].url
        let deeplink = json[JSONKeys.deeplink].url

        self.init(name: name, descriptionText: descriptionText, imageURL: imageURL, deeplink: deeplink)
    }
}
