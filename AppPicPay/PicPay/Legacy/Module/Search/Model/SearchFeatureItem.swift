import SwiftyJSON
import Foundation

final class SearchFeatureItem: NSObject {
    enum ServiceType: String {
        case picPayCard = "pic_pay_card"
        case wallet
        case mgmCode = "mgm_code"
        case reportIncome = "report_income"
    }

    let name: String
    let descriptionText: String
    let imageUrl: String
    let service: ServiceType

    init(name: String, descriptionText: String, imageUrl: String, service: ServiceType) {
        self.name = name
        self.descriptionText = descriptionText
        self.imageUrl = imageUrl
        self.service = service
    }

    init?(json: JSON) {
        guard let serviceRawValue = json["service"].string,
              let featureService = ServiceType(rawValue:  serviceRawValue) else {
            return nil
        }
        service = featureService
        name = json["name"].string ?? ""
        descriptionText = json["description"].string ?? ""
        imageUrl = json["image_url"].string ?? ""
    }
}
