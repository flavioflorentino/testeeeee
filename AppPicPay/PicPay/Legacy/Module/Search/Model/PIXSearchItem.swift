import Foundation
import SwiftyJSON

final class PixSearchItem: NSObject {
    enum PixServiceType: String {
        case keyManager
        case hub
    }
    
    let name: String
    let descriptionText: String
    let imageUrl: String
    let service: PixServiceType

    init(name: String, descriptionText: String, imageUrl: String, service: PixServiceType) {
        self.name = name
        self.descriptionText = descriptionText
        self.imageUrl = imageUrl
        self.service = service
    }
    
    init?(json: JSON) {
        guard
            let pixServiceRawValue = json["service"].string,
            let pixService = PixServiceType(rawValue:  pixServiceRawValue)
            else {
                return nil
        }
        service = pixService
        name = json["name"].string ?? ""
        descriptionText = json["description"].string ?? ""
        imageUrl = json["image_url"].string ?? ""
    }
}
