import SwiftyJSON

final class SearchRecentCacheRow: Equatable {
    var section: String = ""
    var tab: SearchTab
    var row: LegacySearchResultRow
    
    init(row: LegacySearchResultRow, section: String, tab: SearchTab) {
        self.row = row
        self.section = section
        self.tab = tab
    }
    
    init?(json: JSON) {
        guard
            let row = LegacySearchResultRow(json: json["row"]),
            let tab = SearchTab(json: json["tab"])
            else {
                return nil
        }
        self.tab = tab
        self.row = row
        self.section = json["section"].string ?? ""
    }
    
    func toJson() -> JSON {
        var json = JSON()
        let rowJson = row.json ?? row.toJson()
        json["row"] = rowJson ?? [:]
        json["section"] = JSON(section)
        json["tab"] = tab.toJson()
        return json
    }
    
    static func == (lhs: SearchRecentCacheRow, rhs: SearchRecentCacheRow) -> Bool {
        return lhs.row.type == rhs.row.type && lhs.row.basicData.id == rhs.row.basicData.id
    }
}
