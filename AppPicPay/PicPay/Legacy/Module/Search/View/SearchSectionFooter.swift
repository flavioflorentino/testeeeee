import UI
import UIKit

final class SearchSectionFooter: UIView {
    @IBOutlet weak var contentView: UIView!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commomInit()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commomInit()
    }
    
    private func commomInit() {
        contentView.backgroundColor = Palette.ppColorGrayscale200.color
    }
}
