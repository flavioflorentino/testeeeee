import UI
import Core
import FeatureFlag

protocol SearchListCellDelegate: AnyObject {
    func openSearchBottomSheet(
        sender: UITapGestureRecognizer,
        withData data: SearchResultBasics,
        cellObject object: NSObject,
        andProfileImageDelegate delegate: UIPPProfileImageDelegate?
    )
}

final class SearchListCell: CardStyleTableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImage: UIPPProfileImage!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var descriptionsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var studentAccountView: UIView!
    @IBOutlet weak var studentAccountLabel: UILabel!
    @IBOutlet weak var nameLabelTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionLabelTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomSheetOptionsImageView: UIImageView!
    
    var userHasStudentAccountActive: Bool {
        return KVStore().boolFor(.hasStudentAccountActive)
    }
    
    var didTapBottomSheetButton: (() -> Void)?
    
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.textColor = Palette.ppColorGrayscale500.color
        }
    }

    @IBOutlet weak var typeDetailLabel: UILabel! {
        didSet {
            typeDetailLabel.textColor = Palette.ppColorGrayscale400.color
        }
    }

    let ShowIconsType: [SearchResultType: String] = [
        SearchResultType.store:         "stablishment",
        SearchResultType.digitalGood:   "digitalGoods",
        SearchResultType.subscription:  "subscriptions",
        SearchResultType.person:        "pro"
    ]
    
    private enum Layout {
        struct Margin {
            static let trailingSpaceToStudentAccountView: CGFloat = 5
        }
        
        struct Height {
            static let descriptionsShort: CGFloat = 35.0
            static let descriptionsMedium: CGFloat = 50.0
            static let descriptionsHigh: CGFloat = 70.0
        }
        
        struct Alpha {
            static let disabled: CGFloat = 0.8
            static let enabled: CGFloat = 1.0
        }
    }
    
    weak var bottomSheetDelegate: SearchListCellDelegate?
    private var bottomSheetData: (data: SearchResultBasics, object: NSObject, delegate: UIPPProfileImageDelegate?)?
    
    func setup(
        data: SearchResultBasics,
        fullObject: NSObject,
        profileImageDelegate: UIPPProfileImageDelegate? = nil,
        origin: String?,
        shouldDisplayIcon: Bool,
        isSearch: Bool = true
    ){

        super.setup(containerView: bgView, avoidShadow: true)
        
        descriptionsHeightConstraint.constant = Layout.Height.descriptionsMedium
        
        let cellObject = fullObject
        
        self.style = .flat
        
        let title = data.title ?? ""
        let fullString = NSMutableAttributedString(string: title + " ")
        self.nameLabel.attributedText = fullString
        self.nameLabel.numberOfLines = 1
        
        self.descriptionLabel.text = data.descriptionText
        
        self.nameLabel.preferredMaxLayoutWidth = self.nameLabel.frame.size.width
        self.descriptionLabel.preferredMaxLayoutWidth = self.descriptionLabel.frame.size.width
        self.typeDetailLabel.preferredMaxLayoutWidth = self.typeDetailLabel.frame.size.width
        
        if let delegate = profileImageDelegate {
            self.profileImage.delegate = delegate
        }else{
            self.profileImage.removeTapGesture()
        }
        
        self.profileImage.setAny(cellObject)
        self.profileImage.origin = origin
        
        self.profileImage.shouldHideBadgePro = false
        var hasTypeDetail = false
        if data.type?.description == "" || !shouldDisplayIcon{
            self.descriptionLabel.numberOfLines = 2
            
            if shouldDisplayIcon {
                // Put a icon in front of Title
                if let type = data.type, let typeIcon = ShowIconsType[type] {
                    let image1Attachment = NSTextAttachment()
                    image1Attachment.image = UIImage(named: typeIcon)
                    image1Attachment.bounds = CGRect(x:0, y:-3, width:(image1Attachment.image?.size.width)!, height:(image1Attachment.image?.size.height)!)
                    let image1String = NSAttributedString(attachment: image1Attachment)
                    if (type == SearchResultType.person && data.isPro == true) { // In case of Person type, check if he is a PRO
                        self.profileImage.shouldHideBadgePro = true
                        fullString.append(image1String)
                    } else if(type != SearchResultType.person) {
                        fullString.append(image1String)
                    }
                    self.nameLabel.attributedText = fullString
                }
            }
            
            if let nameDetail = data.titleDetail {

                let fullText = NSMutableAttributedString()
                fullText.append(self.nameLabel.attributedText!)
                fullText.append(NSMutableAttributedString(string: " · " + nameDetail))

                let detailStyle = [
                    NSAttributedString.Key.foregroundColor: UIColor(red: 102.0/255, green: 102.0/255, blue: 102.0/255, alpha: 1),
                    NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13, weight: .regular)
                ]
                fullText.addAttributes(detailStyle, range: NSRange(location: title.count, length: fullText.length - title.count))

                self.nameLabel.attributedText = fullText
            }
            
            self.typeDetailLabel.attributedText = NSMutableAttributedString(string: "")
        }else{
            self.descriptionLabel.numberOfLines = 1
            
            // Put a icon in front of Title
            if let type = data.type, let typeIcon = ShowIconsType[type] {
                self.profileImage.shouldHideBadgePro = true
                
                let typeDetailString = NSMutableAttributedString(string: "")
                
                // Type Label
                if let typeDescription = data.type?.description {
                    typeDetailString.append(NSMutableAttributedString(string: typeDescription + " "))
                }
                
                // Type Image
                let imageAttach = NSTextAttachment()
                imageAttach.image = UIImage(named: typeIcon)
                imageAttach.bounds = CGRect(x: 0, y: -2, width: 12, height: 12)
                let typeImageString = NSAttributedString(attachment: imageAttach)
                typeDetailString.append(typeImageString)
                
                // Details
                if let detail = data.titleDetail {
                    typeDetailString.append(NSMutableAttributedString(string: " · " + detail))
                }
                
                self.typeDetailLabel.attributedText = typeDetailString
                hasTypeDetail = true
            }
        }
        
        self.nameLabel.sizeToFit()
        self.descriptionLabel.sizeToFit()
        
        let isTwoLineName = nameLabel.intrinsicContentSize.height > 30
        let isTwoLineDescription = descriptionLabel.intrinsicContentSize.height > 20
        
        if hasTypeDetail || isTwoLineName || isTwoLineDescription {
            descriptionsHeightConstraint.constant = Layout.Height.descriptionsMedium
        }else{
            descriptionsHeightConstraint.constant = Layout.Height.descriptionsShort
        }
        
        if userHasStudentAccountActive, let digitalGood = cellObject as? DGItem {
            configureForStudentAccount(with: digitalGood)
        }
        
        if FeatureManager.isActive(.featureShowOptionsBottomSheet) {
            bottomSheetOptionsImageView.isHidden = false
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(openBottomSheet(_:)))
            bottomSheetOptionsImageView.addGestureRecognizer(tapGesture)
            bottomSheetOptionsImageView.isUserInteractionEnabled = true
            bottomSheetData = (
                data: data,
                object: fullObject,
                delegate: profileImageDelegate
            )
        } else {
            bottomSheetOptionsImageView.isHidden = true
            bottomSheetOptionsImageView.gestureRecognizers?.removeAll()
        }
    }
    
    func disableCell(text: String) {
        typeDetailLabel.text = text
        typeDetailLabel.textColor = Palette.ppColorNegative300.color
        bgView.alpha = Layout.Alpha.disabled
        setBackground(color: Palette.ppColorGrayscale100.color)
        descriptionsHeightConstraint.constant = Layout.Height.descriptionsMedium
        layoutIfNeeded()
    }
    
    private func configureForStudentAccount(with digitalGood: DGItem) {
        guard FeatureManager.isActive(.storeWithUniversityAccountLabel), digitalGood.isStudentAccount else {
            return
        }
        
        studentAccountLabel.textColor = Palette.black.color
        studentAccountView.isHidden = false
        studentAccountView.layer.cornerRadius = studentAccountView.frame.height/2
        studentAccountView.layer.masksToBounds = true
        
        let constraintTrailingSpace: CGFloat = studentAccountView.frame.width + Layout.Margin.trailingSpaceToStudentAccountView
        nameLabelTrailingConstraint.constant = constraintTrailingSpace
        descriptionLabelTrailingConstraint.constant = constraintTrailingSpace
        descriptionsHeightConstraint.constant = Layout.Height.descriptionsHigh
        
        let detailStyle = [
            NSAttributedString.Key.foregroundColor: Palette.ppColorBranding300.color
        ]
        let string = FeatureManager.text(.universityAccountBenefitDescription)
        let typeDetailString = NSAttributedString(
            string: string,
            attributes: detailStyle
        )
        typeDetailLabel.attributedText = typeDetailString
    }
    
    override func prepareForReuse() {
        studentAccountView.isHidden = true
        nameLabelTrailingConstraint.constant = 0
        descriptionLabelTrailingConstraint.constant = 0
        bgView.alpha = Layout.Alpha.enabled
        setBackground(color: .clear)
        typeDetailLabel.textColor = Palette.ppColorGrayscale400.color
        profileImage.imageView?.image = nil
    }
    
    @objc
    public func openBottomSheet(_ sender: UITapGestureRecognizer) {
        guard
            let bsdata = bottomSheetData
            else {
                return
        }
        
        bottomSheetDelegate?.openSearchBottomSheet(
            sender: sender,
            withData: bsdata.data,
            cellObject: bsdata.object,
            andProfileImageDelegate: bsdata.delegate
        )
        
        didTapBottomSheetButton?()
    }
}
