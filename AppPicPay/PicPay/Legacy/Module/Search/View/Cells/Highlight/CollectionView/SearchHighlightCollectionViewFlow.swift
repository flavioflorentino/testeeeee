//
//  HighlightCollectionViewFlow.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 15/01/18.
//

import Foundation
import UIKit

final class SearchHighlightCollectionViewFlow: UICollectionViewFlowLayout {
    
    var mostRecentOffset : CGPoint = CGPoint()
    
    override init() {
        super.init()
        self.scrollDirection = .horizontal
        
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        if velocity.x == 0 {
            return mostRecentOffset
        }
        
        guard let cv = self.collectionView else {
            mostRecentOffset = super.targetContentOffset(forProposedContentOffset: proposedContentOffset)
            return mostRecentOffset
        }

            
        let cvBounds = cv.bounds
        let halfWidth = cvBounds.size.width * 0.5;
        
        
        guard let attributesForVisibleCells = self.layoutAttributesForElements(in: cvBounds) else {
            mostRecentOffset = super.targetContentOffset(forProposedContentOffset: proposedContentOffset)
            return mostRecentOffset
        }
        
        var candidateAttributes : UICollectionViewLayoutAttributes?
        for attributes in attributesForVisibleCells {
            
            // == Skip comparison with non-cell items (headers and footers) == //
            if attributes.representedElementCategory != .cell {
                continue
            }
            
            if (attributes.center.x == 0) || (attributes.center.x > (cv.contentOffset.x + halfWidth) && velocity.x < 0) {
                continue
            }
            candidateAttributes = attributes
        }
        
        // Beautification step , I don't know why it works!
        if(proposedContentOffset.x == -(cv.contentInset.left)) {
            return proposedContentOffset
        }
    
        guard let _ = candidateAttributes else {
            return mostRecentOffset
        }
        mostRecentOffset = CGPoint(x: floor(candidateAttributes!.center.x - halfWidth), y: proposedContentOffset.y)
        return mostRecentOffset
    }
}
