//
//  HighlightCollectionView.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 15/01/18.
//

import Foundation
import UI
import UIKit

final class SearchHighlightCollectionView: UICollectionView {
    fileprivate let flow = SearchHighlightCollectionViewFlow()
    fileprivate let spacing: CGFloat = 8

    init(itemSize: CGSize, data: LegacySearchResultSection) {
        super.init(frame: .zero, collectionViewLayout: flow)
        
        // Preparation
        superview?.setNeedsLayout()
        superview?.layoutIfNeeded()
        
        // UI
        backgroundColor = Colors.backgroundPrimary.color
        showsHorizontalScrollIndicator = false
        
        // Feels like paginating
        decelerationRate = .fast
        
        // Fixes the first and last cell
        contentInset = UIEdgeInsets.init(top: 0, left: spacing * 2, bottom: 0, right: spacing * 2)
        
        // Item size
        guard let layout = self.collectionViewLayout as? SearchHighlightCollectionViewFlow else {
            return
        }
        
        layout.itemSize = itemSize
        layout.minimumInteritemSpacing = spacing
        layout.minimumLineSpacing = spacing
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

