import Foundation
import SDWebImage
import UI

final class SearchHightlightCollectionCell: UICollectionViewCell {
    private let banner = UIImageView()
    private var loading: UILoadView?
    private var infoButton: UIButton!
    
    private var didTapInfoButton: (()-> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        // banner image
        addSubview(banner)
        banner.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        banner.backgroundColor = Palette.ppColorGrayscale200.color
        banner.contentMode = .scaleAspectFill
        banner.clipsToBounds = true
        banner.layer.cornerRadius = 7.0
        layer.cornerRadius = 7.0
        clipsToBounds = true
        backgroundColor = Palette.ppColorGrayscale200.color
        
        // info button
        infoButton = UIButton(type: .custom)
        infoButton.setImage(#imageLiteral(resourceName: "ico_info_circ_white"), for: .normal)
        infoButton.contentMode = .center
        addSubview(infoButton)
        infoButton.addTarget(self, action: #selector(bannerInfoDidTap), for: .touchUpInside)
        infoButton.snp.makeConstraints { make in
            make.right.equalTo(0)
            make.top.equalTo(-2)
            make.height.equalTo(44)
            make.width.equalTo(44)
        }
    }
    
    func setup(banner data: SearchResultBanner, didTapInfoButton: @escaping (() -> Void)) {
        self.didTapInfoButton = didTapInfoButton
        
        infoButton.isHidden = data.info == nil
        banner.image = nil
        
        if loading == nil {
            loading = UILoadView(superview: self, position: .center)
            loading?.startLoading()
            loading?.backgroundColor = Palette.ppColorGrayscale200.color
        } else {
            loading?.startLoading()
        }
        
        if let urlString = data.largeImageUrl {
            banner.sd_setImage(with: URL(string: urlString)) { [weak self] (_, _, _, _) in
                self?.loading?.animatedRemoveFromSuperView()
            }
        }
    }
    
    @objc func bannerInfoDidTap(){
        didTapInfoButton?()
    }
    
    func stop() {
        banner.cancelRequest()
    }
}
