import AnalyticsModule
import Foundation
import UI
import UIKit

final class SearchHightlightTableViewCell: UITableViewCell {
    private var collectionView: SearchHighlightCollectionView?
    private var data: LegacySearchResultSection?
    private var didSelect:((LegacySearchResultSection, LegacySearchResultRow) -> ())?
    
    var didTapInfoButton: ((LegacySearchResultSection, LegacySearchResultRow) -> Void)?
    
    func setup(data: LegacySearchResultSection, height: CGFloat, selection: @escaping (LegacySearchResultSection, LegacySearchResultRow) -> ()) {
        self.superview?.setNeedsLayout()
        self.superview?.layoutIfNeeded()
        self.data = data
        self.didSelect = selection
        backgroundColor = Colors.backgroundPrimary.color
        contentView.backgroundColor = Colors.backgroundPrimary.color
        
        let itemSize = CGSize(width: height * 2.0, height: height)
        
        if collectionView == nil || collectionView?.superview == nil {
            let collectionView = SearchHighlightCollectionView(itemSize: itemSize, data: data)
            collectionView.translatesAutoresizingMaskIntoConstraints = false
            self.collectionView = collectionView

            contentView.addSubview(collectionView)
            setupConstraints(height: height)
        } else {
            collectionView?.reloadData()
        }
        
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.register(SearchHightlightCollectionCell.self, forCellWithReuseIdentifier: "highlightCell")
    }
    
    private func setupConstraints(height: CGFloat) {
        guard let collectionView = self.collectionView else {
            return
        }
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: contentView.topAnchor),
            collectionView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            collectionView.heightAnchor.constraint(equalToConstant: height)
        ])
    }
}


extension SearchHightlightTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let data = data else { return 0 }
        return data.rows.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard
            let data = data,
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "highlightCell", for: indexPath) as? SearchHightlightCollectionCell
        else { return UICollectionViewCell() }
        
        if let banner = data.rows[indexPath.row].basicData as? SearchResultBanner {
            cell.setup(banner: banner) { [weak self] in
                self?.didTapInfoButton?(data, data.rows[indexPath.row])
            }
        }
        
        return cell
    }
}

extension SearchHightlightTableViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let data = data, let row = data.rows[indexPath.row].fullData as? LegacySearchResultRow else {
            return
        }
        if let banner = data.rows[indexPath.row].basicData as? SearchResultBanner {
            Analytics.shared.log(StoreEvent.didTapBanner(name: banner.name ?? banner.id ?? "banner nao identificado"))
        }
        didSelect?(data, row)
    }
}
