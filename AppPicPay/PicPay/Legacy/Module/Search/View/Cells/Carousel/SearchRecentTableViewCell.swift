import Foundation
import FeatureFlag
import UI
import UIKit

final class SearchRecentTableViewCell: CardStyleTableViewCell {
    private let recentCollectionView = RecentsCollectionView()
    fileprivate var didSelect: ((LegacySearchResultSection, LegacySearchResultRow, IndexPath) -> ())?
    fileprivate var section: LegacySearchResultSection?
    
    func setup(data: LegacySearchResultSection, profileImageDelegate: UIPPProfileImageDelegate? = nil, selection: @escaping (LegacySearchResultSection, LegacySearchResultRow, IndexPath) -> ()) {
        let isFlat = !FeatureManager.isActive(.searchCardStyle)
        super.setup(containerView: recentCollectionView, avoidShadow: isFlat)
        style = isFlat ? .flat : .extendedHorizontal
        animateTouches = false
        didSelect = selection
        section = data
        
        backgroundColor = Colors.backgroundPrimary.color
        contentView.backgroundColor = Colors.backgroundPrimary.color
        
        recentCollectionView.setup(results: data, imageDelegate: profileImageDelegate)
        recentCollectionView.recentsDelegate = self
    }
}

extension SearchRecentTableViewCell: RecentsCollectionViewDelegate {
    func didSelectRow(section: LegacySearchResultSection, row: LegacySearchResultRow) {
        
    }
    
    func didSelectCell(at indexPath: IndexPath) {
        guard let section = section else {
            return
        }
        didSelect?(section, section.rows[indexPath.row], indexPath)
    }
}
