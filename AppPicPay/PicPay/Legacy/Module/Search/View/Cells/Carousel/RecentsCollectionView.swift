import Foundation
import UI
import UIKit

protocol RecentsCollectionViewDelegate: NSObjectProtocol {
    func didSelectCell(at indexPath: IndexPath)
    func didSelectRow(section: LegacySearchResultSection, row: LegacySearchResultRow)
}

final class RecentsCollectionView: CarouselCollectionView {
    // PPContact or PPStore
    var result: LegacySearchResultSection?
    weak var recentsDelegate: RecentsCollectionViewDelegate? = nil
    
    func setup(results: LegacySearchResultSection, imageDelegate: UIPPProfileImageDelegate? = nil) {
        register(cellType: RecentsCollectionViewCell.self)
        result = results
        reloadData()
    }
    
    override func commmonInit() {
        super.commmonInit()
        delegate = self
        dataSource = self
        backgroundColor = .clear
        guard let layout = self.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }

        layout.headerReferenceSize = .zero
        layout.footerReferenceSize = .zero
        layout.itemSize = RecentsCollectionViewCell.size
        layout.minimumLineSpacing = Spacing.base00
        layout.minimumInteritemSpacing = .zero
        layout.sectionInset = UIEdgeInsets(
            top: Spacing.base02,
            left: Spacing.base02,
            bottom: Spacing.base02,
            right: Spacing.base02
        )
    }
}

// MARK: Delegate
extension RecentsCollectionView: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        recentsDelegate?.didSelectCell(at: indexPath)
        guard let section = result, let row = result?.rows[indexPath.row] else {
            return
        }
        recentsDelegate?.didSelectRow(section: section, row: row)
    }
    
    // Invalidate the cell (stop downloading the image if it hasn't finished yet)
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let recentCell = cell as? RecentsCollectionViewCell else {
            return
        }
        recentCell.invalidate()
    }
}

// MARK: Data Source
extension RecentsCollectionView: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return result?.rows.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(for: indexPath) as RecentsCollectionViewCell

        guard let result = result else { return cell }

        let row = result.rows[indexPath.row]
        cell.configure(common: row.basicData, fullObject: row.fullData ?? NSObject(), style: result.style)

        return cell
    }
}
