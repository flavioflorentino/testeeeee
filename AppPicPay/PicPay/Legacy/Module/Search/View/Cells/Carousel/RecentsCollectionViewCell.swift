import FeatureFlag
import Foundation
import UI
import UIKit

// MARK: - Layout
private extension RecentsCollectionViewCell.Layout {
    static let size = CGSize(width: 80.0, height: 92.0)

    enum Image {
        static let size = CGSize(width: 54.0, height: 54.0)
        static let margin: CGFloat = 12.0
    }

    enum HightlightBorder {
        static let borderWidth: CGFloat = 2.0
    }
}

final class RecentsCollectionViewCell: UICollectionViewCell {
    // MARK: - Nested types
    fileprivate enum Layout { }

    // MARK: - Properties
    private lazy var highlightBorderView: CircleBorderAnimatedView = {
        let view = CircleBorderAnimatedView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        view.colors = [Palette.animatedBorderPurple.color, Palette.animatedBorderPink.color]

        return view
    }()

    private lazy var imageView: UIPPProfileImage = {
        let imageView = UIPPProfileImage()
        imageView.backgroundColor = .clear

        imageView.imageView?.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
            .with(\.backgroundColor, .clear)
            .with(\.border, Border.Style.medium(color: .grayscale100()))

        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.textColor, .grayscale850())
            .with(\.textAlignment, .center)
            .with(\.lineBreakMode, .byTruncatingTail)
            .with(\.numberOfLines, 2)

        return label
    }()

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()

        highlightBorderView.stopAnimation()
        imageView.imageView?.image = nil
    }
    
    func configure(common: SearchResultBasics, fullObject: NSObject, style: LegacySearchResultSection.Style = .normal) {
        imageView.setAny(fullObject)

        titleLabel.numberOfLines = numberOfLines(for: common.type)
        titleLabel.text = common.title

        guard let news = fullObject as? News, !news.viewed else {
            highlightBorderView.stopAnimation()
            return
        }

        highlightBorderView.startAnimation()
    }

    func numberOfLines(for type: SearchResultType?) -> Int {
        guard let type = type, type == .person || type == .subscription else { return 2 }
        return 1
    }
    
    func invalidate(){
        // Stop the download if we want to invalidate the cell. This prevents weird behaviour when scrolling the
        // collection view on a slow connection
        imageView.imageView?.cancelRequest()
    }
}

// MARK: - ViewConfiguration
extension RecentsCollectionViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubviews(highlightBorderView, imageView, titleLabel)
    }

    func setupConstraints() {
        highlightBorderView.snp.makeConstraints {
            $0.edges.equalTo(imageView).inset(-Layout.HightlightBorder.borderWidth)
            $0.center.equalTo(imageView)
        }

        imageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base00)
            $0.leading.greaterThanOrEqualToSuperview().offset(Layout.Image.margin)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Layout.Image.margin)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Image.size)
        }

        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base00)
            $0.leading.greaterThanOrEqualToSuperview().offset(Spacing.base01)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base01)
            $0.bottom.centerX.equalToSuperview()
        }
    }

    func configureViews() {
        viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        contentView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        imageView.isUserInteractionEnabled = false
    }
}

// MARK: - Static methods
extension RecentsCollectionViewCell {
    static var size: CGSize { Layout.size }
}

// MARK: -
private final class CircleBorderAnimatedView: UIView {
    // MARK: - Properties
    private weak var animationLayer: CALayer?
    private var shouldAnimate = false

    var colors: [UIColor] = []

    // MARK: - Methods
    func stopAnimation() {
        shouldAnimate = false
        animationLayer?.removeFromSuperlayer()
        animationLayer = nil
    }

    func startAnimation() {
        shouldAnimate = true
        setNeedsLayout()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        guard shouldAnimate && animationLayer == nil else { return }

        layer.cornerRadius = frame.height / 2.0
        animationLayer = setCustomCircleBorderAnimation(withColors: colors.map(\.cgColor))
    }
}
