import Foundation
import UI

enum EmptySearchViewStyle {
    case error
    case noResult
}
final class EmptySearchView: NibView {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var boldSubtitle: UILabel!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentViewBottomConstraint: NSLayoutConstraint?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commomInit()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commomInit()
    }
    
    private func commomInit() {
        backgroundColor = Colors.backgroundPrimary.color
        contentView.backgroundColor = Colors.backgroundPrimary.color
        
        title.textColor = Palette.ppColorGrayscale400.color
        subtitle.textColor = Palette.ppColorGrayscale500.color
        boldSubtitle.textColor = Palette.ppColorGrayscale400.color
    }
    
    func setup(style: EmptySearchViewStyle) {
        switch style {
        case .error:
            imageView.image = #imageLiteral(resourceName: "ilu_search_error_con")
            title.text = DefaultLocalizable.errorConnectionViewTitle.text
            subtitle.text = SearchLegacyLocalizable.checkYourConnection.text
            boldSubtitle.text = DefaultLocalizable.tapHereTryAgain.text
        case .noResult:
            imageView.image = #imageLiteral(resourceName: "ilu_search_no_result")
            title.text = SearchLegacyLocalizable.noResults.text
            subtitle.text = SearchLegacyLocalizable.lookedEverywhere.text
            boldSubtitle.text = ""
        }
        
        if let imageWidthValue = imageView.image?.size.width {
            imageWidth.constant = imageWidthValue
        }
    }
}
