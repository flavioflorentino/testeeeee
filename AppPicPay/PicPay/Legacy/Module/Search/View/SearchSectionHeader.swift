import Foundation
import UI

final class SearchSectionHeader: UIView {
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = Palette.ppColorGrayscale500.color
        }
    }
    @IBOutlet weak var seeAllLabel: UILabel! {
        didSet {
            seeAllLabel.textColor = Palette.ppColorBranding300.color
        }
    }
    @IBOutlet weak var rightArrow: UIImageView! {
        didSet {
            rightArrow.tintColor = Palette.ppColorBranding300.color
        }
    }
    @IBOutlet weak var left: NSLayoutConstraint!
    
    var showAll = false {
        didSet {
            seeAllLabel.isHidden = !showAll
            rightArrow.isHidden = !showAll
        }
    }
    
    var seeAll: (() -> ())?
    
    @IBAction private func didTapSeeAll(_ sender: Any) {
        guard showAll else {
            return
        }
        seeAll?()
    }
}
