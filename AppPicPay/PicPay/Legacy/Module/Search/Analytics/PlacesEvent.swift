import AnalyticsModule

enum PlacesEvent: AnalyticsKeyProtocol {
    case didShowFullMap
    case didTapPinOnMap(type: StoreType, storeName: String)
    case didTapPayButtonOnStoreProfile(type: StoreType, storeName: String)
    
    private var name: String {
        switch self {
        case .didShowFullMap:
            return "Locais - Entrou no mapa"
        case .didTapPinOnMap:
            return "Locais - Tocou no pin"
        case .didTapPayButtonOnStoreProfile:
            return "Locais - Tocou em pagar no perfil"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .didTapPinOnMap(let type, let storeName):
            return ["Tipo": type.rawValue, "Lojista": storeName]
        case .didTapPayButtonOnStoreProfile(let type, let storeName):
            return ["Tipo": type.rawValue, "Lojista": storeName]
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        return [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

extension PlacesEvent {
    enum StoreType: String {
        case biz = "Biz"
        case cielo = "Cielo"
    }
}
