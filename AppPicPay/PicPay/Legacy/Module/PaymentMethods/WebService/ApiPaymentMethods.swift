import Core
import FeatureFlag
import UIKit
import SwiftyJSON

protocol AddNewCreditCardProtocol {
    var newEnvironmentEnabled: Bool { get }
    func insertCreditCard(addressCard: AddressCard,
                          insertCard: InsertCard,
                          completion: @escaping (PicPayResult<CardBank>) -> Void)
    func validateCreditCard(validateCard: ValidateCard, completion: @escaping (PicPayResult<ValidateCreditCard>) -> Void)
}

protocol WalletApiProtocol {
    func cardsList(onCacheLoaded: @escaping ((PaymentMethodsListResponse) -> Void),
                   completion: @escaping ((PicPayResult<PaymentMethodsListResponse>) -> Void))
    func cardDelete(card: String, _ completion: @escaping ((PicPayResult<BaseCodableEmptyResponse>) -> Void))
    func cardEdit(card: String,
                  alias: String?,
                  address: ConsumerAddressItem?,
                  _ completion: @escaping ((PicPayResult<BaseCodableEmptyResponse>) -> Void))
}

protocol VerifyCreditCardProtocol {
    var pciIsActiveVerifyCard: Bool { get }
    func saveCvv(cardId: String, cvv: String?)
    func cvvCard(id: String) -> String?
    func verifyStatus(id: String, _ completion: @escaping (PicPayResult<VerificationStatusResponse>) -> Void)
    func verify(id: String, value: Double, _ completion: @escaping (PicPayResult<VerificationStatusResponse>) -> Void)
    func verifyStatusStart(id: String, _ completion: @escaping (PicPayResult<VerificationStatusResponse>) -> Void)
    func verifyStatusCancel(id: String, _ completion: @escaping (PicPayResult<VerificationStatusResponse>) -> Void)
}

final class ApiPaymentMethods: BaseApi, AddNewCreditCardProtocol, WalletApiProtocol, VerifyCreditCardProtocol {
    var pciIsActiveVerifyCard: Bool {
        return FeatureManager.isActive(.pciCardVerification)
    }
    
    var newEnvironmentEnabled: Bool {
        FeatureManager.isActive(.pciCardInsert)
    }
    
    typealias Dependencies = HasFeatureManager & HasKeychainManager
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func saveCvv(cardId: String, cvv: String?) {
        dependencies.keychain.set(key: KeychainKeyPF.cvv(cardId), value: cvv)
    }
    
    func cvvCard(id: String) -> String? {
        dependencies.keychain.getData(key: KeychainKeyPF.cvv(id))
    }
    
    func cardsList(onCacheLoaded: @escaping ((PaymentMethodsListResponse) -> Void),
                   completion: @escaping ((PicPayResult<PaymentMethodsListResponse>) -> Void) ) {
        guard User.isAuthenticated else {
            return
        }
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            guard let endpoint = WebServiceInterface.apiEndpoint(kWsUrlGetPaymentMethods) else {
                return
            }
            requestManager
                .apiRequestWithCacheCodable(onCacheLoaded: onCacheLoaded, endpoint: endpoint, method: .get)
                .responseApiCodable(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func cardDelete(card: String, _ completion: @escaping ((PicPayResult<BaseCodableEmptyResponse>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let params = ["credit_card_id": card]
            requestManager
                .apiRequest(endpoint: WebServiceInterface.apiEndpoint(kWsUrlDeleteCreditCard), method: .post, parameters: params)
                .responseApiCodable(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func cardEdit(card: String,
                  alias: String?,
                  address: ConsumerAddressItem?,
                  _ completion: @escaping ((PicPayResult<BaseCodableEmptyResponse>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            var params: [String: Any] = ["credit_card_id": card]
            if let _alias = alias {
                params["alias"] = _alias
            }
            if let addressDict = address.toDictionary() {
                params["address"] = addressDict
            }
            requestManager
                .apiRequest(endpoint: WebServiceInterface.apiEndpoint(kWsUrlEditCreditCard), method: .post, parameters: params)
                .responseApiCodable(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func verifyStatus(id: String, _ completion: @escaping (PicPayResult<VerificationStatusResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let params: [String: Any] = ["id": id]
            requestManager
                .apiRequest(endpoint: WebServiceInterface.apiEndpoint(kWsUrlVerifyCardStatus), method: .post, parameters: params)
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func verify(id: String, value: Double, _ completion: @escaping (PicPayResult<VerificationStatusResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            var params: [String: Any] = [:]
            params["id"] = id
            params["value"] = value
            requestManager
                .apiRequest(endpoint: WebServiceInterface.apiEndpoint(kWsUrlVerifyCard), method: .post, parameters: params)
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func verifyStatusStart(id: String, _ completion: @escaping (PicPayResult<VerificationStatusResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            var params: [String: Any] = [:]
            params["id"] = id
            requestManager
                .apiRequest(endpoint: WebServiceInterface.apiEndpoint(kWsUrlVerifyCardStatusStart),
                            method: .post,
                            parameters: params)
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func verifyStatusCancel(id: String, _ completion: @escaping (PicPayResult<VerificationStatusResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            var params: [String: Any] = [:]
            params["id"] = id
            requestManager
                .apiRequest(endpoint: WebServiceInterface.apiEndpoint(KWsUrlVerifyCardStatusCancel),
                            method: .post,
                            parameters: params)
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func insertCreditCard(addressCard: AddressCard,
                          insertCard: InsertCard,
                          completion: @escaping (PicPayResult<CardBank>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let paramsAddress = JSONEncoder.dictionary(addressCard, strategy: .useDefaultKeys)
            let paramsCard = JSONEncoder.dictionary(insertCard, strategy: .useDefaultKeys)
            let params = paramsCard?.merging(paramsAddress ?? [:]) { (current, _) in current }
            
            requestManager
                .apiRequest(endpoint: WebServiceInterface.apiEndpoint(kWsUrlInsertCreditCard), method: .post, parameters: params)
                .responseApiCodable(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func validateCreditCard(validateCard: ValidateCard, completion: @escaping (PicPayResult<ValidateCreditCard>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let params = JSONEncoder.dictionary(validateCard, strategy: .useDefaultKeys)
            requestManager
                .apiRequest(endpoint: WebServiceInterface.apiEndpoint(kWsUrlValidateCreditCard),
                            method: .post,
                            parameters: params)
                .responseApiCodable(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
}
