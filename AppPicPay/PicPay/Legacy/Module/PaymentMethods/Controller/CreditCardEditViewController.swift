//
//  CreditCardEditViewController.swift
//  PicPay
//
//  Created by PicPay Eduardo on 8/9/18.
//

import UI
import UIKit

protocol CreditCardEditDelegate: AnyObject {
    func deleteCreditCard(_ card: CardBank)
    func onChangeAddressOfCard()
}

final class CreditCardEditViewController: PPBaseViewController {

	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var headerView: UIView!
	@IBOutlet weak var backButton: UIButton!
	@IBOutlet weak var cardContainerView: UIView!

    var viewModel: CreditCardEditViewModel!
	weak var delegate: CreditCardEditDelegate?
	
	lazy var frontCardView: CreditCardFrontView = {
		let view = CreditCardFrontView()
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	}()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    lazy var loadingView: UIView = {
        let view = UIView(frame: self.view.bounds)
        let activityView = UIActivityIndicatorView(style: .gray)
        activityView.translatesAutoresizingMaskIntoConstraints = false
        activityView.startAnimating()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.addSubview(activityView)
        activityView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        return view
    }()
    
    override func navigationBarIsTransparent() -> Bool {
        return true
    }

    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        
		tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension
		tableView.sectionFooterHeight = 0
		tableView.tableFooterView = UIView()
        tableView.backgroundColor = Palette.ppColorGrayscale100.color
		
		/// Fill the bounce area and header with background color
		var frame = self.tableView.bounds
		frame.origin.y = -frame.size.height
		frame.size.width = UIScreen.main.bounds.width
        
		let bgBouceView = UIView(frame: frame)
		bgBouceView.backgroundColor = Palette.ppColorBranding300.color
        tableView.addSubview(bgBouceView)
        
		/// configure a height proportional to the size of the card
		let cardWidth: CGFloat = frame.width - 2 * 44
		let cardHeight: CGFloat = cardWidth * 0.6208
		let marginBotton: CGFloat = 28
		let marginTop: CGFloat = 18
        
		headerView.frame.size.height = cardHeight + marginBotton + marginTop
        headerView.backgroundColor = Palette.ppColorBranding300.color
        
        title = ""
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		setupCardView()
		loadCards()
	}
	
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editAliasIdentifier" {
            guard let controller = segue.destination as? EditCardAliasViewController else {
            return
        }
            controller.viewModel = EditCardAliasViewModel(with: viewModel.card)
        }
        if segue.identifier == "verificationForEditCardIdentifier" {
            guard let controller = segue.destination as? CardVerificationViewController else {
            return
        }
            controller.viewModel = CardVerificationViewModel(with: viewModel.card)
        }
    }
	
	@IBAction private func backButtonAction(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
}

// MARK: - TableViewDataSource

extension CreditCardEditViewController: UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return viewModel.numberOfSections()
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return viewModel.numberOfRows(in: section)
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowType = viewModel.getRowType(in: indexPath)
        if rowType == .cardVerification {
            if viewModel.card.verifyStatus == .waitingFillValue || viewModel.card.verifyStatus == .verifiable {
                return cellForCardInformationAlert(tableView, rowType: rowType, at: indexPath)
            } else {
                return cellForCardInformation(tableView, rowType: rowType, at: indexPath)
            }
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CardSettingTableViewCell.identifier, for: indexPath) as? CardSettingTableViewCell else { return CardSettingTableViewCell()}
            cell.configure(with: viewModel.card.alias, addressString: viewModel.cardAddressDescription(), and: rowType)
            return cell
        }
	}
}

// MARK: - TableViewDelegate

extension CreditCardEditViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
		let rowType = viewModel.getRowType(in: indexPath)
		switch rowType {
		case .alias:
            performSegue(withIdentifier: "editAliasIdentifier", sender: nil)
            break
		case .address:
			editAdress()
            break
		case .delete:
			showPopupDeleteCard(indexPath)
            break
        case .cardVerification:
            showVerificationActions()
            break;
		}
		return
	}
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 && viewModel.card.verifyStatus == .unknown {
            /// if not veritify status for card, show normal height for section
            return 24
        }
        
        /// calculate height for title
        return viewModel.titleHeader(in: section).isEmpty ? 24 : CGFloat.leastNonzeroMagnitude
	}
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let rowType = viewModel.getRowType(in: indexPath)
        if rowType == .cardVerification && viewModel.card.verifyStatus == .unknown {
            /// hidden verification cell if not setted verify status of card
            return CGFloat.leastNonzeroMagnitude
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // Only necessary to iOS 9. It forces the separator insets to zero.
        cell.separatorInset = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.layoutMargins = UIEdgeInsets.zero
    }
}

extension CreditCardEditViewController: EditCardAliasDelegate {
    
    func onChangeAliasName(of card: CardBank) {
        viewModel.card = card
        tableView.reloadData()
    }
}

extension CreditCardEditViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return (otherGestureRecognizer is UIScreenEdgePanGestureRecognizer)
    }
}

extension CreditCardEditViewController {
    
    /// Return cell whit alert information
    ///
    /// - Parameters:
    ///   - tableView: tableview
    ///   - rowType: row tye
    ///   - indexPath: index path
    /// - Returns: cell with alert information for action popup
    fileprivate func cellForCardInformationAlert(_ tableView: UITableView, rowType: CreditCardEditViewModel.PaymentEditRowType, at indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VerificationCardAlertTableViewCell.identifier, for: indexPath) as? VerificationCardAlertTableViewCell else { return VerificationCardAlertTableViewCell() }
        return cell
    }
    
    /// Return cell of card status information
    ///
    /// - Parameters:
    ///   - tableView: table view
    ///   - rowType: type of row
    ///   - indexPath: indexpath
    /// - Returns: cell status verification
    fileprivate func cellForCardInformation(_ tableView: UITableView, rowType: CreditCardEditViewModel.PaymentEditRowType, at indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VerificationCardInformationTableViewCell.identifier, for: indexPath) as? VerificationCardInformationTableViewCell else { return VerificationCardInformationTableViewCell() }
        cell.configure(isCardValidade: viewModel.isVerifiedCard())
        return cell
    }
    
    /// Show verification popup
    fileprivate func showVerificationActions() {
        let card = viewModel.card
        guard let navigation = navigationController,
            card.verifyStatus != .verified
            else {
            return
        }
        
        CardVerificationHelpers.show(from: navigation, withModel: CardVerificationViewModel(with: card))
    }
    
    fileprivate func setupCardView() {
        cardContainerView.addSubview(frontCardView)
        NSLayoutConstraint.activate(
            [
                frontCardView.leadingAnchor.constraint(equalTo: self.cardContainerView.leadingAnchor, constant: 0),
                frontCardView.topAnchor.constraint(equalTo: self.cardContainerView.topAnchor, constant: 0),
                frontCardView.trailingAnchor.constraint(equalTo: self.cardContainerView.trailingAnchor, constant: 0),
                frontCardView.bottomAnchor.constraint(equalTo: self.cardContainerView.bottomAnchor, constant: 0)
            ]
        )
        
        let card = viewModel.card
        frontCardView.setBackgroundColorAndIcon(bandeiraId: card.creditCardBandeiraId)
        frontCardView.cardNumber = "**** **** **** \(card.lastDigits)"
        frontCardView.ownerName = ""
        frontCardView.expiryDate = ""
    }
    
    // MARK: - Internal Methods
    
    fileprivate func loadCards() {
        tableView.reloadData()
    }
    
    fileprivate func showPopupDeleteCard(_ indexPath: IndexPath) {
        let card = viewModel.card
        let removeCardQuestion = String(format: EditCreditCardLocalizable.removeCardWithLastDigits.text, card.lastDigits)
        let alert = Alert(title: EditCreditCardLocalizable.removeCard.text, text: removeCardQuestion)
        let okButton = Button(title: EditCreditCardLocalizable.removeCard.text, type: .destructive) { [weak self] popupController, _ in
            popupController.dismiss(animated: true, completion: nil)
            self?.navigationController?.popViewController(animated: true)
            self?.delegate?.deleteCreditCard(card)
        }
        let cancelButton = Button(title: DefaultLocalizable.btCancel.text, type: .clean) { [weak self] popupController, _ in
            popupController.dismiss(animated: true, completion: nil)
            self?.tableView.deselectRow(at: indexPath, animated: true)
        }
        alert.buttons = [okButton, cancelButton]
        AlertMessage.showAlert(alert, controller: self)
    }
    
    fileprivate func editAdress() {
        let addressListVM = AddressListViewModel(selectedAddress: viewModel.card.address, displayContextHeader: true)
        let consumerAddressList = AddressListViewController(viewModel: addressListVM)
        
        /// configure header for select new address
        consumerAddressList.addressContextTitle = EditCreditCardLocalizable.invoiceAddress.text
        consumerAddressList.addressContextText = EditCreditCardLocalizable.informInvoiceAddress.text
        consumerAddressList.addressContextImage = #imageLiteral(resourceName: "ilu_invoice")
        
        /// action on selected address in list
        consumerAddressList.onAddressSelected = { [weak self] (address) in
            guard let strongSelf = self, let newAddress = address else {
                return
            }
            let newAddressDescription = strongSelf.viewModel.addressDescription(for: newAddress)
            
            /// Show popup to confirm address change
            let changeAddressQuestion = String(format: EditCreditCardLocalizable.changeInvoiceAddressTo.text, newAddressDescription)
            let alert = Alert(title: EditCreditCardLocalizable.invoiceAddress.text, text: changeAddressQuestion)
            let okButton = Button(title: EditCreditCardLocalizable.changeAddress.text, type: .destructive) { popupController, _ in
                // Action when change is confirmed
                popupController.dismiss(animated: true, completion: {
                    strongSelf.loadingView.frame = strongSelf.view.bounds
                    consumerAddressList.navigationController?.view.addSubview(strongSelf.loadingView)
                    strongSelf.saveNewAddress(with: newAddress)
                })
            }
            let cancelButton = Button(title: DefaultLocalizable.btCancel.text, type: .clean, action: .close)
            alert.buttons = [okButton, cancelButton]
            
            AlertMessage.showAlert(alert, controller: self)
        }
        self.navigationController?.pushViewController(consumerAddressList, animated: true)
    }
    
    /// request for save new address
    ///
    /// - Parameter address: new address
    fileprivate func saveNewAddress(with address: ConsumerAddressItem) {
        viewModel.cardEdit(newAddress: address) { [weak self] error in
            guard let strongSelf = self else {
            return
        }
            strongSelf.loadingView.removeFromSuperview()
            if let error = error {
                AlertMessage.showCustomAlertWithError(error, controller: strongSelf)
            } else {
                strongSelf.delegate?.onChangeAddressOfCard()
                
                /// this navigation controller ther AddressListViewController for presented controlller
                strongSelf.navigationController?.popViewController(animated: true)
            }
        }
    }
}
