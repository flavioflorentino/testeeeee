import UI
import UIKit

protocol EditCardAliasDelegate: AnyObject {
    func onChangeAliasName(of card: CardBank)
}

final class EditCardAliasViewController: PPBaseViewController {
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = Palette.ppColorGrayscale500.color
        }
    }
    
    @IBOutlet weak var aliasTextField: UIPPFloatingTextField! {
        didSet {
            aliasTextField.text = viewModel?.card.alias ?? ""
        }
    }
    
    @IBOutlet weak var backButton: UIButton! {
        didSet {
            let image = Assets.back.image.withRenderingMode(.alwaysTemplate)
            backButton.setImage(image, for: .normal)
            backButton.tintColor = Palette.ppColorGrayscale500.color
        }
    }
	
    var viewModel: EditCardAliasViewModel!
    weak var delegate: EditCardAliasDelegate?
    
    override func navigationBarIsTransparent() -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        aliasTextField.becomeFirstResponder()
    }
    
    private func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
	@IBAction private func saveAlias(_ sender: UIPPButton) {
		guard let alias = aliasTextField.text else {
            return
        }
        sender.startLoadingAnimating()
        viewModel?.cardEdit(newAlias: alias, completion: { [weak self] error in
            guard let strongSelf = self else {
                return
            }
            DispatchQueue.main.async {
                if let error = error {
                    AlertMessage.showCustomAlertWithError(error, controller: strongSelf)
                } else {
                    strongSelf.delegate?.onChangeAliasName(of: strongSelf.viewModel.card)
                    strongSelf.navigationController?.popViewController(animated: true)
                }
            }
        })
	}
	
	@IBAction private func backButtonAction(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
}
