import Foundation
import FeatureFlag

final class WalletOfferCardsWorker: OfferCardsWorkerProtocol {
    var cards: [OfferCard] {
        return walletCards.items
    }
    var headerTitle: String {
        return walletCards.headerTitle
    }
    private var walletCards: WalletOfferCards
    
    init?() {
        guard let dict: [String: Any] = FeatureManager.json(.featureWalletCards) else {
            return nil
        }
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            walletCards = try JSONDecoder(.convertFromSnakeCase).decode(WalletOfferCards.self, from: jsonData)
        } catch {
            // not exist this tag or content in remote config or exist error in json object
            let message: String = "Erro ao fazer parse da lista de cards de oferta da carteira do remote config"
            Log.error(.model, message)
            return nil
        }
    }
}
