import SnapKit
import UI
import UIKit

private extension WalletCardVerifyMessageView.Layout {
    static let iconSize: CGFloat = 14
}

final class WalletCardVerifyMessageView: UIStackView {
    fileprivate enum Layout {}
    
    private let iconView: UIImageView = {
        let imageView = UIImageView(image: Assets.PaymentMethods.cardVerificationIcon.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.text, Strings.Wallet.verifyCard)
            .with(\.textColor, Colors.neutral600.color)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureView()
    }
    
    @available(*, unavailable)
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureView() {
        addArrangedSubview(iconView)
        addArrangedSubview(messageLabel)
        
        iconView.snp.makeConstraints {
            $0.size.equalTo(Layout.iconSize)
        }
        
        spacing = Spacing.base00
        alignment = .center
    }
}
