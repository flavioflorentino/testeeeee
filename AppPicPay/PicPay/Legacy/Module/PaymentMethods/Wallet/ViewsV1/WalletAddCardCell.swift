import SnapKit
import UI
import UIKit

final class WalletAddCardCell: UITableViewCell {
    private lazy var iconView: UIImageView = {
        let imageView = UIImageView(image: Assets.PaymentMethods.iconCardAdd.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.success600.color)
            .with(\.numberOfLines, 1)
            .with(\.text, Strings.Wallet.addCard)
        return label
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.backgroundColor, Colors.backgroundPrimary.color)
            .with(\.border, .light(color: .grayscale100()))
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        containerView.layer.borderColor = Colors.grayscale100.color.cgColor
    }
}

extension WalletAddCardCell: ViewConfiguration {
    func buildViewHierarchy() {
        containerView.addSubview(iconView)
        containerView.addSubview(messageLabel)
        addSubview(containerView)
    }
    
    func setupConstraints() {
        iconView.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview().inset(Spacing.base02)
            $0.size.equalTo(Sizing.base03)
        }
        
        messageLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(iconView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        containerView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        selectionStyle = .none
        
        if UIScreen.isSmall {
            messageLabel.font = Typography.caption(.highlight).font()
        }
    }
}

