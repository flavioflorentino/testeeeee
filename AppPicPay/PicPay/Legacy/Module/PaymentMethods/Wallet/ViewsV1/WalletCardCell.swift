import SnapKit
import UI
import UIKit

protocol WalletCardCellConfigurating: UITableViewCell {
    func configure(card: CardBank, hideStatus: Bool, isSelected: Bool, isUpdating: Bool)
}

protocol WalletCardCellDisplay: AnyObject {
    func displayVerifyCardStatus()
    func hideVerifyCardStatus()
    func displayCardImage(url: URL?)
    func displayCardAsSelected()
    func displayCardAsUnselected()
    func displayCardDescription(text: String, isWarning: Bool)
    func hideCardDescription()
    func displayCardName(text: String)
    func displayLoadingIndicator()
    func hideLoadingIndicator()
}

final class WalletCardCell: UITableViewCell, WalletCardCellConfigurating {
    private lazy var presenter: WalletCardCellPresenter = {
        let presenter = WalletCardCellPresenter()
        presenter.view = self
        return presenter
    }()
    
    private lazy var cardImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.numberOfLines, 1)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale500.color)
        return label
    }()
    
    private lazy var selectedLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale400.color)
            .with(\.text, Strings.Wallet.defaultCard)
            .with(\.textAlignment, .right)
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        return label
    }()
    
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        view.isHidden = true
        return view
    }()
    
    private lazy var nameDescriptionStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [nameLabel, descriptionLabel])
        stackView.spacing = Spacing.base00
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var iconAndTextStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [cardImageView, nameDescriptionStackView, selectedLabel])
        stackView.spacing = Spacing.base02
        stackView.alignment = .top
        return stackView
    }()
    
    private lazy var statusView: UIView = {
        let view = WalletCardVerifyMessageView()
        view.isHidden = true
        return view
    }()
    
    private lazy var mainContentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [iconAndTextStackView, statusView])
        stackView.spacing = Spacing.base02
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.backgroundColor, Colors.backgroundPrimary.color)
            .with(\.border, .light(color: .grayscale100()))
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        containerView.layer.borderColor = Colors.grayscale100.color.cgColor
    }
    
    func configure(card: CardBank, hideStatus: Bool, isSelected: Bool, isUpdating: Bool) {
        presenter.configure(card: card, hideStatus: hideStatus, isSelected: isSelected, isUpdating: isUpdating)
    }
}

extension WalletCardCell: WalletCardCellDisplay {
    func displayVerifyCardStatus() {
        statusView.isHidden = false
    }
    
    func hideVerifyCardStatus() {
        statusView.isHidden = true
    }
    
    func displayCardImage(url: URL?) {
        cardImageView.setImage(url: url, placeholder: #imageLiteral(resourceName: "card_onboarding_icon"))
    }
    
    func displayCardAsSelected() {
        selectedLabel.isHidden = false
    }
    
    func displayCardAsUnselected() {
        selectedLabel.isHidden = true
    }
    
    func displayCardDescription(text: String, isWarning: Bool) {
        descriptionLabel.isHidden = false
        descriptionLabel.text = text
        descriptionLabel.textColor = isWarning ? Colors.critical400.color : Colors.grayscale500.color
    }
    
    func hideCardDescription() {
        descriptionLabel.isHidden = true
    }
    
    func displayCardName(text: String) {
        nameLabel.text = text
    }
    
    func displayLoadingIndicator() {
        loadingIndicator.startAnimating()
        loadingIndicator.isHidden = false
        selectedLabel.isHidden = true
    }
    
    func hideLoadingIndicator() {
        loadingIndicator.stopAnimating()
        loadingIndicator.isHidden = true
    }
}

extension WalletCardCell: ViewConfiguration {
    func buildViewHierarchy() {
        containerView.addSubview(cardImageView)
        containerView.addSubview(mainContentStackView)
        containerView.addSubview(loadingIndicator)
        addSubview(containerView)
    }
    
    func setupConstraints() {
        cardImageView.snp.makeConstraints {
            $0.size.equalTo(Sizing.base03)
        }
        
        mainContentStackView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
        }
        
        loadingIndicator.snp.makeConstraints {
            $0.top.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        containerView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
