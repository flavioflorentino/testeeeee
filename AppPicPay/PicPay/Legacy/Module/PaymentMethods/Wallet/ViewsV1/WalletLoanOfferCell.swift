import AssetsKit
import SnapKit
import UI
import UIKit

final class WalletLoanOfferCell: UITableViewCell {
    private lazy var ilustrationView: UIImageView = {
        let imageView = UIImageView(image:Resources.Illustrations.iluHoldingMoney.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.text, Strings.Wallet.LoanOffer.title)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale500.color)
            .with(\.text, Strings.Wallet.LoanOffer.description)
        return label
    }()
    
    private lazy var disclosureIndicator: UILabel = {
        let label = UILabel()
        label.labelStyle(IconLabelStyle(type: .medium))
            .with(\.text, Iconography.angleRightB.rawValue)
            .with(\.textColor, Colors.branding600.color)
            .with(\.textAlignment, .right)
        label.accessibilityElementsHidden = true
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        return label
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.backgroundColor, Colors.backgroundPrimary.color)
            .with(\.border, .light(color: .grayscale100()))
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        containerView.layer.borderColor = Colors.grayscale100.color.cgColor
    }
}

extension WalletLoanOfferCell: ViewConfiguration {
    func buildViewHierarchy() {
        containerView.addSubviews(ilustrationView, titleLabel, descriptionLabel, disclosureIndicator)
        addSubview(containerView)
    }
    
    func setupConstraints() {
        ilustrationView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.bottom.lessThanOrEqualToSuperview().offset(-Spacing.base02)
            $0.size.equalTo(Sizing.base06)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base02)
            $0.trailing.equalTo(disclosureIndicator.snp.leading).offset(-Spacing.base02)
            $0.leading.equalTo(ilustrationView.snp.trailing).offset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalTo(titleLabel)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
        }
        
        disclosureIndicator.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        containerView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
