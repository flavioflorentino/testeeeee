import Foundation

final class WalletCardCellPresenter {
    weak var view: WalletCardCellDisplay?
    
    func configure(card: CardBank, hideStatus: Bool, isSelected: Bool, isUpdating: Bool) {
        let cardNetworkId = Int(card.creditCardBandeiraId) ?? 0
        let cardNetwork = CreditCardFlagTypeId(rawValue: cardNetworkId)
        
        configureStatus(card: card, hideStatus: hideStatus)
        configureImage(card: card)
        configureSelection(isSelected: isSelected)
        configureDescription(card: card, cardNetwork: cardNetwork)
        configureName(card: card, cardNetwork: cardNetwork)
        configureSelectedLoadingIndicator(isUpdating: isUpdating)
    }
}

private extension WalletCardCellPresenter {
    func configureStatus(card: CardBank, hideStatus: Bool) {
        if !hideStatus, card.verifyStatus == .verifiable || card.verifyStatus == .waitingFillValue {
            view?.displayVerifyCardStatus()
        } else {
            view?.hideVerifyCardStatus()
        }
    }
    
    func configureImage(card: CardBank) {
        view?.displayCardImage(url: URL(string: card.image))
    }
    
    func configureSelection(isSelected: Bool) {
        if isSelected {
            view?.displayCardAsSelected()
        } else {
            view?.displayCardAsUnselected()
        }
    }
    
    func configureDescription(card: CardBank, cardNetwork: CreditCardFlagTypeId?) {
        if card.expired {
            let description: PaymentMethodsLocalizable = card.type == .dda ? .reconnectYourAccount : .cardDefeated
            view?.displayCardDescription(text: description.text, isWarning: true)
            return
        }
        
        if cardNetwork == .picpay {
            view?.hideCardDescription()
            return
        }
        
        view?.displayCardDescription(text: card.description, isWarning: false)
    }
    
    func configureName(card: CardBank, cardNetwork: CreditCardFlagTypeId?) {
        let name = cardNetwork == .picpay ? card.description : card.alias
        view?.displayCardName(text: name)
    }
    
    func configureSelectedLoadingIndicator(isUpdating: Bool) {
        if isUpdating {
            view?.displayLoadingIndicator()
        } else {
            view?.hideLoadingIndicator()
        }
    }
}
