import UI
import UIKit

final class CardTableViewCell: UITableViewCell, WalletCardCellConfigurating {
    @IBOutlet weak var cardBackgroundView: UIView! {
        didSet {
            cardBackgroundView.layer.cornerRadius = 5
            cardBackgroundView.dropShadow()
            cardBackgroundView.backgroundColor = Palette.ppColorGrayscale000.color
        }
    }
    @IBOutlet weak var cardImageFlag: UIImageView!
    @IBOutlet weak var cardName: UILabel! {
        didSet {
            cardName.textColor = Palette.ppColorGrayscale600.color
            cardName.adjustsFontSizeToFitWidth = false
        }
    }
    @IBOutlet weak var cardDescription: UILabel! {
        didSet {
            cardDescription.textColor = Palette.ppColorGrayscale400.color(withCustomDark: .ppColorGrayscale000)
            cardDescription.isHidden = false
        }
    }
    @IBOutlet weak var cardPrimaryLabel: UILabel! {
        didSet {
            cardPrimaryLabel.textColor = Palette.ppColorGrayscale400.color(withCustomDark: .ppColorGrayscale000)
        }
    }
    @IBOutlet weak var activityIndicatorCard: UIActivityIndicatorView!
    @IBOutlet weak var informationStackView: UIStackView!
    @IBOutlet weak var informationLabel: UILabel! {
        didSet {
            informationLabel.textColor = Palette.ppColorNeutral300.color
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor.clear
        informationStackView.isHidden = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        informationStackView.isHidden = true
    }

    func configure(card: CardBank, hideStatus: Bool, isSelected: Bool, isUpdating: Bool) {
        
        if !hideStatus && (card.verifyStatus == .verifiable ||  card.verifyStatus == .waitingFillValue) {
            informationStackView.isHidden = false
        }
        self.cardImageFlag.setImage(url: URL(string: card.image), placeholder: UIImage(named: "card_onboarding_icon"))
        // Cartão Default
        if isSelected {
            cardPrimaryLabel.isHidden = false
            cardImageFlag.alpha = 1.0
            cardName.alpha = 1.0
            cardDescription.alpha = 1.0
        } else {
            cardPrimaryLabel.isHidden = true
            cardImageFlag.alpha = 0.5
            cardName.alpha = 0.5
            cardDescription.alpha = 0.5
        }
        
        if card.expired {
            cardDescription.text = PaymentMethodsLocalizable.cardDefeated.text
            cardDescription.textColor = Palette.ppColorNegative300.color
            if card.type == .dda {
                cardDescription.text = PaymentMethodsLocalizable.reconnectYourAccount.text
            }
        }
        
        
        if isUpdating {
            activityIndicatorCard.startAnimating()
            activityIndicatorCard.isHidden = false
            cardPrimaryLabel.isHidden = true
        } else {
            activityIndicatorCard.stopAnimating()
            activityIndicatorCard.isHidden = true
        }
        
        guard let flagType = CreditCardFlagTypeId(rawValue: Int(card.creditCardBandeiraId) ?? 0) else {
            return
        }
        if flagType == .picpay {
            /// configure cell for credit picpay
            selectionStyle = .none
            cardName.text = card.description
            cardName.isHidden = false
            cardDescription.text = ""
        } else {
            /// set up for normal flat type of card
            selectionStyle = .default
            cardName.isHidden = false
            cardDescription.isHidden = false
            cardName.text = card.alias
            cardDescription.text = card.description
        }
    }
    
    func configureAddCell() {
        cardImageFlag.alpha = 1.0
        cardName.alpha = 1.0
        cardDescription.alpha = 1.0
        cardName.textColor = Palette.ppColorBranding300.color
        cardName.text = PaymentMethodsLocalizable.addCreditCard.text
        cardDescription.isHidden = true
        cardImageFlag.image = UIImage(named: "iconCardAdd")
        cardPrimaryLabel.isHidden = true
        activityIndicatorCard.isHidden = true
        cardName.adjustsFontSizeToFitWidth = true
    }
}
