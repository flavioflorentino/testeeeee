import UIKit
import UI

final class BalancePicPayTableViewCell: UITableViewCell {
    @IBOutlet weak var shouldUsePPBalance: UISwitch!
    @IBOutlet weak var balance: UILabel!
    @IBOutlet weak var useBalanceConstraint: NSLayoutConstraint!
    @IBOutlet weak var switchDesriptionLabel: UILabel!
    
    func configure(usePicPayBalance: Bool, showBalance: Bool, balanceValue: NSDecimalNumber?) {
        shouldUsePPBalance.isOn = usePicPayBalance
        if showBalance {
            if let value = CurrencyFormatter.brazillianRealString(from: balanceValue!) {
                balance.text = "\(PaymentMethodsLocalizable.balanceAvailable.text) \(value)"
                balance.isHidden = false
                useBalanceConstraint.constant = -6
            }
        }
    }
    
    func configureLayoutV1() {
        contentView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
            .with(\.border, .light(color: .grayscale100()))
        
        switchDesriptionLabel.textColor = Colors.grayscale700.color
    }
}
