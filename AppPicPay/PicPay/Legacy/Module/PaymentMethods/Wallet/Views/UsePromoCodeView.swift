import UI
import UIKit

protocol UsePromoCodeDelegate: AnyObject {
    func didTapUsePromoCode()
}

final class UsePromoCodeView: UIView {
    private lazy var icon: UIImageView = {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: 24, height: 24)))
        imageView.image = #imageLiteral(resourceName: "ico_promo_code_green")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let stringAttributes: [NSAttributedString.Key: Any] = [
        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold),
        NSAttributedString.Key.foregroundColor: Palette.ppColorBranding300.color,
        NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
    ]
    
    private lazy var text: UILabel = {
        let label = UILabel()
        let attributedString = NSMutableAttributedString(string: PaymentMethodsLocalizable.usePromotionalCode.text, attributes: stringAttributes)
        label.attributedText = attributedString
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 6
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    weak var delegate: UsePromoCodeDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addComponents()
        layoutComponents()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        stackView.addGestureRecognizer(tapGesture)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        stackView.addArrangedSubview(icon)
        stackView.addArrangedSubview(text)
        addSubview(stackView)
    }
    
    private func layoutComponents() {
        // Helps to keep button on vertical center (CardTableViewCells have a padding on the botton)
        let verticalOffset: CGFloat = -2
        
        NSLayoutConstraint.activate([
            icon.widthAnchor.constraint(equalToConstant: 24),
            icon.heightAnchor.constraint(equalToConstant: 24)
        ])
        
        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: verticalOffset)
        ])
    }
    
    @objc
    private func tapAction() {
        delegate?.didTapUsePromoCode()
    }
}
