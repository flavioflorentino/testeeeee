import UI
import UIKit

final class EmptyCardsTableViewCell: UITableViewCell {
    // MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var addCardLabel: UILabel!
    @IBOutlet weak var shadowView: CustomCommonView!
    @IBOutlet weak var backView: UIView!
    
    // MARK: - Life Cycle
	override func awakeFromNib() {
		super.awakeFromNib()
        prepareLayout()
	}
    
    func configureLayoutV1() {
        shadowView.isHidden = true
        backView.viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.backgroundColor, Colors.backgroundPrimary.color)
            .with(\.border, .light(color: .grayscale100()))
        
        selectionStyle = .none
        backgroundColor = .clear
    }
    
    private func prepareLayout() {
        backgroundColor = .clear
        shadowView.layer.shouldRasterize = true
        shadowView.shadowColor = Palette.ppColorGrayscale600.color
        backView.backgroundColor = Palette.ppColorGrayscale000.color
        titleLabel.textColor = Palette.ppColorGrayscale600.color
        infoLabel.textColor = Palette.ppColorGrayscale400.color
        addCardLabel.textColor = Palette.ppColorBranding300.color
        backView.layer.cornerRadius = 5
    }
}
