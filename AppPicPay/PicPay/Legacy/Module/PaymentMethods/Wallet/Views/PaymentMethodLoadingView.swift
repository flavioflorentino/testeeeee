import Foundation
import UI

final class PaymentMethodLoadingView : NibView {
    @IBOutlet weak var headerViewMock: GradientView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    func configure(withHeader: Bool = true) {
        headerViewMock.isHidden = true
        if !withHeader {
            headerViewMock.isHidden = false
        } else {
            activityIndicator.color = .gray
        }
    }
}
