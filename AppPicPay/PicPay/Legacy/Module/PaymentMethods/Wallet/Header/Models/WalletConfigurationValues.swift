import Foundation

struct WalletRemoteValues: Decodable {
    struct IncomeDescription: Decodable {
        let noDeposit: String
        let withDeposit: String
    }
    
    let incomeDescription: IncomeDescription
}
