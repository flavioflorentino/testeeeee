import AnalyticsModule
import UI
import UIKit

protocol WalletHeaderDisplay: AnyObject {
    func displaySavings(isBadgeHiden: Bool)
    func hideSavings()
    func display(balanceDescription: NSAttributedString)
    func display(balance: String)
}

extension WalletHeaderView.Layout {
    static let minimumViewHeight: CGFloat = 200.0
    static let statusBarHeight: CGFloat = 22.0
    
    enum Size {
        static let savingsButton: CGFloat = Sizing.base03
        static let savingsButtonBadge: CGFloat = Sizing.base02
    }
    
    enum BorderWidth {
        static let savingsButton: CGFloat = 1
        static let savingsButtonBadge: CGFloat = 1
    }
    
    enum GradientColors {
        // Todo: Update with the new colors from the design system, when they become available
        static let balanceOn = (start: #colorLiteral(red: 0.2392156863, green: 0.7019607843, blue: 0.537254902, alpha: 1) , end: #colorLiteral(red: 0.168627451, green: 0.6509803922, blue: 0.4745098039, alpha: 1))
        static let balanceOff = (start: #colorLiteral(red: 0.83, green: 0.83, blue: 0.83, alpha: 1.00), end: #colorLiteral(red: 0.53, green: 0.53, blue: 0.53, alpha: 1.00))
    }
}

final class WalletHeaderView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var presenter: WalletHeaderPresenting = {
        let presenter = WalletHeaderPresenter(
            dependencies: DependencyContainer(),
            isSavingsActive: SavingsRemoteConfigWrapper.isActiveForCurrentUser()
        )
        presenter.view = self
        return presenter
    }()
    
    private lazy var bankAccountView = WalletBankAccountView()
    
    private lazy var savingsButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = Typography.icons(.small).font()
        button.setTitleColor(Colors.white.lightColor, for: .normal)
        button.setTitle(Iconography.arrowGrowth.rawValue, for: .normal)
        button.layer.cornerRadius = Layout.Size.savingsButton / 2
        button.layer.borderColor = Colors.white.lightColor.cgColor
        button.layer.borderWidth = Layout.BorderWidth.savingsButton
        button.addTarget(self, action: #selector(showSavings), for: .touchUpInside)
        return button
    }()
    
    private lazy var savingsButtonBadge: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.critical400.color
        view.layer.cornerRadius = Layout.Size.savingsButtonBadge / 2
        view.layer.borderWidth = Layout.BorderWidth.savingsButtonBadge
        view.layer.borderColor = Colors.white.lightColor.cgColor
        view.isHidden = true
        return view
    }()
    
    private lazy var topBalanceLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.white.lightColor)
            .with(\.textAlignment, .center)
            .with(\.text, Strings.Wallet.balanceAvailableOnWallet)
        return label
    }()
    
    private lazy var balanceView: WalletBalanceView = {
        let view = WalletBalanceView()
        view.didTapBalance = { [weak self] in
            self?.openSavings?()
        }
        view.didTapVisibility = { [weak self] in
            self?.changeBalanceVisibility?()
        }
        return view
    }()
    
    private lazy var balanceDescriptionView: WalletBalanceDescriptionView = {
        let view = WalletBalanceDescriptionView()
        view.didTap = { [weak self] in
            self?.openSavings?()
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(showSavings))
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
        
        return view
    }()
    
    private lazy var balanceContentView = UIView()
    
    private lazy var actionButtonsView: UIView = {
        let view = WalletHeaderActionButtonsView(isWalletLayoutV1: true)
        view.delegate = self
        return view
    }()
    
    private lazy var gradientView = GradientView()
    
    var computedWidth: CGFloat {
        UIScreen.main.bounds.width
    }
    
    var computedHeight: CGFloat {
        let contentViewWidth = UIScreen.main.bounds.width
        let targetSize = CGSize(width: contentViewWidth, height: UIView.layoutFittingCompressedSize.height)
        let size = systemLayoutSizeFitting(targetSize)
        return max(Layout.minimumViewHeight, size.height)
    }
    
    private var topPadding: CGFloat {
        if #available(iOS 11.0, *) {
            return UIApplication.shared.keyWindow?.safeAreaInsets.top ?? .zero
        } else {
            return Layout.statusBarHeight
        }
    }
    
    var addAction: (() -> Void)?
    var withdrawAction: (() -> Void)?
    var changeBalanceVisibility: (() -> Void)?
    var openSavings: (() -> Void)?
    var openStatement: (() -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        balanceContentView.addSubviews(topBalanceLabel, balanceView, balanceDescriptionView)
        addSubviews(gradientView, bankAccountView, savingsButton, savingsButtonBadge, balanceContentView, actionButtonsView)
    }
    
    func setupConstraints() {
        gradientView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        bankAccountView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.lessThanOrEqualTo(savingsButton.snp.leading)
            $0.centerY.equalTo(savingsButton)
        }
        
        savingsButton.snp.makeConstraints {
            $0.top.equalToSuperview().offset(topPadding + Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.size.equalTo(Layout.Size.savingsButton)
        }
        
        savingsButtonBadge.snp.makeConstraints {
            $0.size.equalTo(Sizing.base02)
            $0.top.leading.equalTo(savingsButton).offset(-Spacing.base00)
        }

        topBalanceLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }

        balanceView.snp.makeConstraints {
            $0.top.equalTo(topBalanceLabel.snp.bottom).offset(Spacing.base01)
            $0.centerX.equalToSuperview()
            $0.leading.greaterThanOrEqualToSuperview()
            $0.trailing.lessThanOrEqualToSuperview()
        }

        balanceDescriptionView.snp.makeConstraints {
            $0.top.equalTo(balanceView.snp.bottom).offset(Spacing.base01)
            $0.centerX.bottom.equalToSuperview()
            $0.leading.top.greaterThanOrEqualToSuperview()
            $0.trailing.bottom.lessThanOrEqualToSuperview()
        }

        balanceContentView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base08 + topPadding)
            $0.centerX.equalToSuperview()
            $0.leading.greaterThanOrEqualToSuperview()
            $0.trailing.lessThanOrEqualToSuperview()
        }
        
        actionButtonsView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.leading.greaterThanOrEqualToSuperview()
            $0.trailing.lessThanOrEqualToSuperview()
            $0.top.equalTo(balanceContentView.snp.bottom).offset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base04)
        }
    }
    
    func configureViews() {
        backgroundColor = Layout.GradientColors.balanceOn.start
    }
    
    @objc
    private func showSavings() {
        openSavings?()
    }
}

extension WalletHeaderView: WalletHeaderViewConfiguration {
    func configure(balance: NSDecimalNumber, isLoadingBalance: Bool, isBalanceHidden: Bool) {
        presenter.presentSavingsIfNeeded(balance: balance)
        balanceView.setupBalanceVisibility(isBalanceHidden, animated: false)
        
        presenter.presentBalance(balance)
        balanceView.isLoadingBalance = isLoadingBalance
    }
    
    func configure(useBalance: Bool, animated: Bool = false) {
        let color = useBalance ? Layout.GradientColors.balanceOn : Layout.GradientColors.balanceOff
        backgroundColor = color.start
        
        if animated {
            gradientView.animateTo(startColor: color.start, endColor: color.end)
        } else {
            gradientView.startColor = color.start
            gradientView.endColor = color.end
        }
    }
    
    func configure(userBankAccount: SettingsBankInfo?) {
        bankAccountView.configure(with: userBankAccount)
    }
    
    func setupBalanceVisibility(_ isHidden: Bool, animated: Bool) {
        balanceView.setupBalanceVisibility(isHidden, animated: animated)
    }
}

extension WalletHeaderView: WalletHeaderActionButtonsViewDelegate {
    func add() {
        addAction?()
    }
    
    func withdraw() {
        Analytics.shared.log(WalletEvent.didTapWithdrawalButton)
        withdrawAction?()
    }
    
    func showStatement() {
        openStatement?()
    }
}

extension WalletHeaderView: WalletHeaderDisplay {
    func displaySavings(isBadgeHiden: Bool) {
        savingsButton.isHidden = false
        savingsButtonBadge.isHidden = isBadgeHiden
        balanceDescriptionView.isHidden = false
    }
    
    func hideSavings() {
        savingsButton.isHidden = true
        savingsButtonBadge.isHidden = true
        balanceDescriptionView.isHidden = true
    }
    
    func display(balanceDescription: NSAttributedString) {
        balanceDescriptionView.descriptionText = balanceDescription
    }
    
    func display(balance: String) {
        balanceView.balanceValue = balance
    }
}
