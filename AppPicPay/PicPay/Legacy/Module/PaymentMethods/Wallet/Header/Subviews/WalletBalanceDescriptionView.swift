import SnapKit
import UI
import UIKit

final class WalletBalanceDescriptionView: UIView, ViewConfiguration {
    private lazy var balanceDescriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.white.lightColor
        return label
    }()
    
    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, Colors.white.lightColor)
            .with(\.text, Iconography.angleRight.rawValue)
        label.isAccessibilityElement = false
        return label
    }()
    
    var didTap: (() -> Void)?
    
    var descriptionText: NSAttributedString? {
        didSet {
            iconLabel.isHidden = descriptionText == nil
            balanceDescriptionLabel.attributedText = descriptionText
        }
    }

    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(balanceDescriptionLabel, iconLabel)
    }
    
    func setupConstraints() {
        balanceDescriptionLabel.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
        }
        
        iconLabel.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview()
            $0.leading.equalTo(balanceDescriptionLabel.snp.trailing)
        }
    }
    
    func configureViews() {
        isUserInteractionEnabled = true
        isAccessibilityElement = true
        accessibilityTraits = .button
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapView))
        addGestureRecognizer(tapGesture)
    }
    
    @objc
    private func didTapView() {
        didTap?()
    }
}
