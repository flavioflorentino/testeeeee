import SnapKit
import UI
import UIKit

final class WalletBankAccountView: UIView, ViewConfiguration {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.white.lightColor)
            .with(\.text, Strings.Wallet.myAccount)
        return label
    }()
    
    private lazy var accountLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.white.lightColor)
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(titleLabel, accountLabel)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
        
        accountLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        isHidden = false
    }
    
    func configure(with bank: SettingsBankInfo?) {
        guard let bank = bank else {
            isHidden = true
            return
        }
        isHidden = false
        
        let text = Strings.Wallet.bankAccountDescription(bank.agency, bank.account)
        accountLabel.attributedText = text.attributedStringWithFont(
            primary: Typography.caption().font(),
            secondary: Typography.caption(.highlight).font()
        )
    }
}
