import AssetsKit
import SnapKit
import UI
import UIKit

protocol WalletHeaderActionButtonsViewDelegate: AnyObject {
    func add()
    func withdraw()
    func showStatement()
}

final class WalletHeaderActionButtonsView: UIView, ViewConfiguration {
    private lazy var addButton: WalletHeaderActionButton = {
        let button = WalletHeaderActionButton()
        button.addTarget(self, action: #selector(didTapAddButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var withdrawButton: WalletHeaderActionButton = {
        let button = WalletHeaderActionButton()
        button.addTarget(self, action: #selector(didTapWithdrawButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var statementButton: WalletHeaderActionButton = {
        let button = WalletHeaderActionButton()
        button.addTarget(self, action: #selector(didTapStatementButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [addButton, withdrawButton, statementButton])
        stackView.alignment = .top
        stackView.distribution = .equalCentering
        stackView.spacing = Spacing.base04
        return stackView
    }()
    
    weak var delegate: WalletHeaderActionButtonsViewDelegate?
    
    private let isWalletLayoutV1: Bool
    
    init(isWalletLayoutV1: Bool) {
        self.isWalletLayoutV1 = isWalletLayoutV1
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(buttonsStackView)
    }
    
    func setupConstraints() {
        buttonsStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func configureViews() {
        if isWalletLayoutV1 {
            configureLayoutV1()
        } else {
            configureLegacyLayout()
        }
    }
    
    private func configureLayoutV1() {
        addButton.configure(
            icon: .moneyWithdraw,
            text: Strings.Wallet.addMoney
        )
        
        withdrawButton.configure(
            icon: .moneyInsert,
            text: Strings.Wallet.withdrawMoney
        )
        
        statementButton.configure(
            icon: .receipt,
            text: Strings.Wallet.myStatement
        )
    }
    
    private func configureLegacyLayout() {
        addButton.configure(
            image: Resources.Icons.icoCashin.image,
            text: PaymentMethodsLocalizable.add.text
        )
        
        withdrawButton.configure(
            image: Resources.Icons.icoCashout.image,
            text: PaymentMethodsLocalizable.withdraw.text
        )
        
        statementButton.configure(
            image: Resources.Icons.icoStatementThin.image,
            text: PaymentMethodsLocalizable.statement.text
        )
    }
    
    @objc
    private func didTapAddButton() {
        delegate?.add()
    }
    
    @objc
    private func didTapWithdrawButton() {
        delegate?.withdraw()
    }
    
    @objc
    private func didTapStatementButton() {
        delegate?.showStatement()
    }
}
