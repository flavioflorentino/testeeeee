import AssetsKit
import FeatureFlag
import SnapKit
import UI
import UIKit

extension WalletBalanceView.Layout {
    static let horizontalLineHeight: CGFloat = 2.0
    static let visibilityButtonSize: CGFloat = 28.0
    static let balanceFontSize: CGFloat = 40.0
}

final class WalletBalanceView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
     private var isNewNavigationBarActive: Bool {
        FeatureManager.shared.isActive(.experimentNewAccessSettings)
    }
    
    private var visibilityOnIcon: UIImage {
        isNewNavigationBarActive ? Resources.Icons.icoPasswordVisible.image : Assets.PaymentMethods.visibilityOn.image
    }
    
    private var visibilityOffIcon: UIImage {
        isNewNavigationBarActive ? Resources.Icons.icoPasswordInvisible.image : Assets.PaymentMethods.visibilityOff.image
    }
    
    private lazy var currencyLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: Layout.balanceFontSize, weight: .thin)
        label.textColor = Colors.white.lightColor
        label.text = Strings.Wallet.realCurrencySymbol
        return label
    }()
    
    private lazy var balanceLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.font, UIFont.systemFont(ofSize: Layout.balanceFontSize, weight: .medium))
            .with(\.textColor, Colors.white.lightColor)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapBalanceView))
        label.addGestureRecognizer(tap)
        label.isUserInteractionEnabled = true
        
        return label
    }()
    
    private lazy var horizontalLine: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.white.lightColor
        view.alpha = 0
        return view
    }()
    
    private lazy var visibilityButton: UIImageView = {
        let view = UIImageView()
        view.image = visibilityOnIcon
        view.contentMode = .scaleAspectFit
        view.tintColor = Colors.white.lightColor
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapVisibilityIcon))
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
        
        return view
    }()
    
    private lazy var contentView = UIView()
    
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        view.color = Colors.white.lightColor
        view.isHidden = true
        return view
    }()
    
    var isLoadingBalance: Bool = false {
        didSet {
            if isLoadingBalance {
                loadingIndicator.startAnimating()
                loadingIndicator.isHidden = false
                contentView.alpha = 0
            } else {
                loadingIndicator.stopAnimating()
                loadingIndicator.isHidden = true
                contentView.alpha = 1
            }
        }
    }
    
    var balanceValue: String? {
        didSet {
            balanceLabel.text = balanceValue
        }
    }
    
    var didTapBalance: (() -> Void)?
    var didTapVisibility: (() -> Void)?
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        contentView.addSubviews(currencyLabel, balanceLabel, visibilityButton, horizontalLine)
        addSubview(contentView)
        addSubview(loadingIndicator)
    }
    
    func setupConstraints() {
        currencyLabel.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
        }
        
        balanceLabel.snp.makeConstraints {
            $0.leading.equalTo(currencyLabel.snp.trailing).offset(Spacing.base01)
            $0.top.bottom.equalToSuperview()
        }
        
        horizontalLine.snp.makeConstraints {
            $0.height.equalTo(Layout.horizontalLineHeight)
            $0.leading.trailing.centerY.equalTo(balanceLabel)
        }
        
        visibilityButton.snp.makeConstraints {
            $0.width.equalTo(Layout.visibilityButtonSize)
            $0.top.trailing.bottom.equalToSuperview()
            $0.leading.equalTo(balanceLabel.snp.trailing).offset(Spacing.base01)
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        loadingIndicator.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    func configureViews() {
        isUserInteractionEnabled = true
        backgroundColor = .clear
    }
    
    func setupBalanceVisibility(_ isHidden: Bool, animated: Bool) {
        let duration = animated ? 0.25 : 0
        visibilityButton.image = isHidden ? visibilityOffIcon : visibilityOnIcon
        
        UIView.animate(withDuration: duration) {
            self.balanceLabel.alpha = isHidden ? 0 : 1
            self.horizontalLine.alpha = isHidden ? 1 : 0
        }
        
        balanceLabel.setHiddenComponentsInAppSwitcher(!isHidden)
    }
    
    @objc
    private func didTapBalanceView() {
        didTapBalance?()
    }
    
    @objc
    private func didTapVisibilityIcon() {
        didTapVisibility?()
    }
}
