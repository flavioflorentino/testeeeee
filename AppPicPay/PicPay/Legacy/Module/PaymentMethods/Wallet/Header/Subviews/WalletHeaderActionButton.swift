import SnapKit
import UI
import UIKit

extension WalletHeaderActionButton.Layout {
    enum Size {
        static let iconImage = CGSize(width: 40, height: 40)
    }
}

final class WalletHeaderActionButton: UIButton, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(IconLabelStyle(type: .medium))
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.white.lightColor)
        return label
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .center
        imageView.backgroundColor =  Colors.white.lightColor.withAlphaComponent(0.25)
        imageView.layer.cornerRadius = Layout.Size.iconImage.height / 2
        return imageView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.white.lightColor)
            .with(\.textAlignment, .center)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(image: UIImage?, text: String) {
        iconImageView.image = image
        descriptionLabel.text = text
        accessibilityLabel = text
        iconLabel.isHidden = true
    }
    
    func configure(icon: Iconography, text: String) {
        iconLabel.text = icon.rawValue
        descriptionLabel.text = text
        accessibilityLabel = text
    }
    
    func buildViewHierarchy() {
        addSubview(iconImageView)
        iconImageView.addSubview(iconLabel)
        addSubview(descriptionLabel)
    }
    
    func setupConstraints() {
        iconImageView.snp.makeConstraints {
            $0.top.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.iconImage)
        }
        
        iconLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
            $0.top.equalTo(iconImageView.snp.bottom).offset(Spacing.base01)
            $0.width.greaterThanOrEqualTo(Spacing.base10)
        }
    }
}
