import Core
import FeatureFlag
import Foundation
import UI

typealias GradientColors = (start: UIColor , end: UIColor)

protocol WalletHeaderPresenting: AnyObject {
    func presentSavingsIfNeeded(balance: NSDecimalNumber)
    func presentBalance(_ balance: NSDecimalNumber)
}

final class WalletHeaderPresenter: WalletHeaderPresenting {
    typealias Dependencies = HasKVStore & HasFeatureManager
    private let dependencies: Dependencies
    weak var view: WalletHeaderDisplay?
    
    private let isSavingsActive: Bool
    
    private var userSawOnboarding: Bool {
        dependencies.kvStore.boolFor(KVKey.savings_onboarding_saw)
    }
    
    private var balanceDescription: WalletRemoteValues.IncomeDescription? {
        dependencies.featureManager.object(.walletConfigValues, type: WalletRemoteValues.self)?.incomeDescription
    }
    
    init(dependencies: Dependencies, isSavingsActive: Bool) {
        self.dependencies = dependencies
        self.isSavingsActive = isSavingsActive
    }
    
    func presentSavingsIfNeeded(balance: NSDecimalNumber) {
        guard isSavingsActive else {
            view?.hideSavings()
            return
        }
        let sawOnboarding = dependencies.kvStore.boolFor(KVKey.savings_onboarding_saw)
        view?.displaySavings(isBadgeHiden: sawOnboarding)
        setDescriptionLabel(balance: balance)
    }
    
    func presentBalance(_ balance: NSDecimalNumber) {
        guard balance != .notANumber else {
            view?.display(balance: "")
            return
        }
        view?.display(balance: CurrencyFormatter.twoDecimalsString(from: balance) ?? "")
    }
    
    private func setDescriptionLabel(balance: NSDecimalNumber) {
        guard let balanceDescription = balanceDescription else { return }
        
        let isBalanceZero = balance.compare(NSDecimalNumber.zero) == .orderedSame
        let descriptionText = (isBalanceZero ? balanceDescription.noDeposit : balanceDescription.withDeposit)
            .replacingOccurrences(of: "<b>", with: "**")
            .replacingOccurrences(of: "</b>", with: "**")
        let attributedText = descriptionText.attributedStringWithFont(
            primary: Typography.caption().font(),
            secondary: Typography.caption(.highlight).font()
        )
        
        view?.display(balanceDescription: attributedText)
    }
}
