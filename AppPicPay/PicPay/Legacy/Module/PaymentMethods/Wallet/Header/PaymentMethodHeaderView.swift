import AssetsKit
import AnalyticsModule
import Core
import CoreLegacy
import FeatureFlag
import UI
import UIKit
import SecurityModule
import SnapKit

protocol WalletHeaderViewConfiguration: UIView {
    var addAction: (() -> Void)? { get set }
    var withdrawAction: (() -> Void)? { get set }
    var changeBalanceVisibility: (() -> Void)? { get set }
    var openSavings: (() -> Void)? { get set }
    var openStatement: (() -> Void)? { get set }
    func configure(balance: NSDecimalNumber, isLoadingBalance: Bool, isBalanceHidden: Bool)
    func configure(useBalance: Bool, animated: Bool)
    func configure(userBankAccount: SettingsBankInfo?)
    func setupBalanceVisibility(_ isHidden: Bool, animated: Bool)
}

final class PaymentMethodHeaderView: NibView, WalletHeaderViewConfiguration {
    private var isNewNavigationBarActive: Bool {
        FeatureManager.shared.isActive(.experimentNewAccessSettings)
    }
    
    private var visibilityOnIcon: UIImage {
        isNewNavigationBarActive ? Resources.Icons.icoPasswordVisible.image : Assets.PaymentMethods.visibilityOn.image
    }
    
    private var visibilityOffIcon: UIImage {
        isNewNavigationBarActive ? Resources.Icons.icoPasswordInvisible.image : Assets.PaymentMethods.visibilityOff.image
    }
    
    @IBOutlet weak var gradientTopVerticalSpacingConstraint: NSLayoutConstraint!
    @IBOutlet weak var balanceStackView: UIStackView!
    @IBOutlet weak var balance: UILabel! {
        didSet {
            balance.isUserInteractionEnabled = true
            balance.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapBalanceLabel)))
        }
    }
    @IBOutlet weak var horizontalLine: UIView!
    @IBOutlet weak var visibilityIcon: UIImageView! {
        didSet {
            visibilityIcon.isUserInteractionEnabled = true
            visibilityIcon.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapVisibilityIcon)))
        }
    }
    @IBOutlet weak var gradient: GradientView!
    @IBOutlet weak var balanceAcitivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var descriptionBalanceLabel: UILabel! {
        didSet {
            descriptionBalanceLabel.font = UIFont.systemFont(ofSize: 12, weight: .regular)
            descriptionBalanceLabel.textColor = Palette.ppColorGrayscale000.color(withCustomDark: .ppColorGrayscale000)
        }
    }
    @IBOutlet weak var badgeSavingsView: UIView! {
        didSet {
            badgeSavingsView.backgroundColor = Palette.ppColorNegative300.color
            badgeSavingsView.isHidden = true
            badgeSavingsView.layer.cornerRadius = badgeSavingsView.frame.height / 2
            badgeSavingsView.clipsToBounds = true
        }
    }
    @IBOutlet weak var openSavingsButton: UIButton!
    
    @IBOutlet weak var legacyActionButtonsView: UIView! {
        didSet {
            legacyActionButtonsView.isHidden = true
        }
    }
    
    private lazy var actionButtonsView: UIView = {
        let view = WalletHeaderActionButtonsView(isWalletLayoutV1: false)
        view.delegate = self
        return view
    }()
    
    var addAction: (() -> Void)?
    var withdrawAction: (() -> Void)?
    var changeBalanceVisibility: (() -> Void)?
    var openSavings: (() -> Void)?
    var openStatement: (() -> Void)?

    // MARK: - Initializer
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let actualHeight: CGFloat = gradientTopVerticalSpacingConstraint.constant
        let padding: CGFloat
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top ?? 0.0
            padding = topPadding
        } else {
            let statusBarHeight: CGFloat = 22.0
            padding = statusBarHeight
        }
        gradientTopVerticalSpacingConstraint.constant = actualHeight + padding
        horizontalLine.alpha = 0
        
        layoutIfNeeded()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Public Methods
    
    func configure(balance: NSDecimalNumber, isLoadingBalance: Bool, isBalanceHidden: Bool) {
        self.balance.text = ""
        descriptionBalanceLabel.attributedText = NSAttributedString(string: "")
        
        showSavingsIfNeeded(balance: balance)
        setupBalanceVisibility(isBalanceHidden, animated: false)
        if isLoadingBalance {
            balanceAcitivityIndicator.startAnimating()
            balanceAcitivityIndicator.isHidden = false
            balanceStackView.alpha = 0
        } else {
            balanceAcitivityIndicator.stopAnimating()
            balanceAcitivityIndicator.isHidden = true
            balanceStackView.alpha = 1
            self.balance.text = (balance != .notANumber) ? CurrencyFormatter.twoDecimalsString(from: balance) : ""
        }
    }
    
    private func showSavingsIfNeeded(balance: NSDecimalNumber) {
        if SavingsRemoteConfigWrapper.isActiveForCurrentUser() {
            openSavingsButton.isHidden = false
            descriptionBalanceLabel.isHidden = false
            
            setDescriptionLabel(balance: balance)
            
            let sawOnboarding = KVStore().boolFor(.savings_onboarding_saw)
            badgeSavingsView.isHidden = sawOnboarding
        } else {
            openSavingsButton.isHidden = true
            descriptionBalanceLabel.isHidden = true
        }
    }
    
    private func setDescriptionLabel(balance: NSDecimalNumber) {
        let descriptionBalance: String
        if balance.compare(NSDecimalNumber.zero) == .orderedSame {
            descriptionBalance = SavingsRemoteConfigWrapper.getWalletBalanceDescription(for: .noMoney)
        } else {
            descriptionBalance = SavingsRemoteConfigWrapper.getWalletBalanceDescription(for: .withMoney)
        }
        descriptionBalanceLabel.attributedText = descriptionBalance.attributedString(withBoldIn: [NSRange(location: 0, length: descriptionBalance.count)], using: descriptionBalanceLabel.font)
    }
    
    func configure(useBalance: Bool, animated: Bool = false) {
        var startColor = #colorLiteral(red: 0.83, green: 0.83, blue: 0.83, alpha: 1.00)
        var endColor = #colorLiteral(red: 0.53, green: 0.53, blue: 0.53, alpha: 1.00)
        
        if useBalance {
            startColor = #colorLiteral(red: 0.32, green: 0.91, blue: 0.55, alpha: 1.00)
            endColor = #colorLiteral(red: 0.09, green: 0.75, blue: 0.41, alpha: 1.00)
        }
        
        if animated {
            gradient.animateTo(startColor: startColor, endColor: endColor)
        } else {
            gradient.startColor = startColor
            gradient.endColor = endColor
        }
        
        backgroundColor = startColor
    }
    
    func setupBalanceVisibility(_ isHidden: Bool, animated: Bool) {
        let duration = animated ? 0.25 : 0
        visibilityIcon.image = isHidden ? visibilityOffIcon : visibilityOnIcon
        
        UIView.animate(withDuration: duration) {
            self.balance.alpha = isHidden ? 0 : 1
            self.horizontalLine.alpha = isHidden ? 1 : 0
        }
        
        balance.setHiddenComponentsInAppSwitcher(!isHidden)
    }
    
    func setupButtons(isStatementEnabled: Bool) {
        legacyActionButtonsView.isHidden = isStatementEnabled
        
        guard isStatementEnabled else {
            return
        }
        
        setupButtonsWithStatement()
    }

    @IBAction private func AdicionarAction(_ sender: Any) {
        addAction?()
    }
    
    @IBAction private func retirarAction(_ sender: Any) {
        Analytics.shared.log(WalletEvent.didTapWithdrawalButton)
        withdrawAction?()
    }
    
    @IBAction private func openSavingsAction(_ sender: Any) {
        if SavingsRemoteConfigWrapper.isActiveForCurrentUser() {
            PPAnalytics.trackEvent(AnalyticsSavingsKeys.enterSavings, properties: [AnalyticsSavingsKeys.enterWay: AnalyticsSavingsKeys.buttonClick])
            Analytics.shared.log(WalletEvent.incomeAccessed(type: .button))
            openSavings?()
        }
    }
    
    @objc
    func didTapBalanceLabel() {
        if SavingsRemoteConfigWrapper.isActiveForCurrentUser() {
            PPAnalytics.trackEvent(AnalyticsSavingsKeys.enterSavings, properties: [AnalyticsSavingsKeys.enterWay: AnalyticsSavingsKeys.balanceClick])
            Analytics.shared.log(WalletEvent.incomeAccessed(type: .value))
            openSavings?()
        }
    }
    
    @objc
    func didTapVisibilityIcon() {
        changeBalanceVisibility?()
    }
    
    @objc
    func didTapStatementIcon() {
        openStatement?()
    }
    
    private func setupButtonsWithStatement() {
        gradient.addSubview(actionButtonsView)
        
        actionButtonsView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.leading.greaterThanOrEqualToSuperview()
            $0.trailing.lessThanOrEqualToSuperview()
            $0.top.equalTo(descriptionBalanceLabel.snp.bottom).offset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }
    }
    
    // Bank account is not displayed on this legacy header
    func configure(userBankAccount: SettingsBankInfo?) {}
}

extension PaymentMethodHeaderView: WalletHeaderActionButtonsViewDelegate {
    func add() {
        Analytics.shared.log(WalletEvent.didTapAddMoneyButton)
        addAction?()
    }
    
    func withdraw() {
        Analytics.shared.log(WalletEvent.didTapWithdrawalButton)
        withdrawAction?()
    }
    
    func showStatement() {
        openStatement?()
    }
}
