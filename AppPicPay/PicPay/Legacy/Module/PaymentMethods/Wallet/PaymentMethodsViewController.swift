import AnalyticsModule
import Card
import CashIn // TODO: REMOVE
import Core
import CoreLegacy
import UI
import UIKit
import FeatureFlag
import SecurityModule
import Statement
import FeatureFlag
import Statement
import LendingHub

@objc
protocol PaymentMethodsViewControllerDelegate: AnyObject {
    func paymentMethodsViewControllerDismissed()
    func paymentMethodsCompletion()
}

protocol RemoteCardTableViewCellDelegate: AnyObject {
    func didTapRemoteCard(in cell: UITableViewCell)
    func closeButtonTapped(in cell: UITableViewCell)
}

extension PaymentMethodsViewControllerDelegate {
    func paymentMethodsCompletion() { }
}

enum HeaderTypeOrigin {
    case seeDetails
    case hiring
}

final class PaymentMethodsViewController: PPBaseViewController {
    typealias Dependencies = HasFeatureManager & HasMainQueue

    @IBOutlet weak var tableView: UITableView!
    @objc weak var delegate: PaymentMethodsViewControllerDelegate?
    @objc var origin: String = ""
    @objc var showCloseButton: Bool = false
    @objc var headerText: String? = nil
    
    lazy var viewModel: PaymentMethodsViewModel = PaymentMethodsViewModel(
        statementEnabledService: StatementEnabledService(dependencies: dependencies),
        consumerApiService: ConsumerApi(),
        dependencies: dependencies
    )
    
    var loadingView: PaymentMethodLoadingView?
    fileprivate var emptyCellHeight: CGFloat = 0
    var bgBouceView: UIView?
    
    var currentPageOfPagination: Int = 0
    var expiredCardAlert: [String: [String]] = [
        "dda" : [PaymentMethodsLocalizable.accountConnectionHasExpired.text, PaymentMethodsLocalizable.increaseYourSecurity.text],
        "credit_card" : [PaymentMethodsLocalizable.cardExpired.text, PaymentMethodsLocalizable.wouldYouLikeRemoveIt.text]]
    
    var isSidebarMenu: Bool = true
    
    var headerView: WalletHeaderViewConfiguration?
    
    var headerPaginationController: ViewPagination?
    
    var shouldShowNavigationBarOnDisappear = true
    var headerClickOrigin: HeaderTypeOrigin?
    private let dependencies: Dependencies = DependencyContainer()
    fileprivate var auth: PPAuth?
    private let notification = NSNotification.Name(rawValue: "WalletNeedReload")
    private var introHeaderView: CPPaymentMethodsIntroView? = nil
    private var maxheightHeader: CGFloat = CGFloat(0)
    private var currentCoordinator: Coordinator?
    private var childCoordinator: [Coordinating] = []
    
    private lazy var footerView: UIView = {
        let footerView = UsePromoCodeView(frame: CGRect(origin: .zero, size: CGSize(width: tableView.frame.width, height: 85)))
        footerView.delegate = self
        
        return footerView
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return viewModel.preferredStatusBarStyle
    }
    
    override func navigationBarIsTransparent() -> Bool {
        return viewModel.navigationBarIsTransparent
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // configure notificaiton observers
        setupObservers()
        
        // configure the table view
        setupTableView()
        
        if showCloseButton {
            let closeButton = UIBarButtonItem(title: DefaultLocalizable.btClose.text, style: .plain, target: self, action: #selector(self.dismissVC))
            navigationItem.setLeftBarButton(closeButton, animated: false)
        }
        
        startLoading()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObserver()
        loadData()
        viewModel.removeWalletNotifications()
        setupBackgroundColorAndScrollIndicator()
        
        if viewModel.flowContext == .payment {
            Analytics.shared.log(CheckoutEvent.checkoutPaymentMethodAccessed)
        }
        
        verifyIsStatementEnabled()
    }
    
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        tableView.reloadData()
    }

    private func loadData() {
        updateBalance()
        loadCards(showLoading: false)

        if viewModel.flowContext == .wallet &&
            dependencies.featureManager.isActive(.opsLoanWalletBanner) {
            viewModel.getPersonalCreditWalletBanner() { [weak self] in
                self?.tableView.reloadData()
            }
        }

        viewModel.getStatusLimitWarning { [weak self] in
            self?.setupHeaderView()
            self?.tableView.reloadData()
        }
        
        if viewModel.creditPicpayIsActived {
            viewModel.loadAccountOfCreditPicpay(onCacheLoaded: { [weak self] in
                self?.setupHeaderView()
                self?.tableView.reloadData()
                }, completion: { [weak self] error in
                    guard error == nil else {
                        self?.showAtm24AlertIfNeeded()
                        return
                    }
                    self?.setupHeaderView()
                    self?.tableView.reloadData()
                })
        } else {
            showAtm24AlertIfNeeded()
        }
    }

    private func showAtm24AlertIfNeeded() {
        if !viewModel.hasShownAtm24OfferPopup && FeatureManager.isActive(.featureCashout24hPopup) {
            viewModel.hasShownAtm24OfferPopup = true
            
            let popup = ATM24OfferPopup(confirmAction: { [weak self] in
                let nav = UINavigationController(rootViewController: WithdrawOptionsFactory.make())
                self?.present(nav, animated: true, completion: nil)
                }, cancelAction: nil)
            
            showPopup(popup)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: Notification.Name.Payment.methodChange, object: nil)
        super.viewWillDisappear(animated)
        delegate?.paymentMethodsCompletion()
        viewModel.eventsWillDisplayCellSent = false
        shouldShowNavigationBarOnDisappear = true
        
        removeObserver()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
        PPAnalytics.trackFirstTimeOnlyEvent("FT Carteira", properties: ["breadcrumb": BreadcrumbManager.shared.breadcrumb])
        sendBadgeDismissEvent()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // Layout for header with description text is already correct
        if headerText != nil {
            return
        }
        
        if let headerView = tableView.tableHeaderView {
            let height = self.maxheightHeader + 20
            var headerFrame = headerPaginationController?.view.frame ?? headerView.frame
            
            if height != headerFrame.size.height {
                headerFrame.size.height = height
                headerView.frame = headerFrame
                tableView.tableHeaderView = headerView
            }
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editCreditCardIdentifier" {
            guard let controller = segue.destination as? CreditCardEditViewController else {
                return
            }
            guard let card: CardBank = sender as? CardBank else {
                return
            }
            controller.viewModel = CreditCardEditViewModel(with: card)
            controller.delegate = self
            navigationController?.hidesBottomBarWhenPushed = true
        }
        if segue.identifier == "addNewCreditCardIdentifier" {
            guard let controller = segue.destination as? AddNewCreditCardViewController else {
                return
            }
            controller.viewModel = AddNewCreditCardViewModel(origin: .wallet)
            controller.delegate = self
            controller.hidesBottomBarWhenPushed = true
            navigationController?.hidesBottomBarWhenPushed = true
            Analytics.shared.log(CheckoutEvent.addNewCardAccessed(viewModel.flowContext))
        }
    }

    //Essa Função vai substituir as duas de baixo no futuro
    private func startLendingHub() {
        guard let navigationController = navigationController else { return }
        guard let banner = viewModel.lendingBanner else { return }
        navigationController.setNavigationBarHidden(false, animated: true)
        navigationController.hidesBottomBarWhenPushed = true
        let coordinator = LendingFlowCoordinator(with: banner, navigationController: navigationController)
        SessionManager.sharedInstance.currentCoordinating = coordinator
        coordinator.start() {
            SessionManager.sharedInstance.currentCoordinating = nil
        }
    }
    
    // MARK: - Setup Methods
    func setup(model: PaymentMethodsViewModel) {
        viewModel = model
    }
    
    func setupObservers(){
        NotificationCenter.default.addObserver(self, selector:#selector(pushReceivedHandler), name: NSNotification.Name("PushReceived"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(balanceChangedHandler), name: NSNotification.Name("balanceChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(requestPhysicalCard), name: Notification.Name.CreditPicPay.requestPhysicalCard, object: nil)
    }
    
    // MARK: - User Actions
    
    fileprivate func showCardVerification(card: CardBank){
        guard let nav = navigationController else {
            return
        }
        CardVerificationHelpers.show(from: nav, withModel: CardVerificationViewModel(with: card))
    }
    
    fileprivate func showCardSettings() {
        guard let navigation = navigationController else {
            return
        }
        
        Analytics.shared.log(CardEvent.didTapWalletCardBottomMenu(action: .ppcardSettings))
        
        let coordinator = CardSettingsFlowCoordinator(navigationController: navigation)
        childCoordinator.append(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }
    
    func setupObserver() {
        NotificationCenter.default.addObserver(forName: notification, object: nil, queue: nil) { _ in
            DispatchQueue.main.async {
                self.loadData()
            }
        }
    }
    
    func removeObserver() {
        NotificationCenter.default.removeObserver(self, name: notification, object: nil)
    }
}

// MARK: - TableViewDataSource

extension PaymentMethodsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Balance PicPay
        if (indexPath.section == viewModel.balanceSection && indexPath.row == 0) {
            return setUpbalanceCell(tableView, at: indexPath)
        }
        // Loan Banner
        if (indexPath.section == viewModel.loanSection) {
            return setUpLoanBannerCell(tableView, at: indexPath)
        }
        
        // Consumer Cards
        if (indexPath.section == viewModel.cardSection) {
            return setUpCardCell(tableView, at: indexPath)
        }
        
        // PicPay Card Offer
        if indexPath.section == viewModel.remoteCardSection {
            return setUpRemoteCardCell(tableView, at: indexPath)
        }
        
        // Generic Cell for prevent error
        let cell: CardTableViewCell = tableView.dequeueReusableCell(type: CardTableViewCell.self, forIndexPath: indexPath)
        return cell
    }
    
    private func setUpbalanceCell(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        let cell: BalancePicPayTableViewCell = tableView.dequeueReusableCell(type: BalancePicPayTableViewCell.self, forIndexPath: indexPath)
        cell.configure(usePicPayBalance: ConsumerManager.useBalance(), showBalance: viewModel.flowContext == .payment, balanceValue: viewModel.balance)
        cell.shouldUsePPBalance.addTarget(self, action: #selector(self.useBalanceSwitch), for: .valueChanged)
        if viewModel.isWalletLayoutV1Enabled {
            cell.configureLayoutV1()
        }
        return cell
    }
    
    private func setUpLoanBannerCell(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        if viewModel.isWalletLayoutV1Enabled {
            let dequeuedCell = tableView.dequeueReusableCell(withIdentifier: WalletLoanOfferCell.identifier)
            return dequeuedCell ?? WalletLoanOfferCell()
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LoanWalletTableViewCell.identifier) as? LoanWalletTableViewCell
        else { return UITableViewCell() }
        cell.configure()
        return cell
    }
    
    private func setUpCardCell(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == viewModel.cards.count {
            if viewModel.cards.isEmpty {
                let cell: EmptyCardsTableViewCell = tableView.dequeueReusableCell(type: EmptyCardsTableViewCell.self, forIndexPath: indexPath)
                if viewModel.isWalletLayoutV1Enabled {
                    cell.configureLayoutV1()
                }
                return cell
            }
            
            // Add cell
            return dequeueAddCardCell(indexPath: indexPath)
        }
        
        // Card cell
        let card = viewModel.card(at: indexPath)
        let cell = dequeueCardCell(indexPath: indexPath)
       
        let hideStatusLabelOnCell = viewModel.flowContext == .payment
        let isDefault = viewModel.isDefault(card: card)
        let isUpdating = viewModel.isUpdating(cardAt: indexPath.row)
        cell.configure(card: card, hideStatus: hideStatusLabelOnCell, isSelected: isDefault, isUpdating: isUpdating)
        
        return cell
    }
    
    private func dequeueCardCell(indexPath: IndexPath) -> WalletCardCellConfigurating {
        if viewModel.isWalletLayoutV1Enabled {
            let newCell = tableView.dequeueReusableCell(withIdentifier: WalletCardCell.identifier) as? WalletCardCellConfigurating
            return newCell ?? WalletCardCell()
        } else {
            return tableView.dequeueReusableCell(type: CardTableViewCell.self, forIndexPath: indexPath)
        }
    }
    
    private func dequeueAddCardCell(indexPath: IndexPath) -> UITableViewCell {
        if viewModel.isWalletLayoutV1Enabled {
            let newCell = tableView.dequeueReusableCell(withIdentifier: WalletAddCardCell.identifier)
            return newCell ?? WalletAddCardCell()
        } else {
            let cell: CardTableViewCell = tableView.dequeueReusableCell(type: CardTableViewCell.self, forIndexPath: indexPath)
            cell.configureAddCell()
            return cell
        }
    }
    
    private func setUpRemoteCardCell(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        let cell: OfferCardTableViewCell = tableView.dequeueReusableCell(type: OfferCardTableViewCell.self, forIndexPath: indexPath)
        cell.card = viewModel.remoteCard(at: indexPath)
        cell.delegate = self
        if viewModel.isWalletLayoutV1Enabled {
            cell.configureLayoutV1()
        }
        return cell
    }
}

// MARK: - TableViewDelegate

extension PaymentMethodsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == viewModel.remoteCardSection {
            let deeplinkURL: URL? = viewModel.deeplinkForRemoteCard(at: indexPath)
            navigationController?.presentationController?.delegate = self
            DeeplinkHelper.handleDeeplink(withUrl: deeplinkURL, from: navigationController)
        }
        
        if indexPath.section == viewModel.loanSection {
            startLendingHub()
        }
        
        if indexPath.section == viewModel.cardSection {
            
            // Check if this card is in updating mode (like: under deleting process)
            let isUpdating = viewModel.isUpdating(cardAt: indexPath.row)
            if indexPath.row < viewModel.cards.count && isUpdating {
                return
            }
            
            // Check if clicked card was expired
            if indexPath.row < viewModel.cards.count && viewModel.cards[indexPath.row].expired {
                let cardType = viewModel.cards[indexPath.row].type
                if let alertText = expiredCardAlert[cardType.rawValue] {
                    
                    DispatchQueue.main.async { [weak self] in
                        let alert = UIAlertController(title: alertText[0], message: alertText[1], preferredStyle: .alert)
                        
                        // Can remove credit card
                        if cardType == .credit {
                            alert.addAction(UIAlertAction(title: DefaultLocalizable.yesRemove.text, style: .default, handler: { action in
                                guard let strongSelf = self else {
                                    return
                                }
                                // Delete expired credit card
                                strongSelf.deleteCard(cardRow: indexPath.row)
                            }))
                        }
                        
                        // DDA Reconect account
                        if cardType == .dda {
                            alert.addAction(UIAlertAction(title: PaymentMethodsLocalizable.reconnectMyAccount.text, style: .default, handler: { action in
                                guard let url = URL(string: WebServiceInterface.apiEndpoint(WsWebViewURL.originalOAuth.endpoint)) else {
                                    return
                                }
                                
                                // Reconect Original account
                                let webview = WebViewFactory.make(with: url, includeHeaders: true) { error in
                                    guard let strongSelf = self else {
                                        return
                                    }
                                    if let error = error {
                                        AlertMessage.showAlert(withMessage: error.localizedDescription, controller: strongSelf)
                                    } else {
                                        self?.tableView.reloadData()
                                    }
                                }
                                self?.present(UINavigationController(rootViewController: webview), animated: true)
                            }))
                        }
                        alert.addAction(UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .default, handler: nil))
                        
                        self?.present(alert, animated: true, completion: nil)
                    }
                }
                return
            }
            
            // Open action sheet
            if indexPath.row < viewModel.cards.count {
                onTapCreditCard(with: indexPath)
            }
            
            // Add a new card
            if indexPath.row == viewModel.cards.count {
                performSegue(withIdentifier: "addNewCreditCardIdentifier", sender: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return viewModel.canEditRow(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .normal, title: DefaultLocalizable.delete.text) { (_, indexPath) in
            self.showPopupDeleteCard(cardRow: indexPath.row)
        }
        deleteAction.backgroundColor = Palette.ppColorNegative300.color
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.heightForRow(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.estimatedHeightForRow(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return viewModel.heightForHeader(in: section)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return viewModel.heightForFooter(in: section)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        viewModel.onDisplayTableViewCell(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard viewModel.haveViewForHeader(in: section) else {
            return nil
        }
        let headerView = UITableViewHeaderFooterView()
        headerView.textLabel?.text = viewModel.headerTitle(for: section)
        if viewModel.isWalletLayoutV1Enabled {
            headerView.textLabel?.textColor = Colors.grayscale700.color
        }
        return headerView
    }
}

extension PaymentMethodsViewController {
    
    fileprivate func onTapCreditCard(with indexPath: IndexPath) {
        let card = viewModel.getCard(in: indexPath.row)
        let needsVerification: Bool = card.verifyStatus == .waitingFillValue || card.verifyStatus == .verifiable
        
        switch viewModel.flowContext  {
        case .wallet:
            if needsVerification && viewModel.actionOfCard(in: indexPath) == .openPopup {
                popupForVerificationStatus(for: card)
            } else {
                showActionSheet(card: card, row: indexPath.row, needsVerification: needsVerification)
            }
        case .payment:
            setDefaultCreditCard(card: card)
        }
    }
    
    fileprivate func sendBadgeDismissEvent() {
        if !tabBarItem.badgeValue.isNilOrEmpty {
            Analytics.shared.log(CardEvent.didTapWalletBadge)
        }
    }
    
    fileprivate func showActionSheet(card: CardBank, row: Int, needsVerification: Bool) {
        let flagType = CreditCardFlagTypeId(rawValue: Int(card.creditCardBandeiraId) ?? 0)
        
        if flagType == .picpay {
            viewModel.trackSelectPicpay()
        }
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let verify = UIAlertAction(title: PaymentMethodsLocalizable.checkCard.text, style: .default, handler: { [weak self] (_) in
            self?.showCardVerification(card: card)
        })
        let setDefault = UIAlertAction(title: PaymentMethodsLocalizable.setAsPrimary.text, style: .default, handler: { [weak self] _ in
            self?.setDefaultCreditCard(card: card)
            let isDefault: CardEvent.CardDefault = flagType == .picpay ? .yes : .no
            Analytics.shared.log(CardEvent.didChangeDefault(option: isDefault))
        })
        
        actionSheet.addAction(setDefault)
        
        if viewModel.isVirtualCardAvailable && flagType == .picpay {
            
            let virtualCard = UIAlertAction(
                title: PaymentMethodsLocalizable.myVirtualCard.text,
                style: .default) { [weak self] _ in
                self?.presentVirtualCard()
            }
            
            actionSheet.addAction(virtualCard)
        }
        
        if flagType == .picpay, viewModel.isPhysicalCardAvailable {
            let creditCard = UIAlertAction(title: PaymentMethodsLocalizable.myPicPayCard.text, style: .default) { [weak self] _ in
                self?.presentCreditHome()
            }
            actionSheet.addAction(creditCard)
            
            let cardSettings = UIAlertAction(title: PaymentMethodsLocalizable.picPayCardSettings.text, style: .default) { [weak self] _ in
                self?.showCardSettings()
            }
            actionSheet.addAction(cardSettings)
        }
        
        if needsVerification {
            actionSheet.addAction(verify)
        }
        
        /// If card is picpay flag
        if flagType != .picpay {
            let edit = UIAlertAction(title: PaymentMethodsLocalizable.editCard.text, style: .default, handler: { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.editCard(in: row)
            })
            let delete = UIAlertAction(title: PaymentMethodsLocalizable.deleteCard.text, style: .destructive, handler: { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.showPopupDeleteCard(cardRow: row)
            })
            actionSheet.addAction(edit)
            actionSheet.addAction(delete)
        }
        
        let cancel = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil)
        actionSheet.addAction(cancel)
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    func setDefaultCreditCard(card: CardBank) {
        guard let cardId = Int(card.id) else {
            return
        }
        CreditCardManager.shared.saveDefaultCreditCardId(cardId)
        Analytics.shared.log(CheckoutEvent.mainCreditCardChanged(viewModel.flowContext))
        tableView.reloadData()
    }
    
    func popupForVerificationStatus(for card: CardBank) {
        showCardVerification(card: card)
    }
    
    func editCard(in row: Int) {
        let card = viewModel.getCard(in: row)
        performSegue(withIdentifier: "editCreditCardIdentifier", sender: card)
    }
    
    private func changeBackgroundBouceViewWithHeader() {
        if let backgroundColor = self.headerPaginationController?.currentChild.backgroundColor {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: [UIView.AnimationOptions.beginFromCurrentState], animations: {
                self.bgBouceView?.backgroundColor = backgroundColor
            })
        }
    }
    
    private func setupHeaderView() {
        viewModel.isWalletLayoutV1Enabled ? setupNewHeaderView() : setupLegacyHeaderView()
    }
    
    private func setupNewHeaderView() {
        headerPaginationController = ViewPagination()
        
        let walletHeaderView = WalletHeaderView()
        let headerSize = CGSize(width: walletHeaderView.computedWidth, height: walletHeaderView.computedHeight)
        walletHeaderView.frame = CGRect(origin: .zero, size: headerSize)
        headerView = walletHeaderView
        
        maxheightHeader = max(maxheightHeader, headerSize.height)
        headerPaginationController?.add(child: walletHeaderView)
        
        setupHeaderCallbacks(header: walletHeaderView)
        updateBalance()
        additionalHeaderConfiguration(for: walletHeaderView)
        
        viewModel.getBankAccountInfo { [weak self] bankInfo in
            self?.headerView?.configure(userBankAccount: bankInfo)
        }
    }
    
    private func setupLegacyHeaderView() {
        headerPaginationController = ViewPagination()
        
        // Balance Header
        let header = PaymentMethodHeaderView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: view.frame.size.width, height: 190)))
        header.sizeHeightToFit()
        maxheightHeader = max(self.maxheightHeader, header.frame.size.height)
        headerPaginationController?.add(child: header)
        headerView = header
        
        setupHeaderCallbacks(header: header)
        updateBalance()
        additionalHeaderConfiguration(for: header)
    }
    
    private func setupHeaderCallbacks(header: WalletHeaderViewConfiguration) {
        header.addAction = { [weak self] in
            self?.presentRecharge()
        }
        
        header.withdrawAction = { [weak self] in
            self?.presentWithdrawal()
        }
        
        header.changeBalanceVisibility = { [weak self] in
            guard let self = self else {
                return
            }
            
            let isBalanceHidden = self.viewModel.toggleBalanceVisibility()
            Analytics.shared.log(WalletEvent.balanceVisibility(isVisible: !isBalanceHidden))
            header.setupBalanceVisibility(isBalanceHidden, animated: true)
        }
        
        header.openSavings = { [weak self] in
            if KVStore().boolFor(.savings_onboarding_saw) {
                self?.presentSavings()
            } else {
                self?.presentSavingsOnboarding()
            }
        }
        
        header.openStatement = { [weak self] in
            self?.presentStatement()
        }
    }
    
    private func additionalHeaderConfiguration(for walletHeaderView: UIView) {
        /// Configuration for credit picpay
        configurationForCreditPicPay(in: walletHeaderView)
        
        /// Configure pagination
        headerPaginationController?.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: self.maxheightHeader)
        
        /// prevent to not reset page of header pagination on apper view
        headerPaginationController?.initialPage = currentPageOfPagination
        headerPaginationController?.setPage(page: currentPageOfPagination, animated: false)
        changeBackgroundBouceViewWithHeader()
        headerPaginationController?.didMove = { [weak self] index in
            guard let strongSelf = self else {
                return
            }
            let creditPicPayPage = 1
            
            strongSelf.introHeaderView?.dismiss()
            strongSelf.viewModel.shouldShowCreditPaymentMethodIntro = false
            
            // set the tableview background same as the current page background
            strongSelf.changeBackgroundBouceViewWithHeader()
            
            // sets the home page as the current page to avoid behavior that changes to the initial page
            // when the PaginationView.viewWillAppear method is called.
            if let currentPage = self?.headerPaginationController?.pageControl.currentPage {
                strongSelf.headerPaginationController?.initialPage = currentPage
                strongSelf.currentPageOfPagination = currentPage
                if currentPage == creditPicPayPage {
                    PPAnalytics.trackEvent("Crédito - oferta carteira Swype left saldo", properties: nil)
                } else {
                    PPAnalytics.trackEvent("Crédito - oferta swype right no Saldo", properties: nil)
                }
            }
        }
        
        // page control style
        if let pageControl = headerPaginationController?.pageControl {
            pageControl.currentPageIndicatorTintColor = .white
            pageControl.pageIndicatorTintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
            headerPaginationController?.view.addSubview(pageControl)
            pageControl.backgroundColor = .clear
            pageControl.snp.makeConstraints({ (make) in
                make.centerX.equalToSuperview()
                make.bottom.equalTo(viewModel.isWalletLayoutV1Enabled ? -8 : 0)
            })
            pageControl.currentPage = currentPageOfPagination
            if pageControl.numberOfPages > 1 {
                headerPaginationController?.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: self.maxheightHeader + 20)
                pageControl.isHidden = false
            } else {
                pageControl.isHidden = true
            }
        }
        if viewModel.flowContext == .wallet {
            guard let headerView = headerPaginationController?.view else {
                return
            }
            tableView.tableHeaderView = headerView
            tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.maxheightHeader + 20)
            view.layoutSubviews()
        } else {
            self.bgBouceView?.backgroundColor = Palette.ppColorGrayscale100.color
        }
        
        if let isStatementEnabled = viewModel.isStatementEnabledCached {
            guard let header = headerView as? PaymentMethodHeaderView else { return }
            header.setupButtons(isStatementEnabled: isStatementEnabled)
        }
    }
    
    private func setupTableView() {
        tableView.contentInset.top = viewModel.tableViewContentInsetTop
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerCell(type: CardTableViewCell.self)
        tableView.registerCell(type: BalancePicPayTableViewCell.self)
        tableView.registerCell(type: EmptyCardsTableViewCell.self)
        tableView.registerCell(type: OfferCardTableViewCell.self)
        tableView.register(cellType: LoanWalletTableViewCell.self)
        
        // Wallet v1 cells
        tableView.register(cellType: WalletCardCell.self)
        tableView.register(cellType: WalletAddCardCell.self)
        tableView.register(cellType: WalletLoanOfferCell.self)
        
        if viewModel.isWalletLayoutV1Enabled {
            tableView.backgroundColor = Colors.backgroundPrimary.color
            tableView.showsVerticalScrollIndicator = false
        } else {
            tableView.backgroundColor = Palette.ppColorGrayscale100.color
        }
        setupBackgroundColorAndScrollIndicator()
        
        // its used to complete the bounce`s area with the header's background color
        var frame = tableView.bounds
        frame.origin.y = -frame.size.height
        frame.size.width = UIScreen.main.bounds.width
        bgBouceView = UIView(frame: frame)
        tableView.addSubview(bgBouceView!)
        
        if viewModel.flowContext == .wallet {
            setupHeaderView()
        } else if headerText != nil {
            setupHeaderText()
        }
        
        setTableFooterView()
    }
    
    private func setupBackgroundColorAndScrollIndicator() {
        if viewModel.isWalletLayoutV1Enabled {
            tableView.backgroundColor = Colors.backgroundPrimary.color
            tableView.showsVerticalScrollIndicator = false
        } else {
            tableView.backgroundColor = Palette.ppColorGrayscale100.color
        }
    }
    
    private func setTableFooterView() {
        tableView.tableFooterView = viewModel.showPromotionalCode ? footerView : nil
    }
    
    private func verifyIsStatementEnabled() {
        viewModel.isStatementEnabled { [weak self] isStatementEnabled in
            guard let header = self?.headerView as? PaymentMethodHeaderView else { return }
            header.setupButtons(isStatementEnabled: isStatementEnabled)
        }
    }
    
    func configurationForCreditPicPay(in header: UIView) {
        if let account = viewModel.accountOfCreditPicpay, CreditPicPayHelper(with: account).isCardWalletEnable {
            var hasCredit = false
            guard account.registrationStatus != .notExist else {
                return
            }
            let approved: Bool = ((account.creditStatus == .onRegistration && account.registrationStatus != .underAnalysis))
            
            if approved || account.registrationStatus == .underAnalysis {
                hasCredit = true
                
                // credit text banner
                let creditBanner = CPPaymentMethodsTextHeaderView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: view.frame.size.width, height: 100)))
                creditBanner.textLabel.text = account.offerText
                creditBanner.textLabel.setNeedsLayout()
                
                if approved || account.registrationStatus == .underAnalysis {
                    creditBanner.button.addTarget(self, action: #selector(presentCreditForm), for: .touchUpInside)
                    if approved {
                        headerClickOrigin = .hiring
                        creditBanner.button.setTitle("\(PaymentMethodsLocalizable.setUpMy.text) \(CreditPicPayLocalizable.featureName.text)", for: .normal)
                    }
                }
                
                if account.registrationStatus == .underAnalysis {
                    headerClickOrigin = .seeDetails
                    creditBanner.button.addTarget(self, action: #selector(presentCreditForm), for: .touchUpInside)
                    creditBanner.button.setTitle(DefaultLocalizable.seeDetails.text, for: .normal)
                }
                
                creditBanner.sizeHeightToFit()
                self.maxheightHeader = max(self.maxheightHeader, creditBanner.frame.size.height)
                headerPaginationController?.add(child: creditBanner)
            }
            
            if CreditPicPayHelper(with: account).isCreditActivatedOrBlocked {
                hasCredit = true
                
                // credit text banner
                let frame = CGRect(origin: CGPoint.zero, size: CGSize(width: view.frame.size.width, height: 100))
                let valueLabel = CurrencyFormatter.brazillianRealString(from: NSNumber(value: account.availableCredit))
                let buttonTitle: String =  DefaultLocalizable.seeDetails.text

                let creditValueBanner = CPPaymentMethodsValueHeaderView(with: valueLabel, buttonTitle: buttonTitle, frame: frame, creditStatus: account.creditStatus)
                creditValueBanner.onTapValue = { [weak self] in
                    self?.presentCreditHome()
                }
                creditValueBanner.onTapButton = { [weak self] in
                    self?.presentCreditHome()
                }
                creditValueBanner.sizeHeightToFit()
                self.maxheightHeader = max(self.maxheightHeader, creditValueBanner.frame.size.height)
                headerPaginationController?.add(child: creditValueBanner)
            }
            
            if hasCredit && viewModel.shouldShowCreditPaymentMethodIntro {
                let headerIntro = CPPaymentMethodsIntroView(frame: header.frame)
                headerView?.addSubview(headerIntro)
                headerIntro.snp.makeConstraints { (make) in
                    make.edges.equalToSuperview()
                }
                introHeaderView = headerIntro
            }
        }
    }
    
    fileprivate func setupHeaderText() {
        let labelPadding: CGFloat = 16
        let labelWidth = UIScreen.main.bounds.width - (2 * labelPadding)
        let label = UILabel(frame: CGRect(x: labelPadding, y: labelPadding, width: labelWidth, height: 0))
        label.text = headerText
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
        label.numberOfLines = 0
        label.preferredMaxLayoutWidth = labelWidth
        label.sizeToFit()
        let labelSize = label.sizeThatFits(CGSize(width: labelWidth, height: 0))
        maxheightHeader = labelSize.height
        let textHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: labelSize.height + labelPadding))
        textHeaderView.backgroundColor = .clear
        textHeaderView.addSubview(label)
        tableView.tableHeaderView = textHeaderView
    }
    
    @objc func balanceChangedHandler() {
        DispatchQueue.main.async {
            self.updateBalance()
        }
    }
    
    func updateBalance() {
        headerView?.configure(balance: viewModel.balance, isLoadingBalance: viewModel.isLoadingBalance, isBalanceHidden: viewModel.isBalanceHidden)
        headerView?.configure(useBalance: ConsumerManager.useBalance(), animated: false)
    }
    
    private var isNotCurrentlyVisible: Bool {
        return !(AppManager.shared.mainScreenCoordinator?.currentController() is PaymentMethodsViewController) || viewIfLoaded?.window == nil
    }
    
    // MARK: - Internal Methods
    
    fileprivate func loadCards(showLoading: Bool = true) {
        tableView.reloadData()
        
        if showLoading {
            startLoading()
        }
        
        viewModel.cardsList(cache: { [weak self] in
            self?.stopLoading()
            self?.tableView.reloadData()
            
            }, completion: { [weak self] (error) in
                self?.stopLoading()
                
                guard let error = error else {
                    self?.tableView.reloadData()
                    return
                }
                
                if self?.isNotCurrentlyVisible ?? true {
                    return
                }
                
                if self?.viewModel.cards.isEmpty == true  {
                    AlertMessage.showCustomAlertWithError(error, controller: self)
                }
        })
    }
    
    fileprivate func showPopupDeleteCard(cardRow: Int) {
        let removeCardQuestion = String(format: EditCreditCardLocalizable.removeCardWithLastDigits.text, viewModel.cards[cardRow].lastDigits)
        let alert: Alert = Alert(title: EditCreditCardLocalizable.removeCard.text, text: removeCardQuestion)
        let okButton = Button(title: EditCreditCardLocalizable.removeCard.text, type: .destructive) { [weak self] popupController, _ in
            self?.deleteCard(cardRow: cardRow)
            popupController.dismiss(animated: true)
        }
        let cancelButton:Button = Button(title: DefaultLocalizable.btCancel.text, type: .clean, action: .close)
        alert.buttons = [okButton, cancelButton]
        AlertMessage.showAlert(alert, controller: navigationController?.tabBarController ?? self)
    }
    
    fileprivate func deleteCard(cardRow: Int) {
        let card = viewModel.cards[cardRow]
        deleteCard(card)
    }
    
    fileprivate func deleteCard(_ card: CardBank) {
        viewModel.deleteCard(card, addedCardToDelete: { [weak self] in
            self?.tableView.reloadData()
            }, completion: { [weak self] error in
                guard let strongSelf = self else {
                    return
                }
                if let error = error {
                    AlertMessage.showCustomAlertWithError(error, controller: strongSelf)
                } else {
                    strongSelf.tableView.reloadData()
                    strongSelf.loadCards(showLoading: false)
                }
        })
    }
    
    @objc func dismissVC() {
        dismiss(animated: true, completion: nil)
        delegate?.paymentMethodsViewControllerDismissed()
    }
    
    // MARK: - Loading Methods
    
    func startLoading() {
        if loadingView == nil {
            loadingView = PaymentMethodLoadingView(frame: view.frame)
            loadingView?.configure(withHeader: viewModel.flowContext == .payment)
            view.addSubview(loadingView!)
            loadingView?.snp.makeConstraints({ (make) in
                make.edges.equalToSuperview()
            })
        }
    }
    
    func stopLoading() {
        UIView.animate(withDuration: 0.25, animations: {
            self.loadingView?.layer.opacity = 0.0
        }) { (complete) in
            self.loadingView?.removeFromSuperview()
            self.loadingView = nil
        }
    }
    
    @objc func useBalanceSwitch(button: UISwitch) {
        ConsumerManager.setUseBalance(button.isOn)
        
        Analytics.shared.log(WalletEvent.paymentChanged(type: button.isOn ? .balance : .creditCard))
        Analytics.shared.log(CheckoutEvent.useBalanceChanged(button.isOn ? .optin : .optout, viewModel.flowContext)
        )
        
        if viewModel.flowContext == .wallet {
            headerView?.configure(useBalance: button.isOn, animated: true)
        }
        
        if let currentHeader = headerPaginationController?.currentChild as? WalletHeaderViewConfiguration {
            bgBouceView?.backgroundColor = currentHeader.backgroundColor
        }
    }
    
    //MARK: - User Actions
    private func presentSavings(animated: Bool = true) {
        shouldShowNavigationBarOnDisappear = false
        let vc = SavingsViewController()
        let navController = PPNavigationController(rootViewController: vc)
        navController.setNavigationBarHidden(true, animated: false)
        present(navController, animated: true, completion: nil)
    }
    
    private func presentStatement() {
        let statementController = StatementContainerFactory.make()
        let navigationController = UINavigationController(rootViewController: statementController)
        present(navigationController, animated: true)
    }
    
    private func presentSavingsOnboarding() {
        shouldShowNavigationBarOnDisappear = false
        let vc = SavingsOnboardingViewController.instantiateFromNib()
        vc.didTapSeeNow = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            strongSelf.presentSavings(animated: false)
        }
        let navController = PPNavigationController(rootViewController: vc)
        navController.setNavigationBarHidden(true, animated: false)
        present(navController, animated: true, completion: nil)
    }
    
    func presentWithdrawal() {
        let nav = UINavigationController(rootViewController: WithdrawOptionsFactory.make())
        present(nav, animated: true, completion: nil)
    }
    
    func presentRecharge() {
        shouldShowNavigationBarOnDisappear = false
        let controller = RechargeLoadFactory.make()
        let navigation = PPNavigationController(rootViewController: controller)
        present(navigation, animated: true)
    }
    
    @objc
    func requestPhysicalCard() {
        guard let url = URL(string: "picpay://picpay/card/credit/request") else { return }
        DeeplinkHelper.handleDeeplink(withUrl: url)
    }
    
    func presentCreditHome() {
        guard let account = viewModel.accountOfCreditPicpay,
            let navigationController = self.navigationController else {
                return
        }
        shouldShowNavigationBarOnDisappear = false
        Analytics.shared.log(CardEvent.didTapWalletCardBottomMenu(action: .ppcardHome))
        
        let creditCoordinator = CreditPicPayCoordinator(navigationController: navigationController, account: account)
        currentCoordinator = creditCoordinator
        creditCoordinator.start()
    }
    
    private func presentVirtualCard() {
        guard let virtualCardButton = viewModel.accountOfCreditPicpay?.settings.virtualCardButton,
              let navigationController = self.navigationController else {
            return
        }
        
        let input = VirtualCardFlowInput(button: virtualCardButton)
        let virtualCardCoordinator = VirtualCardFlowCoordinator(navigation: navigationController, input: input)
        virtualCardCoordinator.start()
    }
    
    @objc
    func presentCreditForm() {
        if headerClickOrigin == .hiring {
            Analytics.shared.log(CardEvent.didTapWalletRequestCard(action: .configCard))
        } else if headerClickOrigin == .seeDetails {
            Analytics.shared.log(CardEvent.didTapWalletCardSignUpSeeDetails)
        }
        let backButton = UIBarButtonItem(title: PaymentMethodsLocalizable.comeBack.text, style: .plain, target: self, action: nil)
        navigationItem.backBarButtonItem = backButton
        shouldShowNavigationBarOnDisappear = true
        let account = self.viewModel.accountOfCreditPicpay
        let viewModel: CreditViewModel = CreditViewModel(with: account)
        let controller: CreditViewController = CreditViewController(with: viewModel)
        let navigationController = PPNavigationController(rootViewController: controller)
        navigationController.setNavigationBarHidden(true, animated: false)
        navigationController.presentationController?.delegate = self
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    private func presentPromoCodeViewController() {
        let viewController = PromoCodeViewController(from: .wallet)
        let navController = DismissibleNavigationViewController(rootViewController: viewController)
        present(navController, animated: true)
    }
    
    //MARK: - Observables
    
    @objc func pushReceivedHandler() {
        viewModel.loadConsumerBalance()
    }
}

extension PaymentMethodsViewController: AddNewCreditCardDelegate {    
    func creditCardDidInsert() {
        self.tableView.setEditing(false, animated: false)
        self.tableView.reloadData()
    }
}

extension PaymentMethodsViewController: CreditCardEditDelegate {
    
    func onChangeAddressOfCard() {
        loadCards()
    }
    
    func deleteCreditCard(_ card: CardBank) {
        self.deleteCard(card)
    }
}

extension PaymentMethodsViewController: RemoteCardTableViewCellDelegate {
    func closeButtonTapped(in cell: UITableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        viewModel.dismissOfferCard(at: indexPath)
        tableView.reloadData()
    }
    
    func didTapRemoteCard(in cell: UITableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell),
            let deeplink = viewModel.deeplinkForRemoteCard(at: indexPath, fromButton: true) else {
                return
        }
        DeeplinkHelper.handleDeeplink(withUrl: deeplink, from: navigationController)
        analytics(indexPath: indexPath)
    }
    
    func analytics(indexPath: IndexPath) {
        guard let rules = viewModel.remoteCard(at: indexPath)?.rules else {
            return
        }
        if rules.contains(.cardEnableForRequestPhysycalCard) || rules.contains(.picpayDebitEnableForRegistration) {
            Analytics.shared.log(CardEvent.didTapRequestOffer)
        }
        if rules.contains(.cardEnableForActivateDebit) {
            Analytics.shared.log(CardEvent.didTapActivatedCardOffer)
        }
        if rules.contains(.cardEnableForRegistration) || rules.contains(.cardDebitEnableForRegistration) {
            Analytics.shared.log(UnblockCardEvent.walletOfferClick)
        }
        if rules.contains(.cardEnableForDebitRequestInProgress) {
            Analytics.shared.log(DebitActivationEvent.dropOffContinue)
        }
        if rules.contains(.cardEnableForTracking) {
            Analytics.shared.log(CardTrackingEvent.didOfferClicked)
        }
    }
}

extension PaymentMethodsViewController: UsePromoCodeDelegate {
    func didTapUsePromoCode() {
        presentPromoCodeViewController()
    }
}

// MARK: - CardSettingsFlowDelegate

extension PaymentMethodsViewController: CardSettingsFlowDelegate {
    func didOpenDueDayCard(from navigationController: UINavigationController) {
        guard dependencies.featureManager.isActive(.experimentCardDueDayPresentingBool) else {
            guard let account = viewModel.accountOfCreditPicpay else { return }
            let coordinator = CPDueDayCoordinator(with: navigationController, account: account)
            coordinator.start()
            return
        }
        let dueDayController = DueDayFactory.make(delegate: self)
        navigationController.pushViewController(dueDayController, animated: true)
    }
    
    func didOpenLimitCard(from navigationController: UINavigationController, didUpdateCardLimit: @escaping () -> Void) {
        guard dependencies.featureManager.isActive(.experimentCardCreditLimitPresentingBool) else {
            let coordinator = CreditLimitCoordinator(from: navigationController)
            coordinator.didUpdateCardLimit = didUpdateCardLimit
            currentCoordinator = coordinator
            coordinator.start()
            return
        }
        let coordinator = LimitAdjustmentFlowCoordinator(navigationController: navigationController)
        coordinator.start()
    }
    
    func didAskAuthentication(onSuccess: @escaping (String) -> Void) {
        auth = PPAuth.authenticate({ token, _ in
            guard let password = token else {
                return
            }
            onSuccess(password)
        }, canceledByUserBlock: { })
    }
}

extension PaymentMethodsViewController: UIAdaptivePresentationControllerDelegate {
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        loadData()
    }
}

extension PaymentMethodsViewController: DueDayCoordinatingDelegate {
    func didConfirmDueDay(cardForm: CardForm?, bestPurchaseDay: Int?) {
        navigationController?.popViewController(animated: true)
    }
}
