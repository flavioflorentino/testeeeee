import Advertising
import AnalyticsModule
import Card
import Core
import LoanOffer
import LendingHub
import Statement
import FeatureFlag

enum PaymentMethodsContext: String {
    case payment, wallet
    
    var text: String {
        if case .payment = self {
            return "checkout"
        }
        return self.rawValue
    }
}

final class PaymentMethodsViewModel {
    typealias Dependencies = HasFeatureManager & HasMainQueue
    private let dependencies: Dependencies
    
    private var offerCards: [OfferCard] = []
    private var offerCardsValidation: OfferCardsValidation {
        return OfferCardsValidation(creditAccount: accountOfCreditPicpay, accountInfo: accountInfo)
    }
    private let offerCardsHelper = OfferCardsHelper()
    private var updatingCardsIndex: [Int] = []
    
    let balanceSection: Int = 0
    let loanSection: Int = 1
    var cardSection: Int = 2
    var remoteCardSection: Int = 3
    var shouldShowPicPayBalanceSwitch: Bool = true
    var flowContext: PaymentMethodsContext = .payment
    private var walletOfferCardsWorker: WalletOfferCardsWorker?
    private var apiPaymentMethod: WalletApiProtocol
    private let statementEnabledService: StatementEnabledServicing
    private let consumerApiService: ConsumerApiProtocol
    var cards: [CardBank] = []
    private var useCache: Bool = true
    var showPromotionalCode: Bool {
        return flowContext == .wallet
    }
    private var hasPersonalCredit: Bool = false
    var lendingBanner: LoanWalletBanner?
    
    private var sectionsTitle: [String] {
        var sections: [String] = [String]()
        sections.append(PaymentMethodsLocalizable.balanceInPortfolio.text)
        sections.append(Strings.LoanWallet.loan)
        if let walletOfferCardsWorker = walletOfferCardsWorker, !offerCards.isEmpty {
            sections.append(walletOfferCardsWorker.headerTitle)
        }
        sections.append(PaymentMethodsLocalizable.paymentMethods.text)
        return sections
    }
    var eventsWillDisplayCellSent: Bool = false
    var balance: NSDecimalNumber {
        return ConsumerManager.shared.consumer?.balance ?? 0.0
    }
    var preferredStatusBarStyle: UIStatusBarStyle {
        if !isWalletLayoutV1Enabled, flowContext == .wallet {
            return .lightContent
        }
        return .default
    }
    var navigationBarIsTransparent: Bool {
        return flowContext == .wallet
    }
    var creditPicpayIsActived: Bool {
        return CreditPicPayHelper().isEnableCreditPicPay
    }
    var tableViewContentInsetTop: CGFloat {
        guard flowContext == .wallet else { return 0.0 }
        var statusBar: CGFloat = 22.0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top
            statusBar = topPadding ?? 0.0
        }
        return -statusBar
    }
    var accountOfCreditPicpay: CPAccount?
    var shouldShowCreditPaymentMethodIntro: Bool {
        set (value) {
            CreditPicPayHelper().shouldShowCreditPicPayOfferInPaymentMethodIntro = value
        }
        get {
            return CreditPicPayHelper().shouldShowCreditPicPayOfferInPaymentMethodIntro
        }
    }
    
    var hasShownAtm24OfferPopup: Bool {
        set(value){
            ATM24Helper().hasPresentedAtm24OfferAlert = value
        } get {
            return ATM24Helper().hasPresentedAtm24OfferAlert
        }
    }
    
    var isBalanceHidden: Bool {
        return ConsumerManager.shared.isBalanceHidden
    }
    var isLoadingBalance = true
    
    var isPhysicalCardAvailable: Bool {
        return accountOfCreditPicpay?.settings.physicalCardConfigurationEnabled ?? false
    }
    
    var isVirtualCardAvailable: Bool {
        guard let virtualCardAction = accountOfCreditPicpay?.settings.virtualCardButton.action else {
            return false
        }
        return virtualCardAction != .none
    }
    
    var isWalletLayoutV1Enabled: Bool {
        dependencies.featureManager.isActive(.experimentWalletLayoutV1Available)
    }
    
    private var accountInfo = DigitalAccountInfo(offerUpgradeAccount: .none, isJudiciallyBlocked: false)
    private (set) var isStatementEnabledCached: Bool?
    private var bankAccount: SettingsBankInfo?
    
    init(
        apiPaymentMethod: WalletApiProtocol = ApiPaymentMethods(),
        statementEnabledService: StatementEnabledServicing,
        consumerApiService: ConsumerApiProtocol,
        dependencies: Dependencies
    ) {
        self.apiPaymentMethod = apiPaymentMethod
        self.statementEnabledService = statementEnabledService
        self.consumerApiService = consumerApiService
        self.dependencies = dependencies
    }
    
    func cardsList(cache: @escaping () -> Void, completion: @escaping (Error?) -> Void) {
        apiPaymentMethod.cardsList(onCacheLoaded: { [weak self] (value) in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async {
                if self.useCache {
                    self.checkStatusAndUpdateStatus(for: value.paymentMethods)
                    self.cards = value.paymentMethods
                    self.filterByNoDebitCard()
                    cache()
                }
            }
        }, completion: { [weak self] (result) in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                switch result {
                case .success(let value):
                    
                    let balanceDecimalNumber = NSDecimalNumber(string: value.balanceValue)
                    if balanceDecimalNumber != NSDecimalNumber.notANumber  {
                        ConsumerManager.shared.setConsumerBalance(balanceDecimalNumber)
                    } else {
                        self.loadConsumerBalance()
                    }
                    
                    CreditCardManager.shared.cardList = value.paymentMethods
                    CreditCardManager.shared.updateDefaultCreditCardIfNil()
                    
                    self.cards.removeAll()
                    self.checkStatusAndUpdateStatus(for: value.paymentMethods)
                    self.cards = value.paymentMethods
                    self.filterByNoDebitCard()
                    self.isLoadingBalance = false
                    self.useCache = false
                    completion(nil)
                    break
                case .failure(let error):
                    completion(error)
                    break
                }
            }
        })
    }
    
    private func filterByNoDebitCard() {
        cards = cards.filter { $0.type != .debit }
    }
    
    func loadConsumerBalance() {
        ConsumerManager.shared.loadConsumerBalance()
    }
    
    func cardDelete(cardId: String, completion: @escaping ((Error?) -> Void)) {
        apiPaymentMethod.cardDelete(card: cardId) { [weak self] (result) in
            DispatchQueue.main.async {
                self?.updatingCardsIndex = []
                switch result {
                case .success( _):
                    
                    // check if the deleted credit card is the current default credit card
                    let currentCreditCardId = String(describing: CreditCardManager.shared.defaultCreditCardId)
                    if currentCreditCardId == cardId {
                        CreditCardManager.shared.saveDefaultCreditCardId(0)
                        if let first = self?.cards.first(where: { (cardBank) -> Bool in return cardBank.id != currentCreditCardId }), let cardId = Int(first.id) {
                            CreditCardManager.shared.saveDefaultCreditCardId(cardId)
                        }
                    }
                    
                    // force to reload all consumer data
                    DispatchQueue.global().async {
                        CreditCardManager.shared.loadCardList({ (_) in })
                    }
                    
                    completion(nil)
                    break
                case .failure(let error):
                    completion(error)
                    break
                }
            }
        }
    }
    
    func isUpdating(cardAt index: Int) -> Bool {
        return updatingCardsIndex.contains(index)
    }
    
    func isDefault(card: CardBank) -> Bool {
        if CreditCardManager.shared.defaultCreditCardId == Int(card.id) {
            return true
        }
        return false
    }
    
    func deleteCard(_ card: CardBank, addedCardToDelete: @escaping () -> Void, completion: @escaping (Error?) -> Void) {
        guard let index = cards.firstIndex(of: card) else {
            return
        }
        updatingCardsIndex.append(index)
        addedCardToDelete()
        
        cardDelete(cardId: card.id, completion: completion)
    }
    
    func getCard(in row: Int) -> CardBank {
        return cards[row]
    }
    
    func onDisplayTableViewCell(at indexPath: IndexPath) {
        guard remoteCardSection == indexPath.section,
              let offerCard = remoteCard(at: indexPath),
              let eventOnDisplay = offerCard.eventOnDisplay,
              !eventsWillDisplayCellSent
        else {
            return
        }
        
        PPAnalytics.trackEvent(eventOnDisplay, properties: nil)
        eventsWillDisplayCellSent = true
    }
    
    func toggleBalanceVisibility() -> Bool {
        return ConsumerManager.shared.toggleBalanceVisibility()
    }
    
    func isStatementEnabled(completion: @escaping (Bool) -> Void) {
        if let isStatementEnabled = isStatementEnabledCached {
            completion(isStatementEnabled)
            return
        }
        
        guard dependencies.featureManager.isActive(.experimentWalletHeaderLayoutV2PresentingBool) else {
            completion(false)
            isStatementEnabledCached = false
            return
        }
        
        statementEnabledService.isStatementEnabled { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
            case .success(let isStatementEnabled):
                self.isStatementEnabledCached = isStatementEnabled
                completion(isStatementEnabled)
            case .failure:
                self.isStatementEnabledCached = false
                completion(false)
            }
        }
    }
}

extension PaymentMethodsViewModel {
    // MARK: - Table view methods
    func card(at indexPath: IndexPath) -> CardBank {
        return cards[indexPath.row]
    }
    
    func deeplinkForRemoteCard(at indexPath: IndexPath, fromButton: Bool = false) -> URL? {
        guard offerCards.indices.contains(indexPath.row) else {
            return nil
        }
        let card = offerCards[indexPath.row]
        guard let deeplinkURL: URL = URL(string: card.action?.deeplink ?? "") else {
            return nil
        }
        return deeplinkURL
    }
    
    func remoteCard(at indexPath: IndexPath) -> OfferCard? {
        guard offerCards.indices.contains(indexPath.row) else {
            return nil
        }
        return offerCards[indexPath.row]
    }
    
    func numberOfSections() -> Int {
        let maxSections: Int = offerCards.isEmpty ? 3 : 4
        return maxSections
    }
    
    func numberOfRows(in section: Int) -> Int {
        switch section {
        case cardSection:
            return cards.count + 1
        case balanceSection:
            return shouldShowPicPayBalanceSwitch ? 1 : 0
        case remoteCardSection:
            return offerCards.count
        case loanSection:
            return hasPersonalCredit ? 1 : 0
        default:
            return 0
        }
    }
    
    func estimatedHeightForRow(at indexPath: IndexPath) -> CGFloat {
        let heightCardSection: CGFloat = 206.0
        let defaultHeight: CGFloat = 50.0
        if cards.count == 0 && indexPath.section == cardSection {
            return heightCardSection
        }
        return defaultHeight
    }
    
    func heightForRow(at indexPath: IndexPath) -> CGFloat {
        let defaultHeight: CGFloat = 50.0
        let dinamicHeight: CGFloat = UITableView.automaticDimension
        if (indexPath.section == balanceSection && indexPath.row == 0) {
            return defaultHeight
        }
        if cards.isEmpty && indexPath.section == cardSection {
            return dinamicHeight
        }
        return dinamicHeight
    }
    
    func heightForHeader(in section: Int) -> CGFloat {
        let defaultHeight: CGFloat = 50.0
        if section == loanSection && (!hasPersonalCredit) {
            return 0.01
        }
        if section == balanceSection && flowContext == .wallet {
            return CGFloat.leastNonzeroMagnitude
        }
        return defaultHeight
    }
    
    func heightForFooter(in section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func haveViewForHeader(in section: Int) -> Bool {
        if section == balanceSection && flowContext == .wallet {
            return false
        }
        return true
    }
    
    func headerTitle(for section: Int) -> String {
        if section == loanSection && (!hasPersonalCredit) {
            return ""
        }
        if section == balanceSection && !shouldShowPicPayBalanceSwitch {
            return ""
        }
        
        return sectionsTitle[section].uppercased()
    }
    
    func canEditRow(at indexPath: IndexPath) -> Bool {
        let isValidSection = indexPath.section == cardSection
        let containIndice = cards.indices.contains(indexPath.row)
        guard isValidSection && containIndice else {
            return false
        }
        let card = cards[indexPath.row]
        let creditPicpayAlias = "picpay"
        if card.alias == creditPicpayAlias {
            return false
        }
        
        return !updatingCardsIndex.contains(indexPath.row)
    }
}

extension PaymentMethodsViewModel {
    // MARK: - Remote cards
    func setUpRemoteCards() {
        cardSection = 2
        remoteCardSection = 3
        guard flowContext == .wallet else {
            return
        }
        guard let walletOfferCardsWorker = WalletOfferCardsWorker() else {
            return
        }
        self.walletOfferCardsWorker = walletOfferCardsWorker
        
        let cardsToShow = walletOfferCardsWorker.cards.filter {
            offerCardsHelper.showed($0)
        }
        let cardsGranted = cardsToShow.filter { (offerCard) -> Bool in
            if let rules = offerCard.rules {
                return offerCardsValidation.isGranted(for: rules)
            }
            return true
        }
        
        offerCards = cardsGranted
        guard !cardsGranted.isEmpty else {
            return
        }
        remoteCardSection = 2
        cardSection = 3
    }
    
    func dismissOfferCard(at indexPath: IndexPath) {
        guard offerCards.indices.contains(indexPath.row) else {
            return
        }
        let offerCard = offerCards[indexPath.row]
        offerCardsHelper.close(offerCard)
        offerCards.remove(at: indexPath.row)
        setUpRemoteCards()
    }
    
    func trackSelectPicpay() {
        Analytics.shared.log(CardEvent.didTapWalletRequestCard(action: .selectPicPay))
    }
}

extension PaymentMethodsViewModel {
    // MARK: - User default manager for status cards
    func actionOfCard(in indexPath: IndexPath) -> ActionVerificationStatus {
        let card: CardBank = cards[indexPath.row]
        
        guard card.verifyStatus == .waitingFillValue || card.verifyStatus == .verifiable else {
            if cardActionExist(for: card) {
                /// if exist card in user defaults, and status of verification is changed for not verification, remove this action status
                removeCardOfUserDefaults(card)
            }
            return .noAction
        }
        
        guard cardActionExist(for: card) else {
            /// If not exist action status, add new actions status in user defaults and return to open popup
            addAction(for: card)
            return .openPopup
        }
        
        /// check action for status action
        if let cardsStatus = KVStore().arrayFor(.cardsStatus) as? [[String: Any]] {
            for dict in cardsStatus {
                guard let id = dict["id"] as? String, id == card.id else { continue }
                guard let actionStatusString = dict["actionStatus"] as? String else { continue }
                guard let action = ActionVerificationStatus(rawValue: actionStatusString) else { continue }
                
                /// update actions status if necessari
                updateAction(for: card)
                
                /// return action based in actual status
                switch action {
                case .noAction:
                    return .openPopup
                case .openPopup:
                    return .openActionSheet
                default:
                    return .openActionSheet
                }
            }
        }
        
        return .noAction
    }
    
    /// Update action in user default
    ///
    /// - Parameter card: credit card
    private func updateAction(for card: CardBank) {
        guard var cards = KVStore().arrayFor(.cardsStatus) as? [[String: Any]] else {
            return
        }
        for (index, dict) in cards.enumerated() {
            guard let id = dict["id"] as? String, id == card.id else { continue }
            guard let actionStatusString = dict["actionStatus"] as? String else { continue }
            guard let action = ActionVerificationStatus(rawValue: actionStatusString) else { continue }
            
            switch action {
            case .noAction:
                var auxDict: [String: Any] = dict
                auxDict["actionStatus"] = ActionVerificationStatus.openPopup.rawValue
                cards[index] = auxDict
                break
            case .openPopup:
                var auxDict: [String: Any] = dict
                auxDict["actionStatus"] = ActionVerificationStatus.openActionSheet.rawValue
                cards[index] = auxDict
            default:
                break
            }
            break
        }
        
        KVStore().set(value: cards, with: .cardsStatus)
    }
    
    /// Remove cards of userdefaults
    ///
    /// - Parameter card: card
    private func removeCardOfUserDefaults(_ card: CardBank) {
        guard var cards = KVStore().arrayFor(.cardsStatus) as? [[String: Any]] else {
            return
        }
        for (index, dict) in cards.enumerated() {
            guard let id = dict["id"] as? String, id == card.id else { continue }
            cards.remove(at: index)
            break
        }
        
        KVStore().set(value: cards, with: .cardsStatus)
    }
    
    private func checkStatusAndUpdateStatus(for cards: [CardBank]) {
        for card in cards {
            if card.verifyStatus == .verified && cardActionExist(for: card) {
                removeCardOfUserDefaults(card)
            }
        }
    }
    
    /// Add new action in user defaults
    ///
    /// - Parameter card: credit card
    private func addAction(for card: CardBank) {

        var dict: [String: Any] = [:]
        
        dict["actionStatus"] = ActionVerificationStatus.openPopup.rawValue
        dict["id"] = card.id
        
        var cards: [[String: Any]] = KVStore().arrayFor(.cardsStatus) as? [[String: Any]] ?? []
        cards.append(dict)
        
        KVStore().set(value: cards, with: .cardsStatus)
    }
    
    /// Check exist cards status in user defaults
    ///
    /// - Parameter card: card
    /// - Returns: exist or not in user defaults
    private func cardActionExist(for card: CardBank) -> Bool {
        guard let cards = KVStore().arrayFor(.cardsStatus) as? [[String: Any]] else {
            return false
        }
        for dict in cards {
            if let id = dict["id"] as? String, id == card.id {
                return true
            }
        }
        
        return false
    }

    func loadAccountOfCreditPicpay(onCacheLoaded: @escaping () -> Void, completion: @escaping (Error?) -> Void) {
        let api = CreditPicpayApi()
        api.account(onCacheLoaded: { [weak self] account in
            guard let strongSelf = self else {
            return
        }
            if strongSelf.accountOfCreditPicpay == nil {
                strongSelf.accountOfCreditPicpay = account
            }
            strongSelf.setUpRemoteCards()
            onCacheLoaded()
        }, completion: { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            switch result {
            case .success(let account):
                strongSelf.accountOfCreditPicpay = account
                NotificationCenter.default.post(name: Notification.Name.CreditPicPay.accountLoaded, object: nil)
                strongSelf.setUpRemoteCards()
                completion(nil)
            case .failure(let error):
                completion(error)
                strongSelf.setUpRemoteCards()
            }
        })
    }
    
    func getStatusLimitWarning(completion: @escaping () -> Void) {
        let worker = UpgradeAccountWorker()
        worker.digitalAccountInfo { [weak self] (result) in
            guard let self = self else {
                return
            }
            if case .success(let value) = result {
                self.accountInfo = value
            }
            self.setUpRemoteCards()
            completion()
        }
    }

    func getPersonalCreditWalletBanner(completion: @escaping () -> Void) {
        let service = LoanWalletBannerService(dependencies: DependencyContainer())
        service.getWalletBanner { [weak self] result in
            guard let self = self else { return }
            if case .success(let banner) = result {
                self.lendingBanner = banner
                self.hasPersonalCredit = self.handleWalletBannerResult(banner)
                self.trackPersonalCreditWalletBanner()
            } else {
                self.hasPersonalCredit = false
            }
            completion()
        }
    }
    
    func getBankAccountInfo(completion: @escaping (SettingsBankInfo?) -> Void) {
        if let bankAccount = bankAccount {
            completion(bankAccount)
            return
        }
        
        consumerApiService.fetchBankInfo { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case let .success(bankInfo):
                    self?.bankAccount = bankInfo
                    completion(bankInfo)
                    
                case .failure:
                    completion(nil)
                }
            }
        }
    }

    private func handleWalletBannerResult(_ banner: LoanWalletBanner) -> Bool {
        let hasP2PLending = banner.hasP2POffer && dependencies.featureManager.isActive(.releaseP2PLending)
        return hasP2PLending || banner.type != .noOffer
    }
    
    private func trackPersonalCreditWalletBanner() {
        guard hasPersonalCredit else { return }
        Analytics.shared.log(LendingHubEvent.didSeeHubAccess(origin: .wallet))
    }
}

extension PaymentMethodsViewModel {
    enum ActionVerificationStatus: String {
        case openPopup = "openPopup"
        case openActionSheet = "openActionSheet"
        case noAction = ""
    }
}

extension PaymentMethodsViewModel {
    func removeWalletNotifications() {
        guard AppManager.shared.unreadWalletNotificationsLabel.isNotEmpty else {
            return
        }

        let service = AdvertisingService()
        service.delBadge(key: .wallet) { result in
            if case .success = result {
                AppManager.shared.setUnreadWalletNotificationsLabel(label: "")
            }
        }
    }
}
