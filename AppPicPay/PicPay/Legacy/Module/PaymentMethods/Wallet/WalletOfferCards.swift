import Foundation

struct WalletOfferCards: Codable {
    let headerTitle: String
    let items: [OfferCard]
}
