final class CreditCardVerificationAlertHelper {
    let parentController: UIViewController
    let card: CardBank

    init(whit parentController: UIViewController, and card: CardBank) {
        self.parentController = parentController
        self.card = card
    }
    
    func showPopupForVerificationStatus(onTappedInVerify: @escaping () -> Void) {
        let alert: Alert = Alert(
            title: DefaultLocalizable.checkYourCard.text,
            text: DefaultLocalizable.paymentsWithUnverifiedCards.text
        )
        let confirmButton = Button(title: DefaultLocalizable.checkNow.text, type: .cta) { popupController, _ in
            popupController.dismiss(animated: true, completion: {
                onTappedInVerify()
            })
        }
        let cancelButton = Button(title: DefaultLocalizable.checkLater.text, type: .clean, action: .close)
        alert.image = Alert.Image(name: "creditCardValidationOnboarding", style: .standard, source: .asset)
        alert.buttons = [confirmButton, cancelButton]
        AlertMessage.showAlert(alert, controller: parentController)
    }
}
