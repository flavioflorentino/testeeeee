//
//  CreditCardFlagTypeId.swift
//  PicPay
//
//  Created by Lucas Romano on 10/09/2018.
//

import Foundation

enum CreditCardFlagTypeId: Int {
    case visa = 1
    case master = 2
    case amex = 4
    case elo = 5
    case hipercard = 6
    case picpay = 7
    case unknown = 0
}
