import PCI
import SwiftyJSON

protocol CardVerificationViewModelDisplay: AnyObject {
    func cvvFlowCoordinator(completedCvv: @escaping (String) -> Void, completedNoCvv: @escaping () -> Void)
}

final class CardVerificationViewModel {
    private let api: VerifyCreditCardProtocol
    private let pciService: VerifyCardServing
    
    var card: CardBank
    var verificationStatus: CardVerificationStatus?
    var isHiddenValueTextField: Bool = true
    
    var willFinishVerification: ((UINavigationController) -> Bool)?
    weak var viewController: CardVerificationViewModelDisplay?
    
    init(
        with card: CardBank,
        service: VerifyCreditCardProtocol = ApiPaymentMethods(),
        pciService: VerifyCardServing = VerifyCardService()
    ) {
        self.card = card
        self.api = service
        self.pciService = pciService
    }
    
    /// Send verification with value of payed in credit card
    ///
    /// - Parameters:
    ///   - value: value of payed in credit card
    ///   - completion: completion handle
    func sendVerfication(with value: Double, _ completion: @escaping (Error?) -> Void) {
        api.verify(id: card.id, value: value) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(_):
                    completion(nil)
                    break;
                case .failure(let error):
                    completion(error)
                    break
                }
            }
        }
    }
    
    /// Check the status for configure information em view
    ///
    /// - Parameter completion: completion handle
    func verificationStatus(_ completion: @escaping (Error?) -> Void) {
        
        if card.verifyStatus != .notAvailable {
            verifyStatus(completion)
        } else {
            isHiddenValueTextField = false
            completion(PicPayError(message: PaymentMethodsLocalizable.weHadProblem.text))
        }
    }
    
    private func verifyStatus(_ completion: @escaping (Error?) -> Void) {
        api.verifyStatus(id: card.id) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    strongSelf.verificationStatus = response.verificationStatus
                    strongSelf.isHiddenValueTextField = response.verificationStatus.status != .waitingFillValue
                    completion(nil)
                    break
                case .failure(let error):
                    completion(error)
                    break
                }
            }
        }
    }
    
    func startVerification(onSuccess: @escaping () -> Void, onError: @escaping (PicPayError) -> Void) {
        guard checkNeedCvv() else {
            verifyStatusStartTransaction(onSuccess: onSuccess, onError: onError)
            return
        }
        
        viewController?.cvvFlowCoordinator(completedCvv: { [weak self] cvv in
            self?.verifyStatusStartTransaction(informedCvv: cvv, onSuccess: onSuccess, onError: onError)
        }, completedNoCvv: {
            let picpayError = PicPayError(message: DefaultLocalizable.cvvNotInformed.text)
            onError(picpayError)
        })
    }
    
    private func verifyStatusStartTransaction(informedCvv: String? = nil, onSuccess: @escaping () -> Void, onError: @escaping (PicPayError) -> Void) {
        guard api.pciIsActiveVerifyCard else {
            verifyStatusStart(onSuccess: onSuccess, onError: onError)
            return
        }
        
        verifyStatusStartPci(informedCvv: informedCvv, onSuccess: onSuccess, onError: onError)
    }
    
    private func verifyStatusStart(onSuccess: @escaping () -> Void, onError: @escaping (PicPayError) -> Void) {
        api.verifyStatusStart(id: card.id) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    self?.isHiddenValueTextField = false
                    self?.verificationStatus = response.verificationStatus
                    onSuccess()
                case .failure(let error):
                    onError(error)
                }
            }
        }
    }
    
    private func verifyStatusStartPci(informedCvv: String?, onSuccess: @escaping () -> Void, onError: @escaping (PicPayError) -> Void) {
        let generic = VerifyCardPayload(id: card.id)
        let cvv = createCVVPayload(informedCvv: informedCvv)
        let payload = PaymentPayload(cvv: cvv, generic: generic)
        pciService.startVerifyCard(payload: payload) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    let json = JSON(response.json)
                    self?.saveCvv(informedCvv: informedCvv)
                    self?.verificationStatus = CardVerificationStatus(json: json)
                    onSuccess()
                case .failure(let error):
                    onError(error.picpayError)
                }
            }
        }
    }
    
    private func createCVVPayload(informedCvv: String?) -> CVVPayload? {
        guard let cvv = informedCvv else {
            return createLocalStoreCVVPayload()
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func createLocalStoreCVVPayload() -> CVVPayload? {
        guard let cvv = api.cvvCard(id: card.id) else {
            return nil
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func saveCvv(informedCvv: String?) {
        guard let cvv = informedCvv else {
            return
        }
        
        api.saveCvv(cardId: card.id, cvv: cvv)
    }
    
    private func checkNeedCvv() -> Bool {
        return api.cvvCard(id: card.id) == nil && api.pciIsActiveVerifyCard
    }
    
    func cancelVerify(_ completion: @escaping (Alert?, Error?) -> Void) {
        api.verifyStatusCancel(id: card.id) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    completion(response.verificationStatus.alert, nil)
                    break
                case .failure(let error):
                    completion(nil, error)
                    break
                }
            }
        }
    }
}
