//
//  EditCardAliasViewModel.swift
//  PicPay
//
//  Created by Lucas Romano on 31/08/2018.
//

import Foundation

final class EditCardAliasViewModel {
    
    var card: CardBank
    let api: WalletApiProtocol
    
    init(with card: CardBank, api: WalletApiProtocol = ApiPaymentMethods()) {
        self.card = card
        self.api = api
    }
    
    func cardEdit(newAlias: String, completion: @escaping ((Error?) -> Void)) -> Void {
        api.cardEdit(card: card.id, alias: newAlias, address: nil ) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            switch result {
            case .success( _):
                // force to reload all consumer data
                DispatchQueue.global().async {
                    ConsumerManager.shared.loadConsumerData(completion: { (_, _) in })
                }
                strongSelf.card.alias = newAlias
                completion(nil)
                break
            case .failure(let error):
                completion(error)
                break
            }
        }
    }
}
