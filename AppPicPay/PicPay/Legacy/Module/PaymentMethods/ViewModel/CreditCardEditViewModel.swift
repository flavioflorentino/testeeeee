//
//  CreditCardEditViewModel.swift
//  PicPay
//
//  Created by Lucas Romano on 30/08/2018.
//

import Foundation

final class CreditCardEditViewModel {
    
    var card: CardBank
    let api: ApiPaymentMethods = ApiPaymentMethods()
    
    init(with card: CardBank) {
        self.card = card
    }
    
    enum PaymentEditRowType: String {
        case cardVerification = ""
        case alias = "Apelido do cartão"
        case address = "Endereço de fatura"
        case delete = "Excluir cartão"
    }
    
    private let sectionsDescription:[(sectionTitle: String, rowTypes: [PaymentEditRowType])] = [
        (sectionTitle: DefaultLocalizable.general.text, rowTypes: [.cardVerification, .alias, .address]),
        (sectionTitle: "", rowTypes: [.delete])
    ]
    
    func numberOfSections() -> Int {
        return sectionsDescription.count
    }
    
    func numberOfRows(in section: Int) -> Int {
        return sectionsDescription[section].rowTypes.count
    }
    
    func getRowType(in indexPath: IndexPath) -> PaymentEditRowType {
        return sectionsDescription[indexPath.section].rowTypes[indexPath.row]
    }
    
    func titleHeader(in section: Int) -> String {
        return sectionsDescription[section].sectionTitle
    }
    
    func cardAddressDescription() -> String {
        guard let address = card.address else { return "" }
        return addressDescription(for: address)
    }
    
    func addressDescription(for address: ConsumerAddressItem) -> String {
        var description = ""
        if let street = address.streetName {
            description.append("\(street), ")
        }
        if let number = address.streetNumber {
            description.append("\(number) - ")
        }
        if let neighborhood = address.neighborhood {
            description.append("\(neighborhood), ")
        }
        if let state = address.state {
            description.append("\(state), ")
        }
        if let zipCode = address.zipCode {
            description.append("\(zipCode)")
        }
        return description
    }
    
    func isVerifiedCard() -> Bool {
        return card.verifyStatus == .verified
    }
    
    func cardEdit(newAddress: ConsumerAddressItem, completion: @escaping ((Error?) -> Void)) -> Void {
        api.cardEdit(card: card.id, alias: nil, address: newAddress) { [weak self] result in
            guard let strongSelf = self else {
            return
        }
            DispatchQueue.main.async {
                switch result {
                case .success( _):
                    // force to reload all consumer data
                    DispatchQueue.global().async {
                        ConsumerManager.shared.loadConsumerData(completion: nil)
                        CreditCardManager.shared.loadCardList({ (_) in })
                    }
                    strongSelf.card.address = newAddress
                    completion(nil)
                    break
                case .failure(let error):
                    completion(error)
                    break
                }
            }
        }
    }
    
}
