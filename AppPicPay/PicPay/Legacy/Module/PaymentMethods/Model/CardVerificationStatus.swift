import SwiftyJSON

final class VerificationStatusResponse: BaseApiResponse {
    let verificationStatus: CardVerificationStatus
    
    required init? (json: JSON) {
        let jsonData = json["data"]
        guard let verificationStatus = CardVerificationStatus(json: jsonData) else {
     return nil
}
        self.verificationStatus = verificationStatus
    }
}

final class CardVerificationStatus: BaseApiResponse {
    let success: Bool
    let status: CardBank.Status
    let text: String
    let infoText: String
    let alert: Alert?
    
    required init?(json: JSON) {
        guard let success = json["success"].bool else {
     return nil
}
        self.success = success
        self.status = CardBank.Status(rawValue: json["verify_status"].string ?? "") ?? .unknown
        self.text = json["status_text"].string ?? ""
        self.infoText = json["info_text"].string ?? ""
        self.alert = Alert(json: json["alert"])
    }
}
