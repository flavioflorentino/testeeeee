final class PaymentMethodsListResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case data
        case paymentMethods = "PaymentMethods"
        case balance = "Balance"
        case balanceValue = "balance"
    }
    
    let paymentMethods: [CardBank]
    let balanceValue: String?
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dataContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        paymentMethods = try dataContainer.decode([CardBank].self, forKey: .paymentMethods)
        let balanceContainer = try dataContainer.nestedContainer(keyedBy: CodingKeys.self, forKey: .balance)
        balanceValue = try balanceContainer.decodeIfPresent(String.self, forKey: .balanceValue)
    }
}

final class CardBank: Codable, Equatable, NestedDecodable {
    var id: String
    let type: CardType
    let description: String
    let image: String
    var expired: Bool
    var alias: String
    var address: ConsumerAddressItem?
    var lastDigits: String
    let verifyStatus: Status
    var creditCardBandeiraId: String
    var cardBrand: String?
    var cardIssuer: String?
    let isOneShotCvv: Bool?
    
    static func == (lhs: CardBank, rhs: CardBank) -> Bool {
        return lhs.id == rhs.id
    }
}

extension CardBank {
    enum CardType: String, Codable {
        case debit = "debit_card"
        case credit = "credit_card"
        case creditPicPay = "credit_picpay"
        case creditDebit = "credit_and_debit_card"
        case dda
    }
    
    enum Status: String, Codable {
        case verifiable = "verifiable"
        case waitingFillValue = "waiting_fill_value"
        case notAvailable = "not_available"
        case verified = "verified"
        case unknown = ""
    }
}
