import Foundation

enum EditCreditCardLocalizable: String, Localizable {
    case removeCard
    case invoiceAddress
    case informInvoiceAddress
    case changeInvoiceAddressTo
    case changeAddress
    case removeCardWithLastDigits
    case addCreditCard
    case addDebitCard
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .editCreditCard
    }
}
