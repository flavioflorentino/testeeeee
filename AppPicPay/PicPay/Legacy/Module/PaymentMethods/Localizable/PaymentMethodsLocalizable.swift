import Foundation

enum PaymentMethodsLocalizable: String, Localizable {
    case loading
    case scanYourCreditCard
    case invoiceAddress
    case enterTheAddress
    case wantSelectAddress
    case selectAnother
    case completedDataWillDiscarded
    case discard
    case continueFilling
    case thisCardIsNotVerified
    case verifiedCard
    case weHadProblem
    case accountConnectionHasExpired
    case increaseYourSecurity
    case cardExpired
    case wouldYouLikeRemoveIt
    case reconnectMyAccount
    case checkCard
    case setAsPrimary
    case myPicPayCard
    case myVirtualCard
    case picPayCardSettings
    case editCard
    case deleteCard
    case setUpMy
    case comeBack
    case balanceInPortfolio
    case paymentMethods
    case balanceAvailable
    case cardDefeated
    case addCreditCard
    case reconnectYourAccount
    case usePromotionalCode
    case statement
    case add
    case withdraw
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .paymentMethods
    }
}
