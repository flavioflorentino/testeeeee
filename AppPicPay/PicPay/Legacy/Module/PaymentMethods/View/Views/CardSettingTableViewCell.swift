import UI
import UIKit

final class CardSettingTableViewCell: UITableViewCell {
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	
    static let identifier: String = "editCardSettingsIdentifier"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.textColor = Colors.grayscale700.color
        descriptionLabel.textColor = Colors.grayscale400.color
    }
}

extension CardSettingTableViewCell {
    func configure(with cardAlias: String, addressString: String, and rowType: CreditCardEditViewModel.PaymentEditRowType) {
        titleLabel.text = rowType.rawValue
        switch rowType {
        case .alias:
            descriptionLabel.text = cardAlias
            descriptionLabel.isHidden = false
            break
        case .address:
            descriptionLabel.text = addressString
            descriptionLabel.isHidden = false
            break
        case .delete:
            titleLabel.textColor = Palette.ppColorNegative300.color
            descriptionLabel.isHidden = true
            break
        case .cardVerification:
            break
        }
    }
}
