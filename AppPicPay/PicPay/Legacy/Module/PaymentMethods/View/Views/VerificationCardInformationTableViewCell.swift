//
//  VerificationCardInformationTableViewCell.swift
//  PicPay
//
//  Created by Lucas Romano on 03/09/2018.
//

import UI
import UIKit

final class VerificationCardInformationTableViewCell: UITableViewCell {

    @IBOutlet weak var icoImageVIew: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    static let identifier = "verificationInformationIdentifier"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension VerificationCardInformationTableViewCell {
    
    func configure(isCardValidade: Bool) {
        if isCardValidade {
            icoImageVIew.image = #imageLiteral(resourceName: "validatedCard")
            descriptionLabel.textColor = #colorLiteral(red: 0, green: 0.6862745098, blue: 0.9333333333, alpha: 1)
            descriptionLabel.text = PaymentMethodsLocalizable.verifiedCard.text
        } else {
            icoImageVIew.image = #imageLiteral(resourceName: "notValidateCard")
            descriptionLabel.textColor = Palette.ppColorNegative300.color
            descriptionLabel.text = PaymentMethodsLocalizable.thisCardIsNotVerified.text
        }
    }
    
}
