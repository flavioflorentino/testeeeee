//
//  VerficationCardInformationTableViewCell.swift
//  PicPay
//
//  Created by Lucas Romano on 03/09/2018.
//

import UIKit

final class VerificationCardAlertTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var icoImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    static let identifier = "informationAlertIdentifier"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 4
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
