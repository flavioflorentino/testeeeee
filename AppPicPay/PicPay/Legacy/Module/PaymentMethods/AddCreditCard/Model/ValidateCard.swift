import Foundation

struct ValidateCard: Encodable {
    let cardHolder: String
    let cardNumber: String
    let expiryDate: String
    let cvv: String
    let cardFlagId: Int
    let scanned: Bool
    let origin: String
    let isDebitCard: Bool
    
    private enum CodingKeys: String, CodingKey {
        case cardHolder = "card_holder"
        case cardNumber = "card_number"
        case expiryDate = "expiry_date"
        case cvv
        case cardFlagId = "credit_card_bandeira_id"
        case scanned
        case origin
        case isDebitCard = "insert_as_debit_card"
    }
}
