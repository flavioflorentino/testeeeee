import Foundation

struct AddressCard: Encodable {
    let zipCode: String
    let city: String
    let uf: String
    let number: String
    let complement: String
    let street: String
    
    private enum CodingKeys: String, CodingKey {
        case zipCode = "zip_code"
        case city = "cidade"
        case uf
        case number
        case complement
        case street
    }
}
