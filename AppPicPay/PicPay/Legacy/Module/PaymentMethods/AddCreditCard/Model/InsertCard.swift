import Foundation

struct InsertCard: Encodable {
    let cardHolder: String
    let cardNumber: String
    let expiryDate: String
    let cvv: String
    let alias: String
    let cardFlagId: Int
    let cpf: String
    let phone: String
    let scanned: Bool
    let origin: String
    let scannedCardHolder: String
    let isDebitCard: Bool
    
    private enum CodingKeys: String, CodingKey {
        case cardHolder = "card_holder"
        case cardNumber = "card_number"
        case expiryDate = "expiry_date"
        case cvv
        case alias
        case cardFlagId = "credit_card_bandeira_id"
        case cpf = "holder_cpf"
        case phone = "holder_phone"
        case scanned
        case origin
        case scannedCardHolder = "scanned_card_holder"
        case isDebitCard = "insert_as_debit_card"
    }
}
