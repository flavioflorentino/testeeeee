import AnalyticsModule
import PermissionsKit
import SecurityModule
import SkyFloatingLabelTextField
import UI
import UIKit

protocol AddNewCreditCardDelegate: AnyObject {
    func creditCardDidInsert()
    func debitCardDidInsert()
}

extension AddNewCreditCardDelegate {
    func debitCardDidInsert() {
    }
}

final class AddNewCreditCardViewController: PPBaseViewController, Obfuscable {
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var cardContainerView: UIView!
    @IBOutlet weak var personalImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.isHidden = false
            titleLabel.alpha = 0.0
        }
    }
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var backButtonLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cardContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardContainerTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardContainerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardConstainerLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var personalImageViewTopConstraint: NSLayoutConstraint!
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    lazy var accessoryView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    var initialCardScannerViewFrame: CGRect!
    var initialCardScannerViewCenter: CGPoint!
    private var isLoadingFromServer: Bool = false
    
    lazy var cardScannerView: CardIOView = {
        let view = CardIOView()
        view.layer.cornerRadius = 5
        view.isHidden = true
        view.delegate = self
        view.languageOrLocale = "pt-BR"
        view.useCardIOLogo = true
        view.scannedImageDuration = 0.1
        view.backgroundColor = .clear
        view.frame = CGRect(x: 0, y: 0, width: self.cardContainerView.frame.width, height: self.cardContainerView.frame.width * 2)
        view.center = CGPoint(x: view.center.x, y: 0.315 * view.frame.width)
        return view
    }()
    
    lazy var formContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var nextButton: UIButton = {
        let _button = UIButton()
        _button.translatesAutoresizingMaskIntoConstraints = false
        _button.setTitle(DefaultLocalizable.next.text, for: .normal)
        _button.setTitleColor(.white, for: .normal)
        _button.backgroundColor = Palette.ppColorBranding300.color
        _button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold)
        
        return _button
    }()
    
    lazy var previousButton:UIButton = {
        let _button = UIButton()
        _button.translatesAutoresizingMaskIntoConstraints = false
        _button.setTitle(DefaultLocalizable.btPrevious.text, for: .normal)
        _button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        _button.backgroundColor = .clear
        _button.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        
        return _button
    }()
    
    lazy var pickCardOfCam: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(#imageLiteral(resourceName: "camera"), for: .normal)
        return button
    }()
    
    var frontCardView: CreditCardFrontView = {
        let view = CreditCardFrontView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var personalInfoCardView: PersonalInfoCardView = {
        let view = PersonalInfoCardView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()
    
    lazy var backCardView: CreditCardBackView = {
        let view = CreditCardBackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()
    
    lazy var loadingView: UIView = {
        let view = UIView(frame: self.view.bounds)
        view.backgroundColor = UIColor.white.withAlphaComponent(0.9)
        let label = UILabel()
        label.text = PaymentMethodsLocalizable.loading.text
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .gray
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        let activityView = UIActivityIndicatorView(style: .gray)
        activityView.translatesAutoresizingMaskIntoConstraints = false
        activityView.startAnimating()
        
        view.addSubview(activityView)
        view.addSubview(label)
        
        NSLayoutConstraint.activate(
            [
                activityView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                activityView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
            ]
        )
        NSLayoutConstraint.activate(
            [
                label.topAnchor.constraint(equalTo: activityView.bottomAnchor, constant: 8),
                label.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 8),
                label.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 8)
            ]
        )
        
        return view
    }()
    
    lazy fileprivate var cameraPermission = {
        return PPPermission.camera(withDeniedAlert: PPPermission.Alert(
            title: DefaultLocalizable.authorizePicPayCamera.text,
            message: PaymentMethodsLocalizable.scanYourCreditCard.text,
            settings: DefaultLocalizable.settings.text
        ))
    }()
    
    lazy var permissionView: AskForCameraPermissionView = {
        let view = AskForCameraPermissionView()
        view.cancelButton.isHidden = false
        return view
    }()
    
    weak var delegate: AddNewCreditCardDelegate?
    var viewModel: AddNewCreditCardViewModel = AddNewCreditCardViewModel(origin: .none)
    var isCloseButton: Bool = false
    var isNavigationAllowed: Bool = true {
        didSet {
            self.isNextButtonEnabled = self.isNavigationAllowed
            self.isPreviousButtonEnabled = self.isNavigationAllowed
        }
    }
    var isAddedExtraFields: Bool = false
    var isMovingForward: Bool = false
    var numberOfCardTextField: UITextField!
    var isScannerViewActive: Bool = false
    var creditCardFrontDelegate: CreditCardFrontDelegate!
    var creditCardBackDelegate: CreditCardBackDelegate!
    var personalInfoCardViewDelegate: PersonalInfoCardViewDelegate!
    
    var slidedTextFieldsViewController: SlidedTextFieldsViewController? {
        get {
            return self.children.first(where: {$0 is SlidedTextFieldsViewController}) as? SlidedTextFieldsViewController
        }
    }
    var isNextButtonEnabled:Bool = true {
        didSet {
            if self.isNextButtonEnabled == oldValue {
                return
            }
            self.nextButton.alpha = self.isNextButtonEnabled ? 1 : 0.5
            self.nextButton.isEnabled = self.isNextButtonEnabled
        }
    }
    var isPreviousButtonEnabled:Bool = true {
        didSet {
            if self.isPreviousButtonEnabled == oldValue {
                return
            }
            self.previousButton.alpha = self.isPreviousButtonEnabled ? 1 : 0.5
            self.previousButton.isEnabled = self.isPreviousButtonEnabled
        }
    }
    
    //Screen obfuscation
    var appSwitcherObfuscator: AppSwitcherObfuscating?
    
    // MARK: - View Controller cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupStatusBarBackground()
        setubViews()
        buildAcessoryView()
        createSlideView()
        setupActionsOfAcessoryView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObfuscationConfiguration()
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.shared.log(AddNewCardEvent.addNewCard(.addCard, type: viewModel.flowAddDebitCard ? .debit : .credit))
        
        guard !isLoadingFromServer else {
            return
        }
        slidedTextFieldsViewController?.currentTextFieldBecomeFirstResponse()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        deinitObfuscator()
    }
    
    @IBAction private func backButtonTapped(_ sender: Any) {
        if slidedTextFieldsViewController?.currentPage > 0 {
            showAlertForConfirmExit()
        } else {
            leaveTheViewController()
        }
    }
}

extension AddNewCreditCardViewController: CardIOViewDelegate {
    func cardIOView(_ cardIOView: CardIOView!, didScanCard cardInfo: CardIOCreditCardInfo!) {
        PPAnalytics.trackEvent("Scanner CC Escaneou", properties: nil)
        viewModel.cardNumber = cardInfo.cardNumber
        numberOfCardTextField.becomeFirstResponder()
        showFrontViewOfScannerView()
    }
}

extension AddNewCreditCardViewController {
    
    // MARK: - Setup view's for view controller
    
    func createSlideView() {
        var vc: SlidedTextFieldsViewController?
        if viewModel.flowEmergencyAid {
            let emergencyAidSteps = TextFields.allValues.filter {
                $0 != .cpfOfHolder && $0 != .cvv && $0 != .telOfholder
            }
            let items = emergencyAidSteps.compactMap { TextFields(rawValue: $0.rawValue) }.compactMap { $0.rawValue }
            vc = SlidedTextFieldsViewController(items: items)
        } else {
            vc = SlidedTextFieldsViewController(items: TextFields.allValues.map({ $0.rawValue }).filter({ (textFieldId: String) -> Bool in
                guard let textField = TextFields(rawValue: textFieldId) else {
                    return false
                }

                return textField != .cpfOfHolder && textField != TextFields.telOfholder
            }))
        }
        guard let viewController = vc else { return }
        self.addChild(viewController)
        self.view.layoutIfNeeded()
        viewController.view.frame = CGRect(x: 0, y: 0, width: self.formContainerView.frame.size.width, height: self.formContainerView.frame.size.height)
        viewController.delegate = self
        viewController.enable() //disable scroll
        self.formContainerView.addSubview(viewController.view)
        viewController.didMove(toParent: self)
    }
    
    func setubViews() {
        
        view.clipsToBounds = true
        
        // if presented view controller
        if isCloseButton {
            backButtonLeadingConstraint.constant = 10
            backButton.setImage(#imageLiteral(resourceName: "iconClose"), for: .normal)
            backButton.imageEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        } else {
            backButtonLeadingConstraint.constant = 4
        }
        
        creditCardFrontDelegate = frontCardView
        creditCardBackDelegate = backCardView
        personalInfoCardViewDelegate = personalInfoCardView
        
        headerView.backgroundColor = Palette.ppColorBranding300.color
        headerView.clipsToBounds = true
        cardContainerView.backgroundColor = .clear
        cardContainerView.alpha = 0
        cardContainerView.layer.cornerRadius = 5
        cardContainerView.clipsToBounds = true
        
        // add cards front and back to constainer of card
        cardContainerView.addSubview(backCardView)
        cardContainerView.addSubview(frontCardView)
        cardContainerView.addSubview(cardScannerView)
        cardContainerView.addSubview(personalInfoCardView)
        
        initialCardScannerViewFrame = cardScannerView.frame
        initialCardScannerViewCenter = cardScannerView.center
        
        self.view.addSubview(self.formContainerView)
        
        // setup constraints do front card in container of card
        NSLayoutConstraint.activate(
            [
                frontCardView.leadingAnchor.constraint(equalTo: self.cardContainerView.leadingAnchor, constant: 0),
                frontCardView.topAnchor.constraint(equalTo: self.cardContainerView.topAnchor, constant: 0),
                frontCardView.trailingAnchor.constraint(equalTo: self.cardContainerView.trailingAnchor, constant: 0),
                frontCardView.bottomAnchor.constraint(equalTo: self.cardContainerView.bottomAnchor, constant: 0)
            ]
        )
        
        // setup constraints do front card in container of card
        NSLayoutConstraint.activate(
            [
                backCardView.leadingAnchor.constraint(equalTo: self.cardContainerView.leadingAnchor, constant: 0),
                backCardView.topAnchor.constraint(equalTo: self.cardContainerView.topAnchor, constant: 0),
                backCardView.trailingAnchor.constraint(equalTo: self.cardContainerView.trailingAnchor, constant: 0),
                backCardView.bottomAnchor.constraint(equalTo: self.cardContainerView.bottomAnchor, constant: 0)
            ]
        )
        
        NSLayoutConstraint.activate(
            [
                personalInfoCardView.leadingAnchor.constraint(equalTo: self.cardContainerView.leadingAnchor, constant: 0),
                personalInfoCardView.topAnchor.constraint(equalTo: self.cardContainerView.topAnchor, constant: 0),
                personalInfoCardView.trailingAnchor.constraint(equalTo: self.cardContainerView.trailingAnchor, constant: 0),
                personalInfoCardView.bottomAnchor.constraint(equalTo: self.cardContainerView.bottomAnchor, constant: 0)
            ]
        )
        
        //SlidedTextFields
        NSLayoutConstraint.activate(
            [
                self.formContainerView.topAnchor.constraint(equalTo: self.headerView.bottomAnchor, constant: 20),
                self.formContainerView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 34),
                self.formContainerView.heightAnchor.constraint(equalToConstant: 50),
                self.formContainerView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width - (2 * 34))
            ]
        )
        
        // Animate credit card on launch view controller
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            UIView.animate(withDuration: 0.3, animations: {
                self.cardContainerView.alpha = 1
            }, completion: { finished in
                if finished {
                    self.shakeCard()
                }
            })
        }
    }
    
    func setupStatusBarBackground() {
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        statusBarView.backgroundColor = Palette.ppColorBranding300.color
        view.addSubview(statusBarView)
        view.sendSubviewToBack(statusBarView)
    }
    
    enum TextFields: String {
        case number = "Número do cartão"
        case name = "Nome do titular (nome escrito no cartão)"
        case expiryDate = "Vencimento"
        case cvv = "CVV"
        case alias = "Apelido do cartão (opcional)"
        case cpfOfHolder = "CPF do titular do cartão"
        case telOfholder = "Telefone do títular do cartão"
        
        static let allValues = [number, name, expiryDate, cvv, alias, cpfOfHolder, telOfholder]
    }
    
    func buildAcessoryView() {
        
        // change height of button for iphone 4s
        var buttonSize: CGSize!
        var textSizeOfButons: CGFloat =  17
        if  UIScreen.main.bounds.height == 480 {
            textSizeOfButons = 15
            buttonSize = CGSize(width: 89, height: 30)
        } else {
            buttonSize = CGSize(width: 112, height: 40)
        }
        
        previousButton.titleLabel?.font = UIFont.systemFont(ofSize: textSizeOfButons)
        
        let padding:CGFloat = 5
        accessoryView.frame = CGRect(x: 0, y: 0, width: 0, height: buttonSize.height + (2 * padding))
        accessoryView.addSubview(self.previousButton)
        accessoryView.addSubview(self.nextButton)
        
        NSLayoutConstraint.activate(
            [
                previousButton.leftAnchor.constraint(equalTo: self.accessoryView.leftAnchor, constant: padding),
                previousButton.topAnchor.constraint(equalTo: self.accessoryView.topAnchor, constant: padding),
                previousButton.widthAnchor.constraint(equalToConstant: buttonSize.width),
                previousButton.heightAnchor.constraint(equalToConstant: buttonSize.height)
            ]
        )
        NSLayoutConstraint.activate(
            [
                nextButton.rightAnchor.constraint(equalTo: self.accessoryView.rightAnchor, constant: -padding),
                nextButton.topAnchor.constraint(equalTo: self.accessoryView.topAnchor, constant: padding),
                nextButton.widthAnchor.constraint(equalToConstant: buttonSize.width),
                nextButton.heightAnchor.constraint(equalToConstant: buttonSize.height)
            ]
        )
        
        nextButton.layer.masksToBounds = true
        nextButton.layer.cornerRadius = buttonSize.height / 2
        previousButton.alpha = 0
    }
    
    func setupActionsOfAcessoryView() {
        self.nextButton.addTarget(self, action: #selector(nextButtonAction), for: .touchUpInside)
        self.previousButton.addTarget(self, action: #selector(previousButtonAction), for: .touchUpInside)
        self.pickCardOfCam.addTarget(self, action: #selector(scannerCreditCard), for: .touchUpInside)
    }
    
    // MARK: - Private functions
    
    func defaultTextfield(placeholder: String) -> UITextField {
        let unselectedColor = UIColor(white: 143/255.0, alpha: 0.87)
        let selectedColor = Palette.ppColorBranding300.color
        let textField = SkyFloatingLabelTextField()
        textField.placeholder = placeholder
        textField.lineHeight = 2
        textField.font = UIFont.systemFont(ofSize: 16)
        textField.lineColor = unselectedColor
        textField.titleColor = unselectedColor
        textField.textColor = .black
        textField.placeholderColor = unselectedColor
        textField.selectedLineColor = selectedColor
        textField.selectedTitleColor = selectedColor
        textField.titleFormatter = {$0}
        textField.titleLabel.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.medium)
        textField.autocorrectionType = .no
        textField.inputAccessoryView = self.accessoryView
        return textField
    }
    
    func configure(_ textField: UITextField, with newString: String, oldString: String, maskedString: String, range: NSRange, replacementString string: String) {
        let newPosition:Int
        if newString.count < oldString.count {
            //Removeu, move para o inicio da alteração
            newPosition = range.location
        } else {
            let maskExtraChars = maskedString.count - newString.count
            let addedTextLength = string.count + maskExtraChars
            //Adicionou, move para o final da alteração, considerando o tambem o texto da mascara
            newPosition = range.location + addedTextLength
        }
        
        if let textPosition = textField.position(from: textField.beginningOfDocument, offset: newPosition) {
            //Se a posição é valida para o textField, move o cursor para a posição
            textField.selectedTextRange = textField.textRange(from: textPosition, to: textPosition)
        }
    }
    
    func changeHeaderSize(isKeyboardVisible:Bool) {
        // change header if device is iphone 4s
        if isKeyboardVisible && UIScreen.main.bounds.height == 480 {
            UIView.animate(withDuration: 0.3) {
                self.cardConstainerLeadingConstraint.constant = 47
                self.cardContainerTrailingConstraint.constant = 47
                self.cardContainerTopConstraint.constant = -22
                self.cardContainerBottomConstraint.constant = 8
                NSLayoutConstraint.activate(
                    [
                        self.formContainerView.topAnchor.constraint(equalTo: self.headerView.bottomAnchor, constant: 6),
                        self.formContainerView.heightAnchor.constraint(equalToConstant: 40)
                    ]
                )
                self.cardContainerView.layoutIfNeeded()
            }
        }
    }
    
    // MARK: - Requests
    
    func validateCard(for textFieldSelected: UITextField) {
        if frontCardView.type.id != CreditCardFlagTypeId.amex.rawValue {
            flipCardToFrontView()
        } else {
            creditCardFrontDelegate.removeHighlightCircle()
        }
        
        textFieldSelected.isEnabled = false
        self.loadingView.frame = self.cardContainerView.bounds
        self.cardContainerView.addSubview(self.loadingView)
        isNextButtonEnabled = false
        isPreviousButtonEnabled = false
        isLoadingFromServer = true
        viewModel.validateCredicard { [weak self] (error, isAskCPF) in
            guard let self = self else {
                return
            }
            
            self.isLoadingFromServer = false
            textFieldSelected.isEnabled = true
            self.loadingView.removeFromSuperview()
            if let error = error {
                AlertMessage.showCustomAlertWithError(error, controller: self) {
                    // back to cvv textfield is card not is valid
                    if self.isAmexCard() == false {
                        self.flipCardToBackView()
                    }
                    
                    self.slidedTextFieldsViewController?.currentTextFieldBecomeFirstResponse()
                }
            } else if let isAskCPF = isAskCPF{
                if isAskCPF {
                    guard let controller = self.slidedTextFieldsViewController else {
                        return
                    }
                    // add extras fields in form
                    if self.isAddedExtraFields == false {
                        self.isAddedExtraFields = true
                        controller.addItem(TextFields.cpfOfHolder.rawValue, isDynamicInsertion: true)
                        controller.addItem(TextFields.telOfholder.rawValue, isDynamicInsertion: true)
                    }
                    self.showPersonalCardInfo()
                    controller.nextPage()
                } else {
                    self.showAddressForm()
                }
            }
            
            self.isNextButtonEnabled = true
            self.isPreviousButtonEnabled = true
        }
    }
    
    func saveCreditCard() {
        self.loadingView.frame = self.view.bounds
        self.view.addSubview(self.loadingView)
        isLoadingFromServer = true
        viewModel.sendLogDetection()
        
        viewModel.saveCard(creditSuccess: { [weak self] in
            self?.showCardAddedSuccess(isDebitCard: false)
        }, debitSuccess: { [weak self] in
            self?.showCardAddedSuccess(isDebitCard: true)
        }, onError: saveCardError)
    }
    
    private func saveCardError(_ error: Error) {
        hidesLoading()
        AlertMessage.showCustomAlertWithError(error, controller: self) { [weak self] in
            guard let controller = self?.slidedTextFieldsViewController, let self = self else {
                return
            }
            if TextFields(rawValue: controller.currentItem) == .cvv && !self.isAmexCard() {
                self.flipCardToBackView()
            }
            self.slidedTextFieldsViewController?.currentTextFieldBecomeFirstResponse()
        }
    }
    
    func showCardAddedSuccess(isDebitCard: Bool) {
        hidesLoading()
        Analytics.shared.log(AddNewCardEvent.addNewCard(.addCardSuccess, type: isDebitCard ? .debit : .credit))
        
        let addedCardSuccess = creditCardAddedView(isDebitCard: isDebitCard)
        
        UIApplication.shared.keyWindow?.addSubview(addedCardSuccess)
        
        UIView.animate(withDuration: 0.25, delay: 0, options: .allowUserInteraction, animations: {
            addedCardSuccess.alpha = 1
        }, completion: { [weak self] done in
            guard let self = self else { return }
            if done {
                self.leaveTheViewController()
                isDebitCard ? self.delegate?.debitCardDidInsert() : self.delegate?.creditCardDidInsert()
                CreditCardManager.shared.loadCardList { _ in }
                
                UIView.animate(withDuration: 0.25, delay: 1.5, options: .allowUserInteraction, animations: {
                    addedCardSuccess.alpha = 0.05
                }, completion: {  (done) in
                    addedCardSuccess.removeFromSuperview()
                })
            }
        })
    }
    
    private func creditCardAddedView(isDebitCard: Bool) -> CreditCardAddedView {
        let addedCardSuccess = CreditCardAddedView(frame: view.frame)
        addedCardSuccess.isUserInteractionEnabled = true
        let tapOverView = UITapGestureRecognizer(target: self, action: #selector(closeCardAddedSuccessAndLeave))
        addedCardSuccess.addGestureRecognizer(tapOverView)
        
        addedCardSuccess.setCardType(type: isDebitCard ? .addDebitCard : .addCreditCard)
        
        addedCardSuccess.alpha = 0
        if let flagId = CreditCardFlagTypeId(rawValue: viewModel.type.id) {
            addedCardSuccess.setCardFlagType(type: flagId)
        }
        
        return addedCardSuccess
    }
    
    private func hidesLoading() {
        isLoadingFromServer = false
        loadingView.removeFromSuperview()
    }
    
    @objc func closeCardAddedSuccessAndLeave(_ recognizer:UITapGestureRecognizer) {
        leaveTheViewController()    // Go back to payments view
        recognizer.view?.removeFromSuperview()
    }
    
    func isAmexCard() -> Bool {
        return self.frontCardView.type.id == CreditCardFlagTypeId.amex.rawValue || viewModel.flowEmergencyAid
    }
    
    // MARK: - Actions
    
    func showAddressForm() {
        sendStep("address")
        self.view.endEditing(true)
        let addressListVM = AddressListViewModel(displayContextHeader: true)
        let consumerAddressList = AddressListViewController(viewModel: addressListVM)
        
        consumerAddressList.addressContextTitle = PaymentMethodsLocalizable.invoiceAddress.text
        consumerAddressList.addressContextText = PaymentMethodsLocalizable.enterTheAddress.text
        consumerAddressList.addressContextImage = #imageLiteral(resourceName: "ilu_invoice")
        
        consumerAddressList.onAddressSelected = { [weak self] (address) in
            guard let strongSelf = self else {
                return
            }
            guard let address = address else {
                return
            }
            
            // set consummer addres for convert in creditAddress in view model
            strongSelf.viewModel.consumerAddressCard = address
            
            let alert = Alert(title: DefaultLocalizable.attention.text, text: "\(PaymentMethodsLocalizable.wantSelectAddress.text) \(address.getTitle())")
            let buttonYes = Button(title: DefaultLocalizable.btYes.text, type: .cta) { popupController, _ in
                popupController.dismiss(animated: true, completion: {
                    if let addCreditVC = strongSelf.navigationController?.viewControllers.first(where: {$0 is AddNewCreditCardViewController}) as? AddNewCreditCardViewController {
                        strongSelf.navigationController?.popToViewController(addCreditVC, animated: true)
                        strongSelf.saveCreditCard()
                    }
                })
            }
            let buttonNo = Button(title: PaymentMethodsLocalizable.selectAnother.text, type: .clean, action: .close)
            alert.buttons = [buttonYes, buttonNo]
            AlertMessage.showAlert(alert, controller: strongSelf)
        }
        
        consumerAddressList.onNewAddressAdded = { [weak self] (newAddress) in
            guard let strongSelf = self, let address = newAddress else {
                return
            }
            
            strongSelf.viewModel.consumerAddressCard = address
            
            if let addCreditVC = strongSelf.navigationController?.viewControllers.first(where: {$0 is AddNewCreditCardViewController}) as? AddNewCreditCardViewController {
                strongSelf.navigationController?.popToViewController(addCreditVC, animated: true)
                strongSelf.saveCreditCard()
            }
        }
        
        self.didMove(toParent: self)
        self.navigationController?.pushViewController(consumerAddressList, animated: true)
    }
    
    @objc func nextButtonAction(sender: Any?) {
        if let controller = self.slidedTextFieldsViewController {
            if controller.currentItem == TextFields.number.rawValue && viewModel.flowEmergencyAid {
                self.isMovingForward = true
                controller.nextPage()
                self.isMovingForward = false
            }
            else if controller.currentItem == TextFields.alias.rawValue {
                self.validateCard(for: controller.currentTextField)
            } else if controller.hasNextPage {
                self.isMovingForward = true
                controller.nextPage()
                self.isMovingForward = false
            } else {
                self.showAddressForm()
            }
        }
    }
    
    @objc func previousButtonAction(sender: Any?) {
        if let controller = self.slidedTextFieldsViewController {
            guard let item = TextFields(rawValue: controller.currentItem) else {
                return
            }
            switch item {
            case .cvv:
                // if amex credit card not flip to front view
                if !self.isAmexCard() {
                    flipCardToFrontView()
                }
            case .telOfholder:
                hidePersonalImageView()
            case .cpfOfHolder:
                showFrontViewFromPersonalImageVIew()
            default: break
            }
            controller.previousPage()
        }
    }
    
    // MARK: animation in the header view
    
    @objc func showFrontViewFromPersonalImageVIew() {
        let transitionOptions: UIView.AnimationOptions = [.transitionCrossDissolve, .showHideTransitionViews]
        UIView.transition(with: cardContainerView, duration: 0.3, options: transitionOptions, animations: {
            self.personalInfoCardView.isHidden = true
        })
        UIView.transition(with: cardContainerView, duration: 0.3, options: transitionOptions, animations: {
            self.frontCardView.isHidden = false
        }, completion: { finished in
            if !self.isAmexCard() {
                if finished {
                    self.flipCardToBackView()
                }
            }
        })
    }
    
    @objc func hidePersonalImageView() {
        let transitionOptions: UIView.AnimationOptions = [.transitionCrossDissolve, .showHideTransitionViews]
        UIView.transition(with: headerView, duration: 0.3, options: transitionOptions, animations: {
            self.personalImageView.isHidden = true
        })
        UIView.transition(with: cardContainerView, duration: 0.3, options: transitionOptions, animations: {
            self.cardContainerView.isHidden = false
        })
    }
    
    @objc func showPersonalImageView() {
        let transitionOptions: UIView.AnimationOptions = [.transitionCrossDissolve, .showHideTransitionViews]
        UIView.transition(with: headerView, duration: 0.3, options: transitionOptions, animations: {
            self.cardContainerView.isHidden = true
        })
        UIView.transition(with: cardContainerView, duration: 0.3, options: transitionOptions, animations: {
            self.personalImageView.isHidden = false
        })
    }
    
    @objc func showPersonalCardInfo() {
        let transitionOptions: UIView.AnimationOptions = [.transitionCrossDissolve, .showHideTransitionViews]
        UIView.transition(with: cardContainerView, duration: 0.3, options: transitionOptions, animations: {
            self.frontCardView.isHidden = true
        })
        UIView.transition(with: cardContainerView, duration: 0.3, options: transitionOptions, animations: {
            self.personalInfoCardView.isHidden = false
            self.personalInfoCardView.layoutIfNeeded()
        }, completion: { finished in
            if finished {
                self.personalInfoCardViewDelegate.nameDidTypping(self.viewModel.name)
            }
        })
    }
    
    /// Flip to card view front to card view back
    @objc func flipCardToBackView() {
        let transitionOptions: UIView.AnimationOptions = [.transitionFlipFromRight, .showHideTransitionViews]
        UIView.transition(with: cardContainerView, duration: 0.3, options: transitionOptions, animations: {
            self.frontCardView.isHidden = true
        })
        UIView.transition(with: cardContainerView, duration: 0.3, options: transitionOptions, animations: {
            self.backCardView.isHidden = false
        })
    }
    
    @objc func flipCardToFrontView() {
        let transitionOptions: UIView.AnimationOptions = [.transitionFlipFromLeft, .showHideTransitionViews]
        UIView.transition(with: cardContainerView, duration: 0.3, options: transitionOptions, animations: {
            self.backCardView.isHidden = true
        })
        UIView.transition(with: cardContainerView, duration: 0.3, options: transitionOptions, animations: {
            self.frontCardView.isHidden = false
        })
    }
    
    @objc func showScannerView() {
        PPAnalytics.trackEvent("Scanner CC Abriu", properties: nil)
        
        numberOfCardTextField.isEnabled = false
        self.numberOfCardTextField.resignFirstResponder()
        
        let transitionOptions: UIView.AnimationOptions = [.transitionFlipFromTop, .showHideTransitionViews]
        UIView.transition(with: cardContainerView, duration: 0.3, options: transitionOptions, animations: {
            self.frontCardView.isHidden = true
        })
        
        UIView.transition(with: cardContainerView, duration: 0.3, options: transitionOptions, animations: {
            self.cardScannerView.isHidden = false
            
            self.cardContainerTopConstraint.constant = -(44)
            self.cardContainerTrailingConstraint.constant = 0
            self.cardContainerBottomConstraint.constant = 0
            self.cardConstainerLeadingConstraint.constant = 0
            self.cardContainerView.layoutIfNeeded()
            
            let width = self.cardContainerView.frame.width
            let height = (width * 2)
            let centerY = (0.315 * width)
            
            self.cardScannerView.frame = CGRect(x: 0, y: 0, width: width, height: height)
            self.cardScannerView.center = CGPoint(x: self.cardScannerView.center.x, y: centerY)
            self.cardContainerView.layer.cornerRadius = 0
            
            self.titleLabel.alpha = 1.0
            
        }, completion: { finished in
            if finished {
                self.numberOfCardTextField.isEnabled = true
            }
        })
    }
    
    @objc func showFrontViewOfScannerView() {
        self.isScannerViewActive = false
        let transitionOptions: UIView.AnimationOptions = [.transitionFlipFromBottom, .showHideTransitionViews]
        
        UIView.transition(with: self.cardContainerView, duration: 0.3, options: transitionOptions, animations: {
            self.titleLabel.alpha = 0.0
            
            self.cardContainerTopConstraint.constant = -(20)
            self.cardContainerTrailingConstraint.constant = 44
            self.cardContainerBottomConstraint.constant = 28
            self.cardConstainerLeadingConstraint.constant = 44
            
            self.cardScannerView.frame = self.initialCardScannerViewFrame
            self.cardScannerView.center = self.initialCardScannerViewCenter
            self.cardContainerView.layer.cornerRadius = 5
            
            self.headerView.layoutIfNeeded()
            self.cardScannerView.isHidden = true
        })
        UIView.transition(with: self.cardContainerView, duration: 0.3, options: transitionOptions, animations: {
            self.frontCardView.isHidden = false
            self.titleLabel.alpha = 0.0
        }, completion: { finished in
            if finished {
                self.numberOfCardTextField.becomeFirstResponder()
            }
        })
    }
    
    @objc func shakeCard() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.10
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.cardContainerView.center.x - 5, y: self.cardContainerView.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.cardContainerView.center.x + 5, y: self.cardContainerView.center.y))
        self.cardContainerView.layer.add(animation, forKey: "position")
    }
    
    @objc func scannerCreditCard() {
        if isScannerViewActive {
            showFrontViewOfScannerView()
        } else {
            checkCameraPermission()
        }
    }
    
    func checkCameraPermission(){
        let permission: Permission = PPPermission.camera()
        if permission.status == .notDetermined {
            requestCamerraPermission()
        } else if permission.status == .denied || permission.status == .disabled {
            permissionView.action = { [weak self] (button) in
                guard let strongSelf = self else {
                    return
                }
                PPPermission.request(permission: strongSelf.cameraPermission, { status in
                    guard status == .authorized else {
                        return
                    }
                    strongSelf.scannerCreditCard()
                })
            }
            
            permissionView.view.backgroundColor = UIColor.black.withAlphaComponent(0.95)
            permissionView.descriptionLabel.textColor = .white
            permissionView.isHidden = cameraPermission.status == .authorized
            
            view.endEditing(true)
            view.addSubview(permissionView)
            view.bringSubviewToFront(backButton)
            permissionView.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
        } else {
            showScannerView()
            isScannerViewActive = !isScannerViewActive
        }
    }
    
    @objc func requestCamerraPermission(){
        PPPermission.requestCamera({ [weak self] (status) in
            guard let strongSelf = self else {
                return
            }
            if status == .authorized {
                strongSelf.showScannerView()
                strongSelf.isScannerViewActive = !strongSelf.isScannerViewActive
            }
        })
    }
    
    func showAlertForConfirmExit() {
        // if user pass of first field of form
        let alert = Alert(title: DefaultLocalizable.attention.text, text: PaymentMethodsLocalizable.completedDataWillDiscarded.text)
        let buttonDiscart = Button(title: PaymentMethodsLocalizable.discard.text, type: .destructive) { [weak self] popupController, _ in
            guard let strongSelf = self else {
                return
            }
            let properties: [String: Any] = ["Resposta": "Descartar", "Origem": strongSelf.viewModel.origin]
            PPAnalytics.trackEvent("Popup de sair do cadastro de cartao de credito", properties: properties)
            popupController.dismiss(animated: true, completion: {
                strongSelf.leaveTheViewController()
            })
        }
        let buttonClose = Button(title: PaymentMethodsLocalizable.continueFilling.text, type: .clean) { [weak self] popupController, _ in
            guard let strongSelf = self else {
                return
            }
            let properties: [String: Any] = ["Resposta": "Continuar preenchendo", "Origem": strongSelf.viewModel.origin]
            PPAnalytics.trackEvent("Popup de sair do cadastro de cartao de credito", properties: properties)
            popupController.dismiss(animated: true)
        }
        alert.buttons = [buttonDiscart, buttonClose]
        alert.dismissOnTouchBackground = false
        alert.dismissWithGesture = false
        
        view.endEditing(true)
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func leaveTheViewController() {
        if isCloseButton {
            dismiss(animated: true, completion: nil)
        } else {
            navigationController?.popViewController(animated: false)
        }
    }
    
    func sendStep(_ step: String) {
        let properties: [String: Any] = ["Step": step]
        PPAnalytics.trackEvent("Step do cadastro de cartao de credito", properties: properties)
    }
    
    func dateFormat(with format: String) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = format
        return formatter
    }
}
