import UIKit
import Lottie

final class CreditCardAddedView: NibView {
	@IBOutlet weak var flagImageView: UIImageView!
	@IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
	override init(frame: CGRect) {
		super.init(frame: frame)
		commonInit()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		commonInit()
	}
	
	func commonInit() {
		flagImageView.layer.cornerRadius = 5
		checkImageView.layer.cornerRadius = checkImageView.frame.width / 2
	}
	
    func setCardType(type: EditCreditCardLocalizable) {
        titleLabel.text = type.text
    }
    
	func setCardFlagType(type: CreditCardFlagTypeId) {
		switch type {
		case .amex:
			flagImageView.image = UIImage(named: "iluFlagAmex")
		case .elo:
			flagImageView.image = UIImage(named: "iluFlagElo")
		case .hipercard:
			flagImageView.image = UIImage(named: "iluFlagHiper")
		case .master:
			flagImageView.image = UIImage(named: "iluFlagMaster")
		case .visa:
			flagImageView.image = UIImage(named: "iluFlagVisa")
        case .picpay:
            flagImageView.image = UIImage(named: "iluFlagUnknown")
		case .unknown:
			flagImageView.image = UIImage(named: "iluFlagUnknown")
		}
	}
}

extension CreditCardAddedView {
    enum CardType {
        case debit
        case credit
    }
}
