import UI
import UIKit

final class CreditCardFrontView: UIView, CreditCardProtocol {
    
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var expiryDateLabel: UILabel!
    @IBOutlet weak var cvvLabel: UILabel!
    @IBOutlet weak var circleHighlightView: UIView!
    
    var listOfCreditCardTypes: [CreditCardFlagType] = []
    var gradientLayer: CAGradientLayer?
    var type: CreditCardFlagType = .unknown
    var cardNumber: String? = nil
    var ownerName: String? = nil
    var expiryDate: String? = nil
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        commonInit()
    }
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		commonInit()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		commonInit()
	}
	
	func commonInit() {
		self.listOfCreditCardTypes = self.getListOfCreditCardType()
		self.setup()
	}
}

extension CreditCardFrontView: CreditCardFrontDelegate {
    
    internal func removeHighlightCircle() {
        circleHighlightView.isHidden = true
    }
    
    internal func setCVV(_ cvv: String) {
        if self.type.id == CreditCardFlagTypeId.amex.rawValue {
            circleHighlightView.isHidden = false
            if cvv.isEmpty {
                cvvLabel.text = "0000"
                cvvLabel.textColor = Palette.white.color.withAlphaComponent(0.5)
            } else {
                cvvLabel.text = cvv
                cvvLabel.textColor = Palette.white.color
            }
        }
    }
    
    internal func setNumber(_ number: String) {
        if number.isEmpty {
            numberLabel.text = "**** **** **** ****"
            numberLabel.textColor = Palette.white.color.withAlphaComponent(0.5)
        } else if let numberOfCard = numberLabel.text, numberOfCard.count ==  number.count {
            setFlagType(of: number)
            numberLabel.text = number
            numberLabel.textColor = Palette.white.color
        } else {
            setFlagType(of: number)
            animateLabelOnTyping(self.numberLabel)
            numberLabel.text = number
            numberLabel.textColor = Palette.white.color
        }
    }
    
    internal func setName(_ name: String) {
        if name.isEmpty {
            nameLabel.textColor = Palette.white.color.withAlphaComponent(0.5)
            nameLabel.text = "NOME DO TITULAR"
        } else {
            nameLabel.textColor = Palette.white.color
            animateLabelOnTyping(self.nameLabel)
            nameLabel.text = name.uppercased()
        }
    }
    
    internal func setExpirateDate(_ date: String) {
        if date.isEmpty {
            expiryDateLabel.text = "MM/AA"
            expiryDateLabel.textColor = Palette.white.color.withAlphaComponent(0.5)
        } else {
            expiryDateLabel.textColor = Palette.white.color
            animateLabelOnTyping(self.expiryDateLabel)
            expiryDateLabel.text = date
        }
    }
    
    internal func isValidNumberOfCard() -> Bool {
        guard let numberOfCard = numberLabel.text else {
    return false
}
        return validateCardNumber(number: numberOfCard)
    }
    
    internal func getCardType() -> CreditCardFlagType {
        return self.type
    }
}

extension CreditCardFrontView {
    
    fileprivate func setFlagType(of cardNumber: String) {
        self.type = self.flagType(of: cardNumber)
        if self.type == CreditCardFlagType.amex(id: 4, regex: "") {
            self.cvvLabel.isHidden = false
        } else {
             self.cvvLabel.isHidden = true
        }
        setupColorsAndAnimations()
    }
    
    fileprivate func animateLabelOnTyping(_ label: UILabel) {
        /*let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.02
        animation.repeatCount = 1
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: label.center.x, y: label.center.y + 2))
        animation.toValue = NSValue(cgPoint: CGPoint(x: label.center.x, y: label.center.y - 2))
        label.layer.add(animation, forKey: "position")*/
    }
    
    fileprivate func setup() {
        guard let view = loadViewFromNib() else {
            return
        }
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.layer.cornerRadius = 5
        self.clipsToBounds = true
        self.layer.cornerRadius = 5
        self.backgroundColor = .clear
        self.sizeToFit()
        self.layoutIfNeeded()
        self.addSubview(view)
        
        cvvLabel.isHidden = true
        circleHighlightView.isHidden = true
        circleHighlightView.layer.cornerRadius = circleHighlightView.bounds.height / 2
        circleHighlightView.layer.borderWidth = 2
        circleHighlightView.layer.borderColor = #colorLiteral(red: 1, green: 0.31372549019607843, blue: 0.45882352941176469, alpha: 1).cgColor
        circleHighlightView.backgroundColor = .clear
        
        gradientLayer?.removeFromSuperlayer()
        
        setLabels()
        
        // configure label's size for iphone 4S
        if UIScreen.main.bounds.height == 480 {
            numberLabel.font = UIFont(name: numberLabel.font.fontName, size: 15)
            nameLabel.font = UIFont(name: nameLabel.font.fontName, size: 9)
            expiryDateLabel.font = UIFont(name: expiryDateLabel.font.fontName, size: 9)
        }
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: [.transitionCrossDissolve], animations: {
            self.gradientLayer = self.setupColorOfCard(with: self.type, for: view)
            self.flagImageView.image = self.imageFlag(of: self.type)
        }, completion: nil)
    }
    
    fileprivate func setupColorsAndAnimations() {
        
        // transition for change flag image of credit card view
        UIView.transition(with: flagImageView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.flagImageView.image = self.imageFlag(of: self.type)
        }, completion: nil)
        
        let colors: [CGColor] = self.colorsOfCards(with: self.type)
        
        // Inserta animation for change color gradient
        CATransaction.begin()
        let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
        gradientChangeAnimation.duration = 0.4
        gradientChangeAnimation.fromValue = self.gradientLayer?.colors
        gradientChangeAnimation.toValue = colors
        gradientChangeAnimation.fillMode = .forwards
        gradientChangeAnimation.isRemovedOnCompletion = false
        
        CATransaction.setCompletionBlock {
            // set grandient color after end animation
            self.gradientLayer?.colors = colors
        }
        
        self.gradientLayer?.add(gradientChangeAnimation, forKey: "colorChange")
        CATransaction.commit()
    }
    
    func setLabels() {
        if let cardNum = cardNumber {
            numberLabel.text = cardNum
        }
        if let name = ownerName {
            nameLabel.text = name
        }
        if let expiry = expiryDate {
            expiryDateLabel.text = expiry
        }
    }
    
    func setBackgroundColorAndIcon(bandeiraId: String) {

        switch bandeiraId {
        case "1": type = .visa(id: 0, regex: "")
        case "2": type = .master(id: 0, regex: "")
        case "4": type = .amex(id: 0, regex: "")
        case "5": type = .elo(id: 0, regex: "")
        case "6": type = .hipercard(id: 0, regex: "")
        case "0": type = .unknown
        default:  type = .unknown
        }

        gradientLayer = setupColorOfCard(with: type, for: self)
        flagImageView.image = imageFlag(of: type)
    }
    
    fileprivate func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: Swift.type(of: self))
        let nib = UINib(nibName: String(describing: CreditCardFrontView.self), bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
}

