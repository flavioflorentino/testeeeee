//
//  CreditCardBackView.swift
//  PicPay
//
//  Created by Lucas Romano on 05/06/2018.
//

import UIKit

final class CreditCardBackView: UIView, CreditCardProtocol {
    
    var listOfCreditCardTypes: [CreditCardFlagType] = []
    
    @IBOutlet weak var whiteContainerNumberCvvView: UIView!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var cvvLabel: UILabel!
    
    internal var gradientLayer: CAGradientLayer?
    internal let fontColor: UIColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
    public var type: CreditCardFlagType = .unknown {
        didSet {
            gradientLayer?.colors = self.colorsOfCards(with: self.type)
        }
    }
    
    override func draw(_ rect: CGRect) {
        self.listOfCreditCardTypes = self.getListOfCreditCardType()
        super.draw(rect)
        self.setup()
    }
}

extension CreditCardBackView: CreditCardBackDelegate {
    
    func setCVV(_ number: String) {
        if number.isEmpty {
            cvvLabel.textColor = self.fontColor.withAlphaComponent(0.5)
            cvvLabel.text = "000"
        } else {
            cvvLabel.text = number
            cvvLabel.textColor = self.fontColor
        }
    }
    
    func setType(_ type: CreditCardFlagType) {
        self.type = type
    }
}

extension CreditCardBackView {
    
    fileprivate func setup() {
        guard let view = loadViewFromNib() else {
            return
        }
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.layer.cornerRadius = 5
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        self.backgroundColor = .clear
        self.sizeToFit()
        self.layoutIfNeeded()
        self.addSubview(view)
        gradientLayer?.removeFromSuperlayer()
        gradientLayer = setupColorOfCard(with: self.type, for: view)
        whiteContainerNumberCvvView.layer.cornerRadius = 2
        circleView.layer.cornerRadius = circleView.bounds.height / 2
        circleView.layer.borderWidth = 2
        circleView.layer.borderColor = #colorLiteral(red: 1, green: 0.31372549019607843, blue: 0.45882352941176469, alpha: 1).cgColor
        circleView.backgroundColor = .clear
    }
    
    fileprivate func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: Swift.type(of: self))
        let nib = UINib(nibName: String(describing: CreditCardBackView.self), bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
}
