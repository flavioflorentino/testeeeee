//
//  CreditCardFrontDelegate.swift
//  PicPay
//
//  Created by Lucas Romano on 05/06/2018.
//

import Foundation

protocol CreditCardFrontDelegate: AnyObject {
    func setNumber(_ number: String)
    func setName(_ name: String)
    func setExpirateDate(_ date: String)
    func isValidNumberOfCard() -> Bool
    func getCardType() -> CreditCardFlagType
    func setCVV(_ cvv: String)
    func removeHighlightCircle()
}
