//
//  CreditCardBackDelegate.swift
//  PicPay
//
//  Created by Lucas Romano on 05/06/2018.
//

import Foundation

protocol CreditCardBackDelegate: AnyObject {
    func setCVV(_ number: String)
    func setType(_ type: CreditCardFlagType)
}
