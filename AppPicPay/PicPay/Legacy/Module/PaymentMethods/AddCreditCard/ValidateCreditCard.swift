import Foundation

struct ValidateCreditCard {
    let askCpf: Bool
}

extension ValidateCreditCard: Codable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let data = try values.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        let askCpfInt = try data.decode(Int.self, forKey: .askCpf)
        askCpf = askCpfInt == 1
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        var data = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        try data.encode(askCpf, forKey: .askCpf)
    }
    
    enum CodingKeys: String, CodingKey {
        case data
        case askCpf
    }
}
