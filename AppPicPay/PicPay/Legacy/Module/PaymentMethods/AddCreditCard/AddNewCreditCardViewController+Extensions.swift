import Foundation
import SkyFloatingLabelTextField
import UI

extension AddNewCreditCardViewController: SlidedTextFieldsDelegate {
    
    // MARK: - Slided Text Fields Delegate
    
    func slidedTextFields(viewFor id: SlidedTextFieldsDelegate.Id) -> UIView {
        let textField = self.defaultTextfield(placeholder: id)
        guard let textFieldId: TextFields = TextFields(rawValue: id) else { return textField }
        textField.placeholder = textFieldId.rawValue
        
        switch textFieldId {
        case .number:
            textField.keyboardType = .numberPad
            textField.rightView = self.pickCardOfCam
            textField.rightViewMode = .always
            self.numberOfCardTextField = textField
        case .name:
            textField.keyboardType = .namePhonePad
            textField.autocapitalizationType = .allCharacters
        case .expiryDate:
            textField.keyboardType = .numberPad
        case .cvv:
            textField.keyboardType = .numberPad
        case .alias:
            textField.keyboardType = .namePhonePad
        case .cpfOfHolder:
            textField.keyboardType = .numberPad
        case .telOfholder:
            textField.keyboardType = .numberPad
        }
        return textField
    }
    
    func slidedTextFieldsDidBeginEditing(_ textField: UITextField, id: SlidedTextFieldsDelegate.Id) {
        
        self.changeHeaderSize(isKeyboardVisible: true)
        
        guard let textFieldId = TextFields(rawValue: id) else {
            return
        }
        switch textFieldId {
        case .number:
            sendStep("card number")
            navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            if isScannerViewActive {
                showFrontViewOfScannerView()
            }
            let number = CustomStringMask(mask: "0000 0000 0000 0000 000").maskedText(from: viewModel.cardNumber) ?? ""
            textField.text = number
            creditCardFrontDelegate.setNumber(number.isEmpty ? "" : number)
        case .name:
            sendStep("card holder")
            navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            isNavigationAllowed = false
            textField.text = viewModel.name
            creditCardFrontDelegate.setName(viewModel.name)
            self.isPreviousButtonEnabled = false
        case .cvv:
            sendStep("cvv")
            isNavigationAllowed = false
            textField.text = viewModel.cvv
            creditCardBackDelegate.setCVV(viewModel.cvv.isEmpty ? "" : viewModel.cvv)
            creditCardFrontDelegate.setCVV(viewModel.cvv.isEmpty ? "" : viewModel.cvv)
        case .expiryDate:
            sendStep("expiry")
            creditCardFrontDelegate.removeHighlightCircle()
            isNavigationAllowed = false
            textField.text = viewModel.expiryDate
            creditCardFrontDelegate.setExpirateDate(viewModel.expiryDate.isEmpty ? "" : viewModel.expiryDate)
        case .alias:
            sendStep("alias")
            self.isNextButtonEnabled = true
        case .cpfOfHolder:
            sendStep("cpf")
             isNavigationAllowed = false
        case .telOfholder:
            sendStep("phone")
            isNavigationAllowed = false

        }
        self.isPreviousButtonEnabled = true
        
        let textInField = textField.text ?? ""
        if !textInField.isEmpty {
            isNavigationAllowed = true
        }
    }
    
    func slidedTextFields(_ textField: UITextField, id: SlidedTextFieldsDelegate.Id, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldId = TextFields(rawValue: id) else { return true }
        var shouldChange:Bool = true
        let oldString = textField.text ?? ""
        let newString = (oldString as NSString).replacingCharacters(in: range, with: string)
        
        let clearTextFieldErrorIfNeeded:((UITextField) -> Void) = { (field) in
            if (field as? SkyFloatingLabelTextField)?.errorMessage != nil {
                (field as? SkyFloatingLabelTextField)?.errorMessage = nil
                self.isNextButtonEnabled = true
            }
        }
        clearTextFieldErrorIfNeeded(textField)
        
        switch textFieldId {
        case .number:
            if newString.count <= 23 {
                self.isNextButtonEnabled = false
                (textField as? SkyFloatingLabelTextField)?.errorMessage = nil
                let maskedString = CustomStringMask(mask: "0000 0000 0000 0000 000").maskedText(from: newString.onlyNumbers) ?? ""
                textField.text = maskedString
                viewModel.cardNumber = maskedString.replacingOccurrences(of: "[^0-9]", with: "", options: String.CompareOptions.regularExpression)
                creditCardFrontDelegate.setNumber(maskedString)
                backCardView.type = frontCardView.type
                configure(textField, with: newString, oldString: oldString, maskedString: maskedString, range: range, replacementString: string)
            }
            isNextButtonEnabled = true
            shouldChange = false
        case .name:
            if newString.count <= 26 {
                clearTextFieldErrorIfNeeded(textField)
                var name = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
                name = viewModel.sanitizedCardName(name)
                name = name.uppercased()
                textField.text = name
                creditCardFrontDelegate.setName(name)
                viewModel.name = name
                isNextButtonEnabled = true
            }
            shouldChange = false
        case .cvv:
            if newString.count <= 4 {
                self.isNextButtonEnabled = false
                (textField as? SkyFloatingLabelTextField)?.errorMessage = nil
                
                var maskedString: String!
                if frontCardView.type.id == CreditCardFlagTypeId.amex.rawValue {
                    maskedString = CustomStringMask(mask: "0000").maskedText(from: newString.onlyNumbers) ?? ""
                } else {
                    maskedString = CustomStringMask(mask: "000").maskedText(from: newString.onlyNumbers) ?? ""
                }
                textField.text = maskedString
                viewModel.cvv = maskedString.replacingOccurrences(of: "[^0-9]", with: "", options: String.CompareOptions.regularExpression)
                
                if frontCardView.type.id == CreditCardFlagTypeId.amex.rawValue {
                    creditCardFrontDelegate.setCVV(maskedString)
                } else {
                    creditCardBackDelegate.setCVV(maskedString)
                }
                configure(textField, with: newString, oldString: oldString, maskedString: maskedString, range: range, replacementString: string)
            }
            if newString.count >= 3 {
                isNextButtonEnabled = true
            }
            shouldChange = false
        case .expiryDate:
            if newString.count <= 7 {
                self.isNextButtonEnabled = false
                (textField as? SkyFloatingLabelTextField)?.errorMessage = nil
                let maskedString = CustomStringMask(mask: "00/0000").maskedText(from: newString.onlyNumbers) ?? ""
                textField.text = maskedString
                viewModel.expiryDate = maskedString
                creditCardFrontDelegate.setExpirateDate(maskedString)
                configure(textField, with: newString, oldString: oldString, maskedString: maskedString, range: range, replacementString: string)
            }
            
            if newString.count >= 7 {
                isNextButtonEnabled = true
            }
            
            shouldChange = false
        case .alias:
            viewModel.alias = newString
        case .cpfOfHolder:
            if newString.count <= 14 {
                self.isNextButtonEnabled = false
                (textField as? SkyFloatingLabelTextField)?.errorMessage = nil
                let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
                let maskedString = mask.maskedText(from: newString.onlyNumbers) ?? ""
                textField.text = maskedString
                viewModel.cpfOfHolder = maskedString.replacingOccurrences(of: "[^0-9]", with: "", options: String.CompareOptions.regularExpression)
                configure(textField, with: newString, oldString: oldString, maskedString: maskedString, range: range, replacementString: string)
                personalInfoCardViewDelegate.cpfDidTypping(maskedString)
            }
            if newString.count == 14 {
                isNextButtonEnabled = true
            }
            shouldChange = false
        case .telOfholder:
            let onlyNumbers = newString.replacingOccurrences(of: "[^0-9]", with: "", options: String.CompareOptions.regularExpression)
            if onlyNumbers.count <= 11 {
                self.isNextButtonEnabled = false
                (textField as? SkyFloatingLabelTextField)?.errorMessage = nil
                
                var maskedString = ""
                let phone = onlyNumbers
                if phone.count <= 10 {
                    maskedString = CustomStringMask(mask: "(00) 0000-0000").maskedText(from: phone) ?? ""
                } else if phone.count == 11 {
                    maskedString = CustomStringMask(mask: "(00) 00000-0000").maskedText(from: phone) ?? ""
                } else{
                    maskedString = phone
                }
                
                textField.text = maskedString
                viewModel.telOfholder = phone
                configure(textField, with: newString, oldString: oldString, maskedString: maskedString, range: range, replacementString: string)
            }
            if onlyNumbers.count >= 10 && onlyNumbers.count <= 11 {
                isNextButtonEnabled = true
            }
            shouldChange = false
        }
        
        return shouldChange
    }
    
    func slidedTextFieldsShouldReturn(_ textField: UITextField, id: SlidedTextFieldsDelegate.Id) -> Bool {
        guard let textFieldId = TextFields(rawValue: id) else { return true }
        
        switch textFieldId {
        case .number, .name, .expiryDate, .cvv, .alias:
            self.nextButtonAction(sender: nil)
        default: break
        }
        return true
    }
    
    func slidedTextFieldsShouldEndEditing(_ textField:UITextField, id:Id) -> Bool {
        guard let textFieldId = TextFields(rawValue: id) else { return true }
        if self.isBeingDismissed || !self.isMovingForward {
            return true
        }
        
        // set the errors messages
        switch textFieldId {
        case .number:
            if (textField.text ?? "").count == 0 {
                configureErrorMessage("Coloque o número", textField: textField)
                return false
            } else {
                if !creditCardFrontDelegate.isValidNumberOfCard() && !viewModel.flowEmergencyAid {
                    configureErrorMessage("Número do cartão é inválido", textField: textField)
                    return false
                }
            }
            let type = creditCardFrontDelegate.getCardType()
            viewModel.type = type
            creditCardBackDelegate.setType(type)
        case .name:
            if (textField.text ?? "").count == 0 || viewModel.name.isEmpty {
                configureErrorMessage("Entre com o nome que está no cartão", textField: textField)
                return false
            }
        case .expiryDate:
            if (textField.text ?? "").count == 0 {
                configureErrorMessage("Entre com a data de vencimento.", textField: textField)
                return false
            }
            if (textField.text ?? "").count >= 5 {
                let textFieldString = textField.text ?? ""
                let format = "MM/yyyy"
                let dateExpired = self.dateFormat(with: format).date(from: textFieldString)
                let dateString = self.dateFormat(with: format).string(from: Date())
                guard let date = self.dateFormat(with: format).date(from: dateString) else {
                    return false
                }
                
                if dateExpired == nil || dateExpired < date {
                    configureErrorMessage("Data de vencimento é inválida.", textField: textField)
                    return false
                }
            }
            if !self.isAmexCard() {
                flipCardToBackView()
            }
        case .cvv:
            let text = textField.text ?? ""
            if text.replacingOccurrences(of: "[^0-9]", with: "", options: String.CompareOptions.regularExpression).count == 0 {
                configureErrorMessage("Coloque o código de segurança válido", textField: textField)
                return false
            }

            // if not is amex
            if !self.isAmexCard() {
                flipCardToFrontView()
            }
        case .cpfOfHolder:
            let cpfText = textField.text ?? ""
            if cpfText.count == 0 {
                configureErrorMessage("Coloque com o CPF do dono do cartão", textField: textField)
                return false
            } else if cpfText.count != 14 {
                configureErrorMessage("Insira um CPF válido", textField: textField)
                return false
            }
            showPersonalImageView()
        case .telOfholder:
            if (textField.text ?? "").count == 0 {
                configureErrorMessage("Coloque o telefone do dono do cartão", textField: textField)
                return false
            }
            if (textField.text ?? "").count < 14 {
                configureErrorMessage("Coloque um telefone válido", textField: textField)
                return false
            }
        case .alias:
            showPersonalCardInfo()
        }
        return true
    }
    
    internal func configureErrorMessage(_ message: String, textField: UITextField) {
        (textField as? SkyFloatingLabelTextField)?.errorMessage = message
        isNextButtonEnabled = false
        shakeCard()
    }
}
