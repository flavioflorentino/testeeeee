//
//  PersonalInfoCard.swift
//  PicPay
//
//  Created by Lucas Romano on 13/06/2018.
//

import Foundation

protocol PersonalInfoCardViewDelegate: AnyObject {
    func nameDidTypping(_ name: String)
    func cpfDidTypping(_ cpf: String)
}

final class PersonalInfoCardView: UIView {
    
    @IBOutlet weak var cpfLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.setup()
    }
    
}

extension PersonalInfoCardView {
    
    fileprivate func setup() {
        guard let view = loadViewFromNib() else {
            return
        }
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.layer.cornerRadius = 5
        self.clipsToBounds = true
        self.layer.cornerRadius = 5
        self.backgroundColor = .clear
        self.sizeToFit()
        self.layoutIfNeeded()
        self.addSubview(view)
    }
    
    fileprivate func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: PersonalInfoCardView.self), bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
}

extension PersonalInfoCardView: PersonalInfoCardViewDelegate {
    
    func nameDidTypping(_ name: String) {
        self.nameLabel.text = name
    }
    
    func cpfDidTypping(_ cpf: String) {
        self.cpfLabel.text = cpf
    }
}
