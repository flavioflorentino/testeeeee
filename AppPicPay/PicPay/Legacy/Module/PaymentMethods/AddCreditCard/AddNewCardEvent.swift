import AnalyticsModule
import Foundation

enum AddNewCardEvent: AnalyticsKeyProtocol {
    case addNewCard(_ name: EventName, type: CardType)
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .addNewCard(let name, let type):
            return AnalyticsEvent("Visualizacao de tela", properties: ["nome": name.description, "type": type.description], providers: [.mixPanel, .appsFlyer, .firebase])
        }
    }
}

extension AddNewCardEvent {
    enum CardType: String, CustomStringConvertible {
        case debit = "debito"
        case credit = "credito"
        
        var description: String {
            return self.rawValue
        }
    }
    
    enum EventName: String, CustomStringConvertible {
        case addCard = "Cadastro de cartao"
        case addCardSuccess = "Conclusao do cadastro de cartao"
        
        var description: String {
            return self.rawValue
        }
    }
}
