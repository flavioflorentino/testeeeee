import Core
import Foundation
import PCI
import SecurityModule

final class AddNewCreditCardViewModel: NSObject {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    
    let origin: CreditCardType
    
    var type: CreditCardFlagType = CreditCardFlagType.unknown
    var cardNumber: String = ""
    var cvv: String = ""
    var name: String = ""
    var expiryDate: String = ""
    var alias: String = ""
    var cpfOfHolder: String = ""
    var telOfholder: String = ""
    var scannedNumber: String = ""
    var scannedCardHolder: String = ""
    var consumerAddressCard: ConsumerAddressItem?
    let api: AddNewCreditCardProtocol

    private let service: CardRegistrationServicing

    public var flowAddDebitCard: Bool {
        return origin == .recharge || origin == .emergencyAid
    }

    public var flowEmergencyAid: Bool {
        return origin == .emergencyAid
    }
    
    private let logDetectionService: LogDetectionServicing
    
    init(origin: CreditCardType,
         service: CardRegistrationServicing = CardRegistrationService(),
         logDetectionService: LogDetectionServicing = LogDetectionService()) {
        self.api = ApiPaymentMethods()
        self.origin = origin
        self.service = service
        self.logDetectionService = logDetectionService
    }
    
    init(origin: CreditCardType,
         api: AddNewCreditCardProtocol,
         service: CardRegistrationServicing = CardRegistrationService(),
         logDetectionService: LogDetectionServicing = LogDetectionService()) {
        self.api = api
        self.origin = origin
        self.service = service
        self.logDetectionService = logDetectionService
    }
    
    func validateCredicard(completion: @escaping (Error?, Bool?) -> Void) {
        /// Verify if number of textField is equal to number scanned
        let isScanned: Bool = self.cardNumber == self.scannedNumber
        
        if api.newEnvironmentEnabled {
            validateCreditCardPCI(isScanned: isScanned, completion: completion)
            return
        }
        
        let validateCard = createValidateCard()
        api.validateCreditCard(validateCard: validateCard) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let value):
                    completion(nil, value.askCpf)
                case .failure(let error):
                    completion(error, nil)
                }
            }
        }
    }
    
    private func validateCreditCardPCI(isScanned: Bool, completion: @escaping (Error?, Bool?) -> Void) {
        let currentCvv = flowEmergencyAid ? "123" : cvv
        let payload = CardPayload(
            cardHolder: name,
            cardNumber: cardNumber,
            expiryDate: expiryDate,
            cvv: currentCvv,
            cardFlagId: type.id,
            scanned: isScanned,
            origin: origin.rawValue,
            isDebitCard: flowAddDebitCard
        )
        
        service.validate(card: payload, completion: { result in
            DispatchQueue.main.async {
                switch result {
                case let .success(validation):
                    completion(nil, validation.askCpf)
                case let .failure(error):
                    completion(error.picpayError, nil)
                }
            }
        })
    }
    
    func saveCard(creditSuccess: @escaping() -> Void, debitSuccess: @escaping() -> Void, onError: @escaping (Error) -> Void) {
        guard let address = self.consumerAddressCard else {
            return
        }
       
        if api.newEnvironmentEnabled {
            saveCardPCI(address: address, creditSuccess: creditSuccess, debitSuccess: debitSuccess, onError: onError)
            return
        }

        let addressCard = createAddressCard(address: address)
        let insertCard = createInsertCard()
        api.insertCreditCard(addressCard: addressCard, insertCard: insertCard) { [weak self] response in
            DispatchQueue.main.async {
                switch response {
                case .success(let card):
                    self?.onSuccessSaveCard(card: card, creditSuccess: creditSuccess, debitSuccess: debitSuccess)
                case .failure(let error):
                    onError(error)
                }
            }
        }
    }
    
    private func onSuccessSaveCard(card: CardBank, creditSuccess: @escaping() -> Void, debitSuccess: @escaping() -> Void) {
        saveDefaultCard(card: card)
        saveCardCvv(card: card)
        reloadPaymentMethodsAndCreditCardList()
                          
        checkCardIsDebit(card: card) ? debitSuccess() : creditSuccess()
    }
    
    private func saveCardPCI(
        address: ConsumerAddressItem?,
        creditSuccess: @escaping() -> Void,
        debitSuccess: @escaping() -> Void,
        onError: @escaping (Error) -> Void
    ) {
        let isScanned = self.cardNumber == self.scannedNumber
        let currentCvv = flowEmergencyAid ? "123" : cvv
        let payload = CardPayload(
            cardHolder: name,
            cardNumber: cardNumber,
            expiryDate: expiryDate,
            cvv: currentCvv,
            cardFlagId: type.id,
            scanned: isScanned,
            origin: origin.rawValue,
            alias: alias,
            cpf: cpfOfHolder,
            phone: telOfholder,
            zipcode: address?.zipCode,
            city: address?.city,
            uf: address?.state,
            number: address?.streetNumber,
            complement: address?.addressComplement,
            street: address?.streetName,
            scannedCardHolder: scannedCardHolder,
            isDebitCard: flowAddDebitCard
        )
        
        service.save(card: payload, completion: { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case let .success(creditCard):
                    guard
                        let data = try? JSONSerialization.data(withJSONObject: creditCard.data, options: .prettyPrinted),
                        let card = try? JSONDecoder.decode(data, to: CardBank.self)
                        else {
                            let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
                            onError(error)
                            return
                    }
                    
                    self?.onSuccessSaveCard(card: card, creditSuccess: creditSuccess, debitSuccess: debitSuccess)
                case let .failure(error):
                    onError(error.picpayError)
                }
            }
        })
    }
    
    private func createValidateCard() -> ValidateCard {
        let isScanned: Bool = self.cardNumber == self.scannedNumber
        let currentCvv = flowEmergencyAid ? "123" : cvv
        return ValidateCard(
            cardHolder: name,
            cardNumber: cardNumber,
            expiryDate: expiryDate,
            cvv: currentCvv,
            cardFlagId: type.id,
            scanned: isScanned,
            origin: origin.rawValue,
            isDebitCard: flowAddDebitCard
        )
    }
    
    private func createInsertCard() -> InsertCard {
        let isScanned: Bool = self.cardNumber == self.scannedNumber
        let currentCvv = flowEmergencyAid ? "123" : cvv
        return InsertCard(
            cardHolder: name,
            cardNumber: cardNumber,
            expiryDate: expiryDate,
            cvv: currentCvv, alias: alias,
            cardFlagId: type.id,
            cpf: cpfOfHolder,
            phone: telOfholder,
            scanned: isScanned,
            origin: origin.rawValue,
            scannedCardHolder: scannedCardHolder,
            isDebitCard: flowAddDebitCard
        )
    }
    
    private func createAddressCard(address: ConsumerAddressItem) -> AddressCard {
        return AddressCard(
            zipCode: address.zipCode ?? "",
            city: address.city ?? "",
            uf: address.state ?? "",
            number: address.streetNumber ?? "",
            complement: address.addressComplement ?? "",
            street: address.streetName ?? "")
    }
    
    private func saveDefaultCard(card: CardBank) {
        guard let id = Int(card.id) else {
            return
        }
        
        CreditCardManager.shared.addCard(card)
        if checkCardIsDebit(card: card) {
            CreditCardManager.shared.saveDefaultDebitCard(card.id)
        } else {
            CreditCardManager.shared.saveDefaultCreditCardId(id)
        }
    }
    
    private func saveCardCvv(card: CardBank) {
        guard let id = Int(card.id) else {
            return
        }
        
        dependencies.keychain.set(key: KeychainKeyPF.cvv("\(id)"), value: self.cvv)
    }
    
    private func checkCardIsDebit(card: CardBank) -> Bool {
        return flowAddDebitCard && cardIsDebit(card: card)
    }
    
    private func cardIsDebit(card: CardBank) -> Bool {
        return card.type == .debit || card.type == .creditDebit
    }
    
    private func reloadPaymentMethodsAndCreditCardList() {
        /// reload list of credit card garanted information of inserted card
        DispatchQueue.global().async {
            CreditCardManager.shared.loadCardList({ (error) in
                DispatchQueue.main.async {
                    if error == nil {
                        NotificationCenter.default.post(name: Notification.Name.CrediCard.creditCardFlagLoaded, object: nil)
                        NotificationCenter.default.post(name: Notification.Name.Payment.methodChange, object: nil)
                    }
                }
            })
        }
    }
    
    public func sanitizedCardName(_ name: String) -> String {
        return name.replacingOccurrences(of: "[^a-zA-Z ]", with: "", options: .regularExpression)
    }
}

extension AddNewCreditCardViewModel {
    enum CreditCardType: String {
        case recharge = "Recarga"
        case emergencyAid = "Auxilio Emergencial"
        case payment = "Pagamento"
        case wallet = "Carteira"
        case deeplink = "Deeplink"
        case onboarding = "Onboarding"
        case none = "none"
    }
}

extension AddNewCreditCardViewModel {
    func sendLogDetection() {
        logDetectionService.sendLog()
    }
}
