//
//  CreditCardFlagType.swift
//  PicPay
//
//  Created by Lucas Romano Marquez Rizzi on 15/06/2018.
//

import Foundation

enum CreditCardFlagType {
    
    case visa(id: Int, regex: String)
    case master(id: Int, regex: String)
    case amex(id: Int, regex: String)
    case elo(id: Int, regex: String)
    case hipercard(id: Int, regex: String)
    case unknown
    
    static let allCards = [visa, master, elo, amex, hipercard]
    var id: Int {
        switch self {
        case let .visa(id, _): return id
        case let .master(id, _): return id
        case let .amex(id, _): return id
        case let .elo(id, _): return id
        case let .hipercard(id, _): return id
        case .unknown: return 0
        }
    }
    var regex: String {
        switch self {
        case let .visa(_, regex): return regex
        case let .master(_, regex): return regex
        case let .amex(_, regex): return regex
        case let .elo(_, regex): return regex
        case let .hipercard(_, regex): return regex
        case .unknown: return ""
        }
    }
    
    var image: String {
        switch self {
        case .visa: return "visa"
        case .master: return "mastercard"
        case .elo: return "elo"
        case .amex: return "amex"
        case .hipercard: return "hipercard"
        case .unknown: return ""
        }
    }
}

extension CreditCardFlagType: Equatable {
    
    static func == (lhs: CreditCardFlagType, rhs: CreditCardFlagType) -> Bool {
        switch (lhs, rhs) {
        case (.unknown , .unknown):
            return true
        case let (.visa(_id, _), .visa(id_, _)):
            return _id == id_
        case let (.master(_id, _), .master(id_, _)):
            return _id == id_
        case let (.amex(_id, _), .amex(id_, _)):
            return _id == id_
        case let (.elo(_id, _), .elo(id_, _)):
            return _id == id_
        case let (.hipercard(_id, _), .hipercard(id_, _)):
            return _id == id_
        default:
            return false
        }
    }
}
