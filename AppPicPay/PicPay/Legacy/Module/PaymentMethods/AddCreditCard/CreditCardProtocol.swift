import Foundation
import FeatureFlag

protocol CreditCardProtocol {
    var gradientLayer: CAGradientLayer? { get set }
    var listOfCreditCardTypes: [CreditCardFlagType] { get set }
    func setupColorOfCard(with flag: CreditCardFlagType, for view: UIView) -> CAGradientLayer
    func imageFlag(of flag: CreditCardFlagType) -> UIImage
    func flagType(of number: String) -> CreditCardFlagType
    func colorsOfCards(with flag: CreditCardFlagType) -> [CGColor]
}

extension CreditCardProtocol {

    func imageFlag(of flag: CreditCardFlagType) -> UIImage {
        if !flag.image.isEmpty, let image = UIImage(named: flag.image) {
            return image
        }
        return UIImage()
    }
    
    /// Return flag type of number
    ///
    /// - Parameter number: number of card
    /// - Returns: type of card
    func flagType(of number: String) -> CreditCardFlagType {
        guard !number.isEmpty else { return CreditCardFlagType.unknown }
        let numberOnly = number.replacingOccurrences(of: "[^0-9]", with: "", options: String.CompareOptions.regularExpression)
        let matchesRegex: ((String, String) -> Bool) = { (regex: String, text: String) -> Bool in
            do {
                let regex = try NSRegularExpression(pattern: regex, options: NSRegularExpression.Options.dotMatchesLineSeparators)
                let match = regex.matches(in: text, range: NSRange(location: 0, length: text.length))
                return match.count != 0
            } catch {
                return false
            }
        }
        
        // Validate Cards
        for card in self.listOfCreditCardTypes {
            if matchesRegex(card.regex, numberOnly) {
                return card
            }
        }

        return CreditCardFlagType.unknown
    }
    
    internal func getListOfCreditCardType() -> [CreditCardFlagType] {
        var listOfCards: [CreditCardFlagType] = []
        guard let jsonCreditCard = FeatureManager.json(.cardRegistration) else {
    return []
}
        guard let listCardDict = jsonCreditCard["cards"] as? [[String: Any]] else {
    return []
}
        for dict in listCardDict {
            guard let id = dict["id"] as? Int else {
    return []
}
            guard let regex = dict["regex"] as? String else {
    return []
}
            var type: CreditCardFlagType = .unknown
            switch id {
            case 1:
                type = CreditCardFlagType.visa(id: id, regex: regex)
            case 2:
                type = CreditCardFlagType.master(id: id, regex: regex)
            case 4:
                type = CreditCardFlagType.amex(id: id, regex: regex)
            case 5:
                type = CreditCardFlagType.elo(id: id, regex: regex)
            case 6:
                type = CreditCardFlagType.hipercard(id: id, regex: regex)
            default: break
            }
            listOfCards.append(type)
        }
        return listOfCards
    }
    
    /// Setup background gradient color for card
    ///
    /// - Parameters:
    ///   - flag: flag type
    ///   - view: view for apply gradient color
    /// - Returns: CAGradientLayer
    func setupColorOfCard(with flag: CreditCardFlagType, for view: UIView) -> CAGradientLayer {
        let colors = self.colorsOfCards(with: flag)
        let gradientLayer: CAGradientLayer = view.applyGradient(withColours: colors.map { UIColor(cgColor: $0) }, gradientOrientation: .vertical)
        gradientLayer.cornerRadius = 5
        return gradientLayer
    }
    
    /// Ger array cgColors for flag type
    ///
    /// - Parameter flag: flag type
    /// - Returns: array of cgColors
    func colorsOfCards(with flag: CreditCardFlagType) -> [CGColor] {
        var colors: [CGColor] = []
        switch flag {
        case .master:
            colors =  [#colorLiteral(red: 0.2862745098, green: 0.2862745098, blue: 0.2862745098, alpha: 1).cgColor, #colorLiteral(red: 0.0862745098, green: 0.0862745098, blue: 0.0862745098, alpha: 1).cgColor]
        case .visa:
            colors =  [#colorLiteral(red: 0.2588235294, green: 0.2705882353, blue: 0.6431372549, alpha: 1).cgColor, #colorLiteral(red: 0.1254901961, green: 0.1333333333, blue: 0.3843137255, alpha: 1).cgColor]
        case .elo:
            colors =  [#colorLiteral(red: 0.2862745098, green: 0.2862745098, blue: 0.2862745098, alpha: 1).cgColor, #colorLiteral(red: 0.0862745098, green: 0.0862745098, blue: 0.0862745098, alpha: 1).cgColor]
        case .amex:
            colors =  [#colorLiteral(red: 0.2431372549, green: 0.6392156863, blue: 0.8431372549, alpha: 1).cgColor, #colorLiteral(red: 0.003921568627, green: 0.3843137255, blue: 0.7450980392, alpha: 1).cgColor]
        case .hipercard:
            colors =  [#colorLiteral(red: 0.8549019608, green: 0.2274509804, blue: 0.231372549, alpha: 1).cgColor, #colorLiteral(red: 0.7294117647, green: 0.1176470588, blue: 0.1215686275, alpha: 1).cgColor]
        case .unknown:
            colors =  [#colorLiteral(red: 0.2862745098, green: 0.2862745098, blue: 0.2862745098, alpha: 1).cgColor, #colorLiteral(red: 0.0862745098, green: 0.0862745098, blue: 0.0862745098, alpha: 1).cgColor]
        }
        
        return colors
    }
    
    /// Check validity
    /// reference: https://en.wikipedia.org/wiki/Luhn_algorithm
    ///
    /// - Parameter number: number of card
    /// - Returns: is valid or not
    func validateCardNumber(number: String) -> Bool {
        guard let jsonCreditCard = FeatureManager.json(.cardRegistration) else { return true }
        guard let validadeCard = jsonCreditCard["local_validation_number_card"] as? Bool else { return true }
        guard validadeCard else { return true }
        let numberOnly = number.replacingOccurrences(of: "[^0-9]", with: "", options: String.CompareOptions.regularExpression)
        var sum = 0
        let digitStrings = numberOnly.reversed().map { String($0) }
        for tuple in digitStrings.enumerated() {
            guard let digit = Int(tuple.element) else {
    return false
}
            let odd = tuple.offset % 2 == 1
            switch (odd, digit) {
            case (true, 9):
                sum += 9
            case (true, 0...8):
                sum += (digit * 2) % 9
            default:
                sum += digit
            }
        }
        return sum % 10 == 0
    }

}
