import Foundation

protocol OfferCardsWorkerProtocol {
    var cards: [OfferCard] { get }
}
