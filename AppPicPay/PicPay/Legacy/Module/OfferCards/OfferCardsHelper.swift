import Core
import Foundation

final class OfferCardsHelper {
    private let kvStore = KVStore()
    
    func close(_ card: OfferCard) {
        if let cardIdentifier = card.identifier {
            var closedIdentifiers: [String] = self.closedIdentifiers()
            closedIdentifiers.append(cardIdentifier)
            kvStore.set(value: closedIdentifiers, with: .listOfferCardsWalletDismissed)
        }
    }
    
    func showed(_ card: OfferCard) -> Bool {
        if let identifiers = kvStore.objectForKey(.listOfferCardsWalletDismissed) as? [String],
            let identifier = card.identifier {
            return !identifiers.contains(identifier)
        }
        return true
    }
    
    private func closedIdentifiers() -> [String] {
        guard let identifiers = kvStore.objectForKey(.listOfferCardsWalletDismissed) as? [String] else {
            return []
        }
        return identifiers
    }
}
