import Foundation
import UIKit
import FeatureFlag
import AsyncDisplayKit

final class OfferCardsWorker: OfferCardsWorkerProtocol {
    let cards: [OfferCard]
    
    init?() {
        guard let dict: [String: Any] = FeatureManager.json(.featureWalletCards) else {
            return nil
        }
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            cards = try JSONDecoder(.convertFromSnakeCase).decode([OfferCard].self, from: jsonData)
        } catch {
            // not exist this tag or content in remote config or exist error in json object
            let message: String = "Erro ao fazer parse da lista de cards do credito do remote config"
            Log.error(.model, message)
            return nil
        }
    }
}
