import Foundation

struct OfferCard: Codable {
    let title: String
    let titleColor: String?
    let descriptionColor: String?
    let backgroundColor: String?
    let description: String
    let imgUrl: URL?
    let action: Action?
    let identifier: String?
    let rules: [Rule]?
    let eventOnDisplay: String?
}

extension OfferCard {
    struct Action: Codable {
        let title: String
        let deeplink: String?
        let color: String?
        let event: String?
    }
}

extension OfferCard {
    enum Rule: String, Codable {
        case picpayCreditEnableForRegistration = "picpay_credit_enable_for_registration"
        case picpayDebitEnableForRegistration = "picpay_debit_enable_for_registration"
        case picpayCreditActive = "picpay_credit_active"
        case upgradeAccountCardLimit = "upgrade_account_card_limit"
        case upgradeAccountCardLess = "upgrade_account_card_less_5k"
        case upgradeAccountCardMore = "upgrade_account_card_more_5k"
        case judiciallyBlocked = "account_judicially_blocked"
        case pepLimited = "account_pep_limited"
        case pepRestricted = "account_pep_restricted"
        case cardEnableForRegistration = "picpay_card_enable_for_registration"
        case cardDebitEnableForRegistration = "picpay_card_debit_enable_for_registration"
        case cardEnableForRequestPhysycalCard = "picpay_card_enable_for_request"
        case cardEnableForRequestMigration = "picpay_card_enable_for_request_migration"
        case cardEnableForActivateDebit = "picpay_card_enable_for_activate_debit"
        case cardEnableForDebitRequestInProgress = "picpay_card_enable_for_debit_request_in_progress"
        case cardEnableForTracking = "picpay_card_enable_for_tracking"
        case upgradeDebitToCredit = "picpay_card_upgrade_debit_to_credit_offer"
        case simplifiedDebitRegistration = "picpay_card_simplified_debit_registration"
        case none
        
        init(from decoder: Decoder) throws {
            self = try Rule(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .none
        }
    }
}
