import Foundation
import FeatureFlag

final class OfferCardsValidation {
    typealias Dependencies = HasFeatureManager
    
    private var dependencies: Dependencies
    private let creditAccount: CPAccount?
    private let accountInfo: DigitalAccountInfo
    
    init(creditAccount: CPAccount?, accountInfo: DigitalAccountInfo, dependencies: Dependencies = DependencyContainer()) {
        self.creditAccount = creditAccount
        self.accountInfo = accountInfo
        self.dependencies = dependencies
    }
    
    func isGranted(for list: [OfferCard.Rule]) -> Bool {
        for rule in list {
            if !isGranted(for: rule) {
                return false
            }
        }
        return true
    }
    
    private func isGranted(for rule: OfferCard.Rule) -> Bool {
        switch rule {
        case .upgradeAccountCardLimit:
            return accountInfo.offerUpgradeAccount == .cardLimit
        case .upgradeAccountCardLess:
            return accountInfo.offerUpgradeAccount == .cardLess
        case .upgradeAccountCardMore:
            return accountInfo.offerUpgradeAccount == .cardMore
        case .judiciallyBlocked:
            return accountInfo.isJudiciallyBlocked
        case .pepLimited:
            return accountInfo.status == .some(.limited) && accountInfo.isPep
        case .pepRestricted:
            return accountInfo.registerStatus == .some(.manual)
        case .picpayCreditActive:
            return isGrantedForPicpayCreditActive()
        case .picpayCreditEnableForRegistration:
            return isGrantedForPicpayCreditEnableForRegistration()
        case .picpayDebitEnableForRegistration:
            return isGrantedForPicpayDebitEnableForRegistration()
        case .cardEnableForRegistration:
            return isGrantedForCardEnableForRegistration()
        case .cardDebitEnableForRegistration:
            return isGrantedForCardDebitEnableForRegistration()
        case .cardEnableForRequestPhysycalCard:
            return isGrantedForRequestPhysycalCard()
        case .cardEnableForRequestMigration:
            return isGrantedForCardEnableForRequestMigration()
        case .cardEnableForActivateDebit:
            return isGrantedForCardEnableForActivateDebit()
        case .cardEnableForDebitRequestInProgress:
            return isGrantedForDebitRequestInProgress()
        case .cardEnableForTracking:
            return isGrantedForCardTracking()
        case .upgradeDebitToCredit:
            return isGrantedForUpgradeDebitToCredit()
        case .simplifiedDebitRegistration:
            return isGrantedSimplifiedDebitRegistration()
        case .none:
            return false
        }
    }
    private func isGrantedForPicpayCreditActive() -> Bool {
        guard
            let creditAccount = creditAccount,
            creditAccount.offerEnabled
            else {
            return false
        }
        return creditAccount.creditStatus == .active

    }
    private func isGrantedForPicpayCreditEnableForRegistration() -> Bool {
        return creditAccount?.settings.banners.contains(.cardOffer) ?? false
    }
    private func isGrantedForPicpayDebitEnableForRegistration() -> Bool {
        return creditAccount?.settings.banners.contains(.cardDebitOffer) ?? false
            && dependencies.featureManager.isActive(.flagCardDebitOffer)
    }
    private func isGrantedForCardEnableForRegistration() -> Bool {
        return creditAccount?.settings.banners.contains(.physicalCardActivation) ?? false
    }
    private func isGrantedForCardDebitEnableForRegistration() -> Bool {
        return creditAccount?.settings.banners.contains(.physicalDebitCardActivation) ?? false
            && dependencies.featureManager.isActive(.flagCardDebitOffer)
    }
    private func isGrantedForRequestPhysycalCard() -> Bool {
        return creditAccount?.settings.banners.contains(.askPhysicalCard) ?? false
    }
    private func isGrantedForCardEnableForRequestMigration() -> Bool {
        return creditAccount?.settings.banners.contains(.migrationPosCard) ?? false
    }
    private func isGrantedForCardEnableForActivateDebit() -> Bool {
        return creditAccount?.settings.banners.contains(.cardDebitActivation) ?? false
    }
    private func isGrantedForDebitRequestInProgress() -> Bool {
        return creditAccount?.settings.banners.contains(.debitRequestInProgress) ?? false
            && dependencies.featureManager.isActive(.flagCardDebitOffer)
    }
    private func isGrantedForCardTracking() -> Bool {
        return creditAccount?.settings.banners.contains(.cardTracking) ?? false
            && dependencies.featureManager.isActive(.opsCardTracking)
    }
    
    private func isGrantedForUpgradeDebitToCredit() -> Bool {
        creditAccount?.settings.banners.contains(.upgradeDebitToCredit) ?? false
    }
    
    private func isGrantedSimplifiedDebitRegistration() -> Bool {
        creditAccount?.settings.banners.contains(.simplifiedDebitRegistration) ?? false && dependencies.featureManager.isActive(.simplifiedDebitRegister)
    }
}
