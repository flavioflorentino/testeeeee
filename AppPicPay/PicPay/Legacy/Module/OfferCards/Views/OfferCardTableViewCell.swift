import UI
import UIKit

final class OfferCardTableViewCell: UITableViewCell {
    weak var delegate: RemoteCardTableViewCellDelegate?
    
    private var minimumScaleFactorButton: CGFloat = 0.7
    
    var card: OfferCard? {
        didSet {
            guard let card = card else {
            return
        }
            titleLabel.text = card.title
            descriptionLabel.text = card.description
            setBackgroundColor()
            if let url = card.imgUrl {
                remoteImageView.isHidden = false
                remoteImageView.setImage(url: url)
            } else {
                remoteImageView.isHidden = true
            }
            if let titleColor = Palette.hexColor(with: card.titleColor ?? "") {
                titleLabel.textColor = titleColor
            }
            if let descriptionColor = Palette.hexColor(with: card.descriptionColor ?? "") {
                descriptionLabel.textColor = descriptionColor
            }
            if let backgroundColor = Palette.hexColor(with: card.backgroundColor ?? "") {
                containerView.backgroundColor = backgroundColor
            }
            if let action = card.action {
                actionButton.isHidden = false
                actionButton.setTitle(action.title, for: .normal)
                if let actionColor = Palette.hexColor(with: action.color ?? "") {
                    actionButton.setTitleColor(actionColor, for: .normal)
                }
            } else {
                actionButton.isHidden = true
            }
            closeButton.isHidden = card.identifier == nil
        }
    }
    
    private var gradientLayer: CALayer?
    
    @IBOutlet weak private var closeButton: UIButton!
    @IBOutlet weak private var actionButton: UIButton! {
        didSet {
            actionButton.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
            actionButton.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .bold)
            actionButton.titleLabel?.minimumScaleFactor = minimumScaleFactorButton
            actionButton.titleLabel?.adjustsFontSizeToFitWidth = true
        }
    }
    @IBOutlet weak private var containerView: UIView! {
        didSet {
            containerView.layer.cornerRadius = 5
            containerView.dropShadow()
            containerView.backgroundColor = .white
        }
    }
    @IBOutlet weak private var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = Palette.ppColorGrayscale500.color
            titleLabel.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        }
    }
    @IBOutlet weak private var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.textColor = Palette.ppColorGrayscale300.color
            descriptionLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        }
    }
    @IBOutlet weak private var remoteImageView: UIImageView! {
        didSet {
            remoteImageView.contentMode = .scaleAspectFit
        }
    }
    
    @IBAction private func actionButtonTapped(_ sender: Any) {
        delegate?.didTapRemoteCard(in: self)
    }
    @IBAction private func closeButtonTapped(_ sender: Any) {
        delegate?.closeButtonTapped(in: self)
    }
    
    @IBOutlet weak var containerViewLeading: NSLayoutConstraint!
    @IBOutlet weak var containerViewTrailing: NSLayoutConstraint!
    @IBOutlet weak var containerViewBottom: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        selectionStyle = .none
    }
    
    func configureLayoutV1() {
        containerView.viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.backgroundColor, Colors.backgroundPrimary.color)
            .with(\.border, .light(color: .grayscale100()))
        
        titleLabel.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale700.color)
        
        descriptionLabel.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale500.color)
        
        actionButton.buttonStyle(LinkButtonStyle())
            .with(\.textAlignment, .left)
        
        containerViewLeading.constant = Spacing.base02
        containerViewTrailing.constant = Spacing.base02
        containerViewBottom.constant = Spacing.base01
        
        containerView.layer.shadowColor = nil
        containerView.layer.shadowPath = nil
        containerView.layer.shadowOpacity = 0
    }
    
    private func setBackgroundColor() {
        guard let backgroundColor = card?.backgroundColor else {
            return
        }
        let trimmingString = backgroundColor.removingWhiteSpaces()
        let grantedColors: [UIColor] = trimmingString.components(separatedBy: ",").compactMap { Palette.hexColor(with: $0) }
        if grantedColors.count == 1 {
            containerView.backgroundColor = grantedColors[0]
            return
        }
        containerView.clipsToBounds = true
        if gradientLayer == nil {
            gradientLayer = containerView.applyGradient(withColours: grantedColors, gradientOrientation: .horizontal)
        }
    }
}
