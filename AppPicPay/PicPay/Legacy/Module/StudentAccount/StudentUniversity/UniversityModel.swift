struct FixNestedDecodableToAcceptArray: Codable {
    let data: [University]
}

struct University: Codable {
    let code: String
    let name: String
    let acronym: String?
    let state: String
    let city: String?
    
    var cellTitle: String {
        if let acronym = acronym {
            return "\(name) - \(acronym)"
        }
        return name
    }
    
    var cellSubtitle: String {
        if let city = city {
            return "\(state), \(city)"
        }
        return state
    }
    
    var payload: [String: Any] {
        let params: [String: Any] = ["learning_institution": toString() ?? ""]
        return params
    }
}
