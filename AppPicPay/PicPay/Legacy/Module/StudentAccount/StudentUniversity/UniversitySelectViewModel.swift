import AnalyticsModule

final class UniversitySelectViewModel: NSObject {
    private let coordinator: StudentAccountCoordinator
    private var selected: University?
    private var originalData: [University] = []
    private var filtered: [University] = []
    private var isFiltering = false
    private var data: [University] {
        return isFiltering ? filtered : originalData
    }

    var reloadTableView: () -> Void = { }
    var update: (UniversitySelectView.State) -> Void = { _ in }
    var didSelectUniversity: (String) -> Void = { _ in }
    
    init(with coordinator: StudentAccountCoordinator) {
        self.coordinator = coordinator
        super.init()
    }
    
    func loadUniversities() {
        update(.loading)
        UniversitySelectWorker().load { [weak self] result in
            switch result {
            case .success(let universities):
                self?.originalData = universities.data
                self?.update(.needInput)
            case .failure:
                self?.update(.errorLoading)
            }
        }
    }
    
    func proceed() {
        analyticsSelectedUniversity()
        coordinator.pushDocuments(with: selected)
    }
    
    func close() {
        coordinator.close()
    }
    
    func analytics() {
        Analytics.shared.log(StudentAccountEvent.university)
    }
    
    private func analyticsSelectedUniversity() {
        if let university = selected {
            Analytics.shared.log(StudentAccountEvent.universitySelected(name: university.name, code: university.code))
        }
    }
    
    private func needInput() {
        isFiltering = false
        update(.needInput)
    }
    
    private func filter(with searchText: String) {
        isFiltering = true
        filtered = filterOriginalData(with: searchText)
        reloadTableView()
        checkNotFound()
    }
    
    private func filterOriginalData(with searchText: String) -> [University] {
        return originalData.filter { $0.name.localizedStandardContains(searchText) ||
                                     $0.acronym?.localizedStandardContains(searchText) ?? false ||
                                     $0.city?.localizedStandardContains(searchText) ?? false ||
                                     $0.state.localizedStandardContains(searchText)
        }
    }
    
    private func checkNotFound() {
        guard isFiltering && !filtered.isEmpty else {
            update(.notFound)
            return
        }
        update(.showingResult)
    }
}

extension UniversitySelectViewModel: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        needInput()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            needInput()
            return
        }
        filter(with: searchText)
    }
}

extension UniversitySelectViewModel: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: UniversitySelectCell.identifier, for: indexPath) as? UniversitySelectCell else {
            return UITableViewCell()
        }
        
        let university = data[indexPath.row]
        cell.update(title: university.cellTitle, subtitle: university.cellSubtitle)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let university = data[indexPath.row]
        selected = university
        didSelectUniversity(university.name)
    }
}

final class UniversitySelectWorker {
    func load(_ completion: @escaping (PicPayResult<FixNestedDecodableToAcceptArray>) -> Void) {
        let endpoint = "studentaccount/learning-institutions"
        RequestManager.shared.apiRequest(endpoint: endpoint, method: .get).responseApiCodable(completionHandler: completion)
    }
}
