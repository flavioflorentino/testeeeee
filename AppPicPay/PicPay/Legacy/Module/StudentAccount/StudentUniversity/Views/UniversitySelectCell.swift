import UI

final class UniversitySelectCell: UITableViewCell {
    private enum Layout {
        static let titleColor = Palette.ppColorGrayscale400.color
        static let subtitleColor = Palette.ppColorGrayscale400.color
        static let backgroundColor = Palette.ppColorGrayscale100.color
        static let titleFont = UIFont.systemFont(ofSize: 14, weight: .regular)
        static let subtitleFont = UIFont.systemFont(ofSize: 12, weight: .regular)
    }
    
    static let identifier = String(describing: UniversitySelectCell.self)
    
    private lazy var title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.titleFont
        label.textColor = Layout.titleColor
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var subtitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.subtitleFont
        label.textColor = Layout.subtitleColor
        label.numberOfLines = 0
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        defaultInit()
        addComponents()
        layoutComponents()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func defaultInit() {
        backgroundColor = Layout.backgroundColor
    }
    
    func update(title: String, subtitle: String) {
        self.title.text = title
        self.subtitle.text = subtitle
    }
    
    private func addComponents() {
        contentView.addSubview(title)
        contentView.addSubview(subtitle)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.leadingTrailingCenterX(equalTo: contentView, for: [title, subtitle], constant: 33)
        
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            subtitle.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 8),
            subtitle.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5)
        ])
    }
}
