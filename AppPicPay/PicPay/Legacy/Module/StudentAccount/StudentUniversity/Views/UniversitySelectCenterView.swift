import UI

final class UniversitySelectCenterView: UIView {
    private enum Layout {
        static let boldColor = Palette.ppColorGrayscale500.color
        static let regularColor = Palette.ppColorGrayscale400.color
        static let backgroundColor = Palette.ppColorGrayscale100.color
        
        static let boldFont = UIFont.systemFont(ofSize: 16, weight: .bold)
        static let regularFont = UIFont.systemFont(ofSize: 14, weight: .regular)
        
        static let edgeInset: CGFloat = UIScreen.isSmall ? 10 : 33
        static let titleTopSpacing: CGFloat = UIScreen.isSmall ? 2 : 17
        static let subtitleTopSpacing: CGFloat = UIScreen.isSmall ? 2 : 12
    }
    
    enum State {
        case needInput
        case noResult
        case error
        
        var title: String {
            switch self {
            case .needInput:
                return StudentLocalizable.selectUniversityNeedInputTitle.text
            case .noResult:
                return StudentLocalizable.selectUniversitNoResultsTitle.text
            case .error:
                return StudentLocalizable.errorTitle.text
            }
        }
        
        var subtitle: String? {
            switch self {
            case .needInput:
                return nil
            case .noResult:
                return StudentLocalizable.selectUniversitNoResultsSubtitle.text
            case .error:
                return StudentLocalizable.errorSubtitle.text
            }
        }
        
        var image: UIImage {
            switch self {
            case .needInput:
                return #imageLiteral(resourceName: "icon_search")
            case .noResult:
                return #imageLiteral(resourceName: "icon_not_found")
            case .error:
                return #imageLiteral(resourceName: "sad-3")
            }
        }
        
        var titleStyle: (font: UIFont, color: UIColor) {
            switch self {
            case .needInput:
                return (font: Layout.regularFont, color: Layout.regularColor)
            default:
                return (font: Layout.boldFont, color: Layout.boldColor)
            }
        }
    }
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var subtitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.regularFont
        label.textColor = Layout.regularColor
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    init(state: State) {
        super.init(frame: .zero)
        self.imageView.image = state.image
        self.title.text = state.title
        self.subtitle.text = state.subtitle
        
        defaultInit()
        update(state: state)
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func defaultInit() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = Layout.backgroundColor
    }
    
    func convertState(from stateSelectView: UniversitySelectView.State) -> State {
        switch stateSelectView {
        case .needInput:
            return .needInput
        case .errorLoading:
            return .error
        case .notFound:
            return .noResult
        default:
            return .error
        }
    }
    
    func update(stateSelectView: UniversitySelectView.State) {
        update(state: convertState(from: stateSelectView))
    }
    
    private func update(state: State) {
        imageView.image = state.image
        title.text = state.title
        
        if let text = state.subtitle {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 4
            paragraphStyle.alignment = .center
            subtitle.attributedText = NSMutableAttributedString(string: text, attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        }
        subtitle.isHidden = state.subtitle == nil
        
        updateStyling(with: state)
    }
    
    private func updateStyling(with state: State) {
        title.font = state.titleStyle.font
        title.textColor = state.titleStyle.color
    }
    
    private func addComponents() {
        addSubview(imageView)
        addSubview(title)
        addSubview(subtitle)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor),
            imageView.heightAnchor.constraint(equalToConstant: 90),
            imageView.widthAnchor.constraint(equalToConstant: 90),
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            title.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: Layout.titleTopSpacing),
            title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Layout.edgeInset),
            title.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Layout.edgeInset),
            
            subtitle.topAnchor.constraint(equalTo: title.bottomAnchor, constant: Layout.subtitleTopSpacing),
            subtitle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Layout.edgeInset),
            subtitle.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Layout.edgeInset)
        ])
    }
}
