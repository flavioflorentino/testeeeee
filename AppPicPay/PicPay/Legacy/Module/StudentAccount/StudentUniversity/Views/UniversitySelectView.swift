import UI

final class UniversitySelectView: UIView {
    private enum Layout {
        static let titleColor = Palette.ppColorGrayscale600.color
        static let subtitleColor = Palette.ppColorGrayscale400.color
        static let tipColor = Palette.ppColorGrayscale400.color
        static let backgroundColor = Palette.ppColorGrayscale100.color
        static let titleFont = UIFont.systemFont(ofSize: 18, weight: .medium)
        static let subtitleFont = UIFont.systemFont(ofSize: 14, weight: .regular)
        static let tipFont = UIFont.systemFont(ofSize: 14, weight: .regular)        
        static let centerViewTopSpacing: CGFloat = UIScreen.isSmall ? 3 : 40
        static let titleTopSpacing: CGFloat = UIScreen.isSmall ? 3 : 30
    }
    
    enum State {
        case loading
        case errorLoading
        case needInput
        case notFound
        case showingResult
        case selectedUniversity
    }
    
    private enum SearchBarStyle {
        case selected
        case `default`
    }
    
    private var defaultIcon = Assets.Icons.search.image.withRenderingMode(.alwaysOriginal)
    private let selectedIcon = Assets.Icons.iconChecked.image.withRenderingMode(.alwaysOriginal)
    
    private lazy var title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.titleFont
        label.textColor = Layout.titleColor
        label.numberOfLines = 0
        label.text = StudentLocalizable.selectUniversityTitle.text
        label.textAlignment = .center
        return label
    }()
    
    private lazy var subtitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.subtitleFont
        label.textColor = Layout.subtitleColor
        label.numberOfLines = 0
        label.text = StudentLocalizable.selectUniversitySubtitle.text
        label.textAlignment = .center
        return label
    }()
    
    lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.searchBarStyle = UISearchBar.Style.minimal
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.placeholder = StudentLocalizable.selectUniversitySearch.text
        searchBar.showsCancelButton = false
        return searchBar
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.isHidden = true
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100.0
        tableView.tableFooterView = UIView()
        tableView.keyboardDismissMode = .onDrag
        return tableView
    }()
    
    lazy var button: UIPPButton = {
        let button = UIPPButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.cornerRadius = 22
        button.heightAnchor.constraint(equalToConstant: 48).isActive = true
        button.configure(with: Button(title: DefaultLocalizable.advance.text, type: .cta))
        return button
    }()
    
    private lazy var centerView: UniversitySelectCenterView = {
        let view = UniversitySelectCenterView(state: .needInput)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = false
        return view
    }()
    
    lazy var loadingError: StudentLoadingError = {
        let loadingError = StudentLoadingError()
        addSubview(loadingError)
        NSLayoutConstraint.constraintAllEdges(from: loadingError, to: self)
        loadingError.isHidden = true
        return loadingError
    }()
    
    lazy var loading: UILoadView = {
        let loading = UILoadView(superview: self)
        loading.isHidden = true
        return loading
    }()
    
    init() {
        super.init(frame: .zero)
        
        defaultInit()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(_ state: State, with text: String? = nil) {
        setSearchBarIcon(to: .default)
        
        switch state {
        case .showingResult:
            show(tableView)
            hide(button, centerView, loading, loadingError)
            
        case .selectedUniversity:
            show(button)
            hide(tableView, centerView, loading, loadingError)
            setSearchBarIcon(to: .selected)
            searchBar.text = text
            
        case .loading:
            show(loading)
            hide(tableView, button, loadingError, centerView)
            
        case .errorLoading:
            show(loadingError)
            hide(tableView, button, loading, centerView)
            
        case .needInput, .notFound:
            show(centerView)
            hide(tableView, button, loading, loadingError)
            centerView.update(stateSelectView: state)
        }
    }
    
    private func defaultInit() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = Layout.backgroundColor
        update(.showingResult)
    }
    
    private func addComponents() {
        addSubview(title)
        addSubview(subtitle)
        addSubview(searchBar)
        addSubview(tableView)
        addSubview(button)
        addSubview(centerView)
        addSubview(loading)
        addSubview(loadingError)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.leadingTrailingCenterX(equalTo: self, for: [title, subtitle, searchBar, button], constant: 17)
        NSLayoutConstraint.leadingTrailing(equalTo: self, for: [tableView, centerView])
        
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: topAnchor, constant: Layout.titleTopSpacing),
            subtitle.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 14),
            searchBar.topAnchor.constraint(equalTo: subtitle.bottomAnchor, constant: 18),
            searchBar.heightAnchor.constraint(equalToConstant: 40),
            tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 17),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            button.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 17),
            centerView.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: Layout.centerViewTopSpacing)
        ])
    }
    
    private func setSearchBarIcon(to style: SearchBarStyle) {
        switch style {
        case .selected:
            searchBar.setImage(selectedIcon, for: .search, state: .normal)
        case .default:
            searchBar.setImage(defaultIcon, for: .search, state: .normal)
        }
    }
}
