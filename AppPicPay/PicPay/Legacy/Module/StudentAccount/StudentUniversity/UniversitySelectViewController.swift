final class UniversitySelectViewController: UIViewController {
    private let viewModel: UniversitySelectViewModel
    
    private lazy var selectView: UniversitySelectView = {
        let selectView = UniversitySelectView()
        selectView.translatesAutoresizingMaskIntoConstraints = false
        selectView.button.addTarget(self, action: #selector(proceed), for: .touchUpInside)
        return selectView
    }()
    
    init(with viewModel: UniversitySelectViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        setupViewController()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.analytics()
    }
    
    private func setupViewController() {
        addComponents()
        addConstraints()
        setupRightBarButton()
        setupViewModel()
        
        selectView.tableView.dataSource = viewModel
        selectView.tableView.delegate = viewModel
        selectView.searchBar.delegate = viewModel
        selectView.tableView.register(UniversitySelectCell.self, forCellReuseIdentifier: UniversitySelectCell.identifier)
        
        viewModel.loadUniversities()
    }
    
    private func setupRightBarButton() {
        let barButton = UIBarButtonItem(
            title: DefaultLocalizable.btClose.text,
            style: .plain,
            target: self,
            action: #selector(close)
        )
        navigationItem.rightBarButtonItem = barButton
    }
    
    private func setupViewModel() {
        viewModel.update = { [weak self] state in
            self?.selectView.update(state)
        }
        
        viewModel.didSelectUniversity = { [weak self] university in
            self?.selectView.update(.selectedUniversity, with: university)
        }
        
        viewModel.reloadTableView = { [weak self] in
            self?.selectView.tableView.reloadData()
        }
        
        selectView.loadingError.tryAgain = { [weak self] in
            self?.selectView.update(.errorLoading)
            self?.viewModel.loadUniversities()
        }
    }
    
    private func addComponents() {
        view.addSubview(selectView)
    }
    
    private func addConstraints() {
        NSLayoutConstraint.constraintAllEdges(from: selectView, to: view)
    }
    
    @objc
    private func proceed() {
        viewModel.proceed()
    }
    
    @objc
    private func close() {
        viewModel.close()
    }
}
