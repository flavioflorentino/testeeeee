import AnalyticsModule

final class StudentStatusApprovedViewModel {
    var coordinator: StudentAccountCoordinator
    let payload: StatusPayload
    
    init(with coordinator: StudentAccountCoordinator, payload: StatusPayload) {
        self.coordinator = coordinator
        self.payload = payload
        analytics()
    }
    
    func analytics() {
        Analytics.shared.log(StudentAccountEvent.activatedAccount)
    }
    
    func openBenefits() {
        coordinator.openBenefits()
    }
    
    func openRules() {
        coordinator.openRules()
    }
    
    func close() {
        coordinator.close()
    }
}
