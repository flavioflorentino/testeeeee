final class StudentStatusApprovedViewController: UIViewController {
    private let viewModel: StudentStatusApprovedViewModel
    
    private lazy var approvedView: StudentStatusApprovedView = {
        let approvedView = StudentStatusApprovedView()
        view.addSubview(approvedView)
        NSLayoutConstraint.constraintAllEdges(from: approvedView, to: view)
        return approvedView
    }()
    
    init(with viewModel: StudentStatusApprovedViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        setupViewController()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViewController() {
        setupRightBarButton()
        setupViewModel()
        setupView()
    }
    
    private func setupView() {
        approvedView.update(with: viewModel.payload)
    }
    
    private func setupRightBarButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.btClose.text, style: .plain, target: self, action: #selector(close))
    }
    
    private func setupViewModel() {
        approvedView.benefits = { [weak self] in
            self?.viewModel.openBenefits()
        }
        
        approvedView.rules = { [weak self] in
            self?.viewModel.openRules()
        }
    }
    
    @objc
    private func close() {
        viewModel.close()
    }
}
