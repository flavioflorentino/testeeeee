import UI

final class StudentStatusApprovedView: UIView {
    private enum Layout {
        static let titleColor = Palette.ppColorGrayscale600.color
        static let subtitleColor = Palette.ppColorGrayscale500.color
        static let titleFont = UIFont.systemFont(ofSize: 18, weight: .medium)
        static let subtitleFont = UIFont.systemFont(ofSize: 14)
    }
    
    var benefits: () -> Void = { }
    var rules: () -> Void = { }
    let placeholder = StudentStatus.active.image
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.titleFont
        label.textColor = Layout.titleColor
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var subtitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.subtitleFont
        label.textColor = Layout.subtitleColor
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var benefitsButton: UIPPButton = {
        let button = UIPPButton(title: StudentLocalizable.btBenefits.text, target: self, action: #selector(benefitsAction))
        button.heightAnchor.constraint(equalToConstant: 48).isActive = true
        return button
    }()
    
    private lazy var rulesButton: UIPPButton = {
        let button = UIPPButton(title: StudentLocalizable.btRules.text, target: self, action: #selector(rulesAction), type: .underlined)
        button.heightAnchor.constraint(equalToConstant: 48).isActive = true
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale000.color
        translatesAutoresizingMaskIntoConstraints = false
        
        addComponents()
        layoutComponents()
    }
    
    @available (*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(with status: StatusPayload) {
        loadImage(status.image)
        title.text = status.title
        subtitle.text = status.message
        subtitle.textWith(spacing: 4, aligment: .center)
    }
    
    private func addComponents() {
        addSubview(imageView)
        addSubview(title)
        addSubview(subtitle)
        addSubview(benefitsButton)
        addSubview(rulesButton)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.leadingTrailingCenterX(equalTo: self, for: [imageView, title, subtitle, benefitsButton, rulesButton], constant: 16)
        
        imageView.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: 14.0),
            imageView.bottomAnchor.constraint(equalTo: title.topAnchor, constant: -20.0),
            title.bottomAnchor.constraint(equalTo: subtitle.topAnchor, constant: -16.0),
            subtitle.bottomAnchor.constraint(equalTo: benefitsButton.topAnchor, constant: -21.0),
            benefitsButton.bottomAnchor.constraint(equalTo: rulesButton.topAnchor, constant: -16.0),
            rulesButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -14.0)
        ])
    }
    
    @objc
    private func benefitsAction() {
        benefits()
    }
    
    @objc
    private func rulesAction() {
        rules()
    }
    
    private func loadImage(_ urlString: String?) {
        guard
            let string = urlString,
            let url = URL(string: string)
        else {
            imageView.image = placeholder
            return
        }
        imageView.setImage(url: url, placeholder: placeholder)
    }
}
