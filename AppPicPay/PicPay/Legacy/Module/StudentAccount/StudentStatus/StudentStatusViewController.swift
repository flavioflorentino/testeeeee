import UI

final class StudentStatusViewController: UIViewController {    
    private let viewModel: StudentStatusViewModel
    
    private lazy var loading: UILoadView = {
        return UILoadView(superview: view)
    }()
    
    private lazy var statusView: StudentStatusView = {
        let statusView = StudentStatusView()
        statusView.button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        statusView.translatesAutoresizingMaskIntoConstraints = false
        statusView.isHidden = true
        view.addSubview(statusView)
        return statusView
    }()
    
    private lazy var loadingErrorView: StudentLoadingError = {
        let loadingError = StudentLoadingError()
        view.addSubview(loadingError)
        NSLayoutConstraint.constraintAllEdges(from: loadingError, to: view)
        loadingError.isHidden = true
        return loadingError
    }()

    init(with viewModel: StudentStatusViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        setupViewController()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.loadStatus()
    }
    
    private func setupViewController() {
        addConstraints()
        setupViewModel()
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            statusView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100.0),
            statusView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            statusView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            statusView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
    private func setupViewModel() {
        viewModel.onStatusLoad = { [weak self] status in
            DispatchQueue.main.async {
                self?.removeRightBarButton()
                self?.updateStatusView(with: status)
            }
        }
        
        viewModel.onStatusError = { [weak self] error in
            DispatchQueue.main.async {
                self?.setupRightBarButton()
                self?.loadingErrorView.isHidden = false
            }
        }
        
        viewModel.isLoading = { [weak self] isLoading in
            DispatchQueue.main.async {
                if isLoading {
                    self?.setupRightBarButton()
                    self?.loading.startLoading()
                } else {
                    self?.loading.stopLoading()
                }
            }
        }
        
        loadingErrorView.tryAgain = { [weak self] in
            DispatchQueue.main.async {
                self?.loadingErrorView.isHidden = true
                self?.viewModel.loadStatus()
            }
        }
    }
    
    private func setupRightBarButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.btClose.text, style: .plain, target: self, action: #selector(close))
    }
    
    private func removeRightBarButton() {
        navigationItem.rightBarButtonItem = nil
    }
    
    private func updateStatusView(with payload: StatusPayload) {
        statusView.isHidden = false
        statusView.update(with: payload)
    }
    
    @objc
    private func buttonAction() {
        viewModel.buttonAction()
    }
    
    @objc
    private func close() {
        viewModel.close()        
    }
}
