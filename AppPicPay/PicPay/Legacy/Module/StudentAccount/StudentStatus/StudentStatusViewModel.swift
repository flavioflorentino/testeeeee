import AnalyticsModule
import Core

final class StudentStatusViewModel {
    let coordinator: StudentAccountCoordinator
    
    var onStatusLoad: (_ status: StatusPayload) -> Void = { _ in }
    var onStatusError: (_ error: Error) -> Void = { _ in }
    var isLoading: (_ isLoading: Bool) -> Void = { _ in }
    var status: StudentStatus?
    
    init(with coordinator: StudentAccountCoordinator) {
        self.coordinator = coordinator
    }
    
    func loadStatus() {
        isLoading(true)
        StudentWorker().loadStatus { [weak self] result in
            self?.isLoading(false)
            switch result {
            case .success(let payload):
                Analytics.shared.log(StudentAccountEvent.status(reason: payload.status.eventName))
                self?.status = payload.status
                let isStudent = payload.status == .active
                KVStore().setBool(isStudent, with: .hasStudentAccountActive)
                self?.showStatusOrRoute(with: payload)
            case .failure(let error):
                self?.onStatusError(error)
            }
        }
    }
    
    func showStatusOrRoute(with payload: StatusPayload) {
        let status = payload.status
        guard coordinator.shouldRoute(status) else {
            onStatusLoad(payload)
            return
        }
        coordinator.route(to: payload)
    }
    
    func buttonAction() {
        guard status != .waitingAdjust else {
            Analytics.shared.log(StudentAccountEvent.waitingAdjust)
            routeToSelectUniversity()
            return
        }
        close()
    }
    
    func routeToSelectUniversity() {
        coordinator.pushSelectUniversity()
    }
    
    func close() {        
        coordinator.close()
    }
}

protocol StudentWorkerContract {
    func loadStatus(_ completion: @escaping (PicPayResult<StatusPayload>) -> Void)
    func loadStatus(consumerId: String, _ completion: @escaping (PicPayResult<StatusPayload>) -> Void)
}

final class StudentWorker: StudentWorkerContract {
    func loadStatus(_ completion: @escaping (PicPayResult<StatusPayload>) -> Void) {
        let endpoint = "studentaccount/students/status"
        RequestManager.shared.apiRequest(endpoint: endpoint, method: .get).responseApiCodable(completionHandler: completion)
    }
    
    func loadStatus(consumerId: String, _ completion: @escaping (PicPayResult<StatusPayload>) -> Void) {
        let endpoint = "studentaccount/students/status/\(consumerId)"
        RequestManager.shared.apiRequest(endpoint: endpoint, method: .get).responseApiCodable(strategy: .useDefaultKeys, completionHandler: completion)
    }
}
