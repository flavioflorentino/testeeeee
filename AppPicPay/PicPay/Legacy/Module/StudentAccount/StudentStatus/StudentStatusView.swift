import UI

final class StudentStatusView: UIView {
    private enum Layout {
        static let titleColor = Palette.ppColorGrayscale600.color
        static let subtitleColor = Palette.ppColorGrayscale500.color
        static let titleFont = UIFont.systemFont(ofSize: 18, weight: .medium)
        static let subtitleFont = UIFont.systemFont(ofSize: 14)
    }
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 78).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 78).isActive = true
        return imageView
    }()
    
    private lazy var title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.titleFont
        label.textColor = Layout.titleColor
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var subtitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.subtitleFont
        label.textColor = Layout.subtitleColor
        label.numberOfLines = 0
        return label
    }()
    
    lazy var button: UIPPButton = {
        let button = UIPPButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.cornerRadius = 22
        button.heightAnchor.constraint(equalToConstant: 48).isActive = true
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale000.color
        
        addComponents()
        layoutComponents()
    }
    
    @available (*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(with payload: StatusPayload) {
        imageView.image = payload.status.image
        title.text = payload.title
        subtitle.text = payload.message
        subtitle.textWith(spacing: 4, aligment: .center)
        button.configure(with: Button(title: payload.status.buttonName, type: .cta))
    }
    
    private func addComponents() {
        addSubview(imageView)
        addSubview(title)
        addSubview(subtitle)
        addSubview(button)
    }
    
    private func setLeadingTrailingAndCenterX(in views: [UIView]) {
        for view in views {
            view.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
            view.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
            view.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        }
    }
    
    private func layoutComponents() {        
        NSLayoutConstraint.leadingTrailingCenterX(equalTo: self, for: [title, subtitle, button], constant: 16)
        
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: 20.0),
            title.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 17.0),
            subtitle.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 15.0),
            button.topAnchor.constraint(equalTo: subtitle.bottomAnchor, constant: 21.0)
        ])
    }
}
