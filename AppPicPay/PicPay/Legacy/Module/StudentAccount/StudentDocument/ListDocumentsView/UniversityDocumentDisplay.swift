import UI

final class UniversityDocumentDisplay: UIView {
    struct Setup {
        let title: String
        let subtitle: String
        let image: UIImage?
    }
    
    private enum Layout {
        static let titleFont = UIFont.systemFont(ofSize: 14.0, weight: .medium)
        static let subtitleFont = UIFont.systemFont(ofSize: 14.0, weight: .regular)
        static let titleColor = Palette.ppColorGrayscale500.color
        static let subtitleColor = Palette.ppColorGrayscale400.color
        static let backgroundColor = Palette.ppColorGrayscale000.color
        static let strokeColorReady = Palette.ppColorBranding300.cgColor
        static let strokeColorNeedInput = Palette.ppColorGrayscale100.cgColor
        static let placeholderImage = #imageLiteral(resourceName: "document_placeholder")
    }
    
    private lazy var title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.titleFont
        label.textColor = Layout.titleColor
        label.text = StudentLocalizable.documentsTitle.text
        return label
    }()
    
    private lazy var subtitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.subtitleFont
        label.textColor = Layout.subtitleColor
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        
        return label
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        
        return imageView
    }()
    
    private lazy var close: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 8.0
        imageView.isHidden = true
        imageView.image = #imageLiteral(resourceName: "ico_close_filled")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        view.layer.borderWidth = 1
        return view
    }()
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        defaultInit()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func defaultInit() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .clear
    }
    
    func update(with setup: Setup) {
        title.text = setup.title
        subtitle.text = setup.subtitle
        subtitle.textWith(spacing: 3)
        update(with: setup.image)
    }
    
    func update(with image: UIImage?) {
        if let image = image {
            stateIsReady(with: image)
            return
        }
        stateNeedInput()
    }
    
    private func stateNeedInput() {
        imageView.image = Layout.placeholderImage
        contentView.layer.borderColor = Layout.strokeColorNeedInput
        close.isHidden = true
    }
    
    private func stateIsReady(with image: UIImage) {
        imageView.image = image
        contentView.layer.borderColor = Layout.strokeColorReady
        close.isHidden = false
    }
    
    private func addComponents() {
        addSubview(contentView)
        
        contentView.addSubview(title)
        contentView.addSubview(subtitle)
        contentView.addSubview(imageView)
        contentView.addSubview(close)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.constraintAllEdges(from: contentView, to: self)
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            imageView.widthAnchor.constraint(equalToConstant: 72),
            imageView.heightAnchor.constraint(equalToConstant: 81),
            
            title.topAnchor.constraint(equalTo: imageView.topAnchor),
            title.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 14),
            title.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            
            subtitle.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 8),
            subtitle.leadingAnchor.constraint(equalTo: title.leadingAnchor),
            subtitle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            
            close.heightAnchor.constraint(equalToConstant: 24),
            close.widthAnchor.constraint(equalToConstant: 24),
            close.topAnchor.constraint(equalTo: imageView.topAnchor, constant: -8),
            close.trailingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 8)
        ])
    }
}
