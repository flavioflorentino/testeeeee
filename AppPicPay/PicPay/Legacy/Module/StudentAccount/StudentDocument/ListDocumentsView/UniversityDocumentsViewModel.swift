import AnalyticsModule
import Core

final class UniversityDocumentsViewModel {
    struct Payload {
        var university: University?
        var selfie: UIImage?
        var document: UIImage?
        var validateApiId: String?
    }
    
    let coordinator: StudentAccountCoordinator
    let endpoint = "\(Environment.apiUrl ?? "")studentaccount/students"
    
    private var payload = Payload() {
        didSet {
            let isValid = payload.selfie != nil && payload.document != nil
            updateState(isValid)
        }
    }
    
    var showActionSheet: (_ type: UniversityDocumentType) -> Void = { _ in }
    var showDeleteDialog: (_ type: UniversityDocumentType) -> Void = { _ in }
    var updateState: (_ isValid: Bool) -> Void = { _ in }
    var showLoading: () -> Void = { }
    var errorUploading: () -> Void = { }
    var successUpload: () -> Void = { }
    
    var selfie: UIImage? {
        return payload.selfie
    }
    
    var document: UIImage? {
        return payload.document
    }
    
    init(with coordinator: StudentAccountCoordinator) {
        self.coordinator = coordinator
    }
    
    func tap(_ route: UniversityDocumentType) {
        let image = route == .selfie ? payload.selfie : payload.document
        routeToDeleteOrAction(for: route, with: image)
    }
    
    func takePicture(_ type: UniversityDocumentType) {
        coordinator.presentTakePictureViewController(with: type)
    }
    
    func selectedPicture(_ image: UIImage?, type: UniversityDocumentType) {
        switch type {
        case .selfie:
            payload.selfie = image
        case .document:
            payload.document = image
        }
    }
    
    func analytics() {
        Analytics.shared.log(StudentAccountEvent.shownSubmissionScreen)
    }
    
    func analyticsDocumentFromLibrary() {
        Analytics.shared.log(StudentAccountEvent.attachedDocument(from: .library))
    }
    
    func validatedStudent(onApiId apiId: String) {
        payload.validateApiId = apiId
    }
    
    private func routeToDeleteOrAction(for route: UniversityDocumentType, with image: UIImage?) {
        guard image == nil else {
            showDeleteDialog(route)
            return
        }
        showActionSheet(route)
    }
    
    func upload() {
        showLoading()
        uploadDocument { [weak self] response in
            switch response {
            case .success:
                Analytics.shared.log(StudentAccountEvent.sentDocuments)
                KVStore().setBool(true, with: .studentAccountActivePopup)
                self?.coordinator.pushStatusViewController()
                self?.postFinishedStudentAccountSignUp()
                self?.successUpload()
            case .failure:
                self?.errorUploading()
            }
        }
    }
    
    func close() {
        coordinator.close()
    }
    
    private func postFinishedStudentAccountSignUp() {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: StudentAccountCoordinator.completedSignUp, object: nil)
        }
    }
    
    private func paramsToUpload() -> [String: Any]? {
        var params: [String: Any]? = nil
        
        if let universityParam = coordinator.selectedUniversity?.payload {
            params = universityParam
        }
        
        if let validatedId = payload.validateApiId {
            params?["validate_api_id"] = validatedId
        }
        
        return params
    }
    
    private func filesToUpload(with selfie: Data, and document: Data) -> [FileUpload] {
        return [
            FileUpload(key: "self_picture", data: selfie, fileName: "selfie.jpeg", mimeType: "image/jpeg"),
            FileUpload(key: "document_picture", data: document, fileName: "document.jpeg", mimeType: "image/jpeg")
        ]
    }
    
    private func compactedImage(_ image: UIImage?) -> Data? {
        guard let image = image,
            let imageData = image.jpegData(compressionQuality: 0.8) else {
                return nil
        }
        return imageData
    }
    
    private func uploadDocument(completion: @escaping (PicPayResult<BaseCodableEmptyResponse>) -> Void) {
        guard let params = paramsToUpload(),
            let selfieData = compactedImage(payload.selfie),
            let documentData = compactedImage(payload.document) else {
                errorUploading()
                return
        }
        
        let files = filesToUpload(with: selfieData, and: documentData)
        RequestManager.shared.apiUploadCodable(endpoint, method: .post, files: files, parameters: params, uploadProgress: nil, completion: { document in
            completion(document)
        })
    }
}
