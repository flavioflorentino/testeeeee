import UI

final class UniversityDocumentsView: UIView {
    private enum Layout {
        static let titleFont = UIFont.systemFont(ofSize: 18.0, weight: .medium)
        static let subtitleFont = UIFont.systemFont(ofSize: 14.0, weight: .regular)
        static let titleColor = Palette.ppColorGrayscale600.color
        static let subtitleColor = Palette.ppColorGrayscale400.color
        static let backgroundColor = Palette.ppColorGrayscale100.color
        static let titleTopSpacing: CGFloat = UIScreen.isSmall ? 8 : 30
        static let spacing: CGFloat = UIScreen.isSmall ? 12 : 24
        static let document1TopSpacing: CGFloat = UIScreen.isSmall ? 20 : 47
    }
    
    var tap: (_ type: UniversityDocumentType) -> Void = { _ in }
    var send: () -> Void = { }
    
    private lazy var title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.titleFont
        label.textColor = Layout.titleColor
        label.text = StudentLocalizable.documentsTitle.text
        label.textAlignment = .center
        return label
    }()
    
    private lazy var subtitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.subtitleFont
        label.textColor = Layout.subtitleColor
        label.numberOfLines = 0
        label.text = StudentLocalizable.documentsSubtitle.text
        label.textWith(spacing: 4, aligment: .center)
        return label
    }()
    
    private lazy var firstDocument: UniversityDocumentDisplay = {
        let view = UniversityDocumentDisplay()
        view.update(with: UniversityDocumentDisplay.Setup(title: StudentLocalizable.selfieTitle.text, subtitle: StudentLocalizable.selfieSubtitle.text, image: nil))
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapSelfie)))
        return view
    }()
    
    private lazy var secondDocument: UniversityDocumentDisplay = {
        let view = UniversityDocumentDisplay()
        view.update(with: UniversityDocumentDisplay.Setup(title: StudentLocalizable.documentTitle.text, subtitle: StudentLocalizable.documentSubtitle.text, image: nil))
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapDocument)))
        return view
    }()
    
    private lazy var button: UIPPButton = {
        let button = UIPPButton(title: DefaultLocalizable.btSend.text, target: self, action: #selector(sendAction))
        button.isEnabled = false
        button.disabledBackgrounColor = Palette.ppColorGrayscale400.color
        button.normalBackgrounColor = Palette.ppColorBranding300.color
        return button
    }()
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    private override init(frame: CGRect) {
        super.init(frame: frame)
        defaultInit()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func defaultInit() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = Layout.backgroundColor
    }
    
    private func addComponents() {
        addSubview(title)
        addSubview(subtitle)
        addSubview(firstDocument)
        addSubview(secondDocument)
        addSubview(button)
    }
    
    private func layoutComponents() {
        let views = [title, subtitle, firstDocument, secondDocument, button]
        NSLayoutConstraint.leadingTrailingCenterX(equalTo: self, for: views, constant: Layout.spacing)
        
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: topAnchor, constant: Layout.titleTopSpacing),
            subtitle.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 14),
            firstDocument.topAnchor.constraint(equalTo: subtitle.bottomAnchor, constant: Layout.document1TopSpacing),
            firstDocument.heightAnchor.constraint(equalToConstant: 112),
            secondDocument.topAnchor.constraint(equalTo: firstDocument.bottomAnchor, constant: 8),
            secondDocument.heightAnchor.constraint(equalToConstant: 112),
            button.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
            button.heightAnchor.constraint(equalToConstant: 48)
        ])
    }
    
    @objc
    private func tapSelfie() {
        tap(.selfie)
    }
    
    @objc
    private func tapDocument() {
        tap(.document)
    }
    
    @objc
    private func sendAction() {
        send()
    }
    
    func update(selfie: UIImage?, document: UIImage?) {
        firstDocument.update(with: selfie)
        secondDocument.update(with: document)
    }
    
    func update(with isValid: Bool) {
        button.isEnabled = isValid
    }
    
    func hideLoading() {
        button.stopLoadingAnimating()
    }
    
    func showLoading() {
        button.startLoadingAnimating()
    }
}
