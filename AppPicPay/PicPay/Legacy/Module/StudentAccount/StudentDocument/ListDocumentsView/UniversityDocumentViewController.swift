final class UniversityDocumentsViewController: UIViewController {
    private let viewModel: UniversityDocumentsViewModel
    private var selectigType: UniversityDocumentType?
    
    private lazy var documentsView: UniversityDocumentsView = {
        let documentsView = UniversityDocumentsView()
        documentsView.translatesAutoresizingMaskIntoConstraints = false
        return documentsView
    }()
    
    private lazy var loadingError: StudentLoadingError = {
        let loadingError = StudentLoadingError()
        view.addSubview(loadingError)
        NSLayoutConstraint.constraintAllEdges(from: loadingError, to: view)
        loadingError.isHidden = true
        return loadingError
    }()
    
    init(with viewModel: UniversityDocumentsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        setupViewController()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViewController() {
        addComponents()
        addConstraints()
        setupRightBarButton()
        setupViewModel()
        viewModel.analytics()
    }
    
    private func setupRightBarButton() {
        let barButton = UIBarButtonItem(
            title: DefaultLocalizable.btClose.text,
            style: .plain,
            target: self,
            action: #selector(close)
        )
        navigationItem.rightBarButtonItem = barButton
    }
    
    private func setupViewModel() {
        documentsView.tap = { [weak self] type in
            self?.viewModel.tap(type)
        }
        
        documentsView.send = { [weak self] in
            self?.viewModel.upload()
        }
        
        viewModel.showActionSheet = { [weak self] type in
            guard let actionSheet = self?.actionSheet(for: type) else {
                return
            }
            self?.present(actionSheet, animated: true)
        }
        
        viewModel.showDeleteDialog = { [weak self] type in
            self?.showDeleteDialog(for: type)
        }
        
        viewModel.updateState = { [weak self] isValid in
            self?.documentsView.update(with: isValid)
        }
        
        viewModel.errorUploading = { [weak self] in
            DispatchQueue.main.async {
                self?.loadingError.isHidden = false
                self?.documentsView.hideLoading()
            }
        }
        
        viewModel.showLoading = { [weak self] in
            self?.documentsView.showLoading()
        }
        
        loadingError.tryAgain = { [weak self] in
            self?.loadingError.isHidden = true
            self?.viewModel.upload()
        }
    }
    
    private func addComponents() {
        view.addSubview(documentsView)
    }
    
    private func addConstraints() {
        NSLayoutConstraint.constraintAllEdges(from: documentsView, to: view)
    }
    
    private func showDeleteDialog(for type: UniversityDocumentType) {
        let actionSheet = UIAlertController(title: type.deleteMessage, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: DefaultLocalizable.remove.text, style: .destructive) { [weak self] _ in
            self?.selectedPicture(nil, type: type)
        })
        actionSheet.addAction(UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel))
        present(actionSheet, animated: true)
    }
    
    func selectedPicture(_ image: UIImage?, type: UniversityDocumentType) {        
        viewModel.selectedPicture(image, type: type)
        updateDocuments()
    }
    
    func updateDocuments() {
        documentsView.update(selfie: viewModel.selfie, document: viewModel.document)
    }
    
    private func selectLibrary(_ type: UniversityDocumentType) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        selectigType = type
        present(imagePicker, animated: true)
    }
    
    @objc
    private func close() {
        viewModel.close()
    }
}

enum UniversityDocumentType {
    case selfie
    case document
    
    var deleteMessage: String {
        switch self {
        case .selfie:
            return StudentLocalizable.deleteSelfieAlert.text
        case .document:
            return StudentLocalizable.deleteDocumentAlert.text
        }
    }
}

extension UniversityDocumentsViewController {
    private func actionSheet(for type: UniversityDocumentType) -> UIAlertController {
        let selfie = StudentLocalizable.takePicture.text
        let document = StudentLocalizable.selectFromLibrary.text
        let title = type == .selfie ? StudentLocalizable.selfieTitle.text : StudentLocalizable.documentTitle.text
        
        let actionSheet = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: selfie, style: .default) { [weak self] _ in
            self?.viewModel.takePicture(type)
        })
        
        if type == .document {
            actionSheet.addAction(UIAlertAction(title: document, style: .default) { [weak self] _ in
                self?.selectLibrary(type)
            })
        }
        
        actionSheet.addAction(UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel))
        return actionSheet
    }
}

extension UniversityDocumentsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[.originalImage] as? UIImage, let type = selectigType else {
            return
        }
        
        viewModel.analyticsDocumentFromLibrary()
        selectedPicture(image, type: type)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}
