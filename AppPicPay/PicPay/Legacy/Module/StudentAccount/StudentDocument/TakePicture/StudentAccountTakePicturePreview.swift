import UI

final class StudentAccountTakePicturePreview: UIView {
    private enum Layout {
        static let subtitleFont = UIFont.systemFont(ofSize: 15, weight: .medium)
        static let subtitleColor = Palette.ppColorGrayscale000.color
        static let backgroundColor = PicPayStyle.pictureBackgroundColor
        static let buttonBottomSpacing: CGFloat = UIScreen.isSmall ? -12 : -28
    }
    
    var tryAgain: () -> Void = { }
    var proceed: () -> Void = { }
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var subtitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.subtitleFont
        label.textColor = Layout.subtitleColor
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private lazy var tryAgainButton: UIPPButton = {
        let button = UIPPButton(title: StudentLocalizable.pictureBtTakeAnother.text, target: self, action: #selector(tryAgainAction), type: .white)
        button.heightAnchor.constraint(equalToConstant: 48).isActive = true
        return button
    }()
    
    private lazy var proceedButton: UIPPButton = {
        let button = UIPPButton(title: StudentLocalizable.pictureBtOk.text, target: self, action: #selector(proceedAction))
        button.heightAnchor.constraint(equalToConstant: 48).isActive = true
        return button
    }()
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    private override init(frame: CGRect) {
        super.init(frame: frame)
        defaultInit()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(with image: UIImage, type: UniversityDocumentType) {
        imageView.image = image
        update(with: type)
    }
    
    func update(with type: UniversityDocumentType) {
        switch type {
        case .selfie:
            subtitle.text = StudentLocalizable.checkSelfieSubtitle.text
        case .document:
            subtitle.text = StudentLocalizable.checkDocumentSubtitle.text
        }
    }
    
    private func defaultInit() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = Layout.backgroundColor
    }
    
    private func addComponents() {
        addSubview(imageView)
        addSubview(subtitle)
        addSubview(tryAgainButton)
        addSubview(proceedButton)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: 22),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 28),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -28),
            imageView.bottomAnchor.constraint(equalTo: subtitle.topAnchor, constant: -25),
            
            subtitle.bottomAnchor.constraint(equalTo: tryAgainButton.topAnchor, constant: -25),
            subtitle.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 21),
            subtitle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: -21),
            
            tryAgainButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: Layout.buttonBottomSpacing),
            tryAgainButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            tryAgainButton.heightAnchor.constraint(equalToConstant: 48),
            tryAgainButton.trailingAnchor.constraint(equalTo: centerXAnchor, constant: -16),

            //Colocar em relação a safe area
            proceedButton.bottomAnchor.constraint(equalTo: tryAgainButton.bottomAnchor),
            proceedButton.leadingAnchor.constraint(equalTo: centerXAnchor, constant: 16),
            proceedButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            proceedButton.heightAnchor.constraint(equalToConstant: 48)
        ])
    }
    
    @objc
    private func tryAgainAction() {
        tryAgain()
    }
    
    @objc
    private func proceedAction() {
        proceed()
    }
}
