import AnalyticsModule

final class StudentAccountTakePictureViewModel {
    enum State {
        case needPermission
        case goodToGo
    }
    
    private var coordinator: StudentAccountCoordinator
    private var picture: UIImage?
    
    var camera: (_ state: State) -> Void = { _ in }
    var type: UniversityDocumentType
    
    init(with coordinator: StudentAccountCoordinator, type: UniversityDocumentType) {
        self.coordinator = coordinator
        self.type = type
    }
    
    func close() {
        coordinator.dismissPresentedView()
    }
    
    func checkCameraPermission() {
        camera(PPPermission.camera().status != .authorized ? .needPermission : .goodToGo)
    }
    
    func askCameraPermission() {
        PPPermission.request(permission: PPPermission.camera()) { [weak self] status in
            guard status == .authorized else {
                self?.close()
                return
            }
            self?.camera(.goodToGo)
        }
    }
    
    func update(with image: UIImage?) {
        picture = image
    }
    
    func proceedWithPictureTaken() {
        let event: StudentAccountEvent = type == .selfie ? .tookSelfie : .attachedDocument(from: .camera)
        Analytics.shared.log(event)
        
        guard let image = picture else {
            return
        }
        coordinator.selectedPicture(image, type: type)
        coordinator.dismissPresentedView()
    }
}
