import UI

final class StudentAccountTakePictureView: UIView {
    private enum Layout {
        static let subtitleFont = UIFont.systemFont(ofSize: 15, weight: .medium)
        static let subtitleColor = Palette.ppColorGrayscale000.color
        static let backgroundColor = PicPayStyle.pictureBackgroundColor
        static let pictureFrameBottomSpacing: CGFloat = UIScreen.isSmall ? -12 : -50
        static let subtitleBottomSpacing: CGFloat = UIScreen.isSmall ? -6 : -20
        static let buttonBottomSpacing: CGFloat = UIScreen.isSmall ? -12 : -28
    }
    
    var dismiss: () -> Void = { }
    var takePicture: () -> Void = { }
    
    private lazy var close: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "x")
        imageView.contentMode = .center
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(buttonCloseAction)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    private lazy var pictureFrame: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "selfie_frame")
        return imageView
    }()
    
    private var wraperViewPort: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var viewPort: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var subtitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.subtitleFont
        label.textColor = Layout.subtitleColor
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private lazy var button: UIPPButton = {
        let button = UIPPButton(title: StudentLocalizable.takeSelfie.text,
                                target: self,
                                action: #selector(takePictureAction))
        button.heightAnchor.constraint(equalToConstant: 48).isActive = true
        return button
    }()
    
    private lazy var permission: PermissionInfoView = {
        let view = PermissionInfoView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        NSLayoutConstraint.constraintAllEdges(from: view, to: self)
        return view
    }()
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    private override init(frame: CGRect) {
        super.init(frame: frame)
        defaultInit()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(with type: UniversityDocumentType) {
        switch type {
        case .selfie:
            subtitle.text = StudentLocalizable.takeSelfieSubtitle.text
        case .document:
            subtitle.text = StudentLocalizable.takePictureDocumentSubtitle.text
        }
    }
    
    private func defaultInit() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = Layout.backgroundColor
    }
    
    private func addComponents() {
        addSubview(close)
        addSubview(wraperViewPort)
        wraperViewPort.addSubview(viewPort)
        addSubview(pictureFrame)
        addSubview(subtitle)
        addSubview(button)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.constraintAllEdges(from: viewPort, to: wraperViewPort)
        NSLayoutConstraint.constraintAllEdges(from: wraperViewPort, to: pictureFrame)
        
        NSLayoutConstraint.activate([
            //Colocar em relação a safe Area
            close.topAnchor.constraint(equalTo: topAnchor, constant: 18),
            close.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            close.heightAnchor.constraint(equalToConstant: 38),
            close.widthAnchor.constraint(equalToConstant: 38),
            
            pictureFrame.topAnchor.constraint(equalTo: close.bottomAnchor, constant: 10),
            pictureFrame.centerXAnchor.constraint(equalTo: centerXAnchor),
            pictureFrame.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            pictureFrame.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            pictureFrame.bottomAnchor.constraint(equalTo: subtitle.topAnchor, constant: Layout.pictureFrameBottomSpacing),
            
            subtitle.bottomAnchor.constraint(equalTo: button.topAnchor, constant: Layout.subtitleBottomSpacing),
            subtitle.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -21),
            subtitle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 21),
            
            //Colocar em relação a safe area
            button.bottomAnchor.constraint(equalTo: bottomAnchor, constant: Layout.buttonBottomSpacing),
            button.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -25),
            button.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 25)
        ])
    }
    
    @objc
    private func buttonCloseAction() {
        dismiss()
    }
    
    @objc
    private func takePictureAction() {
        takePicture()
    }
}
