final class StudentAccountTakePictureViewController: UIViewController {
    private let viewModel: StudentAccountTakePictureViewModel
    private var cameraController: CameraController?    
    
    private lazy var pictureView: StudentAccountTakePictureView = {
        let pictureView = StudentAccountTakePictureView()
        pictureView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pictureView)
        NSLayoutConstraint.constraintAllEdges(from: pictureView, to: view)
        return pictureView
    }()
    
    private lazy var previewPicture: StudentAccountTakePicturePreview = {
        let preview = StudentAccountTakePicturePreview()
        preview.isHidden = true
        view.addSubview(preview)
        NSLayoutConstraint.constraintAllEdges(from: preview, to: view)
        return preview
    }()
    
    init(with viewModel: StudentAccountTakePictureViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        
        setupViewController()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraController?.captureSession.stopRunning()
    }
    
    private func setupViewController() {
        pictureView.update(with: viewModel.type)
        setupViewModel()
        setupPreview()
        checkCameraPermission()
    }
    
    private func setupViewModel() {
        pictureView.dismiss = { [weak self] in
            self?.viewModel.close()
        }
        
        pictureView.takePicture = { [weak self] in
            self?.takePicture()
        }
        
        viewModel.camera = { [weak self] state in
            switch state {
            case .needPermission:
                self?.viewModel.askCameraPermission()
            case .goodToGo:
                self?.configureCameraController()
            }            
        }
    }
    
    func setupPreview() {
        previewPicture.tryAgain = { [weak self] in
            self?.hidePreviewPicture()
        }
        
        previewPicture.proceed = { [weak self] in
            self?.viewModel.proceedWithPictureTaken()
        }
    }
    
    private func checkCameraPermission() {
        viewModel.checkCameraPermission()
    }
}

extension StudentAccountTakePictureViewController {
    private func configureCameraController() {
        guard cameraController == nil else {
            cameraController?.captureSession.startRunning()
            return
        }
        
        cameraController = CameraController()
        cameraController?.prepare { [weak self] _ in
            guard let strongSelf = self else {
                return
            }
            
            try? self?.cameraController?.displayPreview(on: strongSelf.pictureView.viewPort)
            if strongSelf.viewModel.type == .selfie {
                try? self?.cameraController?.switchToFrontCamera()
            }
        }
    }
    
    private func takePicture() {
        cameraController?.captureImage { [weak self] image, _ in
            guard var image = image, let cgImage = image.cgImage, let type = self?.viewModel.type else {
                return
            }
            
            if type == .selfie {
                image = UIImage(cgImage: cgImage, scale: image.scale, orientation: .leftMirrored)
            }
            
            self?.showPreviewPicture(with: image)
            self?.cameraController?.captureSession.stopRunning()
        }
    }
    
    private func showPreviewPicture(with image: UIImage) {
        viewModel.update(with: image)
        previewPicture.update(with: image, type: viewModel.type)
        previewPicture.isHidden = false
        pictureView.isHidden = true
    }
    
    private func hidePreviewPicture() {
        previewPicture.isHidden = true
        pictureView.isHidden = false
        configureCameraController()
    }
}
