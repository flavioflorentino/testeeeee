enum StudentStatus: String, Codable {
    case available
    case active
    case notApproved = "not_approved"
    case pending
    case waitingAdjust = "waiting_adjust"
    case expired
    
    var image: UIImage? {
        switch self {
        case .active:
            return #imageLiteral(resourceName: "icon-ativado")
        case .pending:
            return #imageLiteral(resourceName: "ico_clock_green")
        case .notApproved:
            return #imageLiteral(resourceName: "icon-bad")
        case .waitingAdjust, .expired:
            return #imageLiteral(resourceName: "icon-analysis")
        default:
            return nil
        }
    }
    
    var eventName: String {
        switch self {
        case .available:
            return StudentLocalizable.available.text
        case .active:
            return StudentLocalizable.active.text
        case .notApproved:
            return StudentLocalizable.notApproved.text
        case .pending:
            return StudentLocalizable.pending.text
        case .waitingAdjust, .expired:
            return StudentLocalizable.waitingAdjust.text
        }
    }
    
    var buttonName: String {
        switch self {
        case .waitingAdjust:
            return StudentLocalizable.btFixIt.text
        default:
            return DefaultLocalizable.btOkUnderstood.text
        }
    }
}

struct StatusPayload: Codable {
    let status: StudentStatus
    let title: String?
    let message: String?
    let image: String?
}
