import UI

final class StudentLoadingError: UIView {
    private enum Layout {
        static let titleFont = UIFont.systemFont(ofSize: 18, weight: .medium)
        static let titleColor = Palette.ppColorGrayscale600.color
        static let subtitleFont = UIFont.systemFont(ofSize: 14)
        static let subtitleColor = Palette.ppColorGrayscale400.color
    }
    
    var tryAgain: () -> Void = { }
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "sad-3")
        return imageView
    }()
    
    private lazy var title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Layout.titleColor
        label.font = Layout.titleFont
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = StudentLocalizable.errorTitle.text
        
        return label
    }()
    
    private lazy var subtitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Layout.subtitleColor
        label.font = Layout.subtitleFont
        label.numberOfLines = 0
        label.text = StudentLocalizable.errorSubtitle.text
        label.textWith(spacing: 3, aligment: .center)
        return label
    }()
    
    lazy var button: UIPPButton = {
        let button = UIPPButton(title: StudentLocalizable.errorTryAgainButton.text,
                                target: self,
                                action: #selector(tryAgainAction))
        return button
    }()
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        defaultInit()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    func tryAgainAction() {
        tryAgain()
    }
    
    private func defaultInit() {
        backgroundColor = Palette.ppColorGrayscale000.color
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func addComponents() {
        addSubview(imageView)
        addSubview(title)
        addSubview(subtitle)
        addSubview(button)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.leadingTrailingCenterX(equalTo: self, for: [title, subtitle, button], constant: 16)
        
        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalToConstant: 70),
            imageView.heightAnchor.constraint(equalToConstant: 70),
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            
            title.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 14),
            subtitle.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 17),
            button.topAnchor.constraint(equalTo: subtitle.bottomAnchor, constant: 21),
            
            button.heightAnchor.constraint(equalToConstant: 44)
        ])
    }
}
