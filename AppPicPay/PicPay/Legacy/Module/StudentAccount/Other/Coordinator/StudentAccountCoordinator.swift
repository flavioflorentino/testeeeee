import FeatureFlag
import AnalyticsModule

protocol StudentAccountCoordinatorProtocol { }

final class StudentAccountCoordinator: StudentAccountCoordinatorProtocol {
    enum Origin {
        case deeplink
        case signUp
        case settings
        case signUpOffer(withValidatedApiId: String)
        case `default`
    }
    
    static let completedSignUp = NSNotification.Name(rawValue: "FinishedStudentAccountSignUp")
    
    private var startViewController: UIViewController
    private(set) var navigation: UINavigationController
    private var validateApiId: String?
    var selectedUniversity: University?
    
    private lazy var documentsViewController: UniversityDocumentsViewController = {
        let viewModel = UniversityDocumentsViewModel(with: self)
        return UniversityDocumentsViewController(with: viewModel)
    }()
    
    init(from viewController: UIViewController) {
        self.startViewController = viewController
        self.navigation = UINavigationController()
    }
    
    func start(from origin: Origin) {
        let viewModel = StudentStatusViewModel(with: self)
        let studentStatusViewController = StudentStatusViewController(with: viewModel)
        if case let .signUpOffer(id) = origin {
            validateApiId = id
        }
        startAndPush(studentStatusViewController)
    }
    
    func close() {
        startViewController.dismiss(animated: true)
    }
    
    func pushStatusViewController() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }

            let viewModel = StudentStatusViewModel(with: self)
            let viewController = StudentStatusViewController(with: viewModel)
            self.removeEverythingExcept(StudentStatusViewController.self)
            viewController.navigationItem.setHidesBackButton(true, animated: true)
            self.navigation.pushViewController(viewController, animated: true)
        }
    }
        
    func pushSelectUniversity() {
        let viewModel = UniversitySelectViewModel(with: self)
        let viewController = UniversitySelectViewController(with: viewModel)
        removeEverythingExcept(UniversitySelectViewController.self)
        viewController.navigationItem.setHidesBackButton(true, animated: true)
        navigation.pushViewController(viewController, animated: true)
    }
    
    func pushApproved(_ payload: StatusPayload) {
        let viewModel = StudentStatusApprovedViewModel(with: self, payload: payload)
        let viewController = StudentStatusApprovedViewController(with: viewModel)
        removeStatusViewControllerFromNavigationStack()
        navigation.pushViewController(viewController, animated: true)
        viewController.navigationItem.setHidesBackButton(true, animated: true)
    }
    
    func pushDocuments(with university: University?) {
        selectedUniversity = university
        let viewModel = UniversityDocumentsViewModel(with: self)
        documentsViewController = UniversityDocumentsViewController(with: viewModel)
        if let id = validateApiId {
            viewModel.validatedStudent(onApiId: id)
        }
        navigation.pushViewController(documentsViewController, animated: true)
    }
    
    func selectedPicture(_ image: UIImage, type: UniversityDocumentType) {
        documentsViewController.selectedPicture(image, type: type)
    }
    
    func presentTakePictureViewController(with type: UniversityDocumentType) {
        let viewModel = StudentAccountTakePictureViewModel(with: self, type: type)
        let viewController = StudentAccountTakePictureViewController(with: viewModel)
        navigation.present(viewController, animated: true)
    }
     
    func dismissPresentedView() {
        navigation.dismiss(animated: true)
    }
    
    func shouldRoute(_ status: StudentStatus) -> Bool {
        switch status {
        case .available, .active:
            return true
        case .expired, .notApproved, .pending, .waitingAdjust:
            return false
        }
    }
    
    func route(to statusPayload: StatusPayload) {
        DispatchQueue.main.async { [weak self] in
            switch statusPayload.status {
            case .available:
                self?.pushSelectUniversity()
            case .active:
                self?.pushApproved(statusPayload)
            default:
                return
            }
        }
    }
    
    func benefits() -> SettingsWebViewController {
        return Screen.benefits.controller
    }
    
    func rules() -> SettingsWebViewController {
        return Screen.rules.controller
    }
    
    func openBenefits() {
        Analytics.shared.log(StudentAccountEvent.benefits(from: .activatedAccount))
        navigation.pushViewController(benefits(), animated: true)
    }
    
    func openRules() {
        Analytics.shared.log(StudentAccountEvent.rules)
        navigation.pushViewController(rules(), animated: true)
    }
    
    private func startAndPush(_ controller: UIViewController) {
        navigation = UINavigationController(rootViewController: controller)
        startViewController.present(navigation, animated: true)
    }
    
    private func removeEverythingExcept(_ myClass: AnyClass) {
        for controller in navigation.viewControllers where !(controller.isKind(of: myClass)) {
            controller.removeFromParent()
        }
    }
    
    private func removeStatusViewControllerFromNavigationStack() {
        let status = navigation.viewControllers.first { $0 is StudentStatusViewController }
        status?.removeFromParent()
    }
}

private enum Screen {
    case benefits
    case rules
    
    var url: String {
        switch self {
        case .benefits:
            return FeatureManager.text(.universityAccountBenefitsWebview)
        case .rules:
            return FeatureManager.text(.universityAccountRulesWebview)
        }
    }
    
    var title: String {
        switch self {
        case .benefits:
            return StudentLocalizable.titleBenefits.text
        case .rules:
            return StudentLocalizable.titleRules.text
        }
    }
    
    var controller: SettingsWebViewController {
        return SettingsWebViewController(url: url, title: title)!
    }
}
