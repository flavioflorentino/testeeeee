import UI

private extension StudentAccountCodeView.Layout {
    enum Style {
        static let titleFont = UIFont.systemFont(ofSize: 14.0, weight: .regular)
        static let codeTitleFont = UIFont.systemFont(ofSize: 17.0, weight: .regular)
        static let subtitleFont = UIFont.systemFont(ofSize: 12.0, weight: .regular)
    }
    
    enum Size {
        static let imageSize: CGFloat = 28
        static let borderWidth: CGFloat = 0.5
    }
}

final class StudentAccountCodeView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Style.titleFont
        label.textColor = Colors.grayscale500.color
        label.text = RegisterLocalizable.usernameCodeUsed.text.uppercased()
        return label
    }()
    
    private lazy var promoCodeContainerView = UIView()
    
    private lazy var codeTitleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Style.codeTitleFont
        label.textColor = Colors.grayscale600.color
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Style.subtitleFont
        label.textColor = Colors.grayscale400.color
        label.text = StudentLocalizable.studentAccount.text
        return label
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = Assets.Icons.iconCapBlack.image
        return imageView
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        drawBorders()
    }
    
    func update(with code: String) {
        codeTitleLabel.text = code
    }
    
    func buildViewHierarchy() {
        promoCodeContainerView.addSubview(codeTitleLabel)
        promoCodeContainerView.addSubview(subtitleLabel)
        promoCodeContainerView.addSubview(imageView)
        
        addSubview(titleLabel)
        addSubview(promoCodeContainerView)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalTo(Spacing.base02)
            $0.trailing.equalToSuperview()
        }
        
        promoCodeContainerView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        imageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.imageSize)
            $0.leading.equalTo(promoCodeContainerView).offset(Spacing.base02)
            $0.centerY.equalTo(promoCodeContainerView)
        }
        
        codeTitleLabel.snp.makeConstraints {
            $0.top.equalTo(promoCodeContainerView).offset(Spacing.base01)
            $0.leading.equalTo(imageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalTo(promoCodeContainerView).offset(-Spacing.base02)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(codeTitleLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.equalTo(imageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalTo(promoCodeContainerView).offset(-Spacing.base02)
            $0.bottom.equalTo(promoCodeContainerView).offset(-Spacing.base01)
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        promoCodeContainerView.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    private func drawBorders() {
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0, y: 0, width: promoCodeContainerView.frame.width, height: Layout.Size.borderWidth)
        topBorder.backgroundColor = Colors.grayscale300.color.cgColor
        
        let bottonBorder = CALayer()
        bottonBorder.frame = CGRect(x: 0, y: promoCodeContainerView.frame.height - 1, width: self.frame.width, height: Layout.Size.borderWidth)
        bottonBorder.backgroundColor = Colors.grayscale300.color.cgColor
        
        promoCodeContainerView.layer.addSublayer(topBorder)
        promoCodeContainerView.layer.addSublayer(bottonBorder)
    }
}
