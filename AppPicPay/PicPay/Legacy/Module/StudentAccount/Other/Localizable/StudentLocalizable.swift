enum StudentLocalizable: String, Localizable {
    case studentAccount
    case topDisclaimerSignUp
    case disclaimerOneTitleSignUp
    case disclaimerOneSubtitleSignUp
    case disclaimerTwoTitleSignUp
    case disclaimerTwoSubtitleSignUp
    
    case errorTitle
    case errorSubtitle
    case errorTryAgainButton
    
    case selectUniversityTitle
    case selectUniversitySubtitle
    case selectUniversityTip
    case selectUniversitySearch
    
    case selectUniversityNeedInputTitle
    case selectUniversitNoResultsTitle
    case selectUniversitNoResultsSubtitle
    
    case documentsTitle
    case documentsSubtitle
    case selfieTitle
    case selfieSubtitle
    case documentTitle
    case documentSubtitle
    
    case takePicture
    case selectFromLibrary
    case deleteDocumentAlert
    case deleteSelfieAlert
    
    case takeSelfie
    case takeSelfieSubtitle
    case takePictureDocumentSubtitle
    case checkSelfieSubtitle
    case checkDocumentSubtitle
    case pictureBtOk
    case pictureBtTakeAnother
    
    case titleBenefits
    case titleRules
    case btBenefits
    case btRules
    case btFixIt
    
    case activePopupTitle
    case activePopupDescription
    case activePopupBenefitsCashback
    case activePopupButton

    case available
    case active
    case notApproved
    case pending
    case waitingAdjust
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .studentAccount
    }
}
