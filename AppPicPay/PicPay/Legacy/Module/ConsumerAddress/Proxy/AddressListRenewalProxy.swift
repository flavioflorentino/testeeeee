import Foundation
import Registration

final class AddressListRenewalProxy: RegistrationRenewalAddressInput {
    private let service: RenewalAddressServiceServicing
    private var navigationController: UINavigationController?
    private var completion: ((String, String) -> Void)?
    
    init(service: RenewalAddressServiceServicing = RenewalAddressServiceService()) {
        self.service = service
    }
    
    func openAddressList(
        idAddress: String?,
        navigationController: UINavigationController?,
        completion: @escaping (String, String) -> Void)
    {
        self.navigationController = navigationController
        self.completion = completion
        
        var selectedAddress: ConsumerAddressItem?
        if let idAddress = idAddress , let id = Int(idAddress) {
            selectedAddress = ConsumerAddressItem(id: id)
        }
        
        let viewController = getViewController(withSelectedAddress: selectedAddress)
        viewController.onAddressSelected = onAddressSelected
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func onAddressSelected(addressItem: ConsumerAddressItem?) {
        guard
            let addressItem = addressItem,
            let id = addressItem.id,
            let itemId = String(intValue: id)
            else {
                return
        }
        
        showPopup(id: itemId, addressItem: addressItem)
    }
    
    private func showPopup(id: String, addressItem: ConsumerAddressItem) {
        let alert = Alert(title: ConsumerAddressLocalizable.proxyConfirmAddress.text, text: "\(addressItem.getSubtitle())")
        let buttonYes = Button(title: DefaultLocalizable.btConfirm.text, type: .cta) { [weak self] popupController, uiButton in
            self?.sendAddress(id: id, addressItem: addressItem, uiButton: uiButton, alert: popupController)
        }
        
        let buttonNo = Button(title: DefaultLocalizable.btCancel.text, type: .inline, action: .close)
        alert.buttons = [buttonYes, buttonNo]
        AlertMessage.showAlert(alert, controller: navigationController)
    }
    
    private func sendAddress(id: String, addressItem: ConsumerAddressItem, uiButton: UIPPButton, alert: AlertPopupViewController) {
        uiButton.startLoadingAnimating()
        
        service.saveAddress(value: id) { [weak self] result in
            uiButton.stopLoadingAnimating()
            switch result {
            case .success:
                alert.dismiss(animated: true) {
                    self?.completion?(id, addressItem.getSubtitle())
                    self?.navigationController?.popViewController(animated: true)
                }
            case .failure(let error):
                alert.dismiss(animated: true) {
                    AlertMessage.showCustomAlertWithError(error.picpayError, controller: self?.navigationController)
                }
            }
        }
    }
    
    private func getViewController(withSelectedAddress selectedAddress: ConsumerAddressItem?) -> AddressListViewController {
        let viewModel = AddressListViewModel(displayContextHeader: true)
        viewModel.selectedAddress = selectedAddress

        let viewController = AddressListViewController(viewModel: viewModel)
        viewController.addressContextTitle = ConsumerAddressLocalizable.proxyHomeAddress.text
        viewController.addressContextText = ConsumerAddressLocalizable.proxyHomeAddressDescription.text
        viewController.addressContextImage = Assets.AddressImages.home.image

        if #available(iOS 11.0, *) {
            viewController.navigationItem.largeTitleDisplayMode = .never
        }

        return viewController
    }
}
