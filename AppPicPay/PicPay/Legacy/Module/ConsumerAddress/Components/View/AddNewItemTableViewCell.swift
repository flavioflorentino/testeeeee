import UI
import UIKit

class AddNewItemTableViewCell: UITableViewCell {
    
    //MARK: Subviews
    lazy private var plusButton: UIButton = {
        let _button = UIButton()
        _button.translatesAutoresizingMaskIntoConstraints = false
        _button.setImage(#imageLiteral(resourceName: "addSign"), for: .normal)
        _button.contentEdgeInsets = UIEdgeInsets(top: 17, left: 17, bottom: 17, right: 17)
        _button.layer.masksToBounds = true
        _button.layer.borderWidth = 1
        _button.layer.borderColor = UIColor(white: 221.0/255.0, alpha: 1).cgColor
        _button.isUserInteractionEnabled = false
        
        return _button
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        
        return label
    }()
    
    //MARK: Properties
    
    var title:String? = nil {
        didSet {
            titleLabel.text = title
        }
    }
    
    //MARK: UITableViewCell
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initialize()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialize()
    }
    
    //MARK: Methods
    
    internal func initialize() {
        title = ConsumerAddressLocalizable.addNewItem.text
        
        buildViews()
    }
    
    private func buildViews() {
        let buttonSize:CGFloat = 54
        let vMargin: CGFloat = 16
        let hMargin: CGFloat = 16
        
        contentView.addSubview(plusButton)
        contentView.addSubview(titleLabel)
        
        backgroundColor = Palette.ppColorGrayscale000.color(withCustomDark: .ppColorGrayscale500)

        //plusButton
        
        NSLayoutConstraint.activate([
            plusButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: vMargin),
            plusButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -vMargin),
            plusButton.widthAnchor.constraint(equalToConstant: buttonSize),
            plusButton.heightAnchor.constraint(equalToConstant: buttonSize),
            plusButton.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: hMargin)
        ])
        
        plusButton.layer.cornerRadius = buttonSize / 2
        
        //titlelabel
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: plusButton.trailingAnchor, constant: 13),
            titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            titleLabel.heightAnchor.constraint(lessThanOrEqualTo: contentView.heightAnchor)
            ])
    }
}
