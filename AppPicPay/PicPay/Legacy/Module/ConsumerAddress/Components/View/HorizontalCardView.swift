import UI
import UIKit

protocol HorizontalCardViewPresentable: AnyObject {
    func getTitle() -> String
    func getSubtitle() -> String
    func getImage() -> UIImage?
    func getPlaceholderImage() -> UIImage
}

extension HorizontalCardViewPresentable {
    typealias Values = (title:String, subtitle:String, image:UIImage?, placeholderImage:UIImage)
    func getValues() -> Values {
        return (self.getTitle(), self.getSubtitle(), self.getImage(), self.getPlaceholderImage())
    }
}

final class HorizontalCardView: UIView {
    //MARK: Subviews
    lazy private var imageView: UIImageView = {
        let _imageView = UIImageView()
        _imageView.contentMode = .scaleAspectFit
        _imageView.translatesAutoresizingMaskIntoConstraints = false
        return _imageView
    }()
    
    lazy private var titleLabel: UILabel = {
        let _label = UILabel()
        _label.translatesAutoresizingMaskIntoConstraints = false
        _label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        _label.textColor = Palette.ppColorGrayscale500.color(withCustomDark: .ppColorGrayscale000)
        
        return _label
    }()
    
    lazy private var subTitleLabel: UILabel = {
        let _label = UILabel()
        _label.translatesAutoresizingMaskIntoConstraints = false
        _label.font = UIFont.systemFont(ofSize: 13)
        _label.textColor = Palette.ppColorGrayscale400.color(withCustomDark: .ppColorGrayscale000)
        _label.numberOfLines = 0
        
        return _label
    }()
    
    //MARK: Properties
    private var model:HorizontalCardViewPresentable? = nil
    private var shouldLoadConstraints:Bool = true
    //Animation x model, control
    private var isAnimating:Bool = false
    private var valuesToApply:HorizontalCardViewPresentable.Values? = nil
    private var canApplyValues:Bool = false
    
    //MARK: Init
    convenience init(model: HorizontalCardViewPresentable, frame:CGRect = .zero) {
        self.init(frame: frame)
        
        self.setModel(model, animated: false)
    }
    
    //MARK: UIView
    override func updateConstraints() {
        if shouldLoadConstraints {
            self.loadConstraints()
            self.shouldLoadConstraints = false
        }
        
        super.updateConstraints()
    }
    
    //MARK: Private methods
    private func loadConstraints() {
        let imageContainerSize:CGFloat = 54
        let imageSize:CGFloat = 33
        let imageViewContainer:UIView = UIView()
        
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fill
        stack.spacing = 2
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        self.translatesAutoresizingMaskIntoConstraints = false
        imageViewContainer.translatesAutoresizingMaskIntoConstraints = false
        stack.addArrangedSubview(self.titleLabel)
        stack.addArrangedSubview(self.subTitleLabel)
        self.addSubview(stack)
        self.addSubview(imageViewContainer)
        imageViewContainer.addSubview(self.imageView)
        
        //Constraints
        
        // - Image
        imageViewContainer.backgroundColor = Palette.ppColorGrayscale100.color(withCustomDark: .ppColorGrayscale500)
        imageViewContainer.layer.cornerRadius = imageContainerSize / 2
        imageViewContainer.layer.masksToBounds = true
        
        let imageTopGreatConstraint = imageViewContainer.topAnchor.constraint(greaterThanOrEqualTo: self.topAnchor)
        imageTopGreatConstraint.priority = UILayoutPriority(rawValue: 100)
        imageTopGreatConstraint.isActive = true
        
        let imageBottomGreatConstraint = imageViewContainer.bottomAnchor.constraint(greaterThanOrEqualTo: self.bottomAnchor)
        imageBottomGreatConstraint.priority = UILayoutPriority(rawValue: 100)
        imageBottomGreatConstraint.isActive = true
        
        NSLayoutConstraint.activate([
            imageViewContainer.widthAnchor.constraint(equalToConstant: imageContainerSize),
            imageViewContainer.heightAnchor.constraint(equalToConstant: imageContainerSize),
            imageViewContainer.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            imageViewContainer.leftAnchor.constraint(equalTo: self.leftAnchor)
            ])
        NSLayoutConstraint.activate([
            self.imageView.widthAnchor.constraint(equalToConstant: imageSize),
            self.imageView.heightAnchor.constraint(equalToConstant: imageSize),
            self.imageView.centerYAnchor.constraint(equalTo: imageViewContainer.centerYAnchor),
            self.imageView.centerXAnchor.constraint(equalTo: imageViewContainer.centerXAnchor)
            ])
        
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(greaterThanOrEqualTo: self.topAnchor),
            stack.bottomAnchor.constraint(lessThanOrEqualTo: self.bottomAnchor),
            stack.leadingAnchor.constraint(equalTo: imageViewContainer.trailingAnchor, constant: 12),
            stack.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            stack.centerYAnchor.constraint(equalTo: self.centerYAnchor)
            ])
        
    }
    
    private func update(animated: Bool = false) {
        let values:HorizontalCardViewPresentable.Values
        
        if let model = self.model {
            values = model.getValues()
        } else {
            values = ("","",nil,UIImage())
        }
        
        let change:(HorizontalCardViewPresentable.Values) -> Void = { (values) in
            if let image = values.image {
                self.imageView.image = image
            } else {
                self.imageView.image = values.placeholderImage
            }
            self.titleLabel.text = values.title
            self.subTitleLabel.text = values.subtitle
        }
        
        let animationTime:TimeInterval = 0.4
        if animated && !self.isAnimating {
            self.isAnimating = true
            self.canApplyValues = true
            UIView.animate(withDuration: animationTime/2, animations: {
                //Fade-out labels
                self.subviews.forEach({ ($0 as? UILabel)?.alpha = 0 })
            }, completion: { (_) in
                change(self.valuesToApply ?? values)
                self.valuesToApply = nil
                self.canApplyValues = false
                //Fade-in labels after changes
                UIView.animate(withDuration: animationTime/2, animations: {
                    self.isAnimating = false
                    self.subviews.forEach({ ($0 as? UILabel)?.alpha = 1 })
                })
            })
        } else {
            if self.isAnimating && self.canApplyValues {
                self.valuesToApply = values
            } else {
                change(values)
            }
        }
    }
    
    //MARK: Public methods
    func viewMustUpdate(animated: Bool) {
        self.update(animated: animated)
    }
    
    func setModel(_ model:HorizontalCardViewPresentable?, animated:Bool = false) {
        self.model = model
        self.update(animated: animated)
    }
}
