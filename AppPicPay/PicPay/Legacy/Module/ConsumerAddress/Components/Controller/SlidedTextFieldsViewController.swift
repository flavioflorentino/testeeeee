//
//  SlidedTextFieldsViewController.swift
//  PicPay
//
//  Created by Vagner Orlandi on 03/04/18.
//

import UIKit

protocol SlidedTextFieldsDelegate: AnyObject {
    typealias Id = String
    func slidedTextFields(viewFor id:Id) -> UIView
    func slidedTextFieldsDidBeginEditing(_ textField:UITextField, id:Id) -> Void
    func slidedTextFieldsDidEndEditing(_ textField:UITextField, id:Id) -> Void
    func slidedTextFields(_ textField: UITextField, id:String, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    func slidedTextFieldsShouldReturn(_ textField:UITextField, id:Id) -> Bool
    func slidedTextFieldsShouldEndEditing(_ textField:UITextField, id:Id) -> Bool
}

extension SlidedTextFieldsDelegate {
    func slidedTextFields(_ textField: UITextField, id:Id, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func slidedTextFieldsDidBeginEditing(_ textField:UITextField, id:Id) -> Void {}
    func slidedTextFieldsDidEndEditing(_ textField:UITextField, id:Id) -> Void {}
    func slidedTextFieldsShouldReturn(_ textField:UITextField, id:Id) -> Bool { return true }
    func slidedTextFieldsShouldEndEditing(_ textField:UITextField, id:Id) -> Bool { return true }
}

final class SlidedTextFieldsViewController: ViewPagination {

    //MARK: Properties
    
    private var textFields:[SlidedTextFieldsDelegate.Id] = []
    private var items:[SlidedTextFieldsDelegate.Id: UITextField] = [:]
    private var viewInset:CGFloat = 10
    private var isActionWithButtonPage: Bool = false
    private var currentContentOffsetX: CGFloat = 0.0
    
    weak var delegate:SlidedTextFieldsDelegate? = nil {
        didSet {
            buildView()
        }
    }
    
    public var currentTextField: UITextField! {
        let index = Int(self.currentPage)
        let indexForTextField = (index < self.textFields.count ? self.textFields[index] : "")
        return self.items[indexForTextField]
    }
    
    public var currentItem:SlidedTextFieldsDelegate.Id {
        get {
            let index = Int(self.currentPage)
            return (index < self.textFields.count ? self.textFields[index] : "")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        scrollToCurrentItem()
    }
    
    private func scrollToCurrentItem() {
        self.setPage(page: Int(currentPage), animated: true)
    }
    
    //MARK: SlidedTextFieldsViewController
    
    convenience init(nibName: String? = nil, bundle: Bundle? = nil, items:[SlidedTextFieldsDelegate.Id]) {
        self.init(nibName: nibName, bundle: bundle)
        self.textFields = items
    }
    
    private func factory(id: SlidedTextFieldsDelegate.Id) -> UIView {
        let view = self.delegate?.slidedTextFields(viewFor: id) ?? { (viewId) in
            let textField = UITextField()
            textField.borderStyle = .roundedRect
            textField.text = viewId
            return textField
        }(id)
        
        if let textField = view as? UITextField {
            textField.delegate = self
            self.items[id] = textField
        } else {
            for view in view.subviews {
                if let textField = view as? UITextField {
                    textField.delegate = self
                    self.items[id] = textField
                    break
                }
            }
        }
        return view
    }
    
    /// Become first response in current text field
    public func currentTextFieldBecomeFirstResponse() {
        guard let textField = self.items[self.textFields[self.pageControl.currentPage]] else {
            return
        }
        textField.becomeFirstResponder()
    }
    
    public var hasNextPage:Bool {
        get {
            return (self.pageControl.currentPage + 1) < self.pageControl.numberOfPages
        }
    }
    
    public var hasPreviousPage:Bool {
        get {
            return self.pageControl.currentPage > 0
        }
    }
    
    public func indexItem(_ item: SlidedTextFieldsDelegate.Id) -> Int? {
        return self.textFields.firstIndex(of: item)
    }
    
    public func contains(item: SlidedTextFieldsDelegate.Id) -> Bool{
        return self.textFields.contains(item)
    }

    public func addItem(_ item: SlidedTextFieldsDelegate.Id, isDynamicInsertion: Bool = false, at index: Int? = nil) {
        let currentPageControl = self.currentPage
        
        if let index = index {
            self.textFields.insert(item, at: index)
        } else {
            self.textFields.append(item)
        }
        
        if isDynamicInsertion {
            // inserts an item dynamically and sets to show a current view
            self.addChild(child: self.factory(id: item), at: index)
            self.pageControl.currentPage = Int(currentPageControl)
        }
    }
    
    public func removeItem(_ item: SlidedTextFieldsDelegate.Id) {
        guard let textField = items[item] else {
            return
        }
        remove(child: textField)
        if let index = textFields.firstIndex(of: item) {
            textFields.remove(at: index)
        }
        items.removeValue(forKey: item)
        orderChildren()
    }
    
    public func nextPage() {
        if self.hasNextPage {
            isActionWithButtonPage = true
            let nextPage = self.pageControl.currentPage + 1
            if nextPage < self.textFields.count {
                var shouldGoNextPage:Bool = true
                if let textField = self.items[self.textFields[self.pageControl.currentPage]] {
                    shouldGoNextPage = textField.endEditing(false)
                }
                if shouldGoNextPage, let textField = self.items[self.textFields[nextPage]] {
                    textField.becomeFirstResponder()
                    self.setPage(page: nextPage, animated: true)
                }
            }
        }
    }
    
    public func previousPage() {
        if self.hasPreviousPage {
            isActionWithButtonPage = true
            let previousPage = self.pageControl.currentPage - 1
            self.setPage(page: previousPage, animated: true)
            if previousPage >= 0 {
                if let textField = self.items[self.textFields[previousPage]] {
                    textField.becomeFirstResponder()
                }
            }
        }
    }

    private func buildView() {
        if self.scrollView.isScrollEnabled {
            self.scrollView.isDirectionalLockEnabled = true
        }
        self.scrollView.clipsToBounds = false
        self.scrollView.frame = self.view.frame
        
        for item in textFields {
            self.add(child: self.factory(id: item))
        }
    }
    
    // MARK: - ViewPagination
    override func add(child: UIView) {
        self.addChild(child: child, at: nil)
        orderChildren()
    }
    
    func addChild(child: UIView, at index: Int? = nil) {
        super.add(child: child)
        
        //reorganize childs position
        if let leadingConstraint = child.superview?.constraints.first(where: {$0.firstItem === child && $0.firstAttribute == .leading}) {
            leadingConstraint.constant += self.viewInset
        }
        
        if let widthConstriant = child.constraints.first(where: {$0.firstAttribute == .width}) {
            widthConstriant.constant -= (2 * self.viewInset)
        }
        
        if index != nil {
            orderChildren()
        }
    }
    
    func orderChildren(index: Int = 0){
        for i in (index..<textFields.count ){
            let item = items[textFields[i]]
            if let leadingConstraint = item?.superview?.constraints.first(where: {$0.firstItem === item && $0.firstAttribute == .leading}) {
                let leading = self.viewInset + (self.scrollView.frame.size.width * CGFloat(i))
                leadingConstraint.constant = leading
            }
        }
    }
    
    // MARK: UIScrollView delagete
    
    /// The delegate of the scroll view has been implemented only to control
    /// if the swipe is enabled to return to the previous fields of the form.
    ///
    /// Remember that it is only possible to use the swipe to return to the previous field and never to advance.
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.isScrollEnabled && !isActionWithButtonPage && scrollView.contentOffset.x > currentContentOffsetX {
            
            /// if this is dragging scroll view, then you will not be able  to scroll
            guard scrollView.isDragging else {
            return
        }
            scrollView.setContentOffset(CGPoint(x: scrollView.frame.size.width * currentPage, y: 0), animated: false)
            return
        }
        
        super.scrollViewDidScroll(scrollView)
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        super.scrollViewDidEndDecelerating(scrollView)
        
        if scrollView.isScrollEnabled {
            if !isActionWithButtonPage {
                
                /// Update the current content offset.
                /// If this current page is a first, update content offset to 0.0
                currentContentOffsetX = scrollView.frame.size.width * currentPage
                
                /// focus to the updated current text field
                self.currentTextField.becomeFirstResponder()
            }
            
            /// update flag to default value
            isActionWithButtonPage = false
        }
    }
    
    override func setPage(page: Int, animated: Bool) {
        super.setPage(page: page, animated: animated)
        if scrollView.isScrollEnabled {

            /// Update the current content offset.
            /// If this current page is a first, update content offset to 0.0
            currentContentOffsetX = scrollView.frame.size.width * CGFloat(page)
            
            /// reset this flag for default value
            isActionWithButtonPage = false
        }
    }
}

// MARK: - UITextFieldDelegate

extension SlidedTextFieldsViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let id = self.items.first(where: {$0.value === textField})?.key ?? ""
        self.delegate?.slidedTextFieldsDidBeginEditing(textField, id: id)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let id = self.items.first(where: {$0.value === textField})?.key ?? ""
        self.delegate?.slidedTextFieldsDidEndEditing(textField, id: id)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let id = self.items.first(where: {$0.value === textField})?.key ?? ""
        return self.delegate?.slidedTextFields(textField, id: id, shouldChangeCharactersIn: range, replacementString: string) ?? true
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        let id = self.items.first(where: {$0.value === textField})?.key ?? ""
        return self.delegate?.slidedTextFieldsShouldEndEditing(textField, id:id) ?? true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let id = self.items.first(where: {$0.value === textField})?.key ?? ""
        return self.delegate?.slidedTextFieldsShouldReturn(textField, id:id) ?? true
    }
}
