import Foundation

enum ConsumerAddressLocalizable: String, Localizable {
    case addNewItem
    case noNumber
    case nickname
    case streetAvenue
    case complementOptional
    case chooseNameForAddress
    case enterStreetName
    case enterNameNeighborhood
    case enterStreetNumber
    case errorFetchingZipCode
    case zipCode
    case itCanBeUsed
    case registerAnAddress
    case buttonRegisterAddress
    case editAddress
    case removeAddress
    case unablePerformThisOperation
    case signatureYouChose
    case chooseAnImageForThisAddress
    case discardChanges
    case continueEditingAddress
    case youHaveMadeUnsavedChanges
    case chooseNicknameForThatAddress
    case streetNameRequired
    case informYourPostalCode
    case newAddress
    case addNewAddress
    case nicknameAddress
    case complement
    case neighborhood
    case proxyHomeAddress
    case proxyHomeAddressDescription
    case proxyConfirmAddress
    
    var file: LocalizableFile {
        return .consumerAddress
    }
    
    var key: String {
        return self.rawValue
    }
}
