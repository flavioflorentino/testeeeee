final class ConsumerAddressItem: Codable {
    var id: Int?
    var addressName: String?
    var zipCode: String?
    var streetName: String?
    var streetNumber: String?
    var addressComplement: String?
    var iconName: String?
    var city: String?
    var state: String?
    var country: String?
    var neighborhood: String?
    
    init(name: String) {
        addressName = name
    }

    init(id: Int, name: String = "") {
        self.id = id
        self.addressName = name
    }

    func updateAddress(with address: ConsumerAddressItem?) {
        guard let address = address else {
            return
        }
        city = address.city
        country = address.country
        neighborhood = address.neighborhood
        state = address.state
        streetName = address.streetName
        addressComplement = address.addressComplement
    }
    
    func updateAll(with consumerAddress: ConsumerAddressItem) {
        addressName = consumerAddress.addressName
        zipCode = consumerAddress.zipCode ?? zipCode
        streetName = consumerAddress.streetName ?? streetName
        streetNumber = consumerAddress.streetNumber ?? streetNumber
        addressComplement = consumerAddress.addressComplement ?? addressComplement
        iconName = consumerAddress.iconName ?? iconName
        city = consumerAddress.city ?? city
        state = consumerAddress.state ?? state
        country = consumerAddress.country ?? country
        neighborhood = consumerAddress.neighborhood ?? neighborhood
    }
    
    func isLike(_ consumerAddress: ConsumerAddressItem) -> Bool {
        if consumerAddress.zipCode == zipCode &&
            consumerAddress.streetName == streetName &&
            consumerAddress.streetNumber == streetNumber &&
            consumerAddress.city == city &&
            consumerAddress.state == state &&
            consumerAddress.neighborhood == neighborhood {
            return true
        }
        return false
    }
}

extension ConsumerAddressItem: Equatable {
    static func == (lhs: ConsumerAddressItem, rhs: ConsumerAddressItem) -> Bool {
        return lhs.id == rhs.id
    }
}
