import UI
import UIKit

final class AskNewAddressViewController: UIViewController {
    // MARK: - SubViews
    lazy var contentView: UIView = {
        let _view = UIView()
        _view.translatesAutoresizingMaskIntoConstraints = false
        _view.backgroundColor = Palette.ppColorGrayscale000.color
        _view.layer.cornerRadius = 8
        _view.layer.masksToBounds = true
        return _view
    }()
    
    lazy var homeImageView: UIImageView = {
        let _imageView = UIImageView(image: #imageLiteral(resourceName: "home_address"))
        _imageView.translatesAutoresizingMaskIntoConstraints = false
        return _imageView
    }()
    
    lazy var titleLabel: UILabel = {
        let _label = UILabel()
        _label.translatesAutoresizingMaskIntoConstraints = false
        _label.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.semibold)
        _label.textAlignment = .center
        _label.text = ConsumerAddressLocalizable.registerAnAddress.text
        return _label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let _label = UILabel()
        _label.translatesAutoresizingMaskIntoConstraints = false
        _label.font = UIFont.systemFont(ofSize: 14)
        _label.textAlignment = .center
        _label.textColor = #colorLiteral(red: 0.4235294118, green: 0.4392156863, blue: 0.4509803922, alpha: 1)
        _label.numberOfLines = 0
        _label.text = ConsumerAddressLocalizable.signatureYouChose.text
        return _label
    }()
    
    lazy var confirmButton: UIButton = {
        let _button = UIButton()
        _button.translatesAutoresizingMaskIntoConstraints = false
        _button.setTitleColor(Palette.ppColorGrayscale000.color, for: .normal)
        _button.backgroundColor = Palette.ppColorBranding300.color
        _button.setTitle(ConsumerAddressLocalizable.buttonRegisterAddress.text, for: .normal)
        _button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
        return _button
    }()
    
    lazy var cancelButton: UIButton = {
        let _button = UIButton()
        _button.translatesAutoresizingMaskIntoConstraints = false
        _button.setTitle(DefaultLocalizable.btCancel.text, for: .normal)
        _button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        _button.backgroundColor = .clear
        _button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
        
        return _button
    }()
    
    // MARK: - Properties
    var confirmButtonAction: ((Any?) -> Void)?
    
    var text: String? {
        get {
            return subtitleLabel.text
        }
        set(newText) {
            subtitleLabel.text = newText
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildView()
        setupButtonActions()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async { [weak self] in
            self?.confirmButton.layer.cornerRadius = (self?.confirmButton.frame.height ?? 0) / 2
            self?.cancelButton.layer.cornerRadius = (self?.cancelButton.frame.height ?? 0) / 2
        }
    }
    
    // MARK: - Private methods
    
    private func setupButtonActions() {
        confirmButton.addTarget(self, action: #selector(AskNewAddressViewController.confirmButtonPressed), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(AskNewAddressViewController.cancelButtonPressed), for: .touchUpInside)
    }
    
    private func buildView() {
        let imageSize: CGFloat = 100
        let horizontalPadding: CGFloat = 28
        let buttonHeight: CGFloat = 44
        
        view.addSubview(contentView)
        contentView.addSubview(homeImageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(confirmButton)
        contentView.addSubview(cancelButton)

        //Constraints
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 12),
            contentView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12),
            contentView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            contentView.heightAnchor.constraint(lessThanOrEqualTo: view.heightAnchor, constant: -150)
        ])

        // - Image view
        NSLayoutConstraint.activate([
            homeImageView.widthAnchor.constraint(equalToConstant: imageSize),
            homeImageView.heightAnchor.constraint(equalToConstant: imageSize),
            homeImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 42),
            homeImageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
        ])
        
        // - Title label
        titleLabel.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .vertical)
        NSLayoutConstraint.activate([
            titleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: horizontalPadding),
            titleLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -horizontalPadding),
            titleLabel.topAnchor.constraint(equalTo: homeImageView.bottomAnchor, constant: 8)
        ])
        
        // - Subtitle label
        subtitleLabel.setContentHuggingPriority(UILayoutPriority.defaultLow, for: .vertical)
        NSLayoutConstraint.activate([
            subtitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: horizontalPadding),
            subtitleLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -horizontalPadding),
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8)
        ])
        
        // - Confirm button
        NSLayoutConstraint.activate([
            confirmButton.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: horizontalPadding),
            confirmButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -horizontalPadding),
            confirmButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            confirmButton.topAnchor.constraint(equalTo: subtitleLabel.bottomAnchor, constant: 24)
        ])
        
        // - Cancel button
        NSLayoutConstraint.activate([
            cancelButton.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: horizontalPadding),
            cancelButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -horizontalPadding),
            cancelButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            cancelButton.topAnchor.constraint(equalTo: confirmButton.bottomAnchor, constant: 12),
            cancelButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16)
        ])
    }
    
    // MARK: - Handlers
    @objc
    func confirmButtonPressed(sender: Any?) {
        dismiss(animated: true, completion: { [weak self] in
            self?.confirmButtonAction?(sender)
        })
    }
    
    @objc
    func cancelButtonPressed(sender: Any?) {
        dismiss(animated: true, completion: nil)
    }
}
