//
//  EditConsumerAddressViewController.swift
//  PicPay
//
//  Created by Vagner Orlandi on 12/04/18.
//

import UIKit
import SkyFloatingLabelTextField
import UI

final class EditAddressViewController: PPBaseViewController {
    // MARK: - SubViews
    lazy var scrollView: UIScrollView = {
        let _scrollView = UIScrollView()
        _scrollView.translatesAutoresizingMaskIntoConstraints = false
        return _scrollView
    }()
    
    lazy var chooseAddressImageLabel: UILabel = {
        let _label = UILabel()
        _label.translatesAutoresizingMaskIntoConstraints = false
        _label.text = ConsumerAddressLocalizable.chooseAnImageForThisAddress.text
        _label.font = UIFont.systemFont(ofSize: 12)
        _label.textColor = Palette.ppColorGrayscale500.color
        return _label
    }()
    
    lazy var addressImagesContainer: UIView = {
        let _view = UIView()
        _view.translatesAutoresizingMaskIntoConstraints = false
        return _view
    }()
    
    lazy var addressImages: EditAddressImagesScrollView = {
        let _images = EditAddressImagesScrollView()
        _images.translatesAutoresizingMaskIntoConstraints = false
        return _images
    }()
    
    lazy var formContainer: UIView = {
        let _view = UIView()
        _view.translatesAutoresizingMaskIntoConstraints = false
        return _view
    }()
    
    lazy var addressForm: EditAddressFormScrollView = { [unowned self] in
        let _form = EditAddressFormScrollView(textFieldDelegate: self)
        _form.translatesAutoresizingMaskIntoConstraints = false
        _form.showsVerticalScrollIndicator = true
        return _form
    }()
    
    private let loader = Loader.getLoadingView(DefaultLocalizable.wait.text)

    // MARK: - Properties
    
    private var viewModel: EditAddressViewModel
    private var lastZipcode: String?
    private var currentTextFieldAbsoluteBottomPosition: CGPoint = .zero
    private var isAskingForNextTextField: Bool = false
    
    var isSaveButtonEnable: Bool = false {
        didSet {
            navigationItem.rightBarButtonItem?.isEnabled = isSaveButtonEnable
        }
    }
    
    var onAddressUpdated: ((ConsumerAddressItem) -> Void)?
    
    // MARK: - init
    
    init(selectedAddress: ConsumerAddressItem) {
        let addressCopy = ConsumerAddressItem(name: selectedAddress.addressName ?? "")
        addressCopy.updateAll(with: selectedAddress)
        addressCopy.id = selectedAddress.id
        self.viewModel = EditAddressViewModel(address: addressCopy)
        super.init(nibName: nil, bundle: nil)
    }
    
    // MARK: - UIViewController
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        viewModel = EditAddressViewModel(address: ConsumerAddressItem(name: DefaultLocalizable.myHouse.text))
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        title = ConsumerAddressLocalizable.editAddress.text
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        let backArrow = #imageLiteral(resourceName: "back")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backArrow, style: .plain, target: self, action: #selector(self.openAlertIfAlteredFields))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.save.text, style: .plain, target: self, action: #selector(self.updateAddress))
        
        buildViews()
        addressForm.setTextFieldValues(viewModel.addressItem)
        navigationItem.rightBarButtonItem?.isEnabled = isSaveButtonEnable
        
        //Dismiss do keyboard
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard)))
        
        if let imageName = viewModel.addressItem.iconName, let addressImage = EditAddressImagesScrollView.AddressImage(rawValue: imageName) {
            addressImages.setActive(addressImage)
        }
        
        addressImages.onAddressImageSelected = { [weak self] (addressImage) in
            guard let strongSelf = self else {
            return
        }
            strongSelf.isSaveButtonEnable = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerKeyboardObservers()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - Methods
    
    private func buildViews() {
        let horizontalMargin: CGFloat = 20
        
        if let loaderView = loader {
            loaderView.layer.zPosition = 100
            let topView = navigationController?.view ?? view
            topView?.addSubview(loaderView)
        }
        
        //chooseImageLabel
        view.addSubview(chooseAddressImageLabel)
        NSLayoutConstraint.activate([
            chooseAddressImageLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            chooseAddressImageLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: horizontalMargin),
            chooseAddressImageLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -horizontalMargin)
        ])
        
        //chooseAddressImageWidget
        addressImagesContainer.addSubview(addressImages)
        NSLayoutConstraint.activate([
            addressImages.topAnchor.constraint(equalTo: addressImagesContainer.topAnchor),
            addressImages.leftAnchor.constraint(equalTo: addressImagesContainer.leftAnchor),
            addressImages.rightAnchor.constraint(equalTo: addressImagesContainer.rightAnchor),
            addressImages.bottomAnchor.constraint(equalTo: addressImagesContainer.bottomAnchor)
        ])
        view.addSubview(addressImagesContainer)
        NSLayoutConstraint.activate([
            addressImagesContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: horizontalMargin),
            addressImagesContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -horizontalMargin),
            addressImagesContainer.topAnchor.constraint(equalTo: chooseAddressImageLabel.bottomAnchor, constant: 8),
            addressImagesContainer.heightAnchor.constraint(equalToConstant: 54)
        ])
        
        //editAddressForm
        formContainer.addSubview(addressForm)
        NSLayoutConstraint.activate([
            addressForm.topAnchor.constraint(equalTo: formContainer.topAnchor),
            addressForm.leftAnchor.constraint(equalTo: formContainer.leftAnchor),
            addressForm.rightAnchor.constraint(equalTo: formContainer.rightAnchor),
            addressForm.bottomAnchor.constraint(equalTo: formContainer.bottomAnchor)
        ])
        view.addSubview(formContainer)
        NSLayoutConstraint.activate([
            formContainer.topAnchor.constraint(equalTo: addressImagesContainer.bottomAnchor, constant: 20),
            formContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: horizontalMargin),
            formContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -horizontalMargin),
            formContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -15)
        ])
    }
    
    private func registerKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func searchZipcode(_ zipcode: String, zipcodeTextField: UITextField? = nil) {
        let unmaskedZipcode = zipcode.onlyNumbers
        addressForm.isLoadingZipcode = true
        viewModel.searchAddress(by: unmaskedZipcode, { [weak self] (address, error) in
            guard let strongSelf = self else {
            return
        }
            strongSelf.addressForm.isLoadingZipcode = false
            if error == nil {
                strongSelf.addressForm.setTextFieldValues(address, onlyAddress: true)
                strongSelf.lastZipcode = zipcode
            } else {
                (zipcodeTextField as? SkyFloatingLabelTextField)?.errorMessage = strongSelf.viewModel.getErrorMessageCEP(error: error)
            }
        })
    }
    
    private func updateModelAddress() {
        let form = addressForm.getTextFieldValues()
        for (fieldName, newValue) in form {
            viewModel.setFieldValue(fieldName: fieldName, newValue: newValue)
        }
        viewModel.addressItem.iconName = addressImages.getActiveOption().rawValue
    }
    
    @objc
    private func updateAddress() {
        formContainer.endEditing(true)
        //Se houver algum campo com erro
        if let textFieldWithError = addressForm.nextError() {
            //move o foco para esse campo de texto
            textFieldWithError.becomeFirstResponder()
            return
        }
        
        updateModelAddress()
        loader?.isHidden = false
        isSaveButtonEnable = false
        viewModel.updateAddress { [weak self] (updatedAddress, error) in
            guard let strongSelf = self else {
            return
        }
            strongSelf.isSaveButtonEnable = false
            strongSelf.loader?.isHidden = true
            
            if error == nil {
                strongSelf.view.endEditing(true)
                if let onAddressUpdatedCallback = strongSelf.onAddressUpdated {
                    onAddressUpdatedCallback(updatedAddress ?? strongSelf.viewModel.addressItem)
                }
                strongSelf.close()
            } else {
                strongSelf.isSaveButtonEnable = true
                AlertMessage.showAlert(error, controller: self)
            }
        }
    }
    
    private func close() {
        if navigationController?.viewControllers.count > 1 {
            navigationController?.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - Handlers
    
    @objc
    func openAlertIfAlteredFields() {
        if isSaveButtonEnable {
            let discardButton = Button(title: ConsumerAddressLocalizable.discardChanges.text, type: .destructive) { [weak self] alertController, _ in
                alertController.dismiss(animated: true, completion: {
                    self?.close()
                })
            }
            let cancelButton = Button(title: ConsumerAddressLocalizable.continueEditingAddress.text, type: .clean, action: .close)
            let alert = Alert(with: DefaultLocalizable.attention.text, text: ConsumerAddressLocalizable.youHaveMadeUnsavedChanges.text, buttons: [discardButton, cancelButton])
            AlertMessage.showAlert(alert, controller: self)
        } else {
           close()
        }
    }
    
    @objc
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            let avaibleScreen = UIScreen.main.bounds.height - keyboardSize.height
            let keyboardPadding: CGFloat = 20
            if (addressForm.contentOffset.y + currentTextFieldAbsoluteBottomPosition.y) > (avaibleScreen - keyboardPadding) {
                let scrollNeededToAvoidKeyboard = currentTextFieldAbsoluteBottomPosition.y - avaibleScreen + keyboardPadding
                addressForm.contentOffset.y += scrollNeededToAvoidKeyboard
            } else {
                addressForm.contentOffset.y = 0
            }
        }
    }
    
    @objc
    func keyboardWillHide(notification: NSNotification) {
        if addressForm.contentOffset.y != 0 && !isAskingForNextTextField {
            addressForm.contentOffset.y = 0
        }
    }
    
    @objc
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

// MARK: - UITextFieldDelegate

extension EditAddressViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let fieldName = addressForm.getFormFieldName(textField) else { return true }
        
        switch fieldName {
        case .name, .zipcode, .street, .streetNumber:
            isAskingForNextTextField = true
            addressForm.nextField(current: textField)
            isAskingForNextTextField = false
            
        case .complement:
            updateAddress()
            
        default:
            break
        }
        
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let superview = textField.superview else {
            return
        }
        currentTextFieldAbsoluteBottomPosition = superview.convert(textField.frame.origin, to: nil)
        currentTextFieldAbsoluteBottomPosition.y += textField.frame.height
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        currentTextFieldAbsoluteBottomPosition = .zero
        let floatingLabelTextField = textField as? SkyFloatingLabelTextField
        
        guard let fieldName = addressForm.getFormFieldName(textField), let text = textField.text else {
            return
        }
        
        switch fieldName {
        case .name:
            floatingLabelTextField?.errorMessage = text.isEmpty ? ConsumerAddressLocalizable.chooseNicknameForThatAddress.text : nil
            
        case .street:
            floatingLabelTextField?.errorMessage = text.isEmpty ? ConsumerAddressLocalizable.streetNameRequired.text : nil
            
        case .zipcode:
            if let errorMessage = floatingLabelTextField?.errorMessage, !errorMessage.isEmpty {
                break // O servidor já definiu uma mensagem de erro
            } else if text.count != 9 {
                floatingLabelTextField?.errorMessage = ConsumerAddressLocalizable.informYourPostalCode.text
            }
            
        default:
            break
        }
        updateModelAddress()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let fieldName = addressForm.getFormFieldName(textField) else { return true }
        isSaveButtonEnable = true
        var shouldChange: Bool = true
        let oldString = textField.text ?? ""
        let newString = (oldString as NSString).replacingCharacters(in: range, with: string)
        
        let clearTextFieldErrorIfNeeded: ((UITextField) -> Void) = { (field) in
            if (field as? SkyFloatingLabelTextField)?.errorMessage != nil {
                (field as? SkyFloatingLabelTextField)?.errorMessage = nil
            }
        }
        
        switch fieldName {
        case .name, .street:
            clearTextFieldErrorIfNeeded(textField)
            
        case .zipcode:
            if addressForm.isLoadingZipcode {
                return false
            }
            if newString.count <= 9 {
                (textField as? SkyFloatingLabelTextField)?.errorMessage = nil
                viewModel.applyZipcodeMask(textField, stringChanged: string, on: range, newString: newString, oldString: oldString)
            }
            
            if newString.count == 9 && newString != lastZipcode {
                searchZipcode(newString, zipcodeTextField: textField)
            }
            shouldChange = false
            
        default:
            break
        }
        
        return shouldChange
    }
}

// MARK: - UIGestureRecognizerDelegate

extension EditAddressViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == navigationController?.interactivePopGestureRecognizer && isSaveButtonEnable {
            openAlertIfAlteredFields()
            return false
        }
        return true
    }
}
