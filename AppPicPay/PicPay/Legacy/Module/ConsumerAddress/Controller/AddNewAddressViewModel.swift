//
//  AddNewAddressViewModel.swift
//  PicPay
//
//  Created by PicPay Eduardo on 12/12/18.
//

import Foundation

final class AddNewAddressViewModel {
    private let api: ApiConsumerAddressProtocol
    var newAddressItem: ConsumerAddressItem
    
    init(api: ApiConsumerAddressProtocol = ApiConsumerAddress()) {
        self.api = api
        newAddressItem = ConsumerAddressItem(name: DefaultLocalizable.myHouse.text)
    }
    
    func saveNewAddress(_ completion: @escaping ((ConsumerAddressItem?, Error?) -> Void) ) {
        api.save(address: newAddressItem) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let savedAddress):
                    completion(savedAddress, nil)
                case .failure(let error):
                    completion(nil, error)
                }
            }
        }
    }
    
    func searchAddress(by zipcode: String, _ completion: @escaping ((ConsumerAddressItem?, Error?) -> Void)) {
        api.getAddress(by: zipcode) { [weak self] (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let addressFound):
                    self?.newAddressItem.updateAddress(with: addressFound)
                    completion(addressFound, nil)
                case .failure(let error):
                    completion(nil, error)
                }
            }
        }
    }
    
    func getErrorMessageCEP(error: Error?) -> String {
        if let serverErrorMessage = error?.localizedDescription, !serverErrorMessage.isEmpty {
            return serverErrorMessage
        } else {
            return ConsumerAddressLocalizable.errorFetchingZipCode.text
        }
    }
}
