//
//  AddressListViewModel.swift
//  PicPay
//
//  Created by PicPay Eduardo on 12/3/18.
//

import Foundation

enum AddresListTableHeaderState {
    case connectionError, tableItemError, emptyList, noHeader, contextHeader, undefined
}

final class AddressListViewModel {
    private let api: ApiConsumerAddressProtocol
    private var addressList: [ConsumerAddressItem]?
    var selectedAddress: ConsumerAddressItem? // Shows checkmark icon on the respective cell
    
    var isLoading: Bool = false
    var tableHeaderState: AddresListTableHeaderState = .undefined
    var displayContextHeader: Bool = false
    private let title: String
    
    init(api: ApiConsumerAddressProtocol = ApiConsumerAddress(), selectedAddress: ConsumerAddressItem? = nil, displayContextHeader: Bool = false, title: String = DefaultLocalizable.addressTitle.text) {
        self.api = api
        self.displayContextHeader = displayContextHeader
        self.selectedAddress = selectedAddress
        self.title = title
    }
    
    func getAddressList(_ completion: @escaping ((Error?) -> Void)) {
        isLoading = true
        api.getAddressList { [weak self] (response) in
            self?.isLoading = false
            switch response {
            case .success(let result):
                self?.addressList = result
                self?.computeTableHeaderState()
                completion(nil)
            case .failure(let error):
                self?.computeTableHeaderState(error: error)
                completion(error)
            }
        }
    }
    
    func removeAddress(_ address: ConsumerAddressItem, _ completion: @escaping ((Error?) -> Void)) {
        guard let addressId = address.id else {
            completion(PicPayError(message: ConsumerAddressLocalizable.unablePerformThisOperation.text))
            return
        }
        api.remove(addressId: addressId) { [weak self] (response) in
            DispatchQueue.main.async { [weak self] in
                switch response {
                case .success:
                    if let index = self?.addressList?.firstIndex(of: address) {
                        self?.addressList?.remove(at: index)
                    }
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }
    
    func getAddressListSize() -> Int {
        return addressList?.count ?? 0
    }
    
    func isAddressListLoaded() -> Bool {
        return addressList != nil
    }
    
    func needsInitializeTableHeader() -> Bool {
        return tableHeaderState == .undefined && !isLoading
    }
    
    func getAddress(at index: Int) -> ConsumerAddressItem? {
        return (index < getAddressListSize()) ? addressList?[index] : nil
    }
    
    func isSelected(address: ConsumerAddressItem?) -> Bool {
        guard let _selectedAddress = selectedAddress, let _address = address else {
    return false
}
        return _address.id == _selectedAddress.id || _address.isLike(_selectedAddress)
    }
    
    func appendNewAddress(_ newAddress: ConsumerAddressItem) {
        addressList?.append(newAddress)
    }
    
    func updateAddress(address: ConsumerAddressItem, onIndex index: Int) {
        if let list = addressList, list.indices.contains(index) {
            addressList?[index] = address
        }
    }

    func getViewTitle() -> String? {
        return displayContextHeader ? nil : title
    }
    
    func computeTableHeaderState(error: Error? = nil) {
        if error != nil {
            if let _error = error as NSError?, _error.isConnectionError() || addressList == nil {
                tableHeaderState = .connectionError
            } else {
                tableHeaderState = .tableItemError
            }
        } else if getAddressListSize() == 0 {
            tableHeaderState = .emptyList
        } else if displayContextHeader {
            tableHeaderState = .contextHeader
        } else {
            tableHeaderState = .noHeader
        }
    }
    
    func getNumberOfRows() -> Int {
        let numberOfAddresses = getAddressListSize()
        // "numberOfAddresses + 1" para mostrar uma AddNewAddressTableViewCell
        return numberOfAddresses == 0 ? 0 : numberOfAddresses + 1
    }
}
