import UI
import UIKit
import SkyFloatingLabelTextField

final class AddNewAddressViewController: PPBaseViewController {
    fileprivate enum TextFields: String {
        case nome = "Nome"
        case cep = "Cep"
        case rua = "Rua"
        case numero = "Numero"
        case complemento = "Complemento"
        case bairro = "Bairro"
        
        static let allValues = [nome, cep, rua, numero, complemento]
    }
    
    typealias ContraintsConstants = (cardHeight: CGFloat, cardCenterY: CGFloat, headerHeight: CGFloat)
    
    // MARK: - SubViews
    
    lazy private var headerView: AddNewAddressHeaderView = {
        let _view = AddNewAddressHeaderView()
        _view.translatesAutoresizingMaskIntoConstraints = false
        _view.backgroundColor = Palette.ppColorBranding300.color
        return _view
    }()
    
    lazy private var slideTextFieldsContainer: UIView = {
        let _view = UIView()
        _view.translatesAutoresizingMaskIntoConstraints = false
        return _view
    }()
    
    lazy private var accessoryView: AddNewAddressAccessoryView = {
        let _view = AddNewAddressAccessoryView()
        _view.backgroundColor = Palette.ppColorGrayscale000.color
        return _view
    }()
    
    lazy var zipcodeLoadingView: UIActivityIndicatorView = {
        let _view = UIActivityIndicatorView(style: .gray)
        _view.translatesAutoresizingMaskIntoConstraints = false
        _view.hidesWhenStopped = true
        return _view
    }()
    
    lazy private var withoutNumberButton: UIButton = {
        let _button = UIButton()
        let unselectedColorButton = #colorLiteral(red: 0.7333333333, green: 0.7333333333, blue: 0.7333333333, alpha: 1)
        let selectedColotButton = Palette.ppColorBranding300.color
        _button.translatesAutoresizingMaskIntoConstraints = false
        _button.setTitle(ConsumerAddressLocalizable.noNumber.text, for: .normal)
        _button.setTitleColor(unselectedColorButton, for: .normal)
        _button.setTitleColor(selectedColotButton, for: .selected)
        _button.tintColor = _button.titleColor(for: .normal)
        _button.setImage(#imageLiteral(resourceName: "iconUnchecked").withRenderingMode(.alwaysTemplate), for: .normal)
        _button.setImage(#imageLiteral(resourceName: "iconChecked").withRenderingMode(.alwaysTemplate), for: .selected)
        _button.contentEdgeInsets = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
        _button.contentHorizontalAlignment = .left
        _button.imageEdgeInsets.left = 0
        _button.imageEdgeInsets.top = 4
        _button.imageEdgeInsets.bottom = 4
        _button.imageEdgeInsets.right = 4
        _button.imageView?.contentMode = .scaleAspectFit
        _button.backgroundColor = view.backgroundColor
        _button.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.medium)
        return _button
    }()
    
    private let loader = Loader.getLoadingView(DefaultLocalizable.wait.text)
    
    private var slidedTextFieldsViewController: SlidedTextFieldsViewController? {
        return children.first(where: { $0 is SlidedTextFieldsViewController }) as? SlidedTextFieldsViewController
    }
    
    // MARK: - Properties
    
    private let zipcodeStringLength: Int = 9
    private var lastZipcode: String?
    private var isMovingForward: Bool = false
    private var isNavigationAllowed: Bool = true {
        didSet {
            accessoryView.isNextButtonEnabled = isNavigationAllowed
            accessoryView.isPreviousButtonEnabled = isNavigationAllowed
        }
    }
    
    var viewModel: AddNewAddressViewModel
    
    var onAddressSaved: ((ConsumerAddressItem?) -> Void)?
    var onCancel: ((ConsumerAddressItem?) -> Void)?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - init
    
    init (viewModel: AddNewAddressViewModel = AddNewAddressViewModel()) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        viewModel = AddNewAddressViewModel()
        super.init(coder: aDecoder)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        
        buildViews()
        setupActions()
        createSlideView()
        headerView.addressCardView.setAddress(viewModel.newAddressItem)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        slidedTextFieldsViewController?.currentTextFieldBecomeFirstResponse()
    }
    
    // MARK: - Private methods
    
    private func createSlideView() {
        let vc = SlidedTextFieldsViewController(items: TextFields.allValues.map({ $0.rawValue }))
        addChild(vc)
        view.layoutIfNeeded()
        vc.view.frame = CGRect(x: 0, y: 0, width: slideTextFieldsContainer.frame.size.width, height: slideTextFieldsContainer.frame.size.height)
        vc.scrollView.frame = CGRect(x: 0, y: 0, width: slideTextFieldsContainer.frame.size.width, height: slideTextFieldsContainer.frame.size.height)
        vc.delegate = self
        vc.disable() //disable scroll
        slideTextFieldsContainer.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    
    private func setupActions() {
        headerView.closeButton.addTarget(self, action: #selector(self.closeActionHandler), for: .touchUpInside)
        accessoryView.nextButton.addTarget(self, action: #selector(self.nextButtonAction), for: .touchUpInside)
        accessoryView.previousButton.addTarget(self, action: #selector(self.previousButtonAction), for: .touchUpInside)
        withoutNumberButton.addTarget(self, action: #selector(self.withoutNumberButtonAction), for: .touchUpInside)
    }
    
    private func buildViews() {
        let headerHeight: CGFloat = 215
       
        view.addSubview(headerView)
        view.addSubview(slideTextFieldsContainer)
        
        if let loaderView = loader {
            view.addSubview(loaderView)
        }
        
        //Header
        let headerContainerHeightConstraint = headerView.heightAnchor.constraint(equalToConstant: headerHeight)
        headerView.headerHeightConstraint = headerContainerHeightConstraint
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: view.topAnchor),
            headerView.rightAnchor.constraint(equalTo: view.rightAnchor),
            headerView.leftAnchor.constraint(equalTo: view.leftAnchor),
            headerContainerHeightConstraint
        ])
        headerView.setupSubviewsContraints()
        
        //SlidedTextFields
        NSLayoutConstraint.activate([
            slideTextFieldsContainer.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 20),
            slideTextFieldsContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 30),
            slideTextFieldsContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -30),
            slideTextFieldsContainer.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        accessoryView.setupSubviewsContraints()
    }
    
    private func defaultTextfield(placeholder: String) -> UITextField {
        let unselectedColor = Palette.ppColorGrayscale500.color
        let selectedColor = Palette.ppColorBranding300.color(withCustomDark: .ppColorGrayscale000)
        let selectedColorLine = Palette.ppColorBranding300.color
        let selectedColorText = Palette.ppColorGrayscale500.color(withCustomDark: .ppColorGrayscale000)
        
        let textField = SkyFloatingLabelTextField()
        textField.placeholder = placeholder
        textField.lineHeight = 2
        textField.font = UIFont.systemFont(ofSize: 16)
        
        textField.lineColor = unselectedColor
        textField.titleColor = unselectedColor
        textField.placeholderColor = unselectedColor
        
        textField.selectedLineColor = selectedColorLine
        textField.selectedTitleColor = selectedColor
        textField.textColor = selectedColorText
        
        textField.titleFormatter = { $0 }
        textField.titleLabel.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.medium)
        
        textField.autocorrectionType = .no
        textField.inputAccessoryView = accessoryView
        
        return textField
    }
    
    private func saveAddress() {
        loader?.isHidden = false
        view.endEditing(true)
        if UIScreen.main.bounds.height == 480 {
            changeHeaderSize(isKeyboardVisible: false)
        }
        viewModel.saveNewAddress { [weak self] (savedAddress, error) in
            self?.loader?.isHidden = true
           
            if error == nil {
                self?.onAddressSaved?(savedAddress)
                self?.dismiss(animated: true, completion: nil)
            } else {
                AlertMessage.showAlert(error, controller: self)
            }
        }
    }

    private func updateAddress(for zipcode: String, zipcodeTextField: UITextField? = nil) {
        let unmaskedZipcode = zipcode.onlyNumbers
        zipcodeLoadingView.startAnimating()
        isNavigationAllowed = false
        viewModel.searchAddress(by: unmaskedZipcode, { [weak self] (address, error) in
            guard let strongSelf = self else {
            return
        }
            strongSelf.zipcodeLoadingView.stopAnimating()
            strongSelf.accessoryView.isPreviousButtonEnabled = true
            if error == nil {
                strongSelf.headerView.addressCardView.viewMustUpdate(animated: true)
                strongSelf.accessoryView.isNextButtonEnabled = true
                strongSelf.lastZipcode = zipcode
                
                // if the address don't has neightborhood then add a new field for user input it
                let neighborhood = address?.neighborhood ?? ""
                if neighborhood.isEmpty && strongSelf.slidedTextFieldsViewController?.contains(item: TextFields.bairro.rawValue) == false {
                    if let cepFieldIndex = strongSelf.slidedTextFieldsViewController?.indexItem(TextFields.cep.rawValue) {
                        strongSelf.slidedTextFieldsViewController?.addItem(TextFields.bairro.rawValue, isDynamicInsertion: true, at: cepFieldIndex + 1)
                    }
                } else {
                    strongSelf.slidedTextFieldsViewController?.removeItem(TextFields.bairro.rawValue)
                }
            } else {
                strongSelf.lastZipcode = ""
                (zipcodeTextField as? SkyFloatingLabelTextField)?.errorMessage = strongSelf.viewModel.getErrorMessageCEP(error: error)
            }
        })
    }
    
    
    private func changeHeaderSize(isKeyboardVisible: Bool) {
        let minimalSize: ContraintsConstants = (70, 53, 140)
        let fullSize: ContraintsConstants = (85, 85, 215)
        let constant = isKeyboardVisible ? minimalSize : fullSize
        
        guard let oldConstant = headerView.headerHeightConstraint?.constant,
            oldConstant != constant.headerHeight else {
            return
        }
        
        headerView.addressCardHeightConstraint?.constant = constant.cardHeight
        headerView.addressCardCenteYConstraint?.constant = constant.cardCenterY
        headerView.headerHeightConstraint?.constant = constant.headerHeight
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Handlers
    
    @objc
    func closeActionHandler(sender: Any?) {
        dismiss(animated: true, completion: { [weak self] in
            self?.onCancel?(self?.viewModel.newAddressItem)
        })
        view.endEditing(true)
    }
    
    @objc
    func nextButtonAction(sender: Any?) {
        if let controller = slidedTextFieldsViewController {
            if controller.hasNextPage {
                isMovingForward = true
                controller.nextPage()
                isMovingForward = false
            } else {
                saveAddress()
            }
        }
    }
    
    @objc
    func previousButtonAction(sender: Any?) {
        if let controller = slidedTextFieldsViewController {
            controller.previousPage()
        }
    }
    
    @objc
    func withoutNumberButtonAction(sender: Any?) {
        withoutNumberButton.isSelected = !withoutNumberButton.isSelected
        if withoutNumberButton.isSelected {
            let isEmpty = (withoutNumberButton.superview as? UITextField)?.text?.isEmpty ?? true
            (withoutNumberButton.superview as? UITextField)?.text = ""
            (withoutNumberButton.superview as? SkyFloatingLabelTextField)?.errorMessage = nil
            accessoryView.isNextButtonEnabled = true
            viewModel.newAddressItem.streetNumber = ""
            headerView.addressCardView.viewMustUpdate(animated: !isEmpty)
            withoutNumberButton.tintColor = withoutNumberButton.titleColor(for: .selected)
        } else {
            withoutNumberButton.tintColor = withoutNumberButton.titleColor(for: .normal)
        }
    }
}

// MARK: - SlidedTextFieldsDelegate

extension AddNewAddressViewController: SlidedTextFieldsDelegate {
    func slidedTextFields(viewFor id: SlidedTextFieldsDelegate.Id) -> UIView {
        let textField = defaultTextfield(placeholder: id)
        
        //Altera return key do teclado
        if TextFields.allValues.last?.rawValue != id {
            textField.returnKeyType = .next
        } else {
            textField.returnKeyType = .done
        }
        
        guard let textFieldId = TextFields(rawValue: id) else { return textField }
        
        switch textFieldId {
        case .nome:
            textField.text = viewModel.newAddressItem.addressName
            textField.placeholder = ConsumerAddressLocalizable.nickname.text
        
        case .cep:
            textField.placeholder = ConsumerAddressLocalizable.zipCode.text
            textField.keyboardType = .numberPad
            textField.rightView = zipcodeLoadingView
            textField.rightViewMode = .always
            
        case .rua:
            textField.placeholder = ConsumerAddressLocalizable.streetAvenue.text

        case .numero:
            textField.placeholder = DefaultLocalizable.number.text
            textField.keyboardType = .numbersAndPunctuation
            textField.rightView = withoutNumberButton
            textField.rightViewMode = .always
            (textField as? SkyFloatingLabelTextField)?.titleLabel.layer.zPosition = 2

        case .complemento:
            textField.placeholder = ConsumerAddressLocalizable.complementOptional.text
            
        default:
            break
        }
        
        return textField
    }
    
    func slidedTextFieldsDidBeginEditing(_ textField: UITextField, id: SlidedTextFieldsDelegate.Id) {
        guard let textFieldId = TextFields(rawValue: id) else {
            return
        }
        
        if UIScreen.main.bounds.height == 480 {
            changeHeaderSize(isKeyboardVisible: true)
        }

        switch textFieldId {
        case .nome:
            accessoryView.isPreviousButtonEnabled = false
            accessoryView.isNextButtonEnabled = true
            UIView.animate(withDuration: 0.25, animations: {
                self.accessoryView.previousButton.alpha = 0
            })
            
        case .cep:
            accessoryView.isPreviousButtonEnabled = true
            accessoryView.isNextButtonEnabled = lastZipcode == (textField.text ?? "notNil")
            UIView.animate(withDuration: 0.25, animations: {
                self.accessoryView.previousButton.alpha = 1
            })
            
        case .rua:
            isNavigationAllowed = true
            textField.text = viewModel.newAddressItem.streetName
            
        case .numero:
            isNavigationAllowed = true
            accessoryView.nextButton.setTitle(DefaultLocalizable.next.text, for: .normal)
        
        case .bairro:
            isNavigationAllowed = true
            accessoryView.nextButton.setTitle(DefaultLocalizable.next.text, for: .normal)
            
        case .complemento:
            isNavigationAllowed = true
            textField.text = viewModel.newAddressItem.addressComplement
            accessoryView.nextButton.setTitle(DefaultLocalizable.save.text, for: .normal)
        }
    }
    
    private func clearTextFieldErrorIfNeeded(_ textField: UITextField) {
        guard let floatingTextfield = textField as? SkyFloatingLabelTextField else {
            return
        }
        if floatingTextfield.errorMessage != nil {
            floatingTextfield.errorMessage = nil
            accessoryView.isNextButtonEnabled = true
        }
    }
    
    private func changeCharactersInZipcodeField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String, oldString: String, newString: String) {
        if newString.count <= zipcodeStringLength {
            accessoryView.isNextButtonEnabled = false
            (textField as? SkyFloatingLabelTextField)?.errorMessage = nil
            let maskedString = CustomStringMask(mask: "00000-000").maskedText(from: newString.onlyNumbers) ?? ""
            textField.text = maskedString
            viewModel.newAddressItem.zipCode = maskedString
    
            let newPosition: Int
            if newString.count < oldString.count {
                //Removeu, move para o inicio da alteração
                newPosition = range.location
            } else {
                let maskExtraChars = maskedString.count - newString.count
                let addedTextLength = string.count + maskExtraChars
                //Adicionou, move para o final da alteração, considerando o tambem o texto da mascara
                newPosition = range.location + addedTextLength
            }
    
            if let textPosition = textField.position(from: textField.beginningOfDocument, offset: newPosition) {
                //Se a posição é valida para o textField, move o cursor para a posição
                textField.selectedTextRange = textField.textRange(from: textPosition, to: textPosition)
            }
        }
    
        if newString.count == zipcodeStringLength && newString != lastZipcode {
            updateAddress(for: newString, zipcodeTextField: textField)
        }
    }
    
    func slidedTextFields(_ textField: UITextField, id: SlidedTextFieldsDelegate.Id, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldId = TextFields(rawValue: id) else { return true }
        var shouldChange: Bool = true
        let oldString = textField.text ?? ""
        let newString = (oldString as NSString).replacingCharacters(in: range, with: string)
        
        switch textFieldId {
        case .nome:
            clearTextFieldErrorIfNeeded(textField)
            viewModel.newAddressItem.addressName = newString
            
        case .cep:
            if zipcodeLoadingView.isAnimating {
                return false
            }
            changeCharactersInZipcodeField(textField, shouldChangeCharactersIn: range, replacementString: string, oldString: oldString, newString: newString)
            shouldChange = false
            
        case .rua:
            clearTextFieldErrorIfNeeded(textField)
            viewModel.newAddressItem.streetName = newString
            
        case .numero:
            clearTextFieldErrorIfNeeded(textField)
            if withoutNumberButton.isSelected {
                //Desmarca o botão de `sem numero` ao alterar o campo de texto
                withoutNumberButton.isSelected = false
                withoutNumberButton.tintColor = withoutNumberButton.titleColor(for: .normal)
            }
            viewModel.newAddressItem.streetNumber = newString
            
        case .complemento:
            viewModel.newAddressItem.addressComplement = newString
            
        case .bairro:
            viewModel.newAddressItem.neighborhood = newString
            if !newString.isEmpty {
                clearTextFieldErrorIfNeeded(textField)
            }
        }
        
        if textFieldId != .cep {
            headerView.addressCardView.viewMustUpdate(animated: false)
        }
        
        return shouldChange
    }
    
    func slidedTextFieldsShouldReturn(_ textField: UITextField, id: SlidedTextFieldsDelegate.Id) -> Bool {
        guard let textFieldId = TextFields(rawValue: id) else { return true }
        
        switch textFieldId {
        case .nome, .cep, .rua, .numero, .bairro:
            nextButtonAction(sender: nil)
        
        case .complemento:
            saveAddress()
        }
        
        return true
    }
    
    func slidedTextFieldsShouldEndEditing(_ textField: UITextField, id: Id) -> Bool {
        if isBeingDismissed || !isMovingForward {
            return true
        }
        
        guard let textFieldId = TextFields(rawValue: id) else { return true }
        var errorMessage: String?

        if (textField.text ?? "").isEmpty {
            switch textFieldId {
            case .nome:
                errorMessage = ConsumerAddressLocalizable.chooseNameForAddress.text
                
            case .rua:
                errorMessage = ConsumerAddressLocalizable.enterStreetName.text
                
            case .bairro:
                errorMessage = ConsumerAddressLocalizable.enterNameNeighborhood.text
                
            case .numero:
                if !withoutNumberButton.isSelected {
                    errorMessage = ConsumerAddressLocalizable.enterStreetNumber.text
                }
                
            default:
                break
            }
            
            if let errorMsg = errorMessage {
                (textField as? SkyFloatingLabelTextField)?.errorMessage = errorMsg
                accessoryView.isNextButtonEnabled = false
                return false
            }
        }
        return true
    }
}
