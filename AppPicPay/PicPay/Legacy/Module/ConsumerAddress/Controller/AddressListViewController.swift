import UI
import UIKit
import SecurityModule
import SnapKit
import Core

@objc
final class AddressListViewController: PPBaseViewController, Obfuscable {
    enum CellIdentifier: String {
        case addressCard = "AddressCardCell"
        case addItem = "addAddressCell"
    }
    
    // MARK: - Subviews
    
    lazy var tableView: UITableView = {
        let _tableView = UITableView()
        _tableView.backgroundColor = Palette.ppColorGrayscale100.color
        _tableView.showsVerticalScrollIndicator = false
        _tableView.rowHeight = UITableView.automaticDimension
        _tableView.estimatedRowHeight = 84
        _tableView.tableFooterView = UIView()
        _tableView.separatorColor = Palette.ppColorGrayscale100.color
        _tableView.separatorInset = .zero
        return _tableView
    }()
    
    // MARK: - Properties
    
    internal var viewModel: AddressListViewModel
    
    var onAddressSelected: ((ConsumerAddressItem?) -> Void)?
    var onNewAddressAdded: ((ConsumerAddressItem?) -> Void)?
    
    private var loadingView: UILoadView?
    lazy var refreshControl: UIRefreshControl = {
        let refreshCtrl = UIRefreshControl()
        refreshCtrl.addTarget(self, action: #selector(AddressListViewController.refreshTableView), for: .valueChanged)
        return refreshCtrl
    }()
    
    var askNewAddressReasonText: String = ConsumerAddressLocalizable.itCanBeUsed.text
    var addressContextTitle: String = ConsumerAddressLocalizable.registerAnAddress.text
    var addressContextText: String = ConsumerAddressLocalizable.itCanBeUsed.text
    var addressContextButton: String = ConsumerAddressLocalizable.buttonRegisterAddress.text
    var addressContextImage: UIImage = #imageLiteral(resourceName: "ilu_home_100")
    
    //Screen obfuscation
    var appSwitcherObfuscator: AppSwitcherObfuscating?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - init
    init(viewModel: AddressListViewModel = AddressListViewModel()) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    // MARK: - UIViewController
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        viewModel = AddressListViewModel()
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(tableView)
        title = ""
        
        if let topItem = navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
        
        setupTableView()
        getAddressList(showLoading: true)
        title = viewModel.getViewTitle()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObfuscationConfiguration()

        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deinitObfuscator()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if viewModel.needsInitializeTableHeader() {
            updateTableHeader()
        }
    }
    

    
    // MARK: - Loading Methods
    
    func startLoading() {
        loadingView?.removeFromSuperview()
        loadingView = UILoadView(superview: view)
        loadingView?.startLoading()
    }
    
    func stopLoading() {
        loadingView?.animatedRemoveFromSuperView()
    }
    
    // MARK: - Private methods
    
    private func setupTableView() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(AddressCardTableViewCell.self, forCellReuseIdentifier: CellIdentifier.addressCard.rawValue)
        tableView.register(AddNewAddressTableViewCell.self, forCellReuseIdentifier: CellIdentifier.addItem.rawValue)
        tableView.refreshControl = refreshControl
    }

    @objc
    private func getAddressList(showLoading: Bool, _ completion: ((Error?) -> Void)? = nil) {
        if showLoading {
            startLoading()
        }
        viewModel.getAddressList { [weak self] (error) in
            DispatchQueue.main.async {
                if showLoading {
                    self?.stopLoading()
                }
                self?.tableView.reloadData()
                self?.updateTableHeader(withError: error)
                if let callback = completion {
                    callback(error)
                }
            }
        }
    }
    
    func updateTableHeader(withError error: Error? = nil) {
        viewModel.computeTableHeaderState(error: error)
        var headerView: UIView?
        
        switch viewModel.tableHeaderState {
        case .connectionError:
            let errorView = ConnectionErrorView(frame: view.frame)
            errorView.tryAgainTapped = { [weak self] in
                self?.getAddressList(showLoading: true)
            }
            headerView = errorView
        case .tableItemError :
            headerView = TableItemErrorView(frame: CGRect(origin: .zero, size: CGSize(width: view.frame.size.width, height: 70) ))
            headerView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(getAddressList)))
            
        case .noHeader, .undefined:
            headerView = nil
            
        case .emptyList:
            headerView = createOnboardHeader(isFull: true)
        
        case .contextHeader:
            headerView = createOnboardHeader(isFull: false)
        }
        
        tableView.tableHeaderView = headerView
    }

    private func showOptionsForAddress(at indexPath: IndexPath) {
        let optionsActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        optionsActionSheet.addAction(UIAlertAction(title: ConsumerAddressLocalizable.editAddress.text, style: .default, handler: { [weak self] (_) in
            self?.editAddress(at: indexPath)
        }))
        
        optionsActionSheet.addAction(UIAlertAction(title:ConsumerAddressLocalizable.removeAddress.text, style: .destructive, handler: { [weak self] (_) in
            self?.removeAddress(at: indexPath)
        }))
        
        optionsActionSheet.addAction(UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil))
        
        present(optionsActionSheet, animated: true, completion: nil)
    }
    
    private func createOnboardHeader(isFull: Bool = false) -> BaseListOnboardView {
        var onboardViewFrame: CGRect
        if #available(iOS 11.0, *) {
            onboardViewFrame = view.safeAreaLayoutGuide.layoutFrame
        } else {
            onboardViewFrame = view.frame
        }
        let onboardView = BaseListOnboardView(frame: onboardViewFrame)
        onboardView.titleLabel.text = addressContextTitle
        onboardView.textLabel.text = addressContextText
        onboardView.titleLabel.textColor = Palette.ppColorGrayscale500.color
        onboardView.textLabel.textColor = Palette.ppColorGrayscale500.color
        onboardView.imageView.image = addressContextImage
        onboardView.buttonView.configure(with: Button(title: addressContextButton, type: .cta))
        onboardView.buttonView.addTarget(self, action: #selector(self.showNewAddressForm), for: .touchUpInside)
        if !isFull {
            onboardView.hideButton(withBottomMargin: 30)
            onboardView.fitToContent(margin: 10)
        }
        return onboardView
    }
    
    @objc
    private func showNewAddressForm() {
        let addAddress = AddNewAddressViewController()
        addAddress.onAddressSaved = { [weak self] (addedAddress) in
            guard let newAddress = addedAddress else {
            return
        }
            self?.viewModel.appendNewAddress(newAddress)
            self?.onNewAddressAdded?(newAddress)
            self?.tableView.reloadData()
            self?.updateTableHeader()
        }
         present(addAddress, animated: true, completion: nil)
    }
    
    // MARK: - Handlers
    @objc
    private func refreshTableView(_ refreshControl: UIRefreshControl) {
        if !viewModel.isLoading {
            tableView.isUserInteractionEnabled = false
            getAddressList(showLoading: false) { [weak self] (error) in
                refreshControl.endRefreshing()
                self?.tableView.isUserInteractionEnabled = true
                self?.updateTableHeader(withError: error)
            }
        } else {
            refreshControl.endRefreshing()
        }
    }
    
    private func removeAddress(at indexPath: IndexPath) {
        guard let address = viewModel.getAddress(at: indexPath.row) else {
            return
        }
        let loader = Loader.getLoadingView(DefaultLocalizable.wait.text) ?? UIView()
        navigationController?.view.addSubview(loader)
        loader.isHidden = false
        viewModel.removeAddress(address) { [weak self] (error) in
            loader.isHidden = true
            loader.removeFromSuperview()
            if error == nil {
                self?.updateTableHeader()
                self?.tableView.reloadData()
            } else {
                AlertMessage.showAlert(error, controller: self)
            }
        }
    }
    
    private func editAddress(at indexPath: IndexPath) {
        guard let address = viewModel.getAddress(at: indexPath.row) else {
            return
        }
        let editAddressVC = EditAddressViewController(selectedAddress: address)
        editAddressVC.onAddressUpdated = { [weak self] (updatedAddress) in
            self?.viewModel.updateAddress(address: updatedAddress, onIndex: indexPath.row)
            self?.tableView.reloadData()
        }
        navigationController?.pushViewController(editAddressVC, animated: true)
    }
}

// MARK: - UITableViewDelegate

extension AddressListViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var tableViewCell: UITableViewCell?
        let row = indexPath.row
        if row < viewModel.getAddressListSize() {
            if let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.addressCard.rawValue) as? AddressCardTableViewCell {
                cell.address = viewModel.getAddress(at: row)
                cell.isChecked = viewModel.isSelected(address: cell.address)
                tableViewCell = cell
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.addItem.rawValue) as? AddNewAddressTableViewCell {
                cell.separatorInset = .zero
                tableViewCell = cell
            }
        }
        tableViewCell?.selectionStyle = .none
        return tableViewCell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        if row >= viewModel.getAddressListSize() {
            showNewAddressForm()
        } else if let selectedAddress = viewModel.getAddress(at: row) {
            if let onAddressSelectedCallback = onAddressSelected {
                onAddressSelectedCallback(selectedAddress)
            } else {
                showOptionsForAddress(at: indexPath)
            }
        }
    }
}

// MARK: - UITableViewDataSource

extension AddressListViewController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return viewModel.getNumberOfRows()
    }
}
