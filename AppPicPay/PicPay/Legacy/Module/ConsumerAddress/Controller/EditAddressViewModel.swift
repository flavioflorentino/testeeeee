import Foundation
import UI

final class EditAddressViewModel {
    private let api: ApiConsumerAddressProtocol
    var addressItem: ConsumerAddressItem
    
    init(api: ApiConsumerAddressProtocol = ApiConsumerAddress(), address: ConsumerAddressItem) {
        self.api = api
        self.addressItem = address
    }
    
    func searchAddress(by zipcode: String, _ completion: @escaping ((ConsumerAddressItem?, Error?) -> Void)) {
        api.getAddress(by: zipcode) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let result):
                    completion(result, nil)
                case .failure(let error):
                    completion(nil, error)
                }
            }
        }
    }
    
    func updateAddress(_ completion: @escaping ((ConsumerAddressItem?, Error?) -> Void)) {
        api.update(address: addressItem) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let result):
                    completion(result, nil)
                case .failure(let error):
                    completion(nil, error)
                }
            }
        }
    }
    
    func applyZipcodeMask(_ textField: UITextField, stringChanged: String, on range: NSRange, newString: String, oldString: String) {
        let maskedString = CustomStringMask(mask: "00000-000").maskedText(from: newString.onlyNumbers) ?? ""
        textField.text = maskedString
        
        let newPosition: Int
        if newString.count < oldString.count {
            //Removeu, move para o inicio da alteração
            newPosition = range.location
        } else {
            let maskExtraChars = maskedString.count - newString.count
            let addedTextLength = stringChanged.count + maskExtraChars
            //Adicionou, move para o final da alteração, considerando o tambem o texto da mascara
            newPosition = range.location + addedTextLength
        }
        
        if let textPosition = textField.position(from: textField.beginningOfDocument, offset: newPosition) {
            //Se a posição é valida para o textField, move o cursor para a posição
            textField.selectedTextRange = textField.textRange(from: textPosition, to: textPosition)
        }
    }
    
    func getErrorMessageCEP(error: Error?) -> String {
        if let serverErrorMessage = error?.localizedDescription, !serverErrorMessage.isEmpty {
            return serverErrorMessage
        } else {
            return ConsumerAddressLocalizable.errorFetchingZipCode.text
        }
    }
    
    func setFieldValue(fieldName: EditAddressFormScrollView.FormFieldName, newValue: String?) {
        switch fieldName {
        case .name:
            addressItem.addressName = newValue ?? DefaultLocalizable.myHouse.text
            
        case .zipcode:
            addressItem.zipCode = newValue?.onlyNumbers
            
        case .street:
            addressItem.streetName = newValue
            
        case .streetNumber:
            addressItem.streetNumber = newValue
            
        case .complement:
            addressItem.addressComplement = newValue
            
        case .neighborhood:
            addressItem.neighborhood = newValue
            
        case .city:
            addressItem.city = newValue
            
        case .state:
            addressItem.state = newValue
        }
    }
}
