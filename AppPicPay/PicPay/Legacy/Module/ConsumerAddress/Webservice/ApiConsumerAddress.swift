//
//  ApiConsumerAddress.swift
//  PicPay
//
//  Created by Vagner Orlandi on 04/04/18.
//

import UIKit
import SwiftyJSON

protocol ApiConsumerAddressProtocol {
    func save(address: ConsumerAddressItem, _ completion: @escaping ((PicPayResult<ConsumerAddressItem>) -> Void))
    func update(address: ConsumerAddressItem, _ completion: @escaping ((PicPayResult<ConsumerAddressItem>) -> Void))
    func remove(addressId: Int, _ completion: @escaping ((PicPayResult<BaseCodableEmptyResponse>) -> Void))
    func getAddressList(_ completion: @escaping ((PicPayResult<[ConsumerAddressItem]>) -> Void) )
    func getAddress(byId: String, _ completion: @escaping ((PicPayResult<ConsumerAddressItem>) -> Void) )
    func getAddress(by zipcode: String, _ completion: @escaping ((PicPayResult<ConsumerAddressItem>) -> Void))
}

final class ApiConsumerAddress: BaseApi, ApiConsumerAddressProtocol {
    internal func getConsumerAddressSaveParameters(address: ConsumerAddressItem) -> [String: Any] {
        return [
            "address_name": address.addressName ?? "",
            "zip_code": address.zipCode?.onlyNumbers ?? "",
            "country": address.country ?? "",
            "state": address.state ?? "",
            "city": address.city ?? "",
            "neighborhood": address.neighborhood ?? "",
            "street_name": address.streetName ?? "",
            "street_number": address.streetNumber ?? "",
            "address_complement": address.addressComplement ?? "",
            "icon_name": address.iconName ?? "home"
        ]
    }
    
    internal func getConsumerAddressUpdateParameters(address: ConsumerAddressItem) -> [String: Any] {
        var params = getConsumerAddressSaveParameters(address: address)
        params["id"] = address.id ?? ""
        return params
    }
    
    func save(address: ConsumerAddressItem, _ completion: @escaping ((PicPayResult<ConsumerAddressItem>) -> Void)) {
        let endpoint: String = WebServiceInterface.apiEndpoint(kWsUrlConsumerAddress)
        let params = getConsumerAddressSaveParameters(address: address)
        requestManager.apiRequest(endpoint: endpoint, method: .post, parameters: params).responseApiCodable(completionHandler: completion)
    }
    
    func update(address: ConsumerAddressItem, _ completion: @escaping ((PicPayResult<ConsumerAddressItem>) -> Void)) {
        var endpoint: String = WebServiceInterface.apiEndpoint(kWsUrlConsumerAddress)
        endpoint.append("/\(address.id ?? -1)")
        let params = getConsumerAddressUpdateParameters(address: address)
        requestManager.apiRequest(endpoint: endpoint, method: .put, parameters: params).responseApiCodable(completionHandler: completion)
    }
    
    func remove(addressId: Int, _ completion: @escaping ((PicPayResult<BaseCodableEmptyResponse>) -> Void)) {
        var endpoint: String = WebServiceInterface.apiEndpoint(kWsUrlConsumerAddress)
        endpoint.append("/\(addressId)")
        requestManager.apiRequest(endpoint: endpoint, method: .delete).responseApiCodable(completionHandler: completion)
    }
    
    func getAddressList(_ completion: @escaping ((PicPayResult<[ConsumerAddressItem]>) -> Void) ) {
        let endpoint: String = WebServiceInterface.apiEndpoint(kWsUrlConsumerAddressList)
        requestManager.apiRequest(endpoint: endpoint, method: .get).responseApiCodable(completionHandler: completion)
    }
    
    func getAddress(byId: String, _ completion: @escaping ((PicPayResult<ConsumerAddressItem>) -> Void) ) {
        var endpoint: String = WebServiceInterface.apiEndpoint(kWsUrlConsumerAddress)
        endpoint.append("/\(byId)")
        requestManager.apiRequest(endpoint: endpoint, method: .get).responseApiCodable(completionHandler: completion)
    }

    func getAddress(by zipcode: String, _ completion: @escaping ((PicPayResult<ConsumerAddressItem>) -> Void)) {
        let endpoint: String = WebServiceInterface.apiEndpoint(kWsUrlSearchAddressByZipcode).replacingOccurrences(of: "{zipcode}", with: zipcode)
        requestManager.apiRequest(endpoint: endpoint, method: .get).responseApiCodable(completionHandler: completion)
    }
}
