//
//  AddNewAddressAccessoryView.swift
//  PicPay
//
//  Created by PicPay Eduardo on 12/19/18.
//

import UI
import UIKit

final class AddNewAddressAccessoryView: UIView {
    lazy var nextButton: UIButton = {
        let _button = UIButton()
        _button.translatesAutoresizingMaskIntoConstraints = false
        _button.setTitle(DefaultLocalizable.next.text, for: .normal)
        _button.setTitleColor(Palette.ppColorGrayscale000.color, for: .normal)
        _button.backgroundColor = Palette.ppColorBranding300.color
        _button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold)
        return _button
    }()
    
    lazy var previousButton: UIButton = {
        let _button = UIButton()
        _button.translatesAutoresizingMaskIntoConstraints = false
        _button.setTitle(DefaultLocalizable.btPrevious.text, for: .normal)
        _button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        _button.backgroundColor = .clear
        _button.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        return _button
    }()
    
    var isNextButtonEnabled: Bool = true {
        didSet {
            if isNextButtonEnabled == oldValue {
                return
            }
            nextButton.alpha = isNextButtonEnabled ? 1 : 0.5
            nextButton.isEnabled = isNextButtonEnabled
        }
    }
    
    var isPreviousButtonEnabled: Bool = true {
        didSet {
            if isPreviousButtonEnabled == oldValue {
                return
            }
            previousButton.alpha = isPreviousButtonEnabled ? 1 : 0.5
            previousButton.isEnabled = isPreviousButtonEnabled
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(previousButton)
        addSubview(nextButton)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupSubviewsContraints() {
        let buttonSize: CGSize = CGSize(width: 112, height: 40)
        let padding: CGFloat = 5
        frame = CGRect(x: 0, y: 0, width: 0, height: buttonSize.height + (2 * padding))
        
        NSLayoutConstraint.activate([
            previousButton.leftAnchor.constraint(equalTo: leftAnchor, constant: padding),
            previousButton.topAnchor.constraint(equalTo: topAnchor, constant: padding),
            previousButton.widthAnchor.constraint(equalToConstant: buttonSize.width),
            previousButton.heightAnchor.constraint(equalToConstant: buttonSize.height)
            ])
        
        NSLayoutConstraint.activate([
            nextButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -padding),
            nextButton.topAnchor.constraint(equalTo: topAnchor, constant: padding),
            nextButton.widthAnchor.constraint(equalToConstant: buttonSize.width),
            nextButton.heightAnchor.constraint(equalToConstant: buttonSize.height)
            ])
        
        nextButton.layer.masksToBounds = true
        nextButton.layer.cornerRadius = buttonSize.height / 2
        previousButton.alpha = 0
    }
}
