import UI
import UIKit

final class AddressCardTableViewCell: UITableViewCell {
    
    //MARK: Subviews
    lazy private var cardView: HorizontalCardView = {
        let _view = HorizontalCardView()
        _view.translatesAutoresizingMaskIntoConstraints = false
        
        return _view
    }()
    
    lazy private var checkImage: UIImageView = {
        let _imageView = UIImageView(image: #imageLiteral(resourceName: "item_selected"))
        _imageView.translatesAutoresizingMaskIntoConstraints = false
        _imageView.contentMode = .scaleAspectFit
        _imageView.alpha = 0
        return _imageView
    }()
    
    //MARK: UIView
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    private func initialize() {
        buildViews()
    }
    
    //MARK: Properties
    public var address: ConsumerAddressItem? = nil {
        didSet {
            cardView.setModel(address)
        }
    }
    
    public var isChecked: Bool = false {
        didSet {
            isChecked ? createImageCheck() : removeImageCheck()
            NSLayoutConstraint.activate(cardConstraint)
            UIView.animate(withDuration: 0.25, animations: {
                self.checkImage.alpha = self.isChecked ? 1 : 0
            })
        }
    }
    
    public var cardConstraint: [NSLayoutConstraint] {
        
        let traling_card = cardView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: isChecked ? -72 : -15)
        let leading_card = cardView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15)
        let bottom_card = cardView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15)
        let top_card = cardView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15)
        
        return [top_card, bottom_card, leading_card, traling_card]
    }
    
    //MARK: Public methods
    
    private func createImageCheck() {
        
        let checkmarkSize: CGFloat = 16
        
        contentView.addSubview(checkImage)
        
        NSLayoutConstraint.activate([
            checkImage.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24),
            checkImage.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            checkImage.widthAnchor.constraint(equalToConstant: checkmarkSize),
            checkImage.heightAnchor.constraint(equalToConstant: checkmarkSize),
            ])
    }
    
    private func removeImageCheck() {
        
        checkImage.removeFromSuperview()
    }
    
    //MARK: Private methods
    private func buildViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
        contentView.addSubview(cardView)
        NSLayoutConstraint.activate(cardConstraint)
    }
    
}
