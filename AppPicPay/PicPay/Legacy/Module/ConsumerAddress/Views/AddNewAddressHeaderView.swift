import UI
import UIKit

final class AddNewAddressHeaderView: UIView {
    lazy var titleLabel: UILabel = {
        let _label = UILabel()
        _label.translatesAutoresizingMaskIntoConstraints = false
        _label.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold)
        _label.textColor = Palette.ppColorGrayscale000.color
        _label.text = ConsumerAddressLocalizable.newAddress.text
        return _label
    }()
    
    lazy var closeButton: UIButton = {
        let _button = UIButton()
        _button.translatesAutoresizingMaskIntoConstraints = false
        _button.setImage(#imageLiteral(resourceName: "iconClose"), for: .normal)
        return _button
    }()
    
    lazy var addressCardView: AddressCardView = {
        let _view = AddressCardView()
        _view.translatesAutoresizingMaskIntoConstraints = false
        _view.backgroundColor = Palette.ppColorGrayscale000.color
        _view.layer.masksToBounds = true
        _view.layer.cornerRadius = 5
        return _view
    }()
    
    var headerTitle: String? {
        get {
            return titleLabel.text
        }
        set(newTitle) {
            titleLabel.text = newTitle
        }
    }
    
    var addressCardHeightConstraint: NSLayoutConstraint?
    var addressCardCenteYConstraint: NSLayoutConstraint?
    var headerHeightConstraint: NSLayoutConstraint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(closeButton)
        addSubview(titleLabel)
        addSubview(addressCardView)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupSubviewsContraints() {
        setupTitleLabelConstraints()
        setupCardViewConstraints()
        setupCloseButtonConstraints()
    }
    
    func setupTitleLabelConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.centerYAnchor.constraint(equalTo: closeButton.centerYAnchor),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
            ])
    }
    
    func setupCardViewConstraints() {
        let cardHeightConstraint = addressCardView.heightAnchor.constraint(equalToConstant: 85)
        let cardCenterYConstraint = addressCardView.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor, constant: 85)
        addressCardHeightConstraint = cardHeightConstraint
        addressCardCenteYConstraint = cardCenterYConstraint
        NSLayoutConstraint.activate([
            cardCenterYConstraint,
            cardHeightConstraint,
            addressCardView.leftAnchor.constraint(equalTo: leftAnchor, constant: 32),
            addressCardView.rightAnchor.constraint(equalTo: rightAnchor, constant: -32)
            ])
    }
    
    private func setupCloseButtonConstraints() {
        let closeButtonImageSize: CGFloat = 20
        let closeButtonInset: CGFloat = 5
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let statusBarHeight: CGFloat = CGFloat(20)
            let marginTop: CGFloat = CGFloat(10)
            let topPadding: CGFloat = window?.safeAreaInsets.top ?? 0.0
            
            var topAnchorCloseButton: CGFloat = 0.0
            if topPadding != 0 {
                topAnchorCloseButton = (topPadding) + marginTop
            } else {
                topAnchorCloseButton = statusBarHeight + marginTop
            }
            
            NSLayoutConstraint.activate([
                closeButton.topAnchor.constraint(equalTo: topAnchor, constant: topAnchorCloseButton),
                closeButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 15 - closeButtonInset),
                closeButton.widthAnchor.constraint(equalToConstant: closeButtonImageSize + (2 * closeButtonInset)),
                closeButton.heightAnchor.constraint(equalToConstant: closeButtonImageSize + (2 * closeButtonInset))
                ])
        } else {
            NSLayoutConstraint.activate([
                closeButton.topAnchor.constraint(equalTo: topAnchor, constant: 30 - closeButtonInset),
                closeButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 15 - closeButtonInset),
                closeButton.widthAnchor.constraint(equalToConstant: closeButtonImageSize + (2 * closeButtonInset)),
                closeButton.heightAnchor.constraint(equalToConstant: closeButtonImageSize + (2 * closeButtonInset))
                ])
        }
        
        closeButton.contentEdgeInsets = UIEdgeInsets(top: closeButtonInset, left: closeButtonInset, bottom: closeButtonInset, right: closeButtonInset)
    }
}
