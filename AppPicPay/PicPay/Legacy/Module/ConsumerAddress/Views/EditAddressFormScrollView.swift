import UI
import UIKit
import SkyFloatingLabelTextField

final class EditAddressFormScrollView: UIScrollView {

    enum FormFieldName:Int {
        case name
        case zipcode
        case street
        case streetNumber
        case complement
        case neighborhood
        case city
        case state
        
        static let allValues = [name, zipcode, street, streetNumber, complement, neighborhood, city, state]
    }
    
    //MARK: Properties
    
    private var formTextFields:[FormFieldName : UITextField] = [:]
    
    private var shouldLoadConstraints:Bool = true
    
    private weak var textFieldDelegate:UITextFieldDelegate? = nil
    
    lazy private var zipcodeLoadingView: UIActivityIndicatorView = {
        let _view = UIActivityIndicatorView(style: .gray)
        _view.translatesAutoresizingMaskIntoConstraints = false
        _view.hidesWhenStopped = true
        
        return _view
    }()
    
    public var isLoadingZipcode:Bool = false {
        didSet {
            if isLoadingZipcode {
                self.zipcodeLoadingView.startAnimating()
                [formTextFields[.street], formTextFields[.complement]].forEach({ (textField) in
                    textField?.isEnabled = false
                    textField?.alpha = 0.5
                })
            } else {
                self.zipcodeLoadingView.stopAnimating()
                [formTextFields[.street], formTextFields[.complement]].forEach({ (textField) in
                    textField?.isEnabled = true
                    textField?.alpha = 1
                })
            }
        }
    }
    
    //MARK: UIView
    
    init(frame:CGRect = .zero, textFieldDelegate:UITextFieldDelegate) {
        super.init(frame: frame)
        self.textFieldDelegate = textFieldDelegate
        self.createTextFields()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.createTextFields()
    }

    override func updateConstraints() {
        if self.shouldLoadConstraints {
            self.loadConstraints()
            self.shouldLoadConstraints = false
        }
        super.updateConstraints()
    }
    
    //MARK:Private Methods
    
    private func createTextFields() {
        FormFieldName.allValues.forEach({ (fieldName) in
            let textField = EditAddressFormScrollView.textFieldFactory(fieldName)
            textField.delegate = self.textFieldDelegate
            textField.translatesAutoresizingMaskIntoConstraints = false
            if fieldName == .zipcode {
                textField.rightView = self.zipcodeLoadingView
                textField.rightViewMode = .always
            }
            self.formTextFields[fieldName] = textField
            self.addSubview(textField)
        })
    }
    
    private func loadConstraints() {
        let textFieldHeight:CGFloat = 50
        let verticalSpacing:CGFloat = 8
        let horizontalSpacing:CGFloat = 10
        let smallTextFieldWidth:CGFloat = 80
        
        guard let nameField = self.formTextFields[FormFieldName.name],
            let zipcodeField = self.formTextFields[FormFieldName.zipcode],
            let streetField = self.formTextFields[FormFieldName.street],
            let streetNumbeField = self.formTextFields[FormFieldName.streetNumber],
            let complementField = self.formTextFields[FormFieldName.complement],
            let neighborhoodField = self.formTextFields[FormFieldName.neighborhood],
            let cityField = self.formTextFields[FormFieldName.city],
            let stateField = self.formTextFields[FormFieldName.state]
            else {
                return
        }
        
        NSLayoutConstraint.activate([
            nameField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            nameField.topAnchor.constraint(equalTo: self.topAnchor),
            nameField.leftAnchor.constraint(equalTo: self.leftAnchor),
            nameField.widthAnchor.constraint(equalTo: self.widthAnchor)
            ])
        self.contentSize.height = textFieldHeight
        
        NSLayoutConstraint.activate([
            zipcodeField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            zipcodeField.leftAnchor.constraint(equalTo: self.leftAnchor),
            zipcodeField.rightAnchor.constraint(equalTo: nameField.rightAnchor),
            zipcodeField.topAnchor.constraint(equalTo: nameField.bottomAnchor, constant: verticalSpacing)
            ])
        self.contentSize.height += (textFieldHeight + verticalSpacing)
        
        NSLayoutConstraint.activate([
            streetField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            streetField.leftAnchor.constraint(equalTo: self.leftAnchor),
            streetField.topAnchor.constraint(equalTo: zipcodeField.bottomAnchor, constant: verticalSpacing)
            ])
        
        NSLayoutConstraint.activate([
            streetNumbeField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            streetNumbeField.widthAnchor.constraint(equalToConstant: smallTextFieldWidth),
            streetNumbeField.leftAnchor.constraint(equalTo: streetField.rightAnchor, constant: horizontalSpacing),
            streetNumbeField.rightAnchor.constraint(equalTo: nameField.rightAnchor),
            streetNumbeField.topAnchor.constraint(equalTo: zipcodeField.bottomAnchor, constant: verticalSpacing)
            ])
        self.contentSize.height += (textFieldHeight + verticalSpacing)
        
        NSLayoutConstraint.activate([
            complementField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            complementField.leftAnchor.constraint(equalTo: self.leftAnchor),
            complementField.rightAnchor.constraint(equalTo: nameField.rightAnchor),
            complementField.topAnchor.constraint(equalTo: streetField.bottomAnchor, constant: verticalSpacing)
            ])
        self.contentSize.height += (textFieldHeight + verticalSpacing)
        
        NSLayoutConstraint.activate([
            neighborhoodField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            neighborhoodField.leftAnchor.constraint(equalTo: self.leftAnchor),
            neighborhoodField.rightAnchor.constraint(equalTo: nameField.rightAnchor),
            neighborhoodField.topAnchor.constraint(equalTo: complementField.bottomAnchor, constant: verticalSpacing)
            ])
        self.contentSize.height += (textFieldHeight + verticalSpacing)
        
        NSLayoutConstraint.activate([
            cityField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            cityField.leftAnchor.constraint(equalTo: self.leftAnchor),
            cityField.topAnchor.constraint(equalTo: neighborhoodField.bottomAnchor, constant: verticalSpacing)
            ])
        
        NSLayoutConstraint.activate([
            stateField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            stateField.widthAnchor.constraint(equalToConstant: smallTextFieldWidth),
            stateField.leftAnchor.constraint(equalTo: cityField.rightAnchor, constant: horizontalSpacing),
            stateField.rightAnchor.constraint(equalTo: nameField.rightAnchor),
            stateField.topAnchor.constraint(equalTo: neighborhoodField.bottomAnchor, constant: verticalSpacing)
            ])
        self.contentSize.height += (textFieldHeight + verticalSpacing)
    }
    
    private static func textFieldFactory(_ fieldName: FormFieldName) -> UITextField {
        let unselectedColor = Palette.ppColorGrayscale400.color
        let selectedColor = Palette.ppColorBranding300.color
        let textColor = Palette.ppColorGrayscale500.color
        let textField = SkyFloatingLabelTextField()
        textField.font = UIFont.systemFont(ofSize: 16)
        
        textField.lineColor = unselectedColor
        textField.titleColor = unselectedColor
        textField.placeholderColor = unselectedColor
        textField.textColor = textColor
        
        textField.selectedLineColor = selectedColor
        textField.selectedTitleColor = selectedColor
        
        textField.titleFormatter = {$0}
        textField.titleLabel.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.medium)
        
        textField.disabledColor = .lightGray
        textField.autocorrectionType = .no
        
        switch fieldName {
        case .name:
            textField.placeholder = ConsumerAddressLocalizable.nicknameAddress.text
            textField.returnKeyType = .next

        case .zipcode:
            textField.placeholder = ConsumerAddressLocalizable.zipCode.text
            textField.keyboardType = .numberPad
            textField.returnKeyType = .next

        case .street:
            textField.placeholder = ConsumerAddressLocalizable.streetAvenue.text
            textField.returnKeyType = .next

        case .streetNumber:
            textField.placeholder = DefaultLocalizable.number.text
            textField.keyboardType = .numbersAndPunctuation
            textField.returnKeyType = .next

        case .complement:
            textField.placeholder = ConsumerAddressLocalizable.complement.text
            textField.returnKeyType = .done

        case .neighborhood:
            textField.placeholder = ConsumerAddressLocalizable.neighborhood.text
            textField.isEnabled = false
            
        case .city:
            textField.placeholder = DefaultLocalizable.city.text
            textField.isEnabled = false
            
        case .state:
            textField.placeholder = DefaultLocalizable.state.text
            textField.isEnabled = false
        }
        
        return textField
    }
    
    
    
    public func setTextFieldValues(_ consumerAddress: ConsumerAddressItem?, onlyAddress: Bool = false) {
        for (fieldName, textField) in self.formTextFields {
            switch fieldName {
            case .name:
                if !onlyAddress {
                    textField.text = consumerAddress?.addressName
                    (textField as? SkyFloatingLabelTextField)?.errorMessage = nil
                }

            case .zipcode:
                if !onlyAddress {
                    textField.text = CustomStringMask(mask: "00000-000").maskedText(from: consumerAddress?.zipCode?.onlyNumbers)
                }
                (textField as? SkyFloatingLabelTextField)?.errorMessage = nil
                
            case .street:
                textField.text = consumerAddress?.streetName
                (textField as? SkyFloatingLabelTextField)?.errorMessage = nil
                
            case .streetNumber:
                if !onlyAddress {
                    textField.text = consumerAddress?.streetNumber
                }
                
            case .complement:
                textField.text = consumerAddress?.addressComplement
                
            case .neighborhood:
                textField.text = consumerAddress?.neighborhood
                
            case .city:
                textField.text = consumerAddress?.city
                
            case .state:
                textField.text = consumerAddress?.state
                
            }
        }
    }
    
    public func getTextFieldValues() -> [FormFieldName: String?] {
        var formValues:[FormFieldName: String?] = [:]
        
        for (fieldName, textField) in self.formTextFields {
            formValues[fieldName] = textField.text
        }
        
        return formValues
    }
    
    public func getTextFieldValue(_ fieldName:FormFieldName) -> String? {
        if let textField = self.formTextFields.first(where: {$0.key == fieldName})?.value {
            return textField.text
        } else {
            return nil
        }
    }
    
    public func getFormFieldName(_ textField:UITextField) -> FormFieldName? {
        for (fieldName, _textField) in self.formTextFields {
            if _textField === textField {
                return fieldName
            }
        }
        return nil
    }
    
    public func nextError() -> UITextField? {
        for textField in self.formTextFields.values {
            if let errorMessage = (textField as? SkyFloatingLabelTextField)?.errorMessage, !errorMessage.isEmpty {
                return textField
            }
        }
        return nil
    }
    
    public func nextField(current textField:UITextField) {
        var currentFieldDidEndEditing:Bool = false
        
        //Usando FormFieldName para seguir a ordem definida no Array
        for fieldName in FormFieldName.allValues {
            if currentFieldDidEndEditing {
                self.formTextFields[fieldName]?.becomeFirstResponder()
                break
            }
            if textField === self.formTextFields[fieldName] {
                if textField.endEditing(false) {
                    currentFieldDidEndEditing = true
                } else {
                    break
                }
            }
        }
    }

}
