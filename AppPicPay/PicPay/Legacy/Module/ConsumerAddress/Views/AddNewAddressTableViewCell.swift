import UI
import UIKit

final class AddNewAddressTableViewCell: AddNewItemTableViewCell {
    
    override func initialize() {
        super.initialize()
        
        title = ConsumerAddressLocalizable.addNewAddress.text
        backgroundColor = Palette.ppColorGrayscale000.color
    }
}
