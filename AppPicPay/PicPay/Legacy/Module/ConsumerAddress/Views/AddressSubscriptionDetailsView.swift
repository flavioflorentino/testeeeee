import UIKit

final class AddressSubscriptionDetailsView: UIView, SubscriptionDetailsWidget {
    lazy var details: SubscriptionDetailsView<ConsumerAddressItem> = {
        let view = SubscriptionDetailsView<ConsumerAddressItem>()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private var shouldLoadConstraints: Bool = true
    public var onChangeButtonTap: ((Any?, ConsumerAddressItem?) -> Void)? {
        get {
            return details.onChangeButtonTap
        }
        set {
            details.onChangeButtonTap = newValue
        }
    }
    
    //MARK: UIView
    override func updateConstraints() {
        if shouldLoadConstraints {
            loadConstraints()
            shouldLoadConstraints = false
        }
        super.updateConstraints()
    }
    
    //MARK: Private methods
    private func loadConstraints() {
        addSubview(details)
        NSLayoutConstraint.activate([
            details.topAnchor.constraint(equalTo: self.topAnchor),
            details.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            details.leftAnchor.constraint(equalTo: self.leftAnchor),
            details.rightAnchor.constraint(equalTo: self.rightAnchor)
        ])
    }
    
    func setAddress(_ address:ConsumerAddressItem?) {
        guard let item = address else {
            return
        }
        details.setModel(item)
    }

    func viewMustUpdate() {
        details.viewMustUpdate()
    }
}

extension ConsumerAddressItem:SubscriptionDetails {
    func getSubscriptionDetailInfo() -> String {
        return DefaultLocalizable.deliveryAddress.text
    }
    
    func getSubscriptionDetailTitle() -> String? {
        return addressName
    }
    
    func getSubscriptionDetailSubtitle() -> String {
        var subtitle:String = ""
        let appendIfNotEmptyToString: ((inout String, String?, String) -> Void) = { (source, addString, component) in
            if let str = addString, !str.isEmpty {
                if !source.isEmpty {
                    source.append(component)
                }
                source.append(str)
            }
        }
        
        appendIfNotEmptyToString(&subtitle, streetName, "")
        appendIfNotEmptyToString(&subtitle, streetNumber, ", ")
        appendIfNotEmptyToString(&subtitle, addressComplement, ", ")
        appendIfNotEmptyToString(&subtitle, city, ", ")
        appendIfNotEmptyToString(&subtitle, state, " - ")
        return subtitle
    }
}

