//
//  EditAddressImagesScrollView.swift
//  PicPay
//
//  Created by Vagner Orlandi on 16/04/18.
//

import UI
import UIKit

final class EditAddressImagesScrollView: UIScrollView {
    enum AddressImage:String {
        case home     = "home"
        case building = "building"
        case vacation = "vacation"
        case mountain = "mountain"
        case unknown  = ""
        
        static let allValues = [home, building, vacation, mountain]
    }
    
    //MARK:Properties
    
    private var addressButtonImages: [AddressImage : UIButton] = [:]
    
    private var shouldLoadConstraints: Bool = true
    
    private var buttonSize: CGFloat = 54
    
    private var unselectedButtonBorderColor = Palette.ppColorGrayscale300.cgColor
    
    private var selectedButtonBorderColor = Palette.ppColorBranding300.cgColor
    
    private lazy var selectedAnimation:CABasicAnimation = {
        let animation = CABasicAnimation(keyPath: "borderColor")
        animation.fromValue = self.unselectedButtonBorderColor
        animation.toValue = self.selectedButtonBorderColor
        animation.repeatCount = 1
        animation.isRemovedOnCompletion = true
        animation.duration = 0.25
        
        return animation
    }()
    
    private lazy var unselectedAnimation:CABasicAnimation = {
        let animation = CABasicAnimation(keyPath: "borderColor")
        animation.fromValue = self.selectedButtonBorderColor
        animation.toValue = self.unselectedButtonBorderColor
        animation.repeatCount = 0
        animation.isRemovedOnCompletion = true
        animation.duration = 0.25

        return animation
    }()
    
    private var currentImageButton:UIButton? = nil {
        didSet(oldImage) {
            oldImage?.isSelected = false
            self.currentImageButton?.isSelected = false
            
            oldImage?.layer.add(self.unselectedAnimation, forKey: "borderColor")
            oldImage?.layer.borderColor = self.unselectedButtonBorderColor
            self.currentImageButton?.layer.add(self.selectedAnimation, forKey: "borderColor")
            self.currentImageButton?.layer.borderColor = self.selectedButtonBorderColor
        }
    }
    
    //MARK: Public properties
    
    public var onAddressImageSelected:((AddressImage) -> Void)? = nil
    
    //MARK: UIView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.createButtons()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.createButtons()
    }
    
    override func updateConstraints() {
        if self.shouldLoadConstraints {
            self.loadConstraints()
            self.shouldLoadConstraints = false
        }
        super.updateConstraints()
    }
    
    //MARK: Private Methods
    
    private func loadConstraints() {
        let buttonSpacing:CGFloat = 8
        var lastButton:UIButton? = nil
        
        //Usando a ordem do array para montar na sequencia
        for addressImageName in AddressImage.allValues {
            if let button = self.addressButtonImages[addressImageName] {
                self.addSubview(button)
                
                NSLayoutConstraint.activate([
                    button.heightAnchor.constraint(equalToConstant: self.buttonSize),
                    button.widthAnchor.constraint(equalToConstant: self.buttonSize),
                    button.centerYAnchor.constraint(equalTo: self.centerYAnchor)
                    ])
                
                if lastButton != nil {
                    button.leftAnchor.constraint(equalTo: lastButton!.rightAnchor, constant: buttonSpacing).isActive = true
                    self.contentSize.width += (self.buttonSize + buttonSpacing)
                } else {
                    button.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
                    self.contentSize.width = self.buttonSize
                }
                
                lastButton = button
            }
        }
    }
    
    private func createButtons() {
        for addressImageName in AddressImage.allValues {
            if let image = UIImage(named: addressImageName.rawValue) {
                let button = UIButton()
                button.translatesAutoresizingMaskIntoConstraints = false
                button.layer.masksToBounds = true
                button.layer.cornerRadius = self.buttonSize / 2.0
                button.layer.borderWidth = 1
                button.backgroundColor = Palette.ppColorGrayscale000.color(withCustomDark: .ppColorGrayscale500)
                button.layer.borderColor = UIColor(white: 224.0 / 255.0, alpha: 1).cgColor
                button.setImage(image, for: .normal)
                button.contentEdgeInsets = UIEdgeInsets(top: 9, left: 9, bottom: 9, right: 9)
                button.addTarget(self, action: #selector(buttonActionHandler), for: .touchUpInside)
                
                self.addressButtonImages[addressImageName] = button
            }
        }
        
        if let imageName = AddressImage.allValues.first {
            self.setActive(imageName)
        }
    }
    
    //MARK: Public Methods
    
    public func setActive(_ addressImage:AddressImage) {
        if let button = self.addressButtonImages[addressImage] {
            self.currentImageButton = button
        }
    }
    
    public func getActiveOption() -> AddressImage {
        if let addressImageName = self.addressButtonImages.first(where: {$0.value == self.currentImageButton})?.key {
            return addressImageName
        } else {
            return AddressImage.unknown
        }
    }
    
    //MARK: Handlers
    
    @objc func buttonActionHandler(sender: Any?) {
        if let addressImageName = self.addressButtonImages.first(where: {$0.value == (sender as? UIButton)})?.key {
            HapticFeedback.selectionFeedback()
            self.setActive(addressImageName)
            self.onAddressImageSelected?(addressImageName)
        }
    }
}
