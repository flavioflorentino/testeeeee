//
//  AddressCardView.swift
//  PicPay
//
//  Created by Vagner Orlandi on 23/03/18.
//
import UIKit

final class AddressCardView: UIView {
    
    //MARK: Subviews
    lazy private var cardView: HorizontalCardView = {
        let _view = HorizontalCardView()
        _view.translatesAutoresizingMaskIntoConstraints = false
        
        return _view
    }()
    
    //MARK: Properties
    private var shouldLoadConstraints: Bool = true
    
    //MARK: Private methods
    private func loadConstraints() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(self.cardView)
        
        NSLayoutConstraint.activate([
            self.cardView.topAnchor.constraint(equalTo: self.topAnchor, constant: 16),
            self.cardView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16),
            self.cardView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 12),
            self.cardView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -12)
            ])
    }
    
    //MARK: Public methods
    func setAddress(_ address: ConsumerAddressItem?) {
        guard let item = address else {
            return
        }
        self.cardView.setModel(item)
    }
    
    func viewMustUpdate(animated: Bool) {
        self.cardView.viewMustUpdate(animated: animated)
    }
    
    //MARK: UIView
    override func updateConstraints() {
        if shouldLoadConstraints {
            self.loadConstraints()
            self.shouldLoadConstraints = false
        }
        super.updateConstraints()
    }
}

extension ConsumerAddressItem: HorizontalCardViewPresentable {
    func getTitle() -> String {
        return self.addressName ?? ""
    }
    
    func getSubtitle() -> String {
        let array: [(String?, Separator)] = [
            (streetName, .comma),
            (streetNumber, .comma),
            (addressComplement, .hyphen),
            (city, .comma),
            (state, .hyphen),
            (zipCode, .hyphen)
        ]
        
        return descriptionAddress(array: array)
    }
    
    func getImage() -> UIImage? {
        if let iconName = self.iconName, !iconName.isEmpty {
            return UIImage.init(named: iconName)
        } else {
            return nil
        }
    }
    
    func getPlaceholderImage() -> UIImage {
        return #imageLiteral(resourceName: "home")
    }
    
    private func descriptionAddress(array: [(description: String?, separator: Separator)]) -> String {
        var address = ""
        
        for info in array {
            if let description = info.description ,!description.isEmpty {
                address += description + info.separator.value
            }
        }
        
        return address
            .trimmingCharacters(in: .whitespacesAndNewlines)
            .trimmingCharacters(in: CharacterSet(charactersIn: Separator.comma.value))
            .trimmingCharacters(in: CharacterSet(charactersIn: Separator.hyphen.value))
    }
    
    private enum Separator: String {
        case comma = ","
        case hyphen = "-"
        
        var value: String {
            switch self {
            case .comma:
                return ", "
            case .hyphen:
                return " - "
            }
        }
    }
}
