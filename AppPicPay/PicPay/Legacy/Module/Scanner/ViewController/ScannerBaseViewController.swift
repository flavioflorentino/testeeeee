import Core
import FeatureFlag
import UI
import UIKit
import PIX
import Billet

protocol ScannerBaseViewControllerProtocol {
    func minimizeMyCodeView()
    func maximizeMyCodeView()
}

private extension ScannerBaseViewController.Layout {
    static let arrowButtonSize = CGSize(width: 32, height: 32)
    static let minOffsetYAxisMyCodeView: CGFloat = UIApplication.shared.statusBarFrame.height + 72
    static let maxOffsetYAxisMyCodeView: CGFloat = {
        let isSmallScreen = UIScreen.main.bounds.height < 570
        return UIScreen.main.bounds.height - (isSmallScreen ? 124 : 138)
    }()
}

final class ScannerBaseViewController: PPBaseViewController, QrCodeScanning, ViewConfiguration {
    fileprivate enum Layout {}
    // Remove final class from breadcrumb
    override var breadcrumbDescription: String {
        return ""
    }

    // MARK: Public vars

    var scanner: PPScanner?
    var isSetToOpenBoleto = false

    private let scannerType: ScannerType

    // MARK: Private vars

    private lazy var scannerView: UIView = {
        if FeatureManager.shared.isActive(.experimentNewAccessQrCode) {
            let scannerView = NewScannerView(scannerType: scannerType)
            scannerView.delegate = self
            return scannerView
        }
        
        if scannerType == .default {
            let scannerView = ScannerView()
            scannerView.delegate = self
            return scannerView
        } else {
            let scannerView = PixScannerView()
            scannerView.infoDelegate = self
            return scannerView
        }
    }()

    private var arrowButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        button.addTarget(self, action: #selector(didTapArrow), for: .touchUpInside)
        return button
    }()
    
    private lazy var informationLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(IconLabelStyle(type: .large))
            .with(\.textColor, .white(.light))
        label.text = Iconography.infoCircle.rawValue
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapInformationLabel)))
        return label
    }()

    private lazy var requestCameraView: AskForCameraPermissionView = {
        let view = AskForCameraPermissionView()
        view.view.backgroundColor = Palette.black.color.withAlphaComponent(0.8)
        view.descriptionLabel.textColor = Palette.white.color

        view.action = { [weak self] _ in
            self?.viewModel.cameraPermissionAnalytics()
            self?.viewModel.requestCameraPermission()
        }
        return view
    }()

    private lazy var myCodeBottomSheet: MyCodeViewController = {
        let bottomSheetController = MyCodeFactory.make()
        bottomSheetController.setupSheetPresentation(minOffsetY: Layout.minOffsetYAxisMyCodeView, maxOffsetY: Layout.maxOffsetYAxisMyCodeView)
        bottomSheetController.delegate = self
        return bottomSheetController
    }()

    private let viewModel = ScannerBaseViewModel()
    private var isScannerVisible = false
    private var isBottomSheetMaximized = false {
        didSet {
            scanner?.preventNewReads = isBottomSheetMaximized
        }
    }
    
    private let backButtonSide: BackButtonSide
    private let hasMyCodeBottomSheet: Bool

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
        registerObserver()
        viewModel.delegate = self
        presentationController?.delegate = self
        if scannerType == .pix {
            viewModel.callPixScreenAnalytics()
        }
    }
    
    init(backButtonSide: BackButtonSide = .right,
         hasMyCodeBottomSheet: Bool = true,
         scannerType: ScannerType = .default) {
        self.backButtonSide = backButtonSide
        self.hasMyCodeBottomSheet = hasMyCodeBottomSheet
        self.scannerType = scannerType
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
        navigationController?.setNavigationBarHidden(true, animated: animated)
        guard hasMyCodeBottomSheet == false else {
            return
        }
        isScannerVisible = true
        tryStartScanner()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopScanner()
    }

    func buildViewHierarchy() {
        view.addSubviews(scannerView,
                         requestCameraView,
                         arrowButton,
                         informationLabel)

        handleMyCodeBottomSheet(hasMyCodeBottomSheet: hasMyCodeBottomSheet)
    }
    
    private func stopScanner() {
        scanner?.stop()
        isSetToOpenBoleto = false
        isScannerVisible = false
    }

    func setupConstraints() {
        scannerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        requestCameraView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        informationLabel.snp.makeConstraints {
            $0.centerY.equalTo(arrowButton)
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
    }

    func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        scanner?.preventNewReads = false

        let showRequestCameraPermission = viewModel.cameraPermission.status != .authorized
        updateViews(showRequestCameraPermission: showRequestCameraPermission)
        
        informationLabel.isHidden = !FeatureManager.shared.isActive(.experimentNewAccessQrCode) || scannerType == .pix
        
        handleArrowButtonPosition(side: backButtonSide)
    }
    
    private func handleMyCodeBottomSheet(hasMyCodeBottomSheet: Bool) {
        if FeatureManager.shared.isActive(.experimentNewAccessQrCode) {
            return
        }
        
        guard hasMyCodeBottomSheet else {
            return
        }
        addChild(myCodeBottomSheet)
        view.addSubview(myCodeBottomSheet.view)
        myCodeBottomSheet.didMove(toParent: self)
    }
    
    private func handleArrowButtonPosition(side: BackButtonSide) {
        arrowButton.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).inset(Spacing.base01)
            $0.size.equalTo(Layout.arrowButtonSize)
        }
        if side == .right {
            arrowButton.transform = CGAffineTransform(rotationAngle: .pi)
            arrowButton.snp.makeConstraints {
                $0.trailing.equalToSuperview().inset(Spacing.base01)
            }
        } else {
            arrowButton.snp.makeConstraints {
                $0.leading.equalToSuperview().inset(Spacing.base01)
            }
        }
    }

    private func updateViews(showRequestCameraPermission: Bool) {
        scannerView.isHidden = showRequestCameraPermission
        requestCameraView.isHidden = !showRequestCameraPermission
    }

    // MARK: Scanner functions
    private func startScanner() {
        guard scanner == nil  else {
            return
        }
        // Scanner object
        scanner = PPScanner(origin: .main, view: view, onRead: viewModel.didReadNewCode, onFail: { [weak self] in
            self?.updateViews(showRequestCameraPermission: true)
        })
    }

    private func openBoletoScanner() {
        isSetToOpenBoleto = true
        guard FeatureManager.isActive(.isNewBilletHubAvailable) else {
            DGHelpers.openBoletoFlow(viewController: self, origin: "scanner", startWithScanner: true)
            return
        }
        
        let hubViewController = BilletHubFactory.make(with: .scanner)
        let navigation = UINavigationController(rootViewController: hubViewController)
        present(navigation, animated: true)
    }

    // MARK: Observers
    private func registerObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(ScannerBaseViewController.returnToForegroundHandler), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    @objc
    func returnToForegroundHandler(notification: NSNotification) {
        guard isScannerVisible else {
            return
        }
        tryStartScanner()
    }

    // MARK: Actions
    @objc
    func didTapArrow() {
        if navigationController == nil {
            isBottomSheetMaximized ? minimizeMyCodeView() : PaginationCoordinator.shared.showHome()
        } else if navigationController?.viewControllers.count == 1 {
            navigationController?.dismiss(animated: true)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    private func showInfoViewController() {
        let controller = ScannerInfoFactory.make(origin: .sendQrCodeScreen)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func isErrorPopupController(_ controller: UIViewController) -> Bool {
        controller is AlertPopupViewController
    }
    
    @objc
    private func didTapInformationLabel() {
        viewModel.newScannerInformationAnalytics()
        if let url = URL(string: "https://cdn.picpay.com/Engajamento/092020/QR_Code/webview-1.html") {
            let webview = WebViewFactory.make(with: url)
            present(UINavigationController(rootViewController: webview), animated: true)
        }
    }
    
    private func displayActionSheet(imageActivity: UIActivityViewController, linkActivity: UIActivityViewController) {
        let alert = UIAlertController(title: DefaultLocalizable.btShare.text, message: nil, preferredStyle: .actionSheet)
        
        let shareImage = UIAlertAction(title: ScannerLocalizable.shareImage.text, style: .default) { [weak self] _ in
            self?.viewModel.shareQRCodeImageLinkAnalytics(isLink: false)
            self?.present(imageActivity, animated: true)
        }
        let shareLink = UIAlertAction(title: ScannerLocalizable.shareLink.text, style: .default) { [weak self] _ in
            self?.viewModel.shareQRCodeImageLinkAnalytics(isLink: true)
            self?.present(linkActivity, animated: true)
        }
        let cancel = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel)
        
        alert.addAction(shareImage)
        alert.addAction(shareLink)
        alert.addAction(cancel)
        present(alert, animated: true)
    }
}

extension ScannerBaseViewController: PaginationChild {
    func childWillAppear() {
        BreadcrumbManager.shared.addCrumb("ScannerBaseViewController", typeOf: .screen)
        isScannerVisible = true
        tryStartScanner()
        myCodeBottomSheet.configureQRCode()
    }
    
    func childDidAppear() {
        // viewDidAppear don't have override implementation here
    }

    func childWillDisappear() {
        stopScanner()
    }
}

extension ScannerBaseViewController: ScannerBaseViewControllerProtocol {
    func minimizeMyCodeView() {
        myCodeBottomSheet.minimizeView()
    }

    func maximizeMyCodeView() {
        myCodeBottomSheet.maximizeView()
    }
}

extension ScannerBaseViewController: ScannerViewDelegate {
    func didTapScanBillButton() {
        openBoletoScanner()
    }
}

extension ScannerBaseViewController: ScannerInfoDelegate {
    func didTapInfo() {
        showInfoViewController()
    }
}

extension ScannerBaseViewController: MyCodeViewControllerAnimationDelegate {
    func animate(scale: CGFloat) {
        arrowButton.transform = CGAffineTransform(rotationAngle: .pi + scale * .pi / 2)
        isBottomSheetMaximized = scale >= 0.6
    }
}

extension ScannerBaseViewController: ScannerBaseViewModelDelegate {
    func startLoading(message: String) {
        beginState(animated: true, model: StateLoadingViewModel(message: message))
    }

    func stopLoading() {
        endState()
    }

    func internetFailure() {
        let alert = UIAlertController(title: nil, message: DefaultLocalizable.internetFailure.text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: DefaultLocalizable.btOk.text, style: .cancel, handler: { [weak self] _ in
            alert.dismiss(animated: true)
            self?.scanner?.preventNewReads = false
        }))

        present(alert, animated: true)
    }

    func handleError(message: String) {
        endState()

        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: DefaultLocalizable.btOk.text, style: .cancel, handler: { [weak self] _ in
            alert.dismiss(animated: true)
            self?.scanner?.preventNewReads = false
        }))

        present(alert, animated: true)
    }

    func showPavPayment(payment: PaymentViewController, navigation: UINavigationController) {
        present(navigation, animated: true)
    }

    func showNewPavPayment(parkingVC: UIViewController) {
        present(parkingVC, animated: true)
    }

    func showP2pPayment(payment: NewTransactionViewController, navigation: UINavigationController) {
        payment.showCancel = true
        present(navigation, animated: true)
    }

    func showEcommercePayment(navigation: UINavigationController) {
        present(navigation, animated: true)
    }

    func showATM24(controller: UIViewController) {
        present(controller, animated: true)
    }

    func showQRCodeBills(navigation: UINavigationController) {
        present(navigation, animated: true)
    }
    
    func showBilletForm(navigation: UINavigationController) {
        present(navigation, animated: true)
    }

    func showSubcriptionPayment(payment: ProducerPlansViewController, navigation: UINavigationController) {
        payment.onClose = { [weak self] in
            self?.scanner?.prepareForDismiss()
            navigation.dismiss(animated: true)
        }

        present(navigation, animated: true)
    }

    func showP2MPayment(paymentInfo: P2MPaymentInfo) {
        P2MHelpers.present(paymentInfo, from: self) { [weak self] goToOtherScreen in
            if !goToOtherScreen {
                self?.scanner?.preventNewReads = false
            }
        }
    }

    func showP2MLinx(paymentInfo: LinxPaymentInfo) {
        P2MHelpers.presentP2MLinx(paymentInfo, from: self)
    }
    
    func showVendingMachine(navigation: UINavigationController) {
        present(navigation, animated: true)
    }

    func deepLinkOnComplete() {
        didTapArrow()
    }

    func tryStartScanner() {
        guard isScannerVisible else {
            return
        }

        BreadcrumbManager.shared.addCrumb("ScannerBaseViewController", typeOf: .screen)

        switch viewModel.cameraPermission.status {
        case .authorized:
            startScanner()
            updateViews(showRequestCameraPermission: false)
            scanner?.start()
        default:
            updateViews(showRequestCameraPermission: true)
        }
    }
    
    func showPixPayment(details: PIXPaymentDetails) {
        let orchestrator = PIXPaymentOrchestrator(paymentDetails: details, origin: .qrcode)
        guard let paymentController = orchestrator.paymentViewController else {
            return
        }
        if let navigationController = navigationController {
            navigationController.pushViewController(paymentController, animated: true)
            return
        }
        
        guard let paymentSearchNavigation = SessionManager.sharedInstance.getPaymentNavigation() else {
            return
        }
        paymentController.shouldDisplayCloseButton = true
        let navController = UINavigationController(rootViewController: paymentController)
        PaginationCoordinator.shared.showHome(animated: false)
        paymentSearchNavigation.present(navController, animated: true)
    }

    func showAlertError(error: RequestError) {
        AlertMessage.showCustomAlertWithErrorUI(
            error,
            controller: self
        ) {
            self.isScannerVisible = true
            self.tryStartScanner()
        }
    }
}

extension ScannerBaseViewController: UIAdaptivePresentationControllerDelegate {
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        if isErrorPopupController(viewControllerToPresent) == false {
            viewControllerToPresent.modalPresentationStyle = .fullScreen
        }
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
}

extension ScannerBaseViewController: NewScannerViewProtocol {
    func restartScanner(isScanner: Bool) {
        viewModel.scannerSegmentChangedAnalytics(isScanner: isScanner)
        isScannerVisible = isScanner
        isScanner ? tryStartScanner() : stopScanner()
    }
    
    func scanBillTapped() {
        viewModel.callBillsScannerAnalytics()
        openBoletoScanner()
    }
    
    func shareCodeTapped(qrcodeImage: UIImage?, consumerImage: UIImage?, username: String?) {
        viewModel.didTapShareCode(
            withQrCode: qrcodeImage,
            userImage: consumerImage,
            username: username
        ) { [weak self] shareImageActivity, shareLinkActivity in
            self?.displayActionSheet(imageActivity: shareImageActivity, linkActivity: shareLinkActivity)
        }
    }
    
    func didTapPixInfoButton() {
        showInfoViewController()
    }
}

extension ScannerBaseViewController: StatefulProviding { }

