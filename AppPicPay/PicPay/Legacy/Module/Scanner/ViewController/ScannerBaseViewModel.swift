import AnalyticsModule
import Billet
import Core
import FeatureFlag
import Foundation
import PIX
import UI
import VendingMachine

enum BrCodeGuiType: String {
    case P2P = "COM.PICPAY.P2P"
    case P2B = "COM.PICPAY.P2B"
    case LINK = "COM.PICPAY.LINK"
}

protocol ScannerBaseViewModelDelegate: AnyObject {
    func startLoading(message: String)
    func stopLoading()
    func internetFailure()
    func handleError(message: String)
    func showPavPayment(payment: PaymentViewController, navigation: UINavigationController)
    func showNewPavPayment(parkingVC: UIViewController)
    func showP2pPayment(payment: NewTransactionViewController, navigation: UINavigationController)
    func showEcommercePayment(navigation: UINavigationController)
    func showSubcriptionPayment(payment: ProducerPlansViewController, navigation: UINavigationController)
    func showP2MPayment(paymentInfo: P2MPaymentInfo)
    func showP2MLinx(paymentInfo: LinxPaymentInfo)
    func showATM24(controller: UIViewController)
    func showQRCodeBills(navigation: UINavigationController)
    func showBilletForm(navigation: UINavigationController)
    func showVendingMachine(navigation: UINavigationController)
    func deepLinkOnComplete()
    func tryStartScanner()
    func showPixPayment(details: PIXPaymentDetails)
    func showAlertError(error: RequestError)
}

final class ScannerBaseViewModel: NSObject {
    typealias Dependencies = HasAnalytics

    weak var delegate: ScannerBaseViewModelDelegate?
    lazy var cameraPermission = {
        return PPPermission.camera(withDeniedAlert: PPPermission.Alert(
            title: ScannerLocalizable.authorizePicPayToUseYourCamera.text,
            message: ScannerLocalizable.scanPicPayPaymentCodes.text,
            settings: DefaultLocalizable.settings.text
        ))
    }()
    private let pixService: PixQrCodeServicing = PixQrCodeService(dependencies: DependencyContainer())
    private let dependencies: Dependencies

    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }

    // MARK: Error Handling
    func handleError(error: Error?) {
        var message = ""

        switch error?._code ?? NSURLErrorTimedOut{
        case NSURLErrorTimedOut:
            message = ScannerLocalizable.problemsWithYourConnection.text
            break
        case 5005:
            message = ScannerLocalizable.productDoesNotExist.text
            break
        case 1100:
            message = ScannerLocalizable.thisIsNotPicPayCode.text
            break
        default:
            message = error?.localizedDescription ?? ScannerLocalizable.somethingWentWrong.text
            break
        }

        delegate?.handleError(message: message)
    }

    // MARK: Handle new code
    func didReadNewCode(scannedText: String){
        dependencies.analytics.log(QRCodeEvent.didReadCode(code: scannedText))
        
        if checkIfCodeIsBrCode(for: scannedText) {
            let deeplinkString = extractDeeplinkFromBRCode(content: scannedText)
            if let deeplinkURL = URL(string: deeplinkString), DeeplinkHelper.handleDeeplink(withUrl: deeplinkURL, fromScanner: true) {
                delegate?.deepLinkOnComplete()
                return
            }
        }
        
        if let deepLinkURL = URL(string: scannedText), DeeplinkHelper.handleDeeplink(withUrl: deepLinkURL, fromScanner: true) {
            delegate?.deepLinkOnComplete()
            return
        }
        
        if FeatureManager.isActive(.releasePixQRCodePFPaymentEnableBool) && scannedText.lowercased().contains("br.gov.bcb.pix") {
            pixBrCode(scannedText: scannedText)
            return
        }
        
        if scannedText.lowercased().contains(FeatureManager.text(.featureQrCodeScanCashout24).lowercased()) {
            atm24QrCode()
            return
        }
        
        for type in ["iptuvix", "pmv"] where scannedText.contains(type) {
            qrCodeDecode(scannedText: scannedText, type: type)
        }
        
        if FeatureManager.isActive(.vendingMachinePayments) &&
            (scannedText.contains("vmpay") ||
                scannedText.contains("/vending-machine/")) {
            vmpayQrCode(scannedText: scannedText)
            return
        }
        
        // SERA DEPRECIADO O DEEPLINK ESTA GERENCIANDO ISSO
        if scannedText.contains("picpay.me") {
            picpayMeQrCode(scannedText: scannedText)
            return
        }
        
        // TODO: ALTERAR PARA DOMINIO/URL CORRETO
        // SERA DEPRECIADO O DEEPLINK ESTA GERENCIANDO ISSO
        if scannedText.contains("ecommerce.ppay.me/checkout") || scannedText.contains("app.picpay.com/checkout") || scannedText.contains("picpay.me/checkout") {
            dependencies.analytics.log(ScannerEvent.qrcodeScanned(.ecommerce))
            ecommerceQrCode(scannedText: scannedText)
            return
        }
        
        qrCodeDecode(scannedText: scannedText)
    }

    // MARK: Analytics
    func callPixScreenAnalytics() {
        dependencies.analytics.log(PixNavigationEvent.userNavigated(from: .hubScreen, to: .sendQrCodeScreen))
    }
    
    func callBillsScannerAnalytics() {
        dependencies.analytics.log(ScannerEvent.billsScannerAccessed)
    }
    
    func shareQRCodeImageLinkAnalytics(isLink: Bool) {
        dependencies.analytics.log(ScannerEvent.qrcodeShare(isLink ? .shareLink : .shareImage))
    }
    
    func newScannerInformationAnalytics() {
        dependencies.analytics.log(ScannerEvent.qrcodeInformationAccessed)
    }
    
    func scannerSegmentChangedAnalytics(isScanner: Bool) {
        dependencies.analytics.log(ScannerEvent.qrcodeSegmentedChanged(isScanner ? .reader : .myQrCode))
    }
    
    func cameraPermissionAnalytics() {
        dependencies.analytics.log(ScannerEvent.permissionCameraClicked)
    }

    // MARK: Handling Code
    func checkIfCodeIsBrCode(for scannedText: String) -> Bool {
        let scannedTextSuffix = scannedText.suffix(8)
        return scannedText.starts(with: "0002") && scannedTextSuffix.starts(with: "6304")
    }
    
    func extractDeeplinkFromBRCode(content: String) -> String {
        if content.contains(BrCodeGuiType.P2P.rawValue) {
            dependencies.analytics.log(ScannerEvent.qrcodeScanned(.p2p, true))
            return extractLegacyContentFromBrCode(guiType: .P2P, content: content)
        }
        
        if content.contains(BrCodeGuiType.P2B.rawValue) {
            dependencies.analytics.log(ScannerEvent.qrcodeScanned(.p2m, true))
            return extractLegacyContentFromBrCode(guiType: .P2B, content: content)
        }
        
        if content.contains(BrCodeGuiType.LINK.rawValue) {
            dependencies.analytics.log(ScannerEvent.qrcodeScanned(.ecommerceOld, true))
            return extractLegacyContentFromBrCode(guiType: .LINK, content: content)
        }
        
        return content
    }
    
    func extractLegacyContentFromBrCode(guiType: BrCodeGuiType, content: String) -> String {
        guard let range = content.range(of: guiType.rawValue) else { return "" }
        
        // Index up to type + 4 more digits
        let indexForPrefix = content.distance(from: content.startIndex, to: range.upperBound) + 4
        
        // get raw index
        let stringIndexForPrefix = content.index(content.startIndex, offsetBy: indexForPrefix)
        
        // substring + 4 more digits string
        let prefixSubstring = content[..<stringIndexForPrefix]
        
        // characters to read after lengthCount
        let substringReaderCount = prefixSubstring.suffix(2)
        
        // Index up to start of deeplink
        let deeplinkStartIndex = content.index(prefixSubstring.endIndex, offsetBy: Int(substringReaderCount) ?? 0)
        
        // actual deeplink
        let deeplink = content[substringReaderCount.endIndex..<deeplinkStartIndex]
        
        return String(deeplink)
    }
    
    private func pixBrCode(scannedText: String) {
        dependencies.analytics.log(ScannerEvent.qrcodeScanned(.pix))
        delegate?.startLoading(message: DefaultLocalizable.waitProcessingPix.text)
        pixService.validate(scannedText: scannedText) { [weak self] result in
            switch result {
            case let .success(account):
                self?.handlePixValidateSuccess(scannedText: scannedText, account: account)
            case let .failure(error):
                self?.delegate?.showAlertError(error: error.requestError ?? RequestError())
            }
            self?.delegate?.stopLoading()
        }
    }
    
    private func handlePixValidateSuccess(scannedText: String, account: PixQrCodeAccount) {
        let payee = PIXPayee(
            name: account.name,
            description: "\(account.cpfCnpj) | \(account.bank)",
            imageURLString: account.imageUrl,
            id: account.id
        )
        let params = PIXParams(originType: .qrCode, keyType: "QRCODE", key: scannedText, receiverData: nil)
        var items = [account.qrcodeInfos.payerData]
        if let receiverData = account.qrcodeInfos.receiverData {
            items.append(receiverData)
        }
        let details = PIXPaymentDetails(
            title: DefaultLocalizable.sendPayment.text,
            screenType: .qrCode(items: items),
            payee: payee,
            pixParams: params,
            value: account.value,
            isEditable: account.editable
        )
        delegate?.showPixPayment(details: details)
    }
    
    func vmpayQrCode(scannedText: String) {
        let navigation = UINavigationController(rootViewController: MachineStateFactory.make(withTarget: scannedText, self))
        navigation.modalPresentationStyle = .fullScreen
        dependencies.analytics.log(ScannerEvent.qrcodeScanned(.vendingMachine))
        delegate?.showVendingMachine(navigation: navigation)
    }

    func picpayMeQrCode(scannedText: String) {
        guard let url = URL(string: scannedText) else {
            return
        }

        delegate?.startLoading(message: DefaultLocalizable.wait.text)

        let username = url.path.replacingOccurrences(of: "/", with: "")

        let service = DeepLinkApi()
        service.get(username: username) { [weak self] (result) in
            DispatchQueue.main.async {
                self?.delegate?.stopLoading()

                switch result{
                case .success(let content):

                    switch content.type {

                    case .consumer:
                        self?.dependencies.analytics.log(ScannerEvent.qrcodeScanned(.p2p))
                        // Consumer Deeplink
                        guard let p2pPaymentNav = ViewsManager.peerToPeerStoryboardFirtNavigationController(),
                            let p2pPaymentVc = p2pPaymentNav.topViewController as? NewTransactionViewController,
                            let contact = content.data as? PPContact else {
                                return
                        }


                        p2pPaymentVc.preselectedContact = contact
                        self?.delegate?.showP2pPayment(payment: p2pPaymentVc, navigation: p2pPaymentNav)


                        break
                    case.membership:
                        // Subscription Deeplink
                        guard let producer = content.data as? ProducerProfileItem else {
                            return
                        }

                        let subscriptionsModel = ProducerCategoriesViewModel(producer: producer)
                        let subscriptionOptions = ProducerPlansViewController(model: subscriptionsModel, origin: .deeplink)
                        let navigation = PPNavigationController(rootViewController: subscriptionOptions)

                        self?.delegate?.showSubcriptionPayment(payment: subscriptionOptions, navigation: navigation)

                        break
                    default:
                        self?.handleError(error: PicPayError(message: ScannerLocalizable.userCouldNotIdentified.text))
                        return
                    }

                    break
                case .failure(let error):
                    guard error == .notConnectedToInternet else {
                        self?.handleError(error: error)
                        return
                    }
                    self?.delegate?.internetFailure()
                }
            }
        }
    }

    func atm24QrCode() {
        let navigation = UINavigationController(rootViewController: WithdrawOptionsFactory.make())
        navigation.pushViewController(CashoutValuesFactory.make(), animated: false)
        navigation.modalPresentationStyle = .fullScreen
        dependencies.analytics.log(ScannerEvent.qrcodeScanned(.cashout24h))
        delegate?.showATM24(controller: navigation)
    }

    func p2pQrCode(scannedText: String) {
        delegate?.startLoading(message: DefaultLocalizable.wait.text)
        guard let p2pPaymentNav = ViewsManager.peerToPeerStoryboardFirtNavigationController(),
            let p2pPaymentVc = p2pPaymentNav.topViewController as? NewTransactionViewController,
            let url = URL(string: scannedText) else {
                return
        }

        let username = url.path.replacingOccurrences(of: "/", with: "")
        p2pPaymentVc.loadConsumerInfo(username)
        delegate?.showP2pPayment(payment: p2pPaymentVc, navigation: p2pPaymentNav)
        delegate?.stopLoading()
    }

    func qrCodeDecode(scannedText: String, type: String = "") {
        let scannerApi = ScannerApi()
        delegate?.startLoading(message: DefaultLocalizable.wait.text)
        scannerApi.qrCodeDecode(scannedText, type: type) { [weak self] (result) in
            DispatchQueue.main.async {
                self?.delegate?.stopLoading()
                switch result {
                case .success(let qrCodeInfo):
                    self?.handleQRCodeInfo(qrCodeInfo, scannedText)

                case .failure(let error):
                    self?.handleError(error: error)
                }
            }
        }
    }

    func handleQRCodeInfo(_ qrCodeInfo: ScannerApi.QRCodeInfo, _ scannedText: String) {
        guard let data = qrCodeInfo.data else {
            return
        }

        switch qrCodeInfo.type.lowercased() {
        case "p2m":
            dependencies.analytics.log(ScannerEvent.qrcodeScanned(.p2m))
            if let paymentInfo = P2MPaymentInfo(json: data) {
                self.handleP2MPayment(paymentInfo: paymentInfo)
            }

        case "linx":
            dependencies.analytics.log(ScannerEvent.qrcodeScanned(.linx))
            if let paymentInfo = P2MLinxModelAdapter.transformToLinxPaymentInfo(from: data) {
                self.handleP2MLinx(paymentInfo: paymentInfo)
            } else {
                self.handleError(error: PicPayError(message: P2MLinxLocalizable.unidentifiedCode.text))
            }
        case "item_by_qrcode":
            if let dict = data.dictionaryObject {
                self.handleItemByQRCode(dict: dict)
            } else {
                self.handleError(error: PicPayError(message: ScannerLocalizable.codeCouldNotidentified.text))
            }

        case "iptuvix", "pmv":
            dependencies.analytics.log(ScannerEvent.qrcodeScanned(.iptu))
            if let dict = data.dictionaryObject {
                self.handleQRCodeBills(with: qrCodeInfo.type, and: dict)
            } else {
                self.handleError(error: PicPayError(message: ScannerLocalizable.codeCouldNotidentified.text))
            }
            
        case "pix":
            pixBrCode(scannedText: scannedText)
        default:
            self.handleError(error: PicPayError(message: ScannerLocalizable.codeCouldNotidentified.text))
        }
    }

    func handleItemByQRCode(dict: [String: Any]) {
        let item = WSItemRequest.item(byJson: dict)

        DispatchQueue.main.async {
            self.delegate?.stopLoading()
            guard let item = item else {
                return
            }

            let typeParking = "A".utf8.map{Int8($0)}[0]

            if item.seller.currentPlanType.isKind(ofPlan: typeParking) {
                self.dependencies.analytics.log(ScannerEvent.qrcodeScanned(.pav))
                self.handlePavPayment(with: item)
                return
            }
        }

    }

    func handleQRCodeBills(with type: String, and dict: [String : Any]) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            let bills = try JSONDecoder().decode(BilletSelectionModel.self, from: jsonData)
            
            if bills.hasOnlyOneValue {
                let billet = bills.billets[0]
                handleOneBilletPayment(with: billet.linecode)
                return
            }
            
            guard bills.isNotEmpty else {
                handleError(error: PicPayError(message: ScannerLocalizable.codeCouldNotidentified.text))
                return
            }

            let controller = BilletSelectionFactory.make(model: bills)
            let navigation = PPNavigationController(rootViewController: controller)

            delegate?.stopLoading()
            delegate?.showQRCodeBills(navigation: navigation)
        } catch {
            handleError(error: PicPayError(message: ScannerLocalizable.codeCouldNotidentified.text))
        }
    }
    
    func handleOneBilletPayment(with linecode: String) {
        if FeatureManager.isActive(.releaseNewBilletPaymentPresentingBool) {
            let formController = BilletFormFactory.make(with: .qrCodeIptu, linecode: linecode)
            let navigationController = PPNavigationController(rootViewController: formController)
            
            delegate?.stopLoading()
            delegate?.showBilletForm(navigation: navigationController)
            
        } else {
            let request = DGBoletoVerificationRequest()
            request.code = linecode
            
            DGBoletoApi().verify(request: request) { [weak self] result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let billet):
                        let boletoFormController: DGBoletoFormViewController = UIStoryboard.viewController(.digitalGoodsBoleto)
                        
                        if let digitalGood = DGHelpers.getBoletoLocalData() {
                            boletoFormController.model.digitalGood = digitalGood
                        }
                        
                        boletoFormController.model.boleto = billet
                        let navigation = PPNavigationController(rootViewController: boletoFormController)
                        
                        self?.delegate?.stopLoading()
                        self?.delegate?.showBilletForm(navigation: navigation)
                        
                    case .failure:
                        self?.handleError(error: PicPayError(message: ScannerLocalizable.codeCouldNotidentified.text))
                    }
                }
            }
        }
    }

    func ecommerceQrCode(scannedText: String) {
        delegate?.startLoading(message: DefaultLocalizable.wait.text)
        
        let service = EcommerceService()
        
        // extract id from url
        var id = scannedText.replacingOccurrences(of: "https://ecommerce.ppay.me/checkout/", with: "")
        id = id.replacingOccurrences(of: "http://ecommerce.ppay.me/checkout/", with: "")
        id = id.replacingOccurrences(of: "ecommerce.ppay.me/checkout/", with: "")

        id = id.replacingOccurrences(of: "https://ecommerce.picpay.me/checkout/", with: "")
        id = id.replacingOccurrences(of: "http://ecommerce.picpay.me/checkout/", with: "")
        id = id.replacingOccurrences(of: "ecommerce.picpay.me/checkout/", with: "")

        id = id.replacingOccurrences(of: "https://app.picpay.com/checkout/", with: "")
        id = id.replacingOccurrences(of: "http://app.picpay.com/checkout/", with: "")
        id = id.replacingOccurrences(of: "app.picpay.com/checkout/", with: "")

        if let decodedData = Data(base64Encoded: id),
            let decodedString = String(data: decodedData, encoding: .utf8) {
            id = decodedString
        }

        service.getOrderInfo(id) { [weak self] result in
            DispatchQueue.main.async {
                self?.delegate?.stopLoading()
                switch result {
                case .success(let paymentInfo):
                    self?.openEcommercePayment(paymentInfo: paymentInfo)
                case .failure(let error):
                    guard error == .notConnectedToInternet else {
                        self?.handleError(error: error)
                        return
                    }
                    self?.delegate?.internetFailure()
                }
            }
        }
    }
    
    private func openEcommercePayment(paymentInfo: EcommercePaymentInfo) {
        let scannerApi = ScannerApi()
        guard scannerApi.newEcommerceIsActive else {
            let navigationController = legacyEcommercePayment(paymentInfo: paymentInfo)
            delegate?.showEcommercePayment(navigation: navigationController)
            return
        }
        
        let navigationController = newEcommercePayment(paymentInfo: paymentInfo)
        delegate?.showEcommercePayment(navigation: navigationController)
    }
    
    private func legacyEcommercePayment(paymentInfo: EcommercePaymentInfo) -> UINavigationController {
        let controller = EcommercePaymentFactory.make(
            origin: .scanner,
            paymentInfo: paymentInfo
        )
        
        return UINavigationController(rootViewController: controller)
    }

    private func newEcommercePayment(paymentInfo: EcommercePaymentInfo) -> UINavigationController {
        let model = EcommercePaymentOrchestratorModel(origin: .scanner, isModal: true)
        let orchestrator = EcommercePaymentOrchestrator(paymentInfo: paymentInfo, orchestrator: model)
        
        return UINavigationController(rootViewController: orchestrator.paymentViewController)
    }
    
    // MARK: Handle Parking
    func handlePavPayment(with item: MBItem) {
        // New parking screen
        if let sellerId = item.sellerId, let storeId = item.storeId {
            let parkingVC = ParkingCoordinator().start(sellerId: sellerId, storeId: storeId)
            delegate?.showNewPavPayment(parkingVC: parkingVC)
            return
        }
        // Payment view or old parking view

        guard let navigationController = ViewsManager.paymentStoryboardFirtNavigationController(),
            let paymentVC = navigationController.topViewController as? PaymentViewController else {
                return
        }
        paymentVC.item = item
        paymentVC.showCancel = true
        paymentVC.view.backgroundColor = Palette.ppColorGrayscale000.color
        delegate?.showPavPayment(payment: paymentVC, navigation: navigationController)
    }

    func handleP2MPayment(paymentInfo: P2MPaymentInfo) {
        delegate?.showP2MPayment(paymentInfo: paymentInfo)
    }

    func handleP2MLinx(paymentInfo: LinxPaymentInfo) {
        delegate?.showP2MLinx(paymentInfo: paymentInfo)
    }

    func activityToShare(withQRCodeImage qrCodeimage: UIImage? = nil, userImage: UIImage? = nil) -> UIActivityViewController? {
        guard let username = ConsumerManager.shared.consumer?.username else {
            return nil
        }
        // swiftlint:disable:next redundant_type_annotation
        let shareCodeView: ShareCodeView = ShareCodeView.fromNib()
        var activityItens = [Any]()

        if let qrCodeImg = qrCodeimage {
            shareCodeView.setup(username: username, qrCodeImage: qrCodeImg, userImage: userImage)
            guard let snapshot = shareCodeView.snapshot else {
                return nil
            }
            activityItens.append(snapshot)
        } else {
            activityItens.append("\(ScannerLocalizable.accessLink.text) picpay.me/\(username) \(ScannerLocalizable.toPayMeWithPicPay.text)")
        }
        return UIActivityViewController(activityItems: activityItens, applicationActivities: nil)
    }

    func requestCameraPermission() {
        PPPermission.request(permission: cameraPermission) { [weak self] status in
            guard status == .authorized else {
                return
            }
            self?.delegate?.tryStartScanner()
        }
    }
    
    func didTapShareCode(withQrCode qrCode: UIImage?, userImage: UIImage?, username: String?, completion: ((_ shareImageActivity: UIActivityViewController, _ shareLinkActivity: UIActivityViewController) -> Void)) {
        guard let qrCode = qrCode,
              let shareImageActivity = activityToShareImage(withQrCode: qrCode, userImage: userImage, username: username),
              let shareLinkActivity = activityToShareLink(username: username) else {
            return
        }
        completion(shareImageActivity, shareLinkActivity)
    }
}

// MARK: QRCode sharing activities creator
extension ScannerBaseViewModel {
    private func activityToShareImage(withQrCode qrCode: UIImage, userImage: UIImage?, username: String?) -> UIActivityViewController? {
        guard let username = username,
              let views = Bundle.main.loadNibNamed(String(describing: ShareCodeView.self), owner: nil, options: nil),
              let shareCodeView = views.first as? ShareCodeView else {
            return nil
        }
        shareCodeView.setup(username: username, qrCodeImage: qrCode, userImage: userImage)
        
        guard let snapshot = shareCodeView.snapshot else {
            return nil
        }
        return UIActivityViewController(activityItems: [snapshot], applicationActivities: nil)
    }
    
    private func activityToShareLink(username: String?) -> UIActivityViewController? {
        guard let username = username else {
            return nil
        }
        let stringItem = String(format: PaymentRequestLocalizable.shareLinkMessageNoValue.text, username)
        return UIActivityViewController(activityItems: [stringItem], applicationActivities: nil)
    }
}

extension ScannerBaseViewModel: MachineStateCoordinatingDelegate {
    func paymentViewController(
        seller: VendingMachineSeller,
        totalValue: Double,
        items: [VendingMachineItem],
        paymentInputs: VendingMachinePaymentInputs?
    ) -> UIViewController? {
        let paymentInfo = PAVPaymentInfo(storeId: seller.storeId, sellerId: seller.sellerId, totalValue: totalValue)
        return UINavigationController(
            rootViewController:
                PAVPaymentLoadingFactory.make(
                    paymentInfo: paymentInfo,
                    orderItems: items.map { PaymentItem(name: $0.name, unitValue: NSDecimalNumber(decimal: $0.unitValue).doubleValue, quantity: $0.quantity) },
                    dismissPresentingAfterSuccess: false,
                    dependency: DependencyContainer(),
                    actions: PAVPaymentActions(vmPaymentInputs: paymentInputs)
                )
        )
    }
}
