enum ScannerLocalizable: String, Localizable {
    
    case shareLink
    case shareImage
    case scannerMessage
    case scanBillButtonTitle
    case processingPDF
    case couldntFindBarcode
    case authorizePicPayToUseYourCamera
    case scanPicPayPaymentCodes
    case problemsWithYourConnection
    case productDoesNotExist
    case thisIsNotPicPayCode
    case somethingWentWrong
    case userCouldNotIdentified
    case codeCouldNotidentified
    case accessLink
    case toPayMeWithPicPay
    
    case infoTitle
    case firstInstruction
    case secondInstruction
    case thirdInstructionPartOne
    case thirdInstructionPartTwo
    case paymentScreenCommentPlaceholder
    
    case newQRCodeViewShareCodeButtonDescription
    case newSquareScannerMessageDescription
    case scannerSegmentedControlTitle
    case qrcodeViewSegmentedControlTitle
    
    case parkingScannerDescriptionMessage
    case p2mScannerDescriptionMessage
    
    case brCodePrefixValidator
    case brCodeSuffixValidator
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .scanner
    }
}
