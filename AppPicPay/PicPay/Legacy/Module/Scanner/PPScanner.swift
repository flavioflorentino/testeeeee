import Foundation
import UIKit
import ZXingObjC
import FeatureFlag

enum ScannerOrigin: String {
    case boleto = "Boleto"
    case parking = "Parking"
    case p2m = "P2M"
    case main = "Main"
}

typealias PPScannerSuccess = (String) -> ()
typealias PPScannerFail = () -> ()
typealias PPScannerValidation = (String) -> (Bool)

final class PPScanner: NSObject {
    fileprivate var parentView: UIView
    fileprivate var successCallback: PPScannerSuccess?
    fileprivate var failCallback: PPScannerFail?
    fileprivate var parentFrame: CGRect
    fileprivate var useLandscapeOrientation: Bool
    fileprivate var isValid: PPScannerValidation?
    fileprivate var supportedTypes: [AVMetadataObject.ObjectType] = [.qr, .dataMatrix]
    fileprivate let scannerOrigin: ScannerOrigin
    fileprivate let isNative: Bool
    fileprivate var codeScanned: Bool = false
    
    // For using ZXing barcode processing library
    fileprivate lazy var zxCapture = ZXCapture()
    
    // For using the native barcode processing library
    fileprivate lazy var avCaptureSession = AVCaptureSession()
    fileprivate lazy var metadataOutput = AVCaptureMetadataOutput()
    fileprivate var avPreviewLayer: AVCaptureVideoPreviewLayer?
    
    var preventNewReads = false
    
    init(origin: ScannerOrigin, view: UIView, originPosition: CGPoint? = nil, useLandscapeOrientation: Bool = false, supportedTypes: [AVMetadataObject.ObjectType]? = nil, isValid: PPScannerValidation? = nil, onRead: PPScannerSuccess? = nil, onFail: PPScannerFail? = nil) {
        parentView = view
        parentFrame = parentView.layer.frame
        
        // this is for positioning the camera frame when the parentView is not a fullScreen view.
        if let origin = originPosition {
            parentFrame.origin = origin
        }
        
        self.isValid = isValid
        self.useLandscapeOrientation = useLandscapeOrientation
        scannerOrigin = origin

        if let types = supportedTypes {
            self.supportedTypes = types
        }
        
        isNative = (origin == .parking) ? false : FeatureManager.isActive(.nativeScannerActive)
        
        super.init()
        
        if useLandscapeOrientation && parentFrame.width < parentFrame.height {
            let newFrame = CGRect(x: 0, y: 0, width: parentFrame.height + parentFrame.origin.y, height: parentFrame.width)
            parentFrame = newFrame
        }
        
        successCallback = onRead
        failCallback = onFail
        
        setupSession()
    }
    
    private func setupSession() {
        #if DEBUG        
        if isRunningTests {
            return
        }
        #endif

        if isNative {
            setupNativeSession()
        } else {
            setupZXingSession()
        }
    }
    
    private func setupNativeSession() {
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
            return
        }
        
        let videoInput: AVCaptureDeviceInput
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            self.failCallback?()
            return
        }
        
        if (avCaptureSession.canAddInput(videoInput)) {
            avCaptureSession.addInput(videoInput)
        } else {
            self.failCallback?()
            return
        }
        
        if (avCaptureSession.canAddOutput(metadataOutput)) {
            avCaptureSession.addOutput(metadataOutput)
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = supportedTypes
        } else {
            self.failCallback?()
            return
        }
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: avCaptureSession)
        if useLandscapeOrientation {
            previewLayer.connection?.videoOrientation = .landscapeRight
        }
        avPreviewLayer = previewLayer
        previewLayer.frame = parentFrame
        previewLayer.videoGravity = .resizeAspectFill
        parentView.layer.insertSublayer(previewLayer, at: 0)
        avCaptureSession.startRunning()
    }
    
    private func setupZXingSession() {
        zxCapture.camera = zxCapture.back()
        zxCapture.focusMode = .continuousAutoFocus
        zxCapture.delegate = self
        
        let hints = ZXDecodeHints()
        for type in supportedTypes {
            if let zxBarcodeFormat = zXingBarcodeFormat(type: type) {
                hints.addPossibleFormat(zxBarcodeFormat)
            }
        }
        zxCapture.hints = hints

        if useLandscapeOrientation {
            zxCapture.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi / 2))
        } else {
            zxCapture.rotation = 90
        }

        zxCapture.layer.frame = parentFrame
        parentView.layer.insertSublayer(zxCapture.layer, at: 0)
        zxCapture.start()
    }
    
    func prepareForDismiss() {
        if isNative {
            avPreviewLayer?.removeFromSuperlayer()
            avCaptureSession.stopRunning()
        } else {
            zxCapture.layer.removeFromSuperlayer()
            zxCapture.stop()
        }
    }
    
    func stop() {
        if isNative {
            metadataOutput.setMetadataObjectsDelegate(nil, queue: DispatchQueue.main)
            avPreviewLayer?.removeFromSuperlayer()
            avCaptureSession.stopRunning()
        } else {
            zxCapture.delegate = nil
            zxCapture.layer.removeFromSuperlayer()
            zxCapture.stop()
        }
        preventNewReads = true
        if !codeScanned {
            PPAnalytics.trackEvent("Saiu da tela sem escanear", properties: ["isNative": isNative, "origin": scannerOrigin.rawValue])
        }
    }
    
    func start() {
        if isNative {
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            addPreviewLayer(avPreviewLayer)
            avCaptureSession.startRunning()
        } else {
            zxCapture.delegate = self
            addPreviewLayer(zxCapture.layer)
            zxCapture.start()
        }
        preventNewReads = false
        codeScanned = false
        setupTimeEvents()
    }
    
    private func setupTimeEvents() {
        PPAnalytics.timeEvent("Sucesso ao escanear codigo")
        PPAnalytics.timeEvent("Saiu da tela sem escanear")
    }
    
    private func addPreviewLayer(_ layer: CALayer?) {
        if let previewLayer = layer, previewLayer.superlayer == nil {
            parentView.layer.insertSublayer(previewLayer, at: 0)
        }
    }
    
    private func zXingBarcodeFormat(type: AVMetadataObject.ObjectType) -> ZXBarcodeFormat? {
        switch (type) {
        case .qr:
            return kBarcodeFormatQRCode
        case .dataMatrix:
            return kBarcodeFormatDataMatrix
        case .interleaved2of5, .itf14:
            return kBarcodeFormatITF
        default:
            return nil
        }
    }
    
    private func processStringRead(_ stringValue: String) {
        if let isValid = isValid {
            guard isValid(stringValue) else {
            return
        }
        }
        codeScanned = true
        preventNewReads = true
        HapticFeedback.notificationFeedbackSuccess()
        if scannerOrigin != .boleto && scannerOrigin != .p2m {
            PPAnalytics.trackEvent("scanned_text", properties: ["text": stringValue])
        }
        PPAnalytics.trackEvent("Sucesso ao escanear codigo", properties: ["isNative": isNative, "origin": scannerOrigin.rawValue])
        self.successCallback?(stringValue)
    }
}

extension PPScanner: ZXCaptureDelegate {
    func captureResult(_ capture: ZXCapture!, result: ZXResult!) {
        guard !preventNewReads, capture != nil, result != nil else {
            return
        }
        processStringRead(result.text)
    }
}

extension PPScanner: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        guard !preventNewReads, let metadataObject = metadataObjects.first,
            let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject,
            let stringValue = readableObject.stringValue else {
            return
        }
        processStringRead(stringValue)
    }
}
