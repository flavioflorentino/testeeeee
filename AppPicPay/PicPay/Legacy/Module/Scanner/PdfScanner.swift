import Billet
import Core
import FeatureFlag
import Foundation
import UI
import UIKit
import ZXingObjC

@objc final class PdfScanner: NSObject {
    
    // Singleton
    @objc static let shared : PdfScanner =  {
        return PdfScanner()
    }()
    
    // Flag to stop processing loops in background
    var canceledByUser: Bool = false
    // Hash to identify if a pdf processing task is started while other pdf processing task is still running
    // By comparing the hash values it's possible to stop the older task
    var hashBoleto: String = ""
    
    final class LoadingViewController: UIViewController {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        
        override func viewDidLoad() {
            view.addSubview(activityIndicator)
            activityIndicator.translatesAutoresizingMaskIntoConstraints = false
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
            activityIndicator.startAnimating()
            
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.dismissLoadingScreen))
            view.backgroundColor = Palette.ppColorGrayscale000.color
            title = ScannerLocalizable.processingPDF.text
        }
        
        @objc func dismissLoadingScreen() {
            self.dismiss(animated: true, completion: nil)
            PdfScanner.shared.canceledByUser = true
        }
    }
    
    override init() {
        super.init()
    }
    
    @objc func processAndScanPdf(url: NSURL) -> Bool {
        
        if !isValidPdfUrl(nsurl: url) {
            debugPrint("PdfScanner.processAndScanPdf: Extensão inválida de arquivo, não é .pdf")
            return false
        }
        
        let navController = PPNavigationController(rootViewController: LoadingViewController())
        // Dismiss if there is another navigation controller on top
        if let mainScreenCoodinator = AppManager.shared.mainScreenCoordinator {
            mainScreenCoodinator.paymentSearchNavigation().dismiss(animated: false, completion: nil)
            mainScreenCoodinator.paymentSearchNavigation().present(navController, animated: false, completion: nil)
        }
        // Present a loading view controller
       

        PdfScanner.shared.canceledByUser = false
        PdfScanner.shared.hashBoleto = UUID().uuidString // A new random string
        let copyHashBoleto: String = PdfScanner.shared.hashBoleto
        
        DispatchQueue.global(qos: .background).async {
            let boletoVC: DGBoletoFormViewController = UIStoryboard.viewController(.digitalGoodsBoleto)
            boletoVC.model.digitalGood = DGHelpers.getBoletoLocalData()
            let model = DGBoletoFormViewModel()
            var errorMessage: String? = nil
        
            guard let absoluteUrl = url.absoluteURL else {
                return
            }
                
            if let pdfDoc: CGPDFDocument = self.getPdfDocument(from: absoluteUrl) {
                let pageCount = pdfDoc.numberOfPages
                
                for page in 1...pageCount {

                    if PdfScanner.shared.canceledByUser || copyHashBoleto != PdfScanner.shared.hashBoleto {
                        return
                    }
                    
                    guard let pdfPage: CGPDFPage = pdfDoc.page(at: page) else { continue }
                    guard let img: UIImage = self.convertPDFPageToImage(pdfPage: pdfPage, scale: 2.0) else { continue }
                    guard let quartzImg: CGImage = img.cgImage else { continue }
                    if let scannedText: String = self.decodeBarcode(img: quartzImg) {
                        
                        if FeatureManager.isActive(.releaseNewBilletPaymentPresentingBool) {
                            let formController = BilletFormFactory.make(with: .pdfFile, linecode: scannedText)
                            self.showBoletoForm(boletoVC: formController,
                                                currentHashBoleto: copyHashBoleto,
                                                navigationController: navController)
                            
                        } else {
                            model.verifyScannedCode(code: scannedText) { [weak self] error in
                                
                                if let error = error {
                                    errorMessage = error.localizedDescription
                                } else {
                                    model.isScanned = true
                                    boletoVC.model = model
                                    
                                    self?.showBoletoForm(boletoVC: boletoVC,
                                                         currentHashBoleto: copyHashBoleto,
                                                         navigationController: navController)
                                }
                            }
                            return
                        }
                    }
                }
            }
            
            if FeatureManager.isActive(.releaseNewBilletPaymentPresentingBool) {
                let formController = BilletFormFactory.make(with: .pdfFile)
                self.showBoletoForm(boletoVC: formController, currentHashBoleto: copyHashBoleto, navigationController: navController)
            } else {
                boletoVC.onAppearErrorMessage = errorMessage ?? ScannerLocalizable.couldntFindBarcode.text
                self.showBoletoForm(boletoVC: boletoVC, currentHashBoleto: copyHashBoleto, navigationController: navController)
            }
        }

        return true
    }
    
    func isValidPdfUrl(nsurl: NSURL) -> Bool {
        if let urlString = nsurl.absoluteString {
            let lastFour = urlString.suffix(4).lowercased()
            return (lastFour == ".pdf")
        }
        return false
    }
    
    func getPdfDocument(from url: URL) -> CGPDFDocument? {
        var pdfData: CFData
        do {
            pdfData = try NSData(contentsOf: url)
            if let provider: CGDataProvider = CGDataProvider(data: pdfData) {
                return CGPDFDocument(provider)
            }
        } catch let error {
            debugPrint("PdfScanner.getPdfDocument: \(error.localizedDescription)")
        }
        return nil
    }
    
    func convertPDFPageToImage(pdfPage: CGPDFPage, scale: CGFloat) -> UIImage? {
        
        var pageRect: CGRect = pdfPage.getBoxRect(.mediaBox)
        pageRect.size = CGSize(width: pageRect.size.width, height: pageRect.size.height)
        
        var renderRect: CGRect = pageRect
        renderRect.size = CGSize(width: pageRect.size.width * scale, height: pageRect.size.height * scale)
        
        UIGraphicsBeginImageContext(renderRect.size)
        guard let context: CGContext = UIGraphicsGetCurrentContext() else {
            UIGraphicsEndImageContext()
            return nil
        }
        context.saveGState()
        context.translateBy(x: 0.0, y: renderRect.size.height)
        context.scaleBy(x: scale, y: -scale)
        context.setFillColor(Palette.ppColorGrayscale000.cgColor)
        context.setAlpha(1.0)
        context.fill(renderRect)
        context.concatenate(pdfPage.getDrawingTransform(.mediaBox, rect: pageRect, rotate: 0, preserveAspectRatio: true))
        context.drawPDFPage(pdfPage)
        context.restoreGState()
        let pdfImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return pdfImage ?? nil
    }
    
    func decodeBarcode(img: CGImage) -> String? {
        let source = ZXCGImageLuminanceSource(cgImage: img)
        let bitmap = ZXBinaryBitmap(binarizer: ZXGlobalHistogramBinarizer(source: source))
        let reader = ZXMultiFormatReader()
        let multiReader = ZXGenericMultipleBarcodeReader(delegate: reader)
        
        let hints = ZXDecodeHints()
        hints.tryHarder = true
        hints.assumeCode39CheckDigit = true
        
        do {
            if let result: [Any] = try multiReader?.decodeMultiple(bitmap, hints: hints) {
            
                for code in result {
                    let strCode = "\(code)"
                    if strCode.length == 44 { // Only barcodes of 44 digits are accepted
                        return strCode
                    }
                }
            }
        } catch let error {
            debugPrint("PdfScanner.decodeBarcode: \(error.localizedDescription)")
        }
        return nil
    }
    
    func showBoletoForm(boletoVC: UIViewController, currentHashBoleto: String, navigationController: PPNavigationController) {
        if User.isAuthenticated {
            if currentHashBoleto == PdfScanner.shared.hashBoleto {
                DispatchQueue.main.async {
                    navigationController.setViewControllers([boletoVC], animated: true)
                }
            }
        }
    }

}
