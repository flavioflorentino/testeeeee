import AnalyticsModule
import Foundation
import PIX

protocol ScannerInfoInteracting: AnyObject {
    func callScreenAnalytics()
    func interactKnowMore()
    func interactOk()
}

final class ScannerInfoInteractor {
    typealias Dependencies = HasAnalytics
    // MARK: - Variables
    private let dependencies: Dependencies
    private let presenter: ScannerInfoPresenting
    private let knowMoreDeeplinkString = "https://meajuda.picpay.com/hc/pt-br/articles/360051880771-Tudo-sobre-pagamentos-com-Pix"
    private let origin: PixUserNavigation

    // MARK: - Life Cycle
    init(presenter: ScannerInfoPresenting, origin: PixUserNavigation, dependencies: Dependencies) {
        self.presenter = presenter
        self.origin = origin
        self.dependencies = dependencies
    }
}

// MARK: - ScannerInfoInteracting
extension ScannerInfoInteractor: ScannerInfoInteracting {
    func callScreenAnalytics() {
        dependencies.analytics.log(PixNavigationEvent.userNavigated(from: origin, to: .sendQrCodeHowItWorks))
    }

    func interactKnowMore() {
        guard let url = URL(string: knowMoreDeeplinkString) else {
            return
        }
        presenter.didNextStep(action: .deeplink(url: url))
    }
    
    func interactOk() {
        presenter.didNextStep(action: .close)
    }
}
