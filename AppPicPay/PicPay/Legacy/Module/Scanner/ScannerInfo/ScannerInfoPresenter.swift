import Foundation

protocol ScannerInfoPresenting: AnyObject {
    var viewController: ScannerInfoDisplaying? { get set }
    func didNextStep(action: ScannerInfoAction)
}

final class ScannerInfoPresenter {
    // MARK: - Variables
    private let coordinator: ScannerInfoCoordinating
    weak var viewController: ScannerInfoDisplaying?

    // MARK: - Life Cycle
    init(coordinator: ScannerInfoCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ScannerInfoPresenting
extension ScannerInfoPresenter: ScannerInfoPresenting {
    func didNextStep(action: ScannerInfoAction) {
        coordinator.perform(action: action)
    }
}
