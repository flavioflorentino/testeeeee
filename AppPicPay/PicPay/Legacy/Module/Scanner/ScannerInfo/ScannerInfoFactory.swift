import UIKit
import PIX

enum ScannerInfoFactory {
    static func make(origin: PixUserNavigation) -> ScannerInfoViewController {
        let container = DependencyContainer()
        let coordinator: ScannerInfoCoordinating = ScannerInfoCoordinator()
        let presenter: ScannerInfoPresenting = ScannerInfoPresenter(coordinator: coordinator)
        let interactor = ScannerInfoInteractor(presenter: presenter, origin: origin, dependencies: container)
        let viewController = ScannerInfoViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
