import AssetsKit
import UI
import UIKit
import PIX

protocol ScannerInfoDisplaying: AnyObject {
}

private enum Layout {
}

final class ScannerInfoViewController: ViewController<ScannerInfoInteracting, UIView> {

    // MARK: - Visual Components
    private lazy var contentScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = Colors.backgroundPrimary.color
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        let rootEdges = EdgeInsets.rootView
        stackView.compatibleLayoutMargins = EdgeInsets(
            top: Spacing.base01,
            leading: rootEdges.leading,
            bottom: rootEdges.bottom,
            trailing: rootEdges.trailing
        )
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var borderView: UIView = {
        let view = UIView()
        view.border = .light(color: .grayscale100(.default))
        view.cornerRadius = .medium
        return view
    }()
    
    private lazy var iluImageView: UIImageView = {
        let imageView = UIImageView(image: Resources.Illustrations.iluQrcode.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var firstInstructionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.text, ScannerLocalizable.firstInstruction.text)
        return label
    }()
    
    private lazy var secondInstructionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.text, ScannerLocalizable.secondInstruction.text)
        return label
    }()
    
    private lazy var knowMoreButton: UIButton = {
        let button = UIButton()
        let forMoreInfo = ScannerLocalizable.thirdInstructionPartOne.text
        let helpSection = ScannerLocalizable.thirdInstructionPartTwo.text
        let title = NSMutableAttributedString(
            string: forMoreInfo + helpSection,
            attributes: [
                .font: Typography.bodyPrimary().font(),
                .foregroundColor: Colors.grayscale700.color
            ]
        )
        title.addAttributes(
            [
                .foregroundColor: Colors.branding600.color,
                .underlineStyle: NSUnderlineStyle.thick.rawValue,
                .underlineColor: Colors.branding600.color
            ],
            range: NSRange(location: forMoreInfo.count, length: helpSection.count)
        )
        button.contentHorizontalAlignment = .left
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.setAttributedTitle(title, for: .normal)
        button.addTarget(self, action: #selector(didTapKnowMore), for: .touchUpInside)
        return button
    }()
    
    private lazy var okButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(DefaultLocalizable.btOkUnderstood.text, for: .normal)
        button.addTarget(self, action: #selector(didTapOk), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.callScreenAnalytics()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
                if traitCollection.userInterfaceStyle == .dark {
                    borderView.border = .light(color: .grayscale100(.dark))
                } else {
                    borderView.border = .light(color: .grayscale100(.light))
                }
            }
        } else {
            borderView.border = .light(color: .grayscale100(.light))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func buildViewHierarchy() {
        view.addSubview(contentScrollView)
        contentScrollView.addSubview(contentStackView)
        borderView.addSubview(iluImageView)
        contentStackView.addArrangedSubview(firstInstructionLabel)
        contentStackView.addArrangedSubview(borderView)
        contentStackView.addArrangedSubview(secondInstructionLabel)
        contentStackView.addArrangedSubview(knowMoreButton)
        contentStackView.addArrangedSubview(okButton)
    }
    
    override func setupConstraints() {
        contentScrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentStackView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            $0.height.greaterThanOrEqualTo(view.layoutMarginsGuide)
        }
        
        iluImageView.snp.makeConstraints {
            $0.leading.greaterThanOrEqualToSuperview().inset(Spacing.base02)
            $0.center.equalToSuperview()
            $0.top.greaterThanOrEqualToSuperview().inset(Spacing.base03)
        }
    }

    override func configureViews() {
        navigationItem.backBarButtonItem = .noContextTitlelessBackButton
        title = ScannerLocalizable.infoTitle.text
    }
    
    // MARK: - Actions
    @objc
    func didTapKnowMore() {
        interactor.interactKnowMore()
    }
    
    @objc
    func didTapOk() {
        interactor.interactOk()
    }
}

// MARK: - ScannerInfoDisplaying
extension ScannerInfoViewController: ScannerInfoDisplaying {
}
