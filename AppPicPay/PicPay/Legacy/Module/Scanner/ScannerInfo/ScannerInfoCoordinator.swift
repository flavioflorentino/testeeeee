import UIKit

enum ScannerInfoAction: Equatable {
    case deeplink(url: URL)
    case close
}

protocol ScannerInfoCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ScannerInfoAction)
}

final class ScannerInfoCoordinator {
    // MARK: - Variables
    weak var viewController: UIViewController?
}

// MARK: - ScannerInfoCoordinating
extension ScannerInfoCoordinator: ScannerInfoCoordinating {
    func perform(action: ScannerInfoAction) {
        switch action {
        case let .deeplink(url):
            let faqController = WebViewFactory.make(with: url)
            viewController?.navigationController?.pushViewController(faqController, animated: true)
        case .close:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}
