import UI

final class ShareCodeView: UIView {
    @IBOutlet weak var qrCodeImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var ppMeLinkLabel: UILabel!
    @IBOutlet weak var linkBgView: UIView!
    @IBOutlet weak var codeBackgroundView: UIView!
    @IBOutlet weak var bgView: GradientView!
    
    static let identifier = String(describing: ShareCodeView.self)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        codeBackgroundView.layer.cornerRadius = 8
        codeBackgroundView.layer.borderWidth = 1
        codeBackgroundView.layer.borderColor = UIColor(hexStr: "#dddddd")?.cgColor
    }
    
    func setup(username: String, qrCodeImage: UIImage, userImage: UIImage?) {
        userImageView.isHidden = userImage == nil
        userImageView.image = userImage
        qrCodeImageView.image = qrCodeImage
        usernameLabel.text = "@\(username)"
        ppMeLinkLabel.text = String.ppMeLink(username: username).replacingOccurrences(of: "https://", with: "")
        linkBgView.layer.cornerRadius = linkBgView.frame.height / 2.0
        ppMeLinkLabel.sizeToFit()
        ppMeLinkLabel.setNeedsLayout()
        ppMeLinkLabel.layoutIfNeeded()
        linkBgView.setNeedsLayout()
        linkBgView.layoutIfNeeded()
        backgroundColor = .clear
        
        bgView.startColor = Palette.hexColor(with: "#19e365")!
        bgView.endColor = Palette.hexColor(with: "#00ce9e")!
        bgView.startPoint = CGPoint.zero
        bgView.endPoint = CGPoint(x: 1, y: 1)
    }
}
