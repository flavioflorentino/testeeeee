import AssetsKit
import SnapKit
import UI
import UIKit

private extension NewQRCodeScannerView.Layout {
    enum Size {
        static let screenBounds = UIScreen.main.bounds
        static let squareTopOffset: CGFloat = 196.0
        static let squareMaskSize: CGFloat = 200.0
        static let buttonSize: CGSize = CGSize(width: 343, height: 48)
        static let qrcodeViewSize: CGSize = CGSize(width: squareMaskSize, height: squareMaskSize)
    }
}

final class NewQRCodeScannerView: UIView, ViewConfiguration {
    typealias Dependencies = HasConsumerManager
    private let dependencies: Dependencies
    
    fileprivate enum Layout { }
    
    private lazy var qrcodeView = QrCodeFactory.make(size: Layout.Size.qrcodeViewSize)
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, UIColor.white)
        return label
    }()
    
    private lazy var shareCodeButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(ScannerLocalizable.newQRCodeViewShareCodeButtonDescription.text, for: .normal)
        button.addTarget(self, action: #selector(didTapShareCodeButton), for: .touchUpInside)
        return button
    }()
    
    var didTapShareCodeCompletion: ((_ qrcodeImage: UIImage?, _ consumerImage: UIImage?, _ username: String?) -> ())?
    
    override init(frame: CGRect) {
        dependencies = DependencyContainer()
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(qrcodeView,
                    messageLabel,
                    shareCodeButton)
    }
    
    func setupConstraints() {
        qrcodeView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().offset(Layout.Size.squareTopOffset)
            $0.size.equalTo(Layout.Size.qrcodeViewSize)
        }
        
        messageLabel.snp.makeConstraints {
            $0.top.equalTo(qrcodeView.snp.bottom).offset(Spacing.base03)
            $0.centerX.equalTo(snp.centerX)
            $0.height.equalTo(Spacing.base02)
        }
        
        shareCodeButton.snp.makeConstraints {
            $0.top.equalTo(messageLabel.snp.bottom).offset(Spacing.base04)
            $0.centerX.equalTo(snp.centerX)
            $0.size.equalTo(Layout.Size.buttonSize)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.black.lightColor
        qrcodeView.generateQrCode()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        configureQRCode()
    }
    
    private func configureQRCode() {
        qrcodeView.cornerRadius = .medium

        guard let username = dependencies.consumerManager.consumer?.username else { return }
        messageLabel.text = "@" + username
    }
    
    @objc
    private func didTapShareCodeButton() {
        didTapShareCodeCompletion?(qrcodeView.qrCodeImage, qrcodeView.consumerImage, dependencies.consumerManager.consumer?.username)
    }
}

