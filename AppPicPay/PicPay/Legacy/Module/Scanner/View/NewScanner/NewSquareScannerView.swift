import AssetsKit
import SnapKit
import UI
import UIKit
import PIX

extension NewSquareScannerView.Layout {
    enum Size {
        static let screenBounds = UIScreen.main.bounds
        static let squareSizeOffset: CGFloat = 12
        static let squareTopOffset: CGFloat = 196.0
        static let squareMaskSize: CGFloat = 200.0
        static let buttonHeight: CGFloat = 48
        static let margin: CGFloat = 20
        static let pathCornerRadius: CGFloat = 10
        static let buttonEdgeInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }
    
    enum Color {
        static let transparentViewColor = Colors.black.lightColor.withAlphaComponent(0.8)
    }
}

final class NewSquareScannerView: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    var scanBillButtonTapped: (() -> ())?
    var pixInfoButtonTapped: (() -> ())?
    
    private lazy var pixTitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.white.lightColor)
            .with(\.text, DefaultLocalizable.sendPayment.text)
        label.isHidden = true
        return label
    }()
    
    private lazy var pixInfoButton: UIButton = {
        let button = UIButton()
        let icon = Iconography.questionCircle.rawValue
        button.setTitle(icon, for: .normal)
        button.tintColor = Colors.white.lightColor
        button.addTarget(self, action: #selector(didTapPixInfoButton), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private lazy var squarePlaceholder: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Resources.Placeholders.scannerMask.image
        return imageView
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.text = ScannerLocalizable.newSquareScannerMessageDescription.text
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, UIColor.white)
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var scanBillButton: UIButton = {
        let button = UIButton()
        button.setTitle(ScannerLocalizable.scanBillButtonTitle.text, for: .normal)
        button.buttonStyle(SecondaryButtonStyle())
            .with(\.borderColor, (color: .white(.light), state: .normal))
            .with(\.textColor, (color: .white(.light), state: .normal))
            .with(\.contentEdgeInsets, Layout.Size.buttonEdgeInsets)
        button.addTarget(self, action: #selector(didTapBillButton), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(squarePlaceholder,
                    messageLabel,
                    scanBillButton,
                    pixTitleLabel,
                    pixInfoButton)
    }
    
    func setupConstraints() {
        pixTitleLabel.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(compatibleSafeArea.top).offset(Spacing.base02)
        }
        
        pixInfoButton.snp.makeConstraints {
            $0.centerY.equalTo(pixTitleLabel)
            $0.trailing.equalToSuperview().offset(-Spacing.base01)
        }
        
        squarePlaceholder.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().offset(Layout.Size.squareTopOffset)
            $0.size.equalTo(Layout.Size.squareMaskSize)
        }
        
        messageLabel.snp.makeConstraints {
            $0.top.equalTo(squarePlaceholder.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        scanBillButton.snp.makeConstraints {
            $0.top.equalTo(messageLabel.snp.bottom).offset(Spacing.base04)
            $0.centerX.equalTo(snp.centerX)
            $0.height.equalTo(Layout.Size.buttonHeight)
        }
    }
    
    func configureViews() {
        backgroundColor = Layout.Color.transparentViewColor
    }
    
    func configureFor(_ scannerType: ScannerType) {
        switch scannerType {
            case .default:
                configureDefaultScanner()
            case .pix:
                configurePixScanner()
            case .parking:
                configureParkingScanner()
            case .p2m:
                configureP2mScanner()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        configureMaskLayer()
    }
}

private extension NewSquareScannerView {
    func configurePixScanner() {
        pixTitleLabel.isHidden = false
        pixInfoButton.isHidden = false
    }
    
    func configureDefaultScanner() {
        scanBillButton.isHidden = false
    }
    
    func configureParkingScanner() {
        messageLabel.text = ScannerLocalizable.parkingScannerDescriptionMessage.text
    }
    
    func configureP2mScanner() {
        messageLabel.text = ScannerLocalizable.p2mScannerDescriptionMessage.text
    }
    
    func configureMaskLayer() {
        let clearFrame = CGRect(x: squarePlaceholder.frame.origin.x + Layout.Size.squareSizeOffset/2,
                                y: squarePlaceholder.frame.origin.y + Layout.Size.squareSizeOffset/2,
                                width: squarePlaceholder.frame.width - Layout.Size.squareSizeOffset,
                                height: squarePlaceholder.frame.height - Layout.Size.squareSizeOffset)
        
        let path = UIBezierPath(roundedRect: frame, cornerRadius: Layout.Size.pathCornerRadius)
        path.append(UIBezierPath(roundedRect: clearFrame, cornerRadius: Layout.Size.pathCornerRadius))
        path.usesEvenOddFillRule = true
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        maskLayer.fillRule = .evenOdd
        
        layer.mask = maskLayer
    }
    
    @objc
    func didTapBillButton() {
        scanBillButtonTapped?()
    }
    
    @objc
    func didTapPixInfoButton() {
        pixInfoButtonTapped?()
    }
}

