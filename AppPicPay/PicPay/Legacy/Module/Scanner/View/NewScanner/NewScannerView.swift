import AssetsKit
import SnapKit
import UI
import UIKit
import PIX

extension NewScannerView.Layout {
    enum Size {
        static let screenBounds = UIScreen.main.bounds
        static let centerYOffsetMask: CGFloat = -100.0
        static let segmentedControlYOffset: CGFloat = 116.0
        static let squareMaskSize: CGFloat = 200.0
        static let buttonHeight: CGFloat = 44
        static let margin: CGFloat = 20
        static let segmentedControlSize: CGSize = CGSize(width: squareMaskSize + Spacing.base01, height: Spacing.base04)
        static let segmentedControlFontSize: CGFloat = 14.0
    }
    
    enum Color {
        static let transparentViewColor = Colors.black.lightColor.withAlphaComponent(0.8)
    }
}

protocol NewScannerViewProtocol: AnyObject {
    func scanBillTapped()
    func shareCodeTapped(qrcodeImage: UIImage?, consumerImage: UIImage?, username: String?)
    func restartScanner(isScanner: Bool)
    func didTapPixInfoButton()
}

final class NewScannerView: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var segmentedControl: UISegmentedControl = {
        let segmentedControl = UISegmentedControl()
        segmentedControl.insertSegment(withTitle: ScannerLocalizable.scannerSegmentedControlTitle.text, at: 0, animated: false)
        segmentedControl.insertSegment(withTitle: ScannerLocalizable.qrcodeViewSegmentedControlTitle.text, at: 1, animated: false)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Colors.white.lightColor,
                                                 NSAttributedString.Key.font: Typography.bodySecondary().font()],
                                                for: .normal)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Colors.black.lightColor,
                                                 NSAttributedString.Key.font: Typography.bodySecondary().font()],
                                                for: .selected)
        if #available(iOS 13.0, *) {
            segmentedControl.selectedSegmentTintColor = .white
        }
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.backgroundColor = .black
        segmentedControl.tintColor = Colors.white.lightColor
        segmentedControl.border = .light(color: .white(.light))
        segmentedControl.cornerRadius = .light
        segmentedControl.addTarget(self, action: #selector(didTapSegmentedControl), for: .valueChanged)
        segmentedControl.isHidden = true
        return segmentedControl
    }()
    
    private var isScanner: Bool = true {
        didSet {
            squareScannerView.isHidden = !isScanner
            qrcodeScannerView.isHidden = isScanner
        }
    }
    
    private lazy var squareScannerView = NewSquareScannerView()
    private lazy var qrcodeScannerView = NewQRCodeScannerView()
    private var scannerType: ScannerType
    
    var delegate: NewScannerViewProtocol?
    
    init(scannerType: ScannerType) {
        self.scannerType = scannerType
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        buildLayout()
        bringSubviewToFront(segmentedControl)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(squareScannerView,
                    qrcodeScannerView,
                    segmentedControl)
    }
    
    func setupConstraints() {
        segmentedControl.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Layout.Size.segmentedControlYOffset)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.segmentedControlSize)
        }
        
        squareScannerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        qrcodeScannerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func configureViews() {
        squareScannerView.isHidden = !isScanner
        qrcodeScannerView.isHidden = isScanner
        
        squareScannerView.scanBillButtonTapped = { [weak self] in
            self?.delegate?.scanBillTapped()
        }
        
        squareScannerView.pixInfoButtonTapped = { [weak self] in
            self?.delegate?.didTapPixInfoButton()
        }
        
        qrcodeScannerView.didTapShareCodeCompletion = { [weak self] qrcodeImage, consumerImage, username in
            self?.delegate?.shareCodeTapped(qrcodeImage: qrcodeImage, consumerImage: consumerImage, username: username)
        }
        
        squareScannerView.configureFor(scannerType)

        guard case .default = scannerType else { return }
        configureForDefaultScanner()
    }
    
    @objc
    private func didTapSegmentedControl() {
        isScanner.toggle()
        self.delegate?.restartScanner(isScanner: isScanner)
    }
    
    private func configureForDefaultScanner() {
        segmentedControl.isHidden = false
    }
}
