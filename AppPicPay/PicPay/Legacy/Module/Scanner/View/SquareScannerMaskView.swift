import UI

final class SquareScannerMaskView: UIView {
    private typealias SquarePoints = (pointA: CGPoint, pointB: CGPoint, pointC: CGPoint, pointD: CGPoint)
    
    enum Layout {
        static let maskColor = Palette.black.color.withAlphaComponent(0.8)
        static let borderColor = Palette.ppColorBranding300
        static let borderWidth: CGFloat = 6
        static let borderSideLenght: CGFloat = 32
    }
    
    private let centerYOffset: CGFloat
    private let squareSize: CGFloat
    
    private lazy var darkView: UIView = {
        let view = UIView()
        view.backgroundColor = Layout.maskColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var bordersView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    init(frame: CGRect, centerYOffset: CGFloat, squareSize: CGFloat) {
        self.centerYOffset = centerYOffset
        self.squareSize = squareSize
        super.init(frame: frame)
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildViewHierarchy() {
        addSubview(darkView)
        addSubview(bordersView)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.constraintAllEdges(from: darkView, to: self)
        NSLayoutConstraint.constraintAllEdges(from: bordersView, to: self)
    }
    
    private func configureViews() {
        let center = CGPoint(x: frame.width / 2, y: frame.height / 2 + centerYOffset)
        let squarePoints = squareShapePoints(forCenter: center, sideSize: squareSize)
        
        configureDarkView(with: squarePoints)
        configureBordersView(with: squarePoints)
    }
    
    private func configureDarkView(with squarePoints: SquarePoints) {
        let maskPath = UIBezierPath(rect: frame)
        let squarePath = squareShapePath(points: squarePoints)
        maskPath.append(squarePath)
        maskPath.usesEvenOddFillRule = true
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        maskLayer.fillRule = .evenOdd
        
        let borderLayer = CAShapeLayer()
        borderLayer.path = squarePath.cgPath
        
        darkView.layer.addSublayer(borderLayer)
        darkView.layer.mask = maskLayer
    }
    
    private func configureBordersView(with squarePoints: SquarePoints) {
        let squareCornersPath = cornersPath(points: squarePoints, sideSize: Layout.borderSideLenght)
        let corner = CAShapeLayer()
        corner.strokeColor = Layout.borderColor.cgColor
        corner.path = squareCornersPath.cgPath
        corner.lineWidth = Layout.borderWidth
        corner.fillColor = UIColor.clear.cgColor

        bordersView.layer.addSublayer(corner)
    }
    
    private func squareShapePoints(forCenter center: CGPoint, sideSize: CGFloat) -> SquarePoints {
        let halfSize = sideSize / 2
        let points = SquarePoints(
            pointA: CGPoint(x: center.x - halfSize, y: center.y - halfSize),
            pointB: CGPoint(x: center.x + halfSize, y: center.y - halfSize),
            pointC: CGPoint(x: center.x + halfSize, y: center.y + halfSize),
            pointD: CGPoint(x: center.x - halfSize, y: center.y + halfSize))
        return points
    }
    
    private func squareShapePath(points: SquarePoints) -> UIBezierPath {
        let rectPath = UIBezierPath()
        rectPath.move(to: points.pointA)
        rectPath.addLine(to: points.pointB)
        rectPath.addLine(to: points.pointC)
        rectPath.addLine(to: points.pointD)
        rectPath.close()
        return rectPath
    }
    
    private func cornersPath(points: SquarePoints, sideSize: CGFloat) -> UIBezierPath {
        let path = UIBezierPath()
        drawCorner(using: path, cornerPoint: points.pointA, offsetX: sideSize, offsetY: sideSize)
        drawCorner(using: path, cornerPoint: points.pointB, offsetX: -sideSize, offsetY: sideSize)
        drawCorner(using: path, cornerPoint: points.pointC, offsetX: -sideSize, offsetY: -sideSize)
        drawCorner(using: path, cornerPoint: points.pointD, offsetX: sideSize, offsetY: -sideSize)
        return path
    }
    
    private func drawCorner(using path: UIBezierPath, cornerPoint: CGPoint, offsetX: CGFloat, offsetY: CGFloat) {
        path.move(to: CGPoint(x: cornerPoint.x + offsetX, y: cornerPoint.y))
        path.addLine(to: CGPoint(x: cornerPoint.x, y: cornerPoint.y))
        path.addLine(to: CGPoint(x: cornerPoint.x, y: cornerPoint.y + offsetY))
    }
}
