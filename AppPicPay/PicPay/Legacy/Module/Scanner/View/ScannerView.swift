import UI

protocol ScannerViewDelegate: AnyObject {
    func didTapScanBillButton()
}

final class ScannerView: UIView {
    private typealias SquarePoints = (pointA: CGPoint, pointB: CGPoint, pointC: CGPoint, pointD: CGPoint)
    
    enum Layout {
        static let screenBounds = UIScreen.main.bounds
        static let centerYOffsetMask: CGFloat = -40.0
        static let squareMaskSize: CGFloat = max(screenBounds.width - 140, 170)
        static let buttonHeight: CGFloat = 44
        static let margin: CGFloat = 20
    }
    
    private lazy var brandLogo: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "white_logo")
        view.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.text = ScannerLocalizable.scannerMessage.text
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        label.numberOfLines = 0
        label.textColor = Palette.white.color
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var squareMaskView: SquareScannerMaskView = {
        let view = SquareScannerMaskView(frame: Layout.screenBounds, centerYOffset: Layout.centerYOffsetMask, squareSize: Layout.squareMaskSize)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var squarePlaceholder: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var scanBillButton: UIButton = {
        let button = UIButton()
        button.setTitle(ScannerLocalizable.scanBillButtonTitle.text, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        button.layer.cornerRadius = Layout.buttonHeight / 2
        button.layer.borderColor = Palette.white.cgColor
        button.layer.borderWidth = 1
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        button.addTarget(self, action: #selector(tapScanBillButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    weak var delegate: ScannerViewDelegate?
     
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        buildViewHierarchy()
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func buildViewHierarchy() {
        addSubview(squareMaskView)
        addSubview(brandLogo)
        addSubview(messageLabel)
        addSubview(scanBillButton)
        addSubview(squarePlaceholder)
    }

    private func setupConstraints() {
        NSLayoutConstraint.constraintAllEdges(from: squareMaskView, to: self)
        
        var topConstant: CGFloat = 20 // status bar height
        if #available(iOS 11, *) {
            topConstant = 0
        }
        
        NSLayoutConstraint.activate([
            brandLogo.centerXAnchor.constraint(equalTo: centerXAnchor),
            brandLogo.topAnchor.constraint(equalTo: compatibleSafeAreaLayoutGuide.topAnchor, constant: 10 + topConstant),
            brandLogo.heightAnchor.constraint(equalToConstant: 32),
            
            messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Layout.margin),
            messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Layout.margin),
            messageLabel.bottomAnchor.constraint(equalTo: squarePlaceholder.topAnchor, constant: -24),
            
            squarePlaceholder.centerYAnchor.constraint(equalTo: centerYAnchor, constant: Layout.centerYOffsetMask),
            squarePlaceholder.centerXAnchor.constraint(equalTo: centerXAnchor),
            squarePlaceholder.widthAnchor.constraint(equalToConstant: Layout.squareMaskSize),
            squarePlaceholder.heightAnchor.constraint(equalToConstant: Layout.squareMaskSize),
            
            scanBillButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            scanBillButton.topAnchor.constraint(equalTo: squarePlaceholder.bottomAnchor, constant: 36),
            scanBillButton.heightAnchor.constraint(equalToConstant: Layout.buttonHeight)
        ])
    }
    
    @objc
    private func tapScanBillButton() {
        delegate?.didTapScanBillButton()
    }
}
