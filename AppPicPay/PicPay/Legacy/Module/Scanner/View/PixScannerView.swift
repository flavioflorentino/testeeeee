import AssetsKit
import UI
import UIKit

protocol ScannerInfoDelegate: AnyObject {
    func didTapInfo()
}

private extension PixScannerView.Layout {
    static let screenBounds = UIScreen.main.bounds
    static let centerYOffsetMask: CGFloat = -40.0
    static let squareMaskSize: CGFloat = max(screenBounds.width - 140, 170)
}

final class PixScannerView: UIView {
    fileprivate enum Layout {}

    // MARK: - Visual Components
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodyPrimary(.highlight).font()
        label.textColor = Colors.white.lightColor
        label.text = DefaultLocalizable.sendPayment.text
        return label
    }()

    private lazy var infoButton: UIButton = {
        let button = UIButton()
        let icon = Iconography.questionCircle.rawValue
        button.setTitle(icon, for: .normal)
        button.tintColor = Colors.white.lightColor
        button.addTarget(self, action: #selector(didTapInfo), for: .touchUpInside)
        return button
    }()

    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.white.lightColor)
            .with(\.text, ScannerLocalizable.scannerMessage.text)
            .with(\.textAlignment, .center)
        return label
    }()

    private lazy var squareMaskView: SquareScannerMaskView = {
        let view = SquareScannerMaskView(frame: Layout.screenBounds, centerYOffset: Layout.centerYOffsetMask, squareSize: Layout.squareMaskSize)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var squarePlaceholder: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    // MARK: - Variables
    weak var infoDelegate: ScannerInfoDelegate?

    // MARK: - Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildViewHierarchy()
        setupConstraints()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func buildViewHierarchy() {
        addSubview(squareMaskView)
        addSubview(titleLabel)
        addSubview(infoButton)
        addSubview(messageLabel)
        addSubview(squarePlaceholder)
    }

    private func setupConstraints() {
        squareMaskView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        titleLabel.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(compatibleSafeArea.top).offset(Spacing.base02)
        }

        infoButton.snp.makeConstraints {
            $0.centerY.equalTo(titleLabel)
            $0.trailing.equalToSuperview().offset(-Spacing.base01)
        }

        messageLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(squarePlaceholder.snp.top).offset(-Spacing.base03)
        }

        squarePlaceholder.snp.makeConstraints {
            $0.height.width.equalTo(Layout.squareMaskSize)
            $0.centerY.equalTo(snp.centerY).offset(Layout.centerYOffsetMask)
            $0.centerX.equalTo(snp.centerX)
        }
    }
    
    // MARK: - Actions
    @objc private func didTapInfo() {
        infoDelegate?.didTapInfo()
    }
}
