import Core
import FeatureFlag
import UIKit
import SwiftyJSON

final class ScannerApi: BaseApi {
    var newEcommerceIsActive: Bool {
        return FeatureManager.isActive(.newEcommerce)
    }
    
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    final class QRCodeInfo: BaseApiResponse {
        var type: String = ""
        var data: JSON?
        
        required init?(json: JSON) {
            guard let type = json["type"].string else {
                return
            }
            self.type = type
            self.data = json["data"]
        }
    }
    
    /// Get qrCode decode information
    ///
    /// - Parameters:
    ///   - hash: Scanned Text
    ///   - completion: callback for completion
    func qrCodeDecode(_ hash: String, type: String = "", _ completion: @escaping ((PicPayResult<QRCodeInfo>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let params: [String: Any] = [ "value": hash, "type": type]
            requestManager
                .apiRequest(endpoint: kWsUrlScannerQRCodeDecode,
                            method: .post,
                            parameters: params,
                            headers: ["X-Api-Version": "mobile"])
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
}
