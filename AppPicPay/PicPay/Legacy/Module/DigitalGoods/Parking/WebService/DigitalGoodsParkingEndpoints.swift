import Core
import Foundation

struct DigitalGoodsParkingEndpoints: ApiEndpointExposable {
    let digitalGoodType: String
    let endpoint: Endpoint
    
    var path: String {
        switch endpoint {
        case .transaction:
            return "\(digitalGoodType)/transactions/app"
        case let .ticket(ticket, id):
            // Parameters here, because ticket is an URL that has to be encoded (and if is on variable parameters, it is encoded twice)
            return "\(digitalGoodType)/ticket?ticket_number=\(ticket)&parking_id=\(id)"
        }
    }

    var method: HTTPMethod {
        switch endpoint {
        case .transaction:
            return .post
        case .ticket:
            return .get
        }
    }

    var body: Data? {
        guard case let .transaction(params, _) = endpoint else {
            return nil
        }
        return params.toData()
    }

    var customHeaders: [String: String] {
        guard case let .transaction(_, pin) = endpoint else {
            return [:]
        }
        return ["password": pin]
    }
}

extension DigitalGoodsParkingEndpoints {
    enum Endpoint {
        case transaction(params: [String: Any], pin: String)
        case ticket(ticket: String, id: String)
    }
}
