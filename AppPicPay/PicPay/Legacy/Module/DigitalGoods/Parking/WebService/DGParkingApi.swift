import Core
import FeatureFlag
import Foundation

final class DGParkingApiRequest: DGPayRequest {
    // Variable params
    var digitalGoodId: String?
    var ticket: String?
    var price: Double?
    
    override func toParams() throws -> [String : Any] {
        let params = try super.toParams()
        
        let optionalParamsToBeMerged = [
            "id_digitalgoods": digitalGoodId,
            "digitalgoods": [
                "type": "parkings",
                "ticket_number": ticket,
                "value": String(format: "%.2f", price ?? 0.0)
            ] as [String : Any?]
        ] as [String : Any?]
        
        let paramsToBeMerged = try obligatoryParams(params: optionalParamsToBeMerged)
        
        // Merge super's params with this class' specific params overriding any duplicated entries
        return params.merging(paramsToBeMerged, uniquingKeysWith: { (_, new) in new })
    }
}

final class DGParkingApi: BaseApi {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    var digitalGoodType = "digitalgoods/parkings"
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func ticketInfo(id: String, ticket: String, _ completion: @escaping (PicPayResult<DGParking>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
            let params = [
                "ticket_number": ticket,
                "parking_id": id
            ]
            requestManager
                .apiRequest(endpoint: "\(digitalGoodType)/ticket", method: .get, parameters: params)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        var allowedQueryParamAndKey = NSCharacterSet.urlQueryAllowed
        allowedQueryParamAndKey.remove(charactersIn: ";/?:@&=+$, ")
        let encodedUrl = ticket.addingPercentEncoding(withAllowedCharacters: allowedQueryParamAndKey)
        
        let endpoint = DigitalGoodsParkingEndpoints(digitalGoodType: digitalGoodType,
                                                    endpoint: .ticket(ticket: encodedUrl ?? ticket, id: id))
        Api<Data>(endpoint: endpoint).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    // DGParkingApiRequest is a subclass of DGPayRequest.
    // It adds specific (and necessary) information to the Parking payment request
    func pay(request: DGParkingApiRequest, pin: String, _ completion: @escaping ((WrapperResponse) -> Void)) {
        do {
            let params = try request.toParams()
            guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
                requestManager
                    .apiRequest(endpoint: "\(digitalGoodType)/transactions/app", method: .post, parameters: params, pin: pin)
                    .responseApiObject(completionHandler: completion)
                return
            }
            
            // Core network version
            let endpoint = DigitalGoodsParkingEndpoints(digitalGoodType: digitalGoodType,
                                                        endpoint: .transaction(params: params, pin: pin))
            Api<Data>(endpoint: endpoint).execute { result in
                ApiSwiftJsonWrapper(with: result).transform(completion)
            }
        } catch let error {
            guard let error = error as? PicPayError else {
                return
            }
            completion(PicPayResult.failure(error))
        }
    }
}
