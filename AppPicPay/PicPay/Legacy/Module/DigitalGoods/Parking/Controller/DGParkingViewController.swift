import FeatureFlag
import Foundation
import SnapKit
import UI
import ZXingObjC

final class DGParkingViewController: PPBaseViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    // MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageView: UICircularImageView!
    @IBOutlet weak var scannerView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var scanTarget: UIImageView!
    @IBOutlet weak var leftBg: UIView!
    @IBOutlet weak var topBg: UIView!
    @IBOutlet weak var bottomBg: UIView!
    @IBOutlet weak var rightBg: UIView!
    
    // MARK: Private vars
    private var scanner:PPScanner?
    private let permissionView = AskForCameraPermissionView()
    lazy private var cameraPermission = {
        return PPPermission.camera(withDeniedAlert: PPPermission.Alert(
            title: DefaultLocalizable.authorizePicPayCamera.text,
            message: BilletLocalizable.scanPicPayPaymentCodes.text,
            settings: DefaultLocalizable.settings.text
        ))
    }()
    let loader = Loader.getLoadingView(DefaultLocalizable.wait.text)
    
    static func fromNib() -> DGParkingViewController {
        return controllerFromStoryboard(.digitalGoodsParking)
    }
    
    let model = DGParkingViewModel()
    
    private lazy var newScannerView = NewScannerView(scannerType: .parking)

    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setup()
        setupColors()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scanner?.preventNewReads = false
        tryStartScanner()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        scanner?.stop()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    internal func initScanner() {
        if scanner == nil {
            // Scanner object
            scanner = PPScanner(origin: .parking, view: scannerView, supportedTypes: [.interleaved2of5, .qr, .dataMatrix], onRead: { [weak self] (text) in
                self?.onRead(scannedText: text)
            })
        }
    }
    
    func setupViews() {
        if FeatureManager.shared.isActive(.experimentNewAccessQrCode) {
            configureForNewScanner()
        }
        
        permissionView.action = { [weak self] (button) in
            guard let strongSelf = self else {
            return
        }
            PPPermission.request(permission: strongSelf.cameraPermission, { status in
                guard status == .authorized else {
            return
        }
                self?.tryStartScanner()
            })
        }
        
        permissionView.view.backgroundColor = Palette.black.color.withAlphaComponent(0.95)
        permissionView.descriptionLabel.textColor = Palette.white.color
        permissionView.isHidden = cameraPermission.status == .authorized
        
        scannerView.addSubview(permissionView)
        scannerView.bringSubviewToFront(backButton)
        permissionView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        let bgColor = Palette.black.color.withAlphaComponent(0.8)
        leftBg.backgroundColor = bgColor
        rightBg.backgroundColor = bgColor
        topBg.backgroundColor = bgColor
        bottomBg.backgroundColor = bgColor
        
        if let loaderView = self.loader {
            self.view.addSubview(loaderView)
        }
        
        paintSafeAreaBottomInset(withColor: Palette.white.color)
    }
    
    private func configureForNewScanner() {
        backButton.isHidden = true
        scanTarget.isHidden = true
        leftBg.isHidden = true
        topBg.isHidden = true
        bottomBg.isHidden = true
        rightBg.isHidden = true
        
        scannerView.addSubview(newScannerView)
        
        newScannerView.snp.makeConstraints {
            $0.edges.equalTo(scannerView)
        }
    }
    
    // MARK: Public setup
    func setup() {
        titleLabel.text = model.digitalGood?.name
        descriptionLabel.text = model.digitalGood?.itemDescription
        guard let urlString = model.digitalGood?.imgUrl else {
            return
        }
        imageView.setImage(url: URL(string: urlString))
        imageView.borderColor = #colorLiteral(red: 0.8901960784, green: 0.8901960784, blue: 0.8901960784, alpha: 1)
        imageView.borderWidth = 1
    }
    
    private func setupColors() {
        titleLabel.textColor = Palette.black.color
        descriptionLabel.textColor = Palette.black.color.withAlphaComponent(0.8)
    }
    
    // MARK: Scanner/permissions
    fileprivate func onRead(scannedText: String) {
        loader?.isHidden = false
        model.checkTicketNumber(ticket: scannedText) { [weak self] result in
            self?.loader?.isHidden = true
            switch result {
            case .success(let parking):
                self?.model.goToPayment(from: self!, parking: parking, ticket: scannedText)
            case .failure(let error):
                AlertMessage.showAlert(error, controller: self, completion: {
                    self?.scanner?.preventNewReads = false
                })
            }
        }

    }
    
    fileprivate func removeRequestCameraView() {
        permissionView.isHidden = true
    }
    
    fileprivate func presentRequestCameraView() {
        permissionView.isHidden = false
    }
    
    internal func tryStartScanner() {
        switch cameraPermission.status {
        case .authorized:
            removeRequestCameraView()
            if scanner == nil {
                initScanner()
            }
            self.scanner?.start()
        default:
            presentRequestCameraView()
        }
    }
}

// MARK: Actions
extension DGParkingViewController {
    @IBAction private func back(_ sender: Any) {
        navigationController?.dismiss(animated: true, completion: { [weak self] in
            self?.scanner?.prepareForDismiss()
        })
    }
}
