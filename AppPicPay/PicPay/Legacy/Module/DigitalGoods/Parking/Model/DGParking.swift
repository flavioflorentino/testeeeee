//
//  DGParking.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 20/04/18.
//

import Foundation
import SwiftyJSON

final class DGParking: BaseApiResponse {
    var detailsList: [[String: String]]?
    var markdown: String?
    var price: Double?
    var ticketNumber: String?
    
    required init?(json: JSON) {
        guard let list = json["details"].object as? [[String: String]],
            let price = json["value"].double else {
            return
        }
        if let disclaimer = json["disclaimer"].string {
            self.markdown = disclaimer
        }

        self.price = price
        self.detailsList = list
        self.price = price

    }
}

