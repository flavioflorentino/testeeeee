import Core
import FeatureFlag
import Foundation
import PCI

final class DGParkingViewModel: NSObject {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    private let service: ParkingServiceProtocol = ParkingService()
    fileprivate let api = DGParkingApi()
    var digitalGood: DGItem?
    var parking: DGParking?
    
    func checkTicketNumber(ticket: String, completion: @escaping (PicPayResult<DGParking>) -> Void) {
        guard let id = digitalGood?.id else {
            return
        }
        api.ticketInfo(id: id, ticket: ticket) { result in
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func goToPayment(from: UIViewController, parking: DGParking, ticket: String) {
        guard let digitalGood = digitalGood else {
            return
        }
        
        if FeatureManager.isActive(.newParking) {
            openNewPaymentViewController(from: from, product: parking, digitalGood: digitalGood, ticket: ticket)
            return
        }
        
        openLegacyPaymentViewController(from: from, product: parking, digitalGood: digitalGood, ticket: ticket)
    }
    
    private func openLegacyPaymentViewController(from: UIViewController, product: DGParking, digitalGood: DGItem, ticket: String) {
        guard
            let paymentManager = PPPaymentManager(),
            let price = product.price
            else {
                return
        }
        
        parking = product
        parking?.ticketNumber = ticket
        paymentManager.subtotal = NSDecimalNumber(value: price)
        let paymentScreen = DGPaymentViewController.controllerFromStoryboard()
        let details = createPaymentDetails(product: product)
        
        let paymentModel = DGPaymentViewModel(
            amount: price,
            type: .parking,
            sellerName: digitalGood.name,
            sellerImageUrl: digitalGood.imgUrl,
            sellerId: digitalGood.id,
            detailInfo: details,
            dependencies: DependencyContainer()
        )
        
        paymentModel.paymentType = "parking"
        paymentScreen.title = DefaultLocalizable.voucherPaymentTitle.text
        paymentScreen.setup(paymentManager: paymentManager, model: paymentModel, delegate: self)

        // Add the right button to the payment screen + show SFSafariVC on tap
        if let infoUrl = digitalGood.infoUrl {
            paymentScreen.setRightButton()
            paymentScreen.rightBarButtonAction = { controller in
                guard let url = URL(string:infoUrl) else {
            return
        }
                ViewsManager.presentSafariViewController(url, from: controller)
            }
        }

        from.navigationController?.pushViewController(paymentScreen, animated: true)
    }
    
    
    private func openNewPaymentViewController(from: UIViewController, product: DGParking, digitalGood: DGItem, ticket: String) {
        guard let price = product.price else {
            return
        }
        
        let details = createPaymentDetails(product: product)
        let items = DigitalGoodsPayment(
            title: digitalGood.name,
            image: digitalGood.imgUrl,
            link: digitalGood.infoUrl,
            payeeId: digitalGood.sellerId,
            value: price,
            paymentType: .parking
        )
        let model = DigitalGoodsParking(ticket: ticket, price: String(price))
        let service = DigitalGoodsParkingService(model: model, item: digitalGood)
        let orchestrator = DigitalGoodsPaymentOrchestrator(items: items, paymentDetails: details, service: service)
        
        from.navigationController?.pushViewController(orchestrator.paymentViewController, animated: true)
    }
    
    private func createPaymentDetails(product: DGParking) -> [DGPaymentDetailsBase] {
        guard let detailsList = product.detailsList else {
            return []
        }
        
        var details: [DGPaymentDetailsBase] = []
        
        for detail in detailsList {
            let title = detail["title"] ?? ""
            let value = detail["value"] ?? ""
            
            details.append(DGPaymentDetailsItem(name: title, detail: value))
        }
        
        if let disclaimerMarkdown = product.markdown, disclaimerMarkdown.isNotEmpty {
            details.append(DGPaymentImportantDetailItem(text: disclaimerMarkdown))
        }
        
        return details
    }
}


// MARK: Payment request construction
extension DGParkingViewModel: DGPaymentDelegate {
    func didPerformPayment(with manager: PPPaymentManager, cvv: String?, additionalPayload: [String: Any], pin: String, biometry: Bool, privacyConfig: String, someErrorOccurred: Bool, completion: @escaping (WrapperResponse) -> Void) {
        guard let digitalGood = digitalGood else {
            return
        }

        let request = DGParkingApiRequest()
        
        request.paymentManager = manager
        request.pin = pin
        request.biometry = biometry
        request.sellerId = digitalGood.sellerId
        request.digitalGoodId = digitalGood.id
        request.privacyConfig = privacyConfig
        request.someErrorOccurred = someErrorOccurred
        request.ticket = parking?.ticketNumber
        request.price = parking?.price
        
        if FeatureManager.isActive(.pciParking) {
            createTransactionPCI(manager: manager, request: request, password: pin, cvv: cvv, completion: completion)
            return
        }
        
        api.pay(request: request, pin: pin, completion)
    }
    
    private func createTransactionPCI(
        manager: PPPaymentManager,
        request: DGParkingApiRequest,
        password: String,
        cvv: String?,
        completion: @escaping (WrapperResponse) -> Void
    ) {
        guard let parkingPayload = createParkingPayload(manager: manager, request: request) else {
            let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            completion(PicPayResult.failure(error))
            return
        }
        let cvv = createCVVPayload(manager: manager, cvv: cvv)
        let payload = PaymentPayload<DigitalGoodsPayload<Parking>>(cvv: cvv, generic: parkingPayload)
        service.createTransaction(password: password, payload: payload, isNewArchitecture: false) { result in
            let mappedResult = result
                .map { BaseApiGenericResponse(dictionary: $0.json) }
                .mapError { $0.picpayError }
                           
            completion(mappedResult)
        }
    }
    
    private func createParkingPayload(manager: PPPaymentManager, request: DGParkingApiRequest) -> DigitalGoodsPayload<Parking>? {
        guard let params = try? request.toParams(),
            let data = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) else {
                return nil
        }
        
        return try? JSONDecoder.decode(data, to: DigitalGoodsPayload<Parking>.self)
    }
    
    private func createCVVPayload(manager: PPPaymentManager, cvv: String?) -> CVVPayload? {
        guard let cvv = cvv else {
            return createLocalStoreCVVPayload(manager: manager)
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func createLocalStoreCVVPayload(manager: PPPaymentManager) -> CVVPayload? {
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        guard manager.cardTotal()?.doubleValue != .zero,
              let cvv = dependencies.keychain.getData(key: KeychainKeyPF.cvv(id)) else {
                return nil
        }
        
        return CVVPayload(value: cvv)
    }
}
