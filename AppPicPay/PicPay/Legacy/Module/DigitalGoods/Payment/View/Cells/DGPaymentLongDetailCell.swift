//
//  DGPaymentLongDetailCell.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 24/10/17.
//

import Foundation

final class DGPaymentLongDetailCell: UITableViewCell, DGPaymentDetailCellBase {
    @IBOutlet weak var mainLabel: UILabel!

    var height: CGFloat {
        mainLabel.sizeToFit()
        return mainLabel.frame.height + 16
    }
    
    func setup(data: DGPaymentDetailsBase) {
        guard let data = data as? DGPaymentLongDetailItem else {
            return
        }
        mainLabel.text = data.text
    }
}

