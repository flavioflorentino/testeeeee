import Foundation
import UI

final class DGPaymentDetailsCell: UITableViewCell, DGPaymentDetailCellBase {
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var infoButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var distanceToInfo: NSLayoutConstraint!
    private var action: (() -> Void)?
    var height: CGFloat {
        return 30
    }
    
    func setup(data: DGPaymentDetailsBase) {
        guard let data = data as? DGPaymentDetailsItem else {
            return
        }
        
        setupColors()
        mainLabel.text = "\(data.name):"
        detailLabel.text = data.detail
        detailLabel.textColor = data.detailColor
        infoButtonWidth.constant = data.showInfo ? 18 : 0
        distanceToInfo.constant = data.showInfo ? 9 : 0
        backgroundColor = data.backgroundColor
        action = data.action
        
        infoButton.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
    }
    
    private func setupColors() {
        backgroundColor = Colors.groupedBackgroundSecondary.color
        mainLabel.textColor = Palette.ppColorGrayscale400.color
        infoButton.tintColor = Colors.branding600.color
        infoButton.showsTouchWhenHighlighted = false
    }
    
    @objc
    func didTapButton() {
        action?()
    }
}
