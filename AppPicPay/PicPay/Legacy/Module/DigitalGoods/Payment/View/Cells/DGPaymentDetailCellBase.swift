//
//  DGPaymentDetailCellBase.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 24/10/17.
//

import Foundation

protocol DGPaymentDetailCellBase {
    var height: CGFloat { get }
    func setup(data: DGPaymentDetailsBase)
}

