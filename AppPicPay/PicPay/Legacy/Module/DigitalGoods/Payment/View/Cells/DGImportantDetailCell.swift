import Foundation
import UI

protocol DGImportantDetailCellDelegate: AnyObject {
    func importantDetailCellOnTouchLink(url: URL) -> Bool
}

final class DGImportantDetailCell: UITableViewCell, DGPaymentDetailCellBase {
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var markdownView: MarkdownView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var overlayerView: UIView!
    
    private var needLoadMakdown: Bool = true
    public var onHeightChange: (()->Void)? = nil
    
    var height: CGFloat = 91
    weak var delegate: DGImportantDetailCellDelegate?

    func setup(data: DGPaymentDetailsBase) {
        guard let data = data as? DGPaymentImportantDetailItem else {
            return
        }
        
        markdownView.isScrollEnabled = false
        
        if needLoadMakdown {
            markdownView.load(markdown: data.text)
            needLoadMakdown = false
        }
        
        markdownView.onRendered = { [weak self] height in
            self?.height = height + 20
            self?.onHeightChange?()
            self?.activityIndicator.stopAnimating()
            self?.overlayerView.isHidden = true
        }
        
        markdownView.onTouchLink = { [weak self] request in
            guard let url = request.url else {
                return false
            }
            
            if let result = self?.delegate?.importantDetailCellOnTouchLink(url: url) {
                return result
            } else {
                return false
            }
        }
        
        markdownView.backgroundColor = .clear
        background.layer.cornerRadius = 4
        setupColors()
    }
    
    private func setupColors() {
        overlayerView.backgroundColor = Colors.groupedBackgroundTertiary.color
        background.backgroundColor = Colors.groupedBackgroundTertiary.color
        backgroundColor = Colors.backgroundPrimary.color
        contentView.backgroundColor = Colors.backgroundPrimary.color
    }
}
