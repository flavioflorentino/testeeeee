import Foundation
import AnalyticsModule

enum DGPaymentAnalytics: AnalyticsKeyProtocol{
    enum DGPaymentAnalyticsPrivacyOption: String {
        case `public`
        case `private`
    }

    enum DGPaymentAnalyticsOpt: String {
        case optin = "opt in"
        case optout = "opt out"
    }
    
    private struct Keys {
        static let sellerId = "seller_id"
        static let sellerName = "seller_name"
        static let errorTitle = "error_title"
        static let errorMessage = "error_message"
        static let errorCode = "error_code"
    }
    
    case paymentRequestSuccess(sellerId: String, sellerName: String)
    case paymentRequestError(sellerId: String, sellerName: String, errorTitle: String, errorCode: Int, errorMessage: String)
    case checkoutScreenViewed
    case checkoutTransactionPrivacyAccessed
    case checkoutTransactionPrivacyClicked(_ option: DGPaymentAnalyticsPrivacyOption)
    case checkoutPaymentMethodAccessed
    case useBalanceChanged(_ opt: DGPaymentAnalyticsOpt)
    case mainCreditCardChanged
    case addNewCardAccessed
    case installmentAccessed(_ value: Float)
    case installmentSelected(_ value: Float, _ installments: Int)
    
    var name: String{
        switch self {
        case .paymentRequestSuccess(let sellerId, _):
            return "DGPayment - Payment Request Success \(sellerId)"
        case .paymentRequestError(let sellerId,_ , _, _, _):
            return "DGPayment - Payment Request Error \(sellerId)"
        case .checkoutScreenViewed:
            return "Checkout Screen Viewed"
        case .checkoutTransactionPrivacyAccessed:
            return "Checkout Transaction Privacy Accessed"
        case .checkoutTransactionPrivacyClicked:
            return "Checkout Transaction Privacy Clicked"
        case .checkoutPaymentMethodAccessed:
            return "Checkout Payment Method Accessed"
        case .useBalanceChanged:
            return "Use Balance Changed"
        case .mainCreditCardChanged:
            return "Main Credit Card Changed"
        case .addNewCardAccessed:
            return "Add New Card Accessed"
        case .installmentAccessed:
            return "Installment Accessed"
        case .installmentSelected:
            return "Installment Selected"
        }
    }
    
    var properties: [String: Any] {
        switch self {
        case .paymentRequestSuccess(let sellerId, let sellerName):
            return [Keys.sellerId: sellerId, Keys.sellerName: sellerName]
            
        case .paymentRequestError(let sellerId, let sellerName, let errorTitle, let errorCode, let errorMessage):
            return [
                Keys.sellerId: sellerId,
                Keys.sellerName: sellerName,
                Keys.errorTitle: errorTitle,
                Keys.errorCode: errorCode,
                Keys.errorMessage: errorMessage
            ]
        case .checkoutScreenViewed:
            return ["origin": "digitalgoods"]
        case let .checkoutTransactionPrivacyClicked(option):
            return ["option": option.rawValue]
        case let .useBalanceChanged(opt):
            return [
                "action": opt,
                "origin": "checkout"
            ]
        case .mainCreditCardChanged,
             .addNewCardAccessed:
            return ["origin": "checkout"]
        case let .installmentSelected(value, installments):
            return [
                "value": value,
                "installments": installments
            ]
        default:
            return [:]
        }
    }
    
    var providers: [AnalyticsProvider] {
        return [.mixPanel, .appsFlyer]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
