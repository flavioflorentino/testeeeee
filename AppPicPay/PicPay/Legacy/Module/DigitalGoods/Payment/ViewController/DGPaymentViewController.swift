import AnalyticsModule
import Core
import FeatureFlag
import Foundation
import SwiftyJSON
import UI

protocol DGPaymentDelegate: AnyObject {
    func didFinishPayment()
    func didPaymentAppear(_ viewController: UIViewController)
    func didPerformPayment(
        with manager: PPPaymentManager,
        cvv: String?,
        additionalPayload: [String: Any],
        pin: String,
        biometry: Bool,
        privacyConfig: String,
        someErrorOccurred: Bool,
        completion: @escaping (WrapperResponse) -> Void
    )
}

extension DGPaymentDelegate {
    func didFinishPayment() { }
    func didPaymentAppear(_ viewController: UIViewController) { }
}

protocol DGPaymentReceiptDelegate: AnyObject {
    func didTapButtonReceiptOptInAlert(_ controller: DGPaymentViewController, popup: AlertPopupViewController, button: UIPPButton)
}

final class DGPaymentViewController: PPBaseViewController {
    private var coordinator: Coordinating?
    
    override var shouldAddBreadcrumb: Bool {
        return false
    }
    // MARK: IBOutlet
    @IBOutlet weak var sellerImage: UIImageView!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var toolbarContainer: UIView!
    @IBOutlet weak var infoTableView: UITableView! {
        didSet {
            infoTableView.delegate = self
            infoTableView.dataSource = self
            infoTableView.isScrollEnabled = false
            infoTableView.allowsSelection = false
        }
    }
    @IBOutlet weak var amountDescriptionLabel: UILabel!
    @IBOutlet weak var bottomDistance: NSLayoutConstraint!
    @IBOutlet weak var installmentButton: UIPPButton!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var enhancedInstallmentButton: UIPPButton!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: Delegate & Model
    weak var delegate: DGPaymentDelegate?
    weak var receiptDelegate: DGPaymentReceiptDelegate?
    var model: DGPaymentViewModel?
    
    // MARK: Private vars
    fileprivate let loader = Loader.getLoadingView(DefaultLocalizable.wait.text)
    
    // MARK: Detail cell Identifiers
    fileprivate let commonDetailIdentifier = "DGPaymentDetailsCell"
    fileprivate let longDetailIdentifier = "DGPaymentLongDetailCell"
    fileprivate let importantDetailIdentifier = "DGImportantDetailCell"
    
    // MARK: Authentication & Payment vars
    fileprivate var auth: PPAuth?
    fileprivate var toolbarController: PaymentToolbarController?
    fileprivate var someErrorOccurred = false
    
    // MARK: Callbacks
    var rightBarButtonAction: ((DGPaymentViewController) -> ())?
    var prepareForPayment: ((DGPaymentViewController, @escaping (() -> ())) -> ())?
    var importantDetailCell: DGImportantDetailCell?
    
    var receiptData: [String: JSON]?
    
    static func controllerFromStoryboard() -> DGPaymentViewController{
        return DGPaymentViewController.controllerFromStoryboard(.digitalGoodsPayment)
    }
    
    private lazy var installmentsTooltip: InstallmentsTooltip = {
        let toolbarView = toolbarController?.toolbar.view
        return InstallmentsTooltip(view: view, toolbarView: toolbarView)
    }()
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        registerCells()
        setupViewModel()
        setupViews()
        setupColors()
        setupContraints()        
    }
    
    private func registerCells() {
        var nibStore = UINib(nibName: "DGPaymentDetailsCell", bundle: Bundle.main)
        infoTableView.register(nibStore, forCellReuseIdentifier: commonDetailIdentifier)
        
        nibStore = UINib(nibName: "DGPaymentLongDetailCell", bundle: Bundle.main)
        infoTableView.register(nibStore, forCellReuseIdentifier: longDetailIdentifier)
        
        nibStore = UINib(nibName: "DGImportantDetailCell", bundle: Bundle.main)
        infoTableView.register(nibStore, forCellReuseIdentifier: importantDetailIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateInstallments()
        
        // Register the bottomDistance constraint to be variable according to the keyboard state
        KeyboardManager.shared.addConstraint(bottomDistance, parentView: view)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updatePopupInstallments()
        delegate?.didPaymentAppear(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Register the bottomDistance constraint to be variable according to the keyboard state
        KeyboardManager.shared.removeConstraint(for: view)
        stopLoading()
    }
    
    // MARK: Setup
    
    // MUST be called after initialization
    func setup(paymentManager: PPPaymentManager, model: DGPaymentViewModel, delegate: DGPaymentDelegate? = nil) {
        self.model = model
        self.delegate = delegate
        toolbarController = PaymentToolbarController(paymentManager: paymentManager, parent: self, pay: preparePayment)
    }
    
    // Set (i) button in the navigation bar
    func setRightButton() {
        let infoButton = UIButton(type: .infoLight)
        infoButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: infoButton)
    }
    
    private func setupViewModel() {
        model?.detailsUpdate = { [weak self] in
            self?.infoTableView.reloadData()
            self?.amountTextField.text = self?.model?.stringAmount()
        }
    }
    
    private func setupColors() {
        view.backgroundColor = Colors.backgroundPrimary.color
        toolbarContainer.backgroundColor = Colors.backgroundPrimary.color
        infoTableView.backgroundColor = Colors.backgroundPrimary.color
        
        separatorView.backgroundColor = Palette.ppColorGrayscale200.color
                
        amountTextField.superview?.backgroundColor = Colors.backgroundPrimary.color
        amountDescriptionLabel.textColor = Palette.ppColorGrayscale600.color
        amountTextField.textColor = Palette.ppColorGrayscale600.color
    }
    
    @objc fileprivate func rightButtonAction() {
        rightBarButtonAction?(self)
    }
    
    fileprivate func setupViews() {
        // TODO: This might need to become editable?
        amountTextField.isUserInteractionEnabled = false
        self.amountTextField.text = model?.stringAmount()
        
        self.amountDescriptionLabel.text = model?.sellerName ?? ""
        
        if let toolbar = toolbarController?.toolbar {
            self.toolbarContainer.addSubview(toolbar)
        }
        
        installmentButton.borderWidth = 1
        installmentButton.cornerRadius = 6
        installmentButton.highlightedBackgrounColor = .clear
        updateInstallments()
        
        if let urlString = model?.sellerImageUrl {
            self.sellerImage.setImage(url: URL(string: urlString), placeholder: PPStore.photoPlaceholder())
        }
        
        if let image = model?.sellerImage {
            self.sellerImage.cancelRequest()
            self.sellerImage.image = image
        }
        
        if let loader = loader {
            navigationController?.view.addSubview(loader)
        }
        
        if self.hasSafeAreaInsets {
            self.paintSafeAreaBottomInset(withColor: self.toolbarController?.bottomBarBackgroundColor ?? .clear)
        }
    }
    
    fileprivate func setupContraints() {
        toolbarController?.toolbar.snp.makeConstraints({ make in
            make.edges.equalToSuperview()
        })
    }
    
    fileprivate func updatePopupInstallments() {
        guard let paymentManager = model?.paymentManager else {
            return
        }
        installmentsTooltip.show(
            view: view,
            from: FeatureManager.isActive(.featureInstallmentNewButton) ? enhancedInstallmentButton : installmentButton,
            cardValue: paymentManager.cardTotal()
        )
    }
    
    fileprivate func updateInstallments() {
        if FeatureManager.isActive(.featureInstallmentNewButton) {
            headerViewHeightConstraint.constant = 130
            installmentButton.isHidden = true
            configureInstallmentButton(installmentButton: enhancedInstallmentButton) {
                enhancedInstallmentButton.setTitle(
                    String(
                        format: PaymentLocalizable.newInstallmentButtonTitle.text,
                        model?.paymentManager?.selectedQuotaQuantity ?? 0),
                    for: .normal
                )
                enhancedInstallmentButton.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            }
        } else {
            headerViewHeightConstraint.constant = 80
            enhancedInstallmentButton.isHidden = true
            configureInstallmentButton(installmentButton: installmentButton) {
                installmentButton.setTitle("\(model?.paymentManager?.selectedQuotaQuantity ?? 0)x", for: .normal)
            }
        }
    }
    
    private func configureInstallmentButton(installmentButton: UIPPButton, completion: () -> Void) {
        guard let paymentManager = model?.paymentManager else {
            return
        }
        if paymentManager.selectedQuotaQuantity > paymentManager.installmentsList().count {
            if paymentManager.installmentsList().count > 0 {
                model?.paymentManager?.selectedQuotaQuantity = paymentManager.installmentsList().count
            } else {
                model?.paymentManager?.selectedQuotaQuantity = 1
            }
        }
        
        if paymentManager.cardTotal().doubleValue > 0 && paymentManager.total() != 0.0 {
            installmentButton.borderColor = Palette.ppColorBranding300.color
            installmentButton.normalTitleColor = Palette.ppColorBranding300.color
        } else {
            installmentButton.borderColor = .lightGray
            installmentButton.normalTitleColor = .lightGray
        }
        
        completion()
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // MARK: Actions
    @IBAction private func didTapInstallment(_ sender: Any?) {
        guard let paymentManager = model?.paymentManager,
            let sellerId = model?.sellerId else {
                return
        }
        
        guard paymentManager.cardTotal().doubleValue > 0 else {
            AlertMessage.showAlert(withMessage: BilletLocalizable.installmentsAvailable.text, controller: self)
            return
        }
        
        installmentsTooltip.blockTooltip = true
        
        let viewModel = InstallmentsSelectorViewModel(paymentManager: paymentManager,
                                                      payeeId: nil,
                                                      sellerId: sellerId,
                                                      origin: nil,
                                                      titleHeader: nil)
        viewModel.delegate = self
        let controller = InstallmentsSelectorViewController(with: viewModel)
        navigationController?.pushViewController(controller, animated: true)
        model?.trackInstallmentAccessed()
    }
    
    func startLoading() {
        loader?.removeFromSuperview()
        if let loader = loader {
            navigationController?.view.addSubview(loader)
        }
        loader?.isHidden = false
        navigationController?.view.isUserInteractionEnabled = false
    }
    
    func stopLoading() {
        loader?.isHidden = true
        navigationController?.view.isUserInteractionEnabled = true
    }
    
    // MARK: Handle success payments
    fileprivate func paymentSuccess(response: BaseApiGenericResponse) {
        let dataJson = response.json.dictionary?["data"]?.dictionary
        
        guard let data = dataJson, data["receipt"]?.array != nil else {
            someErrorOccurred = true
            AlertMessage.showAlert(withMessage: BilletLocalizable.thereWasAnError.text, controller: self)
            return
        }
        
        receiptData = data
        
        if response.json["data"]["receipt_opt_in"]["alert"].exists(),
            let alert = Alert(json: response.json["data"]["receipt_opt_in"]["alert"]) {
            AlertMessage.showAlert(alert, controller: self) { [weak self] popup, _, button in
                
                if let receiptDelegate = self?.receiptDelegate, let strongSelf = self {
                    receiptDelegate.didTapButtonReceiptOptInAlert(strongSelf, popup: popup, button: button)
                } else {
                    popup.dismiss(animated: true, completion: {
                        self?.showReceipt()
                    })
                }
            }
            return
        }
        
        delegate?.didFinishPayment()
        showReceipt()
    }
    
    public func showReceipt() {
        guard
            let data = receiptData,
            let receipt = data["receipt"]?.array else {
            AlertMessage.showAlert(withMessage: DefaultLocalizable.receiptError.text, controller: self)
            return
        }
        
        var optionalId: String?
        var optionalValue: String?
        
        if let id = data["id"]?.string {
            optionalId = id
        }
        
        if let transaction = data["Transaction"]?.dictionary,
            let id = transaction["id"]?.string,
            let value = transaction["credit_picpay"]?.string {
            optionalId = id
            optionalValue = value
        }
        
        guard let id = optionalId else {
            someErrorOccurred = true
            AlertMessage.showAlert(withMessage: DefaultLocalizable.requestError.text, controller: self)
            return
        }
        
        HapticFeedback.notificationFeedbackSuccess()
        
        let transactionValue = Decimal(string: optionalValue ?? "0.0") ?? 0.0
        model?.trackShowReceipt(id, transactionValue)
        
        let widgets: [ReceiptWidgetItem] = WSReceipt.createReceiptWidgetItem(receipt)
        
        let receiptModel = ReceiptWidgetViewModel(transactionId: id, type: .PAV)
        
        if widgets.count > 0 {
            receiptModel.setReceiptWidgets(items: widgets)
        }
        
        NotificationCenter.default.post(name: Notification.Name.Payment.new,
                                        object: nil,
                                        userInfo: ["type": "Digital Goods"])
        
        TransactionReceipt.showReceiptSuccess(viewController: self, receiptViewModel: receiptModel)
        
    }
    
    // Ask for authentication
    fileprivate func preparePayment(privacyConfig: String) {
        // Check if the presenter added a "prepareForPayment" block and calls it if needed
        if let prepareForPaymentBlock = prepareForPayment {
            prepareForPaymentBlock(self, { [weak self] in
                self?.executePayment(privacyConfig: privacyConfig)
            })
        } else {
            executePayment(privacyConfig: privacyConfig)
        }
    }
    
    fileprivate func executePayment(privacyConfig: String) {
        auth = PPAuth.authenticate({ [weak self] (authToken, biometry) in
            guard let self = self, let authToken = authToken else {
                return
            }
            
            DispatchQueue.main.async {
                self.startLoading()
            }
            
            self.notifyOfPayment(authToken: authToken, biometry: biometry, privacyConfig: privacyConfig)
        }, canceledByUserBlock: {
            
        })
    }
    
    private func createCvvFlowCoordinator(completedCvv: @escaping (String) -> Void, completedNoCvv: @escaping () -> Void) {
        guard
            let navigationController = navigationController,
            let viewModel = model
            else {
                return
        }
        
        let coordinator = CvvRegisterFlowCoordinator(
            navigationController: navigationController,
            paymentType: viewModel.type,
            finishedCvv: completedCvv,
            finishedWithoutCvv: completedNoCvv
        )
        coordinator.start()
       
        self.coordinator = coordinator
    }
    
    // Notify delegates of payment request
    fileprivate func notifyOfPayment(authToken: String, biometry: Bool, privacyConfig: String) {
        guard let needCvv = model?.checkNeedCvv(), needCvv else {
            model?.informedCvv = nil
            makePayment(authToken: authToken, biometry: biometry, privacyConfig: privacyConfig)
            return
        }
        
        createCvvFlowCoordinator(completedCvv: { [weak self] cvv in
            self?.model?.informedCvv = cvv
            self?.makePayment(authToken: authToken, biometry: biometry, privacyConfig: privacyConfig)
        }, completedNoCvv: { [weak self] in
            self?.showCvvError()
        })
    }
    
    private func showCvvError() {
        stopLoading()
        let error = PicPayError(message: DefaultLocalizable.cvvNotInformed.text)
        AlertMessage.showCustomAlertWithError(error, controller: self)
    }
    
    private func makePayment(authToken: String, biometry: Bool, privacyConfig: String) {
        guard let paymentManager = model?.paymentManager,
            let payload = model?.additionalPayload else {
                return
        }
        
        delegate?.didPerformPayment(
            with: paymentManager,
            cvv: model?.informedCvv,
            additionalPayload: payload,
            pin: authToken,
            biometry: biometry,
            privacyConfig: privacyConfig,
            someErrorOccurred: someErrorOccurred,
            completion: { [weak self] result in
            DispatchQueue.main.async {
                self?.stopLoading()
                switch(result) {
                case .success(let response):
                    if let sellerId = self?.model?.sellerId, let sellerName = self?.model?.sellerName {
                        Analytics.shared.log(DGPaymentAnalytics.paymentRequestSuccess(sellerId: sellerId, sellerName: sellerName))
                    }
                    
                    self?.model?.saveCvv()
                    self?.paymentSuccess(response: response)
                    
                    
                case .failure(let error):
                    if let sellerId = self?.model?.sellerId, let sellerName = self?.model?.sellerName {
                        Analytics.shared.log(DGPaymentAnalytics.paymentRequestError(sellerId: sellerId, sellerName: sellerName, errorTitle: error.title, errorCode: error.code, errorMessage: error.message))
                    }
                    
                    self?.someErrorOccurred = true // Duplicated
                    self?.auth?.handleError(error, callback: { error in
                        AlertMessage.showCustomAlertWithErrorUI(error, controller: self)
                    })
                }
            }
        })
    }
}

// MARK: - Installment selector view model delegate
extension DGPaymentViewController: InstallmentsSelectorViewModelDelegate {
    func didSelectedInstallment(_ paymentManager: PPPaymentManager) {
        updateInstallments()
        model?.trackInstallmentSelected()
        NotificationCenter.default.post(name: Notification.Name.Payment.installmentChange, object: nil)
    }
}

// MARK: Details table view delegate & data source
extension DGPaymentViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension DGPaymentViewController: UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let model = model else { return 0 }
        return model.detailInfo.count
    }
    
    // Ask the cell for it's size
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let cell = getCellFor(tableView, indexPath: indexPath) as? DGPaymentDetailCellBase else { return  20 }
        return cell.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getCellFor(tableView, indexPath: indexPath)
    }
    
    // Returns the cell for a given indexPath
    private func getCellFor(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let detailInfo = model?.detailInfo else {
            return UITableViewCell()
        }
        let detailItem = detailInfo[indexPath.row]
        
        switch detailItem.type {
        case .common:
            return getCell(class: DGPaymentDetailsCell.self,
                           detailType: DGPaymentDetailsItem.self,
                           detailItem: detailItem,
                           identifier: commonDetailIdentifier) ?? UITableViewCell()
        case .longText:
            return getCell(class: DGPaymentLongDetailCell.self,
                           detailType: DGPaymentLongDetailItem.self,
                           detailItem: detailItem,
                           identifier: longDetailIdentifier) ?? UITableViewCell()
        case .important:
            if importantDetailCell == nil {
                let cell = getCell(class: DGImportantDetailCell.self,
                                   detailType: DGPaymentImportantDetailItem.self,
                                   detailItem: detailItem,
                                   identifier: importantDetailIdentifier) ?? UITableViewCell() as! DGImportantDetailCell
                cell.onHeightChange = { [weak self] in
                    self?.infoTableView.reloadData()
                }
                cell.delegate = self
                importantDetailCell = cell
            }
            return importantDetailCell!
        }
    }
    
    // Return a ready to use cell (or nil) that has the same type as DetailCell and populated with detailItem's (of type DetailModel) content
    private func getCell
        <DetailCell: DGPaymentDetailCellBase, // DetailCell is a Cell that conforms to DGPaymentDetailCellBase
        DetailModel: DGPaymentDetailsBase> // DetailModel is a subclass of DGPaymentDetailsBase
        (class: DetailCell.Type, // The cell class
        detailType: DetailModel.Type, // The detail class
        detailItem: DGPaymentDetailsBase, // The item
        identifier: String) -> DetailCell? { // the cell identifier
        
        guard
            let cell = infoTableView.dequeueReusableCell(withIdentifier: identifier) as? DetailCell,
            let detailInfo = detailItem as? DetailModel
            else {
                return nil
        }
        
        cell.setup(data: detailInfo)
        
        return cell
    }
}

extension DGPaymentViewController : DGImportantDetailCellDelegate {
    func importantDetailCellOnTouchLink(url: URL) -> Bool {
        //UIApplication.shared.openURL(url)
        ViewsManager.presentSafariViewController(url, from: self)
        return false
    }
}
