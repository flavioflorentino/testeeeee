//
//  DGPaymentDetailsBase.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 24/10/17.
//

import Foundation

enum DGPaymentDetailType {
    case common
    case longText
    case important
}

class DGPaymentDetailsBase: NSObject {
    var type: DGPaymentDetailType
    
    init(type: DGPaymentDetailType) {
        self.type = type
        super.init()
    }
}
