import UI
import UIKit

final class DGPaymentDetailsItem: DGPaymentDetailsBase {
    var name = ""
    var detail = ""
    var detailColor: UIColor
    var showInfo = false
    var backgroundColor: UIColor
    var action: (() ->())?
    
    init?(
        name: String?,
        detail: String?,
        detailColor: UIColor = Colors.grayscale400.color,
        showInfo: Bool = false,
        backgroundColor: UIColor = Colors.groupedBackgroundSecondary.color,
        action: (() -> ())? = nil
    ) {
        guard
            let name = name,
            let detail = detail
            else {
                return nil
        }
        
        self.name = name
        self.detail = detail
        self.detailColor = detailColor
        self.showInfo = showInfo
        self.backgroundColor = backgroundColor
        self.action = action
        super.init(type: .common)
    }
    
    init(
        name: String,
        detail: String,
        detailColor: UIColor = Colors.grayscale400.color,
        showInfo: Bool = false,
        backgroundColor: UIColor = Colors.groupedBackgroundSecondary.color,
        action: (() -> ())? = nil
    ) {
        self.name = name
        self.detail = detail
        self.detailColor = detailColor
        self.showInfo = showInfo
        self.backgroundColor = backgroundColor
        self.action = action
        super.init(type: .common)
    }
}
