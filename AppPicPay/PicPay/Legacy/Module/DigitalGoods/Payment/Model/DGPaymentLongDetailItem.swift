//
//  DGPaymentLongDetailItem.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 24/10/17.
//

import Foundation

/*
 * Payment detail model for cell of type
 *
 * Long text....... Really long text
 * Reeaallly long text ......
 * ....
 *
 */
final class DGPaymentLongDetailItem: DGPaymentDetailsBase {
    var text = ""
    var attributedText = ""
    
    init(text: String) {
        self.text = text
        super.init(type: .longText)
    }
    
    init(attrText: String) {
        self.attributedText = attrText
        super.init(type: .longText)
    }
}
