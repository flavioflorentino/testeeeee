//
//  DGPaymentImportantDetailItem.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 26/10/17.
//

import Foundation
//import MarkdownKit

/*
 * Payment model for the cell of type important (gray background and a text)
 */
final class DGPaymentImportantDetailItem: DGPaymentDetailsBase {
    var text: String?

    init(text: String) {
        self.text = text
//        let markdownParser = MarkdownParser()
//        self.text = markdownParser.parse(text)
        super.init(type: .important)
    }
}
