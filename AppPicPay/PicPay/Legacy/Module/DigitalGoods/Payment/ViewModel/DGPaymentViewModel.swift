import AnalyticsModule
import CoreLegacy
import Core
import FeatureFlag
import Foundation

enum DGPaymentModelAnalytics {
    case showReceipt(_ id: String, _ value: Decimal)
    case installmentAccessed
    case installmentSelected
}

final class DGPaymentViewModel: NSObject {
    typealias ValueInstallmentTuple = (value: String, installments: String)
    typealias Dependencies = HasFeatureManager & HasAnalytics & HasKeychainManager
    private let dependencies: Dependencies
    private var isInvertedInstallmentActive: Bool {
        dependencies.featureManager.isActive(.experimentInvertedInstallment)
    }
    
    let type: CvvRegisterFlowCoordinator.PaymentType
    var paymentType: String? // Only used on tracking events
    var amount: Double
    var sellerImageUrl: String?
    var sellerImage: UIImage?
    var sellerId: String
    var informedCvv: String?
    var paymentManager = PPPaymentManager()
    var detailInfo: [DGPaymentDetailsBase] = [] {
        didSet {
            detailsUpdate?()
        }
    }
    var additionalPayload: [String: Any] = [:]
    var sellerName = ""
    
    var detailsUpdate: (() -> ())?
    
    init(
        amount: Double,
        type: CvvRegisterFlowCoordinator.PaymentType,
        sellerName: String,
        sellerImageUrl: String? = nil,
        sellerImage: UIImage? = nil,
        sellerId: String,
        detailInfo: [DGPaymentDetailsBase]? = nil,
        dependencies: Dependencies
    ) {
        self.amount = amount
        self.type = type
        self.sellerImageUrl = sellerImageUrl
        self.sellerImage = sellerImage
        self.sellerName = sellerName
        self.detailInfo = detailInfo ?? []
        self.sellerId = sellerId
        self.dependencies = dependencies
        super.init()
        
        self.paymentManager?.subtotal = NSDecimalNumber(value: amount)
        guard let origin = CheckoutEvent.CheckoutEventOrigin(rawValue: type.rawValue) else {
            return
        }
        dependencies.analytics.log(CheckoutEvent.checkoutScreenViewed(origin))
    }
    
    func stringAmount() -> String {
        return CurrencyFormatter.brazillianRealString(from: NSNumber(value: amount))
    }
    
    func checkNeedCvv() -> Bool {
        guard
            let cardBank = CreditCardManager.shared.defaultCreditCard,
            let cardValue = paymentManager?.cardTotal()?.doubleValue
            else {
                return false
            }
            
        return CvvRegisterFlowCoordinator.cvvFlowEnable(cardBank: cardBank, cardValue: cardValue, paymentType: type)
    }
    
    func saveCvv() {
        guard let cvv = informedCvv else {
            return
        }
        
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        dependencies.keychain.set(key: KeychainKeyPF.cvv(id), value: cvv)
    }
    
    func trackShowReceipt(_ id: String, _ value: Decimal) {
        dependencies.analytics.log(PaymentEvent.transaction(.digitalGoods, id: id, value: value))
    }
    
    func trackInstallmentAccessed() {
        let data = getValueAndInstallments()
        dependencies.analytics.log(InstallmentButtonEvent.installmentAccessed(data.value, isInvertedInstallmentActive))
    }
    
    func trackInstallmentSelected() {
        let data = getValueAndInstallments()
        dependencies.analytics.log(InstallmentButtonEvent.installmentSelected(data.installments, data.value, isInvertedInstallmentActive))
    }
    
    private func getValueAndInstallments() -> ValueInstallmentTuple {
        guard let manager = paymentManager,
              let value = (manager.subtotal as? Double)?.toString(),
              let installments = manager.selectedQuotaQuantity.toString() else {
                return ("","")
        }
        return (value: value, installments: installments)
    }
}
