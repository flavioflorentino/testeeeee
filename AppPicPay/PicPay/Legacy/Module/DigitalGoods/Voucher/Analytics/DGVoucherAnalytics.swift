import Foundation
import AnalyticsModule

enum DGVoucherAnalytics: AnalyticsKeyProtocol {
    private struct Keys {
        static let sellerId = "seller_id"
        static let digitalGoodName = "digital_good_name"
        static let product_name = "product_name"
    }
    
    case selectecItem(sellerId: String, digitalGoodName: String, productName: String)
    
    var name : String{
        return "DGVoucher - Selected Item"
    }
    
    var properties: [String: Any] {
        switch self {
        case .selectecItem(let sellerId, let digitalGoodName, let productName):
            return [Keys.sellerId: sellerId, Keys.digitalGoodName: digitalGoodName, Keys.product_name: productName]
        }
    }
    
    var providers: [AnalyticsProvider] {
        return [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
