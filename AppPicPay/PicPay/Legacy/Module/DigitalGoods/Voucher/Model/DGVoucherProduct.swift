//
//  DGVoucherProduct.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 20/10/17.
//

import Foundation

protocol DGProduct {
    var id: String { get set }
    var name: String { get set }
    var description: String? { get set }
    var disclaimerMarkdown: String? { get set }
    var hidePrice: Bool { get set }
    var price: Double { get }
    var imageUrl: String? { get set }
    var img: UIImage? { get set }
    
    // For ranged type
    var isRangeType: Bool { get }
    var valueMax: Double { get set }
    var valueMin: Double { get set }
    var customAmount: Double? { get set }
}

// Digital code product
struct DGVoucherResponse: Decodable {
    let data: [DGVoucherProduct]
    let service: DGInfo
}

final class DGVoucherProduct: Decodable, DGProduct {
    var id: String
    var name: String
    var description: String?
    var disclaimerMarkdown: String?
    var valueMax: Double = 0.0
    var valueMin: Double = 0.0
    var customAmount: Double?
    var imageUrl: String?
    var img: UIImage?
    
    var hidePrice: Bool = false

    // Can the user enter a custom value?
    var isRangeType: Bool {
        return valueMax != valueMin
    }
    
    var price: Double {
        // If it's possible to enter a custom value, we check the variable
        // Otherwise we send min (could also be max as they SHOULD be the same for fixed value products)
        return isRangeType ? customAmount ?? 0.0 : valueMin
    }
    
    init(id: String, title: String, hidePrice: Bool, img: UIImage? = nil, imageUrl: String? = nil, raw: Any? = nil) {
        self.name = title
        self.hidePrice = hidePrice
        self.img = img
        self.imageUrl = imageUrl
        self.id = id
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case description
        case disclaimerMarkdown
        case valueMax
        case valueMin
        case imageUrl
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        description = try container.decodeIfPresent(String.self, forKey: .description)
        disclaimerMarkdown = try container.decodeIfPresent(String.self, forKey: .disclaimerMarkdown)
        imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl)
        valueMax = try container.decodeIfPresent(Double.self, forKey: .valueMax) ?? 0.0
        valueMin = try container.decodeIfPresent(Double.self, forKey: .valueMin) ?? 0.0
    }
}
