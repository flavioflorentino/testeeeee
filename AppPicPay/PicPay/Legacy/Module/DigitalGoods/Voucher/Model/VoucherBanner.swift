import UIKit

protocol DGBanner {
    var type: VoucherBannerType { get set }
    var title: String { get set }
    var description: String { get set }
    var image: UIImage { get set }
    var actionTitle: String { get set }
    var actionLink: String { get set }
}

enum VoucherBannerType {
    case howPayBillet
}

struct VoucherBanner: DGBanner {
    var type: VoucherBannerType
    
    var title: String
    
    var description: String
    
    var image: UIImage
    
    var actionTitle: String
    
    var actionLink: String
}
