//
//  DGVoucherListItem.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 01/11/17.
//

import Foundation


enum DGVoucherListItemType {
    case defaultCell
    case product
    case banner
    case grid
    case longText
    case longAttrText
    case header
    case title
    case centeredTitle
    case clearBgCell
}

struct DGVoucherListItem {
    let type: DGVoucherListItemType
    var data: String?
    var markdownText: String?
    var product: DGProduct?
    var banner: DGBanner?
    var height: CGFloat?
    
    init(type: DGVoucherListItemType, product: DGProduct? = nil, banner: DGBanner? = nil, data: String? = "", markdownText: String? = nil) {
        self.type = type
        self.product = product
        self.banner = banner
        self.data = data
        self.markdownText = markdownText
    }

    // Height for each cell
    // These are default values, some cells like "grid" are
    // dynamic and might bypass this function
    func getHeight() -> CGFloat {
        switch type {
        case .defaultCell:
            return 40
        case .clearBgCell:
            return 65
        case .product:
            return 88
        case .banner:
            return 200
        case .longText:
            return 150
        case .longAttrText:
            return 300
        case .header:
            return 160
        case .title:
            return 44
        case .centeredTitle:
            return 70
        case .grid:
            return 500
        }
    }
}
