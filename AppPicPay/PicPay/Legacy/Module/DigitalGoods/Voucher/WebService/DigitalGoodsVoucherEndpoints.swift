import Core
import Foundation

enum DigitalGoodsVoucherEndpoints: ApiEndpointExposable {
    case products(serviceId: String, dgType: String)
    case productsV2(serviceId: String, dgType: String)
    
    var path: String {
        switch self {
        case let .products(_, dgType):
            return "digitalgoods/\(dgType)/products"
        case let .productsV2(serviceId, dgType):
            return "digitalgoods/v2/\(dgType)/\(serviceId)/products"
        }
    }
    
    var parameters: [String : Any] {
        if case let .products(serviceId, _) = self {
            return ["service_id": serviceId]
        }
        return [:]
    }
}
