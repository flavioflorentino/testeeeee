import Core
import FeatureFlag
import Foundation

class DGVoucherApiRequest: DGPayRequest {
    // Variable params
    var digitalGoodId: String?
    var productId: String?
    
    override func toParams() throws -> [String : Any] {
        let params = try super.toParams()
        
        let optionalParamsToBeMerged = [
            "id_digitalgoods": digitalGoodId,
            "digitalgoods": [
                "type": "digitalcodes",
                "product_id": productId
            ]
        ] as [String : Any?]
        
        let paramsToBeMerged = try obligatoryParams(params: optionalParamsToBeMerged)
        
        // Merge super's params with this class' specific params overriding any duplicated entries
        return params.merging(paramsToBeMerged, uniquingKeysWith: { (_, new) in new })
    }
}

final class DGVoucherApi: BaseApi {
    typealias Dependencies = HasFeatureManager & HasMainQueue
    private let dependencies: Dependencies
    var digitalGoodType = "digitalcodes"
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func products(serviceId: String, _ completion: @escaping ((PicPayResult<[DGVoucherProduct]>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
            let params = ["service_id": serviceId]
            requestManager
                .apiRequest(endpoint: "digitalgoods/\(digitalGoodType)/products", method: .get, parameters: params)
                .responseApiCodable(completionHandler: completion)
            return
        }
        
        // Core network version
        let endpoint = DigitalGoodsVoucherEndpoints.products(serviceId: serviceId, dgType: digitalGoodType)
        let encoder = JSONDecoder(.convertFromSnakeCase)
        Api<[DGVoucherProduct]>(endpoint: endpoint).execute(jsonDecoder: encoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case let .success((products, _)):
                    completion(.success(products))
                case let .failure(error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    func getProductsWithDGInfo(serviceId: String, _ completion: @escaping ((PicPayResult<DGVoucherResponse>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
            requestManager
                .apiRequest(endpoint: "digitalgoods/v2/\(digitalGoodType)/\(serviceId)/products", method: .get)
                .responseApiCodable { [weak self] (result: Result<DGVoucherResponse, PicPayError>) in
                    self?.dependencies.mainQueue.async {
                        switch result {
                        case .success(let response):
                            completion(.success(response))
                        case .failure(let error):
                            completion(.failure(error))
                        }
                    }
                }
            return
        }
        
        // Core network version
        let endpoint = DigitalGoodsVoucherEndpoints.productsV2(serviceId: serviceId, dgType: digitalGoodType)
        Api<DGVoucherResponse>(endpoint: endpoint).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case let .success((dgVoucher, _)):
                    completion(.success(dgVoucher))
                case let .failure(error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    func pay(request: DGVoucherApiRequest, pin: String, _ completion: @escaping ((WrapperResponse) -> Void)) {
        // execute core network in other class
        DigitalGoodsApi().pay(request: request, pin: pin, completion)
    }
}
