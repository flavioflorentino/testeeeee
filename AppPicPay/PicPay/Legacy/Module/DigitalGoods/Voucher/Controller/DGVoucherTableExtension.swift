//
//  DGVoucherListTableView.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 01/11/17.
//

import Foundation
import AnalyticsModule

protocol SelectableCell: AnyObject {
    var isCellSelected: Bool? { get set }
}

/*
 *
 * CELL CREATION FUNCTIONS
 *
 */

extension DGVoucherController {
    func createDefaultCell(tableView: UITableView, item: DGVoucherListItem) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: defaultCellIdentifier)
        cell?.textLabel?.text = item.data?.uppercased()
        if let font = listSectionFont {
            cell?.textLabel?.font = font
            cell?.textLabel?.numberOfLines = 2
        }else{
            cell?.textLabel?.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        }
        cell?.backgroundColor = view.backgroundColor
        cell?.textLabel?.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1)
        cell?.selectionStyle = .none
        return cell ?? UITableViewCell()
    }

    func createLongTextCell(tableView: UITableView, item: DGVoucherListItem) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: longCellIdentifier) as? DGVoucherListTextCell else {
                return UITableViewCell()
            }
        cell.selectionStyle = .none
        cell.label.text = item.data?.uppercased()
        cell.label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        cell.label.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1)
        cell.backgroundColor = view.backgroundColor
        return cell
    }

    func createViewCell(tableView: UITableView, item: DGVoucherListItem) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: viewCellIdentifier) as? DGVoucherMarkdownCell else {
                return UITableViewCell()
            }

        cell.setup(markdownText: item.markdownText) { [weak self] height in
            self?.aboutViewHeight = height
            self?.canAnimateTopLabel = false
            self?.tableView.reloadData()
            self?.canAnimateTopLabel = true
        }
        cell.delegate = self

        return cell
    }

    func createHeaderCell(tableView: UITableView) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: defaultCellIdentifier)
        cell?.backgroundColor = .clear
        cell?.textLabel?.text = ""
        cell?.selectionStyle = .none
        return cell ?? UITableViewCell()
    }

    func createProductCell(tableView: UITableView, item: DGVoucherListItem) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? DGVoucherListCell,
            let digitalGood = model.digitalGood,
            let product = item.product else {
                return UITableViewCell()
            }

        cell.configureCell(product: product, imgUrl: digitalGood.imgUrl)
        cell.backgroundColor = view.backgroundColor
        return cell
    }
    
    func createBannerCell(tableView: UITableView, item: DGVoucherListItem) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: bannerCellIdentifier) as? BannerListCell,
            let banner = item.banner
            else {
                return UITableViewCell()
        }

        cell.configureCell(banner: banner, delegate: self)
        cell.backgroundColor = view.backgroundColor
        return cell
    }

    func createGridCell(tableView: UITableView) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: gridCellIdentifier) as? DGVoucherGridTableCell else {
                return UITableViewCell()
            }

        cell.setup(delegate: self, dataSource: self, count: model.products.count)
        cell.circleView.registerCell(name: String(describing: DGVoucherValueCell.self), cellIdentifier: gridValueIdentifier)
        cell.circleView.registerCell(name: String(describing: DGVoucherOtherValueCell.self), cellIdentifier: gridOtherValueIdentifier)
        cell.backgroundColor = view.backgroundColor
        cell.selectionStyle = .none
        cell.circleView.backgroundColor = view.backgroundColor

        return cell
    }
    
    func createClearBgCell(tableView: UITableView, item: DGVoucherListItem) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: clearBgCellIdentifier) as? DGClearBackgroundListCell else {
                return UITableViewCell()
            }
        cell.selectionStyle = .none
        if let product = item.product {
            cell.setup(product: product)
        }
        cell.backgroundColor = view.backgroundColor
        
        return cell
    }

    func createTitleCell(tableView: UITableView, item: DGVoucherListItem) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: titleCellIdentifier) as? DGVoucherListTitleCell else {
                return UITableViewCell()
            }
        cell.title.text = item.data
        cell.selectionStyle = .none
        cell.backgroundColor = view.backgroundColor
        if item.type == .centeredTitle {
            cell.title.textAlignment = .center
            cell.title.font = UIFont.boldSystemFont(ofSize: 18)
        }
        cell.setup(showButton: model.digitalGood?.infoUrl?.isNotEmpty ?? false && model.digitalGood?.infoUrl != "") { [weak self] in
            guard
                let infoUrl = self?.model.digitalGood?.infoUrl,
                let url = URL(string:infoUrl),
                let self = self
                else {
                    return
            }
            
            if self.model is DGBoletoViewModel {
                let event = BilletEvent.cardOnBillsOptionSelected(optionSelected: .knowMore)
                Analytics.shared.log(event)
            }
            
            ViewsManager.presentSafariViewController(url, from: self)
        }
        return cell
    }
}

/*
 *
 * DATA SOURCE
 *
 */
extension DGVoucherController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = tableViewStructure[indexPath.row]
        switch item.type {
        // Default UITableViewCell with some minor tweaks
        case .defaultCell:
            return createDefaultCell(tableView: tableView, item: item)
        // Long text cell, used for descriptions
        case .longText:
            return createLongTextCell(tableView: tableView, item: item)
        // Long text cell, used for descriptions with markdown
        case .longAttrText:
            return createViewCell(tableView: tableView, item: item)
        case .clearBgCell:
            return createClearBgCell(tableView: tableView, item: item)
        // Product cell
        case .product:
            return createProductCell(tableView: tableView, item: item)
        // Product cell
        case .banner:
            return createBannerCell(tableView: tableView, item: item)
        // Clear cell used to create the parallax effect
        case .header:
            return createHeaderCell(tableView: tableView)
        // Title cell (with the "Saiba mais" button)
        case .title, .centeredTitle:
            hideTopLabel()
            return createTitleCell(tableView: tableView, item: item)
        // Grid to select a value
        case .grid:
            return createGridCell(tableView: tableView)
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewStructure.count
    }
}

/*
 *
 * DELEGATE
 *
 */
extension DGVoucherController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = tableViewStructure[indexPath.row]

        if item.type == .grid,
            let cell = self.tableView(tableView, cellForRowAt: indexPath) as? DGVoucherGridTableCell{
            return cell.height
        }

        if item.type == .longText,
            let cell = self.tableView(tableView, cellForRowAt: indexPath) as? DGVoucherListTextCell{
            return cell.height
        }

        if item.type == .longAttrText {
            // At least 32
            return max(32, aboutViewHeight + 32)
        }
        
        if item.type == .title,
            let cell = self.tableView(tableView, cellForRowAt: indexPath) as? DGVoucherListTitleCell{
            return cell.height
        }

        return item.getHeight()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let product = tableViewStructure[indexPath.row].product,
            let cell = tableView.cellForRow(at: indexPath) else {
            return
        }
        
        if let cell = cell as? DGVoucherListCell {
            cell.animateSelection()
        } else if !cell.isKind(of: DGClearBackgroundListCell.self) {
            return
        }
        
        if let sellerId = model.digitalGood?.sellerId, let digitalGoodName = model.digitalGood?.name {
            Analytics.shared.log(DGVoucherAnalytics.selectecItem(sellerId: sellerId, digitalGoodName: digitalGoodName, productName: product.name))
        }
        
        model.product = product
        model.goToPayment(from: self)
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard indexPath.row < tableViewStructure.count, tableViewStructure[indexPath.row].type == .title, !tableView.isHidden else {
            return
        }
        showTopLabel()
    }

    // Top banner effect
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard !shouldHideBanner else {
            return
        }
        var paddingTop: CGFloat = 0.0
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            paddingTop = window?.safeAreaInsets.top ?? 0.0
        }
        if paddingTop == 0.0 {
            paddingTop = 22 // status bar
        }
        
        // Adjust the height of the banner to be without a minimum in the status bar along with navigation bar
        let constantBannerHeight = 200 - (scrollView.contentOffset.y * 0.5)
        let navigationBarHeight: CGFloat = 44
        if constantBannerHeight > (paddingTop + navigationBarHeight) {
            bannerImageHeight.constant = constantBannerHeight
        }

        // Disable blur on bounce and fix scrollview bug
        if (scrollView.contentOffset.y > -1 && scrollView.contentSize.height > view.frame.height + 100) {
            // Normalize content offset, apply some multiplier so it blurs faster than the user is scrolling and apply the result to the view's alpha
            blurView.alpha = (scrollView.contentOffset.y / (scrollView.contentSize.height - view.frame.height)) * 2
        }
    }
}

extension DGVoucherController: DGVoucherMarkdownCellDelegate {
    func voucherListOnTouchLink(url: URL) -> Bool {
        ViewsManager.presentSafariViewController(url, from: self)
        return false
    }
}

extension DGVoucherController: BannerListCellDelegate {
    func didTouchLink(url: URL) {
        let webViewController = WebViewFactory.make(with: url)
        let navController = PPNavigationController(rootViewController: webViewController)
        present(navController, animated: true)
        
        let event = BilletEvent.cardOnBillsOptionSelected(optionSelected: .howToPay)
        Analytics.shared.log(event)
    }
}
