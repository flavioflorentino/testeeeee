//
//  DGVoucherListGrid.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 01/11/17.
//

import Foundation

/*
 *
 * DELEGATE
 *
 */

extension DGVoucherController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product = model.products[indexPath.row]
        let cell = collectionView.cellForItem(at: indexPath)
            
        (cell as? SelectableCell)?.isCellSelected = true
        
        self.handleSelectedProduct(product: product)
    }
    
    func handleSelectedProduct(product: DGProduct) {
        self.model.product = product
        
        // Push the custom selection VC
        if product.isRangeType {
            let nextController = DGVoucherOtherValueController.controllerFromStoryboard(.digitalGoodsCode)
            nextController.setup(screenTitle: screenTitle, max: product.valueMax, min: product.valueMin, amountChange: { [weak self] amount in
                // Trigger model product value update
                let newProduct = self?.model.product as? DGVoucherProduct
                newProduct?.customAmount = amount
                self?.model.product = newProduct
                }, completion: { [weak self] vc in
                    self?.model.goToPayment(from: vc)
            })
            self.navigationController?.pushViewController(nextController, animated: true)
            return
        }
        
        // Go directly to payment
        self.model.goToPayment(from: self)
    }
}


/*
 *
 * DATA SOURCE
 *
 */
extension DGVoucherController: CircleViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let product = model.products[indexPath.row]
        
        // If it's a value
        if !product.isRangeType{
            // Dequeue the cell
            guard
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: gridValueIdentifier, for: indexPath) as? DGVoucherValueCell
                else { return UICollectionViewCell() }
            
            cell.configureCell(product: product)
            return cell
        }
        
        // It's not a fixed value
        return collectionView.dequeueReusableCell(withReuseIdentifier: gridOtherValueIdentifier, for: indexPath)
    }
}
