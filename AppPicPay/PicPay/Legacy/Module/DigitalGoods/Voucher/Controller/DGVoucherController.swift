import AnalyticsModule
import FeatureFlag
import Foundation
import UI
import UIKit

// MARK: - Layout
private extension DGVoucherController.Layout {
    enum Size {
        static let favoriteButton = CGSize(width: 96.0, height: 24.0)
    }
}

protocol DGVoucherControllerDataSource: AnyObject {
    func tableStructure(vc: DGVoucherController) -> [DGVoucherListItem]
}

final class DGVoucherController: PPBaseViewController {
    fileprivate enum Layout { }

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var banner: UIImageView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var bannerImageHeight: NSLayoutConstraint!
    @IBOutlet weak var bannerImageTop: NSLayoutConstraint!
    @IBOutlet weak var viewTop: NSLayoutConstraint!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var topLabelLeftMargin: NSLayoutConstraint!
    @IBOutlet weak var topLabelYCenter: NSLayoutConstraint!
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    @IBOutlet weak var bannerImageHeightLimit: NSLayoutConstraint!
    @IBOutlet weak var cancelButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var cancelButton: UIButton!

    private lazy var favoriteButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(Assets.Favorite.favoriteButtonFavorite.image, for: .normal)
        button.setImage(Assets.Favorite.favoriteButtonUnFavorite.image, for: .highlighted)
        button.setImage(Assets.Favorite.favoriteButtonUnFavorite.image, for: .selected)
        button.addTarget(self, action: #selector(didTapAtFavoriteButton), for: .touchUpInside)

        return button
    }()
    
    let cellIdentifier = "DGVoucherListCell"
    let defaultCellIdentifier = "defaultCellIdentifier"
    let longCellIdentifier = "longCellIdentifier"
    let viewCellIdentifier = "viewCellIdentifier"
    let titleCellIdentifier = "DGVoucherListTitleCell"
    let gridCellIdentifier = "DGVoucherGridTableCell"
    let gridValueIdentifier = "Value"
    let clearBgCellIdentifier = "DGClearBackgroundListCell"
    let gridOtherValueIdentifier = "OtherValue"
    let bannerCellIdentifier = "BannerListCellIdentifier"
    
    var aboutViewHeight: CGFloat = 0
    
    weak var dataSource: DGVoucherControllerDataSource?
    
    var tableViewStructure: [DGVoucherListItem] = []
    var screenTitle = ""
    var listSectionTitle = BilletLocalizable.chooseAnItem.text
    var listSectionFont: UIFont?
    var canAnimateTopLabel = true
    var shouldHideBanner = false
    var statusBarStyle = UIStatusBarStyle.lightContent
    
    var model: DGListViewModel = DGListViewModel()

    private lazy var favoriteService: FavoritesServicing = FavoritesService()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    /*
     * LIFE CYCLE
     */
    override func viewDidLoad() {
        setupViews()
        setupColors()
        tableView.estimatedRowHeight = 250
        tableView.rowHeight = UITableView.automaticDimension
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        handleFavoriteButtonVisibility()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.25) { [weak self] in
            self?.canAnimateTopLabel = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(!shouldHideBanner, animated: true)
        setNeedsStatusBarAppearanceUpdate()
        self.title = screenTitle
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = ""
        
        canAnimateTopLabel = false
    }
    
    private func setupColors() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
    }
    
    /*
     * SETUP FUNCTIONS
     */
    
    // Subscribe to the model's updates about products loading result
    private func setupCallbacks() {
        model.loadingUpdate = { [weak self] isLoading in
            self?.loadingIndicator.isHidden = !isLoading
        }
        
        model.productsUpdate = { [weak self] () in
            self?.setTableViewDataSource()
            self?.canAnimateTopLabel = false
            self?.tableView.reloadData()
            self?.canAnimateTopLabel = true
        }
        
        model.bannersUpdate = { [weak self] () in
            self?.setTableViewDataSource()
            self?.tableView.reloadData()
        }
        
        model.productsUpdateFailed = { [weak self] error in
            AlertMessage.showAlert(withMessage: error.localizedDescription, controller: self) { [weak self] in
                self?.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    // See cellForRowAtIndexPath for comments about the structure
    func setTableViewDataSource() {
        tableViewStructure = []
        
        // Give preference to the dataSource
        if let dataSource = dataSource {
            tableViewStructure = dataSource.tableStructure(vc: self)
            return
        }
        
        tableViewStructure.append(contentsOf: tableHeaderStructure())
        tableViewStructure.append(contentsOf: productsHeaderStructure())
        tableViewStructure.append(contentsOf: productsStructure())
        tableViewStructure.append(contentsOf: descriptionsStructure())
        tableViewStructure.append(contentsOf: bannersStructure())
    }
    
    func productsHeaderStructure() -> [DGVoucherListItem] {
        var tableViewStructure: [DGVoucherListItem] = []
        
        // Grid is displayed as a single cell and for the list, each product is a cell
        if let displayType = model.digitalGood?.displayType,
            displayType == .grid {
            tableViewStructure.append(DGVoucherListItem(type: .longText, data: BilletLocalizable.chooseValue.text))
        }else{
            tableViewStructure.append(DGVoucherListItem(type: .defaultCell, data: listSectionTitle))
        }
        
        return tableViewStructure
    }
    
    func tableHeaderStructure() -> [DGVoucherListItem] {
        var tableViewStructure: [DGVoucherListItem] = []
        if !shouldHideBanner {
            tableViewStructure.append(DGVoucherListItem(type: .header))
        }
        
        tableViewStructure.append(DGVoucherListItem(type: .title, data: model.digitalGood?.name ?? ""))
        
        return tableViewStructure
    }
    
    func productsStructure() -> [DGVoucherListItem] {
        var tableViewStructure: [DGVoucherListItem] = []
        if let displayType = model.digitalGood?.displayType,
            displayType == .grid {
            tableViewStructure.append(DGVoucherListItem(type: .grid))
        } else {
            for product in model.products {
                tableViewStructure.append(DGVoucherListItem(type: .product, product: product))
            }
        }
        
        return tableViewStructure
    }
    
    func descriptionsStructure() -> [DGVoucherListItem] {
        var tableViewStructure: [DGVoucherListItem] = []
        guard let productDescription = model.digitalGood?.longDescriptionMarkdown, productDescription != "" else {
            return []
        }
        tableViewStructure.append(DGVoucherListItem(type: .longText, data: BilletLocalizable.about.text))
        tableViewStructure.append(DGVoucherListItem(type: .longAttrText, data: "", markdownText: productDescription))
        
        return tableViewStructure
    }
    
    func bannersStructure() -> [DGVoucherListItem] {
        var tableViewStructure: [DGVoucherListItem] = []
        
        for banner in model.banners {
            tableViewStructure.append(DGVoucherListItem(type: .banner, banner: banner))
        }
        
        return tableViewStructure
    }
    
    func registerCells() {
        var nibStore = UINib(nibName: "DGVoucherListCell", bundle: Bundle.main)
        tableView.register(nibStore, forCellReuseIdentifier: cellIdentifier)
        
        nibStore = UINib(nibName: "DGClearBackgroundListCell", bundle: Bundle.main)
        tableView.register(nibStore, forCellReuseIdentifier: clearBgCellIdentifier)
        
        nibStore = UINib(nibName: "DGVoucherListTextCell", bundle: Bundle.main)
        tableView.register(nibStore, forCellReuseIdentifier: longCellIdentifier)
        
        nibStore = UINib(nibName: "DGVoucherMarkdownCell", bundle: Bundle.main)
        tableView.register(nibStore, forCellReuseIdentifier: viewCellIdentifier)
        
        nibStore = UINib(nibName: "DGVoucherListTitleCell", bundle: Bundle.main)
        tableView.register(nibStore, forCellReuseIdentifier: titleCellIdentifier)
        
        nibStore = UINib(nibName: "DGVoucherGridTableCell", bundle: Bundle.main)
        tableView.register(nibStore, forCellReuseIdentifier: gridCellIdentifier)
        
        tableView.register(BannerListCell.self, forCellReuseIdentifier: bannerCellIdentifier)
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: defaultCellIdentifier)
    }
    
    func setupBlurEffects() {
        let effect = UIBlurEffect(style: .dark)
        let effectView = UIVisualEffectView(effect: effect)
        blurView.addSubview(effectView)
        blurView.alpha = 0
        
        effectView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    func setupViews() {
        registerCells()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        
        automaticallyAdjustsScrollViewInsets = false
        
        if shouldHideBanner {
            bannerImageHeightLimit.constant = 0
            bannerImageHeight.constant = 0
            cancelButtonHeight.constant = 0
            cancelButton.isHidden = true
            tableViewTop.constant = 0
        }
        
        topLabel.text = model.digitalGood?.name ?? ""
        topLabel.preferredMaxLayoutWidth = UIScreen.main.bounds.width - topLabelLeftMargin.constant * 2
        // Two lines
        if topLabel.intrinsicContentSize.height > 25 {
            topLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
            topLabelYCenter.constant = -4
        }
        
        if let urlString = model.digitalGood?.bannerImgUrl {
            banner.setImage(url: URL(string: urlString))
        }
        
        if let image = model.digitalGood?.bannerImg {
            banner.cancelRequest()
            banner.image = image
        }
        
        view.backgroundColor = UIColor(red: 248.0/255, green: 248.0/255, blue: 248.0/255, alpha: 1)
        
        setupBlurEffects()
        self.loadingIndicator.startAnimating()
        
        setupCallbacks()
    }
    
    func showTopLabel() {
        guard self.canAnimateTopLabel else {
            return
        }
        UIView.animate(withDuration: 0.25) { [weak self] in
            self?.topLabel.alpha = 1
        }
    }
    
    func hideTopLabel() {
        UIView.animate(withDuration: 0.250) { [weak self] in
            self?.topLabel.alpha = 0
        }
    }
    
    @IBAction private func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

private extension DGVoucherController {
    func handleFavoriteButtonVisibility() {
        let featureManager = FeatureManager.shared
        guard
            let item = model.digitalGood, item.id.isNotEmpty
            else {
            return
        }

        showFavoriteButton()

        favoriteService.isFavorite(id: item.id, type: .digitalGood) { [weak self] isFavorite in
            self?.favoriteButton.isSelected = isFavorite
        }
    }

    func showFavoriteButton() {
        view.addSubview(favoriteButton)

        favoriteButton.layout {
            $0.top == view.topAnchor + Spacing.base01
            $0.trailing == view.trailingAnchor - Spacing.base02
            $0.height == Layout.Size.favoriteButton.height
            $0.width == Layout.Size.favoriteButton.width
        }
    }

    @objc
    func didTapAtFavoriteButton() {
        guard let digitalGood = model.digitalGood else {
            return
        }

        favoriteButton.isSelected ? unfavorite(digitalGood) : favorite(digitalGood)
    }

    func favorite(_ item: DGItem) {
        favoriteButton.isSelected = true

        favoriteService.favorite(id: item.id, type: .digitalGood) { [weak self] didFavorite in
            self?.favoriteButton.isSelected = didFavorite

            if didFavorite {
                Analytics.shared.log(FavoritesEvent.statusChanged(true, id: item.id, origin: .digitalGoods))
            }
        }
    }

    func unfavorite(_ item: DGItem) {
        favoriteButton.isSelected = false

        favoriteService.unfavorite(id: item.id, type: .digitalGood) { [weak self] didUnfavorite in
            self?.favoriteButton.isSelected = didUnfavorite == false

            if didUnfavorite {
                Analytics.shared.log(FavoritesEvent.statusChanged(false, id: item.id, origin: .digitalGoods))
            }
        }
    }
}
