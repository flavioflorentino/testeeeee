import Foundation
import UI

final class OtherValueField: UITextField, MaskTextField {
    var maskString: String?
    var maskblock: ((String, String) -> String)?
    var maxlength: Int?
}

final class DGVoucherOtherValueController: BaseFormController {
    @IBOutlet weak var amountTextField: OtherValueField!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nextButton: UIPPButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var amountTextFieldWidth: NSLayoutConstraint!
    
    private var textFieldToolbar:UIView = UIView()
    private let amountTextColor = Palette.ppColorGrayscale500.color
    private var screenTitle = ""
    private var max: Double?
    private var min: Double?
    private var amountChange: ((Double) -> ())?
    private var completion: ((DGVoucherOtherValueController) -> ())?
    private var titleText: String?
    private var currentAmountString: String = ""
    
    var allowCents = true
    var currentAmount = 0.0 {
        didSet {
            amountChange?(currentAmount)
        }
    }
    
    // MARK: Lifecycle
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = screenTitle
    }
    
    func setup(screenTitle: String, title: String? = nil, max: Double?, min: Double?, amountChange: ((Double) -> ())?, completion: @escaping ((DGVoucherOtherValueController) -> ())) {
        self.screenTitle = screenTitle
        self.titleText = title
        self.min = min
        self.max = max
        self.amountChange = amountChange
        self.completion = completion
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareLayout()
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        amountTextField.becomeFirstResponder()
    }
    
    private func prepareLayout() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        currencyLabel.textColor = Palette.ppColorGrayscale500.color
        titleLabel.textColor = Palette.ppColorGrayscale500.color
    }
    
    private func setupViews() {
        DispatchQueue.main.async { [weak self] in
            self?.textFieldToolbar.frame = CGRect(origin: .zero, size: CGSize(width: self?.view.frame.width ?? 0, height: 60))
            self?.amountTextField.inputAccessoryView = self?.textFieldToolbar
        }
        view.backgroundColor = Palette.ppColorGrayscale100.color
        contentView?.backgroundColor = Palette.ppColorGrayscale100.color
        amountTextField.delegate = self
        amountTextField.placeholder = "0,00"
        amountTextField.maxlength = 7
        amountTextField.text = ""
        
        if max == nil || min == nil {
            descriptionLabel.isHidden = true
        }
        
        if let title = titleText {
            titleLabel.text = title
        }
        
        if !allowCents {
            amountTextField.tintColor = .clear
        }
                
        validateAmount()
    }
    
    override func keyboardWillOpen(_ keyboardNotification:Notification){
        let margin:CGFloat = 16
        self.nextButton.removeFromSuperview()
        self.nextButton.translatesAutoresizingMaskIntoConstraints = false
        self.textFieldToolbar.addSubview(self.nextButton)
        NSLayoutConstraint.activate([
            self.nextButton.leftAnchor.constraint(equalTo: self.textFieldToolbar.leftAnchor, constant: margin),
            self.nextButton.rightAnchor.constraint(equalTo: self.textFieldToolbar.rightAnchor, constant: -margin),
            self.nextButton.centerYAnchor.constraint(equalTo: self.textFieldToolbar.centerYAnchor)
        ])
    }
    
    override func keyboardWillHide(_ keyboardNotification:Notification){
        let margin:CGFloat = 16
        let buttonOldFrame:CGRect = self.nextButton.superview?.convert(self.nextButton.frame, to: nil) ?? .zero
        let bottomConstraint:NSLayoutConstraint
        let animationDuration:Double = (keyboardNotification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.doubleValue ?? 0.25
        let animationCurveNumber:NSNumber? = keyboardNotification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
        let animationCurve: UIView.AnimationCurve
        if let animationIntValue = animationCurveNumber?.intValue, let curve = UIView.AnimationCurve(rawValue: animationIntValue) {
            animationCurve = curve
        } else {
            animationCurve = .easeInOut
        }
        self.nextButton.removeFromSuperview()
        self.nextButton.frame = buttonOldFrame
        self.nextButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.nextButton)
        if #available(iOS 11.0, *) {
            bottomConstraint = self.nextButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: margin)
        } else {
            bottomConstraint = self.nextButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: margin)
        }
        NSLayoutConstraint.activate([
            self.nextButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: margin),
            self.nextButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -margin),
            bottomConstraint
        ])
        
        UIView.animate(withDuration: animationDuration, animations: {
            UIView.setAnimationCurve(animationCurve)
            self.nextButton.superview?.layoutIfNeeded()
        }, completion: nil)
    }
    
    override func dismissKeyboard() {
        return
    }
    
    @IBAction private func next(_ sender: Any) {
        guard validateAmount() else {
            return
        }
        completion?(self)
    }
    
    // MARK: Validation
    
    // Validate & format the entered amount
    @discardableResult
    fileprivate func validateAmount() -> Bool {
        guard let text = amountTextField.text else {
    return false
}
        let max = self.max ?? 9999.0
        let min = self.min ?? 0.0
        var valid = true
        
        let amount = text.currencyToNumber.doubleValue
        
        if amount > max {
            descriptionLabel.text = "\(BilletLocalizable.maximumValue.text) \(max.stringAmount)"
            valid = false
        }
        
        if amount < min {
            descriptionLabel.text = "\(BilletLocalizable.minimumValue.text) \(min.stringAmount)"
            valid = false
        }
        
        descriptionLabel.isHidden = valid
        let color = valid ? amountTextColor : Palette.ppColorBranding300.color
        amountTextField.textColor = color
        currencyLabel.textColor = color
        nextButton.isEnabled = valid
        
        // If we don't have any text, it's still not valid, but we don't want to show the description label
        if text == "" {
            valid = false
            currencyLabel.textColor = Palette.ppColorGrayscale300.color
            descriptionLabel.isHidden = true
        }
        
        return valid
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if !allowCents {
            var value = currentAmount == 0 ? "" : String(Int(currentAmount))
            if range.length <= value.count {
                value = value.chopSuffix(range.length)
            }
            value += string
            if value.length < 7 {
                currentAmountString = value.currencyInputFormatting(allowCents: false)
            }
        }else{
            let newString = NSString(string: textField.text ?? "").replacingCharacters(in: range, with: string)
            if newString.length < 11 {
                let amountString = newString.currencyInputFormatting()
                currentAmountString = amountString
            }
        }
        
        textField.text = currentAmountString
        currentAmount = currentAmountString.currencyToNumber.doubleValue
        
        validateAmount()
        
        // Solves a bug that occurs only in iOS 9 and 10: textField not resizing while typing
        if #available(iOS 11.0, *) {
            // newer versions
        } else {
            let stringSize = currentAmountString.size(withAttributes: [.font: UIFont.systemFont(ofSize: 52.0)])
            amountTextFieldWidth.constant = stringSize.width
        }
        
        return false
    }
}
