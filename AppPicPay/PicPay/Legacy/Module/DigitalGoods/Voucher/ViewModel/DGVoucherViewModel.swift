import Core
import FeatureFlag
import Foundation
import PCI

class DGListViewModel: NSObject {
    var origin: String?
    var digitalGood: DGItem? {
        didSet {
            if let digitalGood = digitalGood {
                self.loadProducts(for: digitalGood)
                self.loadBanners()
            }
        }
    }
    
    var paymentManager = PPPaymentManager()
    
    var product: DGProduct? {
        didSet {
            guard let product = product else {
                return
            }
            
            paymentManager?.subtotal = NSDecimalNumber(value: product.price)
        }
    }
    
    var products: [DGProduct] = [] {
        didSet {
            productsUpdate?()
        }
    }
    
    var banners: [DGBanner] = [] {
        didSet {
            bannersUpdate?()
        }
    }
    
    var isLoading = true {
        didSet {
            loadingUpdate?(isLoading)
        }
    }
    
    var loadingUpdate: ((Bool) -> Void)? {
        didSet {
            // Update as soon as it's set
            loadingUpdate?(isLoading)
        }
    }
    
    var productsUpdate: (() -> Void)? {
        didSet {
            // Update as soon as it's set
            productsUpdate?()
        }
    }
    
    var bannersUpdate: (() -> Void)? {
        didSet {
            // Update as soon as it's set
            bannersUpdate?()
        }
    }
    
    var productsUpdateFailed: ((PicPayErrorDisplayable) -> Void)?
    
    func loadProducts(`for` digitalGood: DGItem) {}
    
    func loadBanners() {}
    
    func goToPayment(from: UIViewController) {}
}

class DGVoucherViewModel: DGListViewModel {
    typealias DGVoucherViewModelDependencies = HasFeatureManager & HasKeychainManager

    private let service: VoucherServiceProtocol = VoucherService()
    private let dependencies: DGVoucherViewModelDependencies = DependencyContainer()
    
    override func loadProducts(`for` digitalGood: DGItem) {
        guard digitalGood.sellerId.isEmpty || digitalGood.sellerId == "0" else {
            legacyLoadProducts(digitalGood: digitalGood)
            return
        }

        loadProductsWithInfo(digitalGood: digitalGood)
    }

    private func legacyLoadProducts(digitalGood: DGItem) {
        isLoading = true
        api.products(serviceId: digitalGood.id) { [weak self] result in
            self?.isLoading = false
            switch result {
            case .success(let data):
                self?.products = data
            case .failure(let failure):
                self?.productsUpdateFailed?(failure)
            }
        }
    }

    private func loadProductsWithInfo(digitalGood: DGItem) {
        isLoading = true
        api.getProductsWithDGInfo(serviceId: digitalGood.id) { result in
            self.isLoading = false
            switch result {
            case .success(let response):
                self.products = response.data
                digitalGood.populate(with: response.service)
                self.bannersUpdate?()
            case .failure(let failure):
                self.productsUpdateFailed?(failure)
            }
        }
    }
    
    override func loadBanners() {
        self.banners = []
    }
    
    let api = DGVoucherApi()
    
    private func getDetailsFor(product: DGProduct, service: DGItem) -> [DGPaymentDetailsBase] {
        guard let product = product as? DGVoucherProduct else {
            return []
        }
        var details: [DGPaymentDetailsBase] = []
        
        if let longDescription = product.description {
            details.append(DGPaymentLongDetailItem(text: longDescription))
        }
        if let disclaimerMarkdown = service.disclaimerMarkdown {
            details.append(DGPaymentImportantDetailItem(text: disclaimerMarkdown))
        }
        
        return details
    }
    
    override func goToPayment(from: UIViewController) {
        guard
            let digitalGood = digitalGood,
            let product = product as? DGVoucherProduct
            else {
                return
        }
        
        if FeatureManager.isActive(.newVoucher) {
            openNewPaymentViewController(from: from, product: product, digitalGood: digitalGood)
            return
        }
        
        openLegacyPaymentViewController(from: from, product: product, digitalGood: digitalGood)
    }
    
    private func openNewPaymentViewController(from: UIViewController, product: DGVoucherProduct, digitalGood: DGItem) {
        let details = getDetailsFor(product: product, service: digitalGood)
        let items = DigitalGoodsPayment(
            title: digitalGood.name,
            image: digitalGood.imgUrl,
            link: digitalGood.infoUrl,
            payeeId: digitalGood.sellerId,
            value: product.price,
            paymentType: .voucher
        )
        
        let service = DigitalGoodsVoucherService(product: product, item: digitalGood)
        let orchestrator = DigitalGoodsPaymentOrchestrator(items: items, paymentDetails: details, service: service)
        from.navigationController?.pushViewController(orchestrator.paymentViewController, animated: true)
    }
    
    private func openLegacyPaymentViewController(from: UIViewController, product: DGVoucherProduct, digitalGood: DGItem) {
        guard let paymentManager = paymentManager else {
            return
        }
        let details = getDetailsFor(product: product, service: digitalGood)
        let paymentScreen = DGPaymentViewController.controllerFromStoryboard()
        let paymentModel = DGPaymentViewModel(
            amount: product.price,
            type: .voucher,
            sellerName: digitalGood.name,
            sellerImageUrl: digitalGood.imgUrl,
            sellerId: digitalGood.id,
            detailInfo: details,
            dependencies: DependencyContainer()
        )
        
        paymentScreen.title = DefaultLocalizable.voucherPaymentTitle.text
        paymentScreen.setup(paymentManager: paymentManager, model: paymentModel, delegate: self)
        
        // Add the right button to the payment screen + show SFSafariVC on tap
        if let infoUrl = digitalGood.infoUrl {
            paymentScreen.setRightButton()
            paymentScreen.rightBarButtonAction = { controller in
                guard let url = URL(string: infoUrl) else {
                    return
                }
                
                ViewsManager.presentSafariViewController(url, from: controller)
            }
        }
        
        from.navigationController?.pushViewController(paymentScreen, animated: true)
    }
}

// MARK: Payment request construction
extension DGVoucherViewModel: DGPaymentDelegate {
    // swiftlint:disable function_parameter_count
    func didPerformPayment(with manager: PPPaymentManager,
                           cvv: String?,
                           additionalPayload: [String: Any],
                           pin: String,
                           biometry: Bool,
                           privacyConfig: String,
                           someErrorOccurred: Bool,
                           completion: @escaping (WrapperResponse) -> Void) {
        self.paymentManager = manager
        guard let product = product,
            let digitalGood = digitalGood,
            let paymentManager = paymentManager else {
                return
        }
        
        let request = DGVoucherApiRequest()
        
        request.paymentManager = paymentManager
        request.pin = pin
        request.biometry = biometry
        request.sellerId = digitalGood.sellerId
        request.digitalGoodId = digitalGood.id
        request.productId = product.id
        request.privacyConfig = privacyConfig
        request.someErrorOccurred = someErrorOccurred
        
        if FeatureManager.isActive(.pciVoucher) {
            createTransactionPCI(manager: manager, request: request, password: pin, cvv: cvv, completion: completion)
            return
        }
        
        DGVoucherApi().pay(request: request, pin: pin, completion)
    }
    
    private func createTransactionPCI(
        manager: PPPaymentManager,
        request: DGVoucherApiRequest,
        password: String,
        cvv: String?,
        completion: @escaping (WrapperResponse) -> Void
    ) {
        guard let voucherPayload = createVoucherPayload(manager: manager, request: request) else {
            let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            completion(PicPayResult.failure(error))
            return
        }
        
        let cvv = createCVVPayload(manager: manager, cvv: cvv)
        let payload = PaymentPayload<DigitalGoodsPayload<Voucher>>(cvv: cvv, generic: voucherPayload)
        service.createTransaction(password: password, payload: payload, isNewArchitecture: false) { result in
            DispatchQueue.main.async {
                let mappedResult = result
                    .map { BaseApiGenericResponse(dictionary: $0.json) }
                    .mapError { $0.picpayError }

                completion(mappedResult)
            }
        }
    }
    
    private func createVoucherPayload(manager: PPPaymentManager, request: DGVoucherApiRequest) -> DigitalGoodsPayload<Voucher>? {
        guard let params = try? request.toParams(),
            let data = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) else {
                return nil
        }
        return try? JSONDecoder.decode(data, to: DigitalGoodsPayload<Voucher>.self)
    }
    
    private func createCVVPayload(manager: PPPaymentManager, cvv: String?) -> CVVPayload? {
        guard let cvv = cvv else {
            return createLocalStoreCVVPayload(manager: manager)
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func createLocalStoreCVVPayload(manager: PPPaymentManager) -> CVVPayload? {
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        guard manager.cardTotal()?.doubleValue != .zero,
              let cvv = dependencies.keychain.getData(key: KeychainKeyPF.cvv(id)) else {
                return nil
        }
        
        return CVVPayload(value: cvv)
    }
}
