import Foundation
import UI
import UIKit

// "Other" Cell for the grid selection (used to push the custom value VC)
final class DGVoucherOtherValueCell: UICollectionViewCell, SelectableCell {
    @IBOutlet weak var otherLabel: UILabel!
    @IBOutlet weak var backgroundCircleView: BackgroundCircleValueView!
    
    var valueColor = UIColor(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 1)

    var isCellSelected:Bool? {
        didSet {
            self.backgroundCircleView.isSelected = self.isCellSelected
            otherLabel.textColor = self.isCellSelected == true ? Palette.ppColorGrayscale000.color : valueColor
            
            guard self.isCellSelected == true else {
            return
        }
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                self.isCellSelected = false
            }
        }
    }
    
    override var isSelected: Bool {
        didSet {
            guard isSelected else {
            return
        }
            self.isSelected = false
        }
    }
}
