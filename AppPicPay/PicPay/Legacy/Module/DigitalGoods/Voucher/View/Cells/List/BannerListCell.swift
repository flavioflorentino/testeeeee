import UIKit
import UI
import SnapKit

protocol BannerListCellDelegate: AnyObject {
    func didTouchLink(url: URL)
}

private extension BannerListCell.Layout {
    enum Size {
        static let icon = CGSize(width: 67, height: 67)
    }
}

final class BannerListCell: UITableViewCell {
    fileprivate enum Layout { }
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle(cornerRadius: .light))
            .with(\.backgroundColor, .backgroundPrimary())
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        return label
    }()
    
    private lazy var linkButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle(size: .small))
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector(linkWasPressed), for: .touchUpInside)
        return button
    }()
    
    private lazy var iconImg = UIImageView()
    
    private weak var delegate: BannerListCellDelegate?
    
    private var actionLink: String?
    
    func configureCell(banner: DGBanner, delegate: BannerListCellDelegate) {
        titleLabel.text = banner.title
        descriptionLabel.text = banner.description
        iconImg.image = banner.image
        
        linkButton.setTitle(banner.actionTitle, for: .normal)
        linkButton.buttonStyle(LinkButtonStyle(size: .small))
       
        self.delegate = delegate
        actionLink = banner.actionLink
        
        buildLayout()
    }
    
    @objc
    private func linkWasPressed() {
        guard let urlString = actionLink, let url = URL(string: urlString) else {
            return
        }
        delegate?.didTouchLink(url: url)
    }
}

extension BannerListCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(containerView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(descriptionLabel)
        containerView.addSubview(iconImg)
        containerView.addSubview(linkButton)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        linkButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }
        
        iconImg.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.icon)
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(titleLabel.snp.trailing).offset(Spacing.base02)
            $0.leading.equalTo(descriptionLabel.snp.trailing).offset(Spacing.base02)
            $0.leading.equalTo(linkButton.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
    }
    
    func configureViews() {
        selectionStyle = .none
    }
}
