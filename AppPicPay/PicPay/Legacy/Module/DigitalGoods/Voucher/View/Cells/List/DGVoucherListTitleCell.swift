import Foundation
import UI

final class DGVoucherListTitleCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var button: UIPPButton!
    @IBOutlet weak var buttonWidth: NSLayoutConstraint!
    var didTapInfo: (() -> ())?
    
    var height: CGFloat {
        title.preferredMaxLayoutWidth = UIScreen.main.bounds.size.width - 134
        return title.intrinsicContentSize.height
    }
    
    func setup(showButton: Bool = false, _ didTapInfo: @escaping (() -> ())) {
        setupColors()
        self.didTapInfo = didTapInfo
        button.isHidden = !showButton
        if !showButton {
            buttonWidth.constant = 0
        }
    }
    
    private func setupColors() {
        title.textColor = Palette.ppColorGrayscale600.color
        backgroundColor = Palette.ppColorGrayscale100.color
        contentView.backgroundColor = Palette.ppColorGrayscale100.color
    }
    
    @IBAction private func buttonTapped(_ sender: Any) {
        didTapInfo?()
    }
}
