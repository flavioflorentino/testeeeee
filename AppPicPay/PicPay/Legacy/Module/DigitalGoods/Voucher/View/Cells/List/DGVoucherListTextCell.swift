import Foundation
import UI

final class DGVoucherListTextCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    var height: CGFloat {
        label.preferredMaxLayoutWidth = UIScreen.main.bounds.width - 32
        return label.intrinsicContentSize.height + topConstraint.constant
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupColors()
        
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupColors()
    }
    
    private func setupColors() {
        backgroundColor = Palette.ppColorGrayscale000.color
        contentView.backgroundColor = Palette.ppColorGrayscale100.color
    }
}
