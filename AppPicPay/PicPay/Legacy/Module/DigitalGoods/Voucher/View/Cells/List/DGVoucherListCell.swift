import Foundation
import UIKit
import UI

final class DGVoucherListCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var priceLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var productImageViewWidth: NSLayoutConstraint!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configureCell(product: DGProduct, imgUrl: String? = nil) {
        setupColors()
        selectionStyle = .none
        
        bgView.layer.cornerRadius = 5
        productImageView.layer.cornerRadius = 25
        
        nameLabel.text = product.name
        nameLabel.sizeToFit()
        
        // The cell can either have a price or a description, not both
        if let description = product.description {
            priceLabel.text = description
            priceLabel.textColor = Palette.ppColorGrayscale500.color
            priceLabel.font = UIFont.systemFont(ofSize: 13)
        } else {
            priceLabel.text = product.price.stringAmount
        }
        
        showProductImage()
        if let urlString = imgUrl,
            product.imageUrl == nil {
            productImageView.setImage(url: URL(string: urlString))
        } else if let urlString = product.imageUrl {
            productImageView.setImage(url: URL(string: urlString))
        } else if let img = product.img {
            productImageView.cancelRequest()
            productImageView.image = img
        } else {
            hideProductImage()
        }
        
        priceLabel.isHidden = product.hidePrice
        bgView.dropShadow()
    }
    
    private func setupColors() {
        backgroundColor = Palette.ppColorGrayscale000.color
        bgView.backgroundColor = Palette.ppColorGrayscale000.color
        contentView.backgroundColor = Palette.ppColorGrayscale100.color
    }
    
    func animateSelection() {
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.bgView.backgroundColor = Palette.ppColorGrayscale200.color
            UIView.animate(withDuration: 0.5) { [weak self] in
                self?.bgView.backgroundColor = Palette.ppColorGrayscale000.color
            }
        }
    }
    
    func hideProductImage() {
        productImageViewWidth.constant = 0.0
    }
    
    func showProductImage() {
        productImageViewWidth.constant = 50.0
    }
}
