//
//  DGVoucherGridTableCell.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 01/11/17.
//

import Foundation

final class DGVoucherGridTableCell: UITableViewCell {
    @IBOutlet weak var circleView: CircleItemsView!
    
    var height: CGFloat {
        return circleView.totalHeight + 16
    }
    
    func setup(delegate: UICollectionViewDelegate, dataSource: CircleViewDataSource, count: Int) {
        circleView.delegate = delegate
        circleView.dataSource = dataSource
        self.contentView.isUserInteractionEnabled = false

        circleView.update(totalCount: count)
    }
}
