//
//  DGClearBackgroundListCell.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 05/04/18.
//

import UI
import Foundation

final class DGClearBackgroundListCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var changeLabel: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    var height: CGFloat {
        title.preferredMaxLayoutWidth = UIScreen.main.bounds.size.width - 134
        return title.intrinsicContentSize.height
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        icon.cancelRequest()
    }
    
    func setup(product: DGProduct) {
        title.text = product.name + " -"
        
        if let urlString = product.imageUrl {
            let url = URL(string: urlString)
            icon.setImage(url: url)
        }
        if let image = product.img {
            icon.image = image
        }
        
        icon.backgroundColor = .clear
        icon.layer.cornerRadius = 14
        icon.clipsToBounds = true
        
        let underlineAttribute: [NSAttributedString.Key: Any] = [.underlineStyle: NSUnderlineStyle.single.rawValue, .foregroundColor: Palette.ppColorBranding300.color] 
        let underlineAttributedString = NSAttributedString(string: DefaultLocalizable.change.text, attributes: underlineAttribute)
        
        changeLabel.attributedText = underlineAttributedString
    }
}
