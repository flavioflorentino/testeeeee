import UI
import UIKit

protocol DGVoucherMarkdownCellDelegate : AnyObject {
    func voucherListOnTouchLink(url: URL) -> Bool
}

final class DGVoucherMarkdownCell: UITableViewCell {
    @IBOutlet weak var markdown: MarkdownView!
    var height: CGFloat = 0
    
    private var needLoadMakdown: Bool = true
    weak var delegate: DGVoucherMarkdownCellDelegate?
    
    func setup(markdownText: String?, completion: @escaping ((_ height: CGFloat) -> ())) {
        selectionStyle = .none
        markdown.isScrollEnabled = false
        
        if needLoadMakdown {
            markdown.load(markdown: markdownText)
            needLoadMakdown = false
        }
        
        markdown.onRendered = { [weak self] height in
            guard let previousHeight = self?.height,
                previousHeight != height else {
                    return
            }
            self?.height = height
            completion(height)
        }
        
        markdown.onTouchLink = { [weak self] request in
            guard let url = request.url else {
                return false
            }
            
            if let result = self?.delegate?.voucherListOnTouchLink(url: url) {
                return result
            } else {
                return false
            }
        }
        
        setupColors()
    }
    
    private func setupColors() {
        backgroundColor = Palette.ppColorGrayscale100.color
        contentView.backgroundColor = Palette.ppColorGrayscale100.color
    }
}
