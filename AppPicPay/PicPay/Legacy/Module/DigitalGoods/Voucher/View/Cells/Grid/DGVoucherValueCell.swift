import Foundation
import UI
import UIKit

// Value Cell for the grid selection
final class DGVoucherValueCell: UICollectionViewCell, SelectableCell {
    @IBOutlet weak var backgroundCircleView: BackgroundCircleValueView!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!

    var valueColor: UIColor?
    
    var isCellSelected:Bool? {
        didSet {
            self.backgroundCircleView.isSelected = isCellSelected
            valueLabel.textColor = self.isCellSelected == true ? Palette.ppColorGrayscale000.color : valueColor
            currencyLabel.textColor = valueLabel.textColor
            
            guard self.isCellSelected == true else {
            return
        }
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                self.isCellSelected = false
            }
        }
    }
    
    override var isSelected: Bool {
        didSet {
            guard isSelected else {
            return
        }
            self.isSelected = false
        }
    }
    
    func configureCell(product: DGProduct){
        valueLabel.text = String(format: "%.2f", product.valueMax).replacingOccurrences(of: ".", with: ",").replacingOccurrences(of: ",00", with: "")
        
        if product.isRangeType {
            valueLabel.text = DefaultLocalizable.other.text
        }
        
        valueColor = valueLabel.textColor
    }

}
