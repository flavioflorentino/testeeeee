import FeatureFlag
import Foundation

struct NewStoreEventProperty: ApplicationEventProperty {
    typealias Dependency = HasFeatureManager

    // MARK: - Properties
    private let dependency: Dependency

    init(dependency: Dependency) {
        self.dependency = dependency
    }
    
    var properties: [String: Any] {
        [
            "is_in_experiment_new_store": dependency.featureManager.isActive(.experimentStoreOpenNewStoreBool)
        ]
    }
}
