import UI
import UIKit

final class DGProfileViewController: UIViewController {
    @IBOutlet weak var profileImage: UICircularImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var payButton: UIPPButton!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    
    private var fullImage:ImageFullScreenView?
    
    weak var parentController:UIViewController?
    var digitalGood: DGItem
    
    init(digitalGood: DGItem, controller: UIViewController?) {
        self.digitalGood = digitalGood
        self.parentController = controller
        super.init(nibName: "DGProfileViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureProfile()
        self.loadProfile()
        self.prepareLayout()
    }
    
    private func prepareLayout() {
        self.view.backgroundColor = Palette.ppColorGrayscale000.color
        self.nameLabel.textColor = Palette.ppColorGrayscale600.color
        self.textLabel.textColor = Palette.ppColorGrayscale400.color
        self.loadingView.tintColor = Palette.ppColorGrayscale400.color
        self.payButton.setTitleColor(Palette.white.color)
        self.payButton.setBackgroundColor(Palette.ppColorBranding400.color)
    }
    
    // MARK: - Internal
    func configureProfile() {
        nameLabel.text = digitalGood.name
        textLabel.text = digitalGood.itemDescription
        if let imgUrl = digitalGood.imgUrl {
            profileImage.setImage(url: URL(string: imgUrl), placeholder: PPStore.photoPlaceholder())
        }
        fullImage = ImageFullScreenView(imageView: profileImage)
    }
    
    /**
     Load profile data from serer
     */
    func loadProfile() {
        self.startLoading()
        DigitalGoodsApi().getDGInfo(id: digitalGood.id) { [weak self] result in
            DispatchQueue.main.async {
                self?.stopLoading()
                switch(result) {
                case .success(let data):
                    self?.digitalGood = data
                    self?.configureProfile()
                case .failure(let error):
                    if let strongSelf = self {
                        AlertMessage.showAlert(withMessage: error.localizedDescription, controller: strongSelf)
                    }
                    self?.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    /**!
     * Show the loading view
     */
    func startLoading() {
        self.loadingView.isHidden = false
        self.loadingView.startAnimating()
        self.profileImage.layer.opacity = 0.1
        self.textLabel.layer.opacity = 0.1
        self.nameLabel.layer.opacity = 0.1
        self.payButton.layer.opacity = 0.1
        self.payButton.isEnabled = false
    }
    
    /**!
     * Stop and hide the loading view
     */
    func stopLoading() {
        self.loadingView.isHidden = true
        self.loadingView.stopAnimating()
        self.profileImage.layer.opacity = 1.0
        self.textLabel.layer.opacity = 1.0
        self.nameLabel.layer.opacity = 1.0
        self.payButton.layer.opacity = 1.0
        self.payButton.isEnabled = true
    }
    
    // MARK: - User Actions
    @IBAction private func close(_ sender: AnyObject) {
        dismiss(animated: true)
    }
    
    @IBAction private func pay(_ sender: AnyObject) {
        guard let parentController = self.parentController else {
            return
        }
        
        dismiss(animated: false) { [weak self] in
            guard let self = self else {
                return
            }
            DGHelpers.openDigitalGoodFlow(for: self.digitalGood, viewController: parentController)
        }
    }
}
