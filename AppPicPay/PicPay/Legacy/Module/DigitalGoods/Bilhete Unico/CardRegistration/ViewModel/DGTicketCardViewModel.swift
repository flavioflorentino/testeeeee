import Foundation
import AnalyticsModule

final class DGTicketCardViewModel {
    var cards:[DGTicketCard] = [] {
        didSet {
            didLoadCards?()
        }
    }
    var cities:[DGTicketCity] = [] {
        didSet {
            didLoadCities?()
        }
    }
    
    var warningAlert: Alert? {
        guard let cityWarningAlert = selectedCity?.warningAlert else {
            return nil
        }
        
        let confirmAction = Button(title: cityWarningAlert.confirmButtonTitle, type: .cta, handler: { [weak self] alert, button in
            alert.dismiss(animated: true, completion: nil)
            self?.confirmPermission()
            DGTicketCoordinator.shared.isLoading = true
        })
        let cancelAction = Button(title: cityWarningAlert.cancelButtonTitle, type: .cleanUnderlined, handler: {[weak self] alert,button in
            alert.dismiss(animated: true, completion: nil)
            self?.cancelPermission()
        })
        
        return Alert(with: cityWarningAlert.title, text: cityWarningAlert.message, buttons: [confirmAction, cancelAction], image: nil)
    }
    
    private let api = DGTicketApi()
    
    var selectedCity: DGTicketCity?
    var didLoadCities: (() -> Void)?
    var didLoadCards: (() -> Void)?
    var didCreateCard: ((DGTicketCard) -> Void)?
    var didFail: ((String) -> Void)?
    var didDeletedCard: ((IndexPath) -> Void)?
    var didConfirmPermission: ((RegisterYourCityPlaceholder) -> Void)?
    var didFailOnConfirmPermission: ((Alert) -> Void)?
    
    private var service: DGTicketCardServicing
    private let analytics: Analytics
    
    init (service: DGTicketCardServicing = DGTicketCardService(), analytics: Analytics = Analytics.shared){
        self.service = service
        self.analytics = analytics
    }
    
    func createCard(number: String, name: String) {
        guard let cityId = selectedCity?.id else {
            return
        }
        
        if let cityName = self.selectedCity?.name{
            analytics.log(DGTicketAnalytics.buttonSignCard(cityId: cityId, cityName: cityName))
        }
        
        api.createCard(cityId: cityId, number: number, name: name) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let card):
                    self?.didCreateCard?(card)
                case .failure(let error):
                    self?.didFail?(error.localizedDescription)
                }
            }
        }
    }
    
    func deleteCard(of indexPath: IndexPath) {
        let card: DGTicketCard = cards[indexPath.row]
        api.deleteCard(card: card) { [weak self] result in
            guard let strongSelf = self else {
                self?.didFail?("Houve um erro com a solicitação")
                return
            }
            DispatchQueue.main.async {
                switch result {
                case .success:
                    strongSelf.didDeletedCard?(indexPath)
                case .failure:
                    strongSelf.didFail?("Houve um erro com a solicitação")
                }
            }
        }
    }
    
    func loadCards() {
        api.getCards() { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let cards):
                    self?.cards = cards.list
                case .failure(let error):
                    self?.didFail?(error.localizedDescription)
                }
            }
        }
    }
    
    func loadCities() {
        api.getCities { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let cities):
                    self?.cities = cities.list
                case .failure(let error):
                    self?.didFail?(error.localizedDescription)
                }
            }
        }
    }
    
    func confirmPermission() {
        if let cityId = self.selectedCity?.id, let cityName = self.selectedCity?.name {
            analytics.log(DGTicketAnalytics.buttonSignPopupShare(cityId: cityId, cityName: cityName))
        }
        
        guard let city = selectedCity else {
            return
        }
        
        self.service.confirmPermission(city: city) { [weak self] result in
            switch result{
            case .success(let placeholder):
                self?.didConfirmPermission?(placeholder)
                
            case .failure(let error):
                
                if error.data == nil {
                    let alert = Alert(with: error)
                    self?.didFailOnConfirmPermission?(alert)
                    return
                }
                
                guard let confirmButtonTitle = error.data?.dictionaryObject?["confirmButtonTitle"] as? String, let dismissButtonTitle = error.data?.dictionaryObject?["dismissButtonTitle"] as? String, let redirectURLString = error.data?.dictionaryObject?["redirectUrl"] as? String, let redirectURL = URL(string: redirectURLString) else {
                    
                    let dismissButtom =  Button(title: DefaultLocalizable.btOkUnderstood.text, type: .cta, handler: {alert,button in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    
                    let alert = Alert(with: error.title, text: error.message, buttons: [dismissButtom], image: nil)
                    
                    self?.didFailOnConfirmPermission?(alert)
                    return
                }
                
                let confirmButtom = Button(title: confirmButtonTitle, type: .cta, handler: {alert, button in
                    alert.dismiss(animated: true, completion: nil)
                    
                    if let cityId = self?.selectedCity?.id, let cityName = self?.selectedCity?.name {
                        self?.analytics.log(DGTicketAnalytics.buttonOkConfirmPermissionError(cityId: cityId, cityName: cityName, errorTitle: error.title, errorCode: error.code, errorMessage: error.message))
                    }
                    
                  UIApplication.shared.open(redirectURL)
                    
                })
                
                let dismissButtom =  Button(title: dismissButtonTitle, type: .cleanUnderlined, handler: {alert,button in
                    alert.dismiss(animated: true, completion: nil)
                })
                
                let alert = Alert(with: error.title, text: error.message, buttons: [confirmButtom, dismissButtom], image: nil)
                self?.didFailOnConfirmPermission?(alert)
                
            }
        }
    }
    
    func cancelPermission() {
        if let cityId = self.selectedCity?.id, let cityName = self.selectedCity?.name {
            analytics.log(DGTicketAnalytics.linkCancelPopupShare(cityId: cityId, cityName: cityName))
        }
    }
    
    func nameTextFieldBeginEditing(){
        if let cityId = self.selectedCity?.id, let cityName = self.selectedCity?.name{
            analytics.log(DGTicketAnalytics.inputNickNameCard(cityId: cityId, cityName: cityName))
        }
    }
    
    func openInfo(){
        if let cityId = self.selectedCity?.id, let cityName = self.selectedCity?.name{
            analytics.log(DGTicketAnalytics.linkUnknownCard(cityId: cityId, cityName: cityName))
        }
    }
    
    func citySelected(){
        if let cityId = self.selectedCity?.id, let cityName = self.selectedCity?.name {
            analytics.log(DGTicketAnalytics.selectecCity(cityId: cityId, cityName: cityName))
        }
    }
}
