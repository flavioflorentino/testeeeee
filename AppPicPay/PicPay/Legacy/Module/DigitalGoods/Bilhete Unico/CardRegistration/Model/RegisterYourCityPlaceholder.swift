import Foundation
import SwiftyJSON

struct RegisterYourCityPlaceholder: BaseApiResponse, Decodable {
    struct RegisterYourCityPlaceholderField: Decodable, Equatable {
        let text: String?
        let isEditable: Bool?
    }
    
    let cardNumber: RegisterYourCityPlaceholderField?
    let cardName: RegisterYourCityPlaceholderField?
    
    init?(json: JSON) {
        let decoder = JSONDecoder()
        guard let decodedInstance = try? decoder.decode(Self.self, from: json.rawData(options: .prettyPrinted)) else {
            return nil
        }
        self = decodedInstance
    }
}

extension RegisterYourCityPlaceholder: Equatable {}
