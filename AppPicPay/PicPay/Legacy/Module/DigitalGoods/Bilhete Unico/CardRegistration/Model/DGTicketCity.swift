//
//  DGTicketCity.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 04/04/18.
//

import Foundation
import SwiftyJSON

struct DGTicketCityWarningAlert: BaseApiResponse, Decodable {
    let title: String
    let message: String
    let confirmButtonTitle: String
    let cancelButtonTitle: String
 
    init?(json: JSON) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        guard let data = try? JSONSerialization.data(withJSONObject: json.object, options: .fragmentsAllowed), let decodedInstance = try? decoder.decode(Self.self, from: data) else {
            return nil
        }
        self = decodedInstance
    }
}

final class DGTicketCity: BaseApiResponse, DGProduct {
    var id: String
    var name: String
    var description: String?
    var disclaimerMarkdown: String?
    var hidePrice: Bool = true
    var price: Double = 0.0
    var imageUrl: String?
    var img: UIImage?
    var isRangeType: Bool {
        return false
    }
    var valueMax: Double = 0.0
    var valueMin: Double = 0.0
    var customAmount: Double?
    
    var infoUrl: String?
    
    let warningAlert: DGTicketCityWarningAlert?
    
    required init?(json: JSON) {
        guard
            let id = json["_id"].string,
            let name = json["name"].string,
            let image = json["url_logo"].string
        else {
     return nil
}
        
        self.infoUrl = json["url_info"].string
        self.id = id
        self.imageUrl = image
        self.name = name
        self.warningAlert = DGTicketCityWarningAlert(json: json["warning_alert"])
    }
}


