//
//  DGTicketCard.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 03/04/18.
//

import Foundation
import SwiftyJSON

final class DGTicketCard: BaseApiResponse, DGProduct {
    var id: String
    var name: String
    var description: String?
    var disclaimerMarkdown: String?
    var hidePrice: Bool = true
    var price: Double = 0.0
    var imageUrl: String?
    var img: UIImage?
    var cityId: String?
    var isRangeType: Bool {
        return false
    }
    var valueMax: Double = 0.0
    var valueMin: Double = 0.0
    var customAmount: Double?
    
    required init?(json: JSON) {
        guard
            let id = json["_id"].string,
            let name = json["name"].string,
            let cityId = json["city_id"].string,
            let imgUrl = json["city_url_logo"].string
        else {
     return nil
}
        
        self.id = id
        self.imageUrl = imgUrl
        self.description = json["card"].string
        self.name = name
        self.hidePrice = self.description == nil
        self.cityId = cityId
    }
}
