import Core
import Foundation

protocol DGTicketCardServicing {
    func confirmPermission(city: DGTicketCity, completion: @escaping (Result<RegisterYourCityPlaceholder, PicPayError>) -> Void)
}

final class DGTicketCardService: DGTicketCardServicing {
    private let api = DGTicketApi()
    
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func confirmPermission(city: DGTicketCity, completion: @escaping (Result<RegisterYourCityPlaceholder, PicPayError>) -> Void) {
        
        api.confirmPermission(city: city) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .failure(let error):
                    completion(.failure(error))
                case .success(let response):
                    completion(.success(response))
                }
            }
        }
    }
}
