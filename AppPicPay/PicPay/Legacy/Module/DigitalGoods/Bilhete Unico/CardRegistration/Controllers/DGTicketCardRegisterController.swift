import Foundation
import Validator
import UI

final class DGTicketCardRegisterController: BaseFormController {
    static func fromNib() -> DGTicketCardRegisterController {
        return controllerFromStoryboard(.digitalGoodsTicket)
    }
    
    @IBOutlet weak var cardNumber: UIPPFloatingTextField!
    @IBOutlet weak var cardName: UIPPFloatingTextField!
    @IBOutlet weak var infoButton: UIButton!
    
    var model: DGTicketCardViewModel?
    
    var infoUrl: String?
    var placeholder: RegisterYourCityPlaceholder?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        model?.didCreateCard = { [weak self] card in
            self?.navigationController?.popToRootViewController(animated: true)
            DGTicketCoordinator.shared.card = card
            DGTicketCoordinator.shared.reload()
        }
        setupColors()
        setupInitialTexts()
        configureForm()
        configureValidations()
        
        
        infoButton.isHidden = model?.selectedCity?.infoUrl == nil
        self.title = TicketLegacyLocalizable.registerYourCard.text
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    enum ValidateError : String, Error, CustomStringConvertible, ValidationError {
        case numberRequired = "O número do cartão é obrigatório"
        case nameRequired = "Um nome para o cartão é obrigatório"
        
        var description: String {
            return self.rawValue
        }
        
        var message: String {
            return self.rawValue
        }
    }
    
    private func setupColors() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        cardNumber.textColor = Palette.ppColorGrayscale500.color
        cardName.textColor = Palette.ppColorGrayscale500.color
    }
        
    private func setupInitialTexts(){
        cardNumber.isEnabled = placeholder?.cardNumber?.isEditable ?? true
        cardNumber.text = placeholder?.cardNumber?.text ?? nil
        
        cardName.isEnabled = placeholder?.cardName?.isEditable ?? true
        cardName.text = placeholder?.cardName?.text ?? nil
    }
    
    func configureValidations() {
        var cardNumberRules = ValidationRuleSet<String>()
        cardNumberRules.add(rule: ValidationRuleLength(min: 1, error: ValidateError.numberRequired))
        cardNumber.validationRules = cardNumberRules
        
        var cardNameRules = ValidationRuleSet<String>()
        cardNameRules.add(rule: ValidationRuleLength(min: 1, error: ValidateError.nameRequired))
        cardNameRules.add(rule: ValidationRuleLength(max: 20, error: ValidateError.numberRequired))
        cardName.validationRules = cardNameRules
        
        validateNext()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        model?.didFail = { [weak self] message in
            DGTicketCoordinator.shared.isLoading = false
            AlertMessage.showAlert(withMessage: message, controller: self?.navigationController)
        }
    }
    
    fileprivate func configureForm(){
        addTextField(cardNumber)
        addTextField(cardName)
        
        cardNumber.addTarget(self, action: #selector(textfieldChanged), for: .editingChanged)
        cardName.addTarget(self, action: #selector(textfieldChanged), for: .editingChanged)
        
        let underlineAttribute: [NSAttributedString.Key: Any] = [.underlineStyle: NSUnderlineStyle.single.rawValue, .foregroundColor: Palette.ppColorGrayscale500.color]
        let underlineAttributedString = NSAttributedString(string: TicketLegacyLocalizable.iDontKnowCardNumber.text, attributes: underlineAttribute)
        infoButton.addTarget(self, action: #selector(openInfo), for: .touchUpInside)
        infoButton.setAttributedTitle(underlineAttributedString, for: .normal)
    }
    
    @IBAction private func next(_ sender: Any) {
        guard validate().isValid else {
            return
        }
        
        DGTicketCoordinator.shared.isLoading = true
        model?.createCard(number: cardNumber.text ?? "", name: cardName.text ?? "")
    }
    
    @objc func openInfo() {
        model?.openInfo()
        guard
            let infoUrl = model?.selectedCity?.infoUrl,
            let url = URL(string: infoUrl) else {
            return
        }
        
        ViewsManager.presentSafariViewController(url, from: self)
    }
    
    fileprivate func validateNext(){
        actionButton?.isEnabled = validate(false).isValid
    }
    
    @objc func textfieldChanged() {
        validateNext()
    }
}

extension DGTicketCardRegisterController {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == cardName {
            model?.nameTextFieldBeginEditing()
        }
    }
}
