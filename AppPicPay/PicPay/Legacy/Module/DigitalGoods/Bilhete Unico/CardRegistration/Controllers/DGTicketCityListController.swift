import FeatureFlag
import Foundation
import UI
import UIKit

final class DGTicketCityListViewController: PPBaseViewController {
    private let tableView = UITableView()
    fileprivate let cellIdentifier = "test"
    fileprivate let model = DGTicketCardViewModel()
    fileprivate var products: [DGVoucherListItem] = []
    
    typealias Dependencies = HasFeatureManager
    private var dependencies: Dependencies = DependencyContainer()
    
    init(dependencies: Dependencies = DependencyContainer()){
        super.init(nibName: nil, bundle: nil)
    
        self.dependencies = dependencies
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupColors()
        setupConstraints()
        DGTicketCoordinator.shared.isLoading = true
        model.didLoadCities = { [weak self] in
            DGTicketCoordinator.shared.isLoading = false
            guard let self = self else {
                return
            }
            
            self.products = self.model.cities.map({ city -> DGVoucherListItem in
                return DGVoucherListItem(type: .product, product: city, data: nil, markdownText: nil)
            })
            
            self.tableView.reloadData()
        }
        
        if self.navigationController?.viewControllers.count == 1 {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.btCancel.text, style: .plain, target: self, action: #selector(close))
        }
        
        self.title = TicketLegacyLocalizable.informYourCity.text
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        model.loadCities()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        model.didFail = { [weak self] message in
            DGTicketCoordinator.shared.isLoading = false
            AlertMessage.showAlert(withMessage: message, controller: self?.navigationController)
        }
        
        model.didConfirmPermission = { [weak self] placeholder in
            DGTicketCoordinator.shared.isLoading = false
            
            guard let self = self else {
                return
            }
            
            let vc = DGTicketCardRegisterController.fromNib()
            vc.model = self.model
            vc.placeholder = placeholder
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        model.didFailOnConfirmPermission = { [weak self] alert in
            DGTicketCoordinator.shared.isLoading = false
            AlertMessage.showAlert(alert, controller: self?.navigationController)
        }
    }
    
    @objc func close() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    private func setupViews() {
        view.backgroundColor = UIColor(red: 248.0/255, green: 248.0/255, blue: 248.0/255, alpha: 1)
        tableView.backgroundColor = .clear
        let nibStore = UINib(nibName: "DGVoucherListCell", bundle: Bundle.main)
        tableView.register(nibStore, forCellReuseIdentifier: cellIdentifier)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        
        view.addSubview(tableView)
    }
    
    private func setupConstraints() {
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    private func setupColors() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
        tableView.backgroundColor = Palette.ppColorGrayscale100.color
    }
}

extension DGTicketCityListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return products[indexPath.row].getHeight()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? DGVoucherListCell else {
                return UITableViewCell()
            }
        let item = products[indexPath.row]
        guard let product = item.product else {
                return UITableViewCell()
            }
        cell.configureCell(product: product)
        cell.backgroundColor = view.backgroundColor
        return cell
    }
}

extension DGTicketCityListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DGTicketCardRegisterController.fromNib()
        model.selectedCity = model.cities[indexPath.row]
        vc.model = model
        model.citySelected()
        
        if dependencies.featureManager.isActive(.releaseMetroRioFlowBool), let alert = model.warningAlert {
            AlertMessage.showAlert(alert, controller: self.navigationController)
            return
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
