import FeatureFlag
import Foundation
import UIKit

final class DGTicketCardListViewController: PPBaseViewController {
    
    fileprivate let tableView = UITableView.init(frame: CGRect.zero, style: .grouped)
    fileprivate let editCardButton = UIButton(type: .system)
    fileprivate let cellIdentifier = "test"
    fileprivate let model = DGTicketCardViewModel()
    fileprivate var products: [DGVoucherListItem] = []
    
    /// responsible to know if this is going to screen of voucher or not, therefore it is only made
    /// the verification if it excluded or not the main card when it comes back to the screen of the voucher
    fileprivate var isBackViewController: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = TicketLegacyLocalizable.selectCard.text
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        setupViews()
        setupConstraints()
        didLoadCards()
        didDeleteCard()
        
        DGTicketCoordinator.shared.isLoading = true
        model.loadCards()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isBackViewController = true
        model.didFail = { [weak self] message in
            DGTicketCoordinator.shared.isLoading = false
            AlertMessage.showAlert(withMessage: message, controller: self?.navigationController)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if DGTicketCoordinator.shared.card == nil && isBackViewController {
            /// if the card is nil, reload the data for get principal card for voucher view.
            DGTicketCoordinator.shared.reload()
        }
    }

    private func setupViews() {
        view.backgroundColor = UIColor(red: 248.0/255, green: 248.0/255, blue: 248.0/255, alpha: 1)
        tableView.backgroundColor = .clear
        let nibStore = UINib(nibName: "DGVoucherListCell", bundle: Bundle.main)
        tableView.register(nibStore, forCellReuseIdentifier: cellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        view.addSubview(tableView)
    }
    
    private func setupConstraints() {
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

extension DGTicketCardListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if products.isEmpty {
            return 1
        }
        return products.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return products.first?.getHeight() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? DGVoucherListCell else {
                return UITableViewCell()
            }
        guard indexPath.row < products.count else {
            let product = DGVoucherProduct(id: "-1", title: TicketLegacyLocalizable.addNewCard.text, hidePrice: true)
            product.img = #imageLiteral(resourceName: "icon_round_plus")
            cell.configureCell(product: product)
            cell.backgroundColor = .clear
            return cell
        }
        guard let product = products[indexPath.row].product else {
                return UITableViewCell()
            }
        cell.configureCell(product: product)
        cell.backgroundColor = view.backgroundColor
        return cell
    }
}

extension DGTicketCardListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row < products.count else {
            isBackViewController = false
            self.navigationController?.pushViewController(DGTicketCityListViewController(), animated: true)
            return
        }
        isBackViewController = false
        DGTicketCoordinator.shared.card = products[indexPath.row].product as? DGTicketCard
        DGTicketCoordinator.shared.reload()
        navigationController?.popToRootViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader =  UIView()
        
        // Set Header Title
        let sectionTitle = UILabel(frame: CGRect(x: 10, y: 30, width: 400, height: 21))
        sectionTitle.textColor = UIColor(red:0.43, green:0.43, blue:0.44, alpha:1.00)
        sectionTitle.font = UIFont.systemFont(ofSize: 13.0)
        sectionTitle.text = TicketLegacyLocalizable.registeredCards.text
        viewHeader.addSubview(sectionTitle)
        viewHeader.addSubview(editCardButton)
        editCardButton.tag = section
        editCardButton.frame = CGRect(x: view.frame.size.width - 95, y: 27, width: 77, height: 26)
        editCardButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13.0)
        editCardButton.contentHorizontalAlignment = .right;
        editCardButton.setTitleColor(UIColor(red:0.13, green:0.76, blue:0.37, alpha:1.00), for: .normal)
        editCardButton.addTarget(self, action: #selector(self.selectForDeleteAction), for: .touchUpInside)
        
        if tableView.isEditing {
            editCardButton.setTitle(DefaultLocalizable.finish.text, for: .normal)
            editCardButton.setTitleColor(UIColor(red:0.99, green:0.05, blue:0.11, alpha:1.00), for: .normal)
        } else {
            editCardButton.setTitle(DefaultLocalizable.edit.text, for: .normal)
            editCardButton.setTitleColor(UIColor(red:0.13, green:0.76, blue:0.37, alpha:1.00), for: .normal)
        }
        
        return viewHeader
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
        selectForDeleteAction()
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        selectForDeleteAction()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row < products.count {
            return true
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            removeCard(of: indexPath)
        }
    }
}

extension DGTicketCardListViewController {
    
    func didLoadCards() {
        model.didLoadCards = { [weak self] in
            DGTicketCoordinator.shared.isLoading = false
            guard let strongSelf = self else {
            return
        }
            strongSelf.products = strongSelf.model.cards.map({ card -> DGVoucherListItem in
                return DGVoucherListItem(type: .product, product: card, data: nil, markdownText: nil)
            })
            if strongSelf.products.isEmpty {
                strongSelf.dismiss(animated: true, completion: nil)
            }
            strongSelf.tableView.reloadData()
        }
    }
    
    func didDeleteCard() {
        model.didDeletedCard = { [weak self] indexPath in
            DGTicketCoordinator.shared.isLoading = false
            guard let strongSelf = self else {
            return
        }
            guard let cardDelected = strongSelf.products[indexPath.row].product as? DGTicketCard else {
            return
        }
            
            strongSelf.products.remove(at: indexPath.row)
            strongSelf.tableView.deleteRows(at: [indexPath], with: .bottom)
            strongSelf.selectForDeleteAction()
            
            if strongSelf.products.isEmpty {
                strongSelf.dismiss(animated: true, completion: nil)
                return
            }
            
            if let cardSelected = DGTicketCoordinator.shared.card {
                /// Verify if card delected is principal card, if is principal card set for nil
                if cardSelected.id == cardDelected.id {
                    DGTicketCoordinator.shared.card = nil
                }
            }
        }
    }
    
    func removeCard(of indexPath: IndexPath) {
        guard let product: DGProduct = products[indexPath.row].product else {
            return
        }
        let descriptionText = "\(TicketLegacyLocalizable.removeTransportCard.text) \"\(product.description ?? "")\"?"
        let buttonRemove = Button(title: DefaultLocalizable.remove.text, type: .destructive) { [weak self] popupController, _ in
            DGTicketCoordinator.shared.isLoading = true
            self?.model.deleteCard(of: indexPath)
            popupController.dismiss(animated: true)
        }
        let buttonDismiss = Button(title: DefaultLocalizable.notNow.text, type: .clean, action: .close)
        
        let alert = Alert(with: TicketLegacyLocalizable.removeCard.text, text: descriptionText, buttons: [buttonRemove, buttonDismiss])
        alert.showCloseButton = true
        
        AlertMessage.showAlert(alert, controller: self)
    }
    
    @objc func selectForDeleteAction() {
        if editCardButton.currentTitle == DefaultLocalizable.edit.text {
            tableView.setEditing(true, animated: true)
            editCardButton.setTitle(DefaultLocalizable.finish.text, for: .normal)
            editCardButton.setTitleColor(UIColor(red:0.99, green:0.05, blue:0.11, alpha:1.00), for: .normal)
        } else {
            tableView.setEditing(false, animated: true)
            editCardButton.setTitle(DefaultLocalizable.edit.text, for: .normal)
            editCardButton.setTitleColor(UIColor(red:0.13, green:0.76, blue:0.37, alpha:1.00), for: .normal)
        }
    }
}

