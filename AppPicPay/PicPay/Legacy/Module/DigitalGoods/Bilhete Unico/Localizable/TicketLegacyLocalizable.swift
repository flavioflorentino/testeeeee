enum TicketLegacyLocalizable: String, Localizable {
    case selectCard
    case addNewCard
    case registeredCards
    case removeTransportCard
    case removeCard
    case registerYourCard
    case iDontKnowCardNumber
    case informYourCity
    case doesNotHaveAnyProductsAvailable
    case NumberShares
    case rate
    case singleTicket
    case problemCreatingTransaction
    case cardDataCouldNotBeLoaded
    case value
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .ticketLegacy
    }
}
