//
//  DGTicketFlow.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 27/03/18.
//

import Foundation
import SwiftyJSON

enum DGTicketScreenType: String {
    case list = "tela_de_listagem"
    case main = "transportpass_select_card"
    case bolinhas = "tela_de_bolinhas"
    case valueSelection = "select_value"
    case unitSelection = "select_units"
}

final class DGTicketItemParams: BaseApiResponse {
    var maxValue: Double?
    var minValue: Double?
    var valuePerItem: Double?
    var maxItems: Int?
    var card: DGTicketCard?
    
    required init?(json: JSON) {
        self.maxValue = json["max_value"].double
        self.minValue = json["min_value"].double
        self.valuePerItem = json["value_per_item"].double
        self.maxItems = json["items_max"].int
        self.card = DGTicketCard(json: json["selected_card"])
        
        if maxValue == nil && card == nil {
            return nil
        }
    }
}

protocol DGTicketPayable {
    var payload: [String: Any]? { get }
    var transactionData: [String: Any]? { get }
    var price: Double { get }
}

final class DGTicketItem: BaseApiResponse, DGProduct, DGTicketPayable {
    // DGListItem
    var id: String = ""
    var name: String = ""
    var description: String? = ""
    var disclaimerMarkdown: String?
    var hidePrice: Bool = true
    var price: Double = 0.0
    var imageUrl: String?
    var img: UIImage?
    
    var isRangeType: Bool {
        return false
    }
    
    var valueMax: Double = 0.0
    var valueMin: Double = 0.0
    var customAmount: Double?
    
    var payload: [String: Any]?
    var transactionData: [String: Any]?
    
    // Custom
    var next: DGTicketFlow?
    
    required init?(json: JSON) {
        self.name = json["item"].string ?? ""
        self.imageUrl = json["icon_url"].string
        self.price = json["value"].double ?? 0.0
        self.valueMax = price
        self.valueMin = price
        self.hidePrice = price <= 0.0
        self.description = json["description"].string
        self.next = DGTicketFlow(json: json["next"])
        
        self.payload = json["payload"].dictionaryObject
        self.transactionData = json["transaction"].dictionaryObject
    }
}

final class DGTicketFlow: BaseApiResponse, DGTicketPayable {
    var type: DGTicketScreenType?
    var image: String?
    var items: [DGTicketItem] = []
    var params: DGTicketItemParams?
    
    var payload: [String: Any]?
    var transactionData: [String: Any]?
    var price: Double = 0.0
    
    var title: String?
    var navigationTitle: String?
    
    required init?(json: JSON) {
        if let typeString = json["type"].string {
            type = DGTicketScreenType(rawValue: typeString)
        }
        
        self.image = json["img_url"].string
        self.navigationTitle = json["navigation_title"].string
        self.title = json["title"].string
        
        self.params = DGTicketItemParams(json: json["params"])
        
        self.payload = json["payload"].dictionaryObject
        self.transactionData = json["transaction"].dictionaryObject
        
        for item in json["items"].arrayValue {
            guard let obj = DGTicketItem(json: item) else { continue }
            items.append(obj)
        }
    }
}
