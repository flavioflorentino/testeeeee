//
//  DGTicketViewModel.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 28/03/18.
//

import Foundation

final class DGTicketViewModel: NSObject {
    func load(card: DGTicketCard?, completion: @escaping ((PicPayResult<DGTicketFlow>) -> Void) ) {
        DGTicketApi().getFlow(card: card, completion: { result in
            DispatchQueue.main.async {
                completion(result)
            }
        })
    }
}
