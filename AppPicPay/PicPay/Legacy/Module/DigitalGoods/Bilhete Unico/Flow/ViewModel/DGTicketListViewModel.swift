//
//  DGTicketListViewModel.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 28/03/18.
//

import Foundation
import SwiftyJSON
import AnalyticsModule

final class DGTicketListViewModel: DGListViewModel, DGTicketCoordinated {
    var params: DGTicketItemParams?
    var analytics: Analytics
    var items: [DGTicketItem]? {
        didSet {
            guard let items = items else {
            return
        }
            products = items
            isLoading = false
            productsUpdate?()
        }
    }
    
    init(dg: DGItem, analytics: Analytics = Analytics.shared) {
        self.analytics = analytics
        super.init()
        self.digitalGood = dg
    }
    
    
    override func goToPayment(from: UIViewController) {
        if let item = product as? DGTicketItem {
            DGTicketCoordinator.shared.next(item: item, from: from)
        } else if let product = product as? DGTicketCard {
            if let cityId = product.cityId {
                analytics.log(DGTicketAnalytics.linkChangeCard(cityId: cityId))
            }
            
            DGTicketCoordinator.shared.changeCard(from: from)
        }
    }
}
