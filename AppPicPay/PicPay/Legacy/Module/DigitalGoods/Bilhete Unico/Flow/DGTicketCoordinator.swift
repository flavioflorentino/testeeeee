import AnalyticsModule
import Core
import CoreLegacy
import FeatureFlag
import Foundation
import PCI
import SwiftyJSON
import UI
import UIKit

protocol DGTicketCoordinated {
    var params: DGTicketItemParams? { get set }
    var items: [DGTicketItem]? { get set }
}

final class DGTicketCoordinator: NSObject {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    private let service: TransitPassServiceProtocol
    
    static let shared = DGTicketCoordinator(with: TransitPassService())
    let model = DGTicketViewModel()
    var loadingView = Loader.getLoadingView(DefaultLocalizable.wait.text) ?? UIView()
    
    var digitalGoodId: String?
    var sellerId: String?
    
    var analytics: Analytics = Analytics.shared

    var navigationController: UINavigationController? {
        set(newValue) {
            newValue?.view.addSubview(loadingView)
            loadingView.isHidden = true
            internalNavigationController = newValue
        }
        get {
            return internalNavigationController
        }
    }
    
    var isLoading = false {
        didSet {
            loadingView.isHidden = !isLoading
        }
    }
    private var internalNavigationController: UINavigationController?
    private var periodSelected: (isEnable: Bool, presenter: ValueOptionViewPresenting)?
    
    private lazy var valueOptionViewController: ValueOptionViewController = {
        let controller = ValueOptionViewController(alignment: .center, highlight: .topValue)
        controller.dataSource = self
        controller.delegate = self
        return controller
    }()
    
    private var valueOptionsPresenters: [ValueOptionViewPresenting] {
        return [
            ValueOptionViewPresenter(title: "7", description: DefaultLocalizable.days.text),
            ValueOptionViewPresenter(title: "30", description: DefaultLocalizable.days.text)
        ]
    }
    
    var flow: DGTicketFlow?
    var card: DGTicketCard?
    
    init(with: TransitPassServiceProtocol) {
        service = with
    }
    
    func getVcFor(type: DGTicketScreenType) -> UIViewController {
        switch type {
        case .list, .main:
            let voucher: DGVoucherController = UIStoryboard.viewController(.digitalGoodsCode)
            
            voucher.dataSource = self
            if type != .main {
                voucher.shouldHideBanner = true
                voucher.statusBarStyle = UIStatusBarStyle.default
            }
            
            // Creates a custom DGItem
            let digitalGood = DGItem(id: digitalGoodId ?? "")
            digitalGood.sellerId = sellerId ?? ""
            digitalGood.bannerImgUrl = flow?.image
            digitalGood.displayType = .list
            digitalGood.name = flow?.navigationTitle ?? ""
            
            let voucherModel = DGTicketListViewModel(dg: digitalGood)
            voucherModel.items = flow?.items
            voucher.screenTitle = flow?.navigationTitle ?? ""
            
            if flow?.items.count > 0 {
                voucher.listSectionTitle = (flow?.title ?? "")
            }else{
                voucher.listSectionTitle = TicketLegacyLocalizable.doesNotHaveAnyProductsAvailable.text
                voucher.listSectionFont = UIFont.boldSystemFont(ofSize: 12)
            }
            
            voucher.model = voucherModel
            
            return voucher
        case .bolinhas:
            let circularSelection: DGTicketCircularSelectionController = DGTicketCircularSelectionController.fromNib()
            guard let items = flow?.items else { return UIViewController() }
            circularSelection.setup(items: items, screenTitle: flow?.title)
            circularSelection.title = flow?.navigationTitle
            
            return circularSelection
        case .unitSelection:
            let vc = DGTicketSelectQuantity()
            guard
                let pricePerItem = flow?.params?.valuePerItem,
                let max = flow?.params?.maxItems else { return UIViewController() }
            
            vc.setup(pricePerItem: pricePerItem, screenTitle: flow?.title ?? "", max: max, next: { [weak self] quantity in
                self?.next(item: nil, quantity: quantity, from: vc)
            })
            vc.title = flow?.navigationTitle
            return vc
        case .valueSelection:
            guard
                let min = flow?.params?.minValue,
                let max = flow?.params?.maxValue else { return UIViewController() }
            let nextController = DGVoucherOtherValueController.controllerFromStoryboard(.digitalGoodsCode)
            nextController.allowCents = FeatureManager.isActive(.ticketCents)
            nextController.setup(screenTitle: flow?.navigationTitle ?? "", title: flow?.title ?? "", max: max, min: min, amountChange: nil, completion: { [weak self] vc in
                self?.next(item: nil, quantity: nil, value: vc.currentAmount, from: vc)
                
                if let cityId = self?.card?.cityId {
                    self?.analytics.log(DGTicketAnalytics.buttonSignRecharge(cityId: cityId, currentAmmount: vc.currentAmount))
                }
                
            })
            nextController.title = flow?.navigationTitle
            return nextController
        }
    }
    
    private func paymentScreenDetails(
        model: DGPaymentViewModel,
        transactionData: [String: Any],
        quantity: Int?,
        value: Double?
    ) -> [DGPaymentDetailsBase] {
        
        var details: [DGPaymentDetailsBase] = []
        if let detailsData = transactionData["details"] as? [[String: String]]{
            for detail in detailsData {
                guard
                    let title = detail["title"],
                    let value = detail["value"] else { continue }
                details.append(DGPaymentDetailsItem(name: title, detail: value))
            }
        }
        
        if let quantity = quantity {
            details.append(DGPaymentDetailsItem(name: TicketLegacyLocalizable.NumberShares.text, detail: String(quantity)))
        }
        
        if let fixedFee = transactionData["fixed_fee"] as? Double,
            model.paymentManager?.cardTotal().doubleValue > 0 {
            model.paymentManager?.fixedFee = NSDecimalNumber(value: fixedFee)
            details.append(DGPaymentDetailsItem(name: TicketLegacyLocalizable.rate.text, detail: CurrencyFormatter.brazillianRealString(from: NSNumber(value: fixedFee))))
        }else{
            model.paymentManager?.fixedFee = NSDecimalNumber(value: 0)
        }
        
        if let disclaimer = transactionData["disclaimer"] as? String {
            details.append(DGPaymentImportantDetailItem(text: disclaimer))
        }
        
        return details
    }
    
    private func goToPayment(from: UIViewController, payable: DGTicketPayable?, quantity: Int?, value: Double?) {
        guard let sellerId = sellerId else {
            return
        }
        let paymentScreen = DGPaymentViewController.controllerFromStoryboard()
        let transactionData = payable?.transactionData ?? [:]

        var price = payable?.price ?? 0.0
        let sellerName = transactionData["service_name"] as? String ?? TicketLegacyLocalizable.singleTicket.text
        let sellerImage = transactionData["icon_url"] as? String
        
        if let quantity = quantity,
            let valuePerItem = flow?.params?.valuePerItem{
            price = Double(quantity) * valuePerItem
        }
        
        if let value = value {
            price = value
        }
        
        let paymentModel = DGPaymentViewModel(
            amount: price,
            type: .transitPass,
            sellerName: sellerName,
            sellerImageUrl: sellerImage,
            sellerId: sellerId,
            detailInfo: [],
            dependencies: DependencyContainer()
        )
        if var payload = payable?.payload {
            payload["value"] = price
            payload["quantity"] = quantity ?? NSNull()
            paymentModel.additionalPayload = payload
        }
        paymentScreen.title = DefaultLocalizable.voucherPaymentTitle.text
        let detailInfo = paymentScreenDetails(
            model: paymentModel,
            transactionData: transactionData,
            quantity: quantity,
            value: value
        )
        
        paymentModel.detailInfo = detailInfo
        
        let paymentManager = PPPaymentManager()
        
        paymentManager?.subtotal = NSDecimalNumber(value: price)
        guard let unwrappedPaymentManager = paymentManager else {
            AlertMessage.showAlert(withMessage: TicketLegacyLocalizable.problemCreatingTransaction.text, controller: navigationController)
            return
        }
        paymentScreen.setup(paymentManager: unwrappedPaymentManager, model: paymentModel, delegate: self)
        
        // Add the right button to the payment screen + show SFSafariVC on tap
        if let infoUrl = transactionData["url_webview"] as? String, infoUrl != "" {
            paymentScreen.setRightButton()
            paymentScreen.rightBarButtonAction = { controller in
                guard let url = URL(string:infoUrl) else {
            return
        }

                ViewsManager.presentSafariViewController(url, from: controller)
            }
        }
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name.Payment.methodChange, object: nil)
        NotificationCenter.default.addObserver(forName: Notification.Name.Payment.methodChange, object: nil, queue: nil) { [weak self] (_) in
            guard let self = self else {
                return
            }
            
            let detailInfo = self.paymentScreenDetails(
                model: paymentModel,
                transactionData: transactionData,
                quantity: quantity,
                value: value
            )
            paymentModel.detailInfo = detailInfo
        }
        
        let digitalGood = DigitalGoodsPayment(
            title: sellerName,
            image: sellerImage,
            link: transactionData["url_webview"] as? String,
            payeeId: sellerId,
            value: price,
            paymentType: .transitPass
        )
        
        if FeatureManager.isActive(.newTransitPass) {
            openNewPaymentViewController(
                from: from,
                additionalPayload: paymentModel.additionalPayload,
                digitalGood: digitalGood,
                paymentDetails: paymentModel.detailInfo
            )
            return
        }
        
        from.navigationController?.pushViewController(paymentScreen, animated: true)
    }
    
    
    private func openNewPaymentViewController(
        from: UIViewController,
        additionalPayload: [String: Any],
        digitalGood: DigitalGoodsPayment,
        paymentDetails: [DGPaymentDetailsBase]
    ) {
        guard
            let transitPass = parseToTransitPass(additionalPayload: additionalPayload),
            let digitalGoodId = digitalGoodId,
            let sellerId = sellerId
            else {
                return
        }
        
        let model = DigitalGoodsTransitPass(digitalGoodId: digitalGoodId, sellerId: sellerId)
        let service = DigitalGoodsTransitPassService(model: model, transitPass: transitPass)
        let orchestrator = DigitalGoodsPaymentOrchestrator(items: digitalGood, paymentDetails: paymentDetails, service: service)
        navigationController?.pushViewController(orchestrator.paymentViewController, animated: true)
    }
    
    private func parseToTransitPass(additionalPayload: [String: Any]) -> TransitPass? {
        guard let data = additionalPayload.toData() else {
            return nil
        }
        
        return try? JSONDecoder.decode(data, to: TransitPass.self)
    }
    
    func next(item: DGTicketItem?, quantity: Int? = nil, value: Double? = nil, from origin: UIViewController) {
        guard let next = item?.next,
            let type = next.type else {
            goToPayment(
                from: origin,
                payable: item ?? flow,
                quantity: quantity,
                value: value
            )
            return
        }
        
        self.flow = next
                
        let vc = getVcFor(type: type)
        origin.navigationController?.pushViewController(viewController: vc, animated: true, completion: {
            origin.navigationController?.setNavigationBarHidden(false, animated: true)
        })
    }
    
    func changeCard(from: UIViewController) {
        from.navigationController?.pushViewController(DGTicketCardListViewController(), animated: true)
    }
    
    func reload() {
        loadingView.isHidden = false
        model.load(card: card) { [weak self] result in
            self?.loadingView.isHidden = true
            guard let navigationController = self?.navigationController else {
            return
        }
            switch result {
            case .success(let flow):
                guard let digitalGoodId = self?.digitalGoodId,
                    let sellerId = self?.sellerId else {
            return
        }
                DGTicketCoordinator.shared.start(parent: navigationController, digitalGoodId: digitalGoodId, sellerId: sellerId, card: self?.card, flow: flow)
            case .failure(_):
                AlertMessage.showAlert(withMessage: TicketLegacyLocalizable.cardDataCouldNotBeLoaded.text, controller: navigationController)
            }
        }
    }
    
    func start(parent: UINavigationController, digitalGoodId: String, sellerId: String, card: DGTicketCard? = nil, flow: DGTicketFlow) {
        self.navigationController = parent
        self.flow = flow
        self.card = card
        self.digitalGoodId = digitalGoodId
        self.sellerId = sellerId
        
        var vc: UIViewController = DGTicketCityListViewController()
    
        if let type = flow.type {
            vc = self.getVcFor(type: type)
        }
    
        navigationController?.viewControllers = [vc]
        return
    }
}

extension DGTicketCoordinator: DGVoucherControllerDataSource {
    func tableStructure(vc: DGVoucherController) -> [DGVoucherListItem] {
        var tableViewStructure: [DGVoucherListItem] = []
        
        if (flow?.type != .main) {
            if let title = flow?.title {
                tableViewStructure.append(DGVoucherListItem(type: .centeredTitle, data: title))
            }
            
            tableViewStructure.append(contentsOf: vc.productsStructure())
            tableViewStructure.append(contentsOf: vc.descriptionsStructure())
            return tableViewStructure
        }
        
        tableViewStructure.append(contentsOf: vc.tableHeaderStructure())
        
        if let card = flow?.params?.card {
            self.card = card
            tableViewStructure.append(DGVoucherListItem(type: .clearBgCell, product: card))
        } else {
            self.card = nil
        }
        
        tableViewStructure.append(contentsOf: vc.productsHeaderStructure())
        tableViewStructure.append(contentsOf: vc.productsStructure())
        tableViewStructure.append(contentsOf: vc.descriptionsStructure())
        
        return tableViewStructure
    }
}

// MARK: Payment request construction
extension DGTicketCoordinator: DGPaymentDelegate {
    func didPerformPayment(
        with manager: PPPaymentManager,
        cvv: String?,
        additionalPayload: [String: Any],
        pin: String,
        biometry: Bool,
        privacyConfig: String,
        someErrorOccurred: Bool,
        completion: @escaping (WrapperResponse) -> Void
    ) {
        guard let digitalGoodId = digitalGoodId,
            let sellerId = sellerId else {
            return
        }
        
        let request = DGTicketPayRequest()
        
        request.paymentManager = manager
        request.pin = pin
        request.biometry = biometry
        request.privacyConfig = privacyConfig
        request.someErrorOccurred = someErrorOccurred
        request.sellerId = sellerId
        request.digitalGoodId = digitalGoodId
        
        if FeatureManager.isActive(.pciTransitPass) {
            createTransactionPCI(
                manager: manager,
                request: request,
                password: pin,
                additionalPayload:
                additionalPayload,
                cvv: cvv,
                completion: completion
            )
            return
        }
        
        DGTicketApi().pay(request: request, payload: additionalPayload, pin: pin, completion)
    }
    
    private func createTransactionPCI(
        manager: PPPaymentManager,
        request: DGTicketPayRequest,
        password: String,
        additionalPayload: [String : Any],
        cvv: String?,
        completion: @escaping (WrapperResponse) -> Void
    ) {
        guard let transitPassPayload = createTransitPassPayload(manager: manager, request: request, additionalPayload: additionalPayload) else {
            let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            completion(PicPayResult.failure(error))
            return
        }
        let cvv = createCVVPayload(manager: manager, cvv: cvv)
        let payload = PaymentPayload<DigitalGoodsPayload<TransitPass>>(cvv: cvv, generic: transitPassPayload)
        service.createTransaction(password: password, payload: payload, isNewArchitecture: false) { result in
            DispatchQueue.main.async {
                let mappedResult = result
                    .map { BaseApiGenericResponse(dictionary: $0.json) }
                    .mapError { $0.picpayError }
                               
                completion(mappedResult)
            }
        }
    }
    
    private func createTransitPassPayload(manager: PPPaymentManager, request: DGTicketPayRequest, additionalPayload: [String: Any]) -> DigitalGoodsPayload<TransitPass>? {
        guard let params = try? request.toParams(digitalGoodsPayload: additionalPayload),
            let data = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) else {
                return nil
        }
        return try? JSONDecoder.decode(data, to: DigitalGoodsPayload<TransitPass>.self)
    }

    private func createCVVPayload(manager: PPPaymentManager, cvv: String?) -> CVVPayload? {
        guard let cvv = cvv else {
            return createLocalStoreCVVPayload(manager: manager)
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func createLocalStoreCVVPayload(manager: PPPaymentManager) -> CVVPayload? {
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        guard manager.cardTotal()?.doubleValue != .zero,
              let cvv = dependencies.keychain.getData(key: KeychainKeyPF.cvv(id)) else {
                return nil
        }
        
        return CVVPayload(value: cvv)
    }
}

extension DGTicketCoordinator: ValueOptionViewDataSource {
    func numberOfItems(in valueOptionViewController: ValueOptionViewController) -> Int {
        return valueOptionsPresenters.count
    }
    
    func valueOptionViewController(_ valueOptionViewController: ValueOptionViewController, valueOptionForItemAt item: Int) -> ValueOptionViewPresenting {
        return valueOptionsPresenters[item]
    }
}

extension DGTicketCoordinator: ValueOptionViewDelegate {
    func didSelectItem(_ presenter: ValueOptionViewPresenting) {
        periodSelected = (isEnable: false, presenter: presenter)
    }
}
