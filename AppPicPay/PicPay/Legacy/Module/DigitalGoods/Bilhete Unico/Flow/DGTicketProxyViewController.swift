import Core
import FeatureFlag
import Foundation
import UI

final class DGTicketProxyViewController: PPBaseViewController {
    typealias TicketProxyDependencies = HasFeatureManager & HasMainQueue

    private let digitalGoodId: String
    private var sellerId: String
    private let api: DGTicketApi
    private let dependencies: TicketProxyDependencies

    init(digitalGoodId: String,
         sellerId: String,
         api: DGTicketApi = .init(),
         dependencies: TicketProxyDependencies = DependencyContainer()) {
        self.digitalGoodId = digitalGoodId
        self.sellerId = sellerId
        self.api = api
        self.dependencies = dependencies
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        let loader = UIActivityIndicatorView(style: .gray)
        loader.startAnimating()
        view.addSubview(loader)
        loader.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.height.equalTo(20)
            make.width.equalTo(20)
        }
        
        view.backgroundColor = Palette.ppColorGrayscale000.color
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.btCancel.text, style: .plain, target: self, action: #selector(close))

        getSellerIdIfNeeded() { [weak self] in
            self?.getFlow()
        }
    }

    private func getSellerIdIfNeeded(completion: @escaping () -> Void) {
        guard sellerId.isEmpty || sellerId == "0" else {
            completion()
            return completion()
        }

        api.getInfo(id: digitalGoodId) { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .success(let info):
                self.sellerId = "\(info.sellerId)"
                completion()
            case .failure:
                self.close()
            }
        }
    }

    private func getFlow() {
        api.getFlow(card: nil) { [weak self] result in
            self?.dependencies.mainQueue.async {
                guard let self = self else { return }

                switch result {
                case .success(let flow):
                    self.navigateToFlow(flow)
                case .failure:
                    self.close()
                }
            }
        }
    }
    
    @objc func close() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }

    private func navigateToFlow(_ flow: DGTicketFlow) {
        guard let nav = self.navigationController else { return }

        DGTicketCoordinator.shared.start(parent: nav,
                                         digitalGoodId: self.digitalGoodId,
                                         sellerId: self.sellerId,
                                         flow: flow)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
