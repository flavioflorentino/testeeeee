//
//  DGTicketCircularSelectionController.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 11/04/18.
//


import UIKit

final class DGTicketCircularSelectionController: PPBaseViewController {
    static func fromNib() -> DGTicketCircularSelectionController {
        return controllerFromStoryboard(.digitalGoodsTicket)
    }
    
    @IBOutlet weak var valueList: CircleItemsView!
    @IBOutlet weak var titleLabel: UILabel!
    
    let gridValueIdentifier = "Value"
    let gridOtherValueIdentifier = "OtherValue"
    var items: [DGTicketItem] = []
    
    var screenTitle: String?
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        
        if let screenTitle = screenTitle {
            self.titleLabel.text = screenTitle
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setup(items: [DGTicketItem], screenTitle: String?) {
        self.items = items
        self.screenTitle = screenTitle
    }
    
    // MARK: - Internal Methdos
    
    func configureCollectionView(){
        valueList.dataSource = self
        valueList.delegate = self
        
        valueList.registerCell(name: String(describing: DGVoucherValueCell.self), cellIdentifier: gridValueIdentifier)
        valueList.registerCell(name: String(describing: DGVoucherOtherValueCell.self), cellIdentifier: gridOtherValueIdentifier)
        valueList.update(totalCount: items.count)
        
    }
    
    // MARK: - User Actions
    func next() {

    }
}

extension DGTicketCircularSelectionController: UICollectionViewDelegateFlowLayout {
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        
        (cell as? SelectableCell)?.isCellSelected = true
        
        DGTicketCoordinator.shared.next(item: items[indexPath.row], from: self)
    }
}

extension DGTicketCircularSelectionController: CircleViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let product = items[indexPath.row]
        
        // If it's a value
        if product.name != DefaultLocalizable.other.text {
            // Dequeue the cell
            guard
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: gridValueIdentifier, for: indexPath) as? DGVoucherValueCell
                else { return UICollectionViewCell() }
            
            cell.configureCell(product: product)
            return cell
        }
        
        // It's not a fixed value
        return collectionView.dequeueReusableCell(withReuseIdentifier: gridOtherValueIdentifier, for: indexPath)
    }
}
