//
//  DGTicketSelectQuantity.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 27/03/18.
//

import CoreLegacy
import Foundation
import UI

final class DGTicketSelectQuantity: UIViewController {
    fileprivate let container = UIView()
    fileprivate let titleLabel = UILabel()
    fileprivate var minusButton = UIButton()
    fileprivate var plusButton = UIButton()
    fileprivate let priceLabel = UILabel()
    fileprivate let quantityLabel = UILabel()
    fileprivate let actionButton = UIPPButton()
    
    var count = 1 {
        didSet {
            minusButton.isEnabled = count > 1
            plusButton.isEnabled = count < max ?? 1

            guard let pricePerItem = pricePerItem else {
            return
        }
            self.quantityLabel.text = "\(count)"
            
            setPrice(pricePerItem * Double(count))
        }
    }
    
    private var waitToAutoUpdateTimer: Timer?
    private var autoUpdatetimer: Timer?
    private var isIncrementing = false
    private var screenTitle: String?
    
    fileprivate var pricePerItem: Double?
    fileprivate var max: Int?
    fileprivate var didFinish: ((Int) -> ())?
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        setupViews()
        setupConstraints()
    }
    
    func setup(pricePerItem: Double, screenTitle: String, max: Int, next: @escaping ((Int) -> ())) {
        self.pricePerItem = pricePerItem
        self.max = max
        self.didFinish = next
        self.screenTitle = screenTitle
        
        setPrice(pricePerItem)
    }
    
    private func setupViews() {
        view.backgroundColor = UIColor(red: 245/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1)
        // Container
        view.addSubview(container)
        
        // Title
        container.addSubview(titleLabel)
        titleLabel.text = screenTitle
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        titleLabel.textColor = UIColor(red: 29/255.0, green: 29/255.0, blue: 29/255.0, alpha: 1)
        
        // Plus/minus buttons
        plusButton = setupQuantityUpdateButton()
        minusButton = setupQuantityUpdateButton()
        container.addSubview(minusButton)
        container.addSubview(plusButton)
        
        // Minus
        minusButton.setImage(#imageLiteral(resourceName: "ico_gray_minus"), for: .disabled)
        minusButton.setImage(#imageLiteral(resourceName: "ico_green_minus"), for: .normal)
        
        // Plus
        plusButton.setImage(#imageLiteral(resourceName: "ico_green_plus"), for: .normal)
        plusButton.setImage(#imageLiteral(resourceName: "ico_gray_plus"), for: .disabled)
        plusButton.addTarget(self, action: #selector(stopTimers), for: .touchUpInside)
        plusButton.addTarget(self, action: #selector(stopTimers), for: .touchUpOutside)
        plusButton.addTarget(self, action: #selector(stopTimers), for: .touchDragOutside)
        plusButton.addTarget(self, action: #selector(incrementDown), for: .touchDown)
        
        minusButton.addTarget(self, action: #selector(stopTimers), for: .touchUpInside)
        minusButton.addTarget(self, action: #selector(stopTimers), for: .touchUpOutside)
        minusButton.addTarget(self, action: #selector(stopTimers), for: .touchDragOutside)
        minusButton.addTarget(self, action: #selector(decrementDown), for: .touchDown)
        
        // Quantity
        container.addSubview(quantityLabel)
        quantityLabel.textColor = UIColor(red: 29/255.0, green: 29/255.0, blue: 29/255.0, alpha: 1)
        quantityLabel.font = UIFont.systemFont(ofSize: 60)
        quantityLabel.text = "1"
        
        // Confirm
        container.addSubview(actionButton)
        actionButton.addTarget(self, action: #selector(didTapNext), for: .touchUpInside)
        actionButton.setTitle(DefaultLocalizable.btConfirm.text, for: .normal)
        actionButton.normalTitleColor = Palette.ppColorGrayscale000.color
        actionButton.normalBackgrounColor = Palette.ppColorBranding300.color
        actionButton.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        actionButton.cornerRadius = 22
        
        // Price
        container.addSubview(priceLabel)
    }
    
    private func setupConstraints() {
        container.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.centerX.equalToSuperview()
        }
        
        minusButton.snp.makeConstraints { make in
            make.left.equalTo(view.snp.left).offset(30)
            make.left.equalToSuperview()
            make.size.equalTo(CGSize(width: 72, height: 72))
            make.top.equalTo(titleLabel.snp.bottom).offset(40)
        }
        
        plusButton.snp.makeConstraints { make in
            make.right.equalTo(view.snp.right).offset(-30)
            make.right.equalToSuperview()
            make.size.equalTo(CGSize(width: 72, height: 72))
            make.top.equalTo(titleLabel.snp.bottom).offset(40)
        }
        
        quantityLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalTo(plusButton)
        }
        
        actionButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(priceLabel.snp.bottom).offset(30)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(48)
            make.bottom.equalToSuperview()
        }
        
        priceLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(quantityLabel.snp.bottom).offset(30)
        }
    }
    
    private func setupQuantityUpdateButton() -> UIButton {
        let button = UIButton()
        button.backgroundColor = Palette.ppColorGrayscale000.color
        button.layer.cornerRadius = 36
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.borderWidth = 1
        
        return button
    }
    
    // MARK: Increment/decrement
    @objc func incrementDown() {
        increment()
        stopTimers()
        isIncrementing = true
        waitToAutoUpdateTimer = Timer.scheduledTimer(timeInterval: 0.75, target: self, selector: #selector(self.startAutoUpdate), userInfo: nil, repeats: false)
    }
    
    @objc func decrementDown() {
        decrement()
        stopTimers()
        isIncrementing = false
        waitToAutoUpdateTimer = Timer.scheduledTimer(timeInterval: 0.75, target: self, selector: #selector(self.startAutoUpdate), userInfo: nil, repeats: false)
    }
    
    func getAutoUpdateSelector() -> Selector {
        return isIncrementing ? #selector(increment) : #selector(decrement)
    }
    
    @objc func startAutoUpdate() {
        stopTimers()
        autoUpdatetimer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: getAutoUpdateSelector(), userInfo: nil, repeats: true)
    }
    
    @objc func stopTimers() {
        waitToAutoUpdateTimer?.invalidate()
        autoUpdatetimer?.invalidate()
        waitToAutoUpdateTimer = nil
        autoUpdatetimer = nil
    }
    
    @objc func increment() {
        guard let max = max,
            count < max else {
            return
        }
        self.count += 1
    }
    @objc func decrement() {
        guard count > 1 else {
            return
        }
        self.count -= 1
    }
    
    // MARK: Helpers
    private func setPrice(_ price: Double) {
        priceLabel.font = UIFont.systemFont(ofSize: 14)
        priceLabel.textColor = Palette.ppColorBranding300.color
        
        let label = TicketLegacyLocalizable.value.text
        let string = label + CurrencyFormatter.brazillianRealString(from: NSNumber(value: price))
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttribute(.foregroundColor, value: UIColor(red: 29/255.0, green: 29/255.0, blue: 29/255.0, alpha: 1), range: NSRange(location: 0, length: label.length))
        
        priceLabel.attributedText = attributedString
        
    }
    
    // MARK: Actions
    @objc func didTapNext() {
        didFinish?(count)
    }
}
