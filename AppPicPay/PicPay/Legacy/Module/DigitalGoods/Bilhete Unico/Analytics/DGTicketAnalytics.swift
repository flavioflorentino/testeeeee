import Foundation
import AnalyticsModule

enum DGTicketAnalytics: AnalyticsKeyProtocol {
    private struct Keys {
        static let cityId = "city_id"
        static let cityName = "city_name"
        static let currentAmount = "current_amount"
        static let errorTitle = "error_title"
        static let errorCode = "error_code"
        static let errorMessage = "error_message"
    }
    
    case selectecCity(cityId: String, cityName: String)
    case buttonSignPopupShare(cityId: String, cityName: String)
    case linkCancelPopupShare(cityId: String, cityName: String)
    case buttonSignCard(cityId: String, cityName: String)
    case linkUnknownCard(cityId: String, cityName: String)
    case inputNickNameCard(cityId: String, cityName: String)
    case linkChangeCard(cityId: String)
    case buttonSignRecharge(cityId: String, currentAmmount: Double)
    case buttonOkConfirmPermissionError(cityId: String, cityName: String, errorTitle: String, errorCode: Int, errorMessage: String)
    
    var name : String{
        switch self {
        case .selectecCity:
            return "DGTicket - Selected City"
        case .buttonSignPopupShare:
            return "DGTicket - Button Sign Popup Share"
        case .linkCancelPopupShare:
            return "DGTicket - Link Cancel Popup Share"
        case .buttonSignCard:
            return "DGTicket - Button Sign Card"
        case .linkUnknownCard:
            return "DGTicket - Link Unknown Card"
        case .inputNickNameCard:
            return "DGTicket - Input Nick Name Card"
        case .linkChangeCard:
            return "DGTicket - Link Change Card"
        case .buttonSignRecharge:
            return "DGTicket - Button Sign Recharge"
        case .buttonOkConfirmPermissionError:
            return "DGTicket - Button Ok Confirm Permission Error"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .selectecCity(let cityId, let cityName),
             .buttonSignPopupShare(let cityId, let cityName),
             .linkCancelPopupShare(let cityId, let cityName),
             .buttonSignCard(let cityId, let cityName),
             .linkUnknownCard(let cityId, let cityName),
             .inputNickNameCard(let cityId, let cityName):
            return [Keys.cityId: cityId, Keys.cityName: cityName]
        case .linkChangeCard(let cityId):
            return [Keys.cityId: cityId]
        case .buttonSignRecharge(let cityId, let currentAmount):
            return [Keys.cityId: cityId, Keys.currentAmount: currentAmount]
        case .buttonOkConfirmPermissionError(let cityId, let cityName, let errorTitle, let errorCode, let errorMessage):
            return [
                Keys.cityId: cityId,
                Keys.cityName: cityName,
                Keys.errorTitle: errorTitle,
                Keys.errorCode: errorCode,
                Keys.errorMessage: errorMessage
            ]
            
        }
    }
    
    private var providers: [AnalyticsProvider] {
        return [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
