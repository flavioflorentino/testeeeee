import Core
import Foundation

enum DigitalGoodsTicketEndpoint: ApiEndpointExposable {
    case flow(cardId: String?)
    case cities
    case cards
    case createCard(card: String, name: String, cityId: String)
    case deleteCard(id: String)
    case confirmPermission(cityId: String)
    case pay(params: [String: Any], pin: String)
    case services(id: String)
    
    var type: String {
        "digitalcodes"
    }
    
    var customHeaders: [String: String] {
        guard case let .pay(_, pin) = self else {
            return [:]
        }
        return ["password": pin]
    }
    
    var path: String {
        let path = "digitalgoods/transportpass"
        switch self {
        case .flow:
            return "\(path)/flow"
        case .cities:
            return "\(path)/cities"
        case .cards, .createCard, .deleteCard:
            return "\(path)/cards"
        case .confirmPermission:
            return "\(path)/confirmPermission"
        case .pay:
            return "\(path)/transactions/app"
        case let .services(id):
            return "services/\(id)"
        }
    }
    
    var parameters: [String : Any] {
        guard case let .flow(cardIdOptional) = self, let cardId = cardIdOptional else {
            return [:]
        }
        return ["card_id": cardId]
    }
    
    var method: HTTPMethod {
        switch self {
        case .flow, .cities, .cards, .services:
            return .get
        case .createCard, .confirmPermission, .pay:
            return .post
        case .deleteCard:
            return .delete
        }
    }
    
    var body: Data? {
        switch self {
        case let .createCard(card, name, cityId):
            return ["card": card,
                    "name": name,
                    "city_id": cityId].toData()
        case let .confirmPermission(cityId):
            return ["city_id": cityId].toData()
        case let .pay(params, _):
            return params.toData()
        case let .deleteCard(id):
            return ["id": id].toData()
        default:
            return nil
        }
    }
}
