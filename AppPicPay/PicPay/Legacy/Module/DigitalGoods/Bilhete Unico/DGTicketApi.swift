import Core
import FeatureFlag
import Foundation
import SwiftyJSON

final class DGTicketPayRequest: DGPayRequest {
    // Variable params
    var digitalGoodId: String?
    
    func toParams(digitalGoodsPayload: [String: Any]) throws -> [String : Any] {
        let params = try super.toParams()
        
        let optionalParamsToBeMerged = [
            "id_digitalgoods": digitalGoodId as Any,
            "digitalgoods": digitalGoodsPayload
        ] as [String : Any]
        
        // Merge super's params with this class' specific params overriding any duplicated entries
        return params.merging(optionalParamsToBeMerged, uniquingKeysWith: { (_, new) in new })
    }
}

final class DGTicketApi: BaseApi {
    final class DGTicketValuesResponse: BaseApiListResponse<DGTicketFlow> {}
    final class DGTicketCitiesResponse: BaseApiListResponse<DGTicketCity> {}
    final class DGTicketCardsResponse: BaseApiListResponse<DGTicketCard> {}
    
    typealias Dependencies = HasFeatureManager & HasMainQueue
    
    private let dependencies: Dependencies
    private let path = "digitalgoods/transportpass"
    private let decoder: JSONDecoder = .init(.convertFromSnakeCase)
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func getFlow(card: DGTicketCard?, completion: @escaping ((PicPayResult<DGTicketFlow>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
            var parameters: [String: String] = [:]
            if let card = card {
                parameters["card_id"] = card.id
            }
            requestManager
                .apiRequest(endpoint: "\(path)/flow", method: .get, parameters: parameters)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        Api<Data>(endpoint: DigitalGoodsTicketEndpoint.flow(cardId: card?.id)).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func getCities(_ completion:  @escaping ((PicPayResult<DGTicketCitiesResponse>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
            requestManager
                .apiRequest(endpoint: "\(path)/cities", method: .get, parameters: [:])
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        Api<Data>(endpoint: DigitalGoodsTicketEndpoint.cities).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func getCards(_ completion:  @escaping ((PicPayResult<DGTicketCardsResponse>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
            requestManager
                .apiRequest(endpoint: "\(path)/cards", method: .get, parameters: [:])
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        Api<Data>(endpoint: DigitalGoodsTicketEndpoint.cards).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func createCard(cityId: String,
                    number: String,
                    name: String,
                    _ completion:  @escaping ((PicPayResult<DGTicketCard>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
            let params = [
                "card": number,
                "name": name,
                "city_id": cityId
            ]
            requestManager
                .apiRequest(endpoint: "\(path)/cards", method: .post, parameters: params, headers: nil)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        let endpoint = DigitalGoodsTicketEndpoint.createCard(card: number, name: name, cityId: cityId)
        Api<Data>(endpoint: endpoint).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func getInfo(id: String, completion: @escaping ((PicPayResult<DGInfo>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
            requestManager
                .apiRequest(endpoint: "digitalgoods/services/\(id)", method: .get)
                .responseApiCodable(completionHandler: completion)
            return
        }
        
        // Core network version
        Api<DGInfo>(endpoint: DigitalGoodsTicketEndpoint.services(id: id)).execute { result in
            switch result {
            case let .success((dgInfo, _)):
                completion(.success(dgInfo))
            case let .failure(error):
                completion(.failure(error.picpayError))
            }
        }
    }
    
    func deleteCard(card: DGTicketCard, completion: @escaping ((PicPayResult<BaseApiEmptyResponse>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
            requestManager
                .apiRequest(endpoint: "\(path)/cards", method: .delete, parameters: ["id": card.id])
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        let endpoint = DigitalGoodsTicketEndpoint.deleteCard(id: card.id)
        Api<Data>(endpoint: endpoint).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func confirmPermission(city: DGTicketCity, completion: @escaping ((PicPayResult<RegisterYourCityPlaceholder>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
            requestManager.apiRequest(
                endpoint: "\(path)/confirmPermission",
                method: .post,
                parameters: ["city_id": city.id]
            ).responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        let endpoint = DigitalGoodsTicketEndpoint.confirmPermission(cityId: city.id)
        Api<Data>(endpoint: endpoint).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    // DGVoucherApiRequest is a subclass of DGPayRequest.
    // It adds specific (and necessary) information to the Voucher payment request
    func pay(request: DGTicketPayRequest,
             payload: [String: Any],
             pin: String,
             _ completion: @escaping ((WrapperResponse) -> Void)) {
        do {
            let params = try request.toParams(digitalGoodsPayload: payload)
            guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
                requestManager
                    .apiRequest(endpoint: "\(path)/transactions/app", method: .post, parameters: params, pin: pin)
                    .responseApiObject(completionHandler: completion)
                return
            }
            
            // Core network version
            let endpoint = DigitalGoodsTicketEndpoint.pay(params: params, pin: pin)
            Api<Data>(endpoint: endpoint).execute { result in
                ApiSwiftJsonWrapper(with: result).transform(completion)
            }
        } catch let error {
            guard let error = error as? PicPayError else {
                return
            }
            completion(PicPayResult.failure(error))
        }
    }
}
