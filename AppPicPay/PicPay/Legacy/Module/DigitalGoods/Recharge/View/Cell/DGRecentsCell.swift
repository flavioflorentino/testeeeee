import Foundation
import UI

final class DGRecentsCell: UICollectionViewCell {
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var carrier: UILabel!
    static let identifier = "RecentsCell"
    
    func setup() {
        setupShadow()
        setupColors()
    }
    
    private func setupShadow() {
        let shadowPath = UIBezierPath(rect: bounds)
        layer.cornerRadius = 5
        layer.masksToBounds = false
        layer.shadowColor = Palette.ppColorGrayscale600.color.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowOpacity = 0.1
        layer.shadowPath = shadowPath.cgPath
    }
    
    private func setupColors() {
        phoneNumber.textColor = Palette.ppColorGrayscale600.color
        carrier.textColor = Palette.ppColorGrayscale600.color
        backgroundColor = Palette.ppColorGrayscale000.color
    }
}
