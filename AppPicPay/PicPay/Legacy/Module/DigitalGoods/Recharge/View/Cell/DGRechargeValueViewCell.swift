import UI
import UIKit

final class DGRechargeValueViewCell: UICollectionViewCell {
    @IBOutlet weak var backgroundCircleView: BackgroundCircleValueView!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    
    var isCellSelected: Bool? {
        didSet {
            self.backgroundCircleView.isSelected = isCellSelected
            valueLabel.textColor = self.isCellSelected == true ? Palette.ppColorGrayscale000.color : Palette.ppColorGrayscale500.color
            currencyLabel.textColor = valueLabel.textColor
            
            guard self.isCellSelected == true else {
            return
            }
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                self.isCellSelected = false
            }
        }
    }
    
    override var isSelected: Bool {
        didSet {
            guard isSelected else {
            return
        }
            self.isSelected = false
        }
    }
    
    func configureCell(product: DGRechargeProduct){
        valueLabel.text = String(format: "%.2f", product.value).replacingOccurrences(of: ".", with: ",").replacingOccurrences(of: ",00", with: "")
        setupColors()
    }
    
    func setupColors() {
        valueLabel.textColor = Palette.ppColorGrayscale500.color
        currencyLabel.textColor = Palette.ppColorGrayscale500.color
    }
}
