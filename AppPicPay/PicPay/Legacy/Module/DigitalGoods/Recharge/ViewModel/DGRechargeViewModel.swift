import AnalyticsModule
import UI
import Core
import PCI
import FeatureFlag

final class DGRechargeViewModel: NSObject {
    typealias RechargeDependencies = HasFeatureManager & HasKeychainManager

    public private(set) var presenter: DGRechargePresenting
    
    var apiRecharge : ApiDigitalGoodsRecharge
    private let service: PhoneRechargeServiceProtocol = PhoneRechargeService()
    var carriers: [DGRechargeCarrier] = []
    
    // data for form
    var ddd = ""
    var phoneNumber = ""
    var digitalGood: DGItem?
    var carrier: DGRechargeCarrier? {
        didSet {
            guard let carrier = carrier else {
                return
            }
            Analytics.shared.log(PhoneRechargeEvent.carrierSelected(carrier: carrier.name))
        }
    }
    var product: DGRechargeProduct? {
        didSet {
            guard let product = product else {
                return
            }
            paymentManager?.subtotal = NSDecimalNumber(value: product.value)
            Analytics.shared.log(PhoneRechargeEvent.valueSelected(value: product.value))
        }
    }
    var isLoading = false
    var paymentManager = PPPaymentManager()
    
    private lazy var valueOptionViewController: ValueOptionViewController = {
        let controller = ValueOptionViewController(alignment: .center, highlight: .topValue)
        controller.dataSource = self
        controller.delegate = self
        return controller
    }()
    
    private var valueOptionsPresenters: [ValueOptionViewPresenting] {
        return [
            ValueOptionViewPresenter(title: "10", description: DefaultLocalizable.days.text),
            ValueOptionViewPresenter(title: "30", description: DefaultLocalizable.days.text)
        ]
    }
    
    private var periodSelected: (isEnable: Bool, presenter: ValueOptionViewPresenting)?
    private let dependencies: RechargeDependencies

    init(
        apiRecharge: ApiDigitalGoodsRecharge = ApiDigitalGoodsRecharge(),
        presenter: DGRechargePresenting = DGRechargePresenter(),
        dependencies: RechargeDependencies = DependencyContainer()
    ) {
        self.apiRecharge = apiRecharge
        self.presenter = presenter
        self.dependencies = dependencies
    }
    
    fileprivate var carriersLoadingId: String = ""
    
    func clearCarrier() {
        self.carrier = nil
        self.carriers = []
    }
    
    func formattedPhoneNumber() -> String{
        let mask = CustomStringMask(mask: phoneNumber.count > 8 ? "00000-0000" : "0000-0000")
        return mask.maskedText(from: phoneNumber) ?? ""
    }
    
    func carriers(ddd: String, _ completion: @escaping ((PicPayErrorDisplayable?) -> Void) ) -> Bool {
        if carriersLoadingId != ddd && carriers.isEmpty || (self.ddd != ddd && !carriers.isEmpty) {
            carriersLoadingId = ddd
            
            if (self.ddd != ddd && !carriers.isEmpty){
                carriers.removeAll()
                carrier = nil
            }

            self.loadCarries(ddd: ddd, completion)
            return true
        }
        
        return false
    }

    private func loadCarries(ddd: String, _ completion: @escaping ((PicPayErrorDisplayable?) -> Void)) {
        self.isLoading = true
        apiRecharge.carriers(ddd: ddd) { [weak self] (result) in
            self?.isLoading = false
            self?.carriersLoadingId = ""
            DispatchQueue.main.async {
                switch result {
                case .success(let value):
                    self?.ddd = ddd
                    self?.carriers = value.list
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }

    func loadSellerIdIfNeeded(completion: @escaping ((Result<NoContent, PicPayError>) -> Void)) {
        guard let dg = digitalGood, (dg.sellerId.isEmpty || dg.sellerId == "0") else {
            completion(.success(.init()))
            return
        }

        apiRecharge.getInfo(id: dg.id) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let info):
                self.digitalGood?.sellerId = "\(info.sellerId)"
                completion(.success(.init()))
            case .failure:
                completion(.failure(.generalError))
            }
        }
    }
}

extension DGRechargeViewModel: DGPaymentDelegate {
    func didPerformPayment(with manager: PPPaymentManager, cvv: String?, additionalPayload: [String: Any], pin: String, biometry: Bool, privacyConfig: String, someErrorOccurred: Bool, completion: @escaping (WrapperResponse) -> Void) {
        self.paymentManager = manager
        guard let carrier = carrier,
            let product = product,
            let digitalGood = digitalGood,
            let paymentManager = paymentManager else {
                return
        }
        
        let request = PayRechargeRequest()
        
        request.paymentManager = paymentManager
        request.pin = pin
        request.biometry = biometry
        request.sellerId = digitalGood.sellerId
        request.digitalGoodId = digitalGood.id
        request.ddd = ddd
        request.carrierName = carrier.name
        request.phone = phoneNumber
        request.productId = product.code
        request.privacyConfig = privacyConfig
        request.someErrorOccurred = someErrorOccurred
        
        if FeatureManager.isActive(.pciPhoneRecharge) {
            createTransactionPCI(manager: manager, request: request, password: pin, cvv: cvv, completion: completion)
            return
        }
        
        apiRecharge.payRecharge(request: request, pin: pin, { result in
            // Call completion block
            completion(result)
            
            // save to recents
            DispatchQueue.main.async {
                switch(result) {
                case .success(_):
                    guard let ddd = request.ddd,
                        let phone = request.phone else {
                            return
                    }
                    DGRechargeRecentsManager.saveRecent(ddd: ddd, phoneNumber: phone, carrier: carrier.name)
                default:
                    break
                }
            }
        })
    }
    
    private func createTransactionPCI(
        manager: PPPaymentManager,
        request: PayRechargeRequest,
        password: String,
        cvv: String?,
        completion: @escaping (WrapperResponse) -> Void
    ) {
        guard let phoneRechargePayload = createParkingPayload(manager: manager, request: request) else {
            let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            completion(PicPayResult.failure(error))
            return
        }
        let cvv = createCVVPayload(manager: manager, cvv: cvv)
        let payload = PaymentPayload<DigitalGoodsPayload<PhoneRecharge>>(cvv: cvv, generic: phoneRechargePayload)
        service.createTransaction(password: password, payload: payload, isNewArchitecture: false) { result in
            DispatchQueue.main.async {
                let mappedResult = result
                    .map { BaseApiGenericResponse(dictionary: $0.json) }
                    .mapError { $0.picpayError }
                
                completion(mappedResult)
            }
        }
    }
    
    private func createParkingPayload(manager: PPPaymentManager, request: PayRechargeRequest) -> DigitalGoodsPayload<PhoneRecharge>? {
        guard let params = try? request.toParams(),
            let data = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) else {
                return nil
        }
        
        return try? JSONDecoder.decode(data, to: DigitalGoodsPayload<PhoneRecharge>.self)
    }

    private func createCVVPayload(manager: PPPaymentManager, cvv: String?) -> CVVPayload? {
        guard let cvv = cvv else {
            return createLocalStoreCVVPayload(manager: manager)
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func createLocalStoreCVVPayload(manager: PPPaymentManager) -> CVVPayload? {
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        guard manager.cardTotal()?.doubleValue != .zero,
              let cvv = dependencies.keychain.getData(key: KeychainKeyPF.cvv(id)) else {
                return nil
        }
        
        return CVVPayload(value: cvv)
    }
}

extension DGRechargeViewModel: ValueOptionViewDataSource {
    func numberOfItems(in valueOptionViewController: ValueOptionViewController) -> Int {
        return valueOptionsPresenters.count
    }
    
    func valueOptionViewController(_ valueOptionViewController: ValueOptionViewController, valueOptionForItemAt item: Int) -> ValueOptionViewPresenting {
        return valueOptionsPresenters[item]
    }
}

extension DGRechargeViewModel: ValueOptionViewDelegate {
    func didSelectItem(_ presenter: ValueOptionViewPresenting) {
        periodSelected = (isEnable: false, presenter: presenter)
    }
}
