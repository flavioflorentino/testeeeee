import Foundation

protocol DGRechargePresenting: AnyObject {
    var viewController: DGRechargeDisplay? { get set }
}

final class DGRechargePresenter: DGRechargePresenting {
    weak var viewController: DGRechargeDisplay?
}
