import FeatureFlag
import UI
import UIKit

final class DGRechargeValueViewController: DGRechargeFormViewController {
    @IBOutlet weak var valueList: CircleItemsView!
    let cellIdentifier = "ValueCell"
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupColors()
        configureCollectionView()
        
        actionButton?.isEnabled = false
    }
    
    private func setupColors() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = BilletLocalizable.recharge.text
    }
    
    // MARK: - Internal Methdos
    func configureCollectionView(){
        valueList.dataSource = self
        valueList.delegate = self
        
        if let products = self.model?.carrier?.products {
            let nibName = String(describing: DGRechargeValueViewCell.self)
            valueList.registerCell(name: nibName, cellIdentifier: cellIdentifier)
            valueList.update(totalCount: products.count)
        }
    }
    
    // MARK: - User Actions
    func selectProduct(_ product: DGRechargeProduct){
        actionButton?.isEnabled = true
        model?.product = product
    }
    
    func next() {
        guard
            let product = model?.product,
            let digitalGood = model?.digitalGood,
            let phoneNumber = model?.formattedPhoneNumber(),
            let carrierName = model?.carrier?.name,
            let ddd = model?.ddd
            else {
                return
        }
        

        var details: [DGPaymentDetailsBase] = []
        
        if let markdown = digitalGood.disclaimerMarkdown, !markdown.isEmpty {
            let detailItem = DGPaymentImportantDetailItem(text: markdown)
            details.append(detailItem)
        }
        
        let phone = DGPaymentDetailsItem(name: DefaultLocalizable.number.text, detail: "(\(ddd)) \(phoneNumber)")
        let carrier = DGPaymentDetailsItem(name: BilletLocalizable.operatorPayment.text, detail: carrierName)
        details.append(phone)
        details.append(carrier)
        
        if FeatureManager.isActive(.newPhoneRecharge) {
            openNewPaymentViewController(product: product, digitalGood: digitalGood, details: details)
            return
        }
        
        openLegacyPaymentViewController(product: product, digitalGood: digitalGood, details: details)
    }
    
    private func openNewPaymentViewController(product: DGRechargeProduct, digitalGood: DGItem, details: [DGPaymentDetailsBase]) {
        guard
            let phoneNumber = model?.phoneNumber,
            let carrierName = model?.carrier?.name,
            let ddd = model?.ddd
            else {
                return
        }
        
        let items = DigitalGoodsPayment(
            title: digitalGood.name,
            image: digitalGood.imgUrl,
            link: digitalGood.infoUrl,
            payeeId: digitalGood.sellerId,
            value: product.value,
            paymentType: .phoneRecharge
        )
        
        let model = DigitalGoodsPhoneRecharge(productId: product.code, ddd: ddd, phone: phoneNumber, carrier: carrierName)
        let service = DigitalGoodsPhoneRechargeService(model: model, item: digitalGood)
        let orchestrator = DigitalGoodsPaymentOrchestrator(items: items, paymentDetails: details, service: service)
        navigationController?.pushViewController(orchestrator.paymentViewController, animated: true)
    }
    
    private func openLegacyPaymentViewController(product: DGRechargeProduct, digitalGood: DGItem, details: [DGPaymentDetailsBase]) {
        guard let paymentManager = model?.paymentManager else {
            return
        }
        
        let value = product.value
        let sellerName = digitalGood.name
        let sellerImg = digitalGood.imgUrl
        let sellerId = digitalGood.sellerId
        
        let paymentScreen = DGPaymentViewController.controllerFromStoryboard()
        let paymentModel = DGPaymentViewModel(
            amount: value,
            type: .phoneRecharge,
            sellerName: sellerName,
            sellerImageUrl: sellerImg,
            sellerId: sellerId,
            detailInfo: details,
            dependencies: DependencyContainer()
        )
        
        paymentScreen.title = BilletLocalizable.recharge.text
        paymentScreen.setup(paymentManager: paymentManager, model: paymentModel, delegate: model)
        
        navigationController?.pushViewController(paymentScreen, animated: true)
    }
}

extension DGRechargeValueViewController: UICollectionViewDelegateFlowLayout {
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        
        (cell as? SelectableCell)?.isCellSelected = true
        
        if let product = model?.carrier?.products[indexPath.row] {
            selectProduct(product)

            startLoadingView()
            model?.loadSellerIdIfNeeded { result in
                self.stopLoadingView()
                switch result {
                case .success:
                    self.next()
                case .failure(let error):
                    AlertMessage.showCustomAlertWithError(error, controller: self)
                }
            }
        }
    }
}

extension DGRechargeValueViewController: CircleViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? DGRechargeValueViewCell else { return UICollectionViewCell() }
        
        if let product = model?.carrier?.products[indexPath.row] {
            cell.configureCell(product: product)
        }
        
        return cell
    }
}
