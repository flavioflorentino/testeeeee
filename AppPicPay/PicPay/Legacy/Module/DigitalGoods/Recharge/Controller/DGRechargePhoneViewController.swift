import AnalyticsModule
import FeatureFlag
import Foundation
import Validator
import UI

final class DGRechargePhoneViewController: DGRechargeFormViewController {    
    @IBOutlet var numberTextField: UIPPFloatingTextField!
    @IBOutlet var carrieTextField: UIPPFloatingTextField!
    @IBOutlet var carrieButton: UIButton!
    @IBOutlet var numberFieldTop: NSLayoutConstraint!
    @IBOutlet var numberFieldRecentTop: NSLayoutConstraint!
    @IBOutlet weak var recentsContainer: UIView!
    @IBOutlet weak var recentsCarousel: CarouselCollectionView!
    @IBOutlet weak var carrierActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var contactsButton: UIButton!

    private var recents: [DGRechargeRecent] = []
    private var isFromContactsList: Bool = false
    
    private var hasRecentRecharges = false {
        didSet {
            view.layoutIfNeeded()
            recentsContainer.isHidden = !hasRecentRecharges
            numberFieldTop.isActive = false
            numberFieldRecentTop.isActive = false
            numberFieldRecentTop.isActive = hasRecentRecharges
            numberFieldTop.isActive = !hasRecentRecharges
            view.layoutIfNeeded()
        }
    }
    
    // If the user tap a recent recharge we'll try to prefill a carrier, but it might
    // not be available anymore. This variable will hold the carrier we'll try to fill until
    // we make sure it's still available
    private var carrierPrefill: String?
    
    // Not necessarily a valid ddd, just to prevent multiple api calls
    private var lastDdd = ""

    private lazy var starButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapAtStarButton), for: .touchUpInside)
        button.setImage(Assets.Favorite.starOutline.image, for: .normal)
        button.setImage(Assets.Favorite.starFill.image, for: .selected)
        button.setImage(Assets.Favorite.starFill.image, for: .highlighted)

        return button
    }()

    private lazy var favoriteService: FavoritesServicing = FavoritesService()
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureForm()
        recents = DGRechargeRecentsManager.retrieveRecents()
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.btCancel.text, style: .plain, target: self, action: #selector(close))
        setupColors()

        handleFavoriteStarButtonVisibility()
    }
    
    private func setupColors() {
        recentsContainer.backgroundColor = Palette.ppColorGrayscale000.color
        view.backgroundColor = Palette.ppColorGrayscale000.color
        numberTextField.textColor = Palette.ppColorGrayscale600.color
        carrieTextField.textColor = Palette.ppColorGrayscale600.color
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hasRecentRecharges = !recents.isEmpty
        contactsButton.isHidden = !FeatureManager.isActive(.featureRechargePhoneContactsList)
    }
 
    // MARK: - Configuration
    fileprivate func configureForm() {
        addTextField(numberTextField)
        addTextField(carrieTextField)
        
        let nibCell = UINib(nibName: String(describing: DGRecentsCell.self), bundle: Bundle.main)
        recentsCarousel.register(nibCell, forCellWithReuseIdentifier: DGRecentsCell.identifier)
        
        recentsCarousel.setItemSize(size: CGSize(width: 188, height: 68))
        let margin = CGSize(width: 20, height: 20)
        recentsCarousel.setMargins(left: margin, right: margin)
        recentsCarousel.dataSource = self
        recentsCarousel.delegate = self
        
        configureValidations()
    }
    
    fileprivate enum ValidateError: String, Error, CustomStringConvertible, ValidationError {
        case numberRequired = "Informe o número com DDD"
        case carrieRequired = "Selecione a operadora"
        
        var description: String {
            return self.rawValue
        }
        
        var message: String {
            return self.rawValue
        }
    }
    
    fileprivate func configureValidations() {
        numberTextField.setupPhoneMask(error: ValidateError.numberRequired)
        
        var carrieRules = ValidationRuleSet<String>()
        carrieRules.add(rule: ValidationRuleLength(min: 2, error: ValidateError.carrieRequired))
        carrieTextField.validationRules = carrieRules
        
        actionButton?.isEnabled = false
    }
    
    fileprivate func clearCarrier() {
        self.model?.clearCarrier()
        self.carrieTextField.text = ""
    }
    
    // MARK: - Internal Methods
    
    fileprivate func loadCarriers(ddd: String) {
        numberTextField.errorMessage = nil
        
        clearCarrier()

        let isLoading = model?.carriers(ddd: ddd, { [weak self] error in
            guard let strongSelf = self else {
            return
        }
            
            strongSelf.carrierActivityIndicator.stopAnimating()
            if error != nil {
                AlertMessage.showAlert(withMessage: error?.localizedDescription, controller: strongSelf)
            } else if strongSelf.model?.carriers.isEmpty ?? false {
                strongSelf.numberTextField.errorMessage = BilletLocalizable.thereAreNoOperatorsRegistered.text
            } else {
                strongSelf.numberTextField.errorMessage = nil
            }
            
            // Check if there's any carrier to be filled
            if let carrierName = self?.carrierPrefill,
                let carrier = self?.model?.carriers.first(where: { $0.name == carrierName }) {
                self?.model?.carrier = carrier
                self?.carrieTextField.text = carrierName
                self?.validateNext()
            }
            // Clear prefill carrier
            self?.carrierPrefill = nil
        }) ?? false
        
        if isLoading {
            carrieTextField.text = ""
            carrierActivityIndicator.startAnimating()
            validateNext()
        }
    }
    
    /// Checks if the validation and enable/disable the next button
    fileprivate func validateNext() {
        actionButton?.isEnabled = validate(false).isValid
    }
    
    // MARK: - User Actions
    @IBAction private func next(_ sender: Any) {
        guard let model = model else {
            return
        }
        
        if validate(true).isValid {
            // Use swift 4's str.dropFirst(2) when available
            guard let number = numberTextField.text?.onlyNumbers.chopPrefix(2) else {
            return
        }
            model.phoneNumber = number
            
            let nextController = DGRechargeValueViewController.controllerFromStoryboard(.digitalGoodsRecharge)
            nextController.setup(model: model)
            navigationController?.pushViewController(nextController, animated: true)
        }
    }
    
    @IBAction private func showCarrieOptions(_ sender: Any) {
        guard let model = model,
            !model.isLoading else {
            return
        }
        
        // if carrier array is empty shows an error message
        if model.carriers.isEmpty {
            numberTextField.becomeFirstResponder()
            numberTextField.errorMessage = BilletLocalizable.thereAreNoOperatorsRegistered.text
            return
        }
        
        // show the carrie options
        let actionSheet = UIAlertController(title: nil, message: BilletLocalizable.selectOperator.text, preferredStyle: .actionSheet)
        for carrier in model.carriers {
            actionSheet.addAction(UIAlertAction(title: carrier.name, style: .default, handler: { [weak self] _ in
                self?.model?.carrier = carrier
                self?.carrieTextField.text = carrier.name
                self?.validateNext()
            }))
        }
        actionSheet.addAction(UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil))
        present(actionSheet, animated: true, completion: nil)
    }
    
    
    @IBAction private func showContactsList(_ sender: Any) {
        let viewController = ContactsListFactory.make(delegate: self)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        if #available(iOS 13.0, *) {
            viewController.isModalInPresentation = true
        }
        Analytics.shared.log(RechargePhoneContactsListEvent.contactsAccessed)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc
    func close() {
        dismiss(animated: true, completion: nil)
    }
    
    private func updateCursorPosition(for textField: UITextField, and range: ClosedRange<Int>) {
        guard
            let textField = numberTextField,
            let length = textField.text?.length, length > 0,
            range.contains(length) else {
            return
        }
        
        if range.lowerBound == 14 {
            if let newPosition = textField.position(from: textField.beginningOfDocument, in: .right, offset: 2) {
                textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
            }
        } else {
            let newPosition = textField.beginningOfDocument
            textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
        }
    }
    
    // MARK: - TextFieldDelegate
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let result = super.textField(textField, shouldChangeCharactersIn: range, replacementString: string)
        
        if textField == numberTextField {
            if textField.text?.length > 2 {
                // get only the area code
                let ddd = textField.text?[1..<3]
                if ddd != lastDdd || carrierPrefill != nil {
                    self.lastDdd = ddd ?? ""
                    loadCarriers(ddd: ddd ?? "")
                }
            } else {
                clearCarrier()
                lastDdd = ""
            }
        }
        if isFromContactsList {
            updateCursorPosition(for: textField, and: 14...14)
            isFromContactsList = false
        }
        validateNext()
        return result
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        updateCursorPosition(for: textField, and: 9...10)
    }
    
    private func setPhoneText(phoneNumber: String) {
        let contactPhoneNumber = ContactsPhoneNumber(phoneNumber)
        numberTextField.text = "\(contactPhoneNumber)"
        loadCarriers(ddd: contactPhoneNumber.areaCode)
        isFromContactsList = contactPhoneNumber.areaCode.isEmpty || contactPhoneNumber.number.isEmpty
    }
}

extension DGRechargePhoneViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DGRecentsCell.identifier, for: indexPath) as? DGRecentsCell else {
            return UICollectionViewCell()
        }

        cell.setup()
        
        let recent = recents[indexPath.row]
        cell.phoneNumber.text = recent.formattedPhoneNumber()
        cell.carrier.text = recent.carrier
        
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recents.count
    }
}

extension DGRechargePhoneViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let recent = recents[indexPath.row]
        numberTextField.text = "\(recent.ddd)\(recent.phoneNumber)"
        
        carrierPrefill = recent.carrier
        
        // Force delegate call
        _ = numberTextField.delegate?.textField?(numberTextField, shouldChangeCharactersIn: NSRange(location: 0, length: 0), replacementString: "")
    }
}

extension DGRechargePhoneViewController: ContactsListSelecting, UIActionSheetDelegate {
    func presentContactPhones(contact: UserAgendaContact) {
        if contact.phoneNumbers.count > 1 {
            let actionSheet = UIAlertController(
                title: ContactsListLocalizable.contactsNumberSelectionDescription.text + String(describing: contact.givenName),
                message: nil,
                preferredStyle: .actionSheet
            )
            actionSheet.addAction(UIAlertAction(
                title: ContactsListLocalizable.contactsNumberSelectionCancelButtonTitle.text,
                style: .cancel,
                handler: nil)
            )
            contact.phoneNumbers.forEach { phoneNumber in
                actionSheet.addAction(UIAlertAction(title: String(describing: phoneNumber), style: .default) { [weak self] _ in
                    self?.setPhoneText(phoneNumber: phoneNumber)
                })
            }
            Analytics.shared.log(RechargePhoneContactsListEvent.contactMultipleCellphonesSelected)
            present(actionSheet, animated: true)
        } else {
            setPhoneText(phoneNumber: contact.phoneNumbers.first ?? "")
        }
    }
}

private extension DGRechargePhoneViewController {
    func handleFavoriteStarButtonVisibility() {
        let featureManager = FeatureManager.shared
        guard
            let item = model?.digitalGood
            else {
            return
        }

        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: starButton)

        favoriteService.isFavorite(id: item.id, type: .digitalGood) { [weak self] isFavorite in
            self?.starButton.isSelected = isFavorite
        }
    }

    @objc
    func didTapAtStarButton() {
        guard let digitalGood = model?.digitalGood else {
            return
        }

        starButton.isSelected ? unfavorite(digitalGood) : favorite(digitalGood)
    }

    func favorite(_ item: DGItem) {
        starButton.isSelected = true

        favoriteService.favorite(id: item.id, type: .digitalGood) { [weak self] didFavorite in
            self?.starButton.isSelected = didFavorite

            guard didFavorite else {
                return
            }

            Analytics.shared.log(FavoritesEvent.statusChanged(true, id: item.id, origin: .digitalGoods))
        }
    }

    func unfavorite(_ item: DGItem) {
        starButton.isSelected = false

        favoriteService.unfavorite(id: item.id, type: .digitalGood) { [weak self] didUnfavorite in
            self?.starButton.isSelected = didUnfavorite == false

            guard didUnfavorite else {
                return
            }

            Analytics.shared.log(FavoritesEvent.statusChanged(false, id: item.id, origin: .digitalGoods))
        }
    }
}
