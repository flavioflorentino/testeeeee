//
//  DGRechargeFormViewController.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 27/09/17.
//

import UIKit

class DGRechargeFormViewController: BaseFormController {

    var model: DGRechargeViewModel?
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if model == nil {
            setup()
        }
    }
    

    // MARK: - Dependence Injection
    func setup(model: DGRechargeViewModel = DGRechargeViewModel()){
        self.model = model
    }

}
