import AnalyticsModule
import Foundation

enum PhoneRechargeEvent: AnalyticsKeyProtocol {
    case carrierSelected(carrier: String)
    case valueSelected(value: Double)

    private var name: String {
        switch self {
        case .carrierSelected:
            return "Phone Recharge Carrier Selected"
        case .valueSelected:
            return "Phone Recharge Value Selected"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .carrierSelected(carrier):
            return ["carrier": carrier]
        case let .valueSelected(value):
            return ["value": value]
        }
    }
    
    private var providers: [AnalyticsProvider] {
       [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
       AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
