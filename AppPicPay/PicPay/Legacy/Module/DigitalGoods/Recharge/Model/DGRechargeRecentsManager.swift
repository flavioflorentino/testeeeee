import Core
import Foundation
import UI

final class DGRechargeRecent: NSObject, NSCoding {
    var ddd: String
    var phoneNumber: String
    var carrier: String
    
    init(ddd: String, phoneNumber: String, carrier: String) {
        self.ddd = ddd
        self.phoneNumber = phoneNumber
        self.carrier = carrier
        super.init()
    }
    
    required init(coder decoder: NSCoder) {
        self.ddd = decoder.decodeObject(forKey: "ddd") as? String ?? ""
        self.phoneNumber = decoder.decodeObject(forKey: "phoneNumber") as? String ?? ""
        self.carrier = decoder.decodeObject(forKey: "carrier") as? String ?? ""
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(ddd, forKey: "ddd")
        coder.encode(phoneNumber, forKey: "phoneNumber")
        coder.encode(carrier, forKey: "carrier")
    }
    
    func formattedPhoneNumber() -> String {
        let mask = CustomStringMask(mask: phoneNumber.length > 8 ? "(00) 00000-0000" : "(00) 0000-0000")
        return mask.maskedText(from: "\(ddd)\(phoneNumber)") ?? ""
    }
    
    static func ==(lhs: DGRechargeRecent, rhs: DGRechargeRecent) -> Bool {
        return lhs.carrier == rhs.carrier && lhs.phoneNumber == rhs.phoneNumber && lhs.carrier == rhs.carrier
    }
}

final class DGRechargeRecentsManager: NSObject {
    
    static func clear() {
        KVStore().removeObjectFor(.DGRecents)
    }
    
    static func saveRecent(ddd: String, phoneNumber: String, carrier: String) {
        let newRecent = DGRechargeRecent(ddd: ddd, phoneNumber: phoneNumber, carrier: carrier)
        
        var previousRecent = DGRechargeRecentsManager.retrieveRecents()
        
        if previousRecent.contains(where: { $0 == newRecent }) {
            return
        }
        
        if previousRecent.count >= 10 {
            let _ = previousRecent.popLast()
        }
        
        previousRecent.append(newRecent)
        
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: previousRecent)
        KVStore().setData(encodedData, with: .DGRecents)
    }
    
    static func retrieveRecents() -> [DGRechargeRecent] {
        guard let data = KVStore().objectForKey(.DGRecents) as? Data else {
    return []
}
        
        return NSKeyedUnarchiver.unarchiveObject(with: data) as? [DGRechargeRecent] ?? []
    }
}
