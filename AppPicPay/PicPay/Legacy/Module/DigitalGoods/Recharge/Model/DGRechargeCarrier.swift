//
//  DGRechargeCarrier.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 28/09/17.
//

import SwiftyJSON

final class DGRechargeCarrier: NSObject, BaseApiResponse {
    
    var name: String = ""
    var products: [DGRechargeProduct] = []
    
    required init?(json: JSON) {
        name = json["operator"].string ?? ""
        
        if let produtsJson = json["products"].array {
            for json in produtsJson {
                if let product = DGRechargeProduct(json: json) {
                    products.append(product)
                }
            }
        }
    }
}
