//
//  DGRechargeProduct.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 28/09/17.
//

import SwiftyJSON

final class DGRechargeProduct: NSObject {
    var code: String = ""
    var title: String = ""
    var value: Double = 0.0
    
    required init?(json: JSON) {
        code = json["code"].string ?? ""
        title = json["title"].string ?? ""
        value = json["value"].double ?? 0.0
    }
}
