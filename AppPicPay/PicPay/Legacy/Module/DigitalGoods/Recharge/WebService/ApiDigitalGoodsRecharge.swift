import Core
import FeatureFlag
import UIKit
import SwiftyJSON

final class PayRechargeRequest: DGPayRequest {
    // Variable params
    var digitalGoodId: String?
    var ddd: String?
    var carrierName: String?
    var phone: String?
    var productId: String?
    
    override func toParams() throws -> [String : Any] {
        let params = try super.toParams()
        
        let optionalParamsToBeMerged = [
            "id_digitalgoods": digitalGoodId,
            "digitalgoods": [
                "type": "phonerecharges",
                "ddd": ddd,
                "phone": phone,
                "operator": carrierName,
                "product_id": productId
            ]
        ] as [String : Any?]
        
        let paramsToBeMerged = try obligatoryParams(params: optionalParamsToBeMerged)
        
        // Merge super's params with this class' specific params overriding any duplicated entries
        return params.merging(paramsToBeMerged, uniquingKeysWith: { (_, new) in new })
    }
}

final class ApiDigitalGoodsRecharge: BaseApi {
    
    final class CarriersResponse: BaseApiListResponse<DGRechargeCarrier> {}
    
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    private let decoder: JSONDecoder = .init(.convertFromSnakeCase)
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func carriers(ddd: String, _ completion: @escaping ((PicPayResult<CarriersResponse>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
            let params = ["ddd": ddd, "filtered": "1"]
            requestManager
                .apiRequest(endpoint: "digitalgoods/phonerecharges/products", method: .get, parameters: params)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        let endpoint = DigitalGoodsRechargeEnpoint.products(ddd: ddd)
        Api<Data>(endpoint: endpoint).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func getInfo(id: String, completion: @escaping ((PicPayResult<DGInfo>) -> Void)) {
        requestManager
            .apiRequest(endpoint: "digitalgoods/services/\(id)", method: .get)
            .responseApiCodable(completionHandler: completion)
    }
    
    func payRecharge(request: PayRechargeRequest, pin: String, _ completion: @escaping ((WrapperResponse) -> Void)) {
        // call core network in other class
        DigitalGoodsApi().pay(request: request, pin: pin, completion)
    }
}
