import Core
import Foundation

enum DigitalGoodsRechargeEnpoint: ApiEndpointExposable {
    case products(ddd: String)
    
    var path: String {
        return "digitalgoods/phonerecharges/products"
    }
    
    var parameters: [String : Any] {
        guard case let .products(ddd) = self else {
            return [:]
        }
        return ["ddd": ddd, "filtered": "1"]
    }
}
