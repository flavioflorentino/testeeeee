enum BilletLocalizable: String, Localizable {
    case dueDate
    case taxDueDate
    case billetDueDateTitle
    case billetDueDateMessage
    case gotIt
    case okGotIt
    case acceptAdditionalFee
    case notNow
    case seeReceipt
    case discountValue
    case recipient
    case description
    case billetOverdueTitle
    case billetOverdueMessage
    case billetTimeoutTitle
    case billetTimeoutMessage
    case electricityBillEx
    case verifyingAccount
    case barCode
    case scanPicPayPaymentCodes
    case wrongNumber
    case howMuchDoYouWantToPay
    case tryAgainLater
    case billet
    case payday
    case useBarcodeReader
    case enterBarcodeManually
    case installmentsAvailable
    case thereWasAnError
    case thereAreNoOperatorsRegistered
    case selectOperator
    case recharge
    case operatorPayment
    case iDonKnowMyCode
    case item
    case customerID
    case chooseAnItem
    case chooseValue
    case about
    case maximumValue
    case minimumValue
    case youHaveCopiedBoletoNumber
    case cardConvenienceFeeA
    case cardConvenienceFeeB
    case cardConvenienceFeeDescription
    case cardFeePopupTitle
    case cardFeePopupMessage
    case cardInstallmentFee
    case cardInstallmentFeeDescription
    case installmentFeePopupTitle
    case installmentFeePopupMessage
    case installments
    case totalValue
    case paymentDisclaimerPopupTitle
    case paymentDisclaimerPopupMessage
    case howToPayBilletTitle
    case howToPayBilletDescription
    case howToPayBilletActionTitle
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .billet
    }
}
