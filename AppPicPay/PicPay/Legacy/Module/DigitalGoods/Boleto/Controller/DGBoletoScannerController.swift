import AVFoundation
import Core
import Billet
import FeatureFlag
import Foundation
import UI
import UIKit

final class DGBoletoScannerController: PPBaseViewController {
    
    @IBOutlet weak var flashButton: UIButton! {
        didSet {
            flashButton.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        }
    }
    @IBOutlet weak var containerView: UIView! {
        didSet {
            
        }
    }
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        }
    }
    @IBOutlet weak var topView: UIView! {
        didSet {
            topView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        }
    }
    @IBOutlet weak var bottomView: UIView! {
        didSet {
            bottomView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        }
    }
    
    var scanner: PPScanner?
    var model = DGBoletoFormViewModel()
    var loadingIndicator: UILoadView?
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    // MARK: Private vars
    private var isFlashOn = false {
        didSet {
            flashButton.setImage(isFlashOn ? #imageLiteral(resourceName: "ico_flash_on") : #imageLiteral(resourceName: "ico_flash_off"), for: .normal)
        }
    }
    
    private lazy var cameraPermission = {
        return PPPermission.camera(withDeniedAlert: PPPermission.Alert(
            title: DefaultLocalizable.authorizePicPayCamera.text,
            message: BilletLocalizable.scanPicPayPaymentCodes.text,
            settings: DefaultLocalizable.settings.text
        ))
    }()
    
    private let permissionView = AskForCameraPermissionView()
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupCallbacks()
        setupConstraints()
        setupContainerView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tryStartScanner()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        if isFlashOn {
            toggleFlash(self)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        scanner?.stop()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    @IBAction private func enterManually(_ sender: Any) {
        let viewController: UIViewController
      
        if FeatureManager.isActive(.releaseNewBilletPaymentPresentingBool) {
            viewController = BilletFormFactory.make(with: .scanner)
        } else {
            let boletoForm: DGBoletoFormViewController = UIStoryboard.viewController(.digitalGoodsBoleto)
            boletoForm.model = self.model
            viewController = boletoForm
        }
      
        navigationController?.pushViewController(viewController: viewController, animated: true, completion: nil)
    }
    
    @IBAction private func back(_ sender: Any) {
        scanner?.prepareForDismiss()
        if navigationController?.viewControllers.first == self {
            self.dismiss(animated: true, completion: nil)
            return
        }
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func toggleFlash(_ sender: Any) {
        let device = AVCaptureDevice.default(for: AVMediaType.video)
        guard (device?.hasTorch == true) else {
            return
        }
        do {
            try device?.lockForConfiguration()
            if (device?.torchMode == .on) {
                device?.torchMode = .off
                isFlashOn = false
            } else {
                do {
                    try device?.setTorchModeOn(level: 1.0)
                    isFlashOn = true
                } catch {
                    debugPrint(error)
                }
            }
            device?.unlockForConfiguration()
        } catch {
            debugPrint(error)
        }
    }
}

extension DGBoletoScannerController {
    
    // MARK: Setup functions
    private func setupConstraints() {
        permissionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    private func setupCallbacks() {
        permissionView.action = { [weak self] (button) in
            guard let strongSelf = self else {
            return
        }
            PPPermission.request(permission: strongSelf.cameraPermission, { status in
                guard status == .authorized else {
            return
        }
                self?.tryStartScanner()
            })
        }
    }
    
    private func initScanner() {
        guard scanner == nil else {
            return
        }
        scanner = PPScanner(origin: .boleto, view: containerView, useLandscapeOrientation: true, supportedTypes: [.itf14, .interleaved2of5], isValid: { text in
            return text.length == 44
        }, onRead: { [weak self] (text) in
            self?.onRead(scannedText: text)
        })
    }
    
    func setupViews() {
        loadingIndicator = UILoadView(superview: self.view, position: .center, activityStyle: .white)
        loadingIndicator?.backgroundColor = Palette.ppColorGrayscale600.color.withAlphaComponent(0.8)
        loadingIndicator?.stopLoading()
        
        permissionView.view.backgroundColor = Palette.ppColorGrayscale600.color.withAlphaComponent(0.95)
        permissionView.descriptionLabel.textColor = Palette.ppColorGrayscale000.color
        
        // Back button for the camera view
        let backButton = UIButton()
        backButton.setImage(#imageLiteral(resourceName: "ico_left_arrow"), for: .normal)
        backButton.addTarget(self, action: #selector(back(_:)), for: .touchUpInside)
        
        permissionView.addSubview(backButton)
        backButton.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 60, height: 60))
            make.left.equalToSuperview()
            make.top.equalToSuperview()
        }
        permissionView.isHidden = cameraPermission.status == .authorized
        containerView.addSubview(permissionView)
    }
    
    private func setupContainerView() {
        self.view.removeConstraints(self.view.constraints)
        containerView.translatesAutoresizingMaskIntoConstraints = true
        containerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.height, height: self.view.frame.width)
        rotate(angle: 90, for: self.view)
        
        var width = self.view.frame.size.width
        
        if #available(iOS 13.0, *) {
            width -= 54
        }
        
        containerView.frame = CGRect(x: 0, y: 0, width: width, height: self.view.frame.height)
        containerView.sizeToFit()
    }
    
    // MARK: Scanner functions
    func onRead(scannedText: String) {
        guard let scanner = scanner else {
            return
        }
        // Stop reading new codes
        scanner.preventNewReads = true
        
        loadingIndicator?.startLoading()
        model.verifyScannedCode(code: scannedText) { [weak self] error in
            guard let self = self else { return }
            
            self.loadingIndicator?.stopLoading()
            
            if let error = error {
                AlertMessage.showAlert(withMessage: error.localizedDescription, controller: self)
                self.scanner?.preventNewReads = false
                return
            }
            
            let viewController: UIViewController
            
            if FeatureManager.isActive(.releaseNewBilletPaymentPresentingBool) {
                viewController = BilletFormFactory.make(with: .scanner, linecode: self.model.boleto?.lineCode)
            } else {
                self.model.isScanned = true
                
                let billetForm: DGBoletoFormViewController = UIStoryboard.viewController(.digitalGoodsBoleto)
                billetForm.model = self.model
                
                viewController = billetForm
            }
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    private func removeRequestCameraView() {
        permissionView.isHidden = true
    }
    
    private func presentRequestCameraView() {
        permissionView.isHidden = false
    }
    
    private func tryStartScanner() {
        switch cameraPermission.status {
        case .authorized:
            initScanner()
            removeRequestCameraView()
            self.scanner?.start()
        default:
            presentRequestCameraView()
        }
    }
    
    // MARK: - Aux functions
    
    /// Rotate a view by specified degrees
    ///
    /// - Parameters:
    ///   - angle: angle
    ///   - view: view for rotate
    private func rotate(angle: CGFloat, for view: UIView) {
        let radians = angle / 180.0 * CGFloat(Double.pi)
        let rotation = view.transform.rotated(by: radians)
        view.transform = rotation
    }
}
