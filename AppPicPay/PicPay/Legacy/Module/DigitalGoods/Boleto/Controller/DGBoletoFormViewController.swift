//
//  DGBoletoFormViewController.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 07/02/18.
//

import Foundation
import Validator
import UI

final class DGBoletoFormViewController: BaseFormController {
    
    @IBOutlet weak var boletoField: UIPPFloatingTextView!
    @IBOutlet weak var descriptionField: UIPPFloatingTextField!
    
    var prefilledBoleto: String?
    var loader: UIView?

    private let descriptionPlaceholder = BilletLocalizable.electricityBillEx.text
    private let descriptionTextLength:Int = 50
    
    var model = DGBoletoFormViewModel()
    var onAppearErrorMessage: String? = nil
    var showCameraButton = true
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Only vc in the stack
        if navigationController?.viewControllers.count == 1 {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                title: DefaultLocalizable.btCancel.text,
                style: .plain,
                target: self,
                action: #selector(dismissVc)
            )
        }
        
        if showCameraButton {
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: .camera,
                target: self,
                action: #selector(openScanner)
            )
        }
        
        configureForm()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = model.digitalGood?.name
        
        if let billet = model.boleto {
            setBoletoFieldText(text: billet.lineCode)
            descriptionField.text = billet.placeholder
        }
        validateNext()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        loader = Loader.getLoadingView(BilletLocalizable.verifyingAccount.text)
        if let loader = loader {
            navigationController?.view.addSubview(loader)
        }
        
        loader?.isHidden = true
        
        if let alertMsg = onAppearErrorMessage {
            AlertMessage.showAlert(withMessage: alertMsg, controller: self)
            onAppearErrorMessage = nil
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    fileprivate func setBoletoFieldText(text: String) {
        // Can't just set boletoField.textView.text because we need to mask the string
        _ = boletoField.textView.delegate?.textView?(boletoField.textView, shouldChangeTextIn: NSRange(location: 0, length: 0), replacementText: text)
    }
    
    // MARK: - Configuration
    fileprivate func configureForm(){
        boletoField.topText = BilletLocalizable.barCode.text
        boletoField.textView.returnKeyType = .next
        
        if let string = model.digitalGood?.barcodeInfoUrl, string != "" {
            boletoField.infoButtonAction = { [weak self] in
                guard let strongSelf = self,
                    let url = URL(string: string) else {
            return
        }
                ViewsManager.presentSafariViewController(url, from: strongSelf)
            }
        }else{
            boletoField.infoButtonAction = nil
        }
        addTextView(boletoField)
        boletoField.setupBilletMask()
        
        if model.isScanned {
            boletoField.textView.isUserInteractionEnabled = false
            boletoField.textView.textColor = UIColor.darkGray
        }
        
        addTextField(descriptionField)
        
        if let prefilledBoleto = prefilledBoleto {
            setBoletoFieldText(text: prefilledBoleto)
        }
        
        descriptionField.alwaysShowPlaceholder = true
        descriptionField.setTitleVisible(true)
        
        configureValidations()
    }
    
    // MARK: - Validation
    fileprivate enum ValidateError : String, Error, CustomStringConvertible {
        case boletoRequired = "Informe o código de barras"
        
        var description: String { return self.rawValue }
    }
    
    fileprivate func configureValidations(){
        var boletoRules = ValidationRuleSet<String>()
        boletoRules.add(rule: DGBoletoPartialValidation())
        
        var textView = boletoField.textView
        textView.validationRules = boletoRules
  
        // Description is optional
        descriptionField.validationRules = nil
        
        actionButton?.isEnabled = false
    }
    
    // MARK: - Internal Methods
    fileprivate func completeValidation(showMessageForCompleteValidation: Bool = true) -> Bool {
        if !validate(true).isValid {
            return false
        }
        
        // The only rule in the textView is for *partial* boletos, here we'll make sure
        // It's a *complete* boleto number and it's *valid*
        let completeRule = DGBoletoCompleteValidation()
        if !completeRule.validate(input: boletoField.textView.text) {
            if showMessageForCompleteValidation {
                boletoField.errorMessage = completeRule.error.localizedDescription
            }
            return false
        }
        
        return true
    }

    
    // Checks if the validation and enable/disable the next button
    fileprivate func validateNext(){
        actionButton?.isEnabled = completeValidation(showMessageForCompleteValidation: false)
    }
    
    // MARK: - User Actions
    @IBAction private func next(_ sender: Any){
        guard
            completeValidation(),
            let billetDescription = descriptionField.text
            else {
                return
        }
        
        loader?.isHidden = false
        
        model.verifyPayment(
            code: boletoField.textView.text.onlyNumbers,
            boletoDescription: billetDescription,
            from: self
        ) { [weak self] _ in
            self?.loader?.isHidden = true
        }
    }
    
    
    @objc func dismissVc() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func openScanner() {
        let scanner: DGBoletoScannerController = UIStoryboard.viewController(.digitalGoodsBoleto)
        scanner.model.digitalGood = model.digitalGood
        navigationController?.pushViewController(viewController: scanner, animated: true, completion: nil)
    }
    
    // MARK: - TextFieldDelegate
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if string.length > 1 { // if paste action
            let fullText = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
            
            if fullText.length > descriptionTextLength {
                textField.text = fullText.substring(from: 0, to: descriptionTextLength)
                validateNext()
                return false
            }
        }
        if textField.text?.length > descriptionTextLength-1 && !string.isEmpty {
            validateNext()
            return false
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        model.isScanned = false
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        guard textField.text != "" else {
            return
        }
        validate(true)
    }
    
    // MARK: - TextViewDelegate
    
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            descriptionField.becomeFirstResponder()
            return false
        }
        
        let result = super.textView(textView, shouldChangeTextIn: range, replacementText: text)
        
        if result {
            textView.text = NSString(string: textView.text!).replacingCharacters(in: range, with: text)
        }
        
        validateNext()
        return false
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        guard textView.text != "" else {
            return
        }
        validate(true)
    }
}
