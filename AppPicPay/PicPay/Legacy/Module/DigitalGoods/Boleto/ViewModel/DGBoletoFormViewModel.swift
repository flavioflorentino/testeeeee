import AnalyticsModule
import Core
import FeatureFlag
import Foundation
import PCI
import SwiftyJSON
import UI

enum VerifyErrorCode: String {
    case dueDate = "16032001"
    case timeout = "16032000"
    case othersAlreadyPaid = "16032004"
    case picpayAlreadyPaid = "16032005"
}

final class DGBoletoFormViewModel: NSObject {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    fileprivate var paymentManager = PPPaymentManager()
    var boleto: DGBoleto?
    var digitalGood: DGItemBoleto?
    var origin: String?
    var isScanned = false
    var didShowSurchagePopup = false
    var didShowInstallmentsSurchagePopup = false
    weak var viewController: UIViewController?
    
    private var fillableAmount: Double?
    
    private let popupPresenter = PopupPresenter()
    
    private let servicePCI: PaymentBillServiceProtocol
    
    private let abTest: BilletPaymentABTest = {
        let flag = FeatureManager.text(.experimentBilletPaymentDetailValues)
        return BilletPaymentABTest(rawValue: flag) ?? .a
    }()
    
    init(service: PaymentBillServiceProtocol = PaymentBillService()) {
        self.servicePCI = service
    }
    
    func verifyScannedCode(code: String, completed: @escaping ((PicPayErrorDisplayable?) -> ())) {
        let request = DGBoletoVerificationRequest()
        request.code = code
        
        DGBoletoApi().verify(request: request) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let boleto):
                    self?.boleto = boleto
                    completed(nil)
                case .failure(let error):
                    completed(error)
                }
            }
        }
    }
    
    func verifyPayment(code: String, boletoDescription: String, from vc: UIViewController, completed: @escaping ((PicPayErrorDisplayable?) -> ())) {
        viewController = vc
        
        let request = DGBoletoVerificationRequest()
        request.code = code
        
        if let billet = self.boleto, code == billet.lineCode, billet.amount.isNotEmpty {
            handleVerifyResult(billet: billet, description: boletoDescription, from: vc, completed: completed)
            return
        }
        
        DGBoletoApi().verify(request: request) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let billet):
                    self?.handleVerifyResult(billet: billet, description: boletoDescription, from: vc, completed: completed)
                case .failure(let error):
                    self?.handleVerifyFailure(error, from: vc, completed: completed)
                }
            }
        }
    }
    
    func handleVerifyResult(billet: DGBoleto, description boletoDescription: String, from vc: UIViewController, completed: @escaping ((PicPayErrorDisplayable?) -> ())) {
        guard billet.acceptOverduePayment() else {
            handleUnacceptedOverduePayment()
            completed(nil)
            return
        }
        
        if billet.isFillable {
            handleFillableBoleto(billet, description: boletoDescription, from: vc)
            completed(nil)
            return
        }
        
        if billet.alert != nil {
            billet.alert?.dismissOnTouchBackground = false
            billet.alert?.dismissWithGesture = false
            billet.alert?.overlayMaskAlpha = 0.0
            
            if let alert = billet.alert {
                AlertMessage.showAlert(alert, controller: vc, action: { [weak self] (popup, button, _) in
                    popup.dismiss(animated: true, completion: {
                        self?.goToPayment(billet, boletoDescription: boletoDescription, from: vc, completed: completed)
                    })
                })
                return
            }
        }
        
        goToPayment(billet, boletoDescription: boletoDescription, from: vc, completed: completed)
    }
    
    private func handleFillableBoleto(_ boleto: DGBoleto, description: String, from vc: UIViewController) {
        let nextController = DGVoucherOtherValueController.controllerFromStoryboard(.digitalGoodsCode)
        nextController.setup(screenTitle: digitalGood?.name ?? "", title: BilletLocalizable.howMuchDoYouWantToPay.text, max: 100000.0, min: nil, amountChange: { [weak self] newAmount in
            self?.fillableAmount = newAmount
            }, completion: { [weak self] vc in
                self?.goToPayment(boleto, boletoDescription: description, from: vc, completed: nil)
        })
        vc.navigationController?.pushViewController(nextController, animated: true)
    }
    
    private func handleUnacceptedOverduePayment() {
        let errorInfo = ErrorInfoModel(
            image: Assets.Icons.sadIcon.image,
            title: BilletLocalizable.billetOverdueTitle.text,
            message: BilletLocalizable.billetOverdueMessage.text,
            primaryAction: ErrorAction(title: DefaultLocalizable.btOkUnderstood.text) { errorController in
                let presentingController = errorController.presentingViewController
                presentingController?.presentingViewController?.dismiss(animated: true)
            }
        )
        
        presentError(errorInfo: errorInfo)
        Analytics.shared.log(BilletEvent.paymentDueDateDenied)
    }
    
    private func handleVerifyFailure(
        _ error: PicPayError,
        from controller: UIViewController,
        completed: @escaping ((PicPayErrorDisplayable?) -> ())
    ) {
        let verifyCodeErrors: [VerifyErrorCode] = [.othersAlreadyPaid, .picpayAlreadyPaid]
        let icon = verifyCodeErrors.contains { $0.rawValue == error.picpayCode }
            ? Assets.Icons.iconBigSuccess
            : Assets.Icons.sadIcon
        
        guard
            let transactionId = error.data?.dictionaryObject?["receipt_id"] as? String
            else {
                presentErrorPopup(with: icon.image, title: error.title, message: error.message)
                completed(nil)
                return
        }
        
        self.presentErrorPopup(
            with: icon.image,
            title: error.title,
            message: error.message,
            buttonTitle: BilletLocalizable.seeReceipt.text
        ) { [weak self] in
            self?.presentReceipt(transactionId: transactionId, from: controller)
        }
        
        completed(nil)
    }
    
    private func presentReceipt(transactionId: String, from controller: UIViewController) {
        let receiptModel = ReceiptWidgetViewModel(transactionId: transactionId, type: .PAV)
        TransactionReceipt.showReceipt(viewController: controller, receiptViewModel: receiptModel, feedItemId: nil)
        
        let installmentQuantity = paymentManager?.selectedQuotaQuantity ?? 1
        Analytics.shared.log(BilletEvent.transactionReceiptViewed(installment: installmentQuantity))
        Analytics.shared.log(BilletEvent.cardOnBillsVersion(isSplitApplied: installmentQuantity > 1))
        
        Analytics.shared.log(BilletEvent.transactionAccomplished)
        
        if self.boleto?.interest != nil {
            Analytics.shared.log(BilletEvent.paymentWithInterest)
        } else if self.boleto?.discount != nil {
            Analytics.shared.log(BilletEvent.paymentWithDiscount)
        }
    }
    
    private func goToPayment(_ billet: DGBoleto, boletoDescription: String, from: UIViewController, completed: ((PicPayErrorDisplayable?) -> ())?) {
        
        let boletoString: String = billet.amount.replacingOccurrences(of: ",", with: ".")
        
        guard let value: Double = billet.isFillable ? fillableAmount : Double(boletoString)! else {
            completed?(PicPayError(message: BilletLocalizable.tryAgainLater.text))
            return
        }
        
        self.boleto = billet
        self.boleto?.description = boletoDescription
        
        KVStore().setString(billet.lineCode, with: .lastBoleto)
        
        if billet.isFillable {
            paymentManager?.subtotal = NSDecimalNumber(value: value)
        }else{
            paymentManager?.subtotal = NSDecimalNumber(string: boletoString)
        }
        
        if let surcharge = billet.surcharge {
            paymentManager?.cardFee = NSDecimalNumber(value: surcharge)
        }
        let paymentScreen = DGPaymentViewController.controllerFromStoryboard()
        let paymentModel = DGPaymentViewModel(
            amount: value,
            type: .bill,
            sellerName: digitalGood?.name ?? "",
            sellerImageUrl: digitalGood?.imgUrl ?? "",
            sellerId: billet.sellerId,
            detailInfo: [],
            dependencies: DependencyContainer()
        )
        paymentModel.paymentType = BilletLocalizable.billet.text
        paymentModel.paymentManager = paymentManager
        paymentScreen.title = DefaultLocalizable.voucherPaymentTitle.text
        paymentScreen.setup(paymentManager: paymentManager!, model: paymentModel, delegate: self)
        paymentScreen.receiptDelegate = self
        
        // Add the right button to the payment screen + show SFSafariVC on tap
        if let infoUrl = digitalGood?.infoUrl, infoUrl != "" {
            paymentScreen.setRightButton()
            paymentScreen.rightBarButtonAction = { controller in
                guard let url = URL(string:infoUrl) else {
                    return
                }
                
                ViewsManager.presentSafariViewController(url, from: controller)
            }
        }
        completed?(nil)
        
        setPaymentScreenDetails(billet: billet, paymentScreen: paymentScreen)
        
        NotificationCenter.default.addObserver(
            forName: Notification.Name.Payment.methodChange,
            object: nil,
            queue: nil
        ) { _ in
            self.setPaymentScreenDetails(billet: billet, paymentScreen: paymentScreen)
        }
        
        NotificationCenter.default.addObserver(
            forName: Notification.Name.Payment.installmentChange,
            object: nil,
            queue: nil
        ) { _ in
            self.setPaymentScreenDetails(billet: billet, paymentScreen: paymentScreen)
        }
        
        from.navigationController?.pushViewController(paymentScreen, animated: true)
    }
    
    func setPaymentScreenDetails(billet: DGBoleto, paymentScreen: DGPaymentViewController) {
        let detailsFactory = BilletPaymentDetailFactory(billet: billet, paymentManager: paymentManager)
        var details: [DGPaymentDetailsBase?] = []
        
        details.append(detailsFactory.getBeneficiaryBankDetail())
    
        details.append(detailsFactory.getExpirationDate())
        
        details.append(detailsFactory.getPaymentDateDetail())
        
        details.append(detailsFactory.getInterestDetail { [weak self] in
            Analytics.shared.log(BilletEvent.interestInformationViewed)
            
            self?.presentErrorPopup(
                title: BilletLocalizable.taxDueDate.text,
                message: BilletLocalizable.billetDueDateMessage.text
            ) {
                Analytics.shared.log(BilletEvent.interestInformationUnderstood)
            }
        })
        
        details.append(detailsFactory.getDescriptionDetail())
        
        details.append(detailsFactory.getSurchargeDetail { [weak self] feeDescription, feeAmount in
            self?.presentSurchargePopup(
                title: BilletLocalizable.cardFeePopupTitle.text,
                description: BilletLocalizable.cardFeePopupMessage.text,
                fees: [
                    (surcharge: feeDescription, surchargeAmount: feeAmount)
                ],
                showCancel: false,
                on: paymentScreen,
                actionHandler: {
                    self?.didShowSurchagePopup = true
                }
            )
        })
        
        details.append(detailsFactory.getInstallmentsSurchargeDetail { [weak self] feeDescription, feeAmount in
            self?.presentSurchargePopup(
                title: BilletLocalizable.installmentFeePopupTitle.text,
                description: BilletLocalizable.installmentFeePopupMessage.text,
                fees: [
                    (surcharge: feeDescription, surchargeAmount: feeAmount)
                ],
                showCancel: false,
                on: paymentScreen,
                actionHandler: {
                    self?.didShowInstallmentsSurchagePopup = true
                }
            )
        })
        
        details.append(detailsFactory.getInstallmentsDetail())
        
        details.append(detailsFactory.getDiscountDetail())
        
        details.append(detailsFactory.getInstructionsDetail(with: digitalGood?.disclaimerMarkdown))
        
        paymentScreen.model?.detailInfo = details.compactMap { $0 }
        
        paymentScreen.prepareForPayment = nil
        didShowSurchagePopup = false
        didShowInstallmentsSurchagePopup = false
        prepareForPayment(billet: billet, paymentScreen: paymentScreen)
    }
    
    private func prepareForPayment(billet: DGBoleto, paymentScreen: DGPaymentViewController) {
        guard
            let paymentManager = self.paymentManager,
            paymentManager.cardTotal().doubleValue > 0,
            let surcharge = billet.surcharge
            else {
                return
        }
            
        paymentScreen.prepareForPayment = { [weak self] paymentScreen, completion in
            guard let self = self else {
                return
            }
            
            var canOpenPopup = !(self.didShowSurchagePopup && self.didShowInstallmentsSurchagePopup)
            
            if self.abTest == .a || paymentManager.selectedQuotaQuantity == 1 {
                canOpenPopup = !self.didShowSurchagePopup
            }
            
            guard canOpenPopup else {
                completion()
                return
            }
            
            let installmentQuantity = paymentManager.selectedQuotaQuantity
            Analytics.shared.log(BilletEvent.transactionTaxInformation(installment: installmentQuantity))
            
            let cardFeeDescription = BilletLocalizable.cardConvenienceFeeDescription.text
                .replacingOccurrences(of: "$", with: "\(surcharge)")
            let cardFeeAmount = paymentManager.cardConvenienceFee()?.decimalValue
            
            var title = BilletLocalizable.cardFeePopupTitle.text
            var description = BilletLocalizable.cardFeePopupMessage.text
            var fees = [(surcharge: cardFeeDescription, surchargeAmount: cardFeeAmount?.toCurrencyString())]
            
            if paymentManager.selectedQuotaQuantity > 1 && self.abTest != .a {
                let installmentFeeDescription = BilletLocalizable.cardInstallmentFeeDescription.text
                    .replacingOccurrences(of: "$", with: "\(billet.installmentInterest)")
                let installmentFeeAmount = paymentManager.installmentsTotalFee()?.decimalValue
                
                title = BilletLocalizable.paymentDisclaimerPopupTitle.text
                description = BilletLocalizable.paymentDisclaimerPopupMessage.text
                fees.append(
                    (surcharge: installmentFeeDescription, surchargeAmount: installmentFeeAmount?.toCurrencyString())
                )
            }
            
            self.presentSurchargePopup(
                title: title,
                description: description,
                fees: fees,
                showCancel: true,
                on: paymentScreen,
                actionHandler: {
                    self.didShowSurchagePopup = true
                    self.didShowInstallmentsSurchagePopup = true
                    
                    completion()
                    
                    let event = BilletEvent.transactionTaxInformationOptionSelected(
                        installment: installmentQuantity,
                        optionSelected: .acceptAdditionalTax
                    )
                    Analytics.shared.log(event)
                },
                closeHandler: {
                    self.didShowSurchagePopup = false
                    self.didShowInstallmentsSurchagePopup = false
                    
                    let event = BilletEvent.transactionTaxInformationOptionSelected(
                        installment: installmentQuantity,
                        optionSelected: .notNow
                    )
                    Analytics.shared.log(event)
                }
            )
        }
    }
    
    private func boletoAnalysesChoice(controller: DGPaymentViewController, popup: AlertPopupViewController, button: UIPPButton){
        button.startLoadingAnimating()
        popup.disableAllButtons()
        button.setTitle("", for: .disabled)
        
        DGBoletoApi().acceptAnalyses(data: button.model?.actionData) { (response) in
            DispatchQueue.main.async {
                button.stopLoadingAnimating()
                popup.enableAllButtons()
                
                switch response {
                case .success( _):
                    popup.dismiss(animated: true, completion: {
                        if let buttonAction = button.model?.action, buttonAction == .showReceipt {
                            controller.showReceipt()
                        } else {
                            controller.navigationController?.dismiss(animated: true, completion: nil)
                        }
                    })
                case .failure(let error):
                    popup.dismiss(animated: true, completion: {
                        controller.navigationController?.dismiss(animated: true, completion: {
                            AlertMessage.showAlert(error, controller: controller)
                        })
                    })
                }
            }
        }
    }
    
    private func createCVVPayload(paymentManager manager: PPPaymentManager, cvv: String?) -> CVVPayload? {
        guard let cvv = cvv else {
             return createLocalStoreCVVPayload(manager: manager)
         }
        
         return CVVPayload(value: cvv)
    }
    
    private func createLocalStoreCVVPayload(manager: PPPaymentManager) -> CVVPayload? {
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        guard manager.cardTotal()?.doubleValue != .zero,
              let cvv = dependencies.keychain.getData(key: KeychainKeyPF.cvv(id)) else {
                return nil
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func createBillPayload(request: DGBoletoApiRequest, manager: PPPaymentManager) -> BillPayload? {
        guard let params = try? request.toParams(),
            let data = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) else {
                return nil
        }
        return try? JSONDecoder.decode(data, to: BillPayload.self)
    }
    
    private func payBillPCI(
        request: DGBoletoApiRequest,
        cvv: String?,
        password: String,
        manager: PPPaymentManager,
        completion: @escaping (WrapperResponse) -> Void
    ) {
        guard let billPayload = createBillPayload(request: request, manager: manager) else {
            let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            completion(PicPayResult.failure(error))
            return
        }
        
        let cvv = createCVVPayload(paymentManager: manager, cvv: cvv)
        let payload = PaymentPayload<BillPayload>(cvv: cvv, generic: billPayload)
        servicePCI.pay(password: password, payload: payload, isNewArchitecture: false) { result in
            DispatchQueue.main.async {
                let mappedResult = result
                    .map { BaseApiGenericResponse(dictionary: $0.json) }
                    .mapError { $0.picpayError }

                completion(mappedResult)
            }
        }
    }
}

// MARK: Payment request construction
extension DGBoletoFormViewModel: DGPaymentDelegate {
    func didPerformPayment(with manager: PPPaymentManager, cvv: String?, additionalPayload: [String : Any], pin: String, biometry: Bool, privacyConfig: String, someErrorOccurred: Bool, completion: @escaping (WrapperResponse) -> Void) {
        paymentManager = manager
        
        guard let paymentManager = paymentManager else {
            return
        }
        
        let request = DGBoletoApiRequest()
        request.paymentManager = paymentManager
        request.pin = pin
        request.biometry = biometry
        request.code = isScanned ? boleto?.barCode : boleto?.lineCode
        request.description = boleto?.description
        request.cip = boleto?.cip
        request.privacyConfig = privacyConfig
        request.someErrorOccurred = someErrorOccurred
        
        if let origin = origin {
            request.origin = origin
        }
        
        if FeatureManager.isActive(.pciBill) {
            payBillPCI(request: request, cvv: cvv, password: pin, manager: manager, completion: completion)
            return
        }
        
        
        DGBoletoApi().pay(request: request, pin: pin) { [weak self] (response) in
            ConsumerManager.shared.loadConsumerBalance()
            
            switch(response) {
            case .success(let response):
                completion(.success(response))
            case .failure(let error):
                guard let error = self?.handleErrorCode(error) else {
                    return
                }
                
                completion(.failure(error))
            }
        }
    }
    
    private func handleErrorCode(_ error: PicPayError) -> PicPayError? {
        switch error.picpayCode {
        case VerifyErrorCode.timeout.rawValue:
            let errorInfo = ErrorInfoModel(
                image: Assets.Icons.sadIcon.image,
                title: BilletLocalizable.billetTimeoutTitle.text,
                message: BilletLocalizable.billetTimeoutMessage.text,
                primaryAction: ErrorAction(title: DefaultLocalizable.tryAgain.text) { errorController in
                    Analytics.shared.log(BilletEvent.paymentTimeoutDeniedTryAgain)
                    self.viewController?.navigationController?.popViewController(animated: true)
                    errorController.dismiss(animated: true)
                },
                secondaryAction: ErrorAction(title: DefaultLocalizable.notNow.text) { errorController in
                    Analytics.shared.log(BilletEvent.paymentTimeoutDeniedNotNow)
                    let presentingController = errorController.presentingViewController
                    presentingController?.presentingViewController?.dismiss(animated: true)
                }
            )
            presentError(errorInfo: errorInfo)
        case VerifyErrorCode.dueDate.rawValue:
            let errorInfo = ErrorInfoModel(
                image: Assets.Icons.sadIcon.image,
                title: BilletLocalizable.billetOverdueTitle.text,
                message: BilletLocalizable.billetOverdueMessage.text,
                primaryAction: ErrorAction(title: DefaultLocalizable.btOkUnderstood.text) { errorController in
                    let presentingController = errorController.presentingViewController
                    presentingController?.presentingViewController?.dismiss(animated: true)
                }
            )
            presentError(errorInfo: errorInfo)
            Analytics.shared.log(BilletEvent.paymentDueDateDenied)
            
        default:
            return error
        }
        
        return nil
    }
    
    private func presentError(errorInfo: ErrorInfoModel) {
        DispatchQueue.main.async {
            let errorController = ErrorFactory.make(with: errorInfo)
            self.viewController?.present(errorController, animated: true)
        }
    }
}

// MARK: - DGPaymentReceiptDelegate
extension DGBoletoFormViewModel: DGPaymentReceiptDelegate {
    func shouldShowReceiptAfterOptInAlert() -> Bool {
        return false
    }
    
    func didTapButtonReceiptOptInAlert(_ controller: DGPaymentViewController, popup: AlertPopupViewController, button: UIPPButton) {
        guard let buttonModel = button.model else {
            return
        }
        
        switch buttonModel.action {
        case .showReceipt:
            self.boletoAnalysesChoice(controller: controller, popup: popup, button: button)
        case .cancelBillsAnalysis:
            self.boletoAnalysesChoice(controller: controller, popup: popup, button: button)
        default:
            popup.dismiss(animated: true) {
                controller.showReceipt()
            }
        }
    }
}

// MARK: - Present Popup

private extension DGBoletoFormViewModel {
    func presentSurchargePopup(
        title: String,
        description: String,
        fees: [(surcharge: String, surchargeAmount: String?)],
        showCancel: Bool,
        on viewController: UIViewController,
        actionHandler: (() -> Void)? = nil,
        closeHandler: (() -> Void)? = nil
    ) {
        guard let navController = viewController.navigationController else {
            return
        }
        
        let popup = SurchargeConfirmation()
        popup.setup(
            title: title,
            description: description,
            fees: fees,
            shouldShowCancel: showCancel
        )
        
        popup.action = actionHandler
        popup.close = closeHandler
        
        popupPresenter.showPopup(popup, fromViewController: navController)
    }
    
    func presentErrorPopup(
        with icon: UIImage? = nil,
        title: String,
        message: String,
        buttonTitle: String = BilletLocalizable.okGotIt.text,
        actionHandler: (() -> Void)? = nil
    ) {
        let popupType: PopupType
        
        if let icon = icon {
            popupType = .image(icon)
        } else {
            popupType = .none
        }
        
        let popup = PopupViewController(title: title, preferredType: popupType)
        
        popup.setAttributeDescription(format(message: message))
        
        let action = PopupAction(title: buttonTitle, style: .fill) {
            actionHandler?()
        }
        popup.addAction(action)
        
        popupPresenter.showPopup(popup)
    }
    
    func format(message: String?) -> NSMutableAttributedString? {
        let formattedString = message?
            .replacingOccurrences(of: "<strong>", with: "[b]")
            .replacingOccurrences(of: "</strong>", with: "[b]")
        
        let preString = formattedString?.html2AttributedString?.string
        
        return preString?.attributedStringWith(
            normalFont: Typography.bodySecondary().font(),
            highlightFont: Typography.bodySecondary(.highlight).font(),
            normalColor: Colors.grayscale500.color,
            highlightColor: Colors.grayscale500.color,
            textAlignment: .center,
            underline: false,
            separator: "[b]"
        )
    }
}
