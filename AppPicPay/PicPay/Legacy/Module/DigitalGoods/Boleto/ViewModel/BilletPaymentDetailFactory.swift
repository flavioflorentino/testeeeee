import AnalyticsModule
import CorePayment
import FeatureFlag
import UI

final class BilletPaymentDetailFactory {
    private let billet: DGBoleto
    private let paymentManager: PPPaymentManager?
    
    private var paymentMethod: PaymentMethod {
        let chargeToCard = paymentManager?.cardTotal() ?? 0
        let chargeToAccountBalance = paymentManager?.balanceTotal() ?? 0
        let useBalance = paymentManager?.usePicPayBalance() ?? false
        
        switch (chargeToCard, chargeToAccountBalance) {
        case (.zero, .zero):
            return useBalance ? .accountBalance : .card
        case let (.zero, balance) where balance != .zero:
            return .accountBalance
        case let (card, .zero) where card != .zero:
            return .card
        case let (card, balance) where card != .zero && balance != .zero:
            return .balanceAndCard
        default:
            return .card
        }
    }
    
    private var abTest: BilletPaymentABTest {
        let flag = FeatureManager.text(.experimentBilletPaymentDetailValues)
        return BilletPaymentABTest(rawValue: flag) ?? .a
    }
    
    init(billet: DGBoleto, paymentManager: PPPaymentManager? = nil) {
        self.billet = billet
        self.paymentManager = paymentManager

        let event = BilletEvent.transactionViewedAB(
            installment: paymentManager?.selectedQuotaQuantity ?? 1,
            paymentMethod: paymentMethod
        )
        Analytics.shared.log(event)
        Analytics.shared.log(BilletEvent.transactionViewed)
    }
    
    func getExpirationDate() -> DGPaymentDetailsItem? {
        return DGPaymentDetailsItem(
            name: BilletLocalizable.dueDate.text,
            detail: billet.expirationDate,
            detailColor: billet.isOverdue == true ? Colors.critical900.color : Colors.grayscale400.color
        )
    }
    
    func getPaymentDateDetail() -> DGPaymentDetailsItem? {
        DGPaymentDetailsItem(name: BilletLocalizable.payday.text, detail: billet.paymentDate)
    }
    
    func getInterestDetail(completion: @escaping () -> Void) -> DGPaymentDetailsItem? {
        guard let interest = billet.interest, let value = interest.toCurrencyString() else {
            return nil
        }
        
        Analytics.shared.log(BilletEvent.interestViewed)
        
        return DGPaymentDetailsItem(name: BilletLocalizable.taxDueDate.text, detail: value, showInfo: true) {
            completion()
        }
    }
    
    func getDiscountDetail() -> DGPaymentDetailsItem? {
        guard let discount = billet.discount, let value = discount.toCurrencyString() else {
            return nil
        }
        
        Analytics.shared.log(BilletEvent.discountViewed)
        
        return DGPaymentDetailsItem(name: BilletLocalizable.discountValue.text, detail: value)
    }
    
    func getBeneficiaryBankDetail() -> DGPaymentDetailsItem? {
        DGPaymentDetailsItem(name: BilletLocalizable.recipient.text, detail: billet.beneficiaryBank)
    }
    
    func getDescriptionDetail() -> DGPaymentDetailsItem? {
        guard let description = billet.description, description.isNotEmpty else {
            return nil
        }
        
        return DGPaymentDetailsItem(name: BilletLocalizable.description.text, detail: description)
    }
    
    func getSurchargeDetail(completion: @escaping (_ feeDescription: String, _ feeAmount: String) -> Void) -> DGPaymentDetailsItem? {
        guard
            let paymentManager = paymentManager,
            paymentManager.cardTotal().doubleValue > 0,
            let surcharge = billet.surcharge
            else {
                return nil
        }
        
        let description: String
        let formattedSurcharge: String
        
        switch abTest {
        case .a:
            description = BilletLocalizable.cardConvenienceFeeA.text
            formattedSurcharge = "\(surcharge)%"
        case .b:
            let cardFee = paymentManager.cardConvenienceFee()?.decimalValue ?? 0
            
            description = BilletLocalizable.cardConvenienceFeeB.text
            formattedSurcharge = cardFee.toCurrencyString() ?? ""
        case .c:
            description = BilletLocalizable.cardConvenienceFeeB.text
            formattedSurcharge = "\(surcharge)%"
        }
        
        return DGPaymentDetailsItem(name: description, detail: formattedSurcharge, showInfo: true) {
            Analytics.shared.log(BilletEvent.cardFeeInformation)
            let feeDescription = BilletLocalizable.cardConvenienceFeeDescription.text
                .replacingOccurrences(of: "$", with: "\(surcharge)")
            let feeAmount = paymentManager.cardConvenienceFee()?.decimalValue ?? 0
            
            completion(feeDescription, feeAmount.toCurrencyString() ?? "")
        }
    }
    
    func getInstallmentsSurchargeDetail(completion: @escaping (_ feeDescription: String, _ feeAmount: String) -> Void) -> DGPaymentDetailsItem? {
        guard let paymentManager = paymentManager, paymentManager.selectedQuotaQuantity > 1 else {
            return nil
        }
        
        let description: String
        let formattedSurcharge: String
        
        switch abTest {
        case .b:
            let installmentsFee = paymentManager.installmentsTotalFee()?.decimalValue ?? 0
            
            description = BilletLocalizable.cardInstallmentFee.text
            formattedSurcharge = installmentsFee.toCurrencyString() ?? ""
        case .c:
            description = BilletLocalizable.cardInstallmentFee.text
            formattedSurcharge = "\(billet.installmentInterest)%"
            
        default:
            return nil
        }
        
        return DGPaymentDetailsItem(name: description, detail: formattedSurcharge, showInfo: true) {
            let installment = paymentManager.selectedQuotaQuantity
            Analytics.shared.log(BilletEvent.installmentFeeInformation(installment: installment))
            
            let feeDescription = BilletLocalizable.cardInstallmentFeeDescription.text
                .replacingOccurrences(of: "$", with: "\(self.billet.installmentInterest)")
            let feeAmount = paymentManager.installmentsTotalFee()?.decimalValue ?? 0
            
            completion(feeDescription, feeAmount.toCurrencyString() ?? "")
        }
    }
    
    func getInstallmentsDetail() -> DGPaymentDetailsItem? {
        guard
            let paymentManager = paymentManager,
            paymentManager.selectedQuotaQuantity > 1,
            abTest != .a
            else {
                return nil
        }
        
        let installmentQuantity = paymentManager.selectedQuotaQuantity
        let installmentValue = paymentManager.quotaValue()?.decimalValue ?? 0
        let installments = "\(installmentQuantity)x \(installmentValue.toCurrencyString() ?? "")"
        
        return DGPaymentDetailsItem(name: BilletLocalizable.installments.text, detail: installments)
    }
    
    func getTotalValueDetail() -> DGPaymentDetailsItem? {
        guard let paymentManager = paymentManager, abTest != .a else {
            return nil
        }
        
        let interest = Decimal(billet.interest ?? 0)
        let discount = Decimal(billet.discount ?? 0)
        let subtotal = paymentManager.total()?.decimalValue ?? 0
        let total = (subtotal + interest) - discount
        
        return DGPaymentDetailsItem(
            name: BilletLocalizable.totalValue.text,
            detail: total.toCurrencyString() ?? "",
            detailColor: Colors.grayscale750.color,
            backgroundColor: Colors.groupedBackgroundTertiary.color
        )
    }
    
    func getInstructionsDetail(with digitalGoodDisclaimer: String?) -> DGPaymentImportantDetailItem? {
        if let instructions = billet.instructions {
            return DGPaymentImportantDetailItem(text: instructions)
        } else if let instructions = digitalGoodDisclaimer {
            return DGPaymentImportantDetailItem(text: instructions)
        }
        
        return nil
    }
}
