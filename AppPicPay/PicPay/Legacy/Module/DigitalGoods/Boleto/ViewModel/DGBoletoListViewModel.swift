//
//  DGBoletoListViewModel.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 08/02/18.
//

import Foundation
import SwiftyJSON
import FeatureFlag
import AnalyticsModule
import Billet

final class DGBoletoViewModel: DGVoucherViewModel {
    private let qrCode = DGVoucherProduct(id: "qr", title: BilletLocalizable.useBarcodeReader.text, hidePrice: true, img: #imageLiteral(resourceName: "ico_boleto_camera"))
    private let manual = DGVoucherProduct(id: "manual", title: BilletLocalizable.enterBarcodeManually.text, hidePrice: true, img: #imageLiteral(resourceName: "ico_boleto_manual"))
    
    private let howPayBillet = VoucherBanner(type: .howPayBillet,
                                             title: BilletLocalizable.howToPayBilletTitle.text,
                                             description: BilletLocalizable.howToPayBilletDescription.text,
                                             image: Assets.DigitalGoods.iluBilletPayment.image,
                                             actionTitle: BilletLocalizable.howToPayBilletActionTitle.text,
                                             actionLink: FeatureManager.text(.opsFaqHowToPayBilletUrl))
    
    override func loadProducts(`for` digitalGood: DGItem) {
        isLoading = false
        
        products = [qrCode, manual]
        productsUpdate?()
    }
    
    override func loadBanners() {
        if FeatureManager.isActive(.releaseHowToPayBilletPresenting) {
            banners = [howPayBillet]
        } else {
            banners = []
        }
        
        bannersUpdate?()
    }

    override func goToPayment(from: UIViewController) {
        guard let product = product else { return }
        
        switch product.id {
        case qrCode.id:
            let scanner: DGBoletoScannerController = UIStoryboard.viewController(.digitalGoodsBoleto)
            guard let raw = digitalGood?.raw, let boletoItem = DGItemBoleto(json: raw) else {
                return
            }
            scanner.model.digitalGood = boletoItem
            scanner.model.origin = origin
            from.navigationController?.pushViewController(scanner, animated: true)
            
            let event = BilletPaymentEvent.cardOnBillsOptionSelected(optionSelected: .useReader)
            Analytics.shared.log(event)
            
        case manual.id:
            guard FeatureManager.isActive(.releaseNewBilletPaymentPresentingBool) else {
                let boletoForm: DGBoletoFormViewController = UIStoryboard.viewController(.digitalGoodsBoleto)
                guard let raw = digitalGood?.raw, let boletoItem = DGItemBoleto(json: raw) else {
                    return
                }
                from.title = ""
                boletoForm.model.digitalGood = boletoItem
                boletoForm.model.origin = origin
                from.navigationController?.pushViewController(boletoForm, animated: true)
                return
            }
            
            let origin = BilletOrigin(value: self.origin ?? "")
            let controller = BilletFormFactory.make(with: origin)
            from.navigationController?.pushViewController(controller, animated: true)
            
            let event = BilletPaymentEvent.cardOnBillsOptionSelected(optionSelected: .enter)
            Analytics.shared.log(event)
            
        default:
            return
        }
    }
}
