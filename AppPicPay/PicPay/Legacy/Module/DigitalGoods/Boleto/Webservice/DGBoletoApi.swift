import Core
import FeatureFlag
import Foundation

final class DGBoletoApiRequest: DGPayRequest {
    // Variable params
    var code: String?
    var description: String?
    
    override func toParams() throws -> [String : Any] {
        let params = try super.toParams()
        
        let optionalParamsToBeMerged = [
            "code": code,
            "description": description ?? ""
            ] as [String : Any?]
        
        let paramsToBeMerged = try obligatoryParams(params: optionalParamsToBeMerged)
        
        // Merge super's params with this class' specific params overriding any duplicated entries
        return params.merging(paramsToBeMerged, uniquingKeysWith: { (_, new) in new })
    }
}

final class DGBoletoVerificationRequest: BaseApiRequest {
    // Variable params
    var code: String?
    
    // All params are obligatory
    func obligatoryParams(params: [String: Any?]) throws -> [String: Any] {
        let nonNil = params.nilsRemoved
        
        if nonNil.keys.count < params.keys.count {
            throw PicPayError(message: "Missing params")
        }
        
        return nonNil
    }
    
    func toParams() throws -> [String: Any] {
        let params = [
            "code": code
            ] as [String : Any?]
        
        return try obligatoryParams(params: params)
    }
}

final class DGBoletoApi: BaseApi {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func verify(request: DGBoletoVerificationRequest, _ completion: @escaping ((PicPayResult<DGBoleto>) -> Void) ) {
        do {
            let params = try request.toParams()
            guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
                requestManager
                    .apiRequest(endpoint: kWsUrlBillsBarCode, method: .get, parameters: params)
                    .responseApiObject(completionHandler: completion)
                return
            }
            
            // Core network version
            let endpoint = DigitalGoodsBoletoVerificationEndpoints.barcode(params: params)
            Api<Data>(endpoint: endpoint).execute { result in
                ApiSwiftJsonWrapper(with: result).transform(completion)
            }
        } catch let error {
            guard let error = error as? PicPayError else {
                return
            }
            completion(PicPayResult.failure(error))
        }
        return
    }
    
    // DGBoletoApiRequest is a subclass of DGPayRequest.
    // It adds specific (and necessary) information to the boleto payment request
    func pay(request: DGBoletoApiRequest, pin: String, _ completion: @escaping ((WrapperResponse) -> Void)) {
        do {
            let params = try request.toParams()
            guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
                requestManager
                    .apiRequest(endpoint: kWsUrlBillsPayment, method: .post, parameters: params, pin: pin)
                    .responseApiObject(completionHandler: completion)
                    return
            }
            
            // Core network version
            let endpoint = DigitalGoodsBoletoVerificationEndpoints.payment(params: params, pin: pin)
            Api<Data>(endpoint: endpoint).execute { result in
                ApiSwiftJsonWrapper(with: result).transform(completion)
            }
        } catch let error {
            guard let error = error as? PicPayError else {
                return
            }
            completion(PicPayResult.failure(error))
        }
    }
    
    func acceptAnalyses(data: [AnyHashable: Any]?, _ completion: @escaping ((WrapperResponse) -> Void)) {
        let params:[String: Any] = data as? [String: Any] ?? [:]
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlBillsAnalysesChoice, method: .post, parameters: params)
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        let enpoint = DigitalGoodsBoletoVerificationEndpoints.analysesChoice(params: params)
        Api<Data>(endpoint: enpoint).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
}
