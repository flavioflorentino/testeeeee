import Core
import Foundation

enum DigitalGoodsBoletoVerificationEndpoints: ApiEndpointExposable {
    case barcode(params: [String: Any])
    case analysesChoice(params: [String: Any])
    case payment(params: [String: Any], pin: String)
    
    var customHeaders: [String: String] {
        guard case let .payment(_, pin) = self else {
            return [:]
        }
        return ["password": pin]
    }
    
    var path: String {
        switch self {
        case .barcode:
            return "bills/barcode"
        case .analysesChoice:
            return "bills/analyses/choice"
        case .payment:
            return "bills/v2/payment"
        }
    }
    
    var parameters: [String: Any] {
        guard case let .barcode(params) = self else {
            return [:]
        }
        return params
    }
    
    var body: Data? {
        switch self {
        case .barcode:
            return nil
        case let .analysesChoice(params):
            return params.toData()
        case let .payment(params, _):
            return params.toData()
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .barcode:
            return .get
        case .analysesChoice, .payment:
            return .post
        }
    }
}
