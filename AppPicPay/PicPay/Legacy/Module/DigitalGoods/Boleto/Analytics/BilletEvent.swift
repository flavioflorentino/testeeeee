import AnalyticsModule
import CorePayment
import FeatureFlag

enum BilletEvent: AnalyticsKeyProtocol, Equatable {
    case transactionViewed
    case transactionViewedAB(installment: Int, paymentMethod: PaymentMethod)
    case cardFeeInformation
    case installmentFeeInformation(installment: Int)
    case transactionTaxInformation(installment: Int)
    case transactionTaxInformationOptionSelected(installment: Int, optionSelected: BilletEventOptionSelection)
    case transactionAccomplished
    case transactionReceiptViewed(installment: Int)
    case interestViewed
    case discountViewed
    case interestInformationViewed
    case interestInformationUnderstood
    case paymentWithInterest
    case paymentWithDiscount
    case paymentDueDateDenied
    case paymentTimeoutDenied
    case paymentTimeoutDeniedTryAgain
    case paymentTimeoutDeniedNotNow
    case cardOnBillsOptionSelected(optionSelected: BilletEventOptionSelection)
    case cardOnBillsVersion(isSplitApplied: Bool)
    
    private var abTest: BilletPaymentABTest {
        let flag = FeatureManager.text(.experimentBilletPaymentDetailValues)
        return BilletPaymentABTest(rawValue: flag) ?? .a
    }
    
    private var name: String {
        switch self {
        case .transactionViewed:
            return "Bills Transaction Screen"
        case .transactionViewedAB:
            return "Bills Status Transaction Screen - \(abTest.rawValue)"
        case .cardFeeInformation:
            return "Bills Card Fee Information - \(abTest.rawValue)"
        case .installmentFeeInformation:
            return "Bills Split Fee Information - \(abTest.rawValue)"
        case .transactionTaxInformation:
            return "Bills Payment Tax Information - \(abTest.rawValue)"
        case .transactionTaxInformationOptionSelected:
            return "Bills Payment Tax Information - \(abTest.rawValue) Option Selected"
        case .transactionAccomplished:
            return "Bills Transaction Accomplished"
        case .transactionReceiptViewed:
            return "Bills Receipt Screen - \(abTest.rawValue)"
        case .interestViewed:
            return "Bills Interest Payment Screen - Viewed"
        case .discountViewed:
            return "Bills Discount Payment Screen - Viewed"
        case .interestInformationViewed:
            return "Bills - Interest Information Viewed"
        case .interestInformationUnderstood:
            return "Bills - Interest Information Understood"
        case .paymentWithInterest:
            return "Bills - Payment with Interest"
        case .paymentWithDiscount:
            return "Bills - Payment with Discount"
        case .paymentDueDateDenied:
            return "Bills - Payment Due Date Denied"
        case .paymentTimeoutDenied:
            return "Bills - Payment Timeout Denied"
        case .paymentTimeoutDeniedTryAgain:
            return "Bills - Payment Timeout Denied - Try Again"
        case .paymentTimeoutDeniedNotNow:
            return "Bills - Payment Timeout Denied - Not Now"
        case .cardOnBillsOptionSelected:
            return "Bills - Card On Bills"
        case .cardOnBillsVersion:
            return "Bills - Card On Bills Version"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .transactionViewed, .transactionAccomplished:
            return [
                "test_version": abTest.rawValue
            ]
            
        case let .transactionViewedAB(installment, paymentMethod):
            return [
                "split_viewd": "\(installment)",
                "origin_payment_method": method(for: paymentMethod)
            ]
            
        case let .installmentFeeInformation(installment):
            return [
                "split_viewd": "\(installment)"
            ]
            
        case let .transactionTaxInformation(installment), let .transactionReceiptViewed(installment):
            guard installment > 1 else {
                return [
                    "tax_applied": "card"
                ]
            }
            
            return [
                "tax_applied": "card+split",
                "split_viewd": "\(installment)"
            ]
            
        case let .transactionTaxInformationOptionSelected(installment, optionSelected):
            guard installment > 1 else {
                return [
                    "tax_applied": "card"
                ]
            }
            
            return [
                "tax_applied": "card+split",
                "split_viewd": "\(installment)",
                "option_selected": optionSelected.rawValue
            ]
            
        case let .cardOnBillsOptionSelected(optionSelected):
            return [
                "option_selected": optionSelected.rawValue
            ]
            
        case let .cardOnBillsVersion(isSplitApplied):
            return [
                "version": "\(abTest.rawValue)2",
                "split_applied": isSplitApplied
            ]
            
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        let firebaseEvents: [BilletEvent] = [.transactionViewed, .transactionAccomplished]
        return firebaseEvents.contains(self) ? [.firebase] : [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent(name, properties: properties, providers: providers)
    }
    
    private func method(for paymentMethod: PaymentMethod) -> String {
        switch paymentMethod {
        case .accountBalance:
            return "wallet"
        case .card:
            return "card"
        case .balanceAndCard:
            return "wallet+card"
        }
    }
}

enum BilletEventOptionSelection: String {
    case acceptAdditionalTax = "aceito a taxa adicional"
    case notNow = "agora nao"
    case useReader = "Usar Leitor"
    case enter = "Digitar"
    case knowMore = "Saiba mais"
    case howToPay = "Card como pagar"
}
