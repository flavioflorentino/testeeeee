//
//  DGBoleto.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 09/02/18.
//

import Foundation
import SwiftyJSON

final class DGBoleto: BaseApiResponse {
    var beneficiaryBank: String
    var amount: String
    var isExpired: Bool
    var expirationDate: String?
    var barCode: String
    var instructions: String?
    var lineCode: String
    var isFillable: Bool
    var sellerId: String
    var surcharge: Double?
    var description: String?
    var paymentDate: String?
    var placeholder: String?
    var alert: Alert?
    var cip: [String: Any]?
    var interest: Double?
    var discount: Double?
    var isOverdue: Bool?
    var canReceiveOverdue: Bool?
    var installmentInterest: Double
    
    init(linecode: String, placeholder: String) {
        beneficiaryBank = ""
        amount = ""
        isExpired = false
        barCode = ""
        lineCode = linecode
        isFillable = false
        sellerId = ""
        installmentInterest = 3.49
        self.placeholder = placeholder
    }
    
    required init?(json: JSON) {
        guard
            let amount = json["amount"].string,
            let isExpired = json["expired"].bool,
            let barCode = json["bar_code"].string,
            let isFillable = json["fillable"].bool,
            let lineCode = json["line_code"].string,
            let sellerIdInt = json["seller_id"].int else {
                return nil
        }
        
        let beneficiaryBank = json["beneficiary"].dictionary?["name"]?.string
        let fantasyName = json["beneficiary"].dictionary?["fantasy_name"]?.string
        
        if let name = beneficiaryBank ?? fantasyName {
            self.beneficiaryBank = name
        }else{
            // We *need* a name
            return nil
        }
        
        if let expirationDate = json["due_date"].string {
            self.expirationDate = expirationDate
        }
        
        if let surcharge = json["surcharge"].string,
            let doubleValue = Double(surcharge) {
            self.surcharge = doubleValue
        }
        
        if let paymentDate = json["payment_date"].string {
            self.paymentDate = paymentDate
        }

        self.instructions = json["instructions"].string
        
        self.amount = amount
        self.isExpired = isExpired
        self.sellerId = String(describing: sellerIdInt)
        self.barCode = barCode
        self.lineCode = lineCode
        
        self.isFillable = isFillable
        
        if json["alert"].exists(), let alert = Alert(json: json["alert"]) {
            self.alert = alert
        }
        
        cip = json["cip"].dictionaryObject
        interest = cip?["interest"] as? Double
        discount = cip?["discount"] as? Double
        isOverdue = cip?["is_overdue"] as? Bool
        canReceiveOverdue = cip?["can_receive_overdue"] as? Bool
        installmentInterest = 3.49
    }
    
    func acceptOverduePayment() -> Bool {
        guard
            let isOverdue = self.isOverdue,
            let canReceiveOverdue = self.canReceiveOverdue
            else {
                return !isExpired
        }

        return canReceiveOverdue || !(canReceiveOverdue || isOverdue)
    }
}
