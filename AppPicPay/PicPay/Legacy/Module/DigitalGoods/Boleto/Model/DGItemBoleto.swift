//
//  DGItemBoleto.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 05/03/18.
//

import Foundation
import SwiftyJSON

final class DGItemBoleto: DGItem {
    var barcodeInfoUrl: String?
    var ccAlertTitle: String?
    var ccAlertDescription: String?
    // Fallback for DGVoucherProduct name
    var selectionHeader: String?
    
    required init?(json: JSON) {
        selectionHeader = json["list_header"].string
        barcodeInfoUrl = json["barcode_info_url"].string
        ccAlertTitle = json["credit_card_tax_alert_title"].string
        ccAlertDescription = json["credit_card_tax_alert_disclaimer"].string
        
        super.init(json: json)
    }
}
