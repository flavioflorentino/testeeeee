//
//  DGBoletoPartialValidation.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 22/02/18.
//

import Foundation
import Validator

final class DGBoletoPartialValidation: ValidationRule {
    var error: ValidationError {
        return PicPayError.init(message: BilletLocalizable.wrongNumber.text)
    }
    
    typealias InputType = String
    var input = ""
    private let common = DGBoletoCommonValidation()
    
    func validate(input: InputType?) -> Bool {
        guard let input = input else {
    return false
}
        self.input = input.onlyNumbers
        common.input = self.input
        
        return validate()
    }
    
    private func validate() -> Bool {
        if (common.isConvenio && input.length == 48) ||
            (!common.isConvenio && input.length > 46) {
            return DGBoletoCompleteValidation().validate(input: input)
        }

        let ranges = common.isConvenio ? common.convenioBlockRanges : common.boletoBlockRanges
        for range in  ranges {
            guard input.length > range.upperBound else { return true }
            
            let validationMethodIdentifier = common.isConvenio ? input[common.convenioBlockRanges[0]][2] : nil
            guard common.validateBlock(range: range, verifier: validationMethodIdentifier) else {
    return false
}
        }
        
        return true
    }
}
