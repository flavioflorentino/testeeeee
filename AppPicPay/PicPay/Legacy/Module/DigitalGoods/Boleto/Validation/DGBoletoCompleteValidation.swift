//
//  DGBoletoFormValidation.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 16/02/18.
//

import Foundation
import Validator

final class DGBoletoCompleteValidation: ValidationRule {
    typealias InputType = String
    var input = ""
    private let common = DGBoletoCommonValidation()
    
    
    private func getAllConvenioBlocks() -> [String] {
        return common.convenioBlockRanges.map({ input[$0] })
    }
    
    private func getAllBoletoBlocks() -> [String] {
        return common.boletoBlockRanges.map({ input[$0] })
    }
    
    func validate(input: InputType?) -> Bool {
        guard let input = input else {
            return false
        }
        self.input = input.onlyNumbers
        common.input = self.input
        
        return common.isConvenio ? validateConvenio() : validateBoleto()
    }
    
    private func validateConvenio() -> Bool {
        guard input.count == 48 else {
            return false
        }
        
        let fields = getAllConvenioBlocks()
        let validationMethodIdentifier = fields[0][2]
        guard common.convenioBlockRanges.reduce(true, { $0 && common.validateBlock(range: $1, verifier: validationMethodIdentifier) }) else {
            return false
        }
        
        let dvGeral = fields[0][3]
        let completeVerificationCode = fields[0][0..<3] + fields[0][4..<fields[0].length] + fields[1] + fields[2] + fields[3]
        
        return common.validateBlock(range: nil, code: completeVerificationCode, dv: dvGeral, verifier: validationMethodIdentifier)
    }
    
    private func validateBoleto() -> Bool {
        guard input.count > 46 else {
            return false
        }
        guard common.boletoBlockRanges.reduce(true, { $0 && common.validateBlock(range: $1, verifier: nil) }) else {
            return false
        }
        
        let fields = getAllBoletoBlocks()
        
        let dvGeral = input[32]
        let field5 = input[33..<input.length]
        
        let completeVerificationCode = fields[0][0..<4] + field5 + fields[0][4..<fields[0].length] + fields[1] + fields[2]
        
        return dvGeral == common.calculateMod11Dv(completeVerificationCode)
    }
    
    
    var error: ValidationError {
        return PicPayError(message: BilletLocalizable.wrongNumber.text)
    }
}
