//
//  DGBoletoValidationCommon.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 22/02/18.
//

import Foundation
import Validator

final class DGBoletoCommonValidation: NSObject {
    var input = ""
    var convenioBlockRanges = [
        0..<11,
        12..<23,
        24..<35,
        36..<47
    ]
    
    var boletoBlockRanges = [
        0..<9,
        10..<20,
        21..<31
    ]
    
    var isConvenio: Bool {
        return input.starts(with: "8")
    }
    
    // The verifier param is only used by convenios
    func validateBlock(range: CountableRange<Int>?, code: String? = nil, dv externalDv: String? = nil, verifier: String?) -> Bool {
        var field = ""
        
        if let code = code {
            field = code
        }else if let range = range{
            field = input[range]
        }else{
            return false
        }
        
        var dv = ""
        if let range = range {
            dv = input[min(input.length, range.upperBound)]
        }else if let externalDv = externalDv {
            dv = externalDv
        }else{
            return false
        }
        
        var mod: String?
        if !isConvenio {
            mod = calculateMod10Dv(field)
        }else if (verifier == "6" || verifier == "7" || verifier == nil) {
            mod = calculateMod10Dv(field)
        }else if (verifier == "8" || verifier == "9") {
            mod = calculateMod11Dv(field, zeroOnSpecialCase: true)
        }
        return dv == mod
    }
    
    func calculateMod10Dv(_ code: String) -> String {
        let reversed = code.reversed()
        var sum = 0
        
        for (index, char) in reversed.enumerated() {
            guard let intChar = Int(String(char)) else { continue }
            let multiplier = index % 2 == 0 ? 2 : 1
            let result = String(intChar * multiplier)
            result.forEach({ sum += Int(String($0))! })
        }
        let verifier = 10 - sum % 10
        return verifier == 10 ? "0" : String(verifier)[0]
    }
    
    func calculateMod11Dv(_ code: String, zeroOnSpecialCase: Bool = false) -> String {
        let reversed = code.reversed()
        var sum = 0
        var multiplier = 2
        
        for char in reversed {
            guard let value = Int(String(char)) else { continue }
            sum += value * multiplier
            multiplier = multiplier == 9 ? 2 : multiplier + 1
        }
        
        let dv = String(11 - sum % 11)
        let isSpecial = dv == "10" || dv == "11"
        
        if zeroOnSpecialCase && isSpecial {
            return "0"
        }
        
        return isSpecial ? "1" : dv
    }
}
