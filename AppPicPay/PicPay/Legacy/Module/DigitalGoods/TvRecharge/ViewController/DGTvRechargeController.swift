//
//  DGTvRechargeController.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 08/03/18.
//

import Foundation
import UI

final class DGTvRechargeController: BaseFormController {
    static func fromNib() -> DGTvRechargeController {
        return controllerFromStoryboard(.digitalGoodsTv)
    }
    
    @IBOutlet weak var codeField: UIPPFloatingTextField!
    @IBOutlet weak var infoButton: UIButton! {
        didSet {
            infoButton.isHidden = true
        }
    }
    
    let model = DGTvRechargeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupForm()
        setupViews()
    }
    
    private func setupForm() {
        codeField.delegate = self
        actionButton?.isEnabled = false
    }
    
    private func setupViews() {
        if let infoUrl = model.digitalGood?.infoUrl, !infoUrl.isEmpty {
            let underlineAttribute:[NSAttributedString.Key: Any] = [.underlineStyle: NSUnderlineStyle.single.rawValue, .foregroundColor: UIColor.init(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 1)]
            let underlineAttributedString = NSAttributedString(string: BilletLocalizable.iDonKnowMyCode.text, attributes: underlineAttribute)
            infoButton.setAttributedTitle(underlineAttributedString, for: .normal)
            infoButton.isHidden = false
        }
        
        
        // Can't just set boletoField.textView.text because we need to mask the string
        if let subscriberCode = model.subscriberCode,
            let shouldChange = codeField.delegate?.textField?(codeField, shouldChangeCharactersIn: NSRange(location: 0, length: 0), replacementString: subscriberCode),
            shouldChange {
            codeField.text = subscriberCode
        }
    }
    
    @IBAction private func next(_ sender: Any) {
        model.goToPayment(from: self)
    }
    
    @IBAction private func openInfo(_ sender: Any) {
        guard let string = model.digitalGood?.infoUrl,
            let url = URL(string: string) else {
            return
        }
        ViewsManager.presentSafariViewController(url, from: self)
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let result = super.textField(textField, shouldChangeCharactersIn: range, replacementString: string)
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        
        actionButton?.isEnabled = txtAfterUpdate != ""
        model.subscriberCode = txtAfterUpdate
        return result
    }
}
