//
//  DGTvRechargeApi.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 09/03/18.
//

import Foundation

final class DGTvRechargeRequest: DGVoucherApiRequest {
    // Variable params
    var subscriberCode: String?
    
    override func toParams() throws -> [String : Any] {
        let params = try super.toParams()
        
        let optionalParamsToBeMerged = [
            "digitalgoods": [
                "subscriber_code": subscriberCode,
                "type": "tvrecharges"
            ]
        ] as [String : Any?]
        
        let paramsToBeMerged = try super.obligatoryParams(params: optionalParamsToBeMerged)
        
        // Merge super's params with this class' specific params overriding any duplicated entries
        return params.deepMerge(paramsToBeMerged)
    }
}
