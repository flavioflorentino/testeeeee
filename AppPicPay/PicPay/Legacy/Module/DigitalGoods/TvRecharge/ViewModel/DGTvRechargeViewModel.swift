import Core
import FeatureFlag
import Foundation
import PCI

final class DGTvRechargeViewModel: NSObject {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    private let service: TvRechargeServiceProtocol = TvRechargeService()
    
    var digitalGood: DGItem? {
        didSet {
            if let id = digitalGood?.id {
                subscriberCode = KVStore().dictionaryFor(.tvRecharges)?[id] as? String
            }
        }
    }
    var product: DGProduct?
    var paymentManager = PPPaymentManager()
    var subscriberCode: String?
    
    private func getDetailsFor(product: DGProduct, service: DGItem) -> [DGPaymentDetailsBase] {
        var details: [DGPaymentDetailsBase] = []
        
        details.append(DGPaymentDetailsItem(name: BilletLocalizable.item.text, detail: product.name))
        
        if let subscriberCode = self.subscriberCode {
            details.append(DGPaymentDetailsItem(name: BilletLocalizable.customerID.text, detail: subscriberCode))
        }
        
        if let longDescription = product.description {
            details.append(DGPaymentLongDetailItem(text: longDescription))
        }
        if let disclaimerMarkdown = service.disclaimerMarkdown, disclaimerMarkdown != "" {
            details.append(DGPaymentImportantDetailItem(text: disclaimerMarkdown))
        }
        
        return details
    }
    
    func goToPayment(from: UIViewController) {
        guard let digitalGood = digitalGood,
            let product = product as? DGVoucherProduct,
            let paymentManager = paymentManager
            else {
                return
        }
        
        // Create if not exists
        var cache = KVStore().dictionaryFor(.tvRecharges)
        if cache == nil {
            KVStore().set(value: [:], with: .tvRecharges)
            cache = [:]
        }
        
        if let subscriberCode = subscriberCode, var cache = cache {
            cache[digitalGood.id] = subscriberCode
            KVStore().set(value: cache, with: .tvRecharges)
        }
        
        paymentManager.subtotal = NSDecimalNumber(value: product.price)
        
        guard FeatureManager.isActive(.newTvRecharge) else {
            openLegacyPaymentViewController(from: from, product: product, digitalGood: digitalGood)
            return
        }
        
        openNewPaymentViewController(from: from, product: product, digitalGood: digitalGood)
    }
    
    private func openNewPaymentViewController(from: UIViewController, product: DGProduct, digitalGood: DGItem) {
        guard let subscriberCode = subscriberCode else {
            return
        }
        
        let details = getDetailsFor(product: product, service: digitalGood)
        let items = DigitalGoodsPayment(
            title: digitalGood.name,
            image: digitalGood.imgUrl,
            link: digitalGood.infoUrl,
            payeeId: digitalGood.sellerId,
            value: product.price,
            paymentType: .tvRecharge
        )
        
        let model = DigitalGoodsTvRecharge(subscriberCode: subscriberCode, productId: product.id)
        let service = DigitalGoodsTvRechargeService(model: model, item: digitalGood)
        let orchestrator = DigitalGoodsPaymentOrchestrator(items: items, paymentDetails: details, service: service)
        from.navigationController?.pushViewController(orchestrator.paymentViewController, animated: true)
    }
    
    private func openLegacyPaymentViewController(from: UIViewController, product: DGProduct, digitalGood: DGItem) {
        guard let paymentManager = paymentManager else {
            return
        }
        
        let paymentScreen = DGPaymentViewController.controllerFromStoryboard()
        let details = getDetailsFor(product: product, service: digitalGood)
        let paymentModel = DGPaymentViewModel(
            amount: product.price,
            type: .tvRecharge,
            sellerName: digitalGood.name,
            sellerImageUrl: digitalGood.imgUrl,
            sellerId: digitalGood.id,
            detailInfo: details,
            dependencies: DependencyContainer()
        )
        
        paymentScreen.title = DefaultLocalizable.voucherPaymentTitle.text
        paymentScreen.setup(paymentManager: paymentManager, model: paymentModel, delegate: self)
        
        // Add the right button to the payment screen + show SFSafariVC on tap
        if let infoUrl = digitalGood.infoUrl {
            paymentScreen.setRightButton()
            paymentScreen.rightBarButtonAction = { controller in
                guard let url = URL(string:infoUrl) else {
                    return
                }
                ViewsManager.presentSafariViewController(url, from: controller)
            }
        }
        
        from.navigationController?.pushViewController(paymentScreen, animated: true)
    }
}

// MARK: Payment request construction
extension DGTvRechargeViewModel: DGPaymentDelegate {
    func didPerformPayment(with manager: PPPaymentManager, cvv: String?, additionalPayload: [String: Any], pin: String, biometry: Bool, privacyConfig: String, someErrorOccurred: Bool, completion: @escaping (WrapperResponse) -> Void) {
        self.paymentManager = manager
        guard let product = product,
            let digitalGood = digitalGood,
            let paymentManager = paymentManager else {
            return
        }
        
        let request = DGTvRechargeRequest()
        
        request.paymentManager = paymentManager
        request.pin = pin
        request.biometry = biometry
        request.sellerId = digitalGood.sellerId
        request.digitalGoodId = digitalGood.id
        request.productId = product.id
        request.privacyConfig = privacyConfig
        request.someErrorOccurred = someErrorOccurred
        request.subscriberCode = subscriberCode
        
        if FeatureManager.isActive(.pciTvRecharge) {
            createTransactionPCI(manager: manager, request: request, password: pin, cvv: cvv, completion: completion)
            return
        }
        
        DGVoucherApi().pay(request: request, pin: pin, completion)
    }
    
    private func createTransactionPCI(
        manager: PPPaymentManager,
        request: DGTvRechargeRequest,
        password: String, cvv: String?,
        completion: @escaping (WrapperResponse) -> Void
    ) {
        guard let tvRechargePayload = createTvRechargePayload(manager: manager, request: request) else {
            let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            completion(PicPayResult.failure(error))
            return
        }
        let cvv = createCVVPayload(manager: manager, cvv: cvv)
        let payload = PaymentPayload<DigitalGoodsPayload<TvRecharge>>(cvv: cvv, generic: tvRechargePayload)
        service.createTransaction(password: password, payload: payload, isNewArchitecture: false) { result in
            DispatchQueue.main.async {
                let mappedResult = result
                    .map { BaseApiGenericResponse(dictionary: $0.json) }
                    .mapError { $0.picpayError }
                
                completion(mappedResult)
            }
        }
    }
    
    private func createTvRechargePayload(manager: PPPaymentManager, request: DGTvRechargeRequest) -> DigitalGoodsPayload<TvRecharge>? {
        guard let params = try? request.toParams(),
            let data = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) else {
                return nil
        }
        return try? JSONDecoder.decode(data, to: DigitalGoodsPayload<TvRecharge>.self)
    }
        
    private func createCVVPayload(manager: PPPaymentManager, cvv: String?) -> CVVPayload? {
        guard let cvv = cvv else {
            return createLocalStoreCVVPayload(manager: manager)
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func createLocalStoreCVVPayload(manager: PPPaymentManager) -> CVVPayload? {
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        guard
            manager.cardTotal()?.doubleValue != .zero,
            let cvv = dependencies.keychain.getData(key: KeychainKeyPF.cvv(id))
            else {
                return nil
        }
        
        return CVVPayload(value: cvv)
    }
}
