//
//  DGTvRechargeListViewModel.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 08/03/18.
//

import Foundation

final class DGTvRechargeListViewModel: DGVoucherViewModel {
    override init() {
        super.init()
        api.digitalGoodType = "tvrecharges"
    }
    
    override func goToPayment(from: UIViewController) {
        guard let digitalGood = digitalGood,
            let product = product
            else {
            return
        }
        
        let vc = DGTvRechargeController.fromNib()
        vc.model.digitalGood = digitalGood
        vc.model.product = product
        from.title = ""
        from.navigationController?.pushViewController(vc, animated: true)
    }
}
