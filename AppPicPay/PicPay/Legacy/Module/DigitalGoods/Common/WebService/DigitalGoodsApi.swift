import Core
import FeatureFlag
import Foundation
import SwiftyJSON

// TODO: Can be easily refactored for ALL createTransctions calls (not only digital goods):
// 1- Externalize "origin"
// 2- Maybe change some hardcoded params to variables
// 3- Externalize seller_id
class DGPayRequest: BaseApiRequest {
    // Hardcoded params4
    let planType = "A"
    let addressId = "0"
    let creditCardId = { () -> String in
        let cardId = CreditCardManager.shared.defaultCreditCard?.id ?? "0"
        return cardId
    }()
    
    // Variable params
    var origin = "DigitalGoods"
    var pin: String?
    var biometry: Bool?
    var paymentManager: PPPaymentManager?
    var installment: Int?
    var credit: String?
    var total: String?
    var privacyConfig: String?
    var sellerId: String?
    var someErrorOccurred: Bool?
    
    var cip: [String: Any]?
    
    var paramsToBeMerged: [String: Any]?
    
    // All params are obligatory
    func obligatoryParams(params: [String: Any?]) throws -> [String: Any] {
        let nonNil = params.compactMapValues { $0 }
        
        if nonNil.keys.count < params.keys.count {
            throw PicPayError(message: "Missing params")
        }
        
        return nonNil
    }
    
    func mergeToParams(params: [String: Any]) {
        self.paramsToBeMerged = params
    }
    
    func toParams() throws -> [String: Any] {
        var params: [String : Any?] = [
            "origin": origin,
            "pin": pin,
            "biometry": biometry,
            "credit": nil, // Needs to be set later or it will throw
            "parcelas": installment != nil ? installment : paymentManager?.selectedQuotaQuantity,
            "total": nil, // Needs to be set later or it will throw
            "plan_type": "A",
            "address_id": "0",
            "credit_card_id": CreditCardManager.shared.defaultCreditCard?.id ?? "0",
            "feed_visibility": privacyConfig,
            "duplicated" : someErrorOccurred == true ? 1 : 0
        ]
        
        if let sellerId = sellerId {
            params["seller_id"] = sellerId
        }
        
        // Needs to be true else it's gonna throw
        if let credit = credit {
            params["credit"] = credit
        } else if let credit = paymentManager?.balanceTotal().doubleValue {
            params["credit"] = String(format: "%.2f", credit)
        }
        
        // Needs to be true else it's gonna throw
        if let total = total {
            params["total"] = total
        } else if let total = paymentManager?.cardTotalWithoutInterest().doubleValue {
            params["total"] = String(format: "%.2f", total)
        }
        
        if let cip = cip {
            params["cip"] = cip
        }
        
        return try obligatoryParams(params: params)
    }
}

final class DigitalGoodsApi: BaseApi {
    typealias DGListResponse = BaseApiListResponse<DGItem>
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func getDGInfo(id: String, completion: @escaping ((PicPayResult<DGItem>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
            requestManager
                .apiRequest(endpoint: "digitalgoods/services/\(id)", method: .get, parameters: [:])
                .responseApiObject(completionHandler: completion)
            return
        }
        
        // Core network version
        let endpoint = DigitalGoodsEndpoints.info(id: id)
        Api<Data>(endpoint: endpoint).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
    
    func pay(request: DGPayRequest, pin: String, _ completion: @escaping ((WrapperResponse) -> Void)) {
        do {
            let params = try request.toParams()
            guard dependencies.featureManager.isActive(.transitionApiSwiftJsonDGToCore) else {
                requestManager
                    .apiRequest(endpoint: "api/createTransaction.json", method: .post, parameters: params, pin: pin)
                    .responseApiObject(completionHandler: completion)
                return
            }
            
            // Core network version
            let endpoint = DigitalGoodsEndpoints.transaction(params: params, pin: pin)
            Api<Data>(endpoint: endpoint).execute { result in
                ApiSwiftJsonWrapper(with: result).transform(completion)
            }
        } catch let error {
            guard let error = error as? PicPayError else {
                return
            }
            completion(PicPayResult.failure(error))
        }
    }
}
