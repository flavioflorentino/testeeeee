import Core
import Foundation

enum DigitalGoodsEndpoints: ApiEndpointExposable {
    case transaction(params: [String: Any], pin: String)
    case info(id: String)
    
    var path: String {
        switch self {
        case .transaction:
            return "api/createTransaction.json"
        case let .info(id):
            return "digitalgoods/services/\(id)"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .transaction:
            return .post
        case .info:
            return .get
        }
    }
    
    var body: Data? {
        guard case let .transaction(params, _) = self else {
            return nil
        }
        return params.toData()
    }
    
    var customHeaders: [String : String] {
        guard case let .transaction(_, pin) = self else {
            return [:]
        }
        return ["password": pin]
    }
}
