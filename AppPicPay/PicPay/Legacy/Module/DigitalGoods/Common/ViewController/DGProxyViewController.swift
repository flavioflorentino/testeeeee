import Foundation
import SnapKit
import UI

final class DGProxyViewController: PPBaseViewController {
    
    let serviceId: String
    
    @objc init(with id: String) {
        self.serviceId = id
        super.init(nibName: nil, bundle: nil)
        
    }

    override func viewDidLoad() {
        let loader = UIActivityIndicatorView(style: .gray)
        loader.startAnimating()
        view.addSubview(loader)
        loader.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.height.equalTo(20)
            make.width.equalTo(20)
        }
        
        if self.navigationController?.viewControllers.count == 1 {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.btClose.text, style: .plain, target: self, action: #selector(close))
        }
        
        view.backgroundColor = Palette.ppColorGrayscale000.color

        DigitalGoodsApi().getDGInfo(id: serviceId) { [weak self] result in
            DispatchQueue.main.async {
                switch (result){
                case .success(let data):
                    guard let strongSelf = self else {
            return
        }
                    DGHelpers.openDigitalGoodFlow(for: data, viewController: strongSelf, replace: true, animated: false)
                case .failure(_):
                    break
                }
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func close(){
        dismiss(animated: true, completion: nil)
    }
}
