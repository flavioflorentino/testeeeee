import SwiftyJSON

enum DGService: String {
    case phoneRecharge = "phonerecharges"
    case tvRecharge = "tvrecharges"
    case digitalcodes = "digitalcodes"
    case boleto = "boleto"
    case transportpass = "transportpass"
    case parking = "parkings"
    case charge = "cobrar"
    case externalTransfer = "transferir"
}

struct DGInfo: Decodable {
    let sellerId: Int
    let bannerUrl: String?
    let infoUrl: String?
    let descriptionLargeMarkdown: String?
    let disclaimerMarkdown: String?
    let selectionType: DGDisplayType?
}

enum DGDisplayType: String, Decodable {
    case grid
    case list
}

class DGItem: NSObject, BaseApiResponse {
    var id: String
    var name = ""
    var itemDescription = ""
    var longDescription: String?
    var longDescriptionMarkdown: String?
    var infoUrl: String?
    var service: DGService?
    var sellerId = ""
    var imgUrl: String?
    var bannerImgUrl: String?
    var bannerImg: UIImage?
    var displayType: DGDisplayType = .grid
    var disclaimerMarkdown: String?
    var raw: JSON?
    var isStudentAccount = false
    var screenId: String = ""
    
    init(id: String, service: DGService? = nil) {
        self.id = id
        if let service = service {
            self.service = service
        }
        super.init()
    }
    
    required init?(json: JSON) {
        self.raw = json
        
        id = json["_id"].string ?? ""
        name = json["name"].string ?? ""
        
        itemDescription = json["description"].string ?? ""
        service = DGService(rawValue: json["service"].string ?? "phonerecharges")
        imgUrl = json["image_url"].string ?? ""
        sellerId = String(describing: json["seller_id"].int ?? 0)
        longDescription = json["description_large"].string
        longDescriptionMarkdown = json["description_large_markdown"].string
        disclaimerMarkdown = json["disclaimer_markdown"].string
        infoUrl = json["info_url"].string
        bannerImgUrl = json["banner_img_url"].string
        if let string = json["selection_type"].string,
            let type = DGDisplayType(rawValue: string) {
            displayType = type
        }
        isStudentAccount = json["student_account"].bool ?? false
        screenId = json["screen_identifier"].string ?? ""
    }
    
    init?(dictionary: [String: Any]) {
        let json = JSON(dictionary)
        self.raw = json

        id = json["_id"].string ?? ""
        name = json["name"].string ?? ""
        
        itemDescription = json["description"].string ?? ""
        service = DGService(rawValue: json["service"].string ?? "phonerecharges")
        imgUrl = json["imageUrl"].string ?? ""
        sellerId = String(describing: json["sellerId"].int ?? 0)
        longDescription = json["descriptionLarge"].string
        longDescriptionMarkdown = json["descriptionLargeMarkdown"].string
        disclaimerMarkdown = json["disclaimerMarkdown"].string
        infoUrl = json["infoUrl"].string
        bannerImgUrl = json["bannerImgUrl"].string
        if let string = json["selectionType"].string,
            let type = DGDisplayType(rawValue: string) {
            displayType = type
        }
        isStudentAccount = json["studentAccount"].bool ?? false
        screenId = json["screenIdentifier"].string ?? ""
    }

    func populate(with info: DGInfo) {
        sellerId = "\(info.sellerId)"
        bannerImgUrl = info.bannerUrl
        infoUrl = info.infoUrl
        longDescriptionMarkdown = info.descriptionLargeMarkdown
        disclaimerMarkdown = info.disclaimerMarkdown
        displayType = info.selectionType ?? displayType
    }
}
