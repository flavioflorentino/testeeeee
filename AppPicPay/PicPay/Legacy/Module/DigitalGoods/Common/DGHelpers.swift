import FeatureFlag
import Foundation
import PIX
import Store
import SwiftyJSON

final class DGHelpers: NSObject {
    enum Origin: String {
        case homeSugestions = "from_suggestions"
        case homeFavorites = "from_favorites"
        case payMain = "from_main_payment"
        case payStore = "from_store"
        case fromServices = "from_services"
        case paySearch = "from_search"
        case myQrCode = "from_qr_code"
        case profile = "from_user_profile"
        case digitalGoods = "from_digital_goods"
        case deeplink = "Deeplink"
        case contacts = "from_contacts"
        
        init(originValue: String?, newOrigins: Bool = false) {
            guard
                let originValue = originValue,
                let oldOrigin = SearchHelper.Origin(rawValue: originValue)
            else {
                self = .digitalGoods
                return
            }
            
            switch oldOrigin {
            case .homeSugestions:
                self = .homeSugestions
            case .homeFavorites:
                self = .homeFavorites
            case .payMain:
                self = .payMain
            case .payStore:
                self = newOrigins ? .fromServices : .payStore
            case .paySearch:
                self = .paySearch
            case .myCode:
                self = .myQrCode
            case .contacts:
                self = .contacts
            default:
                self = .digitalGoods
            }
        }
    }
    
    private static let legacyStoreHandler = DGHelpers()
    
    @objc
    static func openBoletoFlow(viewController: UIViewController, origin: String, startWithScanner: Bool = false) {
        guard let vc = DGHelpers.controllerForBoletoFlow(origin: origin, startWithScanner: startWithScanner) else {
            return
        }
        viewController.present(vc, animated: true, completion: nil)
    }
    
    @objc
    static func controllerForBoletoFlow(origin: String, startWithScanner: Bool = false) -> UIViewController? {
        // We should *always use the remote config object for boletos"
        guard let item = DGHelpers.getBoletoLocalData() else {
            return nil
        }
        
        let boletoController: DGVoucherController = UIStoryboard.viewController(.digitalGoodsCode)
        boletoController.model = DGBoletoViewModel()
        
        boletoController.model.digitalGood = item
        boletoController.listSectionTitle = item.selectionHeader ?? ""
        boletoController.model.origin = origin
        
        boletoController.screenTitle = item.name
        
        if startWithScanner {
            let scanner: DGBoletoScannerController = UIStoryboard.viewController(.digitalGoodsBoleto)
            let navController = PPNavigationController(rootViewController: scanner)
            scanner.model.digitalGood = item
            scanner.model.origin = origin
            return navController
        }
        
        return PPNavigationController(rootViewController: boletoController)
    }
    
    @objc
    static func controllerForDigitalGoodFlow(`for` digitalGood: DGItem, origin: String? = nil, replace: Bool = false) -> UIViewController? {
        guard let service = digitalGood.service else {
            return nil
        }
        
        switch service {
        case .phoneRecharge:
            let phoneRecharge: DGRechargePhoneViewController = UIStoryboard.viewController(.digitalGoodsRecharge)
            phoneRecharge.setup()
            phoneRecharge.model?.digitalGood = digitalGood
            
            return PPNavigationController(rootViewController: phoneRecharge)
        case .digitalcodes, .tvRecharge:
            let digitalCodes: DGVoucherController = UIStoryboard.viewController(.digitalGoodsCode)
            digitalCodes.model = DGVoucherViewModel()
            if service == .tvRecharge {
                digitalCodes.model = DGTvRechargeListViewModel()
            }
            digitalCodes.model.digitalGood = digitalGood
            digitalCodes.screenTitle = digitalGood.name
            
            return PPNavigationController(rootViewController: digitalCodes)
        case .boleto:
            return DGHelpers.controllerForBoletoFlow(origin: origin ?? "DigitalGoods")
        case .transportpass:
            let proxy = PPNavigationController(rootViewController: DGTicketProxyViewController(digitalGoodId: digitalGood.id, sellerId: digitalGood.sellerId))
            return proxy
        case .parking:
            let parking = DGParkingViewController.fromNib()
            parking.model.digitalGood = digitalGood
            return PPNavigationController(rootViewController: parking)
        case .charge:
            let dgHelperOrigin = DGHelpers.Origin(originValue: origin)
            return PaymentRequestSelectorFactory().make(origin: dgHelperOrigin)
        case .externalTransfer:
            let dgHelperOrigin = DGHelpers.Origin(originValue: origin)
            return ExternalTransferIntroFactory().make(origin: dgHelperOrigin)
        }
    }
    
    // Starts the digital good flow according to the service type
    @objc
    static func openDigitalGoodFlow(`for` digitalGood: DGItem, viewController: UIViewController, origin: String? = nil, replace: Bool = false, animated: Bool = true) {
        
        let newStoreFeatureFlag = FeatureManager.isActive(.experimentStoreOpenNewStoreBool)
        
        if newStoreFeatureFlag,
           openNewStore(with: digitalGood, from: viewController) {
            return
        }
        
        guard let vc = DGHelpers.controllerForDigitalGoodFlow(for: digitalGood, origin: origin, replace: replace) else {
            return
        }
        
        // Replaces the current VC (used for push notifications)
        if replace, let navController = viewController.navigationController, let last = navController.viewControllers.last {
            let nv = vc as? UINavigationController
            navController.replace(last, by: nv?.viewControllers.last ?? vc)
            return
        }
        
        viewController.present(vc, animated: true)
    }
    
    private static func openNewStore(with dgItem: DGItem, from viewController: UIViewController) -> Bool {
        Store.present(
            item: .init(dgItem.screenId),
            with: legacyStoreHandler,
            from: viewController
        )
    }

    @objc
    static func getBoletoLocalData() -> DGItemBoleto? {
        DGItemBoleto(json: JSON(FeatureManager.json(.boletoData) as Any))
    }
}

extension DGHelpers: LegacyStoreHandler {
    func viewControllerToWebview(with url: URL) -> UIViewController {
        WebViewFactory.make(with: url)
    }

    func routeToPayment(_ item: StorePayment, from viewController: UIViewController) {
        if let paymentViewControler = StorePaymentOrchestrator(item).paymentViewController {
            viewController.present(UINavigationController(rootViewController: paymentViewControler), animated: true)
        }
    }
    
    func viewControllerForDigitalGood(with dictionary: [String: Any]) -> UIViewController? {
        guard let digitalGood = DGItem(dictionary: dictionary),
              let viewController = DGHelpers.controllerForDigitalGoodFlow(for: digitalGood) else {
            return nil
        }
        return viewController
    }
    
    func addressViewController(
        with content: StoreAddressScreenContent,
        onAddressSelected: @escaping (StoreAddressResult) -> Void
    ) -> UIViewController {
        let viewModel = AddressListViewModel(displayContextHeader: content.shouldDisplayHeader)
    
        let viewController = AddressListViewController(viewModel: viewModel)
        viewController.addressContextTitle = content.title
        viewController.addressContextText = content.text
        viewController.addressContextImage = content.image
        
        let addressSelection: (ConsumerAddressItem?) -> () = { addressItem in
            guard
                let addressItem = addressItem,
                let addressId = addressItem.id
            else { return }
            
            let address = StoreAddressResult(
                id: String(addressId),
                alias: addressItem.addressName,
                street: addressItem.streetName,
                number: addressItem.streetNumber,
                complement: addressItem.addressComplement,
                neighborhood: addressItem.neighborhood,
                city: addressItem.city,
                state: addressItem.state,
                zipCode: addressItem.zipCode
            )
            
            onAddressSelected(address)
        }
        
        viewController.onAddressSelected = addressSelection
        viewController.onNewAddressAdded = addressSelection
        
        return viewController
    }
    
    func openReceipt(above viewController: UIViewController, with transactionId: String, type: String) {
        let receiptModel = ReceiptWidgetViewModel(transactionId: transactionId, type: ReceiptWidgetViewModel.ReceiptType(rawValue: type))
        TransactionReceipt.showReceipt(viewController: viewController, receiptViewModel: receiptModel)
    }
}
