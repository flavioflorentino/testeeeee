import Foundation
import SwiftyJSON

final class P2PItem: NSObject {
    let name: String
    let descriptionText: String
    let imageUrl: String
    let service: ServiceType

    init(name: String, descriptionText: String, imageUrl: String, service: ServiceType) {
        self.name = name
        self.descriptionText = descriptionText
        self.imageUrl = imageUrl
        self.service = service
    }
}

extension P2PItem {
    convenience init?(json: JSON) {
        let name = json[CodingKeys.name.rawValue].string ?? ""
        let descriptionText = json[CodingKeys.description.rawValue].string ?? ""
        let imageUrl = json[CodingKeys.imageUrl.rawValue].string ?? ""
        let service = ServiceType(rawValue: json[CodingKeys.service.rawValue].string ?? "") ?? .unknown

        self.init(name: name, descriptionText: descriptionText, imageUrl: imageUrl, service: service)
    }
}

extension P2PItem {
    enum ServiceType: String {
        case transfer = "transferir"
        case unknown
    }
}

private extension P2PItem {
    enum CodingKeys: String {
        case name
        case description
        case imageUrl = "image_url"
        case service
    }
}
