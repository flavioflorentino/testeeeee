import Foundation

struct P2PRequest: Codable {
    private enum CodingKeys: String, CodingKey {
        case payeeId = "payee_id"
        case consumerValue = "consumer_value"
        case surcharge
        case isBiometry = "biometry"
        case password = "pin"
        case credit
        case installments = "parcelas"
        case cardId = "consumer_credit_card_id"
        case privacyConfig = "feed_visibility"
        case message
        case someErrorOccurred = "duplicated"
    }
    
    let payeeId: String
    let consumerValue: String
    let surcharge: Double?
    let isBiometry: String
    let password: String
    let credit: String
    let installments: String
    let cardId: String
    let privacyConfig: String
    let message: String?
    let someErrorOccurred: String
}
