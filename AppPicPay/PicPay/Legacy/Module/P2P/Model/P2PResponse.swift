import Foundation
import SwiftyJSON

struct P2PResponse: Decodable {
    private enum CodingKeys: String, CodingKey {
        case id
        case value = "consumer_value"
        case receipt
        case transaction = "P2PTransaction"
        case data
    }
    
    let id: String
    let value: String
    let widgets: [ReceiptWidgetItem]?
    
    init(from decoder: Decoder) throws {
        var array: [ReceiptWidgetItem] = []
        let rootContainer = try decoder.container(keyedBy: CodingKeys.self)
        let container = try rootContainer.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        let transaction = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .transaction)
        
        id = try transaction.decode(String.self, forKey: .id)
        value = try transaction.decode(String.self, forKey: .value)
        
        guard let json = try? container.decode(JSON.self, forKey: .receipt) else {
            widgets = nil
            return
        }
        
        array = WSReceipt.createReceiptWidgetItem(json)
        
        guard array.isEmpty else {
            widgets = array
            return
        }
        widgets = nil
    }
}
