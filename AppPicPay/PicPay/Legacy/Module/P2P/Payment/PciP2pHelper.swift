import Core
import FeatureFlag
import Foundation
import PCI
import UI

final class PciP2pHelper: NSObject {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    private var coordinator: Coordinating?
    private let service: P2PServiceProtocol = P2PService()
    private var insertedCvv: String?
    
    @objc var paymentIsPci: Bool {
        FeatureManager.isActive(.pciP2P)
    }
    
    @objc
    func createTransaction(request: P2PTransactionRequest, manager: PPPaymentManager, navigationController: UINavigationController?, onSuccess: @escaping (PPP2PTransaction) -> Void, onError: @escaping (NSError) -> Void) {
        guard checkNeedCvv(manager: manager, request: request) else {
            let cvv = isLimitExceededPay(request: request) ? insertedCvv : nil
            makeTransaction(request: request, manager: manager, navigationController: navigationController, informedCvv: cvv, onSuccess: onSuccess, onError: onError)
            insertedCvv = nil
            return
        }
        
        createCvvFlowCoordinator(
            navigationController: navigationController,
            completedCvv: { [weak self] cvv in
                self?.insertedCvv = cvv
                self?.makeTransaction(request: request, manager: manager, navigationController: navigationController, informedCvv: cvv, onSuccess: onSuccess, onError: onError)
            },
            completedNoCvv: {
                let error = PicPayError(message: DefaultLocalizable.cvvNotInformed.text)
                onError(error)
            }
        )
    }
    
    private func makeTransaction(
        request: P2PTransactionRequest,
        manager: PPPaymentManager,
        navigationController: UINavigationController?,
        informedCvv: String?,
        onSuccess: @escaping (PPP2PTransaction) -> Void,
        onError: @escaping (NSError) -> Void
    ) {
        guard let p2PPayload = createP2PPayload(request: request) else {
            let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            onError(error)
            return
        }
        let cvv = createCVVPayload(informedCvv: informedCvv, manager: manager)
        let isFeedzai = FeatureManager.shared.isActive(.transaction_p2p_v2_feedzai)
        let payload = PaymentPayload<P2PPayload>(cvv: cvv, generic: p2PPayload)
        service.createTransaction(
            password: request.pin ?? "",
            isFeedzai: isFeedzai,
            payload: payload,
            isNewArchitecture: false
        ) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let value):
                    let result = PPP2PTransaction()
                    result.wsId = value.p2pId
                    result.sendedValue = NSDecimalNumber(string: value.value)
                    result.receiptItems = value.receiptWidgets.compactMap { WSReceipt.createReceiptWidgetItem(jsonDict: $0) }
                    self?.saveCvv(informedCvv: informedCvv)
                    
                    onSuccess(result)
                case .failure(let error):
                    onError(error.picpayError)
                }
            }
        }
    }
    
    private func createP2PPayload(request: P2PTransactionRequest) -> P2PPayload? {
        guard
            let password = request.pin,
            let biometry = request.biometry?.boolValue,
            let payeeId = request.payeeId,
            let value = request.consumerValue?.doubleValue,
            let credit = request.credit?.doubleValue,
            let install = request.qntParcelas,
            let installments = Int(install),
            let cardId = request.ccId,
            let someErrorOccurred = request.someErrorOccurred?.boolValue,
            let privacyConfig = request.paymentPrivacyConfig
            else {
                return nil
        }
        return P2PPayload(
            password: password,
            biometry: biometry,
            payeeId: payeeId,
            value: value,
            credit: credit,
            installments: installments,
            cardId: cardId,
            someErrorOccurred: someErrorOccurred,
            privacyConfig: privacyConfig,
            message: request.message,
            ignoreBalance: !ConsumerManager.useBalance(),
            extra: request.extraParam,
            surcharge: request.surcharge?.doubleValue,
            payeePhone: request.payeeId
        )
    }
    
    private func createCVVPayload(informedCvv: String?, manager: PPPaymentManager) -> CVVPayload? {
        guard let cvv = informedCvv else {
            return createLocalStoreCVVPayload(manager: manager)
        }
       
        return CVVPayload(value: cvv)
    }
    
    private func createLocalStoreCVVPayload(manager: PPPaymentManager) -> CVVPayload? {
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        guard
            manager.cardTotal()?.doubleValue != .zero,
            let cvv = dependencies.keychain.getData(key: KeychainKeyPF.cvv(id)) else {
                return nil
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func saveCvv(informedCvv: String?) {
        guard let cvv = informedCvv else {
            return
        }
        
        let id = String(CreditCardManager.shared.defaultCreditCardId)
        dependencies.keychain.set(key: KeychainKeyPF.cvv(id), value: cvv)
    }
    
    private func checkNeedCvv(manager: PPPaymentManager, request: P2PTransactionRequest) -> Bool {
        guard
            !isLimitExceededPay(request: request),
            let cardBank = CreditCardManager.shared.defaultCreditCard,
            let cardValue = manager.cardTotal()?.doubleValue else {
                return false
        }
        
        return CvvRegisterFlowCoordinator.cvvFlowEnable(cardBank: cardBank, cardValue: cardValue, paymentType: .p2p)
    }
    
    private func isLimitExceededPay(request: P2PTransactionRequest) -> Bool {
        request.surcharge != nil
    }
    
    private func createCvvFlowCoordinator(
        navigationController: UINavigationController?,
        completedCvv: @escaping (String) -> Void,
        completedNoCvv: @escaping () -> Void
    ) {
        guard let navigationController = navigationController else {
            return
        }
        
        let coordinator = CvvRegisterFlowCoordinator(
            navigationController: navigationController,
            paymentType: .p2p,
            finishedCvv: completedCvv,
            finishedWithoutCvv: completedNoCvv
        )
        coordinator.start()
        
        self.coordinator = coordinator
    }
}
