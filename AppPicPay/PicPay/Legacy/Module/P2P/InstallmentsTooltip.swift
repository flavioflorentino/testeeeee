import AMPopTip
import Core
import Foundation
import UI

final class InstallmentsTooltip: NSObject {
    @objc var blockTooltip: Bool = false
    private var clickScreen: Bool = false
    private var tooltipDismiss: Bool = false
    
    private lazy var popTip: PopTip = {
        let popTip = PopTip()
        popTip.bubbleColor = Palette.tooltip.color
        popTip.shouldDismissOnTap = false
        popTip.shouldDismissOnTapOutside = false
        popTip.offset = 4
        popTip.edgeInsets = UIEdgeInsets(top: 6, left: 8, bottom: 6, right: 8)
        popTip.cornerRadius = 4
        popTip.arrowSize = CGSize(width: 16, height: 16)
        popTip.textColor = Palette.ppColorGrayscale000.color(withCustomDark: .ppColorGrayscale000)
        popTip.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        popTip.edgeMargin = 8
        return popTip
    }()
    
    @objc
    init(view: UIView, toolbarView: UIView?) {
        super.init()
        
        let tapViewGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapView))
        let tapToolbarGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapToolbar))
        
        tapViewGesture.cancelsTouchesInView = false
        tapToolbarGesture.cancelsTouchesInView = false
        
        toolbarView?.addGestureRecognizer(tapViewGesture)
        view.addGestureRecognizer(tapToolbarGesture)
        
        tapViewGesture.delegate = self
        tapToolbarGesture.delegate = self
    }
    
    @objc
    class func timesTooltipDisplay() -> Int {
        let num = KVStore().intFor(.installmentsPopupShow)
        return num
    }
    
    @objc
    func show(view: UIView, from: UIView, cardValue: NSDecimalNumber) {
        displayPopupIfRuleIsValid(view: view, from: from.frame, cardValue: cardValue)
    }

    func show(view: UIView, from: CGRect, cardValue: NSDecimalNumber) {
        displayPopupIfRuleIsValid(view: view, from: from, cardValue: cardValue)
    }
    
    private func displayPopupIfRuleIsValid (view: UIView, from: CGRect, cardValue: NSDecimalNumber) {
        clickScreen = false
        if cardValue.compare(NSNumber(value: 50)) != .orderedAscending && !blockTooltip {
            blockTooltip = true
            if isActivated() {
                let times = InstallmentsTooltip.timesTooltipDisplay()
                PPAnalytics.trackEvent("Installments hint bubble", properties: ["Shown order": times])
                configureShow(view: view, from: from)
            }
        }
    }
    
    private func configureShow(view: UIView, from: CGRect) {
        popTip.show(text: P2MLocalizable.paymentCanInstallments.text, direction: .down, maxWidth: 400, in: view, from: from)
        
        waitTime(time: 2) { [weak self] in
            guard let strongSelf = self else {
            return
        }
            
            if strongSelf.clickScreen {
                strongSelf.hidden()
            }
            strongSelf.tooltipDismiss = true
        }
        
        waitTime(time: 5) { [weak self] in
            self?.hidden()
        }
    }
    
    private func isActivated() -> Bool {
        let num = InstallmentsTooltip.timesTooltipDisplay()
        if num < 5 {
            KVStore().setInt(num + 1, with: .installmentsPopupShow)
            return true
        }
        return  false
    }
    
    private func hidden() {
        popTip.hide()
    }
    
    private func waitTime(time: TimeInterval, handler: @escaping(() -> Void)) {
        DispatchQueue.main.asyncAfter(deadline: .now() + time) {
            handler()
        }
    }
    
    @objc
    private func handleTapView(recognizer: UITapGestureRecognizer) {
        handleTap()
    }
    
    @objc
    private func handleTapToolbar(recognizer: UITapGestureRecognizer) {
       handleTap()
    }
    
    private func handleTap() {
        clickScreen = true
        if tooltipDismiss {
            hidden()
        }
    }
}

extension InstallmentsTooltip: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
