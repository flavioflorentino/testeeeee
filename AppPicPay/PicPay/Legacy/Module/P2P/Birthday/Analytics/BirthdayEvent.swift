import AnalyticsModule
import Foundation

enum BirthdayEvent: AnalyticsKeyProtocol {
    case openBirthday(message: String, userId: Int, cta: String)
    case paymentBirthday(message: String, userId: Int, value: Double, privacy: PrivacyType, cta: String)
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .openBirthday(let message, let userId, let cta):
            return AnalyticsEvent("Abriu Aniversariante do Dia", properties: ["available_messages": message, "birthday_user": userId, "cta": cta], providers: [.mixPanel, .appsFlyer, .firebase])
            
        case .paymentBirthday(let message, let userId, let value, let privacy, let cta):
            return AnalyticsEvent("Presenteou Aniversariante do Dia", properties: ["message": message, "birthday_user": userId, "gift": value, "privacy": privacy.description, "cta": cta], providers: [.mixPanel, .appsFlyer, .firebase])
        }
    }
}

extension BirthdayEvent {
    enum PrivacyType: String, CustomStringConvertible {
        case `private`
        case friends
        
        var description: String {
            return self.rawValue
        }
    }
}
