import Foundation
import FeatureFlag

final class BirthdayGiftFactory {
    static func make(with: PPContact) -> BirthdayGiftViewController {
        let paymentService: BirthdayPaymentServicing = FeatureManager.isActive(.pciBirthday) ? LegacyBirthdayPaymentPciService() : LegacyBirthdayPaymentService()
        let service: BirthdayGiftServiceServicing = BirthdayGiftService(
            consumerManager: ConsumerManager.shared,
            cardManager: CreditCardManager.shared,
            paymentManager: PPPaymentManager())
        
        let viewModel: BirthdayGiftViewModelType = BirthdayGiftViewModel(service: service, paymentService: paymentService, model: with)
        var coordinator: BirthdayGiftCoordinating = BirthdayGiftCoordinator()
        let viewController = BirthdayGiftViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
