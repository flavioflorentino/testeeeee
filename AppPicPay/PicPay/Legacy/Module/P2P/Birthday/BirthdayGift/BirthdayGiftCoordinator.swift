import UI
import UIKit

enum BirthdayGiftAction {
    case paymentSuccess(ReceiptWidgetViewModel)
    case changePaymentMethods
    case openCvv(completedCvv: (String) -> Void, completedNoCvv: () -> Void)
    case close
}

protocol BirthdayGiftCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: BirthdayGiftAction)
}

final class BirthdayGiftCoordinator: BirthdayGiftCoordinating {
    private var coordinator: Coordinating?
    weak var viewController: UIViewController?
    
    func perform(action: BirthdayGiftAction) {
        switch action {
        case .paymentSuccess(let viewModel):
            guard let viewController = viewController else {
                return
            }
            TransactionReceipt.showReceiptSuccess(viewController: viewController, receiptViewModel: viewModel)
            
        case .changePaymentMethods:
            guard let controller = ViewsManager.paymentMethodsStoryboardViewController(withIdentifier: "PaymentMethodsViewController") as? PaymentMethodsViewController else {
                return
            }
            viewController?.navigationController?.pushViewController(controller, animated: true)
            
        case .openCvv(let completedCvv, let completedNoCvv):
            guard let navigation = viewController?.navigationController else {
                return
            }
            
            let coordinator = CvvRegisterFlowCoordinator(
                navigationController: navigation,
                paymentType: .birthday,
                finishedCvv: completedCvv,
                finishedWithoutCvv: completedNoCvv
            )
            coordinator.start()
        
            self.coordinator = coordinator
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
