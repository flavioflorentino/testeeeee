import UI
import UIKit

final class BirthdayGiftViewController: LegacyViewController<BirthdayGiftViewModelType, BirthdayGiftCoordinating, UIView> {
    private enum Layout {
        static let heightButton: CGFloat = 48.0
        static let widthButton: CGFloat = 220.0
    }
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var giftContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var profileView: ProfileUserView = {
        let profile = ProfileUserView()
        profile.translatesAutoresizingMaskIntoConstraints = false
        
        return profile
    }()
    
    private lazy var selectGiftView: SelectGiftView = {
        let gift = SelectGiftView()
        gift.delegate = self
        gift.translatesAutoresizingMaskIntoConstraints = false
        
        return gift
    }()
    
    private lazy var footer: BirthdayFooterView = {
        let footer = BirthdayFooterView()
        footer.delegate = self
        footer.translatesAutoresizingMaskIntoConstraints = false
        
        return footer
    }()

    private lazy var buttonPay: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = Layout.heightButton / 2
        button.addTarget(self, action: #selector(tapPay), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "icoCash"), for: .normal)
        button.adjustsImageWhenHighlighted = false
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    private var auth: PPAuth?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.inputs.viewDidLoad()
    }
 
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.inputs.viewDidAppear()
    }
    
    override func buildViewHierarchy() {
        giftContainerView.addSubview(profileView)
        giftContainerView.addSubview(selectGiftView)
        giftContainerView.addSubview(buttonPay)
        containerView.addSubview(giftContainerView)
        
        view.addSubview(containerView)
        view.addSubview(footer)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        title = BirthdayLocalizable.title.text
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.btClose.text, style: .plain, target: self, action: #selector(didTapClose))
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: view.topAnchor),
            containerView.bottomAnchor.constraint(equalTo: footer.topAnchor)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [containerView, footer])
        NSLayoutConstraint.leadingTrailing(equalTo: giftContainerView, for: [profileView, selectGiftView])
        NSLayoutConstraint.leadingTrailing(equalTo: containerView, for: [giftContainerView], constant: 16)
        
        NSLayoutConstraint.activate([
            giftContainerView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            profileView.topAnchor.constraint(equalTo: giftContainerView.topAnchor, constant: 16),
            selectGiftView.topAnchor.constraint(equalTo: profileView.bottomAnchor, constant: 15)
        ])
        
        NSLayoutConstraint.activate([
            buttonPay.topAnchor.constraint(equalTo: selectGiftView.bottomAnchor, constant: 20),
            buttonPay.bottomAnchor.constraint(equalTo: giftContainerView.bottomAnchor),
            buttonPay.centerXAnchor.constraint(equalTo: giftContainerView.centerXAnchor),
            buttonPay.heightAnchor.constraint(equalToConstant: Layout.heightButton),
            buttonPay.widthAnchor.constraint(equalToConstant: Layout.widthButton)
        ])
        
        NSLayoutConstraint.activate([
            footer.bottomAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.bottomAnchor),
            footer.heightAnchor.constraint(equalToConstant: 56)
        ])
    }
    
    @objc
    private func tapPay() {
        auth = PPAuth.authenticate({ [weak self] password, biometry in
            guard let password = password else {
                return
            }
            
            self?.startLoad()
            self?.viewModel.inputs.makePayment(password: password, isBiometry: biometry)
        }, canceledByUserBlock: nil)
    }
    
    private func startLoad() {
        buttonPay.startLoadingAnimating()
        footer.isUserInteractionEnabled = false
        selectGiftView.isUserInteractionEnabled = false
    }
    
    private func stopLoad() {
        buttonPay.stopLoadingAnimating()
        footer.isUserInteractionEnabled = true
        selectGiftView.isUserInteractionEnabled = true
    }
    
    @objc
    private func didTapClose() {
        viewModel.inputs.close()
    }
}

// MARK: View Model Outputs
extension BirthdayGiftViewController: BirthdayGiftViewModelOutputs {
    func showAdyenWarning() {
        AdyenWarning().show(in: self) { [weak self] in
            self?.viewModel.inputs.continueOn3DSTransaction()
        }
    }
    
    func didNextStep(action: BirthdayGiftAction) {
        coordinator.perform(action: action)
    }
    
    func didReceiveAnError(error: PicPayError) {
        stopLoad()
        AlertMessage.showCustomAlertWithErrorUI(error, controller: self)
    }
    
    func buttonTitle(title: String) {
        let button = Button(title: title, type: .cta)
        buttonPay.configure(with: button)
    }
    
    func updatePrivacy(title: String, image: UIImage) {
        footer.configurePrivacy(title: title, image: image)
    }
    
    func updatePaymentMethod(image: UIImage) {
        footer.configurePayment(image: image)
    }
    
    func updateProfile(username: String, image: String) {
        profileView.configureView(image: image, username: username)
    }
    
    func selectGift(gifts: [Gift]) {
        selectGiftView.configure(with: gifts)
    }
    
    func askPrivacy(title: String, message: String, publicButton: String, privateButton: String, cancelAction: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        let publicAction = UIAlertAction(title: publicButton, style: .default) { [weak self] _ in
            self?.viewModel.inputs.changePaymentPrivacy(type: .friends)
        }
        
        let privateAction = UIAlertAction(title: privateButton, style: .default) { [weak self] _ in
            self?.viewModel.inputs.changePaymentPrivacy(type: .private)
        }
        
        let cancelAction = UIAlertAction(title: cancelAction, style: .cancel)
        
        alert.addAction(publicAction)
        alert.addAction(privateAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
    }
    
    func displayLimitExceeded(model: LimitExceededError) {
        stopLoad()
        let popup = LimitExceededPopup(error: model)
        popup.view.backgroundColor = Palette.black.color.withAlphaComponent(0.6)
        popup.modalPresentationStyle = .overFullScreen
        
        popup.didClose = { [weak self] in
            self?.resignFirstResponder()
        }
        
        popup.buttonTapAction = { [weak self] in
            popup.dismiss(animated: false) {
                self?.startLoad()
                self?.viewModel.inputs.limitExceededPay(surcharge: model.surcharge.doubleValue)
            }
        }
        
        popup.waitButtonTapAction = { [weak self] limitError in
            self?.viewModel.inputs.limitExceededSendNotification(error: limitError)
            popup.dismiss(animated: false)
        }
        
        present(popup, animated: false)
    }
}

extension BirthdayGiftViewController: BirthdayFooterViewDelegate {
    func didTapPrivacy() {
        viewModel.inputs.didTapPrivacy()
    }
    
    func didTapPaymentMethods() {
        viewModel.inputs.didTapPaymentMethods()
    }
}

extension BirthdayGiftViewController: SelectGiftViewDelegate {
    func giftSelected(index: Int) {
        viewModel.inputs.giftSelected(index: index)
    }
}

extension BirthdayGiftViewController: ADYChallengeDelegate {
    func challengeDidFinish(with result: ADYChallengeResult) {
        viewModel.inputs.challengeFinishSuccess()
    }
    
    func challengeDidFailWithError(_ error: Error) {
        stopLoad()
        viewModel.inputs.challengeFinishError(error: error)
    }
}
