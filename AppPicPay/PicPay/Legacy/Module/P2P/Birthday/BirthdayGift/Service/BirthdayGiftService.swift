import Core
import FeatureFlag
import Foundation

final class BirthdayGiftService: BirthdayGiftServiceServicing {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    private let consumerManager: ConsumerManager
    private let cardManager: CreditCardManager
    private let paymentManager: PPPaymentManager
    
    var privacyConfig: FeedItemVisibilityInt {
        let privacy = consumerManager.privacyConfig
        return FeedItemVisibilityInt(rawValue: privacy) ?? FeedItemVisibilityInt.private
    }
    
    var subtotal: NSDecimalNumber {
        return paymentManager.subtotal
    }
    
    var defauldCardId: String {
        let card = cardManager.defaultCreditCard
        return card?.id ?? "0"
    }
    
    var defauldCard: CardBank? {
        return cardManager.defaultCreditCard
    }
    
    var balance: NSDecimalNumber {
        return paymentManager.balance()
    }
    
    var usePicPayBalance: Bool {
        return paymentManager.usePicPayBalance()
    }
    
    var cardTotal: NSDecimalNumber {
        return paymentManager.cardTotal()
    }
    
    var balanceTotal: NSDecimalNumber {
        return paymentManager.balanceTotal()
    }
    
    init(consumerManager: ConsumerManager, cardManager: CreditCardManager, paymentManager: PPPaymentManager) {
        self.consumerManager = consumerManager
        self.cardManager = cardManager
        self.paymentManager = paymentManager
    }
    
    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool {
        return CvvRegisterFlowCoordinator.cvvFlowEnable(cardBank: cardBank, cardValue: cardValue, paymentType: .birthday)
    }
    
    func saveCvvCard(id: String, value: String?) {
        dependencies.keychain.set(key: KeychainKeyPF.cvv(id), value: value)
    }
    
    func cvvCard(id: String) -> String? {
        dependencies.keychain.getData(key: KeychainKeyPF.cvv(id))
    }
    
    func buttonTitle() -> String {
        FeatureManager.text(.buttonGift)
    }
    
    func textGift(type: Gift.GiftType) -> String {
        switch type {
        case .iceCream:
            return FeatureManager.text(.firstGift)
            
        case .beer:
            return FeatureManager.text(.secondGift)
            
        case .cake:
            return FeatureManager.text(.thirdGift)
        }
    }
    
    func configurePayment(value: Double, installment: Int) {
        paymentManager.subtotal = NSDecimalNumber(value: value)
        paymentManager.selectedQuotaQuantity = installment
    }
    
    /// In the old PRO the call was like this ¯\_(ツ)_/¯
    func sendNotificationPicPayPro(payload: [String: Any]) {
        DispatchQueue.global().async {
            WSConsumer.sendNotificationPicPayPro(with: payload) { _, _ in }
        }
    }
}
