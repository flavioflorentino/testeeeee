import Core
import FeatureFlag
import Foundation
import PCI

final class LegacyBirthdayPaymentPciService: BirthdayPaymentServicing {
    private let pci: P2PServiceProtocol
    
    init(pci: P2PServiceProtocol = P2PService()) {
        self.pci = pci
    }
    
    func makePayment(request: P2PRequest, cvv: String?, completion: @escaping(Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        guard let birthdayPayload = createBirthdayPayload(request: request) else {
            let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            completion(.failure(error))
            return
        }
        let cvv = createCVVPayload(cvv: cvv)
        let isFeedzai = FeatureManager.shared.isActive(.transaction_p2p_v2_feedzai)
        let payload = PaymentPayload<P2PPayload>(cvv: cvv, generic: birthdayPayload)
        
        pci.createTransaction(
            password: request.password,
            isFeedzai: isFeedzai,
            payload: payload,
            isNewArchitecture: false
        ) { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let value):
                    let receiptModel = ReceiptWidgetViewModel(transactionId: value.p2pId, type: .P2P)
                    let receipt = value.receiptWidgets.compactMap { WSReceipt.createReceiptWidgetItem(jsonDict: $0) }
                    receiptModel.setReceiptWidgets(items: receipt)

                    completion(.success(receiptModel))

                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    private func createBirthdayPayload(request: P2PRequest) -> P2PPayload? {
        guard
            let value = Double(request.consumerValue),
            let credit = Double(request.credit),
            let installment = Int(request.installments)
            else {
                return nil
        }
        
        return P2PPayload(
            password: request.password,
            biometry: request.isBiometry.boolValue,
            payeeId: request.payeeId,
            value: value,
            credit: credit,
            installments: installment,
            cardId: request.cardId,
            someErrorOccurred: request.someErrorOccurred.boolValue,
            privacyConfig: request.privacyConfig,
            message: request.message,
            surcharge: request.surcharge
        )
    }

    private func createCVVPayload(cvv: String?) -> CVVPayload? {
        guard let cvv = cvv else {
            return nil
        }
        
        return CVVPayload(value: cvv)
    }
}
