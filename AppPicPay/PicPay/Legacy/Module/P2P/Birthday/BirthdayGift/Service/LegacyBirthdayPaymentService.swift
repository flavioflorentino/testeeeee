import Foundation

final class LegacyBirthdayPaymentService: BirthdayPaymentServicing {
    func makePayment(request: P2PRequest, cvv: String?, completion: @escaping(Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let transactionRequest = P2PTransactionRequest()

        transactionRequest.consumerValue = NSDecimalNumber(string: request.consumerValue)
        transactionRequest.pin = request.password
        transactionRequest.credit = NSDecimalNumber(string: request.credit)
        transactionRequest.qntParcelas = request.installments
        transactionRequest.ccId = request.cardId
        transactionRequest.surcharge = request.surcharge as NSNumber?
        transactionRequest.message = request.message
        transactionRequest.biometry = Int(request.isBiometry) as NSNumber?
        transactionRequest.payeeId = request.payeeId
        transactionRequest.paymentPrivacyConfig = request.privacyConfig
        transactionRequest.someErrorOccurred = Int(request.someErrorOccurred) as NSNumber?

        WSTransaction.createP2PTransaction(transactionRequest) { [weak self] transaction, error in
            DispatchQueue.main.async {
                guard let transaction = transaction else {
                    self?.makePaymentError(error: error, completion: completion)
                    return
                }
                
                let receiptModel = ReceiptWidgetViewModel(transactionId: transaction.wsId, type: .P2P)
                if let receiptItems = transaction.receiptItems as? [ReceiptWidgetItem] {
                    receiptModel.setReceiptWidgets(items: receiptItems)
                }

                completion(.success(receiptModel))
            }
        }
    }
    
    private func makePaymentError(error: Error?, completion: @escaping(Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        guard let error = error as NSError?, let picpayError = error as? PicPayError else {
            let picpayError = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            completion(.failure(picpayError))
            return
        }
        
        completion(.failure(picpayError))
    }
}
