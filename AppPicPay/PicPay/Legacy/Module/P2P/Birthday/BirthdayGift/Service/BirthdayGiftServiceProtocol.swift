import Foundation

protocol BirthdayGiftServiceServicing {
    var privacyConfig: FeedItemVisibilityInt { get }
    var subtotal: NSDecimalNumber { get }
    var defauldCardId: String { get }
    var defauldCard: CardBank? { get }
    var balance: NSDecimalNumber { get }
    var usePicPayBalance: Bool { get }
    var cardTotal: NSDecimalNumber { get }
    var balanceTotal: NSDecimalNumber { get }
    
    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool
    func cvvCard(id: String) -> String?
    func saveCvvCard(id: String, value: String?)
    func buttonTitle() -> String
    func textGift(type: Gift.GiftType) -> String
    func configurePayment(value: Double, installment: Int)
    func sendNotificationPicPayPro(payload: [String: Any])
}

protocol BirthdayPaymentServicing {
    func makePayment(request: P2PRequest, cvv: String?, completion: @escaping(Result<ReceiptWidgetViewModel, PicPayError>) -> Void)
}
