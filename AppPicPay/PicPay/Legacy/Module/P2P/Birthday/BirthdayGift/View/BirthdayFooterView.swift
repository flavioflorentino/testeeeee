import Foundation
import UI

protocol BirthdayFooterViewDelegate: AnyObject {
    func didTapPrivacy()
    func didTapPaymentMethods()
}

class BirthdayFooterView: UIView {
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.spacing = -0.5
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    private lazy var buttonPrivacy: UIButton = {
        let button = UIButton()
        button.layer.borderWidth = 0.8
        button.layer.borderColor = Palette.ppColorGrayscale300.cgColor
        button.addTarget(self, action: #selector(tapPrivacy), for: .touchUpInside)
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.adjustsImageWhenHighlighted = false
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 0.0)
        button.imageView?.contentMode = .scaleAspectFill
        
        return button
    }()
    
    private lazy var buttonPayment: UIButton = {
        let button = UIButton()
        button.layer.borderWidth = 0.8
        button.layer.borderColor = Palette.ppColorGrayscale300.cgColor
        button.addTarget(self, action: #selector(tapPayment), for: .touchUpInside)
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.adjustsImageWhenHighlighted = false
        
        return button
    }()
    
    open weak var delegate: BirthdayFooterViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    private func addComponents() {
        stackView.addArrangedSubview(buttonPrivacy)
        stackView.addArrangedSubview(buttonPayment)
        addSubview(stackView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.constraintAllEdges(from: stackView, to: self)
    }
    
    @objc
    private func tapPrivacy() {
        delegate?.didTapPrivacy()
    }
    
    @objc
    private func tapPayment() {
        delegate?.didTapPaymentMethods()
    }
    
    open func configurePrivacy(title: String, image: UIImage) {
        buttonPrivacy.setTitle(title, for: .normal)
        buttonPrivacy.setImage(image, for: .normal)
    }
    
    open func configurePayment(image: UIImage) {
        buttonPayment.setImage(image, for: .normal)
    }
}
