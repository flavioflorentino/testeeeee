import Foundation
import UI

protocol SelectGiftViewDelegate: AnyObject {
    func giftSelected(index: Int)
}

class SelectGiftView: UIView {
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.alignment = .center
        stackView.axis = .horizontal
        stackView.spacing = 5.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        label.numberOfLines = 2
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var viewModel: SelectGiftViewModel = {
        let viewModel = SelectGiftViewModel()
        viewModel.outputs = self
        
        return viewModel
    }()
    
    open weak var delegate: SelectGiftViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .clear
    }
    
    private func addComponents() {
        addSubview(stackView)
        addSubview(descLabel)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            descLabel.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 10),
            descLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            descLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            descLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            descLabel.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    open func configure(with model: [Gift]) {
        viewModel.inputs.viewConfigure(model: model)
    }
}

extension SelectGiftView: SelectGiftViewModelOutputs {
    func addRow(image: String, value: String, index: Int) {
        let gift = GiftView()
        gift.configureView(image: image, value: value, index: index)
        gift.delegate = self
        
        stackView.addArrangedSubview(gift)
    }
    
    func giftChecked(index: Int, isCheck: Bool) {
        guard stackView.arrangedSubviews.indices.contains(index),
            let view = stackView.arrangedSubviews[index] as? GiftView else {
            return
        }
        
        view.isCheck = isCheck
    }
    
    func giftSelected(index: Int, description: String) {
        delegate?.giftSelected(index: index)
        descLabel.text = description
    }
}

extension SelectGiftView: GiftViewDelegate {
    func didTap(index: Int) {
        viewModel.inputs.didTapGift(index: index)
    }
}
