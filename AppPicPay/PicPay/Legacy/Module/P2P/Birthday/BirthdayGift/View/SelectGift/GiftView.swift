import Foundation
import UI

protocol GiftViewDelegate: AnyObject {
    func didTap(index: Int)
}

final class GiftView: UIView {
    private var index = 0
    private let sizeImage: CGFloat = 14.0
    
    private let checkImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.isHidden = true
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    private lazy var imageLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 35)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    weak var delegate: GiftViewDelegate?
    
    var isCheck: Bool = false {
        didSet {
            checkImage.isHidden = !isCheck
            backgroundColor = isCheck ? Palette.ppColorGrayscale200.color : Palette.ppColorGrayscale000.color
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .clear
        checkImage.image = #imageLiteral(resourceName: "check14Px")
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTap))
        addGestureRecognizer(tap)
    }
    
    private func addComponents() {
        addSubview(checkImage)
        addSubview(imageLabel)
        addSubview(descLabel)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            checkImage.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            checkImage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5),
            checkImage.leadingAnchor.constraint(equalTo: imageLabel.trailingAnchor, constant: 2),
            checkImage.widthAnchor.constraint(equalToConstant: sizeImage),
            checkImage.heightAnchor.constraint(equalToConstant: sizeImage)
        ])
        
        NSLayoutConstraint.activate([
            imageLabel.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            imageLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            descLabel.topAnchor.constraint(equalTo: imageLabel.bottomAnchor, constant: 12.0),
            descLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -6.0),
            descLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 2.0),
            descLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -2.0)
        ])
    }
    
    @objc
    private func didTap() {
        delegate?.didTap(index: index)
    }
    
    func configureView(image: String, value: String, index: Int) {
        imageLabel.text = image
        descLabel.text = value
        self.index = index
    }
}
