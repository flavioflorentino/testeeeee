import CoreLegacy
import Foundation

protocol SelectGiftViewModelInputs {
    func viewConfigure(model: [Gift])
    func didTapGift(index: Int)
}

protocol SelectGiftViewModelOutputs: AnyObject {
    func addRow(image: String, value: String, index: Int)
    func giftChecked(index: Int, isCheck: Bool)
    func giftSelected(index: Int, description: String)
}

protocol SelectGiftViewModelType: AnyObject {
    var inputs: SelectGiftViewModelInputs { get }
    var outputs: SelectGiftViewModelOutputs? { get set }
}

final class SelectGiftViewModel: SelectGiftViewModelType, SelectGiftViewModelInputs {
    var inputs: SelectGiftViewModelInputs { return self }
    weak var outputs: SelectGiftViewModelOutputs?
    
    private var model: [Gift] = []
    private var giftSelect = 0
    
    func viewConfigure(model: [Gift]) {
        self.model = model
     
        for (index, element) in model.enumerated() {
            let value = CurrencyFormatter.brazillianRealString(from: NSNumber(value: element.value)) ?? ""
            outputs?.addRow(image: element.type.image, value: value, index: index)
        }
        
        updateGift()
    }
    
    func didTapGift(index: Int) {
        outputs?.giftChecked(index: giftSelect, isCheck: false)
        giftSelect = index
        updateGift()
    }
    
    private func updateGift() {
        guard model.indices.contains(giftSelect) else {
            return
        }
        
        outputs?.giftSelected(index: giftSelect, description: model[giftSelect].description)
        outputs?.giftChecked(index: giftSelect, isCheck: true)
    }
}
