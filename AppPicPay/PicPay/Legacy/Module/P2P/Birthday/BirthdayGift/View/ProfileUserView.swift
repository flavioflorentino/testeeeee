import Foundation
import UI

final class ProfileUserView: UIView {
    private let sizeImage: CGFloat = 100.0
    
    private lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = sizeImage / 2
        image.clipsToBounds = true
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .clear
        descLabel.text = BirthdayLocalizable.chooseGift.text
    }
    
    private func addComponents() {
        addSubview(imageView)
        addSubview(descLabel)
        addSubview(usernameLabel)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: 8.0),
            imageView.widthAnchor.constraint(equalToConstant: sizeImage),
            imageView.heightAnchor.constraint(equalToConstant: sizeImage),
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: self, for: [descLabel, usernameLabel])
        
        NSLayoutConstraint.activate([
            descLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 8.0),
            usernameLabel.topAnchor.constraint(equalTo: descLabel.bottomAnchor, constant: 2.0),
            usernameLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    func configureView(image: String, username: String) {
        usernameLabel.text = username
        imageView.setImage(url: URL(string: image), placeholder: #imageLiteral(resourceName: "avatar_person"))
    }
}
