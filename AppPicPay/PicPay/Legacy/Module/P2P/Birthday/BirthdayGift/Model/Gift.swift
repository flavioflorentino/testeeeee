import Foundation

struct Gift {
    let type: GiftType
    let value: Double
    let description: String
}

extension Gift {
    enum GiftType {
        case iceCream
        case beer
        case cake
        
        var image: String {
            switch self {
            case .iceCream:
                return "🍦"
            case .beer:
                return "🍺"
            case .cake:
                return "🍰"
            }
        }
    }
}
