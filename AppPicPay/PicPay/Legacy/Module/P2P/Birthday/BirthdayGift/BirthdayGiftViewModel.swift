import Core
import Foundation
import AnalyticsModule

protocol BirthdayGiftViewModelInputs {
    func viewDidLoad()
    func viewDidAppear()
    func didTapPaymentMethods()
    func didTapPrivacy()
    func giftSelected(index: Int)
    func makePayment(password: String, isBiometry: Bool)
    func changePaymentPrivacy(type: FeedItemVisibilityInt)
    func continueOn3DSTransaction()
    func challengeFinishSuccess()
    func challengeFinishError(error: Error)
    func limitExceededPay(surcharge: Double)
    func limitExceededSendNotification(error: LimitExceededError)
    func close()
}

protocol BirthdayGiftViewModelOutputs: AnyObject, ADYChallengeDelegate {
    func didNextStep(action: BirthdayGiftAction)
    func didReceiveAnError(error: PicPayError)
    func buttonTitle(title: String)
    func askPrivacy(title: String, message: String, publicButton: String, privateButton: String, cancelAction: String)
    func updateProfile(username: String, image: String)
    func updatePrivacy(title: String, image: UIImage)
    func updatePaymentMethod(image: UIImage)
    func selectGift(gifts: [Gift])
    func displayLimitExceeded(model: LimitExceededError)
    func showAdyenWarning()
}

protocol BirthdayGiftViewModelType: AnyObject {
    var inputs: BirthdayGiftViewModelInputs { get }
    var outputs: BirthdayGiftViewModelOutputs? { get set }
}

final class BirthdayGiftViewModel: BirthdayGiftViewModelType, BirthdayGiftViewModelInputs {
    var inputs: BirthdayGiftViewModelInputs { return self }
    weak var outputs: BirthdayGiftViewModelOutputs?
    
    private let service: BirthdayGiftServiceServicing
    private let paymentService: BirthdayPaymentServicing
    
    private var someErrorOccurred: Bool
    private var paymentPrivacy: FeedItemVisibilityInt
    private let model: PPContact
    private var gifts: [Gift]
    private var giftSelected: Gift?
    
    private var password: String
    private var isBiometry: Bool
    private var insertedCvv: String?
    
    //Mark - Adyen Stuff
    private let adyen: AdyenServicing
    private var transaction: ADYTransaction?
    private var adyenParameters: [String: Any]
    private var adyenPayload: [String: String]
    
    init(service: BirthdayGiftServiceServicing, paymentService: BirthdayPaymentServicing, model: PPContact, adyen: AdyenServicing = AdyenWorker()) {
        self.service = service
        self.paymentService = paymentService
        self.model = model
        self.adyen = adyen
        self.adyenParameters = [:]
        self.adyenPayload = [:]
        
        password = ""
        isBiometry = false
        gifts = []
        someErrorOccurred = false
        paymentPrivacy = .private
    }
    
    func viewDidLoad() {
        paymentPrivacy = service.privacyConfig
        profile()
        updateGifts()
        updatePrivacy()
        setButton()
        trackEventOpen()
    }
    
    func viewDidAppear() {
        updatePaymentMethods(value: giftSelected?.value ?? 0)
    }
    
    func changePaymentPrivacy(type: FeedItemVisibilityInt) {
        paymentPrivacy = type
        updatePrivacy()
    }
    
    func didTapPrivacy() {
        outputs?.askPrivacy(title: BirthdayLocalizable.selectPrivacyTitle.text,
                            message: BirthdayLocalizable.selectPrivacyDesc.text,
                            publicButton: BirthdayLocalizable.privacyPublic.text,
                            privateButton: BirthdayLocalizable.privacyPrivate.text,
                            cancelAction: DefaultLocalizable.btCancel.text)
    }
    
    func didTapPaymentMethods() {
        outputs?.didNextStep(action: .changePaymentMethods)
    }
    
    func close() {
        outputs?.didNextStep(action: .close)
    }
    
    func giftSelected(index: Int) {
        giftSelected = gifts[index]
        updatePaymentMethods(value: giftSelected?.value ?? 0)
    }
    
    func makePayment(password: String, isBiometry: Bool) {
        guard checkNeedCvv() else {
            createTransaction(password: password, isBiometry: isBiometry)
            return
        }
        
        outputs?.didNextStep(action: .openCvv(completedCvv: { [weak self] cvv in
            self?.insertedCvv = cvv
            self?.createTransaction(password: password, isBiometry: isBiometry, informedCvv: cvv)
        }, completedNoCvv: { [weak self] in
            let error = PicPayError(message: DefaultLocalizable.cvvNotInformed.text)
            self?.outputs?.didReceiveAnError(error: error)
        }))
    }
    
    func challengeFinishSuccess() {
        adyen.complete(payload: adyenParameters, succeeded: { [weak self] receipt in
            self?.outputs?.didNextStep(action: .paymentSuccess(receipt))
        }, error: { [weak self] error in
            self?.outputs?.didReceiveAnError(error: error)
        })
    }
    
    func challengeFinishError(error: Error) {
        guard Adyen3DS.challengeCancelled(error as NSError) else {
            let picpayError = PicPayError(message: error.localizedDescription)
            outputs?.didReceiveAnError(error: picpayError)
            return
        }
    }
    
    func continueOn3DSTransaction() {
        adyen.transaction(with: adyenPayload, hasChallenge: { [weak self] transaction, parameters, payload in
            guard let transaction = transaction as? ADYTransaction,
                let parameters = parameters as? ADYChallengeParameters,
                let controller = self?.outputs,
                let self = self else {
                    return
            }
            
            self.transaction = transaction
            self.adyenParameters = payload
            
            self.transaction?.performChallenge(with: parameters, delegate: controller)
            }, succeeded: { [weak self] receipt in
                self?.outputs?.didNextStep(action: .paymentSuccess(receipt))
            }, error: { [weak self] error in
                self?.outputs?.didReceiveAnError(error: error)
            }, injecting: ADYService.self)
    }
    
    func limitExceededPay(surcharge: Double) {
        createTransaction(password: password, isBiometry: isBiometry, surcharge: surcharge, informedCvv: insertedCvv)
        insertedCvv = nil
    }
    
    func limitExceededSendNotification(error: LimitExceededError) {
        service.sendNotificationPicPayPro(payload: error.payload)
    }
    
    private func createTransaction(password: String, isBiometry: Bool, surcharge: Double? = nil, informedCvv: String? = nil) {
        self.password = password
        self.isBiometry = isBiometry
        let request = createRequest(password: password, isBiometry: isBiometry, surcharge: surcharge)
        let cvv = createCvv(informedCvv: informedCvv)
        
        paymentService.makePayment(request: request, cvv: cvv) { [weak self] result in
            switch result {
            case .success(let value):
                self?.saveCvv(informedCvv: informedCvv)
                NotificationCenter.default.post(name: Notification.Name.Payment.new, object: nil, userInfo: ["type": "Aniversário"])
                self?.outputs?.didNextStep(action: .paymentSuccess(value))
                self?.trackEventPayment()
                
            case .failure(let error):
                self?.errorMakePayment(error: error)
            }
        }
    }
    
    private func errorMakePayment(error: PicPayError) {
        guard let payload = canCreate3DSPayload(error: error) else {
            someErrorOccurred = true
            handlerError(error: error)
            return
        }
        
        adyenPayload = payload
        create3DSTransaction()
    }
    
    private func handlerError(error: PicPayError) {
        switch PaymentErrorType(error) {
        case .limitExceeded:
            displayLimitExceeded(error: error)
        default:
            outputs?.didReceiveAnError(error: error)
        }
    }
    
    private func displayLimitExceeded(error: PicPayError) {
        guard let model = LimitExceededError(error: error) else {
            outputs?.didReceiveAnError(error: error)
            return
        }
        
        outputs?.displayLimitExceeded(model: model)
    }
    
    private func canCreate3DSPayload(error: PicPayError) -> [String: String]? {
        guard
            Adyen3DS.is3ds(error: error),
            let payload = Adyen3DS.adyenPayload(error: error)
            else {
                return nil
        }
        
        return payload
    }
    
    private func create3DSTransaction() {
        if adyen.firstAccess3DS {
            outputs?.showAdyenWarning()
        } else {
            continueOn3DSTransaction()
        }
    }
    
    private func setButton() {
        let title = service.buttonTitle()
        outputs?.buttonTitle(title: title)
    }
    
    private func createCvv(informedCvv: String?) -> String? {
        guard let cvv = informedCvv else {
            return createLocalStoreCVVPayload()
        }
        
        return cvv
    }
    
    private func createLocalStoreCVVPayload() -> String? {
        let id = service.defauldCardId
        guard
            service.cardTotal.doubleValue != .zero,
            let cvv = service.cvvCard(id: id)
            else {
                return nil
        }
        
        return cvv
    }
    
    private func createRequest(password: String, isBiometry: Bool, surcharge: Double?) -> P2PRequest {
        let request = P2PRequest(
            payeeId: String(model.wsId),
            consumerValue: String(describing: service.subtotal),
            surcharge: surcharge,
            isBiometry: isBiometry ? "1" : "0",
            password: password,
            credit: service.balanceTotal.stringValue,
            installments: "1",
            cardId: service.defauldCardId,
            privacyConfig: String(paymentPrivacy.rawValue),
            message: giftSelected?.description,
            someErrorOccurred: someErrorOccurred ? "1" : "0")
        
        return request
    }
    
    private func updateGifts() {
        let iceCream = Gift(type: .iceCream, value: 2.00, description: service.textGift(type: .iceCream))
        let beer = Gift(type: .beer, value: 5.00, description: service.textGift(type: .beer))
        let cake = Gift(type: .cake, value: 15.00, description: service.textGift(type: .cake))
        
        gifts = [iceCream, beer, cake]
        outputs?.selectGift(gifts: gifts)
    }
    
    private func profile() {
        let usermane = "@\(model.username ?? "")"
        let image = model.imgUrl ?? ""
        outputs?.updateProfile(username: usermane, image: image)
    }
    
    private func updatePrivacy() {
        switch paymentPrivacy {
        case .private:
            outputs?.updatePrivacy(title: DefaultLocalizable.privacyPrivate.text, image: #imageLiteral(resourceName: "icoPrivPrivado"))
            
        case .friends:
            outputs?.updatePrivacy(title: DefaultLocalizable.privacyPublic.text, image: #imageLiteral(resourceName: "icoPrivAmigos"))
        }
    }
    
    //  💩💩💩💩 Isso aqui está horrível, mas acho que só melhora com um refactor do PaymentManager 💩💩💩💩
    private func updatePaymentMethods(value: Double) {
        service.configurePayment(value: value, installment: 1)
        let canUseBalance = service.balance != .zero && service.usePicPayBalance
        
        switch (service.cardTotal, service.balanceTotal) {
        case (NSDecimalNumber.zero, NSDecimalNumber.zero) where canUseBalance:
            outputs?.updatePaymentMethod(image: #imageLiteral(resourceName: "payment_origin_icon_balance"))
            
        case let (card, balance) where card != .zero && balance != .zero:
            outputs?.updatePaymentMethod(image: #imageLiteral(resourceName: "payment_origin_icon_card_and_balance"))
            
        case let (card, NSDecimalNumber.zero) where card != .zero:
            outputs?.updatePaymentMethod(image: #imageLiteral(resourceName: "payment_origin_icon_card"))
            
        case let (NSDecimalNumber.zero, balance) where balance != .zero:
            outputs?.updatePaymentMethod(image: #imageLiteral(resourceName: "payment_origin_icon_balance"))
            
        default:
            outputs?.updatePaymentMethod(image: #imageLiteral(resourceName: "payment_origin_icon_card"))
        }
    }
    
    private func trackEventOpen() {
        let button = service.buttonTitle()
        let id = model.wsId
        let message = gifts.reduce("") { $0.description + "/" + $1.description }
        
        Analytics.shared.log(BirthdayEvent.openBirthday(message: message, userId: id, cta: button))
    }
    
    private func trackEventPayment() {
        let button = service.buttonTitle()
        let id = model.wsId
        let message = gifts.reduce("") { $0.description + "/" + $1.description }
        let value = giftSelected?.value ?? 0
        
        Analytics.shared.log(BirthdayEvent.paymentBirthday(message: message, userId: id, value: value, privacy: paymentPrivacy == .private ? .private : .friends, cta: button))
    }
    
    private func saveCvv(informedCvv: String?) {
        guard let cvv = informedCvv else {
            return
        }
        
        let id = service.defauldCardId
        service.saveCvvCard(id: id, value: cvv)
    }
    
    private func checkNeedCvv() -> Bool {
        guard let cardBank = service.defauldCard else {
            return false
        }
        
        let cardValue = service.cardTotal.doubleValue
        return service.pciCvvIsEnable(cardBank: cardBank, cardValue: cardValue)
    }
}
