import Foundation

enum BirthdayLocalizable: String, Localizable {
    case title
    case chooseGift
    case privacyPublic
    case privacyPrivate
    case selectPrivacyTitle
    case selectPrivacyDesc
    case askPrivacyTitle
    case askPrivacyDesc
    
    var file: LocalizableFile {
        return .birthday
    }
    
    var key: String {
        return self.rawValue
    }
}
