import Foundation

final class BirthdayGiftLoadFactory: NSObject {
    @objc
    static func make(userId: String) -> UIViewController {
        let service: BirthdayGiftLoadServicing = BirthdayGiftLoadService()
        let viewModel: BirthdayGiftLoadViewModelType = BirthdayGiftLoadViewModel(service: service, userId: userId)
        var coordinator: BirthdayGiftLoadCoordinating = BirthdayGiftLoadCoordinator()
        let viewController = BirthdayGiftLoadViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
