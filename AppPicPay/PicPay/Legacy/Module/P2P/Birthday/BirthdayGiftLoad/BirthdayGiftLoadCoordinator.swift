import UIKit

enum BirthdayGiftLoadAction {
    case openLegacyBirthday(PPContact)
    case openBirthday(PPContact)
    case close
}

protocol BirthdayGiftLoadCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: BirthdayGiftLoadAction)
}

final class BirthdayGiftLoadCoordinator: BirthdayGiftLoadCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: BirthdayGiftLoadAction) {
        switch action {
        case .openLegacyBirthday(let contact):
            let controller = BirthdayGiftFactory.make(with: contact)
            viewController?.navigationController?.pushViewController(controller, animated: false)
            
        case .openBirthday(let contact):
            let orchestrator = BirthdayPaymentOrchestrator(contact: contact)
            viewController?.navigationController?.pushViewController(orchestrator.paymentViewController, animated: false)
            
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
