import FeatureFlag
import Foundation

protocol BirthdayGiftLoadServicing {
    var newBirthdayPaymentEnable: Bool { get }
    func getConsumer(id: String, completion: @escaping(Result<PPContact, PicPayError>) -> Void)
}

final class BirthdayGiftLoadService: BirthdayGiftLoadServicing {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    var newBirthdayPaymentEnable: Bool {
        dependencies.featureManager.isActive(.newBirthday)
    }
    
    func getConsumer(id: String, completion: @escaping(Result<PPContact, PicPayError>) -> Void) {
        WSSocial.getConsumerProfile(id) { contact, _, _, error in
            DispatchQueue.main.async {
                guard let contact = contact else {
                    let picpayError = PicPayError(message: error?.localizedDescription ?? DefaultLocalizable.unexpectedError.text)
                    completion(.failure(picpayError))
                    
                    return
                }
                
                completion(.success(contact))
            }
        }
    }
}
