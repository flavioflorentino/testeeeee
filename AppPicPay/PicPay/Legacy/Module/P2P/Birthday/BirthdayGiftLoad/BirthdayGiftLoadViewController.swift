import UI
import UIKit

final class BirthdayGiftLoadViewController: LegacyViewController<BirthdayGiftLoadViewModelType, BirthdayGiftLoadCoordinating, UIView> {
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.hidesWhenStopped = true
        
        return activityIndicator
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.inputs.viewDidLoad()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(activityIndicator)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        title = BirthdayLocalizable.title.text
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.btClose.text, style: .plain, target: self, action: #selector(didTapClose))
        activityIndicator.startAnimating()
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
    
    @objc
    private func didTapClose() {
        viewModel.inputs.close()
    }
}

// MARK: View Model Outputs
extension BirthdayGiftLoadViewController: BirthdayGiftLoadViewModelOutputs {
    func didNextStep(action: BirthdayGiftLoadAction) {
        coordinator.perform(action: action)
    }
    
    func didReceiveAnError(error: PicPayErrorDisplayable) {
        let alert = Alert(with: error)
        AlertMessage.showAlert(alert, controller: self) { [weak self] _, _, _ in
            self?.viewModel.inputs.close()
        }
    }
}
