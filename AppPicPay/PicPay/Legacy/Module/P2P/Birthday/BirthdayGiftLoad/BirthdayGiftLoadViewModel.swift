import Foundation

protocol BirthdayGiftLoadViewModelInputs {
    func viewDidLoad()
    func close()
}

protocol BirthdayGiftLoadViewModelOutputs: AnyObject {
    func didNextStep(action: BirthdayGiftLoadAction)
    func didReceiveAnError(error: PicPayErrorDisplayable)
}

protocol BirthdayGiftLoadViewModelType: AnyObject {
    var inputs: BirthdayGiftLoadViewModelInputs { get }
    var outputs: BirthdayGiftLoadViewModelOutputs? { get set }
}

final class BirthdayGiftLoadViewModel: BirthdayGiftLoadViewModelType, BirthdayGiftLoadViewModelInputs {
    var inputs: BirthdayGiftLoadViewModelInputs { return self }
    weak var outputs: BirthdayGiftLoadViewModelOutputs?

    private let service: BirthdayGiftLoadServicing
    private let userId: String
    
    init(service: BirthdayGiftLoadServicing, userId: String) {
        self.service = service
        self.userId = userId
    }

    func viewDidLoad() {
        service.getConsumer(id: userId) { [weak self] result in
            switch result {
            case .success(let value):
                self?.requestSucceess(contact: value)
                
            case .failure(let error):
                self?.outputs?.didReceiveAnError(error: error)
            }
        }
    }
    
    func close() {
        outputs?.didNextStep(action: .close)
    }
    
    private func requestSucceess(contact: PPContact) {
        service.newBirthdayPaymentEnable ?
            outputs?.didNextStep(action: .openBirthday(contact)) : outputs?.didNextStep(action: .openLegacyBirthday(contact))
    }
}
