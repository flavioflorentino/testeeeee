enum NotificationLocalizable: String, Localizable {

    case checkout
    case checkoutAt
    case store
    
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .notificationLegacy
    }
}
