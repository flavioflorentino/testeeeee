import Foundation
import UI

final class EcommerceNotificationCell: UITableViewCell {
    
    @IBOutlet weak var profileImage: UIPPProfileImage!
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = Palette.ppColorGrayscale600.color
        }
    }
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.textColor = Palette.ppColorGrayscale400.color
        }
    }
    @IBOutlet weak var actionButton: UIPPButton!
    
    @objc func configure(model: EcommerceNotification) {
        actionButton.setTitle(NotificationLocalizable.checkout.text, for: .normal)
        actionButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        if let store = model.store {
            let storeTitle = NotificationLocalizable.checkoutAt.text
            let storeName = store.name ?? NotificationLocalizable.store.text
            let regularStyle = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)]
            let boldStyle = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)]
            let attrString = NSMutableAttributedString(string: storeTitle + storeName, attributes: regularStyle)
            
            attrString.addAttributes(boldStyle, range: NSRange(location: storeTitle.count, length: storeName.count))
            titleLabel.attributedText = attrString
            self.profileImage.setStore(store)
        }
        dateLabel.text = model.creationDate
    }
    
    
}
