//
//  EcommerceNotification.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 05/12/17.
//

import Foundation

final class EcommerceNotification: PPNotification {
    var store: PPStore?
    
    override init(dictionaty jsonDict:[AnyHashable: Any]?, viaPush:Bool) {
        if  let dict = jsonDict,
            let storeData = dict["store"] as? [AnyHashable: Any]{
            self.store = PPStore(profileDictionary: storeData)
        }
        super.init(dictionaty: jsonDict, viaPush: viaPush)!
    }
}
