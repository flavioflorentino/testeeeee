import Foundation

enum ProfileLocalizable: String, Localizable {
    case pay
    case paymentRequest
    case reportProfile
    case followers
    case following
    
    case unblockUser
    case unblock
    case blockUser
    case block
    case blockedUser
    case report
    case cancel
    case OkIgotIt
    case nowThatUserCanFollowYou
    case thisUserWillNoLongerBeAbleToFollowYou
    case youCanUnblock
    case tryAgain
    case somethingWentWrong
    case unableToBlockThisUser
    case unableToUnblockThisUser
    
    var key: String {
        return rawValue
    }
    var file: LocalizableFile {
        return .profile
    }
}
