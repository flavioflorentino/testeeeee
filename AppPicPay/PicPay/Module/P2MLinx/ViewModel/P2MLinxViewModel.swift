import Foundation
import SwiftyJSON

protocol P2MLinxViewModelInputs: AnyObject {
    var paymentManager: PPPaymentManager { get }
    func showPaymentInfo()
    func didTapInstallment()
    func updatePaymentValue(value: Double)
    func pay(privacyConfig: String?)
    func updateInstallment()
}

final class P2MLinxViewModel {
    private let service: P2MLinxServicing
    private let paymentService: P2MLinxPaymentServicing
    private let presenter: P2MLinxPresenting
    private let paymentInfo: LinxPaymentInfo
    var paymentManager: PPPaymentManager
    private var auth: PPAuth?
    
    init(
        service: P2MLinxServicing,
        paymentService: P2MLinxPaymentServicing,
        presenter: P2MLinxPresenting,
        paymentInfo: LinxPaymentInfo,
        paymentManager: PPPaymentManager = PPPaymentManager()
    ) {
        self.service = service
        self.paymentService = paymentService
        self.presenter = presenter
        self.paymentInfo = paymentInfo
        self.paymentManager = paymentManager
        
        guard let totalValue = Double(paymentInfo.totalValue) else {
            return
        }
        paymentManager.subtotal = NSDecimalNumber(value: totalValue)
    }
}

extension P2MLinxViewModel: P2MLinxViewModelInputs {
    func updateInstallment() {
        if hasInstallmentEnabled() {
            presenter.presentInstallmentChange(
                isInstallmentAllowed: true,
                installmentsNumber: paymentManager.selectedQuotaQuantity
            )
        } else {
            presenter.presentInstallmentChange(
                isInstallmentAllowed: false,
                installmentsNumber: paymentManager.selectedQuotaQuantity
            )
        }
    }
    
    private func hasInstallmentEnabled() -> Bool {
        return paymentManager.cardTotal().doubleValue > 0 &&
            paymentManager.total() != 0.0 &&
            paymentInfo.allowsInstallment == true
    }
    
    func showPaymentInfo() {
        presenter.presentPaymentInfo(paymentInfo: paymentInfo)
    }
    
    func didTapInstallment() {
        guard hasInstallmentEnabled() else {
            showInstallmentTooltip()
            return
        }
        openInstallment()
    }
    
    private func showInstallmentTooltip() {
        presenter.presentInstallmentTooltip()
    }
    
    private func openInstallment() {
        let viewModel = InstallmentsSelectorViewModel(
            paymentManager: paymentManager,
            payeeId: nil,
            sellerId: paymentInfo.sellerId,
            origin: nil,
            titleHeader: nil
        )
        
        presenter.presentInstallment(viewModel: viewModel)
    }
    
    func updatePaymentValue(value: Double) {
        paymentManager.subtotal = NSDecimalNumber(value: value)
    }
    
    func pay(privacyConfig: String?) {
        validatePin(privacyConfig)
    }
    
    private func validatePin(_ privacyConfig: String?) {
        auth = PPAuth.authenticate({ [weak self] authToken, biometry in
            self?.presenter.presentLoading()
            self?.checkCvvAndCreateTransaction(
                password: authToken ?? "",
                biometry: biometry,
                privacyConfig: privacyConfig
            )
            }, canceledByUserBlock: { }
        )
    }
    
    private func checkCvvAndCreateTransaction(password: String, biometry: Bool, privacyConfig: String?) {
        guard checkNeedCvv() else {
            createTransaction(withPin: password, biometry: biometry, privacyConfig: privacyConfig)
            return
        }
        
        presenter.presentAskCvv(completedWithCvv: { [weak self] cvv in
            self?.createTransaction(withPin: password, biometry: biometry, privacyConfig: privacyConfig, informedCvv: cvv)
        }, completedWithoutCvv: { [weak self] in
            let error = PicPayError(message: DefaultLocalizable.cvvNotInformed.text)
            self?.presenter.presentError(error)
        })
    }
    
    private func createTransaction(withPin pin: String, biometry: Bool, privacyConfig: String?, informedCvv: String? = nil) {
        let paymenRequest = service.createPaymentRequest(
            pin: pin,
            biometry: biometry,
            paymentManager: paymentManager,
            linxPaymentInfo: paymentInfo,
            privacyConfig: privacyConfig
        )
        let cvv = createCvv(informedCvv: informedCvv)
        
        paymentService.requestTransaction(paymenRequest: paymenRequest, password: pin, cvv: cvv) { [weak self] result in
            switch result {
            case .success(let receiptModel):
                HapticFeedback.notificationFeedbackSuccess()
                self?.saveCvv(informedCvv: informedCvv)
                NotificationCenter.default.post(
                    name: Notification.Name.Payment.new,
                    object: nil,
                    userInfo: ["type": "P2M"]
                )
                self?.presenter.presentReceipt(receiptWidget: receiptModel)
            case .failure(let error):
                self?.presenter.presentError(error)
            }
        }
    }
    
    private func createCvv(informedCvv: String?) -> String? {
        guard let cvv = informedCvv else {
            return createLocalStoreCVVPayload()
        }
        
        return cvv
    }
    
    private func createLocalStoreCVVPayload() -> String? {
        guard
            paymentManager.cardTotal() != .zero,
            let id = service.defauldCard?.id,
            let cvv = service.cvvCard(id: id)
            else {
                return nil
        }
        
        return cvv
    }
    
    private func saveCvv(informedCvv: String?) {
        guard let id = service.defauldCard?.id, let cvv = informedCvv else {
            return
        }
        service.saveCvvCard(id: id, value: cvv)
    }
    
    private func checkNeedCvv() -> Bool {
        guard
            let cardBank = service.defauldCard,
            let cardValue = paymentManager.cardTotal() as? Double
            else {
                return false
        }
        
        return service.pciCvvIsEnable(cardBank: cardBank, cardValue: cardValue)
    }
}
