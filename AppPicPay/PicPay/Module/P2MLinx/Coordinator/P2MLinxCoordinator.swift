import UI

protocol P2MLinxCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func showReceipt(receiptViewModel: ReceiptWidgetViewModel)
    func showInstallmentList(viewModel: InstallmentsSelectorViewModel)
    func openCvv(completedWithCvv: @escaping (String) -> Void, completedWithoutCvv: @escaping () -> Void)
}

final class P2MLinxCoordinator: P2MLinxCoordinating {
    private var coordinator: Coordinating?
    weak var viewController: UIViewController?
    
    func showReceipt(receiptViewModel: ReceiptWidgetViewModel) {
        guard let vc = viewController else {
            return
        }
        TransactionReceipt.showReceiptSuccess(viewController: vc, receiptViewModel: receiptViewModel)
    }
    
    func showInstallmentList(viewModel: InstallmentsSelectorViewModel) {
        let controller = InstallmentsSelectorViewController(with: viewModel)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    func openCvv(completedWithCvv: @escaping (String) -> Void, completedWithoutCvv: @escaping () -> Void) {
        guard let navigation = viewController?.navigationController else {
            return
        }
        
        let coordinator = CvvRegisterFlowCoordinator(
            navigationController: navigation,
            paymentType: .linx,
            finishedCvv: completedWithCvv,
            finishedWithoutCvv: completedWithoutCvv
        )
        coordinator.start()
        
        self.coordinator = coordinator
    }
}
