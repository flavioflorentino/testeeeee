import SwiftyJSON
import Core

struct LinxPaymentInfo: Decodable {
    enum CodingKeys: String, CodingKey {
        case sellerName = "seller_name"
        case allowsInstallment = "allows_installment"
        case additionalInfo = "additional_info"
        case planType = "plan_type"
        case idTransaction = "id_transacao_lojista"
        case sellerId = "seller_id"
        case items = "itens"
        case sellerImage = "seller_image"
        case totalValue = "total_value"
        case allowsChangeAmountPayable = "allows_change_amount_payable"
    }
    
    let sellerName: String
    let allowsInstallment: Bool
    let additionalInfo: AnyCodable
    let planType: String
    let idTransaction: String
    let sellerId: String
    let items: Item
    let sellerImage: String
    let totalValue: String
    let allowsChangeAmountPayable: Bool
}

extension LinxPaymentInfo {
    struct Item: Decodable {
        let amount: String
        let id: String
    }
}
