import SwiftyJSON

final class P2MLinxModelAdapter {
    static func transformToLinxPaymentInfo(from json: JSON) -> LinxPaymentInfo? {
        let jsonDecoder = JSONDecoder()
        guard
            let data = try? JSONSerialization.data(withJSONObject: json.object),
            let paymentInfo = try? jsonDecoder.decode(LinxPaymentInfo.self, from: data)
            else { return nil }
        return paymentInfo
    }
}
