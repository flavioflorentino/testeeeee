import UIKit
import UI
import AMPopTip

protocol P2MLinxViewDelegate: AnyObject {
    func didTapInstallmentButton()
}

final class P2MLinxView: UIView {
    private struct Layout {
        static let zero: CGFloat = 0
        static let sellerNameFont = UIFont.systemFont(ofSize: 14)
        static let stackHeight: CGFloat = 60
        static let currencyFieldFont = UIFont.systemFont(ofSize: 26)
        static let installmentFont = UIFont.systemFont(ofSize: 15)
        static let rightMargin: CGFloat = -9
        static let leftMargin: CGFloat = 9
        static let separatorHeight: CGFloat = 1
        static let imageSize: CGFloat = 60
        static let contentViewHeight: CGFloat = 92
        static let textfieldFontSize: CGFloat = 26
        static let tooltipVerticalMargin: CGFloat = 6
        static let tooltipHorizontalMargin: CGFloat = 8
        static let tooltipOffset: CGFloat = 4
        static let tooltipEdgeMargin: CGFloat = 4
        static let tooltipCornerRadius: CGFloat = 8
        static let tooltipArrowSize: CGFloat = 16
        static let buttonBorderWidth: CGFloat = 1.5
        static let buttonCornerRadius: CGFloat = 7.0
        static let buttonSize: CGFloat = 32
        static let toolBarBottom: CGFloat = -15
        static let toolBarHeight: CGFloat = 100
    }
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var stackSellerItens: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.alignment = .fill
        stack.spacing = 6
        stack.distribution = .fillProportionally
        return stack
    }()
    
    private lazy var sellerNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = Layout.sellerNameFont
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var valueCurrencyField: CurrencyField = {
        let value = CurrencyField()
        value.fontCurrency = UIFont.systemFont(
            ofSize: Layout.textfieldFontSize,
            weight: .regular
        )
        value.fontValue = UIFont.systemFont(
            ofSize: Layout.textfieldFontSize,
            weight: .regular
        )
        value.positionCurrency = .center
        value.limitValue = 7
        value.translatesAutoresizingMaskIntoConstraints = false
        
        return value
    }()
    
    private lazy var valueCurrencyGestureView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var installmentsButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.borderWidth = Layout.buttonBorderWidth
        button.layer.cornerRadius = Layout.buttonCornerRadius
        button.addTarget(self, action: #selector(didTapInstallment), for: .touchUpInside)
        button.titleLabel?.font = Layout.installmentFont
        return button
    }()
    
    private lazy var installmentsButtonGestureView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var installmentTooltip: PopTip = {
        let popTip = PopTip()
        popTip.bubbleColor = Palette.ppColorGrayscale500.color
        popTip.shouldDismissOnTap = false
        popTip.shouldDismissOnTapOutside = false
        popTip.offset = Layout.tooltipOffset
        popTip.edgeInsets = UIEdgeInsets(
            top: Layout.tooltipVerticalMargin,
            left: Layout.tooltipHorizontalMargin,
            bottom: Layout.tooltipVerticalMargin,
            right: Layout.tooltipHorizontalMargin
        )
        popTip.cornerRadius = Layout.tooltipCornerRadius
        popTip.arrowSize = CGSize(width: Layout.tooltipArrowSize, height: Layout.tooltipArrowSize)
        popTip.textColor = Palette.ppColorGrayscale100.color
        popTip.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        popTip.edgeMargin = Layout.tooltipEdgeMargin
        return popTip
    }()
    
    private lazy var paymentValueTooltip: PopTip = {
        let popTip = PopTip()
        popTip.bubbleColor = Palette.ppColorGrayscale500.color
        popTip.shouldDismissOnTap = false
        popTip.shouldDismissOnTapOutside = false
        popTip.offset = Layout.tooltipOffset
        popTip.edgeInsets = UIEdgeInsets(
            top: Layout.tooltipVerticalMargin,
            left: Layout.tooltipHorizontalMargin,
            bottom: Layout.tooltipVerticalMargin,
            right: Layout.tooltipHorizontalMargin
        )
        popTip.cornerRadius = Layout.tooltipCornerRadius
        popTip.arrowSize = CGSize(width: Layout.tooltipArrowSize, height: Layout.tooltipArrowSize)
        popTip.textColor = Palette.ppColorGrayscale100.color
        popTip.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        popTip.edgeMargin = Layout.tooltipEdgeMargin
        return popTip
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale300.color
        return view
    }()
    
    private lazy var toolBarContainer: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale100.color
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var paymentToolbar: PaymentToolbar = {
        let toolbar = PaymentToolbar(frame: .zero)
        
        toolbar.isUserInteractionEnabled = true
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        return toolbar
    }()
    
    private lazy var toolBarContainerBottomConstraint: NSLayoutConstraint = {
        let constraint = toolBarContainer.bottomAnchor.constraint(equalTo: bottomAnchor)
        constraint.constant = Layout.zero
        return constraint
    }()
    
    weak var delegate: P2MLinxViewDelegate?
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        addKeyboardObservers()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addKeyboardObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    @objc
    private func keyboardWillShow(notification: NSNotification) {
        guard
            let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue,
            let offset = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            else {
                return
        }
        
        toolBarContainerBottomConstraint.constant = keyboardSize.height == offset.height ? -keyboardSize.height : -offset.height
        layoutIfNeeded()
    }
    
    @objc
    private func didTapInstallment() {
        delegate?.didTapInstallmentButton()
    }
    
    @objc
    private func keyboardWillHide(notification: NSNotification) {
        toolBarContainerBottomConstraint.constant = Layout.zero
        layoutIfNeeded()
    }
}

extension P2MLinxView: ViewConfiguration {
    public func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    public func buildViewHierarchy() {
        addSubview(contentView)
        addSubview(toolBarContainer)
        contentView.addSubview(imageView)
        contentView.addSubview(stackSellerItens)
        contentView.addSubview(installmentsButton)
        contentView.addSubview(separatorView)
        stackSellerItens.addArrangedSubview(sellerNameLabel)
        stackSellerItens.addArrangedSubview(valueCurrencyField)
        toolBarContainer.addSubview(paymentToolbar)
    }
    
    public func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(
            equalTo: self,
            for: [contentView, separatorView, toolBarContainer],
            constant: Layout.zero
        )
        NSLayoutConstraint.leadingTrailing(equalTo: toolBarContainer, for: [paymentToolbar], constant: Layout.zero)
        
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: compatibleSafeAreaLayoutGuide.topAnchor),
            contentView.heightAnchor.constraint(equalToConstant: Layout.contentViewHeight)
        ])
        
        NSLayoutConstraint.activate([
            imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: Layout.leftMargin),
            imageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            imageView.heightAnchor.constraint(equalToConstant: Layout.imageSize),
            imageView.widthAnchor.constraint(equalToConstant: Layout.imageSize)
        ])
        
        NSLayoutConstraint.activate([
            stackSellerItens.leftAnchor.constraint(equalTo: imageView.rightAnchor, constant: Layout.leftMargin),
            stackSellerItens.rightAnchor.constraint(equalTo: installmentsButton.leftAnchor, constant: Layout.rightMargin),
            stackSellerItens.centerYAnchor.constraint(equalTo: imageView.centerYAnchor),
            stackSellerItens.heightAnchor.constraint(equalToConstant: Layout.stackHeight)
        ])
        
        NSLayoutConstraint.activate([
            installmentsButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: Layout.rightMargin),
            installmentsButton.heightAnchor.constraint(equalToConstant: Layout.buttonSize),
            installmentsButton.widthAnchor.constraint(equalToConstant: Layout.buttonSize),
            installmentsButton.bottomAnchor.constraint(equalTo: valueCurrencyField.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            separatorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: Layout.separatorHeight)
        ])
        
        NSLayoutConstraint.activate([
            toolBarContainerBottomConstraint,
            toolBarContainer.heightAnchor.constraint(equalToConstant: Layout.toolBarHeight)
        ])
        
        NSLayoutConstraint.activate([
            paymentToolbar.topAnchor.constraint(equalTo: toolBarContainer.topAnchor),
            paymentToolbar.bottomAnchor.constraint(
                equalTo: toolBarContainer.bottomAnchor,
                constant: Layout.toolBarBottom
            )
        ])
    }
    
    private func setupView() {
        buildViewHierarchy()
        configureViews()
        setupConstraints()
    }
    
    func updateButton(viewState: P2MLinxViewState.Button) {
        installmentsButton.layer.borderColor = viewState.borderColor
        installmentsButton.setTitle(viewState.title, for: .normal)
        installmentsButton.setTitleColor(viewState.titleColor, for: .normal)
    }
    
    func bind(viewState: P2MLinxViewState) {
        sellerNameLabel.text = viewState.label.title
        imageView.setImage(url: viewState.imageView.image)
        valueCurrencyField.placeholderColor = viewState.textField.color
        valueCurrencyField.textColor = viewState.textField.color
        valueCurrencyField.value = viewState.textField.value
        installmentsButton.layer.borderColor = viewState.button.borderColor
        installmentsButton.setTitle(viewState.button.title, for: .normal)
        installmentsButton.setTitleColor(viewState.button.titleColor, for: .normal)
        addTextfieldGestureIfNeeded(viewState: viewState.textField)
    }
    
    private func addTextfieldGestureIfNeeded(viewState: P2MLinxViewState.TextField) {
        if viewState.isEnabled == false {
            setupValueCurrencyGestureView()
            addPaymentAmountGesture()
        }
        layoutIfNeeded()
    }
    
    private func setupValueCurrencyGestureView() {
        contentView.addSubview(valueCurrencyGestureView)
        NSLayoutConstraint.activate([
            valueCurrencyGestureView.centerYAnchor.constraint(equalTo: valueCurrencyField.centerYAnchor),
            valueCurrencyGestureView.centerXAnchor.constraint(equalTo: valueCurrencyField.centerXAnchor),
            valueCurrencyGestureView.heightAnchor.constraint(equalTo: valueCurrencyField.heightAnchor),
            valueCurrencyGestureView.widthAnchor.constraint(equalTo: valueCurrencyField.widthAnchor)
        ])
    }
    
    private func addPaymentAmountGesture() {
        let paymentAmountTap = UITapGestureRecognizer(target: self, action: #selector(showPaymentAmountTooltip))
        valueCurrencyGestureView.addGestureRecognizer(paymentAmountTap)
    }
    
    func showInstallmentTooltip() {
        paymentValueTooltip.hide()
        installmentTooltip.show(
            text: P2MLinxLocalizable.installmentNotAllowed.text,
            direction: .down,
            maxWidth: self.frame.width,
            in: self,
            from: installmentsButton.frame
        )
        installmentsButton.isUserInteractionEnabled = false
        waitTime(time: 5) { [weak self] in
            self?.installmentTooltip.hide()
        }
    }
    
    @objc
    private func showPaymentAmountTooltip() {
        installmentTooltip.hide()
        paymentValueTooltip.show(
            text: P2MLinxLocalizable.valueNotEditable.text,
            direction: .down,
            maxWidth: self.frame.width,
            in: self,
            from: valueCurrencyField.frame
        )
        
        waitTime(time: 5) { [weak self] in
            self?.paymentValueTooltip.hide()
        }
    }
    
    private func waitTime(time: TimeInterval, handler: @escaping(() -> Void)) {
        DispatchQueue.main.asyncAfter(deadline: .now() + time) {
            handler()
        }
    }
}
