import Core
import UI

struct P2MLinxViewState {
    struct ImageView {
        let image: URL?
    }
    
    struct Label {
        let title: String
    }
    
    struct TextField {
        let value: Double
        let isEnabled: Bool
        let color: UIColor
    }
    
    struct Button {
        let title: String
        let titleColor: UIColor
        let isEnabled: Bool
        let borderColor: CGColor
    }
    
    let imageView: ImageView
    let label: Label
    let textField: TextField
    let button: Button
}

protocol P2MLinxPresenting: AnyObject {
    var viewController: P2MLinxDisplay? { get set }
    func presentPaymentInfo(paymentInfo: LinxPaymentInfo)
    func presentInstallment(viewModel: InstallmentsSelectorViewModel)
    func presentReceipt(receiptWidget: ReceiptWidgetViewModel)
    func presentError(_ error: PicPayError)
    func presentInstallmentChange(isInstallmentAllowed: Bool, installmentsNumber: Int)
    func presentAskCvv(completedWithCvv: @escaping (String) -> Void, completedWithoutCvv: @escaping () -> Void)
    func presentLoading()
    func presentInstallmentTooltip()
}

final class P2MLinxPresenter: P2MLinxPresenting {
    private let coordinator: P2MLinxCoordinating
    weak var viewController: P2MLinxDisplay?
    
    init(coordinator: P2MLinxCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentPaymentInfo(paymentInfo: LinxPaymentInfo) {
        let url = URL(string: paymentInfo.sellerImage)
        let imageView = P2MLinxViewState.ImageView(image: url)
        let label = P2MLinxViewState.Label(title: paymentInfo.sellerName)
        guard let amount = Double(paymentInfo.totalValue) else {
            return
        }
        let textFieldColor = paymentInfo.allowsChangeAmountPayable == true ?
            Palette.ppColorBranding300.color : Palette.ppColorGrayscale400.color
        
        let textField = P2MLinxViewState.TextField(
            value: amount,
            isEnabled: paymentInfo.allowsChangeAmountPayable,
            color: textFieldColor
        )
        
        let buttonBorderColor = paymentInfo.allowsInstallment == true ?
            Palette.ppColorBranding300.cgColor : Palette.ppColorGrayscale400.cgColor
        
        let buttonTitleColor = paymentInfo.allowsInstallment == true ?
            Palette.ppColorBranding300.color : Palette.ppColorGrayscale400.color
        
        let button = P2MLinxViewState.Button(
            title: "1x",
            titleColor: buttonTitleColor,
            isEnabled: paymentInfo.allowsInstallment,
            borderColor: buttonBorderColor
        )
        
        let viewState = P2MLinxViewState(
            imageView: imageView,
            label: label,
            textField: textField,
            button: button
        )
        
        viewController?.displayPaymentInfo(viewState: viewState)
    }
    
    func presentInstallment(viewModel: InstallmentsSelectorViewModel) {
        viewController?.displayInstallment(viewModel: viewModel)
        coordinator.showInstallmentList(viewModel: viewModel)
    }
    
    func presentInstallmentChange(isInstallmentAllowed: Bool, installmentsNumber: Int) {
        let title = "\(installmentsNumber)x"
        let buttonBorderColor: CGColor
        let buttonTitleColor: UIColor
        
        if isInstallmentAllowed {
            buttonBorderColor = Palette.ppColorBranding300.cgColor
            buttonTitleColor = Palette.ppColorBranding300.color
        } else {
            buttonBorderColor = Palette.ppColorGrayscale400.cgColor
            buttonTitleColor = Palette.ppColorGrayscale400.color
        }
        
        let button = P2MLinxViewState.Button(
            title: title,
            titleColor: buttonTitleColor,
            isEnabled: isInstallmentAllowed,
            borderColor: buttonBorderColor
        )
        
        viewController?.displayInstallmentUpdate(viewState: button)
    }
    
    func presentReceipt(receiptWidget: ReceiptWidgetViewModel) {
        coordinator.showReceipt(receiptViewModel: receiptWidget)
    }
    
    func presentAskCvv(completedWithCvv: @escaping (String) -> Void, completedWithoutCvv: @escaping () -> Void) {
        coordinator.openCvv(completedWithCvv: completedWithCvv, completedWithoutCvv: completedWithoutCvv)
    }
    
    func presentError(_ error: PicPayError) {
        DispatchQueue.main.async { [weak self] in
            self?.viewController?.displayError(error: error)
        }
    }
    
    func presentLoading() {
        viewController?.displayLoading()
    }
    
    func presentInstallmentTooltip() {
        viewController?.displayInstallmentTooltip()
    }
}
