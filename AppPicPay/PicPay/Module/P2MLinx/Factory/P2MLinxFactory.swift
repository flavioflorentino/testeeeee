import FeatureFlag
import Foundation

final class P2MLinxFactory {
    static func make(paymentInfo: LinxPaymentInfo) -> P2MLinxViewController {
        let dependencies = DependencyContainer()
        let service: P2MLinxServicing = P2MLinxService(dependencies: dependencies)
        let paymentService: P2MLinxPaymentServicing = FeatureManager.isActive(.pciLinx) ?
        P2MLinxPaymentPciService(dependencies: dependencies) : P2MLinxPaymentService(dependencies: dependencies)
        
        let coordinator: P2MLinxCoordinating = P2MLinxCoordinator()
        let presenter: P2MLinxPresenting = P2MLinxPresenter(coordinator: coordinator)
        let viewModel = P2MLinxViewModel(
            service: service,
            paymentService: paymentService,
            presenter: presenter,
            paymentInfo: paymentInfo
        )
        let viewController = P2MLinxViewController(viewModel: viewModel)
        
        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
}
