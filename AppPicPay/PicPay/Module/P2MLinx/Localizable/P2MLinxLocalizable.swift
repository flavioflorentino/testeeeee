import Foundation

enum P2MLinxLocalizable: String, Localizable {
    case installmentNotAllowed
    case valueNotEditable
    case cancel
    case title
    case loadingTitle
    case unidentifiedCode
    
    var key: String {
        return self.rawValue
    }
    
    var file: LocalizableFile {
        return .p2mLinx
    }
}
