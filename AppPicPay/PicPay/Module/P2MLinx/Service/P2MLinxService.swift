import Core
import FeatureFlag
import PCI
import SwiftyJSON

protocol P2MLinxServicing {
    var defauldCard: CardBank? { get }
    
    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool
    func saveCvvCard(id: String, value: String?)
    func cvvCard(id: String) -> String?
    func createPaymentRequest(
        pin: String,
        biometry: Bool,
        paymentManager: PPPaymentManager,
        linxPaymentInfo: LinxPaymentInfo,
        privacyConfig: String?
    ) -> LinxPayload
}

final class P2MLinxService: P2MLinxServicing {
    typealias Dependencies = HasCreditCardManager & HasFeatureManager & HasKeychainManager
    private let dependencies: Dependencies
    
    var defauldCard: CardBank? {
        return dependencies.creditCardManager.defaultCreditCard
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool {
        return CvvRegisterFlowCoordinator.cvvFlowEnable(cardBank: cardBank, cardValue: cardValue, paymentType: .linx)
    }
    
    func saveCvvCard(id: String, value: String?) {
        return dependencies.keychain.set(key: KeychainKeyPF.cvv(id), value: value)
    }
    
    func cvvCard(id: String) -> String? {
        return dependencies.keychain.getData(key: KeychainKeyPF.cvv(id))
    }
    
    func createPaymentRequest(
        pin: String,
        biometry: Bool,
        paymentManager: PPPaymentManager,
        linxPaymentInfo: LinxPaymentInfo,
        privacyConfig: String?
    ) -> LinxPayload {
        let additionalInfoRequest = linxPaymentInfo.additionalInfo
        
        let itemRequest = LinxPayload.Item(
            id: linxPaymentInfo.items.id,
            amount: linxPaymentInfo.items.amount
        )
        
        var creditCardId = ""
        let ignoreBalance = !paymentManager.usePicPayBalance()
        var feedVisibility: Int = 3
        if let privacy = privacyConfig {
            feedVisibility = Int(privacy) ?? 3
        }
        
        if dependencies.featureManager.isActive(.opsLinxKillswitchBool) {
            if let totalValue = paymentManager.cardTotalWithoutInterest()?.doubleValue, totalValue > 0.0 {
                creditCardId = String(dependencies.creditCardManager.defaultCreditCardId)
            }
        } else if ignoreBalance {
            creditCardId = String(dependencies.creditCardManager.defaultCreditCardId)
        }
        
        var total = ""
        if let totalValue = paymentManager.cardTotalWithoutInterest()?.doubleValue {
            total = String(format: "%.2f", totalValue)
        }
        
        var credit = ""
        if let creditValue = paymentManager.balanceTotal()?.doubleValue {
            credit = String(format: "%.2f", creditValue)
        }
        
        let paymentRequest = LinxPayload(
            pin: pin,
            gpsAcc: 0,
            ignoreBalance: ignoreBalance,
            fromExplore: 0,
            biometry: biometry,
            credit: credit,
            installment: String(paymentManager.selectedQuotaQuantity),
            sellerTransactionId: linxPaymentInfo.idTransaction,
            sellerId: linxPaymentInfo.sellerId,
            planType: linxPaymentInfo.planType,
            total: total,
            creditCardId: creditCardId,
            feedVisibility: feedVisibility,
            itens: [itemRequest],
            additionalInfo: additionalInfoRequest,
            shippingId: "0",
            shipping: "0",
            addressId: "0"
        )
        return paymentRequest
    }
}
