import Core
import FeatureFlag
import Foundation
import PCI

protocol P2MLinxPaymentServicing {
    func requestTransaction(
        paymenRequest: LinxPayload,
        password: String,
        cvv: String?,
        completion: @escaping ((PicPayResult<ReceiptWidgetViewModel>) -> Void)
    )
}

final class P2MLinxPaymentService: P2MLinxPaymentServicing {
    typealias Dependencies = HasMainQueue & HasFeatureManager
    private let dependencies: Dependencies
    private let requestManager = RequestManager.shared
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func requestTransaction(
        paymenRequest: LinxPayload,
        password: String,
        cvv: String?,
        completion: @escaping ((PicPayResult<ReceiptWidgetViewModel>) -> Void)
    ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let params = paymenRequest.toDictionary() ?? [:]
            requestManager.apiRequest(
                endpoint: kWsUrlCreateTransaction,
                method: .post,
                parameters: params,
                pin: password
            )
            .responseApiObject { [weak self] (response: WrapperResponse) in
                self?.dependencies.mainQueue.async { [weak self] in
                    guard let self = self else {
                        completion(.failure(PicPayError(message: DefaultLocalizable.unexpectedError.text)))
                        return
                    }
                    completion(self.generateResult(response: response))
                }
            }
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    private func generateResult(response: WrapperResponse) -> PicPayResult<ReceiptWidgetViewModel> {
        switch response {
        case .success(let baseGenericResponse):
            let receiptViewModel = createReceiptModel(response: baseGenericResponse)
            if let receiptModel = receiptViewModel {
                return (.success(receiptModel))
            } else {
                return (.failure(PicPayError(message: DefaultLocalizable.receiptError.text)))
            }
        case .failure(let error):
            return (.failure(error))
        }
    }
    
    private func createReceiptModel(response: BaseApiGenericResponse) -> ReceiptWidgetViewModel? {
        guard
            let dic = response.json.dictionary,
            let receiptData = dic[Keys.data.rawValue]?.dictionary,
            let receipt = receiptData[Keys.receipt.rawValue]?.array
        else {
            return nil
        }
        
        var optionalId: String? = receiptData[Keys.id.rawValue]?.string
        
        if let transaction = receiptData[Keys.transaction.rawValue]?.dictionary,
           let id = transaction[Keys.id.rawValue]?.string {
            optionalId = id
        }
        
        guard let id = optionalId else {
            return nil
        }
        
        let widgets: [ReceiptWidgetItem] = WSReceipt.createReceiptWidgetItem(receipt)
        
        let receiptModel = ReceiptWidgetViewModel(transactionId: id, type: .PAV)
        
        if widgets.isEmpty == false {
            receiptModel.setReceiptWidgets(items: widgets)
        }
        
        return receiptModel
    }
}

extension P2MLinxPaymentService {
    enum Keys: String {
        case data
        case receipt
        case id
        case transaction = "Transaction"
    }
}
