import Core
import Foundation
import PCI

final class P2MLinxPaymentPciService: P2MLinxPaymentServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    private let pciService: LinxServicing
    
    init(pciService: LinxServicing = LinxService(), dependencies: Dependencies) {
        self.pciService = pciService
        self.dependencies = dependencies
    }
    
    func requestTransaction(
        paymenRequest: LinxPayload,
        password: String,
        cvv: String?,
        completion: @escaping ((PicPayResult<ReceiptWidgetViewModel>) -> Void)
    ) {
        let cvvPayload = createCVVPayload(cvv: cvv)
        let payload = PaymentPayload<LinxPayload>(cvv: cvvPayload, generic: paymenRequest)
        pciService.createTransaction(password: password, payload: payload, isNewArchitecture: false) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let value):
                    let receiptModel = ReceiptWidgetViewModel(transactionId: value.transactionId ?? "", type: .PAV)
                    let receipt = value.receiptWidgets.compactMap { WSReceipt.createReceiptWidgetItem(jsonDict: $0) }
                    receiptModel.setReceiptWidgets(items: receipt)
                    
                    completion(.success(receiptModel))
                    
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    private func createCVVPayload(cvv: String?) -> CVVPayload? {
        guard let cvv = cvv else {
            return nil
        }
        
        return CVVPayload(value: cvv)
    }
}
