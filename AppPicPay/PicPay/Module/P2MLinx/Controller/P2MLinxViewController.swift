import UI

protocol P2MLinxDisplay: AnyObject {
    func displayPaymentInfo(viewState: P2MLinxViewState)
    func displayInstallment(viewModel: InstallmentsSelectorViewModel)
    func displayError(error: PicPayError)
    func displayLoading()
    func hideLoading()
    func displayInstallmentUpdate(viewState: P2MLinxViewState.Button)
    func displayInstallmentTooltip()
}

final class P2MLinxViewController: ViewController<P2MLinxViewModelInputs, P2MLinxView> {
    private lazy var leftBarButton: UIBarButtonItem = {
        let barButton = UIBarButtonItem(
            title: P2MLinxLocalizable.cancel.text,
            style: .plain,
            target: self,
            action: #selector(dismissController)
        )
        return barButton
    }()
    
    fileprivate var toolBarController: PaymentToolbarController?
    private var loadingView: UIView?
    private var auth: PPAuth?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        setupToolBarController()
        viewModel.showPaymentInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.updateInstallment()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    private func setupToolBarController() {
        rootView.delegate = self
        toolBarController = PaymentToolbarController(
            paymentManager: viewModel.paymentManager,
            parent: self,
            toolbar: rootView.paymentToolbar,
            pay: { [weak self] privacyConfig in
                self?.pay(privacyConfig: privacyConfig)
            }
        )
    }
    
    private func pay(privacyConfig: String) {
        viewModel.pay(privacyConfig: privacyConfig)
    }
    
    override func configureViews() {
        title = P2MLinxLocalizable.title.text
        navigationItem.leftBarButtonItem = leftBarButton
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        if self.hasSafeAreaInsets {
            self.paintSafeAreaBottomInset(withColor: Palette.ppColorGrayscale100.color)
        }
    }
    
    @objc
    private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc
    private func dismissController() {
        dismiss(animated: true, completion: nil)
    }
    
    func createLoadingView(_ title: String) {
        loadingView?.removeFromSuperview()
        loadingView = Loader.getLoadingView(title)
        guard let loadingView = loadingView
            else { return }
        view.addSubview(loadingView)
        loadingView.isHidden = false
    }
}

extension P2MLinxViewController: P2MLinxDisplay {
    func displayPaymentInfo(viewState: P2MLinxViewState) {
        rootView.bind(viewState: viewState)
        viewModel.updateInstallment()
    }
    
    func displayInstallment(viewModel: InstallmentsSelectorViewModel) {
        viewModel.delegate = self
    }
    
    func displayInstallmentUpdate(viewState: P2MLinxViewState.Button) {
        rootView.updateButton(viewState: viewState)
    }
    
    func hideLoading() {
        loadingView?.isHidden = true
    }
    
    func displayLoading() {
        createLoadingView(P2MLinxLocalizable.loadingTitle.text)
    }
    
    func displayInstallmentTooltip() {
        rootView.showInstallmentTooltip()
    }
    
    func displayError(error: PicPayError) {
        loadingView?.isHidden = true
        AlertMessage.showAlert(error, controller: self, alertButton: { [weak self] popup, button, _ in
            popup.dismiss(animated: true) {
                if case .backToScanner = button.action {
                    self?.dismissController()
                }
            }
        })
    }
}

extension P2MLinxViewController: P2MLinxViewDelegate {
    func didTapInstallmentButton() {
        viewModel.didTapInstallment()
    }
}

extension P2MLinxViewController: InstallmentsSelectorViewModelDelegate {
    func didSelectedInstallment(_ paymentManager: PPPaymentManager) {
        viewModel.updateInstallment()
    }
}

extension P2MLinxViewController: CurrencyFieldDelegate {
    func onValueChange(value: Double) {
        viewModel.updatePaymentValue(value: value)
    }
}
