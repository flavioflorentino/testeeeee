import FeatureFlag
import Foundation

extension LegacySearchResultRow {
    convenience init(row: SearchResultRow, featureManager: FeatureManagerContract = FeatureManager.shared) {
        self.init(type: .unknown, fullData: .init())

        self.type = SearchResultType(rawValue: row.type.rawValue) ?? .unknown
        basicData.type = self.type
        basicData.id = row.id
        basicData.imageUrl = row.profileImageUrl
        basicData.title = row.title
        basicData.descriptionText = row.body

        switch row.type {
        case .consumer:
            basicData.title = "@\(basicData.title ?? "")"
            basicData.isPro = row.profileTags.isPro
            basicData.isVerified = row.profileTags.isVerified
            basicData.canReceivePaymentRequest = false
            fullData = PPContact(row: row)
        case .store:
            let store = PPStore(row: row)
            basicData.titleDetail = store.distanceAsString()
            fullData = store
        case .digitalGood, .financial:
            fullData = DGItem(row: row)
        case .p2m:
            fullData = P2MItem(row: row)
        case .membership:
            fullData = featureManager.isActive(.subscription) ? ProducerProfileItem(row: row) : .init()
            basicData.titleDetail = row.footer
        case .p2p:
            basicData.imageUrl = row.profileImageUrl
            basicData.largeImageUrl = row.profileImageUrl
            basicData.title = row.title
            basicData.descriptionText = row.body

            fullData = P2PItem(row: row)
        case .pix:
            basicData.imageUrl = row.profileImageUrl
            basicData.title = row.title
            basicData.descriptionText = row.body
            fullData = PixSearchItem(row: row)
        }
    }
}

private extension PPContact {
    convenience init(row: SearchResultRow) {
        self.init()
        self.wsId = Int(row.id) ?? 0
        self.isVerified = row.profileTags.isVerified
        self.onlineName = row.body
        self.businessAccount = row.profileTags.isPro
        self.imgUrl = row.profileImageUrl
        self.imgUrlLarge = row.profileImageUrl
        self.isPicpayUser = true
        self.contactType = .profilePopUp
        self.username = row.title
    }
}

private extension PPStore {
    convenience init(row: SearchResultRow) {
        self.init()
        self.name = row.title
        self.address = row.body
        self.isCielo = row.legacyTags?.isCielo ?? false
        self.wsId = Int(row.id) ?? 0 //needed for itemByStoreId network call
        self.storeId = row.id
        self.isVerified = row.profileTags.isVerified
        self.img_url = row.profileImageUrl
        if let isParking = row.legacyTags?.isParking {
            self.isParkingPayment = isParking ? 2 : 0
        }
        self.distance = Int32(Double(row.footer ?? "") ?? 0)
    }
}

private extension DGItem {
    convenience init(row: SearchResultRow) {
        let service = DGService(rawValue: row.legacyTags?.service ?? "")
        self.init(id: row.id, service: service)
        self.name = row.title
        self.itemDescription = row.body
        self.imgUrl = row.profileImageUrl
        self.isStudentAccount = row.profileTags.isStudent
    }
}

private extension P2MItem {
    convenience init(row: SearchResultRow) {
        self.init()
        self.name = row.title
        self.descriptionText = row.body
        self.imageUrl = row.profileImageUrl ?? ""
        self.service = ServiceType(rawValue: row.legacyTags?.service ?? "") ?? .unknown
    }
}

private extension ProducerProfileItem {
    convenience init(row: SearchResultRow) {
        self.init()
        self.id = row.id
        self.name = row.title
        self.username = row.title
        self.profileImageUrl = row.profileImageUrl
        self.profileDescription = row.footer ?? ""
    }
}

private extension PixSearchItem {
    convenience init(row: SearchResultRow) {
        let service = PixServiceType(rawValue: row.legacyTags?.service ?? "") ?? .hub
        let name = row.title
        let description = row.body
        let imageUrl = row.profileImageUrl ?? ""
        self.init(name: name, descriptionText: description, imageUrl: imageUrl, service: service)
    }
}

private extension P2PItem {
    convenience init(row: SearchResultRow) {
        let name = row.title
        let description = row.body
        let imageUrl = row.profileImageUrl ?? ""
        let service = P2PItem.ServiceType(rawValue: row.legacyTags?.service ?? "") ?? .unknown
        self.init(name: name, descriptionText: description, imageUrl: imageUrl, service: service)
    }
}
