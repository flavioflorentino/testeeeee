struct SearchResultSection: Decodable {
    let sectionTitle: String
    let rows: [SearchResultRow]
    let totalCount: Int
}

struct SearchResultRow: Decodable {
    let type: SearchType
    let id: String
    let title: String
    let body: String
    let footer: String?
    let profileImageUrl: String?
    let profileTags: SearchProfileTags
    let legacyTags: SearchLegacyTags?
}

struct SearchProfileTags: Decodable {
    let isVerified: Bool
    let isPro: Bool
    let isStudent: Bool
}

struct SearchLegacyTags: Decodable {
    let isCielo: Bool
    let isParking: Bool
    let service: String
}

enum SearchType: String, Decodable {
    case consumer
    case store
    case digitalGood = "digital_good"
    case membership
    case p2m
    case p2p
    case pix
    case financial = "financial_service"
}
