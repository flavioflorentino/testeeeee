import Foundation

extension LegacySearchResultSection {
    convenience init(section: SearchResultSection) {
        self.init(viewType: .list, rows: section.rows.map { LegacySearchResultRow(row: $0) })
        self.title = section.sectionTitle
        self.hasMore = section.rows.count < section.totalCount
    }
}
