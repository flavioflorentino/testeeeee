import Core
import FeatureFlag
import Foundation
import Alamofire

extension SearchApi {
    func newSearch(request: SearchRequest,
                   cacheKey: String,
                   onCacheLoaded: @escaping ((SearchApiResponse) -> Void),
                   completion: @escaping ((PicPayResult<SearchApiResponse>) -> Void)) -> DataRequest? {
        let endpoint = "searchable"
        let expiryTime = FeatureManager.number(.searchResultCache)?.doubleValue

        do {
            let dataRequest = try requestManager.apiRequestWithCache(cacheKey,
                                                                     expiryTime: expiryTime,
                                                                     onCacheLoaded: onCacheLoaded,
                                                                     endpoint: endpoint,
                                                                     method: .get,
                                                                     parameters: request.toParams())
            let handler: (PicPayResult<[SearchResultSection]>) -> Void = { result in
                switch result {
                case .success(let items):
                    let sections = items.map(LegacySearchResultSection.init)
                    let response = BaseApiListResponse<LegacySearchResultSection>()
                    response.list = sections
                    completion(.success(response))
                case .failure(let error):
                    completion(.failure(error))
                }
            }

            return dataRequest.responseApiCodable(completionHandler: handler)
        } catch {
            completion(.failure(PicPayError.generalError))
            return nil
        }
    }
}
