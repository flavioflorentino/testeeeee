enum SearchLocalizable: String, Localizable {
    case all
    case people
    case places
    case store
    case recentSearch
    case establishmentAlertTitle
    case establishmentAlertText
    case contactAndLocationPermissonTitle
    case contactAndLocationPermissionText
    case authorize
    case locationPermissionTitle
    case locationPermissionText
    case paymentRequestDisabled
    case noConnectionErrorTitle
    case noConnectionErrorDescription
    case generalErrorTitle
    case generalErrorDescription
    case emptyStateTitle
    case emptyStateDescription
    case tryAgain
    case waitingInputTitle
    case waitingInputDescription

    var key: String {
        return rawValue
    }
    
    var file: LocalizableFile {
        return .search
    }
}
