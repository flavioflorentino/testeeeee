import AnalyticsModule

enum PhoneNumberEvent: AnalyticsKeyProtocol {
    case change
    
    private var name: String {
        switch self {
        case .change:
            return "Alteração de Telefone verificação SMS"
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}
