import AnalyticsModule

enum ExperimentEvent: AnalyticsKeyProtocol {
    case walletAccessedByBalance
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

private extension ExperimentEvent {
    var name: String {
        switch self {
        case .walletAccessedByBalance:
            return "Wallet Accessed By Balance"
        }
    }

    var properties: [String: Any] { [:] }
    var providers: [AnalyticsProvider] { [.mixPanel] }
}
