import AnalyticsModule
import Foundation

enum IncentiveModalEvent: AnalyticsKeyProtocol {
    case pay
    case notNow
    
    private var name: String {
        switch self {
        case .pay:
            return "Pay"
        case .notNow:
            return "Not Now"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("MGM - APP - Conversion Modal - \(name)", providers: providers)
    }
}
