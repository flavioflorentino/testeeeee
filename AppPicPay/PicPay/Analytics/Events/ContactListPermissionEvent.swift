import AnalyticsModule

enum ContactsListPermissionTypes {
    enum Action: String {
        case notNow = "not now"
        case authorize
    }
    
    enum Origin: String {
        case recharge
        case onboarding
    }
}

enum ContactListPermissionEvent: AnalyticsKeyProtocol {
    case contactsPermission(_ action: ContactsListPermissionTypes.Action, _ origin: ContactsListPermissionTypes.Origin)
    
    private var name: String {
        "Contacts Permission"
    }
    
    private var properties: [String: Any] {
        if case let .contactsPermission(action, origin) = self {
            return [
                "action": action.rawValue,
                "origin": origin.rawValue
            ]
        }
        return [:]
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
