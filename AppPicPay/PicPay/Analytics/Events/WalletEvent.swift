import AnalyticsModule
import Foundation

enum WalletEvent: AnalyticsKeyProtocol {
    case didTapAddMoneyButton
    case didTapWithdrawalButton
    case incomeAccessed(type: IncomeAccessType)
    case balanceVisibility(isVisible: Bool)
    case paymentChanged(type: PaymentMethodType)
    
    enum IncomeAccessType: String {
        case button
        case value
    }
    
    enum PaymentMethodType: String {
        case balance = "saldo"
        case creditCard = "cartão"
    }
    
    private var name: String {
        switch self {
        case .didTapAddMoneyButton:
            return "tocou em adicionar"
        case .didTapWithdrawalButton:
            return "tocou em retirar"
        case .incomeAccessed(let type):
            return "Income Accessed by \(type.rawValue)"
        case .balanceVisibility:
            return "Balance Visibility Changed"
        case .paymentChanged:
            return "Wallet Changed"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .balanceVisibility(let isVisible):
            return ["visible_balance": isVisible]
        case .paymentChanged(let type):
            return ["preferred_method": type.rawValue]
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        return [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .paymentChanged:
            return AnalyticsEvent(name, properties: properties, providers: providers)
        default:
            return AnalyticsEvent("Carteira - \(name)", properties: properties, providers: providers)
        }
    }
}
