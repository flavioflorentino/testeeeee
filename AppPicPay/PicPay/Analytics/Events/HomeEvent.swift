import AnalyticsModule
import Foundation

enum HomeEvent: AnalyticsKeyProtocol {
    case invitePeople
    case settingsAccessed
    case segmentSelection(_ type: TabActivityType)
    case tabActivitiesSelected(type: TabActivityType)
    case screenViewed
    
    enum TabActivityType: String {
        case all = "TODAS"
        case me = "MINHAS"
        case suggestions = "sugestoes"
        case favorites = "favoritos"
    }
    
    private var name: String {
        let prefix = "Inicio - "
        switch self {
        case .invitePeople:
            return "\(prefix)Invite-People Accessed"
        case .segmentSelection:
            return "\(prefix)Home Tab Selected"
        case .tabActivitiesSelected:
            return "\(prefix)Tab-Activities Selected"
        case .settingsAccessed:
            return "\(prefix)Settings Accessed"
        case .screenViewed:
            return "Screen Viewed"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .tabActivitiesSelected(let type):
            return  ["tab": type.rawValue]
        case let .segmentSelection(type):
            return ["tab": type.rawValue]
        case .screenViewed:
            return [
                "screen_name": "Home Screen",
                "business_context": "INICIO"
            ]
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        switch self {
        case .screenViewed:
            return [.eventTracker]
        default:
            return [.mixPanel, .firebase]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
