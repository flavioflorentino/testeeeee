import AnalyticsModule

enum QRCodeEvent: AnalyticsKeyProtocol {
    case didReadCode(code: String)
    case pullUpMyCode
    case tapShareCode
    case didShareCode(type: ShareCodeType)
    
    enum ShareCodeType: String {
        case image = "Imagem"
        case link = "Link de cobrança"
    }
    
    private var name: String {
        switch self {
        case .didReadCode:
            return "código lido"
        case .pullUpMyCode:
            return "meu código"
        case .tapShareCode:
            return "tocou em compartilhar"
        case .didShareCode:
            return "compartilhou"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .didReadCode(let code):
            return ["Código": code]
        case .didShareCode(let type):
            return ["Tipo": type.rawValue]
        default:
            return [:]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("QR code - " + name, properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}
