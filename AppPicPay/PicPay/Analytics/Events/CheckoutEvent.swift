import AnalyticsModule

enum CheckoutEvent: AnalyticsKeyProtocol {
    enum BalanceOption: String {
        case optin = "opt in"
        case optout = "opt out"
    }
    
    enum CheckoutEventOrigin: String {
        case p2p
        case pav
        case p2m
        case linx
        case bluezone
        case subscription
        case invoice
        case birthday
        case recharge
        case ecommerce
        case bills
        case parking
        case phoneRecharge
        case transitPass
        case tvRecharge
        case voucher
        case friendGift
        case cardVerification
        case newPayment
        
        var eventName: String {
            switch self {
            case .p2m,
                 .ecommerce:
                return "store"
            case .bluezone:
                return "parking"
            case .birthday,
                 .friendGift:
                return "gift"
            case .recharge,
                 .phoneRecharge,
                 .tvRecharge,
                 .voucher:
                return "digitalgoods"
            case .bills:
                return "bills"
            case .transitPass:
                return "transportpass"
            case .cardVerification:
                return "credit"
            case .newPayment:
                return "p2p"
            default:
                return self.rawValue
            }
        }
    }
    
    case checkoutScreenViewed(_ origin: CheckoutEventOrigin)
    case addNewCardAccessed(_ origin: PaymentMethodsContext)
    case mainCreditCardChanged(_ origin: PaymentMethodsContext)
    case useBalanceChanged(_ opt: BalanceOption, _ origin: PaymentMethodsContext)
    case checkoutPaymentMethodAccessed
    
    private var name: String {
        switch self {
        case .checkoutScreenViewed:
            return "Checkout Screen Viewed"
        case .addNewCardAccessed:
            return "Add New Card Accessed"
        case .mainCreditCardChanged:
            return "Main Credit Card Changed"
        case .useBalanceChanged:
            return "Use Balance Changed"
        case .checkoutPaymentMethodAccessed:
            return "Checkout Payment Method Accessed"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .checkoutScreenViewed(origin):
            return ["origin": origin.eventName]
        case let .mainCreditCardChanged(origin),
             let .addNewCardAccessed(origin):
            return ["origin": origin.text]
        case let .useBalanceChanged(opt, origin):
            return [
                "action": opt.rawValue,
                "origin": origin.text
            ]
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        return [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
