import AnalyticsModule

enum ProfileEvent: AnalyticsKeyProtocol {
    case touchPicture(from: PictureScreen)
    case accessed(type: ProfileType)
    case profileOpened(userID: String)
    
    enum PictureScreen: String {
        case feed
        case feedDetail = "feed detalhe"
        case pay = "pagar"
        case paySuggestions = "pagar sugestoes"
        case suggestions = "sugestoes"
        case favorites = "favoritos"
    }
    
    enum ProfileType: String {
        case regular
        case university
        case pro
        case verified
    }
    
    enum SocialUserInteractionType: String {
        case viewedProfile = "VISUALIZAR_PERFIL"
    }
    
    enum TargetUserType: String {
        case consumer = "CONSUMER"
    }
    
    private var name: String {
        switch self {
        case .touchPicture:
            return "pic touch"
        case .accessed:
            return "Accessed"
        case .profileOpened:
            return "User Social Interacted"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .touchPicture(from: let screenName):
            return ["screen name": screenName.rawValue]
        case .accessed(type: let type):
            return ["profile type": type.rawValue]
        case let .profileOpened(userID):
            return [
                "social_user_interaction_type": SocialUserInteractionType.viewedProfile.rawValue,
                "target_user_id": userID,
                "target_user_type": TargetUserType.consumer.rawValue
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        switch self {
            case .profileOpened:
                return [.eventTracker]
            default:
                return [.firebase, .mixPanel]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent("Profile \(name)", properties: properties, providers: providers)
    }
}
