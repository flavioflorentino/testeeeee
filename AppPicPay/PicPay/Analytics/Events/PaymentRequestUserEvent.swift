import AnalyticsModule

enum PaymentRequestUserEvent: AnalyticsKeyProtocol {
    case start(origin: String)
    case didOpenHub
    case didRequestLimitExceeded
    case didTapRequestLimitExceededKnowMore
    case didRequestLimitExceededRestart
    case didStartValueScreen(origin: String)
    case didTapMethodCharge(PaymentRequestUserMethod)
    case didNotInformValue
    case didInformValue
    case didTapInformValue
    case didTapSendCharge
    case didTapValue
    case shared
    case didOpenSearchScreen
    case didTapSearchField
    case didSelectUser(Int)
    case selectedUsers(ids: [String])
    case didSelectUserAvatar
    case didOpenPaymentRequestScreen
    case didOpenPaymentDetailScreen(origin: String)
    case didTapOnPrivacy
    case didTapSendPicPayCharge(value: Double, hasMessage: Bool)
    case picpayPaymentRequestSuccess
    case picpayPaymentRequestError
    case didAccessNotification
    case privacyChange(origin: PaymentRequestSettingOrigin, isOn: Bool)
    case notificationChange(origin: PaymentRequestSettingOrigin, isOn: Bool)
    case notAllowed
    case feedItemViewed
    case paidDetail
    
    enum PaymentRequestUserMethod: String {
        case picpay = "via_picpay"
        case qrcode = "via_qr_code"
        case link = "via_link"
        case pix = "via_pix"
    }
    
    enum PaymentRequestSettingOrigin: String {
        case adjustments = "Ajustes"
    }
    
    enum PaymentRequestDetailOrigin: String {
        case userListing = "user_listing"
        case userProfile = "user_profile"
    }
    
    private var name: String {
        switch self {
        case .start:
            return "Started"
        case .didOpenHub:
            return "Hub Viewed"
        case .didRequestLimitExceeded:
            return "Limit Exceeded"
        case .didTapRequestLimitExceededKnowMore:
            return "Limit Exceeded FAQ"
        case .didRequestLimitExceededRestart:
            return "Limit Exceeded Restart"
        case .didStartValueScreen:
            return "Value Started"
        case .didTapMethodCharge:
            return "Method Selected"
        case .didNotInformValue:
            return "Value Not Informed"
        case .didInformValue:
            return "Value Informed"
        case .didTapInformValue:
            return "Value Confirmed"
        case .didTapSendCharge:
            return "Send Request"
        case .didTapValue:
            return "Value Edition"
        case .shared:
            return "Shared"
        case .didOpenSearchScreen:
            return "User Listing Viewed"
        case .didTapSearchField:
            return "Search Activated"
        case .didSelectUserAvatar:
            return "Avatar Selected"
        case .didSelectUser:
            return "User Selected"
        case .selectedUsers:
            return "User Selected"
        case .didOpenPaymentRequestScreen,
             .didOpenPaymentDetailScreen:
            return "Detail Viewed"
        case .didTapOnPrivacy:
            return "Privacy Opened"
        case .didTapSendPicPayCharge:
            return "Sent"
        case .picpayPaymentRequestSuccess:
            return "Success"
        case .picpayPaymentRequestError:
            return "Error"
        case .didAccessNotification:
            return "Notification Accessed"
        case .privacyChange:
            return "Privacy Change"
        case .notificationChange:
            return "Notification Change"
        case .notAllowed:
            return "Not Allowed"
        case .feedItemViewed:
            return "Feed Item Viewed"
        case .paidDetail:
            return "Paid Detail"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .start(origin),
             let .didStartValueScreen(origin),
             let .didOpenPaymentDetailScreen(origin):
            return ["Origem": origin]
        case let .didTapMethodCharge(method):
            return ["Method": method.rawValue]
        case let .didSelectUser(userId):
            return ["user_id": userId]
        case let .selectedUsers(userIds):
            return ["user_id": userIds, "quantity": userIds.count]
        case let .didTapSendPicPayCharge(value, hasMessage):
            return ["request_value": value, "has_message": hasMessage]
        case let .privacyChange(origin, isOn), let .notificationChange(origin, isOn):
            return [
                "origin": origin.rawValue,
                "allows_request": isOn,
            ]
        default:
            return [:]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent("Payment Request \(name)", properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}
