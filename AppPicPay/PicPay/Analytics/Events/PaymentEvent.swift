import AnalyticsModule

enum ExceededPopupField: String {
    case button
    case none
}

enum ExceededPopupAction: String {
    case acceptFee = "accept_fee"
    case notNow = "not_now"
    case close
    case none
}

enum PaymentEvent: AnalyticsKeyProtocol {
    case transaction(_ type: TransactionType, id: String, value: Decimal)
    
    case itemAccessed(type: String, id: String?, name: String?, section: (name: String, position: Int))
    case itemPopupAccessed(type: PaymentItemType, origin: PaymentItemOrigin)
    case itemPopupOpened(name: String)
    case transactionExceededLimit(buttonClicked: Bool, selectedField: ExceededPopupField, action: ExceededPopupAction, newExperience: Bool = false)
    case userNavigated(from: ViewLocation, to: ViewLocation, consumerId: String, timestamp: String)
    
    private var name: String {
        switch self {
        case .transaction:
            return "transaction"
        case .itemAccessed:
            return "Payment Item Accessed"
        case .itemPopupAccessed:
            return "Payment Item Popup Accessed"
        case .itemPopupOpened:
            return "Payment Item Popup Opened"
        case .transactionExceededLimit:
            return "Dialog Interacted"
        case .userNavigated:
            return "User Navigated"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .transaction(type, id, value):
            return [
                "type": type.description,
                "transaction_id": id,
                "transaction_value": value
            ]
        case let .itemAccessed(type, id, name, section):
            return [
                "type": type.lowercased(),
                "item_name": name?.lowercased() ?? "",
                "id": id ?? "",
                "section": section.name.lowercased(),
                "section_position": section.position
            ]
        case let .itemPopupAccessed(type, origin):
            return [
                "origin": origin.rawValue,
                "type": type.rawValue
            ]
        case let .itemPopupOpened(name):
            return [
                "item_name": name.lowercased()
            ]
        case let .transactionExceededLimit(clicked, selectedField, action, newExperience):
            return [
                "is_clicked": clicked,
                "selected_field": selectedField.rawValue,
                "action": action.rawValue,
                "title": "Limite de pagamento via cartão de crédito",
                "location": "after the transaction exceeds this limit the fee payment",
                "new_experience": newExperience
            ]
        case let .userNavigated(from, to, consumerId, timestamp):
            return [
                "from": from.rawValue,
                "to": to.rawValue,
                "user_id": consumerId,
                "timestamp": timestamp
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        switch self {
        case .itemAccessed, .itemPopupAccessed, .itemPopupOpened:
            return [.mixPanel, .firebase]
        default:
            return [.mixPanel, .firebase, .appsFlyer]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

extension PaymentEvent {
    enum ViewLocation: String {
        case buttonInstallment = "button installment"
        case modalInstallmentBalance = "modal installment balance"
        case modalFilledBalance = "modal filled balance"
        case modalInstallmentCreditcard = "modal installment creditcard"
        case screenPaymentMethod = "screen payment method"
        case screenTransaction = "screen transaction"
        case buttonChangePaymentMethod = "button change payment method"
        case buttonNotNow = "button not now"
        case buttonOk = "button ok"
    }
    
    enum TransactionType: String, CustomStringConvertible {
        case digitalGoods
        case ecommerce
        case membership
        case parking
        case p2m
        case p2p
        
        var description: String {
            return self == .digitalGoods ? "digital goods" : self.rawValue
        }
    }
    
    enum PaymentItemType: String {
        case biz
        case p2m
    }
    
    enum PaymentItemOrigin: String {
        case photo
        case map
    }
}
