import AnalyticsModule

enum UserEvent: AnalyticsKeyProtocol {
    case open
    case register
    case tabSelected(type: TabItemType)
    case qrCodeAccessed(type: QrCodeAccessType, origin: TabItemType)
    case darkModeOn(_ isDarkModeOn: Bool)
    
    enum QrCodeAccessType: String {
        case click = "CLICK"
        case swipe = "SWIPE"
    }
    
    enum TabItemType: String {
        case home = "INICIO"
        case wallet = "CARTEIRA"
        case payment = "PAGAR"
        case notifications = "NOTIFICACOES"
        case settings = "AJUSTES"
        case store = "STORE"
    }
    
    private var name: String {
        switch self {
        case .open:
            return "open_application"
        case .register:
            return "register_user"
        case .tabSelected:
            return "Tabbar - Tab Selected"
        case .qrCodeAccessed:
            return "Qr Code Accessed"
        case .darkModeOn:
            return "Dark Mode On"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .tabSelected(type):
            return ["tab": type.rawValue]
        case let .qrCodeAccessed(type, origin):
            return ["method": type.rawValue, "origin": origin.rawValue]
        case let .darkModeOn(isDarkModeOn):
            return ["is_dark_mode": isDarkModeOn]
        default:
            return [:]
        }
    }

    private var providers: [AnalyticsProvider] {
        switch self {
        case .open:
            return[.appsFlyer]
        case .tabSelected, .qrCodeAccessed:
            return [.mixPanel, .firebase]
        default:
            return [.mixPanel, .firebase, .appsFlyer]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
