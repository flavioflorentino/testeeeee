import AnalyticsModule
import Foundation

enum ShareEvent: AnalyticsKeyProtocol {
    case invite(type: InviteCodeType)
    
    enum InviteCodeType: String {
        case system = "SYSTEM"
        case whatsapp = "WHATSAPP"
        case facebook = "FACEBOOK"
        case twitter = "TWITTER"
        case email = "EMAIL"
        case clipboard = "CLIPBOARD"
    }
    
    private var name: String {
        switch self {
        case .invite:
            return "Convidar - Sharable link Shared"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .invite(let type):
            return ["method": type.rawValue]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        return [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
