import AnalyticsModule
import Foundation

enum StoreEvent: AnalyticsKeyProtocol {
    case didTapBanner(name: String)
    case didTapSeeAllOnCategory(name: String, id: String, position: Int)
    
    private var name: String {
        switch self {
        case .didTapBanner:
            return "banner store"
        case .didTapSeeAllOnCategory:
            return "Store Category Item Accessed"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .didTapBanner(name):
            return ["banner": name]
        case let .didTapSeeAllOnCategory(name, id, position):
            return ["category_name": name, "category_id": id, "category_order": position]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        return [.mixPanel, .firebase, .appsFlyer]
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
