import AnalyticsModule
import Foundation

enum StudentAccountEvent: AnalyticsKeyProtocol {
    case shownSubmissionScreen
    case sentDocuments
    case tookSelfie
    case attachedDocument(from: AttachmentChannel)
    case university
    case status(reason: String)
    case activatedAccount
    case activatedAccountPopup
    case activatedAccountPopupClose
    case activatedAccountPopupDismiss
    case rules
    case benefits(from: BenefitsOrigin)
    case universitySelected(name: String, code: String)
    case waitingAdjust
    case view(screen: ScreenName, origin: OfferWebviewOrigin)
    case profileOffer
    case signUp
    case signUpConfirm
    case signUpNotNow
    
    enum BenefitsOrigin: String {
        case settings = "Ajustes"
        case activatedAccount = "Tela conta ativada"
        case popup = "popup onboarding"
    }
    
    enum AttachmentChannel: String {
        case camera = "Câmera"
        case library = "Escolheu na biblioteca"
    }
    
    enum ScreenName: String {
        case offerWebview = "oferta webview"
    }
    
    enum OfferWebviewOrigin: String {
        case registerPromoCodeSignUp = "cadastro codigo inicial"
        case registerPromoCode = "cadastro codigo"
        case walletPromoCode = "carteira codigo"
        case registerDeeplink = "cadastro contexto"
        case settings = "ajustes"
        case deeplink = "deeplink"
        case profile = "perfil"
    }
    
    private var name: String {
        switch self {
        case .shownSubmissionScreen:
            return "Tela de comprovante"
        case .sentDocuments:
            return "Enviou os comprovantes"
        case .tookSelfie:
            return "Tirou selfie"
        case .attachedDocument:
            return "Anexou comprovante"
        case .university:
            return "Tela cadastro de universidade"
        case .universitySelected:
            return "Universidade cadastrada"
        case .status:
            return "Tela de status"
        case .activatedAccount:
            return "Tela Conta Ativada"
        case .waitingAdjust:
            return "Informações pendentes - tocou em corrigir"
        case .activatedAccountPopup:
            return "popup onboarding"
        case .activatedAccountPopupClose:
            return "popup fechar"
        case .activatedAccountPopupDismiss:
            return "popup agora nao"
        case .rules:
            return "Tela Regras"
        case .benefits:
            return "Tela Benefícios"
        case .view:
            return "screenview"
        case .profileOffer:
            return "Profile cta offer"
        case .signUp:
            return "Student Sign Up Screen Viewed"
        case .signUpConfirm:
            return "Student Sign Up Confirm Button Clicked"
        case .signUpNotNow:
            return "Student Sign Up Not Now Button Clicked"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .attachedDocument(let channel):
            return  ["Canal": "\(channel.rawValue)"]
        case .universitySelected(let name, let code):
            return ["Nome da Universidade": name, "Code": code]
        case .status(let reason):
            return ["Status": reason]
        case .benefits(let origin):
            return ["Origem": origin.rawValue]
        case .view(let screen, let origin):
            return [
                "nome da tela": screen.rawValue,
                "origem": origin.rawValue
            ]
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        return [.mixPanel, .firebase, .appsFlyer]
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent("UA - \(name)", properties: properties, providers: providers)
    }
}
