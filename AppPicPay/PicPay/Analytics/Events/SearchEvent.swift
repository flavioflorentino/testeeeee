import AnalyticsModule
import FeatureFlag
import Foundation

enum SearchEvent: AnalyticsKeyProtocol {

    case searchAccessed(_ newLayout: Bool = false, _ newLabel: Bool = false)
    case searchResultViewed(isEmpty: Bool, query: String, tab: String)
    case tabSelected(tab: String)
    case firstScreenViewed(_ paymentScreenTab: PaymentTabIdentifier?)
    case searchStarted(id: UUID, tabName: String, origin: SearchOrigin)
    case resultsViewed(id: UUID, query: String, isEmpty: Bool, tabName: String)
    case resultInteracted(id: UUID, query: String, tab: String, section: String? = nil, pos: Int, itemId: String?, type: InteractionType)
    case resultError(id: UUID, tab: String, query: String, type: SearchViewModel.ErrorType)
    case errorReload(id: UUID, tab: String, query: String, type: SearchViewModel.ErrorType)

    enum InteractionType: String {
        case pictureTouch = "PICTURE_TOUCH"
        case optionsTouch = "OPTIONS_TOUCH"
        case titleTouch = "TITLE_TOUCH"
    }

    enum SearchOrigin: String {
        case home
        case pay = "pagar"
        case places
        case store
        case deeplink
    }

    private var name: String {
        switch self {
        case .searchAccessed:
            return "Pagar - Search Accessed"
        case .searchResultViewed:
            return "Pagar - Search Result Viewed"
        case .tabSelected:
            return "Pagar - Tab Selected "
        case .firstScreenViewed:
            return "First Screen Viewed"
        case .searchStarted:
            return "Search Started"
        case .resultsViewed:
            return "Search Results Viewed"
        case .resultInteracted:
            return "Search Result Interacted"
        case .resultError:
            return "Search Result Error"
        case .errorReload:
            return "Search Error Reload"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .searchResultViewed(isEmpty, query, tab):
            return ["is_empty": isEmpty, "query": query, "tab": tab.uppercased(), "section": "search"]
        case .tabSelected(let tab):
            return ["tab": tab.uppercased()]
        case let .firstScreenViewed(paymentScreenTab):
            return [
                "tab": "pay",
                "sub_tab": paymentScreenTab?.rawValue ?? ""
            ]
        case let .searchAccessed(newLayout, newLabel):
            return [
                "section": "search",
                "is_experiment_home": FeatureManager.shared.isActive(.experimentSearchHomeBool),
                "is_new_layout": newLayout,
                "is_new_label": newLabel
            ]
        case let .searchStarted(id: id, tabName: tabName, origin: origin):
            return [
                "section": getSection(tabName: tabName),
                "search_query_id": id.uuidString,
                "search_api_version": getSearchEngineVersion(for: tabName),
                "experiment_variant": SearchExperiment(tabName: tabName).rawValue,
                "origin": origin.rawValue
            ]
        case let .resultsViewed(id: id, query: query, isEmpty: isEmpty, tabName: tabName):
            return [
                "section": getSection(tabName: tabName),
                "search_query_id": id.uuidString,
                "search_api_version": getSearchEngineVersion(for: tabName),
                "query": query,
                "is_empty": isEmpty,
                "experiment_variant": SearchExperiment(tabName: tabName).rawValue
            ]
        case let .resultInteracted(id: id, query: query, tab: tab, section: section, pos: pos, itemId: itemId, type: type):
            return [
                "section": getSection(tabName: tab, sectionName: section),
                "search_query_id": id.uuidString,
                "search_api_version": getSearchEngineVersion(for: tab),
                "query": query,
                "result_item_id": itemId ?? "",
                "item_position": pos,
                "interaction_type": type.rawValue,
                "experiment_variant": SearchExperiment(tabName: tab).rawValue
            ]
        case let .resultError(id, tab, query, type), let .errorReload(id, tab, query, type):
            return [
                "section": getSection(tabName: tab, sectionName: nil),
                "search_query_id": id.uuidString,
                "search_api_version": getSearchEngineVersion(for: tab),
                "query": query,
                "error_type": type.analyticsValue,
                "experiment_variant": SearchExperiment(tabName: tab).rawValue
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        return [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent(name, properties: properties, providers: providers)
    }

    private func getSection(tabName: String, sectionName: String? = nil) -> String {
        var section: String = "\(tabName)"
        if let sectionName = sectionName, sectionName.isNotEmpty {
            section += " - \(sectionName)"
        }
        return section.lowercased()
    }

    private func getSearchEngineVersion(for tabName: String) -> String {
        let experiment = SearchExperiment(tabName: tabName)
        return experiment == .legacyControl ? SearchExperiment.legacy.rawValue : experiment.rawValue
    }
}

extension SearchViewModel.ErrorType {
    var analyticsValue: String {
        switch self {
        case .noConnection:
            return "connection"
        case .generalError(let code):
            return code
        }
    }
}
