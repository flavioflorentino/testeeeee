import AnalyticsModule

enum LocationPermissionEvent: AnalyticsKeyProtocol {
    case locationPermission(_ action: String, _ origin: String)
    
    private var name: String {
        "Location Permission"
    }
    
    private var properties: [String: Any] {
        if case let .locationPermission(action, origin) = self {
            return [
                "action": action,
                "origin": origin
            ]
        }
        return [:]
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
