import AnalyticsModule

enum SettingsEvent: AnalyticsKeyProtocol {
    case settingsHomeButtonViewed
    case itemSelected(_ item: SettingsItem)
    case mySubscriptions
    
    enum SettingsItem: String {
        case studentAccount = "conta universitaria"
        case profile = "perfil"
    }
    
    private var name: String {
        switch self {
        case .itemSelected:
            return "Ajustes item"
        case .settingsHomeButtonViewed:
            return "Ajustes Settings Home Button Viewed"
        case .mySubscriptions:
            return "Settings - View My Subscriptions Accessed"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .itemSelected(let item):
            return  ["item ajustes": item.rawValue]
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        return [.firebase, .mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
