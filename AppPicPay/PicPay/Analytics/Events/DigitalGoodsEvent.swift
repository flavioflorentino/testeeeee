import AnalyticsModule
import Foundation

enum DigitalGoodsEvent: AnalyticsKeyProtocol {
    case reminderDenied(type: DigitalGoodsType)
    case reminderDismissed(type: DigitalGoodsType)
    case reminderConfirmed(period: String, type: DigitalGoodsType, time: DigitalGoodsTime)
    
    enum DigitalGoodsType: String {
        case mobileRecharge = "Recarga de Celular"
        case singleTicket = "Bilhete Único"
    }
    
    enum DigitalGoodsTime: String {
        case day = "DAY"
        case month = "MONTH"
    }
    
    private var name: String {
        switch self {
        case .reminderDenied:
            return "Reminder Denied"
        case .reminderDismissed:
            return "Reminder Dismissed"
        case .reminderConfirmed:
            return "Reminder Confirmed"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .reminderDenied(let type), .reminderDismissed(let type):
            return ["type": type.rawValue]
        case let .reminderConfirmed(period, type, time):
            return ["period": period, "type": type.rawValue, "time_unit": time.rawValue]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        return [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent("DG - \(name)", properties: properties, providers: providers)
    }
}
