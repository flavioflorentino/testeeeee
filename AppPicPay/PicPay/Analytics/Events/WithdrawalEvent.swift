import AnalyticsModule

enum WithdrawalEvent: AnalyticsKeyProtocol {
    case didTapWithdrawalValueHelp
    case didTapWithdrawalOption(WithdrawType, pending: Bool)
    case didStartBankAccountRegistration
    case didSaveNewBankAccount(isAccountHolder: Bool)
    case didShowEstimatedConclusion
    case didShowConcluded
    
    private var title: String {
        switch self {
        case .didSaveNewBankAccount:
            return ""
        default:
            return "Saque - "
        }
    }
    
    private var name: String {
        switch self {
        case .didTapWithdrawalValueHelp:
            return "tocou no ícone de dúvida"
        case .didTapWithdrawalOption:
            return "opção escolhida"
        case .didStartBankAccountRegistration:
            return "iniciou cadastro de conta"
        case .didShowEstimatedConclusion:
            return "entrou na tela de previsão"
        case .didShowConcluded:
            return "concluído"
        case .didSaveNewBankAccount:
            return "Nova Conta Bancária"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .didTapWithdrawalOption(let option, let pending):
            return ["Tipo de saque": option.analytticsDescription, "Em andamento": pending]
        case .didSaveNewBankAccount(let isAccountHolder):
            return ["Titular da conta": isAccountHolder]
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        return [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent(title + name, properties: properties, providers: providers)
    }
}

extension WithdrawType {
    var analytticsDescription: String {
        switch self {
        case .transfer:
            return "Transferência bancária"
        case .bank24hours:
            return "Saque24horas"
        case .original:
            return "Saque Original"
        default:
            return ""
        }
    }
}
