import AnalyticsModule

enum FeedEvent: AnalyticsKeyProtocol {
    case didAppear(numberOfActivies: Int, opportunitiesActive: Bool)
    
    enum FeedScrollDirection {
        case up
        case down
    }
    
    private var name: String {
        return "Inicio - Feed Viewed"
    }
    
    private var properties: [String: Any] {
        guard case let .didAppear(numberOfActivies, opportunitiesActive) = self else {
            return [:]
        }
        
        return [
            "number_activities": numberOfActivies,
            "opportunities_active": opportunitiesActive ? "TRUE" : "FALSE"
        ]
    }
    
    private var providers: [AnalyticsProvider] {
        return [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

extension FeedItemVisibility {
    var name: String {
        switch self {
        case .Friends:
            return "TODOS"
        case .Private:
            return "MINHAS"
        }
    }
}
