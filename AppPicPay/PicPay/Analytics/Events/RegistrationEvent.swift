import AnalyticsModule

enum RegistrationEvent: AnalyticsKeyProtocol {
    case startScreenViewed(dynamicRegistrations: Bool, registrationSteps: [RegistrationStep])
    case name
    case phone
    case phoneCode
    case phoneConfirmation(_ type: PhoneConfirmationOptionType)
    case phoneCodeVerification
    case smsConfirmation(_ type: SmsConfirmationOptionType)
    case codeError
    case verificationOption(_ type: VerificationOptionType)
    case documentHelp
    case documentError(_ type: DocumentErrorType)
    case email
    case password
    case cpf
    case documentInUse
    case documentModalViewed
    case documentModalOptionSelected(_ option: DocumentInUseOptionType)
    case registerDocument
    case receiveCodeOption(_ type: ReceiveCodeOptionType)
    case receiveCodeWhatsAppFlow(_ active: Bool)
    case validationCode(_ type: ValidationCodeOptionType, action: ValidationCodeActionType?)
    case continuedScreenViewed
    case continuedScreenOptionSelected(_ option: ContinuedScreenOptionType)
    case finish
    case restoringCachedRegistrationError
    case touchPromoCode
    case validatePromoCode(_ code: String)
    case promoCodeError(_ error: String)
    case registerUsername(attempts: Int, withSuggestion: Bool)
    
    private var name: String {
        switch self {
        // Start screen
        case .startScreenViewed:
            return "Register - Screen Viewed"
            
        // Promo code
        case .validationCode:
            return "Validation Code - Screen Viewed"
            
        // Name
        case .name:
            return "cadastro_nome"
            
        // Phone number
        case .phone:
            return "cadastro_telefone"
        case .receiveCodeWhatsAppFlow:
            return "Receive Code - Whatsapp Flow"
        case .receiveCodeOption:
            return "Receive Code - Option Selected"
            
        // Phone code
        case .phoneCode:
            return "cadastro_validacao_sms"
        case .phoneConfirmation:
            return "Phone Registration - Confirmation"
        case .phoneCodeVerification:
            return "Cadastro verificação SMS"
        case .codeError:
            return "Cadastro Verificação SMS - Erro"
        case .verificationOption:
            return "Cadastro Verificação SMS - Opção Escolhida"
        
        case .smsConfirmation:
            return "Sms Confirmation - Option Selected"
            
        // Email
        case .email:
            return "cadastro_email"
            
        // Password
        case .password:
            return "cadastro_senha"
            
        // Document
        case .cpf:
            return "cadastro_cpf"
        case .documentHelp:
            return "Exibiu Ajuda do CPF"
        case .documentError:
            return "Cadastro CPF - Erro"
        case .registerDocument:
            return "Cadastro CPF"
        case .documentInUse:
            return "Sign Up CPF - In Use"
        case .documentModalViewed:
            return "Sign Up CPF Pop Up - Viewed"
        case .documentModalOptionSelected:
            return "SignUp CPF Pop Up - Option Selected"
            
        // Adding new user
        case .finish:
            return "cadastro_conclusao_cadastro"
            
        // Continue
        case .continuedScreenViewed:
            return "Continued Screen - Viewed"
        case .continuedScreenOptionSelected:
            return "Continued Screen - Option Selected"
        case .restoringCachedRegistrationError:
            return "Continued Screen - Order Error"
            
            
        // Username
        case .touchPromoCode:
            return "Tocou em código promocional"
        case .validatePromoCode:
            return "Inseriu código promocional"
        case .promoCodeError:
            return "Inseriu código promocional - erro"
        case .registerUsername:
            return "Cadastrou username"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .startScreenViewed(dynamicRegistrations, registrationSteps):
            return [
                "new_screen": dynamicRegistrations.description,
                "order": registrationSteps.map { $0.description }
            ]
        case .phoneConfirmation(let type):
            return ["option_selected": type.rawValue]
        case .verificationOption(let type):
            return ["opcao_escolhida": type.rawValue]
        case .documentError(let type):
            return ["motivo": type.rawValue]
        case .receiveCodeOption(let type):
            return ["option_selected": type.rawValue]
        case .receiveCodeWhatsAppFlow(let active):
            return ["receive_code_with_whats_app": active.description]
        case .smsConfirmation(let type):
            return ["option_selected": type.rawValue]
        case let .validationCode(type, action):
            var dictionary = ["code_type": type.rawValue]
            if let action = action {
                dictionary["opcao_escolhida"] = action.rawValue
            }
            return dictionary
        case let .documentModalOptionSelected(option):
            return ["option_selected": option.rawValue]
        case let .continuedScreenOptionSelected(option):
            return ["option_selected": option.rawValue]
        case .touchPromoCode:
            return ["Canal": "Cadastro"]
        case let .validatePromoCode(code):
            return ["Código": code]
        case let .promoCodeError(error):
            return ["Motivo": error]
        case let .registerUsername(attempts, withSuggestion):
            return ["Tentativas": attempts, "Usou sugestão": withSuggestion ? "Sim" : "Não"]
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        guard case .phoneCode = self else {
            return [.mixPanel, .firebase]
        }
        
        return [.mixPanel, .firebase, .appsFlyer]
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

extension RegistrationEvent {
    enum PhoneConfirmationOptionType: String {
        case ok = "OK"
        case edit = "EDITAR"
    }
    
    enum VerificationOptionType: String {
        case resendSMS = "REENVIAR SMS"
        case receiveCall = "RECEBER CHAMADA"
    }
    
    enum DocumentErrorType: String {
        case invalidNumber = "NUMERO INVALIDO"
        case errorInserting = "ERRO AO INSERIR"
    }
    
    enum ReceiveCodeOptionType: String {
        case whatsApp = "RECEBER CODIGO VIA WHATSAPP"
        case sms = "RECEBER CODIGO VIA SMS"
        case haveCode = "JA POSSUO CODIGO"
    }
    
    enum SmsConfirmationOptionType: String {
        case resendCode = "REENVIAR CODIGO"
        case editNumber = "EDITAR NUMERO"
        case help = "PRECISO DE AJUDA"
    }
    
    enum ValidationCodeOptionType: String {
        case mgm = "MGM"
        case promo = "PROMO"
        case uniAccount = "CONTA UNIVERSITARIA"
    }
    
    enum ValidationCodeActionType: String {
        case understand = "ENTENDI"
        case regulation = "REGULAMENTO"
        case createUniAccount = "CRIAR CONTAUNI"
    }
    
    enum DocumentInUseOptionType: String {
        case login = "FAZER LOGIN"
        case forgotPassword = "ESQUECI MINHA SENHA"
    }
    
    enum ContinuedScreenOptionType: String {
        case continueRegistration = "CONTINUAR CADASTRO"
        case startNewRegistration = "INICIAR NOVO CADASTRO"
    }
}

private extension RegistrationStep {
    var description: String {
        switch self {
        case .name:
            return "NAME"
        case .phone:
            return "PHONE"
        case .email:
            return "EMAIL"
        case .password:
            return "PASSWORD"
        case .document:
            return "CPF"
        }
    }
}

private extension Bool {
    var description: String {
        self ? "True" : "False"
    }
}
