import Foundation

enum AnalyticsLocalizable: String, Localizable {
    case events
    
    var key: String {
        return self.rawValue
    }
    
    var file: LocalizableFile {
        return .analytics
    }
}
