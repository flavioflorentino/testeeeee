import Foundation
import UI

protocol AnalyticsTrackingViewModelInputs {
    func viewDidLoad()
    func close()
    func clean()
}

protocol AnalyticsTrackingViewModelOutputs: AnyObject {
    func didNextAction(_ action: AnalyticsTrackingAction)
    func analyticsLogs(_ logs: [Section<String, AnalyticsLogViewPresenterProtocol>])
}

protocol AnalyticsTrackingViewModelProtocol: AnyObject {
    var inputs: AnalyticsTrackingViewModelInputs { get }
    var outputs: AnalyticsTrackingViewModelOutputs? { get set }
}

final class AnalyticsTrackingViewModel: AnalyticsTrackingViewModelProtocol, AnalyticsTrackingViewModelInputs {
    var inputs: AnalyticsTrackingViewModelInputs {
        return self
    }
    
    weak var outputs: AnalyticsTrackingViewModelOutputs?
    
    private let service: AnalyticsTrackingServiceProtocol
    
    init(service: AnalyticsTrackingServiceProtocol) {
        self.service = service
    }
    
    func viewDidLoad() {
        loadData()
    }
    
    func close() {
        outputs?.didNextAction(.close)
    }
    
    func clean() {
        service.deleteLogs { [weak self] in
            self?.loadData()
        }
    }
    
    private func loadData() {
        service.logs { [weak self] logs in
            let presenters = logs.map {
                AnalyticsLogViewPresenter(title: "\($0.provider) - \($0.event.keys.first ?? String())", detail: String(describing: $0.event.values.first ?? [:]))
            }
            
            self?.outputs?.analyticsLogs([Section(items: presenters)])
        }
    }
}
