import UI
import UIKit

final class AnalyticsTrackingViewController: LegacyViewController<AnalyticsTrackingViewModelProtocol, AnalyticsTrackingCoordinatorProtocol, UIView> {
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(AnalyticsLogViewCell.self, forCellReuseIdentifier: String(describing: AnalyticsLogViewCell.self))
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 64
        return tableView
    }()
    
    private var dataSource: TableViewHandler<String, AnalyticsLogViewPresenterProtocol, AnalyticsLogViewCell>?
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.inputs.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
    }
    
    // MARK: - ViewConfiguration Methods
    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func configureViews() {
        title = AnalyticsLocalizable.events.text
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.constraintAllEdges(from: tableView, to: view)
    }
    
    // MARK: - Private Methods
    private func updateNavigationBarAppearance() {
        let closeBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .stop,
            target: self,
            action: #selector(tapClose)
        )
        
        let trashBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .trash,
            target: self,
            action: #selector(tapTrash)
        )
        
        navigationItem.rightBarButtonItem = closeBarButtonItem
        navigationItem.leftBarButtonItem = trashBarButtonItem
        navigationController?.navigationBar.tintColor = Palette.ppColorGrayscale000.color
        navigationController?.navigationBar.barTintColor = Palette.ppColorBranding300.color
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Palette.ppColorGrayscale000.color]
    }
    
    @objc
    public func tapClose() {
        viewModel.inputs.close()
    }
    
    @objc
    public func tapTrash() {
        viewModel.inputs.clean()
    }
}

// MARK: View Model Outputs
extension AnalyticsTrackingViewController: AnalyticsTrackingViewModelOutputs {
    func didNextAction(_ action: AnalyticsTrackingAction) {
        coordinator.perform(action: action)
    }
    
    func analyticsLogs(_ logs: [Section<String, AnalyticsLogViewPresenterProtocol>]) {
        dataSource = TableViewHandler(data: logs, configureCell: { _, model, cell in
            cell.setupCell(model)
        })
        tableView.dataSource = dataSource
        tableView.reloadData()
    }
}
