import Foundation

public protocol AnalyticsLogViewPresenterProtocol {
    var title: String { get }
    var detail: String { get }
}

public struct AnalyticsLogViewPresenter: AnalyticsLogViewPresenterProtocol {
    public var title: String
    public var detail: String
}
