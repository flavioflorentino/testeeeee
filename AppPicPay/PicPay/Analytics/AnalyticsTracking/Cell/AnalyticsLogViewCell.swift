import UI
import UIKit

public final class AnalyticsLogViewCell: UITableViewCell {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale600.color
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var detailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 12, weight: .light)
        label.textColor = Palette.ppColorGrayscale600.color
        label.numberOfLines = 0
        return label
    }()
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setupCell(_ presenter: AnalyticsLogViewPresenterProtocol) {
        titleLabel.text = presenter.title
        detailLabel.text = presenter.detail
    }
}

extension AnalyticsLogViewCell: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(titleLabel)
        addSubview(detailLabel)
    }
    
    public func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: self,
                                           for: [titleLabel, detailLabel],
                                           constant: 16)
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            detailLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8),
            detailLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8)
        ])
    }
    
    public func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    private func setupView() {
        buildViewHierarchy()
        configureViews()
        setupConstraints()
    }
}
