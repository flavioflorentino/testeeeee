import AnalyticsModule
import Foundation

protocol AnalyticsTrackingServiceProtocol {
    func deleteLogs(onSuccess: @escaping () -> Void)
    func logs(completion: @escaping ([AnalyticsLoggerTrack]) -> Void)
}

final class AnalyticsTrackingService: AnalyticsTrackingServiceProtocol {
    private let logger: AnalyticsLoggerProtocol
    
    init(logger: AnalyticsLoggerProtocol) {
        self.logger = logger
    }
    
    func deleteLogs(onSuccess: @escaping () -> Void) {
        logger.clean()
        onSuccess()
    }
    
    func logs(completion: @escaping ([AnalyticsLoggerTrack]) -> Void) {
        let logs = logger.getLogs()
        completion(logs.reversed())
    }
}
