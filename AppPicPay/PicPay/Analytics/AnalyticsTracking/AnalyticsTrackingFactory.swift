import AnalyticsModule
import Foundation

final class AnalyticsTrackingFactory {
    static func make() -> AnalyticsTrackingViewController {
        let service: AnalyticsTrackingServiceProtocol = AnalyticsTrackingService(logger: AnalyticsLogger.shared)
        let viewModel: AnalyticsTrackingViewModelProtocol = AnalyticsTrackingViewModel(service: service)
        let coordinator: AnalyticsTrackingCoordinatorProtocol = AnalyticsTrackingCoordinator()
        let viewController = AnalyticsTrackingViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController
        
        return viewController
    }
}
