import UIKit

enum AnalyticsTrackingAction {
    case close
}

protocol AnalyticsTrackingCoordinatorProtocol: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: AnalyticsTrackingAction)
}

final class AnalyticsTrackingCoordinator: AnalyticsTrackingCoordinatorProtocol {
    weak var viewController: UIViewController?
    
    func perform(action: AnalyticsTrackingAction) {
        switch action {
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
