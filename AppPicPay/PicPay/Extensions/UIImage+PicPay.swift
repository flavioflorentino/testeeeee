import Foundation
import UI

extension UIImage {
    convenience init(qrCodeData: String) {
        let data = qrCodeData.data(using: String.Encoding.ascii)
        let ciContext = CIContext()
        
        guard let filter = CIFilter(name: "CIQRCodeGenerator") else {
            self.init()
            return
        }
        filter.setValue(data, forKey: "inputMessage")
        filter.setValue("H", forKey: "inputCorrectionLevel")
        let transform = CGAffineTransform(scaleX: 5, y: 5)
        
        guard let output = filter.outputImage?.transformed(by: transform) else {
            self.init()
            return
        }
        
        let cgImage = ciContext.createCGImage(output,
                                              from: output.extent)
        self.init(cgImage: cgImage!)
    }
    
    convenience init(qrCodeData: String, size: CGSize) {
        let data = qrCodeData.data(using: String.Encoding.ascii)
        let ciContext = CIContext()
        
        guard let filter = CIFilter(name: "CIQRCodeGenerator") else {
            self.init()
            return
        }
        filter.setValue(data, forKey: "inputMessage")
        filter.setValue("H", forKey: "inputCorrectionLevel")
        
        guard let output = filter.outputImage else {
            self.init()
            return
        }
        
        let scaleX = size.width / output.extent.size.width
        let scaleY = size.height / output.extent.size.height
        
        let transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
        
        let cgImage = ciContext.createCGImage(output.transformed(by: transform),
                                              from: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        self.init(cgImage: cgImage!)
    }
    
    class func textEmbededImage(image: UIImage, string: String, color: UIColor, imageAlignment: Int = 0, segFont: UIFont? = nil, opacity: CGFloat) -> UIImage {
        let font = segFont ?? UIFont.systemFont(ofSize: 16.0)
        let expectedTextSize: CGSize = (string as NSString).size(withAttributes: [.font: font])
        let width: CGFloat = expectedTextSize.width + image.size.width + 5.0
        let height: CGFloat = max(expectedTextSize.height, image.size.width)
        let size = CGSize(width: width, height: height)
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        
        guard let context: CGContext = UIGraphicsGetCurrentContext() else {
            return UIImage()
        }
        context.setFillColor(Palette.ppColorGrayscale000.color.withAlphaComponent(opacity).cgColor)
        context.setAlpha(opacity)
        
        let fontTopPosition: CGFloat = (height - expectedTextSize.height) / 2.0
        let textOrigin: CGFloat = (imageAlignment == 0) ? image.size.width + 5 : 0
        let textPoint = CGPoint(x: textOrigin, y: fontTopPosition)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: color.withAlphaComponent(opacity)
        ]
        string.draw(at: textPoint,
                    withAttributes: attributes)
        
        let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: size.height)
        context.concatenate(flipVertical)
        
        let alignment: CGFloat = (imageAlignment == 0) ? 0.0 : expectedTextSize.width + 5.0
        guard let cgImage = image.cgImage else {
            return UIImage()
        }
        context.draw(cgImage,
                     in: CGRect(x: alignment,
                                y: (height - image.size.height) / 2.0,
                                width: image.size.width,
                                height: image.size.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        
        return newImage.withRenderingMode(.alwaysOriginal)
    }
    
    @objc
    func resizeImage(targetSize: CGSize) -> UIImage? {
        let widthRatio = targetSize.width / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if widthRatio > heightRatio {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(origin: .zero, size: CGSize(width: newSize.width, height: newSize.height))
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    /**
     Crop a square image
     - parameter originalImage: UIImage a original image from camera or gallery
     - returns: UIImage A cropped image
     */
    func cropSquare() -> UIImage? {
        let imageSize: CGSize = self.size
        let width: CGFloat = imageSize.width
        let height: CGFloat = imageSize.height
        var returnedImage = self
        if width != height {
            let newDimension: CGFloat = min(width, height)
            let widthOffset: CGFloat = (width - newDimension) / 2
            let heightOffset: CGFloat = (height - newDimension) / 2
            UIGraphicsBeginImageContextWithOptions(CGSize(width: newDimension, height: newDimension), false, 0.0)
            returnedImage.draw(at: CGPoint(x: -widthOffset, y: -heightOffset), blendMode: .copy, alpha: 1.0)
            returnedImage = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
            UIGraphicsEndImageContext()
        }
        return returnedImage
    }
    
    // Rotate UIImage
    func image(withRotation radians: CGFloat) -> UIImage {
        let cgImage = self.cgImage!
        let LARGEST_SIZE = CGFloat(max(self.size.width, self.size.height))
        let context = CGContext(data: nil, width: Int(LARGEST_SIZE), height: Int(LARGEST_SIZE), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: cgImage.colorSpace!, bitmapInfo: cgImage.bitmapInfo.rawValue)!
        
        var drawRect = CGRect.zero
        drawRect.size = self.size
        let drawOrigin = CGPoint(x: (LARGEST_SIZE - self.size.width) * 0.5, y: (LARGEST_SIZE - self.size.height) * 0.5)
        drawRect.origin = drawOrigin
        var tf = CGAffineTransform.identity
        tf = tf.translatedBy(x: LARGEST_SIZE * 0.5, y: LARGEST_SIZE * 0.5)
        tf = tf.rotated(by: CGFloat(radians))
        tf = tf.translatedBy(x: LARGEST_SIZE * -0.5, y: LARGEST_SIZE * -0.5)
        context.concatenate(tf)
        context.draw(cgImage, in: drawRect)
        var rotatedImage = context.makeImage()!
        
        drawRect = drawRect.applying(tf)
        
        rotatedImage = rotatedImage.cropping(to: drawRect)!
        let resultImage = UIImage(cgImage: rotatedImage)
        return resultImage
    }
    
    func grayScaleImage() -> UIImage {
        let imageRect = CGRect(origin: CGPoint.zero, size: self.size)
        let colorSpace = CGColorSpaceCreateDeviceGray()
        let width = Int(self.size.width)
        let height = Int(self.size.height)
        let context = CGContext(data: nil, width: width, height: height, bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: 0)
        
        /// set Fill Color to White (or some other color)
        context?.setFillColor(Palette.ppColorGrayscale000.cgColor)
        /// Draw a white-filled rectangle before drawing your image
        context?.fill(imageRect)
        context?.draw(self.cgImage!, in: imageRect)
        
        let imageRef = context?.makeImage()
        let newImage = UIImage(cgImage: imageRef!)
        return newImage
    }
    
    /// Colorize the image with the given color.
    /// - Parameter color: The color to be setted on the image.
    func imageMaskedAndTinted(with color: UIColor) -> UIImage? {
        guard let maskImage = cgImage else {
            return nil
        }
        
        let width = size.width
        let height = size.height
        let size = CGSize(width: width, height: height)
        let bounds = CGRect(origin: .zero, size: size)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        guard let context = CGContext(data: nil,
                                      width: Int(width),
                                      height: Int(height),
                                      bitsPerComponent: 8,
                                      bytesPerRow: 0,
                                      space: colorSpace,
                                      bitmapInfo: bitmapInfo.rawValue) else {
                                        return nil
        }
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            return UIImage(cgImage: cgImage)
        }
        return nil
    }
}
