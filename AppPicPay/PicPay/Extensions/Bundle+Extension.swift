import Foundation

extension Bundle {
    var releaseVersionNumber: String {
        return infoDictionary?["CFBundleShortVersionString"] as? String ?? String()
    }
    var buildVersionNumber: String {
        return infoDictionary?["CFBundleVersion"] as? String ?? String()
    }
}
