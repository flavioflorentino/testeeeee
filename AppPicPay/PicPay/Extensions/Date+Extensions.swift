extension Date {
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    
    func formattedTime(from date: Date) -> String {
        let diff = Calendar.current.dateComponents([.hour, .minute, .second], from: date, to: self)
        
        let seconds: Int = abs(diff.second ?? 0)
        let minutes: Int = abs(diff.minute ?? 0)
        let hours: Int = abs(diff.hour ?? 0)
        if hours > 0 {
            return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        }
        else {
            return String(format: "%02d:%02d", minutes, seconds)
        }
    }
    
    func stringFormatted(with dateFormat: Format = .ddMMyyyy) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat.rawValue
        formatter.locale = Locale(identifier: "pt_BR_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        let stringText = formatter.string(from: self)
        return dateFormat == .ddDeMMMM ? stringText.capitalizedLowercasedPrepositions : stringText
    }
    
    enum Format: String {
        case ddDeMMMM = "dd 'de' MMMM"
        case ddMMyyyy = "dd/MM/yyyy"
    }
}
