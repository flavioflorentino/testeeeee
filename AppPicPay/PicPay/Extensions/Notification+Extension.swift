//
//  Notification+Extension.swift
//  PicPay
//
//  ☝️☁️ Created by Luiz Henrique Guimarães on 01/11/17.
//

import Foundation

extension Notification.Name {
    
    enum Payment {
        static let new = Notification.Name(rawValue: "PaymentNew")
        static let tapPayButton = Notification.Name(rawValue: "PaymentTabPayButton")
        static let methodChange = Notification.Name(rawValue: "PaymentMethodChange")
        static let installmentChange = Notification.Name(rawValue: "PaymentInstallmentChange")
    }
    
    enum EmailStatus {
        static let updated = Notification.Name("EmailStatusCheckManagerEmailStatusUpdated")
    }
    
    enum Subscriptions {
        static let reload = Notification.Name("fSubscriptionsNeedsReload")
    }

    enum CrediCard {
        static let creditCardFlagLoaded = Notification.Name("ccListLoaded")
    }
    
    enum Consumer {
        static let balanceChanged = Notification.Name("balanceChanged")
    }
}

@objc final class NotificationName : NSObject{
    @objc static let paymentNew: String = Notification.Name.Payment.new.rawValue
}
