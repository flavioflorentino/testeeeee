//
//  NSAttributedString+HTML.swift
//  PicPay
//
//  Created by Vagner Orlandi on 02/02/18.
//

import Foundation


extension NSAttributedString {
    
    @objc func untag(_ tag:String, onTagRange execute:((NSMutableAttributedString, NSRange) -> Void)? = nil) -> NSAttributedString {
        var ranges:[NSRange] = []
        let attrStr = NSMutableAttributedString(attributedString: self)
        let openTag = "<(\(tag))[^>]*>"
        let closeTag = "</\(tag)>"
        var seekRange:NSRange = NSRange(location: 0, length: self.length)
        
        var openTagRange:NSRange
        var closeTagRange:NSRange
        
        while seekRange.length > 0 {
            openTagRange = (attrStr.string as NSString).range(of: openTag, options: .regularExpression, range: seekRange)
            if openTagRange.location == NSNotFound {
                break
            }
            
            seekRange.location = openTagRange.location
            seekRange.length = attrStr.length - seekRange.location
            
            closeTagRange = (attrStr.string as NSString).range(of: closeTag, range: seekRange)
            if closeTagRange.location == NSNotFound {
                break
            }
            
            attrStr.deleteCharacters(in: openTagRange)
            closeTagRange.location -= openTagRange.length
            attrStr.deleteCharacters(in: closeTagRange)
            
            seekRange.location = closeTagRange.location
            seekRange.length = attrStr.length - seekRange.location
            
            let rangeLength = closeTagRange.location - openTagRange.location
            ranges.append(NSRange(location: openTagRange.location, length: rangeLength))
        }
        
        if execute != nil {
            for range in ranges {
                execute?(attrStr, range)
            }
        }
        
        //Remove tags sem pares
        let removeTagsInString:((_ str: NSMutableAttributedString, _ tag:String) -> Void) = {(str, tag) in
            var range:NSRange = (str.string as NSString).range(of: tag, options: .regularExpression)
            
            while (range.location != NSNotFound) {
                str.deleteCharacters(in: range)
                range = (str.string as NSString).range(of: tag, options: .regularExpression)
            }
        }
        
        [openTag, closeTag].forEach { (tagName) in
            removeTagsInString(attrStr, tagName)
        }
        
        return attrStr
    }
    
    @objc func boldfyWithSystemFont(ofSize size:CGFloat = 12, weight: CGFloat = UIFont.Weight.semibold.rawValue) -> NSAttributedString {
        return self.untag("b", onTagRange: { (attrStr, range) in
            attrStr.addAttributes([.font : UIFont.systemFont(ofSize: size, weight: UIFont.Weight(rawValue: weight))], range: range)
        })
    }
    
    @objc func underlinefy() -> NSAttributedString {
        return self.untag("u", onTagRange: { (attrStr, range) in
            attrStr.addAttributes([.underlineStyle : NSUnderlineStyle.single.rawValue], range: range)
        })
    }
}
