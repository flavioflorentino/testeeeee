import Foundation

protocol NestedDecodable { }

struct NestedKey {
    static var key: String { return "data" }
}

extension JSONDecoder {
    static func decode<T: Decodable>(_ data: Data, to type: T.Type) throws -> T {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .useDefaultKeys
        
        return try decoder.decode(T.self, from: data)
    }
    
    func decodeNestedData<T: Decodable>(_ type: T.Type, from data: Data) throws -> T {
        if type.self is NestedDecodable.Type {
            if let dict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
                let inside = dict[NestedKey.key] as? [String: Any],
                let jsonData = try? JSONSerialization.data(withJSONObject: inside) {
                return try self.decode(type.self, from: jsonData)
            }
        }
        return try self.decode(type.self, from: data)
    }
}

extension JSONDecoder.DateDecodingStrategy {
    static let customISO8601 = custom { decoder throws -> Date in
        let container = try decoder.singleValueContainer()
        let string = try container.decode(String.self)
        if let date = DateFormatter.iso8601noFS.date(from: string) {
            return date
        } else if let date = DateFormatter.iso8601Time.date(from: string) {
            return date
        } else if let date = DateFormatter.iso8601noTime.date(from: string) {
            return date
        }
        throw DecodingError.dataCorruptedError(in: container, debugDescription: "Invalid date: \(string)")
    }
}
