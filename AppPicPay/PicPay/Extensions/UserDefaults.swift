import Foundation

extension UserDefaults {
    var isHiddenSingleTicketReminder: Bool {
        get { return bool(forKey: #function) }
        set { set(newValue, forKey: #function) }
    }
    
    var isHiddenRechargeReminder: Bool {
        get { return bool(forKey: #function) }
        set { set(newValue, forKey: #function) }
    }
}
