//
//  NSAtributedString+PicPay.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 17/01/17.
//
//

import UI
import Foundation

public extension NSAttributedString {
    enum ImageSide {
        case right
        case left
    }
    
    /**
     Set Attributed string apply the picpay font
     */
    func attributedPicpayFont(_ fontSize:CGFloat = 12.0, weight: CGFloat = UIFont.Weight.regular.rawValue, fontName: String = "TimesNewRomanPS-BoldMT") -> NSAttributedString?{
        let attrs = NSMutableAttributedString(attributedString: self)
        
        attrs.enumerateAttribute(.font, in: NSMakeRange(0,attrs.length), options: .longestEffectiveRangeNotRequired, using: { (value, range,stop) in
            if let oldFont = value as? UIFont{
                var newFont = UIFont.systemFont(ofSize: fontSize)
                
                if oldFont.fontName == fontName {
                    newFont = UIFont.systemFont(ofSize: fontSize)
                }
                
                attrs.removeAttribute(.font, range: range)
                attrs.addAttribute(.font, value: newFont, range: range)
            }
        })
        return attrs
    }
    
    static func attachment(with image: UIImage, capHeight: CGFloat, scaling: CGFloat = 1.0) -> NSAttributedString {
        let attachment = NSTextAttachment()
        attachment.bounds = CGRect(x: 0,
                                   y: (capHeight - image.size.height) / 2,
                                   width: (image.size.width * scaling),
                                   height: (image.size.height * scaling))
        attachment.image = image
        return NSAttributedString(attachment: attachment)
    }
    
    static func withImageOn(side: ImageSide = .left, image: UIImage, text: String, font: UIFont) -> NSAttributedString {
        let text = NSAttributedString(string: text)
        let nonBreakingSpace = NSAttributedString(string: String.nonBreakingSpace)
        
        let attributedString = NSMutableAttributedString()
        let image = NSAttributedString.attachment(with: image, capHeight: font.capHeight)
        switch side {
        case .right:
            attributedString.append([image, nonBreakingSpace, text])
        case .left:
            attributedString.append([text, nonBreakingSpace, image])
        }
        
        return attributedString
    }
}

public extension NSMutableAttributedString {
    
    @discardableResult
    func setAsLink(_ textToFind:String, linkURL:String, color: UIColor? = nil) -> Bool {
        
        let colorAttr:UIColor = color ?? Palette.ppColorBranding300.color
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(.link, value: linkURL, range: foundRange)
            self.addAttribute(.foregroundColor, value: colorAttr, range: foundRange)
            return true
        }
        return false
    }
    
    func append(_ strings: [NSAttributedString]) {
        strings.forEach { self.append($0) }
    }
}
