//
//  UIImagePicker+Extensions.swift
//  PicPay
//
//  Created by PicPay Eduardo on 10/18/18.
//

import Foundation

extension UIImagePickerController {
    
    @objc func checkCameraPermission(cameraActionDescription: String, _ callback: @escaping () -> Void) {
        
        let permissionCamera = PPPermission.camera(withDeniedAlert: PPPermission.Alert(
            title: "Autorize o PicPay a usar sua câmera",
            message: "Para \(cameraActionDescription), autorize o acesso à câmera nos Ajustes do seu iPhone.",
            settings:"Ajustes"
        ))
        
        switch permissionCamera.status {
        case .authorized:
            callback()
            
        case .denied, .notDetermined, .disabled:
            PPPermission.request(permission: permissionCamera, { status in
                if status == .authorized {
                    callback()
                }
            })
        }
    }
}
