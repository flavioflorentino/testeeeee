extension Encodable {
    func toDictionary(strategy: JSONEncoder.KeyEncodingStrategy = .convertToSnakeCase) -> [String: Any]? {
        let jsonEncoder = JSONEncoder()
        jsonEncoder.keyEncodingStrategy = strategy
        guard let jsonData = try? jsonEncoder.encode(self) else {
            return nil
        }
        if let dictionary = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String: Any] {
            return dictionary
        }
        return nil
    }
    
    func toString(strategy: JSONEncoder.KeyEncodingStrategy = .convertToSnakeCase) -> String? {
        let jsonEncoder = JSONEncoder()
        jsonEncoder.keyEncodingStrategy = strategy
        guard let jsonData = try? jsonEncoder.encode(self) else { return nil }
        
        return String(data: jsonData, encoding: .utf8)
    }
}
