import Core
import Foundation

public extension NSString {
    @objc var html2AttributedString: NSAttributedString? {
        guard let data = data(using: String.Encoding.utf8.rawValue) else {
            return nil
        }
        do {
            return try NSAttributedString(
                data: data,
                options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil
            )
        } catch let error as NSError {
            debugPrint(error.localizedDescription)
            return  nil
        }
    }
        
    @objc
    func html2Attributed(_ font: UIFont, color: UIColor = UIColor.black) -> NSAttributedString? {
        let hexColor = color.toHex()
        let finalString = """
            \(self)
            <style>
            body {
                color: \(hexColor);
            }
            </style>
        """
        guard let data = finalString.data(using: String.Encoding.utf8) else {
            return nil
        }
        do {
            let attributedString = try NSMutableAttributedString(
                data: data,
                options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil
            ).with(font: font)
            
            return attributedString
        } catch let error as NSError {
            debugPrint(error.localizedDescription)
            return  nil
        }
    }
    
    @objc
    func decodingHTMLEntities() -> String {
        return (self as String).decodingHTMLEntities()
    }
}
