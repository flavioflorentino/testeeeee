import UI
import UIKit

extension PPNavigationController {
    func setUpToCreditPicPay() {
        navigationBar.titleTextAttributes = [.foregroundColor: Palette.white.color]
        navigationBar.barTintColor = PicPayStyle.picPayCreditGreen
        navigationBar.tintColor = Palette.white.color
    }
    
    func setUpToUpgradeChecklist(enableLargeTitle: Bool) {
        if #available(iOS 11.0, *) {
            navigationBar.prefersLargeTitles = enableLargeTitle
            navigationBar.largeTitleTextAttributes = [.foregroundColor: Palette.ppColorGrayscale500.color, .font: UIFont.boldSystemFont(ofSize: 28)]
            navigationBar.setBackgroundImage(nil, for: .default)
        } else {
            navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationBar.shadowImage = UIImage()
        }
        
        navigationBar.titleTextAttributes = [.foregroundColor: Palette.ppColorGrayscale500.color]
        navigationBar.barTintColor = Palette.ppColorGrayscale100.color
        view.backgroundColor = Palette.ppColorGrayscale100.color
    }
}
