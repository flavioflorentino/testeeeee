import Foundation

/*!
 To be inject on classes that use UIApplication, this makes it possible to write unit tests on UIApplication.
 Test target already has a UIApplcationSpy, you should use it for tests.
 */
protocol UIApplicationContract {
    func open(_ url: URL, options: [UIApplication.OpenExternalURLOptionsKey: Any], completionHandler completion: ((Bool) -> Void)?)
    func canOpenURL(_ url: URL) -> Bool
}

extension UIApplicationContract {
    // This extension is needed because protocols doesn't support default values.
    func open(_ url: URL) {
        open(url, options: [:], completionHandler: nil)
    }
}

extension UIApplication: UIApplicationContract { }
