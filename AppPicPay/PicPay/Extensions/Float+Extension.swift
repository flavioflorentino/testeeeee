//
//  Double+Extensions.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 16/04/18.
//

extension Float {
    static func percentageInterval(min: Float, max: Float, value: Float) -> Float {
        if value > min &&  value < max {
            let diff = max - min
            let valueBase = value - min
            return Float( (valueBase * 100) / (diff * 100))
        } else if value <= min {
            return 0
        } else{
            return 1
        }
    }
}
