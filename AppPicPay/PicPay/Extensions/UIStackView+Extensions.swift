import UIKit

extension UIStackView {
    func removeAllArrangedSubviews() {
        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }
        
        NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))
        
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
    
    @discardableResult
    func insertLine(after view: UIView, color: UIColor, edgeInset: CGFloat = 0) -> UIView {
        let lineView = UIView()
        let lineHeight: CGFloat = 0.5
        lineView.backgroundColor = color
        insert(view: lineView, afterView: view)
        updateConstraintsInArrangedView(lineView, height: lineHeight, edgeInset: edgeInset)
        
        return lineView
    }
    
    func setSpacing(_ spacing: CGFloat, after view: UIView) {
        let spaceView = UIView()
        insert(view: spaceView, afterView: view)
        updateConstraintsInArrangedView(spaceView, height: spacing, edgeInset: 0)
    }
    
    func addArrangedSubviews(_ views: UIView...) {
        views.forEach { addArrangedSubview($0) }
    }
}

extension UIStackView {
    private func insert(view: UIView, afterView: UIView) {
        if let index = arrangedSubviews.firstIndex(of: afterView) {
            insertArrangedSubview(view, at: index + 1)
        }
    }
    
    private func updateConstraintsInArrangedView(_ view: UIView, height: CGFloat, edgeInset: CGFloat) {
        NSLayoutConstraint.activate([
            view.trailingAnchor.constraint(equalTo: trailingAnchor, constant: edgeInset),
            view.leadingAnchor.constraint(equalTo: leadingAnchor, constant: edgeInset),
            view.heightAnchor.constraint(equalToConstant: height)
        ])
    }
}
