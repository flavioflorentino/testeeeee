//
//  UIFont+Extension.swift
//  PicPay
//
//  Created by PicPay-DEV-PP on 05/12/18.
//

import UIKit

extension UIFont {
    func bold() -> UIFont? {
        let font = UIFont(name: familyName, size: pointSize) ?? self
        let traits: UIFontDescriptor.SymbolicTraits = [fontDescriptor.symbolicTraits, .traitBold]
        let descriptor = font.fontDescriptor.withSymbolicTraits(traits)
        return descriptor.flatMap { UIFont(descriptor: $0, size: pointSize) }
    }
}
