//
//  UIColor+Extension.swift
//  PicPay
//
//  Created by Vagner Orlandi on 20/09/17.
//
//

import Foundation

public extension UIColor {
    var rgba: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0
        getRed(&red, green: &green, blue: &blue, alpha: &alpha)

        return (red, green, blue, alpha)
    }
    
    convenience init?(hexStr: String) {
        if let rgb = Int(hexStr.replacingOccurrences(of: "#", with: ""), radix: 16) {
            self.init(
                red: CGFloat((rgb >> 16) & 0xFF) / 255,
                green: CGFloat((rgb >> 8) & 0xFF) / 255,
                blue: CGFloat(rgb & 0xFF) / 255,
                alpha: 1
            )
        } else {
            return nil
        }
    }
    
    func toHex() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0

        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }

    @objc
    @available(*, deprecated, message: "Use change(_, to:_) the Colors")
    func withCustomDarkColor(_ customDark: UIColor) -> UIColor {
        guard #available(iOS 13.0, *) else {
            return self
        }
        
        return UIColor { traitCollection in
            switch traitCollection.userInterfaceStyle {
            case .dark:
                return customDark
            default:
                return self
            }
        }
    }
}
