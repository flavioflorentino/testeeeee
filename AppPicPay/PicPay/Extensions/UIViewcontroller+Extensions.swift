import Foundation
import UIKit
import UI

extension UIViewController {
    var isModal: Bool {
        let presentingIsModal = presentingViewController != nil
        let presentingIsNavigation = navigationController?.presentingViewController?.presentedViewController == navigationController
        let presentingIsTabBar = tabBarController?.presentingViewController is UITabBarController
        return presentingIsModal || presentingIsNavigation || presentingIsTabBar || false
    }
    
    fileprivate static let InsetTopViewTag: Int = 1001

    fileprivate static let InsetBottomViewTag: Int = 1002

    @objc var hasSafeAreaInsets: Bool {
        if #available(iOS 11.0, *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets != .zero
        }
        return false
    }

    @objc func paintSafeAreaBottomInset(withColor color: UIColor?) {
        let insetView:UIView

        if let reuseView = self.view.viewWithTag(UIViewController.InsetBottomViewTag) {
            insetView = reuseView
        } else {
            insetView = UIView(frame: .zero)
            insetView.tag = UIViewController.InsetBottomViewTag
            insetView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(insetView)
            view.sendSubviewToBack(insetView)

            insetView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            insetView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
            insetView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true

            if #available(iOS 11.0, *) {
                insetView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
            }else{
                insetView.topAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
            }
        }

        insetView.backgroundColor = color
    }

    @objc func paintSafeAreaTopInset(withColor color: UIColor?) {
        let insetView:UIView

        if let reuseView = self.view.viewWithTag(UIViewController.InsetTopViewTag) {
            insetView = reuseView
        } else {
            insetView = UIView(frame: .zero)
            insetView.tag = UIViewController.InsetTopViewTag
            insetView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(insetView)
            view.sendSubviewToBack(insetView)

            insetView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            insetView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            insetView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true

            if #available(iOS 11.0, *) {
                insetView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
            }else{
                insetView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
            }
        }

        insetView.backgroundColor = color
    }
    
    func isOfType(inList types: [UIViewController.Type]) -> Bool {
        return types.contains(where: { self.isKind(of: $0) })
    }
    
    func isNotOfType(inList types: [UIViewController.Type]) -> Bool {
        return !self.isOfType(inList: types)
    }
}
