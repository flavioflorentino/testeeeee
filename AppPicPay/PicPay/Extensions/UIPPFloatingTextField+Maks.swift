import UI
import Validator

extension UIPPFloatingTextField {
    public func setupPhoneMask(error: ValidationError) {
        var numberRules = ValidationRuleSet<String>()
        numberRules.add(rule: ValidationRuleLength(min: 14, max: 16, error: error))

        var textField = self
        textField.validationRules = numberRules

        maskblock = { oldValue, newValue in
            // Handling user input and programmatically updated text field
            let threshold = newValue.count > 10 && !newValue.contains("(") ? 10 : 14
            // string mask for number with 8 and 9 digits
            let mask = CustomStringMask(mask: newValue.count > threshold ? "(00) 00000-0000" : "(00) 0000-0000")
            
            let nValue = mask.unmaskedText(from: newValue)
            return mask.maskedText(from: nValue) ?? ""
        }
    }
}
