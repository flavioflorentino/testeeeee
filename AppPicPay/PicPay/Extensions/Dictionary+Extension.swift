//
//  Dictionary+Extension.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 17/10/17.
//
//

import Foundation

extension Dictionary {
    var nilsRemoved: [Key: AnyObject] {
        var newDict: [Key: AnyObject] = [:]
        for key in self.keys {
            if self[key] != nil && type(of: self[key] as AnyObject) != NSNull.self {
                newDict[key] = self[key]! as AnyObject
            }
        }
        
        return newDict
    }
    
    func deepMerge(_ d2:[String:Any]) -> [String:Any] {
        var result = [String:Any]()
        guard let selfDict = self as? [String: Any] else { return [:] }
        for (k1,v1) in selfDict {
            result[k1] = v1
        }
        for (k2,v2) in d2 {
            if v2 is [String:Any], let v1 = result[k2], v1 is [String:Any] {
                result[k2] = (v1 as! [String:Any]).deepMerge(v2 as! [String:Any]) as Any
            } else {
                result[k2] = v2
            }
        }
        return result
    }
}
