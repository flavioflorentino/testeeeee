extension UILabel {
    public var kerning: CGFloat {
        set {
            if let currentAttibutedText = self.attributedText {
                let attribString = NSMutableAttributedString(attributedString: currentAttibutedText)
                attribString.addAttributes([.kern: newValue], range: NSRange(location: 0, length: currentAttibutedText.length))
                self.attributedText = attribString
            }
        } get {
            var kerning: CGFloat = 0
            if let attributedText = self.attributedText {
                attributedText.enumerateAttribute(.kern, in: NSRange(location: 0, length: attributedText.length)) { (value, _, _) in
                    kerning = value as? CGFloat ?? 0
                }
            }
            return kerning
        }
    }
    
    public func textWith(spacing space: CGFloat, aligment: NSTextAlignment = .left) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = space
        paragraphStyle.alignment = aligment
        attributedText = NSMutableAttributedString(string: text ?? "", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
    }
}
