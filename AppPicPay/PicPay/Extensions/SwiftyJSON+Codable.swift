import SwiftyJSON

extension JSON {
    func decoded<T: Decodable>(strategy: JSONDecoder.KeyDecodingStrategy = .convertFromSnakeCase) -> T? {
        guard let data = try? rawData() else {
            return nil
        }
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .customISO8601
        decoder.keyDecodingStrategy = strategy
        
        var decodedObject: T?
        do {
            decodedObject = try decoder.decodeNestedData(T.self, from: data)
        } catch let error {
            Log.error(.model, "Decoding error: \(error)")
        }
        return decodedObject
    }
}
