extension UIScreen {
    public static var isSmall: Bool {
        return UIScreen.main.bounds.height == 568
    }
}
