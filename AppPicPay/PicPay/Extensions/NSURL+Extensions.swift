//
//  NSURL+Extensions.swift
//  PicPay
//
//  ☝🏽☁️ Created by Luiz Henrique Guimarães on 02/03/18.
//

import Foundation

extension URL {
    
    // get params from query string
    var queryParams: [String: String] {
        var params: [String: String] = [:]
        
        guard let urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: true), let queryItems = urlComponents.queryItems else { return params }
        
        for q in queryItems {
            params[q.name] = q.value
        }
        
        return params
    }
}
