//
//  Double+Currency.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 23/10/17.
//

import CoreLegacy
import Foundation

extension Double {
    var stringAmount:String {
        return CurrencyFormatter.brazillianRealString(from: NSNumber(value: self))
    }
    var decimalValue: Decimal {
        return Decimal(self)
    }
    
    func truncate(places : Int) -> Double {
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }
    
    static func percentageInterval(min: Float, max: Float, value: Float) -> Double {
        if value > min &&  value < max {
            let diff = max - min
            let valueBase = value - min
            return Double( (valueBase * 100) / (diff * 100))
        } else if value <= min {
            return 0
        } else{
            return 1
        }
    }
    
    func getFormattedPrice() -> String {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "pt-br")
        return formatter.string(for: self) ?? "R$ 0,00"
    }
    
    func getFormattedPriceWithoutCurrencySymbol() -> String {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.numberStyle = .decimal
        formatter.locale = Locale(identifier: "pt-br")
        return formatter.string(for: self) ?? ""
    }
}
