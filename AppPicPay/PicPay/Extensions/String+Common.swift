import Foundation
import UI
import CoreLegacy

extension String {
    var onlyNumbers: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }
    
    func attributedString(withBoldIn ranges: [NSRange], using font: UIFont) -> NSAttributedString {
        let attributed = NSMutableAttributedString(string: self, attributes: [.font: font])
        
        guard let boldFont = font.bold() else {
            return attributed
        }
        
        for range in ranges {
            attributed.addAttributes([.font: boldFont], range: range)
        }
        
        return attributed
    }
    
    func attributedString(withBold strings: [String], using font: UIFont) -> NSAttributedString {
        var ranges = [NSRange]()
        
        for string in strings {
            ranges.append((self as NSString).range(of: string))
        }
        
        return attributedString(withBoldIn: ranges, using: font)
    }
    
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    subscript(_ range: CountableRange<Int>) -> String {
        guard range.upperBound <= self.count else { return "" }
        let idx1 = index(startIndex, offsetBy: range.lowerBound)
        let idx2 = index(startIndex, offsetBy: range.upperBound)
        return String(self[idx1..<idx2])
    }
    
    static var nonBreakingSpace: String {
        return "\u{00a0}"
    }
    
    var length: Int {
        get {
            return self.count
        }
    }
    
    var fullRange: NSRange {
        NSRange(location: 0, length: self.count)
    }
    
    static func ppMeLink(username: String) -> String {
        "https://picpay.me/\(username)"
    }
    
    func trim() -> String{
        self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    func chopPrefix(_ count: Int = 1) -> String {
        guard 
            count > 0,
            count <= self.count
            else { return self }
        return String(suffix(from: index(startIndex, offsetBy: count)))
    }
    
    func chopSuffix(_ count: Int = 1) -> String {
        guard 
            count > 0,
            count <= self.count
            else { return self }
        return String(prefix(upTo: index(endIndex, offsetBy: -count)))
    }
    
    func removeDiacriticsAndUppercase() -> String {
        self.lowercased().folding(options: .diacriticInsensitive, locale: .current)
    }
    
    // formatting text for currency textField
    func currencyInputFormatting(allowCents: Bool = true) -> String {
        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        formatter.currencyDecimalSeparator = ","
        formatter.currencyGroupingSeparator = "."
        formatter.currencySymbol = ""
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        
        
        var amountWithPrefix = self
        
        // remove from String: "$", ".", ","
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, amountWithPrefix.count), withTemplate: "")
        
        let double = (amountWithPrefix as NSString).doubleValue
        number = NSNumber(value: (double / (allowCents ? 100 : 1)))
        
        // if first number is 0 or all numbers were deleted
        guard number != 0 as NSNumber else {
            return ""
        }
        
        return formatter.string(from: number) ?? self
    }
    
    var currencyToNumber: NSNumber {
        if self == "" {
            return NSNumber(value: 0)
        }
        let endString = self.replacingOccurrences(of: ".", with: "")
        return CurrencyFormatter.brazillianRealNumber(from: endString.contains("R$") ? endString : "R$\(endString)")
    }
    
    func substring(from: Int, to: Int) -> String {
        let start = index(startIndex, offsetBy: from)
        let end = index(start, offsetBy: to - from)
        return String(self[start ..< end])
    }
    
    func substring(range: NSRange) -> String {
        return substring(from: range.lowerBound, to: range.upperBound)
    }
    
    /**
     Return the string localized
     */
    func localized(_ args:CVarArg...) -> String{
        let string = NSLocalizedString(self, tableName: "Localizable", bundle: Bundle.main , value: "", comment: "")
        return String(format: string, arguments: args)
    }
    
    var glyphCount: Int {
        
        let richText = NSAttributedString(string: self)
        let line = CTLineCreateWithAttributedString(richText)
        return CTLineGetGlyphCount(line)
    }
    
    var isSingleEmoji: Bool {
        
        return glyphCount == 1 && containsEmoji
    }
    
    var containsEmoji: Bool {
        
        return unicodeScalars.contains { $0.isEmoji }
    }
    
    var containsOnlyEmoji: Bool {
        
        return !isEmpty
            && !unicodeScalars.contains(where: {
                !$0.isEmoji
                    && !$0.isZeroWidthJoiner
            })
    }
    
    // Emoji's
    var emojiString: String {
        
        return emojiScalars.map { String($0) }.reduce("", +)
    }
    
    var emojis: [String] {
        
        var scalars: [[UnicodeScalar]] = []
        var currentScalarSet: [UnicodeScalar] = []
        var previousScalar: UnicodeScalar?
        
        for scalar in emojiScalars {
            
            if let prev = previousScalar, !prev.isZeroWidthJoiner && !scalar.isZeroWidthJoiner {
                
                scalars.append(currentScalarSet)
                currentScalarSet = []
            }
            currentScalarSet.append(scalar)
            
            previousScalar = scalar
        }
        
        scalars.append(currentScalarSet)
        
        return scalars.map { $0.map{ String($0) } .reduce("", +) }
    }
    
    fileprivate var emojiScalars: [UnicodeScalar] {
        
        var chars: [UnicodeScalar] = []
        var previous: UnicodeScalar?
        for cur in unicodeScalars {
            
            if let previous = previous, previous.isZeroWidthJoiner && cur.isEmoji {
                chars.append(previous)
                chars.append(cur)
                
            } else if cur.isEmoji {
                chars.append(cur)
            }
            
            previous = cur
        }
        
        return chars
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    func boletoMask() -> CustomStringMask {
        return CustomStringMask(mask: self[0] == "8" ? "00000000000-0 00000000000-0 00000000000-0 00000000000-0" : "00000.00000 00000.000000 00000.000000 0 00000000000000")
    }
    
    func snakeCased() -> String? {
        let pattern = "([a-z0-9])([A-Z])"
        let regex = try? NSRegularExpression(pattern: pattern, options: [])
        let range = NSRange(location: 0, length: count)
        return regex?.stringByReplacingMatches(in: self, options: [], range: range, withTemplate: "$1_$2").lowercased()
    }
    
    func removingWhiteSpaces() -> String {
        return self.replacingOccurrences(of: " ", with: "")
    }
}

extension NSString {
    var sanitized: String {
        // TODO: REMOVER QUANDO A APPLE CORRIGIR BUG MALDITO
        return self.replacingOccurrences(of: "జ్ఞ‌ా", with: "xx").replacingOccurrences(of: "జ్ఞ&zwnj;ా", with: "xx").replacingOccurrences(of: "జ్ఞ", with: "x").replacingOccurrences(of: ";ా", with: "x")
    }
}

extension UnicodeScalar {
    
    var isEmoji: Bool {
        
        switch value {
        case 0x1F600...0x1F64F, // Emoticons
        0x1F300...0x1F5FF, // Misc Symbols and Pictographs
        0x1F680...0x1F6FF, // Transport and Map
        0x1F1E6...0x1F1FF, // Regional country flags
        0x2600...0x26FF,   // Misc symbols
        0x2700...0x27BF,   // Dingbats
        0xFE00...0xFE0F,   // Variation Selectors
        0x1F900...0x1F9FF,  // Supplemental Symbols and Pictographs
        65024...65039, // Variation selector
        8400...8447: // Combining Diacritical Marks for Symbols
            return true
            
        default: return false
        }
    }
    
    var isZeroWidthJoiner: Bool {
        
        return value == 8205
    }
}

// For calculate height and width in views with text
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}

