//
//  UINavigationController+Extensions.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 27/02/18.
//

import Foundation

extension UINavigationController {
    func replace(_ vc: UIViewController, by secondVc: UIViewController?, animated: Bool = false) {
        guard let index = viewControllers.firstIndex(of: vc) else {
            return
        }
        
        var stack = viewControllers
        stack.remove(at: index)       // remove current VC
        if let secondVc = secondVc {
            stack.insert(secondVc, at: index) // add the new one
        }
        
        setViewControllers(stack, animated: animated) // boom!
    }
    
    public func pushViewController(viewController: UIViewController,
                                   animated: Bool,
                                   completion: (() -> Void)?) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        pushViewController(viewController, animated: animated)
        CATransaction.commit()
    }
    
    open override var childForStatusBarStyle: UIViewController? {
        return visibleViewController
    }
    
    func fadeTo(_ viewController: UIViewController) {
        let transition: CATransition = CATransition()
        transition.duration = 0.3
        transition.type = .fade
        view.layer.add(transition, forKey: nil)
        pushViewController(viewController, animated: false)
    }
    
    @objc
    public func fadeReplace(_ vc: UIViewController, by secondVc: UIViewController?) {
        let transition: CATransition = CATransition()
        transition.duration = 0.3
        transition.type = .fade
        view.layer.add(transition, forKey: nil)
        replace(vc, by: secondVc, animated: false)
    }
}
