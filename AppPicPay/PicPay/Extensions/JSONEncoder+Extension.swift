import Foundation

extension JSONEncoder {
    static func dictionary<T: Encodable>(_ object: T, strategy: JSONEncoder.KeyEncodingStrategy = .convertToSnakeCase) -> [String: Any]? {
        let jsonEncoder = JSONEncoder()
        jsonEncoder.keyEncodingStrategy = strategy
        
        guard let jsonData = try? jsonEncoder.encode(object.self),
            let dictionary = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String: Any] else {
                return nil
        }
        
        return dictionary
    }
    
    static func data<T: Encodable>(_ object: T, strategy: JSONEncoder.KeyEncodingStrategy = .convertToSnakeCase) -> Data? {
        let jsonEncoder = JSONEncoder()
        jsonEncoder.keyEncodingStrategy = strategy
        
        return try? jsonEncoder.encode(object.self)
    }
}
