import Foundation

extension DateFormatter {

    static let iso8601noFS = ISO8601DateFormatter()
    
    /// Ex: 2019-02-23 20:10:10
    static let iso8601Time: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [ISO8601DateFormatter.Options.withFullDate,
                                   ISO8601DateFormatter.Options.withDashSeparatorInDate,
                                   ISO8601DateFormatter.Options.withSpaceBetweenDateAndTime,
                                   ISO8601DateFormatter.Options.withTime,
                                   ISO8601DateFormatter.Options.withColonSeparatorInTime]
        return formatter
    }()
    
    /// Ex: 2019-02-23
    static let iso8601noTime: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withFullDate]
        return formatter
    }()

    static func brazillianFormatter(dateFormat: String = "dd/MM/yyyy") -> DateFormatter {
        let formatter = DateFormatter()
        formatter.calendar = Calendar.current
        formatter.locale = Locale(identifier: "pt_BR_POSIX")
        formatter.dateFormat = dateFormat
        return formatter
    }
    
    static func istFormatter() -> DateFormatter {
        return brazillianFormatter(dateFormat: "yyyy-MM-dd'T'HH:mm:ssZ")
    }
}
