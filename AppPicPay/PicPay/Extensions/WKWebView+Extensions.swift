//
//  WKWebView+Extensions.swift
//  PicPay
//
//  Created by Vagner Orlandi on 09/03/18.
//

import UIKit
import WebKit

extension WKWebView {
    func screenCapture() -> UIImage? {
        let fullViewSize = self.scrollView.contentSize
        UIGraphicsBeginImageContextWithOptions(fullViewSize, true, 0)
        self.drawHierarchy(in: CGRect(origin: .zero, size: fullViewSize), afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}

