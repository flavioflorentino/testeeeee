public extension NSMutableAttributedString {
    /// It appends a string value on the end of the NSMutableAttributedString value.
    /// - Parameter string: The appended string value.
    func appendString(_ string: String) {
        self.append(NSAttributedString(string: string))
    }
    
    /// Override the font of NSMutableAttributedString by other font respecting the original font traits.
    /// - Parameter font: The new font to be setted on it.
    func with(font: UIFont) -> NSMutableAttributedString {
        let fullRange = NSRange(location: 0, length: length)
        let key: NSAttributedString.Key = .font
        let enumerationOptions: NSAttributedString.EnumerationOptions = .longestEffectiveRangeNotRequired
        
        let enumerationBlock: (Any?, NSRange, UnsafeMutablePointer<ObjCBool>) -> Void = { value, range, _ in
            guard let originalFont = value as? UIFont,
                let newFont = self.applyTraitsFromFont(originalFont, to: font) else {
                    return
            }
            self.removeAttribute(.font, range: range)
            self.addAttribute(.font, value: newFont, range: range)
        }
        
        enumerateAttribute(key, in: fullRange, options: enumerationOptions, using: enumerationBlock)
        return self
    }
    
    /// Create a new font using the traits of the original font on the final font.
    /// - Parameters:
    ///   - originalFont: The copied traits' font.
    ///   - finalFont: The receiver of traits of the original font.
    private func applyTraitsFromFont(_ originalFont: UIFont, to finalFont: UIFont) -> UIFont? {
        let originalTrait = originalFont.fontDescriptor.symbolicTraits
        if let fontDescriptor = finalFont.fontDescriptor.withSymbolicTraits(originalTrait) {
            return UIFont(descriptor: fontDescriptor, size: 0)
        }
        return finalFont
    }
    
    /// This method searches for links on the text and returns the matches found.
    func getLinksOnText() throws -> [NSTextCheckingResult] {
        let pattern = "(?:(?:https?|ftp|file)://|www.|ftp.)(?:([-A-Z0-9+&@#/%=~_|$?!:,.]*)|[-A-Z0-9+&@#/%=~_|$?!:,.])*(?:([-A-Z0-9+&@#/%=~_|$?!:,.]*)|[A-Z0-9+&@#/%=~_|$])"
        let text = string
        let regex = try NSRegularExpression(pattern: pattern, options: [.caseInsensitive])
        let fullRange = NSRange(location: 0, length: length)
        return regex.matches(in: text, options: [], range: fullRange)
    }
    
    /// This method searches for links on the text and adds attributes on them.
    func searchAndAddAttributesOnLinks() {
        do {
            let matches = try getLinksOnText()
            addLinkAttributes(on: matches)
        } catch {
            Log.error(.app, error.localizedDescription)
        }
    }
    
    /// It adds link and foreground color attributes on specified arrays' content.
    /// - Parameter links: Links to be attributed.
    func addLinkAttributes(on links: [NSTextCheckingResult]) {
        for link in links {
            let text = string as NSString
            let substringValue = text.substring(with: link.range)
            setAsLink(substringValue, linkURL: substringValue)
        }
    }
}
