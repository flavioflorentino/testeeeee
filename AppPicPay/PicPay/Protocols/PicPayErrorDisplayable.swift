import Foundation
import SwiftyJSON
import Validator
import Core

protocol SwiftyJSONable {
    var object: Any { get }
    var dictionaryObject: [String: Any]? { get }
}

extension JSON: SwiftyJSONable { }

protocol PicPayErrorDisplayable: Error {
    var title: String { get }
    var message: String { get }
    var picpayCode: String { get }
    var messageId: Int? { get }
    var data: SwiftyJSONable? { get }
    var alert: Alert? { get }
    
    var localizedDescription: String { get }
    var description: String { get }

    func dataDictionary() -> [String: Any]?
}

extension Core.RequestError: PicPayErrorDisplayable {
    var description: String {
        message
    }
    
    var picpayCode: String {
        code
    }

    var data: SwiftyJSONable? {
        try? JSON(data: jsonData ?? Data())
    }
    
    func dataDictionary() -> [String: Any]? {
        try? JSONSerialization.jsonObject(with: self.jsonData ?? Data(), options: JSONSerialization.ReadingOptions.allowFragments) as? [String: Any]
    }

    var alert: Alert? {
        guard !alertJson.isEmpty else { return nil }
        
        let alert = Alert(json: JSON(alertJson))
        
        if alert?.text == nil {
            alert?.text = NSAttributedString(string: self.message)
        }
        
        if alert?.title == nil {
            alert?.title = NSAttributedString(string: self.title)
        }
        
        return alert
    }
}

extension Core.RequestError: LocalizedError {
    public var errorDescription: String? {
        NSLocalizedString(message, comment: "")
    }
}

extension Core.RequestError: ValidationError { }

extension Core.RequestError: SwiftyJSONable {
    var object: Any {
        do {
            return try JSONSerialization.jsonObject(with: jsonData ?? Data(), options: JSONSerialization.ReadingOptions.allowFragments) as Any
        } catch _ {
            return Data() as Any
        }
    }
    
    var dictionaryObject: [String: Any]? {
        let dict = try? JSONSerialization.jsonObject(with: jsonData ?? Data(), options: JSONSerialization.ReadingOptions.allowFragments) as? [String: Any] ?? [:]
        return dict
    }
}

extension Core.ApiError {
    var picpayError: PicPayError {
        guard
            let error = requestError,
            let data = (error as PicPayErrorDisplayable).data
            else {
                return PicPayError(message: DefaultLocalizable.unexpectedError.text)
        }
        
        return data.dictionaryObject?["Error"] == nil ? PicPayError(json: data) : PicPayError(oldJSON: data)
    }
}
