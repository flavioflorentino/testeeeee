protocol Coordinator: AnyObject {
    var currentCoordinator: Coordinator? { get set }
    func start()
}
