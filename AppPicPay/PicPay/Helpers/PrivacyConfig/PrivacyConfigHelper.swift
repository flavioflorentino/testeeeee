import Core
import FeatureFlag
import Foundation

struct PrivacyConfig {
    let shouldReview: Bool
    let apiResultIsSuccess: Bool
    let isReviewed: Bool
    let hasTransactions: Bool
}

final class PrivacyConfigHelper {
    typealias Dependencies = HasKVStore & HasFeatureManager & HasMainQueue
    private let dependencies: Dependencies
    private let apiSettings: PrivacySettingsApiProtocol
    private let updatePrivacyService: UpdatePrivacyServicing
    
    init(updatePrivacyService: UpdatePrivacyServicing,
         dependencies: Dependencies = DependencyContainer(),
         apiSettings: PrivacySettingsApiProtocol = PrivacySettingsApi()) {
        self.dependencies = dependencies
        self.apiSettings = apiSettings
        self.updatePrivacyService = updatePrivacyService
    }
    
    func setPaymentInPrivateConfig() {
        let config = dependencies.kvStore.intFor(KVKey.privacyCconfig)
        
        guard config == 0 else { return }
        
        apiSettings.getPrivacySettings { result in
            guard case .success(let privacySettings) = result, let paymentInPrivate = privacySettings.paymentInPrivate else { return }
            ConsumerManager.shared.privacyConfig = paymentInPrivate
                ? FeedItemVisibilityInt.private.rawValue
                : FeedItemVisibilityInt.friends.rawValue
        }
    }
    
    func checkReviewPrivacy(cameFromReceipt: Bool, completion: @escaping (_: PrivacyConfig?) -> Void) {
        if cameFromReceipt {
            checkReviewPrivacyAtReceipt(completion)
        } else {
            checkReviewPrivacyAtHome(completion)
        }
    }
    
    func checkReviewPrivacyAtReceipt(_ completion: @escaping (_: PrivacyConfig?) -> Void) {
        guard
            dependencies.featureManager.isActive(.isReviewPrivacyAvailable),
            !dependencies.kvStore.boolFor(KVKey.isUserReviewPrivacy)
        else {
            return completion(nil)
        }
        
        apiSettings.getPrivacySettings { result in
            switch result {
            case let .success(privacySettings):
                let privacyResult = PrivacyConfig(shouldReview: !privacySettings.reviewed,
                                                  apiResultIsSuccess: true,
                                                  isReviewed: privacySettings.reviewed,
                                                  hasTransactions: true)
                completion(privacyResult)
            case .failure:
                let privacyResult = PrivacyConfig(shouldReview: false,
                                                  apiResultIsSuccess: false,
                                                  isReviewed: false,
                                                  hasTransactions: false)
                completion(privacyResult)
            }
        }
    }
    
    func checkReviewPrivacyAtHome(_ completion: @escaping (_: PrivacyConfig?) -> Void) {
        guard dependencies.featureManager.isActive(.isReviewPrivacyHomeAvailable),
              !dependencies.kvStore.boolFor(KVKey.isUserReviewPrivacy) else {
            completion(nil)
            return
        }
                
        updatePrivacyService.checkRevisionStatus { result in
            switch result {
            case let .success(revisionStatus):
                let privacyResult = PrivacyConfig(shouldReview: !revisionStatus.reviewed && revisionStatus.hasTransactions,
                                                  apiResultIsSuccess: true,
                                                  isReviewed: revisionStatus.reviewed,
                                                  hasTransactions: revisionStatus.hasTransactions)
                completion(privacyResult)
            case .failure:
                let privacyResult = PrivacyConfig(shouldReview: false,
                                                  apiResultIsSuccess: false,
                                                  isReviewed: false,
                                                  hasTransactions: false)
                completion(privacyResult)
            }
        }
    }
}
