//
//  Log.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 25/04/18.
//

import Foundation

@objc final class LogDomain: NSObject {
    @objc public static let app = "App"
    @objc public static let view = "View"
    @objc public static let layout = "Layout"
    @objc public static let controller = "Controller"
    @objc public static let routing = "Routing"
    @objc public static let service = "Service"
    @objc public static let network = "Network"
    @objc public static let model = "Model"
    @objc public static let cache = "Cache"
    @objc public static let db = "DB"
    @objc public static let io = "IO"
}

@objc final class Log: NSObject {
    
    public struct Domain: RawRepresentable {
        public let rawValue: String
        public init(rawValue: String) { self.rawValue = rawValue }
        
        public static let app = Domain(rawValue: LogDomain.app)
        public static let view = Domain(rawValue: LogDomain.view)
        public static let layout = Domain(rawValue: LogDomain.layout)
        public static let controller = Domain(rawValue: LogDomain.controller)
        public static let routing = Domain(rawValue: LogDomain.routing)
        public static let service = Domain(rawValue: LogDomain.service)
        public static let network = Domain(rawValue: LogDomain.network)
        public static let model = Domain(rawValue: LogDomain.model)
        public static let cache = Domain(rawValue: LogDomain.cache)
        public static let db = Domain(rawValue: LogDomain.db)
        public static let io = Domain(rawValue: LogDomain.io)
        
        public static func custom(_ value: String) -> Domain {
            return Domain(rawValue: value)
        }
    }
    
    public struct Level: RawRepresentable {
        
        public let rawValue: Int
        public init(rawValue: Int) { self.rawValue = rawValue }
        
        public static let error = Level(rawValue: 0)
        public static let warning = Level(rawValue: 1)
        public static let important = Level(rawValue: 2)
        public static let info = Level(rawValue: 3)
        public static let debug = Level(rawValue: 4)
        public static let verbose = Level(rawValue: 5)
        public static let noise = Level(rawValue: 6)
        
        public static func custom(_ value: Int) -> Level {
            return Level(rawValue: value)
        }
    }
    
    // MARK: - Error
    public static func error(_ domain: Domain,_ message: String?,
                            _ file: String = #file,
                            _ line: Int = #line,
                            _ function: String = #function){
        Log.error([domain], message, file, line, function)
    }
    
    public static func error(_ domain: [Domain],_ message: String?,
                            _ file: String = #file,
                            _ line: Int = #line,
                            _ function: String = #function){
        Log.log(domain, .error, message, file, line, function)
    }
    
    // MARK: - Info
    @objc public static func info(domain: [String], message: String?,
                            file: String = #file,
                            line: Int = #line,
                            function: String = #function){
        var domains:[Domain] = []
        for rawValue in domain{
            domains.append(Domain(rawValue: rawValue))
        }
        Log.info(domains, message, file, line, function)
    }
    
    public static func info(_ domain: Domain,_ message: String?,
                            _ file: String = #file,
                            _ line: Int = #line,
                            _ function: String = #function){
        Log.info([domain], message, file, line, function)
    }
    
    public static func info(_ domain: [Domain],_ message: String?,
                            _ file: String = #file,
                            _ line: Int = #line,
                            _ function: String = #function){
        Log.log(domain, .info, message, file, line, function)
    }
    
    // MARK: - Debug
    public static func debug(_ domain: Domain,_ message: String?,
                             _ file: String = #file,
                             _ line: Int = #line,
                             _ function: String = #function){
        Log.debug([domain], message, file, line, function)
    }
    
    public static func debug(_ domain: [Domain],_ message: String?,
                             _ file: String = #file,
                             _ line: Int = #line,
                             _ function: String = #function){
        Log.log(domain, .debug, message, file, line, function)
    }
    
    
    
    public static func log(_ domain: [Domain],
                    _ level: Level,
                    _ message: String?,
                    _ file: String = #file,
                    _ line: Int = #line,
                    _ function: String = #function) {
        
        #if DEBUG
            print("📁 Files: \(file)")
            print(message ?? "")
        #endif
    }
}
