final class PPDeeplinkProfile: PPDeeplink {
    var profileId: String?
    
    init(url: URL) {
        let path = url.pathComponents.filter { $0 != "/" }
        if path.count >= 2 {
            profileId = path[1]
        }
        
        super.init(url: url)
        self.type = .profile
    }
}
