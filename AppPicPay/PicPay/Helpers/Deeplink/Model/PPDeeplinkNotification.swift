import Foundation

final class PPDeeplinkNotification: PPDeeplink {
    var landscreen: String?
    var notification: PPNotification?
    
    init(url: URL) {
        super.init(url: url)
        
        self.type = .notification
        
        // get notifcation params by url path
        let pathParams = url.pathComponents.filter({ $0 != "n" && $0 != "/" })
        
        if let landscreen = pathParams.first {
            self.landscreen = landscreen
        }
        
        if !pathParams.isEmpty {
            var notificationParams: [String] = ["2"]
            notificationParams.append(contentsOf: pathParams)
            self.notification = PPNotification(dictionaty: ["l": notificationParams], viaPush: true)
        }
    }
}
