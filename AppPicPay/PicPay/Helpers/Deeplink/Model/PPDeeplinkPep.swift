import Foundation

final class PPDeeplinkPep: PPDeeplink {
    let origin: Origin?
    let status: Status?
    
    init(url: URL) {
        let originParam = url.queryParams["origin"] ?? ""
        let statusParam = url.queryParams["status"] ?? ""
        
        self.origin = Origin(rawValue: originParam)
        self.status = Status(rawValue: statusParam)
        
        super.init(url: url)
        self.type = .pep
    }
}

extension PPDeeplinkPep {
    enum Status: String {
        case restricted = "RESTRICTED"
        case limited = "LIMITED"
    }
    
    enum Origin: String {
        case feed = "FEED"
        case wallet = "WALLET"
        case notification = "NOTIFICATION"
        case popup = "POPUP"
    }
}
