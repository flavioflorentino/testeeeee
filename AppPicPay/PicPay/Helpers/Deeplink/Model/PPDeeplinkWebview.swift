final class PPDeeplinkWebview: PPDeeplink {
    var destinationUrl: String?
    
    init(url: URL) {
        super.init(url: url)
        if let websiteUrl = queryParams["url"], isPicpayWebsiteUrl(websiteUrl) {
            destinationUrl = websiteUrl
        }
        self.type = .webview
    }
    
    private func isPicpayWebsiteUrl(_ urlString: String) -> Bool {
        let urlRegex = "https?://(.+\\.)*(picpay\\.com|ppay\\.me)(.+/?)*"
        let urlTest = NSPredicate(format: "SELF MATCHES %@", urlRegex)
        return urlTest.evaluate(with: urlString)
    }
}
