import Foundation

final class PPDeeplinkCielo: PPDeeplink {
    init(url: URL) {
        super.init(url: url)
        self.type = .cielo
    }
}
