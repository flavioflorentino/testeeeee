final class PPDeeplinkAccelerateAnalysis: PPDeeplink {
    let transactionId: String?
    let transactionType: String?
    
    init(url: URL) {
        let data = url.queryParams
        transactionId = data["transactionId"]
        transactionType = data["transactionType"]
        super.init(url: url)
        self.type = .accelerateAnalysis
    }
}
