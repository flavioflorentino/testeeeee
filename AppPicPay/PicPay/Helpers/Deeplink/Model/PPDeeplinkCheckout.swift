import UIKit

final class PPDeeplinkCheckout: PPDeeplink {
    var orderId: String?
    
    init(url: URL) {
        super.init(url: url)
        
        self.type = .checkout
        
        let data = url.queryParams
        
        let a = url.path.split(separator: "/")
        if a.count > 1 {
            var id = String(describing: a[1])
            if let decodedData = Data(base64Encoded: id),
                let decodedString = String(data: decodedData, encoding: .utf8) {
                id = decodedString
            }
            
            self.orderId = id
        }
        
        if let orderId = data["orderId"] {
            self.orderId = orderId
        }
    }
}
