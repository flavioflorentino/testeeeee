import Foundation

final class PPDeeplinkIdentityAnalysis: PPDeeplink {
    init(url: URL) {
        super.init(url: url)
        
         self.type = .identityAnalysis
    }
}
