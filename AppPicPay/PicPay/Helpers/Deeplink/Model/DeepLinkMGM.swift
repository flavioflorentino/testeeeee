final class DeepLinkMGM: PPDeeplink {
    enum Path: String {
        case mgmInvited = "/mgm/mgm_invited"
    }
    
    var name: String?
    var isFromReferralRecommendation: Bool = false
    
    init(url: URL) {
        super.init(url: url)
        
        guard
            let components = URLComponents(url: url, resolvingAgainstBaseURL: true),
            let queryItems = components.queryItems,
            components.path == Path.mgmInvited.rawValue,
            let name = queryItems.first(where: { $0.name == "mgm_inviter_name" })?.value else {
                return
        }
        
        isFromReferralRecommendation = true
        self.name = name.replacingOccurrences(of: "+", with: " ")
    }
}
