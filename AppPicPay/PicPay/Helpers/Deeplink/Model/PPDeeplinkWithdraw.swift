import Foundation

final class PPDeepLinkWithdraw: PPDeeplink {
    var id: String?
    
    init(url: URL) {
        self.id = url.queryParams["id"]
        
        super.init(url: url)
        self.type = .withdraw
    }
}
