import Foundation

final class PPDeeplinkSearch: PPDeeplink {
    var searchSubTab: SearchSubTab?
    
    init(url: URL) {
        super.init(url: url)
        self.type = .search
        searchSubTab = setSearchSubTab(of: url)
    }
}

extension PPDeeplinkSearch {
    private func setSearchSubTab(of url: URL) -> SearchSubTab {
        let paramsfiltered = url.pathComponents.filter {
            let notN = $0 != "n"
            let notBar = $0 != "/"
            let notSearch = $0 != "search"
            return notN && notBar && notSearch
        }
        
        let data = url.queryParams
        var text = data["text"] ?? ""
        text = text.replacingOccurrences(of: "\"", with: "")
        
        guard let firstParam = paramsfiltered.first, firstParam.isNotEmpty else {
            return SearchSubTab(title: "main", text: text)
        }
        
        return SearchSubTab(title: firstParam, text: text)
    }
}

struct SearchSubTab {
    let title: String
    let text: String
}
