import Foundation

final class PPDeeplinkChallenge: PPDeeplink {
    let typeChallenge: String?
    let id: String?
    
    init(url: URL) {
        self.typeChallenge = url.queryParams["type"]
        self.id = url.queryParams["id"]
        
        super.init(url: url)
        self.type = .challenge
    }
}
