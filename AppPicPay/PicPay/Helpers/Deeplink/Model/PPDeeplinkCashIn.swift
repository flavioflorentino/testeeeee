import Foundation

final class PPDeeplinkCashIn: PPDeeplink {
    enum CashInDeeplinkRoute: String {
        case receipt = "transferreceipt"
        case incentive
    }
    
    var id: String?
    var route: CashInDeeplinkRoute?
    
    init?(url: URL) {
        let path = url.pathComponents.filter { $0 != "/" }
        guard path.count >= 2 else { return nil }
        
        route = CashInDeeplinkRoute(rawValue: path[path.count - 2])
        id = path.last
        
        super.init(url: url)
        self.type = .cashin
    }
}
