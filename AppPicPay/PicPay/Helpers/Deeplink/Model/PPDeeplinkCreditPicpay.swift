import Foundation

final class PPDeeplinkCreditPicpay: PPDeeplink {
    var destination: Destination?
    
    init(url: URL) {
        super.init(url: url)
        self.type = .creditPicpay
        destination = setUpDestination(of: url)
    }
}

extension PPDeeplinkCreditPicpay {
    enum Destination {
        case invoice(String)
        case registration
        case home
        case limit
    }
    
    enum DestinationParam: String {
        case card
        case home
        case registration
        case invoice
        case limit
    }
    
    private func setUpDestination(of url: URL) -> Destination? {
        let paramsfiltered = url.pathComponents.filter { $0 != "n" && $0 != "/" && $0 != "credit" }
        
        guard let firstParam = paramsfiltered.first,
            let destinationParam = DestinationParam(rawValue: firstParam) else {
            return nil
        }
        
        switch destinationParam {
        case .card:
            guard let last = paramsfiltered.last, last == "upgrade-debit-to-credit" else {
                return nil
            }
            return .registration
        case .home:
            return .home
        case .registration:
            return .registration
        case .invoice:
            let positionOfId: Int = 1
            guard paramsfiltered.indices.contains(positionOfId) else {
                return nil
            }
            let id = paramsfiltered[positionOfId]
            return.invoice(id)
        case .limit:
            return .limit
        }
    }
}
