import Foundation

final class PPDeeplinkJudicialBlock: PPDeeplink {
    let id: String?
    let origin: String
    
    init(url: URL) {
        self.id = url.queryParams["id"]
        self.origin = url.queryParams["origin"] ?? ""
        
        super.init(url: url)
        self.type = .judicialblock
    }
}
