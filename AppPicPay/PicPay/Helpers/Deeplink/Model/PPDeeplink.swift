import Foundation
import UIKit

@objc
enum PPDeeplinkType: Int {
    case payment = 0
    case notification = 1
    case picpayMe = 2
    case checkout = 3
    case identityAnalysis = 5
    case linkedAccounts = 6
    case cielo = 7
    case creditPicpay = 8
    case promo = 9
    case invite = 10
    case places = 11
    case store = 12
    case recharge = 13
    case search = 14
    case bill = 15
    case profile = 16    
    case upgradeChecklist = 17
    case accelerateAnalysis = 18
    case studentAccount = 19
    case webview = 20
    case myQrcode = 21
    case cardRegistration = 22
    case chargePerson = 23
    case feed = 24
    case promotions = 25
    case favorites = 26
    case cancellationConfirmation = 27
    case judicialblock = 28
    case promotionDetails = 29
    case pep = 30
    case mgm = 31
    case placeIndication = 32
    case savings = 33
    case challenge = 34
    case wallet = 36
    case registrationRenewal = 37
    case cashin = 38
    case userstatement = 39
    case withdraw = 40
    case passwordReset = 41
    case incomeStatements = 42
    case appProtection = 43
    case receipt = 44
    case billTimeline = 45
    case socialFeed = 46
    case payPeople = 47
    case invoiceCashback = 48
    case none = -1
}

class PPDeeplink: NSObject {
    var type: PPDeeplinkType = .none
    var path: String
    var queryParams: [String: String] = [:]
    var track: DeeplinkTrack?
    private let worker: DeeplinkWorkerProtocol
    
    init(url: URL, worker: DeeplinkWorkerProtocol = DeeplinkWorker()) {
        self.path = url.path
        self.queryParams = url.queryParams
        self.worker = worker
        
        super.init()
        
        track = deeplinkTrack(queryParams: queryParams)
    } 
    
    convenience init(url: URL, type: PPDeeplinkType) {
        self.init(url: url)
        self.type = type
    }
    
    func trackEvent() {
        guard let track = self.track else {
            return
        }
        let title = track.title
        let type = track.type
        let properties = track.propertiesDictionary()
        
        switch type {
        case .normal:
            PPAnalytics.trackEvent(title, properties: properties)
        case .firstTimeOnly:
            PPAnalytics.trackFirstTimeOnlyEvent(title, properties: properties)
        }
    }
    
    private func deeplinkTrack(queryParams: [String: String]) -> DeeplinkTrack? {
        guard let id = queryParams["analytics"], let listTrack = worker.getDeeplinkTrack() else {
            return nil
        }
        
        let track = listTrack.filter { $0.id == id }
        
        return track.isEmpty ? nil : track.first
    }
}
