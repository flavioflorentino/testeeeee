import Foundation

private extension PPDeeplinkStore.Constants {
    enum ParameterName {
        static let noRedirect: String = "noRedirect"
    }
}

final class PPDeeplinkStore: PPDeeplink {
    fileprivate enum Constants { }

    // MARK: - Properties
    let noRedirect: Bool
    var dgItem: DGItem?

    // MARK: - Initialization
    init(url: URL) {
        let value = (url.queryParams[Constants.ParameterName.noRedirect] as NSString?)?.boolValue
        self.noRedirect = value ?? false

        super.init(url: url)
        self.type = .store

        let path = url.pathComponents.filter { $0 != "/" }
        if path.count == 2 {
            self.dgItem = DGItem(id: path[1])
        }
    }
}
