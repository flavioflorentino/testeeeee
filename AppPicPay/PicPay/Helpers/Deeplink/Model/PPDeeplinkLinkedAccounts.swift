import Foundation

final class PPDeeplinkLinkedAccounts: PPDeeplink {
    init(url: URL) {
        super.init(url: url)
        self.type = .linkedAccounts
    }
}
