import Foundation

final class PPDeeplinkMe: PPDeeplink {
    var username = ""
    
    init(url: URL) {
        super.init(url: url)
        self.type = .picpayMe
        handleHost(url: url)
    }
    
    private func handleHost(url: URL) {
        guard let host = url.host else {
            return
        }
        let components = url.path.split(separator: "/")
        switch host {
        case "app.picpay.com":
            configureDeeplink(components: components, initialPosition: 1)
        default:
            configureDeeplink(components: components)
        }
    }
    
    private func configureDeeplink(components: [Substring], initialPosition: Int = 0) {
        if components.count > initialPosition {
            username = String(components[initialPosition])
        }
        if components.count > initialPosition + 1 {
            queryParams["payment_value"] = String(components[initialPosition + 1])
        }
    }
}
