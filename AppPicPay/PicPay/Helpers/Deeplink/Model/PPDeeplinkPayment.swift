import Foundation
final class PPDeeplinkPayment: PPDeeplink {
    enum PaymentType: String {
        case user = "user"
        case store = "store"
        case digitalGoods = "digital_good"
    }
    
    var id: String?
    var sellerId: String?
    var transactionHash: String?
    var store: PPStore?
    var user: PPContact?
    var dgItem: DGItem?
    var value: Double?
    var paymentType: PaymentType = .user
    var fromScanner: Bool = false
    
    init(url: URL, fromScanner: Bool = false) {
        super.init(url: url)
        self.fromScanner = fromScanner
        let data = url.queryParams
        
        self.type = .payment
        
        if let id = data["id"] {
            self.id = id
        }
        
        if let sellerId = data["sellerId"] {
            self.sellerId = sellerId
        }
        
        if let transactionHash = data["hash"] {
            self.transactionHash = transactionHash
        }
        
        if let stringValue = data["value"], let value = Double(stringValue) {
            self.value = value / 100
        }
        
        if let type = data["type"], let paymentType = PaymentType(rawValue: type) {
            self.paymentType = paymentType
            self.configDeeplinkPaymentBy(paymentType: paymentType, data: data)
        }
    }
    
    private func configDeeplinkPaymentBy(paymentType: PaymentType, data: [String: String]) {
        switch paymentType {
        case .user:
            guard let id = id else {
                return
            }
            self.user = PPContact()
            self.user?.wsId = Int(id) ?? 0
        case .store:
            guard let sellerId = sellerId else {
                return
            }
            self.store = PPStore()
            self.store?.sellerId = sellerId
        case .digitalGoods:
            guard let id = id else {
                return
            }
            self.dgItem = DGItem(id: id)
            self.dgItem?.service = DGService(rawValue: data["service"] ?? "")
            self.dgItem?.sellerId = self.sellerId ?? ""
        }
    }
}
