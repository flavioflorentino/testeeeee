import Core
import FeatureFlag
import SwiftyJSON
import UIKit

final class DeepLinkApi: BaseApi {
    typealias Dependencies = HasFeatureManager
    func get(username: String, completion: @escaping ((PicPayResult<UsernameResponse>) -> Void)) {
        let dependencies: Dependencies = DependencyContainer()
        if !dependencies.featureManager.isActive(.transitionApiObjToCore) {
            let params = ["profile_id": username, "profile_type": "deep_link"]
            requestManager
                .apiRequest(endpoint: WebServiceInterface.apiEndpoint(kWsUrlGetProfile), method: .post, parameters: params)
                .responseApiObject(completionHandler: completion)
        } else {
            getNewApi(username, completion: completion)
        }
    }
}

private extension DeepLinkApi {
    private func getNewApi(_ username: String, completion: @escaping (PicPayResult<UsernameResponse>) -> Void) {
        let endpoint = DeepLinkEndpoint.profile(id: username)
        Api<Data>(endpoint: endpoint).execute { result in
            ApiSwiftJsonWrapper(with: result).transform(completion)
        }
    }
}

private extension DeepLinkApi {
    enum DeepLinkEndpoint: ApiEndpointExposable {
        case profile(id: String)
        
        var path: String {
            "api/getProfile.json"
        }
        
        var method: HTTPMethod {
            .post
        }
        
        var body: Data? {
            guard case .profile(let id) = self else {
                return nil
            }
            return ["profile_id": id, "profile_type": "deep_link"].toData()
        }
    }
}
