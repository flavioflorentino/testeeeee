import Foundation
import SwiftyJSON

final class UsernameResponse: BaseApiResponse {
    enum ContentType: String {
        case consumer
        case membership
        case unknown
    }
    
    var type: ContentType = .unknown
    var data: Any?
    
    required init?(json: JSON) {
        guard let type = json["data"]["type"].string else {
            return
        }
        
        if let contentType = ContentType(rawValue: type) {
            self.type = contentType
        }
        
        switch self.type {
        case .consumer:
            guard let profile = json["data"]["payload"]["profile"].dictionaryObject else {
                return
            }
            self.data = PPContact(profileDictionary: profile)
        case .membership:
            self.data = ProducerProfileItem(json: json["data"]["payload"])
        default:
            return
        }
    }
}
