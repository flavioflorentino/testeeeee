import Core

final class DeeplinkResolversHelper {
    static var resolvers = [DeeplinkResolver]()
    
    static func findValidResolverWith(_ url: URL) -> DeeplinkResolver? {
        return resolvers
            .first(where: { resolver in
                guard url.pathComponents.filter({ $0 != "/" }).isNotEmpty else {
                    return false
                }
                let result = resolver.canHandle(url: url, isAuthenticated: User.isAuthenticated)
                let expectedResults: [DeeplinkResolverResult] = [.handleable, .onlyWithAuth]
                return expectedResults.contains(result)
            })
    }
    
    static func resolve(_ resolver: DeeplinkResolver, url: URL) -> Bool {
        PPDeeplink(url: url, type: .none).trackEvent()
        let resolverResultType = resolver.canHandle(url: url, isAuthenticated: User.isAuthenticated)
        
        if case .handleable = resolverResultType {
            return resolver.open(url: url, isAuthenticated: User.isAuthenticated)
        } else if case .onlyWithAuth = resolverResultType {
            DeeplinkHelper.storeDeepLink(link: url)
            return false
        }
        
        return false
    }
    
    static func register(_ resolver: DeeplinkResolver) {
        resolvers.append(resolver)
    }

    static func register(_ resolvers: [DeeplinkResolver]) {
         self.resolvers.append(contentsOf: resolvers)
     }
}
