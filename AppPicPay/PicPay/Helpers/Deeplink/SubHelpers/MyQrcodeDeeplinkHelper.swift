final class MyQrcodeDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return goToMyQrcode()
    }
    
    private func goToMyQrcode() -> Bool {
        PaginationCoordinator.shared.showMyQrCode()
        return true
    }
}
