final class CardRegistrationDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return goAddNewCard()
    }
    
    private func goAddNewCard() -> Bool {
        guard let walletNavigation = AppManager.shared.mainScreenCoordinator?.walletNavigation(),
            let addCreditCardVC = ViewsManager.paymentMethodsStoryboardViewController(withIdentifier: "AddNewCreditCardViewController") as? AddNewCreditCardViewController else {
            return false
        }
        AppManager.shared.mainScreenCoordinator?.showWalletScreen()
        
        addCreditCardVC.viewModel = AddNewCreditCardViewModel(origin: .deeplink)
        addCreditCardVC.hidesBottomBarWhenPushed = true
        walletNavigation.pushViewController(addCreditCardVC, animated: false)
        return true
    }
}
