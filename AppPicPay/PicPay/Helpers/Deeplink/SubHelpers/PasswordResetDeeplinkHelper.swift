import Foundation
import PasswordReset

final class PasswordResetDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        let pathComponents = deeplink.path.components(separatedBy: "/")
        guard let path = pathComponents.last, let viewController = controller else { return false }

        let coordinator: PasswordResetCoordinatorProtocol
        if path == "passwordreset" || path == "" {
            coordinator = PasswordResetCoordinator(presenterController: viewController)
        } else {
            coordinator = PasswordResetDeeplinkCoordinator(originController: controller, code: path)
        }

        coordinator.start()
        SessionManager.sharedInstance.currentCoordinating = coordinator

        return true
    }
}
