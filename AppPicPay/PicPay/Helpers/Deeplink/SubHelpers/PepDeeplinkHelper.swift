import UI

final class PepDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        guard
            let deeplink = deeplink as? PPDeeplinkPep,
            let origin = deeplink.origin,
            let status = deeplink.status
            else {
                return false
        }
        
        let viewController: UIViewController
        switch status {
        case .limited:
            viewController = LimitedAccountFactory.make(origin: origin.rawValue)
        case .restricted:
            viewController = RestrictedAccountFactory.make(origin: origin.rawValue)
        }
        
        let navigation = UINavigationController(rootViewController: viewController)
        navigation.modalPresentationStyle = .overFullScreen
        navigation.view.backgroundColor = Palette.ppColorGrayscale000.color
        
        if #available(iOS 11.0, *) {
            viewController.navigationItem.largeTitleDisplayMode = .automatic
            navigation.navigationBar.prefersLargeTitles = true
        }
        
        let mainScreen = AppManager.shared.mainScreenCoordinator?.mainScreen()
        if let from = controller ?? mainScreen {
            from.present(navigation, animated: true)
        }
        
        return true
    }
}
