final class WebviewDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return setupWebview(deeplink, from: controller)
    }
    
    private func setupWebview(_ deeplink: PPDeeplink, from fromViewController: UIViewController?) -> Bool {
        let mainScreen = AppManager.shared.mainScreenCoordinator?.mainScreen()
        
        guard let webviewDeeplink = deeplink as? PPDeeplinkWebview,
            let webviewUrl = webviewDeeplink.destinationUrl,
            let url = URL(string: webviewUrl),
            let from = fromViewController ?? mainScreen else {
            return false
        }
  
         let webview = WebViewFactory.make(with: url)
        
        from.present(PPNavigationController(rootViewController: webview), animated: true)
        return true
    }
}
