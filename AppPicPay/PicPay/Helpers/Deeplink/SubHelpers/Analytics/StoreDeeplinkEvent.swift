import AnalyticsModule
import Foundation

enum StoreDeeplinkEvent: AnalyticsKeyProtocol {
    case didCallDeeplink(params: PAVPaymentHashInfo)
    case didCallDeeplinkWithoutHash(params: [String: String])
    
    func event() -> AnalyticsEventProtocol {
        let properties: [String: Any]
        
        switch self {
        case let .didCallDeeplink(params):
            properties = [
                "origin_id": params.originId,
                "value": params.totalValue ?? "",
                "created_at": params.date,
                "fixed_value": params.fixedValue,
                "origin": params.origin ?? "",
                "seller_id": params.sellerId,
                "consumer_id": ConsumerManager.shared.consumer?.wsId ?? ""
            ]
            
        case let .didCallDeeplinkWithoutHash(params):
            properties = params
        }
        
        return AnalyticsEvent("Link - Payment Done", properties: properties, providers: [.mixPanel])
    }
}
