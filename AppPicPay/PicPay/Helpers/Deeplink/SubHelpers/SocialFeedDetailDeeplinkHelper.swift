import UIKit
import Feed

final class SocialFeedDetailDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        guard let cardId = getCardID(withPath: deeplink.path),
              let cardUUID = UUID(uuidString: cardId),
              let homeNavViewController = AppManager.shared.mainScreenCoordinator?.homeNavigation() else {
            return false
        }
        
        let mainFeedFlowCoordinator = MainFeedFlowCoordinator(viewController: homeNavViewController)
        let detailController = mainFeedFlowCoordinator.startFeedDetail(cardId: cardUUID)
        detailController.hidesBottomBarWhenPushed = true
        homeNavViewController.pushViewController(detailController, animated: true)
        return true
    }
}

private extension SocialFeedDetailDeeplinkHelper {
    func getCardID(withPath path: String) -> String? {
        path.split(separator: "/")
            .last { $0 != DeeplinkType.socialFeedDetail.rawValue }
            .map { String($0) }
    }
}
