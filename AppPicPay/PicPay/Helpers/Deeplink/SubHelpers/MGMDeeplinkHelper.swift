import Core

final class MGMDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        guard let mgmDeepLink = deeplink as? DeepLinkMGM,
            let inviterName = mgmDeepLink.name else {
            return false
        }
        saveInviterData(isFromReferralRecommendation: mgmDeepLink.isFromReferralRecommendation, inviterName: inviterName)
        
        return true
    }
    
    private func saveInviterData(isFromReferralRecommendation: Bool, inviterName: String) {
        KVStore().setBool(isFromReferralRecommendation, with: .isFromReferralRecommendation)
        KVStore().setString(inviterName, with: .inviterName)
    }
}
