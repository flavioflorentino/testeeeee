import UIKit
import FeatureFlag

final class IdentityAnalysisDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        BreadcrumbManager.shared.addCrumb("IdentityAnalysis", typeOf: .deepLink)
        
        guard FeatureManager.isActive(.identityVerification),
            let mainScreen = AppManager.shared.mainScreenCoordinator?.mainScreen() else {
            return false
        }
        let coordinator = IdentityValidationFlowCoordinator(from: mainScreen, originFlow: .settings)
        coordinator.start()
        SessionManager.sharedInstance.currentCoordinator = coordinator
        return true
    }
}
