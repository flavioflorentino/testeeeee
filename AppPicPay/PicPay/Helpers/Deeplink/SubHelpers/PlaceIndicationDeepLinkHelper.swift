import UIKit

final class PlaceIndicationDeepLinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return openPlaceIndication(fromController: controller)
    }
    
    private func openPlaceIndication(fromController: UIViewController?) -> Bool {
        let mainScreen = AppManager.shared.mainScreenCoordinator?.currentController()

        guard let controller = fromController ?? mainScreen else {
            return false
        }
        
        let placeIndicationController = PlaceIndicationFactory.make()
        let navigation = PPNavigationController(rootViewController: placeIndicationController)

        controller.present(navigation, animated: true)
        
        return true
    }
}
