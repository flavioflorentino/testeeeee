import UIKit
import Billet

final class BillDeeplinkHelper: DeeplinkHelperProtocol {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return setupBillDeepLink(deeplink)
    }
}

private extension BillDeeplinkHelper {
    func setupBillDeepLink(_ deeplink: PPDeeplink) -> Bool {
        guard let paymentNavigation = AppManager.shared.mainScreenCoordinator?.paymentSearchNavigation() else {
            return false
        }

        var viewController: UIViewController = paymentNavigation

        AppManager.shared.mainScreenCoordinator?.showPaymentScreen()

        if let searchController = paymentNavigation.viewControllers.first as? SearchWrapper {
            searchController.selectMainTabBar(tab: .store)
            viewController = searchController
        }

        
        guard let linecode = getLinecode(withPath: deeplink.path) else {
            openHub(with: viewController)
            return true
        }

        let formController = BilletFormFactory.make(with: .deeplink, linecode: linecode)
        let billetNavigation = PPNavigationController(rootViewController: formController)
        viewController.present(billetNavigation, animated: true)

        return true
    }

    func openHub(with viewController: UIViewController) {
        guard dependencies.featureManager.isActive(.isNewBilletHubAvailable) else {
            DGHelpers.openBoletoFlow(viewController: viewController, origin: "deeplink")
            return
        }
        
        let hubViewController = BilletHubFactory.make(with: .deeplink)
        let navigation = UINavigationController(rootViewController: hubViewController)
        viewController.present(navigation, animated: true)
    }
    
    func getLinecode(withPath path: String) -> String? {
        path.split(separator: "/")
            .first(where: { $0 != DeeplinkType.boleto.rawValue })
            .map({ String($0) })
    }
}
