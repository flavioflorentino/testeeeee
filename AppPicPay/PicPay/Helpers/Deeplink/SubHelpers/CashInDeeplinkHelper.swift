import UIKit

enum IncentiveId: String {
    case upgrade = "upgradeaccount"
    case reminder = "rememberaccount"
}

final class CashInDeeplinkHelper: DeeplinkHelperProtocol {
    weak var viewController: UIViewController?
    
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        guard let rechargeDeeplink = deeplink as? PPDeeplinkCashIn else { return false }
        viewController = controller ?? AppManager.shared.mainScreenCoordinator?.mainScreen()
        
        return handle(rechargeDeeplink)
    }
}

private extension CashInDeeplinkHelper {
    func handle(_ deeplink: PPDeeplinkCashIn) -> Bool {
        guard let id = deeplink.id else {
            return false
        }
        
        switch deeplink.route {
        case .receipt:
            openReceipt(with: id)
        case .incentive:
            guard let id = IncentiveId(rawValue: id) else { return false }
            openIncentive(for: id)
        case .none:
            return false
        }
        
        return true
    }
    
    func openReceipt(with id: String) {
        let controller = WireTransferReceiptFactory.make(receiptId: id)
        let navigation = UINavigationController(rootViewController: controller)
        viewController?.present(navigation, animated: true)
    }
    
    func openIncentive(for id: IncentiveId) {
        let controller = RechargeInfoFactory.make(for: id)
        let navigation = UINavigationController(rootViewController: controller)
        viewController?.present(navigation, animated: true)
    }
}
