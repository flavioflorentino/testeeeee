import UIKit

final class CheckoutDeepLinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        if let deepLinkCheckout = deeplink as? PPDeeplinkCheckout {
            return setupCheckoutDeepLink(deepLinkCheckout)
        }
        return false
    }
    
    private func setupCheckoutDeepLink(_ deeplink: PPDeeplinkCheckout) -> Bool {
        guard let orderId = deeplink.orderId, let mainScreen = AppManager.shared.mainScreenCoordinator?.mainScreen() else {
            return false
        }
        BreadcrumbManager.shared.addCrumb("Checkout", typeOf: .deepLink)
        
        let controller = EcommerceLoadingFactory.make(orderId: orderId, origin: .deeplink)
        let navigationController = PPNavigationController(rootViewController: controller)
        mainScreen.present(navigationController, animated: true)
        
        return true
    }
}
