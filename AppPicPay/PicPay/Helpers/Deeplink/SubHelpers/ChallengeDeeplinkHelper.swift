import Foundation

final class ChallengeDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        guard
            let deeplink = deeplink as? PPDeeplinkChallenge,
            let type = deeplink.typeChallenge,
            let typeChallenge = Challenge(rawValue: type),
            let mainScreen = AppManager.shared.mainScreenCoordinator?.mainScreen()
        else {
            return false
        }
        
        let viewController = typeChallenge == .identity ?
            IdentityChallengeViewController() : CardChallengeViewController(cardId: deeplink.id ?? "")
    
        let navigationController = UINavigationController(rootViewController: viewController)
        
        mainScreen.present(navigationController, animated: true)
        return true
    }
}

extension ChallengeDeeplinkHelper {
    enum Challenge: String {
        case card = "cardverification"
        case identity = "identityanalysis"
    }
}
