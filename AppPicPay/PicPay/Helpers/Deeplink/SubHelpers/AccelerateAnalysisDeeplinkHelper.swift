import UIKit

final class AccelerateAnalysisDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return setupAccelerateAnalysisDeeplink(deeplink, from: controller)
    }
    
    private func setupAccelerateAnalysisDeeplink(_ deeplink: PPDeeplink, from fromViewController: UIViewController?) -> Bool {
        guard let analysisAcceleration = ViewsManager.antiFraudStoryboardViewController(withIdentifier: "AnalysisAccelerationViewController") as? AnalysisAccelerationViewController,
            let deeplink = deeplink as? PPDeeplinkAccelerateAnalysis,
            let transactionId = deeplink.transactionId,
            let type = deeplink.transactionType else {
                return false
        }
        analysisAcceleration.transactionId = transactionId
        analysisAcceleration.actionType = type
        let navController = DismissibleNavigationViewController(rootViewController: analysisAcceleration)
        
        let mainScreen = AppManager.shared.mainScreenCoordinator?.mainScreen()
        if let from = fromViewController ?? mainScreen {
            from.present(navController, animated: true)
            return true
        }
        return false
    }
}
