import Foundation

final class UpgradeChecklistDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return goToUpgradeChecklist()
    }
    
    private func goToUpgradeChecklist() -> Bool {
        guard let navigationController = AppManager.shared.mainScreenCoordinator?.homeNavigation() else {
            return false
        }
        let coordinador = UpgradeCoordinator(navigationController: navigationController)
        coordinador.start()
        SessionManager.sharedInstance.currentCoordinator = coordinador
        return true
    }
}
