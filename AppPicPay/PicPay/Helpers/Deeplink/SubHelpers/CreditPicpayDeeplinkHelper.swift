import UIKit
import FeatureFlag
import Card

final class CreditPicpayDeeplinkHelper: DeeplinkHelperProtocol {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies =  DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        if let deeplink = deeplink as? PPDeeplinkCreditPicpay {
            return setUpCreditPicpayDeepLink(deeplink)
        }
        return false
    }
    
    
}
private extension CreditPicpayDeeplinkHelper {
    func setupCreditLimit(){
        guard let controller = getNavigationController() else {
            return
        }
        let limitCoordinator = CreditLimitCoordinator(from: controller)
        SessionManager.sharedInstance.currentCoordinator = limitCoordinator
        limitCoordinator.start()
    }
    
    func getNavigationController() -> UINavigationController? {
        let currentController = AppManager.shared.mainScreenCoordinator?.currentController()
        currentController?.navigationController?.popToRootViewController(animated: true)
        return currentController?.navigationController
    }
    
    func setUpCreditPicpayDeepLink(_ deeplink: PPDeeplinkCreditPicpay) -> Bool {
        guard let destination = deeplink.destination else {
            return false
        }
        switch destination {
        case .home:
            AppManager.shared.mainScreenCoordinator?.showWalletScreen()
            let walletController = AppManager.shared.mainScreenCoordinator?.currentController()
            walletController?.navigationController?.popToRootViewController(animated: true)
            guard let navigationController = walletController?.navigationController else {
                return false
            }
            let creditCoordinator = CreditPicPayCoordinator(navigationController: navigationController)
            SessionManager.sharedInstance.currentCoordinator = creditCoordinator
            creditCoordinator.start()
            
        case .registration:
            AppManager.shared.mainScreenCoordinator?.showWalletScreen()
            let walletController = AppManager.shared.mainScreenCoordinator?.currentController()
            let viewModel = CreditViewModel()
            let controller = CreditViewController(with: viewModel)
            let navigationController = PPNavigationController(rootViewController: controller)
            navigationController.setNavigationBarHidden(true, animated: false)
            navigationController.presentationController?.delegate = walletController as? UIAdaptivePresentationControllerDelegate
            walletController?.navigationController?.present(navigationController, animated: true, completion: nil)
            
        case .invoice(let id):
            guard let navigationController = getNavigationController() else {
                return false
            }
            let creditCoordinator = CreditPicPayCoordinator(navigationController: navigationController)
            SessionManager.sharedInstance.currentCoordinator = creditCoordinator
            creditCoordinator.startInInvoiceDetail(with: id)
            
        case .limit:
            guard dependencies.featureManager.isActive(.experimentCardCreditLimitPresentingBool) else {
                setupCreditLimit()
                return false
            }
            
            guard let navigationController = getNavigationController() else {
                return false
            }
            
            let coordinator = LimitAdjustmentFlowCoordinator(navigationController: navigationController)
            coordinator.start()
        }
        return false
    }
}
