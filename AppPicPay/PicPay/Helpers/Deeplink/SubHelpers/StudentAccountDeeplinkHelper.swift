import UIKit

final class StudentAccountDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return setupStudentAccount()
    }
    
    private func setupStudentAccount() -> Bool {
        AppManager.shared.mainScreenCoordinator?.showHomeScreen()
        guard let homeNavigation = AppManager.shared.mainScreenCoordinator?.mainScreen() else {
            return false
        }
        StudentAccountCoordinator(from: homeNavigation).start(from: .deeplink)
        return true
    }
}
