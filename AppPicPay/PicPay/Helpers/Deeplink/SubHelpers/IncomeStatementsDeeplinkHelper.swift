import UIKit
import IncomeTaxReturn

final class IncomeStatementsDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return presentIncomeStatementsOnTopOfSettings()
    }
    
    private func presentIncomeStatementsOnTopOfSettings() -> Bool {
        guard let mainScreenCoordinator = AppManager.shared.mainScreenCoordinator?.currentController() else {
            return false
        }

        let statementController = IncomeStatementsFactory.make(presentedFromDeeplink: true)
        let navigationController = DismissibleNavigationViewController(rootViewController: statementController)
        mainScreenCoordinator.present(navigationController, animated: true)
        return true
    }
}
