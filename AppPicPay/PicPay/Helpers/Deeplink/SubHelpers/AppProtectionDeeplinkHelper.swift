import UIKit

final class AppProtectionDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        let protectionController = AppProtectionFactory.make(shouldHideCloseButton: false)

        let controllerToPresent = controller ?? AppManager.shared.mainScreenCoordinator?.mainScreen()
        controllerToPresent?.present(protectionController, animated: true)

        return true
    }
}
