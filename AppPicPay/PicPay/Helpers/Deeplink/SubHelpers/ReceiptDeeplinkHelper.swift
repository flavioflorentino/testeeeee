import UIKit

final class ReceiptDeeplinkHelper: DeeplinkHelperProtocol {
    weak var viewController: UIViewController?
    
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        viewController = controller ?? AppManager.shared.mainScreenCoordinator?.mainScreen()
        
        guard let receiptType = getTransactionType(withPath: deeplink.path),
              let id = getTransationId(withPath: deeplink.path),
              let viewController = viewController else {
            return false
        }
        
        let type = ReceiptWidgetViewModel.ReceiptType(rawValue: receiptType)
        
        guard  type != .unknown else { return false }
        
        let receiptModel = ReceiptWidgetViewModel(transactionId: id, type: type)
        TransactionReceipt.showReceipt(viewController: viewController, receiptViewModel: receiptModel)
        return true
    }
}

private extension ReceiptDeeplinkHelper {
    func getTransactionType(withPath path: String) -> String? {
        path.split(separator: "/")
            .first(where: { $0 != DeeplinkType.receipt.rawValue })
            .map({ String($0) })
    }
    
    func getTransationId(withPath path: String) -> String? {
        path.split(separator: "/")
            .last(where: { $0 != DeeplinkType.receipt.rawValue })
            .map({ String($0) })
    }
}
