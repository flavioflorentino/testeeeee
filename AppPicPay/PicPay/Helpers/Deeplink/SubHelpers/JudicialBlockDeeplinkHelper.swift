import UI
import Foundation

final class JudicialBlockDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        guard
            let deeplink = deeplink as? PPDeeplinkJudicialBlock,
            let origin = JudicialBlockOrigin(rawValue: deeplink.origin)
            else {
                return false
        }
        
        let viewController = JudiciallyBlockedFactory.make(id: deeplink.id, origin: origin)
        let navigation = UINavigationController(rootViewController: viewController)
        navigation.modalPresentationStyle = .overFullScreen
        navigation.view.backgroundColor = Palette.ppColorGrayscale000.color
        
        if #available(iOS 11.0, *) {
            viewController.navigationItem.largeTitleDisplayMode = .automatic
            navigation.navigationBar.prefersLargeTitles = true
        }
        
        let mainScreen = AppManager.shared.mainScreenCoordinator?.mainScreen()
        if let from = controller ?? mainScreen {
            from.present(navigation, animated: true)
        }
        
        return true
    }
}
