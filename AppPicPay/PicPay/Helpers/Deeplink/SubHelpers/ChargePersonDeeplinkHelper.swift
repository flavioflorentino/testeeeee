import FeatureFlag
import Foundation

final class ChargePersonDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return openChargePersonFlow(from: controller)
    }
    
    private func openChargePersonFlow(from controller: UIViewController?) -> Bool {
        guard let from = controller ?? AppManager.shared.mainScreenCoordinator?.homeNavigation() else {
            return false
        }
        
        from.present(PaymentRequestSelectorFactory().make(origin: .deeplink), animated: true)
        return true
    }
}
