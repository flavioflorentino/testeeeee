import Foundation

final class LinkedAccountsDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return AppManager.shared.mainScreenCoordinator?.currentController() is AccountsListTableViewController
    }
}
