import UIKit
import Billet

final class BillTimelineDeeplinkHelper: DeeplinkHelperProtocol {
    weak var viewController: UIViewController?
    
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        viewController = controller ?? AppManager.shared.mainScreenCoordinator?.mainScreen()
        
        guard let transactionId = getTransactionId(withPath: deeplink.path),
              let feedId = getFeedId(withPath: deeplink.path),
              let viewController = viewController else {
            return false
        }
    
        let controller = getFollowUpController(transactionId: transactionId, externalId: feedId)
        controller.hidesBottomBarWhenPushed = true
        viewController.navigationController?.pushViewController(controller, animated: true)
        return true
    }
}

private extension BillTimelineDeeplinkHelper {
    func getFollowUpController(transactionId: String, externalId: String) -> UIViewController {
        if DependencyContainer().featureManager.isActive(.isFollowUpAvailable) {
            return FollowUpFactory.make(withBillId: transactionId)
        } else {
            return BilletFeedDetailFactory.make(transactionId: transactionId, externalId: externalId)
        }
    }
    
    func getTransactionId(withPath path: String) -> String? {
        path.split(separator: "/")
            .first(where: { $0 != DeeplinkType.receipt.rawValue })
            .map({ String($0) })
    }
    
    func getFeedId(withPath path: String) -> String? {
        path.split(separator: "/")
            .last(where: { $0 != DeeplinkType.receipt.rawValue })
            .map({ String($0) })
    }
}
