import UIKit

final class NotificationDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        if let deepLinkNotification = deeplink as? PPDeeplinkNotification,
            let notification = deepLinkNotification.notification {
            BreadcrumbManager.shared.addCrumb("Notification", typeOf: .deepLink)
            SessionManager.sharedInstance.openNotification(notification)
            
            return true
        }
        return false
    }
}
