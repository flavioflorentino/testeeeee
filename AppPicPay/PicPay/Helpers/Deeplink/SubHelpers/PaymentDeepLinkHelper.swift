import AnalyticsModule
import UIKit

final class PaymentDeepLinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        BreadcrumbManager.shared.addCrumb("Payment", typeOf: .deepLink)
        
        guard let paymentSearchNavigation = SessionManager.sharedInstance.getPaymentNavigation(),
            let link = deeplink as? PPDeeplinkPayment else {
                return false
        }
        
        switch link.paymentType {
        case .store:
            return handleStore(link, navigation: paymentSearchNavigation)
        case .user:
            return handleUser(link)
        case .digitalGoods:
            return handleDigitalGoods(link, from: controller)
        }
    }
    
    func getStorePayment(with deeplink: PPDeeplinkPayment) -> UIViewController? {
        guard let storePaymentVc = ViewsManager.paymentStoryboardFirstViewController() as? PaymentViewController else {
            return nil
        }
        
        if deeplink.queryParams["hash"] == nil {
            Analytics.shared.log(StoreDeeplinkEvent.didCallDeeplinkWithoutHash(params: deeplink.queryParams))
        }
        
        if let store = deeplink.store {
            storePaymentVc.store = store
        } else {
            return PAVPaymentLoadingFactory.make(
                transactionHash: deeplink.transactionHash,
                paymentInfo: nil,
                orderItems: [],
                dismissPresentingAfterSuccess: false,
                dependency: DependencyContainer(),
                actions: nil,
                fromScanner: deeplink.fromScanner)
        }
        
        storePaymentVc.navigationLevel = 10
        storePaymentVc.touchOrigin = "Widget"
        storePaymentVc.showCancel = true
        
        if let cents = deeplink.value {
            storePaymentVc.initialValue = cents.stringAmount
        }
        if let extraParam = deeplink.queryParams["extra"] {
            storePaymentVc.deepLinkExtraParam = extraParam
        }
        if let isFixedValueString = deeplink.queryParams["fixed_value"] as NSString? {
            storePaymentVc.isFixedValue = isFixedValueString.boolValue
        }
        
        if var origin = deeplink.queryParams["origin"], let originId = deeplink.queryParams["origin_id"] {
            if deeplink.fromScanner { origin = "qrcode" }
            let chargeLinkInfo = ["origin": origin, "origin_id": originId]
            let chargeLinkDict = ["charge_link": chargeLinkInfo]
            storePaymentVc.additionalInfo = chargeLinkDict
        }
        
        return storePaymentVc
    }
    
    private func handleStore(_ link: PPDeeplinkPayment, navigation: UINavigationController) -> Bool {
        guard let vc = getStorePayment(with: link) else {
            return false
        }
        
        SessionManager.sharedInstance.dismissCurrentModal()
        AppManager.shared.mainScreenCoordinator?.showPaymentSearchScreen(searchText: nil, page: nil, ignorePermissionPrompt: true)
        
        let nav = PPNavigationController(rootViewController: vc)
        AppManager.shared.mainScreenCoordinator?.paymentSearchNavigation().present(nav, animated: true)
        return true
    }
    
    private func handleUser(_ link: PPDeeplinkPayment) -> Bool {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            SessionManager.sharedInstance.dismissCurrentModal()
            // changes to payment tab
            AppManager.shared.mainScreenCoordinator?.showPaymentSearchScreen(searchText: nil, page: nil, ignorePermissionPrompt: true)
            
            if let vc = NewTransactionViewController.fromStoryboard() {
                vc.touchOrigin = "payment_deeplink"
                vc.loadConsumerInfo(link.id)
                vc.showCancel = true
                if let value = link.value {
                    vc.preselectedPaymentValue = String(format: "%.2f", value)
                }
                if let extraParam = link.queryParams["extra"] {
                    vc.deepLinkExtraParam = extraParam
                }
                if let isFixedValueString = link.queryParams["fixed_value"] as NSString? {
                    vc.isFixedValue = isFixedValueString.boolValue
                }
                let nav = PPNavigationController(rootViewController: vc)
                AppManager.shared.mainScreenCoordinator?.paymentSearchNavigation().present(nav, animated: true, completion: nil)
            }
        }
        return true
    }
    
    private func handleDigitalGoods(_ link: PPDeeplinkPayment, from viewController: UIViewController?) -> Bool {
        guard let dgItem = link.dgItem else {
            SessionManager.sharedInstance.showStore()
            return false
        }
        return SessionManager.sharedInstance.openDigitalGoods(dgItem: dgItem, from: viewController)
    }
}
