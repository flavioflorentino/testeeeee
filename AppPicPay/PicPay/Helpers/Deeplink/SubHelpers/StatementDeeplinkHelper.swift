import UIKit
import Statement

final class StatementDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return presentUserStatementInsideWallet()
    }
    
    private func presentUserStatementInsideWallet() -> Bool {
        guard
            let mainScreenCoordinator = AppManager.shared.mainScreenCoordinator,
            let walletNavigationController = mainScreenCoordinator.walletNavigation()
            else {
                return false
        }
        mainScreenCoordinator.showWalletScreen()
        
        let statementController = StatementContainerFactory.make()
        let navigationController = UINavigationController(rootViewController: statementController)
        walletNavigationController.present(navigationController, animated: true)
        return true
    }
}
