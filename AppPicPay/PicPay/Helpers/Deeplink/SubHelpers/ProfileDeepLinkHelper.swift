import UIKit

final class ProfileDeepLinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return setupProfileDeepLink(deeplink)
    }
    
    private func setupProfileDeepLink(_ deeplink: PPDeeplink) -> Bool {
        guard let homeNavigation = AppManager.shared.mainScreenCoordinator?.homeNavigation(),
            let profile = deeplink as? PPDeeplinkProfile,
            let profileId = profile.profileId else {
                return false
        }
        
        AppManager.shared.mainScreenCoordinator?.showHomeScreen()
        
        let viewController = ProfileFactory.make(profileId: profileId)
        viewController.hidesBottomBarWhenPushed = true
        homeNavigation.pushViewController(viewController, animated: true)
        
        return true
    }
}
