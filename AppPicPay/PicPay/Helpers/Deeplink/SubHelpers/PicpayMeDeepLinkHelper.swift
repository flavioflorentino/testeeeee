import UIKit

final class PicpayMeDeepLinkHelper: DeeplinkHelperProtocol {    
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        BreadcrumbManager.shared.addCrumb("PicPayMe", typeOf: .deepLink)
        
        guard let paymentSearchNavigation = SessionManager.sharedInstance.getPaymentNavigation(),
            let link = deeplink as? PPDeeplinkMe,
            let viewController = getPeerToPeerPayment(withPicpayMe: link) else {
                return false
        }
        
        PaginationCoordinator.shared.showHome(animated: false)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            SessionManager.sharedInstance.dismissCurrentModal()
            paymentSearchNavigation.present(PPNavigationController(rootViewController: viewController), animated: true)
        }
        
        return true
    }
    
    func getPeerToPeerPayment(withPicpayMe deeplink: PPDeeplinkMe) -> UIViewController? {
        return DeeplinkUsernameViewController(username: deeplink.username, queryParams: deeplink.queryParams)
    }
}
