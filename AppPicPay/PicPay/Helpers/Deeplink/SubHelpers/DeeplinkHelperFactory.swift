import Core
import UIKit

protocol DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool
}

enum DeeplinkHelperFactory {
    // swiftlint:disable:next cyclomatic_complexity function_body_length
    static func generateDeeplinkHelper(withType type: PPDeeplinkType) -> DeeplinkHelperProtocol? {
        switch type {
        case .payment:
            return PaymentDeepLinkHelper()
        case .picpayMe:
            return PicpayMeDeepLinkHelper()
        case .notification:
            return NotificationDeeplinkHelper()
        case .creditPicpay:
            return CreditPicpayDeeplinkHelper()
        case .checkout:
            return CheckoutDeepLinkHelper()
        case .cielo:
            return CieloScannerDeeplinkHelper()
        case .identityAnalysis:
            return IdentityAnalysisDeeplinkHelper()
        case .linkedAccounts:
            return LinkedAccountsDeeplinkHelper()
        case .upgradeChecklist:
            return UpgradeChecklistDeeplinkHelper()
        case .invite, .promo, .registrationRenewal:
            return SettingsDeeplinkHelper()
        case .places, .store, .recharge, .search, .wallet:
            return PaymentBarDeeplinkHelper()
        case .bill:
            return BillDeeplinkHelper()
        case .profile:
            return ProfileDeepLinkHelper()
        case .accelerateAnalysis:
            return AccelerateAnalysisDeeplinkHelper()
        case .studentAccount:
            return StudentAccountDeeplinkHelper()
        case .webview:
            return WebviewDeeplinkHelper()
        case .myQrcode:
            return MyQrcodeDeeplinkHelper()
        case .cardRegistration:
            return CardRegistrationDeeplinkHelper()
        case .chargePerson:
            return ChargePersonDeeplinkHelper()
        case .feed:
            return FeedDeeplinkHelper()
        case .promotions,
             .promotionDetails:
            return PromotionsDeeplinkHelper(dependencies: DependencyContainer())
        case .favorites:
            return FavoritesDeeplinkHelper(dependencies: DependencyContainer())
        case .cancellationConfirmation:
            return CancellationConfirmationDeepLinkHelper()
        case .judicialblock:
            return JudicialBlockDeeplinkHelper()
        case .mgm:
            return MGMDeeplinkHelper()
        case .pep:
            return PepDeeplinkHelper()
        case .placeIndication:
            return PlaceIndicationDeepLinkHelper()
        case .savings:
            return SavingsDeeplinkHelper(dependencies: DependencyContainer())
        case .challenge:
            return ChallengeDeeplinkHelper()
        case .cashin:
            return CashInDeeplinkHelper()
        case .userstatement:
            return StatementDeeplinkHelper()
        case .withdraw:
            return WithdrawDeepLinkHelper()
        case .passwordReset:
            return PasswordResetDeeplinkHelper()
        case .incomeStatements:
            return IncomeStatementsDeeplinkHelper()
        case .appProtection:
            return AppProtectionDeeplinkHelper()
        case .receipt:
            return ReceiptDeeplinkHelper()
        case .billTimeline:
            return BillTimelineDeeplinkHelper()
        case .socialFeed:
            return SocialFeedDetailDeeplinkHelper()
        case .payPeople:
            return PayPeopleDeeplinkHelper(dependencies: DependencyContainer())
        case .invoiceCashback:
            return InvoicePromoDeeplinkHelper(dependencies: DependencyContainer())
        case .none:
            debugPrint("We're fucked")
            return nil
        }
    }
}
