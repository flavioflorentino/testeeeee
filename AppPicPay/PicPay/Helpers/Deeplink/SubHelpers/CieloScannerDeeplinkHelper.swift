import UIKit

final class CieloScannerDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return goToCieloScanner()
    }

    private func goToCieloScanner() -> Bool {
        let scanner = P2MScannerViewController.controllerFromStoryboard()
        let nav = PPNavigationController(rootViewController: scanner)
        guard let mainScreen = AppManager.shared.mainScreenCoordinator?.currentController() else {
            return false
        }
        
        mainScreen.present(nav, animated: true)
        return true
    }
}
