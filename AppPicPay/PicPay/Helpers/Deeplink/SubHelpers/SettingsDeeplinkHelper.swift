import UIKit
import Registration

final class SettingsDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return setupSettingsDeepLink(deeplink)
    }
    
    private func setupSettingsDeepLink(_ deeplink: PPDeeplink) -> Bool {
        switch deeplink.type {
        case .invite:
            guard let viewController = ViewsManager.peopleSearchStoryboardViewController("SocialShareCode"),
                  let currentController = AppManager.shared.mainScreenCoordinator?.currentController() else {
                return false
            }
            
            currentController.navigationController?.pushViewController(viewController, animated: true)
            viewController.hidesBottomBarWhenPushed = true
            return true
            
        case .promo:
            guard let settings = AppManager.shared.mainScreenCoordinator?.settingsNavigation() else {
                return false
            }
            let viewController = PromoCodeViewController(from: .deeplink)
            viewController.hidesBottomBarWhenPushed = true
            AppManager.shared.mainScreenCoordinator?.showSettingsScreen()
            settings.popToRootViewController(animated: false)
            settings.pushViewController(viewController, animated: true)
            return true
            
        case .registrationRenewal:
            guard let settings = AppManager.shared.mainScreenCoordinator?.settingsNavigation() else {
                return false
            }
            
            AppManager.shared.mainScreenCoordinator?.showSettingsScreen()
            settings.popToRootViewController(animated: false)
            
            let dependencies = RegistrationRenewalDependencies(
                addressSelector: AddressListRenewalProxy(),
                authManager: AuthProxy(),
                origin: .notification
            )
            let coordinator = RegistrationRenewalFlowCoordinator(with: settings, dependencies: dependencies)
            SessionManager.sharedInstance.currentCoordinating = coordinator
            coordinator.start()
            return true
            
        default:
            return false
        }
    }
}
