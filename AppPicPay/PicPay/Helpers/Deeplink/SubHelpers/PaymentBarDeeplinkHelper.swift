import FeatureFlag
import UIKit

final class PaymentBarDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return setupPaymentBarDeepLink(deeplink, fromController: controller)
    }
    
    private func setupPaymentBarDeepLink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        switch deeplink.type {
        case .places:
            return handlePlaces()
        case .store:
            return handleStore(deeplink)
        case .recharge:
            return handleRecharge(fromController: controller)
        case .search:
            return handleSearch(deeplink)
        case .wallet:
            return handleWallet()
        default:
            return false
        }
    }
    
    private func handlePlaces() -> Bool {
        guard let mainScreenCoordinator = AppManager.shared.mainScreenCoordinator else {
            return false
        }

        mainScreenCoordinator.showLocalsScreen()

        return true
    }
    
    private func handleStore(_ link: PPDeeplink) -> Bool {
        guard let storeDeeplink = link as? PPDeeplinkStore else {
            return false
        }
        
        guard let dgItem = storeDeeplink.dgItem else {
            SessionManager.sharedInstance.showStore()
            return true
        }
        
        if !storeDeeplink.noRedirect {
            SessionManager.sharedInstance.showStore()
        }
        
        return SessionManager.sharedInstance.openDigitalGoods(dgItem: dgItem, redirect: !storeDeeplink.noRedirect)
    }
    
    private func handleRecharge(fromController controller: UIViewController?) -> Bool {
        let originController: UIViewController
        if let controller = controller {
            originController = controller
        } else if let walletNavigation = AppManager.shared.mainScreenCoordinator?.walletNavigation() {
            originController = walletNavigation
            AppManager.shared.mainScreenCoordinator?.showWalletScreen()
        } else {
            return false
        }
        
        let viewController = RechargeLoadFactory.make()
        let rootNavigation = PPNavigationController(rootViewController: viewController)
        
        originController.present(rootNavigation, animated: true)
        return true
    }
    
    private func handleSearch(_ link: PPDeeplink) -> Bool {
        guard !FeatureManager.shared.isActive(.experimentSearchHomeBool) else {
            return handleHomeSearch(link)
        }

        guard let paymentNavigation = AppManager.shared.mainScreenCoordinator?.paymentSearchNavigation(),
              let viewController = paymentNavigation.viewControllers.first as? SearchWrapper,
              let search = link as? PPDeeplinkSearch,
              let subTab = search.searchSubTab else {
            return false
        }
        
        AppManager.shared.mainScreenCoordinator?.showSearchScreen(searchText: subTab.text)
        viewController.selectSearchTabBar(idTab: subTab.title)
        return true
    }

    private func handleHomeSearch(_ link: PPDeeplink) -> Bool {
        guard let search = link as? PPDeeplinkSearch,
              let subTab = search.searchSubTab,
              let navigationController = AppManager.shared.mainScreenCoordinator?.homeNavigation() else {
            return false
        }

        AppManager.shared.mainScreenCoordinator?.showHomeScreen()

        let searchController = SearchWrapper(ignorePermissionPrompt: true, dependencies: DependencyContainer())
        searchController.origin = .deeplink
        searchController.setTabs(tabs: [.main, .consumers, .places, .store], isOnlySearchEnabled: true)
        searchController.showContainerSearch = true
        searchController.shouldShowBackButton = true
        searchController.shouldFocusWhenAppear = true
        searchController.shouldClearOnChangeTab = false
        searchController.selectSearchTabBar(idTab: subTab.title)
        searchController.initialSearch = subTab.text
        searchController.hidesBottomBarWhenPushed = true

        navigationController.pushViewController(searchController, animated: true)

        return true
    }
    
    private func handleWallet() -> Bool {
        guard AppManager.shared.mainScreenCoordinator?.walletNavigation() != nil else {
            return false
        }
        
        AppManager.shared.mainScreenCoordinator?.showWalletScreen()
        
        return true
    }
}
