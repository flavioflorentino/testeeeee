import UIKit

final class FeedDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        return goToHomeScreen()
    }
    
    private func goToHomeScreen() -> Bool {
        AppManager.shared.mainScreenCoordinator?.showHomeScreen()
        return true
    }
}
