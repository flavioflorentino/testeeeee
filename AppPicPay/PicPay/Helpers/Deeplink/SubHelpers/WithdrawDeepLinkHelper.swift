import Cashout
import FeatureFlag
import Foundation
import UIKit

final class WithdrawDeepLinkHelper: DeeplinkHelperProtocol {
    weak var viewController: UIViewController?
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        guard let rechargeDeepLink = deeplink as? PPDeepLinkWithdraw else { return false }
        viewController = controller ?? AppManager.shared.mainScreenCoordinator?.mainScreen()
        
        return handle(rechargeDeepLink)
    }
}

private extension WithdrawDeepLinkHelper {
    func handle(_ deeplink: PPDeepLinkWithdraw) -> Bool {
        guard let id = deeplink.id, dependencies.featureManager.isActive(.isWithdrawReceiptAvailable) else {
            return false
        }
        return openReceipt(with: id)
    }
    
    @discardableResult
    func openReceipt(with id: String) -> Bool {
        let controller = WithdrawReceiptFactory.make(receiptId: id)
        let navigation = UINavigationController(rootViewController: controller)
        viewController?.present(navigation, animated: true)
        return true
    }
}
