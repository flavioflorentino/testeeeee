import Foundation

enum DeeplinkType: String {
    case payment
    case checkout
    case identityanalysis
    case credit
    case POS
    case pos
    case n
    case tps
    case upgradeaccount
    case invite
    case promocode
    case places
    case store
    case recharge
    case search
    case boleto
    case profile
    case accelerateAnalysis
    case picpayMe
    case student_account
    case webview
    case myqrcode
    case cardregistration
    case card
    case chargeperson
    case feed
    case promotions
    case promotionDetails = "promotionsdetails"
    case cancellationConfirmation = "paymentcancellation"
    case favorites
    case judicialblock
    case mgm
    case pep
    case placeIndication = "invitebusiness"
    case savings
    case challenge
    case wallet
    case registrationrenewal
    case user
    case cashin
    case userstatement
    case withdraw
    case passwordReset = "passwordreset"
    case incomeStatements = "incomestatements"
    case appProtection = "accessprotection"
    case receipt
    case bills
    case socialFeedDetail = "social-feed"
    case payPeople = "payperson"
    case invoiceCashback = "invoicecashback"
}

enum DeeplinkFactory {
    // swiftlint:disable:next cyclomatic_complexity function_body_length
    static func generateDeepLink(withUrl url: URL, fromScanner: Bool = false) -> PPDeeplink? {
        guard let type = getType(fromUrl: url) else {
            return nil
        }

        switch type {
        case .payment:
            return PPDeeplinkPayment(url: url, fromScanner: fromScanner)
        case .checkout:
            return PPDeeplinkCheckout(url: url)
        case .identityanalysis:
            return PPDeeplinkIdentityAnalysis(url: url)
        case .credit, .card:
            return PPDeeplinkCreditPicpay(url: url)
        case .pos, .POS:
            return PPDeeplinkCielo(url: url)
        case .n:
            return PPDeeplinkNotification(url: url)
        case .tps:
            return PPDeeplinkLinkedAccounts(url: url)
        case .upgradeaccount:
            return PPDeeplink(url: url, type: .upgradeChecklist)
        case .invite:
            return PPDeeplink(url: url, type: .invite)
        case .promocode:
            return PPDeeplink(url: url, type: .promo)
        case .places:
            return PPDeeplink(url: url, type: .places)
        case .store:
            return PPDeeplinkStore(url: url)
        case .recharge:
            return PPDeeplink(url: url, type: .recharge)
        case .search:
            return PPDeeplinkSearch(url: url)
        case .boleto:
            return PPDeeplink(url: url, type: .bill)
        case .profile:
            return PPDeeplinkProfile(url: url)
        case .accelerateAnalysis:
            return PPDeeplinkAccelerateAnalysis(url: url)
        case .picpayMe, .user:
            return PPDeeplinkMe(url: url)
        case .student_account:
            return PPDeeplink(url: url, type: .studentAccount)
        case .webview:
            return PPDeeplinkWebview(url: url)
        case .myqrcode:
            return PPDeeplink(url: url, type: .myQrcode)
        case .cardregistration:
            return PPDeeplink(url: url, type: .cardRegistration)
        case .chargeperson:
            return PPDeeplink(url: url, type: .chargePerson)
        case .feed:
            return PPDeeplink(url: url, type: .feed)
        case .promotions:
            return PromotionsDeeplink(url: url)
        case .promotionDetails:
            return PromotionDetailsDeeplink(url: url)
        case .favorites:
            return PPDeeplink(url: url, type: .favorites)
        case .cancellationConfirmation:
            return CancellationConfirmationDeepLink(url: url)
        case .judicialblock:
            return PPDeeplinkJudicialBlock(url: url)
        case .mgm:
            return DeepLinkMGM(url: url)
        case .pep:
            return PPDeeplinkPep(url: url)
        case .placeIndication:
            return PPDeeplink(url: url, type: .placeIndication)
        case .savings:
            return PPDeeplink(url: url, type: .savings)
        case .challenge:
            return PPDeeplinkChallenge(url: url)
        case .wallet:
            return PPDeeplink(url: url, type: .wallet)
        case .registrationrenewal:
            return PPDeeplink(url: url, type: .registrationRenewal)
        case .cashin:
            return PPDeeplinkCashIn(url: url)
        case .userstatement:
            return PPDeeplink(url: url, type: .userstatement)
        case .withdraw:
            return PPDeepLinkWithdraw(url: url)
        case .passwordReset:
            return PPDeeplink(url: url, type: .passwordReset)
        case .incomeStatements:
            return PPDeeplink(url: url, type: .incomeStatements)
        case .appProtection:
            return PPDeeplink(url: url, type: .appProtection)
        case .receipt:
            return PPDeeplink(url: url, type: .receipt)
        case .bills:
            return PPDeeplink(url: url, type: .billTimeline)
        case .socialFeedDetail:
            return PPDeeplink(url: url, type: .socialFeed)
        case .payPeople:
            return PPDeeplink(url: url, type: .payPeople)
        case .invoiceCashback:
            return InvoiceCashbackDeeplink(url: url)
        }
    }
    
    private static func getType(fromUrl url: URL) -> DeeplinkType? {
        if let host = url.host, host == "picpay.me" {
            return DeeplinkType.picpayMe
        }
        
        guard let path = url.pathComponents.first(where: { $0 != "/" }),
            let linkType = DeeplinkType(rawValue: path) else {
            return nil
        }
        
        return linkType
    }
}
