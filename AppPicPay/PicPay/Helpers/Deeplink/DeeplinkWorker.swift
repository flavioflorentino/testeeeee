import Foundation
import FeatureFlag

protocol DeeplinkWorkerProtocol {
    func getDeeplinkTrack() -> [DeeplinkTrack]?
}

final class DeeplinkWorker: DeeplinkWorkerProtocol {
    func getDeeplinkTrack() -> [DeeplinkTrack]? {
        return FeatureManager.object(.deepLinkTrackEvent, type: [DeeplinkTrack].self)
    }
}
