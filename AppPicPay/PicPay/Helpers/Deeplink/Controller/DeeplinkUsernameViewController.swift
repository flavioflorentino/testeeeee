import AnalyticsModule
import UIKit

final class DeeplinkUsernameViewController: PPBaseViewController {
    lazy var loadingView: UILoadView = {
        let view = UILoadView(superview: self.view, position: .center)
        return view
    }()
    
    let  username: String
    
    let query: [String: String]?
    
    // MARK: - Initializer
    
    init(username: String, queryParams: [String: String]?) {
        self.username = username
        self.query = queryParams
        super.init(nibName: nil, bundle: nil)
        hidesBottomBarWhenPushed = true
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Life Cyle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkUsername()
    }
    
    // MARK: - Internal Methods
    
    private func setup() {
        loadingView.startLoading()
        let closeButton = UIBarButtonItem(title: "Cancelar", style: .plain, target: self, action: #selector(close))
        navigationItem.leftBarButtonItem = closeButton
    }
    
    private func checkUsername() {
        let api = DeepLinkApi()
        api.get(username: username) { [weak self] result in
            DispatchQueue.main.async {
                self?.loadingView.stopLoading()
                
                switch result {
                case .success(let value):
                        self?.openContent(content: value)
                case .failure(let error):
                    AlertMessage.showAlert(error, controller: self, completion: { [weak self] in
                        if self?.presentationController != nil {
                            self?.dismiss(animated: true, completion: nil)
                        } else {
                            self?.navigationController?.popViewController(animated: true)
                        }
                    })
                }
            }
        }
    }
    
    private func openContent(content: UsernameResponse) {
        switch content.type {
        case .consumer:
            openConsumer(content: content)
        case .membership:
            openMembership(content: content)
        default:
            break
        }
    }
    
    private func openConsumer(content: UsernameResponse) {
        guard
            let contact = content.data as? PPContact,
            let p2pPaymentNav = ViewsManager.peerToPeerStoryboardFirtNavigationController(),
            let p2pPaymentVc = p2pPaymentNav.topViewController as? NewTransactionViewController
            else {
            return
        }
        
        p2pPaymentVc.preselectedContact = contact
        if let strValue = self.query?["payment_value"] {
            p2pPaymentVc.preselectedPaymentValue = strValue.replacingOccurrences(of: ",", with: ".")
        }
        if let extraParam = self.query?["extra"] {
            p2pPaymentVc.deepLinkExtraParam = extraParam
        }
        if let isFixedValueString = self.query?["fixed_value"] as NSString? {
            p2pPaymentVc.isFixedValue = isFixedValueString.boolValue
        }
        if let chargeTransactionIdString = self.query?["charge_transaction_id"] as NSString? {
            Analytics.shared.log(PaymentRequestUserEvent.didAccessNotification)
            p2pPaymentVc.chargeTransactionId = chargeTransactionIdString.integerValue as NSNumber
        }
        showFadeTransition()
        navigationController?.replace(self, by: p2pPaymentVc)
    }
    
    private func openMembership(content: UsernameResponse) {
        guard let producer = content.data as? ProducerProfileItem else {
            return
        }
        
        let subscriptionsModel = ProducerCategoriesViewModel(producer: producer)
        let subscriptionOptions = ProducerPlansViewController(model: subscriptionsModel, origin: .deeplink)
        if let planId = self.query?["plan"] {
            let plan = ProducerPlanItem()
            plan.id = planId
            subscriptionOptions.currentItem = SubscriptionDataCell(subscription: plan)
        }
        subscriptionOptions.onClose = {
            subscriptionOptions.dismiss(animated: true, completion: nil)
        }
        showFadeTransition()
        navigationController?.replace(self, by: subscriptionOptions)
    }
    
    private func showFadeTransition() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        transition.type = .fade
        navigationController?.view.layer.add(transition, forKey: nil)
    }
    
    func removeThisViewFromNavigation() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
            // It removes this controller from navigation stack
            if let viewControllers = self.navigationController?.viewControllers {
                let newViewControllers = viewControllers.filter { $0 is DeeplinkUsernameViewController == false }
                self.navigationController?.viewControllers = newViewControllers
            }
        }
    }
    
    @objc
    func close(_ sender: Any?) {
        dismiss(animated: true, completion: nil)
    }
}
