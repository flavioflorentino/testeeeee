import Core
import UIKit

final class DeeplinkHelper: NSObject {
    private static var resolvers = [DeeplinkResolver]()
    
    /// Handle deeplink
    ///
    /// - Parameter deeplink: deeplink
    /// - Returns: return true if the deep link was handled
    @discardableResult
    @objc
    static func handleDeeplink(withUrl url: URL?, from fromViewController: UIViewController? = nil, fromScanner: Bool = false) -> Bool {
        DeeplinkHelper.deleteSavedLink()
        
        guard let linkUrl = url else {
            return false
        }
        
        if let resolver = DeeplinkResolversHelper.findValidResolverWith(linkUrl) {
            return DeeplinkResolversHelper.resolve(resolver, url: linkUrl)
        }

        guard let deeplink = DeeplinkFactory.generateDeepLink(withUrl: linkUrl, fromScanner: fromScanner) else {
            return false
        }
        
        return DeeplinkHandlerManager().handleDeeplink(deeplink, from: fromViewController, deeplinkURL: linkUrl)
    }
    
    // MARK: - Storage
    @objc
    static func storeDeepLink(link: URL) {
        KVStore().set(value: link.dataRepresentation, with: KVKey.savedDeeplink)
    }
    
    @objc
    static func retrieveSavedDeepLink() -> URL? {
        if let data = KVStore().objectForKey(KVKey.savedDeeplink) as? Data {
            return URL(dataRepresentation: data, relativeTo: nil)
        }
        return nil
    }
    
    @objc
    static func deleteSavedLink() {
        KVStore().removeObjectFor(KVKey.savedDeeplink)
    }
}
