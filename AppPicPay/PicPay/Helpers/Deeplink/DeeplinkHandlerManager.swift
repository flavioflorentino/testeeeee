import Foundation

final class DeeplinkHandlerManager {
    /**
     Open Deeplink
     */
    func handleDeeplink(_ deeplink: PPDeeplink, from fromViewController: UIViewController? = nil, deeplinkURL: URL? = nil) -> Bool {
        deeplink.trackEvent()
        if (deeplink.type != .accelerateAnalysis) || fromViewController == nil {
            SessionManager.sharedInstance.dismissCurrentModal()
        }
        
        if shouldValidateNotAuthenticatedUser(for: deeplink.type) {
            return validateNotAuthenticatedUser(withDeeplink: deeplink, andUrl: deeplinkURL)
        }
        
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: deeplink.type)
        return helper?.handleDeeplink(deeplink, fromController: fromViewController) ?? false
    }

    private func shouldValidateNotAuthenticatedUser(for type: PPDeeplinkType) -> Bool {
        type == .passwordReset ? false : !User.isAuthenticated
    }
    
    private func validateNotAuthenticatedUser(withDeeplink link: PPDeeplink, andUrl deeplinkUrl: URL? = nil) -> Bool {
        switch link {
        case is DeepLinkMGM:
            return MGMDeeplinkHelper().handleDeeplink(link, fromController: nil)
        default:
            guard let url = deeplinkUrl else {
                return false
            }
            DeeplinkHelper.storeDeepLink(link: url)
        }
        return false
    }
}
