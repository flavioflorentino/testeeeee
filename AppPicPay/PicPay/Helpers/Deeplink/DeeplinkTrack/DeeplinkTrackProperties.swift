import Foundation

final class DeeplinkTrackProperties: Codable {
    let key: String
    let value: String
}
