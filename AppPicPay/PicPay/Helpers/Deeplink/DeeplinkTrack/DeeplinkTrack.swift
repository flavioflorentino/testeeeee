import Foundation

final class DeeplinkTrack: Decodable {
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case type
        case properties
    }
    
    enum ActionType: String, Codable {
        case firstTimeOnly = "UNIQUE"
        case normal = "DEFAULT"
    }
    
    let id: String
    let title: String
    let type: ActionType
    let properties: [DeeplinkTrackProperties]?
    
    func propertiesDictionary() -> [String: String]? {
        guard let properties = self.properties else {
            return nil
        }
        var result: [String: String] = [:]
        
        for property in properties {
            result[property.key] = property.value
        }
        
        return result
    }
}
