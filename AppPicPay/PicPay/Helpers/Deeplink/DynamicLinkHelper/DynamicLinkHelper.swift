import Core
import Foundation

public protocol HasDynamicLinkHelper {
    var dynamicLinkHelper: DynamicLinkHelperContract { get }
}

public protocol DynamicLinkHelperContract {
    var referralCodeToRegistration: String? { get }
}

final class DynamicLinkHelper: DynamicLinkHelperContract {
    var referralCodeToRegistration: String? {
        return DynamicLinkHelper.getReferralCodeToRegistration()
    }
    
    @discardableResult
    static func handleFirebaseDynamicLinkDeepLink(url: URL?) -> Bool {
        guard let url = url else {
            return false
        }
        
        var deepLink: String?
        var code: String?

        if let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true), let queryItems = urlComponents.queryItems {
            for component in queryItems {
                if component.name == "dl", let value = component.value?.removingPercentEncoding {
                    deepLink = value
                } else {
                    code = component.name
                }
            }
        }
        
        if let deepLink = deepLink {
            guard let deeplinkURL = URL(string: deepLink) else {
                return false
            }
            if !User.isAuthenticated {
                keepDeepLinkForAfterAuthenticationOrRegister(deepLink)
            } else {
                PPAnalytics.trackEvent("DynamicLinkFirebase", properties: ["deepLink": deeplinkURL.absoluteString], includeMixpanel: true)
                DeeplinkHelper.handleDeeplink(withUrl: deeplinkURL)
            }
        }
        if let code = code, !User.isAuthenticated {
            keepReferralCodeToRegistration(code)
        }
        
        return false
    }
    
    fileprivate static func keepDeepLinkForAfterAuthenticationOrRegister(_ deepLink: String) {
        KVStore().setString(deepLink, with: .firebase_dynamic_link_deep_link)
    }
    
    static func removeDeepLinkToRegistration() {
        KVStore().removeObjectFor(.firebase_dynamic_link_deep_link)
    }
    
    static func getDeepLinkToRegistration() -> URL? {
        guard let deepLink = KVStore().stringFor(.firebase_dynamic_link_deep_link) else {
            return nil
        }
        
        return URL(string: deepLink)
    }
    
    fileprivate static func keepReferralCodeToRegistration(_ code: String) {
        KVStore().setString(code, with: .firebase_dynamic_link_referral_code)
    }
    
    static func removeReferralCodeToRegistration() {
        KVStore().removeObjectFor(.firebase_dynamic_link_referral_code)
    }
    
    static func getReferralCodeToRegistration() -> String? {
        guard let referralCode = KVStore().stringFor(.firebase_dynamic_link_referral_code) else {
            return nil
        }
        
        return referralCode
    }
}
