import UI
import Atributika

struct StringHtmlTextHelper {
    static func htmlToAttributedString(string: String, font: UIFont, foregroundColor: UIColor? = nil) -> NSAttributedString {
        let color = foregroundColor ?? self.color(in: string) ?? Palette.ppColorGrayscale600.color
        let allStyle = Style.font(font).foregroundColor(color)
        let boldTag = Style("b").font(UIFont.boldSystemFont(ofSize: font.pointSize))
        let fontTag = Style("font").font(font)
        
        return string
            .decodingHTMLEntities()
            .style(tags: boldTag, fontTag)
            .styleAll(allStyle)
            .attributedString
    }
    
    private static func color(in string: String) -> UIColor? {
        let stringUppercase = string.uppercased()
        if stringUppercase.contains("#FFA800") {
            return Palette.ppColorAttentive400.color
        }
        if stringUppercase.contains("#ED1846") {
            return Palette.ppColorNegative300.color
        }
        if stringUppercase.contains("#21C25E") {
            return Palette.ppColorBranding300.color
        }
        if stringUppercase.contains("#FFBC00") {
            return Palette.ppColorNegative400.color
        }
        if stringUppercase.contains("#B0B0B0") {
            return Palette.ppColorGrayscale400.color
        }
        if stringUppercase.contains("#6E6E6E") {
            return Palette.ppColorGrayscale400.color
        }
        if stringUppercase.contains("RED") {
            return Palette.ppColorNegative300.color
        }
        if stringUppercase.contains("#BCB5B9") {
            return Palette.ppColorGrayscale400.color
        }
        if stringUppercase.contains("#11C76F") {
            return Palette.ppColorBranding300.color
        }
        if stringUppercase.contains("#F7AD09") {
            return Palette.ppColorAttentive400.color
        }
        if stringUppercase.contains("#00AFEE") {
            return Palette.ppColorNeutral300.color
        }
        if stringUppercase.contains("#FF2048") {
            return Palette.ppColorNegative300.color
        }
        if stringUppercase.contains("#DB291F") {
            return Palette.PartnersColor.banco24horas
        }
        return nil
    }
}
