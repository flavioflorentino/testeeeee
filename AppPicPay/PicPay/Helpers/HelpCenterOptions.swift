import CustomerSupport
import Foundation
import FeatureFlag
import UIKit

enum HelpCenterOptions: String {
    case adicaoViaCartaoDeDebitoCaixa = "adicao-via-cartao-de-debito-caixa"
    case adicionarDinheiroNoPicpay = "adicionar-dinheiro-no-picpay"
    case comoAlteroMeuEnderecoVinculadoAoPicpayCard = "como-altero-meu-endereco-vinculado-ao-picpay-card"
    case comoCalcularMeuPatrimonio = "como-calcular-meu-patrimonio"
    case duvidasSobreOMeuEmprestimoPessoal = "duvidas-sobre-o-meu-emprestimo-pessoal"
    case faqVirtualCardHelper = "faq_virtual_card_helper"
    case faqVirtualCardRegistration = "faq_virtual_card_registration"
    case funcaoDebito = "funcao-debito"
    case home = "home"
    case jaPagueiOBoleto = "ja-paguei-o-boleto"
    case naoRecebiOSmsComOCodigoParaCompletarMeuCadastro = "nao-recebi-o-sms-com-o-codigo-para-completar-meu-cadastro"
    case oQueEValidacaoDeIdentidade = "o-que-e-validacao-de-identidade"
    case ofertaCreditoPessoal = "oferta-credito-pessoal"
    case pagamentoDeIptuEOutrosTributos = "pagamento-de-iptu-e-outros-tributos"
    case password = "password"
    case picpayCard = "picpay-card"
    case porQueMeuPedidoNaoFoiAceito = "por-que-meu-pedido-nao-foi-aceito"
    case porQueNaoConsigoFazerOUpgradeDeConta = "por-que-nao-consigo-fazer-o-upgrade-de-conta"
    case porQuePrecisoAceitarOContratoDeAdesao = "por-que-preciso-aceitar-o-contrato-de-adesao"
    case porQuePrecisoVerificarMeuCartao = "por-que-preciso-verificar-meu-cartao"
    case retirarDinheiroDoPicpay = "retirar-dinheiro-do-picpay"
    case saqueNoBanco24Horas = "saque-no-banco-24horas"
    case simulacaoCreditoPessoal = "simulacao-credito-pessoal"
    case taxasELimitesDePagamentos = "taxas-e-limites-de-pagamentos"
    case tentaramEntregarMeuCartaoENinguemRecebeu = "tentaram-entregar-meu-cartao-e-ninguem-recebeu"
    case upgradeDeConta = "upgrade-de-conta"

    var faqOption: FAQOptions {
        #if DEBUG
        return developmentFaqOption
        #else
        return productionFaqOption
        #endif
    }

    var productionFaqOption: FAQOptions {
        let articleId: String
        switch self {
        case .home:
            return .home
        case .adicaoViaCartaoDeDebitoCaixa,
             .retirarDinheiroDoPicpay,
             .saqueNoBanco24Horas:
            articleId = "360003814672"
        case .adicionarDinheiroNoPicpay,
             .faqVirtualCardHelper,
             .faqVirtualCardRegistration:
            articleId = "360003814572"
        case .comoAlteroMeuEnderecoVinculadoAoPicpayCard,
             .funcaoDebito,
             .picpayCard,
             .tentaramEntregarMeuCartaoENinguemRecebeu:
            articleId = "360003297112"
        case .comoCalcularMeuPatrimonio:
            articleId = "360045897791"
        case .duvidasSobreOMeuEmprestimoPessoal,
             .ofertaCreditoPessoal,
             .porQueMeuPedidoNaoFoiAceito,
             .porQuePrecisoAceitarOContratoDeAdesao,
             .simulacaoCreditoPessoal:
            articleId = "360003819012"
        case .jaPagueiOBoleto,
             .pagamentoDeIptuEOutrosTributos:
            articleId = "360003297072"
        case .naoRecebiOSmsComOCodigoParaCompletarMeuCadastro,
             .oQueEValidacaoDeIdentidade,
             .password,
             .porQueNaoConsigoFazerOUpgradeDeConta,
             .porQuePrecisoVerificarMeuCartao,
             .upgradeDeConta:
            articleId = "360003433711"
        case .taxasELimitesDePagamentos:
            articleId = "360003322111"
        }
        return .category(id: articleId)
    }

    var developmentFaqOption: FAQOptions {
        switch self {
        case .adicaoViaCartaoDeDebitoCaixa,
             .adicionarDinheiroNoPicpay,
             .comoAlteroMeuEnderecoVinculadoAoPicpayCard,
             .comoCalcularMeuPatrimonio,
             .duvidasSobreOMeuEmprestimoPessoal,
             .faqVirtualCardHelper,
             .faqVirtualCardRegistration,
             .funcaoDebito,
             .home,
             .jaPagueiOBoleto,
             .naoRecebiOSmsComOCodigoParaCompletarMeuCadastro,
             .oQueEValidacaoDeIdentidade,
             .ofertaCreditoPessoal,
             .pagamentoDeIptuEOutrosTributos,
             .password,
             .picpayCard,
             .porQueMeuPedidoNaoFoiAceito,
             .porQueNaoConsigoFazerOUpgradeDeConta,
             .porQuePrecisoAceitarOContratoDeAdesao,
             .porQuePrecisoVerificarMeuCartao,
             .retirarDinheiroDoPicpay,
             .saqueNoBanco24Horas,
             .simulacaoCreditoPessoal,
             .taxasELimitesDePagamentos,
             .tentaramEntregarMeuCartaoENinguemRecebeu,
             .upgradeDeConta:
            return .home
        }
    }
}
