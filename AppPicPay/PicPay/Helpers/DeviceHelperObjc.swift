import Core
import Foundation

@objcMembers
final class DeviceHelperObjc: NSObject {
    typealias Dependencies = HasKeychainManagerPersisted
    
    static func deviceId() -> String? {
        let dependencies: Dependencies = DependencyContainer()
        return UIDevice.current.picpayDeviceId(dependencies: dependencies)
    }
}
