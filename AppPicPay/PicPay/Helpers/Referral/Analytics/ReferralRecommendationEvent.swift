import AnalyticsModule
import Core

enum ReferralRecommendationEvent: AnalyticsKeyProtocol {
    case optionSelected(_ option: OptionSelected)
    
    private var name: String {
        "MGM - APP - "
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case let .optionSelected(option):
            return AnalyticsEvent(name + option.rawValue, providers: providers)
        }
    }
}

extension ReferralRecommendationEvent {
    enum OptionSelected: String {
        case register = "Create Account"
        case login = "Sign Up"
        case help = "Sign Up Information"
    }
}
