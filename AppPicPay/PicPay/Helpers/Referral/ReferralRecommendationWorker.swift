import Foundation
import Core

protocol ReferralRecommendationProtocol {
    var isFromReferral: Bool { get }
    var recommendationName: String? { get }
}

final class ReferralRecommendationWorker: ReferralRecommendationProtocol {
    var isFromReferral: Bool
    var recommendationName: String?
    
    init() {
        isFromReferral = KVStore().boolFor(.isFromReferralRecommendation)
        recommendationName = KVStore().stringFor(.inviterName)
        eraseData()
    }
    
    private func eraseData() {
        KVStore().removeObjectFor(.isFromReferralRecommendation)
        KVStore().removeObjectFor(.inviterName)
    }
}
