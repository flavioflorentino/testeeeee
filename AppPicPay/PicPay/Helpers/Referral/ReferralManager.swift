import Foundation

protocol ReferralManageable {
    var referral: ReferralCodeModel? { get }
    var loadError: Error? { get }
    func loadReferral()
}

final class ReferralManager: ReferralManageable {
    static let shared: ReferralManager = ReferralManager()
    var referral: ReferralCodeModel?
    var loadError: Error?
    
    func loadReferral() {
        let service = ReferralCodeService(dependencies: DependencyContainer())
        service.getReferralCodeData { [weak self] result in
            switch result {
            case .success(let referral):
                self?.referral = referral
            case .failure(let error):
                self?.loadError = error
            }
        }
    }
}
