//
//  BiometricTypeAuth.swift
//  PicPay
//
//  Created by Gustavo Storck on 25/09/2018 🤔🇧🇷.
//

import Foundation

@objc final class BiometricTypeAuth: NSObject {
    
    @objc enum BiometricType: Int {
        case none = 0
        case touchID = 1
        case faceID = 2
    }
    
    @objc func biometricType() -> BiometricType {
        let authContext = LAContext()
        
        // The first release of iPhone X was on iOS 11.0.1, so the update of the
        // BiometryType with faceID only exists from that version
        if #available(iOS 11.0.1, *) {
            if authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
                
                switch(authContext.biometryType) {
                case .faceID:
                    return .faceID
                case .touchID:
                    return .touchID
                default:
                    return .none
                }
            } else {
                return .none
            }
        } else {
            return authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? .touchID : .none
        }
    }
}
