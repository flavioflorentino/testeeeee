import Foundation

enum LocalizableFile: String {
    case antiFraud
    case creditPicPay
    case scanner
    case upgradeChecklist
    case settings
    case rechargeBank
    case recharge
    case editCreditCard
    case studentAccount
    case consumerAddress
    case mail
    case personalDataErrorMessage
    case register
    case login
    case p2mLinx
    case captcha
    case identityValidation
    case atm24
    case withdraw
    case ratingAlert
    case scannerHowItWorks
    case birthday
    case phone
    case bankAccount
    case analytics
    case onboarding
    case personalDetails
    case shareCode
    case paymentRequest
    case myCode
    case availableWithdrawal
    case cardVerification
    case businessType
    case deactivateAccount
    case cvv
    case promoCodePopUp
    case tfa
    case tfaPersonalDataCheck
    case favorites
    case profile
    case search
    case bankAccountCheck
    case favorite
    case ecommercePayment
    case signUp
    case promoCodeRegister
    case cancellationConfirmation
    case qrcodeBills
    case contactsList
    case resetPassword
    case payment
    case externalTransfer
    case bacen
    case paymentStandardHeader
    case paymentItem
    case `default`
    case billet
    case placeIndicationAbout
    case paymentRequestToolbar
    case placeIndicationFormView
    case placeIndication
    case appTour
    case searchLegacy
    case limitExceeded
    case paymentLegacy
    case confirmationLegacy
    case popupLegacy
    case ticketLegacy
    case homeLegacy
    case linkedAccounts
    case notificationLegacy
    case p2mLegacy
    case parkingLegacy
    case paymentMethods
    case receiptLegacy
    case saving
    case subscribe
    case facialBiometric
    case documentCapture
    case searchBottomSheetLocalizable
    case emergencyAid
    case mgmIncentiveModal
    case universityAccount
    case permissions
    case validationStatus
    case referralCode
    case profissionalInfo
    case rechargeOnboarding
    case rechargeWireTransfer
    case payPeople
    case unifiedPromoMGM
    case creditAnalysis
    case appProtection
    case updatePrivacy
    case homeSearchComponent
    case transactions
}

protocol Localizable {
    var key: String { get }
    var text: String { get }
    var file: LocalizableFile { get }
}

extension Localizable {
    var text: String {
        return LocalizableHelper.text(for: self)
    }
    var file: LocalizableFile {
        return .default
    }
}

final class LocalizableHelper {
    static func text(for localizable: Localizable) -> String {
        let file = localizable.file == .default ? nil : localizable.file.rawValue
        return Bundle.main.localizedString(forKey: localizable.key, value: nil, table: file)
    }
}
