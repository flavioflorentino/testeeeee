import Contacts
import Core
import Foundation

struct UserContact: Encodable {
    enum CodingKeys: String, CodingKey {
        case completeName
        case phones
        case emails
    }
    
    let completeName: String
    let phones: [String]
    let emails: [String]
    
    init(completeName: String, phones: [String], emails: [String]) {
        self.completeName = completeName
        self.phones = phones
        self.emails = emails
    }
    
    init(cnContact: CNContact) {
        completeName = "\(cnContact.givenName) \(cnContact.familyName) \(cnContact.organizationName)"
        phones = cnContact.phoneNumbers.map { $0.value.stringValue }
        emails = cnContact.emailAddresses.map { $0.value as String }
    }
}

struct UserContacts: Encodable {
    enum CodingKeys: String, CodingKey {
        case contacts
    }
    
    let contacts: [UserContact]
    
    init(contacts: [UserContact]) {
        self.contacts = contacts
    }
    
    func concatenatedPhones() -> String {
        return contacts
            .flatMap { $0.phones }
            .reduce("", { current, next -> String in
                return "\(current)\(next)"
            })
    }
    
    /// This method is only to keep compatibility with the Old request format
    ///
    /// - Returns: old array format
    func toArray() -> [Any] {
        var array = [Any]()
        for contact in contacts {
            let contactArray = NSMutableArray()
            contactArray.add(contact.completeName as NSString)
            contactArray.add(contact.phones as [NSString])
            contactArray.add(contact.emails as [NSString])
            array.append(contactArray)
        }
        return array
    }
}
