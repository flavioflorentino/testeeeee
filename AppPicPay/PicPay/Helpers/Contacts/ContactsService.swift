import Core
import Foundation

protocol ContactsServicing {
    func identifyContacts(contacts: [Any],
                          useCache: Bool,
                          localHash: String,
                          remoteHash: String,
                          completion: @escaping (Result<IdentifyContactsResponse, PicPayError>) -> Void)
}

class ContactsService: ContactsServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func identifyContacts(
        contacts: [Any],
        useCache: Bool,
        localHash: String,
        remoteHash: String,
        completion: @escaping (Result<IdentifyContactsResponse, PicPayError>) -> Void) {
        guard useCache, let contactsCache = loadContactsFromCache() else {
            loadContactsApi(contacts: contacts, localHash: localHash, remoteHash: remoteHash, completion: completion)
            return
        }
        
        completion(.success(contactsCache))
    }
    
    private func loadContactsApi(
        contacts: [Any],
        localHash: String,
        remoteHash: String,
        completion: @escaping (Result<IdentifyContactsResponse, PicPayError>) -> Void) {
        let api = Api<IdentifyContactsResponse>(endpoint: ContactsEndpoint.identifyContacts(contacts: contacts,
                                                                                            localHash: localHash,
                                                                                            remoteHash: remoteHash))
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case let .success(contacts):
                    self?.setContactsCache(contacts.model)
                    completion(.success(contacts.model))
                case let .failure(error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    private func loadContactsFromCache() -> IdentifyContactsResponse? {
        do {
            return try KVStore().retrieveObject(type: IdentifyContactsResponse.self, key: .identifyContactsCache)
        } catch {
            return loadContactsFromMigrationCache()
        }
    }
    
    private func loadContactsFromMigrationCache() -> IdentifyContactsResponse? {
        guard let nsData = KVStore().objectForKey(.identifyContactsCache) as? NSData,
              let data = nsData.gunzipped() else {
            return nil
        }
        
        return try? JSONDecoder().decode(IdentifyContactsResponse.self, from: data as Data)
    }
    
    private func setContactsCache(_ contacts: IdentifyContactsResponse) {
        try? KVStore().saveObject(value: contacts, key: .identifyContactsCache)
    }
}
