import Foundation

struct IdentifyContactsResponse: Codable {
    let identified: [IdentifyContacts]
    
    public init(from decoder: Decoder) throws {
        let rootContainer = try decoder.container(keyedBy: CodingKeys.self)
        let container = try rootContainer.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        identified = try container.decode([IdentifyContacts].self, forKey: .identified)
    }
    
    func encode(to encoder: Encoder) throws {
        var rootContainer = encoder.container(keyedBy: CodingKeys.self)
        var container = rootContainer.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        try container.encode(identified, forKey: .identified)
    }
}

struct IdentifyContacts: Codable {
    let id: String?
    let username: String?
    let userImage: String?
    let name: String?
}

extension IdentifyContactsResponse {
    private enum CodingKeys: String, CodingKey {
        case identified, data
    }
}

extension IdentifyContacts {
    private enum CodingKeys: String, CodingKey {
        case id
        case username
        case name
        case userImage = "img_url"
    }
}
