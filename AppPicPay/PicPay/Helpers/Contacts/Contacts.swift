import Contacts
import Core
import FeatureFlag
import CoreLegacy
import Foundation
import GZIP
import PermissionsKit

enum IdentifyContactsStatus {
    case notAuthenticated
    case notAuthorized
    case usedContactsFromCache
    case receivedContactsFromServer
    case serviceError
    case unknown
}

typealias IdentifyContactsCompletion = (_ identifyStatus: IdentifyContactsStatus) -> Void

/// Facilitates Future Tests
protocol ContactsProtocol: AnyObject {
    func identifyPicpayContacts(completion: IdentifyContactsCompletion?)
    func userContacts() -> UserContacts?
}

/// Swift version of PPContactList class - that will be removed.
@objc
final class Contacts: NSObject, ContactsProtocol {
    typealias Dependencies = HasFeatureManager
    private let service: ContactsServicing
    private let dependencies: Dependencies
    
    var queue = DispatchQueue(label: "com.picpay.picpay.contact")
    
    init(service: ContactsServicing = ContactsService(), dependencies: Dependencies = DependencyContainer()) {
        self.service = service
        self.dependencies = dependencies
    }
    
    /// Identify user contacts that use picpay App
    ///
    /// - Parameter completion: Optional completion block - track the status
    func identifyPicpayContacts(completion: IdentifyContactsCompletion? = nil) {
        if !User.isAuthenticated {
            completion?(IdentifyContactsStatus.notAuthenticated)
        } else if Permission.contacts.status != .authorized {
            completion?(IdentifyContactsStatus.notAuthorized)
        } else {
            queue.sync { [weak self] in
                guard let self = self else {
                    completion?(IdentifyContactsStatus.unknown)
                    return
                }
                
                WSConsumer.ping({ error in
                    var updateSpotlight = false
                    var cache: Bool = false
                    
                    // If the local contact list hash is equal to remote, then uses the local cache.
                    if self.localContactHash() == self.remoteContactHash() {
                        cache = true
                    } else {
                        updateSpotlight = true
                        cache = false
                    }
                    
                    if self.dependencies.featureManager.isActive(.identifyContactsEnable) {
                        self.loadIdentifyContacts(useCache: cache, updateSpotlight: updateSpotlight, completion: completion)
                    }
                })
            }
        }
    }
    
    private func loadIdentifyContacts(useCache: Bool, updateSpotlight: Bool, completion: (IdentifyContactsCompletion?)) {
        guard let contacts = userContacts() else {
            return
        }
        let localHash = localContactHash() ?? ""
        let remoteHash = remoteContactHash() ?? ""

        service.identifyContacts(contacts: contacts.toArray(), useCache: useCache, localHash: localHash, remoteHash: remoteHash) { [weak self] result in
            switch result {
            case let .success(contacts):
                self?.processIdentifiedContacts(registeredUsers: contacts, updateSpotlight: updateSpotlight)
                completion?(useCache ? IdentifyContactsStatus.usedContactsFromCache : IdentifyContactsStatus.receivedContactsFromServer)

            case .failure:
                self?.retryIdentifyContacts(contacts: contacts, localHash: localHash, remoteHash: remoteHash, completion: completion)
            }
        }
    }
    
    private func retryIdentifyContacts(contacts: UserContacts,
                                       localHash: String,
                                       remoteHash: String,
                                       completion: IdentifyContactsCompletion?) {
        service.identifyContacts(contacts: contacts.toArray(), useCache: false, localHash: localHash, remoteHash: remoteHash) { [weak self] result in
            switch result {
            case let .success(contacts):
                self?.processIdentifiedContacts(registeredUsers: contacts, updateSpotlight: true)
                completion?(IdentifyContactsStatus.receivedContactsFromServer)

            case .failure:
                completion?(IdentifyContactsStatus.serviceError)
            }
        }
    }
    
    /// Get all contact from device as array of ContactRequestModel
    ///
    /// - Returns: Array of UserContacts as ContactRequestModel
    func userContacts() -> UserContacts? {
        let store = CNContactStore()
        let keys = [CNContactFamilyNameKey, CNContactGivenNameKey, CNContactOrganizationNameKey, CNContactEmailAddressesKey, CNContactPhoneNumbersKey]
        
        let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
        request.sortOrder = .userDefault
        
        var contactsToIdentify: [UserContact] = []
        
        do {
            try store.enumerateContacts(with: request) { (cnContact: CNContact, _) in
                contactsToIdentify.append(UserContact(cnContact: cnContact))
            }
            
            let contactsRequestModel = UserContacts(contacts: contactsToIdentify)
            // Create and store the contact list hash - keep compatibility
            setLocalContactHash(NSString(string: contactsRequestModel.concatenatedPhones()).md5())
            
            return contactsRequestModel
        } catch {
            debugPrint(error)
            return nil
        }
    }
    
    private func processIdentifiedContacts(registeredUsers: IdentifyContactsResponse, updateSpotlight: Bool) {
        var identifiedContacts: [PPContact] = []

        for contact in registeredUsers.identified {
            if let contactId = contact.id, let id = Int(contactId), id != ConsumerManager.shared.consumer?.wsId {
                let ppContact = PPContact()
                ppContact.wsId = id
                ppContact.username = contact.username
                ppContact.imgUrl = contact.userImage
                ppContact.onlineName = contact.name
                
                identifiedContacts.append(ppContact)
            }
        }

        AppParameters.global().isUpdatingContacts = false
        shouldUpdateSpotLight(update: updateSpotlight, withContacts: identifiedContacts)
    }
    
    private func shouldUpdateSpotLight(update: Bool, withContacts contacts: [PPContact]) {
        if update {
            DispatchQueue.global(qos: .userInteractive).async {
                let searchableContent = SearchableContentManager()
                searchableContent.ppContacts(contacts: contacts)
            }
        }
    }
    
    /// Save the local contact hash
    ///
    /// - Parameter hash: The hash to be saved
    @objc
    func setLocalContactHash(_ hash: String) {
        let data = NSKeyedArchiver.archivedData(withRootObject: hash)
        KVStore().setData(data, with: KVKey.localContactsHash)
    }
    
    /// Loads the local contact hash
    ///
    /// - Returns: The saved hash
    @objc
    func localContactHash() -> String? {
        let hashData = KVStore().objectForKey(KVKey.localContactsHash) as? Data
        return NSKeyedUnarchiver.unarchiveObject(with: hashData ?? Data()) as? String
    }
        
    /// hash to compare the contact list consistency. If the hashes are different, the app will download an updated contact list
    ///
    /// - Parameter hash: the hash
    @objc
    func setRemoteContactHash(hash: String) {
        KVStore().setString(hash, with: KVKey.remoteContactHash)
    }
    
    /// hash to compare the contact list consistency. If the hashes are different, the app will download an updated contact list
    ///
    /// - Returns: the hash
    @objc
    func remoteContactHash() -> String? {
        let hash = KVStore().stringFor(KVKey.remoteContactHash)
        return hash
    }
}
