import Core
import Foundation

enum ContactsEndpoint {
    case identifyContacts(contacts: [Any], localHash: String, remoteHash: String)
}

extension ContactsEndpoint: ApiEndpointExposable {
    var path: String {
        "api/identifyContacts.json"
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var customHeaders: [String: String] {
        guard case let .identifyContacts(_, localHash, remoteHash) = self else {
            return [:]
        }
        return [Parameters.localHash: localHash, Parameters.remoteHash: remoteHash]
    }
    
    var body: Data? {
        guard case let .identifyContacts(model, _, _) = self else {
            return nil
        }
        
        let dictionary = NSDictionary(object: model, forKey: NSString(stringLiteral: "contacts"))
        return try? JSONSerialization.data(withJSONObject: dictionary, options: .fragmentsAllowed)
    }
}

extension ContactsEndpoint {
    private enum Parameters {
        static let localHash = "local_contact_hash"
        static let remoteHash = "remote_contact_hash"
    }
}
