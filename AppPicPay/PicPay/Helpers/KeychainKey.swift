import Core
import Foundation

enum KeychainKeyPF: KeychainKeyable {
    case cvv(String)
    case userIdLogout
    case tokenLogout                                            
    
    public var rawValue: String {
        switch self {
        case .cvv(let cardId):
            return "cvv_card_\(cardId)"
        case .userIdLogout:
            return "user_id_logout"
        case .tokenLogout:
            return "token_logout"
        }
    }
}
