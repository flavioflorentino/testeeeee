//
//  RepeatingTimer.swift
//  PicPay
//
//  Created by Lucas Romano on 23/07/2018.
//

import Foundation

final class RepeatingTimer {
    
    fileprivate enum State {
        case suspended
        case resumed
    }
    
    let timeInterval: TimeInterval
    var eventHandler: (() -> Void)?
    
    private var state: State = .suspended
    private lazy var timer: DispatchSourceTimer = {
        let t = DispatchSource.makeTimerSource()
        let deadLine = DispatchTime.now() + self.timeInterval
        t.schedule(deadline: deadLine, repeating: self.timeInterval)
        t.setEventHandler(handler: { [weak self] in
            self?.eventHandler?()
        })
        return t
    }()
    
    init(timeInterval: TimeInterval) {
        self.timeInterval = timeInterval
    }

    func resume() {
        if state == .resumed {
            return
        }
        state = .resumed
        timer.resume()
    }
    
    func suspend() {
        if state == .suspended {
            return
        }
        state = .suspended
        timer.suspend()
    }
    
    deinit {
        timer.setEventHandler {}
        timer.cancel()
        /*
         If the timer is suspended, calling cancel without resuming
         triggers a crash. This is documented here https://forums.developer.apple.com/thread/15902
         */
        resume()
        eventHandler = nil
    }
}
