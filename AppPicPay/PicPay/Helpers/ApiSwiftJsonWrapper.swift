import Core
import Foundation
import SwiftyJSON

struct ApiSwiftJsonWrapper {
    private let result: ApiCompletion.ApiResult<Data>
    
    init(with result: ApiCompletion.ApiResult<Data>) {
        self.result = result
    }
    
    func transform<T: BaseApiResponse>( _ completion: @escaping ((PicPayResult<T>) -> Void)) {
        switch result {
        case let .success(apiResult):
            let reasonDecodeServerData = DefaultLocalizable.errorDecodeServerJsonData.text
            
            guard let data = apiResult.data else {
                completion(PicPayResult.failure(PicPayError(message: reasonDecodeServerData, code: "api_decode_response_data")))
                return
            }
            
            let json = JSON(data)
            
            if let error200 = transformError200(json: json) {
                completion(PicPayResult.failure(error200))
                return
            }
            
            guard let response = T(json: json) else {
                completion(PicPayResult.failure(PicPayError(message: reasonDecodeServerData, code: "api_decode_response_object")))
                return
            }
            
            completion(PicPayResult.success(response))
        case let .failure(error):
            completion(PicPayResult.failure(error.picpayError))
        }
    }
}

extension ApiSwiftJsonWrapper {
    private func transformError200(json: JSON) -> PicPayError? {
        let errorJson = json["Error"]
        var picpayError: PicPayError?
        if let id = errorJson["id"].string {
            picpayError = PicPayError(message: errorJson["description_pt"].string ?? "",
                                      code: id ,
                                      title: "Ops!")
        } else if let id = errorJson["id"].int {
            picpayError = PicPayError(message: errorJson["description_pt"].string ?? "",
                                      code: String(format: "%d", id),
                                      title: "Ops!")
        }
        return picpayError
    }
}
