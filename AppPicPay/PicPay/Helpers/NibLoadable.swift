import UIKit

protocol NibLoadable {
    static var nibName: String { get }
}

extension NibLoadable {
    static var nib: UINib {
        return UINib(nibName: nibName, bundle: nil)
    }
}

extension UIView: NibLoadable {
    static var nibName: String {
        return String(describing: self)
    }
}

extension UIViewController: NibLoadable {
    static var nibName: String {
        return String(describing: self)
    }
}

extension NibLoadable where Self: UIView {
    static func instantiateFromNib() -> Self {
        guard let view = UINib(nibName: nibName, bundle: nil)
            .instantiate(withOwner: nil, options: nil).first as? Self
            else { fatalError("Couldn't find nib file for \(String(describing: Self.self))") }
        return view
    }
}

extension NibLoadable where Self: UIViewController {
    static func instantiateFromNib() -> Self {
        return Self(nibName: nibName, bundle: nil)
    }
}

extension NibLoadable where Self: UITableView {
    static func instantiateFromNib() -> Self {
        guard let tableView = UINib(nibName: nibName, bundle: nil)
            .instantiate(withOwner: nil, options: nil).first as? Self
            else { fatalError("Couldn't find nib file for \(String(describing: Self.self))") }
        return tableView
    }
}

extension NibLoadable where Self: UITableViewController {
    static func instantiateFromNib() -> Self {
        return Self(nibName: nibName, bundle: nil)
    }
}

extension UITableView {
    func registerCell<T: UITableViewCell>(type: T.Type) {
        register(T.nib, forCellReuseIdentifier: String(describing: T.self))
    }
    
    func registerHeaderFooterView<T: UITableViewHeaderFooterView>(type: T.Type) {
        register(type.nib, forHeaderFooterViewReuseIdentifier: String(describing: T.self))
    }
    
    func dequeueReusableCell<T: UITableViewCell>(type: T.Type) -> T {
        guard let cell = self.dequeueReusableCell(withIdentifier: String(describing: T.self)) as? T
            else { fatalError("Couldn't find nib file for \(String(describing: T.self))") }
        return cell
    }
    
    func dequeueReusableCell<T: UITableViewCell>(type: T.Type, forIndexPath indexPath: IndexPath) -> T {
        guard let cell = self.dequeueReusableCell(withIdentifier: String(describing: T.self), for: indexPath) as? T
            else { fatalError("Couldn't find nib file for \(String(describing: T.self))") }
        return cell
    }
    
    func dequeueResuableHeaderFooterView<T: UITableViewHeaderFooterView>(type: T.Type) -> T {
        guard let headerFooterView = self.dequeueReusableHeaderFooterView(withIdentifier: String(describing: T.self)) as? T
            else { fatalError("Couldn't find nib file for \(String(describing: T.self))") }
        return headerFooterView
    }
}

extension UICollectionView {
    func registerCell<T: UICollectionViewCell>(type: T.Type) {
        register(type.nib, forCellWithReuseIdentifier: String(describing: T.self))
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(type: T.Type, forIndexPath indexPath: IndexPath) -> T {
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: String(describing: T.self), for: indexPath) as? T else {
            fatalError("Couldn't find nib file for \(String(describing: T.self))")
        }
        return cell
    }
}

