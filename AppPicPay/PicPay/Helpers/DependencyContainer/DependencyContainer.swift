import AccessibilityKit
import AnalyticsModule
import Core
import CoreLegacy
import CustomerSupport
import DirectMessageSB
import FeatureFlag
import Foundation
import Socket

typealias AppDependencies =
    HasNoDependency &
    HasMainQueue &
    HasGlobalQueue &
    HasAppManager &
    HasAppParameters &
    HasAuthManager &
    HasConsumerManager &
    HasCreditCardManager &
    HasCustomerSupport &
    HasFeatureManager &
    HasLocationManager &
    HasPaymentManager &
    HasUserManager &
    HasDynamicLinkHelper &
    HasStudentWorker &
    HasAnalytics &
    HasKVStore &
    HasDispatchGroup &
    HasConsumerApi &
    HasCreditCoordinatorManager &
    HasReferralManager &
    HasSocketManager &
    HasAccessibilityManager &
    HasKeychainManager &
    HasKeychainManagerPersisted &
    HasNotificationCenter &
    // MARK: DirectMessageSB
    HasChatApiManager &
    HasChatNotifier &
    HasConnectionStateManufacturing


final class DependencyContainer: AppDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var globalQueue = DispatchQueue.global()
    lazy var appManager: AppManagerContract = AppManager.shared
    lazy var appParameters: AppParametersContract = AppParameters.global()
    lazy var authManager: AuthManagerContract = AuthManager.shared
    lazy var consumerManager: ConsumerManagerContract = ConsumerManager.shared
    lazy var creditCardManager: CreditCardManagerContract = CreditCardManager.shared
    lazy var customerSupport: CustomerSupportContract = CustomerSupportManager.shared
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var locationManager: LocationManagerContract = LocationManager.shared
    lazy var paymentManager: PaymentManagerContract = PPPaymentManager()
    lazy var userManager: UserManagerContract = User()
    lazy var dynamicLinkHelper: DynamicLinkHelperContract = DynamicLinkHelper()
    lazy var studentWorker: StudentWorkerContract = StudentWorker()
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var kvStore: KVStoreContract = KVStore()
    lazy var dispatchGroup: DispatchGroup = DispatchGroup()
    lazy var consumerApi: ConsumerApiProtocol = ConsumerApi()
    lazy var registrationCoordinator: CPRegistrationCoordinatorProtocol? = CPRegistrationCoordinator.shared
    lazy var referralManager: ReferralManageable = ReferralManager.shared
    lazy var socketManager: SocketManageable = SocketManager.shared
    lazy var accessibilityManager: AccessibilityManagerContract = AccessibilityManager.shared
    lazy var keychain: KeychainManagerContract = KeychainManager()
    lazy var keychainWithPersistent: KeychainManagerContract = KeychainManager(isPersistent: true)
    lazy var notificationCenter = NotificationCenter.default
    
    // MARK: DirectMessageSB
    lazy var chatApiManager: ChatApiManaging = ChatApiManager.shared
    lazy var chatNotifier: ChatNotifying = ChatNotifier.shared
    lazy var connectionStateFactory: ConnectionStateManufacturing = ConnectionStateFactory()
}
