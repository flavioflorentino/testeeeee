import Foundation

protocol HasNoDependency {}

protocol HasConsumerManager {
    var consumerManager: ConsumerManagerContract { get set }
}

protocol HasCreditCardManager {
    var creditCardManager: CreditCardManagerContract { get set }
}

protocol HasPaymentManager {
    var paymentManager: PaymentManagerContract { get }
}

protocol HasAuthManager {
    var authManager: AuthManagerContract { get }
}

protocol HasAppManager {
    var appManager: AppManagerContract { get }
}

protocol HasUserManager {
    var userManager: UserManagerContract { get }
}

protocol HasStudentWorker {
    var studentWorker: StudentWorkerContract { get }
}

protocol HasConsumerApi {
    var consumerApi: ConsumerApiProtocol { get }
}

protocol HasCreditCoordinatorManager {
    var registrationCoordinator: CPRegistrationCoordinatorProtocol? { get }
}

protocol HasReferralManager {
    var referralManager: ReferralManageable { get }
}

protocol HasNotificationCenter {
    var notificationCenter: NotificationCenter { get }
}
