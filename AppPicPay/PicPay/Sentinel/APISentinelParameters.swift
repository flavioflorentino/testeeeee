import Foundation

struct APISentinelParameters {
    let code: String?
    let message: String?
    let endpoint: String?
    let statusCode: Int?
    let correlationId: String?
    let newPaymentArchitecture: String?
    
    var toDictionary: [String: Any?] {
        [
            "code": code,
            "message": message,
            "endpoint": endpoint,
            "status_code": statusCode,
            "x_request_id": correlationId,
            "X-New-Payment": newPaymentArchitecture
        ]
    }
}
