import Foundation

@objcMembers
public final class NewPaymentArchitecture: NSObject {
    public static var headerField: String {
        "X-New-Payment"
    }
}
