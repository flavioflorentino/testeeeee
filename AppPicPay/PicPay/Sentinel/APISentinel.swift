import Foundation
import CoreSentinel

enum APISentinel: SentinelEventProtocol {
    case apiError(parameters: APISentinelParameters)
    
    var type: String {
        switch self {
        case .apiError:
            return "api_error"
        }
    }
    
    var name: String? { nil }
    
    var attributes: [String: Any?] {
        switch self {
        case let .apiError(parameters):
            return parameters.toDictionary
        }
    }

    var providers: [SentinelProvider] {
        [.newRelic]
    }
}
