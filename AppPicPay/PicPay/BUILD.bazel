package(default_visibility = ["//visibility:public"])

load("@build_bazel_rules_ios//rules:app.bzl", "ios_application")
load("@build_bazel_rules_ios//rules:framework.bzl", "apple_framework", "apple_framework_packaging")
load("//rules:xcconfigparse.bzl", "process_plist_vars", "process_entitlements_vars")

exports_files(
	glob([
		"Supporting Files/PicPay-Info.plist",
		"Legacy/Views/LaunchScreen.storyboard",
		"**/*.storyboard",
		"**/*.pdf",
		"**/*.xib", 
		"**/*.strings",
		"**/*.xcassets/**",
		"**/GoogleService-*.plist",
		"**/FeatureToggles.plist",
		"**/*.entitlements",
		"**/*.html",
	])
)

srcs = glob([
		"**/*.swift",
		"**/*.h",
		"**/*.m",
	],
	exclude = ["Supporting Files/PicPay-Bridging-Header.h"],
)

apple_framework(
    platforms = {"ios": "10.3"},
    name = "ExternalLibs",
    deps = [
        "@//Libs/Firebase:FirebasePicPayPF",
		"@//Libs/NewRelic"
    ],
)

deps = [
	"@//Modules:PicPay",
	"@//Pods:PicPay",
	":ExternalLibs"
]

ios_application(
    name = "Release",
    bundle_id = "com.picpay.PicPay",
	srcs = srcs,
	entitlements = "Supporting Files/PicPay.entitlements",
	launch_storyboard = "Legacy/Views/LaunchScreen.storyboard",
    minimum_os_version = "10.3",
    infoplists = ["//AppPicPay/PicPay:Supporting Files/PicPay-Info.plist"],
	resources = glob([
		"**/*.storyboard",
		"**/*.xib",
		"**/*.strings",
		"**/Images.xcassets/**",
		"**/*.pdf",
		"**/Icons-Release.xcassets/**",
		"**/GoogleService-*.plist",
		"**/FeatureToggles.plist",
		"**/*.entitlements",
		"**/*.html",
	]),
	deps = deps,
	other_inputs = [":Supporting Files/PicPay-Bridging-Header.h"],
	swift_copts = ["-import-objc-header", "$(location :Supporting Files/PicPay-Bridging-Header.h)"],
    families = [ "iphone" ],
)

process_plist_vars(
	name = "DebugInfoPlist",
	src = "Supporting Files/PicPay-Info.plist",
	replacements = {
		"$(HELPCENTER_PICPAY)": "https://ajuda.picpay.com/picpay/",
		"$(PICPAY_API_HOST)":"https://gateway.service.ppay.me/",
		"$(APP_STORE_ID)": "561524792",
		"$(SHOW_BETA_WARNING)": "YES",
		"$(CURRENT_PROJECT_VERSION)": "200",
		"$(MARKETING_VERSION)": "10.19.27",
		"$(CARD_ZONE_API_URL)": "https://qa.card-zone.picpay.com/",
        "$(EVENT_TRACKER_API_URL)": "https://gateway.service.ppay.me/",
        "$(EVENT_TRACKER_API_PROXY_URL)": "http://api-event-tracking.ms.qa.limbo.work/"
	},
)

process_entitlements_vars(
	name = "DebugEntitlements",
	src = "Supporting Files/PicPay.entitlements",
	replacements = {
		"$(AppIdentifierPrefix)$(PRODUCT_BUNDLE_IDENTIFIER)": "com.picpay.PicPay-Development",
	},
)

ios_application(
    name = "Debug",
    module_name = "PicPay",
    bundle_id = "com.picpay.PicPay-Development",
	# provisioning_profile: "",
	srcs = srcs,
	entitlements = ":DebugEntitlements",
	launch_storyboard = "Legacy/Views/LaunchScreen.storyboard",
    minimum_os_version = "10.3",
    infoplists = [":DebugInfoPlist"],	
	resources = glob([
		"**/*.storyboard",
		"**/*.xib",
		"**/*.strings",
		"**/*.pdf",
		"**/Images.xcassets/**",
		"**/Icons-Debug.xcassets/**",
		"**/GoogleService-Info-Dev.plist",
		"**/FeatureToggles.plist",
		"**/*.entitlements",
		"**/*.html",
	]),
	deps = deps,
	swift_objc_bridging_header = "Supporting Files/PicPay-Bridging-Header.h",
	swift_copts = [
		"-D", "DEBUG",
		"-D", "DEVELOPMENT"
	],
    families = [ "iphone" ],
	visibility = ["//visibility:public"],
)

apple_framework_packaging(
	platforms = {"ios": "10.3"},
    name = "DebugAppUnlinked",
    framework_name = "TestImports-Debug",
    skip_packaging = ["binary"],
    tags = [
        "manual",
        "xcodeproj-ignore-as-target",
    ],
    transitive_deps = [],
    visibility = ["//visibility:public"],
    deps = [
		":Debug_swift",
		":Debug_objc",
    ],
)
