import UI
import UIKit

// ⚠️
// Legacy code, it'll be replaced by Palette enum

enum PicPayStyle {
    static let savingsLighterGreen = #colorLiteral(red: 0.4039215686, green: 0.8862745098, blue: 0.6509803922, alpha: 1) // #67E2A6
    static let savingsLightGreen = #colorLiteral(red: 0.04705882353, green: 0.9098039216, blue: 0.4901960784, alpha: 1) // #0CE87D | gradientGreenPlus300 (primeira cor)
    static let picPayCreditGreen = #colorLiteral(red: 0.1647058824, green: 0.4431372549, blue: 0.431372549, alpha: 1) // #2A716E | gradientGreenPlus400 (primeira cor)
    static let mediumGreen1 = #colorLiteral(red: 0.07058823529, green: 0.6196078431, blue: 0.3294117647, alpha: 1) // #129E54
    static let mediumGreen1Hex = "#129E54" // #129E54
    static let savingsMediumGreen = #colorLiteral(red: 0.05882352941, green: 0.6823529412, blue: 0.3803921569, alpha: 1) // #0FAE61
    static let savingsDarkGreen = #colorLiteral(red: 0.01568627451, green: 0.631372549, blue: 0.3333333333, alpha: 1) // 04A155
    static let mediumGrayHex = "#8F929D" // #8F929D
    static let gradientPicPayCredit: [UIColor] = [Palette.ppColorBrandingPlus400.gradientColor.from, Palette.ppColorBrandingPlus400.gradientColor.to]
    static let gradientGray: [UIColor] = [Palette.ppColorGrayscalePlus.gradientColor.from, Palette.ppColorGrayscalePlus.gradientColor.to]
    static let pageIndicator = #colorLiteral(red: 0.6392156863, green: 0.9450980392, blue: 0.462745098, alpha: 1) // #A3F176
    static let pictureBackgroundColor = #colorLiteral(red: 0.2980392157, green: 0.2705882353, blue: 0.2980392157, alpha: 1) // #4C454C
}

// Objective-c Interface
@objc
class AppStyles: NSObject {
    override fileprivate init() {}
    
    // Controller ----
    @objc
    class func statusBarStyle() -> UIStatusBarStyle { UIStatusBarStyle.default }
    
    // Feed
    @objc
    class func feedSegmentBackgroundColor() -> UIColor { #colorLiteral(red: 0.968627451, green: 0.9803921569, blue: 0.9803921569, alpha: 1) } // #F7FAFA
    
    // Button ----------
    @objc
    class func buttonBorderWidth() -> CGFloat { 1.0 }
    
    @objc
    class func buttonCornerRadius() -> CGFloat { 7.0 }
}
