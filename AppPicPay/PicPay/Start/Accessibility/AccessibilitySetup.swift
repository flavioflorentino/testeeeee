import AccessibilityKit
import AnalyticsModule
import Foundation

final class AccessibilitySetup {
    typealias Dependencies = HasAnalytics & HasAccessibilityManager
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func start() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(featureStatusChanged(notification:)),
            name: .accessibilityFeatureStatusDidChange,
            object: nil
        )
        
        eventFeatureStatus(from: dependencies.accessibilityManager.featuresStatus)
    }
    
    @objc
    private func featureStatusChanged(notification: NSNotification) {
        guard let featureStatus = notification.object as? AccessibilityFeature else { return }
        eventFeatureStatus(from: [featureStatus.type.rawValue: featureStatus.status])
    }
    
    private func eventFeatureStatus(from status: [String: Any]) {
        let event = AccessibilityFeatureEvent.featuresEnabled(
            featureStatus: status
        )
        
        dependencies.analytics.log(event)
    }
}
