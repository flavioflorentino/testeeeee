import AnalyticsModule

enum AccessibilityFeatureEvent: AnalyticsKeyProtocol {
    case featuresEnabled(featureStatus: [String: Any])
    
    private var name: String {
        "Accessibility Features"
    }
    
    private var properties: [String: Any] {
        switch self {
        case .featuresEnabled(let featureStatus):
            return featureStatus
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}
