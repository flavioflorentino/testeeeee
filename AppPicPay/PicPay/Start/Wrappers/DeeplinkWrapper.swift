import Core

final public class DeeplinkWrapper: DeeplinkContract {
    public func open(url: URL) -> Bool {
        return DeeplinkHelper.handleDeeplink(withUrl: url)
    }
}
