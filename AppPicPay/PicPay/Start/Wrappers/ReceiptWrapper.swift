import Core
import Foundation
import UI
import UIKit

final class ReceiptWrapper: ReceiptContract, HasTopViewController {
    func open(withId id: String, receiptType: String, feedItemId: String?) {
        let type = ReceiptWidgetViewModel.ReceiptType(rawValue: receiptType)
        
        guard
            type != .unknown,
            let topViewController = topViewController()
            else {
                return
        }
        
        let receiptModel = ReceiptWidgetViewModel(transactionId: id, type: type)
        TransactionReceipt.showReceipt(viewController: topViewController, receiptViewModel: receiptModel, feedItemId: feedItemId)
    }
}
