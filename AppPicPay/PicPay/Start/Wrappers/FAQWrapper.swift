import UIKit
import Loan
import LoanOffer
import P2PLending

struct FAQWrapper: LoanFAQContract, LoanOfferFAQContract {
    func controller(withTitle title: String? = nil, faqUrl: String) -> FAQResult {
        guard faqUrl.isNotEmpty else {
            return .failure(.emptyUrl)
        }
        
        guard
            let urlString = WebServiceInterface.apiEndpoint(faqUrl),
            let url = URL(string: urlString)
            else {
                return .failure(.couldNotOpen(faqUrl))
        }
        
        guard let controller = WebViewFactory.make(with: url) as? FAQWebView else {
            return .failure(.viewControllerType)
        }
        controller.title = title

        return .success(controller)
    }
}
