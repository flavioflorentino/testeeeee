import UI
import UIKit

struct WebViewWrapper: WebViewCreatable {
    func createWebView(with url: URL) -> UIViewController {
        let controller = WebViewFactory.make(with: url)
        return UINavigationController(rootViewController: controller)
    }
}
