import UIKit
import Loan

public final class RechargeMethodsWrapper: LoanRechargeMethodsContract {
    public func openRechargeLoadView() -> UIViewController {
        let viewController = RechargeLoadFactory.make()
        return PPNavigationController(rootViewController: viewController)
    }
}
