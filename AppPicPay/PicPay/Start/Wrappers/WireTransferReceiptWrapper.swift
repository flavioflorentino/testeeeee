import Foundation
import UI
import UIKit
import Statement

final class WireTransferReceiptWrapper: WireTransferReceiptContract, HasTopViewController {
    func open(withId id: String) {
        guard let topViewController = topViewController() else { return }
        
        let wireTransferReceiptViewController = WireTransferReceiptFactory.make(receiptId: id)
        let navigationController = UINavigationController(rootViewController: wireTransferReceiptViewController)
        topViewController.present(navigationController, animated: true)
    }
}
