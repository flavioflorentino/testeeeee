import Core
import Registration
import DirectMessage

struct UserManagerWrapper: TokenManagerContract, DMUserManagerContract {
    typealias Dependencies = HasUserManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func updateToken(_ token: String) {
        dependencies.userManager.updateToken(token)
    }
    
    var token: String {
        return dependencies.userManager.token ?? ""
    }
}
