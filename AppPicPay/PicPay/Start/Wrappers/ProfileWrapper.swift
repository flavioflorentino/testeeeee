import UIKit
import Core

final public class ProfileWrapper: ProfileContract {
    public func open(withId id: Int, image: String, isPro: Bool, username: String) {
        let contact = PPContact()
        contact.wsId = id
        contact.imgUrl = image
        contact.businessAccount = isPro
        contact.username = username
        
        let navigation = NavigationWrapper().getCurrentNavigation()
        let topViewController = navigation?.topViewController
        
        guard
            let parentViewController = topViewController,
            let profileViewController = NewProfileProxy.openProfile(contact: contact, parent: parentViewController)
            else {
                return
        }
        
        navigation?.pushViewController(profileViewController, animated: true)
    }
}
