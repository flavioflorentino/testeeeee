import Core
import Card

public final class AuthWrapper: AuthContract, AuthenticationContract {
    private var auth: PPAuth?
    public var isAuthenticated: Bool {
        User.isAuthenticated
    }
    
    public func authenticate(sucessHandler: @escaping (String, Bool) -> Void, cancelHandler: (() -> Void)?) {
        auth = PPAuth.authenticate({ token, isBiometric in
            guard let passwordApp = token else {
                return
            }
            sucessHandler(passwordApp, isBiometric)
        }, canceledByUserBlock: {
            cancelHandler?()
        })
    }
    
    public func authenticate(_ completion: @escaping AuthenticationResult) {
        auth = PPAuth.authenticate({ pin, _ in
            guard let pin = pin else {
                completion(.failure(.nilPassword))
                return
            }
            
            completion(.success(pin))
        }, canceledByUserBlock: {
            completion(.failure(.cancelled))
        })
    }

    public func disableBiometricAuthentication() {
        if PPAuth.useTouchId() {
            PPAuth.clearAuthenticationData()
        }
    }
}
