import Foundation
import Registration

struct RegistrationUserNameWrapper: RegistrationComplianceCompleteContract {
    func start(promoCode: String?) -> UIViewController {
        RegistrationUsernameFactory.make(promoCode: promoCode)
    }
}
