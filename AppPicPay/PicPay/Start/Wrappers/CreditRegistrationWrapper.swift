import UIKit
import Core
import LoanOffer

final class CreditRegistrationWrapper: LoanCreditRegistrationContract {
    func start(with navigation: UINavigationController, status: RegistrationStatus, completion: @escaping CreditRegistrationCompletion) {
        let creditViewModel = CreditViewModel()
        creditViewModel.creditForm.isLoanFlow = true
        
        creditViewModel.loadRegistrationStatus { error in
            guard error == nil else {
                completion(.failure(.unknown))
                return
            }
            
            creditViewModel.loadRegistrationData { error in
                guard error == nil else {
                    completion(.failure(.unknown))
                    return
                }
                
                let creditForm = creditViewModel.creditForm
                creditForm.account = creditViewModel.account
                creditForm.isLoanFlow = true
                
                let coordinator = CPRegistrationCoordinator(with: navigation, creditForm: creditForm)
                CPRegistrationCoordinator.shared = coordinator
                
                if status == .review {
                    let viewModel = CreditAttachDocViewModel(with: creditForm, container: DependencyContainer(), isReview: true)
                    let controller = CreditAttachDocViewController(with: viewModel)
                    completion(.success(controller))
                } else {
                    let viewModel = CreditPersonalDataViewModel(with: creditForm, container: DependencyContainer())
                    let controller = CreditPersonalDataViewController(with: viewModel)
                    completion(.success(controller))
                }
            }
        }
    }
}
