import ReceiptKit
import UIKit

final public class MGMWrapper: MGMContract {
    public func open(with parent: UIViewController) -> Bool {
        guard let socialShareCodeController = ViewsManager.peopleSearchStoryboardViewController("SocialShareCode") as? SocialShareCodeViewController else {
            return false
        }
        
        socialShareCodeController.mgmType = .user
        
        parent.present(DismissibleNavigationViewController(rootViewController: socialShareCodeController), animated: true)
        
        return true
    }
}
