import Foundation
import UI
import Card

final class CardCreditFlow: CardFlowProtocol {
    var childViewController: [UIViewController] = []
    
    var viewController: UIViewController?
    
    var navigationController: UINavigationController?
    var hasCard = false
    func startAnotherFlow() {
        if hasCard {
            startToHome()
        } else {
            registrationCard()
        }
    }
    
    private func registrationCard() {
        let viewController = CreditViewController(with: CreditViewModel())
        navigationController?.viewControllers = [viewController]
    }
    
    private func startToHome() {
        let creditPicPayCoordinator = CreditPicPayCoordinator(navigationController: navigationController ?? UINavigationController())
        SessionManager.sharedInstance.currentCoordinator = creditPicPayCoordinator
        creditPicPayCoordinator.startHomeCoordinatorByPresent()
    }
}
