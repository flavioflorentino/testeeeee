import UIKit
import DirectMessageSB

public final class DMSBLegacyFactoryWrapper: DMSBLegacyFactory {
    public func makeProfileViewController(withId id: String) -> UIViewController? {
        ProfileFactory.make(profileId: id)
    }
}
