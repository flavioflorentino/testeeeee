import Foundation
import PIX
import UIKit

public final class IdentityValidationWrapper: IdentityValidationContract {
    var identityValidationCoordinator: IdentityValidationFlowCoordinator?

    public func openIdentityValidation() {
        guard let navigationController = NavigationWrapper().getCurrentNavigation() else { return }
        let coordinator = IdentityValidationFlowCoordinator(from: navigationController, originFlow: .settings)
        coordinator.start()
        identityValidationCoordinator = coordinator
    }
}
