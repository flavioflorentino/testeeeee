import P2PLending

final class TransactionSceneWrapper: TransactionSceneProvider {
    func transactionScene(for borrower: LendingFriend, value: Double, offerIdentifier: UUID) -> UIViewController {
        let orchestrator = P2PLendingOrchestrator(borrower: borrower, value: value, offerIdentifier: offerIdentifier)
        return orchestrator.createPaymentContainer()
    }
}
