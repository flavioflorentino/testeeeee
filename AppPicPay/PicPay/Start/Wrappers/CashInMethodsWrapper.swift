import CashIn
import Loan
import LoanOffer
import P2PLending

public final class CashInMethodsWrapper: CashInMethodsContract { }

public extension CashInMethodsWrapper {
    func startAddValue(on viewController: UIViewController, methodId: Int) {
        let controller = RechargeValueLoaderFactory.make(methodId: methodId)
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    func startGovernmentRecharge(on viewController: UIViewController) {
        let controller = GovernmentRechargeMethodFactory.make(for: [])
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    func goHome() {
        AppManager.shared.mainScreenCoordinator?.showHomeScreen()
    }
}
