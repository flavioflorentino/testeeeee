import Core
import Socket
import DirectMessage

struct DMSocketWrapper: DMSocketContract {
    typealias Dependencies = HasSocketManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func publish(content: Data, onTopic topic: String, completion: @escaping Socket.PublishCompletion) {
        dependencies.socketManager.publish(socketIdentifier: DMSocket.identifier,
                                           content: content,
                                           additionalProperties: [:],
                                           completion: completion)
    }
}
