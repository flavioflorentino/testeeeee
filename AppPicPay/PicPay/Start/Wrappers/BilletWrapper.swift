import AnalyticsModule
import Billet
import Core
import FeatureFlag
import Loan
import P2PLending
import ReceiptKit
import UIKit

public final class BilletWrapper { }

extension BilletWrapper: LoanBilletContract {
    public func openBilletFlow(
        withBarcode barcode: String,
        placeholder: String,
        origin: String,
        completion: @escaping BilletCompletion
    ) {
        let origin = BilletOrigin(value: origin)
        let formController = BilletFormFactory.make(with: origin, linecode: barcode, description: placeholder)
        completion(.success(formController))
    }
}

extension BilletWrapper: BilletFlowContract {    
    public func openBilletPayment(with model: BilletResponse,
                                  description: String?,
                                  origin: BilletOrigin,
                                  from viewController: UIViewController) {
        let orchestratorModel = BilletPaymentOrchestratorModel(origin: origin, model: model, description: description)
        let orchestrator = BilletPaymentOrchestrator(orchestrator: orchestratorModel)
        guard let paymentController = orchestrator.paymentViewController else { return }
        Analytics.shared.log(CheckoutEvent.checkoutScreenViewed(CheckoutEvent.CheckoutEventOrigin.bills))
        viewController.navigationController?.pushViewController(paymentController, animated: true)
    }
}

extension BilletWrapper: BilletContract {
    public func open(with parent: UIViewController) -> Bool {
        guard let rootViewController = UIApplication.shared.keyWindow?.rootViewController else { return false }
        
        parent.dismiss(animated: true) {
            DGHelpers.openBoletoFlow(viewController: rootViewController, origin: BilletOrigin.receipt.rawValue)
        }
        
        return true
    }
}

extension BilletWrapper: BilletSceneProvider {
    public func billetScene(for digitableLine: String, with description: String) -> UIViewController {
        BilletFormFactory.make(with: .p2pLending, linecode: digitableLine, description: description)
    }
}
