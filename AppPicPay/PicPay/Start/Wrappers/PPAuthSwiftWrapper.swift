import Foundation
import Card
import P2PLending

public final class PPAuthSwiftWrapper: Card.PPAuthSwiftContract, P2PLending.PPAuthSwiftContract {
    public func handlePassword() {
        PPAuthSwift.handlePasswordError()
    }
}
