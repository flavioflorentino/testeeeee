import CoreSentinel
import Foundation

struct NewRelicWrapper: NewRelicContract {
    let instance: NewRelic.Type?

    func track(_ type: String, name: String?, attributes: [String : Any]?) {
        instance?.recordCustomEvent(type, name: name, attributes: attributes)
    }
}
