// Future developer: if you're removing firebase from the projet, first of all, congrats to you!
// I wish I could have done that.
// Second: you can remove this wrapper, it's only needed for Firebase compatibility
// TL;DR: Firebase won't work inside a module, so.. we've created a wrapper for it.

import FirebaseRemoteConfig
import FeatureFlag
import AnalyticsModule
import Core

public struct RemoteConfigWrapper: ThirdPartyRemoteConfigContract {
    public let instance: RemoteConfig
    
    public func activate() {
        instance.activate(completion: nil)
    }
    
    public func fetch(withExpirationDuration: TimeInterval, successHandler: @escaping () -> Void) {
        Analytics.shared.log(StartEvent.remoteConfigFetchStarted)
               
        instance.fetch(withExpirationDuration: withExpirationDuration) { status, _ in
            Analytics.shared.log(StartEvent.remoteConfigFetchEnded(
                success: status == .success,
                throttled: status == .throttled)
            )
            
            guard status == .success else { return }
            
            Analytics.shared.log(StartEvent.remoteConfigActivated(success: true))
            KVStore().setBool(true, with: .isRemoteConfigLoaded)
            successHandler()
        }
    }
    
    public func object<T>(type: T.Type = T.self, for key: String) -> T? {
        switch type {
        case is Bool.Type:
            return instance[key].boolValue as? T
        case is String.Type:
            return instance[key].stringValue as? T
        case is NSNumber.Type:
            return instance[key].numberValue as? T
        case is Int.Type:
            return instance[key].numberValue.intValue as? T
        default:
            return instance[key] as? T
        }
    }
}
