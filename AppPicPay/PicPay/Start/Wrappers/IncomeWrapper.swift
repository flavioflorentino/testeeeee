import Foundation
import UI
import UIKit
import Statement

final class IncomeWrapper: IncomeContract, HasTopViewController {
    func open() {
        guard let topViewController = topViewController() else { return }
        
        let savingsViewController = SavingsViewController()
        topViewController.present(savingsViewController, animated: true)
    }
}
