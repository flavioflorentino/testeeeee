import AnalyticsModule
import Core
import CoreSentinel
import Foundation

final class PicPayDefaultApiCompletion: ApiCompletion {
    private let upgradeRequiredNotification = NSNotification.Name(rawValue: "WSAppNeedUpdate")
    private let userNotAuthenticated = NSNotification.Name(rawValue: "WSUserNotAuthenticated")

    override func intercept<E: Decodable>(_ result: ApiResult<E>, response: HTTPURLResponse? = nil, request: URLRequest? = nil) {
        switch result {
        case .failure(.upgradeRequired):
            NotificationCenter.default.post(name: upgradeRequiredNotification, object: nil)
            track(response: response, request: request)
            return
        case let .failure(.notFound(picPayError)),
             let .failure(.unauthorized(picPayError)),
             let .failure(.badRequest(picPayError)),
             let .failure(.tooManyRequests(picPayError)):
            track(requestError: picPayError, response: response, request: request)
            postNotification(for: picPayError)
        case let .failure(.decodeError(error)),
             let .failure(.unknown(error?)):
            track(response: response, request: request, error: error)
        case let .failure(.malformedRequest(string?)):
            trackError(message: string, httpResponse: response, request: request)
        case .failure:
            track(response: response, request: request)
        case .success:
            let headers = response?.allHeaderFields ?? [:]
            syncData(with: headers)
            trackApiEvents(with: headers)
        }
        
        super.intercept(result)
    }
}

extension PicPayDefaultApiCompletion {
    private func track(response: HTTPURLResponse?, request: URLRequest?, error: Error? = nil) {
        trackError(message: error?.localizedDescription, httpResponse: response, request: request)
    }
    
    private func track(requestError: RequestError, response: HTTPURLResponse?, request: URLRequest?) {
        trackError(code: requestError.picpayCode, message: requestError.message, httpResponse: response, request: request)
    }
    
    private func syncData(with allHeaderFields: [AnyHashable: Any]) {
        guard
            !allHeaderFields.isEmpty,
            let syncdataJsonString = allHeaderFields["syncdata"] as? String
            else { return }
        
        NotificationCenter.default.post(name: ApiNotificationsName.syncData, object: nil, userInfo: ["json": syncdataJsonString])
    }
    
    private func postNotification(for requestError: RequestError) {
        if requestError.picpayCode ==  "1" {
            NotificationCenter.default.post(name: userNotAuthenticated, object: nil, userInfo: ["error": requestError])
        }
    }
}

extension PicPayDefaultApiCompletion {
    private struct ApiEvent: AnalyticsKeyProtocol {
        public let name: String
        public let properties: [String: Any]
        private let providers: [AnalyticsProvider] = [.mixPanel]
        
        func event() -> AnalyticsEventProtocol {
            AnalyticsEvent(name, properties: properties, providers: providers)
        }
    }
    
    private func trackApiEvents(with allHeaderFields: [AnyHashable: Any]) {
        guard
              !allHeaderFields.isEmpty,
              let headers = allHeaderFields["mobile_analytics_events"] as? String,
              let data = headers.data(using: .utf8, allowLossyConversion: false),
              let dictionaryEvents = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
              let name = dictionaryEvents["name"] as? String,
              let propertiesList = dictionaryEvents["properties"] as? [[String: Any]] else {
            return
        }
        
        var properties: [String: Any] = [:]
        for propertieDictionary in propertiesList {
            guard let value = propertieDictionary["value"],
                  let name = propertieDictionary["name"] as? String else {
                break
            }
            properties[name] = value
        }
        
        let event = ApiEvent(name: name, properties: properties)
        Analytics.shared.log(event)
    }
}

extension PicPayDefaultApiCompletion {
    private struct ApiErrorEvent: AnalyticsKeyProtocol {
        public let properties: [String: Any]
        private let name = "API Error"
        private let providers: [AnalyticsProvider] = [.mixPanel]
        
        func event() -> AnalyticsEventProtocol {
            AnalyticsEvent(name, properties: properties, providers: providers)
        }
    }
    
    private func trackError(code: String? = nil,
                            message: String? = nil,
                            httpResponse: HTTPURLResponse? = nil,
                            request: URLRequest? = nil) {
        guard let response = httpResponse else {
            return
        }

        let errorProperties: [String: Any] = [
            "code": code ?? "",
            "message": message ?? "",
            "endpoint": response.url?.path ?? "",
            "status_code": response.statusCode
        ]
        
        Analytics.shared.log(ApiErrorEvent(properties: errorProperties))
        
        let parameters = APISentinelParameters(
            code: code,
            message: message,
            endpoint: response.url?.path,
            statusCode: response.statusCode,
            correlationId: request?.allHTTPHeaderFields?[CorrelationId.headerField],
            newPaymentArchitecture: request?.allHTTPHeaderFields?[NewPaymentArchitecture.headerField]
        )
        Sentinel.shared.track(APISentinel.apiError(parameters: parameters))
    }
}
