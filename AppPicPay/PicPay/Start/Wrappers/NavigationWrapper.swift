import UI

final class NavigationWrapper: NavigationContract {
    func getCurrentNavigation() -> UINavigationController? {
        let mainCoordinator = AppManager.shared.mainScreenCoordinator
        guard let currentController = mainCoordinator?.currentController() else {
            return nil
        }
        
        switch currentController {
        case is LegacyHomeViewController:
            return mainCoordinator?.homeNavigation()
        case is PaymentMethodsViewController:
            return mainCoordinator?.walletNavigation()
        case is NotificationsViewController:
            return mainCoordinator?.notificationsNavigation()
        default:
            return currentController.navigationController
        }
    }
    
    func updateCurrentCoordinating(_ coordinating: Coordinating?) {
        SessionManager.sharedInstance.currentCoordinating = coordinating
    }
}
