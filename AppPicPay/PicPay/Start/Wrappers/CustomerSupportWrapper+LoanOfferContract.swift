import CustomerSupport
import Foundation
import LoanOffer

extension CustomerSupportWrapper: LoanOfferCustomerSupportContract {
    public func show(article: FAQArticle) {
        let controller = FAQFactory.make(option: .article(id: article.rawValue))
        NavigationWrapper().getCurrentNavigation()?.present(controller, animated: true)
    }
}
