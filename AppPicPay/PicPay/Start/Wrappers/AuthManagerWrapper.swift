import Foundation
import Core

public final class AuthManagerWrapper: AuthenticationManagerContract {
    typealias Dependencies = HasAuthManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    public func logout(fromContext context: UIViewController?, completion: ((Error?) -> Void)?) {
        dependencies.authManager.logout(fromContext: context, completion: completion)
    }
}
