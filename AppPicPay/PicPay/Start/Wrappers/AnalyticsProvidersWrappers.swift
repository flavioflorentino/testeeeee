import Foundation
import AnalyticsModule
import AppsFlyerLib
import FirebaseAnalytics
import Mixpanel
import EventTracker
import Core

private protocol AnalyticsDefaultable { }

extension AnalyticsDefaultable {
    private var defaultProperties: [String: Any] {
        let isPicPayLover = ConsumerManager.shared.consumer?.isPicPayLover ?? false
        return [
            "Cartão de crédito cadastrado": !CreditCardManager.shared.cardList.isEmpty,
            "consumer_id": ConsumerManager.shared.consumer?.wsId ?? 0,
            "installation_id": UIDevice.current.identifierForVendor?.uuidString ?? 0,
            "is_picpay_lover": isPicPayLover ? "yes" : "no"
        ]
    }
    
    func mergingDefaults(with properties: [String: Any] = [:]) -> [String: Any] {
        guard !properties.isEmpty else {
            return defaultProperties
        }
        return properties.merging(defaultProperties) { first, _ in first }
    }
}

struct AppsFlyerWrapper: AppsFlyerContract, AnalyticsDefaultable {
    let instance: AppsFlyerLib?
    
    func track(_ event: String, properties: [String: Any]) {
        instance?.logEvent(event, withValues: mergingDefaults(with: properties))
    }
}

struct FirebaseWrapper: FirebaseContract, AnalyticsDefaultable {
    func track(_ event: String, properties: [String: Any]) {
        FirebaseAnalytics.Analytics.logEvent(event, parameters: mergingDefaults(with: properties))
    }
}

struct MixPanelWrapper: MixPanelContract, AnalyticsDefaultable {
    let instance: MixpanelInstance

    func track(_ event: String, properties: [String: Any]) {
        updateDistinctIdIfNeeded()
        instance.track(event: event, properties: mergingDefaults(with: properties) as? Properties)
    }
    
    func time(_ event: String) {
        updateDistinctIdIfNeeded()
        instance.time(event: event)
    }

    func reset() {
        instance.reset()
    }

    private func updateDistinctIdIfNeeded() {
        // If not logged make sure we have the same distinctId as EventTracker.
        // Logged users use consumer/sellerid as identity so they already are the same.
        guard ConsumerManager.shared.consumer == nil,
              instance.distinctId != EventTrackerManager.tracker.distinctId else { return }

        instance.identify(distinctId: EventTrackerManager.tracker.distinctId)
    }
}

struct EventTrackerWrapper: EventTrackerContract, AnalyticsDefaultable {
    func track(_ event: String, properties: [String: Any]) {
        EventTrackerManager.tracker.track(eventName: event, properties: properties.mapValues(AnyCodable.init))
    }

    func reset() {
        EventTrackerManager.tracker.reset()
    }
}

@objcMembers
public final class MixPanelOBJC: NSObject, AnalyticsDefaultable {
    override init() { }
    
    var instance: MixpanelInstance {
        Mixpanel.mainInstance()
    }
    
    var distinctId: String {
        instance.distinctId
    }
    
    func set(people: [String: String]) {
        instance.people.set(properties: people as Properties)
    }
    
    func createAlias(_ alias: String, forDistinctID: String) {
        instance.createAlias(alias, distinctId: forDistinctID)
    }
    
    func track(_ event: String, properties: [String: Any] = [:]) {
        let mixPanelProperties = mergingDefaults(with: properties) as? Properties
        instance.track(event: event, properties: mixPanelProperties)
    }
    
    func timeEvent(_ event: String) {
        instance.time(event: event)
    }
}

extension NSString: MixpanelType {
    /**
     Checks if this object has nested object types that Mixpanel supports.
     Will always return true.
     */
    public func isValidNestedTypeAndValue() -> Bool { true }
    
    public func equals(rhs: MixpanelType) -> Bool {
        if rhs is NSString {
            return self == rhs as? NSString
        }
        return false
    }
}
