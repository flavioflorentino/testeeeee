import UIKit
import Core

final public class ContactListWrapper: ContactListContract {
    public func open(withFeedId id: String) {
        let model = LikedContactListViewModel(feedId: id)
        
        let controller = ViewsManager.socialStoryboardViewController(withIdentifier: "ContactListTableViewController")
        guard let contactListController = controller as? ContactListTableViewController else {
            return
        }
        contactListController.model = model
        
        NavigationWrapper().getCurrentNavigation()?.pushViewController(contactListController, animated: true)
    }
}
