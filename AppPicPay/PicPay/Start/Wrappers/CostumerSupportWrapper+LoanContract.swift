import CustomerSupport
import Foundation
import Loan

extension CustomerSupportWrapper: LoanCustomerSupportContract {
    public func show(article: FAQArticle) {
        let controller = FAQFactory.make(option: .article(id: article.rawValue))
        NavigationWrapper().getCurrentNavigation()?.present(controller, animated: true)
    }
    
    public func push(article: FAQArticle) {
        let controller = FAQFactory.make(option: .article(id: article.rawValue))
        NavigationWrapper().getCurrentNavigation()?.pushViewController(controller, animated: true)
    }
}
