import Core
import CoreLegacy
import DirectMessage
import DirectMessageSB
import Feed
import Loan
import LoanOffer
import P2PLending
import PIX
import Card

public enum ConsumerWrapperError: Error {
    case nilConsumer
    case nilBalance
    
    var localizedDescription: String {
        switch self {
        case .nilConsumer:
            return "ConsumerManager's consumer instance is nil"
        case .nilBalance:
            return "ConsumerManager's balance value is nil"
        }
    }
}

struct ConsumerWrapper: CardConsumerContract, ChatUser, DMConsumerContract, FeedConsumerContract, LoanConsumerContract, LoanOfferConsumerContract, PixConsumerContract, UserContract {
    typealias Dependencies = HasConsumerManager & HasUserManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    public var user: PersonConvertible? {
        dependencies.consumerManager.consumer
    }
    
    var consumerBalance: String {
        guard let consumer = dependencies.consumerManager.consumer else {
            return ConsumerWrapperError.nilConsumer.localizedDescription
        }
        
        guard let balance = consumer.balance else {
            return ConsumerWrapperError.nilBalance.localizedDescription
        }
        
        return CurrencyFormatter.brazillianRealString(from: balance)
    }

    var messagingId: Int {
        dependencies.consumerManager.consumer?.wsId ?? .min
    }

    var type: ChatUserType = .consumer
    
    var consumerId: Int {
        guard let consumer = dependencies.consumerManager.consumer else {
            return 0
        }
        return consumer.wsId
    }
    
    var username: String? {
        dependencies.consumerManager.consumer?.username
    }
    
    var token: String {
        dependencies.userManager.token ?? ""
    }
    
    var avatarUrl: String? {
        dependencies.consumerManager.consumer?.img_url
    }
    
    var name: String {
        dependencies.consumerManager.consumer?.name ?? ""
    }
    
    var profileName: String? {
        dependencies.consumerManager.consumer?.name ?? username
    }

    var imageURL: URL? {
        user?.imageURL
    }
    
    var consumerEmail: String {
        dependencies.consumerManager.consumer?.email ?? ""
    }
}

extension MBConsumer: PersonConvertible {
    public var imageURL: URL? {
        guard let string = img_url else {
            return nil
        }
        return URL(string: string)
    }
    
    public var firstName: String? {
        name?.components(separatedBy: .whitespaces).first
    }
}
