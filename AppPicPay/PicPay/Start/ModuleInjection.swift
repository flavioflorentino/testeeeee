import Advertising
import Billet
import Card
import CashIn
import Core
import CustomerSupport
import DynamicRegistration
import DirectMessage
import DirectMessageSB
import Feed
import IncomeTaxReturn
import Loan
import LoanOffer
import LendingHub
import P2PLending
import PIX
import Registration
import SecurityModule
import Statement
import UI
import Store
import PasswordReset
import ReceiptKit

struct ModuleInjection {
    func setup() {
        injectApiCompletion()
        injectAuth()
        injectDeepLink()
        injectReceipt()
        injectBillet()
        injectContactList()
        injectProfile()
        injectRechargeMethods()
        injectConsumerManager()
        injectNavigation()
        setupDeeplinkResolvers()
        injectFAQ()
        injectWebView()
        injectCreditRegistration()
        injectCustomerSupport()
        injectAuthManager()
        injectUserNameRegistration()
        injectTransaction()
        injectUserManager()
        injectIdentityValidation()
        injectSocketManager()
        injectIncome()
        injectWireTransferReceipt()
        injectNavigationToCardSettings()
        injectCashInMethods()
        injectMGM()
        injectDM()
    }

    func getAppAccessProperties(container: DependencyContainer = DependencyContainer()) -> AppAccessProperties {
        let flag = container.featureManager.isActive(.releaseAppProtectionShowBool)
        let isAuthenticated = User.isAuthenticated
        let protectionKey = container.kvStore.boolFor(AppProtectionKey.userDeviceProtectedValue)
        let username = ConsumerManager.shared.consumer?.username ?? ""

        return AppAccessProperties(isFlagOn: flag,
                                   isLoggedIn: isAuthenticated,
                                   isProtectionActive: protectionKey,
                                   username: username)
    }

    private func injectApiCompletion() {
        ApiCompletionSetup.inject(type: PicPayDefaultApiCompletion.self)
    }

    private func injectNavigationToCardSettings() {
        CardSetup.inject(instance: CardCreditFlow())
    }

    private func injectAuth() {
        let wrapper = AuthWrapper()
        let ppAuthSwiftWrapper = PPAuthSwiftWrapper()
        CardSetup.inject(instance: wrapper)
        LoanLegacySetup.shared.inject(instance: wrapper)
        LoanOfferLegacySetup.shared.inject(instance: wrapper)
        PIXSetup.inject(instance: wrapper)
        CardSetup.inject(instance: ppAuthSwiftWrapper)
        FeedSetup.inject(instance: wrapper)
        PasswordResetSetup.inject(instance: wrapper)
        P2PLendingSetup.inject(instance: wrapper)
        P2PLendingSetup.inject(instance: ppAuthSwiftWrapper)
    }

    private func injectDeepLink() {
        let wrapper = DeeplinkWrapper()
        BilletSetup.shared.inject(instance: wrapper)
        AdvertisingSetup.inject(instance: wrapper)
        CardSetup.inject(instance: wrapper)
        RegistrationSetup.shared.inject(instance: wrapper)
        PIXSetup.inject(instance: wrapper)
        CashInSetup.shared.inject(instance: wrapper)
        COPSetup.inject(instance: wrapper)
        StatementSetup.inject(instance: wrapper)
        IncomeTaxReturnSetup.inject(instance: wrapper)
        FeedSetup.inject(instance: wrapper)
        ReceiptKitSetup.shared.inject(instance: wrapper)
        CustomerSupportManager.shared.inject(instance: wrapper)
        LendingHubSetup.shared.inject(instance: wrapper)
    }

    private func injectReceipt() {
        let wrapper = ReceiptWrapper()
        BilletSetup.shared.inject(instance: wrapper)
        StatementSetup.inject(instance: wrapper)
        FeedSetup.inject(instance: wrapper)
    }

    private func injectBillet() {
        let wrapper = BilletWrapper()
        BilletSetup.shared.inject(instance: wrapper)
        LoanLegacySetup.shared.inject(instance: wrapper)
        P2PLendingSetup.inject(instance: wrapper)
        ReceiptKitSetup.shared.inject(instance: wrapper)
    }

    private func injectContactList() {
        let wrapper = ContactListWrapper()
        BilletSetup.shared.inject(instance: wrapper)
    }

    private func injectProfile() {
        let wrapper = ProfileWrapper()
        BilletSetup.shared.inject(instance: wrapper)
    }

    private func injectRechargeMethods() {
        let wrapper = RechargeMethodsWrapper()
        LoanLegacySetup.shared.inject(instance: wrapper)
    }

    private func injectConsumerManager() {
        let wrapper = ConsumerWrapper()
        LoanLegacySetup.shared.inject(instance: wrapper)
        LoanOfferLegacySetup.shared.inject(instance: wrapper)
        P2PLendingSetup.inject(instance: wrapper)
        PIXSetup.inject(instance: wrapper)
        DMLegacySetup.inject(instance: wrapper)
        CardSetup.inject(instance: wrapper)
        DMSBLegacySetup.shared.inject(instance: wrapper)
        FeedSetup.inject(instance: wrapper)
    }

    private func injectNavigation() {
        let wrapper = NavigationWrapper()
        CardSetup.inject(instance: wrapper)
        P2PLendingSetup.inject(instance: wrapper)
        CustomerSupportManager.shared.inject(instance: wrapper)
        PIXSetup.inject(instance: wrapper)
        LoanLegacySetup.shared.inject(instance: wrapper)
        LoanOfferLegacySetup.shared.inject(instance: wrapper)
        COPSetup.inject(instance: wrapper)
        AdvertisingSetup.inject(instance: wrapper)
    }

    private func setupDeeplinkResolvers() {
        let container = DependencyContainer()
        DeeplinkResolversHelper.register(CardDeeplinkResolver())
        DeeplinkResolversHelper.register(HelpcenterDeeplinkResolver())
        DeeplinkResolversHelper.register(LendingDeeplinkResolver(dependencies: container))
        DeeplinkResolversHelper.register(LoanOfferDeeplinkResolver())
        DeeplinkResolversHelper.register(LoanDeeplinkResolvers.make())
        DeeplinkResolversHelper.register(TicketDeeplinkResolver(dependencies: container))
        DeeplinkResolversHelper.register(LegacyTicketDeeplinkResolver(dependencies: container))
        DeeplinkResolversHelper.register(
            PIXDeeplinkResolver(
                dependencies: container,
                paymentOpener: PaymentPIXProxy(),
                bankSelectorType: BanksFactory.self,
                qrCodeScannerType: ScannerBaseViewController.self,
                receiptType: TransactionReceipt.self
        ))
        DeeplinkResolversHelper.register(
            StoreServerDrivenUIDeeplinkResolver(
                with: DGHelpers(),
                paths: ["/store/v2", "/compartilhar_loja"]
            )
        )
        DeeplinkResolversHelper.register(COPDeeplinkResolver())
        DeeplinkResolversHelper.register(RequestTicketByReasonDeeplinkResolver())
    }

    private func injectFAQ() {
        let wrapper = FAQWrapper()
        LoanLegacySetup.shared.inject(instance: wrapper)
        LoanOfferLegacySetup.shared.inject(instance: wrapper)
    }
    
    private func injectWebView() {
        let wrapper = WebViewWrapper()
        BilletSetup.shared.inject(instance: wrapper)
    }

    private func injectCreditRegistration() {
        let wrapper = CreditRegistrationWrapper()
        LoanOfferLegacySetup.shared.inject(instance: wrapper)
    }

    private func injectCustomerSupport() {
        let wrapper = CustomerSupportWrapper(dependencies: DependencyContainer())

        LoanLegacySetup.shared.inject(instance: wrapper)
        LoanOfferLegacySetup.shared.inject(instance: wrapper)
        PIXSetup.inject(instance: wrapper)

        CustomerSupportManager.shared.inject(instance: wrapper)
        #if DEBUG
            CustomerSupportManager.shared.setDebugMode(enabled: true)
        #endif
        CustomerSupportManager.shared.setLocalizableFile(fileName: "Localizable-Zendesk")
    }

    private func injectAuthManager() {
        RegistrationSetup.shared.inject(instance: AuthManagerWrapper())
    }

    private func injectUserNameRegistration() {
        RegistrationSetup.shared.inject(instance: RegistrationUserNameWrapper())
    }

    private func injectTransaction() {
        P2PLendingSetup.inject(instance: TransactionSceneWrapper())
    }

    private func injectUserManager() {
        let userManager = UserManagerWrapper()
        RegistrationSetup.shared.inject(instance: userManager)
        DMLegacySetup.inject(instance: userManager)
        PIXSetup.inject(instance: userManager)
    }

    private func injectIdentityValidation() {
        PIXSetup.inject(instance: IdentityValidationWrapper())
    }

    private func injectSocketManager() {
        DMLegacySetup.inject(instance: DMSocketWrapper())
    }

    private func injectCashInMethods() {
        let wrapper = CashInMethodsWrapper()
        CashInLegacySetup.shared.inject(instance: wrapper)
    }

    private func injectIncome() {
        StatementSetup.inject(instance: IncomeWrapper())
    }

    private func injectWireTransferReceipt() {
        StatementSetup.inject(instance: WireTransferReceiptWrapper())
    }

    private func injectMGM() {
        let wrapper = MGMWrapper()
        ReceiptKitSetup.shared.inject(instance: wrapper)
    }
    
    private func injectDM() {
        let factory = DMSBLegacyFactoryWrapper()
        DMSBLegacySetup.shared.inject(instance: factory)
    }
}
