import FeatureFlag
import Foundation

struct WalletHeaderWithStatementEventProperty: ApplicationEventProperty {
    typealias Dependency = HasFeatureManager

    // MARK: - Properties
    private let dependency: Dependency

    init(dependency: Dependency) {
        self.dependency = dependency
    }
    
    var properties: [String: Any] {
        [
            "is_new_layout_buttons_wallet": dependency.featureManager.isActive(.experimentWalletHeaderLayoutV2PresentingBool)
        ]
    }
}
