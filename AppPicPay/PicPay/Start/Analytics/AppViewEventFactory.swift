import Foundation

protocol ApplicationEventProperty {
    var properties: [String: Any] { get }
}

struct AppViewEvent: ApplicationEventProperty {
    private let propertySources: [ApplicationEventProperty]

    var properties: [String: Any] {
        Dictionary(
            propertySources.flatMap(\.properties),
            uniquingKeysWith: { _, last in last }
        )
    }

    init(propertySources: [ApplicationEventProperty] = []) {
        self.propertySources = propertySources
    }
}

enum AppViewEventFactory {
    static func make() -> AppViewEvent {
        let container = DependencyContainer()

        // Adicione abaixos as propriedades que serão enviadas junto ao evento App View
        return AppViewEvent(propertySources: [
            PayPeopleApplicationEventProperty(dependency: container),
            InstallmentsEventProperty(dependency: container),
            NewStoreEventProperty(dependency: container),
            PromotionsEventProperty(dependency: container),
            WalletHeaderWithStatementEventProperty(dependency: container),
            HomeSearchApplicationEventProperty(dependency: container),
            TransactionsApplicationEventProperty(dependency: container),
            HomeNavigationApplicationEventProperty(dependency: container),
            UpdatePrivacyApplicationEventProperty(dependency: container)
        ])
    }
}
