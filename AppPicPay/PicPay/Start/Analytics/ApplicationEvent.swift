import AnalyticsModule

extension ApplicationEvent: AnalyticsKeyProtocol {
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

enum ApplicationEvent {
    case appView
    case applicationOpened
    case applicationPaused
    case applicationResumed
    case applicationClosed

    private var name: String {
        switch self {
        case .appView:
            return "App View"
        case .applicationOpened:
            return "Application Opened"
        case .applicationPaused:
            return "Application Paused"
        case .applicationResumed:
            return "Application Resumed"
        case .applicationClosed:
            return "Application Closed"
        }
    }

    private var properties: [String: Any] {
        switch self {
        case .appView:
            return AppViewEventFactory.make().properties
        default:
            return [:]
        }
    }

    private var providers: [AnalyticsProvider] {
        switch self {
        case .appView:
            return [.mixPanel, .firebase]
        default:
            return [.eventTracker]
        }
    }
}
