import AnalyticsModule
import Core

enum StartEvent: AnalyticsKeyProtocol {
    case remoteConfigFetchStarted
    case remoteConfigFetchEnded(success: Bool, throttled: Bool)
    case remoteConfigActivated(success: Bool)
    
    private var name: String {
        switch self {
        case .remoteConfigFetchStarted:
            return "Remote Config - Start"
        case .remoteConfigFetchEnded:
            return "Remote Config - Fetch"
        case .remoteConfigActivated:
            return "Remote Config - Activate"
        }
    }
    
    var providers: [AnalyticsProvider] {
        return [.firebase]
    }
    
    var properties: [String: Any] {
        switch self {
        case let .remoteConfigFetchEnded(success, throttled):
            return [
                "success": success.stringValue,
                "throttled": throttled.stringValue
            ]
        case let .remoteConfigActivated(success):
            return ["success": success.stringValue]
        default:
            return [:]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        return AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

private extension Bool {
    var stringValue: String {
        self ? "TRUE" : "FALSE"
    }
}
