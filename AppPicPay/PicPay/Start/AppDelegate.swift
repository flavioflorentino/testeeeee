import AccessibilityKit
import AnalyticsModule
import CoreSpotlight
import Core
import DirectMessageSB
import FirebaseDynamicLinks
import Foundation
import SecurityModule
import UI

protocol RootViewControllerManager: AnyObject {
    func changeRootViewController(_ viewController: UIViewController)
}

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {
    private typealias Dependencies = HasAnalytics & HasAccessibilityManager
    private let dependencies: Dependencies = DependencyContainer()
    private var appAccess: AppAccessProtocol?

    var window: UIWindow?
    lazy var setup = BasicSetup()
    lazy var thirdParties = ThirdParties()
    lazy var moduleInjection = ModuleInjection()
    lazy var accessibilitySetup = AccessibilitySetup(dependencies: dependencies)
    lazy var mainCoordinator = MainCoordinator(
        dependencies: DependencyContainer(),
        rootViewControllerManager: self,
        needsSyncRemoteConfigLoad: !KVStore().boolFor(.isRemoteConfigLoaded)
    )
    
    func application(_ application: UIApplication,
                     // swiftlint:disable:next discouraged_optional_collection
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        moduleInjection.setup()
        
        #if DEBUG
        guard !isRunningTests else {
            thirdParties.setupForTesting()
            window = UIWindow()
            window?.rootViewController = UIViewController()
            return true
        }
        #endif
        
        setup.start()
        setup.appearance()
        thirdParties.setup()
        
        // Precisam ser chamados depois de atualizar as feature flags
        setup.configurePinning()
        
        application.registerForRemoteNotifications()
        
        window? = mainCoordinator.start(with: launchOptions)
        appAccess = thirdParties.setupAppAccessController(window: window)
        appAccess?.applicationDidFinishLaunching(with: moduleInjection.getAppAccessProperties())

        dependencies.analytics.log(ApplicationEvent.applicationOpened)
        accessibilitySetup.start()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        appAccess?.applicationWillResignActive(with: moduleInjection.getAppAccessProperties())
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        dependencies.analytics.log(ApplicationEvent.applicationPaused)
        appAccess?.applicationDidEnterBackground(with: moduleInjection.getAppAccessProperties())
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        setup.didBecomeActive()
        thirdParties.didBecomeActive()
        verifyVersionUpdate()
        setup.sendDetectionLog()
        appAccess?.applicationDidBecomeActive(with: moduleInjection.getAppAccessProperties())
        UIApplication.shared.applicationIconBadgeNumber = 0
        dependencies.analytics.log(ApplicationEvent.applicationResumed)
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        completionHandler(ShortcutManager().handleShortCutItem(shortcutItem))
    }
}

// MARK: Image Cache
extension AppDelegate {
    func applicationWillTerminate(_ application: UIApplication) {
        dependencies.analytics.log(ApplicationEvent.applicationClosed)
        ImageCacher.shared.clearAll()
    }
}

// MARK: - DeepLink
extension AppDelegate {
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        if PdfScanner.shared.processAndScanPdf(url: url as NSURL) ||
            DeeplinkHelper.handleDeeplink(withUrl: url, from: window?.rootViewController) {
            return true
        }
        
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url),
            let url = dynamicLink.url {
            return DynamicLinkHelper.handleFirebaseDynamicLinkDeepLink(url: url)
        }
        
        return false
    }
}

// MARK: - Notifications
extension AppDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        setup.updateNotificationToken(deviceToken)
        thirdParties.updateNotificationToken(deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        setup.updateKVKeyAskedForNotification()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        handleNotification(userInfo, in: application)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        handleNotification(userInfo, in: application)
    }
}

// MARK: - Notifications Handler
private extension AppDelegate {
    func handleNotification(_ userInfo: [AnyHashable: Any], in application: UIApplication) {
        if FeatureManager.isActive(.directMessageSBEnabled), let notification = ChatNotification(dictionary: userInfo) {
            handleSendBirdNotification(notification, in: application)
            return
        }
        
        if let notification = PPNotification(dictionaty: userInfo, viaPush: true) {
            handlePPNotification(notification, in: application)
        }
    }
    
    func handleSendBirdNotification(_ notification: ChatNotification, in application: UIApplication) {
        let sessionManager = SessionManager.sharedInstance

        if application.applicationState == .inactive {
            sessionManager.openSendBirdNotification(notification)
        } else {
            guard let rootView = UIApplication.shared.keyWindow?.rootViewController?.view,
                  !isPresentingChat(from: notification)
            else { return }
            
            ChatNotificationBannerView.present(onView: rootView, viewModel: .init(notification) { notificationView in
                notificationView.dismiss()
                sessionManager.openSendBirdNotification(notification)
            })
        }
    }
    
    func isPresentingChat(from notification: ChatNotification) -> Bool {
        guard let mainScreenCoordinator = AppManager.shared.mainScreenCoordinator,
              let chatPresentingController = mainScreenCoordinator.currentController() as? ChatViewController
        else { return false }
        return chatPresentingController.isFromChannel(url: notification.channel?.channelURL ?? "")
    }
    
    func handlePPNotification(_ notification: PPNotification, in application: UIApplication) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushReceived"), object: nil)
        
        if application.applicationState == .active {
            DispatchQueue.global(qos: .background).async {
                WSConsumer.ackNotification(withId: notification.wsId, completionHandler: { _ in })
            }
            
            showBanner(with: notification)
        } else if application.applicationState == .inactive && !notification.unsuportedVersion {
            SessionManager.sharedInstance.openNotification(notification)
        }
    }
    
    func showBanner(with notification: PPNotification) {
        if notification.unsuportedVersion {
            return
        }
        
        DispatchQueue.main.async { [weak self] in
            guard let window = self?.window else {
            return
        }
            NotificationBannerView.showNotification(notification, superview: window)
        }
    }
}

// MARK: - Handle Deeplinks
extension AppDelegate {
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool { // swiftlint:disable:this discouraged_optional_collection
        switch userActivity.activityType {
        case CSSearchableItemActionType:
            return handleSpotlight(in: userActivity)
        case NSUserActivityTypeBrowsingWeb:
            return handleDeeplink(in: userActivity)
        default:
            break
        }
        return false
    }
    
    func handleSpotlight(in userActivity: NSUserActivity) -> Bool {
        guard let path = userActivity.userInfo?[CSSearchableItemActivityIdentifier] as? String else {
            return false
        }
        let payedId = path.replacingOccurrences(of: "com.picpay.contact.", with: "")
        SessionManager.sharedInstance.openSpotlight(contactWsid: payedId)
        return true
    }
    
    private func handleDeeplink(in userActivity: NSUserActivity) -> Bool {
        guard let url = userActivity.webpageURL else {
            return false
        }
        
        let wasAbleToHandleDeeplink = DeeplinkHelper.handleDeeplink(withUrl: url)
        if !wasAbleToHandleDeeplink {
            return thirdParties.firebaseDeeplink(in: url)
        }
        return wasAbleToHandleDeeplink
    }
}

// MARK: - Old Way of Updating the Window rootViewController
extension AppDelegate: RootViewControllerManager {
    @objc
    func changeRootViewController(_ viewController: UIViewController) {
        assert(Thread.isMainThread, "Do not call this method out of the main thread")
        
        guard let window = window else {
            fatalError("Tried to changeRootViewController to (\(String(describing: viewController.title))) but there was no UIWindow on AppDelegate")
        }
        
        guard
            window.rootViewController != nil,
            let snapshot = window.snapshotView(afterScreenUpdates: true)
            else {
                window.rootViewController = viewController
                return
        }
        
        swapRootViewController(to: viewController, on: window, using: snapshot)
    }
    
    private func swapRootViewController(to viewController: UIViewController, on window: UIWindow, using snapshot: UIView) {
        viewController.view.addSubview(snapshot)
        window.rootViewController = viewController
        
        UIView.animate(withDuration: 0.3,
                       animations: { snapshot.layer.opacity = 0 },
                       completion: { _ in snapshot.removeFromSuperview() })
    }
}

// MARK: - AppVersion Check
extension AppDelegate {
    private func verifyVersionUpdate() {
        WSAppVersion.needUpdate { needUpdate, _, error in
            if error != nil && needUpdate {
                DispatchQueue.main.async { [weak self] in
                    self?.showNeedAppUpdate()
                }
            }
        }
    }
    
    private func showNeedAppUpdate() {
        AlertMessage.showAlert(withMessage: "Atualização necessária", controller: window?.rootViewController, completion: {
            if let appUpdateUrl = URL(string: "https://picpay.com/app_update.php") {
                UIApplication.shared.open(appUpdateUrl, options: [:], completionHandler: nil)
            }
        })
    }
}
