import AnalyticsModule
import Core
import DirectMessageSB
import FeatureFlag

protocol MainCoordinating {
    // swiftlint:disable discouraged_optional_collection
    func start(with launchOptions: [AnyHashable: Any]?) -> UIWindow
    func initialRootViewController(for launchOptions: [AnyHashable: Any]?) -> UIViewController
}

final class MainCoordinator: MainCoordinating {
    typealias Dependencies = HasFeatureManager & HasAnalytics
    private let dependencies: Dependencies
    
    private let needsSyncRemoteConfigLoad: Bool
    private let rootViewControllerManager: RootViewControllerManager
    
    init(
        dependencies: Dependencies,
        rootViewControllerManager: RootViewControllerManager,
        needsSyncRemoteConfigLoad: Bool = true
    ) {
        self.dependencies = dependencies
        self.rootViewControllerManager = rootViewControllerManager
        self.needsSyncRemoteConfigLoad = needsSyncRemoteConfigLoad
    }
    
    // swiftlint:disable discouraged_optional_collection
    func start(with launchOptions: [AnyHashable: Any]?) -> UIWindow {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = initialRootViewController(for: launchOptions)
        window.makeKeyAndVisible()
        return window
    }
    
    // swiftlint:disable discouraged_optional_collection
    func initialRootViewController(for launchOptions: [AnyHashable: Any]?) -> UIViewController {
        let route = self.route(for: launchOptions)
        return initialRootViewController(for: route)
    }
    
    // swiftlint:disable discouraged_optional_collection
    private func route(for launchOptions: [AnyHashable: Any]?) -> Route {
        if needsSyncRemoteConfigLoad {
            return .syncLoadRemoteConfig
        } else if !User.isAuthenticated {
            return .needLogin
        } else if KVStore().boolFor(.isRegisterUsernameStep) {
            return .needRegisterName
        } else if let launchOptions = launchOptions,
                  let notification = getPPNotification(from: launchOptions) {
            return .showNotification(notification)
        }
        return .logged
    }
    
    private func initialRootViewController(for route: Route) -> UIViewController {
        switch route {
        case .syncLoadRemoteConfig:
            return LandingLoaderFactory.make(rootViewControllerManager: rootViewControllerManager)
            
        case .logged:
            let mainTabBar = MainTabBarController.controllerFromStoryboard()
            PaginationCoordinator.shared.setTabController(mainTab: mainTabBar)
            mainTabBar.switchToPaymentTabIfNecessary()

            dependencies.analytics.log(ApplicationEvent.appView)
            PPAuthSwift.migrateKeychainAuth()
            
            return PaginationCoordinator.shared.mainScreen()

        case .needLogin:
            guard
                dependencies.featureManager.isActive(.continueRegistration),
                let cache = RegistrationCacheHelper(dependencies: dependencies).cache
                else {
                    return UINavigationController(rootViewController: SignUpFactory.make())
            }
            
            return UINavigationController(rootViewController: ContinueRegistrationFactory.make(cache: cache))

        case .needRegisterName:
            guard dependencies.featureManager.isActive(.dynamicRegistrationSteps) else {
                return oldUsernameRegistrationController ?? newUsernameRegistrationController
            }
            
            return newUsernameRegistrationController

        case .showNotification(let notification):
            return initialRootViewControllerFor(notification: notification)
        }
    }
}

extension MainCoordinator {
    private enum Route {
        case syncLoadRemoteConfig
        case logged
        case needLogin
        case needRegisterName
        case showNotification(_ notification: PPNotification)
    }
}

// Legacy
extension MainCoordinator {
    private var newUsernameRegistrationController: UIViewController {
        UINavigationController(rootViewController: RegistrationUsernameFactory.make())
    }
    
    private var oldUsernameRegistrationController: UIViewController? {
        let mainNavigation = ViewsManager.registrationStoryboardFirtNavigationController()

        // add the username step into the navigation controller
        if let controller = ViewsManager.registrationStoryboardViewController(withIdentifier: "RegistrationUsernameStep") as? RegistrationStepViewController {
            controller.setup(RegistrationViewModel())
            if let viewsController = mainNavigation?.viewControllers as NSArray? {
                if let vcs = viewsController.adding(controller) as? [UIViewController] {
                    mainNavigation?.viewControllers = vcs
                    return mainNavigation
                }
            }
        }
        return nil
    }
    
    private func initialRootViewControllerFor(notification: PPNotification) -> UIViewController {
        notification.markAsRead()
        
        DispatchQueue.global().async {
            WSConsumer.ackNotification(withId: notification.wsId, completionHandler: { _ in })
        }
        
        let mainController = MainTabBarController.controllerFromStoryboard()
        let notificationsNavigation = mainController.notificationsNavigation()
        var viewsController = notificationsNavigation.viewControllers
        
        // add notifications view controller
        // if the landing screen is a string pass the notificaiton to the notification list
        if let landingScreen = notification.landingScreen() as? String, landingScreen != "wallet" {
            if let notificationsController = notificationsNavigation.viewControllers.first as? NotificationsViewController {
                notificationsController.notificationToOpen = notification
            }
            
            mainController.initialTab = .notifications
        }
        
        // if the landing screen is a view controller than add landingScreen controller at the navigation
        if let landingScreen = notification.landingScreen() as? UIViewController {
            viewsController.append(landingScreen)
            notificationsNavigation.viewControllers = viewsController
            mainController.initialTab = .notifications
        }
        
        PaginationCoordinator.shared.setTabController(mainTab: mainController)
        return PaginationCoordinator.shared.mainScreen()
    }
}

// MARK: - Notifications
private extension MainCoordinator {
    func getPPNotification(from launchOptions: [AnyHashable: Any]) -> PPNotification? {
        isChatNotification(launchOptions)
            ? nil
            : PPNotification(dictionaty: launchOptions, viaPush: true)
    }
    
    func isChatNotification(_ launchOptions: [AnyHashable: Any]) -> Bool {
        guard FeatureManager.isActive(.directMessageSBEnabled),
              let remoteNotification = launchOptions[
                UIApplication.LaunchOptionsKey.remoteNotification.rawValue
              ] as? [AnyHashable: Any]
        else { return false }
        
        return ChatNotification.dataMapper.isChatNotification(remoteNotification)
    }
}
