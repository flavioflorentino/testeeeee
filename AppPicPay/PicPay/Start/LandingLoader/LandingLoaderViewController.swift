import UI

final class LandingLoaderViewController: ViewController<LandingLoaderViewModelInputs, UIView> {
    private enum Layout {
        static let offsetY = -UIScreen.main.bounds.height * 0.175
    }
    private lazy var logoView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.NewGeneration.whiteLogo.image
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
    }
 
    public override func buildViewHierarchy() {
        view.addSubview(logoView)
    }
    
    public override func configureViews() {
        view.backgroundColor = Palette.ppColorBranding300.color
    }
    
    public override func setupConstraints() {
        NSLayoutConstraint.activate([
            logoView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logoView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: Layout.offsetY)
        ])
    }
}
