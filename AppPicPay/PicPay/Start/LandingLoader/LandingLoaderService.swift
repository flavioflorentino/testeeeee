import FeatureFlag
import FirebaseRemoteConfig
import Foundation

protocol LandingLoaderServicing {
    func fetchRemoteConfig(timeout: TimeInterval, completionHandler: @escaping () -> Void)
}

final class LandingLoaderService: LandingLoaderServicing {
    private let featureManager: FeatureManagerContract
    
    init(featureManager: FeatureManagerContract) {
        self.featureManager = featureManager
    }
    
    func fetchRemoteConfig(timeout: TimeInterval, completionHandler: @escaping () -> Void) {
        var isRemoteConfigLoaded = false
        
        featureManager.update {
            DispatchQueue.main.async {
                if !isRemoteConfigLoaded {
                    isRemoteConfigLoaded = true
                    completionHandler()
                }
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            if !isRemoteConfigLoaded {
                isRemoteConfigLoaded = true
                completionHandler()
            }
        }
    }
}
