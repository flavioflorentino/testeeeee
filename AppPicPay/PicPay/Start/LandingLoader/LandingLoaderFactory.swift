import FeatureFlag
import Foundation

final class LandingLoaderFactory {
    static func make(rootViewControllerManager: RootViewControllerManager) -> LandingLoaderViewController {
        let coordinator: LandingLoaderCoordinating = LandingLoaderCoordinator(rootViewControllerManager: rootViewControllerManager)
        let service: LandingLoaderServicing = LandingLoaderService(featureManager: FeatureManager.shared)
        let presenter: LandingLoaderPresenting = LandingLoaderPresenter(coordinator: coordinator)
        let viewModel = LandingLoaderViewModel(service: service, presenter: presenter)
        let viewController = LandingLoaderViewController(viewModel: viewModel)

        coordinator.viewController = viewController

        return viewController
    }
}
