import Core
import FirebaseRemoteConfig

protocol LandingLoaderViewModelInputs: AnyObject {
    func viewDidLoad()
}

final class LandingLoaderViewModel {
    private let service: LandingLoaderServicing
    private let presenter: LandingLoaderPresenting

    init(service: LandingLoaderServicing, presenter: LandingLoaderPresenting) {
        self.service = service
        self.presenter = presenter
    }
    
    private func fetchRemoteConfigAndSetRootViewController() {
        service.fetchRemoteConfig(timeout: 6) { [weak self] in
            guard let self = self else {
                return
            }
            self.presenter.didNextStep(action: .next)
        }
    }
}

extension LandingLoaderViewModel: LandingLoaderViewModelInputs {
    func viewDidLoad() {
        fetchRemoteConfigAndSetRootViewController()
    }
}
