enum LandingLoaderAction {
    case next
}

protocol LandingLoaderCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LandingLoaderAction)
}

final class LandingLoaderCoordinator: LandingLoaderCoordinating {
    weak var viewController: UIViewController?
    private let rootViewControllerManager: RootViewControllerManager
    
    init(rootViewControllerManager: RootViewControllerManager) {
        self.rootViewControllerManager = rootViewControllerManager
    }
    
    func perform(action: LandingLoaderAction) {
        let mainCoordinator = MainCoordinator(
            dependencies: DependencyContainer(),
            rootViewControllerManager: rootViewControllerManager,
            needsSyncRemoteConfigLoad: false
        )
        let nextViewController = mainCoordinator.initialRootViewController(for: nil)
        setRootViewController(nextViewController)
    }
    
    private func setRootViewController(_ viewController: UIViewController) {
        rootViewControllerManager.changeRootViewController(viewController)
    }
}
