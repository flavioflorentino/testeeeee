protocol LandingLoaderPresenting: AnyObject {
    func didNextStep(action: LandingLoaderAction)
}

final class LandingLoaderPresenter: LandingLoaderPresenting {
    private let coordinator: LandingLoaderCoordinating

    init(coordinator: LandingLoaderCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: LandingLoaderAction) {
        coordinator.perform(action: action)
    }
}
