import AnalyticsModule
import AppsFlyerLib
import Core
import CoreSentinel
import CustomerSupport
import DirectMessageSB
import FeatureFlag
import FirebaseCore
import FirebaseAnalytics
import FirebaseRemoteConfig
import FirebaseDynamicLinks
import EventTracker
import Mixpanel
import SecurityModule

struct ThirdParties {
    func setup() {
        setupAppTrackingPermission()
        // This is order dependent and Firebase needs to load first
        // or the FeatureFlagControl does not work
        setupFirebase()
        setupFeatureFlagControl()

        setupEventTracker()
        setupMixPanel()
        setupNewRelic()
        setupGoogleAdWords()
        setupAppsFlyer()
        identify()
    }
    
    func identify(isNewUser: Bool = false) {
        guard let consumer = ConsumerManager.shared.consumer else {
            return
        }

        let consumerWsId = "\(consumer.wsId)"

        EventTrackerManager.tracker.identify(with: .consumerId(consumerWsId))
        Mixpanel.mainInstance().identify(distinctId: consumerWsId)
        
        if User.token != nil {
            AppsFlyerLib.shared().customerUserID = consumerWsId
        }
        
        NewRelic.setUserId(consumerWsId)
        Analytics.setUserProperty(consumerWsId, forName: "consumer_id")
        Analytics.setUserProperty(consumer.email, forName: "consumer_email")
        if let analyticsProperties = consumer.analyticsProperties {
            analyticsProperties.forEach({ (key: String, value: String) in
                Analytics.setUserProperty(value, forName: key)
            })
        }

        connectChat()
    }

    private func connectChat() {
        guard FeatureManager.shared.isActive(.directMessageSBEnabled) else { return }
        ChatApiManager.shared.setup { _ in }
    }
    
    func updateNotificationToken(_ token: Data) {
        #if !(DEBUG)
        Mixpanel.mainInstance().people.addPushDeviceToken(token)
        #endif
        
        if FeatureManager.shared.isActive(.directMessageSBEnabled) {
            ChatApiManager.shared.addPushDeviceToken(token)
        }
    }
    
    func didBecomeActive() {
        AppsFlyerLib.shared().start()
    }

    func setupAppAccessController(window: UIWindow?) -> AppAccessController {
        AppAccessFactory.make(window: window, logoutAction: logout)
    }

    private func logout() {
        AuthManager.shared.logout(fromContext: nil)
        
        if FeatureManager.shared.isActive(.directMessageSBEnabled) {
            ChatApiManager.shared.disconnect(completion: nil)
        }
    }
    
    private func setupNewRelic() {
        NewRelic.disableFeatures(.NRFeatureFlag_WebViewInstrumentation)
        NewRelic.start(withApplicationToken: Credentials.NewRelic.applicationToken)

        let wrapper = NewRelicWrapper(instance: NewRelic.self)
        SentinelSetup.inject(wrapper)
    }
    
    private func setupMixPanel(forTesting: Bool = false) {
        let mixpanel = Mixpanel.initialize(token: Credentials.Mixpanel.token)

        guard !forTesting else {
            return
        }
        
        let wrapper = MixPanelWrapper(instance: mixpanel)
        Analytics.shared.register(tracker: wrapper, for: .mixPanel)
    }
    
    private func setupFirebase(forTesting: Bool = false) {
        let googleServicePlist: String
        #if DEVELOPMENT || DEBUG
        googleServicePlist = "GoogleService-Info-Dev"
        #else
        googleServicePlist = "GoogleService-Info"
        #endif
        
        let filePath = Bundle.main.path(forResource: googleServicePlist, ofType: "plist")
        if let path = filePath, let opts = FirebaseOptions(contentsOfFile: path) {
            FirebaseApp.configure(options: opts)
        }
        
        let config = RemoteConfig.remoteConfig()
        config.configSettings = RemoteConfigSettings()
        config.setDefaults(fromPlist: "FeatureToggles")
        RemoteConfigSetup.inject(instance: RemoteConfigWrapper(instance: config))
        
        guard !forTesting, KVStore().boolFor(.isRemoteConfigLoaded) else { return }
        Analytics.shared.register(tracker: FirebaseWrapper(), for: .firebase, with: FirebaseSanitizer())
        FeatureManager.shared.update()
    }

    private func setupGoogleAdWords() {
        ACTConversionReporter.report(withConversionID: "860958372", label: "-9HoCM_LpW4QpN3EmgM", value: "0.00", isRepeatable: false)
    }
    
    private func setupAppsFlyer() {
        let appsFlyerTracker = AppsFlyerLib.shared()
        
        appsFlyerTracker.appsFlyerDevKey = Credentials.AppsFlyer.devKey
        appsFlyerTracker.appleAppID = "561524792"
        
        if User.token != nil, let consumerId = ConsumerManager.shared.consumer?.wsId {
            appsFlyerTracker.customerUserID = "\(consumerId)"
        }
        
        #if DEBUG
        appsFlyerTracker.isDebug = true
        #endif
        
        let wrapper = AppsFlyerWrapper(instance: appsFlyerTracker)
        Analytics.shared.register(tracker: wrapper, for: .appsFlyer)

        AnalyticsModule.Analytics.shared.log(UserEvent.open)
    }

    private func setupEventTracker() {
        let apiToken = FeatureManager.shared.isActive(.isEventTrackerProxyEnabled)
            ? Credentials.EventTracker.proxyApiToken
            : Credentials.EventTracker.apiToken

        EventTrackerManager.initialize(apiToken: .consumer(apiToken))
        var tracker = EventTrackerManager.tracker
        tracker.contextPropertyProviders.append(AppsFlyerContextPropertyProvider())
        tracker.contextPropertyProviders.append(GeoLocationContextPropertyProvider())

        Analytics.shared.register(tracker: EventTrackerWrapper(), for: .eventTracker)
    }
    
    private func setupFeatureFlagControl() {
        if FeatureFlagControlHelper.isEnabled {
            FeatureFlagControlHelper.start()
            RemoteConfigSetup.inject(instance: FeatureFlagControl.shared.remoteConfigMock)
        }
    }
    
    private func setupAppTrackingPermission() {
        AppTracking.requestAuthorization { status in
            guard case AppTrackingAuthorizationStatus.authorized = status else {
                AppsFlyerLib.shared().disableAdvertisingIdentifier = true
                return
            }
        }
    }
}

// MARK: - Events Context Property Providers
extension ThirdParties {
    private struct GeoLocationContextPropertyProvider: ContextPropertyProviding {
        var properties: [String: AnyCodable] {
            guard let location = LocationManager.shared.lastAvailableLocation else {
                return [:]
            }

            return [
                "latitude": .init(value: location.coordinate.latitude),
                "longitude": .init(value: location.coordinate.longitude)
            ]
        }
    }

    private struct AppsFlyerContextPropertyProvider: ContextPropertyProviding {
        var properties: [String: AnyCodable] {
            [
                "appsflyer_id": .init(value: AppsFlyerLib.shared().getAppsFlyerUID()),
                "advertising_id": .init(value: AppsFlyerLib.shared().advertisingIdentifier)
            ]
        }
    }
}

// Mark - Deeplink
extension ThirdParties {
    func firebaseDeeplink(in url: URL) -> Bool {
        let tryFirebaseDeeplink = DynamicLinks.dynamicLinks().handleUniversalLink(url) { dynamicLink, error in
            guard let url = dynamicLink?.url, error == nil else {
                return
            }
            DynamicLinkHelper.handleFirebaseDynamicLinkDeepLink(url: url)
        }
        return tryFirebaseDeeplink
    }
}

#if DEBUG
// Mark - Unit Testing
extension ThirdParties {
    func setupForTesting() {
        setupFirebase(forTesting: true)
        setupEventTracker()
        setupMixPanel(forTesting: true)
    }
}

public extension RemoteConfigSetup {
    static func restoreDefaults() {
        let config = RemoteConfig.remoteConfig()
        config.configSettings = RemoteConfigSettings()
        config.setDefaults(fromPlist: "FeatureToggles")
        RemoteConfigSetup.inject(instance: RemoteConfigWrapper(instance: config))
    }
}

#endif

// Mark - Feature Toggle
public enum FeatureFlagControlHelper {
    static var isEnabled: Bool {        
        #if DEBUG
        return !isRunningTests
        #else
        return false
        #endif
    }
    
    static func start() {
        #if DEBUG
        let dict = FeatureFlagControlHelper.dictFromFeatureTogglesPlist()
        guard !dict.isEmpty else { return }
                
        FeatureFlagControl.shared.buildSwitches(from: dict)
        FeatureFlagControl.shared.activateCache()
        #endif
    }
    
    static func dictFromFeatureTogglesPlist() -> [String: Any] {
        guard let path = Bundle.main.path(forResource: "FeatureToggles", ofType: "plist") else {
            return [:]
        }
        return NSDictionary(contentsOfFile: path) as? [String: Any] ?? [:]
    }
}
