import Core
import FeatureFlag
import SecurityModule
import UI
import CoreLegacy

struct BasicSetup {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    
    func appearance() {
        UIToolbar.appearance().tintColor = Palette.ppColorBranding300.color
        UIToolbar.appearance().barTintColor = Palette.ppColorGrayscale000.color
        
        UIPageControl.appearance().pageIndicatorTintColor = PicPayStyle.pageIndicator
        UIPageControl.appearance().currentPageIndicatorTintColor = Palette.ppColorGrayscale000.color
        UIPageControl.appearance().backgroundColor = Palette.ppColorBranding300.color

        UINavigationBar.appearance().barTintColor = Palette.ppColorGrayscale100.color
        UINavigationBar.appearance().tintColor = Palette.ppColorBranding300.color
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: Palette.ppColorGrayscale500.color]
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
    }
    
    func start() {
        KVStore().removeObjectFor(.cashbackPopup)
        ConsumerManager.shared.checkFirstInstallAndClearKeychainIfNeeded()
        KVStore().defineBrAsADefaultLocale()
        SessionManager.sharedInstance.loadInitialData()
    }
    
    func didBecomeActive() {
        DispatchQueue.global(qos: .background).async {
            CreditCardManager.shared.loadCardList { _ in }
            Contacts().identifyPicpayContacts()
        }
    }
    
    func updateNotificationToken(_ token: Data) {
        AppParameters.global().pushToken = token.hexEncodedString()
        updateNotificationTokenInBackground()
        updateKVKeyAskedForNotification()
    }

    private func updateNotificationTokenInBackground() {
        DispatchQueue.global(qos: .background).async {
            ConsumerApi().updateNotificationToken()
        }
    }
}

extension BasicSetup {
    func updateKVKeyAskedForNotification() {
        if KVStore().boolFor(.askedForRegistrationRemoteNotification) == false {
            PPAnalytics.trackFirstTimeOnlyEvent("Permissao autorizada - Notificacao", properties: nil)
        }
        
        KVStore().setBool(true, with: .askedForRegistrationRemoteNotification)
    }
}

extension BasicSetup {
    func configurePinning() {
        // This treatment permits we turn on/off the pinning when in debug mode if necessary
        #if DEBUG 
        let shouldPin: PinResponse = .no
        let rawDomains = "picpay.com,gateway.service.ppay.me"
        #else
        let shouldPin: PinResponse = FeatureManager.isActive(.featureCertificatePinning) ? .yes : .no
        let rawDomains = FeatureManager.text(.featureCertificatePinningDomains).chopPrefix().chopSuffix().removingWhiteSpaces()
        #endif
        
        dependencies.keychain.set(key: KeychainKey.shouldPin, value: String(shouldPin.rawValue))
        let domains = rawDomains.replacingOccurrences(of: "\"", with: "")
        dependencies.keychain.set(key: KeychainKey.pinDomains, value: domains)
    }
    
    func sendDetectionLog() {
        if User.isAuthenticated {
            LogDetectionService().sendLog()
        }
    }
}

private extension Data {
    func hexEncodedString() -> String {
        self.map { String(format: "%02x", $0) }.joined()
    }
}
