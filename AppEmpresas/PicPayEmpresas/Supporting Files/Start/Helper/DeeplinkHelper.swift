import Foundation
import UIKit
import FeatureFlag

final class DeeplinkHelper {
    typealias Dependencies = HasFeatureManager
    let dependencies: Dependencies
    let separator = "picpaybiz://picpaybiz/"

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    @discardableResult
    func open(url: URL, from navigationController: UINavigationController) -> Bool {
        guard dependencies.featureManager.isActive(.allowDeepLinkRouter),
              let path = url.absoluteString.components(separatedBy: separator).last else {
            return false
        }

        let urlFirstPart = path.split(separator: "/").first
        switch urlFirstPart {
        case "pix":
            let deepLink = RouterApp.pix
            deepLink.match(navigation: navigationController, url: url)
            return true

        case "helpcenter":
            let deepLink = RouterApp.helpcenter
            deepLink.match(navigation: navigationController, url: url)
            return true

        default:
            let router = RouterApp(rawValue: path)
            router?.match(navigation: navigationController)
            return true
        }
    }
}
