import Foundation
import AnalyticsModule

enum AppDelegateEvents: AnalyticsKeyProtocol {
    case applicationOpened
    case applicationClosed
    case applicationPaused
    case applicationResumed

    private var name: String {
        switch self {
        case .applicationOpened:
            return "Application Opened"
        case .applicationClosed:
            return "Application Closed"
        case .applicationPaused:
            return "Application Paused"
        case .applicationResumed:
            return "Application Resumed"
        }
    }

    private var properties: [String: Any] { [:] }
    private var providers: [AnalyticsProvider] { [.eventTracker] }

    func event() -> AnalyticsEventProtocol { AnalyticsEvent(name, properties: properties, providers: providers) }
}
