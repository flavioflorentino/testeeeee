import Foundation
import Core
import LegacyPJ
import PIX

public final class CostumerSupportWrapper: CostumeSupportContract {
    typealias Dependencies = HasAuthManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    public func openFAQ(in controller: UIViewController) {
        BIZCustomerSupportManager(accessToken: AuthManager.shared.userAuth?.auth.accessToken).presentFAQ(from: controller)
    }
}
