import Core
import Foundation
import LegacyPJ
import PIX

public final class UserAuthManagerWrapper: UserAuthManagerContract {
    typealias Dependencies = HasAuthManager & HasApiSeller
    private let dependencies: Dependencies

    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    public func getUserInfo(completion: @escaping (KeyManagerBizUserInfo?) -> Void) {
        getUser { [weak self] user in
            guard let self = self, let user = user else {
                completion(nil)
                return
            }
            self.getSeller { seller in
                guard let seller = seller, let cnpj = seller.cnpj else {
                    completion(nil)
                    return
                }
                let userInfo = KeyManagerBizUserInfo(name: seller.razaoSocial ?? "",
                                                     cnpj: cnpj,
                                                     mail: seller.email ?? "",
                                                     phone: user.phone ?? "",
                                                     userId: String(seller.id))
                completion(userInfo)
            }
        }
    }
    
    private func getUser(completion: @escaping (User?) -> Void) {
        ApiUser().data { user, _ in
            completion(user)
        }
    }
    
    private func getSeller(completion: @escaping (Seller?) -> Void) {
        dependencies.apiSeller.data { result in
            switch result {
            case .success(let seller):
                completion(seller)
            case .failure:
                completion(nil)
            }
        }
    }
}
