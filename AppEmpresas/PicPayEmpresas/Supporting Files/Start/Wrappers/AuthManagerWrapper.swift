import Foundation
import Core
import LegacyPJ
import PIX

public final class AuthManagerWrapper: BizAuthManagerContract {
    typealias Dependencies = HasAuthManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    public func performActionWithAuthorization(_ message: String?,
                                               _ action: @escaping ((_ result: PerformActionResult) -> Void)) {
        dependencies.authManager.performActionWithAuthorization(message) { result in
            switch result {
            case .canceled:
                action(.canceled)
            case let .success(successString):
                action(.success(successString))
            case let .failure(error):
                action(.failure(errorMessage: error.localizedDescription))
            }
        }
    }
    
    public func getAuthorizationToken() -> String {
        dependencies.authManager.userAuth?.auth.accessToken ?? ""
    }
}
