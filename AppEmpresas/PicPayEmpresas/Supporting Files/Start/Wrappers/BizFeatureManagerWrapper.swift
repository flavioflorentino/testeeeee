import Foundation
import Core
import PIX
import FeatureFlag

public final class BizFeatureManagerWrapper: BizFeatureManagerContract {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    // swiftlint:disable discouraged_optional_boolean
    public func isActive(key: BizFeatureConfig) -> Bool? {
        guard let flag = FeatureConfig(rawValue: key.rawValue) else {
            return nil
        }
        return dependencies.featureManager.isActive(flag)
    }
}
