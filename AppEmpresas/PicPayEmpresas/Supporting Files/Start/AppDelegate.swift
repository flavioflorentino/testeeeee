import Core
import SVProgressHUD
import UIKit
import PIX
import FeatureFlag
import AnalyticsModule

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {
    typealias Dependecies = HasAppCoordinator & HasFeatureManager & HasAnalytics & HasKVStore

    var window: UIWindow?
    lazy var moduleInjection = ModuleInjection()
    lazy var thirdParties = ThirdParties()
    private var dependencies: Dependecies = DependencyContainer()
    
    func application(_ application: UIApplication,
                     // swiftlint:disable:next discouraged_optional_collection
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupDevice()
        
        ModuleInjection().setup()
        
        // the migration will occur only once
        KeychainMigration().migrate()
        
        UNUserNotificationCenter.current().delegate = NotificationManager.shared
        thirdParties.setup()
        setupDefaultScene()

        dependencies.analytics.log(AppDelegateEvents.applicationOpened)

        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        TrackingManager.trackAppLaunch()
        dependencies.analytics.log(AppDelegateEvents.applicationResumed)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        dependencies.analytics.log(AppDelegateEvents.applicationClosed)
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        dependencies.analytics.log(AppDelegateEvents.applicationPaused)
    }
}

// MARK: - Deeplink

extension AppDelegate {
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        guard let root = window?.rootViewController as? UINavigationController else {
                return false
        }

        let container = DependencyContainer()
        let deeplinkHelper = DeeplinkHelper(dependencies: container)
        return deeplinkHelper.open(url: url, from: root)
    }
}
    
// MARK: PUSH

extension AppDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        let bundle = Bundle.main.bundleIdentifier
        
        if AppMode.current == .debug {
            print("NOTIFICATION_TOKEN:", token)
            print("BUNDLE_ID:", bundle ?? "")
        }
        
        TrackingManager.setupDeviceToken(deviceToken: deviceToken)
        NotificationManager.shared.updateToken(token)
    }
}

// MARK: - Internal Methods

extension AppDelegate {
    func changeRootViewController(_ viewController: UIViewController, completion: (() -> Void)? = nil) {
        guard let window = self.window else {
            return
        }
        
        guard window.rootViewController != nil else {
            window.rootViewController = viewController
            return
        }
        
        guard let snapShot = window.snapshotView(afterScreenUpdates: true) else {
            return
        }
        
        viewController.view.addSubview(snapShot)
        
        let currentViewController = window.rootViewController
        window.rootViewController = viewController
        
        UIView.animate(
            withDuration: 0.3,
            animations: { () -> Void in
                snapShot.layer.opacity = 0
            },
            completion: {(_ finished: Bool) -> Void in
                snapShot.removeFromSuperview()
                currentViewController?.view.removeFromSuperview()
                currentViewController?.dismiss(animated: false, completion: completion)
            }
        )
    }
}
    
// MARK: Private Methods

private extension AppDelegate {
    func setupDevice() {
        if dependencies.featureManager.isActive(.isAppLocalDataUnavailable),
           dependencies.kvStore.stringFor(BizKvKey.deviceToken)?.isEmpty == true {
            let deviceToken = String.random(ofLength: 20)
            dependencies.kvStore.setString(deviceToken, with: BizKvKey.deviceToken)
        }
    }

    func setupDefaultScene() {
        window = UIWindow(frame: UIScreen.main.bounds)
        if dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
            dependencies.appCoordinator.displayRoot()
        } else {
            window?.rootViewController = AppManager.shared.initialRootViewController()
        }
        window?.makeKeyAndVisible()
        applyAppStyle()
    }
    
    func applyAppStyle() {
        let appearance = UINavigationBar.appearance()
        let navigationBarAppearance: NavigationBarAppearance = dependencies.featureManager.isActive(.isNavigationBarAppearanceDefaultAvailable)
            ? NavigationBarAppearanceDefault()
            : NavigationBarAppearanceLegacy()
        appearance.setup(appearance: navigationBarAppearance)
        
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setDefaultStyle(.light)
    }
}
