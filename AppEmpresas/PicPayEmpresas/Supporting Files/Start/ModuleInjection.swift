import Core
import PIX

struct ModuleInjection {
    func setup() {
        injectAuthManager()
    }
    
    private func injectAuthManager() {
        PIXSetup.inject(instance: UserAuthManagerWrapper())
        PIXSetup.inject(instance: AuthManagerWrapper())
        PIXSetup.inject(instance: CostumerSupportWrapper())
        PIXSetup.inject(instance: BizFeatureManagerWrapper())
    }
}
