import Foundation

enum AppMode {
    case debug
    case release
    
    static var current: AppMode {
        #if DEBUG
            return .debug
        #else
            return .release
        #endif
    }
}
