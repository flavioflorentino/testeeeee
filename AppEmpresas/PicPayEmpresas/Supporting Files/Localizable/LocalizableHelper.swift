import Foundation

enum LocalizableFile: String {
    case form
    case formDict
    case registrationPersonalInfo
    case registrationPhoneLocalizable
}

protocol Localizable {
    var key: String { get }
    var text: String { get }
    var file: LocalizableFile { get }
}

extension Localizable {
    var text: String {
        LocalizableHelper.text(for: self)
    }
    
    /*
        This should be used when there is a word in the plural as appropriate
        e.g.:
        John has an apple
        John has 2 apples
    */
    /// - Parameters:
    ///   - count: the amount
    func plural(_ count: Int) -> String {
        String.localizedStringWithFormat(self.text, count)
    }
}

enum LocalizableHelper {
    static func text(for localizable: Localizable) -> String {
        let file = localizable.file.rawValue
        return Bundle.main.localizedString(forKey: localizable.key, value: nil, table: file)
    }
}
