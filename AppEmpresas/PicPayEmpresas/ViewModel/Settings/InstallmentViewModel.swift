import AnalyticsModule
import Foundation
import LegacyPJ

protocol InstallmentsType {
    // MARK: - Properties
    
    var rowSaved: Int? { get }
    var numberOfRows: Int { get }
    var header: String { get }
    
    // MARK: - Methods
    
    func dataForRow(_ row: Int) -> InstallmentOption?
    func loadInstallmentsList(_ completion:@escaping( (_ error: LegacyPJError?) -> Void))
    func saveInstallment(rowSelected: Int, password: String, _ completion:@escaping( (_ error: LegacyPJError?) -> Void))
}

final class InstallmentViewModel: InstallmentsType {
    private var options: [InstallmentOption] = []
    private let dependencies: HasAnalytics = DependencyContainer()
    
    private let api: ApiInstallments
    
    private var _rowSaved: Int?
    
    var rowSaved: Int? {
        get {
            if self.numberOfRows == 0 {
                return nil
            }
            
            // Armazena qual a opção tem a flag selected, caso ainda não tenha feito antes
            guard let rowSaved = self._rowSaved else {
                for (index, option) in self.options.enumerated() where option.selected {
                    self._rowSaved = index
                    return self._rowSaved
                }
                return nil
            }
            
            return rowSaved
        }
        
        set {
            self._rowSaved = newValue
        }
    }
    
    var header: String = ""
    
    var numberOfRows: Int {
        options.count
    }
    
    init(apiInstallments: ApiInstallments = ApiInstallments()) {
        self.api = apiInstallments
        self._rowSaved = nil
    }
    
    func dataForRow(_ row: Int) -> InstallmentOption? {
        options[row]
    }
    
    func loadInstallmentsList(_ completion: @escaping((_ error: LegacyPJError?) -> Void)) {
        api.data { [weak self] result in
            switch result {
            case .success(let response):
                self?.header = response.header ?? ""
                
                for item in response.fees {
                    self?.addOption(InstallmentOption(installment: item))
                }
                completion(nil)
                
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    func saveInstallment(rowSelected: Int, password: String, _ completion:@escaping( (_ error: LegacyPJError?) -> Void)) {
        guard let newValue = self.valueForRow(rowSelected) else {
            completion(nil)
            return
        }
        
        self.api.saveMaxInstallments(value: newValue, pin: password) { error in
            if error == nil {
                self.dependencies.analytics.log(SettingsAnalytics.updatedSplit(condition: newValue))
                completion(nil)
            } else {
                DispatchQueue.main.async {
                    completion(error)
                }
            }
        }
    }
}

private extension InstallmentViewModel {
    func addOption(_ option: InstallmentOption) {
        options.append(option)
        _rowSaved = nil
    }
    
    func valueForRow(_ row: Int) -> Int? {
        options[row].value
    }
}

struct InstallmentOption {
    private var option: InstallmentItem
    
    private var hasSubtitle: Bool {
        option.numberOfInstallments != 1
    }
    
    var title: String {
        hasSubtitle ? Strings.Default.installmentsTitle(option.numberOfInstallments.description) : Strings.Default.disabled
    }
    
    var subtitle: String? {
        hasSubtitle ? Strings.Default.installmentsSubtitle(option.adictionalFee) : nil
    }
    
    var value: Int {
        option.numberOfInstallments
    }
    
    var selected: Bool {
        option.selected
    }
    
    init(installment: InstallmentItem) {
        self.option = installment
    }
}
