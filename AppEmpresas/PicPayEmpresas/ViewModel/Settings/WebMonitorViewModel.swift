import Foundation
import LegacyPJ

protocol WebMonitorViewModel {
    // MARK: Properties
    var expire_data: String { get set }
    var token: String { get set }
    
    // MARK: Methods
    func authorize(token: String, _ completion:@escaping ((_ error: LegacyPJError?) -> Void))
}

final class WebMonitorImpViewModel: WebMonitorViewModel {
    // MARK: Properties
    var expire_data: String = ""
    var token: String = ""
    
    var apiSeller: ApiSeller
    
    init(apiSeller: ApiSeller = ApiSeller()) {
        self.apiSeller = apiSeller
    }
    
    // MARK: Methods
    
    func authorize(token: String, _ completion: @escaping( (_ error: LegacyPJError?) -> Void) ) {
        self.apiSeller.monitorAuthorize(token: token) { error in
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
}
