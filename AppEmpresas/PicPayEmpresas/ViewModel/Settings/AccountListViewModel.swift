import Foundation
import LegacyPJ

protocol AccountListViewModel {
    // MARK: - Methods
    
    func numberOfRows() -> Int
    func userAuthForRow(at index: IndexPath) -> UserAuth
    func authenticatedUser() -> UserAuth?
}

final class AccountDataListViewModel: AccountListViewModel {
    // MARK: - Methods
    
    func numberOfRows() -> Int {
        AuthManager.shared.authenticatedUsers.count
    }
    
    func userAuthForRow(at index: IndexPath) -> UserAuth {
        let list: [UserAuth] = AuthManager.shared.authenticatedUsers.reversed()
        return list[index.row]
    }
    
    func authenticatedUser() -> UserAuth? {
        AuthManager.shared.userAuth
    }
}
