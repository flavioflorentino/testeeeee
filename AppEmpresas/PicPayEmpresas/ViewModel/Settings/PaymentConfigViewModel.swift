import Foundation
import LegacyPJ

protocol PaymentConfigViewModel {
    // MARK: Properties
    var seller: Seller { get set }
    
    var restrictToScanner: Bool { get }
    
    // MARK: Methods
    func savePaymentConfig(password: String, isOn: Bool, _ completion:@escaping ((_ error: LegacyPJError?) -> Void))
}

final class PaymentConfigImpViewModel: PaymentConfigViewModel {
    var seller: Seller
    
    var restrictToScanner: Bool {
        seller.store?.restrictToScanner ?? false
    }
    
    // MARK: - Dependence
    var apiSeller: ApiSeller
    
    init(seller: Seller = Seller(), apiSeller: ApiSeller = ApiSeller()) {
        self.apiSeller = apiSeller
        self.seller = seller
    }
    
    // MARK: - Public Methods
    func savePaymentConfig(password: String, isOn: Bool, _ completion:@escaping ((_ error: LegacyPJError?) -> Void)) {
        apiSeller.updateRestrictToScanner(pin: password, restrictToScanner: isOn) { error in
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
}
