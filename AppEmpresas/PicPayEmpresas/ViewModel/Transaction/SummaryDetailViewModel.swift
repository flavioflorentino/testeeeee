import Foundation
import LegacyPJ

protocol SummaryDetailViewModel: TransactionListViewModel {}

final class SummaryDetailDateViewModel: TransactionListAllViewModel {
    var summaryItem: SummaryItem
    
    init(summaryItem: SummaryItem, apiTransaction: ApiTransaction = ApiTransaction()) {
        self.summaryItem = summaryItem
        super.init(container: DependencyContainer(), apiTransaction: apiTransaction)
        
        self.transactionTitle = summaryItem.date
    }
    
    // MARK: - Public Methods
    
    override func loadTransactions(_ completion: @escaping((LegacyPJError?) -> Void)) {
        tableList.isLoading = true
        
        apiTransaction.listByDate(summaryItem.dateId, page: nil ) { [weak self] result in
            switch result {
            case .success(let response):
                self?.tableList.populate(section: 0, list: response.list)
                self?.tableList.pagination = response.pagination
                completion(nil)
            case .failure(let error):
                completion(error)
            }
            self?.tableList.isLoading = false
        }
    }
    
    override func loadMoreTransactions(_ completion: @escaping((LegacyPJError?) -> Void)) {
        guard !tableList.isLoading else { return }
        
        tableList.isLoading = true
        apiTransaction.listByDate(summaryItem.dateId, page: tableList.pagination.nextPage) { [weak self] result in
            switch result {
            case .success(let response):
                self?.tableList.append(section: 0, list: response.list)
                self?.tableList.pagination = response.pagination
                completion(nil)
            case .failure(let error):
                completion(error)
            }
            self?.tableList.isLoading = false
        }
    }
}
