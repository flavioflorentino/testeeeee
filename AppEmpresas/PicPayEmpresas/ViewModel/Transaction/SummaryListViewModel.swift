import Foundation
import LegacyPJ

protocol SummaryListViewModel {
    // MARK: - Methods
    func loadSummaryItems(_ completion:@escaping(LegacyPJError?) -> Void)
    func loadMoreSummaryItems(_ completion:@escaping(LegacyPJError?) -> Void)
    func summaryItemForRow(_ index: IndexPath) -> SummaryItem?
    func numberOfRowsInSection(section: Int) -> Int
    func shouldLoadMoreItems() -> Bool
}

class SummaryListBaseViewModel: SummaryListViewModel {
    // MARK: - Private Properties
    
    var list: [SummaryItem]
    var pagination: Pagination
    var isLoading: Bool = false
    
    // MARK: Dependencies
    
    let apiTransaction: ApiTransaction
    
    init(apiTransaction: ApiTransaction = ApiTransaction()) {
        self.apiTransaction = apiTransaction
        self.list = []
        self.pagination = Pagination()
    }
    
    // MARK: Public Methods
    
    func loadItems(page: Int?, _ completion: @escaping(ApiTransaction.SumaryListResult) -> Void) { }
    
    func loadSummaryItems(_ completion: @escaping(LegacyPJError?) -> Void) {
        isLoading = true
        loadItems(page: nil) { [weak self] result in
            self?.isLoading = false
            switch result {
            case .success(let response):
                self?.list = response.list
                self?.pagination = response.pagination
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    func loadMoreSummaryItems(_ completion: @escaping(LegacyPJError?) -> Void) {
        guard !isLoading else { return }
        
        isLoading = true
        loadItems(page: nil) { [weak self] result in
            self?.isLoading = false
            switch result {
            case .success(let response):
                self?.list.append(contentsOf: response.list)
                self?.pagination = response.pagination
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        list.count
    }
    
    func shouldLoadMoreItems() -> Bool {
        !isLoading && pagination.hasNext
    }
    
    func summaryItemForRow(_ index: IndexPath) -> SummaryItem? {
        if index.row < list.count {
            return list[index.row]
        }
        return nil
    }
}

final class SummaryListByDayViewModel: SummaryListBaseViewModel {
    override func loadItems(page: Int?, _ completion: @escaping(ApiTransaction.SumaryListResult) -> Void) {
        apiTransaction.summaryListByDay(page, completion)
    }
}

final class SummaryListByMonthViewModel: SummaryListBaseViewModel {
    override func loadItems(page: Int?, _ completion: @escaping(ApiTransaction.SumaryListResult) -> Void) {
        apiTransaction.summaryListByMonth(page, completion)
    }
}
