import AnalyticsModule
import AssetsKit
import Core
import Foundation
import LegacyPJ
import UIKit
import FeatureFlag

protocol TransactionListViewModel {
    var transactionTitle: String? { get }
    var seller: Seller? { get }
    var userStatus: UserStatus? { get }
    var user: User? { get }
    var bankAccountError: BankAccountValidation? { get }
    var companyInfo: CompanyInfo? { get }
    
    // MARK: Methods
    /// Load the first page of transactions
    func loadTransactions(_ completion: @escaping((_ error: LegacyPJError?) -> Void))
    /// Load more transaction
    func loadMoreTransactions(_ completion: @escaping((_ error: LegacyPJError?) -> Void))
    /// Get a transaction for a row
    func transationForRow(_ index: IndexPath) -> Transaction?
    /// Return the number of rows for a section
    func numberOfRowsInSection(section: Int) -> Int
    /// Check if has more transactions for load
    func shouldLoadMoreItems() -> Bool
    
    /// Cancel a transaction
    func cancelTransaction(password: String, transaction: Transaction, indexPath: IndexPath, _ completion: @escaping ( (_ error: LegacyPJError?) -> Void ))
    
    /// check if is a loading of data or action performing for a row
    func isLoading(indexPath: IndexPath) -> Bool
    func startLoading(indexPath: IndexPath)
    func stopLoading(indexPath: IndexPath)
    
    func loadSellerAndUserStatus(_ completion: @escaping() -> Void)

    func presentAdvertising(_ completion: @escaping (AlertModalInfo) -> Void)
    func mustPresentWelcome() -> Bool
    func mustPresentHub() -> Bool
    func trackPixRegister()
    func trackPixNotNow()
    func isPixTransactionsButtonEnabled() -> Bool
}

class TransactionListAllViewModel: TransactionListViewModel {
    private typealias Localizable = Strings.Transaction

    // MARK: Properties
    
    var transactionTitle: String?
    private(set) var seller: Seller?
    private(set) var userStatus: UserStatus?
    private(set) var user: User?
    private(set) var bankAccountError: BankAccountValidation?
    private(set) var companyInfo: CompanyInfo? {
        didSet {
            let value = companyInfo?.individual ?? false
            container.kvStore.set(value: value, with: BizKvKey.individualCompany)
        }
    }
    
    var tableList: TableList<Transaction>
    private let service: UserStatusServicing?
    private let container: HasFeatureManager & HasAnalytics & HasKVStore
    private let apiSignUp = ApiSignUp()
    private let homeService = HomeService(dependencies: DependencyContainer())
    
    // MARK: Dependencies
    
    var apiTransaction: ApiTransaction
    
    init(
        container: DependencyContainer,
        apiTransaction: ApiTransaction = ApiTransaction(),
        service: UserStatusServicing? = nil
    ) {
        self.apiTransaction = apiTransaction
        self.tableList = TableList<Transaction>()
        self.service = service
        self.container = container
        self.user = AuthManager.shared.user
    }
    
    // MARK: Internal Methods
    
    func loadSellerAndUserStatus(_ completion: @escaping() -> Void) {
        let group = DispatchGroup()
        
        group.enter()
        loadSeller {
            group.leave()
        }
        
        group.enter()
        loadCheckStatus {
            group.leave()
        }
        
        group.enter()
        checkBankAccountErrors {
            group.leave()
        }
        
        group.notify(queue: .main, execute: completion)
    }
    
    func loadTransactions(_ completion: @escaping((LegacyPJError?) -> Void)) {
        tableList.isLoading = true
        apiTransaction.list(nil) { [weak self] result in
            switch result {
            case .success(let response):
                self?.tableList.populate(section: 0, list: response.list)
                self?.tableList.pagination = response.pagination
                completion(nil)
            case .failure(let error):
                completion(error)
            }
            self?.tableList.isLoading = false
        }
    }
    
    func loadMoreTransactions(_ completion: @escaping ((LegacyPJError?) -> Void)) {
        guard !tableList.isLoading else { return }
        
        tableList.isLoading = true
        apiTransaction.list(tableList.pagination.nextPage) { [weak self] result in
            switch result {
            case .success(let response):
                self?.tableList.append(section: 0, list: response.list)
                self?.tableList.pagination = response.pagination
                completion(nil)
            case .failure(let error):
                completion(error)
            }
            self?.tableList.isLoading = false
        }
    }
    
    func cancelTransaction(password: String, transaction: Transaction, indexPath: IndexPath, _ completion: @escaping ( (_ error: LegacyPJError?) -> Void )) {
        startLoading(indexPath: indexPath)
        apiTransaction.cancelTransaction(pin: password, id: transaction.id) { [weak self] error in
            self?.stopLoading(indexPath: indexPath)
            DispatchQueue.main.async {
                if error == nil {
                    transaction.status = .canceled
                }
                completion(error)
            }
        }
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        tableList.sections[section].rows.count
    }
    
    func shouldLoadMoreItems() -> Bool {
        !tableList.isLoading && tableList.pagination.hasNext
    }
    
    func transationForRow(_ index: IndexPath) -> Transaction? {
        tableList[index]
    }
    
    func isLoading(indexPath: IndexPath) -> Bool {
        tableList.sections[indexPath.section].rows[indexPath.row].isLoading
    }
    
    func startLoading(indexPath: IndexPath) {
        tableList.sections[indexPath.section].rows[indexPath.row].isLoading = true
    }
    
    func stopLoading(indexPath: IndexPath) {
        tableList.sections[indexPath.section].rows[indexPath.row].isLoading = false
    }

    func presentAdvertising(_ completion: @escaping (AlertModalInfo) -> Void) {
        guard seller?.biometry == true,
              seller?.digitalAccount == true,
              container.featureManager.isActive(.releasePIXPresenting),
              !container.kvStore.boolFor(BizKvKey.didPresentAdvertisingPix) else {
            return
        }
        
        container.kvStore.set(value: true, with: BizKvKey.didPresentAdvertisingPix)

        let modalInfoImage = AlertModalInfoImage(image: Resources.Logos.pixLogo.image,
                                                 size: CGSize(width: 150, height: 53))
        let info = AlertModalInfo(
            imageInfo: modalInfoImage,
            title: Localizable.pixCarouselAlertTitle,
            subtitle: Localizable.pixCarouselAlertDescription,
            buttonTitle: Localizable.pixCarouselAlertButton,
            secondaryButtonTitle: Localizable.pixCarouselAlertSecondaryButton
        )

        completion(info)
    }

    func mustPresentWelcome() -> Bool {
        container.kvStore.boolFor(KVKey.isPixKeyManagementWelcomeVisualized)
    }

    func mustPresentHub() -> Bool {
        container.featureManager.isActive(.releaseHubPixPresenting)
    }

    func trackPixRegister() {
        guard let seller = seller else {
            return
        }
        let event = TransactionCarouselAnalytics.modalShowPixEvent(sellerID: seller.id, companyName: seller.razaoSocial ?? "")
        container.analytics.log(event)
    }

    func trackPixNotNow() {
        guard let seller = seller else {
            return
        }
        let event = TransactionCarouselAnalytics.modalNotNowPixEvent(sellerID: seller.id, companyName: seller.razaoSocial ?? "")
        self.container.analytics.log(event)
    }
    
    func isPixTransactionsButtonEnabled() -> Bool {
        container.featureManager.isActive(.releasePIXTransactionsButtonPresenting)
    }
}

// MARK: - Private Methods

private extension TransactionListAllViewModel {
    func loadSeller(_ completion: @escaping () -> Void) {
        let api = ApiSeller()

        api.data { [weak self] result in
            switch result {
            case .success(let seller):
                self?.seller = seller
                TrackingManager.setupMixpanel(sellerID: seller.id)
                
                guard let cnpj = seller.cnpj else {
                    completion()
                    return
                }
                
                self?.loadCompanyInfo(cnpj: cnpj, completion: completion)
            case .failure:
                completion()
            }
        }
    }
    
    func loadCheckStatus(_ completion: @escaping() -> Void) {
        service?.checkStatus(completion: { [weak self] result in
            switch result {
            case .success(let model):
                self?.userStatus = model
            case .failure:
                break
            }
            completion()
        })
    }
    
    func checkBankAccountErrors(_ completion: @escaping() -> Void) {
        guard container.featureManager.isActive(.releaseBankAccountWarning) else {
            completion()
            return
        }
        
        homeService.checkBankAccount { [weak self] result in
            switch result {
            case .success(let accountError):
                self?.bankAccountError = accountError
            case .failure:
                break
            }
            completion()
        }
    }
    
    func loadCompanyInfo(cnpj: String, completion: @escaping() -> Void) {
        guard container.featureManager.isActive(.releaseBankAccountWarning) else {
            completion()
            return
        }
        
        apiSignUp.verifySeller(cnpj) { [weak self] result in
            switch result {
            case .success(let companyInfo):
                self?.companyInfo = companyInfo
            case .failure:
                break
            }
            completion()
        }
    }
}
