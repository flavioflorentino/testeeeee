import Foundation
import LegacyPJ

protocol HistoryItemViewModel {
    // MARK: - Properties
    var historyItem: HistoryItem { get set }
    
    // MARK: - Methods
    func loadHistoryItem(_ completion:@escaping ( (_ error: LegacyPJError?) -> Void) )
}

final class OperatorReeiptViewModel: HistoryItemViewModel {
    var historyItem: HistoryItem
    
    // MARK: Dependencies
    
    let apiTransaction: ApiTransaction
    
    init(historyItem: HistoryItem, apiTransaction: ApiTransaction = ApiTransaction()) {
        self.apiTransaction = apiTransaction
        self.historyItem = historyItem
    }
    
    func loadHistoryItem(_ completion:@escaping ( (_ error: LegacyPJError?) -> Void) ) {
        apiTransaction.transactionHistory(historyItem.id) { [weak self] item, error in
            if let item = item {
                self?.historyItem = item
            }
            
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
}
