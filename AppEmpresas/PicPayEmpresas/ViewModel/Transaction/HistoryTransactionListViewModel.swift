import Foundation
import LegacyPJ

protocol HistoryTransactionListViewModel {
    // MARK: - Properties
    
    var operatorr: Operator { get }
    
    // MARK: - Methods
    
    func loadHistoryItems(_ completion: @escaping((Error?) -> Void))
    func loadMoreHistoryItems(_ completion: @escaping((Error?) -> Void))
    func historyItemForRow(_ index: IndexPath) -> HistoryItem?
    func numberOfRowsInSection(section: Int) -> Int
    func shouldLoadMoreItems() -> Bool
}

final class HistoryTransactionListAllViewModel: HistoryTransactionListViewModel {
    // MARK: - Properties
    var operatorr: Operator
    
    // MARK: - Private Properties
    
    private var list: [HistoryItem]
    private var hasNext: Bool = false
    private var isLoading: Bool = false
    
    // MARK: Dependencies
    
    let apiTransaction: ApiTransaction
    
    init(operatorr: Operator, apiTransaction: ApiTransaction = ApiTransaction()) {
        self.apiTransaction = apiTransaction
        self.list = []
        self.operatorr = operatorr
    }
    
    // MARK: Public Methods
    
    func loadHistoryItems(_ completion: @escaping((Error?) -> Void)) {
        isLoading = true
        apiTransaction.userSellerTransactionHistory(operatorr.id, lastId: nil) { [weak self] result in
            self?.isLoading = false
            
            switch result {
            case .success(let response):
                self?.list = response.list
                self?.hasNext = response.hasNext
                completion(nil)
                
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    func loadMoreHistoryItems(_ completion: @escaping((Error?) -> Void)) {
        guard !isLoading else { return }
        
        isLoading = true
        apiTransaction.userSellerTransactionHistory(operatorr.id, lastId: list.last?.id) { [weak self] result in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self?.isLoading = false
                
                switch result {
                case .success(let response):
                    self?.list.append(contentsOf: response.list)
                    self?.hasNext = response.hasNext
                    completion(nil)
                    
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        list.count
    }
    
    func shouldLoadMoreItems() -> Bool {
        !isLoading && hasNext
    }
    
    func historyItemForRow(_ index: IndexPath) -> HistoryItem? {
        if index.row < list.count {
            return list[index.row]
        }
        return nil
    }
}
