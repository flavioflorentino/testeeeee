import Foundation
import LegacyPJ

protocol ExportMovementViewModel {
    // MARK: Properties
    var fromDate: String { get set }
    var toDate: String { get set }
    var type: String { get set }
    
    // MARK: - Methods
    func exportMovement(_ completion: @escaping((_ email: String?, _ error: LegacyPJError?) -> Void))
}

final class ExportMovementImpViewModel: ExportMovementViewModel {
    // MARK: Properties
    
    var fromDate: String = ""
    var toDate: String = ""
    var type: String = ""
    
    // MARK: Dependencies
    
    var apiMovement: ApiMovement
    
    init(apiMovement: ApiMovement = ApiMovement()) {
        self.apiMovement = apiMovement
    }
    
    func exportMovement(_ completion: @escaping((_ email: String?, _ error: LegacyPJError?) -> Void)) {
        apiMovement.exportMovement(from: fromDate, to: toDate, type: type) { email, error in
            DispatchQueue.main.async {
                completion(email, error)
            }
        }
    }
}
