import Foundation
import LegacyPJ

protocol MovementListViewModel {
    var onboardTitle: String { get }
    var onboardText: String { get }
    
    // MARK: - Methods
    
    func loadMovementItems(_ completion:@escaping( (_ response: MovementItemListResponse) -> Void) )
    func loadMoreMovementItems(_ completion:@escaping( (_ response: MovementItemListResponse) -> Void) )
    func movementItemForRow(_ index: IndexPath) -> MovementItemRow?
    func numberOfRowsInSection(section: Int) -> Int
    func shouldLoadMoreItems() -> Bool
    func numberOfSections() -> Int
    func movementItemForSection(section: Int) -> MovementItem?
}

class MovementListBaseViewModel: MovementListViewModel {
    var onboardTitle: String = ""
    var onboardText: String = ""
    
    // MARK: - Private Properties
    
    var list: [MovementItem]
    var hasNext: Bool = false
    var isLoading: Bool = false
    
    // MARK: Dependencies
    
    let apiMovement: ApiMovement
    
    init(apiMovement: ApiMovement = ApiMovement()) {
        self.apiMovement = apiMovement
        self.list = []
        configure()
    }
    
    // MARK: Public Methods
    func configure() {}
    
    func loadItems(lastId: Int?, _ completion: @escaping((_ response: MovementItemListResponse) -> Void) ) {
        // Implementation on child class
    }
    
    func loadMovementItems(_ completion: @escaping ((_ response: MovementItemListResponse) -> Void)) {
        isLoading = true
        loadItems(lastId: nil) { [weak self] response in
            DispatchQueue.main.async {
                self?.isLoading = false
                self?.list = response.list
                
                self?.hasNext = response.hasNext
                completion(response)
            }
        }
    }
    
    func loadMoreMovementItems(_ completion: @escaping ((_ response: MovementItemListResponse) -> Void)) {
        guard !isLoading else { return }
        isLoading = true
        loadItems(lastId: nil) { [weak self] response in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self?.list.append(contentsOf: response.list)
                self?.hasNext = response.hasNext
                completion(response)
                self?.isLoading = false
            }
        }
    }
    
    func numberOfSections() -> Int {
        list.count
    }
    
    func  movementItemForSection(section: Int) -> MovementItem? {
        list[section]
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        list[section].list.count
    }
    
    func shouldLoadMoreItems() -> Bool {
        !isLoading && hasNext
    }
    
    func movementItemForRow(_ index: IndexPath) -> MovementItemRow? {
        list[index.section].list[index.row]
    }
}

final class MovementListNormalViewModel: MovementListBaseViewModel {
    override func configure() {
        super.configure()
        onboardTitle = "Sem atividades passadas"
        onboardText = "Nesta tela você verá uma lista com o detalhamento das atividades anteriores."
    }
    
    override func loadItems(lastId: Int?, _ completion: @escaping((_ response: MovementItemListResponse) -> Void)) {
        apiMovement.list(lastId, completion)
    }
}

final class MovementListFutureViewModel: MovementListBaseViewModel {
    override func configure() {
        super.configure()
        onboardTitle = "Sem lançamentos futuros"
        onboardText = "Nesta tela você verá uma lista de todos os seus lançamentos futuros"
    }
    
    override func loadItems(lastId: Int?, _ completion: @escaping((_ response: MovementItemListResponse) -> Void) ) {
        apiMovement.listFutureMovement(lastId, completion)
    }
}
