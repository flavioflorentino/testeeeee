import Foundation
import LegacyPJ

protocol OperatorCurrentTransactionsViewModel: TransactionListViewModel {
    var totalValue: String { get }
    var userSeller: Operator { get set }
}

final class OperatorCurrentTransactionsAllViewModel: TransactionListAllViewModel, OperatorCurrentTransactionsViewModel {
    // MARK: Properties
    
    var totalValue: String = ""
    var userSeller: Operator
    
    // MARK: Private Properties
    
    // MARK: Dependencies
    
    init(userSeller: Operator, apiTransaction: ApiTransaction = ApiTransaction()) {
        self.userSeller = userSeller
        super.init(container: DependencyContainer(), apiTransaction: apiTransaction)
    }
    
    // MARK: - Public Methods
    
    override func loadTransactions(_ completion: @escaping((LegacyPJError?) -> Void)) {
        tableList.isLoading = true
        apiTransaction.userSellerTransactionList(userSeller.id, page: nil) { [weak self] result in
            self?.tableList.isLoading = false
            
            switch result {
            case .success(let response):
                self?.totalValue = response.totalValue
                self?.tableList.populate(section: 0, list: response.list.list)
                self?.tableList.pagination = response.list.pagination
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    override func loadMoreTransactions(_ completion: @escaping((LegacyPJError?) -> Void)) {
        guard !tableList.isLoading else { return }
        
        tableList.isLoading = true
        apiTransaction.userSellerTransactionList(userSeller.id, page: tableList.pagination.nextPage) { [weak self] result in
            self?.tableList.isLoading = false
            
            switch result {
            case .success(let response):
                self?.totalValue = response.totalValue
                self?.tableList.append(section: 0, list: response.list.list)
                self?.tableList.pagination = response.list.pagination
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
}
