import UIKit
import LegacyPJ

protocol OperatorListViewModel {
    // MARK: Methods
    
    func loadOperators(_ completion: @escaping(LegacyPJError?) -> Void)
    func loadMoreOperators(_ completion: @escaping(LegacyPJError?) -> Void)
    func operatorForRow(_ index: IndexPath) -> Operator?
    func numberOfRowsInSection(section: Int) -> Int
    func shouldLoadMoreItems() -> Bool
    
    func deleteOperator(password: String, userSellerId: Int, _ completion: @escaping( (_ error: LegacyPJError?) -> Void ))
}

final class OperatorListAllViewModel: OperatorListViewModel {
    // MARK: Properties
    
    private var list: [Operator]
    private var pagination = Pagination()
    private var isLoading: Bool = false
    
    // MARK: Dependencies
    
    let apiSeller: ApiSeller
    
    init(apiSeller: ApiSeller = ApiSeller()) {
        self.apiSeller = apiSeller
        self.list = [Operator]()
    }
    
    // MARK: Public Methods
    
    func loadOperators(_ completion: @escaping(LegacyPJError?) -> Void) {
        isLoading = true
        apiSeller.operatorsList(nil) { [weak self] result in
            self?.isLoading = false
            switch result {
            case .success(let response):
                self?.list = response.operators
                self?.pagination = response.pag
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    func loadMoreOperators(_ completion: @escaping(LegacyPJError?) -> Void) {
        guard !isLoading else { return }
        
        isLoading = true
        apiSeller.operatorsList(nil) { [weak self] result in
            self?.isLoading = false
            switch result {
            case .success(let response):
                self?.list.append(contentsOf: response.operators)
                self?.pagination = response.pag
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        list.count
    }
    
    func shouldLoadMoreItems() -> Bool {
        !isLoading && pagination.hasNext
    }
    
    func operatorForRow(_ index: IndexPath) -> Operator? {
        if index.row < list.count {
            return list[index.row]
        }
        return nil
    }
    
    func deleteOperator(password: String, userSellerId: Int, _ completion: @escaping( (_ error: LegacyPJError?) -> Void )) {
        apiSeller.deleteOperator(pin: password, userSellerId: userSellerId) { error in
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
}
