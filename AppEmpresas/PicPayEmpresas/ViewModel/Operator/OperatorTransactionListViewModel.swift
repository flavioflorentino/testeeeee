import Foundation
import LegacyPJ

protocol OperatorTransactionListViewModel: TransactionListViewModel {
    var totalValue: String { get }
    
     func chashierClose(password: String, _ completion:@escaping( (_ historyItem: HistoryItem?, _ error: LegacyPJError?) -> Void))
}

final class OperatorTransactionAllListViewModel: TransactionListAllViewModel, OperatorTransactionListViewModel {
    // MARK: Properties
    
    var totalValue: String = ""
    
    // MARK: Dependencies
    
    let apiOperator: ApiOperator
    
    init(apiTransaction: ApiTransaction = ApiTransaction(), apiOperator: ApiOperator = ApiOperator()) {
        self.apiOperator = apiOperator
        super.init(container: DependencyContainer(), apiTransaction: apiTransaction)
    }
    
    // MARK: - Internal Methods
    
    func chashierClose(password: String, _ completion:@escaping( (_ historyItem: HistoryItem?, _ error: LegacyPJError?) -> Void)) {
        apiOperator.cashierBallance(pin: password) { historyItem, error in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                completion(historyItem, error)
            }
        }
    }
    
    override func loadTransactions(_ completion: @escaping((LegacyPJError?) -> Void)) {
        tableList.isLoading = true
        apiTransaction.userSellerTransactionList(nil, page: nil) { [weak self] result in
            self?.tableList.isLoading = false
            
            switch result {
            case .success(let response):
                self?.totalValue = response.totalValue
                self?.tableList.populate(section: 0, list: response.list.list)
                self?.tableList.pagination = response.list.pagination
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    override func loadMoreTransactions(_ completion: @escaping((LegacyPJError?) -> Void)) {
        guard !tableList.isLoading else { return }
        
        tableList.isLoading = true
        apiTransaction.userSellerTransactionList(nil, page: tableList.pagination.nextPage) { [weak self] result in
            self?.tableList.isLoading = false
            
            switch result {
            case .success(let response):
                self?.totalValue = response.totalValue
                self?.tableList.append(section: 0, list: response.list.list)
                self?.tableList.pagination = response.list.pagination
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
}
