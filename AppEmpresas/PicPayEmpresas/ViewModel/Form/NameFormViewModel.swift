import Foundation
import LegacyPJ

protocol NameFormViewModel {
    // MARK: Properties
    var nameHasInitialLoad: Bool { get }
    
    var seller: Seller { get set }
    
    // MARK: Methods
    func saveName(password: String, _ completion: @escaping( (_ error: LegacyPJError?) -> Void) )
    func loadName(_ completion: @escaping( (_ error: LegacyPJError?) -> Void) )
}

final class UpdateNameFormViewModel: NameFormViewModel {
    var nameHasInitialLoad: Bool = true
    var seller: Seller
    
    // MARK: - Dependence
    
    var apiSeller: ApiSeller
    
    init(apiSeller: ApiSeller = ApiSeller()) {
        self.apiSeller = apiSeller
        self.seller = Seller()
    }
    
     // MARK: Public Methods
    
    func saveName(password: String, _ completion: @escaping ((LegacyPJError?) -> Void)) {
        guard let name = seller.name else {
            return
        }
        apiSeller.updateDisplayName(pin: password, name: name) { error in
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
    
    func loadName(_ completion: @escaping ((LegacyPJError?) -> Void)) {
        completion(nil)
    }
}
