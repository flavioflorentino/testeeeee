import Foundation
import LegacyPJ

protocol PhoneFormViewModel {
    // MARK: - Properties
    var seller: Seller { get set }
    var user: User { get set }
    
    // MARK: - Methods
    func savePhoneData(password: String, _ completion: @escaping( (_ error: LegacyPJError?) -> Void) )
}

final class UpdatePhoneFormViewModel: PhoneFormViewModel {
    // MARK: - Properties

    var seller: Seller
    var user: User
    
    // MARK: - Dependence
    
    var apiSeller: ApiSeller
    
    init(user: User, seller: Seller, apiSeller: ApiSeller = ApiSeller()) {
        self.user = user
        self.seller = seller
        self.apiSeller = apiSeller
    }
    
    // MARK: - Public Methods
    
    func savePhoneData(password: String, _ completion: @escaping( (_ error: LegacyPJError?) -> Void) ) {
        if let userPhone = user.phone, let storePhone = seller.store?.phone {
            self.apiSeller.updatePhones(pin: password, userPhone: userPhone, storePhone: storePhone) { error in
                DispatchQueue.main.async {
                    completion(error)
                }
            }
        }
    }
}
