import Foundation
import LegacyPJ

protocol BankSelectFormViewModel {
    // MARK: Methods
    
    func loadBanks(_ completion:@escaping( (_ error: LegacyPJError?) -> Void) )
    func bankForRow(_ index: IndexPath) -> Bank
    func numberOfRowsInSection(section: Int) -> Int
    func shouldLoadMoreItems() -> Bool
}

final class BankSelectListFormViewModel: BankSelectFormViewModel {    
    var list: [Bank]
    var pagination: Pagination
    var isLoading: Bool = false
    
    // MARK: Dependencies
    
    var apiCommon: ApiCommon
    
    init( apiCommon: ApiCommon = ApiCommon()) {
        self.apiCommon = apiCommon
        self.list = []
        self.pagination = Pagination()
    }
    
    // MARK: Methods
    
    func loadBanks(_ completion:@escaping( (_ error: LegacyPJError?) -> Void) ) {
        apiCommon.bankList { [weak self] result in
            switch result {
            case .success(let bankList):
                self?.list = bankList
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    func bankForRow(_ index: IndexPath) -> Bank {
        list[index.row]
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        list.count
    }
    
    func shouldLoadMoreItems() -> Bool {
        pagination.hasNext
    }
}
