import Foundation
import LegacyPJ

protocol PasswordFormViewModel {
    // MARK: - Properties
    
    var password: String { get set }
    var currentPassword: String { get set }
    
    var user: User { get }
    
    // MARK: - Methods
    func savePassword(_ completion: @escaping( (_ error: LegacyPJError?) -> Void ))
}

final class UpdatePasswordFormViewModel: PasswordFormViewModel {
    // MARK: - Properties
    
     var password: String
     var currentPassword: String
    
    // MARK: - Dependence
    
    var apiUser: ApiUser
    var user: User
    
    init(user: User, apiUser: ApiUser = ApiUser()) {
        self.apiUser = apiUser
        self.password = ""
        self.currentPassword = ""
        self.user = user
    }
    
    // MARK: - Public Methods
    
    func savePassword(_ completion: @escaping( (_ error: LegacyPJError?) -> Void )) {
        apiUser.changePassword(currentPassword, password: password, id: user.id ?? 0) { error in
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
}
