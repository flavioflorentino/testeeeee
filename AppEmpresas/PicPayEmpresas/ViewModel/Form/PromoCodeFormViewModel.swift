import Foundation
import LegacyPJ

protocol PromoCodeFormViewModelType: AnyObject {
    func useCode(code: String, _ completion: @escaping ((_ result: PromoCodeResult?, _ error: LegacyPJError?) -> Void) )
}

final class PromoCodeFormViewModel: PromoCodeFormViewModelType {
    // MARK: - Dependence
    
    var apiSeller: ApiSeller
    
    init(apiSeller: ApiSeller = ApiSeller()) {
        self.apiSeller = apiSeller
    }
    
    // MARK: Public Methods
    
    func useCode(code: String, _ completion: @escaping ((_ result: PromoCodeResult?, _ error: LegacyPJError?) -> Void) ) {
        apiSeller.usePromoCode(code: code) { result, error in
            DispatchQueue.main.async {
                completion(result, error)
            }
        }
    }
}
