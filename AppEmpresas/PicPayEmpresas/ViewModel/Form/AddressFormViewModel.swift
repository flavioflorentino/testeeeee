import Foundation
import LegacyPJ

protocol AddressFormViewModel {
    // MARK: Properties
    var addressSaveButtonLabel: String { get }
    var addressHasInitialLoad: Bool { get }
    var addressFormType: FormType { get }
    
    var address: Address { get set }
    
    // MARK: Methods
    func saveAddress(password: String, _ completion: @escaping( (_ error: LegacyPJError?) -> Void) )
    func loadAddress(_ completion: @escaping( (_ error: LegacyPJError?) -> Void) )
    func loadAddressByCep(cep: String, callback: @escaping (_ address: Address?, _ error: LegacyPJError?) -> Void)
    func loadAddressCoordinate(_ callback: @escaping (_ lat: Double?, _ lon: Double?, _ error: LegacyPJError?) -> Void)
}

final class UpdateAddressFormViewModel: AddressFormViewModel {
     var address: Address

    var addressSaveButtonLabel: String = "Buscar"
    var addressHasInitialLoad: Bool = true
    var addressFormType: FormType = .update
    
    // MARK: - Dependence
    var apiCommon: ApiCommon
    var apiSeller: ApiSeller
    
    init(apiCommon: ApiCommon = ApiCommon(), apiSeller: ApiSeller = ApiSeller()) {
        address = Address()
        self.apiCommon = apiCommon
        self.apiSeller = apiSeller
    }
    
    /// Load the address for cep
    /// Obs.: the callback will be called in the main thread
    func loadAddressByCep(cep: String, callback: @escaping (_ address: Address?, _ error: LegacyPJError?) -> Void) {
        apiCommon.addressByCEP(cep: cep) { address, error in
            DispatchQueue.main.async {
                callback(address, error)
            }
        }
    }
    
    /// Update the seller address
    func saveAddress(password: String, _ completion: @escaping( (_ error: LegacyPJError?) -> Void) ) {
        apiSeller.updateAddress(pin: password, address: address) { error in
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
    
    func loadAddress(_ completion: @escaping( (_ error: LegacyPJError?) -> Void) ) {
        completion(nil)
    }
    
    func loadAddressCoordinate(_ callback: @escaping (_ lat: Double?, _ lon: Double?, _ error: LegacyPJError?) -> Void) {
        guard address.cep != nil else {
            return
        }
        
        apiCommon.addressCoordinate(address: address, { [weak self] lat, lon, error in
            self?.address.lat = lat
            self?.address.lon = lon
            
            DispatchQueue.main.async {
                callback(lat, lon, error)
            }
        })
    }
}
