import UIKit
import LegacyPJ

enum FormType {
    case insert
    case update
}

protocol OperatorFormModel {
    // MARK: Properties
    var saveButtonLabel: String { get }
    var hasInitialLoad: Bool { get }
    var formType: FormType { get }
    
    var data: Operator { get set }
    var image: Data? { get set }
    
    // MARK: Methods
    func save(pin: String, uploadProgress: @escaping( (_ progress: Double) -> Void), _ completion: @escaping( (_ error: LegacyPJError?) -> Void) )
    func loadData(_ completion: @escaping( (_ error: LegacyPJError?) -> Void) )
}

final class AddOperatorFormViewModel: OperatorFormModel {
    var image: Data?
    var data: Operator
    var saveButtonLabel: String = "Cadastrar operador"
    var hasInitialLoad: Bool = false
    var formType = FormType.insert
    
    // MARK: - Dependence
    
    var apiSeller: ApiSeller
    
    init(apiSeller: ApiSeller = ApiSeller()) {
        self.apiSeller = apiSeller
        self.data = Operator()
    }
    
    // MARK: Public Methods
    
    func save(pin: String, uploadProgress:@escaping ((_ progress: Double) -> Void), _ completion: @escaping ((LegacyPJError?) -> Void)) {
        apiSeller.createOperator(
            pin: pin,
            operatorData: data,
            image: image,
            uploadProgress: { [weak self] progress in
                if self?.image != nil {
                    uploadProgress(progress)
                }
            }, completion: { error in
                completion(error)
            }
        )
    }
    
    func loadData(_ completion: @escaping( (_ error: LegacyPJError?) -> Void) ) {
        completion(nil)
    }
}

final class UpdateOperatorFormViewModel: OperatorFormModel {
    var image: Data?
    var data: Operator
    var saveButtonLabel: String = "Salva alterações"
    var hasInitialLoad: Bool = true
    var formType = FormType.update
    
    // MARK: - Dependence
    
    var apiSeller: ApiSeller
    
    init(operatorr: Operator, apiSeller: ApiSeller = ApiSeller()) {
        self.apiSeller = apiSeller
        self.data = operatorr
    }
    
    // MARK: Public Methods
    
    func save(
        pin: String,
        uploadProgress: @escaping (( _ progress: Double) -> Void),
        _ completion: @escaping ((LegacyPJError?) -> Void)
    ) {
        apiSeller.updateOperator(
            pin: pin,
            operatorData: data,
            image: image,
            uploadProgress: { [weak self] progress in
                if self?.image != nil {
                    uploadProgress(progress)
                }
            }, completion: { error in
                DispatchQueue.main.async {
                    completion(error)
                }
            }
        )
    }
    
    func loadData(_ completion: @escaping ((LegacyPJError?) -> Void)) {
        completion(nil)
    }
}
