import Foundation
import LegacyPJ

protocol DayFormViewModel {
    // MARK: Properties
    var dayHasInitialLoad: Bool { get }
    
    var feeFirstDay: String { get }
    var feeLastDay: String { get }
    var feeDefaultDay: Int? { get set }
    var feeListItems: [String: PercentageFee] { get }
    var feeSelectedDay: Int { get set }
    var zeroFee: Bool { get set }
    
    // MARK: Methods
    
    func loadFeeDay(_ completion: @escaping BaseCompletion)
    func feeByDay(day: String) -> String
    func rawFeeByDay(day: String) -> Double
    func loadFeeList(_ callback: @escaping BaseCompletion)
    func saveFeeDay(password: String, _ completion: @escaping BaseCompletion)
}

final class UpdateDayFormViewModel: DayFormViewModel {
    // MARK: Properties
    
    var dayHasInitialLoad: Bool = true
    var feeFirstDay: String = ""
    var feeLastDay: String = ""
    var feeDefaultDay: Int?
    var feeListItems: [String: PercentageFee] = [:]
    var zeroFee: Bool = false
    
    var feeSelectedDay: Int = 0
    
    // MARK: - Dependence
    var apiCommon: ApiCommon
    var apiSeller: ApiSeller
    
    init(apiCommon: ApiCommon = ApiCommon(), apiSeller: ApiSeller = ApiSeller()) {
        self.apiCommon = apiCommon
        self.apiSeller = apiSeller
    }
    
    // MARK: Public Methods
    
    /// Load the list of fee
    func loadFeeList(_ callback: @escaping BaseCompletion) {
        apiCommon.feeList { [weak self] object, error in
            if let firstDay = object?.firstDay {
                self?.feeFirstDay = firstDay
            }
            
            if let feeLastDay = object?.lastDay {
                self?.feeLastDay = feeLastDay
            }
            
            if let defaultDay = object?.defaultDay {
                if let day = Int(defaultDay) {
                    self?.feeDefaultDay = day
                }
            }
            
            if let list = object?.items {
                self?.feeListItems = list
            }
            
            if let daysToReleaseCc = object?.daysToReleaseCc {
                self?.feeSelectedDay = daysToReleaseCc
            }
            
            if let zeroFee = object?.zeroFee {
                self?.zeroFee = zeroFee
            }
            
            DispatchQueue.main.async {
                callback(error)
            }
        }
    }
    
    /// Get the fee for the day
    func feeByDay(day: String) -> String {
        if let fee = self.feeListItems[day] {
            return  fee.value
        }
        return "-"
    }
    
    /// Get the fee for the day
    func rawFeeByDay(day: String) -> Double {
        if let fee = self.feeListItems[day] {
            return  fee.rawValue
        }
        return 0.0
    }
    
    /// Save the seleted plan
    func saveFeeDay(password: String, _ completion: @escaping BaseCompletion) {
        apiSeller.updatePlanFee(pin: password, daysToReleaseCc: feeSelectedDay) { error in
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
    
    func loadFeeDay(_ completion: @escaping BaseCompletion) {
        completion(nil)
    }
}
