import Foundation
import LegacyPJ

protocol EmailFormViewModel {
    // MARK: - Properties
    var user: User { get set }
    
    // MARK: - Methods
    func saveEmail(password: String, _ completion: @escaping( (_ error: LegacyPJError?) -> Void ))
}

final class UpdateEmailFormViewModel: EmailFormViewModel {
    // MARK: - Properties
    
     var user: User
    
    // MARK: - Dependence
    
    var apiUser: ApiUser
    
    init(apiUser: ApiUser = ApiUser()) {
        self.apiUser = apiUser
        self.user = User()
    }
    
    // MARK: - Public Methods
    
    func saveEmail(password: String, _ completion: @escaping( (_ error: LegacyPJError?) -> Void )) {
        var u = User()
        u.id = user.id
        u.email = user.email
        
        apiUser.update(pin: password, user: u) { error in
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
}
