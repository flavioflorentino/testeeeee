import Foundation
import LegacyPJ

protocol CnpjFormViewModel {
    // MARK: Properties
    var cnpjHasInitialLoad: Bool { get }
    var accessToken: String? { get }
    var seller: Seller { get set }
    
    // MARK: Methods
    func saveCnpj(password: String, _ completion: @escaping( (_ error: LegacyPJError?) -> Void) )
    func loadCnpj(_ completion: @escaping( (_ error: LegacyPJError?) -> Void) )
}

final class UpdateCnpjFormViewModel: CnpjFormViewModel {
    typealias Dependencies = HasAuthManager
    var cnpjHasInitialLoad: Bool = true
    var seller: Seller
    var accessToken: String? {
        dependencies.authManager.userAuth?.auth.accessToken
    }
    
    // MARK: - Dependence
    
    var apiSeller: ApiSeller
    
    private let dependencies: Dependencies
    init(dependencies: Dependencies, seller: Seller, apiSeller: ApiSeller = ApiSeller()) {
        self.dependencies = dependencies
        self.apiSeller = apiSeller
        self.seller = seller
    }
    
    // MARK: Methods
    
    func saveCnpj(password: String, _ completion: @escaping ((LegacyPJError?) -> Void)) {
        var sellerUpdate = Seller()
        sellerUpdate.cnpj = seller.cnpj
        sellerUpdate.razaoSocial = seller.razaoSocial
        
        apiSeller.update(pin: password, seller: sellerUpdate) { _, error in
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
    
    func loadCnpj(_ completion: @escaping ((LegacyPJError?) -> Void)) {
        completion(nil)
    }
}
