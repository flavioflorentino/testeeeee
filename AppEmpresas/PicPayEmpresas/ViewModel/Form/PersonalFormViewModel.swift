import Foundation
import LegacyPJ

protocol PersonalFormViewModel {
    // MARK: - Properties
    var personalHasInitialLoad: Bool { get }
    var personalData: User { get set }
    
    // MARK: - Methods
    func savePersonalData(password: String, _ completion: @escaping( (_ error: LegacyPJError?) -> Void) )
    func loadPersonalData(_ completion: @escaping( (_ error: LegacyPJError?) -> Void) )
}

final class UpdatePersonalFormViewModel: PersonalFormViewModel {
    // MARK: - Properties
    
    var personalHasInitialLoad: Bool = true
    var personalData: User
    
    // MARK: - Dependence
    
    var apiUser: ApiUser
    
    init(apiUser: ApiUser = ApiUser()) {
        self.apiUser = apiUser
        self.personalData = User()
    }
    
    // MARK: - Public Methods
    
    func savePersonalData(password: String, _ completion: @escaping( (_ error: LegacyPJError?) -> Void) ) {
        var user = User()
        user.id = personalData.id
        user.name = personalData.name
        user.cpf = personalData.cpf
        user.birthDate = personalData.birthDate
        
        apiUser.update(pin: password, user: user) { error in
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
    
    func loadPersonalData(_ completion: @escaping( (_ error: LegacyPJError?) -> Void) ) {
        apiUser.data { [weak self] user, error in
            if let personalData = user {
                self?.personalData = personalData
            }
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
}
