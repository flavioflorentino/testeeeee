import Foundation
import LegacyPJ

protocol BankAccountFormViewModel {
    // MARK: Properties
    var bankAccountLoaded: Bool { get }
    var bankAccount: BankAccount { get set }
    
    // MARK: Methods
    func saveBankAccount(password: String, _ completion: @escaping( (_ error: LegacyPJError?) -> Void) )
    func loadBankAccount(_ completion: @escaping( (_ error: LegacyPJError?) -> Void) )
}

final class UpdateBankAccountFormViewModel: BankAccountFormViewModel {
    // MARK: Properties
    
    var bankAccountLoaded: Bool = false
    var bankAccount: BankAccount
    
    // MARK: - Dependence
    
    var apiSeller: ApiSeller
    
    init(apiSeller: ApiSeller = ApiSeller()) {
        self.apiSeller = apiSeller
        self.bankAccount = BankAccount()
    }
    
    // MARK: Public Methods
    
    func saveBankAccount(password: String, _ completion: @escaping ((LegacyPJError?) -> Void)) {
        apiSeller.updateBankAccount(pin: password, bankAccount: bankAccount) { error in
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
    
    func loadBankAccount(_ completion: @escaping ((LegacyPJError?) -> Void)) {
        apiSeller.bankAccount { [weak self] bankAccount, error in
            if let bankAccount = bankAccount {
                self?.bankAccount = bankAccount
                self?.bankAccountLoaded = true
            }
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
}
