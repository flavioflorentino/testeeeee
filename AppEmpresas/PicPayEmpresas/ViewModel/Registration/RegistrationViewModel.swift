import AnalyticsModule
import Core
import CoreSellerAccount
import Foundation
import Mixpanel
import LegacyPJ
import FeatureFlag

enum RegistrationType {
    case personal, company
}

final class RegistrationViewModel {
    // MARK: Internal Properties

    // form data
    var account: SignUpAccount
    var companyType: CompanyType = .multiPartners
    var bankAccountType: RegistrationType
    var bankAccountValue: BankAccountType.Value?
    var ableToDigitalAccount = false
    
    // extra data
    var banks: [Bank] = [Bank]()
    var feeFirstDay: String = "1"
    var feeLastDay: String = "1"
    var feeDefaultDay: String = "1"
    var feeListItems: [String: String] = [:]
    
    var bankAccountTypes: [BankAccountType] = []
    
    let apiSignUp: ApiSignUp
    private let bankAccountTypeService: BankAccountTypesServicing = BankAccountTypesService()
    private let dependencies: HasAnalytics & HasKVStore & HasFeatureManager & HasAuthManager = DependencyContainer()
    
    // MARK: - Initializers
    
    init(apiSignUp: ApiSignUp = ApiSignUp() ) {
        self.apiSignUp = apiSignUp
        self.account = SignUpAccount()
        
        self.account.distinctId = Mixpanel.mainInstance().distinctId
        
        companyType = .multiPartners
        bankAccountType = .company
    }
}

// MARK: - Internal Methods

extension RegistrationViewModel {
    func toDictionary() -> [String: Any] {
        let dictionary: [String: Any] = [
            "account": account.toDictionary(),
            "companyType": companyType == .individual ? "individual" : "multiPartners",
            "bankAccountType": bankAccountType == .company ? "company" : "personal",
            "ableToDigitalAccount": ableToDigitalAccount,
            "banks": banks.map {
                $0.toDictionary()
            },
            "feeFirstDay": feeFirstDay,
            "feeLastDay": feeLastDay,
            "feeDefaultDay": feeDefaultDay,
            "feeListItems": feeListItems
        ]
        
        return dictionary
    }
    
    func isCallVerificationEnabled() -> Bool {
        false
    }
    
    /// Return the delay to make varification through a call
    func callDelay() -> Int {
        0
    }
    
    /// Return the delay to make varification through sms
    func smsDelay() -> Int {
        0
    }
    
    /// Return the date of the last phone verification
    func phoneVerificationDate() -> NSDate {
        NSDate()
    }
    
    /// Finish the phone step
    func finishPhoneStep(callback:@escaping (_ error: LegacyPJError?) -> Void ) {
        let completePhone = account.ddd + account.phone
        
        apiSignUp.verifyMobilePhone(phone: completePhone) { [weak self] _, error in
            if self?.dependencies.featureManager.isActive(.isAppLocalDataUnavailable) == true {
                self?.dependencies.kvStore.setString(completePhone, with: BizKvKey.phoneValidationNumber)
            } else {
                AppManager.shared.localData.phoneValidationNumber = completePhone
            }
            
            callback(error)
        }
    }
    
    /// Finish the cnpj step
    func finishCnpjStep(callback:@escaping (_ error: LegacyPJError?) -> Void ) {
        apiSignUp.validateCNPJ(cnpj: account.cnpj) { [weak self] error in
            self?.handleCNPJValidation(error, callback: callback)
        }
        
        // Try to load the list
        loadFeeList { _ in }
        loadBankList { }
    }
    
    /// Finish the phone step
    func requestCallToPhone(callback:@escaping (_ error: LegacyPJError?) -> Void ) {}
    
    /// Finish the code verification step
    func finishVerifyPhoneStep(callback:@escaping (_ error: LegacyPJError?) -> Void ) {
        let phone: String?
        if dependencies.featureManager.isActive(.isAppLocalDataUnavailable) {
            phone = dependencies.kvStore.stringFor(BizKvKey.phoneValidationNumber)
        } else {
            phone = AppManager.shared.localData.phoneValidationNumber
        }
        apiSignUp.verifyMobilePhoneCode(code: account.code, phone: phone ?? "") { error in
            callback(error)
        }
    }
    
    /// Load the address for cep
    /// Obs.: the callback will be called in the main thread
    func loadAddressByCep(cep: String, callback: @escaping (_ address: Address?, _ error: LegacyPJError?) -> Void) {
        apiSignUp.verifyCEP(cep: cep) { address, error in
            DispatchQueue.main.async {
                callback(address, error)
            }
        }
    }
    
    /// Finish the address step
    /// Obs.: the callback will be called in the main thread
    func finishAddressStep(callback: @escaping (_ error: LegacyPJError?) -> Void) {
        let address = SignUpAddress(
            cep: account.cep,
            address: account.address,
            number: account.addressNumber,
            district: account.district,
            city: account.city,
            state: account.state
        )
        
        apiSignUp.mapCoordinate(address: address, { [weak self] lat, lon, _ in
            DispatchQueue.main.async {
                if let lat = lat {
                    self?.account.lat = lat
                }
                if let lon = lon {
                    self?.account.lon = lon
                }
                
                callback(nil)
            }
        })
    }
    
    /// Finish the image step
    /// Obs.: the callback will be called in the main thread
    func finishImageStep(callback: @escaping (_ error: LegacyPJError?) -> Void) {
        callback(nil)
        
        // upload is performed in background
        if let imageData = account.image {
            let deviceToken: String?

            if dependencies.featureManager.isActive(.isAppLocalDataUnavailable) {
                deviceToken = dependencies.kvStore.stringFor(BizKvKey.deviceToken)
            } else {
                deviceToken = AppManager.shared.localData.deviceToken
            }
            apiSignUp.uploadLogoImage(cnpj: account.cnpj, deviceToken: deviceToken ?? "", image: imageData, uploadProgress: nil, { [weak self] imageUrl, _ in
                if let url = imageUrl {
                    self?.account.imageUrl = url
                    self?.account.image = nil
                }
            })
        }
    }
    
    /// Finish the bank step
    /// Obs.: the callback will be called in the main thread
    func finishBankStep(callback: @escaping (_ error: LegacyPJError?) -> Void) {
        if bankAccountType == .personal {
            verifyCpfAccountIntegrity(callback)
        } else {
            loadFeeList(callback: callback)
        }
    }
    
    /// Finish tax day
    /// Obs.: the callback will be called in the main thread
    func finishTaxDay(callback: @escaping (_ error: LegacyPJError?) -> Void) {
        callback(nil)
    }
    
    /// Finish the personal step
    /// Obs.: the callback will be called in the main thread
    func finishPersonalStep(callback: @escaping (_ error: LegacyPJError?) -> Void) {
        apiSignUp.verifyDuplicatedUser(cnpj: account.cnpj, cpf: account.partner.cpf, email: account.partner.email) { error in
            DispatchQueue.main.async {
                callback(error)
            }
        }
    }
    
    /// Finish registration
    func finishRegistration(callback: @escaping (_ error: LegacyPJError?) -> Void) {
        account.distinctId = Mixpanel.mainInstance().distinctId
        
        apiSignUp.createAccount(account) { [weak self] auth, error in
            guard let self = self else { return }
            
            guard let auth = auth else {
                DispatchQueue.main.async {
                    callback(error)
                }
                return
            }
            
            if self.dependencies.featureManager.isActive(.isAppLocalDataUnavailable) == true {
                self.dependencies.kvStore.setBool(true, with: BizKvKey.needShowStartPromoCodePopup)
            } else {
                AppManager.shared.localData.needShowStartPromoCodePopup = true
            }
            
            self.dependencies.authManager.authenticateNewAccount(auth, completion: { error in
                TrackingManager.setupMixpanel()
                DispatchQueue.main.async {
                    callback(error)
                }
            })
        }
    }
    
    /// Load the bank list
    func loadBankList(callback: @escaping() -> Void) {
        let api = ApiCommon()
        if banks.isNotEmpty {
            callback()
        } else {
            api.bankList { [weak self] result in
                switch result {
                case .success(let bankList):
                    self?.banks = bankList
                case .failure:
                    break
                }
                callback()
            }
            callback()
        }
    }
    
    /// Load the fee list
    func loadFeeList(callback: @escaping (_ error: LegacyPJError?) -> Void) {
        if feeListItems.isNotEmpty {
            callback(nil)
        } else {
            apiSignUp.feeList { [weak self] firstDay, lastDay, defaultDay, list, error in
                if let firstDay = firstDay {
                    self?.feeFirstDay = firstDay
                }
                
                if let feeLastDay = lastDay {
                    self?.feeLastDay = feeLastDay
                }
                
                if let defaultDay = defaultDay {
                    self?.feeDefaultDay = defaultDay
                }
                
                if let list = list {
                    self?.feeListItems = list
                }
                DispatchQueue.main.async {
                   callback(error)
                }
            }
        }
    }
    
    /// Get the fee for the day
    func feeByDay(day: String) -> String? {
        feeListItems[day]
    }
    
    /// Return the sms time delay do resend the verification code
    func smsDelayTime() -> String {
        let verificationSentDate = self.phoneVerificationDate()
        let smsDelay = self.smsDelay()
        
        var interval = Int(verificationSentDate.timeIntervalSinceNow)
        interval = smsDelay - (interval * -1)
        
        if interval > 0 {
            return self.timeFormatted(totalSeconds: interval)
        }
        
        return ""
    }
    
    /// Return the sms time delay do resend the verification code
    func callDelayTime() -> String {
        let verificationSentDate = self.phoneVerificationDate()
        let callDelay = self.callDelay()
        
        var interval = Int(verificationSentDate.timeIntervalSinceNow)
        interval = callDelay - (interval * -1)
        
        if interval > 0 {
            return self.timeFormatted(totalSeconds: interval)
        }
        
        return ""
    }
    
    func timeFormatted(totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        let hours: Int = totalSeconds / 3_600
        if hours > 0 {
            return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        } else {
            return String(format: "%02d:%02d", minutes, seconds)
        }
    }
    
    func setAddress(type: RegistrationType, address: [String: String]) {
        let cep = address["cep"] ?? ""
        let street = address["street"] ?? ""
        let city = address["city"] ?? ""
        let complement = address["complement"] ?? ""
        let district = address["district"] ?? ""
        let addressNumber = address["number"] ?? ""
        let state = address["state"] ?? ""
        
        switch type {
        case .company:
            account.cep = cep
            account.address = street
            account.city = city
            account.complement = complement
            account.district = district
            account.addressNumber = addressNumber
            account.state = state
        case .personal:
            account.partner.address.cep = cep
            account.partner.address.street = street
            account.partner.address.city = city
            account.partner.address.complement = complement
            account.partner.address.district = district
            account.partner.address.addressNumber = addressNumber
            account.partner.address.state = state
        }
    }
    
    func fetchBankAccountTypes(bankID: String, completion: @escaping(Error?) -> Void) {
        bankAccountTypeService.fetchBankAccountTypes(bankID: bankID, cnpj: account.cnpj) { [weak self] result in
            switch result {
            case .success(let list):
                self?.bankAccountTypes = list
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
}

// MARK: - Private Methods

private extension RegistrationViewModel {
    func checkCompanyType(_ completion: @escaping (_ error: LegacyPJError?) -> Void) {
        apiSignUp.verifySeller(account.cnpj) { [weak self] result in
            switch result {
            case .success(let companyInfo):
                guard let companyInfo = companyInfo, companyInfo.valid else {
                    completion(LegacyPJError.invalidCnpj)
                    return
                }
                self?.companyType = companyInfo.individual ? .individual : .multiPartners
                self?.ableToDigitalAccount = companyInfo.ableToDigitalAccount
                self?.account.termsAccepted = companyInfo.ableToDigitalAccount
                completion(nil)
            case .failure:
                completion(LegacyPJError.invalidCnpj)
            }
        }
    }
    
    func verifyCpfAccountIntegrity(_ completion: @escaping (_ error: LegacyPJError?) -> Void) {
        apiSignUp.verifyCPFIntegrity(account.cnpj, cpf: account.documentBank) { [weak self] error, _ in
            if let error = error {
                self?.dependencies.analytics.log(BankAccountAnalytics.invalidDocument())
                completion(error)
                return
            }
            self?.loadFeeList(callback: completion)
        }
    }
    
    func handleCNPJValidation(_ error: LegacyPJError?, callback: @escaping (_ error: LegacyPJError?) -> Void) {
        if let error = error {
            callback(error)
            return
        }
        
        apiSignUp.link(
            cnpj: account.cnpj,
            cpf: account.partner.cpf,
            name: account.partner.name
        ) { [weak self] error in
            self?.handleCNPJCPFLink(error, callback: callback)
        }
    }
    
    func handleCNPJCPFLink(_ error: LegacyPJError?, callback: @escaping(_ error: LegacyPJError?) -> Void) {
        DispatchQueue.main.async {
            if let error = error {
                callback(error)
            } else {
                self.checkCompanyType(callback)
            }
        }
    }
}
