import Foundation
import LegacyPJ

protocol PPCodeViewModel {
    var code: CodePicPay? { get set }
    
    // MARK: - Methods
    
    /// Load de ppcode
    func loadCodePicPay(_ completion: @escaping (( _ error: LegacyPJError?) -> Void))
    
    /// Check if the code is valid
    func codeIsValid() -> Bool
}

final class PPCodeImpViewModel: PPCodeViewModel {
    var code: CodePicPay?
    
    // MARK: Dependencies
    
    var apiSeller: ApiSeller
    
    init(apiSeller: ApiSeller = ApiSeller()) {
        self.apiSeller = apiSeller
    }
    
    // MARK: - Methods
    func loadCodePicPay(_ completion: @escaping (( _ error: LegacyPJError?) -> Void)) {
        apiSeller.ppcode { [weak self] code, error in
            if let code = code {
                self?.code = code
            }
            completion(error)
        }
    }
    
    /// Check if the code is valid
    func codeIsValid() -> Bool {
        guard let code = self.code
            else { return false }

        if code.expire >= Date() {
            return true
        }
        
        return false
    }
}
