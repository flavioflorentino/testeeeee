import Foundation
import LegacyPJ
import UIKit
import FeatureFlag

protocol LoginViewModel {
    // MARK: Properties
    var delegate: LoginViewModelDelegate? { get set }
    var username: String { get set }
    var password: String { get set }
    var cnpj: String { get set }
    
    // MARK: Methods
    func login()
    func validate(_ completion:@escaping ((_ error: LegacyPJError?) -> Void) )
    func completeSignUp()
}

protocol LoginViewModelDelegate: AnyObject {
    func loginDidFinish()
    func loginDidSuccess()
    func loginDidFail(error: LegacyPJError)
    func userNeedsCompleteAccount()
}

final class LoginImpViewModel: LoginViewModel {
    // MARK: Properties
    weak var delegate: LoginViewModelDelegate?
    
    var username: String = ""
    var password: String = ""
    var cnpj: String = ""
    
    // MARK: Dependencies
    
    var apiAuth: ApiAuth
    
    init(apiAuth: ApiAuth = ApiAuth()) {
        self.apiAuth = apiAuth
    }
    
    // MARK: Methods
    
    func login() {
        AuthManager.shared.login(cnpj: cnpj, username: username, password: password) { error in
            DispatchQueue.main.async {
                self.delegate?.loginDidFinish()
                
                if let error = error {
                    self.delegate?.loginDidFail(error: error)
                } else if let user = AuthManager.shared.userAuth {
                    self.redirect(user: user)
                }
            }
        }
    }
    
    func redirect(user: UserAuth) {
        if user.auth.completed {
            delegate?.loginDidSuccess()
        } else {
            delegate?.userNeedsCompleteAccount()
            AuthManager.shared.removeUserAuth(userAuth: user)
        }
    }
    
    func validate(_ completion:@escaping ((_ error: LegacyPJError?) -> Void) ) {
        apiAuth.validate(cnpj: cnpj) { error in
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
    
    func completeSignUp() {
        guard let url = URL(string: FeatureManager.shared.text(.urlCompleteAccount)) else {
            return
        }
        UIApplication.shared.open(url)
    }
}
