import Foundation
import LegacyPJ

struct SignUpAccount {
    // form data
    var ddd: String = ""
    var phone: String = ""
    var code: String = ""
    var mobilePhone: String = ""
    var mobilePhoneDdd: String = ""
    
    var cnpj: String = ""
    var companyName: String = ""
    
    var cep: String = ""
    var address: String = ""
    var addressNumber: String = ""
    var complement: String = ""
    var district: String = ""
    var city: String = ""
    var state: String = ""
    var lat: Double = 0.0
    var lon: Double = 0.0
    
    var image: Data?
    var imageUrl: String = ""
    var displayName: String = ""
    
    var bank: Bank?
    var days: Int = 0
    
    var branch: String = ""
    var branchDigit: String = ""
    var account: String = ""
    var accountDigit: String = ""
    var legacyAccountType: LegacyBankAccountType = .checking
    var accountOperation: String = ""
    
    var partner = Partner()
    
    var password: String = ""
    var distinctId: String = ""
    var documentBank: String = ""

    var termsAccepted: Bool = false
    var withoutAddress: Bool = false
    
    // New values to replace legacyAccountType and accountOperation
    var accountValue: BankAccountType.Value?
    var accountType: BankAccountType.OwnerType?
}

extension SignUpAccount {
    func toDictionary() -> [String: Any] {
        var dictionary: [String: Any] = [
            "ddd": ddd,
            "phone": phone,
            "code": code,
            "mobilePhone": mobilePhone,
            "mobilePhoneDdd": mobilePhoneDdd,
            "cnpj": cnpj,
            "companyName": companyName,
            "cep": cep,
            "address": address,
            "addressNumber": addressNumber,
            "complement": complement,
            "district": district,
            "city": city,
            "state": state,
            "lat": lat,
            "lon": lon,
            "image": image?.base64EncodedString() ?? Data().base64EncodedString(),
            "imageUrl": imageUrl,
            "displayName": displayName,
            "bank": bank?.toDictionary() ?? Bank("", "", "", "").toDictionary(),
            "days": days,
            "branch": branch,
            "branchDigit": branchDigit,
            "account": account,
            "accountDigit": accountDigit,
            "accountType": legacyAccountType.rawValue,
            "accountOperation": accountOperation,
            "partner": partner.toDictionary(),
            "password": password,
            "distinctId": distinctId,
            "documentBank": documentBank,
            "termsAccepted": termsAccepted,
            "without_address": withoutAddress
        ]
        
        if let accountValue = accountValue, let accountType = accountType {
            dictionary["value"] = accountValue.rawValue
            dictionary["type"] = accountType.rawValue
        }
        
        return dictionary
    }
}

extension SignUpAccount {
    var confirmationItem: AccountConfirmationItem {
        let documentType: AccountConfirmationItem.DocumentType = documentBank.onlyNumbers.count > 11 ? .cnpj : .cpf
        let accountType = documentType == .cnpj ? 
            Strings.BankAccountConfirmation.companyAccount : Strings.BankAccountConfirmation.personalAccount
        let type: String

        switch self.legacyAccountType {
        case .checking:
            type = "\(Strings.Default.currentAccount) \(accountType)"
        case .savings:
            type = "\(Strings.Default.savingAccount) \(accountType)"
        }

        return AccountConfirmationItem(
            imageUrl: bank?.image ?? "", 
            bank: bank?.name ?? "",
            branch: branchDigit.isEmpty ? branch : "\(branch)-\(branchDigit)",
            account: "\(account)-\(accountDigit)", 
            document: documentBank,
            documentType: documentType, 
            type: type
        )
    }
}
