import Foundation
import LegacyPJ
import SwiftyJSON

struct Seller: Equatable {
    var id = 0
    var name: String?
    var razaoSocial: String?
    var nomeFantasia: String?
    var email: String?
    var cnpj: String?
    var imageUrl: String?
    var daysToReleaseCc: Int?
    var store: Store?
    var automaticWithdrawal = false
    var digitalAccount = false
    var biometry = false
    var renewalRequired = false
    
    init() { }
    
    init?(json: JSON) {
        guard
            let id = json["id"].int,
            let name = json["name"].string
            else { return nil }
        
        self.id = id
        self.name = name
        
        if let razaoSocial = json["razaoSocial"].string {
            self.razaoSocial = razaoSocial
        }
        
        if let nomeFantasia = json["nomeFantasia"].string {
            self.nomeFantasia = nomeFantasia
        }
        
        if let email = json["email"].string {
            self.email = email
        }
        
        if let image = json["img_url"].string {
            self.imageUrl = image
        }
        
        if let cnpj = json["cpf_cnpj"].string {
            self.cnpj = cnpj
        }
        
        if let receiveDay = json["plan"]["days_to_release_cc"].int {
            self.daysToReleaseCc = receiveDay
        }

        if let digitalAccount = json["terms_accepted"].bool {
            self.digitalAccount = digitalAccount
        }
        
        automaticWithdrawal = json["automatic_withdraw"].bool ?? false
        biometry = json["biometry"].bool ?? false
        
        if let renewalJson = json["renovation_history"].dictionary {
            renewalRequired = renewalJson["need_update"]?.bool ?? false
        }
        
        self.store = Store(json: json["store"])
    }
    
    func toDictionary() -> [String: Any] {
        var p: [String: Any] = [:]
        if id != 0 {
            p["id"] = id
        }
        
        if let name = self.name {
            p["name"] = name
        }
        if let razaoSocial = self.razaoSocial {
            p["razaoSocial"] = razaoSocial
        }
        if let nomeFantasia = self.nomeFantasia {
            p["nomeFantasia"] = nomeFantasia
        }
        if let email = self.email {
            p["email"] = email
        }
        if let cpfcnpj = self.cnpj {
            p["cpf_cnpj"] = cpfcnpj
        }
        if let image = self.imageUrl {
            p["img_url"] = image
        }
        if let receiveDay = self.daysToReleaseCc {
            p["plan"] = ["days_to_release_cc": receiveDay]
        }
        if let store = self.store {
            p["store"] = store.toDictionary()
        }
        
        return p
    }
}

struct CompanyInfo: Decodable, Equatable {
    let valid: Bool
    let individual: Bool
    let ableToDigitalAccount: Bool

    private enum CodingKeys: String, CodingKey {
        case valid, individual
        case ableToDigitalAccount = "able_to_digital_account"
    }
}
