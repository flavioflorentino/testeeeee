import UI
import UIKit

@IBDesignable
class UIPPButton: UIButton {
    // MARK: - Private Properties
    
    private var activituIndicator: UIActivityIndicatorView?
    private var tempTitle: String?
    private var tempAttributedTitle: NSAttributedString?
    
    // MARK: - Designable
    
    @IBInspectable var type: Int = 0
    
    @IBInspectable var borderColor: UIColor = Colors.branding500.color
    @IBInspectable var borderWidth: CGFloat = 1
    @IBInspectable var cornerRadius: CGFloat = 7
    
    @IBInspectable var leftIconPadding: CGFloat = 20.0
    
    @IBInspectable var tintImage: Bool = false
    
    @IBInspectable var normalTitleColor: UIColor = Colors.branding500.color
    @IBInspectable var normalBackgrounColor: UIColor = Colors.white.color
    
    @IBInspectable var disabledTitleColor: UIColor = Colors.branding500.color
    @IBInspectable var disabledBackgrounColor: UIColor = Colors.white.color
    @IBInspectable var disabledBorderColor: UIColor = Colors.white.color
    
    @IBInspectable var highlightedImage: String = ""
    @IBInspectable var highlightedTitleColor: UIColor = Colors.white.color
    @IBInspectable var highlightedBackgrounColor: UIColor = Colors.branding500.color
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Public Methods
    
    func startLoadingAnimating(style: UIActivityIndicatorView.Style = .white) {
        activituIndicator?.removeFromSuperview()
        
        let activityIndicator = UIActivityIndicatorView(style: style)
        activityIndicator.center = CGPoint(x: frame.width / 2, y: frame.height / 2 )
        self.activituIndicator = activityIndicator
        addSubview(activityIndicator)
        activityIndicator.startAnimating()
        tempTitle = title(for: state)
        tempAttributedTitle = attributedTitle(for: state)
        
        setTitle("", for: state)
        setAttributedTitle(nil, for: state)
    }
    
    func stopLoadingAnimating() {
        activituIndicator?.removeFromSuperview()
        activituIndicator = nil
        setTitle(tempTitle, for: state)
        setAttributedTitle(tempAttributedTitle, for: state)
    }
    
    // MARK: - Layout Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if tintImage {
            self.adjustsImageWhenHighlighted = false
            if let image = self.imageView?.image {
                self.setImage(image.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: UIControl.State.normal)
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        
        guard var frame = imageView?.frame else { return }
        frame.origin.x = leftIconPadding
        imageView?.frame = frame
        
        switch type {
        case 0:
            configureDefaultType()
        case 1:
            configureCustomType()
        default:
            break
        }
    }
    
    private func configureDefaultType() {
        if self.state == UIControl.State.normal {
            if buttonType == .custom {
                titleLabel?.textColor = normalTitleColor
            } else {
                tintColor = normalTitleColor
            }
            self.backgroundColor = normalBackgrounColor
            if tintImage {
                self.imageView?.tintColor = normalTitleColor
            }
        }
        if self.state == UIControl.State.highlighted {
            if !highlightedImage.isEmpty {
                self.setImage(UIImage(named: highlightedImage), for: UIControl.State.highlighted)
            }
            self.titleLabel?.textColor = highlightedTitleColor
            self.backgroundColor = highlightedBackgrounColor
            if tintImage {
                self.imageView?.tintColor = highlightedTitleColor
            }
        }
        if self.state == UIControl.State.disabled {
            self.titleLabel?.textColor = disabledTitleColor
            self.layer.borderColor = disabledBorderColor.cgColor
            self.backgroundColor = disabledBackgrounColor
            if tintImage {
                self.imageView?.tintColor = disabledTitleColor
            }
        }
    }
    
    private func configureCustomType() {
        if self.state == UIControl.State.highlighted {
            self.titleLabel?.textColor = normalTitleColor
            self.backgroundColor = normalBackgrounColor
            if tintImage {
                self.imageView?.tintColor = normalTitleColor
            }
        }
        if self.state == UIControl.State.normal {
            if !highlightedImage.isEmpty {
                self.setImage(UIImage(named: highlightedImage), for: UIControl.State.highlighted)
            }
            self.titleLabel?.textColor = highlightedTitleColor
            self.backgroundColor = highlightedBackgrounColor
            if tintImage {
                self.imageView?.tintColor = highlightedTitleColor
            }
        }
        if self.state == UIControl.State.disabled {
            self.titleLabel?.textColor = disabledTitleColor
            self.layer.borderColor = disabledBorderColor.cgColor
            self.backgroundColor = disabledBackgrounColor
            if tintImage {
                self.imageView?.tintColor = disabledTitleColor
            }
        }
    }
}
