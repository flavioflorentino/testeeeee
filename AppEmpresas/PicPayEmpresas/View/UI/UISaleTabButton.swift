import AssetsKit
import SnapKit
import UI
import UIKit
import FeatureFlag

final class UISaleTabButton: UIButton {
    typealias Dependencies = HasFeatureManager
    
    private let startColor: UIColor
    private let endColor: UIColor
    private let startPoint: CGPoint
    private let endPoint: CGPoint
    
    private let selectedBackground = Colors.branding900.color
    private let unselectedBackground = Colors.branding600.color
    private let container: Dependencies
    
    private lazy var gradientLayer: CAGradientLayer? = {
        if container.featureManager.isActive(.releaseNewHome) {
            return nil
        }
        
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [
            startColor.cgColor,
            endColor.cgColor
        ]
        gradient.startPoint = startPoint
        gradient.endPoint = endPoint
        return gradient
    }()
    
    private lazy var saleImage: UIImageView = {
        let image: UIImage
        if container.featureManager.isActive(.releaseNewHome) {
            image = Resources.Icons.icoPjTabBarReceive.image
        } else {
            image = Assets.icoTabReceive.image
        }
        let view = UIImageView(image: image)
        return view
    }()
    
    init(
        startColor: UIColor = Palette.Business.backgroundDefault.gradientColor.from,
        endColor: UIColor = Palette.Business.backgroundDefault.gradientColor.to,
        startPoint: CGPoint = CGPoint(x: 0.0, y: 0.5),
        endPoint: CGPoint = CGPoint(x: 1.0, y: 0.5),
        container: Dependencies = DependencyContainer()
    ) {
        self.startColor = startColor
        self.endColor = endColor
        self.startPoint = startPoint
        self.endPoint = endPoint
        self.container = container
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let gradientLayer = gradientLayer {
            gradientLayer.frame = bounds
        }
        
        layer.cornerRadius = frame.size.height * 0.5
    }
    
    func setBackgroundStatus(selected: Bool) {
        guard container.featureManager.isActive(.releaseNewHome) else {
            setupLegacyBackgroundStatus(selected: selected)
            return
        }
        
        backgroundColor = selected ? selectedBackground : unselectedBackground
    }
    
    func setupLegacyBackgroundStatus(selected: Bool) {
        guard let gradientLayer = gradientLayer else { return }
        
        guard selected else {
            layer.insertSublayer(gradientLayer, at: 0)
            return
        }
        
        backgroundColor = Colors.branding700.color
        gradientLayer.removeFromSuperlayer()
    }
}

// MARK: - ViewConfiguration

extension UISaleTabButton: ViewConfiguration {
    func configureViews() {
        layer.masksToBounds = true
        layer.cornerRadius = frame.size.height * 0.5
        
        if container.featureManager.isActive(.releaseNewHome) {
            backgroundColor = Colors.branding600.color
        }
    }
    
    func buildViewHierarchy() {
        if let gradientLayer = gradientLayer {
            layer.addSublayer(gradientLayer)
        }
        
        addSubview(saleImage)
    }
    
    func setupConstraints() {
        saleImage.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
}
