import UI
import UIKit

@IBDesignable
final class UIRoundedTextField: UITextField {
    typealias InputType = String
    
    /**
     It stylizes the text field as error or not
     */
    var hasError: Bool = false {
        didSet {
            if hasError == true {
                self.layer.borderColor = #colorLiteral(red: 0.9294117647, green: 0.09411764706, blue: 0.2745098039, alpha: 1)
                self.layer.borderWidth = 1.0
            } else {
                self.layer.borderColor = backgroundColor?.cgColor
                self.layer.borderWidth = 1.0
            }
        }
    }
    
    var maskPattern: String?
    
    @IBInspectable var inset: CGFloat = 0
    @IBInspectable var insetLeft: CGFloat = 15
    @IBInspectable var insetRight: CGFloat = 15
    @IBInspectable var insetTop: CGFloat = 0
    @IBInspectable var insetBottom: CGFloat = 0
    @IBInspectable var cornerRadius: CGFloat = 20.0
    
    @IBInspectable var placeHolderTextColor: UIColor? {
        didSet {
            let placeholderText = placeholder ?? ""
            if let color = placeHolderTextColor {
                attributedPlaceholder = NSAttributedString(
                    string: placeholderText,
                    attributes: [NSAttributedString.Key.foregroundColor: color]
                )
            }
        }
    }
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.comomInit()
	}
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
		self.comomInit()
    }
	
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        bounds.inset(by: UIEdgeInsets(top: insetTop, left: insetLeft, bottom: insetBottom, right: insetRight))
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        bounds.inset(by: UIEdgeInsets(top: insetTop, left: insetLeft, bottom: insetBottom, right: insetRight))
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        bounds.inset(by: UIEdgeInsets(top: insetTop, left: insetLeft, bottom: insetBottom, right: insetRight))
    }
}

private extension UIRoundedTextField {
    func comomInit() {
        borderStyle = .none
        
        if backgroundColor == nil {
            backgroundColor = Colors.white.color
        }
        
        layer.borderColor = backgroundColor?.cgColor
        layer.cornerRadius = cornerRadius
        layer.borderWidth = 1.0
        layer.masksToBounds = true
        
        frame = frame.inset(by: UIEdgeInsets(top: insetTop, left: insetLeft, bottom: insetBottom, right: insetRight))
    }
}
