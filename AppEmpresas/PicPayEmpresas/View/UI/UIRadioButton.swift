import Foundation
import UI
import UIKit

@IBDesignable
final class UIRadioButton: UIButton {
    var outerCircleLayer = CAShapeLayer()
    var innerCircleLayer = CAShapeLayer()
    var setCircleRadius: CGFloat {
        let width = bounds.width
        let height = bounds.height
        
        let length = width > height ? height : width
        return (length - outerCircleLineWidth) / 2
    }
    
    private var setCircleFrame: CGRect {
        let width = bounds.width
        let height = bounds.height
        
        let radius = setCircleRadius
        let x: CGFloat
        let y: CGFloat
        
        if width > height {
            y = outerCircleLineWidth / 2
            x = (width / 2) - radius
        } else {
            x = outerCircleLineWidth / 2
            y = (height / 2) - radius
        }
        
        let diameter = 2 * radius
        return CGRect(x: x, y: y, width: diameter, height: diameter)
    }
    
    private var circlePath: UIBezierPath {
        UIBezierPath(roundedRect: setCircleFrame, cornerRadius: setCircleRadius)
    }
    
    private var fillCirclePath: UIBezierPath {
        let trueGap = innerCircleGap + (outerCircleLineWidth / 2)
        return UIBezierPath(roundedRect: setCircleFrame.insetBy(dx: trueGap, dy: trueGap), cornerRadius: setCircleRadius)
    }
    
    override var isSelected: Bool {
        didSet {
            setFillState()
        }
    }
    
    @IBInspectable var activeColor: UIColor = Colors.branding500.color {
        didSet {
            outerCircleLayer.strokeColor = activeColor.cgColor
            setFillState()
        }
    }
    @IBInspectable var inactiveColor: UIColor = Colors.grayscale600.color {
        didSet {
            outerCircleLayer.strokeColor = inactiveColor.cgColor
            setFillState()
        }
    }
    
    @IBInspectable var outerCircleLineWidth: CGFloat = 2.0 {
        didSet {
            setCircleLayouts()
        }
    }
    @IBInspectable var innerCircleGap: CGFloat = 3.0 {
        didSet {
            setCircleLayouts()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInitialization()
    }
    
    // MARK: Initialization
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInitialization()
    }
    
    private func customInitialization() {
        outerCircleLayer.frame = bounds
        outerCircleLayer.lineWidth = outerCircleLineWidth
        outerCircleLayer.fillColor = UIColor.clear.cgColor
        outerCircleLayer.strokeColor = inactiveColor.cgColor
        layer.addSublayer(outerCircleLayer)
        
        innerCircleLayer.frame = bounds
        innerCircleLayer.lineWidth = outerCircleLineWidth
        innerCircleLayer.fillColor = UIColor.clear.cgColor
        innerCircleLayer.strokeColor = UIColor.clear.cgColor
        layer.addSublayer(innerCircleLayer)
        
        setFillState()
    }
    
    private func setCircleLayouts() {
        outerCircleLayer.frame = bounds
        outerCircleLayer.lineWidth = outerCircleLineWidth
        outerCircleLayer.path = circlePath.cgPath
        
        innerCircleLayer.frame = bounds
        innerCircleLayer.lineWidth = outerCircleLineWidth
        innerCircleLayer.path = fillCirclePath.cgPath
    }
    
    // MARK: Custom
    private func setFillState() {
        if self.isSelected {
            outerCircleLayer.strokeColor = activeColor.cgColor
            innerCircleLayer.fillColor = activeColor.cgColor
        } else {
            outerCircleLayer.strokeColor = inactiveColor.cgColor
            innerCircleLayer.fillColor = UIColor.clear.cgColor
        }
    }
    // Overriden methods.
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        customInitialization()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setCircleLayouts()
    }
}
