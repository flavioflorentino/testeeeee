import UIKit

final class LoadingHeaderView: UIView {
    var activityIndicator: UIActivityIndicatorView
    
    override init(frame: CGRect) {
        activityIndicator = UIActivityIndicatorView(style: .gray)
        
        super.init(frame: frame)
    
        var f = activityIndicator.frame
        f.origin.x = (frame.size.width / 2) - (f.size.width / 2)
        f.origin.y = (frame.size.height / 2) - (f.size.height / 2)
        activityIndicator.frame = f
        
        self.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    convenience init(autoLayout: () -> Void) {
        self.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
