import UIKit

final class OnboardView: NibView {
    private var action: (() -> Void)?
    
    @IBOutlet var ilustrationImage: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var actionButton: UIPPButton!
    
    @objc
    func didTapAction() {
        action?()
    }
    
    func setupActionButton(title: String, action: @escaping () -> Void) {
        self.actionButton.setTitle(title, for: .normal)
        self.actionButton.isHidden = false
        self.actionButton.addTarget(self, action: #selector(OnboardView.didTapAction), for: .touchUpInside)
        self.action = action
    }
}
