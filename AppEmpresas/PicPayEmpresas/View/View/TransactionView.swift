import LegacyPJ
import SDWebImage
import UIKit

final class TransactionView: NibView {
    var activityIndicator: UIActivityIndicatorView?
    var overlay: UIView?

    // MARK: IB Outlet
    
    @IBOutlet var photoView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var dateTimeLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    @IBOutlet var valueDetailLabel: UILabel!
    @IBOutlet var valueDetailHeight: NSLayoutConstraint!
    @IBOutlet var operatorLabel: UILabel!
    @IBOutlet var operatorHeight: NSLayoutConstraint!
        
    // MARK: Public Methods
    
    func configure(_ transaction: Transaction) {
        photoView.layer.cornerRadius = 20.0
        photoView.clipsToBounds = true
        
        // data config
        photoView.image = nil
        if let consumer = transaction.consumer {
            nameLabel.text = consumer.username
            photoView.sd_setImage(with: URL(string: consumer.imageUrl))
        }
        
        dateTimeLabel.text = transaction.date
        valueLabel.text = transaction.siteValue
        
        if let name = transaction.sellerOperator?.username {
            operatorLabel.text = "Recebido por \(name)"
            operatorHeight.constant = 14
        } else {
            operatorLabel.text = ""
            operatorHeight.constant = 0
        }
        
        switch transaction.status {
        case .canceled:
            valueLabel.textColor = UIColor.lightGray
            valueDetailLabel.text = "Valor devolvido"
            valueDetailHeight.constant = 12
        default:
            valueDetailLabel.text = ""
            valueDetailHeight.constant = 0
        }
        
        self.layoutIfNeeded()
    }
}
