import UIKit

protocol TransactionEmptyListViewDelegate: AnyObject {
    func transactionEmptyListViewDidTapActionButton()
}

final class TransactionEmptyListView: NibView {
    weak var delegate: TransactionEmptyListViewDelegate?
    @IBOutlet var actionButton: UIButton!
    
    // MARK: - Initializer
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        actionButton.isHidden = true
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - User Actions
    
    @IBAction func action(_ sender: Any) {
        delegate?.transactionEmptyListViewDidTapActionButton()
    }
    
    // MARK: - Public Methods
    func setActionButtonTitle(title: String) {
        let attr = NSMutableAttributedString(string: title)
        actionButton.setAttributedTitle(attr, for: .normal)
    }
}
