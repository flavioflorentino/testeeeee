import LegacyPJ
import UIKit

final class MovementItemTableViewCell: UITableViewCell {
    // MARK: - Private methods
    enum DetailStyle {
        case balance
        case normal
    }

    static let rowHeight = 25.0
    static let balanceRowHeight = 35.0
    
    var detailsView: UIView?
    
    // MARK: - IB Outlet
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    @IBOutlet var iconImage: UIImageView!
    
    // MARK: - Public Methods
    static func calculatedHeight(_ item: MovementItemRow) -> CGFloat {
        var height = 44.0 // Closed size
        if item.expanded {
            height += rowHeight * Double(item.details.count)
            height += 10.0 // Padding
            height += balanceRowHeight // Last line (Balance)
        }
        return CGFloat(height)
    }
    
    func configureCell(_ item: MovementItemRow) {
        setNeedsLayout()
        
        // appearance
        separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        selectionStyle = .none
        
        // configure data
        dateLabel.text = item.label
        valueLabel.text = item.balanceValue
        
        // configure details view
        detailsView?.removeFromSuperview()
        let detailsView = UIView()
        self.detailsView = detailsView
        detailsView.backgroundColor = #colorLiteral(red: 0.91, green: 0.92, blue: 0.93, alpha: 1.00)
        
        if item.expanded { 
            // details view
            
            let totalDetails = item.details.count
            var height = Double(totalDetails) * MovementItemTableViewCell.rowHeight
            height += 10 // pading
            height += MovementItemTableViewCell.balanceRowHeight
            detailsView.frame = CGRect(x: 0, y: 44, width: UIScreen.main.bounds.size.width, height: CGFloat(height))
            
            for (i, detail) in item.details.enumerated() {
                addDetail(detail.name, value: detail.value, position: i)
            }
            
            addDetail(item.balanceLabel, value: item.balanceValue, position: item.details.count, style: .balance)
            
            addSubview(detailsView)
            
            // change ico
            iconImage.image = Assets.icoCircleMinus.image
        } else {
            detailsView.removeFromSuperview()
            iconImage.image = Assets.icoCirclePlus.image
        }
        
        setNeedsLayout()
    }
    
    private func addDetail(_ text: String, value: String, position: Int, style: DetailStyle = .normal) {
        let screenWidth = UIScreen.main.bounds.size.width
        
        switch style {
        case .normal:
            var y = MovementItemTableViewCell.rowHeight * Double(position)
            y += 5 // top padding
            
            // Label
            let label = UILabel(frame: CGRect(x: 20, y: CGFloat(y), width: screenWidth, height: CGFloat(MovementItemTableViewCell.rowHeight) ))
            label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.medium)
            label.text = text
            label.textColor = #colorLiteral(red: 0.4784313725, green: 0.4784313725, blue: 0.4901960784, alpha: 1)
            detailsView?.addSubview(label)
            
            // Label
            let valueLabel = UILabel(frame: CGRect(x: 20, y: CGFloat(y), width: screenWidth - 40.0, height: CGFloat(MovementItemTableViewCell.rowHeight) ))
            valueLabel.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.medium)
            valueLabel.text = value
            valueLabel.textColor = #colorLiteral(red: 0.48, green: 0.48, blue: 0.49, alpha: 1.00)
            valueLabel.textAlignment = .right
            detailsView?.addSubview(valueLabel)
            
        case .balance:
            var y = MovementItemTableViewCell.rowHeight * Double(position)
            y += 10 // top padding
            
            let line = UIView(frame: CGRect(x: 20, y: CGFloat(y), width: screenWidth - 40.0, height: 1 / UIScreen.main.scale))
            line.backgroundColor = #colorLiteral(red: 0.48, green: 0.48, blue: 0.49, alpha: 1.00)
            detailsView?.addSubview(line)
            
            // Label
            let label = UILabel(frame: CGRect(x: 20, y: CGFloat(y), width: screenWidth - 40.0, height: CGFloat(MovementItemTableViewCell.balanceRowHeight) ))
            label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)
            label.text = text
            label.textColor = #colorLiteral(red: 0.48, green: 0.48, blue: 0.49, alpha: 1.00)
            detailsView?.addSubview(label)
            
            // Label
            let valueLabel = UILabel(frame: CGRect(x: 20, y: CGFloat(y), width: screenWidth - 40.0, height: CGFloat(MovementItemTableViewCell.balanceRowHeight) ))
            valueLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)
            valueLabel.text = value
            valueLabel.textColor = #colorLiteral(red: 0.48, green: 0.48, blue: 0.49, alpha: 1.00)
            valueLabel.textAlignment = .right
            detailsView?.addSubview(valueLabel)
        }
    }
}
