import LegacyPJ
import SDWebImage
import UIKit

final class BankTableViewCell: UITableViewCell {
    @IBOutlet var bankNameLabel: UILabel!
    @IBOutlet var bankImage: UIImageView!
    
    func configureCell(_ bank: Bank) {
        bankNameLabel.text = "\(bank.code) - \(bank.name)"
        if let imageUrl = bank.image {
            bankImage.sd_setImage(with: URL(string: imageUrl))
        } else {
            bankImage.image = Assets.icoBankPlaceholder.image
        }
        
        bankImage.layer.cornerRadius = 15.0
        bankImage.clipsToBounds = true
    }
}
