import UIKit

final class SettingsLogoutTableViewCell: UITableViewCell {
    @IBOutlet var button: UIButton!
    
    func configureCell(_ row: SettingsRow) {
        separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        button.setTitle(row.title, for: .normal)
        button.isEnabled = false
    }
}
