import UIKit

final class SettingsDetailTableViewCell: SettingsTableViewCell {
    // MARK: IB Outlet
    
    @IBOutlet var detailLabel: UILabel!
    
    override func configureCell(_ row: SettingsRow) {
        descriptionLabel.text = row.title
        valueLabel.text = row.value
        detailLabel?.text = row.subtitle
    }
}
