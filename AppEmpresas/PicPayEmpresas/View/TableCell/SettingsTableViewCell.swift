import UIKit

class SettingsTableViewCell: UITableViewCell {
    // MARK: IB Outlet
    
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    @IBOutlet private var iconImageView: UIImageView?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        valueLabel.text = ""
        valueLabel.isHidden = true
        iconImageView?.image = nil
        iconImageView?.isHidden = true
    }
    
    func configureCell(_ row: SettingsRow) {
        descriptionLabel?.text = row.title
        
        if let value = row.value {
            valueLabel.text = value
            valueLabel.isHidden = false
        }
        
        if let icon = row.icon {
            iconImageView?.image = icon
            iconImageView?.isHidden = false
        }
    }
}
