import LegacyPJ
import UIKit

final class HistoryItemTableViewCell: UITableViewCell {
    // MARK: - IB Outlet
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    
    // MARK: - Public Methods
    
    func configureCell(_ item: HistoryItem) {
        // appearance
        separatorInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)

        // configure data
        dateLabel.text = item.date
        timeLabel.text = item.time
        valueLabel.text = item.totalReceived
    }
}
