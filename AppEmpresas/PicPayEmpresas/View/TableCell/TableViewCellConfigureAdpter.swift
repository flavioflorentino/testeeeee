import Foundation
import UIKit

protocol TableViewCellConfigureAdpter {
    func configureCellAdpter(cell: UITableViewCell, data: Any?)
}
