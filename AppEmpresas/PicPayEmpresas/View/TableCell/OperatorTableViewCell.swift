import LegacyPJ
import UIKit

final class OperatorTableViewCell: UITableViewCell {
    // MARK: IB Outlet
    
    @IBOutlet var photoView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    
    // MARK: Public Methods
    
    func configureCell(_ op: Operator) {
        // cel config
        self.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        photoView.layer.cornerRadius = 20.0
        photoView.clipsToBounds = true
        
        // data config
        if let imageUrl = op.imageUrl {
            photoView.sd_setImage(with: URL(string: imageUrl), placeholderImage: Assets.iluOperatorPlaceholder.image)
        } else {
           photoView.image = Assets.iluOperatorPlaceholder.image
        }
        
        if let name = op.username {
            nameLabel.text = name
        } else {
            nameLabel.text = ""
        }
        
        valueLabel.text = ""
        
        self.layoutIfNeeded()
    }
}
