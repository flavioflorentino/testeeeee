import UIKit

final class SettingsSwitchTableViewCell: UITableViewCell {
    typealias ChangeAction = ((_ cell: SettingsSwitchTableViewCell, _ configSwitch: UISwitch) -> Void)
    
    var changeAction: ChangeAction?
    
    // MARK: - IBOutlet
    
    @IBOutlet var configSwitch: UISwitch!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var configLabel: UILabel!
    
    // MARK: - Private Methos
    @objc
    private func changeSwitch(_ sender: UISwitch) {
        changeAction?(self, configSwitch)
    }
    
    // MARK: - Public Methods
    
    func configureCell(isOn: Bool, changeAction:@escaping ChangeAction) {
        self.changeAction = changeAction
        
        configSwitch.isOn = isOn
        configSwitch.addTarget(self, action: #selector(changeSwitch(_:)), for: .valueChanged)
    }
    
    func startLoadingAnimating() {
        configSwitch.isEnabled = false
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        UIView.animate(withDuration: 0.5) { 
            self.configSwitch.layer.opacity = 0.5
        }
    }
    
    func stopLoadingAnimating() {
        configSwitch.isEnabled = true
        activityIndicator.stopAnimating()
        UIView.animate(withDuration: 0.5) {
            self.configSwitch.layer.opacity = 1.0
        }
    }
}
