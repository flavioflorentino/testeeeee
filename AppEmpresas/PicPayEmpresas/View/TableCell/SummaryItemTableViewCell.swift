import LegacyPJ
import UIKit

final class SummaryItemTableViewCell: UITableViewCell {
    // MARK: - IB Outlet
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    
    // MARK: - Public Methods
    
    func configureCell(_ item: SummaryItem) {
        // appearance
        separatorInset = UIEdgeInsets.zero
        
        // configure data
        dateLabel.text = item.date
        valueLabel.text = item.totalReceived
    }
}
