import LegacyPJ
import SDWebImage
import UIKit

final class AccountTableViewCell: UITableViewCell {
    @IBOutlet var logoImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var detailLabel: UILabel!
    
    func configureCell(_ userAuth: UserAuth) {
        separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        logoImageView.layer.cornerRadius = 22.00
        logoImageView.layer.masksToBounds = true
        
        var name = userAuth.user.username ?? ""
        name = userAuth.user.name ?? name
        
        nameLabel.text = name
        
        if userAuth.user.isAdmin {
            detailLabel.text = userAuth.user.email ?? ""
        } else {
            detailLabel.text = "Operador"
        }
        
        if let imageUrl = userAuth.user.imageUrl, let url = URL(string: imageUrl) {
            logoImageView.sd_setImage(with: url, placeholderImage: Assets.iluOperatorPlaceholder.image)
        } else {
            logoImageView.image = Assets.iluOperatorPlaceholder.image
        }
    }
}
