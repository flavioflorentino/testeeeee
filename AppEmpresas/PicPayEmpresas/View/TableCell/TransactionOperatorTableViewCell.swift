import LegacyPJ
import UIKit

final class TransactionOperatorTableViewCell: TransactionTableViewCell {
    @IBOutlet var operatorNameLabel: UILabel!
 
    override func configureCell(_ transaction: Transaction) {
        super.configureCell(transaction)
        
        if let name = transaction.sellerOperator?.username {
            operatorNameLabel.text = "Recebido por \(name)"
        } else {
            operatorNameLabel.text = "-"
        }
    }
}
