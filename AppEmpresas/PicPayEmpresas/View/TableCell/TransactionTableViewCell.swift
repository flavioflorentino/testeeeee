import LegacyPJ
import SDWebImage
import UIKit

class TransactionTableViewCell: UITableViewCell {
    var activityIndicator: UIActivityIndicatorView?
    var overlay: UIView?
    
    // MARK: IB Outlet
    
    @IBOutlet var photoView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var dateTimeLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    @IBOutlet var valueDetailLabel: UILabel!
    @IBOutlet var valueDetailHeight: NSLayoutConstraint!
    
    // MARK: Public Methods
    
    func configureCell(_ transaction: Transaction) {
        clearCell()
        
        // cel config
        self.separatorInset = UIEdgeInsets.zero
        photoView.layer.cornerRadius = 20.0
        photoView.clipsToBounds = true
        
        selectionStyle = .none
        
        // data config
        if let consumer = transaction.consumer {
            nameLabel.text = consumer.username
            photoView.sd_setImage(with: URL(string: consumer.imageUrl), placeholderImage: Assets.iluPersonPlaceholder.image)
        } else {
            photoView.image = Assets.iluPersonPlaceholder.image
        }
        
        dateTimeLabel.text = transaction.date
        valueLabel.text = transaction.siteValue
        
        switch transaction.status {
        case .canceled:
            valueLabel.textColor = UIColor.lightGray
            valueDetailLabel.text = "Valor devolvido"
            valueDetailHeight.constant = 12
        default:
            valueDetailLabel.text = ""
            valueDetailHeight.constant = 0
        }
        
        self.layoutIfNeeded()
    }
    
    func startLoading() {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.center = CGPoint(x: (UIScreen.main.bounds.size.width / 2), y: frame.size.height / 2)
        self.activityIndicator = activityIndicator
        
        let overlay = UIView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
        overlay.backgroundColor = UIColor.white
        overlay.layer.opacity = 0.85
        overlay.addSubview(activityIndicator)
        self.overlay = overlay
        
        addSubview(overlay)
        activityIndicator.startAnimating()
    }
    
    func stopLoading() {
        self.overlay?.removeFromSuperview()
    }
    
    /// Clear cell for reuse
    func clearCell() {
        nameLabel.text = ""
        photoView.image = nil
        dateTimeLabel.text = ""
        valueLabel.text = ""
        valueDetailLabel.text = ""
        valueDetailHeight.constant = 0
        overlay?.removeFromSuperview()
    }
}
