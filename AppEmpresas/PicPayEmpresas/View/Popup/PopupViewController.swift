import SnapKit
import UI
import UIKit

protocol PopupPresentable: AnyObject {
    var popup: PopupViewController? { get set }
}

protocol PopupDelegate: AnyObject {
    func popUpDidClose()
}

private extension PopupViewController.Layout {
    enum PopUpView {
        static let cornerRadius = CornerRadius.strong
    }
    
    enum CloseButton {
        static let size = CGSize(width: 40, height: 40)
    }
}

final class PopupViewController: UIViewController {
    fileprivate struct Layout { }
    
    var contentController: UIViewController?
    var hasCloseButton = true
    weak var delegate: PopupDelegate?
    
    private lazy var backgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var popupView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Layout.PopUpView.cornerRadius
        view.backgroundColor = Colors.backgroundPrimary.color
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var closeButtonView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.iconPopupClose.image, for: .normal)
        button.addTarget(self, action: #selector(close(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var contentView = UIView()
    
    // MARK: - Inits
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        modalPresentationStyle = .overCurrentContext
        modalTransitionStyle = .crossDissolve
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        delegate?.popUpDidClose()
    }
}

// MARK: - ViewConfiguration

extension PopupViewController: ViewConfiguration {
    func configureViews() {
        view.backgroundColor = Colors.black.color.withAlphaComponent(0.5)
        setupViewTapGesture()
        
        if let contentController = contentController {
            embed(controller: contentController)
        }
        
        setupCloseButton()
        setupKeyboardObservers()
    }
    
    func setupConstraints() {
        backgroundView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        popupView.snp.makeConstraints {
            $0.leading.equalTo(backgroundView).offset(Spacing.base02)
            $0.trailing.equalTo(backgroundView).offset(-Spacing.base02)
            $0.centerY.equalTo(backgroundView)
        }
        
        closeButtonView.snp.makeConstraints {
            $0.top.leading.trailing.equalTo(popupView)
        }
        
        closeButton.snp.makeConstraints {
            $0.size.equalTo(Layout.CloseButton.size).priority(.medium)
            $0.top.trailing.bottom.equalTo(closeButtonView)
        }
        
        contentView.snp.makeConstraints {
            $0.top.equalTo(closeButtonView.snp.bottom)
            $0.leading.trailing.bottom.equalTo(popupView)
        }
    }
    
    func buildViewHierarchy() {
        closeButtonView.addSubview(closeButton)
        popupView.addSubview(closeButtonView)
        popupView.addSubview(contentView)
        backgroundView.addSubview(popupView)
        view.addSubview(backgroundView)
    }
}

// MARK: - Private Methods

private extension PopupViewController {
    func setupViewTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        tapGesture.delaysTouchesBegan = false
        tapGesture.delaysTouchesEnded = false
        view.addGestureRecognizer(tapGesture)
    }
    
    func embed(controller: UIViewController) {
        if let presentable = controller as? PopupPresentable {
            presentable.popup = self
        }
        
        addChild(controller)
        contentView.addSubview(controller.view)
        controller.view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        controller.didMove(toParent: self)
    }
    
    func setupCloseButton() {
        if !hasCloseButton {
            closeButton.snp.remakeConstraints {
                $0.height.equalTo(0)
            }
        }
    }
    
    func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillOpen),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    // MARK: - Keyboard Events Handler
    
    @objc
    func keyboardWillOpen(keyboardNotification: NSNotification) {
        let key = UIResponder.keyboardAnimationDurationUserInfoKey
        
        guard let animationNumber = keyboardNotification.userInfo?[key] as? NSNumber,
            let keyboardSize = keyboardNotification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect else {
            return
        }
        
        let animationDuration = animationNumber.doubleValue
        view.layoutIfNeeded()
        
        // workaround tabbar - 60pt
        let offset = keyboardSize.size.height - 60
        backgroundView.snp.remakeConstraints {
            $0.bottom.equalTo(view).offset(offset)
        }
        
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc
    func keyboardWillHide(keyboardNotification: NSNotification) {
        let key = UIResponder.keyboardAnimationDurationUserInfoKey
        
        guard let animationNumber = keyboardNotification.userInfo?[key] as? NSNumber else {
            return
        }
        
        let animationDuration = animationNumber.doubleValue
        view.layoutIfNeeded()
        
        backgroundView.snp.remakeConstraints {
            $0.bottom.equalTo(view)
        }
        
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc
    func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
