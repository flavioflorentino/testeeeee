import UI
import UIKit

extension Palette {
    enum Business {
        case backgroundDefault
        
        case bluishGray200
        case bluishGray400
        
        case branding700
        case greenBackgroudNotification
        
        case grayscale400
        case grayscale600
        
        case positive300
        
        var color: UIColor {
            switch self {
            case .positive300:
                return #colorLiteral(red: 0.1294117647, green: 0.7607843137, blue: 0.368627451, alpha: 1) // #21C25E
            case .grayscale600:
                return #colorLiteral(red: 0.02352941176, green: 0.09019607843, blue: 0.1019607843, alpha: 1) // #06171A
            case .grayscale400:
                return #colorLiteral(red: 0.5607843137, green: 0.6156862745, blue: 0.6156862745, alpha: 1) // #8F9D9D
            case .bluishGray200:
                return #colorLiteral(red: 0.768627451, green: 0.8235294118, blue: 0.8823529412, alpha: 1) // #8F9D9D
            case .bluishGray400:
                return #colorLiteral(red: 0.2980392157, green: 0.3764705882, blue: 0.4470588235, alpha: 1) // #8F9D9D
            case .branding700:
                return #colorLiteral(red: 0.1725490196, green: 0.431372549, blue: 0.4392156863, alpha: 1) // #2C6E70
            case .greenBackgroudNotification:
                return #colorLiteral(red: 0.9254901961, green: 0.9725490196, blue: 0.9529411765, alpha: 1) // #ECF8F3
            default:
                return .black
            }
        }
        
        var gradientColor: GradientBicolor {
            switch self {
            case .backgroundDefault:
                return GradientBicolor(from: #colorLiteral(red: 0.03921568627, green: 0.2078431373, blue: 0.2352941176, alpha: 1), to: #colorLiteral(red: 0.09411764706, green: 0.3960784314, blue: 0.4352941176, alpha: 1))
            default:
                return GradientBicolor(from: .black, to: .white)
            }
        }
    }
}
