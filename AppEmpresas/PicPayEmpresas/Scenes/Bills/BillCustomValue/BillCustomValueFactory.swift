enum BillCustomValueFactory {
    static func make(_ billet: BillPay) -> BillCustomValueViewController {
        let coordinator: BillCustomValueCoordinating = BillCustomValueCoordinator()
        let presenter: BillCustomValuePresenting = BillCustomValuePresenter(coordinator: coordinator)
        let viewModel = BillCustomValueViewModel(presenter: presenter, billet: billet, dependencies: DependencyContainer())
        let viewController = BillCustomValueViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
