import AnalyticsModule
import Foundation
import LegacyPJ

protocol BillCustomValueViewModelInputs: AnyObject {
    func confirmButtonTouched()
    func checkEnableConfirmButton(value: Double)
}

final class BillCustomValueViewModel {
    typealias Dependecies = HasAuthManager & HasAnalytics
    private let presenter: BillCustomValuePresenting
    private let dependencies: Dependecies
    private var billet: BillPay

    init(presenter: BillCustomValuePresenting, billet: BillPay, dependencies: Dependecies) {
        self.presenter = presenter
        self.billet = billet
        self.dependencies = dependencies
    }
}

private extension BillCustomValueViewModel {
    func logValueInserted() {
        if let sellerID = dependencies.authManager.user?.id {
            dependencies.analytics.log(BillCustomValueAnalytics.valueInserted(sellerID: sellerID, value: billet.totalValue))
        }
    }
}

extension BillCustomValueViewModel: BillCustomValueViewModelInputs {
    func checkEnableConfirmButton(value: Double) {
        billet.totalValue = value
        presenter.changeEnableValueConfirmButton(enable: value > 0)
    }
    
    func confirmButtonTouched() {
        logValueInserted()
        presenter.didNextStep(action: .checkPayment(billet: billet))
    }
}
