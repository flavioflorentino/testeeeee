import UIKit

protocol BillCustomValueDisplay: AnyObject {
    func changeEnableValueConfirmButton(enable: Bool)
}
