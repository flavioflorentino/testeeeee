import UI
import UIKit

extension BillCustomValueViewController.Layout {
    enum Size {
        static let confirmButtonHeight: CGFloat = 48
    }
    enum CornerRadius {
        static let confirmButton: CGFloat = 24
    }
    enum Fonts {
        static let currency = UIFont.systemFont(ofSize: 24, weight: .regular)
        static let value = UIFont.systemFont(ofSize: 52, weight: .light)
    }
}

final class BillCustomValueViewController: ViewController<BillCustomValueViewModelInputs, UIView> {
    fileprivate typealias Localizable = Strings.BillCustomValue
    fileprivate enum Layout { }

    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.valueDescription
        label.textAlignment = .center
        label.textColor = Colors.grayscale700.color
        label.typography = .bodySecondary()
        return label
    }()
    
    private lazy var amountTextField: CurrencyField = {
        let field = CurrencyField()
        field.fontCurrency = Layout.Fonts.currency
        field.fontValue = Layout.Fonts.value
        field.placeholderColor = Colors.grayscale300.color
        field.textColor = Colors.grayscale700.color
        field.spacing = 2
        field.limitValue = 6
        field.delegate = self
        return field
    }()
    
    private lazy var confirmButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(Localizable.buttonTitle, for: .normal)
        button.cornerRadius = Layout.CornerRadius.confirmButton
        button.normalBackgrounColor = Colors.branding700.color
        button.normalTitleColor = Colors.white.color
        button.disabledBackgrounColor = Colors.grayscale300.color
        button.disabledTitleColor = Colors.white.color
        button.borderColor = .clear
        button.isEnabled = false
        button.addTarget(self, action: #selector(confirmButtonAction(_:)), for: .touchUpInside)
        return button
    }()
 
    override func buildViewHierarchy() {
        view.addSubview(infoLabel)
        view.addSubview(amountTextField)
        view.addSubview(confirmButton)
    }
    
    override func configureViews() {
        title = Localizable.title
        view.backgroundColor = Colors.white.lightColor
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func setupConstraints() {
        infoLabel.layout {
            $0.top == view.topAnchor + Spacing.base06
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
        }
        
        amountTextField.layout {
            $0.top == infoLabel.bottomAnchor + Spacing.base00
            $0.centerX == view.centerXAnchor
        }
        
        confirmButton.layout {
            $0.top == amountTextField.bottomAnchor + Spacing.base05
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
            $0.height == Layout.Size.confirmButtonHeight
        }
    }
}

@objc
extension BillCustomValueViewController {
    func confirmButtonAction(_ sender: UIButton) {
        viewModel.confirmButtonTouched()
    }
}

// MARK: View Model Outputs
extension BillCustomValueViewController: BillCustomValueDisplay {
    func changeEnableValueConfirmButton(enable: Bool) {
        confirmButton.isEnabled = enable
    }
}

extension BillCustomValueViewController: CurrencyFieldDelegate {
    func onValueChange(value: Double) {
        viewModel.checkEnableConfirmButton(value: value)
    }
}
