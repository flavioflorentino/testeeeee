import UIKit

enum BillCustomValueAction {
    case checkPayment(billet: BillPay)
}

protocol BillCustomValueCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BillCustomValueAction)
}

final class BillCustomValueCoordinator: BillCustomValueCoordinating {
    weak var viewController: UIViewController?

    func perform(action: BillCustomValueAction) {
        switch action {
        case .checkPayment(let billet):
            let checkBiletViewController = BillPayFactory.make(billet: billet)
            viewController?.navigationController?.pushViewController(checkBiletViewController, animated: true)
        }
    }
}
