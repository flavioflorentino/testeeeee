import Core
import Foundation

protocol BillCustomValuePresenting: AnyObject {
    var viewController: BillCustomValueDisplay? { get set }
    func changeEnableValueConfirmButton(enable: Bool)
    func didNextStep(action: BillCustomValueAction)
}

final class BillCustomValuePresenter: BillCustomValuePresenting {
    private let coordinator: BillCustomValueCoordinating
    weak var viewController: BillCustomValueDisplay?

    init(coordinator: BillCustomValueCoordinating) {
        self.coordinator = coordinator
    }
    
    func changeEnableValueConfirmButton(enable: Bool) {
        viewController?.changeEnableValueConfirmButton(enable: enable)
    }
    
    func didNextStep(action: BillCustomValueAction) {
        coordinator.perform(action: action)
    }
}
