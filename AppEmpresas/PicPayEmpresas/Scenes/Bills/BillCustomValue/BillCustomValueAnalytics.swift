import AnalyticsModule

enum BillCustomValueAnalytics: AnalyticsKeyProtocol {
    case valueInserted(sellerID: Int, value: Double)
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case let .valueInserted(sellerID, value):
            let properties = [
                "seller_id": "\(sellerID)",
                "value": "\(value)"
            ]
            return AnalyticsEvent("Bill - Value inserted ", properties: properties, providers: [.mixPanel])
        }
    }
}
