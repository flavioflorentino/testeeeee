import Core

protocol BillFormServicing {
    func verifyPayment(code: String, billetDescription: String, completion: @escaping (Result<BilletData?, ApiError>) -> Void)
}

final class BillFormService: BillFormServicing {
    typealias Dependencies = HasMainQueue
    var dependencies: Dependencies?
    
    init(_ dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func verifyPayment(code: String, billetDescription: String, completion: @escaping (Result<BilletData?, ApiError>) -> Void) {
        let codeUnmasked = code.onlyNumbers
        let endpoint = BillFormServiceEndpoint.verifyPayment(code: codeUnmasked, description: billetDescription)
        BizApi<BilletData>(endpoint: endpoint).bizExecute { [weak self] result in
            self?.dependencies?.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }
}
