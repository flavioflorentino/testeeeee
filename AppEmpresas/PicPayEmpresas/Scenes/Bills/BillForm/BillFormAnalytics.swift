import AnalyticsModule

enum BillFormAnalytics: AnalyticsKeyProtocol {
    case dataInserted(_ sellerID: Int)
    case dataInsertedError(_ sellerId: Int)
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .dataInserted(let sellerID):
            return logEvent(sellerID: sellerID, name: "Bill - Data inserted")
            
        case .dataInsertedError(let sellerID):
            return logEvent(sellerID: sellerID, name: "Bill - Data inserted error")
        }
    }
}

private extension BillFormAnalytics {
    func logEvent(sellerID: Int, name: String) -> AnalyticsEventProtocol {
        let property = ["seller_id": "\(sellerID)"]
        return AnalyticsEvent(name, properties: property, providers: [.mixPanel])
    }
}
