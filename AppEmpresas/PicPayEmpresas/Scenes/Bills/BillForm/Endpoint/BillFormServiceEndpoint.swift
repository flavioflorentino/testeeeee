import Core

enum BillFormServiceEndpoint {
    case verifyPayment(code: String, description: String)
}

extension BillFormServiceEndpoint: ApiEndpointExposable {
    var path: String {
        "/transaction/search-bill"
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var body: Data? {
        switch self {
        case let .verifyPayment(code, _):
            return ["code": code].toData()
        }
    }
}
