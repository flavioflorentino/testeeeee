protocol BillFormDisplay: AnyObject {
    func stopLoadingAnimation()
    func startLoadingAnimation()
    func prepareViewForPresent()
    func presentValidBilletCode()
    func enableActionButton(_ enable: Bool)
    func presentInvalidBilletCode(_ message: String)
    func presentBilletBlockMask(maskedBlock: String)
    func setTextFieldCode(code: String)
}
