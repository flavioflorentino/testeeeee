import Foundation
import Validator

final class BilletCompleteValidation {
    var input = ""
    
    private let minimumBilletLength = 46
    private let billetAgreementLength = 48
    private let common = BilletCommonValidation()
    
    private func getAllAgreementBlocks() -> [String] {
        common.agreementBlockRanges.map({ input[$0] })
    }
    
    private func getAllBilletBlocks() -> [String] {
        common.billetBlockRanges.map({ input[$0] })
    }
    
    func validate(input: String) -> Bool {
        self.input = input.onlyNumbers
        common.input = self.input
        
        return common.isAgreement ? validateAgreement() : validateBillet()
    }
    
    private func validateAgreement() -> Bool {
        guard input.count == billetAgreementLength else {
            return false
        }
        
        let fields = getAllAgreementBlocks()
        let validationMethodIdentifier = fields[0][2]
        guard common.agreementBlockRanges.allSatisfy({
            common.validateBlock(range: $0, verifier: validationMethodIdentifier)
        }) else {
            return false
        }
        
        let dvGeral = fields[0][3]
        let completeVerificationCode = fields[0][0..<3] + fields[0][4..<fields[0].count] + fields[1] + fields[2] + fields[3]
        
        return common.validateBlock(range: nil, verifier: validationMethodIdentifier, code: completeVerificationCode, dv: dvGeral)
    }
    
    private func validateBillet() -> Bool {
        guard input.count > minimumBilletLength else {
            return false
        }
        
        guard common.billetBlockRanges.allSatisfy({
            common.validateBlock(range: $0, verifier: nil)
        }) else {
            return false
        }
        
        let fields = getAllBilletBlocks()
        let dvGeral = input[32]

        let range = 33..<input.count
        let field5 = String(input[range])
        
        let completeVerificationCode = fields[0][0..<4] + field5 + fields[0][4..<fields[0].count] + fields[1] + fields[2]
        
        return dvGeral == common.calculateMod11Dv(completeVerificationCode)
    }
}
