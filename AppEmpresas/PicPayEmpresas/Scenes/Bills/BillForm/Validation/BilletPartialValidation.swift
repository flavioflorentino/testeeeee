import Foundation
import Validator

enum BilletPartialValidation {
    static var input = ""
    private static let common = BilletCommonValidation()
    
    static func validate(input: String) -> Bool {
        self.input = input.onlyNumbers
        common.input = self.input
        
        return validate()
    }
    
    private static func validate() -> Bool {
        if (common.isAgreement && input.count == 48) ||
            (!common.isAgreement && input.count > 46) {
            return BilletCompleteValidation().validate(input: input)
        }
        
        let ranges = common.isAgreement ? common.agreementBlockRanges : common.billetBlockRanges
        for range in  ranges {
            guard input.count > range.upperBound else {
                return true
            }
            
            let validationMethodIdentifier = common.isAgreement ? input[common.agreementBlockRanges[0]][2] : nil
            guard common.validateBlock(range: range, verifier: validationMethodIdentifier) else {
                return false
            }
        }
        
        return true
    }
}
