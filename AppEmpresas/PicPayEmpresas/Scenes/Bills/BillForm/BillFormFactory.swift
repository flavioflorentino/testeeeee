import Foundation

enum BillFormFactory {
    static func make(code: String? = nil) -> BillFormViewController {
        let container = DependencyContainer()
        let service: BillFormServicing = BillFormService(DependencyContainer())
        let coordinator: BillFormCoordinating = BillFormCoordinator()
        let presenter: BillFormPresenting = BillFormPresenter(coordinator: coordinator)
        let viewModel = BillFormViewModel(service: service, presenter: presenter, dependencies: container, code: code)
        let viewController = BillFormViewController(viewModel: viewModel)
        viewController.hidesBottomBarWhenPushed = true

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
