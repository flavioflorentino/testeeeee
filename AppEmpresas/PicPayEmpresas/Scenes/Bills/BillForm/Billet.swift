import Foundation

struct BilletData: Decodable {
    private enum CodingKeys: String, CodingKey {
        case barCode = "bar_code"
        case billType = "bill_type"
        case lineCode = "line_code"
        case codeType = "code_type"
        case sellerId = "seller_id"
        case dueDate = "due_date"
        case paymentDate = "payment_date"
        case amountNominal = "amount_nominal"
        case beneficiary, amount, expired, fillable, instructions, surcharge, cip
    }
    
    let billType: String?
    let beneficiary: Beneficiary
    let amount: String
    let amountNominal: String?
    let expired: Bool
    let paymentDate: String
    let dueDate: String?
    let fillable: Bool
    let barCode, lineCode, codeType: String
    let instructions: String?
    let surcharge: String?
    let sellerId: Int
    let cip: Cip
}

struct Beneficiary: Decodable {
    private enum CodingKeys: String, CodingKey {
        case code, name
        case fantasyName = "fantasy_name"
        case imageUrl = "img_url"
        case limitHour = "limit_hour"
    }
    
    let code: Int?
    let limitHour: String
    let name: String?
    let fantasyName: String?
    let imageUrl: String?
}

struct Cip: Decodable {
    private enum CodingKeys: String, CodingKey {
        case amount
        case interest
        case canReceiveOverdue = "can_receive_overdue"
        case paymentValue = "payment_value"
        case dateSearch = "date_search"
        case isRegisterValid = "is_register_valid"
    }
    
    let amount: Double
    let interest: Double?
    let canReceiveOverdue: Bool
    let paymentValue: Double?
    let dateSearch: String?
    let isRegisterValid: Bool
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        amount = try container.decode(Double.self, forKey: .amount)
        interest = try container.decodeIfPresent(Double.self, forKey: .interest)
        canReceiveOverdue = try container.decodeIfPresent(Bool.self, forKey: .canReceiveOverdue) ?? false
        paymentValue = try container.decodeIfPresent(Double.self, forKey: .paymentValue)
        dateSearch = try container.decodeIfPresent(String.self, forKey: .dateSearch)
        isRegisterValid = try container.decode(Bool.self, forKey: .isRegisterValid)
    }
    
    init(
        amount: Double,
        interest: Double?,
        canReceiveOverdue: Bool,
        paymentValue: Double?,
        dateSearch: String?,
        isRegisterValid: Bool
    ) {
        self.amount = amount
        self.interest = interest
        self.canReceiveOverdue = canReceiveOverdue
        self.paymentValue = paymentValue
        self.dateSearch = dateSearch
        self.isRegisterValid = isRegisterValid
    }
}

struct Billet: Decodable {
    let data: BilletData
}
