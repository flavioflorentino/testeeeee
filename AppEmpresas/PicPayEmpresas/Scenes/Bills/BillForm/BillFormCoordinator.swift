import UIKit

enum BillFormAction {
    case showAlert(title: String, message: String, buttonTitle: String)
    case fillableBillet(billet: BillPay)
    case checkPayment(billet: BillPay)
    case dismiss
    case back
}

protocol BillFormCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BillFormAction)
}

private extension BillFormCoordinator.Layout {
    enum Size {
        static let modalInfo = CGSize(width: 52, height: 52)
    }
}

final class BillFormCoordinator: BillFormCoordinating {
    fileprivate enum Layout { }
    weak var viewController: UIViewController?
    
    func perform(action: BillFormAction) {
        switch action {
        case let .showAlert(title, message, buttonTitle):
            showAlert(contentController: createPopUp(with: title, message: message, buttonTitle: buttonTitle))
        case let .fillableBillet(billet):
            let fillableBilletViewController = BillCustomValueFactory.make(billet)
            viewController?.navigationController?.pushViewController(fillableBilletViewController, animated: true)
        case .checkPayment(let billet):
            let checkBiletViewController = BillPayFactory.make(billet: billet)
            viewController?.navigationController?.pushViewController(checkBiletViewController, animated: true)
        case .back:
            viewController?.navigationController?.popViewController(animated: true)
        case .dismiss:
            viewController?.dismiss(animated: true)
        }
    }
    
    private func createPopUp(with title: String, message: String, buttonTitle: String) -> AlertModalViewController {
        let modalInfoImage = AlertModalInfoImage(
            image: Assets.Emoji.iconBad.image,
            size: Layout.Size.modalInfo)
        let info = AlertModalInfo(
            imageInfo: modalInfoImage,
            title: title,
            subtitle: message,
            buttonTitle: buttonTitle
        )
        
        let alertPopup = AlertModalFactory.make(alertModalInfo: info)
        
        alertPopup.touchButtonAction = { alert in
            alert.dismiss(animated: true)
        }

        return alertPopup
    }
    
   private func showAlert(contentController: UIViewController) {
       let popup = PopupViewController()
       popup.contentController = contentController
       viewController?.present(popup, animated: true, completion: nil)
   }
}
