import SnapKit
import UI
import UIKit

// MARK: Layout Extension

private extension BillFormViewController.Layout {
    enum Fonts {
        static let textField = UIFont.systemFont(ofSize: 14)
        static let buttonTitle = UIFont.systemFont(ofSize: 17, weight: .semibold)
    }
    
    enum Insets {
        static let keyboard = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
    }
    
    enum Spacing {
        static let `default`: CGFloat = 20
    }
}

final class BillFormViewController: ViewController<BillFormViewModelInput, UIView> {
    fileprivate enum Layout { }
    
    private lazy var billetNumberTextField: UIPPFloatingTextView = {
        let billetField = UIPPFloatingTextView()
        billetField.topText = Strings.BillForm.billetNumber
        billetField.backgroundColor = Colors.white.color
        billetField.textView.font = Layout.Fonts.textField
        billetField.textView.keyboardType = .numberPad
        billetField.textView.delegate = self
        billetField.layer.borderWidth = Border.none
        billetField.textView.returnKeyType = .next
        billetField.textView.textColor = Colors.grayscale850.color
        billetField.textView.tintColor = Colors.branding300.color
        billetField.textView.isUserInteractionEnabled = true
        billetField.infoButtonAction = nil
        billetField.setupBilletMask()
        return billetField
    }()
    
    private lazy var billetDescriptionTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Strings.BillForm.billetDescriptionPlaceholder
        textField.title = Strings.BillForm.billetDescriptionTitle
        textField.selectedTitle = Strings.BillForm.billetDescriptionTitle
        textField.textColor = Colors.grayscale850.color
        textField.titleColor = Colors.grayscale400.color
        textField.placeholderColor = Colors.grayscale300.color
        textField.backgroundColor = Colors.white.color
        textField.font = Layout.Fonts.textField
        textField.keyboardType = .default
        textField.delegate = self
        textField.clipsToBounds = true
        textField.alwaysShowPlaceholder = true
        textField.layer.borderWidth = Border.none
        return textField
    }()
    
    private lazy var actionButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(Strings.BillForm.advance, for: .normal)
        button.titleLabel?.font = Layout.Fonts.buttonTitle
        button.normalTitleColor = Colors.white.color
        button.normalBackgrounColor = Colors.branding300.color
        button.highlightedBackgrounColor = Colors.branding400.color
        button.disabledBackgrounColor = Colors.grayscale400.color
        button.disabledTitleColor = Colors.white.color
        button.cornerRadius = Spacing.base03
        button.isEnabled = false
        button.addTarget(self, action: #selector(didTapAdvanceButton(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.isUserInteractionEnabled = true
        return scrollView
    }()
    
    private let descriptionMaxChars = 40
    private var shouldChangeText = true
    
     override func viewDidLoad() {
        super.viewDidLoad()
        prepareViewForPresent()
        checkForScanner()
    }
    
    func checkForScanner() {
        viewModel.checkForScannedCode()
    }
    
    private func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
     override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(billetNumberTextField)
        contentView.addSubview(billetDescriptionTextField)
        contentView.addSubview(actionButton)
    }
    
     override func configureViews() {
        view.backgroundColor = UI.Colors.white.color
        title = Strings.BillForm.viewTitle
        
        addGestures()
    }
    
    private func addGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        tapGesture.delaysTouchesBegan = false
        tapGesture.delaysTouchesEnded = false
        view.addGestureRecognizer(tapGesture)
    }
    
     override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.size.equalTo(view)
        }
        
        billetNumberTextField.snp.makeConstraints {
            $0.top.equalTo(view).offset(Spacing.base04)
            $0.leading.equalTo(view).offset(Layout.Spacing.default)
            $0.trailing.equalTo(view).offset(-Layout.Spacing.default)
            $0.height.greaterThanOrEqualTo(Spacing.base06)
        }
        
        billetDescriptionTextField.snp.makeConstraints {
            $0.top.equalTo(billetNumberTextField.snp.bottom).offset(Spacing.base02)
            $0.leading.equalTo(view).offset(Layout.Spacing.default)
            $0.trailing.equalTo(view).offset(-Layout.Spacing.default)
            $0.height.equalTo(Spacing.base06)
        }
        
        actionButton.snp.makeConstraints {
            $0.top.equalTo(billetDescriptionTextField.snp.bottom).offset(Spacing.base06)
            $0.leading.equalTo(view).offset(Layout.Spacing.default)
            $0.trailing.equalTo(view).offset(-Layout.Spacing.default)
            $0.height.equalTo(Spacing.base06)
        }
    }
}

@objc
extension BillFormViewController {
    func didTapAdvanceButton(_ sender: UIButton) {
        viewModel.verifyBillForm(with: billetNumberTextField.textView.text, description: billetDescriptionTextField.text ?? "")
    }
    
    // MARK: - Keyboard Events Handler
    
    func keyboardWillShow(_ notification: NSNotification) {
        scrollView.contentInset = Layout.Insets.keyboard
    }
    
    func keyboardWillHide(_ notification: NSNotification) {
        scrollView.contentInset = .zero
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

// MARK: - View Model Outputs

extension BillFormViewController: BillFormDisplay {
    func setTextFieldCode(code: String) {
        billetNumberTextField.textView.text = code
        enableActionButton(true)
    }
    
    func presentBilletBlockMask(maskedBlock: String) {
        billetNumberTextField.textView.text = maskedBlock
        shouldChangeText = false
    }
    
    func presentInvalidBilletCode(_ message: String) {
        billetNumberTextField.errorMessage = message
    }
    
    func presentValidBilletCode() {
        billetNumberTextField.errorMessage = nil
    }
    
    func prepareViewForPresent() {
        addObservers()
        billetDescriptionTextField.becomeFirstResponder()
        billetDescriptionTextField.resignFirstResponder()
    }
    
    func startLoadingAnimation() {
        showLoad(true)
    }
    
    func stopLoadingAnimation() {
        showLoad(false)
    }
    
    func enableActionButton(_ enable: Bool) {
        actionButton.isEnabled = enable
    }
    
    private func showLoad(_ load: Bool) {
        guard load else {
            actionButton.stopLoadingAnimating()
            actionButton.setTitle(Strings.BillForm.advance, for: .normal)
            return
        }
        
        actionButton.startLoadingAnimating()
        actionButton.setTitle(nil, for: .normal)
    }
}

// MARK: - UITextFieldDelegate

extension BillFormViewController: UITextFieldDelegate, UITextViewDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            return true
        }
        
        if textField == billetDescriptionTextField {
            return textField.text?.count != descriptionMaxChars
        }
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        viewModel.validateBillet(with: textView.text, replacementText: text)
        
        guard let ppTextView = textView.superview as? MaskTextField else {
            return shouldChangeText
        }
        
        viewModel.setBilletMask(text: textView.text, range: range, replacementText: text, superview: ppTextView)
        
        return shouldChangeText
    }
}

extension BillFormViewController: ErrorViewDelegate {
    func didTouchConfirmButton() {
        viewModel.closeErrorView()
    }
}
