import AnalyticsModule
import Foundation
import LegacyPJ

protocol BillFormViewModelInput: AnyObject {
    func closeErrorView()
    func validateBillet(with billetCode: String, replacementText: String)
    func verifyBillForm(with code: String, description: String)
    func setBilletMask(text: String, range: NSRange, replacementText: String, superview: MaskTextField)
    func checkForScannedCode()
}

final class BillFormViewModel {
    typealias Dependecies = HasAuthManager & HasAnalytics
    private typealias Localizable = Strings.BillForm
    private let service: BillFormServicing
    private let presenter: BillFormPresenting
    private let dependencies: Dependecies
    private let billetAgreementCode: Character = "8"
    private var code: String?
    
    init(service: BillFormServicing, presenter: BillFormPresenting, dependencies: Dependecies, code: String?) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.code = code
    }
}

private extension BillFormViewModel {
    func logInvalidCode() {
        if let sellerID = dependencies.authManager.user?.id {
            dependencies.analytics.log(BillFormAnalytics.dataInsertedError(sellerID))
        }
    }
}

extension BillFormViewModel: BillFormViewModelInput {
    func closeErrorView() {
        presenter.didNextStep(action: .dismiss)
    }
    
    func checkForScannedCode() {
        guard let codeMasked = code?.billetMask().maskedText(from: code) else {
            return
        }
        
        presenter.setTextFieldCode(code: codeMasked)
    }
    
    func validateBillet(with billetCode: String, replacementText: String) {
        let billetTotalLength = 46
        let billetCodeCount = billetCode.isEmpty ? replacementText.onlyNumbers.count : billetCode.onlyNumbers.count
        let billetValid = BilletPartialValidation.validate(input: billetCode.onlyNumbers)
        
        if !billetValid {
            logInvalidCode()
        }
        
        presenter.validatedBilletCode(billetValid)
        presenter.changeActionButtonAvailability(billetCodeCount >= billetTotalLength && billetValid)
    }
    
    func setBilletMask(
        text: String,
        range: NSRange,
        replacementText: String,
        superview: MaskTextField
    ) {
        let newText = (text as NSString).replacingCharacters(in: range, with: replacementText)
        
        if let maskABlock = superview.maskBlock {
            let blockMask = maskABlock(text, newText)
            presenter.presentBilletBlockMask(blockMask: blockMask)
        }
    }
    
    func verifyBillForm(with code: String, description: String) {
        presenter.startLoading()
        service.verifyPayment(code: code, billetDescription: description) { [weak self] result in
            self?.presenter.stopLoading()
            
            switch result {
            case let .success(billet):
                self?.handleVerifyPaymentResult(billet: billet, description: description)
            case .failure(.badRequest(let error)):
                self?.presenter.presentErrorVerifyingPayment(message: error.message)
            case .failure:
                self?.presenter.presentErrorVerifyingPayment(message: Localizable.unexpectedError)
            }
        }
    }
    
    private func handleVerifyPaymentResult(billet: BilletData?, description boletoDescription: String) {
        guard let billet = billet, let totalValue = Double(billet.amount) else {
            presenter.presentErrorVerifyingPayment(message: Localizable.unexpectedError)
            return
        }
        
        let billetInitialValue = billet.amountNominal ?? billet.amount
        
        let checkPaymentBillet = BillPay(
            code: billet.barCode,
            description: boletoDescription,
            beneficiary: billet.beneficiary.name ?? billet.beneficiary.fantasyName ?? "",
            expirationDate: billet.dueDate ?? billet.paymentDate,
            isExpired: billet.expired,
            initialValue: billetInitialValue.currency(),
            fee: billet.cip.interest?.toCurrencyString(),
            totalValue: totalValue,
            instuctions: billet.instructions,
            data: makeCheckPaymentItems(billet, description: boletoDescription)
        )
        
        fireAnalytics()
        
        if billet.fillable {
            presenter.didNextStep(action: .fillableBillet(billet: checkPaymentBillet))
            return
        }
        
        if verifyOverduePayment(billet) {
            presenter.presentUnacceptedOverduePayment()
            return
        }
        
        presenter.didNextStep(action: .checkPayment(billet: checkPaymentBillet))
    }
    
    private func fireAnalytics() {
        if let sellerId = dependencies.authManager.user?.id {
            dependencies.analytics.log(BillFormAnalytics.dataInserted(sellerId))
        }
    }
    
    // verify if the overdue bill can be payed
    private func verifyOverduePayment(_ billet: BilletData) -> Bool {
        billet.expired && billet.cip.canReceiveOverdue == false
    }
    
    private func makeCheckPaymentItems(_ billet: BilletData, description: String) -> [CheckPaymentItem] {
        var beneficiaryName: String = ""
        
        if let name = billet.beneficiary.name {
            beneficiaryName = name
        }
        
        if let fantasyName = billet.beneficiary.fantasyName {
            beneficiaryName = fantasyName
        }
        
        return [
            CheckPaymentItem(title: Localizable.recipient, details: beneficiaryName),
            CheckPaymentItem(title: Localizable.description, details: description),
            CheckPaymentItem(title: Localizable.paymentDate, details: billet.paymentDate)
        ]
    }
}
