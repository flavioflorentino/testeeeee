import UI

protocol BillFormPresenting: AnyObject {
    var viewController: BillFormDisplay? { get set }
    func stopLoading()
    func startLoading()
    func didNextStep(action: BillFormAction)
    func validatedBilletCode(_ valid: Bool)
    func changeActionButtonAvailability(_ enable: Bool)
    func presentBilletBlockMask(blockMask: String)
    func presentUnacceptedOverduePayment()
    func presentErrorVerifyingPayment(message: String)
    func setTextFieldCode(code: String)
}

final class BillFormPresenter: BillFormPresenting {
    private typealias Localizable = Strings.BillForm
    private let coordinator: BillFormCoordinating
    var viewController: BillFormDisplay?
    
    init(coordinator: BillFormCoordinating) {
        self.coordinator = coordinator
    }
    
    func stopLoading() {
        viewController?.stopLoadingAnimation()
    }
    
    func startLoading() {
        viewController?.startLoadingAnimation()
    }
    
    func validatedBilletCode(_ valid: Bool) {
        valid ? viewController?.presentValidBilletCode() : viewController?.presentInvalidBilletCode(Localizable.invalidBilletMessage)
    }
    
    func changeActionButtonAvailability(_ enable: Bool) {
        viewController?.enableActionButton(enable)
    }
    
    func presentBilletBlockMask(blockMask: String) {
        viewController?.presentBilletBlockMask(maskedBlock: blockMask)
    }
    
    func presentErrorVerifyingPayment(message: String) {
        coordinator.perform(
            action: .showAlert(
                title: Localizable.paymentErrorAlertTitle,
                message: message,
                buttonTitle: Localizable.okGotIt))
    }
    
    func presentUnacceptedOverduePayment() {
        coordinator.perform(
            action: .showAlert(
                title: Localizable.billetOverdueTitle,
                message: Localizable.billetOverdueMessage,
                buttonTitle: Localizable.okGotIt))
    }
    
    func didNextStep(action: BillFormAction) {
        coordinator.perform(action: action)
    }
    
    func setTextFieldCode(code: String) {
        viewController?.setTextFieldCode(code: code)
    }
}
