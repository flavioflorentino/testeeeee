import Core

protocol ScannerServicing {
    typealias BarcodeCompletionBlock = (Result<BilletData?, ApiError>) -> Void
    func getLineCode(code: String, completion: @escaping BarcodeCompletionBlock)
}

final class ScannerService {
    typealias Dependencies = HasMainQueue
    
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ScannerServicing
extension ScannerService: ScannerServicing {
    func getLineCode(code: String, completion: @escaping BarcodeCompletionBlock) {
        let endpoint = BillFormServiceEndpoint.verifyPayment(code: code, description: "")
        BizApi<BilletData>(endpoint: endpoint).bizExecute { result in
            self.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }
}
