import UI

protocol ScannerDisplay: AnyObject {
    func startLoading()
    func stopLoading()
    func showError()
}

private extension ScannerViewController.Layout {
    enum Size {
        static let sideMultiplier: CGFloat = 0.325
        static let button = CGSize(width: 332, height: 40)
    }
    
    enum Angle {
        static let right: CGFloat = .pi / 2
    }
    
    enum Alpha {
        static let multiplier: CGFloat = 0.8
    }
}

final class ScannerViewController: ViewController<ScannerViewModelInputs, UIView>, LoadingViewProtocol {
    fileprivate typealias Localizable = Strings.Scanner
    fileprivate enum Layout { }
    
    lazy var loadingView = LoadingView()
    
    private lazy var flashButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.Scanner.flash.image, for: .normal)
        button.addTarget(self, action: #selector(toggleFlash), for: .touchUpInside)
        return button
    }()
    
    private lazy var goToTypeButton: UIPPButton = {
        let button = UIPPButton()
        
        button.normalBackgrounColor = .clear
        button.transform = CGAffineTransform(rotationAngle: Layout.Angle.right)
        button.cornerRadius = Layout.Size.button.height / 2
        button.addTarget(self, action: #selector(goToType), for: .touchUpInside)
        button.setTitle(Localizable.button, for: .normal)
        return button
    }()
    
    private lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.Scanner.back.image, for: .normal)
        button.addTarget(self, action: #selector(closeCamera), for: .touchUpInside)
        return button
    }()
    
    private lazy var scannerView: BillScanner = {
        let scanner = BillScanner()
        scanner.delegate = self
        return scanner
    }()
    
    private lazy var topView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.black.color.withAlphaComponent(Layout.Alpha.multiplier)
        return view
    }()
    
    private lazy var bottomView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.black.color.withAlphaComponent(Layout.Alpha.multiplier)
        return view
    }()
    
    private lazy var lineImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.image = Assets.Scanner.line.image
        return image
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .white())
        label.text = Localizable.description
        label.transform = CGAffineTransform(rotationAngle: Layout.Angle.right)
        return label
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        scannerView.resetSetup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
    }

    override func buildViewHierarchy() {
        view.addSubview(scannerView)
        
        topView.addSubview(backButton)
        topView.addSubview(descriptionLabel)
        topView.addSubview(flashButton)
        
        bottomView.addSubview(goToTypeButton)
        scannerView.addSubview(topView)
        scannerView.addSubview(bottomView)
        scannerView.addSubview(lineImageView)
    }
    
    override func setupConstraints() {
        scannerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        bottomView.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
            $0.width.equalToSuperview().multipliedBy(Layout.Size.sideMultiplier)
        }
        
        goToTypeButton.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.button)
            $0.centerX.centerY.equalToSuperview()
        }
        
        topView.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview()
            $0.width.equalToSuperview().multipliedBy(Layout.Size.sideMultiplier)
        }
        
        backButton.snp.makeConstraints {
            $0.top.equalTo(view.snp.topMargin).offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
        }
        
        flashButton.snp.makeConstraints {
            $0.bottom.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        lineImageView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalTo(bottomView.snp.trailing)
            $0.trailing.equalTo(topView.snp.leading)
        }
    }
}

// MARK: ScannerDisplay
extension ScannerViewController: ScannerDisplay {
    func stopLoading() {
        stopLoadingView()
    }
    
    func startLoading() {
        startLoadingView()
    }
    
    func showError() {
        presentErrorAlert()
    }
}

@objc
private extension ScannerViewController {
    func closeCamera() {
        viewModel.close()
    }
    
    func goToType() {
        viewModel.goToType()
    }
    
    func toggleFlash() {
        viewModel.toggleFlash()
    }
}

extension ScannerViewController: BillScannerDelegate {
    func scanningDidFail() {
        showError()
    }
    
    func scanningSucceeded(with code: String?) {
        viewModel.verifyCode(code: code)
    }
}

private extension ScannerViewController {
    func presentErrorAlert() {
        let popup = UI.PopupViewController(
            title: Localizable.errorTitle,
            description: Localizable.errorMessage,
            preferredType: .image(Assets.Emoji.iconBad.image)
        )
        
        let confirmation = PopupAction(
            title: Localizable.okGotIt,
            style: .fill
        ) {
            self.scannerView.resetSetup()
        }
        
        popup.didCloseDismiss = {
            self.scannerView.resetSetup()
        }
        
        popup.addAction(confirmation)
        popup.view.transform = CGAffineTransform(rotationAngle: Layout.Angle.right)
        
        present(popup, animated: true, completion: nil)
    }
}
