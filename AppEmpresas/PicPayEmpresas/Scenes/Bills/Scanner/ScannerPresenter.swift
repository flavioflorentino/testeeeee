import AVFoundation
import Foundation

protocol ScannerPresenting: AnyObject {
    var viewController: ScannerDisplay? { get set }
    func goToBillForm(lineCode: String)
    func startLoading()
    func showError()
    func close()
}

final class ScannerPresenter {
    private let coordinator: ScannerCoordinating
    weak var viewController: ScannerDisplay?
    
    private var isFlashOn = false

    init(coordinator: ScannerCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ScannerPresenting
extension ScannerPresenter: ScannerPresenting {
    func close() {
        coordinator.perform(action: .close)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }

    func goToBillForm(lineCode: String) {
        stopLoading()
        coordinator.perform(action: .form(code: lineCode))
    }
    
    func showError() {
        stopLoading()
        viewController?.showError()
    }
}
