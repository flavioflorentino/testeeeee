import AnalyticsModule
import Foundation

enum ScannerAnalytics: AnalyticsKeyProtocol {
    case didTapToggleFlash(sellerId: Int, isFlashOn: Bool)
    case didTapType(sellerId: Int)
    
    func event() -> AnalyticsEventProtocol {
        let properties: [String: Any]
        let name: String
        
        switch self {
        case .didTapType(let sellerId):
            name = "Bill - Code Type"
            properties = ["seller_id": sellerId]
        case let .didTapToggleFlash(sellerId, isFlashOn):
            name = "Bill - Turn on Flash"
            properties = [
                "seller_id": sellerId,
                "flash": isFlashOn
            ]
        }
        
        return AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
    }
}
