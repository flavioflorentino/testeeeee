import Foundation

enum ScannerFactory {
    static func make() -> ScannerViewController {
        let container = DependencyContainer()
        let service: ScannerServicing = ScannerService(dependencies: container)
        let coordinator: ScannerCoordinating = ScannerCoordinator(dependencies: container)
        let presenter: ScannerPresenting = ScannerPresenter(coordinator: coordinator)
        let viewModel = ScannerViewModel(service: service, presenter: presenter, dependencies: container)
        let viewController = ScannerViewController(viewModel: viewModel)
        viewController.hidesBottomBarWhenPushed = true

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
