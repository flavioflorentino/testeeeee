import UIKit

enum ScannerAction {
    case form(code: String)
    case close
}

protocol ScannerCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ScannerAction)
}

final class ScannerCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ScannerCoordinating
extension ScannerCoordinator: ScannerCoordinating {
    func perform(action: ScannerAction) {
        switch action {
        case let .form(code):
            let controller = BillFormFactory.make(code: code)
            viewController?.navigationController?.isNavigationBarHidden = false
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .close:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}
