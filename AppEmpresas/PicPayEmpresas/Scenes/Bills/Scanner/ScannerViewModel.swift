import AnalyticsModule
import AVFoundation
import Foundation
import LegacyPJ

protocol ScannerViewModelInputs: AnyObject {
    func toggleFlash()
    func verifyCode(code: String?)
    func goToType()
    func close()
}

final class ScannerViewModel {
    typealias Dependencies = HasAuthManager & HasAnalytics
    private let dependencies: Dependencies

    private let service: ScannerServicing
    private let presenter: ScannerPresenting
    private var isFlashOn = false

    init(service: ScannerServicing, presenter: ScannerPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - ScannerViewModelInputs
extension ScannerViewModel: ScannerViewModelInputs {
    func close() {
        presenter.close()
    }
    
    func goToType() {
        presenter.goToBillForm(lineCode: "")
        fireAnalytics(action: .type)
    }
    
    func verifyCode(code: String?) {
        guard let code = code else {
            return
        }
        
        presenter.startLoading()
        
        service.getLineCode(code: code) { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
            case .failure:
                self.presenter.showError()
            case .success(let billet):
                guard let lineCode = billet?.lineCode else {
                    self.presenter.showError()
                    return
                }
                self.presenter.goToBillForm(lineCode: lineCode)
            }
        }
    }
    
    func toggleFlash() {
        guard
            let device = AVCaptureDevice.default(for: AVMediaType.video),
            device.hasTorch == true
            else {
                return
        }
        
        do {
            try device.lockForConfiguration()
            if device.torchMode == .on {
                device.torchMode = .off
            } else {
                try device.setTorchModeOn(level: 1.0)
            }
            isFlashOn.toggle()
            device.unlockForConfiguration()
        } catch {
            return
        }
        fireAnalytics(action: .flash)
    }
}

private extension ScannerViewModel {
    enum AnalyticsActions {
        case type
        case flash
    }
    
    func fireAnalytics(action: AnalyticsActions) {
        guard let sellerId = dependencies.authManager.user?.id else {
            return
        }
        
        switch action {
        case .type:
            dependencies.analytics.log(ScannerAnalytics.didTapType(sellerId: sellerId))
        case .flash:
            dependencies.analytics.log(ScannerAnalytics.didTapToggleFlash(sellerId: sellerId, isFlashOn: isFlashOn))
        }
    }
}
