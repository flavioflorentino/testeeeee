import AVFoundation
import Foundation
import UIKit

protocol BillScannerDelegate: AnyObject {
    func scanningDidFail()
    func scanningSucceeded(with code: String?)
}

final class BillScanner: UIView {
    override class var layerClass: AnyClass {
        AVCaptureVideoPreviewLayer.self
    }
    
    override var layer: AVCaptureVideoPreviewLayer {
        guard let layer = super.layer as? AVCaptureVideoPreviewLayer else {
            return AVCaptureVideoPreviewLayer()
        }
        return layer
    }
    
    private var captureSession: AVCaptureSession?
    weak var delegate: BillScannerDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension BillScanner {
    var isRunning: Bool {
        captureSession?.isRunning ?? false
    }
    
    func initialSetup() {
        clipsToBounds = true
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
            return
        }
        
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        guard captureSession?.canAddInput(videoInput) == true else {
            scanningDidFail()
            return
        }
        
        captureSession?.addInput(videoInput)
        
        let metaDataOutput = AVCaptureMetadataOutput()
        
        guard captureSession?.canAddOutput(metaDataOutput) == true else {
            scanningDidFail()
            return
        }
        
        captureSession?.addOutput(metaDataOutput)
        
        metaDataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        metaDataOutput.metadataObjectTypes = [.itf14, .interleaved2of5]
        
        layer.session = captureSession
        layer.videoGravity = .resizeAspectFill
        
        captureSession?.startRunning()
    }
    
    func startScanning() {
        captureSession?.startRunning()
    }
    
    func stopScanning() {
        captureSession?.stopRunning()
    }
    
    func scanningDidFail() {
        delegate?.scanningDidFail()
        captureSession = nil
    }
    
    func found(code: String) {
        delegate?.scanningSucceeded(with: code)
    }
}

extension BillScanner: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(
        _ output: AVCaptureMetadataOutput,
        didOutput metadataObjects: [AVMetadataObject],
        from connection: AVCaptureConnection
    ) {
        stopScanning()
        
        if let metadataObject = metadataObjects.first {
            guard
                let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject,
                let stringValue = readableObject.stringValue
            else {
                return
            }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
    }
}

extension BillScanner {
    func resetSetup() {
        initialSetup()
    }
}
