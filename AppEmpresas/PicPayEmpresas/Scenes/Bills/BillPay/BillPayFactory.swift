import Foundation

enum BillPayFactory {
    static func make(billet: BillPay) -> BillPayViewController {
        let container = DependencyContainer()
        let service: BillPayServicing = BillPayService(dependencies: container)
        let coordinator: BillPayCoordinating = BillPayCoordinator()
        let presenter: BillPayPresenting = BillPayPresenter(coordinator: coordinator)
        let viewModel = BillPayViewModel(
            service: service,
            presenter: presenter,
            billet: billet,
            dependencies: container
        )
        
        let viewController = BillPayViewController(viewModel: viewModel)
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
