import SnapKit
import UI
import UIKit

protocol BillPayBodyViewDelegate: AnyObject {
    func infoButtonTouched()
}

private extension BillPayBodyView.Layout {
    enum Fonts {
        static let regular = Typography.caption().font()
        static let bold = Typography.caption(.highlight).font()
    }
    
    enum InfoTextView {
        private static let margin = Spacing.base01
        static let textInsets = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
    }
}

final class BillPayBodyView: UIView {
    private typealias Localizable = Strings.BillPay
    fileprivate enum Layout {}
    
    // MARK: - Internal Properties
    
    weak var delegate: BillPayBodyViewDelegate?
    
    // MARK: - Private Properties
    
    private lazy var bodyStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base02
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var detailsView = UIView()
    private lazy var expirationStackView = BillPayItemStackView()
    
    private lazy var detailsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base01
        stackView.distribution = .equalSpacing
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var feeStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.addArrangedSubview(feeItemLabel)
        stackView.addArrangedSubview(feeValueStackView)
        return stackView
    }()
    
    private lazy var feeItemLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.regular
        label.textColor = Colors.grayscale500.color
        label.text = Localizable.fee
        return label
    }()
    
    private lazy var feeValueStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base00
        stackView.addArrangedSubview(feeValueLabel)
        stackView.addArrangedSubview(feeInfoButton)
        return stackView
    }()
    
    private lazy var feeValueLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.bold
        label.textColor = Colors.critical600.color
        return label
    }()
    
    private lazy var feeInfoButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.icoInfoButton.image, for: .normal)
        button.addTarget(self, action: #selector(infoButtonTouched(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var totalValueView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale050.color
        view.addSubview(totalValueStackView)
        return view
    }()
    
    private lazy var totalValueStackView = BillPayItemStackView()
    
    private lazy var infoTextView: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = Colors.grayscale050.color
        textView.textColor = Colors.grayscale500.color
        textView.font = Layout.Fonts.regular
        textView.textContainerInset = Layout.InfoTextView.textInsets
        textView.isScrollEnabled = false
        return textView
    }()
    
    // MARK: - Inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Actions
    
    @objc
    func infoButtonTouched(_ sender: UIButton) {
        delegate?.infoButtonTouched()
    }
}

// MARK: - Internal Methods

extension BillPayBodyView {
    func setup(checkPayment: BillPay) {
        setupExpirationStackView(date: checkPayment.expirationDate, isExpired: checkPayment.isExpired)
        setupItemsStackView(data: checkPayment.data)
        
        if let initialValue = checkPayment.initialValue {
            setupInitialValueStackView(initialValue: initialValue)
        }
        
        if let fee = checkPayment.fee {
            setupFeeStackView(fee: fee)
        }
        
        if let totalValueString = checkPayment.totalValue.toCurrencyString() {
            setupTotalValueView(totalValue: totalValueString)
        }
        
        setupInfoTextView(info: Localizable.infoText)
    }
}

// MARK: - Private Methods

private extension BillPayBodyView {
    func setupExpirationStackView(date: String, isExpired: Bool) {
        let dateString = date
        let itemColor = isExpired ? Colors.critical600 : Colors.grayscale600
        expirationStackView.set(title: Localizable.expirationDate, value: dateString, valueColor: itemColor.color)
        detailsStackView.addArrangedSubview(expirationStackView)
    }
    
    func setupItemsStackView(data: [CheckPaymentItem]) {
        for item in data {
            let stackView = BillPayItemStackView()
            guard !item.details.isEmpty else {
                continue
            }
            stackView.set(title: item.title, value: item.details)
            detailsStackView.addArrangedSubview(stackView)
        }
    }
    
    func setupInitialValueStackView(initialValue: String) {
        let stackView = BillPayItemStackView()
        stackView.set(title: Localizable.initialValue, value: initialValue)
        detailsStackView.addArrangedSubview(stackView)
    }
    
    func setupFeeStackView(fee: String) {
        feeValueLabel.text = fee
        detailsStackView.addArrangedSubview(feeStackView)
    }
    
    func setupTotalValueView(totalValue: String) {
        setupTotalValueConstraints()
        totalValueStackView.set(title: Localizable.valueToPay, value: totalValue)
        bodyStackView.addArrangedSubview(totalValueView)
    }
    
    func setupTotalValueConstraints() {
        totalValueStackView.snp.makeConstraints {
            $0.top.equalTo(totalValueView).offset(Spacing.base01)
            $0.leading.equalTo(totalValueView).offset(Spacing.base02)
            $0.trailing.equalTo(totalValueView).offset(-Spacing.base02)
            $0.bottom.equalTo(totalValueView).offset(-Spacing.base01)
        }
    }
    
    func setupInfoTextView(info: String) {
        infoTextView.text = info
        addSubview(infoTextView)
        setupInfoTextViewConstraints()
    }
    
    func setupInfoTextViewConstraints() {
        infoTextView.snp.makeConstraints {
            $0.top.equalTo(bodyStackView.snp.bottom).offset(Spacing.base02)
            $0.leading.equalTo(self).offset(Spacing.base02)
            $0.trailing.equalTo(self).offset(-Spacing.base02)
            $0.bottom.equalTo(self)
        }
    }
}

// MARK: - ViewConfiguration

extension BillPayBodyView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(bodyStackView)
        bodyStackView.addArrangedSubview(detailsView)
        detailsView.addSubview(detailsStackView)
    }
    
    func setupConstraints() {
        bodyStackView.snp.makeConstraints {
            $0.top.leading.trailing.equalTo(self)
        }
        
        detailsStackView.snp.makeConstraints {
            $0.top.bottom.equalTo(detailsView)
            $0.leading.equalTo(detailsView).offset(Spacing.base02)
            $0.trailing.equalTo(detailsView).offset(-Spacing.base02)
        }
    }
}
