import UI
import UIKit

protocol BillPayFooterViewDelegate: AnyObject {
    func didTapPayBillet()
}

private extension BillPayFooterView.Layout {
    enum Sizes {
        static let buttonHeight: CGFloat = 44
    }
    
    enum Others {
        static let buttonCornerRadius: CGFloat = 22
    }
}
    
final class BillPayFooterView: UIView {
    fileprivate enum Layout { }
    
    weak var delegate: BillPayFooterViewDelegate?
    
    private lazy var payButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(Strings.BillPay.payBillet, for: .normal)
        button.titleLabel?.font = Typography.bodySecondary(.highlight).font()
        button.normalTitleColor = Palette.white.color
        button.normalBackgrounColor = Palette.ppColorBranding300.color
        button.highlightedBackgrounColor = Palette.ppColorBranding400.color
        button.cornerRadius = Layout.Others.buttonCornerRadius
        button.addTarget(self, action: #selector(didTapPayBillet(_:)), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    func didTapPayBillet(_ sender: UIButton) {
        delegate?.didTapPayBillet()
    }

    func setButtonEnable(_ isEnable: Bool) {
        payButton.isEnabled = isEnable
    }
}

extension BillPayFooterView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(payButton)
    }
    
    func setupConstraints() {
        payButton.layout {
            $0.top == topAnchor
            $0.bottom == bottomAnchor
            $0.leading == leadingAnchor + Spacing.base02
            $0.trailing == trailingAnchor - Spacing.base02
            $0.height == Layout.Sizes.buttonHeight
        }
    }
}
