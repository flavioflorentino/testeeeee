import UI
import UIKit

final class BillPayItemStackView: UIStackView {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.caption().font()
        label.textColor = Colors.grayscale500.color
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.caption(.highlight).font()
        label.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        distribution = .equalSpacing
        axis = .horizontal
        spacing = Spacing.base01
        buildLayout()
    }
    
    @available(*, unavailable)
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Public Methods

extension BillPayItemStackView {
    func set(title: String, value: String, valueColor: UIColor = Colors.grayscale600.color) {
        titleLabel.text = title
        valueLabel.text = value
        valueLabel.textColor = valueColor
    }
}

// MARK: - ViewConfiguration

extension BillPayItemStackView: ViewConfiguration {
    func buildViewHierarchy() {
        addArrangedSubview(titleLabel)
        addArrangedSubview(valueLabel)
    }
    
    func setupConstraints() { }
}
