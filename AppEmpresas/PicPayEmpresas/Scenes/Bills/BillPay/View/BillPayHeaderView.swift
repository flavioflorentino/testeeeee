import UI
import UIKit

private extension BillPayHeaderView.Layout {
    enum Sizes {
        static let imageSize: CGFloat = 60
        static let separatorHeight: CGFloat = 1
    }
}

final class BillPayHeaderView: UIView {
    private typealias Localizable = Strings.BillPay
    fileprivate enum Layout { }
    
    // MARK: - Private Properties
    
    private lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = Assets.iconBillet.image
        image.layer.cornerRadius = Layout.Sizes.imageSize / 2
        image.clipsToBounds = true
        return image
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.grayscale600.color
        return label
    }()
    
    private lazy var currencyLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 26)
        label.textColor = Colors.grayscale400.color
        return label
    }()
    
    private lazy var separator: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale300.color
        return view
    }()
    
    // MARK: - Inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration

extension BillPayHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(imageView)
        addSubview(descLabel)
        addSubview(currencyLabel)
        addSubview(separator)
    }
    
    func setupConstraints() {
        imageView.layout {
            $0.top == topAnchor + Spacing.base01
            $0.leading == leadingAnchor + Spacing.base02
            $0.height == Layout.Sizes.imageSize
            $0.width == Layout.Sizes.imageSize
        }
        
        descLabel.layout {
            $0.top == topAnchor + Spacing.base02
            $0.leading == imageView.trailingAnchor + Spacing.base02
            $0.trailing == trailingAnchor - Spacing.base02
        }
        
        currencyLabel.layout {
            $0.top == descLabel.bottomAnchor
            $0.leading == descLabel.leadingAnchor
            $0.trailing == trailingAnchor - Spacing.base02
        }
        
        separator.layout {
            $0.top == imageView.bottomAnchor + Spacing.base01
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor
            $0.height == Layout.Sizes.separatorHeight
        }
    }
}

// MARK: - Public Methods

extension BillPayHeaderView {
    func setCurrency(currency: String) {
        currencyLabel.text = currency
    }
}

// MARK: - Private Methods

private extension BillPayHeaderView {
    func setupView() {
        backgroundColor = .clear
        descLabel.text = Localizable.bankslip
    }
}
