import AnalyticsModule

enum BillPayAnalytics: AnalyticsKeyProtocol {
    case billetLoaded(_ billet: BillPay, sellerId: Int)
    case billetPaymentTry(_ billet: BillPay, sellerId: Int)
    case billetPayed(_ billet: BillPay, sellerId: Int)
    case billetRefused(reason: String, billet: BillPay, sellerId: Int)
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case let .billetLoaded(billet, sellerId):
            let properties = params(billet, sellerId: sellerId)
            return AnalyticsEvent("Bill - Data Loaded", properties: properties, providers: [.mixPanel])
        case let .billetPaymentTry(billet, sellerId):
            let properties = billetPaymentTryParams(billet, sellerId: sellerId)
            return AnalyticsEvent("Bill - Payment Cta", properties: properties, providers: [.mixPanel])
        case let .billetPayed(billet, sellerId):
            let properties = params(billet, sellerId: sellerId)
            return AnalyticsEvent("Bill - Payment done", properties: properties, providers: [.mixPanel])
        case let .billetRefused(reason, billet, sellerId):
            let properties = billetRefusedParams(reason: reason, billet: billet, sellerId: sellerId)
            return AnalyticsEvent("Bill - Payment Reproved ", properties: properties, providers: [.mixPanel])
        }
    }
}

private extension BillPayAnalytics {
    func params(_ billet: BillPay, sellerId: Int) -> [String: Any] {
        let properties: [String: Any] = [
            "value": billet.totalValue,
            "description": billet.description ?? String(),
            "seller_id": "\(sellerId)",
            "expiration_date": billet.expirationDate,
            "interests": billet.fee != nil
        ]
        
        return properties
    }
    
    func billetPaymentTryParams(_ billet: BillPay, sellerId: Int) -> [String: Any] {
        let properties: [String: Any] = [
            "value": billet.totalValue,
            "description": billet.description ?? String(),
            "seller_id": "\(sellerId)",
            "expiration_date": billet.expirationDate,
            "interests": billet.fee != nil,
            "recipient": billet.beneficiary
        ]
        
        return properties
    }
    
    func billetRefusedParams(reason: String, billet: BillPay, sellerId: Int) -> [String: Any] {
        let properties: [String: Any] = [
            "reason": reason,
            "value": billet.totalValue,
            "description": billet.description ?? String(),
            "seller_id": "\(sellerId)",
            "expiration_date": billet.expirationDate,
            "interests": billet.fee != nil,
            "recipient": billet.beneficiary
        ]
        
        return properties
    }
}
