import Foundation

struct BillPay {
    let code: String
    let description: String?
    let beneficiary: String
    let expirationDate: String
    let isExpired: Bool
    let initialValue: String?
    let fee: String?
    var totalValue: Double
    let instuctions: String?
    let data: [CheckPaymentItem]
}

struct CheckPaymentItem {
    let title: String
    let details: String
}
