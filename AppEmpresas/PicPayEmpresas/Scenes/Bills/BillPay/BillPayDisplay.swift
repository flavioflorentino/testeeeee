import Foundation

protocol BillPayDisplay: AnyObject {
    func showHeaderView(value: String)
    func showBodyView(billet: BillPay)
    func showLoading()
    func hideLoading()
}
