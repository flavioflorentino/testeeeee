import SnapKit
import UI
import UIKit

final class BillPayViewController: ViewController<BillPayViewModelInput, UIView> {
    typealias Localizable = Strings.BillPay
    
    // MARK: - Public Properties
    
    lazy var loadingView = LoadingView()
    
    // MARK: - Private Properties
    
    private lazy var contentView = UIView()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var headerView = BillPayHeaderView()
    
    private lazy var bodyView: BillPayBodyView = {
        let bodyView = BillPayBodyView()
        bodyView.delegate = self
        return bodyView
    }()
    
    private lazy var footerView: BillPayFooterView = {
        let footerView = BillPayFooterView()
        footerView.delegate = self
        return footerView
    }()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadBillet()
    }
    
    // MARK: - Public Methods
    
     override func configureViews() {
        view.backgroundColor = Palette.white.color
        title = Strings.BillForm.viewTitle
    }
    
     override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(headerView)
        contentView.addSubview(bodyView)
        contentView.addSubview(footerView)
    }
    
     override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalTo(view)
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalTo(view)
            $0.height.greaterThanOrEqualTo(view)
        }
        
        headerView.snp.makeConstraints {
            $0.top.leading.trailing.equalTo(contentView)
        }
        
        bodyView.snp.makeConstraints {
            $0.top.equalTo(headerView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalTo(contentView)
        }
        
        footerView.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(bodyView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalTo(contentView)
            $0.bottom.equalTo(contentView).offset(-Spacing.base03)
        }
    }
}

// MARK: - BillPayDisplay

extension BillPayViewController: BillPayDisplay {
    func showHeaderView(value: String) {
        headerView.setCurrency(currency: value)
    }
    
    func showBodyView(billet: BillPay) {
        bodyView.setup(checkPayment: billet)
    }
    
    func showLoading() {
        footerView.setButtonEnable(false)
        startLoadingView()
    }
    
    func hideLoading() {
        footerView.setButtonEnable(true)
        stopLoadingView()
    }
}

// MARK: - BillPayFooterViewDelegate

extension BillPayViewController: BillPayFooterViewDelegate {
    func didTapPayBillet() {
        viewModel.payBillet()
    }
}

// MARK: - BillPayBodyViewDelegate

extension BillPayViewController: BillPayBodyViewDelegate {
    func infoButtonTouched() {
        viewModel.infoButtonTouched()
    }
}

// MARK: - LoadingViewProtocol

extension BillPayViewController: LoadingViewProtocol { }
