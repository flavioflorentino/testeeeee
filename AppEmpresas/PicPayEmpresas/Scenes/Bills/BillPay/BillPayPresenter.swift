import Core
import Foundation
import UIKit

protocol BillPayPresenting: AnyObject {
    var viewController: BillPayDisplay? { get set }
    
    func showBillet(billet: BillPay)
    func showExpiredInfoAlert()
    func setLoading(isLoading: Bool)
    func showSuccessPayBillet(billetId: String)
    func showError(apiError: ApiError)
    func showError(requestError: RequestError)
    func showFactorAuthentication(params: Bill2FAParams, delegate: Bill2FADelegate)
    func finishFlow()
}

final class BillPayPresenter: BillPayPresenting {
    private typealias Localizable = Strings.BillPay
    
    private let coordinator: BillPayCoordinating
    weak var viewController: BillPayDisplay?

    init(coordinator: BillPayCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - BillPayPresenting

extension BillPayPresenter {
    func showBillet(billet: BillPay) {
        if let currency = billet.totalValue.toCurrencyString() {
            viewController?.showHeaderView(value: currency)
        }
        
        viewController?.showBodyView(billet: billet)
    }
    
    func showExpiredInfoAlert() {
        let modalInfo = AlertModalInfo(
            title: Localizable.expiredBillet,
            subtitle: Localizable.expiredBilletDetails,
            buttonTitle: Localizable.modalButtonTitle
        )
        
        coordinator.perform(action: .expiredInfo(modalInfo: modalInfo))
    }
    
    func setLoading(isLoading: Bool) {
        isLoading ? viewController?.showLoading() : viewController?.hideLoading()
    }
    
    func showSuccessPayBillet(billetId: String) {
        coordinator.perform(action: .paymentSuccess(billetId: billetId))
    }
    
    func showError(apiError: ApiError) {
        showError(message: apiError.localizedDescription)
    }
    
    func showError(requestError: RequestError) {
        showError(message: requestError.message, title: requestError.title)
    }
    
    func showFactorAuthentication(params: Bill2FAParams, delegate: Bill2FADelegate) {
        let action = BillPayAction.factorAuthentication(params: params, delegate: delegate)
        coordinator.perform(action: action)
    }
    
    func finishFlow() {
        coordinator.perform(action: .finishFlow)
    }
}

// MARK: - Private Methods

private extension BillPayPresenter {
    func showError(message: String, title: String? = nil) {
        let modalImage = AlertModalInfoImage(
            image: Assets.Emoji.iconBad.image,
            size: CGSize(width: 80, height: 80)
        )
        
        let modalInfo = AlertModalInfo(
            imageInfo: modalImage,
            title: title,
            subtitle: message,
            buttonTitle: Localizable.modalButtonTitle
        )
        
        coordinator.perform(action: .paymentError(modalInfo: modalInfo))
    }
}
