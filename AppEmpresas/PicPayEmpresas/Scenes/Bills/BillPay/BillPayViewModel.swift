import AnalyticsModule
import Core
import Foundation
import FeatureFlag
import LegacyPJ

protocol BillPayViewModelInput: AnyObject {
    func loadBillet()
    func payBillet()
    func infoButtonTouched()
}

final class BillPayViewModel {
    typealias Dependencies = HasAuthManager & HasAnalytics
    // MARK: - Private Properties
    
    private let service: BillPayServicing
    private let presenter: BillPayPresenting
    private let billet: BillPay
    private let dependencies: Dependencies
    
    // MARK: - Inits
    
    init(
        service: BillPayServicing,
        presenter: BillPayPresenting,
        billet: BillPay,
        dependencies: Dependencies
    ) {
        self.service = service
        self.presenter = presenter
        self.billet = billet
        self.dependencies = dependencies
    }
}

// MARK: - BillPayViewModelInput

struct PayBilletParams {
    let code: String
    let amount: Double
    let billDescription: String
    let pin: String

    init(from billet: BillPay, pin: String) {
        code = billet.code
        amount = billet.totalValue
        billDescription = billet.description ?? ""
        self.pin = pin
    }
}

extension BillPayViewModel: BillPayViewModelInput {
    func loadBillet() {
        presenter.showBillet(billet: billet)
        
        if let sellerId = dependencies.authManager.user?.id {
            dependencies.analytics.log(BillPayAnalytics.billetLoaded(billet, sellerId: sellerId))
        }
    }
    
    func payBillet() {
        logBillPaymentTry()
        log2FAEvent(.started(billet: billet))
        presenter.setLoading(isLoading: true)
        if FeatureManager.shared.isActive(.bill2FA) {
            service.status { [weak self] result in
                switch result {
                case .success(let response):
                    self?.verifyStatus(status: response)
                case .failure:
                    self?.factorAuthenticatorPassword()
                }
            }
        } else {
            factorAuthenticatorPassword()
        }
    }
    
    func infoButtonTouched() {
        presenter.showExpiredInfoAlert()
    }
}

private extension BillPayViewModel {
    func payBill(with params: PayBilletParams) {
        service.payBillet(with: params) { [weak self] result in
            self?.presenter.setLoading(isLoading: false)

            switch result {
            case .success(let response):
                guard let id = response?.id else {
                    self?.presenter.showError(apiError: ApiError.bodyNotFound)
                    return
                }

                self?.logBillPayed()
                self?.log2FAEvent(.success(.password, id: id))
                self?.presenter.showSuccessPayBillet(billetId: id)

            case .failure(let apiError):
                if let requestError = apiError.requestError {
                    self?.presenter.showError(requestError: requestError)
                    self?.logBillRefused(reason: requestError.message)
                } else {
                    self?.presenter.showError(apiError: apiError)
                    self?.logBillRefused(reason: apiError.localizedDescription)
                }
            }
        }
    }
    
    func logBillPaymentTry() {
        guard let sellerId = dependencies.authManager.user?.id else {
            return
        }
        dependencies.analytics.log(BillPayAnalytics.billetPaymentTry(billet, sellerId: sellerId))
    }
    
    func log2FAPaymentTry() {
        dependencies.analytics.log(Bill2FAAnalytics.started(billet: billet))
    }
    
    func log2FAPaymentPassword() {
        dependencies.analytics.log(Bill2FAAnalytics.methodChoosen(method: .password))
    }
    
    func log2FAPaymentSms() {
        dependencies.analytics.log(Bill2FAAnalytics.methodChoosen(method: .sms))
    }
    
    func logBillPayed() {
        guard let sellerId = dependencies.authManager.user?.id else {
            return
        }
        dependencies.analytics.log(BillPayAnalytics.billetPayed(billet, sellerId: sellerId))
    }
    
    func logBill2FASuccess(_ id: String) {
        dependencies.analytics.log(Bill2FAAnalytics.success(.password, id: id))
    }
    
    func logBillRefused(reason: String) {
        guard let sellerId = dependencies.authManager.user?.id else {
            return
        }
        let event = BillPayAnalytics.billetRefused(reason: reason, billet: billet, sellerId: sellerId)
        dependencies.analytics.log(event)
    }
    
    func log2FAEvent(_ event: Bill2FAAnalytics) {
        dependencies.analytics.log(event)
    }
    
    func verifyStatus(status: Bill2FAResponse) {
        let detail = status.detail
        guard
            detail.method == .sms,
            let phone = detail.mobilePhone
            else {
                factorAuthenticatorPassword()
                return
        }
        
        let params = Bill2FAParams(
            billet: billet,
            phone: phone
        )
        log2FAEvent(.methodChoosen(method: .sms))
        presenter.showFactorAuthentication(params: params, delegate: self)
    }
}

extension BillPayViewModel: Bill2FADelegate {
    func factorAuthenticatorPassword() {
        log2FAEvent(.methodChoosen(method: .password))
        dependencies.authManager.performActionWithAuthorization { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case .success(let pin):
                let params = PayBilletParams(from: self.billet, pin: pin)
                self.payBill(with: params)
            case .canceled:
                self.log2FAEvent(.canceled(.password))
                self.presenter.setLoading(isLoading: false)
            default:
                break
            }
        }
    }
    
    func resultSMSFactor(id: String) {
        presenter.setLoading(isLoading: false)
        logBillPayed()
        presenter.showSuccessPayBillet(billetId: id)
    }
    
    func finishFlow() {
        presenter.finishFlow()
    }
}
