import Core

enum BillPayEndpoint {
    case payBillet(_ params: PayBilletParams)
}

extension BillPayEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .payBillet:
            return "/transaction/pay-bill"
        }
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var body: Data? {
        switch self {
        case let .payBillet(params):
            return [
                "code": params.code,
                "amount": params.amount,
                "description": params.billDescription
            ].toData()
        }
    }
    
    var customHeaders: [String: String] {
        switch self {
        case let .payBillet(params):
            return headerWith(pin: params.pin)
        }
    }
}
