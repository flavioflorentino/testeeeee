import UIKit

enum BillPayAction {
    case paymentSuccess(billetId: String)
    case paymentError(modalInfo: AlertModalInfo)
    case expiredInfo(modalInfo: AlertModalInfo)
    case factorAuthentication(params: Bill2FAParams, delegate: Bill2FADelegate)
    case finishFlow
}

protocol BillPayCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BillPayAction)
}

final class BillPayCoordinator: BillPayCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: BillPayAction) {
        switch action {
        case .paymentSuccess(let billetId):
            let receiptViewController = ReceiptFactory.make(.billId(billetId))
            viewController?.present(receiptViewController, animated: true) {
                self.viewController?.navigationController?.popToRootViewController(animated: true)
            }
        case .paymentError(let modalInfo):
            showModalAlert(modalInfo: modalInfo)
        case .expiredInfo(let modalInfo):
            showModalAlert(modalInfo: modalInfo)
        case let .factorAuthentication(params, delegate):
            let controller = Bill2FAFactory.make(params: params, delegate: delegate)
            let navigation = UINavigationController(rootViewController: controller)
            navigation.modalPresentationStyle = .fullScreen
            viewController?.navigationController?.present(navigation, animated: true)
        case .finishFlow:
            viewController?.navigationController?.popToRootViewController(animated: true)
        }
    }
}

private extension BillPayCoordinator {
    func showModalAlert(modalInfo: AlertModalInfo) {
        let alertPopup = AlertModalFactory.make(alertModalInfo: modalInfo)
        alertPopup.touchButtonAction = { alert in
            alert.dismiss(animated: true)
        }
        showAlert(contentController: alertPopup)
    }
    
    func showAlert(contentController: UIViewController) {
        let popup = PopupViewController()
        popup.contentController = contentController
        viewController?.present(popup, animated: true, completion: nil)
    }
}
