import Core
import LegacyPJ

protocol BillPayServicing {
    typealias PayBilletCompletion = (Result<BillPayResponse?, ApiError>) -> Void
    func payBillet(with params: PayBilletParams, completion: @escaping PayBilletCompletion)
    func status(completion: @escaping (Result<Bill2FAResponse, ApiError>) -> Void)
}

final class BillPayService {
    typealias Dependencies = HasMainQueue & HasAuthManager
    private let dependencies: Dependencies
    
    // MARK: - Inits
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - BillPayServicing

extension BillPayService: BillPayServicing {
    func payBillet(with params: PayBilletParams, completion: @escaping PayBilletCompletion) {
        let endpoint = BillPayEndpoint.payBillet(params)
        BizApi<BillPayResponse>(endpoint: endpoint).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map { $0.model.data }
                completion(mappedResult)
            }
        }
    }
    
    func status(completion: @escaping (Result<Bill2FAResponse, ApiError>) -> Void) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        let endpoint = Bill2FAEndpoint.sendSMS
        Core.Api<Bill2FAResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                
                completion(mappedResult)
            }
        }
    }
}
