import UIKit

protocol ReceiptDisplay: AnyObject {
    func didReceiveReceipt(_ items: [BillSection])
    func stopLoading()
}
