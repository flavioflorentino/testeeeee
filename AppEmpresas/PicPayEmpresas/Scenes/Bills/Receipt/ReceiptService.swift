import Core

protocol ReceiptServicing {
    func getReceipt(id: String, completion: @escaping (Result<ReceiptBill?, ApiError>) -> Void)
}

final class ReceiptService: ReceiptServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func getReceipt(id: String, completion: @escaping (Result<ReceiptBill?, ApiError>) -> Void) {
        let endpoint = ReceiptEndpoint.getReceipt(id)
        
        BizApi<ReceiptBill>(endpoint: endpoint).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }
}
