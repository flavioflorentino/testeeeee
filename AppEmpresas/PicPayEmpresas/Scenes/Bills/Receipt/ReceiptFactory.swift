import Foundation
import UIKit

enum ReceiptStyle {
    case billId(_ id: String)
    case billReceipt(_ receipt: ReceiptBill)
}

enum ReceiptFactory {
    static func make(_ style: ReceiptStyle) -> UINavigationController {
        let container = DependencyContainer()
        let service: ReceiptServicing = ReceiptService(dependencies: container)
        let coordinator: ReceiptCoordinating = ReceiptCoordinator()
        let presenter: ReceiptPresenting = ReceiptPresenter(coordinator: coordinator)
        let viewModel = ReceiptViewModel(service: service, presenter: presenter, style: style)
        let viewController = ReceiptViewController(viewModel: viewModel)
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.setup(appearance: NavigationBarAppearanceOldRegistration())

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return navigationController
    }
}
