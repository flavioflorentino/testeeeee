import Foundation
import UI
import UIKit

enum BillCellType {
    case paragraph, detail, header
}

struct BillSection {
    let section: [BillCellModel]
}

protocol BillCellIdentifier {
    var type: BillCellType { get }
    var identifier: String { get }
}

class BillCellModel: BillCellIdentifier {
    let type: BillCellType
    let identifier: String
    
    init(_ type: BillCellType, _ identifier: String) {
        self.type = type
        self.identifier = identifier
    }
}

final class BillHeaderCellModel: BillCellModel {
    let transactionId: String
    let timestamp: String
    let initialValue: String
    let abatementValue: String
    let discountValue: String
    let interestsValue: String
    let penaltyValue: String
    let finalValue: String
    
    init(
        _ transactionId: String,
        timestamp: String,
        initialValue: String,
        abatementValue: String,
        discountValue: String,
        interestsValue: String,
        penaltyValue: String,
        finalValue: String
    ) {
        self.transactionId = transactionId
        self.timestamp = timestamp
        self.initialValue = initialValue
        self.abatementValue = abatementValue
        self.discountValue = discountValue
        self.interestsValue = interestsValue
        self.penaltyValue = penaltyValue
        self.finalValue = finalValue
        super.init(.header, BillHeaderCell.identifier)
    }
    
    convenience init?(receiptBill: ReceiptBill?) {
        guard let receipt = receiptBill else {
            return nil
        }
        
        let billIDInt: Int = receipt.register?.billID ?? 0
        let billID = String(billIDInt)
        let finalValue: String = receipt.amountPaid.toCurrencyString() ?? String()
        let initialValue = BillHeaderCellModel.currencyOrEmpty(value: receipt.register?.amountTitle)
        let abatementValue = BillHeaderCellModel.currencyOrEmpty(value: receipt.register?.amountAbatementTitle)
        let discountValue = BillHeaderCellModel.currencyOrEmpty(value: receipt.register?.discountAmount)
        let interestsValue = BillHeaderCellModel.currencyOrEmpty(value: receipt.register?.interestAmount)
        let penaltyValue = BillHeaderCellModel.currencyOrEmpty(value: receipt.register?.penaltyAmount)
        
        self.init(
            billID,
            timestamp: receipt.createdAt,
            initialValue: initialValue,
            abatementValue: abatementValue,
            discountValue: discountValue,
            interestsValue: interestsValue,
            penaltyValue: penaltyValue,
            finalValue: finalValue
        )
    }
    
    private static func currencyOrEmpty(value: Double?) -> String {
        guard let value = value, value > 0, let valueCurrency = value.toCurrencyString() else {
            return String()
        }
        
        return valueCurrency
    }
}

final class BillDetailCellModel: BillCellModel {
    let key: String
    let value: String
    
    init(_ key: String, value: String) {
        self.key = key
        self.value = value
        super.init(.detail, BillDetailCell.identifier)
    }
    
    static func receiptDetails(_ receiptBill: ReceiptBill?) -> [BillDetailCellModel] {
        guard let receipt = receiptBill else { return [] }
        
        var detailItens: [BillDetailCellModel] = []
        
        if let billDescription = receipt.receiptBillDescription, billDescription.isNotEmpty {
            detailItens.append(BillDetailCellModel(Strings.Receipt.billDescription, value: billDescription))
        }
        
        if let dueDate = receipt.dueDate {
            detailItens.append(BillDetailCellModel(Strings.Receipt.billDueDate, value: dueDate))
        }
        
        if let recipientName = receipt.beneficiary.name, recipientName.isNotEmpty {
            detailItens.append(BillDetailCellModel(Strings.Receipt.billRecipientName, value: recipientName))
        }
        
        if let payingBank = receipt.payingBank, payingBank.isNotEmpty {
            detailItens.append(BillDetailCellModel(Strings.Receipt.billInstitutionalName, value: payingBank))
        }
        
        return detailItens
    }
}

final class BillParagraphCellModel: BillCellModel {
    let title: String
    let value: String
    
    init?(_ title: String, value: String?) {
        guard let value = value else {
            return nil
        }
        
        self.title = title
        self.value = value
        super.init(.paragraph, BillParagraphCell.identifier)
    }
}
