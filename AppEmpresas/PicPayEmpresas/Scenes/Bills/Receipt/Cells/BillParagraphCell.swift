import SnapKit
import UI

final class BillParagraphCell: UITableViewCell {
    fileprivate enum Layout { }
    
    private lazy var titleLable: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary(.highlight).font()
        label.textColor = Colors.grayscale700.lightColor
        label.textAlignment = .left
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.grayscale700.lightColor
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fill
        stack.spacing = Spacing.base01
        stack.addArrangedSubview(titleLable)
        stack.addArrangedSubview(valueLabel)
        return stack
    }()
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }
    
    func configCell(_ model: BillParagraphCellModel) {
        titleLable.text = model.title
        valueLabel.text = model.value
    }
}

extension BillParagraphCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.top.equalTo(self).offset(Spacing.base01)
            $0.bottom.equalTo(self).inset(Spacing.base01)
            $0.leading.equalTo(self).offset(Spacing.base03)
            $0.trailing.equalTo(self).inset(Spacing.base03)
        }
    }
}
