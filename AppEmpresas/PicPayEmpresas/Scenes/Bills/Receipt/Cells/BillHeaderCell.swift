import SnapKit
import UI

extension BillHeaderCell.Layout {
    enum ImageView {
        static let size = CGSize(width: 60, height: 60)
    }
    
    enum LineView {
        static let height: CGFloat = 1
    }
}

final class BillHeaderCell: UITableViewCell {
    private typealias Localizable = Strings.Receipt
    fileprivate enum Layout { }
    
    private lazy var billImage = UIImageView(image: Assets.Receipt.bill.image)
    
    private lazy var billTitleLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.title(.large).font()
        label.text = Localizable.billTitle
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    private lazy var transactionLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.grayscale300.lightColor
        label.text = Localizable.receiptTransaction
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        return label
    }()
    
    private lazy var transactionValue: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.grayscale300.lightColor
        return label
    }()
    
    private lazy var transactionStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.addArrangedSubview(transactionLabel)
        stack.addArrangedSubview(transactionValue)
        return stack
    }()
    
    private lazy var timestampValue: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.grayscale300.lightColor
        return label
    }()
    
    private lazy var transactionTimestampStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.addArrangedSubview(transactionStack)
        stack.addArrangedSubview(timestampValue)
        return stack
    }()
    
    private lazy var billBaseInfoStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base01
        stack.addArrangedSubview(billTitleLabel)
        stack.addArrangedSubview(transactionTimestampStack)
        return stack
    }()
    
    private lazy var initialAmountLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.grayscale700.lightColor
        label.text = Localizable.billInitialValue
        return label
    }()
    
    private lazy var initialAmountValue: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary(.highlight).font()
        label.textColor = Colors.grayscale700.lightColor
        return label
    }()
    
    private lazy var initialAmountStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .equalSpacing
        stack.addArrangedSubview(initialAmountLabel)
        stack.addArrangedSubview(initialAmountValue)
        return stack
    }()
    
    private lazy var abatementLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.grayscale700.lightColor
        label.text = Localizable.billAbatement
        return label
    }()
    
    private lazy var abatementValue: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary(.highlight).font()
        label.textColor = Colors.grayscale700.lightColor
        return label
    }()
    
    private lazy var abatementStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .equalSpacing
        stack.addArrangedSubview(abatementLabel)
        stack.addArrangedSubview(abatementValue)
        return stack
    }()
    
    private lazy var discountLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.grayscale700.lightColor
        label.text = Localizable.billDiscount
        return label
    }()
    
    private lazy var discountValue: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary(.highlight).font()
        label.textColor = Colors.grayscale700.lightColor
        return label
    }()
    
    private lazy var discountStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .equalSpacing
        stack.addArrangedSubview(discountLabel)
        stack.addArrangedSubview(discountValue)
        return stack
    }()
    
    private lazy var interestLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.grayscale700.lightColor
        label.text = Localizable.billInterest
        return label
    }()
    
    private lazy var interestValue: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary(.highlight).font()
        label.textColor = Colors.grayscale700.lightColor
        return label
    }()
    
    private lazy var interestStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .equalSpacing
        stack.addArrangedSubview(interestLabel)
        stack.addArrangedSubview(interestValue)
        return stack
    }()
    
    private lazy var penaltyLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.grayscale700.lightColor
        label.text = Localizable.billPenalty
        return label
    }()
    
    private lazy var penaltyValue: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary(.highlight).font()
        label.textColor = Colors.grayscale700.lightColor
        return label
    }()
    
    private lazy var penaltyStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .equalSpacing
        stack.addArrangedSubview(penaltyLabel)
        stack.addArrangedSubview(penaltyValue)
        return stack
    }()
    
    private lazy var amountListStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.addArrangedSubview(initialAmountStack)
        stack.addArrangedSubview(abatementStack)
        stack.addArrangedSubview(discountStack)
        stack.addArrangedSubview(interestStack)
        stack.addArrangedSubview(penaltyStack)
        return stack
    }()
    
    private lazy var finalAmountLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodyPrimary(.highlight).font()
        label.text = Localizable.billFinalValue
        return label
    }()
    
    private lazy var finalAmountValue: UILabel = {
        let label = UILabel()
        label.font = Typography.bodyPrimary(.highlight).font()
        return label
    }()
    
    private lazy var finalAmountStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .equalSpacing
        stack.addArrangedSubview(finalAmountLabel)
        stack.addArrangedSubview(finalAmountValue)
        return stack
    }()
    
    private lazy var firstLineView = UIView()
    
    private lazy var secondLineView = UIView()
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
}

// MARK: - Internal Methods

extension BillHeaderCell {
    func configureCell(with params: BillHeaderCellModel) {
        transactionValue.text = params.transactionId
        timestampValue.text = params.timestamp
        
        if params.initialValue.isEmpty {
            initialAmountLabel.text = String()
            initialAmountValue.text = String()
        } else {
            initialAmountValue.text = params.initialValue
        }
        
        if params.abatementValue.isEmpty {
            abatementLabel.text = String()
            abatementValue.text = String()
        } else {
            abatementValue.text = params.abatementValue
        }
        
        if params.discountValue.isEmpty {
            discountLabel.text = String()
            discountValue.text = String()
        } else {
            discountValue.text = params.discountValue
        }
        
        if params.interestsValue.isEmpty {
            interestLabel.text = String()
            interestValue.text = String()
        } else {
            interestValue.text = params.interestsValue
        }
        
        if params.penaltyValue.isEmpty {
            penaltyLabel.text = String()
            penaltyValue.text = String()
        } else {
            penaltyValue.text = params.penaltyValue
        }
        
        if params.initialValue.isEmpty, params.abatementValue.isEmpty, params.discountValue.isEmpty {
            firstLineView.isHidden = true
            amountListStack.isHidden = true
        }
        
        finalAmountValue.text = params.finalValue
    }
}

// MARK: - ViewConfiguration

extension BillHeaderCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(billImage)
        addSubview(billBaseInfoStack)
        addSubview(firstLineView)
        addSubview(amountListStack)
        addSubview(secondLineView)
        addSubview(finalAmountStack)
    }
    
    func setupConstraints() {
        billImage.snp.makeConstraints {
            $0.top.leading.equalTo(self).offset(Spacing.base03)
            $0.size.equalTo(Layout.ImageView.size)
        }
        
        billBaseInfoStack.snp.makeConstraints {
            $0.top.equalTo(self).offset(Spacing.base03)
            $0.leading.equalTo(billImage.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalTo(self).inset(Spacing.base03)
        }
        
        firstLineView.snp.makeConstraints {
            $0.leading.equalTo(self).offset(Spacing.base03)
            $0.trailing.equalTo(self).inset(Spacing.base03)
            $0.top.equalTo(billBaseInfoStack.snp.bottom).offset(Spacing.base02)
            $0.height.equalTo(Layout.LineView.height)
        }
        
        amountListStack.snp.makeConstraints {
            $0.leading.equalTo(self).offset(Spacing.base03)
            $0.trailing.equalTo(self).inset(Spacing.base03)
            $0.top.equalTo(firstLineView.snp.bottom).offset(Spacing.base01)
        }
        
        secondLineView.snp.makeConstraints {
            $0.leading.equalTo(self).offset(Spacing.base03)
            $0.trailing.equalTo(self).inset(Spacing.base03)
            $0.top.equalTo(amountListStack.snp.bottom).offset(Spacing.base01)
            $0.height.equalTo(Layout.LineView.height)
        }
        
        finalAmountStack.snp.makeConstraints {
            $0.leading.equalTo(self).offset(Spacing.base03)
            $0.trailing.equalTo(self).inset(Spacing.base03)
            $0.top.equalTo(secondLineView.snp.bottom).offset(Spacing.base01)
            $0.bottom.equalTo(self).inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        firstLineView.backgroundColor = Colors.grayscale100.lightColor
        secondLineView.backgroundColor = Colors.grayscale100.lightColor
        backgroundColor = Colors.white.lightColor
    }
}
