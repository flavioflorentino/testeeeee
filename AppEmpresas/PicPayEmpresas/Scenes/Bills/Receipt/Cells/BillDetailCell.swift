import SnapKit
import UI

final class BillDetailCell: UITableViewCell {
    fileprivate enum Layout { }
    
    private lazy var keyLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.grayscale700.lightColor
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary(.highlight).font()
        label.textColor = Colors.grayscale700.lightColor
        label.textAlignment = .right
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .equalCentering
        stack.spacing = Spacing.base00
        stack.addArrangedSubview(keyLabel)
        stack.addArrangedSubview(valueLabel)
        return stack
    }()
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }
    
    func configCell(_ model: BillDetailCellModel) {
        keyLabel.text = model.key
        valueLabel.text = model.value
    }
}

extension BillDetailCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.top.equalTo(self).offset(Spacing.base01)
            $0.bottom.equalTo(self).inset(Spacing.base01)
            $0.leading.equalTo(self).offset(Spacing.base03)
            $0.trailing.equalTo(self).inset(Spacing.base03)
        }
    }
}
