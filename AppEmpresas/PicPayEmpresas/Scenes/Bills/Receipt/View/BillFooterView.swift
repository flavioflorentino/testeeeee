import SnapKit
import UI

extension BillFooterView.Layout {
    enum ImageView {
        static let size = CGSize(width: 78, height: 36)
    }
}

final class BillFooterView: UIView {
    fileprivate enum Layout { }
    
    private lazy var imageView = UIImageView(image: Assets.Receipt.receiptFooter.image)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("not implemented yet!!!")
    }
}

extension BillFooterView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(imageView)
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.top.equalTo(self).offset(Spacing.base01)
            $0.centerX.equalTo(self)
            $0.size.equalTo(Layout.ImageView.size)
            $0.bottom.equalTo(self).inset(Spacing.base01)
        }
    }
}
