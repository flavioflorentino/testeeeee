import SnapKit
import UI

extension SeparatorView.Layout {
    enum Line {
        static let color = Colors.black.lightColor.withAlphaComponent(0.1)
        static let height: CGFloat = 1
    }
}

final class SeparatorView: UITableViewHeaderFooterView {
    fileprivate enum Layout { }
    
    private lazy var lineView = UIView()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("not implemented yet!!!")
    }
}

extension SeparatorView: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(lineView)
    }
    
    func setupConstraints() {
        lineView.snp.makeConstraints {
            $0.top.leading.trailing.equalTo(self)
            $0.height.equalTo(Layout.Line.height)
        }
    }
    
    func configureViews() {
        lineView.backgroundColor = Layout.Line.color
    }
}
