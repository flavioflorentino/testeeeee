import UI
import UIKit

extension ReceiptViewController.Layout {
    enum TableView {
        static let sectionHeaderHeight: CGFloat = 12
    }
    
    enum LoagindView {
        static let alpha: CGFloat = 0.8
    }
}

final class ReceiptViewController: ViewController<ReceiptViewModelInputs, UIView>, LoadingViewProtocol {
    typealias Localizable = Strings.Receipt
    
    fileprivate struct Layout { }
    
    private var dataSource: TableViewHandler<String, BillCellModel, UITableViewCell>?
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.tableFooterView = BillFooterView()
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.sectionFooterHeight = CGFloat.leastNonzeroMagnitude
        tableView.estimatedSectionHeaderHeight = Layout.TableView.sectionHeaderHeight
        tableView.register(BillHeaderCell.self, forCellReuseIdentifier: BillHeaderCell.identifier)
        tableView.register(BillDetailCell.self, forCellReuseIdentifier: BillDetailCell.identifier)
        tableView.register(BillParagraphCell.self, forCellReuseIdentifier: BillParagraphCell.identifier)
        return tableView
    }()
    
    private lazy var closeBarButton: UIBarButtonItem = {
        let item = UIBarButtonItem(
            title: Localizable.receiptClose,
            style: .plain,
            target: self,
            action: #selector(didTapClose)
        )
        item.tintColor = Colors.branding300.color
        return item
    }()
    
     lazy var loadingView = LoadingView()

     override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.getReceiptInfo()
        startLoadingView()
    }
    
     override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        tableView.tableHeaderView = nil
        
        guard let footerView = tableView.tableFooterView else {
            return
        }
        let height = footerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var footerFrame = footerView.frame

        if height != footerFrame.size.height {
            footerFrame.size.height = height
            footerView.frame = footerFrame
            tableView.tableFooterView = footerView
        }
    }
 
     override func buildViewHierarchy() {
        navigationItem.leftBarButtonItem = closeBarButton
        view.addSubview(tableView)
    }
    
     override func configureViews() {
        title = Localizable.receiptTitle
    }
    
     override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

// MARK: View Model Outputs
extension ReceiptViewController: ReceiptDisplay {
    typealias Sections = Section<String, BillCellModel>
    
    func didReceiveReceipt(_ items: [BillSection]) {
        let result: [Sections] = items.map { Sections(items: $0.section) }
        
        dataSource = TableViewHandler(
            data: result,
            cellIdentifier: { $1.identifier },
            configureMultiCell: bindCell,
            configureSection: bindSection
        )
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        tableView.reloadData()
    }
    
    func stopLoading() {
        stopLoadingView()
    }
}

private extension ReceiptViewController {
    func bindCell(_ row: Int, _ model: BillCellModel, _ cell: UITableViewCell) {
        switch cell {
        case let cell as BillHeaderCell:
            guard let model = model as? BillHeaderCellModel else {
                return
            }
            cell.configureCell(with: model)
        case let cell as BillDetailCell:
            guard let model = model as? BillDetailCellModel else {
                return
            }
            cell.configCell(model)
        case let cell as BillParagraphCell:
            guard let model = model as? BillParagraphCellModel else {
                return
            }
            cell.configCell(model)
        default:
            break
        }
    }
    
    func bindSection(_ section: Int, _ model: Sections) -> Header? {
        section == 0 ? nil : .view(SeparatorView())
    }
}

@objc
extension ReceiptViewController {
    func didTapClose() {
        viewModel.dissmiss()
    }
}
