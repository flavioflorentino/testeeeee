import Core

enum ReceiptEndpoint {
    case getReceipt(_ id: String)
}

extension ReceiptEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .getReceipt(let id):
            return "/transaction/bill/\(id)"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getReceipt:
            return .get
        }
    }
}
