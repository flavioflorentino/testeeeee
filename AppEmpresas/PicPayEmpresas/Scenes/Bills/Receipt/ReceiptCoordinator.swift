import UIKit

enum ReceiptAction {
    case error(_ message: String)
    case dismiss
}

protocol ReceiptCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ReceiptAction)
}

final class ReceiptCoordinator: ReceiptCoordinating {
    weak var viewController: UIViewController?

    func perform(action: ReceiptAction) {
        switch action {
        case let .error(message):
            showErrorView(message)
        case .dismiss:
            viewController?.dismiss(animated: true)
        }
    }
}

private extension ReceiptCoordinator {
    func showErrorView(_ message: String) {
        let errorViewController = ErrorViewController()
        errorViewController.setErrorView(
            title: Strings.Receipt.receiptErrorTitle,
            message: message,
            image: Assets.Emoji.iconBad.image
        )
        
        viewController?.present(errorViewController, animated: true)
    }
}
