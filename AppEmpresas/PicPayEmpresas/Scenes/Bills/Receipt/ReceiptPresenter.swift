import Core
import Foundation

protocol ReceiptPresenting: AnyObject {
    var viewController: ReceiptDisplay? { get set }
    func didNextStep(action: ReceiptAction)
    func sections(_ items: [BillSection])
    func showError(_ message: String)
}

final class ReceiptPresenter: ReceiptPresenting {
    private let coordinator: ReceiptCoordinating
    weak var viewController: ReceiptDisplay?

    init(coordinator: ReceiptCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: ReceiptAction) {
        coordinator.perform(action: action)
    }
    
    func sections(_ items: [BillSection]) {
        viewController?.didReceiveReceipt(items)
        viewController?.stopLoading()
    }
    
    func showError(_ message: String) {
        coordinator.perform(action: .error(message))
    }
}
