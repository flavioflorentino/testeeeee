import Foundation
import LegacyPJ

protocol ReceiptViewModelInputs: AnyObject {
    func getReceiptInfo()
    func dissmiss()
}

struct ReceiptStatusHistory: Decodable {
    let version: String
    let value: String
}

struct ReceiptBeneficiary: Decodable {
    enum CodingKeys: String, CodingKey {
        case code
        case name
        case imageUrl = "img_url"
        case limitHour = "limit_hour"
    }

    let code: Int?
    let name: String?
    let imageUrl: String?
    let limitHour: String?
}

struct ReceiptTransaction: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case statusID = "status_id"
        case installments, interest, status
        case walletAmount = "wallet_amount"
        case creditCardAmount = "credit_card_amount"
        case installmentAmount = "installment_amount"
        case surcharge
    }
    
    let id: Int
    let statusID: String
    let installments: Int
    let interest: Int
    let status: String
    let walletAmount: Double
    let creditCardAmount: Int
    let installmentAmount: Int
    let surcharge: Int
}

struct ReceiptRegister: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case billID = "bill_id"
        case status
        case lineCode = "line_code"
        case charges, discounts
        case typePersonOrigin = "type_person_origin"
        case cnpjOrigin = "cnpj_origin"
        case companyNameOrigin = "company_name_origin"
        case tradeNameOrigin = "trade_name_origin"
        case typePersonPayer = "type_person_payer"
        case cnpjPayer = "cnpj_payer"
        case companyNamePayer = "company_name_payer"
        case tradeNamePayer = "trade_name_payer"
        case dueDateTitle = "due_date_title"
        case datePaymentLimitTitle = "date_payment_limit_title"
        case amountTitle = "amount_title"
        case amountAbatementTitle = "amount_abatement_title"
        case discountAmount = "discount_amount"
        case interestAmount = "interest_amount"
        case penaltyAmount = "penalty_amount"
        case totalChargeAmount = "total_charge_amount"
        case updatedAt = "updated_at"
        case createdAt = "created_at"
    }

    let id: Int
    let billID: Int
    let status: String
    let lineCode: String

    let charges: Int?
    let discounts: Int?
    
    let typePersonOrigin: String?
    let cnpjOrigin: String?
    let companyNameOrigin: String?
    let tradeNameOrigin: String?
    let typePersonPayer: String?
    let cnpjPayer: String?
    let companyNamePayer: String?
    let tradeNamePayer: String?
    let dueDateTitle: String?
    let datePaymentLimitTitle: String?
    let amountTitle: Double?
    let amountAbatementTitle: Double?
    let discountAmount: Double?
    let interestAmount: Double?
    let penaltyAmount: Double?
    let totalChargeAmount: Double?
    
    let updatedAt: String
    let createdAt: String
}

struct ReceiptBillCip: Decodable {
    enum CodingKeys: String, CodingKey {
        case isOverdue = "is_overdue"
        case amount
        case canEditValue = "can_edit_value"
        case canReceiveOverdue = "can_receive_overdue"
        case dateSearch = "date_search"
    }

    let isOverdue: Bool
    let amount: Decimal
    let canEditValue: Bool
    let canReceiveOverdue: Bool
    let dateSearch: String
}

struct ReceiptBill: Decodable {
    enum CodingKeys: String, CodingKey {
        case id, status
        case statusID = "status_id"
        case statusHistory = "status_history"
        case transaction
        case billType = "bill_type"
        case beneficiary
        case consumerID = "consumer_id"
        case amountCode = "amount_code"
        case amountPaid = "amount_paid"
        case createdAt = "created_at"
        case dueDate = "due_date"
        case expired
        case barCode = "bar_code"
        case lineCode = "line_code"
        case codeType = "code_type"
        case receiptBillDescription = "description"
        case register, cip
        case payingBank = "paying_bank"
    }

    let id: Int
    let status: String
    let statusID: String
    let statusHistory: [ReceiptStatusHistory]
    let transaction: ReceiptTransaction?
    let billType: String
    let beneficiary: ReceiptBeneficiary
    let consumerID: String?
    let amountCode: Decimal
    let amountPaid: Decimal
    let createdAt: String
    let dueDate: String?
    let expired: Bool
    let barCode: String
    let lineCode: String
    let codeType: String
    let receiptBillDescription: String?
    let register: ReceiptRegister?
    let cip: ReceiptBillCip?
    let payingBank: String?
}

final class ReceiptViewModel {
    typealias Localizable = Strings.Receipt
    private let service: ReceiptServicing
    private let presenter: ReceiptPresenting
    private let style: ReceiptStyle

    init(service: ReceiptServicing, presenter: ReceiptPresenting, style: ReceiptStyle) {
        self.service = service
        self.presenter = presenter
        self.style = style
    }
}

extension ReceiptViewModel: ReceiptViewModelInputs {
    func getReceiptInfo() {
        switch style {
        case .billId(let id):
            receipt(by: id)
        case .billReceipt(let receipt):
            present(receipt)
        }
    }
    
    func dissmiss() {
        presenter.didNextStep(action: .dismiss)
    }
}

private extension ReceiptViewModel {
    func receipt(by id: String) {
        service.getReceipt(id: id) { [weak self] result in
            switch result {
            case .success(let receiptBill):
                self?.present(receiptBill)
            case .failure(.badRequest(let error)):
                self?.presenter.showError(error.message)
            case .failure:
                self?.presenter.showError(LegacyPJError.requestError.message)
            }
        }
    }
    
    func present(_ receiptBill: ReceiptBill?) {
        let firstSection = [BillHeaderCellModel(receiptBill: receiptBill)].compactMap { $0 }
        
        let secondSection = BillDetailCellModel.receiptDetails(receiptBill).compactMap { $0 }
        
        let thirdSection = [
            BillParagraphCellModel(Strings.Receipt.billBarCode, value: receiptBill?.barCode)
        ].compactMap { $0 }

        let sections = [
            BillSection(section: firstSection),
            BillSection(section: secondSection),
            BillSection(section: thirdSection)
        ]
        
        presenter.sections(sections)
    }
}
