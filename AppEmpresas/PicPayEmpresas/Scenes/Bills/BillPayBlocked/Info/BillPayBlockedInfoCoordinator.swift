import UIKit

enum BillPayBlockedInfoAction {
    case dismiss
}

protocol BillPayBlockedInfoCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BillPayBlockedInfoAction)
}

final class BillPayBlockedInfoCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - BillPayBlockedInfoCoordinating
extension BillPayBlockedInfoCoordinator: BillPayBlockedInfoCoordinating {
    func perform(action: BillPayBlockedInfoAction) {
        switch action {
        case .dismiss:
            viewController?.navigationController?.dismiss(animated: true)
        }
    }
}
