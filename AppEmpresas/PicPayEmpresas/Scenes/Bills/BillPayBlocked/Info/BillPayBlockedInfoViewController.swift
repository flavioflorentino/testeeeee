import UI

protocol BillPayBlockedInfoDisplay: AnyObject { }

final class BillPayBlockedInfoViewController: ViewController<BillPayBlockedInfoViewModelInputs, UIView> {
    fileprivate typealias Localizable = Strings.BillPayBlocked
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        return button
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .grayscale750())
        label.numberOfLines = 1
        label.text = Localizable.billPayBlockedInfoTitle
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
        label.text = Localizable.billPayBlockedInfoDescription
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.logStart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceTransparent())
    }

     override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
    }
    
     override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
            $0.bottom.lessThanOrEqualTo(view.compatibleSafeArea.bottom).offset(-Spacing.base03)
        }
    }

     override func configureViews() {
        view.backgroundColor = .backgroundPrimary()
        setupRightNavigationBarButton(closeButton)
    }
}

@objc
private extension BillPayBlockedInfoViewController {
    func didTapClose() {
        viewModel.finish()
    }
}

// MARK: BillPayBlockedInfoDisplay
extension BillPayBlockedInfoViewController: BillPayBlockedInfoDisplay { }
