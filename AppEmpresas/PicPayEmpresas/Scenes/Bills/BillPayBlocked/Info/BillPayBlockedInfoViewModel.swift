import AnalyticsModule
import Foundation

protocol BillPayBlockedInfoViewModelInputs: AnyObject {
    func logStart()
    func finish()
}

final class BillPayBlockedInfoViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let presenter: BillPayBlockedInfoPresenting

    init(presenter: BillPayBlockedInfoPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - BillPayBlockedInfoViewModelInputs
extension BillPayBlockedInfoViewModel: BillPayBlockedInfoViewModelInputs {
    func logStart() {
        dependencies.analytics.log(BillPayBlockedAnalytics.accessDenied)
    }
    
    func finish() {
        presenter.didNextStep(action: .dismiss)
    }
}
