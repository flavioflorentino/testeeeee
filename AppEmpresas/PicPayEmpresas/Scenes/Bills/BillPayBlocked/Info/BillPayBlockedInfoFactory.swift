import Foundation

enum BillPayBlockedInfoFactory {
    static func make() -> BillPayBlockedInfoViewController {
        let container = DependencyContainer()
        let coordinator: BillPayBlockedInfoCoordinating = BillPayBlockedInfoCoordinator()
        let presenter: BillPayBlockedInfoPresenting = BillPayBlockedInfoPresenter(coordinator: coordinator)
        let viewModel = BillPayBlockedInfoViewModel(presenter: presenter, dependencies: container)
        let viewController = BillPayBlockedInfoViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
