import Foundation

protocol BillPayBlockedInfoPresenting: AnyObject {
    var viewController: BillPayBlockedInfoDisplay? { get set }
    func didNextStep(action: BillPayBlockedInfoAction)
}

final class BillPayBlockedInfoPresenter {
    private let coordinator: BillPayBlockedInfoCoordinating
    weak var viewController: BillPayBlockedInfoDisplay?

    init(coordinator: BillPayBlockedInfoCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - BillPayBlockedInfoPresenting
extension BillPayBlockedInfoPresenter: BillPayBlockedInfoPresenting {
    func didNextStep(action: BillPayBlockedInfoAction) {
        coordinator.perform(action: action)
    }
}
