import AnalyticsModule
import Foundation

protocol BillPayBlockedViewModelInputs: AnyObject {
    func logStart()
    func confirm()
    func presentInfo()
}

final class BillPayBlockedViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let presenter: BillPayBlockedPresenting

    init(presenter: BillPayBlockedPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - BillPayBlockedViewModelInputs
extension BillPayBlockedViewModel: BillPayBlockedViewModelInputs {
    func logStart() {
        dependencies.analytics.log(BillPayBlockedAnalytics.accessDenied)
    }
    
    func confirm() {
        presenter.didNextStep(action: .confirm)
    }
    
    func presentInfo() {
        presenter.didNextStep(action: .info)
    }
}
