import Foundation

enum BillPayBlockedFactory {
    static func make() -> BillPayBlockedViewController {
        let container = DependencyContainer()
        let coordinator: BillPayBlockedCoordinating = BillPayBlockedCoordinator()
        let presenter: BillPayBlockedPresenting = BillPayBlockedPresenter(coordinator: coordinator)
        let viewModel = BillPayBlockedViewModel(presenter: presenter, dependencies: container)
        let viewController = BillPayBlockedViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
