import Foundation

protocol BillPayBlockedPresenting: AnyObject {
    var viewController: BillPayBlockedDisplay? { get set }
    func didNextStep(action: BillPayBlockedAction)
}

final class BillPayBlockedPresenter {
    private let coordinator: BillPayBlockedCoordinating
    weak var viewController: BillPayBlockedDisplay?

    init(coordinator: BillPayBlockedCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - BillPayBlockedPresenting
extension BillPayBlockedPresenter: BillPayBlockedPresenting {
    func didNextStep(action: BillPayBlockedAction) {
        coordinator.perform(action: action)
    }
}
