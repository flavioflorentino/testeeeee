import UIKit

enum BillPayBlockedAction {
    case confirm
    case info
}

protocol BillPayBlockedCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BillPayBlockedAction)
}

final class BillPayBlockedCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - BillPayBlockedCoordinating
extension BillPayBlockedCoordinator: BillPayBlockedCoordinating {
    func perform(action: BillPayBlockedAction) {
        switch action {
        case .confirm:
            viewController?.navigationController?.popViewController(animated: true)
        case .info:
            let controller = BillPayBlockedInfoFactory.make()
            let navigation = UINavigationController(rootViewController: controller)
            navigation.modalPresentationStyle = .fullScreen
            viewController?.navigationController?.present(navigation, animated: true)
        }
    }
}
