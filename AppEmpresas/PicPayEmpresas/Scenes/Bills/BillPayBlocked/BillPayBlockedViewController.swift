import UI

protocol BillPayBlockedDisplay: AnyObject { }

private extension BillPayBlockedViewController.Layout {
    enum Image {
        static let size: CGFloat = 90.0
    }
}

final class BillPayBlockedViewController: ViewController<BillPayBlockedViewModelInputs, UIView> {
    fileprivate typealias Localizable = Strings.BillPayBlocked
    fileprivate enum Layout { }
    
    private lazy var blockImageView: UIImageView = {
        let image = Assets.imgFailure.image
        let imageView = UIImageView()
        imageView.image = Assets.imgWarning.image
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale750())
        label.numberOfLines = 1
        label.text = Localizable.billPayBlockedTitle
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale600())
        label.text = Localizable.billPayBlockedDescription
        return label
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            blockImageView,
            SpacerView(size: Spacing.base04),
            titleLabel,
            SpacerView(size: Spacing.base02),
            descriptionLabel
        ])
        stackView.axis = .vertical
        stackView.alignment = .center
        return stackView
    }()
    
    private lazy var okButton: UIButton = {
        let button = UIButton()
        button
            .buttonStyle(PrimaryButtonStyle())
            .with(\.textAlignment, .center)
        button.setTitle(Localizable.billPayBlockedOk, for: .normal)
        button.addTarget(self, action: #selector(didTapOk), for: .touchUpInside)
        return button
    }()
    
    private lazy var infoButton: UIButton = {
        let button = UIButton()
        button
            .buttonStyle(LinkButtonStyle())
            .with(\.textAlignment, .center)
        button.setTitle(Localizable.billPayBlockedInfoTitle, for: .normal)
        button.addTarget(self, action: #selector(didTapInfo), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.logStart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceTransparent())
        navigationController?.navigationBar.tintColor = Colors.branding400.color
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceLegacy())
    }

     override func buildViewHierarchy() {
        view.addSubview(contentStackView)
        view.addSubview(okButton)
        view.addSubview(infoButton)
    }
    
     override func setupConstraints() {
        blockImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Image.size)
        }
        
        contentStackView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.centerY.equalToSuperview().offset(-Spacing.base05)
        }
        
        okButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.top.equalTo(contentStackView.snp.bottom).offset(Spacing.base02)
        }
        
        infoButton.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(okButton.snp.bottom).offset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
            $0.centerX.equalToSuperview()
        }
    }

     override func configureViews() {
        let backButton = UIBarButtonItem()
        backButton.title = String()
        
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        view.backgroundColor = .backgroundPrimary()
    }
}

@objc
private extension BillPayBlockedViewController {
    func didTapOk() {
        viewModel.confirm()
    }
    
    func didTapInfo() {
        viewModel.presentInfo()
    }
}

// MARK: BillPayBlockedDisplay
extension BillPayBlockedViewController: BillPayBlockedDisplay { }
