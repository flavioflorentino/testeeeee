import AnalyticsModule

enum BillPayBlockedAnalytics: AnalyticsKeyProtocol {
    case accessDenied
    case helpAccessed

    private var name: String {
        switch self {
        case .accessDenied:
            return "2FA - Access Denied"
        case .helpAccessed:
            return "2FA - Help Accessed"
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name,
                       properties: ["flow": "bill-payment"],
                       providers: [.mixPanel])
    }
}
