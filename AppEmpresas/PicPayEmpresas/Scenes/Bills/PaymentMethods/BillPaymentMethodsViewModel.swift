import AnalyticsModule
import Foundation
import LegacyPJ

protocol BillPaymentMethodsViewModelInputs: AnyObject {
    func openTypeMethod()
    func openScannerMethod()
}

final class BillPaymentMethodsViewModel {
    typealias Dependencies = HasAuthManager & HasAnalytics

    private let presenter: BillPaymentMethodsPresenting
    private let dependencies: Dependencies

    init(presenter: BillPaymentMethodsPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - BillPaymentMethodsViewModelInputs
extension BillPaymentMethodsViewModel: BillPaymentMethodsViewModelInputs {
    func openTypeMethod() {
        fireAnalytics(type: .type)
        presenter.didNextStep(action: .type)
    }
    
    func openScannerMethod() {
        fireAnalytics(type: .scanner)
        presenter.didNextStep(action: .scanner)
    }
}

private extension BillPaymentMethodsViewModel {
    func fireAnalytics(type: BillPaymentMethodsAction) {
        guard let sellerId = dependencies.authManager.user?.id else {
            return
        }
        
        switch type {
        case .scanner:
            dependencies.analytics.log(BillPaymentMethodsAnalytics.didTapScanner(sellerId: sellerId))
        case .type:
            dependencies.analytics.log(BillPaymentMethodsAnalytics.didTapType(sellerId: sellerId))
        }
    }
}
