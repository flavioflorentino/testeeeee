import UI

private extension BillPaymentMethodsViewController.Layout {
    enum Size {
        static let headerAspect: CGFloat = 0.562_5
    }
}

final class BillPaymentMethodsViewController: ViewController<BillPaymentMethodsViewModelInputs, UIView> {
    fileprivate typealias Localizable = Strings.BillPaymentMethods
    fileprivate enum Layout {}
    
    private lazy var header: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.image = Assets.BillPaymentMethods.header.image
        return image
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.title
        return label
    }()
    
    private lazy var subTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.subTitle
        return label
    }()
    
    private lazy var scannerMethodView = BillPaymentMethodCardView()
    
    private lazy var typeMethodView = BillPaymentMethodCardView()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceTransparent())
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceLegacy())
    }
 
    override func buildViewHierarchy() {
        view.addSubview(header)
        view.addSubview(titleLabel)
        view.addSubview(subTitleLabel)
        view.addSubview(scannerMethodView)
        view.addSubview(typeMethodView)
    }
    
    override func configureViews() {
        let backButton = UIBarButtonItem()
        backButton.title = String()
        
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        scannerMethodView.set(
            title: Localizable.scanner,
            icon: Assets.BillPaymentMethods.cam.image,
            delegate: self
        )
        
        typeMethodView.set(
            title: Localizable.typed,
            icon: Assets.BillPaymentMethods.typecode.image,
            delegate: self
        )
    }
    
    override func setupConstraints() {
        header.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
            $0.height.equalTo(view.snp.width).multipliedBy(Layout.Size.headerAspect)
        }
        
        titleLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(header.snp.bottom).offset(Spacing.base03)
        }
        
        subTitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        scannerMethodView.snp.makeConstraints {
            $0.top.equalTo(subTitleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        typeMethodView.snp.makeConstraints {
            $0.top.equalTo(scannerMethodView.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    override func configureStyles() {
        view.backgroundColor = Colors.backgroundSecondary.color
        
        titleLabel.labelStyle(TitleLabelStyle(type: .xLarge))
        
        subTitleLabel.labelStyle(BodySecondaryLabelStyle())
    }
}

extension BillPaymentMethodsViewController: BillPaymentTypeViewDelegate {
    func didSelect(_ view: BillPaymentMethodCardView) {
        if view == scannerMethodView {
            viewModel.openScannerMethod()
        } else if view == typeMethodView {
            viewModel.openTypeMethod()
        }
    }
}
