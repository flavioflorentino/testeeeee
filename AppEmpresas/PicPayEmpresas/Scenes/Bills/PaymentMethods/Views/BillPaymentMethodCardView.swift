import UI

protocol BillPaymentTypeViewDelegate: AnyObject {
    func didSelect(_ view: BillPaymentMethodCardView)
}

private extension BillPaymentMethodCardView.Layout {
    enum Size {
        static let height: CGFloat = 80
        static let icon: CGFloat = 50
        static let disclosure: CGFloat = 14
    }
    
    enum Shadow {
        static let radius: CGFloat = 2
        static let opacity: Float = 0.05
        static let color = Colors.black.color.cgColor
        static let offset = CGSize(width: 0, height: 2)
    }
}

final class BillPaymentMethodCardView: UIView {
    fileprivate enum Layout { }
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle(cornerRadius: .light))
            .with(\.backgroundColor, .backgroundPrimary())
        return view
    }()
    
    private lazy var icon: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var typeLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var disclosure: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Assets.BillPaymentMethods.disclosure.image
        return imageView
    }()
    
    weak var delegate: BillPaymentTypeViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(
        title: String,
        icon: UIImage,
        delegate: BillPaymentTypeViewDelegate
    ) {
        self.typeLabel.text = title
        self.icon.image = icon
        self.delegate = delegate
    }
}

private extension BillPaymentMethodCardView {
    @objc
    func didSelect() {
        delegate?.didSelect(self)
    }
    
    func addShadow() {
        layer.shadowColor = Layout.Shadow.color
        layer.shadowOpacity = Layout.Shadow.opacity
        layer.shadowRadius = Layout.Shadow.radius
        layer.shadowOffset = Layout.Shadow.offset
    }
}

extension BillPaymentMethodCardView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(contentView)
        contentView.addSubview(icon)
        contentView.addSubview(typeLabel)
        contentView.addSubview(disclosure)
    }
    
    func setupConstraints() {
        snp.makeConstraints {
            $0.height.equalTo(Layout.Size.height)
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        icon.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.centerY.equalToSuperview()
            $0.size.equalTo(Layout.Size.icon)
        }
        
        typeLabel.snp.makeConstraints {
            $0.leading.equalTo(icon.snp.trailing).offset(Spacing.base00)
            $0.centerY.equalToSuperview()
        }
        
        disclosure.snp.makeConstraints {
            $0.leading.equalTo(typeLabel.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalTo(contentView).inset(Spacing.base01)
            $0.size.equalTo(Layout.Size.disclosure)
            $0.centerY.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        addShadow()
        isUserInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didSelect))
        addGestureRecognizer(tapGesture)
    }
}
