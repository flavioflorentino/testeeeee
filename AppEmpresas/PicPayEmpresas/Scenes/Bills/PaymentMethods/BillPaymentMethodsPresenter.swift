import Foundation

protocol BillPaymentMethodsPresenting: AnyObject {
    func didNextStep(action: BillPaymentMethodsAction)
}

final class BillPaymentMethodsPresenter {
    private let coordinator: BillPaymentMethodsCoordinating

    init(coordinator: BillPaymentMethodsCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - BillPaymentMethodsPresenting
extension BillPaymentMethodsPresenter: BillPaymentMethodsPresenting {
    func didNextStep(action: BillPaymentMethodsAction) {
        coordinator.perform(action: action)
    }
}
