import AnalyticsModule
import Foundation

enum BillPaymentMethodsAnalytics: AnalyticsKeyProtocol {
    case didTapScanner(sellerId: Int)
    case didTapType(sellerId: Int)
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .didTapType(let sellerId):
            return getEvent(for: "Bill - Options type", and: sellerId, isScanner: false)
        case .didTapScanner(let sellerId):
            return getEvent(for: "Bill - Options type", and: sellerId, isScanner: true)
        }
    }
}

private extension BillPaymentMethodsAnalytics {
    func getEvent(
        for name: String,
        and sellerId: Int,
        isScanner: Bool
    ) -> AnalyticsEvent {
        let properties: [String: Any] = [
            "seller_id": sellerId,
            "code_reader": isScanner,
            "code_type": !isScanner
        ]
        
        return AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
    }
}
