import UIKit

enum BillPaymentMethodsAction {
    case scanner
    case type
}

protocol BillPaymentMethodsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BillPaymentMethodsAction)
}

final class BillPaymentMethodsCoordinator: BillPaymentMethodsCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: BillPaymentMethodsAction) {
        let controller: UIViewController
        switch action {
        case .scanner:
            controller = ScannerFactory.make()
        case .type:
            controller = BillFormFactory.make()
        }
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
