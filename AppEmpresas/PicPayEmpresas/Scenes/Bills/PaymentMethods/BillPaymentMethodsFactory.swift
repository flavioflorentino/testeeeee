import Foundation

enum BillPaymentMethodsFactory {
    static func make() -> BillPaymentMethodsViewController {
        let dependencies = DependencyContainer()
        let coordinator: BillPaymentMethodsCoordinating = BillPaymentMethodsCoordinator()
        let presenter: BillPaymentMethodsPresenting = BillPaymentMethodsPresenter(coordinator: coordinator)
        let viewModel = BillPaymentMethodsViewModel(presenter: presenter, dependencies: dependencies)
        let viewController = BillPaymentMethodsViewController(viewModel: viewModel)

        coordinator.viewController = viewController

        return viewController
    }
}
