struct Bill2FAResponse: Decodable {
    let detail: Bill2FADetailResponse
    let status: String
    let success: Bool
}

struct Bill2FADetailResponse: Decodable {
    let mobilePhone: String?
    let method: Bill2FAStatusResponse
}

enum Bill2FAStatusResponse: String, Decodable {
    case sms
    case password
}

enum Bill2FAStatusError {
    case tokenExpired
    case tokenInvalid
    case blocked
    case lastChance
    
    init?(rawValue: String?) {
        switch rawValue {
        case "tfa_token_expired":
            self = .tokenExpired
        case "tfa_token_invalid":
            self = .tokenInvalid
        case "tfa_token_blocked":
            self = .blocked
        case "tfa_token_last_try":
            self = .lastChance
        default:
            return nil
        }
    }
}
