import AnalyticsModule
import Core
import Foundation

struct PayBillet2FAParams {
    let code: String
    let amount: Double
    let billDescription: String
    let token: String
    
    init(from billet: BillPay, token: String) {
        code = billet.code
        amount = billet.totalValue
        billDescription = billet.description ?? ""
        self.token = token
    }
}

public protocol Bill2FADelegate: AnyObject {
    func factorAuthenticatorPassword()
    func resultSMSFactor(id: String)
    func finishFlow()
}

protocol Bill2FAViewModelInputs: Bill2FAViewDelegate {
    func prepareForReceiveCode()
    func resetStatus()
    func verifyCode(_ code: String)
    func resendCode()
    func finish()
    func logBeginTyping()
}

final class Bill2FAViewModel {
    private let service: Bill2FAServicing
    private let presenter: Bill2FAPresenting
    private var billet: BillPay
    private var phone: String
    private var isLastChance = false
    private var startedDate = Date()
    private var isTypingLogSent = false
    private let dependencies: HasAnalytics = DependencyContainer()
    private weak var delegate: Bill2FADelegate?
    
    init(
        service: Bill2FAServicing,
        presenter: Bill2FAPresenting,
        billet: BillPay,
        phone: String,
        delegate: Bill2FADelegate?
    ) {
        self.service = service
        self.presenter = presenter
        self.billet = billet
        self.phone = phone
        self.delegate = delegate
    }
}

// MARK: - Bill2FAViewModelInputs

extension Bill2FAViewModel: Bill2FAViewModelInputs {
    func prepareForReceiveCode() {
        presenter.displayRegular(phone)
        prepareTimer()
    }
    
    func resetStatus() {
        presenter.resetStatus()
    }
    
    func verifyCode(_ code: String) {
        presenter.displayLoading()
        let params = PayBillet2FAParams(from: billet, token: code)
        service.payBillet(params: params) { result in
            self.presenter.dismissLoading()
            switch result {
            case .success(let response):
                guard let data = response.data else {
                    self.presenter.displayError()
                    self.logBill2FAError(.decode)
                    return
                }
                self.logBill2FA(.success(.sms, id: data.id))
                self.presenter.didNextStep(action: .dismiss)
                self.delegate?.resultSMSFactor(id: data.id)
            case .failure(let error):
                self.handleVerifyCodeError(error)
            }
        }
    }
    
    func resendCode() {
        presenter.resetStatus()
        presenter.displayResendLoading()
        service.sendSMS { result in
            self.presenter.dismissResendLoading()
            switch result {
            case .success(let response):
                self.verifyStatus(status: response)
                self.logBill2FA(.codeResended)
            case .failure:
                self.presenter.displayResendCodeError()
                self.presenter.enableResend()
                self.logBill2FAError(.requestFailed)
            }
        }
    }
    
    func finish() {
        logBill2FA(.canceled(.sms))
        presenter.didNextStep(action: .dismiss)
        delegate?.finishFlow()
    }
    
    func logBeginTyping() {
        if !isTypingLogSent {
            isTypingLogSent = true
            let timeInterval = Date().timeIntervalSince(startedDate)
            let seconds = Int(timeInterval.truncatingRemainder(dividingBy: 60))
            let time = "\(seconds)"
            logBill2FA(.deliveryTime(time: time))
        }
    }
}

private extension Bill2FAViewModel {
    func handleVerifyCodeError(_ error: ApiError) {
        presenter.displayKeyboard()
        switch error {
        case .connectionFailure, .timeout:
            self.presenter.displayNetworkError()
            logBill2FAError(.connection)
        case .badRequest(let body):
            guard
                let data = body.jsonData,
                let body = try? JSONDecoder().decode(ResponseBiz<NoContent>.self, from: data)
                else {
                    presenter.displayError()
                    logBill2FAError(.badRequestDecode)
                    return
            }
            handleVerifyCodeMetaError(body.meta)
        default:
            presenter.displayError()
            logBill2FAError(.generic)
        }
    }
    
    func handleVerifyCodeMetaError(_ meta: Meta?) {
        guard
            let errorType = meta?.errorType,
            let status = Bill2FAStatusError(rawValue: errorType)
            else {
                presenter.showApiError(title: meta?.errorTitle, description: meta?.errorMessage)
                logBill2FAError(.requestFailed)
                return
        }
        switch status {
        case .tokenExpired:
            presenter.displayExpired()
            presenter.dismissKeyboard()
            logBill2FA(.codeExpired)
        case .tokenInvalid:
            presenter.displayInvalid()
            logBill2FA(.codeIncorrect)
        case .blocked:
            let status = Bill2FAAction.blocked(delegate: self)
            presenter.didNextStep(action: status)
            logBill2FA(.blocked)
        case .lastChance:
            isLastChance = true
            presenter.displayLastChance(phone)
        }
    }
    
    func verifyStatus(status: Bill2FAResponse) {
        let detail = status.detail
        guard
            detail.method == .sms,
            let phone = detail.mobilePhone
            else {
                presenter.didNextStep(action: .dismiss)
                delegate?.factorAuthenticatorPassword()
                return
        }
        
        self.phone = phone
        if isLastChance {
            presenter.displayLastChance(phone)
        } else {
            presenter.displayRegular(phone)
        }
        
        prepareTimer()
    }
    
    func prepareTimer() {
        var time = 60
        presenter.displayTime(time)
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            guard time > 1 else {
                self.presenter.enableResend()
                timer.invalidate()
                return
            }
            time -= 1
            self.presenter.displayTime(time)
        }
    }
    
    func logBill2FA(_ event: Bill2FAAnalytics) {
        dependencies.analytics.log(event)
    }
    
    func logBill2FAError(_ error: Bill2FAError) {
        logBill2FA(.error(error))
    }
    
    func logBill2FASuccess(_ id: String) {
        logBill2FA(.success(.sms, id: id))
    }
}

extension Bill2FAViewModel: Bill2FABlockedDelegate {
    func finishFlow() {
        finish()
    }
}
