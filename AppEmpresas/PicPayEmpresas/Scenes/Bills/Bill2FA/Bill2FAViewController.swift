import SnapKit
import UI

protocol Bill2FADisplay: AnyObject {
    func resetStatus()
    func displayLoading()
    func dismissLoading()
    func displayResendLoading(text: NSAttributedString)
    func dismissResendLoading()
    func displayError(_ message: String)
    func displayTitle(title: String)
    func displayDescription(description: NSAttributedString)
    func displayTime(time: NSAttributedString)
    func displayKeyboard()
    func dismissKeyboard()
    func enableResend()
    func withoutNetwork()
}

final class Bill2FAViewController: ViewController<Bill2FAViewModelInputs, UIView> {
    fileprivate typealias Localizable = Strings.Bill2FA
    fileprivate enum Layout { }
    
    private lazy var mainView: Bill2FAView = {
        let view = Bill2FAView()
        view.delegate = viewModel
        return view
    }()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var networkView = FacialBiometricsErrorView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addObservers()
        viewModel.prepareForReceiveCode()
    }

    override func buildViewHierarchy() {
        view.addSubview(mainView)
    }
    
    override func setupConstraints() {
        mainView.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview()
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
        }
    }

    override func configureViews() {
        view.backgroundColor = .backgroundPrimary()
        title = Localizable.viewTitle
        setupRightNavigationBarButton(closeButton)
    }
    
    private func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: UIWindow.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIWindow.keyboardWillHideNotification,
            object: nil
        )
    }
}

@objc
extension Bill2FAViewController {
    func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
            let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            else {
                return
        }
        
        updateKeyboardConstraint(userInfo: userInfo, inset: keyboardSize.size.height)
    }
    
    func keyboardWillHide(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        updateKeyboardConstraint(userInfo: userInfo, inset: 0.0)
    }
    
    private func updateKeyboardConstraint(userInfo: [AnyHashable: Any], inset: CGFloat) {
        guard let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
            let animationCurve = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue
            else {
                return
        }
        
        mainView.snp.updateConstraints {
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(inset)
        }
        
        UIView.animate(
            withDuration: animationDuration,
            delay: 0,
            options: UIView.AnimationOptions(rawValue: animationCurve),
            animations: {
                self.view.layoutIfNeeded()
            },
            completion: nil
        )
    }
    
    func didTapCloseButton(_ sender: UIButton) {
        viewModel.finish()
    }
}

// MARK: Bill2FADisplay
extension Bill2FAViewController: Bill2FADisplay {
    func resetStatus() {
        mainView.resetStatus()
    }
    
    func displayLoading() {
        mainView.displayLoading()
        mainView.dismissKeyboard()
    }
    
    func dismissLoading() {
        mainView.dismissLoading()
    }
    
    func displayError(_ message: String) {
        mainView.displayError(message)
        mainView.displayKeyboard()
    }
    
    func displayResendLoading(text: NSAttributedString) {
        mainView.displayResendLoading(message: text)
    }
    
    func dismissResendLoading() {
        mainView.dismissResendLoading()
    }
    
    func displayTitle(title: String) {
        mainView.displayTitle(title: title)
    }
    
    func displayDescription(description: NSAttributedString) {
        mainView.displayDescription(description: description)
    }
    
    func displayTime(time: NSAttributedString) {
        mainView.displayTime(time: time)
    }
    
    func displayKeyboard() {
        mainView.displayKeyboard()
    }
    
    func dismissKeyboard() {
        mainView.dismissKeyboard()
    }
    
    func enableResend() {
        mainView.enableResend()
    }
    
    func withoutNetwork() {
        if view.subviews.contains(networkView) {
            return
        }
        let topConstraint = networkView.topAnchor.constraint(
            equalTo: view.compatibleSafeAreaLayoutGuide.topAnchor,
            constant: -Spacing.base10
        )
        
        networkView.setup(with: Localizable.billSMSNetworkError) { [weak self] _ in
            guard let self = self else {
                return
            }
            
            topConstraint.constant = -Spacing.base10
            UIView.animate(
                withDuration: 0.3,
                animations: self.alertErrorViewAnimated,
                completion: self.finishAlertErrorViewAnimated(_:)
            )
        }
        
        view.addSubview(networkView)
        
        networkView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        topConstraint.isActive = true
        view.layoutIfNeeded()
        
        topConstraint.constant = Spacing.base02
        UIView.animate(withDuration: 0.3) {
            self.alertErrorViewAnimated()
        }
    }
    
    private func alertErrorViewAnimated() {
        view.layoutIfNeeded()
    }
    
    private func finishAlertErrorViewAnimated(_ finish: Bool) {
        networkView.removeFromSuperview()
    }
}
