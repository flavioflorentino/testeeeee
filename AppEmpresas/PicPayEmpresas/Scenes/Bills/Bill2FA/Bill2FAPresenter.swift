import Core
import UI

private extension Bill2FAPresenter.Layout {
    enum Fonts {
        static let sizeDefault: CGFloat = 16.0
    }
}

protocol Bill2FAPresenting: AnyObject {
    var viewController: Bill2FADisplay? { get set }
    func didNextStep(action: Bill2FAAction)
    func resetStatus()
    func displayRegular(_ number: String)
    func displayLastChance(_ number: String)
    func displayTime(_ time: Int)
    func displayKeyboard()
    func dismissKeyboard()
    func displayLoading()
    func dismissLoading()
    func displayResendLoading()
    func dismissResendLoading()
    func displayError()
    func displayInvalid()
    func displayExpired()
    func displayResendCodeError()
    func showApiError(title: String?, description: String?)
    func enableResend()
    func displayNetworkError()
}

final class Bill2FAPresenter {
    fileprivate typealias Localizable = Strings.Bill2FA
    fileprivate enum Layout { }

    private let coordinator: Bill2FACoordinating
    weak var viewController: Bill2FADisplay?

    init(coordinator: Bill2FACoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - Bill2FAPresenting
extension Bill2FAPresenter: Bill2FAPresenting {
    func didNextStep(action: Bill2FAAction) {
        coordinator.perform(action: action)
    }
    
    func resetStatus() {
        viewController?.resetStatus()
    }
    
    func displayRegular(_ number: String) {
        let title = Localizable.billSMSTitle
        let message = Localizable.billSMSDescription(number)
        let description = formatMessage(with: message)
        displayMessages(title: title, description: description)
    }
    
    func displayLastChance(_ number: String) {
        let title = Localizable.billSMSLastChanceTitle
        let message = Localizable.billSMSLastChanceDescription(number)
        let description = formatMessage(with: message)
        displayMessages(title: title, description: description)
    }
    
    private func displayMessages(title: String, description: NSAttributedString) {
        viewController?.displayTitle(title: title)
        viewController?.displayDescription(description: description)
        viewController?.displayKeyboard()
    }
    
    func displayTime(_ time: Int) {
        let message = Localizable.billSMSTimer(time)
        let attr = [NSAttributedString.Key.foregroundColor: Colors.grayscale400.color]
        let attributeMessage = formatMessage(with: message, attributes: attr)
        viewController?.displayTime(time: attributeMessage)
    }
    
    func displayKeyboard() {
        viewController?.displayKeyboard()
    }
    
    func dismissKeyboard() {
        viewController?.dismissKeyboard()
    }
    
    func displayLoading() {
        viewController?.displayLoading()
    }

    func dismissLoading() {
        viewController?.dismissLoading()
    }
    
    func displayError() {
        viewController?.displayError(Localizable.billSMSError)
    }

    func displayInvalid() {
        viewController?.displayError(Localizable.billSMSInvalid)
    }
    
    func displayExpired() {
        viewController?.displayError(Localizable.billSMSExpiredCode)
    }
    
    func displayResendCodeError() {
        showError(message: Localizable.billSMSSendCodeError, title: Localizable.billSMSErrorTitle)
    }
    
    func showApiError(title: String?, description: String?) {
        showError(message: description ?? Localizable.billSMSError, title: title)
    }
    
    func displayResendLoading() {
        let attr = [NSAttributedString.Key.foregroundColor: Colors.grayscale500.color]
        let attributeMessage = NSAttributedString(string: Localizable.billSMSWait, attributes: attr)
        viewController?.displayResendLoading(text: attributeMessage)
    }
    
    func dismissResendLoading() {
        viewController?.dismissResendLoading()
    }
    
    func enableResend() {
        viewController?.enableResend()
    }
    
    func displayNetworkError() {
        viewController?.withoutNetwork()
    }
}

// MARK: - Private Methods

private extension Bill2FAPresenter {
    func formatMessage(with message: String, attributes: [NSAttributedString.Key: Any] = [:]) -> NSAttributedString {
        let attributeMessage = NSAttributedString(string: message, attributes: attributes)
            .boldfyWithSystemFont(ofSize: Layout.Fonts.sizeDefault, weight: UIFont.Weight.bold.rawValue)
        return attributeMessage
    }
    
    func showError(message: String, title: String? = nil) {
        let modalImage = AlertModalInfoImage(
            image: Assets.Emoji.iconBad.image
        )
        
        let modalInfo = AlertModalInfo(
            imageInfo: modalImage,
            title: title,
            subtitle: message,
            buttonTitle: Localizable.billSMSOk
        )
        
        coordinator.perform(action: .resend(modalInfo: modalInfo))
    }
}
