import SnapKit
import UI

protocol Bill2FAViewDelegate: AnyObject {
    func resendCode()
    func resetStatus()
    func verifyCode(_ code: String)
    func logBeginTyping()
}

private extension Bill2FAView.Layout {
    enum Fonts {
        static let `default` = UIFont.systemFont(ofSize: 24.0)
    }
}

final class Bill2FAView: UIView {
    fileprivate typealias Localizable = Strings.Bill2FA
    fileprivate enum Layout { }
    
    weak var delegate: Bill2FAViewDelegate?
    
    private lazy var scrollView = UIScrollView()
    private lazy var containerView = UIView()
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        return label
    }()
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        return label
    }()
    private lazy var loadingView = UIActivityIndicatorView(style: .gray)
    private lazy var codeTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.billSMSPlaceholder
        textField.placeholderColor = Colors.grayscale400.color
        textField.title = Localizable.billSMSPlaceholder
        textField.titleColor = Colors.grayscale400.color
        textField.selectedTitle = Localizable.billSMSPlaceholder
        textField.selectedTitleColor = Colors.branding600.color
        textField.textColor = Colors.grayscale750.color
        textField.font = Layout.Fonts.default
        textField.keyboardType = .numberPad
        if #available(iOS 12.0, *) {
            textField.textContentType = .oneTimeCode
        }
        textField.layer.borderWidth = Border.none
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return textField
    }()
    private lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.isHidden = true
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .critical900())
            .with(\.textAlignment, .left)
        return label
    }()
    private lazy var codeStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [codeTextField, errorLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            titleLabel,
            SpacerView(size: Spacing.base02),
            descriptionLabel,
            SpacerView(size: Spacing.base03),
            codeStackView
        ])
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        return stackView
    }()
    private lazy var timerButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.billSMSResend, for: .normal)
        button
            .buttonStyle(LinkButtonStyle())
            .with(\.typography, .bodyPrimary())
            .with(\.textAlignment, .center)
        button.addTarget(self, action: #selector(didTapResend), for: .touchUpInside)
        return button
    }()
    private lazy var resendLoading = UIActivityIndicatorView(style: .gray)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

@objc
extension Bill2FAView {
    func didTapResend(_ sender: UIButton) {
        delegate?.resendCode()
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        delegate?.resetStatus()

        let codeSize = 6
        let code = codeTextField.text
        codeTextField.text = code?
            .prefix(codeSize)
            .trimmingCharacters(in: .letters)
            .trimmingCharacters(in: .whitespacesAndNewlines)
        
        delegate?.logBeginTyping()
        if let code = codeTextField.text, code.count == codeSize {
           sendCode(code)
        }
    }
    
    private func sendCode(_ code: String) {
        delegate?.verifyCode(code)
    }
}

extension Bill2FAView {
    func resetStatus() {
        errorStatus(false)
    }
    
    func displayLoading() {
        loadingView.startAnimating()
    }
    
    func dismissLoading() {
        loadingView.stopAnimating()
    }
    
    func displayResendLoading(message: NSAttributedString) {
        timerButton.setAttributedTitle(message, for: .disabled)
        timerButton.isEnabled = false

        shouldDisplayResendLoading(true)
    }
    
    func dismissResendLoading() {
        shouldDisplayResendLoading(false)
    }
    
    func displayError(_ message: String) {
        errorStatus(true, with: message)
    }
    
    func displayTitle(title: String) {
        titleLabel.text = title
    }
    
    func displayDescription(description: NSAttributedString) {
        descriptionLabel.attributedText = description
    }
    
    func displayTime(time: NSAttributedString) {
        timerButton.setAttributedTitle(time, for: .disabled)
        timerButton.isEnabled = false
    }
    
    func displayKeyboard() {
        codeTextField.isUserInteractionEnabled = true
        codeTextField.becomeFirstResponder()
        scrollView.flashScrollIndicators()
    }
    
    func dismissKeyboard() {
        codeTextField.resignFirstResponder()
        codeTextField.isUserInteractionEnabled = false
    }
    
    func enableResend() {
        timerButton.isEnabled = true
    }
    
    private func errorStatus(_ hasError: Bool, with message: String = "") {
        if hasError {
            codeTextField.lineColor = Colors.critical900.color
            codeTextField.selectedLineColor = Colors.critical900.color
            codeTextField.titleColor = Colors.critical900.color
            codeTextField.selectedTitleColor = Colors.critical900.color
        } else {
            codeTextField.lineColor = Colors.grayscale400.color
            codeTextField.selectedLineColor = Colors.branding300.color
            codeTextField.titleColor = Colors.grayscale400.color
            codeTextField.selectedTitleColor = Colors.branding600.color
        }
        
        errorLabel.isHidden = !hasError
        errorLabel.text = message
        
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
    private func shouldDisplayResendLoading(_ isLoading: Bool) {
        isLoading
            ? resendLoading.startAnimating()
            : resendLoading.stopAnimating()
        let spacingForLoading = Spacing.base01 + resendLoading.size.width
        let rightMargin = isLoading
            ? spacingForLoading
            : 0
        timerButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: rightMargin)
    }
}

extension Bill2FAView: ViewConfiguration {
    public func buildViewHierarchy() {
        containerView.addSubview(containerStackView)
        containerView.addSubview(timerButton)
        timerButton.addSubview(resendLoading)
        codeTextField.addSubview(loadingView)
        scrollView.addSubview(containerView)
        addSubview(scrollView)
    }
    
    func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.leading.top.trailing.bottom.equalToSuperview()
        }
        containerView.snp.makeConstraints {
            $0.leading.top.trailing.bottom.equalToSuperview()
            $0.height.equalToSuperview().priority(.low)
            $0.width.equalToSuperview()
        }
        containerStackView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base03)
        }
        loadingView.snp.makeConstraints {
            $0.trailing.equalToSuperview()
            $0.bottom.equalToSuperview().inset(Spacing.base01)
        }
        resendLoading.snp.makeConstraints {
            $0.trailing.centerY.equalToSuperview()
        }
        timerButton.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(codeStackView.snp.bottom).offset(Spacing.base04)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
    }
}
