import Core

enum Bill2FAEndpoint {
    case sendSMS
    case payBillet(params: PayBillet2FAParams)
}

extension Bill2FAEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .sendSMS:
            return "/tfa/seller"
        case .payBillet:
            return "/transaction/pay-bill"
        }
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var customHeaders: [String: String] {
        guard case let .payBillet(params) = self else {
            return [:]
        }
        return ["tfa-token": params.token]
    }
    
    var body: Data? {
        guard case let .payBillet(params) = self else {
            return nil
        }
        return [
            "code": params.code,
            "amount": params.amount,
            "description": params.billDescription
        ].toData()
    }
}
