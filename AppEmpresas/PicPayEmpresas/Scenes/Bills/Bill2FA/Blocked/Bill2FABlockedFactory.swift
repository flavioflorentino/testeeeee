import Foundation

enum Bill2FABlockedFactory {
    static func make(delegate: Bill2FABlockedDelegate?) -> Bill2FABlockedViewController {
        let coordinator: Bill2FABlockedCoordinating = Bill2FABlockedCoordinator()
        let presenter: Bill2FABlockedPresenting = Bill2FABlockedPresenter(coordinator: coordinator)
        let viewModel = Bill2FABlockedViewModel(presenter: presenter, delegate: delegate)
        let viewController = Bill2FABlockedViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
