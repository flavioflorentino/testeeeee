import UI

protocol Bill2FABlockedPresenting: AnyObject {
    var viewController: Bill2FABlockedDisplay? { get set }
    func didNextStep(action: Bill2FABlockedAction)
}

final class Bill2FABlockedPresenter {
    private let coordinator: Bill2FABlockedCoordinating
    weak var viewController: Bill2FABlockedDisplay?

    init(coordinator: Bill2FABlockedCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - Bill2FABlockedPresenting
extension Bill2FABlockedPresenter: Bill2FABlockedPresenting {
    func didNextStep(action: Bill2FABlockedAction) {
        coordinator.perform(action: action)
    }
}
