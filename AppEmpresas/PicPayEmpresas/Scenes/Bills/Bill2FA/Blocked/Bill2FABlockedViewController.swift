import UI

protocol Bill2FABlockedDisplay: AnyObject { }

private extension Bill2FABlockedViewController.Layout {
    enum Image {
        static let size = 90.0
    }
}

final class Bill2FABlockedViewController: ViewController<Bill2FABlockedViewModelInputs, UIView> {
    fileprivate typealias Localizable = Strings.Bill2FA
    fileprivate enum Layout { }
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(finishFlow), for: .touchUpInside)
        return button
    }()
    
    private lazy var statusImageView: UIImageView = {
        let image = Assets.imgFailure.image
        let imageView = UIImageView()
        imageView.image = Assets.imgFailure.image
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale750())
        label.numberOfLines = 2
        label.text = Localizable.billSMSBlockTitle
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale750())
        label.text = Localizable.billSMSBlockDescription
        return label
    }()
    
    private lazy var contentContainer: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            statusImageView,
            titleLabel,
            descriptionLabel
        ])
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    private lazy var primaryButton: UIButton = {
        let button = UIButton()
        button
            .buttonStyle(PrimaryButtonStyle())
            .with(\.textAlignment, .center)
        button.setTitle(Localizable.billSMSOk, for: .normal)
        button.addTarget(self, action: #selector(finishFlow), for: .touchUpInside)
        return button
    }()

     override func buildViewHierarchy() {
        view.addSubview(contentContainer)
        view.addSubview(primaryButton)
    }
    
     override func setupConstraints() {
        statusImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Image.size)
        }
        
        contentContainer.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.centerY.equalToSuperview().offset(-Spacing.base05)
            $0.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        primaryButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.top.greaterThanOrEqualTo(contentContainer.snp.bottom).offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(Spacing.base08)
        }
    }

     override func configureViews() {
        view.backgroundColor = .backgroundPrimary()
        title = Localizable.viewTitle
        setupRightNavigationBarButton(closeButton)
    }
    
    @objc
    func finishFlow(_ sender: UIButton) {
        viewModel.finishFlow()
    }
}

// MARK: Bill2FABlockedDisplay
extension Bill2FABlockedViewController: Bill2FABlockedDisplay { }
