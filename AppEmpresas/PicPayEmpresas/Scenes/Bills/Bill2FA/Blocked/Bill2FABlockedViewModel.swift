import Foundation

protocol Bill2FABlockedDelegate: AnyObject {
    func finishFlow()
}

protocol Bill2FABlockedViewModelInputs: AnyObject {
    func finishFlow()
}

final class Bill2FABlockedViewModel {
    private let presenter: Bill2FABlockedPresenting
    private weak var delegate: Bill2FABlockedDelegate?

    init(presenter: Bill2FABlockedPresenting, delegate: Bill2FABlockedDelegate?) {
        self.presenter = presenter
        self.delegate = delegate
    }
}

// MARK: - Bill2FABlockedViewModelInputs
extension Bill2FABlockedViewModel: Bill2FABlockedViewModelInputs {
    func finishFlow() {
        presenter.didNextStep(action: .dismiss)
        delegate?.finishFlow()
    }
}
