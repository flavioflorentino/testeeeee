import UIKit

enum Bill2FABlockedAction {
    case dismiss
}

protocol Bill2FABlockedCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: Bill2FABlockedAction)
}

final class Bill2FABlockedCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - Bill2FACoordinating
extension Bill2FABlockedCoordinator: Bill2FABlockedCoordinating {
    func perform(action: Bill2FABlockedAction) {
        switch action {
        case .dismiss:
            viewController?.navigationController?.dismiss(animated: true)
        }
    }
}
