import Foundation

public struct Bill2FAParams {
    let billet: BillPay
    let phone: String
}

enum Bill2FAFactory {
    static func make(params: Bill2FAParams, delegate: Bill2FADelegate?) -> Bill2FAViewController {
        let container = DependencyContainer()
        let service: Bill2FAServicing = Bill2FAService(dependencies: container)
        let coordinator: Bill2FACoordinating = Bill2FACoordinator()
        let presenter: Bill2FAPresenting = Bill2FAPresenter(coordinator: coordinator)
        let viewModel = Bill2FAViewModel(
            service: service,
            presenter: presenter,
            billet: params.billet,
            phone: params.phone,
            delegate: delegate
        )
        let viewController = Bill2FAViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
