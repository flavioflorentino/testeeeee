import AnalyticsModule

enum Bill2FAError: String {
    case requestFailed = "request-failed"
    case generic
    case decode
    case badRequestDecode = "bad-request-decode"
    case connection
}

enum Bill2FAAnalytics: AnalyticsKeyProtocol {
    case started(billet: BillPay)
    case methodChoosen(method: Bill2FAStatusResponse)
    case deliveryTime(time: String)
    case codeExpired
    case codeResended
    case codeIncorrect
    case error(_ error: Bill2FAError)
    case blocked
    case success(_ method: Bill2FAStatusResponse, id: String)
    case canceled(_ method: Bill2FAStatusResponse)

    private var name: String {
        switch self {
        case .started:
            return "2FA - Started"
        case .methodChoosen:
            return "2FA - Method Chosen"
        case .deliveryTime:
            return "2FA - SMS Delivery Time"
        case .codeExpired:
            return "2FA - Code Expired"
        case .codeResended:
            return "2FA - Code Resended"
        case .codeIncorrect:
            return "2FA - Code Incorrect"
        case .error:
            return "2FA - Error"
        case .blocked:
            return "2FA - Attempts Exceeded"
        case .success:
            return "2FA - Success"
        case .canceled:
            return "2FA - Canceled"
        }
    }

    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }

    private var properties: [String: Any] {
        switch self {
        case .started(billet: let billet):
            return startedParams(billet)
        case .methodChoosen(let method), .canceled(let method):
            return defaultParams(method)
        case .deliveryTime(let time):
            var properties: [String: Any] = defaultParams(.sms)
            properties["sms_delivery_time"] = time
            return properties
        case .codeExpired, .codeIncorrect, .codeResended, .blocked:
            return defaultParams(.sms)
        case .error(let error):
            var properties: [String: Any] = defaultParams(.sms)
            properties["error_type"] = error.rawValue
            return properties
        case let .success(method, id):
            var properties: [String: Any] = defaultParams(method)
            properties["transaction_id"] = id
            return properties
        }
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

private extension Bill2FAAnalytics {
    func defaultParams(_ method: Bill2FAStatusResponse) -> [String: Any] {
        let properties: [String: Any] = [
            "2fa_method": method.rawValue,
            "flow": "bill-payment"
        ]

        return properties
    }

    func startedParams(_ billet: BillPay) -> [String: Any] {
        let defaultPaymentDate = billet.data.first(where: { $0.title == Strings.BillForm.paymentDate })?.title
        let todayPaymentDate = DateFormatter.custom(value: Date())
        let paymentDate = defaultPaymentDate ?? todayPaymentDate
        let properties: [String: Any] = [
            "value": billet.totalValue,
            "recipient": billet.beneficiary,
            "description": billet.description ?? String(),
            "expiration_date": billet.expirationDate,
            "payment_date": paymentDate,
            "flow": "bill-payment"
        ]
        return properties
    }
}
