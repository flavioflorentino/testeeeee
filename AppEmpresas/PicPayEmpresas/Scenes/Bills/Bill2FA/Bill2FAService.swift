import Core

protocol Bill2FAServicing {
    typealias PayBilletCompletion = (Result<ResponseBiz<BillPayResponse>, ApiError>) -> Void
    func sendSMS(completion: @escaping (Result<Bill2FAResponse, ApiError>) -> Void)
    func payBillet(params: PayBillet2FAParams, completion: @escaping PayBilletCompletion)
}

final class Bill2FAService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - Bill2FAServicing
extension Bill2FAService: Bill2FAServicing {
    func sendSMS(completion: @escaping (Result<Bill2FAResponse, ApiError>) -> Void) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        let endpoint = Bill2FAEndpoint.sendSMS
        Core.Api<Bill2FAResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                
                completion(mappedResult)
            }
        }
    }
    
    func payBillet(params: PayBillet2FAParams, completion: @escaping PayBilletCompletion) {
        let endpoint = Bill2FAEndpoint.payBillet(params: params)
        BizApi<BillPayResponse>(endpoint: endpoint).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
}
