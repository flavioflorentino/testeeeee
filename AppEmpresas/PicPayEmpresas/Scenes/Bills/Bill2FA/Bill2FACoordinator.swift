import UIKit

enum Bill2FAAction: Equatable {
    case dismiss
    case blocked(delegate: Bill2FABlockedDelegate?)
    case resend(modalInfo: AlertModalInfo)
    
    static func == (lhs: Bill2FAAction, rhs: Bill2FAAction) -> Bool {
        switch (lhs, rhs) {
        case (.dismiss, .dismiss):
            return true
        case (.blocked, .blocked):
            return true
        case (.resend, .resend):
            return true
        default:
            return false
        }
    }
}

protocol Bill2FACoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: Bill2FAAction)
}

final class Bill2FACoordinator {
    weak var viewController: UIViewController?
}

// MARK: - Bill2FACoordinating
extension Bill2FACoordinator: Bill2FACoordinating {
    func perform(action: Bill2FAAction) {
        switch action {
        case .dismiss:
            viewController?.navigationController?.dismiss(animated: true)
        case .blocked(let delegate):
            let controller = Bill2FABlockedFactory.make(delegate: delegate)
            let navigation = UINavigationController(rootViewController: controller)
            viewController?.navigationController?.present(navigation, animated: true)
        case .resend(let modalInfo):
            showModalAlert(modalInfo: modalInfo)
        }
    }
}

private extension Bill2FACoordinator {
    func showModalAlert(modalInfo: AlertModalInfo) {
        let alertPopup = AlertModalFactory.make(alertModalInfo: modalInfo)
        alertPopup.touchButtonAction = { alert in
            alert.dismiss(animated: true)
        }
        showAlert(contentController: alertPopup)
    }
    
    func showAlert(contentController: UIViewController) {
        let popup = PopupViewController()
        popup.contentController = contentController
        viewController?.navigationController?.present(popup, animated: true, completion: nil)
    }
}
