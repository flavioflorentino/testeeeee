import UIKit

enum TransactionsContainerFactory {
    static func make(seller: Seller, type: TransactionsListType) -> TransactionsContainerViewController {
        let container = DependencyContainer()
        let presenter: TransactionsContainerPresenting = TransactionsContainerPresenter()
        let interactor = TransactionsContainerInteractor(presenter: presenter, dependencies: container, seller: seller, type: type)
        let viewController = TransactionsContainerViewController(interactor: interactor)
        presenter.viewController = viewController
        return viewController
    }
}
