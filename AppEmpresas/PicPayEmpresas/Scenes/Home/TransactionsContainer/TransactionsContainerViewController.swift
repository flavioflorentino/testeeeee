import SnapKit
import UI
import UIKit

protocol TransactionsContainerDisplaying: AnyObject {
    func setupPicPayTransactions(viewController: PicPayTransactionsViewController)
    func setupPixTransactions(viewController: PixTransactionsViewController)
    func displayActiveList(_ tabOption: TransactionTabOption)
    func setupSegmentedControl(_ tabs: [String])
    func displayTitle()
    func displaySegmentedControl()
    func bringPicPayToFront()
    func bringPixToFront()
    func showPicPayTransactions(_ show: Bool)
    func showPixTransactions(_ show: Bool)
    func reloadPicPayTransactions()
    func reloadPixTransactions()
}

private extension TransactionsContainerViewController.Layout {
    enum SegmentedControl {
        static let height: CGFloat = 50.0
    }

    enum ActionSheet {
        static let frameOrigin = CGPoint(x: 0.0, y: 10.0)
        static let frameHeight: CGFloat = 60.0
        static let frameWidthOffset: CGFloat = 20.0
        static let cornerRadius: CGFloat = 10.0
    }
}

final class TransactionsContainerViewController: ViewController<TransactionsContainerInteracting, UIView> {
    private typealias Localizable = Strings.Home.Transaction
    fileprivate enum Layout { }
    
    private lazy var transactionsTitle: UILabel = {
        let label = UILabel()
        label.text = Localizable.Header.title
        label.labelStyle(TitleLabelStyle(type: .small)).with(\.textColor, Colors.black.color)
        return label
    }()
    
    private lazy var segmentedControl: CustomSegmentedControl = {
        let view = CustomSegmentedControl()
        view.textColor = Colors.grayscale500.color
        view.selectorViewColor = Colors.branding600.color
        view.selectorTextColor = Colors.branding600.color
        view.delegate = self
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private var userListViewController: PicPayTransactionsViewController?
    
    private var pixListViewController: PixTransactionsViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setupHeader()
        interactor.setupPicPayTransactions()
        interactor.setupActiveList(transactionTab: 0)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(containerView)
    }
    
    override func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

// MARK: - HomeTransactionDisplaying
extension TransactionsContainerViewController: TransactionsContainerDisplaying {
    func setupPicPayTransactions(viewController: PicPayTransactionsViewController) {
        userListViewController = viewController
    }
    
    func setupPixTransactions(viewController: PixTransactionsViewController) {
        pixListViewController = viewController
    }
    
    func displayActiveList(_ tabOption: TransactionTabOption) {
        segmentedControl.setIndex(index: tabOption.rawValue)
    }
    
    func setupSegmentedControl(_ tabs: [String]) {
        segmentedControl.setButtonTitles(buttonTitles: tabs)
    }
    
    func displayTitle() {
        view.addSubview(transactionsTitle)
        
        transactionsTitle.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        containerView.snp.remakeConstraints {
            $0.top.equalTo(transactionsTitle.snp.bottom)
            $0.bottom.leading.trailing.equalToSuperview()
        }
    }
    
    func displaySegmentedControl() {
        view.addSubview(segmentedControl)
        
        segmentedControl.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.height.equalTo(Layout.SegmentedControl.height)
            $0.leading.trailing.equalToSuperview()
        }
        
        containerView.snp.remakeConstraints {
            $0.top.equalTo(segmentedControl.snp.bottom)
            $0.bottom.leading.trailing.equalToSuperview()
        }
    }
    
    func bringPicPayToFront() {
        guard let userListViewController = userListViewController else { return }
        containerView.bringSubviewToFront(userListViewController.view)
    }
    
    func bringPixToFront() {
        guard let pixListViewController = pixListViewController else { return }
        containerView.bringSubviewToFront(pixListViewController.view)
    }
    
    func showPicPayTransactions(_ show: Bool) {
        guard let userListViewController = userListViewController else { return }
        show ? addViewController(userListViewController) : removeViewController(userListViewController)
        userListViewController.view.isHidden = !show
    }
    
    func showPixTransactions(_ show: Bool) {
        guard let pixListViewController = pixListViewController else { return }
        show ? addViewController(pixListViewController) : removeViewController(pixListViewController)
        pixListViewController.view.isHidden = !show
    }
    
    func reloadPicPayTransactions() {
        userListViewController?.reloadTransactions()
    }
    
    func reloadPixTransactions() {
        pixListViewController?.reloadTransactions()
    }
}

// MARK: - Try Again
extension TransactionsContainerViewController: TransactionsErrorViewDelegate {
    func didTapTryAgain() {
        reload()
    }
}

// MARK: - Reload
extension TransactionsContainerViewController: HomeLoadable {
    func reload() {
        interactor.reload(transactionTab: segmentedControl.selectedIndex)
    }
}

// MARK: - CustomSegmentedControlDelegate

extension TransactionsContainerViewController: CustomSegmentedControlDelegate {
    func changeToIndex(index: Int) {
        interactor.changeTab(to: index)
    }
}

private extension TransactionsContainerViewController {
    func addViewController(_ viewController: UIViewController) {
        addChild(viewController)
        containerView.addSubview(viewController.view)
        viewController.didMove(toParent: self)
        
        viewController.view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func removeViewController(_ viewController: UIViewController?) {
        guard let viewController = viewController else { return }
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
}
