import Foundation
import UIKit

protocol TransactionsContainerPresenting: AnyObject {
    var viewController: TransactionsContainerDisplaying? { get set }
    func setupPicPayTransactions(seller: Seller, type: TransactionsListType)
    func setupPixTransactions(seller: Seller, type: TransactionsListType)
    func presentTitle()
    func presentSegmentedControl(_ tabs: [TransactionTabOption])
    func hideAll()
    func presentActiveList(_ tabOption: TransactionTabOption)
    func showPicPayList()
    func showPixList()
    func reloadPicPayTransactions()
    func reloadPixTransactions()
}

final class TransactionsContainerPresenter {
    weak var viewController: TransactionsContainerDisplaying?
    
    private var picPayTransactions: PicPayTransactionsViewController?
    private var pixTransactions: PixTransactionsViewController?
    private var linkTransactions: LinkTransactionsViewController?
}

// MARK: - HomeTransactionsPresenting
extension TransactionsContainerPresenter: TransactionsContainerPresenting {
    func setupPicPayTransactions(seller: Seller, type: TransactionsListType) {
        guard picPayTransactions == nil else { return }
        let picPayTransactions = PicPayTransactionsFactory.make(seller: seller, type: type)
        self.picPayTransactions = picPayTransactions
        viewController?.setupPicPayTransactions(viewController: picPayTransactions)
    }
    
    func setupPixTransactions(seller: Seller, type: TransactionsListType) {
        guard pixTransactions == nil else { return }
        let pixTransactions = PixTransactionsFactory.make(seller: seller, type: type)
        self.pixTransactions = pixTransactions
        viewController?.setupPixTransactions(viewController: pixTransactions)
    }
    
    func presentTitle() {
        viewController?.displayTitle()
    }
    
    func presentSegmentedControl(_ tabs: [TransactionTabOption]) {
        viewController?.setupSegmentedControl(tabs.map { $0.description })
        viewController?.displaySegmentedControl()
    }

    func hideAll() {
        viewController?.showPicPayTransactions(false)
        viewController?.showPixTransactions(false)
    }
    
    func presentActiveList(_ tabOption: TransactionTabOption) {
        viewController?.displayActiveList(tabOption)
    }
    
    func showPicPayList() {
        viewController?.bringPicPayToFront()
        viewController?.showPicPayTransactions(true)
    }
    
    func showPixList() {
        viewController?.bringPixToFront()
        viewController?.showPixTransactions(true)
    }
    
    func reloadPicPayTransactions() {
        viewController?.reloadPicPayTransactions()
    }
    
    func reloadPixTransactions() {
        viewController?.reloadPixTransactions()
    }
}
