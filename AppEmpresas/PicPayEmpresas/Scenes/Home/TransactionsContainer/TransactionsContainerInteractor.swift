import AnalyticsModule
import Core
import Foundation
import FeatureFlag
import LegacyPJ

protocol TransactionsContainerInteracting: AnyObject {
    func setupPicPayTransactions()
    func setupHeader()
    func setupActiveList(transactionTab tab: Int)
    func changeTab(to index: Int)
    func reload(transactionTab: Int)
}

final class TransactionsContainerInteractor {
    typealias Dependencies = HasAuthManager & HasFeatureManager

    private let dependencies: Dependencies
    private let presenter: TransactionsContainerPresenting
    private let seller: Seller
    private var tabs: [TransactionTabOption] = [.picPayTransactions]
    
    lazy var shouldEnablePix: Bool = {
        guard let isAdmin = dependencies.authManager.user?.isAdmin else { return false }
        return dependencies.featureManager.isActive(.releasePIXPresenting) &&
            seller.digitalAccount &&
            seller.biometry &&
            isAdmin
    }()
    
    let type: TransactionsListType

    init(presenter: TransactionsContainerPresenting, dependencies: Dependencies, seller: Seller, type: TransactionsListType) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.seller = seller
        self.type = type
    }
}

// MARK: - HomeTransactionInteracting
extension TransactionsContainerInteractor: TransactionsContainerInteracting {
    func setupPicPayTransactions() {
        presenter.setupPicPayTransactions(seller: seller, type: type)
        presenter.showPicPayList()
    }
    
    func setupHeader() {
        if shouldEnablePix {
            tabs.append(.pixTransactions)
        }
        
        tabs.count == 1 ? presenter.presentTitle() : presenter.presentSegmentedControl(tabs)
    }

    func setupActiveList(transactionTab tab: Int) {
        guard tab < tabs.count, tab >= 0 else { return }
        presenter.presentActiveList(tabs[tab])
    }
    
    func changeTab(to index: Int) {
        guard index < tabs.count, index >= 0 else { return }
        
        presenter.presentActiveList(tabs[index])
        presenter.hideAll()
        
        switch tabs[index] {
        case .picPayTransactions:
            presenter.setupPicPayTransactions(seller: seller, type: type)
            presenter.showPicPayList()
        case .pixTransactions:
            presenter.setupPixTransactions(seller: seller, type: type)
            presenter.showPixList()
        default:
            break
        }
    }
    
    func reload(transactionTab: Int) {
        guard
            transactionTab < tabs.count, transactionTab >= 0,
            let tab = tabs[safe: transactionTab]
        else { return }
        
        presenter.presentActiveList(tabs[transactionTab])
        presenter.hideAll()
        
        switch tab {
        case .picPayTransactions:
            presenter.reloadPicPayTransactions()
            presenter.showPicPayList()
        case .pixTransactions:
            presenter.reloadPixTransactions()
            presenter.showPixList()
        default:
            break
        }
    }
}
