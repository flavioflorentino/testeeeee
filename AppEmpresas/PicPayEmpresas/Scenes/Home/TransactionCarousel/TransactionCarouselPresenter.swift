import AssetsKit
import Core
import Foundation

protocol TransactionCarouselPresenting: AnyObject {
    var viewController: TransactionCarouselDisplay? { get set }
    func setTransactionItems(_ items: [TransactionCarouselModel])
    func reloadCellAt(indexPath: IndexPath)
    func showPayBillet(hasBiometry: Bool, firstInteraction: Bool, isBlocked: Bool)
    func showPIX(seller: Seller, welcomePresented: Bool, pixAvailablePresented: Bool)
    func showTransfer(hasBiometry: Bool)
    func showCreateCampaign()
    func showMaterialSolicitation()
    func showPrintMaterial()
}

final class TransactionCarouselPresenter: TransactionCarouselPresenting {
    private typealias Localizable = Strings.TransactionCarousel
    
    private let coordinator: TransactionCarouselCoordinating
    weak var viewController: TransactionCarouselDisplay?

    init(coordinator: TransactionCarouselCoordinating) {
        self.coordinator = coordinator
    }
    
    func setTransactionItems(_ items: [TransactionCarouselModel]) {
        viewController?.setTransactionItems(items)
    }
    
    func reloadCellAt(indexPath: IndexPath) {
        viewController?.reloadCellAt(indexPath: indexPath)
    }
    
    func showPayBillet(hasBiometry: Bool, firstInteraction: Bool, isBlocked: Bool) {
        guard hasBiometry else {
            showBilletTransferSoon()
            return
        }
        
        if firstInteraction {
            showPayBilletAvailable(isBlocked: isBlocked)
        } else {
            coordinator.perform(action: .payBillet(isBlocked: isBlocked))
        }
    }
    
    func showPIX(seller: Seller, welcomePresented: Bool, pixAvailablePresented: Bool) {
        coordinator.perform(action: .pix(seller: seller,
                                         welcomePresented: welcomePresented,
                                         pixAvailablePresented: pixAvailablePresented))
    }
    
    func showTransfer(hasBiometry: Bool) {
        if hasBiometry {
            showTransferSoon()
        } else {
            showBilletTransferSoon()
        }
    }
    
    func showCreateCampaign() {
        coordinator.perform(action: .createCampaign)
    }
    
    func showMaterialSolicitation() {
        coordinator.perform(action: .materialSolicitation)
    }
    
    func showPrintMaterial() {
        coordinator.perform(action: .printMaterial)
    }
}

private extension TransactionCarouselPresenter {
    func showPayBilletAvailable(isBlocked: Bool) {
        let modalInfoImage = AlertModalInfoImage(image: Assets.Receipt.bill.image)
        let info = AlertModalInfo(
            imageInfo: modalInfoImage,
            title: Localizable.payBilletsTitle,
            subtitle: Localizable.payBilletsSubtitle,
            buttonTitle: Localizable.payBilletsButtonTitle,
            secondaryButtonTitle: Localizable.payBilletsSecondaryButtonTitle
        )
        coordinator.perform(action: .payBilletAvailable(modalInfo: info, isBlocked: isBlocked))
    }
    
    func showBilletTransferSoon() {
        let info = getSoonAlertInfo(
            title: Localizable.billetTransferTitle,
            subtitle: Localizable.billetTransferSubtitle
        )
        coordinator.perform(action: .payBilletSoon(modalInfo: info))
    }
    
    func showTransferSoon() {
        let info = getSoonAlertInfo(
            title: Localizable.transferAlertTitle,
            subtitle: Localizable.transferAlertText
        )
        coordinator.perform(action: .transfer(modalInfo: info))
    }
    
    func getSoonAlertInfo(title: String, subtitle: String) -> AlertModalInfo {
        let modalInfoImage = AlertModalInfoImage(image: Assets.Emoji.iconHappy.image)
        return AlertModalInfo(
            imageInfo: modalInfoImage,
            title: title,
            subtitle: subtitle,
            buttonTitle: Localizable.understood
        )
    }
}
