struct TransactionCarouselResponse: Decodable {
    let detail: TransactionCarouselDetailResponse
}

struct TransactionCarouselDetailResponse: Decodable {
    let blocked: Bool
}
