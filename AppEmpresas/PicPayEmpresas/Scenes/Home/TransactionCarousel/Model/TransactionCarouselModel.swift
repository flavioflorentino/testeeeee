import AssetsKit
import Core
import Foundation
import UI

private typealias CarouselLocalizable = Strings.TransactionCarousel

enum TransactionCarouselType {
    case transfer
    case payBillet
    case pix
    case createCampaign
    case material
    case print

    var userDefaultsKey: BizKvKey {
        switch self {
        case .payBillet:
            return .didInteractTransactionCarouselPayBillet
        case .transfer:
            return .didInteractTransactionCarouselTransfer
        case .pix:
            return .didPresentAdvertisingPix
        case .createCampaign:
            return .didInteractCarouselCreateCampaign
        case .material:
            return .didInteractTransactionCarouselMaterial
        case .print:
            return .didInteractTransactionCarouselPrint
        }
    }
}

struct TransactionCarouselBadge {
    let title: String
    let color: UIColor
}

protocol TransactionCarouselModel {
    var type: TransactionCarouselType { get }
    var title: String { get }
    var image: UIImage { get }
    var enabled: Bool { get }
}

struct TransactionCarouselPayBillet: TransactionCarouselModel {
    let type: TransactionCarouselType = .payBillet
    let title: String = CarouselLocalizable.payBillets
    let image: UIImage = Resources.Icons.icoBarcode.image
    let enabled: Bool
    
    init(hasBiometry: Bool) {
        enabled = hasBiometry
    }
}

struct TransactionCarouselPixItem: TransactionCarouselModel {
    let type: TransactionCarouselType = .pix
    let title: String = CarouselLocalizable.pixCarouselItem
    let image: UIImage = Resources.Icons.icoPixFill.image
    let enabled: Bool = true
}

struct TransactionCarouselMaterialItem: TransactionCarouselModel {
    var type: TransactionCarouselType = .material
    var title: String = CarouselLocalizable.materialCarouselItem
    var image: UIImage = Resources.Icons.icoMaterialRequest.image
    var enabled: Bool = true
}

struct TransactionCarouselPrintItem: TransactionCarouselModel {
    var type: TransactionCarouselType = .print
    var title = CarouselLocalizable.printCarouselItem
    var image = Resources.Icons.icoPrint.image
    var enabled = true
}

struct TransactionCarouselTransfer: TransactionCarouselModel {
    let type: TransactionCarouselType = .transfer
    let title: String = CarouselLocalizable.makeTranfer
    let image: UIImage = Resources.Icons.icoTransfer.image.withRenderingMode(.alwaysTemplate)
    let enabled: Bool = false
}

struct TransactionCarouselCreateCampaign: TransactionCarouselModel {
    let type: TransactionCarouselType = .createCampaign
    let title = CarouselLocalizable.createCapaign
    let image = Resources.Icons.icoStore.image
    let enabled = true
}
