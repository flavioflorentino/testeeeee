import Core

enum TransactionCarouselEndpoint {
    case isBlocked
}

extension TransactionCarouselEndpoint: ApiEndpointExposable {
    var path: String {
        "/tfa/blocked"
    }
    
    var method: HTTPMethod {
        .get
    }
}
