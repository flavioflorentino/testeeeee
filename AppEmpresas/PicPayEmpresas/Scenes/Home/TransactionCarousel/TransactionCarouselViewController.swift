import UI
import UIKit

private extension TransactionCarouselViewController.Layout {
    enum CollectionView {
        static let sectionSpacing = Spacing.base01
        static let itemSpacing = Spacing.base01
        static let cellHeight: CGFloat = 104
        static let cellWidth: CGFloat = 126
        static let height = cellHeight + Spacing.base00
    }
}

final class TransactionCarouselViewController: ViewController<TransactionCarouselViewModelInputs, UIView> {
    // MARK: - Private Properties
    
    fileprivate struct Layout { }
    
    private lazy var flowLayout: UICollectionViewFlowLayout = {
        let sectionSpacing = Layout.CollectionView.sectionSpacing
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumInteritemSpacing = Layout.CollectionView.itemSpacing
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: sectionSpacing, bottom: 0, right: sectionSpacing)
        flowLayout.scrollDirection = .horizontal
        return flowLayout
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        collectionView.register(TransactionCarouselCell.self, forCellWithReuseIdentifier: TransactionCarouselCell.identifier)
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
    }()
    
    private var dataSource: CollectionViewHandler<TransactionCarouselModel, TransactionCarouselCell>?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadTransactionItems()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupFlowLayout()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(collectionView)
    }
    
    override func configureViews() {
        view.backgroundColor = .clear
    }
    
    override func setupConstraints() {
        collectionView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Layout.CollectionView.height)
        }
    }
}

// MARK: - Private Methods

private extension TransactionCarouselViewController {
    func bindCell(_ row: Int, _ data: TransactionCarouselModel, _ cell: TransactionCarouselCell) {
        let badge = viewModel.badgeFor(item: data)
        cell.setup(transaction: data, badge: badge)
    }
    
    func setupFlowLayout() {
        let cellWidth = Layout.CollectionView.cellWidth
        let cellHeight = Layout.CollectionView.cellHeight
        flowLayout.itemSize = CGSize(width: cellWidth, height: cellHeight)
    }
}

// MARK: View Model Outputs

extension TransactionCarouselViewController: TransactionCarouselDisplay {
    func setTransactionItems(_ items: [TransactionCarouselModel]) {
        dataSource = CollectionViewHandler(
            data: items,
            cellType: TransactionCarouselCell.self,
            configureCell: bindCell
        )
        
        collectionView.dataSource = dataSource
        collectionView.reloadData()
    }
    
    func reloadCellAt(indexPath: IndexPath) {
        collectionView.reloadItems(at: [indexPath])
    }
}

// MARK: - UICollectionViewDelegate

extension TransactionCarouselViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.selectCellAt(indexPath: indexPath)
    }
}
