import Foundation

enum TransactionCarouselFactory {
    static func make(hasBiometry: Bool, userStatus: UserStatus?, seller: Seller) -> TransactionCarouselViewController {
        let container = DependencyContainer()
        let service: TransactionCarouselServicing = TransactionCarouselService(dependencies: container)
        let coordinator: TransactionCarouselCoordinating = TransactionCarouselCoordinator(dependencies: container)
        let presenter: TransactionCarouselPresenting = TransactionCarouselPresenter(coordinator: coordinator)
        let viewModel = TransactionCarouselViewModel(
            service: service,
            presenter: presenter,
            dependencies: container,
            hasBiometry: hasBiometry,
            userStatus: userStatus,
            seller: seller
        )
        let viewController = TransactionCarouselViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
