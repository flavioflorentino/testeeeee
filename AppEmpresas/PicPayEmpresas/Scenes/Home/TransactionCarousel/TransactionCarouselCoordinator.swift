import UIKit
import PIX
import AnalyticsModule
import FeatureFlag
import LegacyPJ

enum TransactionCarouselAction {
    case payBilletAvailable(modalInfo: AlertModalInfo, isBlocked: Bool)
    case payBilletSoon(modalInfo: AlertModalInfo)
    case payBillet(isBlocked: Bool)
    case transfer(modalInfo: AlertModalInfo)
    case createCampaign
    case pix(seller: Seller, welcomePresented: Bool, pixAvailablePresented: Bool)
    case materialSolicitation
    case printMaterial
}

enum CarouselAlertAction {
    case payBillet
    case pix(seller: Seller)
    case materialSolicitation
}

protocol TransactionCarouselCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: TransactionCarouselAction)
}

final class TransactionCarouselCoordinator: TransactionCarouselCoordinating {
    weak var viewController: UIViewController?
    
    typealias Dependencies = HasAuthManager & HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: TransactionCarouselAction) {
        switch action {
        case let .payBilletAvailable(modalInfo, isBlocked):
            showCarouselAlert(modalInfo: modalInfo, action: .payBillet, isBlocked: isBlocked)
        case .payBillet(let isBlocked):
            navigateToPayBillet(isBlocked: isBlocked)
        case .payBilletSoon(let modalInfo), .transfer(let modalInfo):
            showSoonAlert(modalInfo: modalInfo)
        case .createCampaign:
            let controller = CampaignHomeFactory.make()
            viewController?.pushViewController(controller, hidesBottomBar: true)
        case let .pix(seller, isWelcomePresented, pixAvaiablePresented):
            navigateToPIX(seller: seller, welcomePresented: isWelcomePresented, pixAvaiablePresented: pixAvaiablePresented)
        case .materialSolicitation:
            navigateToMaterialSolicitation()
        case .printMaterial:
            navigateToPrintMaterial()
        }
    }
}

// MARK: - Private Methods

private extension TransactionCarouselCoordinator {
    func showCarouselAlert(modalInfo: AlertModalInfo, action: CarouselAlertAction, isBlocked: Bool = false) {
        let alertPopup = AlertModalFactory.make(alertModalInfo: modalInfo)
        
        alertPopup.touchButtonAction = { [weak self] alert in
            guard let self = self else {
                return
            }
            alert.dismiss(animated: true) {
                if case .payBillet = action {
                    self.navigateToPayBillet(isBlocked: isBlocked)
                }
            }
        }
        
        alertPopup.touchSecondaryButtonAction = { alert in            
            alert.dismiss(animated: true)
        }
        
        showAlert(contentController: alertPopup)
    }
    
    func showSoonAlert(modalInfo: AlertModalInfo) {
        let alertPopup = AlertModalFactory.make(alertModalInfo: modalInfo)
        alertPopup.touchButtonAction = { alert in
            alert.dismiss(animated: true)
        }
        showAlert(contentController: alertPopup)
    }
    
    func showAlert(contentController: UIViewController) {
        let popup = PopupViewController()
        popup.hasCloseButton = false
        popup.contentController = contentController
        viewController?.present(popup, animated: true, completion: nil)
    }
    
    func navigateToPayBillet(isBlocked: Bool) {
        if isBlocked {
            let controller = BillPayBlockedFactory.make()
            viewController?.pushViewController(controller, hidesBottomBar: true)
        } else {
            let isScannerEnabled = FeatureManager.shared.isActive(.releaseScannerPresenting)
            let paymentViewController = isScannerEnabled ? BillPaymentMethodsFactory.make() :  BillFormFactory.make()

            viewController?.navigationController?.pushViewController(paymentViewController, animated: true)
        }
    }
    
    func navigateToPIX(seller: Seller, welcomePresented: Bool, pixAvaiablePresented: Bool) {
        guard let navigationController = viewController?.navigationController else {
            return
        }
        let userInfo = KeyManagerBizUserInfo(name: seller.razaoSocial ?? "",
                                             cnpj: seller.cnpj ?? "",
                                             mail: seller.email ?? "",
                                             phone: dependencies.authManager.user?.phone ?? "",
                                             userId: String(seller.id))
                                             
        let coordinator = PIXBizFlowCoordinator(with: navigationController, userInfo: userInfo)
        
        if dependencies.featureManager.isActive(.releaseHubPixPresenting) {
            coordinator.perform(flow: pixAvaiablePresented ? .hub : .welcomePage)
            return
        }
        coordinator.perform(flow: .optin(action: welcomePresented ? .keyManager(input: .none) : .onboarding))
    }
    
    func navigateToMaterialSolicitation() {
        let controller = MaterialFlowControlFactory.make()
        let navigationController = UINavigationController(rootViewController: controller)
        viewController?.present(navigationController, animated: true)
    }
    
    func navigateToPrintMaterial() {
        let controller = ChooseQRCodePrintFactory.make()
        viewController?.pushViewController(controller)
    }
}
