import AnalyticsModule
import Core
import Foundation
import UI
import UIKit
import FeatureFlag
import LegacyPJ

protocol TransactionCarouselViewModelInputs: AnyObject {
    func loadTransactionItems()
    func selectCellAt(indexPath: IndexPath)
    func badgeFor(item: TransactionCarouselModel) -> TransactionCarouselBadge?
}

final class TransactionCarouselViewModel {
    typealias Dependencies = HasAuthManager & HasKVStore & HasFeatureManager & HasAnalytics
    
    private typealias Localizable = Strings.TransactionCarousel
    
    // MARK: - Private Properties
    
    private let service: TransactionCarouselServicing
    private let presenter: TransactionCarouselPresenting
    private let dependencies: Dependencies
    private let hasBiometry: Bool
    private let userStatus: UserStatus?
    private let seller: Seller
    
    private lazy var items: [TransactionCarouselModel] = {
        var items: [TransactionCarouselModel] = []
        
        if let user = dependencies.authManager.user {
            if FeatureEnablerHelper.shouldEnablePIX(seller: seller, isAdmin: user.isAdmin) {
                items.append(TransactionCarouselPixItem())
            }
        }

        if dependencies.featureManager.isActive(.releaseCashbackPresentingBool) {
            items.append(TransactionCarouselCreateCampaign())
        }
        
        if let userStatus = userStatus, !userStatus.boardRequestRefused {
            if dependencies.featureManager.isActive(.allowApplyMaterial), !userStatus.boardRequested {
                items.append(TransactionCarouselMaterialItem())
            }
            
            if dependencies.featureManager.isActive(.allowQRCodeFlow), userStatus.boardRequested {
                items.append(TransactionCarouselPrintItem())
            }
        }
        
        if dependencies.featureManager.isActive(.isBilletPaymentAvailable), seller.digitalAccount, seller.biometry {
            items.append(TransactionCarouselPayBillet(hasBiometry: hasBiometry))
        }

        if dependencies.featureManager.isActive(.isTransferCarouselButtonAvailable) {
            items.append(TransactionCarouselTransfer())
        }

        return items
    }()
    
    // MARK: - Inits
    init(service: TransactionCarouselServicing,
         presenter: TransactionCarouselPresenting,
         dependencies: Dependencies,
         hasBiometry: Bool,
         userStatus: UserStatus?,
         seller: Seller) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.hasBiometry = hasBiometry
        self.userStatus = userStatus
        self.seller = seller
    }
}

// MARK: - TransactionCarouselViewModelInputs

extension TransactionCarouselViewModel: TransactionCarouselViewModelInputs {
    func loadTransactionItems() {
        presenter.setTransactionItems(items)
    }
    
    func selectCellAt(indexPath: IndexPath) {
        let item = items[indexPath.row]
        
        switch item.type {
        case .payBillet:
            isPayBilletBlocked(item: item)
        case .transfer:
            presenter.showTransfer(hasBiometry: hasBiometry)
        case .createCampaign:
            presenter.showCreateCampaign()
        case .pix:
            showPIX(item: item)
        case .material:
            presenter.showMaterialSolicitation()
        case .print:
            presenter.showPrintMaterial()
        }
        
        dependencies.kvStore.set(value: true, with: item.type.userDefaultsKey)
        presenter.reloadCellAt(indexPath: indexPath)
        
        logSelection(type: item.type)
    }
    
    func badgeFor(item: TransactionCarouselModel) -> TransactionCarouselBadge? {
        let didInteract = dependencies.kvStore.boolFor(item.type.userDefaultsKey)
        
        switch item.type {
        case .payBillet:
            return getBilletBadge(didInteract: didInteract, hasBiometry: hasBiometry)
        case .transfer:
            return getSoonBadge(didInteract: didInteract)
        case .createCampaign:
            return getNewBadge(didInteract: didInteract)
        case .pix:
            return getNewBadge(didInteract: didInteract)
        case .material:
            return getNewBadge(didInteract: didInteract)
        case .print:
            return getNewBadge(didInteract: didInteract)
        }
    }
}

// MARK: - Private Methods

private extension TransactionCarouselViewModel {
    func getBilletBadge(didInteract: Bool, hasBiometry: Bool) -> TransactionCarouselBadge? {
        guard hasBiometry else {
            return getSoonBadge(didInteract: didInteract)
        }
        return getNewBadge(didInteract: didInteract)
    }
    
    func getNewBadge(didInteract: Bool) -> TransactionCarouselBadge? {
        if didInteract {
            return nil
        }
        return TransactionCarouselBadge(title: Localizable.new, color: Colors.neutral600.color)
    }
    
    func getSoonBadge(didInteract: Bool) -> TransactionCarouselBadge {
        let color: UIColor = didInteract ? Colors.grayscale300.color : Colors.warning600.color
        return TransactionCarouselBadge(title: Localizable.soon, color: color)
    }
    
    func logSelection(type: TransactionCarouselType) {
        guard let user = dependencies.authManager.user, let sellerID = user.id, let companyName = seller.razaoSocial else {
            return
        }
        
        switch type {
        case .payBillet:
            dependencies.analytics.log(TransactionCarouselAnalytics.billTouched(sellerID: sellerID))
        case .transfer:
            dependencies.analytics.log(TransactionCarouselAnalytics.transferTouched(sellerID: sellerID))
        case .createCampaign:
            break
            // implementar o analitcs para o fluxo de campanha que será realizado em uma outra task
        case .pix:
            let event = TransactionCarouselAnalytics.pixTouched(sellerID: seller.id, companyName: companyName)
            dependencies.analytics.log(event)
        case .material:
            dependencies.analytics.log(MaterialRequestAnalytics.newHeaderEvent)
        case .print:
            dependencies.analytics.log(MaterialRequestAnalytics.printMaterialScreen)
        }
    }
    
    func isPayBilletBlocked(item: TransactionCarouselModel) {
        let firstInteraction = !dependencies.kvStore.boolFor(item.type.userDefaultsKey)
        if dependencies.featureManager.isActive(.bill2FA) {
            service.isBlocked { result in
                switch result {
                case .success(let response):
                    let isBlocked = response.detail.blocked
                    self.showPayBillet(firstInteraction, isBlocked: isBlocked)
                case .failure:
                    self.showPayBillet(firstInteraction, isBlocked: false)
                }
            }
        } else {
            showPayBillet(firstInteraction, isBlocked: false)
        }
    }
    
    func showPayBillet(_ firstInteraction: Bool, isBlocked: Bool) {
        presenter.showPayBillet(
            hasBiometry: hasBiometry,
            firstInteraction: firstInteraction,
            isBlocked: isBlocked
        )
    }
    
    func showPIX(item: TransactionCarouselModel) {
        let isWelcomeVisualized = service.isWelcomePixPresented
        let isPixAvailaablePresented = service.isPixAvaiablePresented
        presenter.showPIX(seller: seller,
                          welcomePresented: isWelcomeVisualized,
                          pixAvailablePresented: isPixAvailaablePresented)
    }
}
