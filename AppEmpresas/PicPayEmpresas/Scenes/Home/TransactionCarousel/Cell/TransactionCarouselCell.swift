import SnapKit
import UI
import UIKit

private extension TransactionCarouselCell.Layout {
    enum CardView {
        static let shadowColor = Colors.grayscale100.color.cgColor
        static let shadowOffset = CGSize.zero
        static let shadowRadius: CGFloat = 2
        static let shadowOpacity: Float = 1
    }
    
    enum Badge {
        static let cornerRadius = CornerRadius.light
        static let textInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
    }
    
    enum IconImageView {
        static let size = CGSize(width: 24, height: 24)
    }
}

final class TransactionCarouselCell: UICollectionViewCell {
    // MARK: - Private Properties
    
    fileprivate struct Layout { }
    
    private lazy var disabledLayer: CALayer = {
        let disabledLayer = CALayer()
        disabledLayer.zPosition = 1
        disabledLayer.backgroundColor = Colors.white.color.withAlphaComponent(0.6).cgColor
        return disabledLayer
    }()
    
    private lazy var cardView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.white.color
        view.layer.cornerRadius = CornerRadius.light
        view.clipsToBounds = true
        view.layer.masksToBounds = false
        view.layer.shadowColor = Layout.CardView.shadowColor
        view.layer.shadowOffset = Layout.CardView.shadowOffset
        view.layer.shadowRadius = Layout.CardView.shadowRadius
        view.layer.shadowOpacity = Layout.CardView.shadowOpacity
        return view
    }()
    
    private lazy var badge: UITextView = {
        let textView = UITextView()
        textView.font = Typography.caption(.highlight).font()
        textView.clipsToBounds = true
        textView.layer.cornerRadius = Layout.Badge.cornerRadius
        textView.textColor = Colors.white.color
        textView.isUserInteractionEnabled = false
        textView.isScrollEnabled = false
        textView.textContainerInset = Layout.Badge.textInsets
        textView.layer.zPosition = 2
        return textView
    }()
    
    private lazy var iconImageView = UIImageView()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.grayscale700.color
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 2
        return label
    }()
    
    // MARK: - Inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func draw(_ rect: CGRect) {
        disabledLayer.frame = cardView.frame
    }
}

// MARK: - Public Methods

extension TransactionCarouselCell {
    func setup(transaction: TransactionCarouselModel, badge: TransactionCarouselBadge?) {
        iconImageView.image = transaction.image
        titleLabel.text = transaction.title
        
        if !transaction.enabled {
            cardView.layer.addSublayer(disabledLayer)
            iconImageView.tintColor = Colors.grayscale300.color
        } else {
            disabledLayer.removeFromSuperlayer()
            iconImageView.tintColor = Colors.branding600.color
        }
        
        self.badge.isHidden = badge == nil
        self.badge.text = badge?.title
        self.badge.backgroundColor = badge?.color
    }
}

// MARK: - ViewConfiguration

extension TransactionCarouselCell: ViewConfiguration {
    func configureViews() {
        backgroundColor = .clear
    }
    
    func setupConstraints() {
        cardView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview().inset(Layout.CardView.shadowOffset.height)
        }
        
        badge.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base00)
            $0.trailing.equalToSuperview().inset(Spacing.base00)
        }
        
        iconImageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.IconImageView.size)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(iconImageView.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func buildViewHierarchy() {
        cardView.addSubview(badge)
        cardView.addSubview(iconImageView)
        cardView.addSubview(titleLabel)
        contentView.addSubview(cardView)
    }
}
