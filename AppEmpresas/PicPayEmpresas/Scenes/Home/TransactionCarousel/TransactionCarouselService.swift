import Core

protocol TransactionCarouselServicing {
    var isWelcomePixPresented: Bool { get }
    var isPixAvaiablePresented: Bool { get }
    func isBlocked(completion: @escaping (Result<TransactionCarouselResponse, ApiError>) -> Void)
}

final class TransactionCarouselService {
    typealias Dependencies = HasMainQueue & HasKVStore
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - TransactionCarouselServicing
extension TransactionCarouselService: TransactionCarouselServicing {
    var isPixAvaiablePresented: Bool {
        dependencies.kvStore.boolFor(KVKey.isPixAvailableFeedbackVisualized)
    }
    
    var isWelcomePixPresented: Bool {
       dependencies.kvStore.boolFor(KVKey.isPixKeyManagementWelcomeVisualized)
    }
    
    func isBlocked(completion: @escaping (Result<TransactionCarouselResponse, ApiError>) -> Void) {
        let endpoint = TransactionCarouselEndpoint.isBlocked
        Core.Api<TransactionCarouselResponse>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
}
