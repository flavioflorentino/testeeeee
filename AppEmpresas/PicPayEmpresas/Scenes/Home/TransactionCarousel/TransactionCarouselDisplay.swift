import Foundation

protocol TransactionCarouselDisplay: AnyObject {
    func setTransactionItems(_ items: [TransactionCarouselModel])
    func reloadCellAt(indexPath: IndexPath)
}
