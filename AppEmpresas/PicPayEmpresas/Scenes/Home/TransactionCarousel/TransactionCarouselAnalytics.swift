import AnalyticsModule
import Foundation

enum TransactionCarouselAnalytics: AnalyticsKeyProtocol {
    case billTouched(sellerID: Int)
    case transferTouched(sellerID: Int)
    case pixTouched(sellerID: Int, companyName: String)
    case modalShowPixEvent(sellerID: Int, companyName: String)
    case modalNotNowPixEvent(sellerID: Int, companyName: String)
    
    private var name: String {
        switch self {
        case .billTouched:
            return "Bill - Cta Button Click"
        case .transferTouched:
            return "Transfer - Cta Button Click"
        case .pixTouched:
            return "Pix - Cta Button Click"
        case .modalShowPixEvent:
            return "Pix - Button Register"
        case .modalNotNowPixEvent:
            return "Pix - Button Not Now"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .billTouched(sellerID), let .transferTouched(sellerID):
            return ["seller_id": "\(sellerID)"]
        case let .pixTouched(sellerID, companyName),
             let .modalShowPixEvent(sellerID, companyName),
             let .modalNotNowPixEvent(sellerID, companyName):
            return [
                "seller_id": "\(sellerID)",
                "user_name": companyName
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
