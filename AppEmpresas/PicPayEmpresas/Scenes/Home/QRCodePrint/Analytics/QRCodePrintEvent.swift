import AnalyticsModule
import Foundation

enum QRCodePrintEvent: AnalyticsKeyProtocol {
    case downloadPDFAction
    case sendPDFMailAction
    case printAction
    case downloadPDFSuccess
    case sendPDFMailSuccess
    case downloadPDFScreen
    case sendPDFMailScreen
    
    private var name: String {
        switch self {
        case .downloadPDFAction:
            return "Gerar PDF Button"
        case .sendPDFMailAction:
            return "Send PDF by Email Button"
        case .printAction:
            return "Imprimir Button"
        case .downloadPDFSuccess:
            return "Success Gerar PDF Button"
        case .sendPDFMailSuccess:
            return "Success Send PDF by Email Button"
        case .downloadPDFScreen:
            return "Success Compartilhar"
        case .sendPDFMailScreen:
            return "Success Send Email"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: [:], providers: providers)
    }
}
