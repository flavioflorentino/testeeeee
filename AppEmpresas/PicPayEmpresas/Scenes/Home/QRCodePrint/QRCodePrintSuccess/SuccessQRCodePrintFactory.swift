import UI

enum SuccessQRCodePrintFactory {
    static func make(type: SuccessQRCodePrintViewController.SuccessScreenType, email: String? = nil) -> SuccessQRCodePrintViewController {
        let service: SuccessQRCodePrintServicing = SuccessQRCodePrintService()
        let coordinator: SuccessQRCodePrintCoordinating = SuccessQRCodePrintCoordinator()
        let presenter: SuccessQRCodePrintPresenting = SuccessQRCodePrintPresenter(coordinator: coordinator)
        let viewModel = SuccessQRCodePrintViewModel(service: service, presenter: presenter)
        let viewController = SuccessQRCodePrintViewController(type: type, viewModel: viewModel, email: email)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
