import UIKit

enum SuccessQRCodePrintAction {
    case back
    case backRoot
}

protocol SuccessQRCodePrintCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: SuccessQRCodePrintAction)
}

final class SuccessQRCodePrintCoordinator: SuccessQRCodePrintCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: SuccessQRCodePrintAction) {
        switch action {
        case .back:
            viewController?.navigationController?.popViewController(animated: true)
        case .backRoot:
            viewController?.navigationController?.popToRootViewController(animated: true)
        }
    }
}
