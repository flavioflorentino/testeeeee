import Foundation

protocol SuccessQRCodePrintViewModelInputs: AnyObject {
    func backToRoot()
    func backScreen()
}

final class SuccessQRCodePrintViewModel {
    private let service: SuccessQRCodePrintServicing
    private let presenter: SuccessQRCodePrintPresenting

    init(service: SuccessQRCodePrintServicing, presenter: SuccessQRCodePrintPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

extension SuccessQRCodePrintViewModel: SuccessQRCodePrintViewModelInputs {
    func backToRoot() {
        presenter.didNextStep(action: .backRoot)
    }
    
    func backScreen() {
        presenter.didNextStep(action: .back)
    }
}
