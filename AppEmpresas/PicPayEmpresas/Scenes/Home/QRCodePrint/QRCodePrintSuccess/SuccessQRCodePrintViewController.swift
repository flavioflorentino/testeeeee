import AnalyticsModule
import SnapKit
import UI
import UIKit

extension SuccessQRCodePrintViewController.Layout {
    enum Font {
        static let title = UIFont.systemFont(ofSize: 28, weight: .semibold)
        static let defaultDescription = UIFont.systemFont(ofSize: 16, weight: .light)
        static let bold = UIFont.systemFont(ofSize: 16, weight: .bold)
        static let backButtonFont = UIFont.systemFont(ofSize: 14, weight: .medium)
    }
    
    enum Constraints {
        static let successImageTop: CGFloat = 103
        static let successImageSize: CGFloat = 96
        static let titleHeight: CGFloat = 34
        static let titleTop: CGFloat = 13
        static let descriptionTop: CGFloat = 34
        static let leadingMargin: CGFloat = 16
        static let tralingMargin: CGFloat = -16
        static let acceptHeight: CGFloat = 48
        static let backTop: CGFloat = 28
        static let backBottom: CGFloat = -27
    }
    
    enum Others {
        static let cornerRaius: CGFloat = 24
    }
}

final class SuccessQRCodePrintViewController: ViewController<SuccessQRCodePrintViewModelInputs, UIView> {
    private typealias Localizable = Strings.Qrcodeprint
    fileprivate enum Layout {}
    
    enum SuccessScreenType: Int {
        case email
        case share
        case form
    }
    
    private lazy var successImage: UIImageView = {
        let image = Assets.iconBigSuccess.image
        let imageView = UIImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale600.color
        label.font = Layout.Font.title
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = Layout.Font.defaultDescription
        label.numberOfLines = 4
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var acceptButton: UIPPButton = {
        let button = UIPPButton(type: .system)
        button.setTitle(Localizable.acceptButton, for: .normal)
        button.titleLabel?.font = Layout.Font.bold
        button.normalBackgrounColor = Palette.ppColorBranding300.color
        button.highlightedBackgrounColor = Palette.ppColorBranding300.color.withAlphaComponent(0.6)
        button.normalTitleColor = Palette.white.color
        button.highlightedTitleColor = Palette.white.color
        button.cornerRadius = Layout.Others.cornerRaius
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var backButton: UIButton = {
        let button = UIButton()
        let underlineTitle = NSAttributedString(
            string: Localizable.backButton,
            attributes: [
                NSAttributedString.Key.foregroundColor: Palette.ppColorGrayscale400.color,
                NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
                NSAttributedString.Key.font: Layout.Font.backButtonFont
            ]
        )
        button.setAttributedTitle(underlineTitle, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isHidden = false
        button.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        return button
    }()
    
    private var originType: SuccessScreenType?
    private var email: String?
    
    private let dependencies: HasAnalytics = DependencyContainer()
    
    init(type: SuccessScreenType, viewModel: SuccessQRCodePrintViewModelInputs, email: String? = nil) {
        super.init(viewModel: viewModel)
        self.email = email
        originType = type
        switch type {
        case .email:
            configureScreenLikeEmail()
        case .share:
            configureScreenLikeShare()
        case .form:
            configureScreenLikeForm()
        }
    }
 
    override func buildViewHierarchy() {
        view.addSubview(successImage)
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(acceptButton)
        view.addSubview(backButton)
    }
    
    override func configureViews() {
        navigationController?.setNavigationBarHidden(true, animated: true)
        view.backgroundColor = Palette.white.color
    }
    
    override func setupConstraints() {
        successImage.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().offset(Layout.Constraints.successImageTop)
            $0.size.equalTo(Layout.Constraints.successImageSize)
        }
        
        titleLabel.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(successImage.snp.bottom).offset(Layout.Constraints.titleTop)
            $0.height.equalTo(Layout.Constraints.titleHeight)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Layout.Constraints.descriptionTop)
            $0.leading.equalToSuperview().offset(Layout.Constraints.leadingMargin)
            $0.trailing.equalToSuperview().offset(Layout.Constraints.tralingMargin)
        }
        
        acceptButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Layout.Constraints.leadingMargin)
            $0.trailing.equalToSuperview().offset(Layout.Constraints.tralingMargin)
            $0.height.equalTo(Layout.Constraints.acceptHeight)
        }
        
        backButton.snp.makeConstraints {
            $0.top.equalTo(acceptButton.snp.bottom).offset(Layout.Constraints.backTop)
            $0.leading.equalToSuperview().offset(Layout.Constraints.leadingMargin)
            $0.trailing.equalToSuperview().offset(Layout.Constraints.tralingMargin)
            $0.bottom.equalToSuperview().offset(Layout.Constraints.backBottom)
        }
    }
    
    private func configureScreenLikeEmail() {
        dependencies.analytics.log(QRCodePrintEvent.sendPDFMailScreen)
        guard let email = email
            else {
                return
        }
        
        titleLabel.text = Localizable.titleSendEmail
        
        let defaultText = NSMutableAttributedString(
            string: Localizable.descriptionSendEmail,
            attributes: [
                NSAttributedString.Key.font: Layout.Font.defaultDescription,
                NSAttributedString.Key.foregroundColor: Palette.ppColorGrayscale500.color
            ]
        )

        let emailText = NSAttributedString(
            string: email,
            attributes: [
                NSAttributedString.Key.font: Layout.Font.bold,
                NSAttributedString.Key.foregroundColor: Palette.ppColorGrayscale500.color
            ]
        )

        defaultText.append(emailText)
        descriptionLabel.attributedText = defaultText
    }
    
    private func configureScreenLikeShare() {
        dependencies.analytics.log(QRCodePrintEvent.downloadPDFScreen)
        titleLabel.text = Localizable.titleShare
        descriptionLabel.text = Localizable.descriptionShare
    }
    
    private func configureScreenLikeForm() {
        titleLabel.text = Localizable.titleForm
        descriptionLabel.text = Localizable.descriptionForm
        backButton.isHidden = true
    }
    
    @objc
    private func nextAction() {
        viewModel.backToRoot()
    }
    
    @objc
    private func backAction() {
        viewModel.backScreen()
    }
}

// MARK: View Model Outputs
extension SuccessQRCodePrintViewController: SuccessQRCodePrintDisplay {}
