import Core
import Foundation

protocol SuccessQRCodePrintPresenting: AnyObject {
    var viewController: SuccessQRCodePrintDisplay? { get set }
    func didNextStep(action: SuccessQRCodePrintAction)
}

final class SuccessQRCodePrintPresenter: SuccessQRCodePrintPresenting {
    private let coordinator: SuccessQRCodePrintCoordinating
    weak var viewController: SuccessQRCodePrintDisplay?

    init(coordinator: SuccessQRCodePrintCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: SuccessQRCodePrintAction) {
        coordinator.perform(action: action)
    }
}
