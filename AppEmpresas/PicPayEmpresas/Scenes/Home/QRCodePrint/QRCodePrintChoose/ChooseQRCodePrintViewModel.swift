import AnalyticsModule
import Foundation

enum QRCodeChooseButttonsType: Int {
    case email = 0
    case share
}

protocol ChooseQRCodePrintViewModelInputs: AnyObject {
    func showShareScreenSuccess()
    func didSelected(type: QRCodeChooseButttonsType)
}

struct SendEmail: Decodable {
    let email: String
}

final class ChooseQRCodePrintViewModel {
    private let dependencies: HasAnalytics = DependencyContainer()
    private let service: ChooseQRCodePrintServicing
    private let presenter: ChooseQRCodePrintPresenting

    init(service: ChooseQRCodePrintServicing, presenter: ChooseQRCodePrintPresenting) {
        self.service = service
        self.presenter = presenter
    }
    
    private func sendEmailService() {
        dependencies.analytics.log(QRCodePrintEvent.sendPDFMailAction)
        service.sendEmail { [weak self] result in
            self?.presenter.stopLoad()
            switch result {
            case .success(let model):
                self?.dependencies.analytics.log(QRCodePrintEvent.sendPDFMailSuccess)
                self?.presenter.didNextStep(action: .successEmail(email: model?.email ?? ""))
            case .failure(let error):
                self?.presenter.handleError(error: error)
            }
        }
    }
    
    private func downloadPDFService() {
        dependencies.analytics.log(QRCodePrintEvent.downloadPDFAction)
        service.downloadPDF { [weak self] result in
            self?.presenter.stopLoad()
            switch result {
            case .success(let data):
                self?.dependencies.analytics.log(QRCodePrintEvent.downloadPDFSuccess)
                self?.openShare(data: data)
            case .failure(let error):
                self?.presenter.handleError(error: error)
            }
        }
    }
    
    private func openShare(data: Data) {
        presenter.openShare(data: data)
    }
}

extension ChooseQRCodePrintViewModel: ChooseQRCodePrintViewModelInputs {
    func didSelected(type: QRCodeChooseButttonsType) {
        presenter.startLoad()
        switch type {
        case .email:
            sendEmailService()
        case .share:
            downloadPDFService()
        }
    }
    
    func showShareScreenSuccess() {
        presenter.didNextStep(action: .successShare)
    }
}
