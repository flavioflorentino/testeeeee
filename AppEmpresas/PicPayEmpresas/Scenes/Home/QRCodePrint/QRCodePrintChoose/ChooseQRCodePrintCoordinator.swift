import UIKit

enum ChooseQRCodePrintAction {
    case successEmail(email: String)
    case successShare
    case error
}

protocol ChooseQRCodePrintCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ChooseQRCodePrintAction)
}

final class ChooseQRCodePrintCoordinator: ChooseQRCodePrintCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: ChooseQRCodePrintAction) {
        var type: SuccessQRCodePrintViewController.SuccessScreenType  = .email
        var email: String?
        
        switch action {
        case .successShare:
            type = .share
            pushSuccessViewController(type: type, email: email)
        case .successEmail(let value):
            email = value
            pushSuccessViewController(type: type, email: email)
        case .error:
            let errorViewController = ErrorViewController()
            errorViewController.delegate = self
            viewController?.present(errorViewController, animated: true, completion: nil)
        }
    }
    
    private func pushSuccessViewController(type: SuccessQRCodePrintViewController.SuccessScreenType, email: String?) {
        let controller = SuccessQRCodePrintFactory.make(type: type, email: email)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}

extension ChooseQRCodePrintCoordinator: ErrorViewDelegate {
    func didTouchConfirmButton() {
        viewController?.dismiss(animated: true)
    }
}
