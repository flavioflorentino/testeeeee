import Core
import Foundation

protocol ChooseQRCodePrintPresenting: AnyObject {
    var viewController: ChooseQRCodePrintDisplay? { get set }
    func didNextStep(action: ChooseQRCodePrintAction)
    func openShare(data: Data)
    func handleError(error: ApiError)
    func startLoad()
    func stopLoad()
}

final class ChooseQRCodePrintPresenter: ChooseQRCodePrintPresenting {
    private let coordinator: ChooseQRCodePrintCoordinating
    weak var viewController: ChooseQRCodePrintDisplay?

    init(coordinator: ChooseQRCodePrintCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: ChooseQRCodePrintAction) {
        coordinator.perform(action: action)
    }
    
    func openShare(data: Data) {
        viewController?.openShare(data: data)
    }
    
    func handleError(error: ApiError) {
        coordinator.perform(action: .error)
    }
    
    func startLoad() {
        viewController?.startLoad()
    }
       
   func stopLoad() {
        viewController?.stopLoad()
   }
}
