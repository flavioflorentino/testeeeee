import UIKit

enum QRCodeCellType {
    case header
    case info
    case button
}

struct QRCodeChoose {
    let type: QRCodeCellType
    let title: String?
    let description: String?
    let image: UIImage?
    
    init(type: QRCodeCellType, title: String? = nil, description: String? = nil, image: UIImage? = nil) {
        self.type = type
        self.title = title
        self.description = description
        self.image = image
    }
}
