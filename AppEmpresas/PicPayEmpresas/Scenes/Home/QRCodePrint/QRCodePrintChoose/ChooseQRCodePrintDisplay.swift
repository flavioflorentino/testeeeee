import UIKit

protocol ChooseQRCodePrintDisplay: AnyObject {
    func startLoad()
    func stopLoad()
    func openShare(data: Data)
}
