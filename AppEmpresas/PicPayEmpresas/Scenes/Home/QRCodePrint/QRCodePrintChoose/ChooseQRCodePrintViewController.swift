import UI
import UIKit

extension ChooseQRCodePrintViewController.Layout {
    enum Size {
        static let widthOffset: CGFloat = 16
        static let heightHeader: CGFloat = 90
        static let heightCell: CGFloat = 65
    }
}

final class ChooseQRCodePrintViewController: ViewController<ChooseQRCodePrintViewModelInputs, UIView> {
    private typealias Localizable = Strings.Qrcodeprint
    fileprivate enum Layout {}
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 70, left: 0, bottom: 0, right: 0)
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.backgroundColor = Palette.ppColorGrayscale100.color
        collection.dataSource = self
        collection.delegate = self
        return collection
    }()
    
    private let sections = 2
    
    private let sectionInfo = [
        QRCodeChoose(type: .header),
        QRCodeChoose(
            type: .info,
            title: Localizable.chooseInfoTitleDownload,
            description: Localizable.chooseInfoDescriptionDownload
        ),
        QRCodeChoose(
            type: .info,
            title: Localizable.chooseInfoTitlePrint,
            description: Localizable.chooseInfoDescriptionPrint
        ),
        QRCodeChoose(
            type: .info,
            title: Localizable.chooseInfoTitleReceive,
            description: Localizable.chooseInfoDescriptionReceive
        )
    ]
    
    private let sectionButton = [
        QRCodeChoose(type: .button, title: Localizable.chooseTitleButtonEmail, image: Assets.icoSendEmail.image),
        QRCodeChoose(type: .button, title: Localizable.chooseTitleButtonShare, image: Assets.icoShare.image)
    ]
    
    private var dataSource: [[QRCodeChoose]] = []
    
    lazy var loadingView = LoadingView()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Localizable.chooseNavigationTitle
        dataSource = [sectionInfo, sectionButton]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
 
     override func buildViewHierarchy() {
        view.addSubview(collectionView)
    }
    
     override func configureViews() {
        collectionView.register(QRCodeChooseDescriptionViewCell.self, forCellWithReuseIdentifier: String(describing: QRCodeChooseDescriptionViewCell.self))
        collectionView.register(QRCodeChooseHeaderViewCell.self, forCellWithReuseIdentifier: String(describing: QRCodeChooseHeaderViewCell.self))
        collectionView.register(QRCodeChooseButtonCell.self, forCellWithReuseIdentifier: String(describing: QRCodeChooseButtonCell.self))
    }
    
     override func setupConstraints() {
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func createCell(model: QRCodeChoose, indexPath: IndexPath) -> UICollectionViewCell {
        switch model.type {
        case .header:
            guard
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: String(describing: QRCodeChooseHeaderViewCell.self),
                    for: indexPath) as? QRCodeChooseHeaderViewCell
                else {
                    return UICollectionViewCell()
            }
            return cell
        case .info:
            guard
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: String(describing: QRCodeChooseDescriptionViewCell.self),
                    for: indexPath) as? QRCodeChooseDescriptionViewCell
                else {
                    return UICollectionViewCell()
            }
            cell.setup(item: model)
            return cell
        case .button:
            guard
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: String(describing: QRCodeChooseButtonCell.self),
                    for: indexPath) as? QRCodeChooseButtonCell
                else {
                    return UICollectionViewCell()
            }
            cell.setup(model: model)
            return cell
        }
    }
}

// MARK: View Model Outputs
extension ChooseQRCodePrintViewController: ChooseQRCodePrintDisplay {
    func startLoad() {
        startLoadingView()
    }
    
    func stopLoad() {
        stopLoadingView()
    }
    
    func openShare(data: Data) {
        let activityController = UIActivityViewController(activityItems: [data], applicationActivities: nil)
        activityController.completionWithItemsHandler = { [weak self] _, completed, _, error in
            if completed, error == nil {
                self?.viewModel.showShareScreenSuccess()
            }
        }
        activityController.popoverPresentationController?.sourceView = view
        present(activityController, animated: true)
    }
}
extension ChooseQRCodePrintViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let item = dataSource[indexPath.section][indexPath.row]
        guard case .header = item.type else {
            return CGSize(width: collectionView.frame.width - Layout.Size.widthOffset, height: Layout.Size.heightCell)
        }
        return CGSize(width: view.frame.width, height: Layout.Size.heightHeader)
    }
}

extension ChooseQRCodePrintViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let type = QRCodeChooseButttonsType(rawValue: indexPath.row) else {
                return
        }
        viewModel.didSelected(type: type)
    }
}

extension ChooseQRCodePrintViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        sections
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        dataSource[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = dataSource[indexPath.section][indexPath.row]
        let cell = createCell(model: item, indexPath: indexPath)
        return cell
    }
}

extension ChooseQRCodePrintViewController: LoadingViewProtocol {}
