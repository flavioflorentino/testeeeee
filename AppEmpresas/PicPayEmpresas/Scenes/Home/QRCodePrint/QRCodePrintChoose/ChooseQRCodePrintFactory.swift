import Foundation

enum ChooseQRCodePrintFactory {
    static func make() -> ChooseQRCodePrintViewController {
        let service: ChooseQRCodePrintServicing = ChooseQRCodePrintService(DependencyContainer())
        let coordinator: ChooseQRCodePrintCoordinating = ChooseQRCodePrintCoordinator()
        let presenter: ChooseQRCodePrintPresenting = ChooseQRCodePrintPresenter(coordinator: coordinator)
        let viewModel = ChooseQRCodePrintViewModel(service: service, presenter: presenter)
        let viewController = ChooseQRCodePrintViewController(viewModel: viewModel)
        viewController.hidesBottomBarWhenPushed = true

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
