import Core

protocol ChooseQRCodePrintServicing {
    func downloadPDF(completion: @escaping (Result<Data, ApiError>) -> Void)
    func sendEmail(completion: @escaping(Result<SendEmail?, ApiError>) -> Void)
}

final class ChooseQRCodePrintService: ChooseQRCodePrintServicing {
    typealias Dependencies = HasMainQueue
    var dependencies: Dependencies?
    
    init(_ dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func downloadPDF(completion: @escaping (Result<Data, ApiError>) -> Void) {
        Core.Api<Data>(endpoint: QRCodeServiceEndPoint.download).execute {[weak self] result in
            let mappedResult = result
                .map { $0.model }
            self?.dependencies?.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
    
    func sendEmail(completion: @escaping (Result<SendEmail?, ApiError>) -> Void) {
        BizApi<SendEmail>(endpoint: QRCodeServiceEndPoint.email).execute {[weak self] result in
            let mappedResult = result
                .map { $0.model.data }
            self?.dependencies?.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
}
