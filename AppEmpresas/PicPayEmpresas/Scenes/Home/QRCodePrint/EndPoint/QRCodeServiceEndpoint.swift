import Core

enum QRCodeServiceEndPoint {
    case download
    case email
    case address
}

extension QRCodeServiceEndPoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .address:
            return "/seller/address"
        case .email:
            return "/send-ppcode"
        case .download:
            return "/download-ppcode"
        }
    }
    
    var method: HTTPMethod {
        .get
    }
}
