import UI
extension QRCodeChooseButtonCell.Layout {
    enum Font {
        static let titleFont = UIFont.systemFont(ofSize: 14, weight: .medium)
    }
    
    enum Constraints {
        static let leadingIconPadding: CGFloat = 16
        static let trailingIconPadding: CGFloat = -8
        static let widthIcon: CGFloat = 40
        static let trailingTitlePadding: CGFloat = -5
        static let arrowTrailingPadding: CGFloat = -19
    }
    
    enum Others {
        static let cornerRadius: CGFloat = 5
    }
}

final class QRCodeChooseButtonCell: UICollectionViewCell, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.Font.titleFont
        label.textColor = Palette.ppColorGrayscale600.color
        return label
    }()
    
    private lazy var arrowImage: UIImageView = {
        let image = Assets.icoArrow.image
        let imageView = UIImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    func setupConstraints() {
        iconImage.layout {
            $0.centerY == centerYAnchor
            $0.leading == leadingAnchor + Layout.Constraints.leadingIconPadding
            $0.trailing == titleLabel.leadingAnchor + Layout.Constraints.trailingIconPadding
            $0.width == Layout.Constraints.widthIcon
        }
        
        titleLabel.layout {
            $0.centerY == iconImage.centerYAnchor
            $0.trailing == arrowImage.leadingAnchor + Layout.Constraints.trailingTitlePadding
        }
        
        arrowImage.layout {
            $0.centerY == iconImage.centerYAnchor
            $0.trailing == self.trailingAnchor + Layout.Constraints.arrowTrailingPadding
        }
    }
    
    func buildViewHierarchy() {
        addSubview(iconImage)
        addSubview(titleLabel)
        addSubview(arrowImage)
    }
    
    func configureViews() {
        backgroundColor = Palette.white.color
        layer.masksToBounds = true
        layer.cornerRadius = Layout.Others.cornerRadius
    }
    
    func setup(model: QRCodeChoose) {
        guard
            let title = model.title,
            let image = model.image
            else {
                return
        }
        
        titleLabel.text = title
        iconImage.image = image
        buildLayout()
    }
}
