import UI

extension QRCodeFormHeaderView.Layout {
    enum Constraints {
        static let headerHeight: CGFloat = 100
        static let imageHeaderTop: CGFloat = 32
    }
}

final class QRCodeFormHeaderView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var headerBackView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.Business.bluishGray200.color
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var qrCodeImage: UIImageView = {
        let image = Assets.icoQrCodePrint.image
        let imageView = UIImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
         NSLayoutConstraint.activate([
             headerBackView.topAnchor.constraint(equalTo: self.topAnchor),
             headerBackView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
             headerBackView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
             headerBackView.heightAnchor.constraint(equalToConstant: Layout.Constraints.headerHeight)
         ])
         
        NSLayoutConstraint.activate([
             qrCodeImage.topAnchor.constraint(equalTo: self.topAnchor, constant: Layout.Constraints.imageHeaderTop),
             qrCodeImage.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        ])
    }
    
    func buildViewHierarchy() {
        addSubview(headerBackView)
        addSubview(qrCodeImage)
    }
}
