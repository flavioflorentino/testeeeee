import UI

extension QRCodeFormInfoView.Layout {
    enum Constraints {
        static let marginText: CGFloat = 24
        static let topOffeset: CGFloat = -16
        static let titleHeight: CGFloat = 24
    }
    
    enum Font {
        static let title = UIFont.systemFont(ofSize: 16, weight: .semibold)
        static let description = UIFont.systemFont(ofSize: 12)
    }
}

final class QRCodeFormInfoView: UIView, ViewConfiguration {
    private typealias Localizable = Strings.Qrcodeprint
    fileprivate enum Layout {}
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .center
        label.font = Layout.Font.title
        label.text = Localizable.titleInfoForm
        return label
    }()
    
    private lazy var firstParagraphDescription: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = Layout.Font.description
        label.text = Localizable.firstParagraphForm
        label.numberOfLines = 4
        return label
    }()
    
    private lazy var secondParagraphDescription: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = Layout.Font.description
        label.text = Localizable.seconfParagraphForm
        label.numberOfLines = 4
        return label
    }()
    
    private lazy var thirdParagraphDescription: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = Layout.Font.description
        label.text = Localizable.thirdParagraphForm
        label.numberOfLines = 4
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
             titleLabel.topAnchor.constraint(equalTo: self.topAnchor),
             titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Layout.Constraints.marginText),
             titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Layout.Constraints.marginText),
             titleLabel.bottomAnchor.constraint(equalTo: firstParagraphDescription.topAnchor, constant: Layout.Constraints.topOffeset),
             titleLabel.heightAnchor.constraint(equalToConstant: Layout.Constraints.titleHeight)
        ])

        NSLayoutConstraint.activate([
            firstParagraphDescription.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Layout.Constraints.marginText),
            firstParagraphDescription.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Layout.Constraints.marginText),
            firstParagraphDescription.bottomAnchor.constraint(equalTo: secondParagraphDescription.topAnchor, constant: Layout.Constraints.topOffeset)
        ])

        NSLayoutConstraint.activate([
            secondParagraphDescription.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Layout.Constraints.marginText),
            secondParagraphDescription.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Layout.Constraints.marginText),
            secondParagraphDescription.bottomAnchor.constraint(equalTo: thirdParagraphDescription.topAnchor, constant: Layout.Constraints.topOffeset)
        ])

        NSLayoutConstraint.activate([
            thirdParagraphDescription.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Layout.Constraints.marginText),
            thirdParagraphDescription.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Layout.Constraints.marginText)
        ])
    }
    
    func buildViewHierarchy() {
        addSubview(titleLabel)
        addSubview(firstParagraphDescription)
        addSubview(secondParagraphDescription)
        addSubview(thirdParagraphDescription)
    }
}
