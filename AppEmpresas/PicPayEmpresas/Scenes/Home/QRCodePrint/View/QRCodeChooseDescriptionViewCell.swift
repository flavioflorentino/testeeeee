import UI

extension QRCodeChooseDescriptionViewCell.Layout {
    enum Font {
        static let descriptionFont = UIFont.systemFont(ofSize: 12)
        static let titleFont = UIFont.systemFont(ofSize: 16, weight: .bold)
        static let dotFont = UIFont.systemFont(ofSize: 24)
    }
    
    enum Constraints {
        static let leadingPadding: CGFloat = 8
        static let trailingPadding: CGFloat = -24
        static let topDescriptionPadding: CGFloat = 4
        static let leadingDescriptionPadding: CGFloat = 24
    }
    
    enum Others {
        static let numberOfLines = 4
    }
}

final class QRCodeChooseDescriptionViewCell: UICollectionViewCell, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = Layout.Others.numberOfLines
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.Font.descriptionFont
        label.numberOfLines = Layout.Others.numberOfLines
        label.textColor = Palette.ppColorGrayscale400.color
        return label
    }()
    
    func setup(item: QRCodeChoose) {
        guard
            let description = item.description,
            let title = item.title
            else {
            return
        }
        descriptionLabel.text = description
        configureTitle(with: title)
        buildViewHierarchy()
        configureViews()
        setupConstraints()
    }
    
    func setupConstraints() {
        titleLabel.layout {
            $0.top == topAnchor
            $0.leading == leadingAnchor + Layout.Constraints.leadingPadding
            $0.trailing == trailingAnchor + Layout.Constraints.trailingPadding
        }
        
        descriptionLabel.layout {
            $0.top == titleLabel.bottomAnchor + Layout.Constraints.topDescriptionPadding
            $0.leading == leadingAnchor + Layout.Constraints.leadingDescriptionPadding
            $0.trailing == trailingAnchor + Layout.Constraints.trailingPadding
        }
    }
    
    func buildViewHierarchy() {
        addSubview(titleLabel)
        addSubview(descriptionLabel)
    }
    
    func configureTitle(with text: String) {
        let dotText = NSMutableAttributedString(
            string: "• ",
            attributes: [
                NSAttributedString.Key.font: Layout.Font.dotFont,
                NSAttributedString.Key.foregroundColor: Palette.ppColorBranding300.color
            ]
        )
        
        let titleAttributedText = NSAttributedString(
            string: text,
            attributes: [
                NSAttributedString.Key.font: Layout.Font.titleFont,
                NSAttributedString.Key.foregroundColor: Palette.ppColorGrayscale500.color
            ]
        )
        
        dotText.append(titleAttributedText)
        titleLabel.attributedText = dotText
    }
}
