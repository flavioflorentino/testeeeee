import UI

extension QRCodeChooseHeaderViewCell.Layout {
    enum Padding {
        static let gradientTopPadding: CGFloat = -70
        static let printTopPadding: CGFloat = -85
    }
    
    enum Size {
        static let gradientHeight: CGFloat = 100
    }
}

final class QRCodeChooseHeaderViewCell: UICollectionViewCell, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var gradientLayer: CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.colors = [
            Palette.Business.backgroundDefault.gradientColor.from.cgColor,
            Palette.Business.backgroundDefault.gradientColor.to.cgColor
        ]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        return gradient
    }()
    
    private lazy var gradientRect: UIView = {
        let view = UIView()
        view.backgroundColor = .green
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var printImage: UIImageView = {
        let literal = Assets.imgPrintG.image
        let image = UIImageView(image: literal)
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            gradientRect.topAnchor.constraint(equalTo: self.topAnchor, constant: Layout.Padding.gradientTopPadding),
            gradientRect.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            gradientRect.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            gradientRect.heightAnchor.constraint(equalToConstant: Layout.Size.gradientHeight)
        ])
        
        NSLayoutConstraint.activate([
            printImage.topAnchor.constraint(equalTo: gradientRect.bottomAnchor, constant: Layout.Padding.printTopPadding),
            printImage.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        ])
    }
    
    func buildViewHierarchy() {
        addSubview(gradientRect)
        gradientRect.layer.insertSublayer(gradientLayer, at: 0)
        addSubview(printImage)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = gradientRect.bounds
    }
}
