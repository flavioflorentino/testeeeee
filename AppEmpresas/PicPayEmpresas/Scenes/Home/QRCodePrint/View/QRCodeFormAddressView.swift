import LegacyPJ
import UI
import UIKit

extension QRCOdeFormAddressView.Layout {
    enum Constraints {
        static let marginDefault: CGFloat = 24
        static let width: CGFloat = 90
        static let height: CGFloat = 40
        static let topSpacing: CGFloat = 23
        static let textHeight: CGFloat = 24
        static let topSpacingText: CGFloat = 16
        static let horizontalSpacingText: CGFloat = -16
    }
}

final class QRCOdeFormAddressView: UIView, ViewConfiguration {
    private typealias Localizable = Strings.Qrcodeprint
    fileprivate enum Layout {}
    
    private lazy var titleAddressLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = Localizable.registerAddress
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        return label
    }()
    
    private lazy var cepField: UITextField = {
        let text = UIRoundedTextField()
        text.layer.borderColor = Palette.ppColorBranding300.color.cgColor
        text.placeholder = Localizable.cepPlaceHolder
        text.keyboardType = .numberPad
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()
    
    private lazy var streetTextField: UIRoundedTextField = {
        let text = UIRoundedTextField()
        text.layer.borderColor = Palette.ppColorBranding300.color.cgColor
        text.placeholder = Localizable.streetPlaceHolder
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()
    
    private lazy var numberTextField: UIRoundedTextField = {
        let text = UIRoundedTextField()
        text.layer.borderColor = Palette.ppColorBranding300.color.cgColor
        text.placeholder = Localizable.numberPlaceHolder
        text.keyboardType = .numberPad
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()
    
    private lazy var neiborhoodTextField: UIRoundedTextField = {
        let text = UIRoundedTextField()
        text.layer.borderColor = Palette.ppColorBranding300.color.cgColor
        text.placeholder = Localizable.neighborhoodPlaceHolder
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()
    
    private lazy var cityTextField: UIRoundedTextField = {
        let text = UIRoundedTextField()
        text.layer.borderColor = Palette.ppColorBranding300.color.cgColor
        text.placeholder = Localizable.cityPlaceHolder
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()
    
    private lazy var stateTextField: UIRoundedTextField = {
        let text = UIRoundedTextField()
        text.layer.borderColor = Palette.ppColorBranding300.color.cgColor
        text.placeholder = Localizable.statePlaceHolder
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(address: Address) {
        cepField.text = address.cep
        streetTextField.text = address.address
        numberTextField.text = address.number
        neiborhoodTextField.text = address.district
        cityTextField.text = address.city
        stateTextField.text = address.state
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            titleAddressLabel.topAnchor.constraint(equalTo: self.topAnchor),
            titleAddressLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Layout.Constraints.marginDefault),
            titleAddressLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Layout.Constraints.marginDefault),
            titleAddressLabel.heightAnchor.constraint(equalToConstant: Layout.Constraints.textHeight)
        ])

        NSLayoutConstraint.activate([
            cepField.topAnchor.constraint(equalTo: titleAddressLabel.bottomAnchor, constant: Layout.Constraints.topSpacingText),
            cepField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Layout.Constraints.marginDefault),
            cepField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Layout.Constraints.marginDefault),
            cepField.heightAnchor.constraint(equalToConstant: Layout.Constraints.height)
        ])

        NSLayoutConstraint.activate([
            streetTextField.topAnchor.constraint(equalTo: cepField.bottomAnchor, constant: Layout.Constraints.topSpacing),
            streetTextField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Layout.Constraints.marginDefault),
            streetTextField.trailingAnchor.constraint(equalTo: numberTextField.leadingAnchor, constant: Layout.Constraints.horizontalSpacingText),
            streetTextField.heightAnchor.constraint(equalToConstant: Layout.Constraints.height)
        ])

        NSLayoutConstraint.activate([
            numberTextField.centerYAnchor.constraint(equalTo: streetTextField.centerYAnchor),
            numberTextField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Layout.Constraints.marginDefault),
            numberTextField.widthAnchor.constraint(equalToConstant: Layout.Constraints.width),
            numberTextField.heightAnchor.constraint(equalToConstant: Layout.Constraints.height)
        ])

        NSLayoutConstraint.activate([
            neiborhoodTextField.topAnchor.constraint(equalTo: streetTextField.bottomAnchor, constant: Layout.Constraints.topSpacing),
            neiborhoodTextField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Layout.Constraints.marginDefault),
            neiborhoodTextField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Layout.Constraints.marginDefault),
            neiborhoodTextField.heightAnchor.constraint(equalToConstant: Layout.Constraints.height)
        ])

        NSLayoutConstraint.activate([
            cityTextField.topAnchor.constraint(equalTo: neiborhoodTextField.bottomAnchor, constant: Layout.Constraints.topSpacing),
            cityTextField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Layout.Constraints.marginDefault),
            cityTextField.trailingAnchor.constraint(equalTo: stateTextField.leadingAnchor, constant: Layout.Constraints.horizontalSpacingText),
            cityTextField.heightAnchor.constraint(equalToConstant: Layout.Constraints.height)
        ])

        NSLayoutConstraint.activate([
            stateTextField.centerYAnchor.constraint(equalTo: cityTextField.centerYAnchor),
            stateTextField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Layout.Constraints.marginDefault),
            stateTextField.widthAnchor.constraint(equalToConstant: Layout.Constraints.width),
            stateTextField.heightAnchor.constraint(equalToConstant: Layout.Constraints.height)
        ])
    }
    
    func buildViewHierarchy() {
        addSubview(titleAddressLabel)
        addSubview(cepField)
        addSubview(streetTextField)
        addSubview(numberTextField)
        addSubview(neiborhoodTextField)
        addSubview(cityTextField)
        addSubview(stateTextField)
    }
}
