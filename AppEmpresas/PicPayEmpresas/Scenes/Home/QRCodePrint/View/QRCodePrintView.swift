import AssetsKit
import UI
import UIKit

protocol QRCodePrintViewActionDelegate: AnyObject {
    func confirmPrintButtonAction()
}

extension QRCodePrintView.Layout {
    enum PrintImage {
        static let size = CGSize(width: 93, height: 96)
    }

    enum BoxView {
        static let shadowRadius: CGFloat = 2
        static let shadowOpacity: Float = 1
    }
}

final class QRCodePrintView: UIView, ViewConfiguration {
    private typealias Localizable = Strings.Qrcodeprint
    fileprivate enum Layout {}
    
    private lazy var boxView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.white.color
        view.layer.cornerRadius = CornerRadius.light
        view.layer.shadowColor = Colors.grayscale100.color.cgColor
        view.layer.shadowOpacity = Layout.BoxView.shadowOpacity
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = Layout.BoxView.shadowRadius
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
            .with(\.text, Localizable.qrCodeTitle)
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.text, Localizable.qrCodeDescription)
        return label
    }()

    private lazy var printImage = UIImageView(image: Resources.Illustrations.iluQrDownload.image)

    private lazy var printButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.printButtonTitle, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector(actionPrintButton), for: .touchUpInside)
        return button
    }()

    weak var delegate: QRCodePrintViewActionDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        self.backgroundColor = Colors.white.color
    }
    
    func setupConstraints() {
        boxView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
        }

        titleLabel.snp.makeConstraints {
            $0.top.leading.equalTo(boxView).inset(Spacing.base02)
            $0.trailing.equalTo(printImage.snp.leading).offset(-Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(boxView).offset(Spacing.base02)
            $0.trailing.equalTo(printImage.snp.leading).offset(-Spacing.base02)
        }

        printButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalTo(boxView).offset(Spacing.base02)
            $0.trailing.equalTo(printImage.snp.leading).offset(-Spacing.base02)
            $0.bottom.equalTo(boxView).offset(-Spacing.base02)
        }
        
        printImage.snp.makeConstraints {
            $0.centerY.equalTo(boxView)
            $0.trailing.equalTo(boxView).offset(-Spacing.base02)
            $0.size.equalTo(Layout.PrintImage.size)
        }
    }
    
    func buildViewHierarchy() {
        addSubview(boxView)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        addSubview(printImage)
        addSubview(printButton)
    }
    
    @objc
    private func actionPrintButton() {
        delegate?.confirmPrintButtonAction()
    }
}
