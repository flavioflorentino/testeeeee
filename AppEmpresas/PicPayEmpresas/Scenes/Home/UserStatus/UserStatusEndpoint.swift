import Core

enum UserStatusEndpoint {
    case checkStatus
}

extension UserStatusEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .checkStatus:
            return "/check"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .checkStatus:
            return .get
        }
    }
    
    var body: Data? {
        switch self {
        case .checkStatus:
            return nil
        }
    }
}
