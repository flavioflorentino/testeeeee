import Foundation
import Core

protocol UserStatusServicing {
    func checkStatus(completion: @escaping(Result<UserStatus?, ApiError>) -> Void)
}

final class UserStatusService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension UserStatusService: UserStatusServicing {
    func checkStatus(completion: @escaping (Result<UserStatus?, ApiError>) -> Void) {
        let endPoint = UserStatusEndpoint.checkStatus
        BizApi<UserStatus>(endpoint: endPoint).bizExecute {[weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model.data)
                completion(mappedResult)
            }
        }
    }
}
