struct UserStatus: Decodable, Equatable {
    private enum CodingKeys: String, CodingKey {
        case boardStep = "board_step"
        case boardRequestRefused = "board_request_refused"
        case boardRequested = "board_requested"
        case config
        case sellerID = "seller_id"
    }
    
    let boardRequestRefused: Bool
    let boardStep: String?
    let boardRequested: Bool
    let config: UserStatusConfig?
    let sellerID: Int
}

struct UserStatusConfig: Decodable, Equatable {
    let blocked: String?
}
