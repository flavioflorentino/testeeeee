import AssetsKit
import CoreSellerAccount
import Foundation

private extension HomePresenter.Layout {
    enum ModalInfo {
        static let size = CGSize(width: 150, height: 53)
    }
}

protocol HomePresenting: AnyObject {
    var viewController: HomeDisplaying? { get set }
    
    func startLoading()
    func stopLoading()
    func endRefreshing()
    func presentAccountErrorComponent(bankAccountError: BankAccountValidation)
    func presentWalletComponent(bankAccountError: BankAccountValidation?, seller: Seller, companyInfo: CompanyInfo?)
    func presentCarouselComponent(seller: Seller, userStatus: UserStatus?)
    func presentMaterialHeader(userStatus: UserStatus)
    func presentTransactionComponent(seller: Seller)
    func presentNewTransactionComponent(seller: Seller)
    func presentAdvertising()
    
    func didNextStep(action: HomeAction)
}

final class HomePresenter {
    private typealias TransactionLocalizable = Strings.Transaction
    
    fileprivate enum Layout { }
    
    private let coordinator: HomeCoordinating
    weak var viewController: HomeDisplaying?

    init(coordinator: HomeCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - HomePresenting
extension HomePresenter: HomePresenting {
    func startLoading() {
        viewController?.startLoading()
    }

    func stopLoading() {
        viewController?.stopLoading()
    }

    func endRefreshing() {
        viewController?.endRefreshing()
    }
    
    func presentAccountErrorComponent(bankAccountError: BankAccountValidation) {
        guard
            let validationIssue = bankAccountError.validationIssue,
            let buttonTitle = bankAccountError.validationIssue?.action?.message else { return }
            
        let text = validationIssue.title ?? validationIssue.message
        
        viewController?.displayAccountErrorComponent(text: text, buttonTitle: buttonTitle)
    }
    
    func presentWalletComponent(bankAccountError: BankAccountValidation?, seller: Seller, companyInfo: CompanyInfo?) {
        var bankError: WithdrawalOptionsBankError?
        
        if let bankAccountError = bankAccountError {
            let individual = companyInfo?.ableToDigitalAccount ?? false
            let companyType: CompanyType = individual ? .individual : .multiPartners
            bankError = WithdrawalOptionsBankError(bankError: bankAccountError, seller: seller, companyType: companyType)
        }
        
        viewController?.displayWalletComponent(bankError: bankError)
    }
    
    func presentCarouselComponent(seller: Seller, userStatus: UserStatus?) {
        viewController?.displayCarouselComponent(hasBiometry: seller.biometry, seller: seller, userStatus: userStatus)
    }
    
    func presentMaterialHeader(userStatus: UserStatus) {
        viewController?.displayMaterialHeader(userStatus: userStatus)
    }
    
    func presentTransactionComponent(seller: Seller) {
        viewController?.displayTransactionComponent(seller: seller)
    }
    
    func presentNewTransactionComponent(seller: Seller) {
        viewController?.displayNewTransactionComponent(seller: seller)
    }
    
    func presentAdvertising() {
        let size = Layout.ModalInfo.size
        let modalInfoImage = AlertModalInfoImage(image: Resources.Logos.pixLogo.image, size: size)
        let info = AlertModalInfo(
            imageInfo: modalInfoImage,
            title: TransactionLocalizable.pixCarouselAlertTitle,
            subtitle: TransactionLocalizable.pixCarouselAlertDescription,
            buttonTitle: TransactionLocalizable.pixCarouselAlertButton,
            secondaryButtonTitle: TransactionLocalizable.pixCarouselAlertSecondaryButton
        )
        viewController?.displayAdvertising(with: info)
    }
    
    func didNextStep(action: HomeAction) {
        coordinator.perform(action: action)
    }
}
