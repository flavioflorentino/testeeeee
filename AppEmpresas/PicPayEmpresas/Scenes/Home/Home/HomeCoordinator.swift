import CoreSellerAccount
import PIX
import UIKit

enum HomeAction: Equatable {
    case editBankAccount(CompanyType, Seller)
    case pix(userInfo: KeyManagerBizUserInfo, flow: PIXBizFlow)
}

protocol HomeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: HomeAction)
}

final class HomeCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - HomeCoordinating
extension HomeCoordinator: HomeCoordinating {
    func perform(action: HomeAction) {
        switch action {
        case let .editBankAccount(companyType, seller):
            let controller = BankAccountTipsFactory.makeEditTips(companyType: companyType, seller: seller)
            viewController?.pushViewController(controller)
        case let .pix(userInfo, flow):
            navigateToPix(userInfo: userInfo, flow: flow)
        }
    }
}

private extension HomeCoordinator {
    func navigateToPix(userInfo: KeyManagerBizUserInfo, flow: PIXBizFlow) {
        guard let navigationController = viewController?.navigationController else { return }
        
        let coordinator = PIXBizFlowCoordinator(with: navigationController, userInfo: userInfo)
        coordinator.perform(flow: flow)
    }
}
