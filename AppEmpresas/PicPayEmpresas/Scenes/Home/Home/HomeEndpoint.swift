import Core
import Foundation

struct BankAccountValidation: Decodable, Equatable {
    let bankID: Int
    let validationIssue: BankAccountErrorValidation?
    
    private enum CodingKeys: String, CodingKey {
        case bankID = "bank_account_id"
        case validationIssue = "validation_issue"
    }
}

struct BankAccountErrorValidation: Decodable, Equatable {
    enum ReasonType: String, Decodable {
        case invalidBank = "bank_account_invalid"
        case other
    }
    
    let title: String?
    let message: String
    let reason: ReasonType
    let action: BankAccountErrorAction?
}

struct BankAccountErrorAction: Decodable, Equatable {
    let message: String
    let deepLink: String
    
    private enum CodingKeys: String, CodingKey {
        case message
        case deepLink = "deep_link"
    }
}

enum HomeEndpoint {
    case checkBankAccount
}

extension HomeEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .checkBankAccount:
            return "/v2/bank-account/check"
        }
    }
}
