import AnalyticsModule
import Core
import CoreSellerAccount
import Foundation
import LegacyPJ
import PIX
import FeatureFlag

protocol HomeInteracting: AnyObject {
    func loadHome()
    func snackBarAction()
    func trackPixNotNow()
    func trackPixRegister()
    func pixAction()
}

final class HomeInteractor {
    typealias Dependencies =
        HasApiSeller &
        HasApiSignUp &
        HasMainQueue &
        HasAnalytics &
        HasAuthManager &
        HasKVStore &
        HasSellerInfo &
        HasFeatureManager
    
    private let dependencies: Dependencies

    private let service: HomeServicing
    private let presenter: HomePresenting
    
    private var seller: Seller?
    private var userStatus: UserStatus?
    private var bankAccountError: BankAccountValidation?
    private var accountDetails: AccountDetails?
    private var companyInfo: CompanyInfo? {
        didSet {
            let value = companyInfo?.individual ?? false
            dependencies.kvStore.set(value: value, with: BizKvKey.individualCompany)
        }
    }

    init(service: HomeServicing, presenter: HomePresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - HomeInteracting

extension HomeInteractor: HomeInteracting {
    func loadHome() {
        presenter.startLoading()
        let group = DispatchGroup()
        
        group.enter()
        loadSeller {
            group.leave()
        }
        
        group.enter()
        loadCheckStatus {
            group.leave()
        }
        
        group.enter()
        checkBankAccountErrors {
            group.leave()
        }
        
        group.enter()
        checkAccountDetails {
            group.leave()
        }
        
        group.notify(queue: dependencies.mainQueue) { [weak self] in
            guard let self = self else { return }
            
            self.presenter.stopLoading()
            self.didLoadHome(
                seller: self.seller,
                bankAccountError: self.bankAccountError,
                userStatus: self.userStatus,
                companyInfo: self.companyInfo
            )
        }
    }
    
    func snackBarAction() {
        guard let seller = seller, let companyInfo = companyInfo else { return }
        
        let companyType: CompanyType = companyInfo.individual ? .individual : .multiPartners
        presenter.didNextStep(action: .editBankAccount(companyType, seller))
    }
    
    func trackPixNotNow() {
        guard let seller = seller else { return }
        let companyName = seller.razaoSocial ?? ""
        let event = TransactionCarouselAnalytics.modalNotNowPixEvent(sellerID: seller.id, companyName: companyName)
        dependencies.analytics.log(event)
    }
    
    func trackPixRegister() {
        guard let seller = seller else { return }
        let companyName = seller.razaoSocial ?? ""
        let event = TransactionCarouselAnalytics.modalShowPixEvent(sellerID: seller.id, companyName: companyName)
        dependencies.analytics.log(event)
    }
    
    func pixAction() {
        guard
            let seller = seller,
            let user = dependencies.authManager.user,
            let name = seller.razaoSocial,
            let cnpj = seller.cnpj,
            let email = seller.email,
            let phone = user.phone else { return }
        
        let userInfo = KeyManagerBizUserInfo(
            name: name,
            cnpj: cnpj,
            mail: email,
            phone: phone,
            userId: String(seller.id)
        )
        
        let welcomePresented = service.welcomePresented
        let mustPresentHub = service.mustPresentHub
        
        if mustPresentHub {
            let flow: PIXBizFlow = welcomePresented ? .hub : .welcomePage
            presenter.didNextStep(action: .pix(userInfo: userInfo, flow: flow))
            return
        }
        
        let action: PIXBizOptinAction = welcomePresented ? .keyManager(input: .none) : .onboarding
        presenter.didNextStep(action: .pix(userInfo: userInfo, flow: .optin(action: action)))
    }
}

// MARK: - Private Methods

private extension HomeInteractor {
    func loadSeller(_ completion: @escaping() -> Void) {
        dependencies.apiSeller.data { [weak self] result in
            switch result {
            case .success(let seller):
                self?.seller = seller
                TrackingManager.setupMixpanel(sellerID: seller.id)
                
                guard let cnpj = seller.cnpj else {
                    completion()
                    return
                }
                
                self?.loadCompanyInfo(cnpj: cnpj, completion: completion)
            case .failure:
                completion()
            } 
        }
    }

    func loadCheckStatus(_ completion: @escaping() -> Void) {
        service.checkStatus(completion: { [weak self] result in
            switch result {
            case .success(let model):
                self?.userStatus = model
            case .failure:
                break
            }
            completion()
        })
    }

    func checkBankAccountErrors(_ completion: @escaping() -> Void) {
        guard service.bankAccountWarningsEnabled else {
            completion()
            return
        }
        
        service.checkBankAccount { [weak self] result in
            switch result {
            case .success(let accountError):
                self?.bankAccountError = accountError
                
                if let reason = accountError?.validationIssue?.reason.rawValue {
                    let event = BankAccountErrorAnalytics.home(errorType: reason)
                    self?.dependencies.analytics.log(event)
                }
            case .failure:
                break
            }
            completion()
        }
    }

    func loadCompanyInfo(cnpj: String, completion: @escaping() -> Void) {
        guard service.bankAccountWarningsEnabled else {
            completion()
            return
        }
        
        dependencies.apiSignUp.verifySeller(cnpj) { [weak self] result in
            switch result {
            case .success(let companyInfo):
                self?.companyInfo = companyInfo
            case .failure:
                break
            }
            completion()
        }
    }
    
    func checkAccountDetails(completion: @escaping() -> Void) {
        guard service.receiveAccountEnabled else {
            completion()
            return
        }
        
        dependencies.sellerInfo.accountDetails { [weak self] result in
            switch result {
            case .success(let accountDetails):
                self?.accountDetails = accountDetails
            case .failure:
                break
            }
            completion()
        }
    }
    
    func didLoadHome(
        seller: Seller?,
        bankAccountError: BankAccountValidation?,
        userStatus: UserStatus?,
        companyInfo: CompanyInfo?
    ) {
        guard let seller = seller else {
            presenter.endRefreshing()
            return
        }
        
        if let bankAccountError = bankAccountError {
            presenter.presentAccountErrorComponent(bankAccountError: bankAccountError)
        }
        
        setupWalletComponent(bankAccountError: bankAccountError, seller: seller, companyInfo: companyInfo)
        
        if seller.digitalAccount {
            presenter.presentCarouselComponent(seller: seller, userStatus: userStatus)
        }
        
        setupMaterialHeader(hasDigitalAccount: seller.digitalAccount, userStatus: userStatus)
        
        if dependencies.featureManager.isActive(.isExternalLinkAvailable) {
            presenter.presentNewTransactionComponent(seller: seller)
        } else {
            presenter.presentTransactionComponent(seller: seller)
        }
        
        setupAdvertising()
        presenter.endRefreshing()
    }
    
    func setupWalletComponent(bankAccountError: BankAccountValidation?, seller: Seller, companyInfo: CompanyInfo?) {
        if service.receiveAccountEnabled {
            presenter.presentWalletComponent(bankAccountError: bankAccountError, seller: seller, companyInfo: companyInfo)
            return
        }
        
        guard seller.digitalAccount else { return }
        presenter.presentWalletComponent(bankAccountError: bankAccountError, seller: seller, companyInfo: companyInfo)
    }
    
    func setupMaterialHeader(hasDigitalAccount: Bool, userStatus: UserStatus?) {
        guard let userStatus = userStatus,
              service.allowApplyMaterial,
              !hasDigitalAccount,
              !userStatus.boardRequestRefused else {
            return
        }
        presenter.presentMaterialHeader(userStatus: userStatus)
    }
    
    func setupAdvertising() {
        guard seller?.biometry == true,
              seller?.digitalAccount == true,
              service.pixPresentingEnabled,
              !dependencies.kvStore.boolFor(BizKvKey.didPresentAdvertisingPix) else {
            return
        }
        
        dependencies.kvStore.set(value: true, with: BizKvKey.didPresentAdvertisingPix)

        presenter.presentAdvertising()
    }
}
