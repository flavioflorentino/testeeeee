import AssetsKit
import SnapKit
import UI
import UIKit

protocol HomeLoadable {
    func reload()
}

protocol HomeDisplaying: AnyObject {
    func startLoading()
    func stopLoading()
    func endRefreshing()
    func displayAccountErrorComponent(text: String, buttonTitle: String?)
    func displayWalletComponent(bankError: WithdrawalOptionsBankError?)
    func displayCarouselComponent(hasBiometry: Bool, seller: Seller, userStatus: UserStatus?)
    func displayMaterialHeader(userStatus: UserStatus)
    func displayNewTransactionComponent(seller: Seller)
    func displayTransactionComponent(seller: Seller)
    func displayAdvertising(with modalInfo: AlertModalInfo)
}

final class HomeViewController: ViewController<HomeInteracting, UIView>, StatefulTransitionViewing {
    private lazy var leftNavigationItem: UIBarButtonItem = {
        let imageView = UIImageView(image: Resources.Logos.businessLogo.image)
        return UIBarButtonItem(customView: imageView)
    }()
    
    private lazy var rightNavigationItem: UIBarButtonItem = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, Colors.grayscale900.lightColor)
        label.text = Iconography.eye.rawValue
        return UIBarButtonItem(customView: label)
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .clear
        scrollView.refreshControl = refreshControl
        return scrollView
    }()
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefreshControl), for: .valueChanged)
        return refreshControl
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private var feedbackContentView: UIView?
    private var walletViewController: WalletBalanceViewController?
    private var carouselViewController: TransactionCarouselViewController?
    private var materialSolicitationController: MaterialSolicitationHeaderViewController?
    private var transactionsViewController: TransactionsContainerViewController?
    private var newTransactionsViewController: TransactionsViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
        interactor.loadHome()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureViews()
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(stackView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        containerView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalTo(view)
            $0.leading.trailing.equalToSuperview()
        }
        
        stackView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        setupNavigationBar()
    }
}

// MARK: - HomeDisplaying

extension HomeViewController: HomeDisplaying {
    func startLoading() {
        beginState(model: StateLoadingViewModel(message: ""))
    }
    
    func stopLoading() {
        endState()
    }

    func endRefreshing() {
        refreshControl.endRefreshing()
    }
    
    func displayAccountErrorComponent(text: String, buttonTitle: String?) {
        if let feedbackContentView = feedbackContentView {
            feedbackContentView.removeFromSuperview()
            self.feedbackContentView = nil
        }
        
        let layoutType: ApolloFeedbackCardViewLayoutType
        if let buttonTitle = buttonTitle {
            let action = ApolloFeedbackCardButtonAction(title: buttonTitle, action: accountErrorAction)
            layoutType = .oneAction(action)
        } else {
            layoutType = .onlyText
        }
        
        let feedbackCard = ApolloFeedbackCard(
            description: text,
            iconType: .error,
            layoutType: layoutType,
            emphasisType: .high
        )
        
        let contentView = UIView()
        contentView.addSubview(feedbackCard)
        stackView.addArrangedSubview(contentView)
        
        feedbackCard.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
        }
        
        feedbackContentView = contentView
    }
    
    func displayWalletComponent(bankError: WithdrawalOptionsBankError?) {
        removeViewController(walletViewController)
        let walletViewController = WalletBalanceFactory.make(bankError: bankError)
        addViewController(walletViewController)
        self.walletViewController = walletViewController
    }
    
    func displayCarouselComponent(hasBiometry: Bool, seller: Seller, userStatus: UserStatus?) {
        removeViewController(carouselViewController)
        let carouselViewController = TransactionCarouselFactory.make(hasBiometry: hasBiometry, userStatus: userStatus, seller: seller)
        addViewController(carouselViewController)
        self.carouselViewController = carouselViewController
    }
    
    func displayMaterialHeader(userStatus: UserStatus) {
        removeViewController(materialSolicitationController)
        let materialSolicitationController = MaterialSolicitationHeaderFactory.make(status: userStatus)
        materialSolicitationController.delegate = self
        addViewController(materialSolicitationController)
        self.materialSolicitationController = materialSolicitationController
    }
    
    func displayTransactionComponent(seller: Seller) {
        guard let transactionsViewController = transactionsViewController else {
            let controller = TransactionsContainerFactory.make(seller: seller, type: .compact)
            addViewController(controller)
            self.transactionsViewController = controller
            return
        }
        stackView.addArrangedSubview(transactionsViewController.view)
    }
    
    func displayNewTransactionComponent(seller: Seller) {
        guard let newTransactionsViewController = newTransactionsViewController else {
            let controller = TransactionsFactory.make(seller: seller, type: .compact)
            addViewController(controller)
            self.newTransactionsViewController = controller
            return
        }
        stackView.addArrangedSubview(newTransactionsViewController.view)
    }
    
    func displayAdvertising(with modalInfo: AlertModalInfo) {
        let alertPopup = AlertModalFactory.make(alertModalInfo: modalInfo)
        
        alertPopup.touchButtonAction = { [weak self] alert in
            guard let self = self else { return }
            alert.dismiss(animated: true) {
                self.interactor.trackPixRegister()
                self.interactor.pixAction()
            }
        }
        
        alertPopup.touchSecondaryButtonAction = { [weak self] alert in
            guard let self = self else { return }
            self.interactor.trackPixNotNow()
            alert.dismiss(animated: true)
        }
        
        let popup = PopupViewController()
        popup.hasCloseButton = false
        popup.contentController = alertPopup
        present(popup, animated: true)
    }
}

// MARK: - Private Methods

private extension HomeViewController {
    func setupNavigationBar() {
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceDefault())
        navigationItem.leftBarButtonItem = leftNavigationItem
    }
    
    func addViewController(_ viewController: UIViewController) {
        addChild(viewController)
        stackView.addArrangedSubview(viewController.view)
        viewController.didMove(toParent: self)
    }
    
    func removeViewController(_ viewController: UIViewController?) {
        guard let viewController = viewController else { return }
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    @objc
    func accountErrorAction() {
        interactor.snackBarAction()
    }
}

// MARK: - Actions
@objc
private extension HomeViewController {
    func handleRefreshControl() {
        interactor.loadHome()
        transactionsViewController?.reload()
        newTransactionsViewController?.reload()
    }
}

// MARK: - MaterialSolicitationHeaderDelegate

extension HomeViewController: MaterialSolicitationHeaderDelegate {
    func updateTable() {
    }
}
