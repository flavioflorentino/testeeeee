import Core
import Foundation
import FeatureFlag

protocol HomeServicing {
    var bankAccountWarningsEnabled: Bool { get }
    var welcomePresented: Bool { get }
    var pixPresentingEnabled: Bool { get }
    var mustPresentHub: Bool { get }
    var allowApplyMaterial: Bool { get }
    var receiveAccountEnabled: Bool { get }
    func checkStatus(completion: @escaping(Result<UserStatus?, ApiError>) -> Void)
    func checkBankAccount(completion: @escaping(Result<BankAccountValidation?, ApiError>) -> Void)
}

final class HomeService {
    typealias Dependencies = HasMainQueue & HasFeatureManager & HasKVStore
    private let dependencies: Dependencies
    
    lazy var bankAccountWarningsEnabled = dependencies.featureManager.isActive(.releaseBankAccountWarning)
    lazy var allowApplyMaterial = dependencies.featureManager.isActive(.allowApplyMaterial)
    lazy var pixPresentingEnabled = dependencies.featureManager.isActive(.releasePIXPresenting)
    lazy var welcomePresented = dependencies.kvStore.boolFor(KVKey.isPixKeyManagementWelcomeVisualized)
    lazy var mustPresentHub = dependencies.featureManager.isActive(.releaseHubPixPresenting)
    lazy var receiveAccountEnabled = dependencies.featureManager.isActive(.isReceivementAccountAvailable)

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - HomeServicing
extension HomeService: HomeServicing {
    func checkStatus(completion: @escaping (Result<UserStatus?, ApiError>) -> Void) {
        let endPoint = UserStatusEndpoint.checkStatus
        BizApi<UserStatus>(endpoint: endPoint).bizExecute {[weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model.data)
                completion(mappedResult)
            }
        }
    }
    
    func checkBankAccount(completion: @escaping(Result<BankAccountValidation?, ApiError>) -> Void) {
        let endPoint = HomeEndpoint.checkBankAccount
        BizApi<BankAccountValidation>(endpoint: endPoint).bizExecute {[weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model.data)
                completion(mappedResult)
            }
        }
    }
}
