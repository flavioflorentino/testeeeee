import Foundation
import UI

protocol MaterialFlowControlDisplay: AnyObject {
    func startLoading()
    func stopLoading()
}

private extension MaterialFlowControlViewController.Layout {
    enum LoadingView {
        static let alpha: CGFloat = 0.8
    }
}

final class MaterialFlowControlViewController: ViewController<MaterialFlowControlViewModelInputs, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }
    
    lazy var loadingView: LoadingView = {
        let loadingView = LoadingView()
        loadingView.alpha = Layout.LoadingView.alpha
        return loadingView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadNextStep()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: extension for View Model Outputs

extension MaterialFlowControlViewController: MaterialFlowControlDisplay {
    func startLoading() {
        startLoadingView()
    }
    
    func stopLoading() {
        stopLoadingView()
    }
}
