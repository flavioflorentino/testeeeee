import AnalyticsModule
import LegacyPJ
import UIKit

enum MaterialFlowControlAction: Equatable {
    case solicitation
    case activation
    case deliveryForecast
    case thirdSolicitation(BizErrorViewLayout)
    case error(BizErrorViewLayout)
    
    static func == (lhs: MaterialFlowControlAction, rhs: MaterialFlowControlAction) -> Bool {
        switch (lhs, rhs) {
        case (.solicitation, .solicitation),
             (.activation, .activation),
             (.deliveryForecast, .deliveryForecast):
            return true
        case let (.thirdSolicitation(lhsError), .thirdSolicitation(rhsError)),
             let (.error(lhsError), .error(rhsError)):
            return lhsError == rhsError
        default:
            return false
        }
    }
}

protocol MaterialFlowControlCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: MaterialFlowControlAction)
}

final class MaterialFlowControlCoordinator: MaterialFlowControlCoordinating {
    typealias Dependencies = HasAuthManager & HasAnalytics
    
    weak var viewController: UIViewController?
    
    private var stepError: MaterialStepRequestError?
    private var thirdSolicitationError: ThirdSolicitationError?
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: MaterialFlowControlAction) {
        let controller: UIViewController
        
        switch action {
        case .solicitation:
            controller = OptinApplyMaterialFactory.make()
        case .activation:
            controller = DeliveryDetailFactory.make(state: .boardActivation)
            
        case .deliveryForecast:
            controller = DeliveryDetailFactory.make(state: .deliveryForecast)
            
        case .thirdSolicitation(let viewParams):
            thirdSolicitationError = ThirdSolicitationError(viewController: viewController, dependencies: dependencies)
            controller = BizErrorViewFactory.make(errorView: viewParams, errorCoordinatorDelegate: thirdSolicitationError)
            
        case .error(let viewParams):
            stepError = MaterialStepRequestError(viewController: viewController)
            controller = BizErrorViewFactory.make(errorView: viewParams, errorCoordinatorDelegate: stepError)
        }
        
        viewController?.navigationController?.pushViewController(controller, animated: false)
    }
}
