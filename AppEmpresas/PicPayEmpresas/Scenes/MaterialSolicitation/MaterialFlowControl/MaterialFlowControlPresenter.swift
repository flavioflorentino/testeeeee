import Foundation

protocol MaterialFlowControlPresenting: AnyObject {
    var viewController: MaterialFlowControlDisplay? { get set }
    func displayThirdSolicitation()
    func displayStepError()
    func didNextStep(action: MaterialFlowControlAction)
    func startLoading()
    func stopLoading()
}

final class MaterialFlowControlPresenter {
    private typealias Localizable = Strings.MaterialSolicitation
    
    private let coordinator: MaterialFlowControlCoordinating
    weak var viewController: MaterialFlowControlDisplay?

    init(coordinator: MaterialFlowControlCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - MaterialFlowControlPresenting
extension MaterialFlowControlPresenter: MaterialFlowControlPresenting {
    func displayThirdSolicitation() {
        let viewConfig = BizErrorViewLayout(
            title: Localizable.thirdSolicitationTitle,
            message: Localizable.thirdSolicitationDescription,
            image: Assets.Emoji.iconBad.image,
            buttonTitle: Localizable.thirdSolicitationButton
        )
        coordinator.perform(action: .thirdSolicitation(viewConfig))
    }
    
    func displayStepError() {
        let viewConfig = BizErrorViewLayout(
            title: Localizable.stepRequestErrorTitle,
            message: Localizable.stepRequestErrorDescription,
            image: Assets.Emoji.iconBad.image
        )
        coordinator.perform(action: .error(viewConfig))
    }
    
    func didNextStep(action: MaterialFlowControlAction) {
        coordinator.perform(action: action)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
}
