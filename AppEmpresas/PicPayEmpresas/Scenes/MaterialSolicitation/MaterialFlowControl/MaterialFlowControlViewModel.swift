import AnalyticsModule
import Foundation

protocol MaterialFlowControlViewModelInputs: AnyObject {
    func loadNextStep()
}

final class MaterialFlowControlViewModel {
    private let service: MaterialFlowControlServicing
    private let presenter: MaterialFlowControlPresenting
    private let dependencies: HasAnalytics = DependencyContainer()

    init(service: MaterialFlowControlServicing, presenter: MaterialFlowControlPresenting) {
        self.service = service
        self.presenter = presenter
    }
    
    private func goToNextStep(_ nextStep: MaterialFlowControlResponse?) {
        guard let boardStep = nextStep?.boardStep else {
            presenter.displayStepError()
            return
        }
        let flowStep = MaterialFlowStep(rawValue: boardStep)
        
        switch flowStep {
        case .solicitation, .brCode:
            presenter.didNextStep(action: .solicitation)
        case .activation:
            presenter.didNextStep(action: .activation)
        case .deliveryForecast:
            presenter.didNextStep(action: .deliveryForecast)
        default:
            presenter.displayThirdSolicitation()
        }
    }
}

// MARK: - MaterialFlowControlViewModelInputs
extension MaterialFlowControlViewModel: MaterialFlowControlViewModelInputs {
    func loadNextStep() {
        dependencies.analytics.log(MaterialRequestAnalytics.settingsTabEvent)
        presenter.startLoading()
        service.loadNextStep {[weak self] result in
            self?.presenter.stopLoading()
            
            switch result {
            case .success(let nextStep):
                self?.goToNextStep(nextStep)
            case .failure:
                self?.presenter.displayStepError()
            }
        }
    }
}
