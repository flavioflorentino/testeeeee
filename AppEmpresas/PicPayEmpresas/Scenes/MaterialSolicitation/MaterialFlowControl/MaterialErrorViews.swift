import AnalyticsModule
import Foundation
import LegacyPJ

final class MaterialStepRequestError: BizErrorViewCoordinatorDelegate {
    private let viewController: UIViewController?
    private let dependencies: HasAnalytics = DependencyContainer()
    
    init(viewController: UIViewController?) {
        self.viewController = viewController
        dependencies.analytics.log(MaterialRequestAnalytics.stepErrorScreen)
    }
    
    func buttonAction() {
        viewController?.dismiss(animated: true)
    }
    
    func closeAction() {
        viewController?.dismiss(animated: true)
    }
}

final class ThirdSolicitationError: BizErrorViewCoordinatorDelegate {
    typealias Dependencies = HasAuthManager & HasAnalytics
    
    private let viewController: UIViewController?
    private let dependencies: Dependencies
    
    lazy var customerSupport: BizCustomerSupportManagerDelegate = BIZCustomerSupportManager(accessToken: dependencies.authManager.userAuth?.auth.accessToken)
    
    init(viewController: UIViewController?, dependencies: Dependencies) {
        self.viewController = viewController
        self.dependencies = dependencies
        dependencies.analytics.log(MaterialRequestAnalytics.unavailableScreen)
    }
    
    func buttonAction() {
        guard let viewController = viewController else {
            return
        }
        customerSupport.presentTicketList(from: viewController, with: nil)
    }
    
    func closeAction() {
        viewController?.dismiss(animated: true)
    }
}

final class MaterialSolicitationError: BizErrorViewCoordinatorDelegate {
    private let dependencies: HasAnalytics = DependencyContainer()
    private let viewController: UIViewController?
    private let solicitationErrorAnalytics = MaterialSolicitationErrorViewAnalytics()
    
    init(viewController: UIViewController?) {
        self.viewController = viewController
        dependencies.analytics.log(MaterialRequestAnalytics.screenViewed(analyticsStruct: solicitationErrorAnalytics))
    }
    
    func buttonAction() {
        viewController?.dismiss(animated: true)
    }
    
    func closeAction() {
        viewController?.dismiss(animated: true)
    }
}
