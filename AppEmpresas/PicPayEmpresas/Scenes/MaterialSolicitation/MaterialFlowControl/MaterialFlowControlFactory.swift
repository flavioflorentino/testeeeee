import Foundation

enum MaterialFlowControlFactory {
    static func make() -> MaterialFlowControlViewController {
        let container = DependencyContainer()
        let service: MaterialFlowControlServicing = MaterialFlowControlService(dependencies: container)
        let coordinator: MaterialFlowControlCoordinating = MaterialFlowControlCoordinator(dependencies: container)
        let presenter: MaterialFlowControlPresenting = MaterialFlowControlPresenter(coordinator: coordinator)
        let viewModel = MaterialFlowControlViewModel(service: service, presenter: presenter)
        let viewController = MaterialFlowControlViewController(viewModel: viewModel)
        
        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
}
