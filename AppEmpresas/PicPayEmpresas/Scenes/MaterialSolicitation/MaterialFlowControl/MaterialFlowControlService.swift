import Core

protocol MaterialFlowControlServicing {
    func loadNextStep(completion: @escaping(Result<MaterialFlowControlResponse?, ApiError>) -> Void)
}

final class MaterialFlowControlService {
    typealias Dependencies = HasMainQueue
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - MaterialFlowControlServicing
extension MaterialFlowControlService: MaterialFlowControlServicing {
    func loadNextStep(completion: @escaping(Result<MaterialFlowControlResponse?, ApiError>) -> Void) {
        let endpoint = MaterialSolicitationEndPoint.materialSolicitationCheckStatus
        let jsonDecoder = JSONDecoder(.convertFromSnakeCase)
    
        BizApi<MaterialFlowControlResponse>(endpoint: endpoint).bizExecute(jsonDecoder: jsonDecoder) { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model.data)
                
                completion(mappedResult)
            }
        }
    }
}

struct MaterialFlowControlResponse: Decodable {
    let boardStep: String
}
