import Foundation

enum MaterialFlowStep: String {
    case solicitation = "solicitacao"
    case activation = "ativacao"
    case deliveryForecast = "previsao"
    case thirdSolicitation = "bloqueia"
    case brCode
}
