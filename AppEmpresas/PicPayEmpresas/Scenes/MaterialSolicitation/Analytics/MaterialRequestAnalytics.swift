import AnalyticsModule
import Foundation
import LegacyPJ

protocol MaterialRequestAnalyticsProtocol {
    var screenName: String? { get }
    var buttonName: String? { get }
    var dialogName: String? { get }
    var dialogText: String? { get set }
    var optionSelected: String? { get }
    var buttonType: MaterialSolicitationButtonTypes? { get set }
    var codeNumber: String? { get set }
}

extension MaterialRequestAnalyticsProtocol {
    var screenName: String? { nil }
    var buttonName: String? { nil }
    var dialogName: String? { nil }
    var dialogText: String? {
        get { nil }
        set { dialogText = newValue }
    }
    var optionSelected: String? { nil }
    var buttonType: MaterialSolicitationButtonTypes? {
        get { nil }
        set { buttonType = newValue }
    }
    var codeNumber: String? {
        get { nil }
        set { codeNumber = newValue }
    }
    
    func validProperties() -> [String: Any] {
        let properties = [
            "screen_name": screenName,
            "button_name": buttonName,
            "dialog_name": dialogName,
            "dialog_text": dialogText,
            "option_selected": optionSelected,
            "code_number": codeNumber
        ]
        
        var validProperties: [String: Any] = [:]
        for (key, propertie) in properties {
            if let propertie = propertie {
                validProperties[key] = propertie
            }
        }
        return validProperties
    }
}

enum MaterialSolicitationButtonTypes {
    case confirm
    case decline
    case retry
}

enum MaterialRequestAnalytics: AnalyticsKeyProtocol {
    case screenViewed(analyticsStruct: MaterialRequestAnalyticsProtocol)
    case buttonClicked(analyticsStruct: MaterialRequestAnalyticsProtocol)
    case dialogOptionSelected(analyticsStruct: MaterialRequestAnalyticsProtocol)
    case dialogViewed(analyticsStruct: MaterialRequestAnalyticsProtocol)
    case codeScanned(analyticsStruct: MaterialRequestAnalyticsProtocol)
    case oldHeaderEvent
    case newHeaderEvent
    case settingsTabEvent
    case unavailableScreen
    case stepErrorScreen
    case printMaterialScreen
    
    var name: String {
        switch self {
        case .screenViewed:
            return "Screen Viewed"
        case .buttonClicked:
            return "Button Clicked"
        case .dialogOptionSelected:
            return "Dialog Option Selected"
        case .dialogViewed:
            return "Dialog Viewed"
        case .codeScanned:
            return "Code Scanned"
        case .newHeaderEvent:
            return "Material Request - New header"
        case .printMaterialScreen:
            return "Material Request - Print Material Screen"
        case .oldHeaderEvent:
            return "Material Request - Old header"
        case .settingsTabEvent:
            return "Material Request - Settings Tab"
        case .unavailableScreen:
            return "Material Request - Unavailable"
        case .stepErrorScreen:
            return "Material Request - Step Error"
        }
    }
    
    private var properties: [String: Any] {        
        switch self {
        case .screenViewed(let analyticsStruct):
            return analyticsStruct.validProperties()
        case .buttonClicked(let analyticsStruct):
            return analyticsStruct.validProperties()
        case .dialogOptionSelected(let analyticsStruct):
            return analyticsStruct.validProperties()
        case .dialogViewed(let analyticsStruct):
            return analyticsStruct.validProperties()
        case .codeScanned(let analyticsStruct):
            return analyticsStruct.validProperties()
        default:
            return [:]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        var defaultProperties: [String: Any] = [:]
        let contextKey = "business_context"
        let contextValue = "RECEBER_MATERIAL"
        
        defaultProperties = [contextKey: contextValue ]
        defaultProperties.merge(properties, uniquingKeysWith: { _, current in current })
        
        return AnalyticsEvent(name, properties: defaultProperties, providers: [.eventTracker])
    }
}
