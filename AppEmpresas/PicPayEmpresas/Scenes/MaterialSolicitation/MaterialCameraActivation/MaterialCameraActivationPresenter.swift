import Foundation

protocol MaterialCameraActivationPresenting: AnyObject {
    var viewController: MaterialCameraActivationDisplay? { get set }
    func didNextStep(action: MaterialCameraActivationAction)
    func handleError(error: String)
    func createCameraPermissionPopup()
    func startLoad()
}

final class MaterialCameraActivationPresenter: MaterialCameraActivationPresenting {
    private let coordinator: MaterialCameraActivationCoordinating
    weak var viewController: MaterialCameraActivationDisplay?

    init(coordinator: MaterialCameraActivationCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - MaterialCameraActivationPresenting
extension MaterialCameraActivationPresenter {    
    func didNextStep(action: MaterialCameraActivationAction) {
        coordinator.perform(action: action)
    }
    
    func handleError(error: String) {
        viewController?.showError(error: error)
    }
    
    func createCameraPermissionPopup() {
        viewController?.createCameraPermissionPopUp()
    }
    
    func startLoad() {
        viewController?.startApolloLoader(loadingText: "Aguarde")
    }
}
