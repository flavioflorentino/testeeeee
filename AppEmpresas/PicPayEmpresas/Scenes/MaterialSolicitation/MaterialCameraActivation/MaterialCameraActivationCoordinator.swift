import UI
import UIKit

enum MaterialCameraActivationAction: Equatable {
    case activationSuccess
    case showPopup(controller: UI.PopupViewController)
    case showPopupError(controller: UI.PopupViewController)
    case goToSettings
    case dismiss
    case close
}

protocol MaterialCameraActivationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: MaterialCameraActivationAction)
}

final class MaterialCameraActivationCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - MaterialCameraActivationCoordinating
extension MaterialCameraActivationCoordinator: MaterialCameraActivationCoordinating {
    func perform(action: MaterialCameraActivationAction) {
        switch action {
        case .activationSuccess:
            let controller = SolicitationSuccessFactory.make(state: .activation)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .showPopupError(let controller), .showPopup(let controller):
            viewController?.showPopup(controller)
        case .goToSettings:
        guard
            let url = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(url) else {
            return
        }
        UIApplication.shared.open(url, completionHandler: nil)
        case .dismiss:
            viewController?.dismiss(animated: true, completion: nil)
        case .close:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}
