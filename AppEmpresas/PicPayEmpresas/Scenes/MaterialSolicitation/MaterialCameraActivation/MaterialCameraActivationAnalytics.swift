struct MaterialActivationCameraAnalytics: MaterialRequestAnalyticsProtocol {
    var codeNumber: String?
}

struct MaterailCameraActivationPopupError: MaterialRequestAnalyticsProtocol {
    var screenName: String? {
        "ATIVACAO_DA_PLAQUINHA_ERRO_SCAN"
    }
    
    var dialogName: String? {
        "ATIVAR_PLAQUINHA"
    }
    
    var optionSelected: String? {
        "ESCANEAR_NOVAMENTE"
    }
}
