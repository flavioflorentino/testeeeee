import Core
import Foundation

protocol MaterialCameraActivationServicing {
    func activateCode(code: String, completion: @escaping(Result<MaterialActivation?, ApiError>) -> Void)
}

final class MaterialCameraActivationService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - MaterialCameraActivationServicing
extension MaterialCameraActivationService: MaterialCameraActivationServicing {
    func activateCode(code: String, completion: @escaping(Result<MaterialActivation?, ApiError>) -> Void) {
        let endPoint = MaterialSolicitationEndPoint.materialActivation(code: code)
        BizApi<MaterialActivation>(endpoint: endPoint).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model.data)
                completion(mappedResult)
            }
        }
    }
}

struct MaterialActivation: Decodable {
    let success: Bool
    let error: String?
    let data: String?
}
