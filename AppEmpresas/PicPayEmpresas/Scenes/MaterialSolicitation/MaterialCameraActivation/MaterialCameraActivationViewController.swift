import AVFoundation
import UI
import UIKit

protocol MaterialCameraActivationDisplay: ApolloLoadable {
    func showError(error: String)
    func createCameraPermissionPopUp()
}

private extension MaterialCameraActivationViewController.Layout {
    enum Label {
        static let topOffset: CGFloat = 150
    }
}

final class MaterialCameraActivationViewController: ViewController<MaterialCameraActivationViewModelInputs, UIView> {
    fileprivate typealias Localizable = Strings.MaterialSolicitation
    fileprivate enum Layout {}
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    var captureSession = AVCaptureSession()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.activationDescription
        label.numberOfLines = 3
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .white())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var errorLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, .critical400())
        label.isHidden = true
        return label
    }()
    
    private lazy var frameImage = UIImageView(image: Assets.bgFrameQrcode.image)
    private lazy var contentView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.trackViewDidLoad()
        checkPermissionCameraAccess()
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceTransparent())
        configurePreviewCamera()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(contentView)
        view.addSubview(frameImage)
        view.addSubview(infoLabel)
        view.addSubview(errorLabel)
    }
    
    override func setupConstraints() {
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        frameImage.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        infoLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Layout.Label.topOffset)
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
        }
        
        errorLabel.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base10)
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - BUTTON ACTION -
@objc
extension MaterialCameraActivationViewController {
    func closeCamera() {
        viewModel.didNextStep(action: .close)
    }
}

// MARK: - CREATE POPUP ACTIONS -
private extension MaterialCameraActivationViewController {
    func createDeniedButtons() -> [PopupAction] {
        let settingsButton = PopupAction(title: Localizable.cameraPermissionButton, style: .fill) { [weak self] in
            self?.viewModel.didNextStep(action: .goToSettings)
        }
        
        let laterButton = PopupAction(title: Localizable.cameraPermissionLaterButton, style: .link) { [weak self] in
            self?.viewModel.didNextStep(action: .close)
        }
        
        return [settingsButton, laterButton]
    }
}

// MARK: - PERMISSION CAMERA -
private extension MaterialCameraActivationViewController {
    func checkPermissionCameraAccess() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            configureInputCamera()
            configureOutputCamera()
        case .notDetermined:
            requestAccess()
        case .restricted, .denied:
            viewModel.createPopupCameraPermission()
        @unknown default:
            viewModel.createPopupCameraPermission()
        }
    }
    func requestAccess() {
        AVCaptureDevice.requestAccess(for: .video) {[weak self] response in
            if response {
                self?.configureInputCamera()
                self?.configureOutputCamera()
            }
        }
    }
}

// MARK: - CONFIGURE CAMERA -
private extension MaterialCameraActivationViewController {
    func configureInputCamera() {
        guard
            let captureDevice = AVCaptureDevice.default(for: .video),
            let deviceInput = try? AVCaptureDeviceInput(device: captureDevice) else {
            return
        }
        
        if captureSession.canAddInput(deviceInput) {
            captureSession.addInput(deviceInput)
        }
    }
    
    func configureOutputCamera() {
        let dataOutput = AVCaptureMetadataOutput()
        
        if captureSession.canAddOutput(dataOutput) {
            captureSession.addOutput(dataOutput)
        }
        
        dataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        dataOutput.metadataObjectTypes = [.qr, .dataMatrix]
    }
    
    func configurePreviewCamera() {
        let previewPlayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewPlayer.videoGravity = .resizeAspectFill
        contentView.layer.insertSublayer(previewPlayer, at: 0)
        previewPlayer.frame = contentView.layer.bounds
        captureSession.startRunning()
    }
}

// MARK: - MaterialCameraActivationDisplay -
extension MaterialCameraActivationViewController: MaterialCameraActivationDisplay {
    func showError(error: String) {
        errorLabel.text = error
        errorLabel.isHidden = false
        frameImage.image = Assets.bgFrameQrcodeError.image
        stopApolloLoader {
            self.captureSession.startRunning()
        }
    }
    
    func createCameraPermissionPopUp() {
        let cameraPermissionPopUp = UI.PopupViewController(
            title: Localizable.cameraPermissionTitle,
            description: Localizable.cameraPermissionDescription,
            preferredType: .text
        )
        cameraPermissionPopUp.hideCloseButton = true
        createDeniedButtons().forEach { cameraPermissionPopUp.addAction($0) }
        
        viewModel.didNextStep(action: .showPopup(controller: cameraPermissionPopUp))
    }
}

extension MaterialCameraActivationViewController: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        guard let metaData = metadataObjects.first as? AVMetadataMachineReadableCodeObject,
            let qrValue = metaData.stringValue else {
            return
        }
        
        errorLabel.isHidden = true
        viewModel.validateCode(code: qrValue)
        captureSession.stopRunning()
    }
}
