import AnalyticsModule
import Foundation

protocol MaterialCameraActivationViewModelInputs: AnyObject {
    func trackViewDidLoad()
    func validateCode(code: String)
    func createPopupCameraPermission()
    func didNextStep(action: MaterialCameraActivationAction)
}

final class MaterialCameraActivationViewModel {
    fileprivate typealias Localizable = Strings.MaterialSolicitation
    private let service: MaterialCameraActivationServicing
    private let presenter: MaterialCameraActivationPresenting
    private let dependencies: HasAnalytics = DependencyContainer()
    private var cameraActivationAnalytics = MaterialActivationCameraAnalytics()
    private var cameraPopupErrorAnalytics = MaterailCameraActivationPopupError()

    init(service: MaterialCameraActivationServicing, presenter: MaterialCameraActivationPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

// MARK: - MaterialCameraActivationViewModelInputs
extension MaterialCameraActivationViewModel: MaterialCameraActivationViewModelInputs {
    func validateCode(code: String) {
        presenter.startLoad()
        service.activateCode(code: code) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let material):
                guard let material = material, !material.success else {
                    self.presenter.didNextStep(action: .activationSuccess)
                    self.cameraActivationAnalytics.codeNumber = code
                    self.dependencies.analytics.log(
                        MaterialRequestAnalytics.codeScanned(
                            analyticsStruct: self.cameraActivationAnalytics
                        )
                    )
                    return
                }
                self.presenter.handleError(error: material.error ?? Localizable.activationError)
            case .failure:
                self.presenter.handleError(error: Localizable.activationError)
            }
        }
    }
    
    func trackViewDidLoad() {
        dependencies.analytics.log(MaterialRequestAnalytics.dialogViewed(analyticsStruct: cameraPopupErrorAnalytics))
    }
    
    func createPopupCameraPermission() {
        presenter.createCameraPermissionPopup()
    }
    
    func didNextStep(action: MaterialCameraActivationAction) {
        if case .showPopupError = action {
            dependencies.analytics.log(MaterialRequestAnalytics.dialogOptionSelected(analyticsStruct: cameraPopupErrorAnalytics))
        }
        
        presenter.didNextStep(action: action)
    }
}
