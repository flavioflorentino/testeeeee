import Foundation

enum MaterialCameraActivationFactory {
    static func make() -> MaterialCameraActivationViewController {
        let container = DependencyContainer()
        let service: MaterialCameraActivationServicing = MaterialCameraActivationService(dependencies: container)
        let coordinator: MaterialCameraActivationCoordinating = MaterialCameraActivationCoordinator()
        let presenter: MaterialCameraActivationPresenting = MaterialCameraActivationPresenter(coordinator: coordinator)
        let viewModel = MaterialCameraActivationViewModel(service: service, presenter: presenter)
        let viewController = MaterialCameraActivationViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
