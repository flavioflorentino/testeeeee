struct MaterialDeliveryForeCastAnalytics: MaterialRequestAnalyticsProtocol {
    var screenName: String? {
        "SOLICITACAO_DE_MATERIAL_DATA_PREVISTA"
    }
}

struct MaterialActivationAnalytics: MaterialRequestAnalyticsProtocol {
    var screenName: String? {
        "ATIVAR_PLAQUINHA"
    }
    
    var buttonName: String? {
        "ATIVACAO_ENTREGA_DE_MATERIAIS"
    }
}
