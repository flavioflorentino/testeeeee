import Foundation

enum DeliveryDetailFactory {
    static func make(state: DeliveryDetailState) -> DeliveryDetailViewController {
        let coordinator: DeliveryDetailCoordinating = DeliveryDetailCoordinator(dependencies: DependencyContainer())
        let presenter: DeliveryDetailPresenting = DeliveryDetailPresenter(coordinator: coordinator)
        let viewModel = DeliveryDetailViewModel(presenter: presenter, state: state)
        let viewController = DeliveryDetailViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
