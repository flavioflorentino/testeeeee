import UI

protocol DeliveryDetailDisplay: AnyObject {
    func setView(screenTitle: String, descriptionText: NSAttributedString, buttonTitle: String)
    func setView(
        screenTitle: String,
        buttonTitle: String,
        linkText: NSAttributedString,
        linkAttributes: [NSAttributedString.Key: Any]
    )
}

protocol CloseButtonDelegate: AnyObject {
    func didTapCloseButton()
}

final class DeliveryDetailViewController: ViewController<DeliveryDetailViewModelInputs, UIView> {
    private typealias Localizable = Strings.MaterialSolicitation
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    private lazy var headerView: DeliveryDetailHeaderView = {
        let view = DeliveryDetailHeaderView()
        view.delegate = self
        return view
    }()
    
    private lazy var descriptionTextView: UITextView = {
        let text = UITextView()
        text.font = Typography.bodyPrimary().font()
        text.textColor = Colors.grayscale700.color
        text.isEditable = false
        text.isSelectable = true
        text.isScrollEnabled = false
        text.backgroundColor = .clear
        text.textContainerInset = .zero
        text.textContainer.lineFragmentPadding = 0
        return text
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapConfirmButton), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func buildViewHierarchy() {
        view.addSubview(headerView)
        view.addSubview(descriptionTextView)
        view.addSubview(confirmButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func setupConstraints() {
        headerView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
        
        descriptionTextView.snp.makeConstraints {
            $0.top.equalTo(headerView.snp.bottom).offset(Spacing.base04)
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
        }
        
        confirmButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }
    }
    
    @objc
    func didTapConfirmButton() {
        viewModel.didNextStep()
    }
}

extension DeliveryDetailViewController: DeliveryDetailDisplay {
    func setView(screenTitle: String, descriptionText: NSAttributedString, buttonTitle: String) {
        headerView.title = screenTitle
        descriptionTextView.attributedText = descriptionText
        descriptionTextView.textAlignment = .center
        confirmButton.setTitle(buttonTitle, for: .normal)
    }
    
    func setView(
        screenTitle: String,
        buttonTitle: String,
        linkText: NSAttributedString,
        linkAttributes: [NSAttributedString.Key: Any]
    ) {
        headerView.title = screenTitle
        descriptionTextView.attributedText = linkText
        descriptionTextView.linkTextAttributes = linkAttributes
        descriptionTextView.textAlignment = .center
        descriptionTextView.delegate = self
        confirmButton.setTitle(buttonTitle, for: .normal)
    }
}

extension DeliveryDetailViewController: CloseButtonDelegate {
    func didTapCloseButton() {
        viewModel.dismissScreen()
    }
}

extension DeliveryDetailViewController: UITextViewDelegate {
    func textView(
        _ textView: UITextView,
        shouldInteractWith URL: URL,
        in characterRange: NSRange,
        interaction: UITextItemInteraction
    ) -> Bool {
        viewModel.showCustomerSupport()
        return false
    }
}
