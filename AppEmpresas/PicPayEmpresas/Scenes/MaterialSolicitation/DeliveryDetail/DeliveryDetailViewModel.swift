import AnalyticsModule
import Foundation

protocol DeliveryDetailViewModelInputs: AnyObject {
    func setupView()
    func dismissScreen()
    func didNextStep()
    func showCustomerSupport()
}

final class DeliveryDetailViewModel {
    private let presenter: DeliveryDetailPresenting
    private let dependencies: HasAnalytics = DependencyContainer()

    private let state: DeliveryDetailState
    private let deliveryForeCastAnalytics = MaterialDeliveryForeCastAnalytics()
    private let activationAnalytics = MaterialActivationAnalytics()
    
    init(presenter: DeliveryDetailPresenting, state: DeliveryDetailState) {
        self.presenter = presenter
        self.state = state
    }
}

// MARK: - DeliveryDetailViewModelInputs
extension DeliveryDetailViewModel: DeliveryDetailViewModelInputs {
    func setupView() {
        if state == .deliveryForecast {
            dependencies.analytics.log(MaterialRequestAnalytics.screenViewed(analyticsStruct: deliveryForeCastAnalytics))
            presenter.setupDescriptionDeliveryForecast()
        } else {
            dependencies.analytics.log(MaterialRequestAnalytics.screenViewed(analyticsStruct: activationAnalytics))
            presenter.setupDescriptionBoardActivation()
        }
    }
    
    func dismissScreen() {
        presenter.didNextStep(action: .rootScreen)
    }
    
    func didNextStep() {
        guard state == .deliveryForecast else {
            dependencies.analytics.log(MaterialRequestAnalytics.buttonClicked(analyticsStruct: activationAnalytics))
            presenter.didNextStep(action: .qrCodeScreen)
            return
        }
        presenter.didNextStep(action: .rootScreen)
    }
    
    func showCustomerSupport() {
        presenter.didNextStep(action: .customerSupport)
    }
}
