import Foundation
import UI

enum DeliveryDetailState {
    case deliveryForecast
    case boardActivation
}

protocol DeliveryDetailPresenting: AnyObject {
    var viewController: DeliveryDetailDisplay? { get set }
    func didNextStep(action: DeliveryDetailAction)
    func setupDescriptionDeliveryForecast()
    func setupDescriptionBoardActivation()
}

final class DeliveryDetailPresenter {
    private typealias Localizable = Strings.MaterialSolicitation
    
    private let coordinator: DeliveryDetailCoordinating
    weak var viewController: DeliveryDetailDisplay?

    init(coordinator: DeliveryDetailCoordinating) {
        self.coordinator = coordinator
    }
}

private extension DeliveryDetailPresenter {
    var linkAttributes: [NSAttributedString.Key: Any] {
        [
            .font: Typography.bodyPrimary(.highlight).font(),
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .foregroundColor: Colors.branding400.color
        ]
    }
    
    var descriptionAttributes: [NSAttributedString.Key: Any] {
        let style = NSMutableParagraphStyle()
        style.lineHeightMultiple = 1.26
        style.alignment = .center
        return [
            .font: Typography.bodyPrimary().font(),
            .foregroundColor: Colors.grayscale700.color,
            .paragraphStyle: style
        ]
    }
    
    var deliveryForecastDescription: NSAttributedString {
        let defaultStyle = attributedStyle(with: Typography.bodyPrimary(.default).font())
        let highlightStyle = attributedStyle(with: Typography.bodyPrimary(.highlight).font())

        let text = NSMutableAttributedString()
        text.append(NSAttributedString(
            string: Localizable.deliveryForecastDescription,
            attributes: defaultStyle))
        text.append(NSAttributedString(
            string: Localizable.deliveryForecastDescriptionBold,
            attributes: highlightStyle))

        return text
    }
    
    var descriptionLink: NSMutableAttributedString {
        guard let url = URL(string: Localizable.boardActivationPlaceholderLink) else {
            return NSMutableAttributedString()
        }
        let attributedText = NSMutableAttributedString(
            string: Localizable.boardActivationDescription,
            attributes: descriptionAttributes
        )
        addAttributes(to: attributedText, url: url, highlight: Localizable.boardActivationDescriptionLink)
        
        return attributedText
    }
    
    func attributedStyle(with font: UIFont) -> [NSAttributedString.Key: Any] {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.26
        
        let style: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.paragraphStyle: paragraphStyle
        ]
        return style
    }
    
    func addAttributes(
        to attributedText: NSMutableAttributedString,
        url: URL,
        highlight: String
    ) {
        let attributes: [NSAttributedString.Key: Any] = [
            .link: url,
            .font: Typography.bodyPrimary(.highlight).font(),
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .foregroundColor: Colors.branding400.color
        ]
        let range = NSString(string: Localizable.boardActivationDescription).range(of: highlight)
        attributedText.addAttributes(attributes, range: range)
    }
}

// MARK: - DeliveryDetailPresenting
extension DeliveryDetailPresenter: DeliveryDetailPresenting {
    func didNextStep(action: DeliveryDetailAction) {
        coordinator.perform(action: action)
    }
    
    func setupDescriptionDeliveryForecast() {
        viewController?.setView(
            screenTitle: Localizable.deliveryForecastTitle,
            descriptionText: deliveryForecastDescription,
            buttonTitle: Localizable.deliveryForecastButton
        )
    }
    
    func setupDescriptionBoardActivation() {
        viewController?.setView(
            screenTitle: Localizable.boardActivationTitle,
            buttonTitle: Localizable.boardActivationButton,
            linkText: descriptionLink,
            linkAttributes: linkAttributes
        )
    }
}
