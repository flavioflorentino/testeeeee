import LegacyPJ
import UIKit

enum DeliveryDetailAction {
    case rootScreen
    case qrCodeScreen
    case customerSupport
}

protocol DeliveryDetailCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: DeliveryDetailAction)
}

final class DeliveryDetailCoordinator: DeliveryDetailCoordinating {
    typealias Dependencies = HasAuthManager
    
    weak var viewController: UIViewController?
    
    private let dependencies: Dependencies
    
    lazy var customerSupport: BizCustomerSupportManagerDelegate = BIZCustomerSupportManager(accessToken: dependencies.authManager.userAuth?.auth.accessToken)
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: DeliveryDetailAction) {
        switch action {
        case .rootScreen:
            viewController?.dismiss(animated: true) {
                self.viewController?.navigationController?.popToRootViewController(animated: true)
            }
            
        case .qrCodeScreen:
            let controller = MaterialCameraActivationFactory.make()
            viewController?.navigationController?.pushViewController(controller, animated: true)
            
        case .customerSupport:
            guard let viewController = viewController else {
                return
            }
            customerSupport.presentTicketList(from: viewController, with: nil)
        }
    }
}
