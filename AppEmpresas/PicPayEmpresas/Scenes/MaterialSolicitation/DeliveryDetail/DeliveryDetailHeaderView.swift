import UI

extension DeliveryDetailHeaderView.Layout {
    enum Image {
        static let size = CGSize(width: 100, height: 129)
    }
    
    enum CloseButton {
        static let size = CGSize(width: 32, height: 32)
    }
    
    enum Constraints {
        static let headerHeight: CGFloat = 100
    }
    
    enum Title {
        static let height: CGFloat = 24
    }
}

final class DeliveryDetailHeaderView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var headerBackView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale900.color
        return view
    }()
    
    private lazy var slabImage = UIImageView(image: Assets.imgMaterialSolicitaionSlab.image)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .white())
        return label
    }()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setBackgroundImage(Assets.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        return button
    }()
    
    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    weak var delegate: CloseButtonDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.centerX.equalToSuperview()
            $0.height.equalTo(Layout.Title.height)
        }
        
        closeButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.CloseButton.size)
            $0.centerY.equalTo(titleLabel)
        }
        
        headerBackView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(slabImage)
        }
        
        slabImage.snp.makeConstraints {
            $0.centerX.bottom.equalToSuperview()
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.size.equalTo(Layout.Image.size)
        }
    }
    
    func buildViewHierarchy() {
        addSubview(headerBackView)
        addSubview(slabImage)
        addSubview(closeButton)
        addSubview(titleLabel)
    }
    
    @objc
    func didTapCloseButton() {
        delegate?.didTapCloseButton()
    }
}
