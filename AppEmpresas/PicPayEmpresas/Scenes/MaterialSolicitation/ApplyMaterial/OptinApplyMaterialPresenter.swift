import Core
import Foundation
import UI

protocol OptinApplyMaterialPresenting: AnyObject {
    var viewController: OptinApplyMaterialDisplay? { get set }
    func didNextStep(action: OptinApplyMaterialAction)
    func showDeclinePopUp()
    func closeScreen()
    func createErrorView()
}

final class OptinApplyMaterialPresenter: OptinApplyMaterialPresenting {
    private let coordinator: OptinApplyMaterialCoordinating
    weak var viewController: OptinApplyMaterialDisplay?

    init(coordinator: OptinApplyMaterialCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: OptinApplyMaterialAction) {
        coordinator.perform(action: action)
    }
    
    func showDeclinePopUp() {
        viewController?.createPopUpError()
    }
    
    func createErrorView() {
        viewController?.createErrorView()
    }
    
    func closeScreen() {
        coordinator.perform(action: .back)
    }
}
