import UI
import UIKit

enum OptinApplyMaterialAction: Equatable {
    case showAlert(_ popUp: UI.PopupViewController)
    case showErrorView(_ controller: ErrorViewController)
    case dismissAlert
    case addressScreen
    case back
    
    static func == (lhs: OptinApplyMaterialAction, rhs: OptinApplyMaterialAction) -> Bool {
        switch (lhs, rhs) {
        case (.dismissAlert, .dismissAlert),
             (.addressScreen, .addressScreen),
             (.back, .back):
            return true
        case let (.showAlert(lhsAlert), .showAlert(rhsAlert)):
            return lhsAlert == rhsAlert
        case let (.showErrorView(lhsError), .showErrorView(rhsError)):
            return lhsError == rhsError
        default:
            return false
        }
    }
}

protocol OptinApplyMaterialCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: OptinApplyMaterialAction)
}

final class OptinApplyMaterialCoordinator: OptinApplyMaterialCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: OptinApplyMaterialAction) {
        switch action {
        case .showAlert(let popUp):
            viewController?.showPopup(popUp)
        case .showErrorView(let controller):
            viewController?.present(controller, animated: true)
        case .dismissAlert:
            viewController?.dismiss(animated: true)
        case .back:
            viewController?.dismiss(animated: true) {
                self.viewController?.navigationController?.popToRootViewController(animated: true)
            }
        case .addressScreen:
            let addressViewController = MaterialAddressFactory.make()
            viewController?.pushViewController(addressViewController)
        }
    }
}
