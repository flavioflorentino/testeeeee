import UI

protocol OptinApplyMaterialDelegate: AnyObject {
    func actionButton(type: OptinApplyMaterialButtonType)
}

extension OptinApplyMaterialButtonStackViewItem.Layout {
    enum Size {
        static let buttonHeight: CGFloat = 48
    }
}

final class OptinApplyMaterialButtonStackViewItem: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var button: UIPPButton = {
        let button = UIPPButton(frame: .zero)
        button.addTarget(self, action: #selector(actionButton), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: OptinApplyMaterialDelegate?
    
    private var material: OptinApplyMaterial?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(button)
    }
    
    func setupConstraints() {
        button.layout {
            $0.top == topAnchor
            $0.leading == leadingAnchor + Spacing.base02
            $0.trailing == trailingAnchor - Spacing.base02
            $0.bottom == bottomAnchor
            $0.centerX == centerXAnchor
            $0.height == Layout.Size.buttonHeight
        }
    }
    
    func setup(material: OptinApplyMaterial) {
        self.material = material
        button.setTitle(material.title, for: .normal)
        
        guard case .button = material.buttonType else {
            configureLinkType()
            return
        }
        
        configureButtonType()
    }
    
    private func configureButtonType() {
        button.normalBackgrounColor = Colors.brandingBase.color
        button.highlightedBackgrounColor = Colors.brandingBase.color
        button.normalTitleColor = Colors.white.color
        button.highlightedTitleColor = Colors.white.color
        button.cornerRadius = Layout.Size.buttonHeight / 2
        button.layer.masksToBounds = true
    }
    
    private func configureLinkType() {
        button.titleLabel?.typography = .linkSecondary()
        button.borderColor = .clear
        button.normalBackgrounColor = .clear
        let attributeString = NSAttributedString(string: material?.title ?? "", attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
        button.setAttributedTitle(attributeString, for: .normal)
    }
    
    @objc
    private func actionButton() {
        guard let type = material?.buttonType else {
            return
        }
        delegate?.actionButton(type: type)
    }
}
