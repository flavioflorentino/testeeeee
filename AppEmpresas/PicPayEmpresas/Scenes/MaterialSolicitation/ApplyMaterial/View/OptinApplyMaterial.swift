enum OptinApplyMaterialType {
    case header
    case info
    case button
}

enum OptinApplyMaterialButtonType {
    case button
    case link
}

struct OptinApplyMaterial {
    let type: OptinApplyMaterialType
    let title: String?
    let description: String?
    let buttonType: OptinApplyMaterialButtonType?
    
    init(type: OptinApplyMaterialType, title: String? = nil, description: String? = nil, buttonType: OptinApplyMaterialButtonType? = nil) {
        self.type = type
        self.title = title
        self.description = description
        self.buttonType = buttonType
    }
}
