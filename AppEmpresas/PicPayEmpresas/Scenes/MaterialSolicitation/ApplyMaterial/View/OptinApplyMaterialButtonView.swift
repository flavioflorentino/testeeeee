import UI

final class OptinApplyMaterialButtonView: UIView, ViewConfiguration {
    private typealias Localizable = Strings.OptinApplyMaterial
    
    weak var delegate: OptinApplyMaterialDelegate? {
        didSet {
            acceptButtonView.delegate = delegate
            declineButtonView.delegate = delegate
        }
    }
    
    private lazy var acceptButtonView: OptinApplyMaterialButtonStackViewItem = {
        var view = OptinApplyMaterialButtonStackViewItem()
        let optinApplyMaterial = OptinApplyMaterial(
            type: .button,
            title: Localizable.titleOptinAcceptButton,
            buttonType: .button
        )
        view.setup(material: optinApplyMaterial)
        return view
    }()
    
    private lazy var declineButtonView: OptinApplyMaterialButtonStackViewItem = {
        var view = OptinApplyMaterialButtonStackViewItem()
        let optinApplyMaterial = OptinApplyMaterial(
            type: .button,
            title: Localizable.titleOptinbDeclineButton,
            buttonType: .link
        )
        view.setup(material: optinApplyMaterial)
        return view
    }()
    
    private lazy var buttonStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        stackView.addArrangedSubview(acceptButtonView)
        stackView.addArrangedSubview(declineButtonView)
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(buttonStackView)
    }
    
    func setupConstraints() {
        buttonStackView.layout {
            $0.top == topAnchor + Spacing.base03
            $0.leading == leadingAnchor + Spacing.base02
            $0.trailing == trailingAnchor - Spacing.base02
            $0.bottom == bottomAnchor - Spacing.base02
        }
    }
}
