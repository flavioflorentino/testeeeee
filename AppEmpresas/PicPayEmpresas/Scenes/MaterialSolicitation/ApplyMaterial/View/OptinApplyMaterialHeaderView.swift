import UI

extension OptinApplyMaterialHeaderView.Layout {
    enum SlabImage {
        static let size = CGSize(width: 100, height: 129)
    }
    
    enum CloseButton {
        static let size = CGSize(width: 32, height: 32)
    }
}

final class OptinApplyMaterialHeaderView: UIView, ViewConfiguration {
    private typealias Localizable = Strings.OptinApplyMaterial
    fileprivate enum Layout {}
    
    private lazy var headerBackView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.Business.grayscale600.color
        return view
    }()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setBackgroundImage(Assets.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var viewTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.OptinApplyMaterial.title
        label.typography = .title(.small)
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = Colors.white.color
        return label
    }()
    
    private lazy var slabImage: UIImageView = {
        let imageView = UIImageView(image: Assets.imgMaterialSolicitaionSlab.image)
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.titleOptinHeaderCell
        label.typography = .title(.xLarge)
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = Colors.grayscale700.color
        return label
    }()
    
    weak var delegate: CloseButtonDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        headerBackView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(slabImage)
        }
        
        viewTitleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.bottom.equalTo(slabImage.snp.top).offset(-Spacing.base02)
            $0.centerX.equalToSuperview()
        }
        
        closeButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.CloseButton.size)
            $0.bottom.equalTo(slabImage.snp.top).offset(-Spacing.base02)
            $0.centerY.equalTo(viewTitleLabel.snp.centerY)
        }
        
        slabImage.snp.makeConstraints {
            $0.size.equalTo(Layout.SlabImage.size)
            $0.centerX.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(slabImage.snp.bottom).offset(Spacing.base04)
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
        }
    }
    
    func buildViewHierarchy() {
        addSubview(headerBackView)
        addSubview(closeButton)
        addSubview(viewTitleLabel)
        addSubview(slabImage)
        addSubview(titleLabel)
    }
    
    @objc
    func didTapCloseButton() {
        delegate?.didTapCloseButton()
    }
}
