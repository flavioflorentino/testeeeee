import UI

final class OptinApplyMaterialInfoView: UIView, ViewConfiguration {
    private typealias Localizable = Strings.OptinApplyMaterial
    
    private lazy var freeMaterialInfoView: InfoStackViewItem = {
        var view = InfoStackViewItem()
        let optinApplyMaterial = OptinApplyMaterial(
            type: .info,
            title: Localizable.titleInfo1,
            description: Localizable.descriptionInfo1
        )
        view.setup(material: optinApplyMaterial)
        return view
    }()
    
    private lazy var qrCodeInfoView: InfoStackViewItem = {
        var view = InfoStackViewItem()
        let optinApplyMaterial = OptinApplyMaterial(
            type: .info,
            title: Localizable.titleInfo2,
            description: Localizable.descriptionInfo2
        )
        view.setup(material: optinApplyMaterial)
        return view
    }()
    
    private lazy var dispatchInfoView: InfoStackViewItem = {
        var view = InfoStackViewItem()
        let optinApplyMaterial = OptinApplyMaterial(
            type: .info,
            title: Localizable.titleInfo3,
            description: Localizable.descriptionInfo3
        )
        view.setup(material: optinApplyMaterial)
        return view
    }()
    
    private lazy var materialStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.addArrangedSubview(freeMaterialInfoView)
        stackView.addArrangedSubview(qrCodeInfoView)
        stackView.addArrangedSubview(dispatchInfoView)
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        materialStackView.layout {
            $0.top == topAnchor + Spacing.base02
            $0.leading == leadingAnchor + Spacing.base02
            $0.trailing == trailingAnchor - Spacing.base02
            $0.bottom == bottomAnchor - Spacing.base02
        }
    }
    
    func buildViewHierarchy() {
        addSubview(materialStackView)
    }
}
