import UI
import UIKit

protocol OptinMaterialPopupViewControllerActions: AnyObject {
    func confirmAction()
    func declineAction()
}

final class OptinMaterialPopUpView: UIViewController, ViewConfiguration {
    fileprivate typealias Localizable = Strings.MaterialSolicitation
    
    weak var delegate: OptinMaterialPopupViewControllerActions?
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.optoutConfirmButtonTitle, for: .normal)
        button.addTarget(self, action: #selector(confirmAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var declineButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle())
        let attText = NSAttributedString(
            string: Localizable.optoutDeclineButtonTitle,
            attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue, .foregroundColor: Colors.grayscale300.color]
        )
        button.setAttributedTitle(attText, for: .normal)
        button.addTarget(self, action: #selector(dismissAction), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
    }
    
    func buildViewHierarchy() {
        view.addSubview(confirmButton)
        view.addSubview(declineButton)
    }
    
    func setupConstraints() {
        confirmButton.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base00)
            $0.leading.trailing.equalToSuperview()
        }
        
        declineButton.snp.makeConstraints {
            $0.top.equalTo(confirmButton.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
}

@objc
extension OptinMaterialPopUpView {
    private func confirmAction(sender: UIButton) {
        delegate?.confirmAction()
    }
    
    private func dismissAction(sender: UIButton) {
        delegate?.declineAction()
    }
}
