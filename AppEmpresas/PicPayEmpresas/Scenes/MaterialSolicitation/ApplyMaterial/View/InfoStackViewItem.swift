import UI

extension InfoStackViewItem.Layout {
    enum Measures {
        static let dotViewDimension: CGFloat = 8
    }
}

final class InfoStackViewItem: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var dotView: UIView = {
        var view = UIView()
        view.backgroundColor = Colors.branding300.color
        view.cornerRadius = .light
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.typography = .title(.small)
        label.textColor = Colors.grayscale700.color
        return label
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = Colors.grayscale300.color
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        dotView.layout {
            $0.leading == leadingAnchor
            $0.centerY == titleLabel.centerYAnchor
            $0.height == Layout.Measures.dotViewDimension
            $0.width == Layout.Measures.dotViewDimension
        }
        
        titleLabel.layout {
            $0.top == topAnchor + Spacing.base01
            $0.leading == dotView.trailingAnchor + Spacing.base01
            $0.trailing == trailingAnchor - Spacing.base01
        }
        
        infoLabel.layout {
            $0.top == titleLabel.bottomAnchor + Spacing.base01
            $0.leading == titleLabel.leadingAnchor
            $0.trailing == titleLabel.trailingAnchor
            $0.bottom == bottomAnchor - Spacing.base01
        }
    }
    
    func buildViewHierarchy() {
        addSubview(dotView)
        addSubview(titleLabel)
        addSubview(infoLabel)
    }
    
    func setup(material: OptinApplyMaterial) {
        titleLabel.text = material.title
        infoLabel.text = material.description
    }
}
