import UI
import UIKit

protocol OptinApplyMaterialDisplay: AnyObject {
    func createPopUpError()
    func createErrorView()
}

final class OptinApplyMaterialViewController: ViewController<OptinApplyMaterialViewModelInputs, UIView> {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    fileprivate typealias Localizable = Strings.MaterialSolicitation
    
    private lazy var headerView: OptinApplyMaterialHeaderView = {
        var view = OptinApplyMaterialHeaderView()
        view.delegate = self
        return view
    }()
    
    private lazy var infoView = OptinApplyMaterialInfoView()
    
    private lazy var buttonView: OptinApplyMaterialButtonView = {
        var view = OptinApplyMaterialButtonView()
        view.delegate = self
        return view
    }()
    
    private lazy var materialInfoStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.addArrangedSubview(headerView)
        stackView.addArrangedSubview(infoView)
        stackView.addArrangedSubview(buttonView)
        return stackView
    }()
    
    private lazy var contentView: UIView = {
        var view = UIView()
        view.addSubview(materialInfoStackView)
        return view
    }()
    
    private lazy var scrollView: UIScrollView = {
        var scrollView = UIScrollView()
        scrollView.addSubview(contentView)
        return scrollView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.sendAnalytics()
        title = Localizable.viewTitle
    }
    
    override func configureViews() {
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.width.equalTo(view)
            $0.edges.equalToSuperview()
        }
        
        materialInfoStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

// MARK: View Model Outputs
extension OptinApplyMaterialViewController: OptinApplyMaterialDisplay {
    func createPopUpError() {
        let controller = OptinMaterialPopUpView()
        controller.delegate = self
        let errorPopup = UI.PopupViewController(
            title: Localizable.optoutTitlePopup,
            description: Localizable.optoutDescriptionPopup,
            preferredType: .custom(controller)
        )
        errorPopup.hideCloseButton = true
        
        viewModel.nextStep(action: .showAlert(errorPopup))
    }
    
    func createErrorView() {
        let controller = ErrorViewController()
        controller.delegate = self
        viewModel.nextStep(action: .showErrorView(controller))
    }
}

extension OptinApplyMaterialViewController: OptinApplyMaterialDelegate {
    func actionButton(type: OptinApplyMaterialButtonType) {
        viewModel.actionButton(type: type)
    }
}

extension OptinApplyMaterialViewController: CloseButtonDelegate {
    func didTapCloseButton() {
        viewModel.closeScreen()
    }
}

extension OptinApplyMaterialViewController: OptinMaterialPopupViewControllerActions {
    func confirmAction() {
        viewModel.declineMaterial()
    }
    
    func declineAction() {
        viewModel.nextStep(action: .dismissAlert)
    }
}

extension OptinApplyMaterialViewController: ErrorViewDelegate {
    func didTouchConfirmButton() {
        viewModel.nextStep(action: .dismissAlert)
    }
}
