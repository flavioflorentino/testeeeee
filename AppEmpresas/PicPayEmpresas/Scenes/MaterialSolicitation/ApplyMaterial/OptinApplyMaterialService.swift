import Core
import Foundation

protocol OptinApplyMaterialServicing {
    func checkOptOut(completion: @escaping(Result<OptoutMaterialSolicitation?, ApiError>) -> Void)
}

final class OptinApplyMaterialService: OptinApplyMaterialServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func checkOptOut(completion: @escaping(Result<OptoutMaterialSolicitation?, ApiError>) -> Void) {
        let endPoint = MaterialSolicitationEndPoint.materialCheckOptOut
        
        BizApi<OptoutMaterialSolicitation>(endpoint: endPoint).bizExecute {[weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model.data)
                completion(mappedResult)
            }
        }
    }
}
