import Foundation

enum OptinApplyMaterialFactory {
    static func make() -> OptinApplyMaterialViewController {
        let container = DependencyContainer()
        let service: OptinApplyMaterialServicing = OptinApplyMaterialService(dependencies: container)
        let coordinator: OptinApplyMaterialCoordinating = OptinApplyMaterialCoordinator()
        let presenter: OptinApplyMaterialPresenting = OptinApplyMaterialPresenter(coordinator: coordinator)
        let viewModel = OptinApplyMaterialViewModel(service: service, presenter: presenter)
        let viewController = OptinApplyMaterialViewController(viewModel: viewModel)
        viewController.hidesBottomBarWhenPushed = true

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
