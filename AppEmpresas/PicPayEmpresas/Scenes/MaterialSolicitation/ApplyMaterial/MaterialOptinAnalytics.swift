struct MaterialOptinAnalytics: MaterialRequestAnalyticsProtocol {
    var screenName: String? {
        "SOLICITACAO_DE_MATERIAL"
    }
    
    var buttonName: String? {
        switch buttonType {
        case .confirm:
            return "RECEBER_MATERIAL"
        case .decline:
            return "NAO_RECEBER_MATERIAL"
        default:
            return nil
        }
    }
    
    var buttonType: MaterialSolicitationButtonTypes?
}

struct MaterialOptinPopUpAnalytics: MaterialRequestAnalyticsProtocol {
    var screenName: String? {
        "NAO_RECEBIMENTO_MATERIAL"
    }
    
    var dialogName: String? {
        "CONFIRMACAO_NAO_RECEBIMENTO_MATERIAL"
    }
    
    var optionSelected: String? {
        "SIM_TENHO_CERTEZA"
    }
}
