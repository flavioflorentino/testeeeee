struct OptoutMaterialSolicitation: Decodable {
    private enum CodingKeys: String, CodingKey {
        case dontWantBoard = "dont_want_board"
        case sellerId = "seller_id"
        case updatedAt = "updated_at"
        case createdAt = "created_at"
        case id
    }
    
    let dontWantBoard: Bool
    let sellerId: Int
    let updatedAt: String
    let createdAt: String
    let id: Int
}
