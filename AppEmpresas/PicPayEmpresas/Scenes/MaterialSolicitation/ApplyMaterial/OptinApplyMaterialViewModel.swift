import AnalyticsModule
import Foundation

protocol OptinApplyMaterialViewModelInputs: AnyObject {
    func sendAnalytics()
    func actionButton(type: OptinApplyMaterialButtonType)
    func closeScreen()
    func declineMaterial()
    func nextStep(action: OptinApplyMaterialAction)
}

final class OptinApplyMaterialViewModel {
    private let service: OptinApplyMaterialServicing
    private let presenter: OptinApplyMaterialPresenting
    private let dependencies: HasAnalytics = DependencyContainer()
    private var optinAnalytics = MaterialOptinAnalytics()
    private let optinPopupAnalytics = MaterialOptinPopUpAnalytics()

    init(service: OptinApplyMaterialServicing, presenter: OptinApplyMaterialPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

extension OptinApplyMaterialViewModel: OptinApplyMaterialViewModelInputs {
    func sendAnalytics() {
        dependencies.analytics.log(MaterialRequestAnalytics.screenViewed(analyticsStruct: optinAnalytics))
    }
    
    func actionButton(type: OptinApplyMaterialButtonType) {
        switch type {
        case .button:
            presenter.didNextStep(action: .addressScreen)
            optinAnalytics.buttonType = .confirm
            dependencies.analytics.log(MaterialRequestAnalytics.buttonClicked(analyticsStruct: optinAnalytics))
        case .link:
            presenter.showDeclinePopUp()
            optinAnalytics.buttonType = .decline
            dependencies.analytics.log(MaterialRequestAnalytics.buttonClicked(analyticsStruct: optinAnalytics))
        }
    }
    
    func closeScreen() {
        presenter.closeScreen()
    }
    
    func declineMaterial() {
        presenter.didNextStep(action: .dismissAlert)
        service.checkOptOut { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success:
                self.presenter.didNextStep(action: .back)
                self.dependencies.analytics.log(MaterialRequestAnalytics.dialogOptionSelected(analyticsStruct: self.optinPopupAnalytics))
            case .failure:
                self.presenter.createErrorView()
            }
        }
    }
    
    func nextStep(action: OptinApplyMaterialAction) {
        presenter.didNextStep(action: action)
    }
}
