import LegacyPJ
import UI
import UIKit

protocol MaterialAddressDisplay: AnyObject {
    func showAddress(address: Address)
    func startLoading()
    func stopLoading()
}

private extension MaterialAddressViewController.Layout {
    enum Measures {
        static let errorImageSize = CGSize(width: 68, height: 68)
        static let successImageSize = CGSize(width: 160, height: 160)
    }
    
    enum LoadingView {
        static let alpha: CGFloat = 0.8
    }
}

final class MaterialAddressViewController: ViewController<MaterialAddressViewModelInputs, UIView>, LoadingViewProtocol {
    private typealias Localizable = Strings.MaterialSolicitation
    
    fileprivate enum Layout { }
    
    private lazy var materialAddressTitle: UILabel = {
        var label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale700())
        label.text = Localizable.materialAddressTitle
        return label
    }()
    
    private lazy var materialAddressDescription: UILabel = {
        var label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
        label.attributedText = highlightTextSlice()
        return label
    }()
    
    private lazy var titleDescriptionStackView: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Spacing.base03
        stack.axis = .vertical
        stack.addArrangedSubview(materialAddressTitle)
        stack.addArrangedSubview(materialAddressDescription)
        return stack
    }()
    
    private lazy var addressCardViewController: AddressCardViewController = {
        let viewController = AddressCardFactory.make(buttonDelegate: self)
        addChild(viewController)
        return viewController
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.materialSolicitationAddressConfirmButton, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapConfirmButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var materialAddressStackView: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Spacing.base03
        stack.axis = .vertical
        stack.alignment = .leading
        stack.addArrangedSubview(titleDescriptionStackView)
        stack.addArrangedSubview(addressCardViewController.view)
        stack.addArrangedSubview(confirmButton)
        return stack
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.keyboardDismissMode = .onDrag
        return scrollView
    }()
    
    private lazy var contentView = UIView()
    
    lazy var loadingView: LoadingView = {
        let loadingView = LoadingView()
        loadingView.alpha = Layout.LoadingView.alpha
        return loadingView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configuraViews()
        viewModel.loadAddress()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(materialAddressStackView)
        
        addressCardViewController.didMove(toParent: self)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        title = Localizable.viewTitle
    }
    
    override func setupConstraints() {
        setupInnerViewsConstraints()
        
        scrollView.snp.makeConstraints {
            $0.leading.equalTo(view)
            $0.trailing.equalTo(view)
            $0.top.equalTo(view)
            $0.bottom.equalTo(view)
        }
        
        contentView.snp.makeConstraints {
            $0.leading.equalTo(scrollView)
            $0.trailing.equalTo(scrollView)
            $0.top.equalTo(scrollView)
            $0.bottom.equalTo(scrollView)
            $0.width.equalTo(view)
        }
        
        materialAddressStackView.snp.makeConstraints {
            $0.leading.equalTo(contentView)
            $0.trailing.equalTo(contentView)
            $0.top.equalTo(contentView).offset(Spacing.base04)
            $0.bottom.equalTo(contentView).offset(-Spacing.base04)
        }
    }
    
    @objc
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc
    func didTapConfirmButton(_ sender: UIButton) {
        addressCardViewController.confirmButtonDidTap()
    }
    
    @objc
    func keyboardWillShow(notification: NSNotification) {
        guard
            let userInfo = notification.userInfo,
            let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            contentView.frame.origin.y == 0
        else {
            return
        }
        contentView.frame.origin.y -= keyboardSize.cgRectValue.height
    }
    
    @objc
    func keyboardWillHide(notification: NSNotification) {
        if contentView.frame.origin.y != 0 {
            contentView.frame.origin.y = 0
        }
    }
}

// MARK: Private methods

private extension MaterialAddressViewController {
    func configuraViews() {
        tapGesture()
        keyboardObservers()
    }
    
    func tapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    func keyboardObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    func setupInnerViewsConstraints() {
        titleDescriptionStackView.snp.makeConstraints {
            $0.leading.equalTo(materialAddressStackView).offset(Spacing.base03)
            $0.trailing.equalTo(materialAddressStackView).offset(-Spacing.base03)
        }
        addressCardViewController.view.snp.makeConstraints {
            $0.leading.equalTo(materialAddressStackView).offset(Spacing.base02)
            $0.trailing.equalTo(materialAddressStackView).offset(-Spacing.base02)
        }
        confirmButton.snp.makeConstraints {
            $0.leading.equalTo(materialAddressStackView).offset(Spacing.base03)
            $0.trailing.equalTo(materialAddressStackView).offset(-Spacing.base03)
            $0.bottom.equalTo(materialAddressStackView).offset(-Spacing.base03)
        }
    }
    
    func attributedStyle(with font: UIFont) -> [NSAttributedString.Key: Any] {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.26
        
        let style: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.paragraphStyle: paragraphStyle
        ]
        return style
    }
    
    func highlightTextSlice() -> NSAttributedString {
        let defaultStyle = attributedStyle(with: Typography.bodyPrimary(.default).font())
        let highlightStyle = attributedStyle(with: Typography.bodyPrimary(.highlight).font())
        
        let text = NSMutableAttributedString()
        text.append(NSAttributedString(
            string: Localizable.materialAddressDescriptionFirstDefaultSlice,
            attributes: defaultStyle))
        text.append(NSAttributedString(
            string: Localizable.materialAddressDescriptionHighlightSlice,
            attributes: highlightStyle))
        text.append(NSAttributedString(
            string: Localizable.materialAddressDescriptionSecondDefaultSlice,
            attributes: defaultStyle))
        
        return text
    }
}

// MARK: extension for View Model Outputs

extension MaterialAddressViewController: MaterialAddressDisplay {
    func showAddress(address: Address) {
        addressCardViewController.setup(address: address)
        addressCardViewController.setup(title: Localizable.addressCardTitle)
    }
    
    func startLoading() {
        startLoadingView()
    }
    
    func stopLoading() {
        stopLoadingView()
    }
}

extension MaterialAddressViewController: ButtonDelegateOutput {
    func sendAddress(addressValidation: AddressValidation) {
        viewModel.requestMaterial(address: addressValidation)
    }
}
