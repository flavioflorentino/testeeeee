struct MaterialAddressAnalytics: MaterialRequestAnalyticsProtocol {
    var screenName: String? {
        "SOLICITACAO_DE_MATERIAL_ENDERECO_ENTREGA"
    }
    
    var buttonName: String? {
        "CONFIMAR_ENDERECO"
    }
}
