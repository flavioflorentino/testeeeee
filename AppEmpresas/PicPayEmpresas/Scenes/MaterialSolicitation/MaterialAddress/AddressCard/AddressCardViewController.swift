import InputMask
import LegacyPJ
import UI
import UIKit

protocol AddressCardDisplay: AnyObject {
    func loadAddressByPostalCode(address: Address)
    func startLoading()
    func stopLoading()
    func dismissKeyboard()
    func setup(address: Address)
    func setPostalCode(_ postalCode: String?)
    func setFields(with address: Address)
    func updateLayoutPostalCodeField(isValid: Bool, message: String)
    func updateLayoutAddressField(isValid: Bool, message: String)
    func updateLayoutComplementField(isValid: Bool, message: String)
    func updateLayoutNumberField(isValid: Bool, message: String)
    func updateLayoutDistrictField(isValid: Bool, message: String)
    func updateLayoutCityField(isValid: Bool, message: String)
    func updateLayoutStateField(isValid: Bool, message: String)
    func confirmButtonDidTap()
}

extension AddressCardViewController.Layout {
    enum MinorTextField {
        static let width: CGFloat = 0.35
    }

    enum MajorTextField {
        static let width: CGFloat = 0.6
    }
    
    enum LoadingView {
        static let alpha: CGFloat = 0.8
    }
}

protocol EnableButtonDelegate: AnyObject {
    func setButtonStatus(with status: Bool)
}

final class AddressCardViewController: ViewController<AddressCardViewModelInputs, UIView>, LoadingViewProtocol {
    private typealias Localizable = Strings.MaterialSolicitation
    
    fileprivate enum Layout { }
    
    private lazy var addressCardTitle: UILabel = {
        var label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
        return label
    }()
    
    private lazy var postalCodeStackView: BusinessTextField = {
        let stackView = BusinessTextField()
        stackView.set(
            title: Localizable.addressCardPostalCode,
            textFieldDelegate: self,
            keyboardType: .numberPad,
            isRequired: true
        )
        return stackView
    }()
    
    private lazy var streetStackView: BusinessTextField = {
        let stackView = BusinessTextField()
        stackView.set(
            title: Localizable.addressCardStreet,
            textFieldDelegate: self,
            isRequired: true
        )
        return stackView
    }()
    
    private lazy var addressComplementStackView: BusinessTextField = {
        let stackView = BusinessTextField()
        stackView.set(
            title: Localizable.addressCardAddressComplement,
            textFieldDelegate: self
        )
        return stackView
    }()
    
    private lazy var addressNumberStackView: BusinessTextField = {
        let stackView = BusinessTextField()
        stackView.set(
            title: Localizable.addressCardAddressNumber,
            textFieldDelegate: self,
            isRequired: true
        )
        return stackView
    }()
    
    private lazy var neighborhoodStackView: BusinessTextField = {
        let stackView = BusinessTextField()
        stackView.set(
            title: Localizable.addressCardNeighborhood,
            textFieldDelegate: self,
            isRequired: true
        )
        return stackView
    }()
    
    private lazy var cityStackView: BusinessTextField = {
        let stackView = BusinessTextField()
        stackView.set(
            title: Localizable.addressCardCity,
            textFieldDelegate: self,
            isRequired: true
        )
        return stackView
    }()
    
    private lazy var stateStackView: BusinessTextField = {
        let stackView = BusinessTextField()
        stackView.set(
            title: Localizable.addressCardState,
            textFieldDelegate: self,
            isRequired: true
        )
        return stackView
    }()
    
    private lazy var numberNeighborhoodStackView: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Spacing.base03
        stack.axis = .horizontal
        stack.alignment = .top
        stack.addArrangedSubview(addressNumberStackView)
        stack.addArrangedSubview(neighborhoodStackView)
        return stack
    }()
    
    private lazy var cityStateStackView: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Spacing.base03
        stack.axis = .horizontal
        stack.alignment = .top
        stack.addArrangedSubview(cityStackView)
        stack.addArrangedSubview(stateStackView)
        return stack
    }()
    
    private lazy var contentView: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Spacing.base03
        stack.axis = .vertical
        stack.addArrangedSubview(addressCardTitle)
        stack.addArrangedSubview(postalCodeStackView)
        stack.addArrangedSubview(streetStackView)
        stack.addArrangedSubview(addressComplementStackView)
        stack.addArrangedSubview(numberNeighborhoodStackView)
        stack.addArrangedSubview(cityStateStackView)
        return stack
    }()
    
    private lazy var materialAddressView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundPrimary.color
        view.addSubview(contentView)
        
        view.layer.shadowColor = Colors.grayscale300.color.cgColor
        view.layer.shadowRadius = CornerRadius.medium
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowOpacity = 1
        view.layer.cornerRadius = CornerRadius.strong
        
        return view
    }()
    
    lazy var loadingView: LoadingView = {
        let loadingView = LoadingView()
        loadingView.alpha = Layout.LoadingView.alpha
        return loadingView
    }()
    
    override func buildViewHierarchy() {
        view.addSubview(materialAddressView)
    }
    
    override func setupConstraints() {
        addressNumberStackView.snp.makeConstraints {
            $0.width.equalTo(contentView).multipliedBy(Layout.MinorTextField.width)
        }
        neighborhoodStackView.snp.makeConstraints {
            $0.width.equalTo(contentView).multipliedBy(Layout.MajorTextField.width)
        }
        cityStackView.snp.makeConstraints {
            $0.width.equalTo(contentView).multipliedBy(Layout.MajorTextField.width)
        }
        stateStackView.snp.makeConstraints {
            $0.width.equalTo(contentView).multipliedBy(Layout.MinorTextField.width)
        }
        
        contentView.snp.makeConstraints {
            $0.leading.equalTo(materialAddressView).offset(Spacing.base03)
            $0.trailing.equalTo(materialAddressView).offset(-Spacing.base03)
            $0.top.equalTo(materialAddressView).offset(Spacing.base04)
            $0.bottom.equalTo(materialAddressView).offset(-Spacing.base05)
        }
        
        materialAddressView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    func setup(title: String) {
        addressCardTitle.text = title
    }
    
    func address() -> AddressValidation {
        let address = AddressValidation(
            postalCode: postalCodeAddressField(),
            address: streetAddressField(),
            district: neighborhoodAddressField(),
            city: cityAddressField(),
            state: stateAddressField(),
            number: numberAddressField(),
            complement: complementAddressField()
        )
        return address
    }
}

// MARK: View Model Outputs
extension AddressCardViewController: AddressCardDisplay {
    func updateLayoutPostalCodeField(isValid: Bool, message: String) {
        postalCodeStackView.updateStatus(validField: isValid, text: message)
    }
    
    func updateLayoutAddressField(isValid: Bool, message: String) {
        streetStackView.updateStatus(validField: isValid, text: message)
    }
    
    func updateLayoutComplementField(isValid: Bool, message: String) {
        addressComplementStackView.updateStatus(validField: isValid, text: message)
    }
    
    func updateLayoutNumberField(isValid: Bool, message: String) {
        addressNumberStackView.updateStatus(validField: isValid, text: message)
    }
    
    func updateLayoutDistrictField(isValid: Bool, message: String) {
        neighborhoodStackView.updateStatus(validField: isValid, text: message)
    }
    
    func updateLayoutCityField(isValid: Bool, message: String) {
        cityStackView.updateStatus(validField: isValid, text: message)
    }
    
    func updateLayoutStateField(isValid: Bool, message: String) {
        stateStackView.updateStatus(validField: isValid, text: message)
    }
    
    func loadAddressByPostalCode(address: Address) {
        viewModel.setup(address)
        viewModel.updateLayoutAllFields(addressValidation: self.address())
    }
    
    func setPostalCode(_ postalCode: String?) {
        postalCodeStackView.updateTextField(with: postalCode, mask: "[00000]-[000]")
        postalCodeStackView.isValid = true
    }
    
    func startLoading() {
        startLoadingView()
    }
    
    func stopLoading() {
        stopLoadingView()
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func setup(address: Address) {
        viewModel.setup(address)
    }
    
    func setFields(with address: Address) {
        addressNumberStackView.updateTextField(with: address.number)
        addressComplementStackView.updateTextField(with: address.complement)
        streetStackView.updateTextField(with: address.address)
        neighborhoodStackView.updateTextField(with: address.district)
        cityStackView.updateTextField(with: address.city)
        stateStackView.updateTextField(with: address.state)
    }
    
    func postalCodeAddressField() -> AddressField {
        AddressField(
            text: postalCodeStackView.textFieldValue() ?? "",
            isRequired: postalCodeStackView.isRequired,
            isValid: postalCodeStackView.isValid
        )
    }
    
    func streetAddressField() -> AddressField {
        AddressField(
            text: streetStackView.textFieldValue() ?? "",
            isRequired: streetStackView.isRequired,
            isValid: streetStackView.isValid
        )
    }
    
    func complementAddressField() -> AddressField {
        AddressField(
            text: addressComplementStackView.textFieldValue() ?? "",
            isRequired: addressComplementStackView.isRequired,
            isValid: addressComplementStackView.isValid
        )
    }
    
    func numberAddressField() -> AddressField {
        AddressField(
            text: addressNumberStackView.textFieldValue() ?? "",
            isRequired: addressNumberStackView.isRequired,
            isValid: addressNumberStackView.isValid
        )
    }
    
    func neighborhoodAddressField() -> AddressField {
        AddressField(
            text: neighborhoodStackView.textFieldValue() ?? "",
            isRequired: neighborhoodStackView.isRequired,
            isValid: neighborhoodStackView.isValid
        )
    }
    
    func cityAddressField() -> AddressField {
        AddressField(
            text: cityStackView.textFieldValue() ?? "",
            isRequired: cityStackView.isRequired,
            isValid: cityStackView.isValid
        )
    }
    
    func stateAddressField() -> AddressField {
        AddressField(
            text: stateStackView.textFieldValue() ?? "",
            isRequired: stateStackView.isRequired,
            isValid: stateStackView.isValid
        )
    }
    
    func confirmButtonDidTap() {
        viewModel.updateLayoutAllFields(addressValidation: address())
        viewModel.sendAddress(addressValidation: address())
    }
}

extension AddressCardViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if postalCodeStackView.equalsTextField(to: textField) {
            viewModel.updateLayoutPostalCodeField(postalCodeAddressField())
        }
        if streetStackView.equalsTextField(to: textField) {
            viewModel.updateLayoutStreetField(streetAddressField())
        }
        if addressComplementStackView.equalsTextField(to: textField) {
            viewModel.updateLayoutComplementField(complementAddressField())
        }
        if addressNumberStackView.equalsTextField(to: textField) {
            viewModel.updateLayoutNumberField(numberAddressField())
        }
        if neighborhoodStackView.equalsTextField(to: textField) {
            viewModel.updateLayoutDistrictField(neighborhoodAddressField())
        }
        if cityStackView.equalsTextField(to: textField) {
            viewModel.updateLayoutCityField(cityAddressField())
        }
        if stateStackView.equalsTextField(to: textField) {
            viewModel.updateLayoutStateField(stateAddressField())
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if
            addressNumberStackView.equalsTextField(to: textField),
            string.isNotEmpty,
            let text = textField.text,
            text.isEmpty
        {
            textField.text = string
            addressNumberStackView.isValid = true
            viewModel.updateLayoutNumberField(numberAddressField())
            return false
        }

        guard
            postalCodeStackView.equalsTextField(to: textField),
            string.isNotEmpty,
            let postalCode = textField.text as NSString?
        else {
            return true
        }

        let unformattedPostalCode = postalCode.replacingCharacters(in: range, with: string)
            .replacingOccurrences(of: "-", with: "")
        let maxLength = 8

        if unformattedPostalCode.count == maxLength {
            viewModel.loadAddress(by: unformattedPostalCode)
        }
        textField.text = maskString(input: unformattedPostalCode, mask: "[00000]-[000]")

        return false
    }
    
    func maskString(input: String, mask: String) -> String {
        do {
            let mask: Mask = try Mask(format: mask)
            let result: Mask.Result = mask.apply(
                toText: CaretString(string: input),
                autocomplete: true
            )
            return result.formattedText.string
        } catch {
            return ""
        }
    }
}
