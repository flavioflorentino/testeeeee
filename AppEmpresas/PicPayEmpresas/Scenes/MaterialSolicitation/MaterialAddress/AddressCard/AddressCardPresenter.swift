import Core
import LegacyPJ
import Foundation

protocol AddressCardPresenting: AnyObject {
    var viewController: AddressCardDisplay? { get set }
    func loadAddressByPostalCode(address: Address)
    func startLoading()
    func setup(address: Address)
    func updateLayoutPostalCodeField(isValid: Bool, message: String)
    func updateLayoutAddressField(isValid: Bool, message: String)
    func updateLayoutComplementField(isValid: Bool, message: String)
    func updateLayoutNumberField(isValid: Bool, message: String)
    func updateLayoutDistrictField(isValid: Bool, message: String)
    func updateLayoutCityField(isValid: Bool, message: String)
    func updateLayoutStateField(isValid: Bool, message: String)
    func confirmButtonConfig()
}

final class AddressCardPresenter: AddressCardPresenting {
    weak var viewController: AddressCardDisplay?
    
    func loadAddressByPostalCode(address: Address) {
        viewController?.dismissKeyboard()
        viewController?.loadAddressByPostalCode(address: address)
        viewController?.stopLoading()
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func setup(address: Address) {
        if let postalCode = address.cep {
            viewController?.setPostalCode(postalCode)
        }
        viewController?.setFields(with: address)
    }
    
    func updateLayoutPostalCodeField(isValid: Bool, message: String) {
        viewController?.dismissKeyboard()
        viewController?.updateLayoutPostalCodeField(isValid: isValid, message: message)
        viewController?.stopLoading()
    }
    
    func updateLayoutAddressField(isValid: Bool, message: String) {
        viewController?.updateLayoutAddressField(isValid: isValid, message: message)
    }
    
    func updateLayoutComplementField(isValid: Bool, message: String) {
        viewController?.updateLayoutComplementField(isValid: isValid, message: message)
    }
    
    func updateLayoutNumberField(isValid: Bool, message: String) {
        viewController?.updateLayoutNumberField(isValid: isValid, message: message)
    }
    
    func updateLayoutDistrictField(isValid: Bool, message: String) {
        viewController?.updateLayoutDistrictField(isValid: isValid, message: message)
    }
    
    func updateLayoutCityField(isValid: Bool, message: String) {
        viewController?.updateLayoutCityField(isValid: isValid, message: message)
    }
    
    func updateLayoutStateField(isValid: Bool, message: String) {
        viewController?.updateLayoutStateField(isValid: isValid, message: message)
    }
    
    func confirmButtonConfig() {
        viewController?.confirmButtonDidTap()
    }
}
