import Foundation
import LegacyPJ

protocol ButtonDelegateOutput: AnyObject {
    func sendAddress(addressValidation: AddressValidation)
}

protocol AddressCardViewModelInputs: AnyObject {
    func loadAddress(by cep: String)
    func setup(_ address: Address)
    func updateLayoutAllFields(addressValidation: AddressValidation)
    func updateLayoutPostalCodeField(_ field: AddressField?)
    func updateLayoutStreetField(_ field: AddressField)
    func updateLayoutComplementField(_ field: AddressField)
    func updateLayoutNumberField(_ field: AddressField)
    func updateLayoutDistrictField(_ field: AddressField)
    func updateLayoutCityField(_ field: AddressField)
    func updateLayoutStateField(_ field: AddressField)
    func isValidAllFields(addressValidation: AddressValidation) -> Bool
    func sendAddress(addressValidation: AddressValidation)
}

final class AddressCardViewModel {
    private typealias Localizable = Strings.MaterialSolicitation
    
    private let service: AddressCardServicing
    private let presenter: AddressCardPresenting
    
    weak var buttonDelegate: ButtonDelegateOutput?
    
    init(service: AddressCardServicing, presenter: AddressCardPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

extension AddressCardViewModel: AddressCardViewModelInputs {
    func loadAddress(by postalCode: String) {
        presenter.startLoading()
        
        service.loadAddress(by: postalCode) { [weak self] result in
            switch result {
            case .success(let address):
                self?.presenter.loadAddressByPostalCode(address: address)
            case .failure:
                self?.updateLayoutPostalCodeField(nil)
            }
        }
    }
    
    func sendAddress(addressValidation: AddressValidation) {
        if isValidAllFields(addressValidation: addressValidation) {
            buttonDelegate?.sendAddress(addressValidation: addressValidation)
        }
    }
}

// MARK: Setup

extension AddressCardViewModel {
    func setup(_ address: Address) {
        presenter.setup(address: address)
    }
    
    func errorTextConfigured(errorMessage: String? = nil) -> String {
        guard let errorMessage = errorMessage else {
            return Localizable.addressCardRequiredField
        }
        return errorMessage
    }
}

// MARK: Validations

extension AddressCardViewModel {
    func isRequiredAndFilledField(addressField: AddressField) -> Bool {
        !addressField.isRequired || (addressField.isRequired && addressField.text.trimmed.isNotEmpty)
    }
    
    func isValidPostalCodeField(addressField: AddressField, validation: Bool = true) -> Bool {
        isRequiredAndFilledField(addressField: addressField) && addressField.isValid && validation
    }
    
    func isValidPostalCode(text: String) -> Bool {
        text.trimmed.count == 9
    }
    
    func isValidAllFields(addressValidation: AddressValidation) -> Bool {
        addressValidation.fields().allSatisfy { $0.isValid && isRequiredAndFilledField(addressField: $0) }
    }
}

// MARK: Update Layout

extension AddressCardViewModel {
    func updateLayoutAllFields(addressValidation: AddressValidation) {
        updateLayoutPostalCodeField(addressValidation.postalCode)
        updateLayoutStreetField(addressValidation.address)
        updateLayoutComplementField(addressValidation.complement)
        updateLayoutNumberField(addressValidation.number)
        updateLayoutDistrictField(addressValidation.district)
        updateLayoutCityField(addressValidation.city)
        updateLayoutStateField(addressValidation.state)
    }
    
    func updateLayoutPostalCodeField(_ field: AddressField?) {
        var errorText = errorTextConfigured()
        
        guard let field = field else {
            errorText = errorTextConfigured(errorMessage: Localizable.addressCardPostalCodeError)
            presenter.updateLayoutPostalCodeField(isValid: false, message: errorText)
            return
        }
        
        guard isRequiredAndFilledField(addressField: field) else {
            presenter.updateLayoutPostalCodeField(isValid: false, message: errorText)
            return
        }
        
        let isValid = isValidPostalCodeField(addressField: field, validation: isValidPostalCode(text: field.text))
        errorText = errorTextConfigured(errorMessage: Localizable.addressCardPostalCodeError)
        
        presenter.updateLayoutPostalCodeField(isValid: isValid, message: errorText)
    }
    
    func updateLayoutStreetField(_ field: AddressField) {
        let isValid = isRequiredAndFilledField(addressField: field)
        presenter.updateLayoutAddressField(isValid: isValid, message: errorTextConfigured())
    }
    
    func updateLayoutComplementField(_ field: AddressField) {
        let isValid = isRequiredAndFilledField(addressField: field)
        presenter.updateLayoutComplementField(isValid: isValid, message: errorTextConfigured())
    }
    
    func updateLayoutNumberField(_ field: AddressField) {
        let isValid = isRequiredAndFilledField(addressField: field)
        presenter.updateLayoutNumberField(isValid: isValid, message: errorTextConfigured())
    }
    
    func updateLayoutDistrictField(_ field: AddressField) {
        let isValid = isRequiredAndFilledField(addressField: field)
        presenter.updateLayoutDistrictField(isValid: isValid, message: errorTextConfigured())
    }
    
    func updateLayoutCityField(_ field: AddressField) {
        let isValid = isRequiredAndFilledField(addressField: field)
        presenter.updateLayoutCityField(isValid: isValid, message: errorTextConfigured())
    }
    
    func updateLayoutStateField(_ field: AddressField) {
        let isValid = isRequiredAndFilledField(addressField: field)
        presenter.updateLayoutStateField(isValid: isValid, message: errorTextConfigured())
    }
}
