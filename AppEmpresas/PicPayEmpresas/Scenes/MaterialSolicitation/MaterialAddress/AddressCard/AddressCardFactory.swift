import Foundation

enum AddressCardFactory {
    static func make(buttonDelegate: ButtonDelegateOutput) -> AddressCardViewController {
        let container = DependencyContainer()
        let service: AddressCardServicing = AddressCardService(dependencies: container)
        let presenter: AddressCardPresenting = AddressCardPresenter()
        let viewModel = AddressCardViewModel(service: service, presenter: presenter)
        let viewController = AddressCardViewController(viewModel: viewModel)

        presenter.viewController = viewController
        viewModel.buttonDelegate = buttonDelegate

        return viewController
    }
}
