import Core
import LegacyPJ

protocol AddressCardServicing {
    func loadAddress(by postalCode: String, completion: @escaping(Result<Address, LegacyPJError>) -> Void)
}

final class AddressCardService: AddressCardServicing {
    typealias Dependencies = HasMainQueue
    
    // MARK: Private Properties
    
    private let dependencies: Dependencies
    
    // MARK: - Inits

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func loadAddress(by postalCode: String, completion: @escaping(Result<Address, LegacyPJError>) -> Void) {
        let apiSignUp = ApiSignUp()
        apiSignUp.verifyCEP(cep: postalCode) {[weak self] address, error in
            if error == nil, let address = address {
                self?.dependencies.mainQueue.async {
                    completion(.success(address))
                }
                return
            }
            if let error = error {
                self?.dependencies.mainQueue.async {
                    completion(.failure(error))
                }
            }
        }
    }
}
