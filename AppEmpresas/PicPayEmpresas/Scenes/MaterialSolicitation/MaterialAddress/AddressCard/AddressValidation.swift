struct AddressValidation {
    let postalCode: AddressField
    let address: AddressField
    let district: AddressField
    let city: AddressField
    let state: AddressField
    let number: AddressField
    let complement: AddressField
    
    func fields() -> [AddressField] {
        var fields = [AddressField]()
        fields.append(postalCode)
        fields.append(address)
        fields.append(district)
        fields.append(city)
        fields.append(state)
        fields.append(number)
        fields.append(complement)
        
        return fields
    }
}

struct AddressField {
    var text: String
    var isRequired: Bool = false
    var isValid: Bool = true
}
