import Core

enum MaterialSolicitationEndPoint {
    case materialSolicitation(address: MaterialSolicitationRequest)
    case materialAddressSolicitation
    case materialSolicitationMonitoring
    case materialActivation(code: String)
    case materialCheckOptOut
    case materialSolicitationCheckStatus
}

extension MaterialSolicitationEndPoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .materialSolicitation:
            return "/board-request"
        case .materialAddressSolicitation:
            return "/seller/address"
        case .materialSolicitationMonitoring:
            return "/board-delivery"
        case .materialActivation:
            return "/board-activate"
        case .materialCheckOptOut:
            return "/seller-configuration"
        case .materialSolicitationCheckStatus:
            return "/check"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .materialSolicitation, .materialActivation, .materialCheckOptOut:
            return .post
        default:
            return .get
        }
    }
    
    var body: Data? {
        switch self {
        case let .materialSolicitation(address):
            return [
                "cep": address.cep.replacingOccurrences(of: "-", with: ""),
                "state": address.state,
                "address": address.address,
                "number": address.number,
                "complement": address.complement,
                "city": address.city,
                "district": address.district
            ].toData()
        case .materialActivation(let code):
            return ["qr_code": code].toData()
        case .materialCheckOptOut:
            return ["dont_want_board": true].toData()
        default:
            return nil
        }
    }
}
