import UIKit

enum MaterialAddressAction: Equatable {
    case solicitationSuccess
    case solicitationError(BizErrorViewLayout)
    
    static func == (lhs: MaterialAddressAction, rhs: MaterialAddressAction) -> Bool {
        switch (lhs, rhs) {
        case (.solicitationSuccess, .solicitationSuccess):
            return true
        case let (.solicitationError(lhsError), .solicitationError(rhsError)):
            return lhsError == rhsError
        default:
            return false
        }
    }
}

protocol MaterialAddressCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: MaterialAddressAction)
}

final class MaterialAddressCoordinator: MaterialAddressCoordinating {
    weak var viewController: UIViewController?
    private var solicitationErrorView: MaterialSolicitationError?
    
    func perform(action: MaterialAddressAction) {
        switch action {
        case .solicitationSuccess:
            let controller = SolicitationSuccessFactory.make(state: .solicitation)
            viewController?.navigationController?.pushViewController(controller, animated: true)
            
        case .solicitationError(let viewParams):
            solicitationErrorView = MaterialSolicitationError(viewController: viewController)
            let controller = BizErrorViewFactory.make(errorView: viewParams, errorCoordinatorDelegate: solicitationErrorView)
            
            viewController?.navigationController?.present(controller, animated: true)
        }
    }
}
