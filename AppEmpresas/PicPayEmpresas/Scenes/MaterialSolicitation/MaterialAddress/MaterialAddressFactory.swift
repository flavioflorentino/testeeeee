import Foundation

enum MaterialAddressFactory {
    static func make() -> MaterialAddressViewController {
        let container = DependencyContainer()
        let service: MaterialAddressServicing = MaterialAddressService(dependencies: container)
        let coordinator: MaterialAddressCoordinating = MaterialAddressCoordinator()
        let presenter: MaterialAddressPresenting = MaterialAddressPresenter(coordinator: coordinator)
        let viewModel = MaterialAddressViewModel(service: service, presenter: presenter)
        let viewController = MaterialAddressViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
