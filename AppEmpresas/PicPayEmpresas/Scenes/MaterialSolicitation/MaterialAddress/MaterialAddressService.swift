import Core

protocol MaterialAddressServicing {
    func loadAddress(completion: @escaping(Result<MaterialAddressResponse?, ApiError>) -> Void)
    func requestMaterial(
        materialSolicitationRequest: MaterialSolicitationRequest,
        completion: @escaping(Result<MaterialSolicitationResponse?, ApiError>) -> Void
    )
}

final class MaterialAddressService: MaterialAddressServicing {
    typealias Dependencies = HasMainQueue & HasApiSeller
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func loadAddress(completion: @escaping(Result<MaterialAddressResponse?, ApiError>) -> Void) {
        let endpoint = MaterialSolicitationEndPoint.materialAddressSolicitation
    
        BizApi<MaterialAddressResponse>(endpoint: endpoint).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model.data)
                
                completion(mappedResult)
            }
        }
    }
    
    func requestMaterial(
        materialSolicitationRequest: MaterialSolicitationRequest,
        completion: @escaping(Result<MaterialSolicitationResponse?, ApiError>) -> Void
    ) {
        let endpoint = MaterialSolicitationEndPoint.materialSolicitation(address: materialSolicitationRequest)
    
        BizApi<MaterialSolicitationResponse>(endpoint: endpoint).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }
}
