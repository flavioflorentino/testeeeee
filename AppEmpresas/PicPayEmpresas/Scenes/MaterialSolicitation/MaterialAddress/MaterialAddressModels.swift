import Foundation

struct MaterialAddressResponse: Decodable {
    let cep: String
    let address: String
    let district: String
    let city: String
    let state: String
    let number: String
    let complement: String?
}

struct MaterialSolicitationRequest: Encodable {
    let cep: String
    let address: String
    let district: String
    let city: String
    let state: String
    let number: String
    let complement: String?
}

struct MaterialSolicitationResponse: Decodable {
    private enum CodingKeys: String, CodingKey {
        case postalCode = "cep"
        case street = "address"
        case district
        case city
        case state
        case number
        case complement
        case sellerId = "seller_id"
        case lastUpdate = "updated_at"
        case createdDate = "created_at"
        case id
    }
    
    let postalCode: String
    let street: String
    let district: String?
    let city: String
    let state: String
    let number: String
    let complement: String?
    let sellerId: Int
    let lastUpdate: String
    let createdDate: String
    let id: Int
}
