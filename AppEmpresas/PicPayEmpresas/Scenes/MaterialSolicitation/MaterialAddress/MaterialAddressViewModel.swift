import AnalyticsModule
import Foundation

// MARK: protocol for View Controller Inputs

protocol MaterialAddressViewModelInputs: AnyObject {
    func loadAddress()
    func requestMaterial(address: AddressValidation)
}

final class MaterialAddressViewModel {
    private let service: MaterialAddressServicing
    private let presenter: MaterialAddressPresenting
    private let addressAnalytics = MaterialAddressAnalytics()
    
    private let dependencies: HasAnalytics = DependencyContainer()
    
    init(service: MaterialAddressServicing, presenter: MaterialAddressPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

// MARK: extension for View Controller Inputs

extension MaterialAddressViewModel: MaterialAddressViewModelInputs {
    func loadAddress() {
        dependencies.analytics.log(MaterialRequestAnalytics.screenViewed(analyticsStruct: addressAnalytics))
        presenter.startLoading()
        service.loadAddress {[weak self] result in
            switch result {
            case .success(let address):
                self?.presenter.showMaterialAddress(materialAddressResponse: address)
            case .failure:
                return
            }
        }
    }
    
    func requestMaterial(address: AddressValidation) {
        dependencies.analytics.log(MaterialRequestAnalytics.buttonClicked(analyticsStruct: addressAnalytics))
        
        presenter.startLoading()
        
        let materialSolicitationRequest = MaterialSolicitationRequest(
            cep: address.postalCode.text,
            address: address.address.text,
            district: address.district.text,
            city: address.city.text,
            state: address.state.text,
            number: address.number.text,
            complement: address.complement.text
        )
        service.requestMaterial(materialSolicitationRequest: materialSolicitationRequest) {[weak self] result in
            switch result {
            case .success:
                self?.presenter.didNextStep(action: .solicitationSuccess)
            case .failure:
                self?.presenter.showErrorView()
            }
        }
    }
}
