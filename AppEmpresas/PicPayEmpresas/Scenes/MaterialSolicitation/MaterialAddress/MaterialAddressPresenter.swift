import Core
import Foundation
import LegacyPJ

protocol MaterialAddressPresenting: AnyObject {
    var viewController: MaterialAddressDisplay? { get set }
    func startLoading()
    func didNextStep(action: MaterialAddressAction)
    func showMaterialAddress(materialAddressResponse: MaterialAddressResponse?)
    func showErrorView()
}

final class MaterialAddressPresenter: MaterialAddressPresenting {
    private let coordinator: MaterialAddressCoordinating
    weak var viewController: MaterialAddressDisplay?

    init(coordinator: MaterialAddressCoordinating) {
        self.coordinator = coordinator
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func didNextStep(action: MaterialAddressAction) {
        viewController?.stopLoading()
        coordinator.perform(action: action)
    }
    
    func showMaterialAddress(materialAddressResponse: MaterialAddressResponse?) {
        var address = Address()
        address.cep = materialAddressResponse?.cep
        address.address = materialAddressResponse?.address
        address.district = materialAddressResponse?.district
        address.city = materialAddressResponse?.city
        address.state = materialAddressResponse?.state
        address.number = materialAddressResponse?.number
        address.complement = materialAddressResponse?.complement
        
        viewController?.showAddress(address: address)
        viewController?.stopLoading()
    }
    
    func showErrorView() {
        viewController?.stopLoading()
        
        let viewConfig = BizErrorViewLayout(
            title: Strings.MaterialSolicitation.materialSolicitationErrorTitle,
            message: Strings.MaterialSolicitation.materialSolicitationErrorSubtitle,
            image: Assets.Emoji.iconBad.image
        )
        coordinator.perform(action: .solicitationError(viewConfig))
    }
}
