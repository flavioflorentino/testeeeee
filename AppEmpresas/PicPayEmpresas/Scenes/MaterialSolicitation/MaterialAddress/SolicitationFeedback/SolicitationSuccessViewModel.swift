import AnalyticsModule
import Foundation

protocol SolicitationSuccessViewModelInputs: AnyObject {
    func setupView()
    func didNextStep()
}

final class SolicitationSuccessViewModel {
    private let presenter: SolicitationSuccessPresenting
    private let dependencies: HasAnalytics = DependencyContainer()
    private let state: SuccessFeedbackState
    private let solicitationSuccessAnalytics = MaterialSolicitationSuccessAnalytics()
    private let activationSucessAnalytics = MaterialActivationSuccessAnalytics()

    init(presenter: SolicitationSuccessPresenting, state: SuccessFeedbackState) {
        self.presenter = presenter
        self.state = state
    }
}

extension SolicitationSuccessViewModel: SolicitationSuccessViewModelInputs {
    func setupView() {
        if state == .solicitation {
            dependencies.analytics.log(MaterialRequestAnalytics.screenViewed(analyticsStruct: solicitationSuccessAnalytics))
            presenter.setupSolicitation()
        } else {
            dependencies.analytics.log(MaterialRequestAnalytics.screenViewed(analyticsStruct: activationSucessAnalytics))
            presenter.setupActivation()
        }
    }
    
    func didNextStep() {
        guard state == .solicitation else {
            presenter.didNextStep(action: .rootScreen)
            return
        }
        dependencies.analytics.log(MaterialRequestAnalytics.buttonClicked(analyticsStruct: solicitationSuccessAnalytics))
        presenter.didNextStep(action: .deliveryForecast)
    }
}
