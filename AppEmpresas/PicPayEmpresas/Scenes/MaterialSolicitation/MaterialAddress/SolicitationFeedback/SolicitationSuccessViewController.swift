import SnapKit
import UI
import UIKit

protocol SolicitationSuccessDisplay: AnyObject {
    func setView(imageAsset: ImageAsset, title: String, description: String, buttonTitle: String, state: SuccessFeedbackState)
}

private extension SolicitationSuccessViewController.Layout {
    enum Button {
        static let height: CGFloat = 48
    }
    enum Image {
        static let solicitaionSize = CGSize(width: 160, height: 160)
        static let successSize = CGSize(width: 112, height: 143)
    }
    enum Title {
        static let height: CGFloat = 33
    }
}

final class SolicitationSuccessViewController: ViewController<SolicitationSuccessViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    private let imageView = UIImageView()
    
    private let titleLabel: UILabel = {
        var label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private let subtitleLabel: UILabel = {
        var label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var feedbackView: UIView = {
        let view = UIView()
        view.addSubview(imageView)
        view.addSubview(titleLabel)
        view.addSubview(subtitleLabel)
        return view
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapConfirmButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var buttonView: UIView = {
        let view = UIView()
        view.addSubview(confirmButton)
        return view
    }()
    
    private lazy var contentView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.addArrangedSubview(feedbackView)
        stack.addArrangedSubview(buttonView)
        return stack
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.setupView()
        navigationController?.navigationBar.isHidden = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func buildViewHierarchy() {
        view.addSubview(contentView)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.top.equalTo(feedbackView).offset(Spacing.base04)
            $0.centerX.equalTo(feedbackView)
            $0.size.equalTo(Layout.Image.solicitaionSize)
        }

        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base03)
            $0.leading.equalTo(feedbackView).offset(Spacing.base05)
            $0.trailing.equalTo(feedbackView).offset(-Spacing.base05)
        }

        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalTo(feedbackView).offset(Spacing.base02)
            $0.trailing.equalTo(feedbackView).offset(-Spacing.base02)
            $0.bottom.lessThanOrEqualTo(feedbackView).offset(-Spacing.base02)
        }

        contentView.snp.makeConstraints {
            $0.leading.equalTo(view).offset(Spacing.base02)
            $0.trailing.equalTo(view).offset(-Spacing.base02)
            $0.top.equalTo(view).offset(Spacing.base02)
            $0.bottom.equalTo(view).offset(-Spacing.base02)
        }

        confirmButton.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(buttonView).offset(Spacing.base03)
            $0.height.equalTo(Layout.Button.height)
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.bottom.equalToSuperview().offset(-Spacing.base03)
        }
    }
    
    @objc
    func didTapConfirmButton() {
        viewModel.didNextStep()
    }
}

extension SolicitationSuccessViewController: SolicitationSuccessDisplay {
    func setView(imageAsset: ImageAsset, title: String, description: String, buttonTitle: String, state: SuccessFeedbackState) {
        imageView.image = imageAsset.image
        titleLabel.text = title
        subtitleLabel.text = description
        confirmButton.setTitle(buttonTitle, for: .normal)
        
        imageView.snp.updateConstraints {
            guard state == .solicitation else {
                $0.size.equalTo(Layout.Image.successSize)
                return
            }
            
            $0.size.equalTo(Layout.Image.solicitaionSize)
        }
    }
}
