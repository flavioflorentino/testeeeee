import Foundation

enum SuccessFeedbackState {
    case solicitation
    case activation
}

protocol SolicitationSuccessPresenting: AnyObject {
    var viewController: SolicitationSuccessDisplay? { get set }
    func didNextStep(action: SolicitationSuccessAction)
    func setupSolicitation()
    func setupActivation()
}

final class SolicitationSuccessPresenter: SolicitationSuccessPresenting {
    private typealias Localizable = Strings.MaterialSolicitation
    
    private let coordinator: SolicitationSuccessCoordinating
    weak var viewController: SolicitationSuccessDisplay?
    
    init(coordinator: SolicitationSuccessCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: SolicitationSuccessAction) {
        coordinator.perform(action: action)
    }
    
    func setupSolicitation() {
        viewController?.setView(
            imageAsset: Assets.imgMaterialSolicitation,
            title: Localizable.materialSolicitationSuccessTitle,
            description: Localizable.materialSolicitationSuccessSubtitle,
            buttonTitle: Localizable.solicitationSuccessButton,
            state: .solicitation
        )
    }
    
    func setupActivation() {
        viewController?.setView(
            imageAsset: Assets.imgMaterialActivationSuccess,
            title: Localizable.activationSuccessTitle,
            description: Localizable.activationSuccessSubtitle,
            buttonTitle: Localizable.activationSuccessButton,
            state: .activation
        )
    }
}
