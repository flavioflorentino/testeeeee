import UIKit

enum SolicitationSuccessAction {
    case deliveryForecast
    case rootScreen
}

protocol SolicitationSuccessCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: SolicitationSuccessAction)
}

final class SolicitationSuccessCoordinator: SolicitationSuccessCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: SolicitationSuccessAction) {
        switch action {
        case .deliveryForecast:
            let controller = DeliveryDetailFactory.make(state: .deliveryForecast)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .rootScreen:
            viewController?.dismiss(animated: true) {
                self.viewController?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}
