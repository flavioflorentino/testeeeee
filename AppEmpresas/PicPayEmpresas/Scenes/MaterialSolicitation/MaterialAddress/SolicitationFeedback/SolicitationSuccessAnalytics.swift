struct MaterialSolicitationSuccessAnalytics: MaterialRequestAnalyticsProtocol {
    var screenName: String? {
        "SOLICITACAO_DE_MATERIAL_SUCESSO"
    }
    
    var buttonName: String? {
        "ACOMPANHAR_MATERIAL"
    }
}

struct MaterialActivationSuccessAnalytics: MaterialRequestAnalyticsProtocol {
    var screenName: String? {
        "ATIVACAO_ENTREGA_DE_MATERIAIS_COMPLETA"
    }
}
