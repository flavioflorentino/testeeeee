import Foundation

enum SolicitationSuccessFactory {
    static func make(state: SuccessFeedbackState) -> SolicitationSuccessViewController {
        let coordinator: SolicitationSuccessCoordinating = SolicitationSuccessCoordinator()
        let presenter: SolicitationSuccessPresenting = SolicitationSuccessPresenter(coordinator: coordinator)
        let viewModel = SolicitationSuccessViewModel(presenter: presenter, state: state)
        let viewController = SolicitationSuccessViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
