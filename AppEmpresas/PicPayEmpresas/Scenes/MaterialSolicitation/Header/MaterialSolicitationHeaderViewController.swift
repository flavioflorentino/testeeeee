import UI
import UIKit

protocol MaterialSolicitationHeaderDelegate: AnyObject {
    func updateTable()
}

protocol MaterialSolicitationHeaderDisplay: AnyObject {
    func insertHeader(material: MaterialSolicitationHeaderView)
    func insertHeader(print: QRCodePrintView)
}

private extension MaterialSolicitationHeaderViewController.Layout {
    enum Size {
        static let heightHeader: CGFloat = 158
    }
}

final class MaterialSolicitationHeaderViewController: ViewController<MaterialSolicitationHeaderViewModelInputs, UIView> {
    typealias Localizable = Strings.MaterialSolicitation
    fileprivate enum Layout {}
    
    private lazy var container = UIView()
    weak var delegate: MaterialSolicitationHeaderDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.isCheckShowStatus()
    }

    override func buildViewHierarchy() {
        view.addSubview(container)
    }
    
    override func setupConstraints() {
        container.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    private func addHeaderView(view: UIView) {
        container.addSubview(view)
        view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        delegate?.updateTable()
    }
}

@objc
extension MaterialSolicitationHeaderViewController {
    private func confirmMaterialAction() {
        viewModel.materialSolicitation()
    }
    
    private func confirmePrintAction() {
        viewModel.printMaterialSolicitation()
    }
}

// MARK: MaterialSolicitationHeaderDisplay
extension MaterialSolicitationHeaderViewController: MaterialSolicitationHeaderDisplay {
    func insertHeader(material: MaterialSolicitationHeaderView) {
        material.delegate = self
        addHeaderView(view: material)
    }
    
    func insertHeader(print: QRCodePrintView) {
        print.delegate = self
        addHeaderView(view: print)
    }
}

extension MaterialSolicitationHeaderViewController: MaterialSolicitationHeaderViewDelegate {
    func confirmButtonAction() {
        viewModel.materialSolicitation()
    }
}

extension MaterialSolicitationHeaderViewController: QRCodePrintViewActionDelegate {
    func confirmPrintButtonAction() {
        viewModel.printMaterialSolicitation()
    }
}
