import Foundation
import AnalyticsModule

protocol MaterialSolicitationHeaderViewModelInputs: AnyObject {
    func materialSolicitation()
    func printMaterialSolicitation()
    func isCheckShowStatus()
}

final class MaterialSolicitationHeaderViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let presenter: MaterialSolicitationHeaderPresenting
    private let model: UserStatus

    init(presenter: MaterialSolicitationHeaderPresenting, model: UserStatus, dependencies: Dependencies) {
        self.presenter = presenter
        self.model = model
        self.dependencies = dependencies
    }
}

// MARK: - MaterialSolicitationHeaderViewModelInputs
extension MaterialSolicitationHeaderViewModel: MaterialSolicitationHeaderViewModelInputs {
    func materialSolicitation() {
        presenter.didNextStep(action: .optinMaterialSolicitation)
        dependencies.analytics.log(MaterialRequestAnalytics.oldHeaderEvent)
    }
    
    func printMaterialSolicitation() {
        presenter.didNextStep(action: .printMaterialSolicitation)
    }
    
    func isCheckShowStatus() {
        guard
            let step = model.boardStep,
            let state = MaterialFlowStep(rawValue: step) else {
            return
        }
        
        presenter.controlViewLoad(state: state)
    }
}
