import Foundation

protocol MaterialSolicitationHeaderPresenting: AnyObject {
    var viewController: MaterialSolicitationHeaderDisplay? { get set }
    func didNextStep(action: MaterialSolicitationHeaderAction)
    func controlViewLoad(state: MaterialFlowStep)
}

final class MaterialSolicitationHeaderPresenter: MaterialSolicitationHeaderPresenting {
    private typealias Localizable = Strings.MaterialSolicitation
    private let coordinator: MaterialSolicitationHeaderCoordinating
    weak var viewController: MaterialSolicitationHeaderDisplay?

    init(coordinator: MaterialSolicitationHeaderCoordinating) {
        self.coordinator = coordinator
    }
    
    private func insertMaterialHeader(title: String, description: String, buttonTitle: String) {
        let materialView = MaterialSolicitationHeaderView()
        materialView.setup(
            title: title,
            description: description,
            buttonTitle: buttonTitle
        )
        viewController?.insertHeader(material: materialView)
    }
}

// MARK: - MaterialSolicitationHeaderPresenting
extension MaterialSolicitationHeaderPresenter {
    func didNextStep(action: MaterialSolicitationHeaderAction) {
        coordinator.perform(action: action)
    }
    
    func controlViewLoad(state: MaterialFlowStep) {
        switch state {
        case .solicitation:
            insertMaterialHeader(
                title: Localizable.headerTitleSolicitation,
                description: Localizable.headerDescriptionSolicitation,
                buttonTitle: Localizable.headerConfirmButtonSolicitation
            )
        case .activation:
            insertMaterialHeader(
                title: Localizable.headerTitleActivation,
                description: Localizable.headerDescriptionActivation,
                buttonTitle: Localizable.headerConfirmButtonActivation
            )
        case .brCode:
            insertMaterialHeader(
                title: Localizable.headerTitleUpdate,
                description: Localizable.headerDescriptionUpdate,
                buttonTitle: Localizable.headerConfirmButtonUpdate
            )
        case .thirdSolicitation:
            let printView = QRCodePrintView()
            viewController?.insertHeader(print: printView)
        default:
            break
        }
    }
}
