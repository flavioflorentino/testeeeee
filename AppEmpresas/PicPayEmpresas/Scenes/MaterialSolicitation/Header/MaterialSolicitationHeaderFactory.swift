import Foundation

enum MaterialSolicitationHeaderFactory {
    static func make(status: UserStatus) -> MaterialSolicitationHeaderViewController {
        let container = DependencyContainer()
        let coordinator: MaterialSolicitationHeaderCoordinating = MaterialSolicitationHeaderCoordinator()
        let presenter: MaterialSolicitationHeaderPresenting = MaterialSolicitationHeaderPresenter(coordinator: coordinator)
        let viewModel = MaterialSolicitationHeaderViewModel(presenter: presenter, model: status, dependencies: container)
        let viewController = MaterialSolicitationHeaderViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
