import AssetsKit
import UI
import UIKit

protocol MaterialSolicitationHeaderViewDelegate: AnyObject {
    func confirmButtonAction()
}

extension MaterialSolicitationHeaderView.Layout {
    enum SlabImage {
        static let size = CGSize(width: 96, height: 110)
    }

    enum BoxView {
        static let shadowRadius: CGFloat = 2
        static let shadowOpacity: Float = 1
        static let borderWidth: CGFloat = 0.5
    }
}

final class MaterialSolicitationHeaderView: UIView, ViewConfiguration {
    typealias Localizable = Strings.MaterialSolicitation
    fileprivate enum Layout {}
    
    private lazy var boxView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.white.color
        view.layer.cornerRadius = CornerRadius.medium
        view.layer.shadowColor = Colors.grayscale100.color.cgColor
        view.layer.shadowOpacity = Layout.BoxView.shadowOpacity
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = Layout.BoxView.shadowRadius
        
        view.layer.borderWidth = Layout.BoxView.borderWidth
        view.layer.borderColor = Colors.grayscale050.color.cgColor
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()

    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle())
            .with(\.textColor, (color: .branding600(), state: .normal))
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector(confirmAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var slabImageView = UIImageView(image: Resources.Illustrations.iluSign.image)
    
    weak var delegate: MaterialSolicitationHeaderViewDelegate?
    
    // MARK: Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        boxView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
        }

        titleLabel.snp.makeConstraints {
            $0.top.leading.equalTo(boxView).inset(Spacing.base02)
            $0.trailing.equalTo(slabImageView.snp.leading).offset(-Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(boxView).offset(Spacing.base02)
            $0.trailing.equalTo(slabImageView.snp.leading).offset(-Spacing.base03)
        }
        
        confirmButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalTo(boxView).offset(Spacing.base02)
            $0.trailing.equalTo(slabImageView.snp.leading).offset(-Spacing.base02)
            $0.bottom.equalTo(boxView).offset(-Spacing.base02)
        }
        
        slabImageView.snp.makeConstraints {
            $0.centerY.equalTo(boxView)
            $0.trailing.equalTo(boxView).offset(-Spacing.base02)
            $0.size.equalTo(Layout.SlabImage.size)
        }
    }
    
    func buildViewHierarchy() {
        addSubview(boxView)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        addSubview(confirmButton)
        addSubview(slabImageView)
    }
    
    func setup(title: String, description: String, buttonTitle: String) {
        titleLabel.text = title
        descriptionLabel.text = description
        confirmButton.setTitle(buttonTitle, for: .normal)
    }
}

@objc
extension MaterialSolicitationHeaderView {
    func confirmAction() {
        delegate?.confirmButtonAction()
    }
}
