import UIKit

enum MaterialSolicitationHeaderAction {
    case optinMaterialSolicitation
    case printMaterialSolicitation
}

protocol MaterialSolicitationHeaderCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: MaterialSolicitationHeaderAction)
}

final class MaterialSolicitationHeaderCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - MaterialSolicitationHeaderCoordinating
extension MaterialSolicitationHeaderCoordinator: MaterialSolicitationHeaderCoordinating {
    func perform(action: MaterialSolicitationHeaderAction) {
        switch action {
        case .optinMaterialSolicitation:
            let controller = MaterialFlowControlFactory.make()
            let navigationController = UINavigationController(rootViewController: controller)
            viewController?.present(navigationController, animated: true)
        case .printMaterialSolicitation:
            let controller = ChooseQRCodePrintFactory.make()
            viewController?.pushViewController(controller)
        }
    }
}
