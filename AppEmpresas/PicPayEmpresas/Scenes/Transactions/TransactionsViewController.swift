import UI
import UIKit

protocol TransactionsDisplaying: AnyObject {
    func setupPicPayTransactions(viewController: PicPayTransactionsViewController)
    func setupPixTransactions(viewController: PixTransactionsViewController)
    func setupLinkTransactions(viewController: LinkTransactionsViewController)
    func setupSegmentedControl(_ tabs: [String])
    func setupExternalLink()
    func displayTitle()
    func reloadPicPayTransactions()
    func reloadPixTransactions()
    func reloadLinkTransactions()
}

private extension TransactionsViewController.Layout {
    enum SegmentedControl {
        static let height: CGFloat = 50.0
    }

    enum ActionSheet {
        static let frameOrigin = CGPoint(x: 0.0, y: 10.0)
        static let frameHeight: CGFloat = 60.0
        static let frameWidthOffset: CGFloat = 20.0
        static let cornerRadius: CGFloat = 10.0
    }
}

final class TransactionsViewController: ViewController<TransactionsInteracting, UIView> {
    fileprivate enum Layout { }

    private typealias Localizable = Strings.Home.Transaction
    
    private lazy var transactionsTitle: UILabel = {
        let label = UILabel()
        label.text = Localizable.Header.title
        label.labelStyle(TitleLabelStyle(type: .small)).with(\.textColor, Colors.black.color)
        label.isHidden = true
        return label
    }()
    
    private lazy var segmentedControl: CustomSegmentedControl = {
        let view = CustomSegmentedControl()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = Colors.grayscale500.color
        view.selectorViewColor = Colors.branding600.color
        view.selectorTextColor = Colors.branding600.color
        view.addTarget(self, action: #selector(segmentControlValueChanged), for: .valueChanged)
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = Colors.backgroundPrimary.color
        scrollView.isPagingEnabled = true
        scrollView.delegate = self
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var stackViewContentView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [transactionsTitle, segmentedControl])
        stackView.axis = .vertical
        return stackView
    }()
    
    private var userListViewController: PicPayTransactionsViewController?
    
    private var pixListViewController: PixTransactionsViewController?
    
    private var linkListViewController: LinkTransactionsViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setupHeader()
        interactor.setupTransactions()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(stackView)
        view.addSubview(scrollView)
        scrollView.addSubview(stackViewContentView)
    }
    
    override func setupConstraints() {
        segmentedControl.snp.makeConstraints {
            $0.height.equalTo(Layout.SegmentedControl.height)
        }
        
        stackView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview()
        }
        
        scrollView.snp.makeConstraints {
            $0.top.equalTo(stackView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
        
        stackViewContentView.snp.makeConstraints {
            $0.edges.height.equalToSuperview()
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

extension TransactionsViewController: TransactionsDisplaying {
    func setupPicPayTransactions(viewController: PicPayTransactionsViewController) {
        userListViewController = viewController
        addChild(viewController)
        stackViewContentView.addArrangedSubview(viewController.view)
        viewController.didMove(toParent: self)
        
        viewController.view.snp.makeConstraints {
            $0.height.width.equalTo(scrollView)
        }
    }
    
    func setupPixTransactions(viewController: PixTransactionsViewController) {
        pixListViewController = viewController
        addChild(viewController)
        stackViewContentView.addArrangedSubview(viewController.view)
        viewController.didMove(toParent: self)
        
        viewController.view.snp.makeConstraints {
            $0.height.width.equalTo(scrollView)
        }
    }
    
    func setupLinkTransactions(viewController: LinkTransactionsViewController) {
        linkListViewController = viewController
        addChild(viewController)
        stackViewContentView.addArrangedSubview(viewController.view)
        viewController.didMove(toParent: self)
        
        viewController.view.snp.makeConstraints {
            $0.height.width.equalTo(scrollView)
        }
    }
    
    func setupSegmentedControl(_ tabs: [String]) {
        segmentedControl.setButtonTitles(buttonTitles: tabs)
    }
    
    func setupExternalLink() {
        segmentedControl.buttonStyle = Typography.bodySecondary()
        segmentedControl.buttonDistribution = .fillEqually
        segmentedControl.showSeparator = true
    }
    
    func displayTitle() {
        transactionsTitle.isHidden = false
    }
    
    func reloadPicPayTransactions() {
        userListViewController?.reloadTransactions()
    }
    
    func reloadPixTransactions() {
        pixListViewController?.reloadTransactions()
    }
    
    func reloadLinkTransactions() {
        linkListViewController?.reloadTransactions()
    }
}

@objc
extension TransactionsViewController {
    func segmentControlValueChanged() {
        let selectedIndex = CGFloat(segmentedControl.selectedIndex)
        let contentOffset = CGPoint(x: selectedIndex * scrollView.bounds.width, y: 0)
        scrollView.setContentOffset(contentOffset, animated: true)
    }
}

extension TransactionsViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard scrollView == self.scrollView else { return }
        
        let pagesOffset = Array(stride(from: 0, to: scrollView.contentSize.width, by: scrollView.bounds.width))
        if let index = pagesOffset.firstIndex(of: scrollView.contentOffset.x) {
            segmentedControl.setIndex(index: Int(index))
        }
    }
}

extension TransactionsViewController: TransactionsErrorViewDelegate {
    func didTapTryAgain() {
        reload()
    }
}

extension TransactionsViewController: HomeLoadable {
    func reload() {
        interactor.reload(transactionTab: segmentedControl.selectedIndex)
    }
}
