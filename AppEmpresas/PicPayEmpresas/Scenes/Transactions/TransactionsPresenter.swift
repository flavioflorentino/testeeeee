import Foundation

protocol TransactionsPresenting: AnyObject {
    var viewController: TransactionsDisplaying? { get set }
    
    func setupPicPayTransactions(seller: Seller)
    func setupPixTransactions(seller: Seller)
    func setupLinkTransactions(seller: Seller)
    func setupNewSegmentedControl()
    func presentTitle()
    func presentSegmentedControl(_ tabs: [TransactionTabOption])
    func reloadPicPayTransactions()
    func reloadPixTransactions()
    func reloadLinkTransactions()
}

enum TransactionsListType {
    case compact
    case full
}

final class TransactionsPresenter {
    weak var viewController: TransactionsDisplaying?
    private let type: TransactionsListType
    
    private var picPayTransactions: PicPayTransactionsViewController?
    private var pixTransactions: PixTransactionsViewController?
    private var linkTransactions: LinkTransactionsViewController?
    
    init(type: TransactionsListType) {
        self.type = type
    }
}

extension TransactionsPresenter: TransactionsPresenting {
    func setupPicPayTransactions(seller: Seller) {
        guard picPayTransactions == nil else { return }
        let picPayTransactions = PicPayTransactionsFactory.make(seller: seller, type: type)
        self.picPayTransactions = picPayTransactions
        viewController?.setupPicPayTransactions(viewController: picPayTransactions)
    }
    
    func setupPixTransactions(seller: Seller) {
        guard pixTransactions == nil else { return }
        let pixTransactions = PixTransactionsFactory.make(seller: seller, type: type)
        self.pixTransactions = pixTransactions
        viewController?.setupPixTransactions(viewController: pixTransactions)
    }
    
    func setupLinkTransactions(seller: Seller) {
        guard linkTransactions == nil else { return }
        let linkTransactions = LinkTransactionsFactory.make(seller: seller, type: type)
        self.linkTransactions = linkTransactions
        viewController?.setupLinkTransactions(viewController: linkTransactions)
    }
    
    func setupNewSegmentedControl() {
        viewController?.setupExternalLink()
    }
    
    func presentTitle() {
        viewController?.displayTitle()
    }
    
    func presentSegmentedControl(_ tabs: [TransactionTabOption]) {
        viewController?.setupSegmentedControl(tabs.map { $0.description })
    }
    
    func reloadPicPayTransactions() {
        viewController?.reloadPicPayTransactions()
    }
    
    func reloadPixTransactions() {
        viewController?.reloadPixTransactions()
    }
    
    func reloadLinkTransactions() {
        viewController?.reloadLinkTransactions()
    }
}
