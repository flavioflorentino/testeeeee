import UIKit

enum TransactionsFactory {
    static func make(seller: Seller, type: TransactionsListType) -> TransactionsViewController {
        let container = DependencyContainer()
        let presenter: TransactionsPresenting = TransactionsPresenter(type: type)
        let interactor = TransactionsInteractor(presenter: presenter, dependencies: container, seller: seller)
        let viewController = TransactionsViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
