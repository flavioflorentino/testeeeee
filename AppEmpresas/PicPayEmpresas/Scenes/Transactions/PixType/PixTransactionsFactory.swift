import Foundation

enum PixTransactionsFactory {
    static func make(seller: Seller, type: TransactionsListType) -> PixTransactionsViewController {
        let container = DependencyContainer()
        let service: PixTransactionsServicing = PixTransactionsService(dependencies: container)
        let coordinator: PixTransactionsCoordinating = PixTransactionsCoordinator(dependencies: container)
        let presenter: PixTransactionsPresenting = PixTransactionsPresenter(coordinator: coordinator)
        let interactor = PixTransactionsInteractor(seller: seller, service: service, presenter: presenter, dependencies: container, type: type)
        let viewController = PixTransactionsViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
