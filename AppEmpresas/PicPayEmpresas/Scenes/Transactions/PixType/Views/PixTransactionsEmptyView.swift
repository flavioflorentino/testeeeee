import AssetsKit
import Foundation
import SnapKit
import UI
import UIKit

protocol PixTransactionsEmptyViewDelegate: AnyObject {
    func didTapPixKeyRegistration()
}

private extension PixTransactionsEmptyView.Layout {
    enum ImageView {
        static let size = CGSize(width: 152, height: 139)
    }
}

final class PixTransactionsEmptyView: UIView {
    private typealias Localizable = Strings.Home.Transaction.EmptyView.Pix
    fileprivate enum Layout { }
    
    private lazy var imageView = UIImageView(image: Resources.Illustrations.iluGirlPix.image)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.black.color)
        label.text = Localizable.title
        return label
    }()

    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale600.color)
        let attrText = NSMutableAttributedString()
        attrText.append(createDefaultText(Localizable.subtitle))
        attrText.append(createUnderlineText(Localizable.keyRegistration))
        label.attributedText = attrText
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapPixLabel))
        label.addGestureRecognizer(tapGesture)
        label.isUserInteractionEnabled = true
        return label
    }()

    weak var delegate: PixTransactionsEmptyViewDelegate?
    
    init(delegate: PixTransactionsEmptyViewDelegate?) {
        self.delegate = delegate
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PixTransactionsEmptyView: ViewConfiguration {
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    func buildViewHierarchy() {
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.ImageView.size)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base04)
        }
    }
}

private extension PixTransactionsEmptyView {
    func createDefaultText(_ text: String) -> NSAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodySecondary().font(),
            .foregroundColor: Colors.black.color
        ]
        
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    func createUnderlineText(_ text: String) -> NSAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodySecondary().font(),
            .foregroundColor: Colors.branding600.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        return NSAttributedString(string: text, attributes: attributes)
    }

    @objc
    func didTapPixLabel() {
        delegate?.didTapPixKeyRegistration()
    }
}
