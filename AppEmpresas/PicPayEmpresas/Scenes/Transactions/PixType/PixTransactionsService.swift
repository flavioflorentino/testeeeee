import Core
import Foundation
import LegacyPJ

protocol PixTransactionsServicing {
    func fetchTransactions(page: Int, completion: @escaping (Result<TransactionListResponse, LegacyPJError>) -> Void)
}

final class PixTransactionsService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    private let apiTransaction: ApiTransaction

    init(dependencies: Dependencies, apiTransaction: ApiTransaction = ApiTransaction()) {
        self.dependencies = dependencies
        self.apiTransaction = apiTransaction
    }
}

// MARK: - HomeTransactionServicing
extension PixTransactionsService: PixTransactionsServicing {
    func fetchTransactions(page: Int = 0, completion: @escaping (Result<TransactionListResponse, LegacyPJError>) -> Void) {
        apiTransaction.list(page, endpoint: TransactionsEndpoint.pix.absoluteStringUrl) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let response):
                    completion(.success(response))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
