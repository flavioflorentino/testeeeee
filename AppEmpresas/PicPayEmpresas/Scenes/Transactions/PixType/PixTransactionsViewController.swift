import LegacyPJ
import UI
import UIKit

protocol PixTransactionsDisplaying: AnyObject {
    func enableScroll()
    func setupTransactionsListView()
    func display(transactions: [TransactionViewModel])
    
    func setupButton()
    
    func startLoading()
    func stopLoading()
    
    func displayEmptyView()
    func displayErrorView()
    func resetViews()
    
    func displayAlert(for transaction: Transaction, indexPath: IndexPath)
}

private extension PixTransactionsViewController.Layout {
    enum ActionSheet {
        static let frameOrigin = CGPoint(x: 0.0, y: 10.0)
        static let frameHeight: CGFloat = 60.0
        static let frameWidthOffset: CGFloat = 20.0
        static let cornerRadius: CGFloat = 10.0
    }
}

final class PixTransactionsViewController: ViewController<PixTransactionsInteracting, UIView> {
    private typealias Localizable = Strings.Home.Transaction
    fileprivate enum Layout { }
    
    private lazy var loadingView: UIActivityIndicatorView = {
        if #available(iOS 13.0, *) {
            return UIActivityIndicatorView(style: .large)
        }
        return UIActivityIndicatorView(style: .gray)
    }()
    
    private lazy var transactionsViewController = TransactionsListView(delegate: self)
    
    private lazy var showAllTransactionsButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.Button.title, for: .normal)
        button.buttonStyle(SecondaryButtonStyle())
        button.addTarget(self, action: #selector(buttonTouched), for: .touchUpInside)
        return button
    }()
    
    private lazy var errorView = TransactionsErrorView(delegate: self)
    private lazy var emptyView = PixTransactionsEmptyView(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.initialFetch()
        interactor.setupPaginationIfNeeded()
    }
    
    func reloadTransactions() {
        interactor.initialFetch()
    }
}

// MARK: - Button Methods
@objc
private extension PixTransactionsViewController {
    func buttonTouched() {
        interactor.didNextStep()
    }
}

// MARK: - PixTransactionsDisplaying
extension PixTransactionsViewController: PixTransactionsDisplaying {
    func enableScroll() {
        transactionsViewController.enableScroll()
    }
    
    func display(transactions: [TransactionViewModel]) {
        transactionsViewController.display(transactions: transactions)
    }
    
    func setupTransactionsListView() {
        addSubViewInTab(with: transactionsViewController)
        setupTransactionsConstraints()
    }
    
    func setupButton() {
        view.addSubview(showAllTransactionsButton)
        view.bringSubviewToFront(showAllTransactionsButton)
        setupButtonConstraints()
    }
    
    func startLoading() {
        view.addSubview(loadingView)
        setupLoadingViewConstraints()
        loadingView.startAnimating()
    }
    
    func stopLoading() {
        loadingView.stopAnimating()
        removeView(loadingView)
    }
    
    func displayEmptyView() {
        view.addSubview(emptyView)
        setupEmptyViewConstraints()
    }

    func displayErrorView() {
        view.addSubview(errorView)
        setupErrorViewConstraints()
    }
    
    func resetViews() {
        removeView(loadingView)
        removeViewController(transactionsViewController)
        removeView(showAllTransactionsButton)
        removeView(emptyView)
        removeView(errorView)
    }
    
    func displayAlert(for transaction: Transaction, indexPath: IndexPath) {
        // todo - refatorar usando componentes do UI asap
        let alert = UIAlertController(title: "\n\n\n", message: nil, preferredStyle: .actionSheet)

        let size = CGSize(
            width: view.frame.size.width - Layout.ActionSheet.frameWidthOffset,
            height: Layout.ActionSheet.frameHeight
        )
        let frame = CGRect(origin: Layout.ActionSheet.frameOrigin, size: size)
        let transactionView = TransactionView(frame: frame)
        transactionView.backgroundColor = .clear
        transactionView.layer.cornerRadius = Layout.ActionSheet.cornerRadius
        alert.view.addSubview(transactionView)
        transactionView.configure(transaction)

        createActions(for: transaction, indexPath: indexPath).forEach({ alert.addAction($0) })

        present(alert, animated: true)
        alert.view.setNeedsLayout()
    }
}

// MARK: - Home Transaction List Delegate
extension PixTransactionsViewController: TransactionsListViewDelegate {
    func fetchMoreItemsIfNeeded(indexPath: IndexPath) {
        interactor.fetchMoreItemsIfNeeded(indexPath: indexPath)
    }
    
    func didSelect(at indexPath: IndexPath) {
        interactor.didSelect(at: indexPath)
    }
}

// MARK: - Try Again
extension PixTransactionsViewController: TransactionsErrorViewDelegate {
    func didTapTryAgain() {
        interactor.initialFetch()
    }
}

// MARK: - Pix Key Registration
extension PixTransactionsViewController: PixTransactionsEmptyViewDelegate {
    func didTapPixKeyRegistration() {
        interactor.showPixHub()
    }
}

private extension PixTransactionsViewController {
    func addSubViewInTab(with viewController: UIViewController) {
        removeViewController(viewController)
        addChild(viewController)
        view.addSubview(viewController.view)
        view.bringSubviewToFront(viewController.view)
        viewController.didMove(toParent: self)
    }
    
    func removeViewController(_ viewController: UIViewController?) {
        guard let viewController = viewController else { return }
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    func removeView(_ viewCustom: UIView?) {
        guard let viewCustom = viewCustom else { return }
        viewCustom.removeFromSuperview()
    }
    
    func setupLoadingViewConstraints() {
        loadingView.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
            $0.top.leading.greaterThanOrEqualToSuperview().inset(Spacing.base02)
            $0.trailing.bottom.lessThanOrEqualToSuperview().inset(Spacing.base02)
        }
    }
    
    func setupTransactionsConstraints() {
        transactionsViewController.view.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
    }
    
    func setupButtonConstraints() {
        showAllTransactionsButton.snp.makeConstraints {
            $0.top.equalTo(transactionsViewController.view.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }
    
    func setupEmptyViewConstraints() {
        emptyView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func setupErrorViewConstraints() {
        errorView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func createActions(for transaction: Transaction, indexPath: IndexPath) -> [UIAlertAction] {
        var actions: [UIAlertAction] = []
        actions.append(contentsOf: pixActions(for: transaction))
        actions.append(UIAlertAction(
            title: Strings.Default.cancel,
            style: .cancel
        ))
        return actions
    }
    
    func pixActions(for transaction: Transaction) -> [UIAlertAction] {
        var actions: [UIAlertAction] = []
        
        actions.append(UIAlertAction(
            title: Strings.Transaction.Alert.Button.receipt,
            style: .default,
            handler: { _ in
                self.interactor.openPixReceipt(transaction)
            })
        )

        if transaction.status != .canceled {
            actions.append(UIAlertAction(
                title: Strings.Transaction.Alert.Button.returnPix,
                style: .destructive,
                handler: { _ in
                    self.interactor.openPixPayment(transaction)
                })
            )
        }

        return actions
    }
}
