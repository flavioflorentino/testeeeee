import AnalyticsModule
import Core
import Foundation
import LegacyPJ

protocol PixTransactionsInteracting: AnyObject {
    func initialFetch()
    func fetchTransactions(at page: Int?)
    func fetchMoreItemsIfNeeded(indexPath: IndexPath)
    func setupPaginationIfNeeded()
    func didSelect(at indexPath: IndexPath)
    func showPixHub()
    func openPixReceipt(_ transaction: Transaction)
    func openPixPayment(_ transaction: Transaction)
    func didNextStep()
}

final class PixTransactionsInteractor {
    typealias Dependencies = HasAnalytics & HasKVStore
    private let dependencies: Dependencies
    
    private let service: PixTransactionsServicing
    private let presenter: PixTransactionsPresenting
    
    private let seller: Seller
    
    private let compactModeMaxTransactions = 5
    private var type: TransactionsListType
    private var page: Int?
    private var hasNextPage = false
    private var isLoading = false
    private var transactions: [Transaction] = []
    
    init(seller: Seller, service: PixTransactionsServicing, presenter: PixTransactionsPresenting, dependencies: Dependencies, type: TransactionsListType) {
        self.seller = seller
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.type = type
    }
}

// MARK: - PixTransactionsInteracting
extension PixTransactionsInteractor: PixTransactionsInteracting {
    func setupPaginationIfNeeded() {
        guard type == .full else { return }
        presenter.setupPagination()
    }
    
    func initialFetch() {
        fetchTransactions(at: page)
    }
    
    func fetchMoreItemsIfNeeded(indexPath: IndexPath) {
        guard type == .full, hasNextPage, indexPath.row == (transactions.count - 1) else { return }
        
        fetchTransactions(at: page)
    }
    
    func fetchTransactions(at page: Int?) {
        guard !isLoading else { return }
        
        isLoading = true
        presenter.showLoading()
        service.fetchTransactions(page: page ?? 0) { [weak self] result in
            guard let self = self else { return }
            
            self.isLoading = false
            
            self.presenter.hideLoading()
            switch result {
            case .success(let response) where response.list.isEmpty:
                self.presenter.presentEmptyState()
            case .success(let response):
                if self.type == .full {
                    self.transactions += response.list
                } else {
                    self.transactions = Array(response.list.prefix(self.compactModeMaxTransactions))
                }
                
                self.hasNextPage = response.pagination.hasNext
                self.page = response.pagination.nextPage
                
                self.presenter.present(
                    transactions: self.transactions,
                    showButton: self.type == .compact && response.list.count > self.compactModeMaxTransactions
                )
            case .failure:
                self.presenter.presentErrorState()
            }
        }
    }
    
    func didSelect(at indexPath: IndexPath) {
        let transaction = transactions[indexPath.row]
        
        dependencies.analytics.log(TransactionAnalytics.transactionDetails(transaction))
        presenter.presentAlert(for: transaction, indexPath: indexPath)
    }
    
    func showPixHub() {
        let welcomePresented = dependencies.kvStore.boolFor(KVKey.isPixKeyManagementWelcomeVisualized)
        let pixAvailablePresented = dependencies.kvStore.boolFor(KVKey.isPixAvailableFeedbackVisualized)
        presenter.didNextStep(
            action: .pix(seller, welcomePresented: welcomePresented, pixAvailablePresented: pixAvailablePresented)
        )
    }
    
    func openPixReceipt(_ transaction: Transaction) {
        let isRefundAllowed = transaction.status != .canceled
        presenter.didNextStep(action: .presentPixReceipt(transaction.id, isRefundAllowed: isRefundAllowed))
    }
    
    func openPixPayment(_ transaction: Transaction) {
        presenter.didNextStep(action: .returnPixPayment(transaction.id))
    }
    
    func didNextStep() {
        let transactionsDescriptor = TransactionsDescriptor(
            displayHeader: .tabs,
            seller: seller,
            selectedTab: .pixTransactions
        )
        presenter.didNextStep(action: .showAllTransactions(transactionsDescriptor: transactionsDescriptor))
    }
}
