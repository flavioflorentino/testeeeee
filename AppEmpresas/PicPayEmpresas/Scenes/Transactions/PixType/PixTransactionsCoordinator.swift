import PIX
import UIKit
import FeatureFlag
import LegacyPJ

enum PixTransactionsAction: Equatable {
    case showAllTransactions(transactionsDescriptor: TransactionsDescriptor)
    case presentPixReceipt(_ transactionId: Int, isRefundAllowed: Bool)
    case returnPixPayment(_ transactionId: Int)
    case pix(_ seller: Seller, welcomePresented: Bool, pixAvailablePresented: Bool)
}

protocol PixTransactionsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PixTransactionsAction)
}

final class PixTransactionsCoordinator {
    typealias Dependencies = HasAuthManager & HasFeatureManager
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - PixTransactionsCoordinating
extension PixTransactionsCoordinator: PixTransactionsCoordinating {
    func perform(action: PixTransactionsAction) {
        switch action {
        case let .showAllTransactions(transactionsDescriptor):
            let controller: UIViewController
            
            if dependencies.featureManager.isActive(.isExternalLinkAvailable) {
                controller = TransactionsFactory.make(seller: transactionsDescriptor.seller, type: .full)
            } else {
                controller = TransactionListContainerFactory.make(transactionsDescriptor)
            }
            
            viewController?.pushViewController(controller)
        case let .presentPixReceipt(id, isRefundAllowed):
            guard let navigationController = viewController?.navigationController else {
                return
            }

            let coordinator = PIXBizFlowCoordinator(with: navigationController)

            coordinator.perform(
                flow: .receipt(transactionId: String(id), isRefundAllowed: isRefundAllowed)
            )
        case .returnPixPayment(let id):
            guard let navigationController = viewController?.navigationController else {
                return
            }

            let coordinator = PIXBizFlowCoordinator(with: navigationController)

            coordinator.perform(
                flow: .cashout(action: .refund(transactionId: String(id)))
            )
        case let .pix(seller, isWelcomePresented, pixAvaiablePresented):
            navigateToPIX(seller: seller, welcomePresented: isWelcomePresented, pixAvaiablePresented: pixAvaiablePresented)
        }
    }
}

private extension PixTransactionsCoordinator {
    func navigateToPIX(seller: Seller, welcomePresented: Bool, pixAvaiablePresented: Bool) {
        guard let navigationController = viewController?.navigationController else {
            return
        }
        let userInfo = KeyManagerBizUserInfo(name: seller.razaoSocial ?? "",
                                             cnpj: seller.cnpj ?? "",
                                             mail: seller.email ?? "",
                                             phone: dependencies.authManager.user?.phone ?? "",
                                             userId: String(seller.id))
                                             
        let coordinator = PIXBizFlowCoordinator(with: navigationController, userInfo: userInfo)
        
        if dependencies.featureManager.isActive(.releaseHubPixPresenting) {
            coordinator.perform(flow: pixAvaiablePresented ? .hub : .welcomePage)
            return
        }
        coordinator.perform(flow: .optin(action: welcomePresented ? .keyManager(input: .none) : .onboarding))
    }
}
