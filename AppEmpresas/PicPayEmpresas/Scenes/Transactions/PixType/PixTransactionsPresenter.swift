import Foundation
import LegacyPJ

protocol PixTransactionsPresenting: AnyObject {
    var viewController: PixTransactionsDisplaying? { get set }
    func setupPagination()
    func present(transactions: [Transaction], showButton: Bool)
    func presentAlert(for transaction: Transaction, indexPath: IndexPath)
    func showLoading()
    func hideLoading()
    func presentErrorState()
    func presentEmptyState()
    func didNextStep(action: PixTransactionsAction)
}

final class PixTransactionsPresenter {
    private let coordinator: PixTransactionsCoordinating
    weak var viewController: PixTransactionsDisplaying?

    init(coordinator: PixTransactionsCoordinating) {
        self.coordinator = coordinator
    }
}

extension PixTransactionsPresenter: PixTransactionsPresenting {
    func setupPagination() {
        viewController?.enableScroll()
    }
    
    func present(transactions: [Transaction], showButton: Bool) {
        viewController?.resetViews()
        viewController?.setupTransactionsListView()
        viewController?.display(transactions: transactions.map({ TransactionViewModel(with: $0) }))
        
        if showButton {
            viewController?.setupButton()
        }
    }
    
    func presentAlert(for transaction: Transaction, indexPath: IndexPath) {
        viewController?.displayAlert(for: transaction, indexPath: indexPath)
    }
    
    func showLoading() {
        viewController?.resetViews()
        viewController?.startLoading()
    }
    
    func hideLoading() {
        viewController?.stopLoading()
    }
    
    func presentErrorState() {
        viewController?.resetViews()
        viewController?.displayErrorView()
    }
    
    func presentEmptyState() {
        viewController?.resetViews()
        viewController?.displayEmptyView()
    }
    
    func didNextStep(action: PixTransactionsAction) {
        coordinator.perform(action: action)
    }
}
