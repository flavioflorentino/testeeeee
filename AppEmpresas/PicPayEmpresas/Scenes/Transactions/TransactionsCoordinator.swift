import UIKit

enum TransactionsAction {
}

protocol TransactionsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: TransactionsAction)
}

final class TransactionsCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension TransactionsCoordinator: TransactionsCoordinating {
    func perform(action: TransactionsAction) {
    }
}
