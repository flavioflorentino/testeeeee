import AssetsKit
import Foundation
import SnapKit
import UI
import UIKit

private extension PicPayTransactionsEmptyView.Layout {
    enum ImageView {
        static let size = CGSize(width: 152, height: 139)
    }
}

final class PicPayTransactionsEmptyView: UIView {
    private typealias Localizable = Strings.Home.Transaction.EmptyView.User
    fileprivate enum Layout { }
    
    private lazy var imageView = UIImageView(image: Resources.Illustrations.iluGirlCoin.image)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.black.color)
        label.text = Localizable.title
        return label
    }()

    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale600.color)
        label.text = Localizable.subtitle
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PicPayTransactionsEmptyView: ViewConfiguration {
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    func buildViewHierarchy() {
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().offset(Spacing.base04)
            $0.size.equalTo(Layout.ImageView.size)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.bottom.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
}
