import Foundation
import LegacyPJ

protocol PicPayTransactionsPresenting: AnyObject {
    var viewController: PicPayTransactionsDisplaying? { get set }
    
    func present(transactions: [Transaction], showButton: Bool)
    func update(transaction: Transaction)
    
    func setupPagination()
    func willStartPaginating()
    func didEndPaginating()
    func startLoading()
    func stopLoading()
    func startLoading(at indexPath: IndexPath)
    func stopLoading(at indexPath: IndexPath)
    
    func presentEmptyState()
    func presentErrorState()
    func presentErrorPopUp(with error: String)
    func presentAlert(for transaction: Transaction, indexPath: IndexPath)
    
    func didNextStep(action: PicPayTransactionsAction)
}

final class PicPayTransactionsPresenter {
    private let coordinator: PicPayTransactionsCoordinating
    weak var viewController: PicPayTransactionsDisplaying?

    init(coordinator: PicPayTransactionsCoordinating) {
        self.coordinator = coordinator
    }
}

extension PicPayTransactionsPresenter: PicPayTransactionsPresenting {
    func present(transactions: [Transaction], showButton: Bool) {
        viewController?.resetViews()
        viewController?.setupTransactionsListView()
        viewController?.display(transactions: transactions.map { TransactionViewModel(with: $0) })
        
        if showButton {
            viewController?.setupButton()
        }
    }
    
    func update(transaction: Transaction) {
        viewController?.update(transaction: TransactionViewModel(with: transaction))
    }
    
    func setupPagination() {
        viewController?.enableScroll()
    }
    
    func willStartPaginating() {
        viewController?.startPaginatingLoader()
    }
    
    func didEndPaginating() {
        viewController?.stopPaginatingLoader()
    }
    
    func startLoading() {
        viewController?.resetViews()
        viewController?.startLoading()
    }

    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func startLoading(at indexPath: IndexPath) {
        viewController?.startLoading(at: indexPath)
    }

    func stopLoading(at indexPath: IndexPath) {
        viewController?.stopLoading(at: indexPath)
    }
    
    func presentEmptyState() {
        viewController?.resetViews()
        viewController?.displayEmptyView()
    }
    
    func presentErrorState() {
        viewController?.resetViews()
        viewController?.displayErrorView()
    }
    
    func presentErrorPopUp(with error: String) {
        coordinator.perform(action: .modalError(error))
    }
    
    func presentAlert(for transaction: Transaction, indexPath: IndexPath) {
        viewController?.displayAlert(transaction, indexPath: indexPath)
    }
    
    func didNextStep(action: PicPayTransactionsAction) {
        coordinator.perform(action: action)
    }
}
