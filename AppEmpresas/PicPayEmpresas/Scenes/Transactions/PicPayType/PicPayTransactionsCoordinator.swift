import UI
import UIKit
import FeatureFlag

enum PicPayTransactionsAction: Equatable {
    case showAllTransactions(transactionsDescriptor: TransactionsDescriptor)
    case modalError(_ message: String)
}

protocol PicPayTransactionsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PicPayTransactionsAction)
}

final class PicPayTransactionsCoordinator {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    private typealias Localizable = Strings.Transaction.Transactions

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func showErrorPopUp(_ message: String) {
        let action = PopupAction(title: Localizable.Error.action, style: .tryAgain)
        let popUp = UI.PopupViewController(
            title: Localizable.Error.title,
            description: message,
            image: Assets.errorIcon.image
        )

        popUp.hideCloseButton = true
        popUp.addAction(action)
        viewController?.showPopup(popUp)
    }
}

extension PicPayTransactionsCoordinator: PicPayTransactionsCoordinating {
    func perform(action: PicPayTransactionsAction) {
        switch action {
        case let .showAllTransactions(descriptor):
            let controller: UIViewController
            
            if dependencies.featureManager.isActive(.isExternalLinkAvailable) {
                controller = TransactionsFactory.make(seller: descriptor.seller, type: .full)
            } else {
                controller = TransactionListContainerFactory.make(descriptor)
            }
            
            viewController?.pushViewController(controller)
        case let .modalError(message):
            showErrorPopUp(message)
        }
    }
}
