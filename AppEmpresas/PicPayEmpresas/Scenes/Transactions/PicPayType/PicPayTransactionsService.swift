import Core
import Foundation
import LegacyPJ

protocol PicPayTransactionsServicing {
    func fetchTransactions(page: Int, completion: @escaping (Result<TransactionListResponse, LegacyPJError>) -> Void)
    func authenticate(_ completion: @escaping (_ result: AuthManager.PerformActionResult) -> Void)
    func cancelTransaction(_ pin: String, id: Int, completion: @escaping (Result<Void, LegacyPJError>) -> Void)
}

final class PicPayTransactionsService {
    typealias Dependencies = HasMainQueue & HasAuthManager
    private let dependencies: Dependencies
    private let apiTransaction: ApiTransaction

    init(dependencies: Dependencies, apiTransaction: ApiTransaction = ApiTransaction()) {
        self.dependencies = dependencies
        self.apiTransaction = apiTransaction
    }
}

extension PicPayTransactionsService: PicPayTransactionsServicing {
    func fetchTransactions(page: Int = 0, completion: @escaping (Result<TransactionListResponse, LegacyPJError>) -> Void) {
        apiTransaction.list(page, endpoint: TransactionsEndpoint.user.absoluteStringUrl) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let response):
                    completion(.success(response))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }

    func authenticate(_ completion: @escaping (_ result: AuthManager.PerformActionResult) -> Void) {
        dependencies.authManager.performActionWithAuthorization(nil, completion)
    }

    func cancelTransaction(_ pin: String, id: Int, completion: @escaping (Result<Void, LegacyPJError>) -> Void) {
        apiTransaction.cancelTransaction(pin: pin, id: id) { [weak self] error in
            self?.dependencies.mainQueue.async {
                guard let error = error else {
                    completion(.success(()))
                    return
                }
                completion(.failure(error))
            }
        }
    }
}
