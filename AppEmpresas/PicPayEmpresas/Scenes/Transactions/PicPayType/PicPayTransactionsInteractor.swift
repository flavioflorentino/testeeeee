import AnalyticsModule
import Foundation
import LegacyPJ

protocol PicPayTransactionsInteracting: AnyObject {
    func initialFetch()
    func setupPaginationIfNeeded()
    func fetchTransactions(at page: Int?)
    func fetchMoreItemsIfNeeded(indexPath: IndexPath)
    func didSelect(at indexPath: IndexPath)
    func authenticateUser(_ transaction: Transaction, indexPath: IndexPath)
    func didNextStep()
}

final class PicPayTransactionsInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: PicPayTransactionsServicing
    private let presenter: PicPayTransactionsPresenting
    
    private let seller: Seller

    private let compactModeMaxTransactions = 5
    private var type: TransactionsListType
    private var page: Int?
    private var hasNextPage = false
    private var isLoading = false
    private var transactions: [Transaction] = []
    
    init(seller: Seller, service: PicPayTransactionsServicing, presenter: PicPayTransactionsPresenting, dependencies: Dependencies, type: TransactionsListType) {
        self.seller = seller
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.type = type
    }
}

extension PicPayTransactionsInteractor: PicPayTransactionsInteracting {
    func setupPaginationIfNeeded() {
        guard type == .full else { return }
        presenter.setupPagination()
    }
    
    func initialFetch() {
        presenter.startLoading()
        fetchTransactions()
    }
    
    func fetchMoreItemsIfNeeded(indexPath: IndexPath) {
        guard type == .full, hasNextPage, indexPath.row == (transactions.count - 1) else { return }

        presenter.willStartPaginating()
        fetchTransactions(at: page)
    }
    
    func fetchTransactions(at page: Int? = nil) {
        guard !isLoading else {
            stopLoading()
            return
        }
        
        isLoading = true
        
        service.fetchTransactions(page: page ?? 0) { [weak self] result in
            guard let self = self else { return }
            
            self.isLoading = false
            
            self.stopLoading()
            switch result {
            case .success(let response) where response.list.isEmpty:
                self.presenter.presentEmptyState()
            case .success(let response):
                if self.type == .full {
                    self.transactions += response.list
                } else {
                    self.transactions = Array(response.list.prefix(self.compactModeMaxTransactions))
                }
                
                self.hasNextPage = response.pagination.hasNext
                self.page = response.pagination.nextPage
                
                self.presenter.present(
                    transactions: self.transactions,
                    showButton: self.type == .compact && response.list.count > self.compactModeMaxTransactions
                )
            case .failure:
                self.presenter.presentErrorState()
            }
        }
    }
    
    func didSelect(at indexPath: IndexPath) {
        let transaction = transactions[indexPath.row]
        
        dependencies.analytics.log(TransactionAnalytics.transactionDetails(transaction))
        presenter.presentAlert(for: transaction, indexPath: indexPath)
    }
    
    func authenticateUser(_ transaction: Transaction, indexPath: IndexPath) {
        service.authenticate { [weak self] result in
            switch result {
            case .success(let pin):
                self?.cancelTransaction(transaction, pin: pin, indexPath: indexPath)
            case .failure(let error):
                self?.presenter.presentErrorPopUp(with: error.localizedDescription)
            case .canceled:
                break
            }
        }
    }
    
    func didNextStep() {
        let transactionsDescriptor = TransactionsDescriptor(
            displayHeader: .tabs,
            seller: seller,
            selectedTab: .picPayTransactions
        )
        presenter.didNextStep(action: .showAllTransactions(transactionsDescriptor: transactionsDescriptor))
    }
}

private extension PicPayTransactionsInteractor {
    func stopLoading() {
        presenter.didEndPaginating()
        presenter.stopLoading()
    }
    
    func cancelTransaction(_ transaction: Transaction, pin: String, indexPath: IndexPath) {
        presenter.startLoading(at: indexPath)
        service.cancelTransaction(pin, id: transaction.id) { [weak self] result in
            self?.presenter.stopLoading(at: indexPath)
            switch result {
            case .success:
                self?.handleReturnTransactionSuccess(transaction, indexPath: indexPath)
            case .failure(let error):
                self?.presenter.presentErrorPopUp(with: error.localizedDescription)
            }
        }
    }
    
    func handleReturnTransactionSuccess(_ transaction: Transaction, indexPath: IndexPath) {
        dependencies.analytics.log(TransactionAnalytics.transactionReturned(transaction, origin: .transaction))
        presenter.update(transaction: transaction)
    }
}
