import Foundation

enum PicPayTransactionsFactory {
    static func make(seller: Seller, type: TransactionsListType) -> PicPayTransactionsViewController {
        let container = DependencyContainer()
        let service: PicPayTransactionsServicing = PicPayTransactionsService(dependencies: container)
        let coordinator: PicPayTransactionsCoordinating = PicPayTransactionsCoordinator(dependencies: container)
        let presenter: PicPayTransactionsPresenting = PicPayTransactionsPresenter(coordinator: coordinator)
        let interactor = PicPayTransactionsInteractor(seller: seller, service: service, presenter: presenter, dependencies: container, type: type)
        let viewController = PicPayTransactionsViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
