import Foundation
import LegacyPJ
import UI
import UIKit

struct TransactionViewModel {
    let photoUrl: String?
    let isPlaceholderEnabled: Bool
    let name: String
    let date: String?
    let value: String
    let status: TransactionStatus
    
    var statusDescription: NSAttributedString {
        let foregroundColor = (colorState[status] ?? Colors.grayscale700).color
        return NSAttributedString(string: status.description, attributes: [.foregroundColor: foregroundColor])
    }
    
    private let colorState: [TransactionStatus: DynamicColorType] = [
        .approved: Colors.branding700,
        .completed: Colors.branding700,
        .returned: Colors.branding700,
        .canceled: Colors.critical600,
        .unknown: Colors.branding700
    ]
    
    private let originalFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.locale = Locale(identifier: "pt_BR_POSIX")
        return df
    }()
    
    private let legacyDateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        df.locale = Locale(identifier: "pt_BR_POSIX")
        return df
    }()
    
    private let dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "HH:mm dd/MM/yy"
        df.locale = Locale(identifier: "pt_BR_POSIX")
        return df
    }()
    
    init(with transaction: Transaction) {
        name = transaction.consumer?.username ?? ""
        
        photoUrl = transaction.consumer?.imageUrl
        isPlaceholderEnabled = true
        
        if let timestamp = transaction.timestamp, let date = legacyDateFormatter.date(from: timestamp) {
            var formatedDate = dateFormatter.string(from: date)
            
            if let operatorName = transaction.sellerOperator?.username {
                formatedDate = "\(formatedDate) | \(operatorName)"
            }
            
            self.date = formatedDate
        } else {
            self.date = nil
        }
        
        value = transaction.siteValue
        status = TransactionStatus(rawValue: transaction.status.rawValue) ?? .unknown
    }
    
    init(with transaction: LinkTransaction) {
        name = transaction.consumer.name
        
        photoUrl = nil
        isPlaceholderEnabled = false
        
        if let date = originalFormatter.date(from: transaction.createdDate) {
            self.date = dateFormatter.string(from: date)
        } else {
            self.date = nil
        }
        
        value = transaction.value.toCurrencyString() ?? ""
        status = TransactionStatus(rawValue: transaction.status.rawValue) ?? .unknown
    }
}
