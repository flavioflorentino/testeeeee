enum TransactionStatus: String, Decodable, Equatable {
    case approved = "P"
    case completed = "C"
    case returned = "R"
    case canceled = "I"
    case refused = "D"
    case waiting = "O"
    case underAnalysis = "A"
    case notAuthorized = "F"
    case chargeback = "B"
    case inProgress = "E"
    case unknown
    
    private typealias Localizable = Strings.Home.Link.Transaction
    
    var description: String {
        switch self {
        case .approved:
            return Localizable.Status.approved
        case .completed:
            return Localizable.Status.completed
        case .returned:
            return Localizable.Status.returned
        case .canceled:
            return Localizable.Status.canceled
        default:
            return ""
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let rawValue = try container.decode(RawValue.self)
        self = TransactionStatus(rawValue: rawValue) ?? .unknown
    }
}
