import LegacyPJ
import SnapKit
import UI
import UIKit

protocol TransactionsListViewDisplaying: AnyObject {
    func enableScroll()
    func display(transactions: [TransactionViewModel])
    func startLoading()
    func stopLoading()
    func startLoading(at indexPath: IndexPath)
    func stopLoading(at indexPath: IndexPath)
}

protocol TransactionsListViewDelegate: AnyObject {
    func fetchMoreItemsIfNeeded(indexPath: IndexPath)
    func didSelect(at indexPath: IndexPath)
}

private extension TransactionsListView.Layout {
    enum Table {
        static let rowHeight: CGFloat = 70
        static let footerHeight: CGFloat = 25
    }
}

final class TransactionsListView: UIViewController {
    fileprivate enum Layout { }
    
    private lazy var loadingView = UIActivityIndicatorView(style: .gray)
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(TransactionListCell.self, forCellReuseIdentifier: TransactionListCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Layout.Table.rowHeight
        tableView.tableFooterView = loadingView
        tableView.sectionFooterHeight = Layout.Table.footerHeight
        tableView.isScrollEnabled = false
        tableView.separatorStyle = .none
        tableView.delegate = self
        return tableView
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, TransactionViewModel> = {
        let dataSource = TableViewDataSource<Int, TransactionViewModel>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, transaction -> TransactionListCell? in
            let cellIdentifier = TransactionListCell.identifier
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TransactionListCell
            cell?.setup(with: transaction)
            self.delegate?.fetchMoreItemsIfNeeded(indexPath: indexPath)
            return cell
        }
        return dataSource
    }()
    
    private weak var delegate: TransactionsListViewDelegate?

    init(delegate: TransactionsListViewDelegate) {
        super.init(nibName: nil, bundle: nil)
        self.delegate = delegate
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
    }
}

extension TransactionsListView: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        tableView.dataSource = dataSource
    }
}

extension TransactionsListView: TransactionsListViewDisplaying {
    func enableScroll() {
        tableView.isScrollEnabled = true
    }
    
    func display(transactions: [TransactionViewModel]) {
        dataSource.update(items: transactions, from: 0)
        
        let height = tableView.contentSize.height
        tableView.snp.makeConstraints {
            $0.height.equalTo(height)
        }
    }
    
    func startLoading() {
        loadingView.startAnimating()
    }
    
    func stopLoading() {
        loadingView.stopAnimating()
    }

    func startLoading(at indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? TransactionListCell else { return }
        cell.startLoading()
    }

    func stopLoading(at indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? TransactionListCell else { return }
        cell.stopLoading()
    }
}

extension TransactionsListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelect(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
