import Foundation
import UIKit
import SnapKit
import UI

struct TransactionDetailsAlertConfigurator {
    let indexPath: IndexPath
    let photoUrl: String?
    let name: String?
    let date: String?
    let value: String?
    let status: NSAttributedString?
    let isRefundAvailable: Bool
}

private extension TransactionDetailsAlertView.Layout { }

final class TransactionDetailsAlertView: UIView {
    private typealias Localizable = Strings.Home.Transaction.ErrorView
    fileprivate enum Layout { }
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [photoImageView, dataStackView, valueStackView])
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base02
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        return stackView
    }()
    
    private lazy var photoImageView: UIImageView = {
        let image = UIImageView()
        image.imageStyle(RoundedImageStyle(size: .medium, cornerRadius: .full))
        image.contentMode = .scaleAspectFill
        image.isHidden = true
        return image
    }()
    
    private lazy var dataStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [nameLabel, dateLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        return stackView
    }()
    
    private lazy var valueStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [valueLabel, statusLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        return stackView
    }()

    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.black.color)
        return label
    }()

    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale400.color)
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.branding700.color)
            .with(\.textAlignment, .right)
        return label
    }()
    
    private lazy var statusLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle(type: .default))
            .with(\.textColor, Colors.branding700.color)
            .with(\.textAlignment, .right)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(configurator: TransactionDetailsAlertConfigurator) {
        if let photoUrl = configurator.photoUrl, let url = URL(string: photoUrl) {
            photoImageView.isHidden = false
            photoImageView.sd_setImage(with: url)
        }
        nameLabel.text = configurator.name
        dateLabel.text = configurator.date
        valueLabel.text = configurator.value
        statusLabel.attributedText = configurator.status
    }
}

extension TransactionDetailsAlertView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
}
