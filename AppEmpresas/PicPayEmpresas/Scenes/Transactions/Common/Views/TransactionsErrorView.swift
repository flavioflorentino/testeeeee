import AssetsKit
import Foundation
import SnapKit
import UI
import UIKit

protocol TransactionsErrorViewDelegate: AnyObject {
    func didTapTryAgain()
}

private extension TransactionsErrorView.Layout {
    enum ImageView {
        static let size = CGSize(width: 152, height: 153)
    }
}

final class TransactionsErrorView: UIView {
    private typealias Localizable = Strings.Home.Transaction.ErrorView
    fileprivate enum Layout { }
    
    weak var delegate: TransactionsErrorViewDelegate?
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var contentView = UIView()
    
    private lazy var imageView = UIImageView(image: Resources.Illustrations.iluError.image)
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, textLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.textAlignment, .center)
        label.text = Localizable.title
        return label
    }()

    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.textAlignment, .center)
        label.text = Localizable.text
        return label
    }()
    
    private lazy var tryAgainButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.tryAgain, for: .normal)
        button.addTarget(self, action: #selector(didTapTryAgain), for: .touchUpInside)
        button.buttonStyle(LinkButtonStyle(size: .small, icon: (name: Iconography.redo, alignment: IconAlignment.right)))
        return button
    }()
    
    private lazy var allTransactionsButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.allTransactions, for: .normal)
        button.buttonStyle(SecondaryButtonStyle())
        button.addTarget(self, action: #selector(didTapAllTransactions), for: .touchUpInside)
        return button
    }()
    
    init(delegate: TransactionsErrorViewDelegate?) {
        self.delegate = delegate
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension TransactionsErrorView: ViewConfiguration {
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    func buildViewHierarchy() {
        addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(imageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(textLabel)
        contentView.addSubview(tryAgainButton)
        contentView.addSubview(allTransactionsButton)
    }
    
    func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            $0.height.equalTo(compatibleSafeArea.height).priority(.low)
        }
        
        imageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().offset(Spacing.base06)
            $0.size.equalTo(Layout.ImageView.size)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        textLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        tryAgainButton.snp.makeConstraints {
            $0.top.equalTo(textLabel.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        allTransactionsButton.snp.makeConstraints {
            $0.top.equalTo(tryAgainButton.snp.bottom).offset(Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.lessThanOrEqualToSuperview().offset(-Spacing.base05)
        }
    }
}

@objc
private extension TransactionsErrorView {
    func didTapTryAgain() {
        delegate?.didTapTryAgain()
    }
    
    func didTapAllTransactions() {
        // TODO: Próximo PR
    }
}
