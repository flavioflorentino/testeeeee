import Foundation
import AssetsKit
import SnapKit
import UI
import UIKit

protocol TransactionListCellDisplay: AnyObject {
    func setup(with item: TransactionViewModel)
    func startLoading()
    func stopLoading()
}

private extension TransactionListCell.Layout {
    enum PhotoImage {
        static let size = CGSize(width: 40, height: 40)
        static let cornerRadius: CGFloat = 20
    }
    enum OverlayView {
        static let opacity: Float = 0.85
    }
}

final class TransactionListCell: UITableViewCell {
    private typealias Localizable = Strings.Transaction
    fileprivate enum Layout { }
    
    private lazy var photoImageView: UIImageView = {
        let view = UIImageView()
        view.layer.masksToBounds = true
        view.layer.cornerRadius = Layout.PhotoImage.cornerRadius
        view.isHidden = true
        return view
    }()

    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale600())
        label.numberOfLines = 1
        return label
    }()

    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        label.numberOfLines = 1
        label.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        return label
    }()
    
    private lazy var dateNameStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [dateLabel, nameLabel])
        stackView.spacing = Spacing.base00
        stackView.axis = .vertical
        stackView.alignment = .leading
        return stackView
    }()

    private lazy var statusLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .right)
        label.numberOfLines = 1
        return label
    }()

    private lazy var balanceLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .right)
        label.numberOfLines = 1
        return label
    }()

    private lazy var statusPriceStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [statusLabel, balanceLabel])
        stackView.spacing = Spacing.base00
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [photoImageView, dateNameStackView, statusPriceStackView])
        stackView.spacing = Spacing.base02
        return stackView
    }()

    private lazy var overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.opacity = Layout.OverlayView.opacity
        return view
    }()
    
    private lazy var activityIndicator = UIActivityIndicatorView(style: .gray)

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        photoImageView.image = nil
        photoImageView.isHidden = true
        
        nameLabel.text = ""
        dateLabel.text = ""
        statusLabel.text = ""
        statusLabel.attributedText = NSAttributedString()
        balanceLabel.text = ""
        stopLoading()
    }
}

extension TransactionListCell: ViewConfiguration {
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }

    func buildViewHierarchy() {
        contentView.addSubview(stackView)
    }
    
    func setupConstraints() {
        photoImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.PhotoImage.size)
        }
        
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
        }
    }
}

extension TransactionListCell: TransactionListCellDisplay {
    func setup(with item: TransactionViewModel) {
        dateLabel.text = item.date
        nameLabel.text = item.name
        balanceLabel.text = item.value

        if item.isPlaceholderEnabled, let photoUrl = item.photoUrl {
            photoImageView.isHidden = false
            photoImageView.sd_setImage(with: URL(string: photoUrl), placeholderImage: Assets.iluPersonPlaceholder.image)
        }
        
        statusLabel.isHidden = item.status == .unknown
        statusLabel.attributedText = item.statusDescription
    }

    func startLoading() {
        contentView.addSubview(activityIndicator)
        contentView.addSubview(overlayView)

        overlayView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        activityIndicator.snp.makeConstraints {
            $0.centerY.centerX.equalToSuperview()
        }

        activityIndicator.startAnimating()
    }

    func stopLoading() {
        activityIndicator.removeFromSuperview()
        overlayView.removeFromSuperview()
    }
}
