import AnalyticsModule
import Core
import Foundation
import FeatureFlag
import LegacyPJ

protocol TransactionsInteracting: AnyObject {
    func setupHeader()
    func setupTransactions()
    func reload(transactionTab: Int)
}

final class TransactionsInteractor {
    typealias Dependencies = HasAuthManager & HasFeatureManager

    private let dependencies: Dependencies
    private let presenter: TransactionsPresenting
    private let seller: Seller
    private var tabs: [TransactionTabOption] = [.picPayTransactions]
    
    private lazy var shouldEnablePix: Bool = {
        guard let isAdmin = dependencies.authManager.user?.isAdmin else { return false }
        return dependencies.featureManager.isActive(.releasePIXPresenting) &&
            seller.digitalAccount &&
            seller.biometry &&
            isAdmin
    }()
    
    private lazy var shouldEnableExternalLink: Bool = {
        dependencies.featureManager.isActive(.isExternalLinkAvailable)
    }()
    
    init(presenter: TransactionsPresenting, dependencies: Dependencies, seller: Seller) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.seller = seller
    }
}

extension TransactionsInteractor: TransactionsInteracting {
    func setupHeader() {
        if shouldEnablePix {
            tabs.append(.pixTransactions)
        }
        
        if shouldEnableExternalLink {
            tabs.append(.linkTransactions)
            presenter.setupNewSegmentedControl()
        }
        tabs.count == 1 ? presenter.presentTitle() : presenter.presentSegmentedControl(tabs)
    }
    
    func setupTransactions() {
        presenter.setupPicPayTransactions(seller: seller)
        
        if shouldEnablePix {
            presenter.setupPixTransactions(seller: seller)
        }
        
        if shouldEnableExternalLink {
            presenter.setupLinkTransactions(seller: seller)
        }
    }

    func reload(transactionTab: Int) {
        guard
            transactionTab < tabs.count, transactionTab >= 0,
            let tab = tabs[safe: transactionTab]
        else { return }
        
        switch tab {
        case .picPayTransactions:
            presenter.reloadPicPayTransactions()
        case .pixTransactions:
            presenter.reloadPixTransactions()
        case .linkTransactions:
            presenter.reloadLinkTransactions()
        }
    }
}
