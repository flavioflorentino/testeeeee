import LegacyPJ
import SnapKit
import UI
import UIKit

protocol LinkTransactionsDisplay: AnyObject {
    func enableScroll()
    func setupTransactionsListView()
    func display(transactions: [TransactionViewModel])
    func displayDetails(with configurator: TransactionDetailsAlertConfigurator)
    
    func setupButton()
    
    func startLoading()
    func stopLoading()
    func startPaginatingLoader()
    func stopPaginatingLoader()
    
    func startLoading(at indexPath: IndexPath)
    func stopLoading(at indexPath: IndexPath)
    
    func displayEmptyView()
    func displayErrorView()
    func displayErrorAlert()
    func resetViews()
}

private extension LinkTransactionsViewController.Layout {
    enum ActionSheet {
        static let frameOrigin = CGPoint(x: 0.0, y: 10.0)
        static let frameHeight: CGFloat = 60.0
        static let frameWidthOffset: CGFloat = 20.0
        static let cornerRadius: CGFloat = 10.0
    }
}

final class LinkTransactionsViewController: ViewController<LinkTransactionsInteracting, UIView> {
    private typealias Localizable = Strings.Home.Transaction
    fileprivate enum Layout { }
    
    private lazy var loadingView: UIActivityIndicatorView = {
        if #available(iOS 13.0, *) {
            return UIActivityIndicatorView(style: .large)
        }
        return UIActivityIndicatorView(style: .gray)
    }()
    
    private lazy var transactionsListView = TransactionsListView(delegate: self)
    
    private lazy var showAllTransactionsButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.Button.title, for: .normal)
        button.buttonStyle(SecondaryButtonStyle())
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var errorView = TransactionsErrorView(delegate: self)
    private lazy var emptyView = LinkTransactionsEmptyView(delegate: self)

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.initialFetch()
    }
    
    func reloadTransactions() {
        interactor.initialFetch()
    }
}

extension LinkTransactionsViewController: LinkTransactionsDisplay {
    func enableScroll() {
        transactionsListView.enableScroll()
    }
    
    func display(transactions: [TransactionViewModel]) {
        transactionsListView.display(transactions: transactions)
    }
    
    func setupTransactionsListView() {
        addTransactionsListView(with: transactionsListView)
        setupTransactionsConstraints()
    }
    
    func setupButton() {
        view.addSubview(showAllTransactionsButton)
        view.bringSubviewToFront(showAllTransactionsButton)
        setupButtonConstraints()
        showAllTransactionsButton.isHidden = false
    }
    
    func startLoading() {
        view.addSubview(loadingView)
        setupLoadingViewConstraints()
        loadingView.startAnimating()
    }
    
    func stopLoading() {
        loadingView.stopAnimating()
        removeView(loadingView)
    }
    
    func startLoading(at indexPath: IndexPath) {
        transactionsListView.startLoading(at: indexPath)
    }

    func stopLoading(at indexPath: IndexPath) {
        transactionsListView.stopLoading(at: indexPath)
    }
    
    func startPaginatingLoader() {
        transactionsListView.startLoading()
    }
    
    func stopPaginatingLoader() {
        transactionsListView.stopLoading()
    }
    
    func displayDetails(with configurator: TransactionDetailsAlertConfigurator) {
        let alert = UIAlertController(title: "\n\n\n", message: nil, preferredStyle: .actionSheet)

        let size = CGSize(
            width: view.frame.size.width - Layout.ActionSheet.frameWidthOffset,
            height: Layout.ActionSheet.frameHeight
        )
        let frame = CGRect(origin: Layout.ActionSheet.frameOrigin, size: size)
        
        let view = TransactionDetailsAlertView(frame: frame)
        view.backgroundColor = .clear
        view.layer.cornerRadius = Layout.ActionSheet.cornerRadius
        alert.view.addSubview(view)
        
        view.setup(configurator: configurator)

        let refundAction = UIAlertAction(
            title: Strings.Transaction.Alert.Button.return,
            style: .destructive,
            handler: { _ in
                self.interactor.refundTransaction(at: configurator.indexPath)
            })
        
        refundAction.isEnabled = configurator.isRefundAvailable
        
        let cancelAction = UIAlertAction(title: Strings.Default.cancel, style: .cancel)
        
        alert.addAction(refundAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
        alert.view.setNeedsLayout()
    }
    
    func displayEmptyView() {
        view.addSubview(emptyView)
        setupEmptyViewConstraints()
    }

    func displayErrorView() {
        view.addSubview(errorView)
        setupErrorViewConstraints()
    }
    
    func displayErrorAlert() {
        let action = PopupAction(title: Strings.Transaction.Transactions.Error.action, style: .tryAgain)
        let popUp = UI.PopupViewController(
            title: Strings.Transaction.Transactions.Error.title,
            description: Strings.Transaction.Transactions.Error.description,
            image: Assets.errorIcon.image
        )

        popUp.hideCloseButton = true
        popUp.addAction(action)
        showPopup(popUp)
    }
    
    func resetViews() {
        removeView(loadingView)
        removeViewController(transactionsListView)
        removeView(showAllTransactionsButton)
        removeView(emptyView)
        removeView(errorView)
    }
}

@objc
private extension LinkTransactionsViewController {
    func didTapButton() {
        interactor.didNextStep()
    }
}

extension LinkTransactionsViewController: TransactionsListViewDelegate {
    func fetchMoreItemsIfNeeded(indexPath: IndexPath) {
        interactor.fetchMoreItemsIfNeeded(indexPath: indexPath)
    }
    
    func didSelect(at indexPath: IndexPath) {
        interactor.didSelect(at: indexPath)
    }
}

extension LinkTransactionsViewController: LinkTransactionsEmptyViewDelegate {
    func didTapCharge() {
        interactor.charge()
    }
    
    func didTapKnowMore() {
        // TODO: Próximo PR
    }
}

extension LinkTransactionsViewController: TransactionsErrorViewDelegate {
    func didTapTryAgain() {
        // TODO: Próximo PR
    }
}

private extension LinkTransactionsViewController {
    func addTransactionsListView(with viewController: UIViewController) {
        addChild(viewController)
        view.addSubview(viewController.view)
        viewController.didMove(toParent: self)
    }
    
    func removeViewController(_ viewController: UIViewController?) {
        guard let viewController = viewController else { return }
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    func removeView(_ viewCustom: UIView?) {
        guard let viewCustom = viewCustom else { return }
        viewCustom.removeFromSuperview()
    }
    
    func setupLoadingViewConstraints() {
        loadingView.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
            $0.top.leading.greaterThanOrEqualToSuperview().inset(Spacing.base02)
            $0.trailing.bottom.lessThanOrEqualToSuperview().inset(Spacing.base02)
        }
    }
    
    func setupTransactionsConstraints() {
        transactionsListView.view.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
    }
    
    func setupButtonConstraints() {
        showAllTransactionsButton.snp.makeConstraints {
            $0.top.equalTo(transactionsListView.view.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }
    
    func setupEmptyViewConstraints() {
        emptyView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func setupErrorViewConstraints() {
        errorView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}
