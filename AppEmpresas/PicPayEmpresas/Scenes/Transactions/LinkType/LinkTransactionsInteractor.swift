import Foundation
import LegacyPJ
import Core

protocol LinkTransactionsInteracting: AnyObject {
    func initialFetch()
    func setupPaginationIfNeeded()
    func fetchTransactions(at page: Int?)
    func fetchMoreItemsIfNeeded(indexPath: IndexPath)
    func didSelect(at indexPath: IndexPath)
    func refundTransaction(at indexPath: IndexPath)
    func charge()
    func didNextStep()
}

final class LinkTransactionsInteractor {
    typealias Dependencies = HasAuthManager
    
    private let dependencies: Dependencies
    private let service: LinkTransactionsServicing
    private let presenter: LinkTransactionsPresenting
    
    private let seller: Seller
    private var transactions: [LinkTransaction] = []
    
    private let compactModeMaxTransactions = 5
    private let perPage = 10
    private var type: TransactionsListType
    private var page: Int = 0
    private var hasNextPage = false
    private var isLoading = false
    
    init(seller: Seller, dependencies: Dependencies, service: LinkTransactionsServicing, presenter: LinkTransactionsPresenting, type: TransactionsListType) {
        self.seller = seller
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
        self.type = type
    }
}

extension LinkTransactionsInteractor: LinkTransactionsInteracting {
    func setupPaginationIfNeeded() {
        guard type == .full else { return }
        presenter.setupPagination()
    }
    
    func initialFetch() {
        presenter.startLoading()
        fetchTransactions()
    }
    
    func fetchMoreItemsIfNeeded(indexPath: IndexPath) {
        guard type == .full, hasNextPage, indexPath.row == (transactions.count - 1) else { return }

        presenter.willStartPaginating()
        fetchTransactions(at: page)
    }
    
    func fetchTransactions(at page: Int? = nil) {
        guard !isLoading else {
            stopLoading()
            return
        }
        
        isLoading = true
        
        service.fetch(sellerID: seller.id, page: page, perPage: perPage) { [weak self] result in
            guard let self = self else { return }
            
            self.isLoading = false
            
            self.stopLoading()
            switch result {
            case .success(let response) where response.data.isEmpty:
                self.presenter.presentEmptyState()
            case .success(let response):
                if self.type == .full {
                    self.transactions += response.data
                } else {
                    self.transactions = Array(response.data.prefix(self.compactModeMaxTransactions))
                }
                
                self.hasNextPage = response.meta.pagination.hasNext
                self.page = response.meta.pagination.nextPage
                
                self.presenter.present(
                    transactions: self.transactions,
                    showButton: self.type == .compact && response.data.count > self.compactModeMaxTransactions
                )
            case .failure:
                self.presenter.presentErrorState()
            }
        }
    }
    
    func didSelect(at indexPath: IndexPath) {
        guard let transaction = transactions[safe: indexPath.row] else { return }
        presenter.presentDetails(of: transaction, at: indexPath)
    }
    
    func refundTransaction(at indexPath: IndexPath) {
        guard let transaction = transactions[safe: indexPath.row] else { return }
        
        presenter.startLoading(at: indexPath)
        service.refundTransaction(id: transaction.id) { [weak self] result in
            self?.presenter.stopLoading(at: indexPath)
            switch result {
            case .success:
                // TODO: Próximo PR (atualizar a UI)
                break
            case .failure:
                self?.presenter.presentErrorAlert()
            }
        }
    }
    
    func didNextStep() {
        let transactionsDescriptor = TransactionsDescriptor(
            displayHeader: .tabs,
            seller: seller,
            selectedTab: .linkTransactions
        )
        presenter.didNextStep(action: .showAll(descriptor: transactionsDescriptor))
    }
    
    func charge() {
        presenter.didNextStep(action: .charge)
    }
}

private extension LinkTransactionsInteractor {
    func stopLoading() {
        presenter.didEndPaginating()
        presenter.stopLoading()
    }
}
