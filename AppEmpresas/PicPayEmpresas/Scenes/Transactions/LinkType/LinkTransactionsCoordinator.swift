import UI
import UIKit

enum LinkTransactionsAction {
    case showAll(descriptor: TransactionsDescriptor)
    case charge
}

protocol LinkTransactionsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LinkTransactionsAction)
}

final class LinkTransactionsCoordinator: LinkTransactionsCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: LinkTransactionsAction) {
        switch action {
        case let .showAll(descriptor: transactionsDescriptor):
            let controller = TransactionsFactory.make(seller: transactionsDescriptor.seller, type: .full)
            viewController?.pushViewController(controller)
        case .charge:
            guard let tabBarController = viewController?.tabBarController as? MainTabBarController else {
                return
            }
            
            tabBarController.select(viewController: viewController, tag: MainTabBarController.MainTabBarItens.ReceiveTab.rawValue)
        }
    }
}
