import Foundation

struct LinkTransactionMeta: Decodable {
    let pagination: PaginationResponse
}

struct PaginationResponse: Decodable {
    let currentPage: Int
    let numberOfPages: Int
    
    var hasNext: Bool {
        currentPage < numberOfPages
    }
    
    var nextPage: Int {
        currentPage + 1
    }
    
    private enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case numberOfPages = "total_pages"
    }
}

struct LinkTransactionResponse: Decodable {
    let data: [LinkTransaction]
    let meta: LinkTransactionMeta
}
