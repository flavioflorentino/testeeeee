import Foundation

struct LinkTransaction: Decodable, Equatable {
    let id: Int
    let code: String
    let status: TransactionStatus
    let value: Double
    let valueToSettle: Double?
    let consumer: LinkConsumer
    let createdDate: String
    let updatedDate: String
    
    private enum CodingKeys: String, CodingKey {
        case id
        case code
        case status = "status_id"
        case value
        case valueToSettle = "value_to_settle"
        case consumerName = "consumer_name"
        case consumerEmail = "consumer_email"
        case consumerCpf = "consumer_cpf"
        case consumerCnpj = "consumer_cnpj"
        case createdDate = "created_at"
        case updatedDate = "updated_at"
    }
    
    init(id: Int,
         code: String,
         status: TransactionStatus,
         value: Double,
         valueToSettle: Double?,
         consumer: LinkConsumer,
         createdDate: String,
         updatedDate: String
    ) {
        self.id = id
        self.code = code
        self.status = status
        self.value = value
        self.valueToSettle = valueToSettle
        self.consumer = consumer
        self.createdDate = createdDate
        self.updatedDate = updatedDate
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        code = try container.decode(String.self, forKey: .code)
        status = try container.decode(TransactionStatus.self, forKey: .status)
        value = try container.decode(Double.self, forKey: .value)
        valueToSettle = try container.decode(Double?.self, forKey: .valueToSettle)
        
        let consumerName = try container.decode(String.self, forKey: .consumerName)
        let consumerEmail = try container.decode(String?.self, forKey: .consumerEmail)
        let consumerCpf = try container.decode(String?.self, forKey: .consumerCpf)
        let consumerCnpj = try container.decode(String?.self, forKey: .consumerCnpj)
        consumer = LinkConsumer(name: consumerName, email: consumerEmail, cpf: consumerCpf, cnpj: consumerCnpj)
        
        createdDate = try container.decode(String.self, forKey: .createdDate)
        updatedDate = try container.decode(String.self, forKey: .createdDate)
    }
}
