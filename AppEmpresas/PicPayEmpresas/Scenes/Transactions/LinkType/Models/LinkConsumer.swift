import Foundation

struct LinkConsumer: Decodable, Equatable {
    let name: String
    let email: String?
    let cpf: String?
    let cnpj: String?
}
