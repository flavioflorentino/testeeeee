import AssetsKit
import Foundation
import SnapKit
import UI
import UIKit

protocol LinkTransactionsEmptyViewDelegate: AnyObject {
    func didTapCharge()
    func didTapKnowMore()
}

private extension LinkTransactionsEmptyView.Layout {
    enum ImageView {
        static let size = CGSize(width: 152, height: 139)
    }
}

final class LinkTransactionsEmptyView: UIView {
    private typealias Localizable = Strings.Home.Transaction.EmptyView.Link
    fileprivate enum Layout { }
    
    private lazy var imageView = UIImageView(image: Resources.Illustrations.iluGirlCoin.image)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.textAlignment, .center)
            .with(\.text, Localizable.title)
        return label
    }()

    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.textAlignment, .center)
            .with(\.text, Localizable.subtitle)
        return label
    }()
    
    private lazy var chargeButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.chargeButton, for: .normal)
        button.buttonStyle(SecondaryButtonStyle())
        button.addTarget(self, action: #selector(didTapCharge), for: .touchUpInside)
        return button
    }()
    
    private lazy var knowMoreButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.knowMoreLink, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTapKnowMore), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private weak var delegate: LinkTransactionsEmptyViewDelegate?
    
    init(delegate: LinkTransactionsEmptyViewDelegate?) {
        self.delegate = delegate
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension LinkTransactionsEmptyView: ViewConfiguration {
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    func buildViewHierarchy() {
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addSubview(chargeButton)
        addSubview(knowMoreButton)
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().offset(Spacing.base06)
            $0.size.equalTo(Layout.ImageView.size)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        chargeButton.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        knowMoreButton.snp.makeConstraints {
            $0.top.equalTo(chargeButton.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.lessThanOrEqualToSuperview().offset(-Spacing.base05)
        }
    }
}

@objc
extension LinkTransactionsEmptyView {
    func didTapCharge() {
        delegate?.didTapCharge()
    }
    
    func didTapKnowMore() {
        delegate?.didTapKnowMore()
    }
}
