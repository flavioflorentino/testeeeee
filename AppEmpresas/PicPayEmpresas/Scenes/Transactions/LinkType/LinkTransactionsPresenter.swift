import Foundation
import UI

protocol LinkTransactionsPresenting: AnyObject {
    var viewController: LinkTransactionsDisplay? { get set }

    func present(transactions: [LinkTransaction], showButton: Bool)
    func presentDetails(of transaction: LinkTransaction, at indexPath: IndexPath)
    
    func setupPagination()
    func willStartPaginating()
    func didEndPaginating()
    func startLoading()
    func stopLoading()
    func startLoading(at indexPath: IndexPath)
    func stopLoading(at indexPath: IndexPath)
    
    func presentEmptyState()
    func presentErrorState()
    func presentErrorAlert()
    
    func didNextStep(action: LinkTransactionsAction)
}

final class LinkTransactionsPresenter {
    private let coordinator: LinkTransactionsCoordinating
    weak var viewController: LinkTransactionsDisplay?

    init(coordinator: LinkTransactionsCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - HomeExternalLinkPresenting
extension LinkTransactionsPresenter: LinkTransactionsPresenting {
    func present(transactions: [LinkTransaction], showButton: Bool) {
        viewController?.resetViews()
        viewController?.setupTransactionsListView()
        viewController?.display(transactions: transactions.map(TransactionViewModel.init))
        
        if showButton {
            viewController?.setupButton()
        }
    }
    
    func presentDetails(of transaction: LinkTransaction, at indexPath: IndexPath) {
        let viewModel = TransactionViewModel(with: transaction)
        
        let configurator = TransactionDetailsAlertConfigurator(
            indexPath: indexPath,
            photoUrl: viewModel.photoUrl,
            name: viewModel.name,
            date: viewModel.date,
            value: viewModel.value,
            status: viewModel.statusDescription,
            isRefundAvailable: transaction.status != .canceled
        )
        
        viewController?.displayDetails(with: configurator)
    }
    
    func setupPagination() {
        viewController?.enableScroll()
    }
    
    func willStartPaginating() {
        viewController?.startPaginatingLoader()
    }
    
    func didEndPaginating() {
        viewController?.stopPaginatingLoader()
    }
    
    func startLoading() {
        viewController?.resetViews()
        viewController?.startLoading()
    }

    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func startLoading(at indexPath: IndexPath) {
        viewController?.startLoading(at: indexPath)
    }

    func stopLoading(at indexPath: IndexPath) {
        viewController?.stopLoading(at: indexPath)
    }
    
    func presentEmptyState() {
        viewController?.resetViews()
        viewController?.displayEmptyView()
    }
    
    func presentErrorState() {
        viewController?.resetViews()
        viewController?.displayErrorView()
    }
    
    func presentErrorAlert() {
        viewController?.displayErrorAlert()
    }
    
    func didNextStep(action: LinkTransactionsAction) {
        coordinator.perform(action: action)
    }
}
