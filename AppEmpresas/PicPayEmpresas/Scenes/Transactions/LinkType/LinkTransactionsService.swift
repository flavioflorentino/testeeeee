import Foundation
import Core
import LegacyPJ

protocol LinkTransactionsServicing {
    func fetch(
        sellerID: Int,
        page: Int?,
        perPage: Int,
        completion: @escaping(Result<LinkTransactionResponse, ApiError>) -> Void
    )
    func authenticate(_ completion: @escaping (_ result: AuthManager.PerformActionResult) -> Void)
    func refundTransaction(id: Int, completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

final class LinkTransactionsService {
    typealias Dependencies = HasMainQueue & HasAuthManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension LinkTransactionsService: LinkTransactionsServicing {
    func fetch(
        sellerID: Int,
        page: Int?,
        perPage: Int,
        completion: @escaping(Result<LinkTransactionResponse, ApiError>) -> Void
    ) {
        let endpoint = LinkTransactionsEndpoint.transactions(sellerID: sellerID, page: page, perPage: perPage)
        
        Core.Api<LinkTransactionResponse>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func authenticate(_ completion: @escaping (_ result: AuthManager.PerformActionResult) -> Void) {
        dependencies.authManager.performActionWithAuthorization(nil, completion)
    }
    
    func refundTransaction(id: Int, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = LinkTransactionsEndpoint.refund(transactionId: String(id))
        
        BizApi<NoContent>(endpoint: endpoint).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                BizApiHelper.unwrapBizResult(result: result, completion: completion)
            }
        }
    }
}
