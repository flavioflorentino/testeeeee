import Foundation

enum LinkTransactionsFactory {
    static func make(seller: Seller, type: TransactionsListType) -> LinkTransactionsViewController {
        let container = DependencyContainer()
        let service: LinkTransactionsServicing = LinkTransactionsService(dependencies: container)
        let coordinator: LinkTransactionsCoordinating = LinkTransactionsCoordinator()
        let presenter: LinkTransactionsPresenting = LinkTransactionsPresenter(coordinator: coordinator)
        let interactor = LinkTransactionsInteractor(seller: seller, dependencies: container, service: service, presenter: presenter, type: type)
        let viewController = LinkTransactionsViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
