import Core
import Foundation

enum LinkTransactionsEndpoint {
    case transactions(sellerID: Int, page: Int?, perPage: Int)
    case refund(transactionId: String)
}

extension LinkTransactionsEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .transactions:
            return "/mundipagg-transactions"
        case .refund(let transactionId):
            return "/payment-checkout/\(transactionId)"
        }
    }
    
    var parameters: [String: Any] {
        switch self {
        case let .transactions(sellerID, page, perPage):
            var params = [
                "filter[seller_id]": sellerID,
                "per_page": perPage
            ]
            
            if let page = page {
                params["page"] = page
            }
            
            return params
        default:
            return [:]
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .refund:
            return .delete
        default:
            return .get
        }
    }
}
