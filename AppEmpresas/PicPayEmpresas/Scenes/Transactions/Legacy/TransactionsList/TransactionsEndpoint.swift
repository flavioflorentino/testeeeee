import Core
import Foundation

enum TransactionsEndpoint: ApiEndpointExposable {
    case user
    case pix
    
    var path: String {
        switch self {
        case .user:
            return "/transaction"
        case .pix:
            return "/pix-transactions"
        }
    }
}
