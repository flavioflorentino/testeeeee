import Core
import Foundation
import LegacyPJ

protocol TransactionsListServicing {
    func fetchTransactions(page: Int?,
                           endpoint: TransactionsEndpoint,
                           completion: @escaping (Result<TransactionListResponse, LegacyPJError>) -> Void)
    func authenticate(_ completion: @escaping (_ result: AuthManager.PerformActionResult) -> Void)
    func cancelTransaction(_ pin: String, id: Int, completion: @escaping (Result<Void, LegacyPJError>) -> Void)
}

final class TransactionsListService {
    typealias Dependencies = HasMainQueue & HasAuthManager

    private let dependencies: Dependencies
    private let apiTransaction: ApiTransaction

    init(dependencies: Dependencies, apiTransaction: ApiTransaction = ApiTransaction()) {
        self.dependencies = dependencies
        self.apiTransaction = apiTransaction
    }
}

// MARK: - TransactionsListServicing
extension TransactionsListService: TransactionsListServicing {
    func fetchTransactions(page: Int?,
                           endpoint: TransactionsEndpoint,
                           completion: @escaping (Result<TransactionListResponse, LegacyPJError>) -> Void) {
        apiTransaction.list(page, endpoint: endpoint.absoluteStringUrl) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result)
            }
        }
    }

    func authenticate(_ completion: @escaping (_ result: AuthManager.PerformActionResult) -> Void) {
        dependencies.authManager.performActionWithAuthorization { result in
            completion(result)
        }
    }

    func cancelTransaction(_ pin: String, id: Int, completion: @escaping (Result<Void, LegacyPJError>) -> Void) {
        apiTransaction.cancelTransaction(pin: pin, id: id) { [weak self] error in
            self?.dependencies.mainQueue.async {
                guard let error = error else {
                    completion(.success(()))
                    return
                }
                completion(.failure(error))
            }
        }
    }
}
