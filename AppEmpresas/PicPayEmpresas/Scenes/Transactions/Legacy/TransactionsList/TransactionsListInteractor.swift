import AnalyticsModule
import Foundation
import LegacyPJ

protocol TransactionsListInteracting: AnyObject {
    func initialFetch()
    func fetchMoreItensIfNeeded(_ indexPath: IndexPath)
    func refresh()
    func didSelectTransaction(at indexPath: IndexPath)
    func cancelTransaction(_ indexPath: IndexPath)
    func openPixReceipt(_ transaction: Transaction)
    func returnPixPayment(_ transaction: Transaction)
}

final class TransactionsListInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: TransactionsListServicing
    private let presenter: TransactionsListPresenting
    private let type: TransactionContainerListType

    private var page: Int?
    private var hasNextPage = false
    private var isLoading = false
    private var transactions: [Transaction] = []

    init(service: TransactionsListServicing,
         presenter: TransactionsListPresenting,
         dependencies: Dependencies,
         type: TransactionContainerListType) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.type = type
    }
}

// MARK: - TransactionsListInteracting
extension TransactionsListInteractor: TransactionsListInteracting {
    func initialFetch() {
        fetchTransactions(page)
    }

    func fetchMoreItensIfNeeded(_ indexPath: IndexPath) {
        guard hasNextPage, indexPath.row == (transactions.count - 1) else { return }

        fetchTransactions(page)
    }

    func refresh() {
        presenter.prepareToReload()
        page = nil
        transactions = []
        fetchTransactions(page)
    }

    func didSelectTransaction(at indexPath: IndexPath) {
        let transaction = transactions[indexPath.row]
        dependencies.analytics.log(TransactionAnalytics.transactionDetails(transaction))

        presenter.showAlert(for: transaction, indexPath: indexPath)
    }

    func cancelTransaction(_ indexPath: IndexPath) {
        service.authenticate { [weak self] result in
            switch result {
            case .success(let pin):
                self?.returnTransaction(pin, indexPath: indexPath)
            case .failure(let error):
                self?.presenter.fail(error.localizedDescription)
            case .canceled:
                break
            }
        }
    }

    func openPixReceipt(_ transaction: Transaction) {
        let isRefundAllowed = transaction.status != .canceled
        presenter.didNextStep(action: .presentPixReceipt(transaction.id, isRefundAllowed: isRefundAllowed))
    }

    func returnPixPayment(_ transaction: Transaction) {
        presenter.didNextStep(action: .returnPixPayment(transaction.id))
    }
}

private extension TransactionsListInteractor {
    func fetchTransactions(_ page: Int?) {
        guard !isLoading else { return }

        isLoading = true
        let endpoint: TransactionsEndpoint = (type == .user) ? .user : .pix
        service.fetchTransactions(page: page ?? 0, endpoint: endpoint) { [weak self] result in
            self?.isLoading = false
            switch result {
            case .success(let response):
                self?.hasNextPage = response.pagination.hasNext
                self?.page = response.pagination.nextPage
                self?.transactions += response.list
                self?.presenter.didFetchTransactions(response.list)
            case .failure(let error):
                self?.presenter.fail(error.localizedDescription)
            }
        }
    }

    func returnTransaction(_ pin: String, indexPath: IndexPath) {
        presenter.startLoading(at: indexPath)
        let transaction = transactions[indexPath.row]
        service.cancelTransaction(pin, id: transaction.id) { [weak self] result in 
            self?.presenter.stopLoading(at: indexPath)
            switch result {
            case .success:
                guard let self = self else { return }

                self.dependencies.analytics.log(TransactionAnalytics.transactionReturned(transaction, origin: .transaction))
                self.transactions[indexPath.row].status = .canceled
                self.presenter.reloadTransactions(self.transactions)
            case .failure(let error):
                self?.presenter.fail(error.localizedDescription)
            }
        }
    }
}
