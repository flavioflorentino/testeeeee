import Foundation
import LegacyPJ

protocol TransactionsListPresenting: AnyObject {
    var viewController: TransactionsListDisplaying? { get set }
    func didFetchTransactions(_ transactions: [Transaction])
    func didNextStep(action: TransactionsListAction)
    func prepareToReload()
    func reloadTransactions(_ transactions: [Transaction])
    func showAlert(for transaction: Transaction, indexPath: IndexPath)
    func fail(_ message: String)
    func startLoading(at indexPath: IndexPath)
    func stopLoading(at indexPath: IndexPath)
}

final class TransactionsListPresenter {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: TransactionsListCoordinating
    weak var viewController: TransactionsListDisplaying?

    init(coordinator: TransactionsListCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - TransactionsListPresenting
extension TransactionsListPresenter: TransactionsListPresenting {
    func didFetchTransactions(_ transactions: [Transaction]) {
        viewController?.displayTransactions(transactions)
    }
    
    func didNextStep(action: TransactionsListAction) {
        coordinator.perform(action: action)
    }

    func prepareToReload() {
        viewController?.prepareToReload()
    }

    func showAlert(for transaction: Transaction, indexPath: IndexPath) {
        viewController?.showAlert(transaction, indexPath: indexPath)
    }

    func reloadTransactions(_ transactions: [Transaction]) {
        viewController?.reloadTransactions(transactions)
    }

    func fail(_ message: String) {
        didNextStep(action: .modalError(message))
    }

    func startLoading(at indexPath: IndexPath) {
        viewController?.startLoading(at: indexPath)
    }

    func stopLoading(at indexPath: IndexPath) {
        viewController?.stopLoading(at: indexPath)
    }
}
