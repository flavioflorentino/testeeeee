import LegacyPJ
import UI
import UIKit

protocol TransactionsCellPresenting: AnyObject {
    var cell: TransactionsCellDisplay? { get set }

    func setup(_ item: Transaction)
}

struct TransactionsCellItem {
    let photoUrl: String
    let timestampOperator: String
    let userName: String
    let balance: String
}

final class TransactionsCellPresenter: TransactionsCellPresenting {
    private typealias Localizable = Strings.Transaction
    weak var cell: TransactionsCellDisplay?

    func setup(_ transaction: Transaction) {
        var timestampOperatorValue = ""
        let photoUrl = transaction.consumer?.imageUrl ?? ""
        let userName = transaction.consumer?.username ?? ""

        if let timestamp = transaction.timestamp, let date = timestamp.transactionTimestamp {
            timestampOperatorValue = DateFormatter.custom(value: date, format: .timeAndDate)
        } else {
            timestampOperatorValue = transaction.rawDate
        }
        if let operatorName = transaction.sellerOperator?.username {
            timestampOperatorValue += " | \(operatorName)"
        }

        let item = TransactionsCellItem(
            photoUrl: photoUrl,
            timestampOperator: timestampOperatorValue,
            userName: userName,
            balance: transaction.siteValue
        )
        cell?.setup(item)

        setupStatus(transaction.status)
    }
}

private extension TransactionsCellPresenter {
    func setupStatus(_ status: TransactionStatusLegacy) {
        switch status {
        case .unknown:
            cell?.hideStatus()
        case .canceled:
            cell?.showStatus(text: Localizable.Transactions.Cell.returned, color: Colors.critical600.color)
        }
    }
}
