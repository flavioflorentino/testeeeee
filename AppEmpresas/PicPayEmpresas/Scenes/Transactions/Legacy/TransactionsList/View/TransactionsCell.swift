import AssetsKit
import SnapKit
import UI
import UIKit

protocol TransactionsCellDisplay: AnyObject {
    func setup(_ cellItem: TransactionsCellItem)
    func hideStatus()
    func showStatus(text: String, color: UIColor)
    func startLoading()
    func stopLoading()
}

private extension TransactionsCell.Layout {
    enum PhotoImage {
        static let size = CGSize(width: 40, height: 40)
        static let cornerRadius: CGFloat = 20
    }
}

final class TransactionsCell: UITableViewCell {
    private typealias Localizable = Strings.Transaction
    fileprivate enum Layout { }
    
    private lazy var photoImageView: UIImageView = {
        let view = UIImageView()
        view.layer.masksToBounds = true
        view.layer.cornerRadius = Layout.PhotoImage.cornerRadius
        return view
    }()

    private lazy var timestampOperatorLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale600())
        label.numberOfLines = 1
        return label
    }()

    private lazy var userNameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        label.numberOfLines = 1
        label.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        return label
    }()

    private lazy var timestampUserNameStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [timestampOperatorLabel, userNameLabel])
        stackView.spacing = Spacing.base00
        stackView.axis = .vertical
        stackView.alignment = .leading
        return stackView
    }()

    private lazy var statusLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .right)
        label.numberOfLines = 1
        return label
    }()

    private lazy var balanceLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .right)
        label.numberOfLines = 1
        return label
    }()

    private lazy var statusPriceStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [statusLabel, balanceLabel])
        stackView.spacing = Spacing.base00
        stackView.axis = .vertical
        return stackView
    }()

    private lazy var overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.opacity = 0.85
        return view
    }()
    
    private lazy var activityIndicator = UIActivityIndicatorView(style: .gray)

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        photoImageView.image = nil
        userNameLabel.text = ""
        timestampOperatorLabel.text = ""
        statusLabel.text = ""
        balanceLabel.text = ""
        stopLoading()
    }
}

extension TransactionsCell: ViewConfiguration {
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }

    func buildViewHierarchy() {
        contentView.addSubview(photoImageView)
        contentView.addSubview(timestampUserNameStackView)
        contentView.addSubview(statusPriceStackView)
    }
    
    func setupConstraints() {
        photoImageView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.centerY.equalToSuperview()
            $0.size.equalTo(Layout.PhotoImage.size)
        }

        timestampUserNameStackView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.equalTo(photoImageView.snp.trailing).offset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }

        statusPriceStackView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.greaterThanOrEqualTo(timestampUserNameStackView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
    }
}

extension TransactionsCell: TransactionsCellDisplay {
    func setup(_ cellItem: TransactionsCellItem) {
        timestampOperatorLabel.text = cellItem.timestampOperator
        userNameLabel.text = cellItem.userName
        balanceLabel.text = cellItem.balance
        photoImageView.sd_setImage(with: URL(string: cellItem.photoUrl), placeholderImage: Assets.iluPersonPlaceholder.image)
    }

    func showStatus(text: String, color: UIColor) {
        statusLabel.isHidden = false
        statusLabel.text = text
        statusLabel.textColor = color
    }

    func hideStatus() {
        statusLabel.isHidden = true
        statusLabel.text = ""
    }

    func startLoading() {
        contentView.addSubview(activityIndicator)
        contentView.addSubview(overlayView)

        overlayView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        activityIndicator.snp.makeConstraints {
            $0.centerY.centerX.equalToSuperview()
        }

        activityIndicator.startAnimating()
    }

    func stopLoading() {
        activityIndicator.removeFromSuperview()
        overlayView.removeFromSuperview()
    }
}
