import PIX
import UI
import UIKit

enum TransactionsListAction: Equatable {
    case presentPixReceipt(_ transctionId: Int, isRefundAllowed: Bool)
    case returnPixPayment(_ transctionId: Int)
    case modalError(_ message: String)
}

protocol TransactionsListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: TransactionsListAction)
}

final class TransactionsListCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    private typealias Localizable = Strings.Transaction.Transactions

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - TransactionsListCoordinating
extension TransactionsListCoordinator: TransactionsListCoordinating {
    func perform(action: TransactionsListAction) {
        switch action {
        case let .presentPixReceipt(id, isRefundAllowed):
            guard let navigationController = viewController?.navigationController else {
                return
            }

            let coordinator = PIXBizFlowCoordinator(with: navigationController)

            coordinator.perform(
                flow: .receipt(transactionId: "\(id)", isRefundAllowed: isRefundAllowed)
            )
        case .returnPixPayment(let id):
            guard let navigationController = viewController?.navigationController else {
                return
            }

            let coordinator = PIXBizFlowCoordinator(with: navigationController)

            coordinator.perform(
                flow: .cashout(action: .refund(transactionId: "\(id)"))
            )
        case .modalError(let message):
            errorPopUp(message)
        }
    }
}

private extension TransactionsListCoordinator {
    func errorPopUp(_ message: String) {
        let action = PopupAction(title: Localizable.Error.action, style: .tryAgain)
        let popUp = UI.PopupViewController(
            title: Localizable.Error.title,
            description: message,
            image: Assets.errorIcon.image
        )

        popUp.hideCloseButton = true
        popUp.addAction(action)
        viewController?.showPopup(popUp)
    }
}
