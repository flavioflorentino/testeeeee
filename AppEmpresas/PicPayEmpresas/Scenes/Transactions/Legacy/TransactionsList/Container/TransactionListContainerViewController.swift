import SnapKit
import UI
import UIKit

protocol TransactionListContainerDisplaying: AnyObject {
    func displayActiveList(_ tabOption: TransactionTabOption)
    func displaySegmentedControl()
}

private extension TransactionListContainerViewController.Layout {
    enum SegmentedControl {
        static let height: CGFloat = 50.0
    }
}

final class TransactionListContainerViewController: ViewController<TransactionListContainerInteracting, UIView> {
    private typealias Localizable = Strings.Transaction.Transactions

    fileprivate enum Layout { }
    
    private lazy var historyButton = UIBarButtonItem(
        title: Localizable.history,
        style: .plain,
        target: self,
        action: #selector(historyButtonTouched)
    )
    
    private lazy var segmentedControl: CustomSegmentedControl = {
        let segmentedControl = CustomSegmentedControl(buttonTitles: [Localizable.Segmented.user, Localizable.Segmented.pix])
        segmentedControl.textColor = Colors.grayscale500.color
        segmentedControl.selectorViewColor = Colors.branding600.color
        segmentedControl.selectorTextColor = Colors.branding600.color
        segmentedControl.delegate = self
        segmentedControl.backgroundColor = .clear
        return segmentedControl
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var userListViewController: TransactionsListViewController = {
        TransactionsListFactory.make(type: .user)
    }()
    
    private lazy var pixListViewController: TransactionsListViewController = {
        TransactionsListFactory.make(type: .pix)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interactor.setupHeader()
        interactor.setupActiveList()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceDefault())
    }

    override func buildViewHierarchy() {
        view.addSubview(containerView)
        
        addViewController(userListViewController)
        addViewController(pixListViewController)
    }
    
    override func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        title = Localizable.title
        navigationItem.rightBarButtonItem = historyButton
    }
}

// MARK: - Objc Actions

@objc
private extension TransactionListContainerViewController {
    func historyButtonTouched() {
        interactor.historyAction()
    }
}

// MARK: - Private Methods

private extension TransactionListContainerViewController {
    func addViewController(_ viewController: UIViewController) {
        addChild(viewController)
        containerView.addSubview(viewController.view)
        viewController.didMove(toParent: self)
        
        viewController.view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

// MARK: - TransactionListContainerDisplaying

extension TransactionListContainerViewController: TransactionListContainerDisplaying {
    func displayActiveList(_ tabOption: TransactionTabOption) {
        segmentedControl.setIndex(index: tabOption.rawValue)
        changeToIndex(index: tabOption.rawValue)
    }

    func displaySegmentedControl() {
        view.addSubview(segmentedControl)

        segmentedControl.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.height.equalTo(Layout.SegmentedControl.height)
            $0.leading.trailing.equalToSuperview()
        }

        containerView.snp.remakeConstraints {
            $0.top.equalTo(segmentedControl.snp.bottom)
            $0.bottom.leading.trailing.equalToSuperview()
        }
    }
}

// MARK: - CustomSegmentedControlDelegate

extension TransactionListContainerViewController: CustomSegmentedControlDelegate {
    func changeToIndex(index: Int) {
        let viewToShow: UIView
        let viewToHide: UIView
        
        if index == TransactionContainerListType.user.rawValue {
            viewToShow = userListViewController.view
            viewToHide = pixListViewController.view
        } else {
            viewToShow = pixListViewController.view
            viewToHide = userListViewController.view
        }
        
        containerView.bringSubviewToFront(viewToShow)
        viewToShow.isHidden = false
        viewToHide.isHidden = true
    }
}
