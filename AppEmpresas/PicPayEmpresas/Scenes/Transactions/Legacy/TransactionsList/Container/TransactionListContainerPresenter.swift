import Foundation

protocol TransactionListContainerPresenting: AnyObject {
    var viewController: TransactionListContainerDisplaying? { get set }
    func presentSegmentedControl()
    func presentActiveList(_ tabOption: TransactionTabOption)
    func didNextStep(action: TransactionListContainerAction)
}

final class TransactionListContainerPresenter {
    private let coordinator: TransactionListContainerCoordinating
    weak var viewController: TransactionListContainerDisplaying?

    init(coordinator: TransactionListContainerCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - TransactionListContainerPresenting
extension TransactionListContainerPresenter: TransactionListContainerPresenting {
    func presentSegmentedControl() {
        viewController?.displaySegmentedControl()
    }

    func presentActiveList(_ tabOption: TransactionTabOption) {
        viewController?.displayActiveList(tabOption)
    }
    
    func didNextStep(action: TransactionListContainerAction) {
        coordinator.perform(action: action)
    }
}
