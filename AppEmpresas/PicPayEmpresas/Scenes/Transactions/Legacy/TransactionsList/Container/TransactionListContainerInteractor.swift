import Foundation
import LegacyPJ

enum TransactionContainerListType: Int, Equatable {
    case user, pix
}

protocol TransactionListContainerInteracting: AnyObject {
    func setupHeader()
    func setupActiveList()
    func historyAction()
}

final class TransactionListContainerInteractor {
    typealias Dependencies = HasAuthManager
    private let dependencies: Dependencies
    private let presenter: TransactionListContainerPresenting
    private let transactionsDescriptor: TransactionsDescriptor

    init(presenter: TransactionListContainerPresenting, descriptor: TransactionsDescriptor, dependencies: Dependencies) {
        self.presenter = presenter
        self.transactionsDescriptor = descriptor
        self.dependencies = dependencies
    }
}

// MARK: - TransactionListContainerInteracting
extension TransactionListContainerInteractor: TransactionListContainerInteracting {
    func setupHeader() {
        guard transactionsDescriptor.displayHeader != .title else { return }
        presenter.presentSegmentedControl()
    }

    func setupActiveList() {
        presenter.presentActiveList(transactionsDescriptor.selectedTab)
    }
    
    func historyAction() {
        presenter.didNextStep(action: .history)
    }
}
