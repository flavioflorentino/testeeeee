import UIKit

enum TransactionListContainerAction {
    case history
}

protocol TransactionListContainerCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: TransactionListContainerAction)
}

final class TransactionListContainerCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - TransactionListContainerCoordinating
extension TransactionListContainerCoordinator: TransactionListContainerCoordinating {
    func perform(action: TransactionListContainerAction) {
        switch action {
        case .history:
            let controller: SummaryViewController = ViewsManager.instantiateViewController(.transaction)
            viewController?.pushViewController(controller)
        }
    }
}
