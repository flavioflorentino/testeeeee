import UIKit

enum TransactionListContainerFactory {
    static func make(_ descriptor: TransactionsDescriptor) -> TransactionListContainerViewController {
        let container = DependencyContainer()
        let coordinator: TransactionListContainerCoordinating = TransactionListContainerCoordinator()
        let presenter: TransactionListContainerPresenting = TransactionListContainerPresenter(coordinator: coordinator)
        let interactor = TransactionListContainerInteractor(presenter: presenter, descriptor: descriptor, dependencies: container)
        let viewController = TransactionListContainerViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
