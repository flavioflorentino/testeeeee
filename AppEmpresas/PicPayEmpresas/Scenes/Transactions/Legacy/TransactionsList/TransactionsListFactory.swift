import UIKit

enum TransactionsListFactory {
    static func make(type: TransactionContainerListType) -> TransactionsListViewController {
        let container = DependencyContainer()
        let service: TransactionsListServicing = TransactionsListService(dependencies: container)
        let coordinator: TransactionsListCoordinating = TransactionsListCoordinator(dependencies: container)
        let presenter: TransactionsListPresenting = TransactionsListPresenter(coordinator: coordinator, dependencies: container)
        let interactor = TransactionsListInteractor(service: service, presenter: presenter, dependencies: container, type: type)
        let viewController = TransactionsListViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
