import LegacyPJ
import UI
import UIKit

protocol TransactionsListDisplaying: AnyObject {
    func displayTransactions(_ transactions: [Transaction])
    func prepareToReload()
    func showAlert(_ transaction: Transaction, indexPath: IndexPath)
    func reloadTransactions(_ transactions: [Transaction])
    func startLoading(at indexPath: IndexPath)
    func stopLoading(at indexPath: IndexPath)
}

private extension TransactionsListViewController.Layout {
    enum TableView {
        static let estimatedRowHeight: CGFloat = 80
    }
}

final class TransactionsListViewController: ViewController<TransactionsListInteracting, UIView>, LoadingViewProtocol {
    private typealias Localizable = Strings.Transaction
    fileprivate enum Layout { }

    lazy var loadingView = LoadingView()
    
    private lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(refreshTransactions), for: .valueChanged)
        return control
    }()

    private lazy var dataSource: TableViewDataSource<Int, Transaction> = {
        let dataSource = TableViewDataSource<Int, Transaction>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, transaction -> TransactionsCell? in
            let cellIdentifier = TransactionsCell.identifier
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TransactionsCell
            let presenter: TransactionsCellPresenting = TransactionsCellPresenter()
            presenter.cell = cell
            presenter.setup(transaction)
            self.interactor.fetchMoreItensIfNeeded(indexPath)
            return cell
        }
        return dataSource
    }()

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.estimatedRowHeight = Layout.TableView.estimatedRowHeight
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.tableFooterView = UIView()
        tableView.refreshControl = refreshControl
        tableView.delegate = self
        tableView.register(
            TransactionsCell.self,
            forCellReuseIdentifier: TransactionsCell.identifier
        )
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.initialFetch()
        startLoadingView()
    }

    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }

    override func configureViews() { 
        title = Localizable.Transactions.title
        tableView.dataSource = dataSource
        setupNavigationBar()
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

private extension TransactionsListViewController {
    func setupNavigationBar() {
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceDefault())
    }
}

@objc
extension TransactionsListViewController {
    func refreshTransactions() {
        interactor.refresh()
    }

    func transactionsHistory() {
        let controller: SummaryViewController = ViewsManager.instantiateViewController(.transaction)
        pushViewController(controller)
    }
}

extension TransactionsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        interactor.didSelectTransaction(at: indexPath)
    }
}

// MARK: - TransactionsListDisplaying
extension TransactionsListViewController: TransactionsListDisplaying {
    func displayTransactions(_ transactions: [Transaction]) {
        refreshControl.endRefreshing()
        stopLoadingView()
        dataSource.add(items: transactions, to: 0)
    }

    func prepareToReload() {
        dataSource.remove(section: 0)
    }

    func reloadTransactions(_ transactions: [Transaction]) {
        dataSource.update(items: transactions, from: 0)
    }

    func showAlert(_ transaction: Transaction, indexPath: IndexPath) {
        // todo - refatorar usando componentes do UI asap
        let alert = UIAlertController(title: "\n\n\n", message: nil, preferredStyle: .actionSheet)

        let viewFrame = CGRect(x: 0.0, y: 10.0, width: view.frame.size.width - 20, height: 60)
        let transactionView = TransactionView(frame: viewFrame)
        transactionView.backgroundColor = .clear
        transactionView.layer.cornerRadius = 10.0
        alert.view.addSubview(transactionView)
        transactionView.configure(transaction)

        createActions(for: transaction, indexPath: indexPath).forEach({ alert.addAction($0) })

        present(alert, animated: true)
        alert.view.setNeedsLayout()
    }

    func startLoading(at indexPath: IndexPath) {    
        if let cell = tableView.cellForRow(at: indexPath) as? TransactionsCell {
            cell.startLoading()
        }
    }

    func stopLoading(at indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? TransactionsCell {
            cell.stopLoading()
        }
    }
}

private extension TransactionsListViewController {
    func createActions(for transaction: Transaction, indexPath: IndexPath) -> [UIAlertAction] {
        var actions: [UIAlertAction] = []

        if transaction.isPix {
            actions.append(UIAlertAction(
                title: Strings.Transaction.Alert.Button.receipt,
                style: .default,
                handler: { [weak self] _ in
                    self?.interactor.openPixReceipt(transaction)
                })
            )

            if transaction.status != .canceled {
                actions.append(UIAlertAction(
                    title: Strings.Transaction.Alert.Button.returnPix,
                    style: .destructive,
                    handler: { [weak self] _ in
                        self?.interactor.returnPixPayment(transaction)
                    })
                )
            }
        } else {
            let action = UIAlertAction(
                title: Strings.Transaction.Alert.Button.return,
                style: .destructive,
                handler: { [weak self] _ in
                    self?.interactor.cancelTransaction(indexPath)
                })
            action.isEnabled = transaction.status != .canceled
            actions.append(action)
        }
        
        actions.append(UIAlertAction(
            title: Strings.Default.cancel,
            style: .cancel
        ))

        return actions
    }
}
