import Foundation

enum TransactionHeaderOption: Equatable { 
    case title, tabs
}

enum TransactionTabOption: Int, Equatable {
    case picPayTransactions, pixTransactions, linkTransactions
    
    var description: String {
        switch self {
        case .picPayTransactions:
            return Strings.Home.Transaction.Tab.picPay
        case .pixTransactions:
            return Strings.Home.Transaction.Tab.pix
        case .linkTransactions:
            return Strings.Home.Transaction.Tab.link
        }
    }
}

struct TransactionsDescriptor: Equatable {
    let displayHeader: TransactionHeaderOption
    let selectedTab: TransactionTabOption
    let seller: Seller
    
    init(displayHeader: TransactionHeaderOption, seller: Seller, selectedTab: TransactionTabOption) {
        self.displayHeader = displayHeader
        self.selectedTab = selectedTab
        self.seller = seller
    }
}
