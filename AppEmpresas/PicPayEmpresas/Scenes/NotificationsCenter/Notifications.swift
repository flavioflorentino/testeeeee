import Foundation

struct Notifications: Decodable {
    let notifications: [Message]
}

struct Message: Decodable {
    private enum CodingKeys: String, CodingKey {
        case deeplink, message, template, created, seen
        case appVersion = "app_version"
    }
    
    let deeplink: String?
    let appVersion: String?
    let template: String
    let message: String?
    let created: String?
    let seen: Bool
    
    init(deeplink: String?,
         appVersion: String?,
         template: String,
         message: String?,
         created: String?,
         seen: Bool
    ) {
        self.deeplink = deeplink
        self.appVersion = appVersion
        self.template = template
        self.message = message
        self.created = created
        self.seen = seen
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        deeplink = try container.decodeIfPresent(String.self, forKey: .deeplink)
        appVersion = try container.decodeIfPresent(String.self, forKey: .appVersion)
        template = try container.decode(String.self, forKey: .template)
        message = try container.decodeIfPresent(String.self, forKey: .message)
        created = try container.decodeIfPresent(String.self, forKey: .created)
        seen = try container.decodeIfPresent(Bool.self, forKey: .seen) ?? false
    }
}
