import Core
import Foundation
import UI

protocol NotificationCenterPresenting: AnyObject {
    var viewController: NotificationCenterDisplay? { get set }
    func updateTable(notifications: [Message])
    func handleError(error: String)
    func startLoad()
    func stopLoad()
    func didNextStep(action: NotificationCenterAction)
}

final class NotificationCenterPresenter: NotificationCenterPresenting {
    func didNextStep(action: NotificationCenterAction) {
        coordinator.perform(action: action)
    }
    
    private let coordinator: NotificationCenterCoordinating
    weak var viewController: NotificationCenterDisplay?

    init(coordinator: NotificationCenterCoordinating) {
        self.coordinator = coordinator
    }
    
    func updateTable(notifications: [Message]) {
        let cells = notifications.map {
            NotificationCenterViewCellPresenter(
                template: $0.template,
                message: $0.message,
                seen: $0.seen,
                created: $0.created
            )
        }
        viewController?.updateTable(notifications: cells)
    }
    
    func handleError(error: String) {
        viewController?.showError()
    }
    
    func startLoad() {
        viewController?.startLoad()
    }
    
    func stopLoad() {
        viewController?.stopLoad()
    }
}
