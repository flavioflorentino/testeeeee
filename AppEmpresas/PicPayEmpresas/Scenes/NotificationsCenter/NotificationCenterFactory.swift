import Foundation

enum NotificationCenterFactory {
    static func make() -> NotificationCenterViewController {
        let service: NotificationCenterServicing = NotificationCenterService(DependencyContainer())
        let coordinator: NotificationCenterCoordinating = NotificationCenterCoordinator()
        let presenter: NotificationCenterPresenting = NotificationCenterPresenter(coordinator: coordinator)
        let viewModel = NotificationCenterViewModel(service: service, presenter: presenter)
        let viewController = NotificationCenterViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
