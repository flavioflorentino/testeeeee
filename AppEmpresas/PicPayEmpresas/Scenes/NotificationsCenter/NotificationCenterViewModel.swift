import Core

protocol NotificationCenterViewModelInputs: AnyObject {
    func loadNotifications(wasCalledFromRefreshControl: Bool)
    func didTapCell(at row: Int)
}

final class NotificationCenterViewModel {
    private let service: NotificationCenterServicing
    private let presenter: NotificationCenterPresenting
    private var messages: [Message] = []

    init(service: NotificationCenterServicing, presenter: NotificationCenterPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

extension NotificationCenterViewModel: NotificationCenterViewModelInputs {
    func didTapCell(at row: Int) {
        guard let deeplink = messages[row].deeplink else { return }
        presenter.didNextStep(action: .tapCell(deepLink: deeplink))
    }
    
    func loadNotifications(wasCalledFromRefreshControl: Bool) {
        if !wasCalledFromRefreshControl {
            presenter.startLoad()
        }
        
        service.getNotifications { [weak self] result in
            self?.presenter.stopLoad()
            switch result {
            case .success(let notifications):
                self?.presenter.updateTable(notifications: notifications.notifications)
                self?.messages = notifications.notifications
            case .failure(let error):
                self?.presenter.handleError(error: error.localizedDescription)
            }
        }
    }
}
