import UIKit

enum NotificationCenterAction {
    case tapCell(deepLink: String)
}

protocol NotificationCenterCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: NotificationCenterAction)
}

final class NotificationCenterCoordinator: NotificationCenterCoordinating {
    weak var viewController: UIViewController?
    private let prefixSeparator = "picpaybiz://picpaybiz/"
    private let sufixWebView = "?url="
    
    func perform(action: NotificationCenterAction) {
        switch action {
        case .tapCell(let deeplink):
            guard let multiPath = deeplink.components(separatedBy: prefixSeparator).last else {
                return
            }
            
            let listPath = multiPath.components(separatedBy: sufixWebView)
            let parameters = listPath.last
            
            guard let path = listPath.first else {
                return
            }
            
            if path.split(separator: "/").first == "pix" {
                let router = RouterApp.pix
                router.match(navigation: viewController?.navigationController, url: URL(string: deeplink))
                return
            }
            
            let router = RouterApp(rawValue: path)
            router?.match(
                navigation: viewController?.navigationController,
                parameters: parameters
            )
        }
    }
}
