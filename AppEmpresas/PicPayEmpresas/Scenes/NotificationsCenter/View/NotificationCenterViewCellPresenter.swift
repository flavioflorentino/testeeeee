import Foundation

protocol NotificationCenterViewCellPresenting {
    var template: String { get }
    var message: String? { get }
    var seen: Bool { get }
    var created: String? { get }
}

struct NotificationCenterViewCellPresenter: NotificationCenterViewCellPresenting {
    var template: String
    var message: String?
    var seen: Bool
    var created: String?
}
