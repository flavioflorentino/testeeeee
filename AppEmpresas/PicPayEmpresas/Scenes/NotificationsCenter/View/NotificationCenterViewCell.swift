import SnapKit
import UI
import UIKit

extension NotificationCenterViewCell.Layout {
    enum Constraints {
        static let leadingSpacing: CGFloat = 11
        static let bottomSpacing: CGFloat = 12
        static let minHeightDescripton: CGFloat = 18
    }
    
    enum Font {
        static let description = UIFont.systemFont(ofSize: 14)
        static let moreInfos = UIFont.systemFont(ofSize: 11)
    }
}

final class NotificationCenterViewCell: UITableViewCell, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 4
        label.textColor = Palette.ppColorGrayscale600.color
        label.font = Layout.Font.description
        return label
    }()
    
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = Layout.Font.moreInfos
        return label
    }()
    
    var presenter: NotificationCenterViewCellPresenting? {
        didSet {
            setup()
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        guard
            let presenter = presenter,
            let message = presenter.message?
                .replacingOccurrences(of: "[", with: "<")
                .replacingOccurrences(of: "]", with: ">") else {
            return
        }
        
        let attributeMessage = NSAttributedString(string: message)
            .boldfyWithSystemFont(ofSize: 14, weight: UIFont.Weight.medium.rawValue)
        
        descriptionLabel.attributedText = attributeMessage
        dateLabel.text = presenter.created
        
        backgroundColor = presenter.seen ? Colors.white.color : Colors.business050.color
        separatorInset = UIEdgeInsets(top: 0, left: Spacing.base02, bottom: 0, right: Spacing.base02)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: true)
    }
    
    func setupConstraints() {
        descriptionLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Layout.Constraints.leadingSpacing)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.greaterThanOrEqualTo(Layout.Constraints.minHeightDescripton)
        }
        
        dateLabel.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Layout.Constraints.bottomSpacing)
            $0.leading.equalToSuperview().offset(Layout.Constraints.leadingSpacing)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Layout.Constraints.bottomSpacing)
        }
    }
    
    func buildViewHierarchy() {
        addSubview(descriptionLabel)
        addSubview(dateLabel)
    }
}
