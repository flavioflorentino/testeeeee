import Foundation
import SnapKit
import UI
import UIKit

protocol NotificationCenterDisplay: AnyObject {
    func updateTable(notifications: [NotificationCenterViewCellPresenting])
    func showError()
    func startLoad()
    func stopLoad()
}

final class NotificationCenterViewController: ViewController<NotificationCenterViewModelInputs, UIView> {
    typealias Localizable = Strings.Notificationcenter
    
    private lazy var activityView: UIActivityIndicatorView = {
        if #available(iOS 13.0, *) {
            return UIActivityIndicatorView(style: .large)
        } else {
            return UIActivityIndicatorView(style: .gray)
        }
    }()
    
    private lazy var tableView: UITableView = {
        let table = UITableView()
        table.estimatedRowHeight = 72.0
        table.rowHeight = UITableView.automaticDimension
        table.refreshControl = UIRefreshControl()
        table.refreshControl?.addTarget(self, action: #selector(refreshControlBeganLoading), for: .valueChanged)
        table.separatorStyle = .none
        table.separatorInset = .zero
        table.tableFooterView = UIView()
        table.delegate = self
        table.register(NotificationCenterViewCell.self, forCellReuseIdentifier: NotificationCenterViewCell.identifier)
        return table
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, NotificationCenterViewCellPresenting> = {
        let dataSource = TableViewDataSource<Int, NotificationCenterViewCellPresenting>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, model -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(
                withIdentifier: NotificationCenterViewCell.identifier,
                for: indexPath
            ) as? NotificationCenterViewCell
            cell?.presenter = model
            return cell
        }
        return dataSource
    }()
    
    private lazy var errorViewController: ErrorViewController = {
        let errorViewController = ErrorViewController()
        errorViewController.setErrorView(buttonTitle: Strings.Default.tryAgain)
        errorViewController.delegate = self
        addChild(errorViewController)
        return errorViewController
    }()
    
    private lazy var emptyView: EmptyView = {
        let configuration = EmptyViewConfiguration(
            image: Assets.iluTransactionEmpty.image,
            title: Localizable.notificationsEmptyTitle,
            info: Localizable.notificationsEmptyBody,
            refreshButtonTitle: nil,
            buttonAction: nil
        )
        return EmptyView(configuration: configuration)
    }()
    
    // MARK: - Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetView()
        viewModel.loadNotifications(wasCalledFromRefreshControl: false)
    }
 
    override func buildViewHierarchy() {
        view.addSubview(tableView)
        view.addSubview(activityView)
    }
    
    override func configureViews() {
        title = Localizable.notificationCenterTitle
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        activityView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
}

// MARK: View Model Outputs

extension NotificationCenterViewController: NotificationCenterDisplay {
    func showError() {
        view.addSubview(errorViewController.view)
        errorViewController.didMove(toParent: self)
        errorViewController.view.snp.makeConstraints {
            $0.edges.equalTo(view.compatibleLayoutMargins.asUIEdgeInsets)
        }
    }
    
    func updateTable(notifications: [NotificationCenterViewCellPresenting]) {
        if notifications.isEmpty {
            showEmptyScreen()
            return
        }
        
        emptyView.removeFromSuperview()
        
        tableView.dataSource = dataSource
        tableView.separatorStyle = .singleLine
        dataSource.update(items: notifications, from: 0)
    }
    
    func startLoad() {
        view.bringSubviewToFront(activityView)
        activityView.startAnimating()
    }
    
    func stopLoad() {
        tableView.refreshControl?.endRefreshing()
        activityView.stopAnimating()
    }
}

// MARK: - Private Methods

private extension NotificationCenterViewController {
    func resetView() {
        errorViewController.view.removeFromSuperview()
        emptyView.removeFromSuperview()
        viewModel.loadNotifications(wasCalledFromRefreshControl: false)
    }
    
    func showEmptyScreen() {
        tableView.addSubview(emptyView)
        emptyView.frame = tableView.frame
    }
}

// MARK: - Objc Methods

@objc
private extension NotificationCenterViewController {
    func refreshControlBeganLoading() {
        viewModel.loadNotifications(wasCalledFromRefreshControl: true)
    }
}

// MARK: - ErrorViewDelegate

extension NotificationCenterViewController: ErrorViewDelegate {
    func didTouchConfirmButton() {
        errorViewController.view.removeFromSuperview()
        viewModel.loadNotifications(wasCalledFromRefreshControl: false)
    }
}

// MARK: - UITableViewDelegate

extension NotificationCenterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didTapCell(at: indexPath.row)
    }
}
