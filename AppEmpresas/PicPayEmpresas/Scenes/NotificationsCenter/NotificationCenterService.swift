import Core
import LegacyPJ

protocol NotificationCenterServicing {
    func getNotifications(completion: @escaping (Result<Notifications, ApiError>) -> Void)
}

final class NotificationCenterService: NotificationCenterServicing {
    typealias Dependencies = HasMainQueue & HasAuthManager
    var dependencies: Dependencies?
    
    init(_ dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getNotifications(completion: @escaping (Result<Notifications, ApiError>) -> Void) {
        guard let sellerId = self.dependencies?.authManager.user?.id else {
            completion(.failure(ApiError.bodyNotFound))
            return
        }
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        Core.Api<Notifications>(endpoint: NotificationCenterServiceEndPoint.notifications(
                   sellerId: sellerId))
            .execute(jsonDecoder: decoder) {[weak self] result in
                let mappedResult = result
                    .map { $0.model }
                self?.dependencies?.mainQueue.async {
                    completion(mappedResult)
                }
            }
    }
}
