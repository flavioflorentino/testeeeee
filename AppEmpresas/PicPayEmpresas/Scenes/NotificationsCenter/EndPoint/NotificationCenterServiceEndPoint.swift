import Core

enum NotificationCenterServiceEndPoint {
    case notifications(sellerId: Int)
}

extension NotificationCenterServiceEndPoint: ApiEndpointExposable {
    var path: String {
        "/notification"
    }
    
    var method: HTTPMethod {
        .get
    }
    
    var parameters: [String: Any] {
        switch self {
        case let .notifications(sellerId):
            return ["sellerId": sellerId, "device": "IOS"]
        }
    }
}
