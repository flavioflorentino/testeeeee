import AnalyticsModule
import Foundation

protocol RegistrationChooseProfileViewModelInputs: AnyObject {
    func dontHaveCNPJ()
    func nextStep()
    func trackingDidLoad()
}

final class RegistrationChooseProfileViewModel {
    private let presenter: RegistrationChooseProfilePresenting
    private let dependencies: HasAnalytics = DependencyContainer()

    init(presenter: RegistrationChooseProfilePresenting) {
        self.presenter = presenter
    }
}

extension RegistrationChooseProfileViewModel: RegistrationChooseProfileViewModelInputs {
    func dontHaveCNPJ() {
        dependencies.analytics.log(RegistrationAnalytics.typePRO)
        TrackingManager.trackEventPublicAuth(
            .obBusinessType,
            authEventName: .obMultBusinessType,
            properties: [TrackingManager.TrackingManagerProperties.type.rawValue: "PRO"]
        )
        
        presenter.didNextStep(action: .toPro)
    }
    
    func nextStep() {
        dependencies.analytics.log(RegistrationAnalytics.typeBusiness)
        TrackingManager.trackEventPublicAuth(
            .obBusinessType,
            authEventName: .obMultBusinessType,
            properties: [TrackingManager.TrackingManagerProperties.type.rawValue: "Business"]
        )
        
        presenter.didNextStep(action: .nextScreen)
    }
    
    func trackingDidLoad() {
        dependencies.analytics.log(RegistrationAnalytics.identifyProfile)
    }
}
