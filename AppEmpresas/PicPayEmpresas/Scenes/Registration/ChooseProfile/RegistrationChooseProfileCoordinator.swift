import UIKit

enum RegistrationChooseProfileAction {
    case toPro
    case nextScreen
}

protocol RegistrationChooseProfileCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationChooseProfileAction)
}

final class RegistrationChooseProfileCoordinator: RegistrationChooseProfileCoordinating {
    weak var viewController: UIViewController?
    
    private let model: RegistrationViewModel
    
    init(model: RegistrationViewModel) {
        self.model = model
    }
    
    func perform(action: RegistrationChooseProfileAction) {
        switch action {
        case .toPro:
            let controller: RegistrationProViewController = ViewsManager.instantiateViewController(.registration)
            viewController?.pushViewController(controller)
        case .nextScreen:
            let controller: RegistrationCNPJStepViewController = ViewsManager.instantiateViewController(.registration)
            controller.setup(model: model)
            viewController?.pushViewController(controller)
        }
    }
}
