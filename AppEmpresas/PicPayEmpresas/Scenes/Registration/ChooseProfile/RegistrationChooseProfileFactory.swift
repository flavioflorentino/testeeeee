import Foundation

enum RegistrationChooseProfileFactory {
    static func make(model: RegistrationViewModel) -> RegistrationChooseProfileViewController {
        let coordinator: RegistrationChooseProfileCoordinating = RegistrationChooseProfileCoordinator(model: model)
        let presenter: RegistrationChooseProfilePresenting = RegistrationChooseProfilePresenter(coordinator: coordinator)
        let viewModel = RegistrationChooseProfileViewModel(presenter: presenter)
        let viewController = RegistrationChooseProfileViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
