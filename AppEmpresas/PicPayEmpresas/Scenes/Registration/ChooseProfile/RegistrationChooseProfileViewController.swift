import SnapKit
import UI
import UIKit

private extension RegistrationChooseProfileViewController.Layout {
    enum Button {
        static let titleInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
    }
    
    enum Size {
        static let stackHeight: CGFloat = 168
    }
}

final class RegistrationChooseProfileViewController: ViewController<RegistrationChooseProfileViewModelInputs, UIView> {
    private typealias Localizable = Strings.RegistrationChooseProfile
    fileprivate enum Layout { }
    
    // MARK: - Variables
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.title
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.subTitle
        label.numberOfLines = 3
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale600())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var buttonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var haveCNPJButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(Localizable.haveCNPJtitleButton, for: .normal)
        button.normalBackgrounColor = Colors.branding400.color
        button.normalTitleColor = Colors.white.color
        button.highlightedBackgrounColor = Colors.branding700.color
        button.addTarget(self, action: #selector(nextStepAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var dontHaveCNPJButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(Localizable.dontHaveCNPJTitleButton, for: .normal)
        button.titleLabel?.numberOfLines = 2
        button.titleLabel?.textAlignment = .center
        button.normalBackgrounColor = Colors.branding400.color
        button.normalTitleColor = Colors.white.color
        button.highlightedBackgrounColor = Colors.branding700.color
        button.titleLabel?.numberOfLines = 2
        button.titleLabel?.textAlignment = .center
        button.titleEdgeInsets = Layout.Button.titleInsets
        button.addTarget(self, action: #selector(dontHaveCNPJAction), for: .touchUpInside)
        return button
    }()

    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.trackingDidLoad()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(infoLabel)
        buttonStackView.addArrangedSubview(haveCNPJButton)
        buttonStackView.addArrangedSubview(dontHaveCNPJButton)
        view.addSubview(buttonStackView)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
        
        infoLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base05)
            $0.trailing.equalToSuperview().inset(Spacing.base05)
        }
        
        buttonStackView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.Size.stackHeight)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.grayscale050.color
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceOldRegistration())
    }
}

// MARK: - Private Functions
private extension RegistrationChooseProfileViewController {
    @objc
    private func dontHaveCNPJAction() {
        viewModel.dontHaveCNPJ()
    }
    
    @objc
    private func nextStepAction() {
        viewModel.nextStep()
    }
}

// MARK: View Model Outputs
extension RegistrationChooseProfileViewController: RegistrationChooseProfileDisplay {}
