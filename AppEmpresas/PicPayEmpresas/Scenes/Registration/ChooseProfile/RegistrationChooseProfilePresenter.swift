import Core
import Foundation

protocol RegistrationChooseProfilePresenting: AnyObject {
    var viewController: RegistrationChooseProfileDisplay? { get set }
    func didNextStep(action: RegistrationChooseProfileAction)
}

final class RegistrationChooseProfilePresenter: RegistrationChooseProfilePresenting {
    private let coordinator: RegistrationChooseProfileCoordinating
    weak var viewController: RegistrationChooseProfileDisplay?

    init(coordinator: RegistrationChooseProfileCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: RegistrationChooseProfileAction) {
        coordinator.perform(action: action)
    }
}
