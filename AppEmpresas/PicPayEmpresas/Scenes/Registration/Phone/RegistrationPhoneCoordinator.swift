import UIKit

enum RegistrationPhoneAction {
    case nextScreen
}

protocol RegistrationPhoneCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationPhoneAction)
}

final class RegistrationPhoneCoordinator {
    private let model: RegistrationViewModel
    
    weak var viewController: UIViewController?

    init(model: RegistrationViewModel) {
        self.model = model
    }
}

// MARK: - RegistrationPhoneCoordinating
extension RegistrationPhoneCoordinator: RegistrationPhoneCoordinating {
    func perform(action: RegistrationPhoneAction) {
        switch action {
        case .nextScreen:
            let controller: RegistrationAddressStepViewController = ViewsManager.instantiateViewController(.registration)
            controller.setup(model: model)
            controller.addressType = .personal
            viewController?.pushViewController(controller)
        }
    }
}
