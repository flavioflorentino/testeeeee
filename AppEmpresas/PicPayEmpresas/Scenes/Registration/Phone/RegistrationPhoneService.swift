import Foundation

protocol RegistrationPhoneServicing {
    func registerPhone(model: PhoneRegisterModel)
}

final class RegistrationPhoneService {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    private let model: RegistrationViewModel

    init(dependencies: Dependencies, model: RegistrationViewModel) {
        self.dependencies = dependencies
        self.model = model
    }
}

// MARK: - RegistrationPhoneServicing
extension RegistrationPhoneService: RegistrationPhoneServicing {
    func registerPhone(model: PhoneRegisterModel) {
        /*
         Futuramente sera implementada uma requests para novos endpoints. O atual é enviado uma vez no final do
         cadastro, por isso estou armazenando local e alimentando um modelo legado.
         */
        
        self.model.account.ddd = model.ddd
        self.model.account.phone = model.phone
        
        if let ddd = model.dddOpcinal {
            self.model.account.mobilePhoneDdd = ddd
        }
        
        if let phone = model.phoneOpcional {
            self.model.account.mobilePhone = phone
        }
    }
}

struct PhoneRegisterModel {
    let ddd: String
    let phone: String
    let dddOpcinal: String?
    let phoneOpcional: String?
}
