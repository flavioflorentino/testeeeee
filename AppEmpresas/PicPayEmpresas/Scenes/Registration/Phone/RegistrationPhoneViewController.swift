import UI
import UIKit
import Validator

protocol RegistrationPhoneDisplay: AnyObject {
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)])
    func submitValues()
}

private extension RegistrationPhoneViewController.Layout {
    enum DDDTextField {
        static let width: CGFloat = 80
        static let maxLeght = 2
    }
    
    enum PhoneTextField {
        static let maxLeght = 9
    }
}

final class RegistrationPhoneViewController: ViewController<RegistrationPhoneViewModelInputs, UIView> {
    private typealias Localizable = RegistrationPhoneLocalizable
    fileprivate enum Layout { }
    
    private var inputs: [UIPPFloatingTextField] = []
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        label.text = Localizable.title.text
        return label
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale300())
            .with(\.textAlignment, .center)
        label.text = Localizable.subTitle.text
        return label
    }()
    
    private lazy var dddRequiredTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.keyboardType = .numberPad
        textField.placeholder = Localizable.dddPlaceHolder.text
        inputs.append(textField)
        return textField
    }()
    
    private lazy var phoneRequiredTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.keyboardType = .numberPad
        textField.placeholder = Localizable.cellPhonePlaceHolder.text
        inputs.append(textField)
        return textField
    }()
    
    private lazy var dddOpcionalTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.keyboardType = .numberPad
        textField.placeholder = Localizable.dddPlaceHolder.text
        inputs.append(textField)
        return textField
    }()
    
    private lazy var phoneOpcionalTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.keyboardType = .numberPad
        textField.placeholder = Localizable.phonePlaceHolder.text
        inputs.append(textField)
        return textField
    }()
    
    private lazy var  confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.Default.continue, for: .normal)
        button.addTarget(self, action: #selector(confirmAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var dddNumberMask: TextFieldMasker = {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.ddd)
        return TextFieldMasker(textMask: mask)
    }()
    
    private lazy var cellPhoneNumberMaks: TextFieldMasker = {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cellPhone)
        return TextFieldMasker(textMask: mask)
    }()
    
    private lazy var phoneNumberMaks: TextFieldMasker = {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.phone)
        return TextFieldMasker(textMask: mask)
    }()
    
     override func viewDidLoad() {
        super.viewDidLoad()
        TrackingManager.trackEventPublicAuth(.obPhoneNumbers, authEventName: .obMultPhoneNumbers, properties: nil)
    }

     override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(infoLabel)
        view.addSubview(dddRequiredTextField)
        view.addSubview(phoneRequiredTextField)
        view.addSubview(dddOpcionalTextField)
        view.addSubview(phoneOpcionalTextField)
        view.addSubview(confirmButton)
    }
    
     override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.centerX.equalToSuperview().offset(Spacing.base01)
        }
        
        infoLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.trailing.equalToSuperview().inset(Spacing.base04)
        }
        
        dddRequiredTextField.snp.makeConstraints {
            $0.top.equalTo(infoLabel.snp.bottom).offset(Spacing.base05)
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.width.equalTo(Layout.DDDTextField.width)
        }

        phoneRequiredTextField.snp.makeConstraints {
            $0.centerY.equalTo(dddRequiredTextField.snp.centerY)
            $0.leading.equalTo(dddRequiredTextField.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base04)
        }

        dddOpcionalTextField.snp.makeConstraints {
            $0.top.equalTo(dddRequiredTextField.snp.bottom).offset(Spacing.base05)
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.width.equalTo(Layout.DDDTextField.width)
        }

        phoneOpcionalTextField.snp.makeConstraints {
            $0.centerY.equalTo(dddOpcionalTextField.snp.centerY)
            $0.leading.equalTo(dddOpcionalTextField.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base04)
        }
        
        confirmButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.equalToSuperview().inset(Spacing.base04)
        }
    }

     override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        setupTextFieldMaks()
        setupTextFieldRules()
        addObservers()
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

extension RegistrationPhoneViewController {
    @objc
    private func confirmAction() {
        viewModel.updateFields(textFields: inputs)
        viewModel.validateSubmit(textFields: inputs)
    }
}

// MARK: - Oberserver Functions
@objc
extension RegistrationPhoneViewController {
    func keyboardWillShow(_ notification: NSNotification) {
        confirmButton.snp.remakeConstraints {
            $0.top.equalTo(phoneOpcionalTextField.snp.bottom).offset(Spacing.base09)
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().inset(Spacing.base03)
        }
    }
    
    func keyboardWillHide(_ notification: NSNotification) {
        confirmButton.snp.remakeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.equalToSuperview().inset(Spacing.base04)
        }
    }
}

extension RegistrationPhoneViewController {
    private func setupTextFieldMaks() {
        dddNumberMask.bind(to: dddRequiredTextField)
        dddNumberMask.bind(to: dddOpcionalTextField)
        cellPhoneNumberMaks.bind(to: phoneRequiredTextField)
        phoneNumberMaks.bind(to: phoneOpcionalTextField)
    }

    private func setupTextFieldRules() {
        var dddRule = ValidationRuleSet<String>()
        dddRule.add(rule: ValidationRuleLength(min: 2, error: Localizable.dddErrorMessage))
        dddRequiredTextField.validationRules = dddRule

        var cellPhoneRules = ValidationRuleSet<String>()
        cellPhoneRules.add(rule: ValidationRuleLength(min: 9, error: Localizable.phoneErrorMessage))
        phoneRequiredTextField.validationRules = cellPhoneRules
    }
}

// MARK: RegistrationPhoneDisplay
extension RegistrationPhoneViewController: RegistrationPhoneDisplay {
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)]) {
        fields.forEach {
            if let message = $0.message, !message.isEmpty {
                $0.textField.lineColor = Colors.branding300.color
            }
            $0.textField.errorMessage = $0.message
        }
    }
    
    func submitValues() {
        guard
            let ddd = dddRequiredTextField.text,
            let phone = phoneRequiredTextField.text else {
            return
        }
        
        viewModel.submitValues(
            ddd: ddd,
            phone: phone,
            dddOpcional: dddOpcionalTextField.text,
            phoneOpcional: phoneOpcionalTextField.text
        )
    }
}
