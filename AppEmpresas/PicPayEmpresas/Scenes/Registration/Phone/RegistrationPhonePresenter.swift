import Foundation

protocol RegistrationPhonePresenting: AnyObject {
    var viewController: RegistrationPhoneDisplay? { get set }
    func didNextStep(action: RegistrationPhoneAction)
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)])
    func validateSubmit(value: Bool)
}

final class RegistrationPhonePresenter: RegistrationPhonePresenting {
    private let coordinator: RegistrationPhoneCoordinating
    weak var viewController: RegistrationPhoneDisplay?

    init(coordinator: RegistrationPhoneCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - RegistrationPhonePresenting
extension RegistrationPhonePresenter {    
    func didNextStep(action: RegistrationPhoneAction) {
        coordinator.perform(action: action)
    }
    
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)]) {
        viewController?.updateFields(fields: fields)
    }
    
    func validateSubmit(value: Bool) {
        if value {
            viewController?.submitValues()
        }
    }
}
