import Foundation

protocol RegistrationPhoneViewModelInputs: AnyObject {
    func updateFields(textFields: [UIPPFloatingTextField])
    func validateSubmit(textFields: [UIPPFloatingTextField])
    func submitValues(ddd: String, phone: String, dddOpcional: String?, phoneOpcional: String?)
}

final class RegistrationPhoneViewModel {
    private let service: RegistrationPhoneServicing
    private let presenter: RegistrationPhonePresenting

    init(service: RegistrationPhoneServicing, presenter: RegistrationPhonePresenting) {
        self.service = service
        self.presenter = presenter
    }
}

// MARK: - RegistrationPhoneViewModelInputs
extension RegistrationPhoneViewModel: RegistrationPhoneViewModelInputs {
    func updateFields(textFields: [UIPPFloatingTextField]) {
        let result: [(message: String?, textField: UIPPFloatingTextField)] = textFields.map {
            switch $0.validate() {
            case .invalid(let error):
                return(error.first?.message, $0)
            default:
                return("", $0)
            }
        }
        
        presenter.updateFields(fields: result)
    }
    
    func validateSubmit(textFields: [UIPPFloatingTextField]) {
        let result = textFields.allSatisfy { $0.validate().isValid }
        presenter.validateSubmit(value: result)
    }
    
    func submitValues(ddd: String, phone: String, dddOpcional: String?, phoneOpcional: String?) {
        let model = PhoneRegisterModel(ddd: ddd, phone: phone, dddOpcinal: dddOpcional, phoneOpcional: phoneOpcional)
        service.registerPhone(model: model)
        trackingOnFinish(ddd: ddd, phone: phone)
        presenter.didNextStep(action: .nextScreen)
    }
}

// MARK: Private Functions
private extension RegistrationPhoneViewModel {
    func trackingOnFinish(ddd: String, phone: String) {
        let properties = [
            TrackingManager.TrackingManagerProperties.ddd.rawValue: ddd,
            TrackingManager.TrackingManagerProperties.landline.rawValue: "YES"
        ]
        
        TrackingManager.trackEventPublicAuth(.obSettedPhoneNumbers, authEventName: .obMultSettedPhoneNumbers, properties: properties)
    }
}
