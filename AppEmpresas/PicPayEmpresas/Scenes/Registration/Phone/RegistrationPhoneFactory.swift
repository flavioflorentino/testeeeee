import Foundation

enum RegistrationPhoneFactory {
    static func make(model: RegistrationViewModel) -> RegistrationPhoneViewController {
        let container = DependencyContainer()
        let service: RegistrationPhoneServicing = RegistrationPhoneService(dependencies: container, model: model)
        let coordinator: RegistrationPhoneCoordinating = RegistrationPhoneCoordinator(model: model)
        let presenter: RegistrationPhonePresenting = RegistrationPhonePresenter(coordinator: coordinator)
        let viewModel = RegistrationPhoneViewModel(service: service, presenter: presenter)
        let viewController = RegistrationPhoneViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
