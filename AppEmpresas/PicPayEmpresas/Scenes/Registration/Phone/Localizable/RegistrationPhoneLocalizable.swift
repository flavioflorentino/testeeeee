import Foundation
import Validator

enum RegistrationPhoneLocalizable: String, Localizable, ValidationError {
    case title
    case subTitle
    case dddPlaceHolder
    case cellPhonePlaceHolder
    case phonePlaceHolder
    case dddErrorMessage
    case phoneErrorMessage
    
    var key: String {
        rawValue
    }
    
    var message: String {
        text
    }
    
    var file: LocalizableFile {
        .registrationPhoneLocalizable
    }
}
