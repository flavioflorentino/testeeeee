import Foundation

protocol LegalPersonInfoPresenting: AnyObject {
    var viewController: LegalPersonInfoDisplaying? { get set }
    func displaySomething()
    func didNextStep(action: LegalPersonInfoAction)
}

final class LegalPersonInfoPresenter {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: LegalPersonInfoCoordinating
    weak var viewController: LegalPersonInfoDisplaying?

    init(coordinator: LegalPersonInfoCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - LegalPersonInfoPresenting
extension LegalPersonInfoPresenter: LegalPersonInfoPresenting {
    func displaySomething() {
        viewController?.displaySomething()
    }
    
    func didNextStep(action: LegalPersonInfoAction) {
        coordinator.perform(action: action)
    }
}
