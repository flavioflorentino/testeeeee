import Foundation

protocol LegalPersonInfoServicing {
}

final class LegalPersonInfoService {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - LegalPersonInfoServicing
extension LegalPersonInfoService: LegalPersonInfoServicing {
}
