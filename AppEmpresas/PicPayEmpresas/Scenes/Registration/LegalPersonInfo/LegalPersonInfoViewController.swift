import UI
import UIKit
import SnapKit

protocol LegalPersonInfoDisplaying: AnyObject {
    func displaySomething()
}

final class LegalPersonInfoViewController: ViewController<LegalPersonInfoInteracting, UIView> {
    fileprivate typealias Localizable = Strings.RegistrationLegalPersonInfo
    
    private lazy var descriptionLabel: UILabel = {
        var label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
            .with(\.textAlignment, .left)
        label.text = Localizable.description
        return label
    }()
    
    private lazy var fullNameTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.namePlaceHolder
        textField.placeholderColor = Colors.grayscale400.color
        textField.title = Localizable.nameTitle
        textField.titleColor = Colors.grayscale400.color
        textField.selectedTitle = Localizable.nameTitle
        textField.selectedTitleColor = Colors.branding600.color
        textField.textColor = Colors.grayscale750.color
        textField.font = Typography.bodyPrimary(.default).font()
        textField.layer.borderWidth = Border.none
        textField.setTitleVisible(true)
        
        return textField
    }()
    
    private lazy var emailTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.mailPlaceHolder
        textField.placeholderColor = Colors.grayscale400.color
        textField.title = Localizable.mailTitle
        textField.titleColor = Colors.grayscale400.color
        textField.selectedTitle = Localizable.mailTitle
        textField.selectedTitleColor = Colors.branding600.color
        textField.textColor = Colors.grayscale750.color
        textField.font = Typography.bodyPrimary(.default).font()
        textField.layer.borderWidth = Border.none
        textField.setTitleVisible(true)
        
        return textField
    }()
    
    private lazy var cpfTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.cpfPlaceHolder
        textField.placeholderColor = Colors.grayscale400.color
        textField.title = Localizable.cpfTitle
        textField.titleColor = Colors.grayscale400.color
        textField.selectedTitle = Localizable.cpfTitle
        textField.selectedTitleColor = Colors.branding600.color
        textField.textColor = Colors.grayscale750.color
        textField.font = Typography.bodyPrimary(.default).font()
        textField.layer.borderWidth = Border.none
        textField.setTitleVisible(true)
        
        return textField
    }()
    
    private lazy var cellphoneTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.cellphonePlaceHolder
        textField.placeholderColor = Colors.grayscale400.color
        textField.title = Localizable.cellphoneTitle
        textField.titleColor = Colors.grayscale400.color
        textField.selectedTitle = Localizable.cellphoneTitle
        textField.selectedTitleColor = Colors.branding600.color
        textField.textColor = Colors.grayscale750.color
        textField.font = Typography.bodyPrimary(.default).font()
        textField.layer.borderWidth = Border.none
        textField.setTitleVisible(true)
        
        return textField
    }()
    
    private lazy var birthdayTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.birthdayPlaceHolder
        textField.placeholderColor = Colors.grayscale400.color
        textField.title = Localizable.birthdayTitle
        textField.titleColor = Colors.grayscale400.color
        textField.selectedTitle = Localizable.birthdayTitle
        textField.selectedTitleColor = Colors.branding600.color
        textField.textColor = Colors.grayscale750.color
        textField.font = Typography.bodyPrimary(.default).font()
        textField.layer.borderWidth = Border.none
        textField.setTitleVisible(true)
        
        return textField
    }()
    
    private lazy var mothersNameTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.motherNamePlaceHolder
        textField.placeholderColor = Colors.grayscale400.color
        textField.title = Localizable.motherNameTitle
        textField.titleColor = Colors.grayscale400.color
        textField.selectedTitle = Localizable.motherNameTitle
        textField.selectedTitleColor = Colors.branding600.color
        textField.textColor = Colors.grayscale750.color
        textField.font = Typography.bodyPrimary(.default).font()
        textField.layer.borderWidth = Border.none
        textField.setTitleVisible(true)
        
        return textField
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.buttonOKTitle, for: .normal)
        return button
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.keyboardDismissMode = .onDrag
        return scrollView
    }()
    
    private lazy var contentView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.doSomething()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceDefault())
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        
        scrollView.addSubview(contentView)
        
        contentView.addSubviews(
            descriptionLabel,
            fullNameTextField,
            emailTextField,
            cpfTextField,
            cellphoneTextField,
            birthdayTextField,
            mothersNameTextField,
            confirmButton
        )
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
            $0.leading.trailing.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalTo(view.compatibleSafeArea.width)
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        fullNameTextField.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        emailTextField.snp.makeConstraints {
            $0.top.equalTo(fullNameTextField.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        cpfTextField.snp.makeConstraints {
            $0.top.equalTo(emailTextField.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        cellphoneTextField.snp.makeConstraints {
            $0.top.equalTo(cpfTextField.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        birthdayTextField.snp.makeConstraints {
            $0.top.equalTo(cellphoneTextField.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        mothersNameTextField.snp.makeConstraints {
            $0.top.equalTo(birthdayTextField.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        confirmButton.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(mothersNameTextField.snp.bottom).offset(Spacing.base04)
            $0.bottom.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }

    override func configureViews() {
        title = Localizable.title
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}

// MARK: - LegalPersonInfoDisplaying
extension LegalPersonInfoViewController: LegalPersonInfoDisplaying {
    func displaySomething() { }
}
