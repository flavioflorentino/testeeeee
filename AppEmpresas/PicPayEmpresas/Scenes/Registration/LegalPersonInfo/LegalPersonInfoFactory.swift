import UIKit

enum LegalPersonInfoFactory {
    static func make() -> LegalPersonInfoViewController {
        let container = DependencyContainer()
        let service: LegalPersonInfoServicing = LegalPersonInfoService(dependencies: container)
        let coordinator: LegalPersonInfoCoordinating = LegalPersonInfoCoordinator(dependencies: container)
        let presenter: LegalPersonInfoPresenting = LegalPersonInfoPresenter(coordinator: coordinator, dependencies: container)
        let interactor = LegalPersonInfoInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = LegalPersonInfoViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
