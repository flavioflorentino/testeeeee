import Foundation

protocol LegalPersonInfoInteracting: AnyObject {
    func doSomething()
}

final class LegalPersonInfoInteractor {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let service: LegalPersonInfoServicing
    private let presenter: LegalPersonInfoPresenting

    init(service: LegalPersonInfoServicing, presenter: LegalPersonInfoPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - LegalPersonInfoInteracting
extension LegalPersonInfoInteractor: LegalPersonInfoInteracting {
    func doSomething() {
        presenter.displaySomething()
    }
}
