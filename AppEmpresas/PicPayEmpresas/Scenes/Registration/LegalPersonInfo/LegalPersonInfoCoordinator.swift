import UIKit

enum LegalPersonInfoAction {
}

protocol LegalPersonInfoCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LegalPersonInfoAction)
}

final class LegalPersonInfoCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - LegalPersonInfoCoordinating
extension LegalPersonInfoCoordinator: LegalPersonInfoCoordinating {
    func perform(action: LegalPersonInfoAction) {
    }
}
