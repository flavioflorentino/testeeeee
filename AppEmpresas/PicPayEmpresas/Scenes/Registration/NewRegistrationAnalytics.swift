import AnalyticsModule
import Foundation
import LegacyPJ

enum NewRegistrationAnalytics: String, AnalyticsKeyProtocol {
    case obRegister = "OB Register"
    case obMultRegister = "OBM Register"
    
    func event() -> AnalyticsEventProtocol {
        var properties: [String: Any] = [:]
        if
            let user = AuthManager.shared.user,
            let userId = user.id,
            let name = user.name,
            let email = user.email,
            let phone = user.phone
        {
            properties = [
                "user_id": userId,
                "nome": name,
                "email": email,
                "telefone": phone
            ]
        }
        
        return AnalyticsEvent(rawValue, properties: properties, providers: [.mixPanel])
    }
    
    func eventFirebase() -> AnalyticsEventProtocol {
        var properties: [String: Any] = [:]
        if
            let user = AuthManager.shared.user,
            let userId = user.id,
            let name = user.name,
            let email = user.email,
            let phone = user.phone
        {
            properties = [
                "user_id": userId,
                "nome": name,
                "email": email,
                "telefone": phone
            ]
        }
        
        return AnalyticsEvent(rawValue, properties: properties, providers: [.firebase])
    }
}

enum NewRegistrationAnalyticsFirebase: String, AnalyticsKeyProtocol {
    case beginRegistration = "begin_registration"
    
    func event() -> AnalyticsEventProtocol {
        var properties: [String: Any] = [:]
        if
            let user = AuthManager.shared.user,
            let userId = user.id,
            let name = user.name,
            let email = user.email,
            let phone = user.phone
        {
            properties = [
                "user_id": userId,
                "nome": name,
                "email": email,
                "telefone": phone
            ]
        }
        
        return AnalyticsEvent(rawValue, properties: properties, providers: [.firebase])
    }
}
