import AnalyticsModule
import Foundation

protocol RegistrationAccountTypeViewModelInputs: AnyObject {
    func fireOpenEvent()
    func personalAccount()
    func companyAccount()
}

enum AccountType {
    case personal, company
}

protocol RegistrationAccountTypeProtocol: AnyObject {
    func didSelect(_ accountType: AccountType)
}

final class RegistrationAccountTypeViewModel {
    enum Trigger {
        case onBoarding, settings
    }
    private let presenter: RegistrationAccountTypePresenting
    private let model: RegistrationViewModel
    private let trigger: Trigger
    
    weak var delegate: BankAccountDelegate?
    var seller: Seller?
    
    private let dependencies: HasAnalytics = DependencyContainer()

    init(presenter: RegistrationAccountTypePresenting, model: RegistrationViewModel, trigger: Trigger = .onBoarding) {
        self.presenter = presenter
        self.model = model
        self.trigger = trigger
    }
}

extension RegistrationAccountTypeViewModel: RegistrationAccountTypeViewModelInputs {
    func fireOpenEvent() {
        let origin = trigger == .onBoarding
        let event: RegistrationAccountTypeAnalytics =
            origin ? .openOnBoarding : .openOnSettings(seller?.id)
        dependencies.analytics.log(event)
    }
    
    func personalAccount() {
        let origin = trigger == .onBoarding
        let event: RegistrationAccountTypeAnalytics =
            origin ? .typeOnBoarding(.pf) : .typeOnSettings(.pf, sellerId: seller?.id)
        dependencies.analytics.log(event)
        delegate?.didSelect(.personal)
        presenter.didNextStep(action: .personalBank(model, delegate: delegate))
    }
    
    func companyAccount() {
        let origin = trigger == .onBoarding
        let event: RegistrationAccountTypeAnalytics =
            origin ? .typeOnBoarding(.pj) : .typeOnSettings(.pj, sellerId: seller?.id)
        dependencies.analytics.log(event)
        delegate?.didSelect(.company)
        presenter.didNextStep(action: .companyBank(model, delegate: delegate))
    }
}
