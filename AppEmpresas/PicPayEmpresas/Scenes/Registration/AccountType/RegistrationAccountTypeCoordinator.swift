import UIKit

enum RegistrationAccountTypeAction {
    case personalBank(_ model: RegistrationViewModel, delegate: BankAccountDelegate?)
    case companyBank(_ model: RegistrationViewModel, delegate: BankAccountDelegate?)
}

enum RegistrationAccountTypeFlow {
    case registration, settings
}

protocol RegistrationAccountTypeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationAccountTypeAction)
}

final class RegistrationAccountTypeCoordinator: RegistrationAccountTypeCoordinating {
    weak var viewController: UIViewController?
    private let flow: RegistrationAccountTypeFlow
    
    init(_ flow: RegistrationAccountTypeFlow = .registration) {
        self.flow = flow
    }
    
    func perform(action: RegistrationAccountTypeAction) {
        switch flow {
        case .registration:
            registrationAction(action)
        case .settings:
            settingsAction(action)
        }
    }
    
    private func registrationAction(_ action: RegistrationAccountTypeAction) {
        guard let navigation = viewController?.navigationController else {
            return
        }
        
        let bankViewController: RegistrationBankSelectStepViewController = ViewsManager.instantiateViewController(.registration)
        
        switch action {
        case .companyBank(let model, _):
            model.bankAccountType = .company
            bankViewController.setup(model: model)
        case .personalBank(let model, _):
            model.bankAccountType = .personal
            bankViewController.setup(model: model)
        }
        
        navigation.pushViewController(bankViewController, animated: true)
    }
    
    private func settingsAction(_ action: RegistrationAccountTypeAction) {
        guard let navigation = viewController?.navigationController else {
            return
        }
        
        let bankViewController: BankSelectFormViewController = ViewsManager.instantiateViewController(.settings)
        
        switch action {
        case .companyBank(_, let delegate):
            bankViewController.setup(delegate: delegate)
            delegate?.didSelect(.company)
        case .personalBank(_, let delegate):
            bankViewController.setup(delegate: delegate)
            delegate?.didSelect(.personal)
        }
        
        navigation.pushViewController(bankViewController, animated: true)
        navigation.viewControllers.removeAll { $0.isKind(of: RegistrationAccountTypeViewController.self) }
    }
}
