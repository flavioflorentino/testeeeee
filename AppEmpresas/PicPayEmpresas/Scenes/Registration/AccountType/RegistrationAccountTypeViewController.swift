import UI

final class RegistrationAccountTypeViewController: ViewController<RegistrationAccountTypeViewModelInputs, UIView> {
    private typealias Localizable = Strings.RegistrationAccountType
    
    private enum Layout {
        static let buttonCornerRadius: CGFloat = 8
        static let buttonHeight: CGFloat = 168
        static let buttonWidth: CGFloat = 136
        static let companyImage = Assets.bankAccountCompany.image
        static let descriptionBottomSpacing: CGFloat = 60
        static let descriptionWidth: CGFloat = 240
        static let imageEdgeInsets = UIEdgeInsets(top: 24, left: 40, bottom: 76, right: 28)
        static let labelBoldFont = UIFont.boldSystemFont(ofSize: 12)
        static let labelBottomContraint: CGFloat = -36
        static let labelFont = UIFont.systemFont(ofSize: 12)
        static let labelWidth: CGFloat = 120
        static let stackSpacing: CGFloat = 16
        static let titleBottomSpacing: CGFloat = 8
        static let titleFont = UIFont.boldSystemFont(ofSize: 18)
        static let userImage = Assets.bankAccountPersonal.image
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.titleFont
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = Localizable.registerNewAccount
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.labelFont
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = Localizable.chooseAccountType
        return label
    }()
    
    private lazy var personalAccountLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.labelBoldFont
        label.textColor = Palette.white.color
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = Localizable.personalAccount
        return label
    }()
    
    private lazy var personalAccountButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = Palette.ppColorBranding300.color
        button.layer.cornerRadius = Layout.buttonCornerRadius
        button.setImage(Layout.userImage, for: .normal)
        button.contentMode = .center
        button.imageView?.contentMode = .scaleAspectFit
        button.imageEdgeInsets = Layout.imageEdgeInsets
        button.addTarget(self, action: #selector(personalAccountButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var companyAccountLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Layout.labelBoldFont
        label.textColor = Palette.white.color
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = Localizable.companyAccount
        return label
    }()
    
    private lazy var companyAccountButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = Palette.ppColorBranding300.color
        button.layer.cornerRadius = Layout.buttonCornerRadius
        button.setImage(Layout.companyImage, for: .normal)
        button.contentMode = .center
        button.imageView?.contentMode = .scaleAspectFit
        button.imageEdgeInsets = Layout.imageEdgeInsets
        button.addTarget(self, action: #selector(companyAccountButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.alignment = .center
        stack.spacing = Layout.stackSpacing
        return stack
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildViewHierarchy()
        configureViews()
        setupConstraints()
        viewModel.fireOpenEvent()
    }
    
    override func buildViewHierarchy() {
        personalAccountButton.addSubview(personalAccountLabel)
        companyAccountButton.addSubview(companyAccountLabel)
        stackView.addArrangedSubview(personalAccountButton)
        stackView.addArrangedSubview(companyAccountButton)
        
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(stackView)
    }
    
    override func configureViews() {
        if let color = Palette.hexColor(with: "#F2F2F2") {
            view.backgroundColor = color
        }
    }
    
    override func setupConstraints() {
        personalAccountLabel.widthAnchor.constraint(equalToConstant: Layout.labelWidth).isActive = true
        personalAccountLabel.centerXAnchor.constraint(equalTo: personalAccountButton.centerXAnchor).isActive = true
        personalAccountLabel.bottomAnchor.constraint(equalTo: personalAccountButton.bottomAnchor, constant: Layout.labelBottomContraint).isActive = true
        
        companyAccountLabel.widthAnchor.constraint(equalToConstant: Layout.labelWidth).isActive = true
        companyAccountLabel.centerXAnchor.constraint(equalTo: companyAccountButton.centerXAnchor).isActive = true
        companyAccountLabel.bottomAnchor.constraint(equalTo: companyAccountButton.bottomAnchor, constant: Layout.labelBottomContraint).isActive = true
        
        personalAccountButton.widthAnchor.constraint(equalToConstant: Layout.buttonWidth).isActive = true
        personalAccountButton.heightAnchor.constraint(equalToConstant: Layout.buttonHeight).isActive = true
        
        companyAccountButton.widthAnchor.constraint(equalToConstant: Layout.buttonWidth).isActive = true
        companyAccountButton.heightAnchor.constraint(equalToConstant: Layout.buttonHeight).isActive = true
        
        stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: Layout.descriptionBottomSpacing).isActive = true
        
        descriptionLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        descriptionLabel.widthAnchor.constraint(equalToConstant: Layout.descriptionWidth).isActive = true
        descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Layout.titleBottomSpacing).isActive = true
        descriptionLabel.bottomAnchor.constraint(equalTo: stackView.topAnchor, constant: -60.0).isActive = true
        
        titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    @objc
    private func personalAccountButtonTapped(_ sender: UIButton) {
        viewModel.personalAccount()
    }
    
    @objc
    private func companyAccountButtonTapped(_ sender: UIButton) {
        viewModel.companyAccount()
    }
}
