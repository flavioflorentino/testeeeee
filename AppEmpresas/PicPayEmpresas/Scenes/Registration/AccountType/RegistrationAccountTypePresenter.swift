import Foundation

protocol RegistrationAccountTypePresenting: AnyObject {
    func didNextStep(action: RegistrationAccountTypeAction)
}

final class RegistrationAccountTypePresenter: RegistrationAccountTypePresenting {
    private let coordinator: RegistrationAccountTypeCoordinating

    init(coordinator: RegistrationAccountTypeCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: RegistrationAccountTypeAction) {
        coordinator.perform(action: action)
    }
}
