import AnalyticsModule

enum RegistrationAccountTypeAnalytics: AnalyticsKeyProtocol {
    case openOnBoarding
    case typeOnBoarding(_ type: AccountType)
    case openOnSettings(_ sellerId: Int?)
    case typeOnSettings(_ type: AccountType, sellerId: Int?)
    
    enum AccountType: String {
        case pf, pj
    }
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .openOnBoarding:
            return AnalyticsEvent("OB Bank Account Type", properties: [:], providers: [.appsFlyer])
        case .typeOnBoarding(let type):
            let prop = ["bank_account_type": type.rawValue.uppercased()]
            return AnalyticsEvent("OB Bank Account Type Option", properties: prop, providers: [.appsFlyer])
        case .openOnSettings(let sellerId):
            let prop = ["seller_id": "\(sellerId ?? 0)"]
            return AnalyticsEvent("Bank Account Type", properties: prop, providers: [.appsFlyer])
        case let .typeOnSettings(type, sellerId):
            let prop = [
                "bank_account_type": type.rawValue.uppercased(),
                "seller_id": "\(sellerId ?? 0)"
            ]
            return AnalyticsEvent("Bank Account Type Option", properties: prop, providers: [.appsFlyer])
        }
    }
}
