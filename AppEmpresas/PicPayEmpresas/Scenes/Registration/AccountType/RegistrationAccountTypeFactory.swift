import Foundation

enum RegistrationAccountTypeFactory {
    static func make(model: RegistrationViewModel) -> RegistrationAccountTypeViewController {
        let coordinator: RegistrationAccountTypeCoordinating = RegistrationAccountTypeCoordinator()
        let presenter: RegistrationAccountTypePresenting = RegistrationAccountTypePresenter(coordinator: coordinator)
        let viewModel = RegistrationAccountTypeViewModel(presenter: presenter, model: model)
        let viewController = RegistrationAccountTypeViewController(viewModel: viewModel)

        coordinator.viewController = viewController

        return viewController
    }
    
    static func makeForSettings(_ delegate: BankAccountDelegate, seller: Seller?) -> RegistrationAccountTypeViewController {
        let coordinator: RegistrationAccountTypeCoordinating = RegistrationAccountTypeCoordinator(.settings)
        let presenter: RegistrationAccountTypePresenting = RegistrationAccountTypePresenter(coordinator: coordinator)
        let model = RegistrationViewModel()
        let viewModel = RegistrationAccountTypeViewModel(presenter: presenter, model: model, trigger: .settings)
        viewModel.delegate = delegate
        viewModel.seller = seller
        let viewController = RegistrationAccountTypeViewController(viewModel: viewModel)

        coordinator.viewController = viewController

        return viewController
    }
}
