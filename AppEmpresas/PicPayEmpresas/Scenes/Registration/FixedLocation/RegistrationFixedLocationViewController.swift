import UI

private extension RegistrationFixedLocationViewController.Layout {
    enum Size {
        static let stackHeight: CGFloat = 168
    }
    
    enum Button {
        static let titleInsets = UIEdgeInsets(top: 60, left: 8, bottom: 0, right: 8)
    }
}

final class RegistrationFixedLocationViewController: ViewController<RegistrationFixedLocationViewModelInputs, UIView> {
    private typealias Localizable = Strings.RegistrationFixedLocation
    fileprivate enum Layout { }
    
    // MARK: - Variables
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.title
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.subTitle
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale600())
            .with(\.textAlignment, .center)
        label.numberOfLines = 3
        return label
    }()
    
    private lazy var buttonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = Spacing.base02
        stackView.addArrangedSubview(withoutLocationButton)
        stackView.addArrangedSubview(withLocationButton)
        return stackView
    }()
    
    private lazy var storeImage = UIImageView(image: Assets.iconStore.image)
    private lazy var personImage = UIImageView(image: Assets.iconPerson.image)
    
    private lazy var withLocationButton: UIPPButton = {
        let button = UIPPButton()
        button.addSubview(storeImage)
        button.setTitle(Localizable.withFixedLocationButton, for: .normal)
        button.titleEdgeInsets = Layout.Button.titleInsets
        button.titleLabel?.numberOfLines = 2
        button.textAlignment = .center
        button.normalBackgrounColor = Colors.branding400.color
        button.normalTitleColor = Colors.white.color
        button.highlightedBackgrounColor = Colors.branding700.color
        button.addTarget(self, action: #selector(nextStepAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var withoutLocationButton: UIPPButton = {
        let button = UIPPButton()
        button.addSubview(personImage)
        button.setTitle(Localizable.withoutFixedPointButton, for: .normal)
        button.titleEdgeInsets = Layout.Button.titleInsets
        button.titleLabel?.numberOfLines = 2
        button.textAlignment = .center
        button.normalBackgrounColor = Colors.branding400.color
        button.normalTitleColor = Colors.white.color
        button.highlightedBackgrounColor = Colors.branding700.color
        button.addTarget(self, action: #selector(withoutlocationAction), for: .touchUpInside)
        return button
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.trackingViewDidLoad()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(infoLabel)
        view.addSubview(buttonStackView)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
        
        infoLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base05)
            $0.trailing.equalToSuperview().inset(Spacing.base05)
        }
        
        buttonStackView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.Size.stackHeight)
        }
        
        personImage.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.centerX.equalToSuperview()
        }
        
        storeImage.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.centerX.equalToSuperview()
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.grayscale050.color
    }
}

// MARK: Button Actions
@objc
extension RegistrationFixedLocationViewController {
    private func nextStepAction() {
        viewModel.nextStep()
    }
    
    private func withoutlocationAction() {
        viewModel.withoutLocation()
    }
}
