import Foundation

enum RegistrationFixedLocationFactory {
    static func make(model: RegistrationViewModel) -> RegistrationFixedLocationViewController {
        let container = DependencyContainer()
        let service: RegistrationFixedLocationServicing = RegistrationFixedLocationService(dependencies: container)
        let coordinator: RegistrationFixedLocationCoordinating = RegistrationFixedLocationCoordinator()
        let presenter: RegistrationFixedLocationPresenting = RegistrationFixedLocationPresenter(coordinator: coordinator)
        let viewModel = RegistrationFixedLocationViewModel(service: service, presenter: presenter, model: model)
        let viewController = RegistrationFixedLocationViewController(viewModel: viewModel)

        coordinator.viewController = viewController

        return viewController
    }
}
