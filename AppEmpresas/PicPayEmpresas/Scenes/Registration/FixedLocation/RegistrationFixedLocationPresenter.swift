import Foundation

protocol RegistrationFixedLocationPresenting: AnyObject {
    func didNextStep(action: RegistrationFixedLocationAction)
}

final class RegistrationFixedLocationPresenter: RegistrationFixedLocationPresenting {
    private let coordinator: RegistrationFixedLocationCoordinating

    init(coordinator: RegistrationFixedLocationCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - RegistrationFixedLocationPresenting
extension RegistrationFixedLocationPresenter {
    func didNextStep(action: RegistrationFixedLocationAction) {
        coordinator.perform(action: action)
    }
}
