import AnalyticsModule
import Foundation

protocol RegistrationFixedLocationViewModelInputs: AnyObject {
    func withoutLocation()
    func nextStep()
    func trackingViewDidLoad()
}

final class RegistrationFixedLocationViewModel {
    private let service: RegistrationFixedLocationServicing
    private let presenter: RegistrationFixedLocationPresenting
    private let model: RegistrationViewModel
    private let dependencies: HasAnalytics = DependencyContainer()

    init(service: RegistrationFixedLocationServicing, presenter: RegistrationFixedLocationPresenting, model: RegistrationViewModel) {
        self.service = service
        self.presenter = presenter
        self.model = model
    }
}

// MARK: - RegistrationFixedLocationViewModelInputs
extension RegistrationFixedLocationViewModel: RegistrationFixedLocationViewModelInputs {
    func withoutLocation() {
        model.account.withoutAddress = true
        service.registerCompletedStep(model: model)
        presenter.didNextStep(action: .without(model: model))
    }
    
    func nextStep() {
        model.account.withoutAddress = false
        service.registerCompletedStep(model: model)
        presenter.didNextStep(action: .nextScreen(model: model))
    }
    
    func trackingViewDidLoad() {
        dependencies.analytics.log(RegistrationAnalytics.addressType)
    }
}
