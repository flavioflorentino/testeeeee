import Foundation
import LegacyPJ

protocol RegistrationFixedLocationServicing {
    func registerCompletedStep(model: RegistrationViewModel)
}

final class RegistrationFixedLocationService {
    typealias Dependencies = HasAuthManager
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RegistrationFixedLocationServicing
extension RegistrationFixedLocationService: RegistrationFixedLocationServicing {
    func registerCompletedStep(model: RegistrationViewModel) {
        // Futuramente esse service irá implementar uma chamada real para o backend, completando uma etapa do cadastro
        dependencies.authManager.storeSignUpRegister(data: model.toDictionary().toData() ?? Data())
    }
}
