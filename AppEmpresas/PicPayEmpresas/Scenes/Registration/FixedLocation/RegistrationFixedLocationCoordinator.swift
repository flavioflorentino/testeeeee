import UIKit

enum RegistrationFixedLocationAction {
    case nextScreen(model: RegistrationViewModel)
    case without(model: RegistrationViewModel)
}

protocol RegistrationFixedLocationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationFixedLocationAction)
}

final class RegistrationFixedLocationCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - RegistrationFixedLocationCoordinating
extension RegistrationFixedLocationCoordinator: RegistrationFixedLocationCoordinating {
    func perform(action: RegistrationFixedLocationAction) {
        switch action {
        case .without(let model):
            let controller: RegistrationImageStepViewController = ViewsManager.instantiateViewController(.registration)
            controller.setup(model: model)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .nextScreen(let model):
            let controller: RegistrationAddressStepViewController = ViewsManager.instantiateViewController(.registration)
            controller.setup(model: model)
            controller.addressType = .company
            viewController?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
