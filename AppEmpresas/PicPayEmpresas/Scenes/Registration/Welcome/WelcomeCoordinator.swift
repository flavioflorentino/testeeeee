import UIKit

enum WelcomeAction {
    case rules
}

protocol WelcomeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: WelcomeAction)
}

final class WelcomeCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - WelcomeCoordinating
extension WelcomeCoordinator: WelcomeCoordinating {
    func perform(action: WelcomeAction) {
        let controller = RegistrationRulesFactory.make()
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
