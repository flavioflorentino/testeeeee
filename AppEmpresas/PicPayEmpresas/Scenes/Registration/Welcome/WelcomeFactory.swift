import UIKit

enum WelcomeFactory {
    static func make() -> WelcomeViewController {
        let container = DependencyContainer()
        let coordinator: WelcomeCoordinating = WelcomeCoordinator()
        let presenter: WelcomePresenting = WelcomePresenter(coordinator: coordinator)
        let interactor = WelcomeInteractor(presenter: presenter, dependencies: container)
        let viewController = WelcomeViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
