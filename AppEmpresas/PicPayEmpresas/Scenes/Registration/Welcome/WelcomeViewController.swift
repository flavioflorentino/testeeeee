import UI
import UIKit
import AssetsKit

protocol WelcomeDisplaying: AnyObject {
    func hideZeroTax(_ shouldHide: Bool)
}

private extension WelcomeViewController.Layout {
    enum Image {
        static let size = CGSize(width: 343, height: 226)
    }
}

final class WelcomeViewController: ViewController<WelcomeInteracting, UIView> {
    private typealias Localizable = Strings.RegistrationIntro
    fileprivate enum Layout { }
    
    private lazy var titleLabel: UILabel = {
        var label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .black)
            .with(\.textAlignment, .left)
        label.text = Localizable.welcomeTitle
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        var label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .left)
        label.text = Localizable.welcomeDescription
        return label
    }()
    
    private lazy var descriptionHighlightedLabel: UILabel = {
        var label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .left)
        label.text = Localizable.welcomeDescriptionHighlight
        return label
    }()
    
    private lazy var imageView = UIImageView(image: Resources.Illustrations.iluPavPeople.image)
    
    private lazy var imageViewBox: UIView = {
        let view = UIView()
        view.addSubview(imageView)
        return view
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.welcomeButton, for: .normal)
        button.addTarget(self, action: #selector(didTapConfirmButton), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.hideZeroTax()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceDefault())
    }
 
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(descriptionHighlightedLabel)
        view.addSubview(imageViewBox)
        view.addSubview(confirmButton)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        descriptionHighlightedLabel.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        imageViewBox.snp.makeConstraints {
            $0.top.equalTo(descriptionHighlightedLabel.snp.bottom)
            $0.leading.trailing.equalToSuperview()
        }
        
        imageView.snp.makeConstraints {
            $0.centerY.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Image.size)
        }

        confirmButton.snp.makeConstraints {
            $0.top.equalTo(imageViewBox.snp.bottom)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }

    override func configureViews() {
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
    
    @objc
    func didTapConfirmButton() {
        interactor.loadRules()
    }
}

// MARK: - WelcomeDisplaying
extension WelcomeViewController: WelcomeDisplaying {
    func hideZeroTax(_ shouldHide: Bool) {
        descriptionHighlightedLabel.isHidden = shouldHide
    }
}
