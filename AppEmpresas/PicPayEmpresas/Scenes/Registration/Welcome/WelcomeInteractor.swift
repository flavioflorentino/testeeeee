import Foundation
import AnalyticsModule
import FeatureFlag

protocol WelcomeInteracting: AnyObject {
    func loadRules()
    func hideZeroTax()
}

final class WelcomeInteractor {
    typealias Dependencies = HasFeatureManager & HasAnalytics
    private let dependencies: Dependencies
    
    private let presenter: WelcomePresenting

    init(presenter: WelcomePresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - WelcomeInteracting
extension WelcomeInteractor: WelcomeInteracting {
    func loadRules() {
        dependencies.analytics.log(RegistrationRulesAnalytics.onboarding)
        presenter.didNextStep(action: .rules)
    }
    
    func hideZeroTax() {
        let shouldHideTax = !dependencies.featureManager.isActive(.registrationTaxZero)
        presenter.hideZeroTax(shouldHideTax)
    }
}
