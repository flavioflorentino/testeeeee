import AnalyticsModule
import Foundation

enum WelcomeRegistrationAnalytics: String, AnalyticsKeyProtocol {
    case onboarding = "OB Welcome"
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(rawValue, providers: [.mixPanel])
    }
}
