import Core

enum RegistrationPasswordServiceEndPoint {
    case createAccount(account: RegistrationPasswordParameters)
}

extension RegistrationPasswordServiceEndPoint: ApiEndpointExposable {
    var path: String {
        "/account"
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var body: Data? {
        switch self {
        case .createAccount(let account):
            return try? JSONEncoder().encode(account)
        }
    }
}
