import Core

protocol RegistrationPasswordServicing {
    func createAccount(model: RegistrationViewModel, _ completion: @escaping (Result<RegistrationPasswordAccess?, ApiError>) -> Void)
}

final class RegistrationPasswordService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RegistrationPasswordServicing
extension RegistrationPasswordService: RegistrationPasswordServicing {
    func createAccount(model: RegistrationViewModel, _ completion: @escaping (Result<RegistrationPasswordAccess?, ApiError>) -> Void) {
        let parametersModel = convertLegacyInEncodable(model: model.account)
        let endpoint = RegistrationPasswordServiceEndPoint.createAccount(account: parametersModel)
        BizApi<RegistrationPasswordAccess>(endpoint: endpoint).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }
}

// MARK: - CONVERT LEGACY MODEL IN ENCODABLE MODEL -
extension RegistrationPasswordService {
    func convertLegacyInEncodable(model: SignUpAccount) -> RegistrationPasswordParameters {
        let addressModel = RegistrationAddressModel(model: model.partner.address)
        let partnerModel = RegistrationPartnerModel(model: model.partner, addressModel: addressModel)
        let model = RegistrationPasswordParameters(model: model, partnerModel: partnerModel)
        
        return model
    }
}
