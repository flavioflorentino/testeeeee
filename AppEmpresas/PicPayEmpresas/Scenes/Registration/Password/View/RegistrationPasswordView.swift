import Foundation
import Validator
import UI
import UIKit

protocol RegistrationPasswordViewDelegate: AnyObject {
    func confirmPassword(inputs: [UIPPFloatingTextField])
    func openURL(url: String)
}

private extension RegistrationPasswordView.Layout {
    enum Size {
        static let showPasswordButtonWidth: CGFloat = 20.0
    }
}

final class RegistrationPasswordView: UIView {
    fileprivate enum Layout { }
    private typealias Localizable = Strings.RegistrationPassword
    
    // MARK: Enums
    enum ValidateError: ValidationError {
        case passwordInvalid
        case termsNotChecked
        
        var rawValue: String {
            switch self {
            case .passwordInvalid:
                return Localizable.passwordMinimumCharacters
                
            case .termsNotChecked:
                return Localizable.acceptTermsToContinue
            }
        }
        
        var description: String {
            rawValue
        }
        
        var message: String {
            rawValue
        }
    }
    
    private enum TextFieldType: Int {
        case password = 0
        case confirmPassword
    }
    
    private let maxPasswordLenght = 6
    
    // MARK: Private Lazy Vars
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.subTitle
        label.labelStyle(BodyPrimaryLabelStyle()).with(\.textColor, .grayscale600())
        label.textAlignment = .center
        return label
    }()
    
    private lazy var warningCard: ApolloFeedbackCard = {
        let card = ApolloFeedbackCard(description: Localizable.warningDescription, iconType: .warning, layoutType: .onlyText)
        return card
    }()
    
    private lazy var passwordTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.isSecureTextEntry = true
        textField.delegate = self
        textField.border = .none
        textField.tag = TextFieldType.password.rawValue
        textField.returnKeyType = .next
        return textField
    }()
    
    private lazy var showPasswordButton: UIButton = {
        let button = UIButton(type: .custom)
        button.titleLabel?.font = Typography.icons(.medium).font()
        button.setTitleColor(Colors.black.color, for: .normal)
        button.setTitle(Iconography.eyeSlash.rawValue, for: .normal)
        button.addTarget(self, action: #selector(showPasswordAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var passwordTipsLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.passwordMinimumCharacters
        label.labelStyle(CaptionLabelStyle()).with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var confirmPasswordTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.isSecureTextEntry = true
        textField.delegate = self
        textField.border = .none
        textField.tag = TextFieldType.confirmPassword.rawValue
        textField.returnKeyType = .done
        return textField
    }()
    
    private lazy var showConfirmPasswordButton: UIButton = {
        let button = UIButton(type: .custom)
        button.titleLabel?.font = Typography.icons(.medium).font()
        button.setTitleColor(Colors.black.color, for: .normal)
        button.setTitle(Iconography.eyeSlash.rawValue, for: .normal)
        button.addTarget(self, action: #selector(showConfirmPasswordAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var termsCheckbox: UILabel = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(agreeTermsAction))
        let label = UILabel()
        label.labelStyle(IconLabelStyle(type: .large))
            .with(\.textColor, .grayscale300())
            .with(\.cornerRadius, .strong)
        label.text = Iconography.circle.rawValue
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(tap)
        label.layer.masksToBounds = true
        return label
    }()
    
    private lazy var termsTextView: UITextView = {
        let text = UITextView()
        text.font = Typography.bodyPrimary().font()
        text.textColor = Colors.grayscale700.color
        text.isEditable = false
        text.isSelectable = true
        text.isScrollEnabled = false
        text.backgroundColor = .clear
        text.textContainerInset = .zero
        text.textContainer.lineFragmentPadding = 0
        text.delegate = self
        return text
    }()
    
    private lazy var messageError: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle()).with(\.textColor, .critical900())
        label.textAlignment = .center
        label.numberOfLines = 3
        label.isHidden = true
        return label
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.continueButton, for: .normal)
        button.isEnabled = false
        button.addTarget(self, action: #selector(confirmAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var inputs = [passwordTextField, confirmPasswordTextField]
    
    // MARK: Private Vars
    private var agreeTerms: Bool = false
    
    weak var delegate: RegistrationPasswordViewDelegate?
    
    // MARK: Life Cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        endEditing(true)
    }
}

// MARK: Public Functions
extension RegistrationPasswordView {
    func setAgreementTextView(linkText: NSAttributedString, linkAttributes: [NSAttributedString.Key: Any]) {
        termsTextView.attributedText = linkText
        termsTextView.linkTextAttributes = linkAttributes
    }
    
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField?)]) {
        fields.forEach {
            if let message = $0.message, !message.isEmpty {
                $0.textField?.lineColor = Colors.branding300.color
            }
            $0.textField?.errorMessage = $0.message
        }
    }
    
    func updateMessageError(error: String) {
        messageError.text = error
        messageError.isHidden = false
    }
}

// MARK: View Configuration
extension RegistrationPasswordView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(
            titleLabel,
            warningCard,
            passwordTextField,
            passwordTipsLabel,
            confirmPasswordTextField,
            messageError,
            termsCheckbox,
            termsTextView,
            confirmButton,
            showPasswordButton,
            showConfirmPasswordButton
        )
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        warningCard.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        passwordTextField.snp.makeConstraints {
            $0.top.equalTo(warningCard.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        showPasswordButton.snp.makeConstraints {
            $0.centerY.equalTo(passwordTextField.snp.centerY)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.width.greaterThanOrEqualTo(Layout.Size.showPasswordButtonWidth)
        }
        
        passwordTipsLabel.snp.makeConstraints {
            $0.top.equalTo(passwordTextField.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        confirmPasswordTextField.snp.makeConstraints {
            $0.top.equalTo(passwordTipsLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        showConfirmPasswordButton.snp.makeConstraints {
            $0.centerY.equalTo(confirmPasswordTextField.snp.centerY)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.width.greaterThanOrEqualTo(Layout.Size.showPasswordButtonWidth)
        }
        
        messageError.snp.makeConstraints {
            $0.top.equalTo(confirmPasswordTextField.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.greaterThanOrEqualTo(Spacing.base02)
        }
        
        termsTextView.snp.makeConstraints {
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.height.greaterThanOrEqualTo(Spacing.base04)
        }
        
        termsCheckbox.snp.makeConstraints {
            $0.size.greaterThanOrEqualTo(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalTo(termsTextView.snp.leading).offset(-Spacing.base01)
            $0.centerY.equalTo(termsTextView.snp.centerY)
        }
        
        confirmButton.snp.makeConstraints {
            $0.top.equalTo(termsTextView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        createRules()
    }
    
    func createRules() {
        var passwordRules = ValidationRuleSet<String>()
        passwordRules.add(rule: ValidationRuleLength(min: maxPasswordLenght, max: maxPasswordLenght, error: ValidateError.passwordInvalid))

        passwordTextField.validationRules = passwordRules
        confirmPasswordTextField.validationRules = passwordRules
    }
}

// MARK: Actions
@objc
private extension RegistrationPasswordView {
    func agreeTermsAction() {
        termsCheckbox.text = agreeTerms ? Iconography.circle.rawValue : Iconography.check.rawValue
        termsCheckbox.textColor = agreeTerms ? Colors.grayscale300.color : Colors.white.color
        termsCheckbox.backgroundColor = agreeTerms ? .clear : Colors.branding600.color
        agreeTerms.toggle()
        confirmButton.isEnabled.toggle()
    }
    
    func showPasswordAction() {
        passwordTextField.isSecureTextEntry.toggle()
        
        let title = passwordTextField.isSecureTextEntry ? Iconography.eyeSlash.rawValue : Iconography.eye.rawValue
        showPasswordButton.setTitle(title, for: .normal)
    }
    
    func showConfirmPasswordAction() {
        confirmPasswordTextField.isSecureTextEntry.toggle()
        
        let title = confirmPasswordTextField.isSecureTextEntry ? Iconography.eyeSlash.rawValue : Iconography.eye.rawValue
        showConfirmPasswordButton.setTitle(title, for: .normal)
    }
    
    func confirmAction() {
        messageError.isHidden = true
        delegate?.confirmPassword(inputs: [passwordTextField, confirmPasswordTextField])
    }
}

extension RegistrationPasswordView: UITextViewDelegate {
    func textView(
        _ textView: UITextView,
        shouldInteractWith URL: URL,
        in characterRange: NSRange,
        interaction: UITextItemInteraction) -> Bool {
        delegate?.openURL(url: URL.absoluteString)
        return false
    }
}

extension RegistrationPasswordView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let index = textField.tag + 1
        if textField.tag == TextFieldType.confirmPassword.rawValue || inputs.count < index {
            textField.resignFirstResponder()
            return true
        }
        
        inputs[index].becomeFirstResponder()
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, text.count == maxPasswordLenght && !string.isEmpty {
            return false
        }
        
        return true
    }
}
