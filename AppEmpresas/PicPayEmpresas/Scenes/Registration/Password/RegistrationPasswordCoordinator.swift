import UIKit

enum RegistrationPasswordAction: Equatable {
    case openURL(url: String)
    case nextStep
    case handleError
}

protocol RegistrationPasswordCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationPasswordAction)
}

final class RegistrationPasswordCoordinator {
    typealias Dependencies = HasAppManager
    private let dependencies: Dependencies
    
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RegistrationPasswordCoordinating
extension RegistrationPasswordCoordinator: RegistrationPasswordCoordinating {
    func perform(action: RegistrationPasswordAction) {
        switch action {
        case .openURL(let url):
            let controller = ViewsManager.webViewController(url: url)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .nextStep:
            dependencies.appManager.presentAuthenticatedViewController()
        case .handleError:
            let configuration = BizErrorViewLayout(
                title: Strings.ErrorView.anErrorOcurred,
                message: Strings.ErrorView.weReSorry,
                image: Assets.Emoji.iconBad.image
            )
            let controller = BizErrorViewFactory.make(errorView: configuration, errorCoordinatorDelegate: self)
            viewController?.navigationController?.present(controller, animated: true)
        }
    }
}

extension RegistrationPasswordCoordinator: BizErrorViewCoordinatorDelegate {
    func buttonAction() {
        viewController?.dismiss(animated: true)
    }
    
    func closeAction() {
        viewController?.dismiss(animated: true)
    }
}
