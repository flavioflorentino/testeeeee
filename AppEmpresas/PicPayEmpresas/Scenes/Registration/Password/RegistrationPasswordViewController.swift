import UI
import UIKit

protocol RegistrationPasswordDisplaying: AnyObject {
    func setAgreementTextView(linkText: NSAttributedString, linkAttributes: [NSAttributedString.Key: Any])
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField?)])
    func showMessageError(error: String)
}

private extension RegistrationPasswordViewController.Layout {}

final class RegistrationPasswordViewController: ViewController<RegistrationPasswordInteracting, UIView> {
    fileprivate enum Layout { }
    private typealias Localizable = Strings.RegistrationPassword
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        return scroll
    }()
    
    private lazy var passwordView: RegistrationPasswordView = {
        let view = RegistrationPasswordView()
        view.delegate = self
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = Localizable.title
        view.backgroundColor = Colors.grayscale050.color
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceOldRegistration())
        interactor.configureTerms()
        interactor.trackDidLoad()
    }
    
    override func buildViewHierarchy() {
        scrollView.addSubview(passwordView)
        view.addSubview(scrollView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        passwordView.snp.makeConstraints {
            $0.centerX.centerY.edges.equalToSuperview()
        }
    }
}

// MARK: - RegistrationPasswordDisplaying
extension RegistrationPasswordViewController: RegistrationPasswordDisplaying {
    func setAgreementTextView(linkText: NSAttributedString, linkAttributes: [NSAttributedString.Key: Any]) {
        passwordView.setAgreementTextView(linkText: linkText, linkAttributes: linkAttributes)
    }
    
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField?)]) {
        passwordView.updateFields(fields: fields)
    }
    
    func showMessageError(error: String) {
        passwordView.updateMessageError(error: error)
    }
}

extension RegistrationPasswordViewController: RegistrationPasswordViewDelegate {
    func confirmPassword(inputs: [UIPPFloatingTextField]) {
        interactor.updateFields(inputs: inputs)
        interactor.submitPassword(inputs: inputs)
    }
    
    func openURL(url: String) {
        interactor.openURL(url: url)
    }
}
