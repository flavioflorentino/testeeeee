import Foundation
import UI

protocol RegistrationPasswordPresenting: AnyObject {
    var viewController: RegistrationPasswordDisplaying? { get set }
    func configureTerms()
    func didNextStep(action: RegistrationPasswordAction)
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField?)])
    func showMessageError(error: String)
}

final class RegistrationPasswordPresenter {
    private typealias Localizable = Strings.RegistrationPassword

    private let coordinator: RegistrationPasswordCoordinating
    weak var viewController: RegistrationPasswordDisplaying?

    init(coordinator: RegistrationPasswordCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - RegistrationPasswordPresenting
extension RegistrationPasswordPresenter: RegistrationPasswordPresenting {
    func configureTerms() {
        let attributedText = AttributedTextURLHelper.createAttributedTextWithLinks(
            text: Localizable.acceptTheTermsAndCondiditions,
            attributedTextURLDelegate: self,
            linkTypes: [LinkType.termsAndCondition.rawValue, LinkType.contractDigitalAccount.rawValue]
        )
        
        viewController?.setAgreementTextView(linkText: attributedText, linkAttributes: AttributedTextURLHelper.linkAttributes)
    }
    
    func didNextStep(action: RegistrationPasswordAction) {
        coordinator.perform(action: action)
    }
    
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField?)]) {
        viewController?.updateFields(fields: fields)
    }
    
    func showMessageError(error: String) {
        viewController?.showMessageError(error: error)
    }
}

extension RegistrationPasswordPresenter: AttributedTextURLDelegate {
    enum LinkType: String {
        case termsAndCondition
        case contractDigitalAccount
    }
    
    func getUrlInfo(linkType: String) -> AttributedTextURLInfo {
        switch linkType {
        case LinkType.termsAndCondition.rawValue:
            return AttributedTextURLInfo(
                urlAddress: RegistrationPasswordEndpoint.useTerms.url,
                text: Localizable.useTerms
            )
        case LinkType.contractDigitalAccount.rawValue:
            return AttributedTextURLInfo(
                urlAddress: RegistrationPasswordEndpoint.contract.url,
                text: Localizable.contract
            )
        default:
            return AttributedTextURLInfo(urlAddress: "", text: "")
        }
    }
}
