import UIKit

enum RegistrationPasswordFactory {
    static func make(model: RegistrationViewModel) -> RegistrationPasswordViewController {
        let container = DependencyContainer()
        let service: RegistrationPasswordServicing = RegistrationPasswordService(dependencies: container)
        let coordinator: RegistrationPasswordCoordinating = RegistrationPasswordCoordinator(dependencies: container)
        let presenter: RegistrationPasswordPresenting = RegistrationPasswordPresenter(coordinator: coordinator)
        let interactor = RegistrationPasswordInteractor(model: model, service: service, presenter: presenter, dependencies: container)
        let viewController = RegistrationPasswordViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
