import Foundation
import AnalyticsModule
import LegacyPJ
import Core
import FeatureFlag

protocol RegistrationPasswordInteracting: AnyObject {
    func configureTerms()
    func openURL(url: String)
    func updateFields(inputs: [UIPPFloatingTextField])
    func submitPassword(inputs: [UIPPFloatingTextField])
    func trackDidLoad()
}

final class RegistrationPasswordInteractor {
    private typealias Localizable = Strings.RegistrationPassword
    
    typealias Dependencies = HasAnalytics & HasAppManager & HasAuthManager & HasKVStore & HasFeatureManager
    private let dependencies: Dependencies
    
    private let service: RegistrationPasswordServicing
    private let presenter: RegistrationPasswordPresenting
    private var model: RegistrationViewModel

    init(
        model: RegistrationViewModel,
        service: RegistrationPasswordServicing,
        presenter: RegistrationPasswordPresenting,
        dependencies: Dependencies
    ) {
        self.model = model
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - RegistrationPasswordInteracting
extension RegistrationPasswordInteractor: RegistrationPasswordInteracting {
    func configureTerms() {
        presenter.configureTerms()
    }
    
    func openURL(url: String) {
        let event = url == RegistrationPasswordEndpoint.useTerms.url ?
            RegistrationPasswordAnalytics.useTerms :
            RegistrationPasswordAnalytics.contract
        dependencies.analytics.log(event)
        
        presenter.didNextStep(action: .openURL(url: url))
    }
    
    func updateFields(inputs: [UIPPFloatingTextField]) {
        let result: [(message: String?, textField: UIPPFloatingTextField?)] = inputs.map {
            switch $0.validate() {
            case .invalid(let error):
                return (error.first?.message, $0)
            default:
                return(nil, $0)
            }
        }
        
        presenter.updateFields(fields: result)
    }
    
    func submitPassword(inputs: [UIPPFloatingTextField]) {
        let validate = validateInputs(inputs: inputs)
        guard validate.isValid else { return }
        
        model.account.password = validate.password
        model.account.termsAccepted = true
        
        trackSettedPassword(passwordInput: inputs.first)
        service.createAccount(model: model) { [weak self] result in
            switch result {
            case .success(let model):
                self?.trackSuccess()
                self?.trackNextStep()
                self?.setValuesInAuthManager(model: model)
                self?.presenter.didNextStep(action: .nextStep)
            case .failure:
                self?.presenter.didNextStep(action: .handleError)
            }
        }
    }
}

// MARK: - Trackings -
extension RegistrationPasswordInteractor {
    func trackSettedPassword(passwordInput: UIPPFloatingTextField?) {
        guard let passwordInput = passwordInput else { return }
        
        let analyticsProperties: [String: Any] = [
            TrackingManager.TrackingManagerProperties.openPasswordTextField.rawValue:
            passwordInput.isSecureTextEntry ? "YES" : "NO"
        ]
        
        dependencies.analytics.log(RegistrationAnalytics.settedPassword(properties: analyticsProperties))
    }
    
    func trackNextStep() {
        dependencies.analytics.log(RegistrationAnalytics.newAccountRegister)
        TrackingManager.trackFirebaseEvent("complete_registration")
    }
    
    func trackSuccess() {
        let type = model.bankAccountType == .personal ?
            BankAccountAnalyticsType(rawValue: "PF") :
            BankAccountAnalyticsType(rawValue: "PJ")
        dependencies.analytics.log(RegistrationAnalytics.finished(type ?? BankAccountAnalyticsType.pf))
    }
    
    func trackDidLoad() {
        TrackingManager.trackEventPublicAuth(.obPassword, authEventName: .obMultPassword, properties: nil)
    }
}

// MARK: - Helper -
private extension RegistrationPasswordInteractor {
    func validateInputs(inputs: [UIPPFloatingTextField]) -> (isValid: Bool, password: String) {
        let result = inputs.allSatisfy { $0.validate().isValid }
        
        guard result else { return (false, "") }
        if inputs.first?.text != inputs.last?.text {
            presenter.showMessageError(error: Localizable.confirmPasswordIsInvalid)
            return (false, "")
        }
        
        guard let password = inputs.first?.text else {
            presenter.showMessageError(error: Localizable.confirmPasswordIsInvalid)
            return (false, "")
        }
        
        return (true, password)
    }
    
    func setValuesInAuthManager(model: RegistrationPasswordAccess?) {
        guard let model = model, let authModel = Auth(json: model.access.toJSON()) else { return }
        if dependencies.featureManager.isActive(.isAppLocalDataUnavailable) {
            dependencies.kvStore.setBool(true, with: BizKvKey.needShowStartPromoCodePopup)
        } else {
            dependencies.appManager.localData.needShowStartPromoCodePopup = true
        }
        dependencies.authManager.authenticateNewAccount(authModel) { _ in
            TrackingManager.setupMixpanel()
        }
    }
}
