import Foundation
import SwiftyJSON

struct RegistrationPasswordAccess: Decodable {
    let access: Access
    let createAccount: Bool
    let completed: Bool
    
    private enum CodingKeys: String, CodingKey {
        case access
        case createAccount = "create_account"
        case completed
    }
}

struct Access: Decodable {
    let accessToken: String
    let tokenType: String
    let expiresIn: Int
    let refreshToken: String
    let biometry: String
    let completed: Bool
    
    private enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case tokenType = "token_type"
        case expiresIn = "expires_in"
        case refreshToken = "refresh_token"
        case biometry
        case completed
    }
    
    func toJSON() -> JSON {
        let json: JSON = [
            "access_token": accessToken,
            "token_type": tokenType,
            "expires_in": Date(timeIntervalSince1970: TimeInterval(expiresIn)),
            "refresh_token": refreshToken,
            "biometry": biometry,
            "completed": completed
        ]
        return json
    }
}
