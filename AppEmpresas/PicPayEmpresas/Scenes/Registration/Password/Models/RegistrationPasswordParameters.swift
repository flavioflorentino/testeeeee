import Foundation

struct RegistrationPasswordParameters: Encodable {
    let cnpj: String
    let corporateName: String
    let cep: String
    let address: String
    let state: String
    let city: String
    let district: String
    let complement: String
    let number: String
    let lat: Double
    let lon: Double
    let displayName: String
    let image: String
    let mobilePhone: String
    let phone: String
    let name: String
    let email: String
    let cpf: String
    let birthDate: String
    let partner: RegistrationPartnerModel
    let password: String
    let admin: Bool
    let bankBranch: String
    let bankBranchDigit: String
    let bankId: String?
    let accountBank: String
    let accountBankDigit: String
    let operation: String
    let legacyType: String
    let daysToReleaseCC: Int
    let distinctId: String
    let documentBank: String
    let termsAccepted: Bool
    let withoutAddress: Bool
    let value: String?
    let type: String?
    
    private enum CodingKeys: String, CodingKey {
        case cnpj
        case corporateName = "razao_social"
        case cep
        case address
        case state
        case city
        case district
        case complement
        case number
        case lat
        case lon
        case displayName = "display_name"
        case image
        case mobilePhone = "mobile_phone"
        case phone
        case name
        case email
        case cpf
        case birthDate = "birth_date"
        case partner
        case password
        case admin
        case bankBranch = "agencia"
        case bankBranchDigit = "agencia_dv"
        case bankId = "banco_id"
        case accountBank = "conta"
        case accountBankDigit = "conta_dv"
        case operation = "operacao"
        case legacyType = "tipo"
        case daysToReleaseCC = "days_to_release_cc"
        case distinctId = "distinct_id"
        case documentBank = "document_bank"
        case termsAccepted = "terms_accepted"
        case withoutAddress = "without_address"
        case value
        case type
    }
    
    init(model: SignUpAccount, partnerModel: RegistrationPartnerModel) {
        cnpj = model.cnpj.onlyNumbers
        corporateName = model.companyName
        cep = model.cep.onlyNumbers
        address = model.address
        state = model.state
        city = model.city
        district = model.district
        complement = model.complement
        number = model.addressNumber
        lat = model.lat
        lon = model.lon
        displayName = model.displayName
        image = model.imageUrl
        mobilePhone = (model.mobilePhoneDdd + model.mobilePhone)
        phone = (model.ddd + model.phone)
        name = model.partner.name
        email = model.partner.email
        cpf = model.partner.cpf.onlyNumbers
        birthDate = model.partner.birthday
        partner = partnerModel
        password = model.password
        admin = true
        bankBranch = model.branch
        bankBranchDigit = model.branchDigit
        bankId = model.bank?.code
        accountBank = model.account
        accountBankDigit = model.accountDigit
        operation = model.accountOperation
        legacyType = model.legacyAccountType.rawValue
        daysToReleaseCC = model.days
        distinctId = model.distinctId
        documentBank = model.documentBank
        termsAccepted = model.termsAccepted
        withoutAddress = model.withoutAddress
        value = model.accountValue?.rawValue
        type = model.accountType?.rawValue
    }
}
