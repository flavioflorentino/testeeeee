import Foundation
import LegacyPJ

struct RegistrationPartnerModel: Encodable {
    let name: String
    let email: String
    let cpf: String
    let birthDate: String
    let motherName: String
    let address: RegistrationAddressModel
    
    private enum CodingKeys: String, CodingKey {
        case name
        case email
        case cpf
        case birthDate = "birth_date"
        case motherName = "mother_name"
        case address
    }
    
    init(model: Partner, addressModel: RegistrationAddressModel) {
        name = model.name
        email = model.email
        cpf = model.cpf.onlyNumbers
        birthDate = model.birthday.onlyNumbers
        motherName = model.motherName
        address = addressModel
    }
}
