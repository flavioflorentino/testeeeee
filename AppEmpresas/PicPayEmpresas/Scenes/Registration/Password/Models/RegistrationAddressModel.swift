import Foundation
import LegacyPJ

struct RegistrationAddressModel: Encodable {
    let street: String
    let number: String
    let district: String
    let complement: String
    let city: String
    let state: String
    let postalCode: String
    
    private enum CodingKeys: String, CodingKey {
        case street
        case number
        case district
        case complement
        case city
        case state
        case postalCode = "postal_code"
    }
    
    init(model: PartnerAddress) {
        street = model.street
        number = model.addressNumber
        district = model.district
        complement = model.complement
        city = model.city
        state = model.state
        postalCode = model.cep
    }
}
