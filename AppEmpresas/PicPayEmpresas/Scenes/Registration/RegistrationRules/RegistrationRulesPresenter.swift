import Foundation
import UI
import UIKit
import FeatureFlag

protocol RegistrationRulesPresenting: AnyObject {
    var viewController: RegistrationRulesDisplay? { get set }
    func setupLinks()
    func didNextStep(action: RegistrationRulesAction)
    func open(url: URL)
}

final class RegistrationRulesPresenter {
    private typealias Localizable = Strings.RegistrationIntro
    
    private let coordinator: RegistrationRulesCoordinating
    weak var viewController: RegistrationRulesDisplay?
    
    init(coordinator: RegistrationRulesCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - RegistrationRulesPresenting
extension RegistrationRulesPresenter: RegistrationRulesPresenting {
    func didNextStep(action: RegistrationRulesAction) {
        coordinator.perform(action: action)
    }
    
    func setupLinks() {
        let attributedText = AttributedTextURLHelper.createAttributedTextWithLinks(
            text: Localizable.rulesInfoDataCollect,
            attributedTextURLDelegate: self,
            linkTypes: [LinkType.contract.rawValue, LinkType.privacy.rawValue]
        )
        viewController?.setAgreementTextView(linkText: attributedText, linkAttributes: AttributedTextURLHelper.linkAttributes)
    }
    
    func open(url: URL) {
        coordinator.perform(action: .open(url: url))
    }
}

extension RegistrationRulesPresenter: AttributedTextURLDelegate {
    enum LinkType: String {
        case contract
        case privacy
    }
    
    func getUrlInfo(linkType: String) -> AttributedTextURLInfo {
        switch linkType {
        case LinkType.contract.rawValue:
            return AttributedTextURLInfo(
                urlAddress: FeatureManager.shared.text(.urlTerms),
                text: Localizable.rulesInfoDataCollectContract
            )
        case LinkType.privacy.rawValue:
            return AttributedTextURLInfo(
                urlAddress: Localizable.rulesPrivacyPolicyLink,
                text: Localizable.rulesInfoDataCollectPrivacyPolicy
            )
        default:
            break
        }
        return AttributedTextURLInfo(urlAddress: "", text: "")
    }
}
