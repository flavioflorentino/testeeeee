import Foundation
import AnalyticsModule

protocol RegistrationRulesInteracting: AnyObject {
    func setupLinks()
    func loadRegistration()
    func open(url: URL)
}

final class RegistrationRulesInteractor {
    typealias Dependencies = HasAnalytics
    
    private let presenter: RegistrationRulesPresenting
    private let dependencies: Dependencies

    init(presenter: RegistrationRulesPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - RegistrationRulesInteracting
extension RegistrationRulesInteractor: RegistrationRulesInteracting {
    func setupLinks() {
        presenter.setupLinks()
    }
    
    func loadRegistration() {
        presenter.didNextStep(action: .register)
    }
    
    func open(url: URL) {
        presenter.open(url: url)
    }
}
