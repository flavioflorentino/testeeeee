import AnalyticsModule
import Foundation

enum RegistrationRulesAnalytics: String, AnalyticsKeyProtocol {
    case onboarding = "OB Register Rules"
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(rawValue, providers: [.mixPanel])
    }
}
