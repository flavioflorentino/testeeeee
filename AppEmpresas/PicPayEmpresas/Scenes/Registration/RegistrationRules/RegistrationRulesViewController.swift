import UI
import UIKit
import SnapKit

protocol RegistrationRulesDisplay: AnyObject {
    func setAgreementTextView(
        linkText: NSAttributedString,
        linkAttributes: [NSAttributedString.Key: Any]
    )
}

final class RegistrationRulesViewController: ViewController<RegistrationRulesInteracting, UIView> {
    private typealias Localizable = Strings.RegistrationIntro

    private lazy var descriptionLabel: UILabel = {
        var label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
            .with(\.textAlignment, .left)
        label.text = Localizable.rulesInfoDescription
        return label
    }()

    private lazy var validCNPJView: RegistrationRulesView = {
        let view = RegistrationRulesView()
        view.setIcon(iconName: Iconography.bag.rawValue)
        view.setInfo(Localizable.rulesInfoValidCNPJ)
        return view
    }()

    private lazy var infoLegalPersonView: RegistrationRulesView = {
        let view = RegistrationRulesView()
        view.setIcon(iconName: Iconography.user.rawValue)
        view.setInfo(Localizable.rulesInfoLegalPersonDetails)
        return view
    }()

    private lazy var pictureLegalPersonView: RegistrationRulesView = {
        let view = RegistrationRulesView()
        view.setIcon(iconName: Iconography.camera.rawValue)
        view.setInfo(Localizable.rulesInfoLegalPersonPicture)
        return view
    }()

    private lazy var infoStackView: UIStackView = {
        var stackView = UIStackView(arrangedSubviews: [validCNPJView, infoLegalPersonView, pictureLegalPersonView])
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = Spacing.base02
        stackView.setContentCompressionResistancePriority(.required, for: .vertical)
        return stackView
    }()

    private lazy var infoAgreementTextView: UITextView = {
        let text = UITextView()
        text.font = Typography.bodyPrimary().font()
        text.textColor = Colors.grayscale700.color
        text.isEditable = false
        text.isSelectable = true
        text.isScrollEnabled = false
        text.backgroundColor = .clear
        text.textContainerInset = .zero
        text.textContainer.lineFragmentPadding = 0
        text.delegate = self
        return text
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.beginSignUpButton, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapConfirmButton), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interactor.setupLinks()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceDefault())
    }

    override func buildViewHierarchy() {
        view.addSubviews(
            descriptionLabel,
            infoStackView,
            infoAgreementTextView,
            confirmButton
        )
    }

    override func setupConstraints() {
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        infoStackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.centerY.equalToSuperview().offset(-Spacing.base05)
            $0.top.greaterThanOrEqualTo(descriptionLabel.snp.bottom).offset(Spacing.base05)
            $0.bottom.lessThanOrEqualTo(infoAgreementTextView.snp.top).offset(-Spacing.base05)
        }

        infoAgreementTextView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(confirmButton.snp.top).offset(-Spacing.base02)
        }

        confirmButton.snp.makeConstraints {
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }

    override func configureViews() {
        title = Localizable.rulesTitle
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }

    @objc
    func didTapConfirmButton() {
        interactor.loadRegistration()
    }
}

extension RegistrationRulesViewController: RegistrationRulesDisplay {
    func setAgreementTextView(
        linkText: NSAttributedString,
        linkAttributes: [NSAttributedString.Key: Any]
    ) {
        infoAgreementTextView.attributedText = linkText
        infoAgreementTextView.linkTextAttributes = linkAttributes
    }
}

extension RegistrationRulesViewController: UITextViewDelegate {
    func textView(
        _ textView: UITextView,
        shouldInteractWith URL: URL,
        in characterRange: NSRange,
        interaction: UITextItemInteraction
    ) -> Bool {
        interactor.open(url: URL)
        return false
    }
}
