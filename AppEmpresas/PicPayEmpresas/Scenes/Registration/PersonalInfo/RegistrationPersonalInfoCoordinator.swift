import UIKit

enum RegistrationPersonalInfoAction {
    case nextScreen
}

protocol RegistrationPersonalInfoCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationPersonalInfoAction)
}

final class RegistrationPersonalInfoCoordinator: RegistrationPersonalInfoCoordinating {
    private let model: RegistrationViewModel

    weak var viewController: UIViewController?

    init(model: RegistrationViewModel) {
        self.model = model
    }
    
    func perform(action: RegistrationPersonalInfoAction) {
        if case .nextScreen = action {
            let controller: RegistrationPhoneViewControllerOld = ViewsManager.instantiateViewController(.registration)
            controller.setup(model: model)
            viewController?.pushViewController(controller)
        }
    }
}
