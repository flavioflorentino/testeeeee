import Core

enum PersonalInfoServiceEndPoint {
    case verifyDuplicatedUser(cpf: String, mail: String)
}

extension PersonalInfoServiceEndPoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .verifyDuplicatedUser:
            return "/account/verify-duplicate-user"
        }
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var body: Data? {
        switch self {
        case let .verifyDuplicatedUser(cpf, mail):
            return ["cpf": cpf.onlyNumbers, "email": mail].toData()
        }
    }
}
