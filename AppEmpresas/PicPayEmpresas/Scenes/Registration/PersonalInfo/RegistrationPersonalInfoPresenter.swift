import Core
import Foundation

protocol RegistrationPersonalInfoPresenting: AnyObject {
    var viewController: RegistrationPersonalInfoDisplay? { get set }
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)])
    func didNextStep(action: RegistrationPersonalInfoAction)
    func validateSubmit(value: Bool)
    func handleError(message: String)
}

final class RegistrationPersonalInfoPresenter: RegistrationPersonalInfoPresenting {
    private let coordinator: RegistrationPersonalInfoCoordinating
    weak var viewController: RegistrationPersonalInfoDisplay?

    init(coordinator: RegistrationPersonalInfoCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: RegistrationPersonalInfoAction) {
        coordinator.perform(action: action)
    }
    
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)]) {
        viewController?.updateFields(fields: fields)
    }
    
    func validateSubmit(value: Bool) {
        guard value else {
            viewController?.handleError(error: nil)
            return
        }
        
        viewController?.submitValues()
    }
    
    func handleError(message: String) {
        viewController?.handleError(error: message)
    }
}
