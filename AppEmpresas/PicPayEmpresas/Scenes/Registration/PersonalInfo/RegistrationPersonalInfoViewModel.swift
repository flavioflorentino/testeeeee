import Foundation

protocol RegistrationPersonalInfoViewModelInputs: AnyObject {
    func updateFields(textFields: [UIPPFloatingTextField])
    func validateSubmit(textFields: [UIPPFloatingTextField])
    func submitValues(name: String, mail: String, cpf: String, birthday: String, motherName: String)
    func trackingViewDidLoad()
}

final class RegistrationPersonalInfoViewModel {
    private let service: RegistrationPersonalInfoServicing
    private let presenter: RegistrationPersonalInfoPresenting

    init(service: RegistrationPersonalInfoServicing, presenter: RegistrationPersonalInfoPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

extension RegistrationPersonalInfoViewModel: RegistrationPersonalInfoViewModelInputs {
    func updateFields(textFields: [UIPPFloatingTextField]) {
        let result: [(message: String?, textField: UIPPFloatingTextField)] = textFields.map {
            switch $0.validate() {
            case .invalid(let error):
                return(error.first?.message, $0)
            default:
                return("", $0)
            }
        }
        
        presenter.updateFields(fields: result)
    }
    
    func validateSubmit(textFields: [UIPPFloatingTextField]) {
        let result = textFields.allSatisfy { $0.validate().isValid }
        presenter.validateSubmit(value: result)
    }
    
    func submitValues(name: String, mail: String, cpf: String, birthday: String, motherName: String) {
        let model = PersonalInfoModel(name: name, mail: mail, cpf: cpf, birthday: birthday, motherName: motherName)
        service.registerPersonalInfo(model: model) {[weak self] result in
            switch result {
            case .success(let userVerify):
                guard let userVerify = userVerify, userVerify.verify else {
                    self?.presenter.handleError(message: RegistrationPersonalInfoLocalizable.verifyDuplciateUserError.rawValue)
                    return
                }
                self?.presenter.didNextStep(action: .nextScreen)
            case .failure(let error):
                self?.presenter.handleError(
                    message: error.requestError?.message ?? RegistrationPersonalInfoLocalizable.verifyDuplciateUserError.rawValue
                )
            }
        }
    }
    
    func trackingViewDidLoad() {
        TrackingManager.trackEventPublicAuth(.obPersonalData, authEventName: .obMultPersonalData, properties: nil)
    }
}
