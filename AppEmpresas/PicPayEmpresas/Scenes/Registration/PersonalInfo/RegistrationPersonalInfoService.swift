import Core
import LegacyPJ

protocol RegistrationPersonalInfoServicing {
    func registerPersonalInfo(model: PersonalInfoModel, _ completion: @escaping (Result<VerifyDuplicatedUser?, ApiError>) -> Void)
}

struct VerifyDuplicatedUser: Decodable {
    let verify: Bool
}

final class RegistrationPersonalInfoService: RegistrationPersonalInfoServicing {
    typealias Dependencies = HasAuthManager & HasMainQueue
    private let dependencies: Dependencies
    private let model: RegistrationViewModel

    init(dependencies: Dependencies, model: RegistrationViewModel) {
        self.dependencies = dependencies
        self.model = model
    }
    
    func registerPersonalInfo(model: PersonalInfoModel, _ completion: @escaping (Result<VerifyDuplicatedUser?, ApiError>) -> Void) {
        /*
         Futuramente sera implementada uma requests para novos endpoints. O atual é enviado uma vez no final do
         cadastro, por isso estou armazenando local e alimentando um modelo legado.
         */
        storagePersonalInfo(model: model)
        
        let endPoint = PersonalInfoServiceEndPoint.verifyDuplicatedUser(cpf: model.cpf, mail: model.mail)
        BizApi<VerifyDuplicatedUser>(endpoint: endPoint).bizExecute {[weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map { $0.model.data })
            }
        }
    }
    
    private func storagePersonalInfo(model: PersonalInfoModel) {
        self.model.account.partner.name = model.name
        self.model.account.partner.email = model.mail
        self.model.account.partner.cpf = model.cpf
        self.model.account.partner.birthday = model.birthday
        self.model.account.partner.motherName = model.motherName
                
        dependencies.authManager.storeUserRegister(model: self.model.account.partner)
    }
}

struct PersonalInfoModel {
    let name: String
    let mail: String
    let cpf: String
    let birthday: String
    let motherName: String
}
