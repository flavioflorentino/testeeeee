import UI
import UIKit
import Validator

protocol RegistrationPersonalInfoViewDelegate: AnyObject {
    func didTapConfirmButton(inputs: [UIPPFloatingTextField])
    func textDidBeginEditing(textField: UITextField)
}

private extension RegistrationPersonalInfoView.Layout {
    enum Size {
        static let inputHeight: CGFloat = 40
    }
}

final class RegistrationPersonalInfoView: UIView, ViewConfiguration {
    typealias Localizable = RegistrationPersonalInfoLocalizable
    
    fileprivate enum Layout {}
    
    private enum TextFieldType: Int {
        case name = 0
        case mail
        case cpf
        case birthday
        case motherName
    }
    
    weak var delegate: RegistrationPersonalInfoViewDelegate?
    
    // MARK: - Private Lazy Vars
    
    private var inputs: [UIPPFloatingTextField] = []
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.subTitle.text
        label.numberOfLines = 2
        return label
    }()

    private lazy var nameTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.namePlaceHolder.text
        textField.border = .none
        textField.returnKeyType = .next
        textField.delegate = self
        textField.tag = TextFieldType.name.rawValue
        inputs.append(textField)
        return textField
    }()
    
    private lazy var mailTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.mailPlaceHolder.text
        textField.border = .none
        textField.returnKeyType = .next
        textField.delegate = self
        textField.tag = TextFieldType.mail.rawValue
        inputs.append(textField)
        return textField
    }()
    
    private lazy var cpfTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.keyboardType = .numberPad
        textField.placeholder = Localizable.cpfPlaceHolder.text
        textField.border = .none
        textField.returnKeyType = .next
        textField.delegate = self
        textField.tag = TextFieldType.cpf.rawValue
        inputs.append(textField)
        return textField
    }()
    
    private lazy var birthdayTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.keyboardType = .numberPad
        textField.placeholder = Localizable.birthdayPlaceHolder.text
        textField.border = .none
        textField.returnKeyType = .next
        textField.tag = TextFieldType.birthday.rawValue
        textField.delegate = self
        inputs.append(textField)
        return textField
    }()
    
    private lazy var motherNameTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.motherNamePlaceHolder.text
        textField.border = .none
        textField.returnKeyType = .done
        textField.delegate = self
        textField.tag = TextFieldType.motherName.rawValue
        inputs.append(textField)
        return textField
    }()
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fillEqually
        stack.spacing = Spacing.base05
        return stack
    }()
    
    private lazy var activity: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView(style: .white)
        activity.stopAnimating()
        activity.isHidden = true
        return activity
    }()
    
    private lazy var okButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.buttonOKTitle.text, for: .normal)
        button.addTarget(self, action: #selector(confirmAction), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Lazy Masks
    
    private lazy var cpfNumberMask: TextFieldMasker = {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        return TextFieldMasker(textMask: mask)
    }()
    
    private lazy var birthdayNumberMaks: TextFieldMasker = {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.date)
        return TextFieldMasker(textMask: mask)
    }()
    
    private lazy var emailDisclaimer: ApolloFeedbackCard = {
        let feedbackCard = ApolloFeedbackCard(description: Localizable.emailDisclaimer.text, iconType: .warning, layoutType: .onlyText)
        return feedbackCard
    }()
    // MARK: - Life Cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        endEditing(true)
    }
    
    func configureViews() {
        setupTextFieldMaks()
        setupTextFieldRules()
    }
    
    func setupConstraints() {
        infoLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.greaterThanOrEqualTo(Spacing.base04)
        }
        
        emailDisclaimer.snp.makeConstraints {
            $0.top.equalTo(infoLabel.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.greaterThanOrEqualTo(Spacing.base09)
        }

        stackView.snp.makeConstraints {
            $0.top.equalTo(emailDisclaimer.snp_bottomMargin).offset(Spacing.base05)
            $0.leading.equalTo(snp.leading).offset(Spacing.base02)
            $0.trailing.equalTo(snp.trailing).inset(Spacing.base02)
        }
        
        okButton.snp.makeConstraints {
            $0.top.equalTo(stackView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(Spacing.base01)
        }
        
        activity.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
        }
        
        nameTextField.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.inputHeight)
        }
        
        mailTextField.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.inputHeight)
        }
        
        cpfTextField.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.inputHeight)
        }
        
        birthdayTextField.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.inputHeight)
        }
        
        motherNameTextField.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.inputHeight)
        }
    }
    
    func buildViewHierarchy() {
        addSubview(infoLabel)
        addSubview(emailDisclaimer)
        addSubview(stackView)
        addSubview(okButton)
        
        okButton.addSubview(activity)
        
        stackView.addArrangedSubview(nameTextField)
        stackView.addArrangedSubview(mailTextField)
        stackView.addArrangedSubview(cpfTextField)
        stackView.addArrangedSubview(birthdayTextField)
        stackView.addArrangedSubview(motherNameTextField)
    }
    
    func configureStyles() {
        okButton.buttonStyle(PrimaryButtonStyle())
        
        infoLabel
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale600())
            .with(\.textAlignment, .center)
    }
}
// MARK: - Private Functions
extension RegistrationPersonalInfoView {
    @objc
    private func confirmAction() {
        delegate?.didTapConfirmButton(inputs: inputs)
    }
}

// MARK: - Setup TextField Configuration
extension RegistrationPersonalInfoView {
    private func setupTextFieldMaks() {
        cpfNumberMask.bind(to: cpfTextField)
        birthdayNumberMaks.bind(to: birthdayTextField)
    }
    
    private func setupTextFieldRules() {
        var nameRule = ValidationRuleSet<String>()
        nameRule.add(rule: ValidationRuleLength(min: 10, error: Localizable.nameMessageError))
        nameTextField.validationRules = nameRule

        var emailRules = ValidationRuleSet<String>()
        emailRules.add(rule: ValidationRulePattern(pattern: EmailValidationPattern.standard, error: Localizable.mailMessageError))
        mailTextField.validationRules = emailRules

        var cpfRules = ValidationRuleSet<String>()
        cpfRules.add(rule: ValidationRuleLength(min: 14, error: Localizable.cpfMessageError))
        cpfTextField.validationRules = cpfRules

        var birthdayRules = ValidationRuleSet<String>()
        birthdayRules.add(rule: ValidationRuleLength(min: 10, error: Localizable.birthdayMessageError))
        birthdayTextField.validationRules = birthdayRules

        var motherNameRules = ValidationRuleSet<String>()
        motherNameRules.add(rule: ValidationRuleLength(min: 5, error: Localizable.motherNameMessageError))
        motherNameTextField.validationRules = motherNameRules
    }
}

// MARK: - Public Functions

extension RegistrationPersonalInfoView {
    func getNameText() -> String? {
        nameTextField.text
    }
    
    func getMailText() -> String? {
        mailTextField.text
    }
    
    func getCPFText() -> String? {
        cpfTextField.text
    }
    
    func getBirthdayText() -> String? {
        birthdayTextField.text
    }
    
    func getMotherNameText() -> String? {
        motherNameTextField.text
    }
    
    func startLoad() {
       activity.isHidden = false
       activity.startAnimating()
       okButton.setTitle("", for: .normal)
    }
       
    func stopLoad() {
       activity.isHidden = true
       activity.stopAnimating()
       okButton.setTitle(Localizable.buttonOKTitle.text, for: .normal)
    }
}

extension RegistrationPersonalInfoView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case TextFieldType.motherName.rawValue:
            textField.resignFirstResponder()
            return true
        default:
            inputs[textField.tag + 1].becomeFirstResponder()
            return false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mailTextField {
            textField.text = textField.text?.lowercased()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.textDidBeginEditing(textField: textField)
    }
}
