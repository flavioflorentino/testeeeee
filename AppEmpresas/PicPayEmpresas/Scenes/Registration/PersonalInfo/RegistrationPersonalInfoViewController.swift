import UI
import UIKit
import Validator

protocol RegistrationPersonalInfoDisplay: AnyObject {
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)])
    func submitValues()
    func handleError(error: String?)
}

private extension RegistrationPersonalInfoViewController.Layout {
    enum Size {
        static let contentScrollHeight: CGFloat = 575
        static let offsetKeyboard: CGFloat = 50
    }
}

final class RegistrationPersonalInfoViewController: ViewController<RegistrationPersonalInfoViewModelInputs, UIView> {
    private typealias Localizable = RegistrationPersonalInfoLocalizable
    
    fileprivate enum Layout {}
    
    // MARK: - Private Lazy Vars
    
    private lazy var scroll = UIScrollView()
    
    private lazy var contentView: RegistrationPersonalInfoView = {
        let view = RegistrationPersonalInfoView()
        view.delegate = self
        return view
    }()
    
    private lazy var alertError = FacialBiometricsErrorView()
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.trackingViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        contentView.stopLoad()
        view.endEditing(true)
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scroll)
        scroll.addSubview(contentView)
    }
    
    override func setupConstraints() {
        scroll.snp.makeConstraints {
            $0.centerX.centerY.top.leading.trailing.bottom.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.top.bottom.leading.trailing.width.equalTo(scroll)
            $0.height.equalTo(Layout.Size.contentScrollHeight)
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.grayscale050.color
        addObservers()
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceOldRegistration())
        title = Localizable.title.message
    }
}

// MARK: - View Delegate
extension RegistrationPersonalInfoViewController: RegistrationPersonalInfoViewDelegate {
    func didTapConfirmButton(inputs: [UIPPFloatingTextField]) {
        contentView.startLoad()
        viewModel.updateFields(textFields: inputs)
        viewModel.validateSubmit(textFields: inputs)
        
        alertError.removeFromSuperview()
    }
    
    func textDidBeginEditing(textField: UITextField) {
        scroll.contentOffset.y = textField.frame.origin.y - Layout.Size.offsetKeyboard
    }
}

// MARK: - Oberserver Functions
extension RegistrationPersonalInfoViewController {
    @objc
    func keyboardWillShow(_ notification: NSNotification) {
        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        scroll.snp.updateConstraints {
            $0.bottom.equalToSuperview().inset(keyboardRect.height)
        }
    }
    
    @objc
    func keyboardWillHide(_ notification: NSNotification) {
        scroll.snp.updateConstraints {
            $0.bottom.equalToSuperview()
        }
        
        scroll.contentOffset.y = 0
    }
}

// MARK: View Model Outputs
extension RegistrationPersonalInfoViewController: RegistrationPersonalInfoDisplay {
    func handleError(error: String?) {
        contentView.stopLoad()
        guard let error = error else {
            return
        }
        
        let topConstraint = alertError.topAnchor.constraint(
            equalTo: view.compatibleSafeAreaLayoutGuide.topAnchor,
            constant: -Spacing.base10
        )
        
        alertError.setup(with: error) {[weak self] _ in
            guard let self = self else {
                return
            }
            
            topConstraint.constant = -Spacing.base10
            UIView.animate(
                withDuration: 0.3,
                animations: self.alertErrorViewAnimated,
                completion: self.finishAlertErrorViewAnimated(_:)
            )
        }
        
        view.addSubview(alertError)
        
        alertError.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        topConstraint.isActive = true
        view.layoutIfNeeded()
        
        topConstraint.constant = Spacing.base02
        UIView.animate(withDuration: 0.3) {
            self.alertErrorViewAnimated()
        }
    }
    
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)]) {
        fields.forEach {
            if let message = $0.message, !message.isEmpty {
                $0.textField.lineColor = Colors.branding300.color
            }
            $0.textField.errorMessage = $0.message
        }
    }
    
    func submitValues() {
        guard
            let name = contentView.getNameText(),
            let mail = contentView.getMailText(),
            let cpf = contentView.getCPFText(),
            let birthday = contentView.getBirthdayText(),
            let motherName = contentView.getMotherNameText()
            else {
                return
        }
        
        viewModel.submitValues(
            name: name,
            mail: mail,
            cpf: cpf,
            birthday: birthday,
            motherName: motherName
        )
    }
    
    private func alertErrorViewAnimated() {
        view.layoutIfNeeded()
    }
    
    private func finishAlertErrorViewAnimated(_ finish: Bool) {
        alertError.removeFromSuperview()
    }
}
