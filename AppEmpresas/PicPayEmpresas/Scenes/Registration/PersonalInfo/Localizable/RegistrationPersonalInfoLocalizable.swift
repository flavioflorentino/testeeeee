import Foundation
import Validator

enum RegistrationPersonalInfoLocalizable: String, Localizable, ValidationError {
    case title
    case subTitle
    case namePlaceHolder
    case mailPlaceHolder
    case cpfPlaceHolder
    case birthdayPlaceHolder
    case motherNamePlaceHolder
    case buttonOKTitle
    case nameMessageError
    case mailMessageError
    case cpfMessageError
    case birthdayMessageError
    case motherNameMessageError
    case verifyDuplciateUserError
    case emailDisclaimer
    
    var key: String {
        rawValue
    }
    
    var message: String {
        text
    }
    
    var file: LocalizableFile {
        .registrationPersonalInfo
    }
}
