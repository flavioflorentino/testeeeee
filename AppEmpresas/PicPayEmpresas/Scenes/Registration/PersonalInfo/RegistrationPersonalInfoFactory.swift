import Foundation

enum RegistrationPersonalInfoFactory {
    static func make(model: RegistrationViewModel) -> RegistrationPersonalInfoViewController {
        let container = DependencyContainer()
        let service: RegistrationPersonalInfoServicing = RegistrationPersonalInfoService(dependencies: container, model: model)
        let coordinator: RegistrationPersonalInfoCoordinating = RegistrationPersonalInfoCoordinator(model: model)
        let presenter: RegistrationPersonalInfoPresenting = RegistrationPersonalInfoPresenter(coordinator: coordinator)
        let viewModel = RegistrationPersonalInfoViewModel(service: service, presenter: presenter)
        let viewController = RegistrationPersonalInfoViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
