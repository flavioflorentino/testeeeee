import CoreSellerAccount
import Foundation
import UIKit

enum BankAccountTipsStatus {
    case register(registrationModel: RegistrationViewModel)
    case edit(seller: Seller)
}

enum BankAccountTipsFactory {
    static func makeEditTips(
        companyType: CompanyType,
        seller: Seller
    ) -> BankAccountTipsViewController {
        make(status: .edit(seller: seller), companyType: companyType)
    }
    
    static func makeRegisterTips(
        companyType: CompanyType,
        registrationModel: RegistrationViewModel
    ) -> BankAccountTipsViewController {
        make(status: .register(registrationModel: registrationModel), companyType: companyType)
    }
}

private extension BankAccountTipsFactory {
    static func make(
        status: BankAccountTipsStatus,
        companyType: CompanyType
    ) -> BankAccountTipsViewController {
        let container = DependencyContainer()
        let coordinator: BankAccountTipsCoordinating = BankAccountTipsCoordinator(container: container)
        let presenter: BankAccountTipsPresenting = BankAccountTipsPresenter(coordinator: coordinator)
        
        let interactor = BankAccountTipsInteractor(
            presenter: presenter,
            dependencies: container,
            status: status,
            companyType: companyType
        )
        
        let viewController = BankAccountTipsViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
