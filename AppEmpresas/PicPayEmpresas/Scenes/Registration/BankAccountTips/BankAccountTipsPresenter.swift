import AssetsKit
import CoreSellerAccount
import Foundation
import UI

protocol BankAccountTipsPresenting: AnyObject {
    var viewController: BankAccountTipsDisplay? { get set }
    
    func displayTitle(_ title: String)
    func displayText(_ text: String)
    func displayFAQTitle(_ title: String)
    func displayTips(companyType: CompanyType)
    func didNextStep(action: BankAccountTipsAction)
    func navigateToBankSelection(registrationModel: RegistrationViewModel)
}

final class BankAccountTipsPresenter {
    private typealias Localizable = Strings.BankAccount.BankAccountTips
    
    private let coordinator: BankAccountTipsCoordinating
    weak var viewController: BankAccountTipsDisplay?

    init(coordinator: BankAccountTipsCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - Private Methods

private extension BankAccountTipsPresenter {
    func setupIndividualTips() {
        let firstIcon = Resources.Icons.icoGreenStore.image
        let firstTipString = NSMutableAttributedString()
        firstTipString.append(createDefaultText(text: Localizable.Individual.FirstTip.first))
        firstTipString.append(createBoldText(text: Localizable.Individual.FirstTip.second))
        
        let secondIcon = Resources.Icons.icoGreenSuitcase.image
        let secondTipString = NSMutableAttributedString()
        secondTipString.append(createDefaultText(text: Localizable.Individual.SecondTip.first))
        secondTipString.append(createBoldText(text: Localizable.Individual.SecondTip.second))
        
        let tips: [BankAccountTip] = [
            BankAccountTip(icon: firstIcon, attributedText: firstTipString),
            BankAccountTip(icon: secondIcon, attributedText: secondTipString)
        ]
        
        viewController?.displayTips(tips)
    }
    
    func setupMultiPartnersTips() {
        let firstIcon = Resources.Icons.icoGreenSuitcase.image
        let firstTipString = NSMutableAttributedString()
        firstTipString.append(createDefaultText(text: Localizable.Multi.FirstTip.first))
        firstTipString.append(createBoldText(text: Localizable.Multi.FirstTip.second))
        
        let secondIcon = Resources.Icons.icoGreenStore.image
        let secondTipString = NSMutableAttributedString()
        secondTipString.append(createDefaultText(text: Localizable.Multi.SecondTip.first))
        secondTipString.append(createBoldText(text: Localizable.Multi.SecondTip.second))
        
        let tips: [BankAccountTip] = [
            BankAccountTip(icon: firstIcon, attributedText: firstTipString),
            BankAccountTip(icon: secondIcon, attributedText: secondTipString)
        ]
        
        viewController?.displayTips(tips)
    }
    
    func createDefaultText(text: String) -> NSAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodyPrimary().font(),
            .foregroundColor: Colors.black.color
        ]
        
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    func createBoldText(text: String) -> NSAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodyPrimary(.highlight).font(),
            .foregroundColor: Colors.black.color
        ]
        
        return NSAttributedString(string: text, attributes: attributes)
    }
}

// MARK: - BankAccountTipsPresenting

extension BankAccountTipsPresenter: BankAccountTipsPresenting {
    func displayTitle(_ title: String) {
        viewController?.displayTitle(title)
    }
    
    func displayText(_ text: String) {
        viewController?.displayText(text)
    }
    
    func displayFAQTitle(_ title: String) {
        viewController?.displayFAQTitle(title)
    }

    func displayTips(companyType: CompanyType) {
        switch companyType {
        case .individual:
            setupIndividualTips()
        case .multiPartners:
            setupMultiPartnersTips()
        }
    }
    
    func didNextStep(action: BankAccountTipsAction) {
        coordinator.perform(action: action)
    }
    
    func navigateToBankSelection(registrationModel: RegistrationViewModel) {
        coordinator.navigateToBankSelection(registrationModel: registrationModel)
    }
}
