import AnalyticsModule
import CoreSellerAccount
import LegacyPJ
import UIKit

enum BankAccountTipsAction: Equatable {
    case editBankAccount(seller: Seller?, companyType: CompanyType)
    case helpModal(info: AlertModalInfo, seller: Seller?)
}

protocol BankAccountTipsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BankAccountTipsAction)
    
    // This method is being used because RegistrationViewModel is legacy and cannot implement Equatable
    func navigateToBankSelection(registrationModel: RegistrationViewModel)
}

final class BankAccountTipsCoordinator {
    typealias Dependencies = HasAuthManager & HasAnalytics
    
    weak var viewController: UIViewController?
    
    private let container: Dependencies
    
    init(container: Dependencies) {
        self.container = container
    }
}

// MARK: - BankAccountTipsCoordinating

extension BankAccountTipsCoordinator: BankAccountTipsCoordinating {
    func perform(action: BankAccountTipsAction) {
        switch action {
        case let .editBankAccount(seller, companyType):
            let bankAccountViewController = BankAccountFormFactory.make(seller, companyType: companyType)
            viewController?.navigationController?.pushViewController(bankAccountViewController, animated: true)
        case let .helpModal(info, seller):
            let alertController = createPopUp(info: info, seller: seller)
            showAlert(contentController: alertController)
        }
    }
    
    func navigateToBankSelection(registrationModel: RegistrationViewModel) {
        let bankViewController: RegistrationBankSelectStepViewController = ViewsManager.instantiateViewController(.registration)
        bankViewController.setup(model: registrationModel)
        viewController?.navigationController?.pushViewController(bankViewController, animated: true)
    }
}

// MARK: - Private Methods

private extension BankAccountTipsCoordinator {
    func createPopUp(info: AlertModalInfo, seller: Seller?) -> AlertModalViewController {
        let alertPopup = AlertModalFactory.make(alertModalInfo: info)
        
        alertPopup.touchButtonAction = { [weak self] alert in
            self?.container.analytics.log(BankAccountFormAnalytics.issueResolved)
            alert.dismiss(animated: true)
        }
        
        alertPopup.touchSecondaryButtonAction = { [weak self] alert in
            self?.container.analytics.log(BankAccountFormAnalytics.needMoreHelp)
            alert.dismiss(animated: true) {
                self?.navigateToChat(seller: seller)
            }
        }

        return alertPopup
    }
    
    func showAlert(contentController: UIViewController) {
        let popup = PopupViewController()
        popup.contentController = contentController
        popup.hasCloseButton = false
        viewController?.present(popup, animated: true, completion: nil)
    }
    
    func navigateToChat(seller: Seller?) {
        guard let viewController = viewController else { return }
        let token = container.authManager.userAuth?.auth.accessToken
        BIZCustomerSupportManager(accessToken: token).presentFAQ(from: viewController, with: seller)
    }
}
