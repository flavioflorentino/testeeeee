import AnalyticsModule

enum BankAccountTipsAnalytics: AnalyticsKeyProtocol {
    case toCheck
    case toKnowMore
    case editBankAccount
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .toCheck:
            return AnalyticsEvent("Bank Account - To Check", providers: [.mixPanel])
        case .toKnowMore:
            return AnalyticsEvent("Bank Account - To Know More", providers: [.mixPanel])
        case .editBankAccount:
            return AnalyticsEvent("Bank Account - To Update", providers: [.mixPanel])
        }
    }
}
