import AnalyticsModule
import AssetsKit
import CoreSellerAccount
import Foundation

protocol BankAccountTipsInteracting: AnyObject {
    func initialLoad()
    
    func continueAction()
    func noAccountAction()
}

final class BankAccountTipsInteractor {
    private typealias Localizable = Strings.BankAccount.BankAccountTips
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let presenter: BankAccountTipsPresenting
    private let status: BankAccountTipsStatus
    private let companyType: CompanyType

    init(
        presenter: BankAccountTipsPresenting,
        dependencies: Dependencies,
        status: BankAccountTipsStatus,
        companyType: CompanyType
    ) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.status = status
        self.companyType = companyType
    }
}

// MARK: - Private Methods

private extension BankAccountTipsInteractor {
    func setupTitleAndText() {
        switch status {
        case .register:
            presenter.displayTitle(Localizable.Register.title)
            presenter.displayText(Localizable.Register.text)
        case .edit:
            presenter.displayTitle(Localizable.Edit.title)
            presenter.displayText(Localizable.Edit.text)
        }

        switch companyType {
        case .individual:
            presenter.displayFAQTitle(Localizable.Individual.secondaryButtonTitle)
        case .multiPartners:
            presenter.displayFAQTitle(Localizable.Multi.secondaryButtonTitle)
        }
    }
    
    func editBankAccountEvent() {
        dependencies.analytics.log(BankAccountTipsAnalytics.editBankAccount)
    }

    func helpModalEvent() {
        dependencies.analytics.log(BankAccountTipsAnalytics.toKnowMore)
    }
}

// MARK: - BankAccountTipsInteracting

extension BankAccountTipsInteractor: BankAccountTipsInteracting {
    func initialLoad() {
        setupTitleAndText()
        presenter.displayTips(companyType: companyType)
    }
    
    func continueAction() {
        switch status {
        case .edit(let seller):
            presenter.didNextStep(action: .editBankAccount(seller: seller, companyType: companyType))
            editBankAccountEvent()
        case .register(let registrationModel):
            presenter.navigateToBankSelection(registrationModel: registrationModel)
        }
    }
    
    func noAccountAction() {
        var seller: Seller?
        if case let BankAccountTipsStatus.edit(statusSeller) = status {
            seller = statusSeller
        }
        
        let subtitle: String
        switch companyType {
        case .individual:
            subtitle = Localizable.HelpModal.Individual.subtitle
        case .multiPartners:
            subtitle = Localizable.HelpModal.Multi.subtitle
        }
        
        let imageInfo = AlertModalInfoImage(image: Resources.Icons.icoBlueInfo.image)
        let info = AlertModalInfo(
            imageInfo: imageInfo,
            title: Localizable.HelpModal.title,
            subtitle: subtitle,
            buttonTitle: Localizable.HelpModal.buttonTitle,
            secondaryButtonTitle: Localizable.HelpModal.secondaryButtonTitle
        )
        presenter.didNextStep(action: .helpModal(info: info, seller: seller))
        helpModalEvent()
    }
}
