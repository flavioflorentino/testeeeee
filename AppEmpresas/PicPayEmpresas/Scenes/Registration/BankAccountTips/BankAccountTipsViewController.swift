import SnapKit
import UI
import UIKit

struct BankAccountTip {
    let icon: UIImage
    let attributedText: NSMutableAttributedString
}

protocol BankAccountTipsDisplay: AnyObject {
    func displayTitle(_ title: String)
    func displayText(_ text: String)
    func displayFAQTitle(_ title: String) 
    func displayTips(_ tips: [BankAccountTip])
}

final class BankAccountTipsViewController: ViewController<BankAccountTipsInteracting, UIView> {
    private typealias Localizable = Strings.BankAccount.BankAccountTips
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle()).with(\.textColor, Colors.grayscale600.color)
        return label
    }()
    
    private lazy var tipsView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var tipsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [continueButton, noAccountButton])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.buttonTitle, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTouchContinueButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var noAccountButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTouchNoAccountButton), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
        interactor.initialLoad()
    }

    override func buildViewHierarchy() {
        tipsView.addSubview(tipsStackView)
        
        view.addSubview(titleLabel)
        view.addSubview(tipsView)
        view.addSubview(buttonsStackView)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        tipsView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom)
            $0.leading.trailing.equalTo(titleLabel)
        }
        
        tipsStackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
        
        buttonsStackView.snp.makeConstraints {
            $0.top.equalTo(tipsView.snp.bottom)
            $0.leading.trailing.equalTo(titleLabel)
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - Actions

@objc
private extension BankAccountTipsViewController {
    func didTouchContinueButton() {
        interactor.continueAction()
    }
    
    func didTouchNoAccountButton() {
        interactor.noAccountAction()
    }
}

// MARK: BankAccountTipsDisplay

extension BankAccountTipsViewController: BankAccountTipsDisplay {
    func displayTitle(_ title: String) {
        self.title = title
    }
    
    func displayText(_ text: String) {
        titleLabel.text = text
    }

    func displayFAQTitle(_ title: String) {
        noAccountButton.setTitle(title, for: .normal)
    }
    
    func displayTips(_ tips: [BankAccountTip]) {
        for tip in tips {
            let tipView = BankAccountTipsTipView()
            tipView.setup(icon: tip.icon, attributedText: tip.attributedText)
            tipsStackView.addArrangedSubview(tipView)
        }
    }
}
