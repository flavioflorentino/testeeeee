import SnapKit
import UI
import UIKit

private extension BankAccountTipsTipView.Layout {
    enum Image {
        static let size = CGSize(width: 24, height: 24)
    }
}

final class BankAccountTipsTipView: UIView {
    fileprivate enum Layout { }

    private lazy var iconImageView = UIImageView()
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: Internal Methods

extension BankAccountTipsTipView {
    func setup(icon: UIImage, attributedText: NSAttributedString) {
        iconImageView.image = icon
        titleLabel.attributedText = attributedText
    }
}

// MARK: ViewConfiguration

extension BankAccountTipsTipView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(iconImageView)
        addSubview(titleLabel)
    }
    
    func setupConstraints() {
        iconImageView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.Image.size)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(iconImageView)
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview()
        }
    }
}
