import Core

enum BankAccountTypesEndpoint {
    case getAccountTypes(bankID: String, cnpj: String)
}

extension BankAccountTypesEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case let .getAccountTypes(bankID, cnpj):
            return "/v2/bank/account-types/\(bankID)/\(cnpj)"
        }
    }
}
