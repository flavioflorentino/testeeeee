import Core
import Foundation

protocol BankAccountTypesServicing {
    func fetchBankAccountTypes(bankID: String, cnpj: String, completion: @escaping(Result<[BankAccountType], ApiError>) -> Void)
}

final class BankAccountTypesService {
    typealias Dependencies = HasMainQueue
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
}

extension BankAccountTypesService: BankAccountTypesServicing {
    func fetchBankAccountTypes(bankID: String, cnpj: String, completion: @escaping(Result<[BankAccountType], ApiError>) -> Void) {
        let endpoint = BankAccountTypesEndpoint.getAccountTypes(bankID: bankID, cnpj: cnpj.onlyNumbers)
        
        BizApi<[BankAccountType]>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let optionalResult = result.map(\.model.data)
                
                switch optionalResult {
                case .success(let list):
                    guard let list = list else {
                        completion(.failure(ApiError.bodyNotFound))
                        return
                    }
                    
                    completion(.success(list))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}

struct BankAccountType: Decodable {
    enum Value: String, Decodable {
        case current
        case savings
        case currentEasy = "current_easy"
    }
    
    enum OwnerType: String, Decodable {
        case individual, company
    }
    
    let label: String
    let value: Value
    let type: OwnerType
}
