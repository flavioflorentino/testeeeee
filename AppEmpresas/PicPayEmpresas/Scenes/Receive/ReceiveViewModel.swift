import Foundation

protocol ReceiveViewModelInputs: AnyObject {
    func linkAction()
    func loadQrCodeImage()
}

final class ReceiveViewModel {
    private let service: ReceiveServicing
    private let presenter: ReceivePresenting

    init(service: ReceiveServicing, presenter: ReceivePresenting) {
        self.service = service
        self.presenter = presenter
    }
}

extension ReceiveViewModel: ReceiveViewModelInputs {
    func linkAction() {
        presenter.didNextStep(action: .valueScreen)
    }
    
    func loadQrCodeImage() {
        service.loadQrCodeImage { [weak self] result in
            switch result {
            case .success(let model):
                self?.presenter.generatedQrImage(with: model?.paymentLink)
            case .failure:
                self?.presenter.handleError()
            }
        }
    }
}
