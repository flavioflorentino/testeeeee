import UI
import UIKit
protocol RefreshQRErrorDelegate: AnyObject {
    func refreshAction()
}
extension ReceiveQRErrorView.Layout {
    enum Size {
        static let sadImageSize: CGFloat = 72
    }
}

final class ReceiveQRErrorView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var sadImagem = UIImageView(image: Assets.iconBadGray.image)
    
    private lazy var refreshButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.setTitle(Strings.Receive.qrRefreshLabel, for: .normal)
        button.setImage(Assets.iconRefresh.image, for: .normal)
        button.addTarget(self, action: #selector(actionRefreshButton), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: RefreshQRErrorDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.lightColor
    }
    
    func setupConstraints() {
        sadImagem.layout {
            $0.centerX == centerXAnchor
            $0.top == topAnchor + Spacing.base05
            $0.width == Layout.Size.sadImageSize
            $0.height == Layout.Size.sadImageSize
        }
        
        refreshButton.layout {
            $0.top == sadImagem.bottomAnchor + Spacing.base05
            $0.centerX == centerXAnchor
            $0.leading == leadingAnchor + Spacing.base06
            $0.trailing == trailingAnchor - Spacing.base06
            $0.height == Spacing.base03
        }
    }
    
    func buildViewHierarchy() {
        addSubview(sadImagem)
        addSubview(refreshButton)
    }
    
    @objc
    private func actionRefreshButton() {
        delegate?.refreshAction()
    }
}
