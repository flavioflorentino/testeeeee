import UI
import UIKit
extension ImageLoadView.Layout {
    enum Constraints {
        static let activitySize: CGFloat = 40
        static let labelTop: CGFloat = 10
    }
}

final class ImageLoadView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var activityView: UIActivityIndicatorView = {
        if #available(iOS 13.0, *) {
            return UIActivityIndicatorView(style: .large)
        } else {
            return UIActivityIndicatorView(style: .gray)
        }
    }()
    
    private lazy var loadLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Receive.qrLoadLabel
        label.font = Typography.bodyPrimary(.default).font()
        label.textColor = Colors.grayscale500.color
        return label
    }()
    
    override var isHidden: Bool {
        didSet {
            isHidden == true ? activityView.stopAnimating() : activityView.startAnimating()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
        activityView.startAnimating()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.lightColor
    }
    
    func setupConstraints() {
        activityView.layout {
            $0.centerX == centerXAnchor
            $0.top == topAnchor + Spacing.base09
            $0.width == Layout.Constraints.activitySize
            $0.height == Layout.Constraints.activitySize
        }
        
        loadLabel.layout {
            $0.top == activityView.bottomAnchor + Layout.Constraints.labelTop
            $0.centerX == centerXAnchor
        }
    }
    
    func buildViewHierarchy() {
        addSubview(activityView)
        addSubview(loadLabel)
    }
}
