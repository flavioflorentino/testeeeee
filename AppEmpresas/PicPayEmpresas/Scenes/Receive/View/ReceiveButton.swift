import SnapKit
import UI
import UIKit

struct ReceiveButtonConfiguration {
    let image: UIImage
    let title: String
    let information: String
}

extension ReceiveButton.Layout {
    enum Size {
        static let image = CGSize(width: 50, height: 50)
        static let arrow = CGSize(width: 24, height: 24)
        static let titleLabelSize: CGFloat = 21
    }
    
    enum Constraint {
        static let arrowTrailing: CGFloat = 11
    }
}

final class ReceiveButton: UIButton, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var image = UIImageView()
    
    private lazy var title: UILabel = {
        let label = UILabel()
        label.textColor = Colors.grayscale700.color
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        return label
    }()
    
    private lazy var info: UILabel = {
        let label = UILabel()
        label.textColor = Colors.grayscale500.color
        label.font = Typography.caption().font()
        label.numberOfLines = 3
        return label
    }()
    
    private lazy var arrowImage = UIImageView(image: Assets.iconBlackArrow.image)
    
    init(configuration: ReceiveButtonConfiguration) {
        super.init(frame: .zero)
        setup(configuration: configuration)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        image.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.Size.image)
        }
        
        title.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.equalTo(image.snp.trailing).offset(Spacing.base03)
            $0.trailing.equalTo(arrowImage.snp.leading).offset(Spacing.base02)
            $0.height.equalTo(Layout.Size.titleLabelSize)
        }
        
        info.snp.makeConstraints {
            $0.top.equalTo(title.snp.bottom)
            $0.leading.equalTo(image.snp.trailing).offset(Spacing.base03)
            $0.trailing.equalTo(arrowImage.snp.leading).offset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
        }
        
        arrowImage.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().inset(Layout.Constraint.arrowTrailing)
            $0.size.equalTo(Layout.Size.arrow)
        }
    }
    
    func buildViewHierarchy() {
        addSubview(image)
        addSubview(title)
        addSubview(info)
        addSubview(arrowImage)
    }
    
    func configureViews() {
        backgroundColor = Colors.white.lightColor
    }
    
    private func setup(configuration: ReceiveButtonConfiguration) {
        image.image = configuration.image
        title.text = configuration.title
        info.text = configuration.information
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.beginTracking(touch, with: event)
        backgroundColor = Colors.grayscale200.color
        return true
    }
    
    override func cancelTracking(with event: UIEvent?) {
        super.cancelTracking(with: event)
        backgroundColor = Colors.white.lightColor
    }
    
    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        super.endTracking(touch, with: event)
        backgroundColor = Colors.white.lightColor
    }
}
