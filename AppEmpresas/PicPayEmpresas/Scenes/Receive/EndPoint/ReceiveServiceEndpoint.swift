import Core

enum ReceiveServiceEndpoint {
    case qrImage
    case generatedLink(value: Int?)
}

extension ReceiveServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .qrImage:
            return "/ppcode"
        case .generatedLink:
            return  "/charge"
        }
    }
    
    var method: HTTPMethod {
        .get
    }
    
    var parameters: [String: Any] {
        switch self {
        case .generatedLink(let value):
            guard let value = value else {
                return [:]
            }
            return ["value": value]
        case .qrImage:
            return [:]
        }
    }
}
