import AnalyticsModule

enum ReceiveAnalytics: AnalyticsKeyProtocol {
    case touchShareLink
    case inputValueScreen
    case sendLinkAction(value: Double?)
    
    private var name: String {
        switch self {
        case .touchShareLink:
            return "Send Charge Button"
        case .inputValueScreen:
            return "Choose Charge Value"
        case .sendLinkAction:
            return "Send Link Button"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    private var properties: [String: Any] {
        switch self {
        case .sendLinkAction(let value):
            return ["valor": value ?? 0]
        default:
            return [:]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
