import Foundation

enum ReceiveFactory {
    static func make() -> ReceiveViewController {
        let service: ReceiveServicing = ReceiveService(dependencies: DependencyContainer())
        let coordinator: ReceiveCoordinating = ReceiveCoordinator()
        let presenter: ReceivePresenting = ReceivePresenter(coordinator: coordinator)
        let viewModel = ReceiveViewModel(service: service, presenter: presenter)
        let viewController = ReceiveViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
