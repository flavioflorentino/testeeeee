import Core
import Foundation
import UIKit

protocol ReceivePresenting: AnyObject {
    var viewController: ReceiveDisplay? { get set }
    func didNextStep(action: ReceiveAction)
    func generatedQrImage(with code: String?)
    func handleError()
}

final class ReceivePresenter: ReceivePresenting {
    enum ImageConfigurator: String {
        case filterName = "CIQRCodeGenerator"
        case inputMessage
        case inputCorrectionLevel
        case qualityCorrection = "H"
    }
    
    private let coordinator: ReceiveCoordinating
    private let sizeDefault: CGFloat = 206
    weak var viewController: ReceiveDisplay?

    init(coordinator: ReceiveCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: ReceiveAction) {
        coordinator.perform(action: action)
    }
    
    func generatedQrImage(with code: String?) {        
        guard
            let code = code,
            let filter = CIFilter(name: ImageConfigurator.filterName.rawValue)
            else {
                return
        }
        
        filter.setValue(code.data(using: String.Encoding.utf8), forKey: ImageConfigurator.inputMessage.rawValue)
        filter.setValue(ImageConfigurator.qualityCorrection.rawValue, forKey: ImageConfigurator.inputCorrectionLevel.rawValue)
        
        guard let output = filter.outputImage else {
            return
        }
        
        let scaleX = sizeDefault / output.extent.size.width
        let scaleY = sizeDefault / output.extent.size.height
        
        let transformedImage = output.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
        
        viewController?.updateQrImage(image: UIImage(ciImage: transformedImage))
    }
    
    func handleError() {
        viewController?.showQrError()
    }
}
