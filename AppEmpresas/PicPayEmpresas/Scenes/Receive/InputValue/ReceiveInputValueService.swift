import Core
import Foundation

struct SharePaymentLink: Decodable {
    let url: String
}

protocol ReceiveInputValueServicing {
    typealias LinkCompletionBlock = (Result<SharePaymentLink, ApiError>) -> Void
    
    func getLink(value: Int?, _ completion: @escaping LinkCompletionBlock)
}

final class ReceiveInputValueService: ReceiveInputValueServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getLink(value: Int?, _ completion: @escaping LinkCompletionBlock) {
        Core.Api<SharePaymentLink>(endpoint: ReceiveServiceEndpoint.generatedLink(value: value)).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
