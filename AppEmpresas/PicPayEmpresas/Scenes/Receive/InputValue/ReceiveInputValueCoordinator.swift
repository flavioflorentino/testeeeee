import UIKit

enum ReceiveInputValueAction {
    case goBack
}

protocol ReceiveInputValueCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ReceiveInputValueAction)
}

final class ReceiveInputValueCoordinator: ReceiveInputValueCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: ReceiveInputValueAction) {
        if case .goBack = action {
            viewController?.navigationController?.popToRootViewController(animated: true)
        }
    }
}
