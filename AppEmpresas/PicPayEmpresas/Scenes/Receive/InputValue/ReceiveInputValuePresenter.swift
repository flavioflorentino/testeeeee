import Core
import Foundation

protocol ReceiveInputValuePresenting: AnyObject {
    var viewController: ReceiveInputValueDisplay? { get set }
    func didNextStep(action: ReceiveInputValueAction)
    func changeEnableValueConfirmButton(enable: Bool)
    func showShareActivity(link: String, value: Double?)
    func handleError()
}

final class ReceiveInputValuePresenter: ReceiveInputValuePresenting {
    private let coordinator: ReceiveInputValueCoordinating
    weak var viewController: ReceiveInputValueDisplay?

    init(coordinator: ReceiveInputValueCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: ReceiveInputValueAction) {
        coordinator.perform(action: action)
    }
    
    func changeEnableValueConfirmButton(enable: Bool) {
        viewController?.changeEnableValueConfirmButton(enable: enable)
    }
    
    func showShareActivity(link: String, value: Double?) {
        viewController?.showActivityShare(link: link, value: value)
    }
    
    func handleError() {
        viewController?.showError()
    }
}
