import AnalyticsModule
import UI
import UIKit

protocol ReceiveInputValueDisplay: AnyObject {
    func changeEnableValueConfirmButton(enable: Bool)
    func showActivityShare(link: String, value: Double?)
    func showError()
}

extension ReceiveInputValueViewController.Layout {
    enum Size {
        static let confirmButtonHeight: CGFloat = 48
    }
    
    enum Fonts {
        static let fontCurrency = UIFont.systemFont(ofSize: 24, weight: .regular)
        static let fontValue = UIFont.systemFont(ofSize: 54, weight: .light)
    }
}

final class ReceiveInputValueViewController: ViewController<ReceiveInputValueViewModelInputs, UIView> {
    private typealias Localizable = Strings.Receive
    fileprivate enum Layout {}
    
    private let dependencies: HasAnalytics = DependencyContainer()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.valueDescription
        label.textAlignment = .center
        label.textColor = Colors.grayscale700.color
        label.font = Typography.bodySecondary().font()
        return label
    }()
    
    private lazy var amountTextField: CurrencyField = {
        let field = CurrencyField()
        field.fontCurrency = Layout.Fonts.fontCurrency
        field.fontValue = Layout.Fonts.fontValue
        field.placeholderColor = Colors.grayscale300.color
        field.textColor = Colors.grayscale700.color
        field.spacing = 2
        field.limitValue = 8
        field.delegate = self
        return field
    }()
    
    private lazy var confirmButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(Localizable.titleInputValue, for: .normal)
        button.cornerRadius = Layout.Size.confirmButtonHeight / 2
        button.normalBackgrounColor = Colors.branding700.color
        button.normalTitleColor = Colors.white.color
        button.disabledBackgrounColor = Colors.grayscale300.color
        button.disabledTitleColor = Colors.white.color
        button.borderColor = .clear
        button.isEnabled = false
        button.addTarget(self, action: #selector(confirmAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var withoutValueButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.setTitle(Localizable.withoutValueLink, for: .normal)
        button.addTarget(self, action: #selector(generateLinkWithoutValue), for: .touchUpInside)
        return button
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        amountTextField.becomeFirstResponder()
        dependencies.analytics.log(ReceiveAnalytics.inputValueScreen)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(infoLabel)
        view.addSubview(amountTextField)
        view.addSubview(confirmButton)
        view.addSubview(withoutValueButton)
    }
    
    override func configureViews() {
        title = Localizable.titleInputValue
        view.backgroundColor = Colors.white.lightColor
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
        view.isUserInteractionEnabled = true
    }
    
    override func setupConstraints() {
        infoLabel.snp.makeConstraints {
            $0.top.equalTo(view).offset(Spacing.base06)
            $0.leading.equalTo(view).offset(Spacing.base02)
            $0.trailing.equalTo(view).inset(Spacing.base02)
        }
        
        amountTextField.snp.makeConstraints {
            $0.top.equalTo(infoLabel.snp.bottom).offset(Spacing.base00)
            $0.centerX.equalToSuperview()
        }
        
        confirmButton.snp.makeConstraints {
            $0.top.equalTo(amountTextField.snp.bottom).offset(Spacing.base05)
            $0.leading.equalTo(view).offset(Spacing.base02)
            $0.trailing.equalTo(view).inset(Spacing.base02)
            $0.height.equalTo(Layout.Size.confirmButtonHeight)
        }
        
        withoutValueButton.snp.makeConstraints {
            $0.top.equalTo(confirmButton.snp.bottom).offset(Spacing.base03)
            $0.centerX.equalToSuperview()
        }
    }
}

// MARK: - Private Methods

private extension ReceiveInputValueViewController {
    func showLoad(_ load: Bool) {
        guard load else {
            confirmButton.stopLoadingAnimating()
            confirmButton.setTitle(Localizable.titleInputValue, for: .normal)
            return
        }
        
        confirmButton.startLoadingAnimating()
        confirmButton.setTitle(nil, for: .normal)
    }
}

// MARK: - @Obj Methods

@objc
private extension ReceiveInputValueViewController {
    func confirmAction() {
        view.endEditing(true)
        showLoad(true)
        viewModel.generatedLink(value: amountTextField.value)
    }
    
    func generateLinkWithoutValue() {
        view.endEditing(true)
        showLoad(true)
        viewModel.generatedLinkWithoutValue()
    }
    
    func viewTapped() {
        view.endEditing(true)
    }
}

// MARK: View Model Outputs
extension ReceiveInputValueViewController: ReceiveInputValueDisplay {
    func changeEnableValueConfirmButton(enable: Bool) {
        confirmButton.isEnabled = enable
    }
    
    func showActivityShare(link: String, value: Double?) {
        let activityController = UIActivityViewController(activityItems: [link], applicationActivities: nil)
        activityController.completionWithItemsHandler = { [weak self] _, _, _, error in
            self?.viewModel.handleActivityResult(error: error)
        }
        
        showLoad(false)
        dependencies.analytics.log(ReceiveAnalytics.sendLinkAction(value: value))
        activityController.popoverPresentationController?.sourceView = view
        present(activityController, animated: true)
    }
    
    func showError() {
        let errorViewController = ErrorViewController()
        errorViewController.delegate = self
        present(errorViewController, animated: true, completion: nil)
    }
}

extension ReceiveInputValueViewController: CurrencyFieldDelegate {
    func onValueChange(value: Double) {
        viewModel.checkEnableConfirmButton(value: value)
    }
}

extension ReceiveInputValueViewController: ErrorViewDelegate {
    func didTouchConfirmButton() {
        dismiss(animated: true, completion: nil)
    }
}
