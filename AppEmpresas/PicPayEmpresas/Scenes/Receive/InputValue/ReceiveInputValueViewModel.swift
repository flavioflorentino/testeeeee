import Foundation

protocol ReceiveInputValueViewModelInputs: AnyObject {
    func checkEnableConfirmButton(value: Double)
    func generatedLink(value: Double?)
    func generatedLinkWithoutValue()
    func handleActivityResult(error: Error?)
}

final class ReceiveInputValueViewModel {
    private let service: ReceiveInputValueServicing
    private let presenter: ReceiveInputValuePresenting
    private let percent: Double = 100

    init(service: ReceiveInputValueServicing, presenter: ReceiveInputValuePresenting) {
        self.service = service
        self.presenter = presenter
    }
}

extension ReceiveInputValueViewModel: ReceiveInputValueViewModelInputs {
    func checkEnableConfirmButton(value: Double) {
        guard value > 0 else {
            presenter.changeEnableValueConfirmButton(enable: false)
            return
        }
        
        presenter.changeEnableValueConfirmButton(enable: true)
    }
    
    func generatedLinkWithoutValue() {
        generatedLink()
    }
    
    func generatedLink(value: Double? = nil) {
        var intValue: Int?
        
        if let value = value {
            intValue = Int(value * percent)
        }
        
        service.getLink(value: intValue) { [weak self] result in
            switch result {
            case .success(let model):
                self?.presenter.showShareActivity(link: model.url, value: value)
            case .failure:
                self?.presenter.handleError()
            }
        }
    }
    
    func handleActivityResult(error: Error?) {
        guard error == nil else {
            presenter.handleError()
            return
        }
        
        presenter.didNextStep(action: .goBack)
    }
}
