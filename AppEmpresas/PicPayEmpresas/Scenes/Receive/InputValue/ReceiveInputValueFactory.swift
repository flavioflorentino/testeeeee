import Foundation

enum ReceiveInputValueFactory {
    static func make() -> ReceiveInputValueViewController {
        let service: ReceiveInputValueServicing = ReceiveInputValueService(dependencies: DependencyContainer())
        let coordinator: ReceiveInputValueCoordinating = ReceiveInputValueCoordinator()
        let presenter: ReceiveInputValuePresenting = ReceiveInputValuePresenter(coordinator: coordinator)
        let viewModel = ReceiveInputValueViewModel(service: service, presenter: presenter)
        let viewController = ReceiveInputValueViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
