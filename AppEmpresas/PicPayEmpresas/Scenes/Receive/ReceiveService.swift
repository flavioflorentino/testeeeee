import Core
import Foundation

protocol ReceiveServicing {
    func loadQrCodeImage(_ completion: @escaping (Result<SellerQRCode?, ApiError>) -> Void)
}

final class ReceiveService: ReceiveServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func loadQrCodeImage(_ completion: @escaping (Result<SellerQRCode?, ApiError>) -> Void) {
        BizApi<SellerQRCode>(endpoint: ReceiveServiceEndpoint.qrImage).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result
                    .map { $0.model.data }
                
                completion(mappedResult)
            }
        }
    }
}
