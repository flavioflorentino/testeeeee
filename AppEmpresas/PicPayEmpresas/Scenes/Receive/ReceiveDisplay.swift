import UIKit

protocol ReceiveDisplay: AnyObject {
    func updateQrImage(image: UIImage)
    func showQrError()
}
