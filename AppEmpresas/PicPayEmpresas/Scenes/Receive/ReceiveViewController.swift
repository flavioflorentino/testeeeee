import SnapKit
import UI
import UIKit

extension ReceiveViewController.Layout {
    enum Size {
        static let image = CGSize(width: 206, height: 206)
        static let buttonHeight: CGFloat = 88
    }
}

final class ReceiveViewController: ViewController<ReceiveViewModelInputs, UIView> {
    private typealias Localizable = Strings.Receive
    fileprivate enum Layout {}
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = CornerRadius.light
        return imageView
    }()
    
    private lazy var imageLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.qrImageLabel
        label.numberOfLines = 2
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.grayscale950.lightColor
        label.textAlignment = .center
        return label
    }()
    
    private lazy var button: ReceiveButton = {
        let configuration = ReceiveButtonConfiguration(
            image: Assets.iconShareLink.image,
            title: Localizable.titleShareLinkButton,
            information: Localizable.descriptionShareLinkButton
        )
        
        let button = ReceiveButton(configuration: configuration)
        button.addTarget(self, action: #selector(generateLinkAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var loadImageView: ImageLoadView = {
        let view = ImageLoadView()
        view.layer.masksToBounds = true
        view.layer.cornerRadius = CornerRadius.light
        return view
    }()
    
    private lazy var errorQRView: ReceiveQRErrorView = {
        let view = ReceiveQRErrorView()
        view.delegate = self
        view.layer.masksToBounds = true
        view.layer.cornerRadius = CornerRadius.light
        return view
    }()
    
    private lazy var popUpError = FacialBiometricsErrorView()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        viewModel.loadQrCodeImage()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(loadImageView)
        view.addSubview(imageView)
        view.addSubview(imageLabel)
        view.addSubview(button)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.grayscale100.lightColor
    }
    
    override func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base09)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.image)
        }
        
        imageLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        button.snp.makeConstraints {
            $0.top.equalTo(imageLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Size.buttonHeight)
        }
        
        loadImageView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base09)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.image)
        }
    }
    
    @objc
    private func generateLinkAction() {
        viewModel.linkAction()
    }
}

// MARK: View Model Outputs
extension ReceiveViewController: ReceiveDisplay {
    func updateQrImage(image: UIImage) {
        loadImageView.isHidden = true
        imageView.image = image
    }
    
    func showQrError() {
        loadImageView.isHidden = true
        
        let topConstraint = popUpError.topAnchor.constraint(
            equalTo: view.compatibleSafeAreaLayoutGuide.topAnchor,
            constant: -Spacing.base10
        )
        
        popUpError.setup(with: Localizable.popUpErrorTitle) { [weak self] _ in
            guard let self = self else {
                return
            }
            
            topConstraint.constant = -Spacing.base10
            UIView.animate(
                withDuration: 0.3,
                animations: self.popUpErrorViewAnimated,
                completion: self.finishPopUpErrorViewAnimated(_:)
            )
        }
        
        view.addSubview(popUpError)
        view.addSubview(errorQRView)
        
        errorQRView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base09)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.image)
        }
        
        popUpError.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        topConstraint.isActive = true
        view.layoutIfNeeded()
        
        topConstraint.constant = Spacing.base02
        UIView.animate(withDuration: 0.3) {
            self.popUpErrorViewAnimated()
        }
    }
    
    private func popUpErrorViewAnimated() {
        view.layoutIfNeeded()
    }
    
    private func finishPopUpErrorViewAnimated(_ finish: Bool) {
        popUpError.removeFromSuperview()
    }
}

extension ReceiveViewController: RefreshQRErrorDelegate {
    func refreshAction() {
        loadImageView.isHidden = false
        errorQRView.removeFromSuperview()
        viewModel.loadQrCodeImage()
    }
}
