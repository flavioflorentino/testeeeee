struct SellerQRCode: Decodable {
    private enum CodingKeys: String, CodingKey {
        case key
        case expire = "expire_key"
        case paymentLink
    }
    
    let key: String
    let expire: Int
    let paymentLink: String
}
