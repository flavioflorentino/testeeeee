import UIKit

enum ReceiveAction {
    case valueScreen
}

protocol ReceiveCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ReceiveAction)
}

final class ReceiveCoordinator: ReceiveCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: ReceiveAction) {
        if case .valueScreen = action {
            let controller = ReceiveInputValueFactory.make()
            viewController?.pushViewController(controller)
        }
    }
}
