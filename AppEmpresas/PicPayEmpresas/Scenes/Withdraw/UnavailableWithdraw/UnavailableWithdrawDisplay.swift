import UIKit

protocol UnavailableWithdrawDisplay: AnyObject {
    func withdrawItem(_ item: WithdrawItem)
}
