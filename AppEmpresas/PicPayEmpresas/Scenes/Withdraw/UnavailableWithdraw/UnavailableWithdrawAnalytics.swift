import AnalyticsModule

enum UnavailableWithdrawAnalytics: AnalyticsKeyProtocol {
    case warningLimit(sellerId: Int)
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .warningLimit(let sellerId):
            let props = ["user_id": sellerId]
            return AnalyticsEvent("withdraw_limit_warning", properties: props, providers: [.mixPanel])
        }
    }
}
