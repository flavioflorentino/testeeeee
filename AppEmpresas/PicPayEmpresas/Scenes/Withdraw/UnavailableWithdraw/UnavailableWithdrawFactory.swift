import Foundation

enum UnavailableWithdrawFactory {
    static func make(
        _ withdrawItem: UnavailableWithdrawItem,
        delegate: UnavailableWithdrawDelegate? = nil
    ) -> UnavailableWithdrawViewController {
        let container = DependencyContainer()
        let coordinator: UnavailableWithdrawCoordinating = UnavailableWithdrawCoordinator()
        let presenter: UnavailableWithdrawPresenting = UnavailableWithdrawPresenter(coordinator: coordinator)
        let viewModel = UnavailableWithdrawViewModel(presenter: presenter, withdrawItem: withdrawItem, dependencies: container)
        viewModel.delegate = delegate
        let viewController = UnavailableWithdrawViewController(viewModel: viewModel)
        viewController.modalPresentationStyle = .overCurrentContext
        viewController.modalTransitionStyle = .crossDissolve

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
