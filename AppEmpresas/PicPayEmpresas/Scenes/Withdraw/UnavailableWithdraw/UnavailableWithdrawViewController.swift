import SnapKit
import UI
import UIKit

extension UnavailableWithdrawViewController.Layout {
    enum LineView {
        static let top: CGFloat = 16
        static let height: CGFloat = 1
    }
    
    enum ActionButton {
        static let height: CGFloat = 48
        static let cornerRadius: CGFloat = 24
    }
    
    enum Fonts {
        static let title = UIFont.systemFont(ofSize: 18, weight: .bold)
        static let label = UIFont.systemFont(ofSize: 14, weight: .light)
        static let value = UIFont.systemFont(ofSize: 14, weight: .bold)
        static let button = UIFont.systemFont(ofSize: 16, weight: .regular)
    }
}

final class UnavailableWithdrawViewController: ViewController<UnavailableWithdrawViewModelInputs, UIView> {
    fileprivate struct Layout { }
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.white.color
        view.layer.cornerRadius = CornerRadius.strong
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.title
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = Strings.UnavailableWithdraw.unavailableWithdrawTitle
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.label
        label.text = Strings.UnavailableWithdraw.unavailableWithdrawDescription
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale300.color
        return view
    }()
    
    private lazy var withdrawLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.label
        label.text = Strings.UnavailableWithdraw.unavailableWithdrawAmount
        label.textAlignment = .left
        return label
    }()
    
    private lazy var withdrawValueLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.value
        label.textAlignment = .right
        return label
    }()
    
    private lazy var withdrawStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = Spacing.base00
        stack.addArrangedSubview(withdrawLabel)
        stack.addArrangedSubview(withdrawValueLabel)
        return stack
    }()
    
    private lazy var accountLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.label
        label.text = Strings.UnavailableWithdraw.unavailableWithdrawAccount
        label.textAlignment = .left
        return label
    }()
    
    private lazy var accountValueLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.value
        label.textAlignment = .right
        return label
    }()
    
    private lazy var accountStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = Spacing.base00
        stack.addArrangedSubview(accountLabel)
        stack.addArrangedSubview(accountValueLabel)
        return stack
    }()
    
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.label
        label.text = Strings.UnavailableWithdraw.unavailableWithdrawDate
        label.textAlignment = .left
        return label
    }()
    
    private lazy var dateValueLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.value
        label.textAlignment = .right
        return label
    }()
    
    private lazy var dateStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = Spacing.base00
        stack.addArrangedSubview(dateLabel)
        stack.addArrangedSubview(dateValueLabel)
        return stack
    }()
    
    private lazy var verticalStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base01
        stack.addArrangedSubview(withdrawStack)
        stack.addArrangedSubview(accountStack)
        stack.addArrangedSubview(dateStack)
        return stack
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.Default.confirm, for: .normal)
        button.titleLabel?.font = Layout.Fonts.button
        button.setTitleColor(Palette.white.color, for: .normal)
        button.backgroundColor = Palette.ppColorBranding300.color
        button.layer.cornerRadius = Layout.ActionButton.cornerRadius
        button.setTitle(Strings.UnavailableWithdraw.unavailableWithdrawButtonTitle, for: .normal)
        button.addTarget(self, action: #selector(didTapActionButton), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadData()
    }

    override func buildViewHierarchy() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(lineView)
        contentView.addSubview(verticalStack)
        contentView.addSubview(actionButton)
        view.addSubview(contentView)
    }
    
    override func configureViews() {
        contentView.snp.makeConstraints {
            $0.leading.equalTo(view).offset(Spacing.base03)
            $0.trailing.equalTo(view).offset(-Spacing.base03)
            $0.centerY.equalTo(view)
            $0.top.greaterThanOrEqualTo(view).offset(Spacing.base08)
            $0.bottom.lessThanOrEqualTo(view).offset(-Spacing.base08)
        }
        
        titleLabel.snp.makeConstraints {
            $0.leading.equalTo(contentView).offset(Spacing.base03)
            $0.trailing.equalTo(contentView).offset(-Spacing.base03)
            $0.top.equalTo(contentView).offset(Spacing.base04)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.leading.equalTo(contentView).offset(Spacing.base03)
            $0.trailing.equalTo(contentView).offset(-Spacing.base03)
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
        }
        
        lineView.snp.makeConstraints {
            $0.leading.trailing.equalTo(contentView)
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.height.equalTo(Layout.LineView.height)
        }
        
        verticalStack.snp.makeConstraints {
            $0.leading.equalTo(contentView).offset(Spacing.base03)
            $0.trailing.equalTo(contentView).offset(-Spacing.base03)
            $0.top.equalTo(lineView.snp.bottom).offset(Spacing.base03)
        }
        
        actionButton.snp.makeConstraints {
            $0.leading.equalTo(contentView).offset(Spacing.base03)
            $0.trailing.equalTo(contentView).offset(-Spacing.base03)
            $0.top.equalTo(verticalStack.snp.bottom).offset(Spacing.base03)
            $0.bottom.equalTo(contentView).offset(-Spacing.base04)
            $0.height.equalTo(Layout.ActionButton.height)
        }
    }
    
    override func setupConstraints() {
        view.backgroundColor = Palette.black.color.withAlphaComponent(0.2)
    }
}

// MARK: View Model Outputs
extension UnavailableWithdrawViewController: UnavailableWithdrawDisplay {
    func withdrawItem(_ item: WithdrawItem) {
        withdrawValueLabel.text = item.amount
        accountValueLabel.text = item.account
        dateValueLabel.text = item.date
    }
}

// MARK: Objc Target Functions
@objc
extension UnavailableWithdrawViewController {
    func didTapActionButton() {
        viewModel.didTapActionButton()
    }
}
