import UIKit

enum UnavailableWithdrawAction {
    case dismiss
}

protocol UnavailableWithdrawCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: UnavailableWithdrawAction)
}

final class UnavailableWithdrawCoordinator: UnavailableWithdrawCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: UnavailableWithdrawAction) {
        if action == .dismiss {
            viewController?.dismiss(animated: true)
        }
    }
}
