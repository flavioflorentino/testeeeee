import Core
import Foundation

protocol UnavailableWithdrawPresenting: AnyObject {
    var viewController: UnavailableWithdrawDisplay? { get set }
    func didNextStep(action: UnavailableWithdrawAction)
    func withdrawItem(_ item: UnavailableWithdrawItem)
}

struct WithdrawItem {
    let account: String
    let amount: String
    let date: String
}

final class UnavailableWithdrawPresenter: UnavailableWithdrawPresenting {
    private let coordinator: UnavailableWithdrawCoordinating
    weak var viewController: UnavailableWithdrawDisplay?

    init(coordinator: UnavailableWithdrawCoordinating) {
        self.coordinator = coordinator
    }

    func didNextStep(action: UnavailableWithdrawAction) {
        coordinator.perform(action: action)
    }

    func withdrawItem(_ item: UnavailableWithdrawItem) {
        guard
            let accountBank = item.accountBank,
            let createdAt = item.createdAt,
            let amount = item.amount
            else {
                return
        }

        let account = accountBank.agency + " " + accountBank.account
        let date = DateFormatter.custom(value: createdAt.date ?? Date())
        let item = WithdrawItem(account: account, amount: "\(amount)".currency(), date: date)
        viewController?.withdrawItem(item)
    }
}
