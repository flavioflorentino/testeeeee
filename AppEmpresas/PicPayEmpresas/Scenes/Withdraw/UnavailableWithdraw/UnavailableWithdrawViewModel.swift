import AnalyticsModule
import Foundation
import LegacyPJ

protocol UnavailableWithdrawViewModelInputs: AnyObject {
    func loadData()
    func didTapActionButton()
}

protocol UnavailableWithdrawDelegate: AnyObject {
    func viewDidDismiss()
}

struct UnavailableWithdrawItem: Decodable, Equatable {
    enum CodingKeys: String, CodingKey {
        case pending, amount, createdAt = "created_at", accountBank = "account_bank"
    }

    struct AccountBank: Decodable, Equatable {
        let agency: String
        let account: String
    }

    let pending: Bool
    let amount: Double?
    let createdAt: ServerTimestamp?
    let accountBank: AccountBank?
}

final class UnavailableWithdrawViewModel {
    typealias Dependencies = HasAuthManager & HasAnalytics
    private let dependencies: Dependencies
    private let presenter: UnavailableWithdrawPresenting
    private let withdrawItem: UnavailableWithdrawItem

    weak var delegate: UnavailableWithdrawDelegate?

    init(presenter: UnavailableWithdrawPresenting, withdrawItem: UnavailableWithdrawItem, dependencies: Dependencies) {
        self.presenter = presenter
        self.withdrawItem = withdrawItem
        self.dependencies = dependencies
    }
}

extension UnavailableWithdrawViewModel: UnavailableWithdrawViewModelInputs {
    func loadData() {
        if let id = dependencies.authManager.user?.id {
            dependencies.analytics.log(UnavailableWithdrawAnalytics.warningLimit(sellerId: id))
        }
        presenter.withdrawItem(withdrawItem)
    }

    func didTapActionButton() {
        presenter.didNextStep(action: .dismiss)
        delegate?.viewDidDismiss()
    }
}
