import Core
import Foundation

protocol AutomaticWithdrawalServicing {
    func getAccounts(_ completion: @escaping (Result<[WithdrawAccount], ApiError>) -> Void)
    func getWithdrawalStatus(_ completion: @escaping(Result<Bool, ApiError>) -> Void)
    func updateAutomaticWithdrawal(
        _ pin: String, 
        accountID: Int, 
        completion: @escaping(Result<NoContent, ApiError>) -> Void
    )
}

final class AutomaticWithdrawalService: AutomaticWithdrawalServicing {
    typealias Dependencies = HasMainQueue & HasApiSeller
    private let dependencies: Dependencies
    
    // MARK: - Inits
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getAccounts(_ completion: @escaping (Result<[WithdrawAccount], ApiError>) -> Void) {
        BizApi<[WithdrawAccount]>(endpoint: AutomaticWithdrawalEndpoint.getMainAccount).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                BizApiHelper.unwrapBizResult(result: result, completion: completion)
            }
        }
    }
    
    func getWithdrawalStatus(_ completion: @escaping(Result<Bool, ApiError>) -> Void) {
        dependencies.apiSeller.data { result in
            switch result {
            case .success(let seller):
                completion(.success(seller.automaticWithdrawal))
            case .failure(let error):
                let reqError = RequestError(picPayError: error)
                completion(.failure(.badRequest(body: reqError)))
            }
        }
    }
    
    func updateAutomaticWithdrawal(
        _ pin: String, 
        accountID: Int, 
        completion: @escaping(Result<NoContent, ApiError>) -> Void
    ) {
        let endpoint = AutomaticWithdrawalEndpoint.updateStatus(pin, accountID: accountID)
        BizApi<NoContent>(endpoint: endpoint).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                BizApiHelper.unwrapBizResult(result: result, completion: completion)
            }
        }
    }
}
