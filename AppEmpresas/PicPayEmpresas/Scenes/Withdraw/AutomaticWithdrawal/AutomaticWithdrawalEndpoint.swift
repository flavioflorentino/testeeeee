import Core

enum AutomaticWithdrawalEndpoint {
    case updateStatus(_ pin: String, accountID: Int)
    case status
    case getMainAccount
}

extension AutomaticWithdrawalEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .updateStatus:
            return "/withdrawals/auto"
        case .status:
            return "/seller"
        case .getMainAccount:
            return "/withdrawals/bank-account"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getMainAccount, .status:
            return .get
        case .updateStatus:
            return .post
        }
    }
    
    var body: Data? {
        switch self {
        case .getMainAccount, .status:
            return nil
        case let .updateStatus(_, accountID):
            return [
                "bankAccountId": accountID
            ].toData()
        }
    }
    
    var customHeaders: [String: String] {
        switch self {
        case let .updateStatus(pin, _):
            return headerWith(pin: pin)
        default:
            return [:]
        }
    }
}
