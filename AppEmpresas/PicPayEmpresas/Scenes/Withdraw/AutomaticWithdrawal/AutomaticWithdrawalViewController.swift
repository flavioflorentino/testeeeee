import SnapKit
import UI
import UIKit

extension AutomaticWithdrawalViewController.Layout {
    enum SwitchView {
        static let shadowRadius: CGFloat = 4
        static let shadowOpacity: Float = 0.1
        static let shadowOffset = CGSize(width: 0, height: 3)
    }
    
    enum SwitchViewStack {
        static let top: CGFloat = 16
        static let leading: CGFloat = 16
        static let trailing: CGFloat = 16
        static let bottom: CGFloat = 16
    }
    
    enum DetailsLabel {
        static let top: CGFloat = 16
        static let leading: CGFloat = 32
        static let trailing: CGFloat = 32
    }
    
    enum Fonts {
        static let small = UIFont.systemFont(ofSize: 12, weight: .regular)
        static let regular = UIFont.systemFont(ofSize: 16, weight: .regular)
    }
}

final class AutomaticWithdrawalViewController: ViewController<AutomaticWithdrawalViewModelInputs, UIView> {
    // MARK: - Private Properties
    
    fileprivate enum Layout { }
    
    private lazy var bankAccountView = BankAccountView()
    
    private lazy var switchView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.white.color
        view.layer.shadowColor = Palette.ppColorGrayscale600.cgColor
        view.layer.shadowOpacity = Layout.SwitchView.shadowOpacity
        view.layer.shadowOffset = Layout.SwitchView.shadowOffset
        view.layer.shadowRadius = Layout.SwitchView.shadowRadius
        return view
    }()
    
    private lazy var switchViewStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.addArrangedSubview(withdrawSwitchLabel)
        stack.addArrangedSubview(withdrawSwitch)
        return stack
    }()
    
    private lazy var withdrawSwitchLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.regular
        label.text = Strings.AutomaticWithdrawal.enableWithdrawal
        return label
    }()
    
    private lazy var withdrawSwitch: UISwitch = {
        let withdrawSwitch = UISwitch()
        withdrawSwitch.addTarget(self, action: #selector(switchDidChangeValue), for: .valueChanged)
        return withdrawSwitch
    }()
    
    private lazy var detailsLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.font = Layout.Fonts.small
        label.textColor = Palette.ppColorGrayscale500.color
        label.text = Strings.AutomaticWithdrawal.withdrawalDetails
        return label
    }()
    
    // MARK: - Public Properties
    
    lazy var loadingView: LoadingView = {
        let loadingView = LoadingView()
        loadingView.alpha = 0.8
        return loadingView
    }()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadData()
    }
    
    // MARK: - Public Methods
    
    override func buildViewHierarchy() {
        switchView.addSubview(switchViewStack)
        view.addSubview(bankAccountView)
        view.addSubview(switchView)
        view.addSubview(detailsLabel)
    }
    
    override func configureViews() {
        title = Strings.AutomaticWithdrawal.title
        view.backgroundColor = Palette.ppColorGrayscale100.color
    }
    
    override func setupConstraints() {
        bankAccountView.snp.makeConstraints {
            $0.top.leading.trailing.equalTo(view)
        }
        
        switchView.snp.makeConstraints {
            $0.top.equalTo(bankAccountView.snp.bottom)
            $0.leading.trailing.equalTo(view)
        }
        
        switchViewStack.snp.makeConstraints {
            $0.top.equalTo(switchView).offset(Layout.SwitchViewStack.top)
            $0.leading.equalTo(switchView).offset(Layout.SwitchViewStack.leading)
            $0.trailing.equalTo(switchView).offset(-Layout.SwitchViewStack.trailing)
            $0.bottom.equalTo(switchView).offset(-Layout.SwitchViewStack.bottom)
        }
        
        detailsLabel.snp.makeConstraints {
            $0.top.equalTo(switchView.snp.bottom).offset(Layout.DetailsLabel.top)
            $0.leading.equalTo(view).offset(Layout.DetailsLabel.leading)
            $0.trailing.equalTo(view).offset(-Layout.DetailsLabel.trailing)
        }
    }
    
    // MARK: - Actions
    @objc
    private func switchDidChangeValue(_ sender: UISwitch) {
        viewModel.updateAutomaticWithdrawal(newStatus: sender.isOn)
    }
}

// MARK: View Model Outputs

extension AutomaticWithdrawalViewController: AutomaticWithdrawalDisplay {
    func showLoading() {
        startLoadingView()
    }
    
    func hideLoading() {
        stopLoadingView()
    }
    
    func showMainAccount(_ accountItem: WithdrawAccountItem) {
        bankAccountView.setupAccount(accountItem, hideChangeButton: true)
    }
    
    func reverseSwitch() {
        withdrawSwitch.isOn.toggle()
    }
    
    func showError(error: Error) {
        let errorAlert = UIAlertController(error: error)
        errorAlert.show()
    }
    
    func setSwitch(to isOn: Bool) {
        withdrawSwitch.isOn = isOn
    }
}

// MARK: - LoadingViewProtocol

extension AutomaticWithdrawalViewController: LoadingViewProtocol { }
