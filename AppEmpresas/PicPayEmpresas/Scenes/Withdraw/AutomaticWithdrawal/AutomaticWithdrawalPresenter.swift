import Foundation

protocol AutomaticWithdrawalPresenting: AnyObject {
    var viewController: AutomaticWithdrawalDisplay? { get set }
    
    func setLoading(isLoading: Bool)
    func setAutomaticWithdrawalStatus(_ value: Bool)
    func showMainAccount(account: WithdrawAccount)
    func reverseSwitch()
    func showError(error: Error)
    func showErrorView()
}

final class AutomaticWithdrawalPresenter: AutomaticWithdrawalPresenting {
    private let coordinator: AutomaticWithdrawalCoordinating
    weak var viewController: AutomaticWithdrawalDisplay?

    init(coordinator: AutomaticWithdrawalCoordinating) {
        self.coordinator = coordinator
    }
    
    func setLoading(isLoading: Bool) {
        isLoading ? viewController?.showLoading() : viewController?.hideLoading()
    }
    
    func setAutomaticWithdrawalStatus(_ value: Bool) {
        viewController?.setSwitch(to: value)
    }
    
    func showMainAccount(account: WithdrawAccount) {
        let finalAccount = "\(account.account)-\(account.accountDigit)"
               
        let accountItem = WithdrawAccountItem(
           bankName: account.bank.name,
           imageUrl: account.bank.imageUrl,
           branch: account.branch,
           account: finalAccount
        )
        
        viewController?.showMainAccount(accountItem)
    }
    
    func reverseSwitch() {
        viewController?.reverseSwitch()
    }
    
    func showError(error: Error) {
        viewController?.showError(error: error)
    }
    
    func showErrorView() {
        coordinator.perform(action: .errorView)
    }
}
