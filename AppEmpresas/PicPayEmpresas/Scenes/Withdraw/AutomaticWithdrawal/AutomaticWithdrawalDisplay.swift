import Foundation

protocol AutomaticWithdrawalDisplay: AnyObject {
    func showLoading()
    func hideLoading()
    func showMainAccount(_ accountItem: WithdrawAccountItem)
    func reverseSwitch()
    func setSwitch(to isOn: Bool)
    func showError(error: Error)
}
