import AnalyticsModule
import Core
import Foundation
import LegacyPJ

protocol AutomaticWithdrawalViewModelDelegate: AnyObject {
    func successOnUpdateAutomaticWithdrawalStatus(status: Bool)
}

protocol AutomaticWithdrawalViewModelInputs: AnyObject {
    func loadData()
    func updateAutomaticWithdrawal(newStatus: Bool)
}

final class AutomaticWithdrawalViewModel {
    typealias Dependencies = HasAuthManager & HasKVStore & HasAnalytics
    
    // MARK: - Internal Properties
    
    weak var delegate: AutomaticWithdrawalViewModelDelegate?
    
    // MARK: - Private Properties
    
    private let service: AutomaticWithdrawalServicing
    private let presenter: AutomaticWithdrawalPresenting
    private let dependencies: Dependencies
    private var mainAccount: WithdrawAccount?
    
    // swiftlint:disable discouraged_optional_boolean
    private let isAutomaticWithdrawalEnabled: Bool?
    
    // MARK: - Inits
    
    init(
        service: AutomaticWithdrawalServicing,
        presenter: AutomaticWithdrawalPresenting,
        dependencies: Dependencies,
        isAutomaticWithdrawalEnabled status: Bool? = nil
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.isAutomaticWithdrawalEnabled = status
    }
    // swiftlint:enable discouraged_optional_boolean
}

// MARK: - AutomaticWithdrawalViewModelInputs

extension AutomaticWithdrawalViewModel: AutomaticWithdrawalViewModelInputs {
    func loadData() {
        presenter.setLoading(isLoading: true)
        
        guard let status = isAutomaticWithdrawalEnabled else {
            getWithdrawalStatus()
            return
        }
        presenter.setAutomaticWithdrawalStatus(status)
        getMainAccount()
    }
    
    func updateAutomaticWithdrawal(newStatus: Bool) {
        guard let mainAccount = mainAccount else {
            // Main account should never be nil on this step.
            return
        }
        
        dependencies.authManager.performActionWithAuthorization { [weak self] result in
            switch result {
            case .success(let pin):
                self?.requestUpdateAutomaticWithdrawal(withPin: pin, mainAccount: mainAccount, newStatus: newStatus)
                
            case .failure(let error):
                self?.presenter.reverseSwitch()
                self?.presenter.showError(error: error)
                
            case .canceled:
                self?.presenter.reverseSwitch()
            }
        }
    }
}

// MARK: - Private Methods

private extension AutomaticWithdrawalViewModel {
    private func requestUpdateAutomaticWithdrawal(withPin pin: String, mainAccount: WithdrawAccount, newStatus: Bool) {
        presenter.setLoading(isLoading: true)
        
        service.updateAutomaticWithdrawal(pin, accountID: mainAccount.id) { [weak self] result in
            self?.presenter.setLoading(isLoading: false)
            
            switch result {
            case .success:
                self?.delegate?.successOnUpdateAutomaticWithdrawalStatus(status: newStatus)
                self?.logAutomaticWithdrawal(enabled: newStatus)
            case .failure(.badRequest(let error)):
                self?.presenter.reverseSwitch()
                self?.presenter.showError(error: error)
            default:
                self?.presenter.reverseSwitch()
                self?.presenter.showError(error: LegacyPJError.requestError)
            }
        }
    }
    
    private func logAutomaticWithdrawal(enabled: Bool) {
        guard let userID = dependencies.authManager.user?.id else {
            return
        }
        
        if enabled {
            dependencies.analytics.log(AutomaticWithdrawalAnalytics.enabled(userID: userID))
        } else {
            dependencies.analytics.log(AutomaticWithdrawalAnalytics.disabled(userID: userID))
        }
    }
    
    private func getWithdrawalStatus() {
        service.getWithdrawalStatus { [weak self] result in
            switch result {
            case .success(let status):
                self?.presenter.setAutomaticWithdrawalStatus(status)
                self?.getMainAccount()
            case .failure(.badRequest(let error)):
                self?.presenter.showError(error: error)
            default:
                self?.presenter.showError(error: LegacyPJError.requestError)
            }
        }
    }
    
    private func getMainAccount() {
        service.getAccounts { [weak self] result in
            self?.presenter.setLoading(isLoading: false)
            
            switch result {
            case .success(let accounts):
                guard let mainAccount = accounts.first(where: { $0.isPrimary }) else {
                    self?.presenter.showErrorView()
                    return
                }
                
                self?.mainAccount = mainAccount
                self?.presenter.showMainAccount(account: mainAccount)
                
            case .failure(.badRequest):
                self?.presenter.showErrorView()
                
            case .failure:
                self?.presenter.showErrorView()
            }
        }
    }
}
