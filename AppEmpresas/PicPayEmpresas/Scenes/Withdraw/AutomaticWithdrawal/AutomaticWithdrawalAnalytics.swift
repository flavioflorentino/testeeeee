import AnalyticsModule

enum AutomaticWithdrawalAnalytics: AnalyticsKeyProtocol {
    case enabled(userID: Int)
    case disabled(userID: Int)
    
    func event() -> AnalyticsEventProtocol {
        let name: String
        let userID: Int
        
        switch self {
        case .enabled(let keyUserID):
            name = "automatic_withdraw_enabled"
            userID = keyUserID
            
        case .disabled(let keyUserID):
            name = "automatic_withdraw_disabled"
            userID = keyUserID
        }
        
        return getEventsFor(name: name, userID: userID)
    }
}

private extension AutomaticWithdrawalAnalytics {
    func getEventsFor(name: String, userID: Int) -> AnalyticsEvent {
        let properties: [String: Any] = [
            "user_id": userID
        ]
        return AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
    }
}
