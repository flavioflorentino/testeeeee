import Foundation

// swiftlint:disable discouraged_optional_boolean
enum AutomaticWithdrawalFactory {
    static func make(
        initialEnabled: Bool? = nil,
        viewModelDelegate: AutomaticWithdrawalViewModelDelegate? = nil
    ) -> AutomaticWithdrawalViewController {
        let container = DependencyContainer()
        let service: AutomaticWithdrawalServicing = AutomaticWithdrawalService(dependencies: container)
        let coordinator: AutomaticWithdrawalCoordinating = AutomaticWithdrawalCoordinator()
        let presenter: AutomaticWithdrawalPresenting = AutomaticWithdrawalPresenter(coordinator: coordinator)
        let viewModel = AutomaticWithdrawalViewModel(
            service: service,
            presenter: presenter,
            dependencies: container,
            isAutomaticWithdrawalEnabled: initialEnabled
        )
        viewModel.delegate = viewModelDelegate
        
        let viewController = AutomaticWithdrawalViewController(viewModel: viewModel)
        viewController.hidesBottomBarWhenPushed = true

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
// swiftlint:enable discouraged_optional_boolean
