import UIKit

enum AutomaticWithdrawalAction {
    case errorView
}

protocol AutomaticWithdrawalCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: AutomaticWithdrawalAction)
}

final class AutomaticWithdrawalCoordinator: AutomaticWithdrawalCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: AutomaticWithdrawalAction) {
        switch action {
        case .errorView:
            let errorViewController = ErrorViewController()
            errorViewController.delegate = self
            viewController?.navigationController?.pushViewController(errorViewController, animated: true)
        }
    }
}

// MARK: - ErrorViewDelegate

extension AutomaticWithdrawalCoordinator: ErrorViewDelegate {
    func didTouchConfirmButton() {
        viewController?.navigationController?.popViewController(animated: true)
    }
}
