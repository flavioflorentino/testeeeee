import AnalyticsModule
import Foundation
import FeatureFlag
import LegacyPJ

protocol WithdrawSuccessViewModelInputs: AnyObject {
    func loadContent()
    func actionButtonTapped()
    func autoWithdrawButtonTapped()
}

final class WithdrawSuccessViewModel {
    typealias Dependencies = HasAuthManager & HasFeatureManager & HasAnalytics
    private let dependencies: Dependencies

    private let presenter: WithdrawSuccessPresenting
    private let showAutomaticWithdraw: Bool
    private let type: WithdrawType
    
    var customMessage: String?

    init(presenter: WithdrawSuccessPresenting,
         dependencies: Dependencies,
         type: WithdrawType,
         showAutoWithdraw: Bool = true) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.showAutomaticWithdraw = showAutoWithdraw
        self.type = type
    }
}

private extension WithdrawSuccessViewModel {
    func logWithdrawal() {
        guard let id = dependencies.authManager.user?.id else {
            return
        }
        
        let analyticsType: WithdrawalOptionsAnalyticsType
        
        switch type {
        case .bankAccount:
            analyticsType = .bankAccount
        case .picpay:
            analyticsType = .walletPf
        }
        
        let event = WithdrawSuccessAnalytics.automaticWithdrawIntention(sellerId: id, type: analyticsType)
        dependencies.analytics.log(event)
    }
}

extension WithdrawSuccessViewModel: WithdrawSuccessViewModelInputs {
    func loadContent() {
        if let message = customMessage {
            presenter.presentCustomMessage(message)
        }
        
        if showAutomaticWithdraw {
            presenter.showAutomaticWithdrawal()
        }
    }
    
    func actionButtonTapped() {
        presenter.didNextStep(action: .dismiss)
    }
    
    func autoWithdrawButtonTapped() {
        logWithdrawal()
        presenter.didNextStep(action: .automaticWithdraw)
    }
}
