import Foundation
import UIKit

enum WithdrawSuccessFactory {
    static func make(
        type: WithdrawType,
        showAutoWithdraw: Bool = true,
        message: String? = nil,
        navigationController: UINavigationController? = nil
    ) -> WithdrawSuccessViewController {
        let container = DependencyContainer()
        let coordinator: WithdrawSuccessCoordinating = WithdrawSuccessCoordinator()
        let presenter: WithdrawSuccessPresenting = WithdrawSuccessPresenter(coordinator: coordinator)
        let viewModel = WithdrawSuccessViewModel(presenter: presenter,
                                                 dependencies: container,
                                                 type: type,
                                                 showAutoWithdraw: showAutoWithdraw)
        viewModel.customMessage = message
        let viewController = WithdrawSuccessViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.navigationController = navigationController
        presenter.viewController = viewController

        return viewController
    }
}
