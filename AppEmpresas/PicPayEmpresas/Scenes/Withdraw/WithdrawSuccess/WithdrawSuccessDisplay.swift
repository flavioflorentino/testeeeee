import UIKit

protocol WithdrawSuccessDisplay: AnyObject {
    func customSuccessMessage(_ message: String)
    func showAutomaticWithdrawalElements()
}
