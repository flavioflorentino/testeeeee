import Core
import Foundation

protocol WithdrawSuccessPresenting: AnyObject {
    var viewController: WithdrawSuccessDisplay? { get set }
    func presentCustomMessage(_ message: String)
    func didNextStep(action: WithdrawSuccessAction)
    func showAutomaticWithdrawal()
}

final class WithdrawSuccessPresenter: WithdrawSuccessPresenting {
    private let coordinator: WithdrawSuccessCoordinating
    weak var viewController: WithdrawSuccessDisplay?

    init(coordinator: WithdrawSuccessCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentCustomMessage(_ message: String) {
        viewController?.customSuccessMessage(message)
    }
    
    func didNextStep(action: WithdrawSuccessAction) {
        coordinator.perform(action: action)
    }
    
    func showAutomaticWithdrawal() {
        viewController?.showAutomaticWithdrawalElements()
    }
}
