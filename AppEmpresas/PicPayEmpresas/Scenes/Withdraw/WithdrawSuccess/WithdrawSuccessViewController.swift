import UI
import UIKit

extension WithdrawSuccessViewController.Layout {
    enum ImageView {
        static let size: CGFloat = 88
    }
    
    enum ActionButton {
        static let width: CGFloat = 280
        static let height: CGFloat = 48
        static let cornerRadius: CGFloat = 24
    }
    
    enum Fonts {
        static let title = UIFont.systemFont(ofSize: 32, weight: .bold)
        static let description = UIFont.systemFont(ofSize: 16, weight: .regular)
        static let button = UIFont.systemFont(ofSize: 16, weight: .semibold)
        static let withdraw = UIFont.systemFont(ofSize: 14, weight: .regular)
    }
}

final class WithdrawSuccessViewController: ViewController<WithdrawSuccessViewModelInputs, UIView> {
    fileprivate struct Layout { }

    private lazy var imageView: UIImageView = {
        let image = Assets.iconBigSuccess.image
        return UIImageView(image: image)
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.WithdrawSuccess.viewTitle
        label.font = Layout.Fonts.title
        label.textAlignment = .center
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.WithdrawSuccess.viewDescription
        label.font = Layout.Fonts.description
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var actionButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(Strings.WithdrawSuccess.actionButtonTitle, for: .normal)
        button.titleLabel?.font = Layout.Fonts.button
        button.normalTitleColor = Palette.white.color
        button.normalBackgrounColor = Palette.ppColorBranding300.color
        button.cornerRadius = Layout.ActionButton.cornerRadius
        button.addTarget(self, action: #selector(didtapActionButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var automaticWithdrawLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.WithdrawSuccess.withdrawLabel
        label.font = Layout.Fonts.withdraw
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        return label
    }()
    
    private lazy var automaticWithdrawButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.setTitle(Strings.WithdrawSuccess.withdrawButtonTitle, for: .normal)
        button.updateColor(
            Palette.ppColorGrayscale400.color,
            highlightedColor: Palette.ppColorGrayscale400.color,
            font: Layout.Fonts.withdraw
        )
        button.addTarget(self, action: #selector(didTapAutomaticWithdraw), for: .touchUpInside)
        return button
    }()
    
    private lazy var automaticWithdrawStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.addArrangedSubview(automaticWithdrawLabel)
        stack.addArrangedSubview(automaticWithdrawButton)
        return stack
    }()

     override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadContent()
    }
 
     override func buildViewHierarchy() {
        view.addSubview(imageView)
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(actionButton)
        view.addSubview(automaticWithdrawStack)
    }
    
     override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        automaticWithdrawStack.isHidden = true
    }
    
     override func setupConstraints() {
        descriptionLabel.layout {
            $0.centerX == view.centerXAnchor
            $0.centerY == view.centerYAnchor
            $0.leading == view.leadingAnchor + UI.Spacing.base02
            $0.trailing == view.trailingAnchor - UI.Spacing.base02
        }
        
        titleLabel.layout {
            $0.centerX == view.centerXAnchor
            $0.bottom == descriptionLabel.topAnchor - UI.Spacing.base01
        }
        
        imageView.layout {
            $0.height == Layout.ImageView.size
            $0.width == Layout.ImageView.size
            $0.centerX == view.centerXAnchor
            $0.bottom == titleLabel.topAnchor - UI.Spacing.base02
        }
        
        actionButton.layout {
            $0.height == Layout.ActionButton.height
            $0.width == Layout.ActionButton.width
            $0.centerX == view.centerXAnchor
            $0.top == descriptionLabel.bottomAnchor + UI.Spacing.base03
        }
        
        automaticWithdrawStack.layout {
            $0.centerX == view.centerXAnchor
            $0.top == actionButton.bottomAnchor + UI.Spacing.base08
        }
    }
}

// MARK: Objc Target Functions
@objc
extension WithdrawSuccessViewController {
    func didtapActionButton() {
        viewModel.actionButtonTapped()
    }
    
    func didTapAutomaticWithdraw() {
        viewModel.autoWithdrawButtonTapped()
    }
}

// MARK: View Model Outputs
extension WithdrawSuccessViewController: WithdrawSuccessDisplay {
    func customSuccessMessage(_ message: String) {
        descriptionLabel.text = message
    }
    
    func showAutomaticWithdrawalElements() {
        automaticWithdrawStack.isHidden = false
    }
}
