import UIKit

enum WithdrawSuccessAction {
    case dismiss
    case automaticWithdraw
}

protocol WithdrawSuccessCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var navigationController: UINavigationController? { get set }
    func perform(action: WithdrawSuccessAction)
}

final class WithdrawSuccessCoordinator: WithdrawSuccessCoordinating {
    weak var viewController: UIViewController?
    weak var navigationController: UINavigationController?
    
    func perform(action: WithdrawSuccessAction) {
        switch action {
        case .dismiss:
            viewController?.dismiss(animated: true)
        case .automaticWithdraw:
            viewController?.dismiss(animated: true)
            guard let navigation = navigationController else { return }
            let router = RouterApp.automaticWithdrawal
            router.match(navigation: navigation)
        }
    }
}
