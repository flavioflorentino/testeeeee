import AnalyticsModule

enum WithdrawSuccessAnalytics: AnalyticsKeyProtocol {
    case automaticWithdrawIntention(sellerId: Int, type: WithdrawalOptionsAnalyticsType)

    func event() -> AnalyticsEventProtocol {
        switch self {
        case let .automaticWithdrawIntention(sellerId, type):
            let properties: [String: Any] = [
                "user_id": sellerId,
                "type": type.rawValue
            ]
            return AnalyticsEvent("automatic_withdraw_intention", properties: properties, providers: [.mixPanel])
        }
    }
}
