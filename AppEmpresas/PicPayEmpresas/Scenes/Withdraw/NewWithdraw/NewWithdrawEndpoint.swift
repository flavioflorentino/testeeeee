import Core

enum NewWithdrawEndpoint {
    case getMainAccount
    case withdrawStatus
    case picpayAccountWithdraw(_ pin: String, value: Decimal)
    case bankAccountWithdraw(_ pin: String, accountId: Int, value: Decimal)
}

extension NewWithdrawEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .getMainAccount:
            return "/withdrawals/bank-account"
        case .withdrawStatus:
            return "/withdraw/status"
        case .bankAccountWithdraw:
            return "/withdrawals"
        case .picpayAccountWithdraw:
            return "/withdrawals/b2p"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getMainAccount, .withdrawStatus:
            return .get
        case .bankAccountWithdraw, .picpayAccountWithdraw:
            return .post
        }
    }
    
    var body: Data? {
        switch self {
        case .getMainAccount, .withdrawStatus:
            return nil
        case let .bankAccountWithdraw(_, accountId, value):
            return [
                "bankAccountId": accountId,
                "value": value
            ].toData()
        case let .picpayAccountWithdraw(_, value):
            return [
                "value": value
            ].toData()
        }
    }
    
    var customHeaders: [String: String] {
        switch self {
        case let .bankAccountWithdraw(pin, _, _):
            return headerWith(pin: pin)
        case let .picpayAccountWithdraw(pin, _):
            return headerWith(pin: pin)
        default:
            return [:]
        }
    }
}
