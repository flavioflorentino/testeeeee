import AnalyticsModule
import Foundation
import LegacyPJ

protocol NewWithdrawViewModelInputs: AnyObject {
    func loadView()
    func getAvailableAmount()
    func newAmountValue(_ valueTyped: Double)
    func requestWithdraw(_ amount: Double)
    func logAccountChange()
    func dismiss()
}

final class NewWithdrawViewModel {
    typealias Dependencies = HasAuthManager & HasAnalytics

    private let withdrawType: WithdrawType
    private let service: NewWithdrawServicing
    private let presenter: NewWithdrawPresenting
    private let dependencies: Dependencies

    var balanceItem: WalletBalanceItem?
    var mainAccount: WithdrawAccount?
    var picpayAccount: PicpayAccountModel?

    init(
        _ withdrawType: WithdrawType,
        service: NewWithdrawServicing,
        presenter: NewWithdrawPresenting,
        dependencies: Dependencies
    ) {
        self.withdrawType = withdrawType
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension NewWithdrawViewModel: NewWithdrawViewModelInputs {
    func loadView() {
        getAvailableAmount()

        switch withdrawType {
        case .bankAccount:
            guard let mainAccount = mainAccount else {
                presenter.rollBackFail(LegacyPJError.requestError)
                return
            }
            presenter.showBankAccount()
            checkWithdrawStatus(mainAccount)
        case .picpay:
            guard let picpayAccount = picpayAccount else {
                presenter.rollBackFail(LegacyPJError.requestError)
                return
            }

            presenter.showPicPayAccount()
            presenter.didReceivePicPayAccount(picpayAccount)
        }
    }

    func getAvailableAmount() {
        presenter.availableAmount(balanceItem?.available ?? "")
    }

    func newAmountValue(_ valueTyped: Double) {
        guard let available = balanceItem?.available, valueTyped > 0 else {
            presenter.enableButton(false)
            return
        }

        if let number = available.bindToNumber() {
            presenter.enableButton(number.doubleValue >= valueTyped)
        }
    }

    func requestWithdraw(_ amount: Double) {
        switch withdrawType {
        case .bankAccount:
            bankAccountWithdraw(amount)
        case .picpay:
            picpayAccountWithdraw(amount)
        }
    }

    func dismiss() {
        presenter.didNextStep(action: .dismiss)
    }

    func logAccountChange() {
        dependencies.analytics.log(WithdrawalOptionsAnalytics.withdrawalChange)
    }
}

private extension NewWithdrawViewModel {
    func checkWithdrawStatus(_ account: WithdrawAccount) {
        service.checkAvailableWithdraw { [weak self] result in
            switch result {
            case .success(let withdrawItem):
                guard let withdrawItem = withdrawItem, withdrawItem.pending else {
                    self?.presenter.didReceiveBankAccount(account)
                    return
                }
                self?.presenter.pendingWithdraw(withdrawItem)
            case .failure(.badRequest(let error)):
                self?.presenter.rollBackFail(LegacyPJError(message: error.message))
            default:
                self?.presenter.rollBackFail(LegacyPJError.requestError)
            }
        }
    }

    func bankAccountWithdraw(_ amount: Double) {
        guard let account = mainAccount, amount > 0 else { return }

        service.authenticate { [weak self] result in
            switch result {
            case .success(let pin):
                self?.newBankAccountWithdraw(pin, account: account, amount: amount)
            case .canceled:
                self?.presenter.didCancelAuth()
            case .failure(let error):
                self?.presenter.fail(error.localizedDescription)
            }
        }
    }

    func picpayAccountWithdraw(_ amount: Double) {
        guard let account = picpayAccount, amount > 0 else { return }

        service.authenticate { [weak self] result in
            switch result {
            case .success(let pin):
                self?.newPicPayAccountWithdraw(pin, username: account.username, amount: amount)
            case .canceled:
                self?.presenter.didCancelAuth()
            case .failure(let error):
                self?.presenter.fail(error.localizedDescription)
            }
        }
    }

    func newPicPayAccountWithdraw(_ pin: String, username: String, amount: Double) {
        presenter.startLoading()
        service.requestPicPayAccountWithdraw(pin, amount: amount) { [weak self] result in
            switch result {
            case .success(let response):
                self?.picpayWithdrawEvent(for: username, amount: amount)
                self?.presenter.picpayAccountSuccess(response?.message ?? "")
            case .failure(.badRequest(let error)):
                self?.presenter.fail(error.message)
            case .failure:
                self?.presenter.fail(LegacyPJError.requestError.message)
            }
        }
    }

    func newBankAccountWithdraw(_ pin: String, account: WithdrawAccount, amount: Double) {
        presenter.startLoading()
        service.requestBankAccountWithdraw(pin, account: account, amount: amount) { [weak self] result in
            switch result {
            case .success(let response):
                self?.bankWithdrawEvent(for: account, amount: amount)
                self?.presenter.bankAccountSuccess(response?.message ?? "")
            case .failure(.badRequest(let error)):
                self?.presenter.fail(error.message)
            default:
                self?.presenter.fail(LegacyPJError.requestError.message)
            }
        }
    }

    func bankWithdrawEvent(for account: WithdrawAccount, amount: Double) {
        let params = NewWithdrawAnalytics.WithdrawBankData(
            type: .bankAccount,
            value: amount,
            bankCode: account.bank.id,
            bankName: account.bank.name)

        dependencies.analytics.log(NewWithdrawAnalytics.bankWithdraw(data: params))
    }

    func picpayWithdrawEvent(for username: String, amount: Double) {
        let params = NewWithdrawAnalytics.WithdrawPicPayData(
            type: .picpayAccount,
            value: amount,
            username: username)

        dependencies.analytics.log(NewWithdrawAnalytics.picpayWithdraw(data: params))
    }
}
