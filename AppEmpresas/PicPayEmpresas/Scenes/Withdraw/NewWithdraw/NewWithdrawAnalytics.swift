import AnalyticsModule

enum NewWithdrawAnalytics: AnalyticsKeyProtocol {
    case bankWithdraw(data: WithdrawBankData)
    case picpayWithdraw(data: WithdrawPicPayData)

    func event() -> AnalyticsEventProtocol {
        switch self {
        case .bankWithdraw(let data):
            return AnalyticsEvent("withdraw", properties: data.properties(), providers: [.mixPanel])
        case .picpayWithdraw(let data):
            return AnalyticsEvent("withdraw", properties: data.properties(), providers: [.mixPanel])
        }
    }
}

extension NewWithdrawAnalytics {
    enum WithdrawAnalyticsType: String, RawRepresentable {
        case bankAccount = "bank_account"
        case picpayAccount = "wallet-pf"
    }

    struct WithdrawBankData {
        let type: WithdrawAnalyticsType
        let value: Double
        let bankCode: String
        let bankName: String

        func properties() -> [String: Any] {
            let properties: [String: Any] = [
                "type": type.rawValue,
                "value": value,
                "bank_code": bankCode,
                "bank_name": bankName
            ]
            return properties
        }
    }

    struct WithdrawPicPayData {
        let type: WithdrawAnalyticsType
        let value: Double
        let username: String

        func properties() -> [String: Any] {
            let properties: [String: Any] = [
                "type": type.rawValue,
                "value": value,
                "username": username
            ]
            return properties
        }
    }
}
