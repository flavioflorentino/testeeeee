import Core
import Foundation
import LegacyPJ

protocol NewWithdrawPresenting: AnyObject {
    var viewController: NewWithdrawDisplay? { get set }
    func showBankAccount()
    func showPicPayAccount()
    func didNextStep(action: NewWithdrawAction)
    func didReceivePicPayAccount(_ account: PicpayAccountModel)
    func didReceiveBankAccount(_ account: WithdrawAccount)
    func availableAmount(_ amount: String)
    func enableButton(_ state: Bool)
    func bankAccountSuccess(_ message: String)
    func picpayAccountSuccess(_ message: String)
    func fail(_ message: String)
    func rollBackFail(_ error: LegacyPJError)
    func didCancelAuth()
    func pendingWithdraw(_ withdrawItem: UnavailableWithdrawItem)
    func startLoading()
}

struct WithdrawAccountItem {
    let bankName: String
    let imageUrl: String
    let branch: String
    let account: String
}

extension WithdrawAccountItem {
    init(_ withdrawAccount: WithdrawAccount) {
        bankName = withdrawAccount.bank.name
        imageUrl = withdrawAccount.bank.imageUrl
        branch = withdrawAccount.branch
        account = "\(withdrawAccount.account)-\(withdrawAccount.accountDigit)"
    }
}

final class NewWithdrawPresenter: NewWithdrawPresenting {
    private let coordinator: NewWithdrawCoordinating
    weak var viewController: NewWithdrawDisplay?

    init(coordinator: NewWithdrawCoordinating) {
        self.coordinator = coordinator
    }

    func didNextStep(action: NewWithdrawAction) {
        coordinator.perform(action: action)
    }

    func showBankAccount() {
        viewController?.perform(.showBankAccount)
    }

    func showPicPayAccount() {
        viewController?.perform(.showPicPayAccount)
    }

    func didReceiveBankAccount(_ account: WithdrawAccount) {
        viewController?.perform(.presentBankAccount(WithdrawAccountItem(account)))
    }

    func didReceivePicPayAccount(_ account: PicpayAccountModel) {
        viewController?.perform(.presentPicPayAccount(account))
    }

    func availableAmount(_ amount: String) {
        viewController?.perform(.availableAmount(amount.currency()))
    }

    func enableButton(_ enable: Bool) {
        viewController?.perform(.enableButton(enable))
    }

    func bankAccountSuccess(_ message: String) {
        viewController?.perform(.stopLoading)
        coordinator.perform(action: .successBankAccount(message: message))
    }

    func picpayAccountSuccess(_ message: String) {
        viewController?.perform(.stopLoading)
        coordinator.perform(action: .successPicPayAccount(message: message))
    }

    func fail(_ message: String) {
        viewController?.perform(.validationMessage(message))
    }

    func rollBackFail(_ error: LegacyPJError) {
        viewController?.perform(.rollBackError(error.message))
    }

    func didCancelAuth() {
        viewController?.perform(.stopLoading)
    }

    func pendingWithdraw(_ withdrawItem: UnavailableWithdrawItem) {
        didNextStep(action: .pendingWithdraw(withdrawItem))
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
}
