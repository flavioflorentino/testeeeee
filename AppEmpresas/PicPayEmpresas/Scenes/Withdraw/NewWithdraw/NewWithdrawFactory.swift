import Foundation

enum NewWithdrawFactory {
    static func make(
        _ balanceItem: WalletBalanceItem,
        bankAccount: WithdrawAccount
    ) -> NewWithdrawViewController {
        let container = DependencyContainer()
        let service: NewWithdrawServicing = NewWithdrawService(dependencies: container)
        let coordinator: NewWithdrawCoordinating = NewWithdrawCoordinator()
        let presenter: NewWithdrawPresenting = NewWithdrawPresenter(coordinator: coordinator)
        let viewModel = NewWithdrawViewModel(.bankAccount, service: service, presenter: presenter, dependencies: container)
        viewModel.balanceItem = balanceItem
        viewModel.mainAccount = bankAccount
        let viewController = NewWithdrawViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
    
    static func make(
        _ balanceItem: WalletBalanceItem,
        picpayAccount: PicpayAccountModel
    ) -> NewWithdrawViewController {
        let container = DependencyContainer()
        let service: NewWithdrawServicing = NewWithdrawService(dependencies: container)
        let coordinator: NewWithdrawCoordinating = NewWithdrawCoordinator()
        let presenter: NewWithdrawPresenting = NewWithdrawPresenter(coordinator: coordinator)
        let viewModel = NewWithdrawViewModel(.picpay, service: service, presenter: presenter, dependencies: container)
        viewModel.balanceItem = balanceItem
        viewModel.picpayAccount = picpayAccount
        let viewController = NewWithdrawViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
