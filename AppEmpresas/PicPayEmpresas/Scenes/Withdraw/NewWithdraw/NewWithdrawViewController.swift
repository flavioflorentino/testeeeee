import SnapKit
import UI
import UIKit

protocol NewWithdrawDisplay: AnyObject {
    func startLoading()
    func perform(_ displayAction: NewWithdrawDisplayAction)
}

enum NewWithdrawDisplayAction {
    case presentPicPayAccount(_ accountItem: PicpayAccountModel)
    case presentBankAccount(_ accountItem: WithdrawAccountItem)
    case availableAmount(_ amount: String)
    case enableButton(_ enable: Bool)
    case validationMessage(_ message: String)
    case stopLoading
    case rollBackError(_ message: String)
    case showBankAccount
    case showPicPayAccount
}

extension NewWithdrawViewController.Layout {
    enum BankNameLabel {
        static let top: CGFloat = 17
        static let leading: CGFloat = 10
    }
    
    enum BankImage {
        static let size: CGFloat = 32
        static let top: CGFloat = 16
        static let leading: CGFloat = 20
    }
    
    enum BranchAccountStack {
        static let spacing: CGFloat = 4
        static let top: CGFloat = 4
        static let leading: CGFloat = 10
    }
    
    enum BranchLabel {
        static let top: CGFloat = 4
    }
    
    enum BranchNumberLabel {
        static let top: CGFloat = 4
        static let leading: CGFloat = 10
    }
    
    enum LineView {
        static let top: CGFloat = 16
        static let height: CGFloat = 1
    }
    
    enum HowMuchLabel {
        static let top: CGFloat = 16
    }
    
    enum AmountTextField {
        static let top: CGFloat = 16
        static let leading: CGFloat = 16
        static let trailing: CGFloat = 16
    }
    
    enum CurrencySymbol {
        static let size: CGFloat = 12
    }
    
    enum AvailableStack {
        static let top: CGFloat = 16
        static let spacing: CGFloat = 4
        static let leading: CGFloat = 16
        static let trailing: CGFloat = 16
    }
    
    enum WithdrawButton {
        static let top: CGFloat = 12
        static let height: CGFloat = 48
        static let leading: CGFloat = 16
        static let trailing: CGFloat = 16
        static let cornerRadius: CGFloat = 24
    }
    
    enum Colors {
        static let label = Palette.ppColorGrayscale500.color
        static let value = Palette.ppColorGrayscale600.color
    }
    
    enum Fonts {
        static let superSmallRegular = UIFont.systemFont(ofSize: 12, weight: .regular)
        static let superSmallBold = UIFont.systemFont(ofSize: 12, weight: .bold)
        static let smallBold = UIFont.systemFont(ofSize: 12, weight: .bold)
        static let mediumRegular = UIFont.systemFont(ofSize: 14, weight: .regular)
        static let mediumBold = UIFont.systemFont(ofSize: 14, weight: .bold)
        static let mediumUpBold = UIFont.systemFont(ofSize: 16, weight: .bold)
        static let bigRegular = UIFont.systemFont(ofSize: 24, weight: .regular)
        static let giantRegular = UIFont.systemFont(ofSize: 52, weight: .regular)
    }
    
    enum LoagindView {
        static let alpha: CGFloat = 0.8
    }
}

final class NewWithdrawViewController: ViewController<NewWithdrawViewModelInputs, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }
    
    private lazy var bankAccountView = BankAccountView()
    
    private lazy var picpayAccountView = PicPayAccountView()
    
    private lazy var howMuchLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.NewWithdraw.howMuchWithdraw
        label.font = Layout.Fonts.mediumRegular
        label.textColor = Layout.Colors.label
        label.textAlignment = .left
        return label
    }()
    
    private lazy var amountTextField: CurrencyField = {
        let value = CurrencyField()
        value.fontCurrency = Layout.Fonts.bigRegular
        value.fontValue = Layout.Fonts.giantRegular
        value.placeholderColor = Palette.ppColorGrayscale400.color
        value.textColor = Palette.ppColorBranding300.color
        value.tintColor = Palette.ppColorBranding300.color
        value.positionCurrency = .top
        value.delegate = self
        return value
    }()
    
    private lazy var currencySymbol: UIImageView = {
        let image = Assets.cifrao.image
        let imageView = UIImageView(image: image)
        return imageView
    }()
    
    private lazy var availableLabel: UILabel = {
        let label = UILabel()
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        label.text = Strings.NewWithdraw.available
        label.font = Layout.Fonts.mediumRegular
        label.textColor = Layout.Colors.label
        label.textAlignment = .right
        return label
    }()
    
    private lazy var availableNumberLabel: UILabel = {
        let label = UILabel()
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        label.font = Layout.Fonts.mediumBold
        label.textColor = Layout.Colors.label
        label.textAlignment = .left
        return label
    }()
    
    private lazy var availableStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.alignment = .center
        stack.spacing = Layout.AvailableStack.spacing
        stack.addArrangedSubview(currencySymbol)
        stack.addArrangedSubview(availableLabel)
        stack.addArrangedSubview(availableNumberLabel)
        return stack
    }()
    
    private lazy var withdrawButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(Strings.Default.confirm, for: .normal)
        button.titleLabel?.font = Layout.Fonts.mediumUpBold
        button.normalTitleColor = Palette.white.color
        button.disabledTitleColor = Palette.white.color
        button.normalBackgrounColor = Palette.ppColorBranding300.color
        button.highlightedBackgrounColor = Palette.ppColorBranding400.color
        button.disabledBackgrounColor = Palette.ppColorGrayscale400.color
        button.cornerRadius = Layout.WithdrawButton.cornerRadius
        button.isEnabled = false
        button.addTarget(self, action: #selector(withdrawButtonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var loadingView: LoadingView = {
        let view = LoadingView()
        view.backgroundColor = view.backgroundColor?.withAlphaComponent(Layout.LoagindView.alpha)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.loadView()
        startLoadingView()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(bankAccountView)
        view.addSubview(picpayAccountView)
        view.addSubview(howMuchLabel)
        view.addSubview(amountTextField)
        view.addSubview(availableStack)
        view.addSubview(withdrawButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.white.color
        title = Strings.NewWithdraw.title
        bankAccountView.isHidden = true
        bankAccountView.delegate = self
        picpayAccountView.isHidden = true
        picpayAccountView.delegate = self
    }
    
    override func setupConstraints() {
        bankAccountView.snp.makeConstraints {
            $0.top.leading.trailing.equalTo(view)
        }
        
        picpayAccountView.snp.makeConstraints {
            $0.top.leading.trailing.equalTo(view)
        }
        
        howMuchLabel.snp.makeConstraints {
            $0.top.equalTo(bankAccountView.snp.bottom).offset(Layout.HowMuchLabel.top)
            $0.centerX.equalTo(view)
        }
        
        amountTextField.snp.makeConstraints {
            $0.top.equalTo(howMuchLabel.snp.bottom).offset(Layout.AmountTextField.top)
            $0.centerX.equalTo(view)
            $0.leading.greaterThanOrEqualTo(view).offset(Layout.AmountTextField.leading)
            $0.trailing.lessThanOrEqualTo(view).offset(-Layout.AmountTextField.trailing)
        }
        
        currencySymbol.snp.makeConstraints {
            $0.size.equalTo(Layout.CurrencySymbol.size)
        }
        
        availableStack.snp.makeConstraints {
            $0.top.equalTo(amountTextField.snp.bottom).offset(Layout.AvailableStack.top)
            $0.centerX.equalTo(view)
            $0.leading.greaterThanOrEqualTo(view).offset(Layout.AvailableStack.leading)
            $0.trailing.lessThanOrEqualTo(view).offset(-Layout.AvailableStack.trailing)
        }
        
        withdrawButton.snp.makeConstraints {
            $0.top.equalTo(availableStack.snp.bottom).offset(Layout.WithdrawButton.top)
            $0.centerX.equalTo(view)
            $0.leading.equalTo(view).offset(Layout.WithdrawButton.leading)
            $0.trailing.equalTo(view).offset(-Layout.WithdrawButton.trailing)
            $0.height.equalTo(Layout.WithdrawButton.height)
        }
    }
}

// MARK: View Model Outputs
extension NewWithdrawViewController: NewWithdrawDisplay {
    func startLoading() {
        startLoadingView()
        view.endEditing(true)
    }
    
    func perform(_ displayAction: NewWithdrawDisplayAction) {
        switch displayAction {
        case .presentBankAccount(let accountItem):
            bankAccountView.setupAccount(accountItem)
            stopLoadingView()
            amountTextField.becomeResponder()
        case .presentPicPayAccount(let accountItem):
            picpayAccountView.setupAccount(accountItem)
            stopLoadingView()
            amountTextField.becomeResponder()
        case .availableAmount(let amount):
            availableNumberLabel.text = amount
        case .enableButton(let enable):
            withdrawButton.isEnabled = enable
        case .stopLoading:
            stopLoadingView()
        case .validationMessage(let message):
            validationMessage(message)
        case .rollBackError(let message):
            rollBackErrorMessage(message)
        case .showBankAccount:
            bankAccountView.isHidden = false
            view.bringSubviewToFront(bankAccountView)
        case .showPicPayAccount:
            picpayAccountView.isHidden = false
            view.bringSubviewToFront(picpayAccountView)
        }
    }
}

// MARK: Private functions
private extension NewWithdrawViewController {
    func validationMessage(_ message: String) {
        stopLoadingView()
        let popup = newPopup(with: message)
        present(popup, animated: true, completion: nil)
    }
    
    func rollBackErrorMessage(_ message: String) {
        stopLoadingView()
        let popup = newPopup(with: message)
        popup.delegate = self
        present(popup, animated: true, completion: nil)
    }
    
    func newPopup(with message: String) -> PopupViewController {
        let popup = PopupViewController()
        let alertPopup = AlertPopupController()
        alertPopup.popupTitle = Strings.NewWithdraw.validationTitle
        alertPopup.popupText = message
        popup.contentController = alertPopup
        return popup
    }
}

// MARK: PopupDelegate
extension NewWithdrawViewController: PopupDelegate {
    func popUpDidClose() {
        viewModel.dismiss()
    }
}

// MARK: Currency Field Delegate
extension NewWithdrawViewController: CurrencyFieldDelegate {
    func onValueChange(value: Double) {
        viewModel.newAmountValue(value)
    }
}

// MARK: Button Targets
@objc
extension NewWithdrawViewController {
    func withdrawButtonTapped() {
        viewModel.requestWithdraw(amountTextField.value)
    }
}

// MARK: PicPay Account View Delegate
extension NewWithdrawViewController: AccountViewDelegate {
    func didTapChangeAccountButton() {
        viewModel.logAccountChange()
        viewModel.dismiss()
    }
}
