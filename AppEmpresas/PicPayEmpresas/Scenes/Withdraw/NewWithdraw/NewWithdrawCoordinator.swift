import UIKit

enum NewWithdrawAction: Equatable {
    case successBankAccount(message: String)
    case successPicPayAccount(message: String)
    case pendingWithdraw(_ withdraw: UnavailableWithdrawItem)
    case dismiss
}

protocol NewWithdrawCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: NewWithdrawAction)
}

final class NewWithdrawCoordinator: NewWithdrawCoordinating {
    weak var viewController: UIViewController?

    func perform(action: NewWithdrawAction) {
        switch action {
        case let .successBankAccount(message):
            navigateToSuccess(message: message, type: .bankAccount)
        case let .successPicPayAccount(message):
            navigateToSuccess(message: message, type: .picpay)
        case let .pendingWithdraw(withdrawItem):
            let controller = UnavailableWithdrawFactory.make(withdrawItem, delegate: self)
            viewController?.tabBarController?.present(controller, animated: true)
        case .dismiss:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}

private extension NewWithdrawCoordinator {
    func navigateToSuccess(message: String, type: WithdrawType) {
        let controller = WithdrawSuccessFactory.make(
            type: type,
            showAutoWithdraw: false,
            message: message,
            navigationController: viewController?.navigationController
        )
        viewController?.navigationController?.present(controller, animated: true)
        viewController?.navigationController?.popToRootViewController(animated: true)
    }
}

extension NewWithdrawCoordinator: UnavailableWithdrawDelegate {
    func viewDidDismiss() {
        perform(action: .dismiss)
    }
}
