import Core
import LegacyPJ

protocol NewWithdrawServicing {
    func authenticate(_ completion: @escaping (_ result: AuthManager.PerformActionResult) -> Void)
    func requestBankAccountWithdraw(
        _ pin: String,
        account: WithdrawAccount,
        amount: Double,
        completion: @escaping (Result<BankWithdrawResponse?, ApiError>) -> Void
    )
    func requestPicPayAccountWithdraw(
        _ pin: String,
        amount: Double,
        completion: @escaping (Result<PicPayWithdrawResponse?, ApiError>) -> Void
    )
    func checkAvailableWithdraw(_ completion: @escaping (Result<UnavailableWithdrawItem?, ApiError>) -> Void)
}

struct BankWithdrawResponse: Decodable {
    let message: String
}

struct PicPayWithdrawResponse: Decodable {
    let id: String
    let status: String
    let value: Double
    let balance: Double
    let message: String
}

struct WithdrawBank: Decodable, Equatable {
    private enum CodingKeys: String, CodingKey {
        case id
        case name = "nome"
        case imageUrl = "img_url"
    }

    let id: String
    let name: String
    let imageUrl: String
}

struct WithdrawAccount: Decodable, Equatable {
    private enum CodingKeys: String, CodingKey {
        case id
        case type = "tipo"
        case operation = "operacao"
        case bankId = "banco_id"
        case branch = "agencia"
        case account = "conta"
        case accountDigit = "conta_dv"
        case isPrimary = "is_primary"
        case bank
    }

    let id: Int
    let type: String
    let operation: String?
    let bankId: String
    let branch: String
    let account: String
    let accountDigit: String
    let isPrimary: Bool
    let bank: WithdrawBank

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        type = try container.decode(String.self, forKey: .type)
        operation = try container.decodeIfPresent(String.self, forKey: .operation)
        bankId = try container.decode(String.self, forKey: .bankId)
        branch = try container.decode(String.self, forKey: .branch)
        account = try container.decode(String.self, forKey: .account)
        accountDigit = try container.decode(String.self, forKey: .accountDigit)
        let primary = try container.decode(Int.self, forKey: .isPrimary)
        isPrimary = primary == 1 ? true : false
        bank = try container.decode(WithdrawBank.self, forKey: .bank)
    }
}

final class NewWithdrawService: NewWithdrawServicing {
    typealias Dependencies = HasMainQueue & HasAuthManager
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func authenticate(_ completion: @escaping (_ result: AuthManager.PerformActionResult) -> Void) {
        dependencies.authManager.performActionWithAuthorization { result in
            completion(result)
        }
    }

    func requestBankAccountWithdraw(
        _ pin: String,
        account: WithdrawAccount,
        amount: Double,
        completion: @escaping (Result<BankWithdrawResponse?, ApiError>) -> Void
    ) {
        let endpoint = NewWithdrawEndpoint.bankAccountWithdraw(pin, accountId: account.id, value: Decimal(amount))
        BizApi<BankWithdrawResponse>(endpoint: endpoint).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result
                    .map { $0.model.data }

                completion(mappedResult)
            }
        }
    }

    func requestPicPayAccountWithdraw(
        _ pin: String,
        amount: Double,
        completion: @escaping (Result<PicPayWithdrawResponse?, ApiError>) -> Void
    ) {
        let endpoint = NewWithdrawEndpoint.picpayAccountWithdraw(pin, value: Decimal(amount))

        BizApi<PicPayWithdrawResponse>(endpoint: endpoint).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map {
                    $0.model.data
                }
                completion(mappedResult)
            }
        }
    }

    func checkAvailableWithdraw(_ completion: @escaping (Result<UnavailableWithdrawItem?, ApiError>) -> Void) {
        let endpoint = NewWithdrawEndpoint.withdrawStatus

        BizApi<UnavailableWithdrawItem>(endpoint: endpoint).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map { $0.model.data }
                completion(mappedResult)
            }
        }
    }
}
