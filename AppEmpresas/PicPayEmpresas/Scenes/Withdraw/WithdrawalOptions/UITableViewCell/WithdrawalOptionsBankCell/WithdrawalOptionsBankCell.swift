import SnapKit
import UI
import UIKit

fileprivate extension WithdrawalOptionsBankCell.Layout {
    enum Fonts {
        static let small = UIFont.systemFont(ofSize: 12, weight: .regular)
        static let smallBold = UIFont.systemFont(ofSize: 12, weight: .bold)
        static let regular = UIFont.systemFont(ofSize: 14, weight: .regular)
        static let bold = UIFont.systemFont(ofSize: 14, weight: .bold)
    }
    
    enum PerfilImageView {
        static let size = CGSize(width: 40, height: 40)
    }
    
    enum AccessoryImageView {
        static let size = CGSize(width: 8, height: 16)
    }
    
    enum ReceiveLabel {
        static let size = CGSize(width: 176, height: 16)
    }
}

final class WithdrawalOptionsBankCell: UITableViewCell {
    typealias Localizable = Strings.WithdrawalOptions
    
    fileprivate struct Layout { }
    
    // MARK: - Private Properties
    
    private lazy var personalAccLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.bold
        label.textColor = Palette.ppColorGrayscale500.color
        label.text = Localizable.otherAccounts
        return label
    }()
    
    private lazy var profileImageView = UIImageView()
    
    private lazy var infoStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .leading
        stack.spacing = Spacing.base01
        return stack
    }()
    
    private lazy var bankLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.bold
        return label
    }()
    
    private lazy var accountLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.regular
        label.textColor = Palette.ppColorGrayscale400.color
        return label
    }()
    
    private lazy var accessoryImageView = UIImageView(image: Assets.accessoryArrow.image)
    
    private lazy var receiveLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.contentMode = .center
        label.font = Layout.Fonts.smallBold
        label.text = Localizable.receiveInTwoDays
        label.textColor = Palette.white.color
        label.backgroundColor = Palette.ppColorBranding300.color
        label.layer.cornerRadius = CornerRadius.light
        label.clipsToBounds = true
        return label
    }()
    
    // MARK: - Inits
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        buildLayout()
    }
}

// MARK: - Public Methods

extension WithdrawalOptionsBankCell {
    func setup(with model: BankAccountWithdrawOption) {
        bankLabel.text = model.bankName
        accountLabel.text = model.account
        receiveLabel.text = model.receiveInfo
        profileImageView.sd_setImage(with: URL(string: model.imageUrl))
    }
}

// MARK: - ViewConfiguration

extension WithdrawalOptionsBankCell: ViewConfiguration {
    func setupConstraints() {
        personalAccLabel.snp.makeConstraints {
            $0.top.equalTo(contentView).offset(Spacing.base02)
            $0.leading.equalTo(contentView).offset(Spacing.base02)
            $0.trailing.equalTo(contentView).offset(-Spacing.base02)
        }
        
        profileImageView.snp.makeConstraints {
            $0.top.equalTo(personalAccLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalTo(contentView).offset(Spacing.base02)
            $0.size.equalTo(Layout.PerfilImageView.size)
        }
        
        infoStackView.snp.makeConstraints {
            $0.top.equalTo(profileImageView)
            $0.leading.equalTo(profileImageView.snp.trailing).offset(Spacing.base02)
            $0.bottom.equalTo(contentView).offset(-Spacing.base02)
        }
        
        accessoryImageView.snp.makeConstraints {
            $0.leading.equalTo(infoStackView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalTo(contentView).offset(-Spacing.base02)
            $0.size.equalTo(Layout.AccessoryImageView.size)
            $0.centerY.equalTo(infoStackView)
        }
        
        receiveLabel.snp.makeConstraints {
            $0.size.equalTo(Layout.ReceiveLabel.size)
        }
    }
    
    func buildViewHierarchy() {
        infoStackView.addArrangedSubview(bankLabel)
        infoStackView.addArrangedSubview(accountLabel)
        infoStackView.addArrangedSubview(receiveLabel)
        
        contentView.addSubview(personalAccLabel)
        contentView.addSubview(profileImageView)
        contentView.addSubview(infoStackView)
        contentView.addSubview(accessoryImageView)
    }
}
