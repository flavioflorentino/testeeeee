import Core

enum WithdrawalOptionsEndpoint {
    case getMainAccount
    case getPicpayUsers
}

extension WithdrawalOptionsEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .getMainAccount:
            return "/withdrawals/bank-account"
        case .getPicpayUsers:
            return "/seller/partners"
        }
    }
}
