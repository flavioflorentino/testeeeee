import AnalyticsModule
import Foundation

enum WithdrawalOptionsAnalyticsType: String {
    case walletPf = "wallet-pf"
    case bankAccount = "bank-account"
}

enum WithdrawalOptionsAnalytics: AnalyticsKeyProtocol {
    case bankErrorShow(accountType: String, errorType: String)
    case bankErrorUpdate(accountType: String, errorType: String)
    case bankErrorClose(accountType: String, errorType: String)
    case bankErrorFail(accountType: String, errorType: String)
    case withdrawalOption(type: WithdrawalOptionsAnalyticsType)
    case withdrawalChange
    case profileValidationAlert
    case profileCreate

    private var name: String {
        switch self {
        case .bankErrorShow:
            return "Bank Account Error Withdraw"
        case .bankErrorUpdate:
            return "Bank Account Error Withdraw - To Update"
        case .bankErrorClose:
            return "Bank Account Error Withdraw - Not now"
        case .bankErrorFail:
            return "Bank Account Error Systemic Error"
        case .withdrawalOption:
            return "Withdraw Intention Options"
        case .withdrawalChange:
            return "Withdraw Change Option"
        case .profileValidationAlert:
            return "Withdraw Profile Validation Alert"
        case .profileCreate:
            return "Withdraw Create Profile"
        }
    }

    private var properties: [String: Any] {
        switch self {
        case let .bankErrorShow(accountType, errorType),
             let .bankErrorUpdate(accountType, errorType),
             let .bankErrorClose(accountType, errorType),
             let .bankErrorFail(accountType, errorType):
            return ["account_type": accountType, "error_type": errorType]
        case .withdrawalOption(let type):
            return ["type": type.rawValue]
        default:
            return [:]
        }
    }

    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
