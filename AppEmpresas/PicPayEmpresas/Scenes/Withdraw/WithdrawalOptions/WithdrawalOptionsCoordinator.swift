import AnalyticsModule
import CoreSellerAccount
import SnapKit
import UI
import UIKit
import FeatureFlag

enum WithdrawalOptionsAction {
    case openPicPayApp
    case openPicPayWithdrawal(account: PicpayAccountModel, balanceItem: WalletBalanceItem)
    case openBankWithdrawal(account: WithdrawAccount, balanceItem: WalletBalanceItem)
    case errorView
    case bankErrorModal(info: AlertModalInfo, bankError: WithdrawalOptionsBankError)
}

protocol WithdrawalOptionsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: WithdrawalOptionsAction)
}

final class WithdrawalOptionsCoordinator: WithdrawalOptionsCoordinating {
    typealias Dependencies = HasFeatureManager & HasAnalytics
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: WithdrawalOptionsAction) {
        switch action {
        case .openPicPayApp:
            openPicpayApp()
        case let .openPicPayWithdrawal(picpayAccount, balanceItem):
            let controller = NewWithdrawFactory.make(balanceItem, picpayAccount: picpayAccount)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case let .openBankWithdrawal(bankAccount, balanceItem):
            let controller = NewWithdrawFactory.make(balanceItem, bankAccount: bankAccount)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .errorView:
            presentErrorView()
        case let .bankErrorModal(info, bankError):
            presentBankErrorModal(info: info, bankError: bankError)
        }
    }
}

// MARK: - ErrorViewDelegate
extension WithdrawalOptionsCoordinator: ErrorViewDelegate {
    func didTouchConfirmButton() {
        viewController?.navigationController?.popViewController(animated: true)
    }
}

private extension WithdrawalOptionsCoordinator {
    func openPicpayApp() {
        let appURLScheme = dependencies.featureManager.text(.urlPicPayScheme)
        if let appURL = URL(string: appURLScheme), UIApplication.shared.canOpenURL(appURL) {
            UIApplication.shared.open(appURL)
            return
        }
        
        let urlStr = dependencies.featureManager.text(.urlPicPayAppStore)
        if let url = URL(string: urlStr), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func presentErrorView() {
        guard let view = viewController?.view else { return }

        let errorView = ErrorViewController()
        viewController?.addChild(errorView)
        view.addSubview(errorView.view)
        view.bringSubviewToFront(errorView.view)
        errorView.didMove(toParent: viewController)
        
        errorView.view.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
        }

        errorView.delegate = self
    }
    
    func presentBankErrorModal(info: AlertModalInfo, bankError: WithdrawalOptionsBankError) {
        guard let reason = bankError.bankError.validationIssue?.reason.rawValue else { return }
        let alertPopup = AlertModalFactory.make(alertModalInfo: info)
        
        alertPopup.touchButtonAction = { alert in
            alert.dismiss(animated: true) { [weak self] in
                let event = WithdrawalOptionsAnalytics.bankErrorUpdate(
                    accountType: bankError.companyType.rawValue,
                    errorType: reason
                )
                self?.dependencies.analytics.log(event)
                self?.navigateToEditBankAccount(companyType: bankError.companyType, seller: bankError.seller)
            }
        }
        
        alertPopup.touchSecondaryButtonAction = { [weak self] alert in
            let event = WithdrawalOptionsAnalytics.bankErrorClose(
                accountType: bankError.companyType.rawValue,
                errorType: reason
            )
            self?.dependencies.analytics.log(event)
            alert.dismiss(animated: true)
        }
        
        showAlert(contentController: alertPopup)
    }
    
    func showAlert(contentController: UIViewController) {
        let popup = PopupViewController()
        popup.contentController = contentController
        popup.hasCloseButton = false
        viewController?.present(popup, animated: true)
    }
    
    func navigateToEditBankAccount(companyType: CompanyType, seller: Seller) {
        let controller = BankAccountTipsFactory.makeEditTips(companyType: companyType, seller: seller)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
