import Foundation

enum WithdrawalOptionsFactory {
    static func make(_ balanceItem: WalletBalanceItem, bankError: WithdrawalOptionsBankError?) -> WithdrawalOptionsViewController {
        let container = DependencyContainer()
        let service: WithdrawalOptionsServicing = WithdrawalOptionsService(dependencies: container)
        let coordinator: WithdrawalOptionsCoordinating = WithdrawalOptionsCoordinator(dependencies: container)
        let presenter: WithdrawalOptionsPresenting = WithdrawalOptionsPresenter(coordinator: coordinator)
        
        let viewModel: WithdrawalOptionsViewModelProtocol = WithdrawalOptionsViewModel(
            balanceItem: balanceItem,
            bankAccountError: bankError
        )
        let interactor = WithdrawalOptionsInteractor(
            service: service,
            presenter: presenter,
            dependencies: container,
            viewModel: viewModel)
        
        let viewController = WithdrawalOptionsViewController(interactor: interactor)
        viewController.hidesBottomBarWhenPushed = true

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
