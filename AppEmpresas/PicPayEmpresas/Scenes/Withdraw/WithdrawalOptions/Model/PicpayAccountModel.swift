import Foundation

struct PicpayAccountModel: Decodable, Equatable {
    let name: String?
    let email: String
    let username: String
    let verified: Bool
}
