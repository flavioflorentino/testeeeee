import Foundation

enum WithdrawType: Equatable {
    case picpay, bankAccount
}

protocol WithdrawalOptionsInfo {
    var type: WithdrawType { get }
}

struct BankAccountWithdrawOption: WithdrawalOptionsInfo, Equatable {
    let bankAccount: WithdrawAccount
    let type: WithdrawType = .bankAccount

    var imageUrl: String {
        bankAccount.bank.imageUrl
    }
    
    var bankName: String {
        bankAccount.bank.name
    }

    var account: String {
        let account = "\(bankAccount.account)-\(bankAccount.accountDigit)"
        return "Ag: \(bankAccount.branch) | Conta: \(account)"
    }

    var receiveInfo: String {
        Strings.WithdrawalOptions.receiveInTwoDays
    }
}

struct PicPayWithdrawOption: WithdrawalOptionsInfo, Equatable {
    let picpayAccount: PicpayAccountModel?
    let type: WithdrawType = .picpay
    
    var username: String? {
        guard let username = picpayAccount?.username else {
            return nil
        }
        return "@\(username)"
    }
}
