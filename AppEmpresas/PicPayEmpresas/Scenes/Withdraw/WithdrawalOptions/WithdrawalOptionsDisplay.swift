import Foundation

protocol WithdrawalOptionsDisplay: AnyObject {
    func showLoading()
    func hideLoading()
    
    func didReceiveOptions(_ options: [WithdrawalOptionsInfo])
    func presentPicpayAccountInactive()
}
