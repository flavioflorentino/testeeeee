import AnalyticsModule
import FeatureFlag
import Core
import CoreSellerAccount
import LegacyPJ
import Foundation

protocol WithdrawalOptionsInteracting: AnyObject {
    func loadOptions()
    func selectIndexPath(indexPath: IndexPath)
}

final class WithdrawalOptionsInteractor {
    typealias Analytics = WithdrawalOptionsAnalytics
    typealias Dependencies = HasMainQueue & HasAuthManager & HasAnalytics & HasSellerInfo & HasFeatureManager
    
    // MARK: - Private Properties

    private let dependencies: Dependencies
    private let service: WithdrawalOptionsServicing
    private let presenter: WithdrawalOptionsPresenting
    private let viewModel: WithdrawalOptionsViewModelProtocol
    private var withdrawalOptions: [WithdrawalOptionsInfo] = []

    // MARK: - Inits

    init(
        service: WithdrawalOptionsServicing,
        presenter: WithdrawalOptionsPresenting,
        dependencies: Dependencies,
        viewModel: WithdrawalOptionsViewModelProtocol
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.viewModel = viewModel
    }
}

// MARK: - Private Methods

private extension WithdrawalOptionsInteractor {
    func fetchAccounts(shouldLoadPicPayAccount: Bool) {
        let group = DispatchGroup()

        var bankResult: Result<WithdrawAccount, Error>?
        var picpayResult: Result<PicpayAccountModel?, Error>?
        
        group.enter()
        fetchMainAccount { result in
            bankResult = result
            group.leave()
        }
        
        if shouldLoadPicPayAccount {
            group.enter()
            fetchPicpayAccount { result in
                picpayResult = result
                group.leave()
            }
        }
        
        group.notify(queue: dependencies.mainQueue) { [weak self] in
            guard let self = self else { return }
            self.setupOptions(bankResult: bankResult, picpayResult: picpayResult)
            self.presenter.setLoading(isLoading: false)
        }
    }
    
    func fetchMainAccount(completion: @escaping(Result<WithdrawAccount, Error>) -> Void) {
        service.getAccounts { result in
            switch result {
            case .success(let accounts):
                guard let mainAccount = accounts.first(where: { $0.isPrimary }) else {
                    let error = LegacyPJError(message: "")
                    completion(.failure(error))
                    return
                }

                completion(.success(mainAccount))

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func fetchPicpayAccount(completion: @escaping(Result<PicpayAccountModel?, Error>) -> Void) {
        service.getPicpayUsers { result in
            switch result {
            case .success(let accounts):
                completion(.success(accounts.first))

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func setupOptions(bankResult: Result<WithdrawAccount, Error>?, picpayResult: Result<PicpayAccountModel?, Error>?) {
        var withdrawalOptions: [WithdrawalOptionsInfo] = []
        
        switch picpayResult {
        case .success(let picpayAccount):
            let picpay = PicPayWithdrawOption(picpayAccount: picpayAccount)
            withdrawalOptions.append(picpay)
        case .failure:
            presenter.showErrorView()
            return
        case nil:
            break
        }

        switch bankResult {
        case .success(let mainAccount):
            let bank = BankAccountWithdrawOption(bankAccount: mainAccount)
            withdrawalOptions.append(bank)
        case .failure, nil:
            presenter.showErrorView()
            return
        }

        self.withdrawalOptions = withdrawalOptions
        presenter.setWithdrawalOptions(withdrawalOptions)
    }

    func navigateToPicpayAccount(picpayAccount: PicpayAccountModel?) {
        guard let picpayAccount = picpayAccount else {
            dependencies.analytics.log(Analytics.profileCreate)
            presenter.presentNoPicpayAccount()
            return
        }

        guard picpayAccount.verified else {
            dependencies.analytics.log(Analytics.profileValidationAlert)
            presenter.presentPicpayAccountInactive()
            return
        }

        presenter.presentPicpayWithdrawal(account: picpayAccount, balanceItem: viewModel.balanceItem)
    }
    
    func navigateToBankAccount(bankModel: BankAccountWithdrawOption) {
        if let bankError = viewModel.bankAccountError, let reason = bankError.bankError.validationIssue?.reason.rawValue {
            let event = WithdrawalOptionsAnalytics.bankErrorShow(
                accountType: bankError.companyType.rawValue,
                errorType: reason
            )
            dependencies.analytics.log(event)
            presenter.presentBankError(bankError: bankError)
            return
        }
        
        presenter.presentBankWithdrawal(account: bankModel.bankAccount, balanceItem: viewModel.balanceItem)
    }

    func logWithdrawalEvent(type: WithdrawalOptionsAnalyticsType) {
        dependencies.analytics.log(Analytics.withdrawalOption(type: type))
    }
}

// MARK: - WithdrawalOptionsInteracting

extension WithdrawalOptionsInteractor: WithdrawalOptionsInteracting {
    func selectIndexPath(indexPath: IndexPath) {
        guard withdrawalOptions.count > indexPath.row else { return }
        let model = withdrawalOptions[indexPath.row]

        if let bankModel = model as? BankAccountWithdrawOption {
            logWithdrawalEvent(type: .bankAccount)
            navigateToBankAccount(bankModel: bankModel)
            return
        }

        if let picpayModel = model as? PicPayWithdrawOption {
            logWithdrawalEvent(type: .walletPf)
            navigateToPicpayAccount(picpayAccount: picpayModel.picpayAccount)
        }
    }

    func loadOptions() {
        presenter.setLoading(isLoading: true)
        
        guard dependencies.featureManager.isActive(.isReceivementAccountAvailable) else {
            fetchAccounts(shouldLoadPicPayAccount: true)
            return
        }
        
        dependencies.sellerInfo.accountDetails { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let accountDetails):
                let isNotLTDA = accountDetails.companyNature != .ltda
                let isNotOthers = accountDetails.companyNature != .other
                let isNotMultipartnersOrOther = isNotLTDA && isNotOthers
                let isNotReceivement = accountDetails.bizAccountType != .receivementAccount
                let shouldLoadPicPayAccount = isNotReceivement || isNotMultipartnersOrOther
                self.fetchAccounts(shouldLoadPicPayAccount: shouldLoadPicPayAccount)
            case .failure:
                self.fetchAccounts(shouldLoadPicPayAccount: false)
            }
        }
    }
}
