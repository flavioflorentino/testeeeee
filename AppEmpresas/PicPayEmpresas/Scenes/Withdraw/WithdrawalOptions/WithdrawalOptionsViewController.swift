import SnapKit
import UI
import UIKit

fileprivate extension WithdrawalOptionsViewController.Layout {
    enum TableView {
        static let rowHeight = UITableView.automaticDimension
        static let estimatedRowHeight: CGFloat = 200
    }
    
    enum LoadingView {
        static let alpha: CGFloat = 0.8
    }
}

final class WithdrawalOptionsViewController: ViewController<WithdrawalOptionsInteracting, UIView> {
    typealias Localizable = Strings.WithdrawalOptions
    
    fileprivate struct Layout { }
    
    // MARK: - Public Properties
    
    lazy var loadingView: LoadingView = {
        let loadingView = LoadingView()
        loadingView.alpha = Layout.LoadingView.alpha
        return loadingView
    }()
    
    // MARK: - Private Properties
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.tableFooterView = UIView()
        tableView.rowHeight = Layout.TableView.rowHeight
        tableView.estimatedRowHeight = Layout.TableView.estimatedRowHeight
        tableView.register(WithdrawalOptionsBankCell.self, forCellReuseIdentifier: WithdrawalOptionsBankCell.identifier)
        tableView.register(WithdrawalOptionsPicPayCell.self, forCellReuseIdentifier: WithdrawalOptionsPicPayCell.identifier)
        return tableView
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, WithdrawalOptionsInfo> = {
        let dataSource = TableViewDataSource<Int, WithdrawalOptionsInfo>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = bindCell(_:_:_:)
        return dataSource
    }()

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadOptions()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func configureViews() {
        title = Localizable.title
        view.backgroundColor = Colors.grayscale100.color
        tableView.dataSource = dataSource
        tableView.delegate = self
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

// MARK: Private Methods

private extension WithdrawalOptionsViewController {
    func bindCell(_ tableView: UITableView, _ indexPath: IndexPath, _ model: WithdrawalOptionsInfo) -> UITableViewCell {
        if
            let model = model as? PicPayWithdrawOption,
            let cell = tableView.dequeueReusableCell(withIdentifier: WithdrawalOptionsPicPayCell.identifier, for: indexPath) as? WithdrawalOptionsPicPayCell {
            cell.setup(model: model)
            return cell
        }
        
        if
            let model = model as? BankAccountWithdrawOption,
            let cell = tableView.dequeueReusableCell(withIdentifier: WithdrawalOptionsBankCell.identifier, for: indexPath) as? WithdrawalOptionsBankCell {
            cell.setup(with: model)
            return cell
        }
        
        return UITableViewCell()
    }
}

// MARK: View Model Outputs

extension WithdrawalOptionsViewController: WithdrawalOptionsDisplay {
    func showLoading() {
        startLoadingView()
    }
    
    func hideLoading() {
        stopLoadingView()
    }
    
    func didReceiveOptions(_ options: [WithdrawalOptionsInfo]) {
        dataSource.add(items: options, to: 0)
    }
    
    func presentPicpayAccountInactive() {
        let popup = PopupViewController()
        let alertPopup = AlertPopupController()
        alertPopup.popupTitle = Localizable.validateTitle
        alertPopup.popupText = Localizable.validateText
        popup.contentController = alertPopup
        present(popup, animated: true, completion: nil)
    }
}

extension WithdrawalOptionsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.selectIndexPath(indexPath: indexPath)
    }
}

// MARK: - LoadingViewProtocol

extension WithdrawalOptionsViewController: LoadingViewProtocol { }
