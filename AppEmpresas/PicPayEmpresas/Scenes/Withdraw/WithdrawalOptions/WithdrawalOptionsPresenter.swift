import Core
import Foundation
import UIKit

protocol WithdrawalOptionsPresenting: AnyObject {
    var viewController: WithdrawalOptionsDisplay? { get set }
    func setLoading(isLoading: Bool)
    func setWithdrawalOptions(_ options: [WithdrawalOptionsInfo])
    func showErrorView()
    func presentBankWithdrawal(account: WithdrawAccount, balanceItem: WalletBalanceItem)
    func presentPicpayWithdrawal(account: PicpayAccountModel, balanceItem: WalletBalanceItem)
    func presentPicpayAccountInactive()
    func presentNoPicpayAccount()
    func presentBankError(bankError: WithdrawalOptionsBankError)
}

final class WithdrawalOptionsPresenter: WithdrawalOptionsPresenting {
    private typealias Localizable = Strings.WithdrawalOptions
    private let coordinator: WithdrawalOptionsCoordinating
    weak var viewController: WithdrawalOptionsDisplay?

    init(coordinator: WithdrawalOptionsCoordinating) {
        self.coordinator = coordinator
    }
    
    func setLoading(isLoading: Bool) {
        isLoading ? viewController?.showLoading() : viewController?.hideLoading()
    }
    
    func setWithdrawalOptions(_ options: [WithdrawalOptionsInfo]) {
        viewController?.didReceiveOptions(options)
    }
    
    func showErrorView() {
        coordinator.perform(action: .errorView)
    }
    
    func presentBankWithdrawal(account: WithdrawAccount, balanceItem: WalletBalanceItem) {
        coordinator.perform(action: .openBankWithdrawal(account: account, balanceItem: balanceItem))
    }
    
    func presentPicpayWithdrawal(account: PicpayAccountModel, balanceItem: WalletBalanceItem) {
        coordinator.perform(action: .openPicPayWithdrawal(account: account, balanceItem: balanceItem))
    }
    
    func presentPicpayAccountInactive() {
        viewController?.presentPicpayAccountInactive()
    }
    
    func presentNoPicpayAccount() {
        coordinator.perform(action: .openPicPayApp)
    }
    
    func presentBankError(bankError: WithdrawalOptionsBankError) {
        let imageInfo = AlertModalInfoImage(image: Assets.errorIcon.image, size: CGSize(width: 60, height: 60))
        let info = AlertModalInfo(
            imageInfo: imageInfo,
            title: Localizable.BankError.Modal.title,
            subtitle: Localizable.BankError.Modal.subtitle,
            buttonTitle: Localizable.BankError.Modal.PrimaryButton.title,
            secondaryButtonTitle: Localizable.BankError.Modal.SecondaryButton.title
        )
        
        coordinator.perform(action: .bankErrorModal(info: info, bankError: bankError))
    }
}
