import CoreSellerAccount
import Foundation

protocol WithdrawalOptionsViewModelProtocol {
    var balanceItem: WalletBalanceItem { get }
    var bankAccountError: WithdrawalOptionsBankError? { get }
}

struct WithdrawalOptionsViewModel: WithdrawalOptionsViewModelProtocol {
    let balanceItem: WalletBalanceItem
    let bankAccountError: WithdrawalOptionsBankError?
}

struct WithdrawalOptionsBankError: Equatable {
    let bankError: BankAccountValidation
    let seller: Seller
    let companyType: CompanyType
}
