import Core

protocol WithdrawalOptionsServicing {
    func getAccounts(_ completion: @escaping (Result<[WithdrawAccount], ApiError>) -> Void)
    func getPicpayUsers(_ completion: @escaping (Result<[PicpayAccountModel], ApiError>) -> Void)
}

final class WithdrawalOptionsService {
    typealias Dependencies = HasMainQueue
    
    // MARK: Private Properties
    
    private let dependencies: Dependencies
    
    // MARK: - Inits

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - Public Methods

extension WithdrawalOptionsService: WithdrawalOptionsServicing {
    func getAccounts(_ completion: @escaping (Result<[WithdrawAccount], ApiError>) -> Void) {
        BizApi<[WithdrawAccount]>(endpoint: WithdrawalOptionsEndpoint.getMainAccount).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                BizApiHelper.unwrapBizResult(result: result, completion: completion)
            }
        }
    }
    
    func getPicpayUsers(_ completion: @escaping (Result<[PicpayAccountModel], ApiError>) -> Void) {
        BizApi<[PicpayAccountModel]>(endpoint: WithdrawalOptionsEndpoint.getPicpayUsers).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                BizApiHelper.unwrapBizResult(result: result, completion: completion)
            }
        }
    }
}
