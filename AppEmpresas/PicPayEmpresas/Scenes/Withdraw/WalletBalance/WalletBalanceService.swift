import Core

protocol WalletBalanceServicing {
    func userBalance(_ completion: @escaping (Result<WalletBalanceItem?, ApiError>) -> Void)
}

final class WalletBalanceService: WalletBalanceServicing {
    typealias Dependencies = HasMainQueue
    var dependencies: Dependencies?
    
    init(_ dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func userBalance(_ completion: @escaping (Result<WalletBalanceItem?, ApiError>) -> Void) {
        BizApi<WalletBalanceItem>(endpoint: WalletBalanceEndpoint.getBalance).execute { [weak self] result in
            self?.dependencies?.mainQueue.async {
                completion(result.map { $0.model.data })
            }
        }
    }
}
