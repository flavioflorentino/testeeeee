import AnalyticsModule
import Core
import Foundation
import LegacyPJ

protocol WalletBalanceViewModelInputs: AnyObject {
    func setupView()
    func getBalance()
    func newWithdraw()
    func changeVisibility()
    func showStatementTab()
}

struct WalletBalanceItem: Decodable, Equatable {
    private enum CodingKeys: String, CodingKey {
        case available = "available_balance"
        case withheld = "withheld_balance"
        case blocked = "blocked_balance"
    }

    let available: String
    let withheld: String
    let blocked: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        available = try container.decode(String.self, forKey: .available)
        withheld = try container.decode(String.self, forKey: .withheld)
        blocked = try container.decodeIfPresent(String.self, forKey: .blocked)
    }
}

final class WalletBalanceViewModel {
    typealias Dependencies = HasAuthManager & HasAnalytics & HasKVStore
    private let dependencies: Dependencies
    private let service: WalletBalanceServicing
    private let presenter: WalletBalancePresenting
    
    private var balance: WalletBalanceItem?
    private let bankError: WithdrawalOptionsBankError?

    init(
        service: WalletBalanceServicing,
        presenter: WalletBalancePresenting,
        dependencies: Dependencies,
        balance: WalletBalanceItem? = nil,
        bankError: WithdrawalOptionsBankError? = nil
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.balance = balance
        self.bankError = bankError
    }
}

extension WalletBalanceViewModel: WalletBalanceViewModelInputs {
    func setupView() {
        let isBalanceHidden = dependencies.kvStore.boolFor(BizKvKey.isBalanceHidden)
        presenter.withdrawValues(isHidden: isBalanceHidden, animated: false)
        presenter.presentCurrency(Locale.currencySymbol())
    }

    func getBalance() {
        service.userBalance { [weak self] result in
            switch result {
            case .success(let balance):
                guard let balance = balance else { return }
                self?.balance = balance
                self?.presenter.presentBalance(balance)
            case .failure:
                self?.presenter.showError()
            }
        }
    }
    
    func newWithdraw() {
        guard let balance = balance else { return }
        
        if let sellerId = dependencies.authManager.user?.id {
            dependencies.analytics.log(WalletBalanceAnalytics.withdrawIntention(sellerId: sellerId))
        }
        
        if let bankError = bankError, let reason = bankError.bankError.validationIssue?.reason.rawValue {
            let event = BankAccountErrorAnalytics.homeToCheck(errorType: reason)
            dependencies.analytics.log(event)
        }
        
        presenter.didNextStep(action: .newWithdraw(balance, bankError: bankError))
    }

    func changeVisibility() {
        let isHidden = !dependencies.kvStore.boolFor(BizKvKey.isBalanceHidden)
        presenter.withdrawValues(isHidden: isHidden, animated: true)
        logEventFor(isHidden)
        dependencies.kvStore.set(value: isHidden, with: BizKvKey.isBalanceHidden)
    }

    func showStatementTab() {
        presenter.didNextStep(action: .showStatementTab)
    }
}

private extension WalletBalanceViewModel {
    func logEventFor(_ visibilityOn: Bool) {
        guard
            let sellerId = dependencies.authManager.user?.id,
            let balance = balance
            else {
                return
        }
        let event = WalletBalanceAnalytics.showBalanceStatus(balance.available, sellerId: sellerId, action: visibilityOn)
        dependencies.analytics.log(event)
    }
}
