import UI
import UIKit

protocol WalletBalanceDisplay: AnyObject {
    func setupCurrency(_ symbol: String?)
    func setupVisibility(_ hidden: Bool, animated: Bool)
    func display(_ balance: String, withheld: String)
    func newWithdrawalView(enabled: Bool)
    func displayBlockedBalance(_ blockedBalance: String)
    func displayErrorView()
}

extension WalletBalanceViewController.Layout {
    enum LineView {
        static let height: CGFloat = 1
    }

    enum BoxView {
        static let shadowRadius: CGFloat = 2
        static let shadowOpacity: Float = 1
    }
}

final class WalletBalanceViewController: ViewController<WalletBalanceViewModelInputs, UIView> {
    fileprivate enum Layout { }

    private lazy var boxView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.white.color
        view.layer.cornerRadius = CornerRadius.light
        view.layer.shadowColor = Colors.grayscale100.color.cgColor
        view.layer.shadowOpacity = Layout.BoxView.shadowOpacity
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = Layout.BoxView.shadowRadius
        return view
    }()
    
    private lazy var valuesView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapBalanceView))
        view.addGestureRecognizer(gesture)
        return view
    }()
    
    private lazy var withdrawView: WalletBalanceWithdrawView = {
        let view = WalletBalanceWithdrawView()
        view.visibilityTapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapVisibilityImage))
        return view
    }()
    
    private lazy var blockedStackView: WalletBalanceBlockedStackView = {
        let stackView = WalletBalanceBlockedStackView()
        stackView.isHidden = true
        return stackView
    }()
    
    private lazy var futureStackView = WalletBalanceFutureStackView()
    
    private lazy var arrowLabel: UILabel = {
        let label = UILabel()
        label.text = Iconography.angleRightB.rawValue
        label
            .labelStyle(IconLabelStyle(type: .large))
            .with(\.textColor, Colors.branding600.color)
        return label
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale050.lightColor
        return view
    }()

    private lazy var newWithdrawalView: WalletBalanceNewWithdrawView = {
        let view = WalletBalanceNewWithdrawView()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapWithdrawButton))
        view.addGestureRecognizer(tapGesture)
        return view
    }()
    
    private lazy var errorView: WalletBalanceErrorView = {
        let view = WalletBalanceErrorView()
        view.tryAgainAction = { [weak self] in
            self?.errorView.removeFromSuperview()
            self?.viewModel.getBalance()
        }
        return view
    }()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.setupView()
        viewModel.getBalance()
    }

    override func buildViewHierarchy() {
        valuesView.addSubviews(withdrawView, blockedStackView, futureStackView, arrowLabel)
        
        view.addSubviews(
            boxView,
            valuesView,
            lineView,
            newWithdrawalView
        )
    }

    override func configureViews() {
        view.backgroundColor = .clear
    }

    override func setupConstraints() {
        boxView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
        }
        
        valuesView.snp.makeConstraints {
            $0.top.leading.trailing.equalTo(boxView)
        }
        
        withdrawView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        setBlockedStackViewConstraints()
        blockedStackView.snp.makeConstraints {
            $0.height.equalTo(0)
        }
        
        futureStackView.snp.makeConstraints {
            $0.top.equalTo(blockedStackView.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
            $0.trailing.lessThanOrEqualToSuperview()
        }
        
        arrowLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-Spacing.base01)
        }
        
        lineView.snp.makeConstraints {
            $0.top.equalTo(valuesView.snp.bottom)
            $0.height.equalTo(Layout.LineView.height)
            $0.leading.trailing.equalTo(boxView)
        }
        
        newWithdrawalView.snp.makeConstraints {
            $0.top.equalTo(lineView.snp.bottom)
            $0.leading.trailing.equalTo(boxView)
            $0.bottom.equalTo(boxView)
        }
    }
}

// MARK: - Private Methods

private extension WalletBalanceViewController {
    func setBlockedStackViewConstraints() {
        blockedStackView.snp.remakeConstraints {
            $0.top.equalTo(withdrawView.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.lessThanOrEqualToSuperview()
        }
    }
}

// MARK: - Actions

@objc
private extension WalletBalanceViewController {
    func didTapWithdrawButton() {
        viewModel.newWithdraw()
    }

    func didTapVisibilityImage() {
        viewModel.changeVisibility()
    }

    func didTapBalanceView() {
        viewModel.showStatementTab()
    }
}

// MARK: View Model Outputs

extension WalletBalanceViewController: WalletBalanceDisplay {
    func setupCurrency(_ symbol: String?) {
        withdrawView.setCurrency(symbol)
        futureStackView.setCurrency(symbol)
        blockedStackView.setCurrency(symbol)
    }

    func setupVisibility(_ hidden: Bool, animated: Bool) {
        let duration = animated ? 0.25 : 0
        let labelAlpha: CGFloat = hidden ? 0 : 1
        let lineAlpha: CGFloat = hidden ? 1 : 0
        
        withdrawView.setVisibility(isHidden: hidden)

        UIView.animate(withDuration: duration) {
            self.withdrawView.setAlpha(labelAlpha: labelAlpha, lineAlpha: lineAlpha)
            self.blockedStackView.setAlpha(labelAlpha: labelAlpha, lineAlpha: lineAlpha)
            self.futureStackView.setAlpha(labelAlpha: labelAlpha, lineAlpha: lineAlpha)
        }
    }

    func display(_ balance: String, withheld: String) {
        withdrawView.setValue(balance)
        futureStackView.setValue(withheld)
    }
    
    func displayBlockedBalance(_ blockedBalance: String) {
        blockedStackView.setValue(blockedBalance)
        blockedStackView.isHidden = false
        setBlockedStackViewConstraints()
        self.parent?.viewDidLayoutSubviews()
    }

    func newWithdrawalView(enabled: Bool) {
        newWithdrawalView.setEnabled(isEnabled: enabled)
    }
    
    func displayErrorView() {
        valuesView.addSubview(errorView)
        errorView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        valuesView.bringSubviewToFront(errorView)
    }
}
