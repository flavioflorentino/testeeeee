import Core
import CustomerSupport
import UIKit
import FeatureFlag

enum WalletBalanceAction: Equatable {
    case newWithdraw(_ balanceItem: WalletBalanceItem, bankError: WithdrawalOptionsBankError? = nil)
    case showStatementTab
}

protocol WalletBalanceCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: WalletBalanceAction)
}

final class WalletBalanceCoordinator: WalletBalanceCoordinating {
    typealias Dependencies = HasFeatureManager & HasMainQueue
    weak var viewController: UIViewController?

    private let dependencies: Dependencies

    // MARK: - Init

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func perform(action: WalletBalanceAction) {
        switch action {
        case let .newWithdraw(balanceItem, bankError):
            navigateToNewWithdrawal(balanceItem: balanceItem, bankError: bankError)
        case .showStatementTab:
            dependencies.mainQueue.asyncAfter(deadline: .now() + 0.2) {
                 self.switchToStatementTab()
            }
        }
    }
}

private extension WalletBalanceCoordinator {
    func switchToStatementTab() {
        viewController?.tabBarController?.selectedIndex = 1
    }

    func navigateToNewWithdrawal(balanceItem: WalletBalanceItem, bankError: WithdrawalOptionsBankError?) {
        let controller = WithdrawalOptionsFactory.make(balanceItem, bankError: bankError)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    func navigateToJudicialBlockadeDetails() {
        let faqID = dependencies.featureManager.text(.faqArticleJudicialBlockade)
        let options = FAQOptions.article(id: faqID)
        let faqViewController = FAQFactory.make(option: options)
        let navController = UINavigationController(rootViewController: faqViewController)
        viewController?.navigationController?.present(navController, animated: true, completion: nil)
    }
}
