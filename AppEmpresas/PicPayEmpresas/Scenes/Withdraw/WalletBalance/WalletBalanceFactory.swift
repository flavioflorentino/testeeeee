import Foundation

enum WalletBalanceFactory {
    static func make(bankError: WithdrawalOptionsBankError?) -> WalletBalanceViewController {
        let container = DependencyContainer()
        let service: WalletBalanceServicing = WalletBalanceService(DependencyContainer())
        let coordinator: WalletBalanceCoordinating = WalletBalanceCoordinator(dependencies: container)
        let presenter: WalletBalancePresenting = WalletBalancePresenter(coordinator: coordinator)
        let viewModel = WalletBalanceViewModel(service: service, presenter: presenter, dependencies: container, bankError: bankError)
        let viewController = WalletBalanceViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
