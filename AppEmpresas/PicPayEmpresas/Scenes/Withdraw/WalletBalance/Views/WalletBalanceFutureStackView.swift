import UI
import UIKit

final class WalletBalanceFutureStackView: UIStackView {
    private lazy var futureLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.WalletBalance.futureMoviments
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        label.numberOfLines = 1
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        return label
    }()

    private lazy var futureCurrency: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        label.numberOfLines = 1
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        return label
    }()

    private lazy var futureValue: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .left)
        label.adjustsFontSizeToFitWidth = true
        label.setContentHuggingPriority(.defaultLow, for: .horizontal)
        label.numberOfLines = 1
        return label
    }()

    private lazy var futureLine: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale200.lightColor
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        axis = .horizontal
        distribution = .fill
        alignment = .center
        spacing = Spacing.base00
        buildLayout()
    }
    
    @available(*, unavailable)
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Internal Methods

extension WalletBalanceFutureStackView {
    func setCurrency(_ currency: String?) {
        futureCurrency.text = currency
    }
    
    func setAlpha(labelAlpha: CGFloat, lineAlpha: CGFloat) {
        futureValue.alpha = labelAlpha
        futureLine.alpha = lineAlpha
    }
    
    func setValue(_ value: String) {
        futureValue.text = value
    }
}

// MARK: - ViewConfiguration

extension WalletBalanceFutureStackView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(futureLine)
        addArrangedSubview(futureLabel)
        addArrangedSubview(futureCurrency)
        addArrangedSubview(futureValue)
    }
    
    func setupConstraints() {
        futureLine.snp.makeConstraints {
            $0.leading.trailing.centerY.equalTo(futureValue)
            $0.height.equalTo(1)
        }
    }
}
