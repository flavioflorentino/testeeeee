import SnapKit
import UI
import UIKit

extension WalletBalanceErrorView.Layout {
    enum IconLabel {
        static let size = CGSize(width: 20, height: 20)
    }
}

final class WalletBalanceErrorView: UIView {
    private typealias Localizable = Strings.WalletBalance
    fileprivate enum Layout { }
    
    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label.text = Iconography.exclamationCircle.rawValue
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, Colors.notification600.color)
        
        return label
    }()
    
    private lazy var titleSubtitleStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
        stackView.axis = .vertical
        stackView.alignment = .leading
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.ErrorView.title
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.numberOfLines, 1)
            .with(\.textColor, Colors.black.color)
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.ErrorView.subtitle
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.numberOfLines, 1)
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.adjustsFontSizeToFitWidth, true)
        return label
    }()
    
    private lazy var tryAgainButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.ErrorView.buttonTitle, for: .normal)
        button
            .buttonStyle(LinkButtonStyle(size: .small, icon: (name: .sync, alignment: .right)))
        button.titleLabel?.font = Typography.bodySecondary().font()
        button.addTarget(self, action: #selector(tryAgainTouched), for: .touchUpInside)
        return button
    }()
    
    var tryAgainAction: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension WalletBalanceErrorView: ViewConfiguration {
    func configureViews() {
        backgroundColor = Colors.white.color
    }
    
    func buildViewHierarchy() {
        addSubview(iconLabel)
        addSubview(titleSubtitleStackView)
        addSubview(tryAgainButton)
    }
    
    func setupConstraints() {
        iconLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.top.equalToSuperview().offset(Spacing.base03)
        }
        
        titleSubtitleStackView.snp.makeConstraints {
            $0.top.equalTo(iconLabel)
            $0.leading.equalTo(iconLabel.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        tryAgainButton.snp.makeConstraints {
            $0.leading.equalTo(titleSubtitleStackView)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
        }
    }
}

@objc
private extension WalletBalanceErrorView {
    func tryAgainTouched() {
        tryAgainAction?()
    }
}
