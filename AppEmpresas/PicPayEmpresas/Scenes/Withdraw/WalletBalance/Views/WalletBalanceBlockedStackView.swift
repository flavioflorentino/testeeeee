import SnapKit
import UI
import UIKit

final class WalletBalanceBlockedStackView: UIStackView {
    private lazy var blockedWithdrawLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        label.text = Strings.WalletBalance.blockedWithdraw
        label.numberOfLines = 1
        return label
    }()

    private lazy var blockedWithdrawCurrency: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .notification600())
        label.numberOfLines = 1
        return label
    }()

    private lazy var blockedWithdrawValue: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .notification600())
        label.numberOfLines = 1
        return label
    }()

    private lazy var blockedWithdrawLine: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.notification600.lightColor
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        axis = .horizontal
        distribution = .fill
        alignment = .center
        spacing = Spacing.base00
        buildLayout()
    }
    
    @available(*, unavailable)
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Internal Methods

extension WalletBalanceBlockedStackView {
    func setCurrency(_ currency: String?) {
        blockedWithdrawCurrency.text = currency
    }
    
    func setAlpha(labelAlpha: CGFloat, lineAlpha: CGFloat) {
        blockedWithdrawValue.alpha = labelAlpha
        blockedWithdrawLine.alpha = lineAlpha
    }
    
    func setValue(_ value: String) {
        blockedWithdrawValue.text = value
    }
}

// MARK: - ViewConfiguration

extension WalletBalanceBlockedStackView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(blockedWithdrawLine)
        addArrangedSubview(blockedWithdrawLabel)
        addArrangedSubview(blockedWithdrawCurrency)
        addArrangedSubview(blockedWithdrawValue)
    }
    
    func setupConstraints() {
        blockedWithdrawLine.snp.makeConstraints {
            $0.leading.trailing.centerY.equalTo(blockedWithdrawValue)
            $0.height.equalTo(1)
        }
    }
}
