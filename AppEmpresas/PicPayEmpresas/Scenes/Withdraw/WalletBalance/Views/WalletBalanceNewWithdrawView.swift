import SnapKit
import UI
import UIKit

final class WalletBalanceNewWithdrawView: UIView {
    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label.text = Iconography.moneyInsert.rawValue
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, disabledColor)
        return label
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.WalletBalance.withdrawButtonTitle
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, disabledColor)
            .with(\.numberOfLines, 1)
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var arrowLabel: UILabel = {
        let label = UILabel()
        label.text = Iconography.angleRightB.rawValue
        label
            .labelStyle(IconLabelStyle(type: .large))
            .with(\.textColor, disabledColor)
        return label
    }()
    
    private let disabledColor = Colors.grayscale500.color
    private let enabledColor = Colors.brandingBase.color
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration

extension WalletBalanceNewWithdrawView: ViewConfiguration {
    func configureViews() {
        backgroundColor = .clear
        layer.cornerRadius = CornerRadius.light
        clipsToBounds = true
    }
    
    func buildViewHierarchy() {
        addSubview(iconLabel)
        addSubview(titleLabel)
        addSubview(arrowLabel)
    }
    
    func setupConstraints() {
        iconLabel.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview().inset(Spacing.base02)
        }
        
        titleLabel.snp.makeConstraints {
            $0.leading.equalTo(iconLabel.snp.trailing).offset(Spacing.base02)
            $0.centerY.equalTo(iconLabel)
        }
        
        arrowLabel.snp.makeConstraints {
            $0.trailing.equalToSuperview().offset(-Spacing.base01)
            $0.centerY.equalTo(iconLabel)
        }
    }
}

extension WalletBalanceNewWithdrawView {
    func setEnabled(isEnabled: Bool) {
        let color = isEnabled ? enabledColor : disabledColor
        iconLabel.textColor = color
        titleLabel.textColor = color
        arrowLabel.textColor = color
        isUserInteractionEnabled = isEnabled
    }
}
