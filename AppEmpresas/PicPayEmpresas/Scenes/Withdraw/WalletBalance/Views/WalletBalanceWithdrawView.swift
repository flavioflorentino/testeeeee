import SnapKit
import UI
import UIKit

extension WalletBalanceWithdrawView.Layout {
    enum VisibilityLine {
        static let height: CGFloat = 2
    }
    
    enum VisibilityImage {
        static let size = CGSize(width: 15, height: 12)
    }
}

final class WalletBalanceWithdrawView: UIView {
    fileprivate enum Layout { }
    
    var visibilityTapGesture: UITapGestureRecognizer? {
        didSet {
            guard let tapGesture = visibilityTapGesture else {
                return
            }
            visibilityImage.addGestureRecognizer(tapGesture)
        }
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.WalletBalance.availableAmount
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .black())
        label.numberOfLines = 1
        return label
    }()

    private lazy var visibilityImage: UIImageView = {
        let imageView = UIImageView(image: Assets.visibilityOn.image)
        imageView.isUserInteractionEnabled = true
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var withdrawCurrency: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .black())
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        label.numberOfLines = 1
        return label
    }()

    private lazy var withdrawValue: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .black())
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 1
        return label
    }()

    private lazy var withdrawLine: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale050.lightColor
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration

extension WalletBalanceWithdrawView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(titleLabel)
        addSubview(visibilityImage)
        addSubview(withdrawCurrency)
        addSubview(withdrawValue)
        addSubview(withdrawLine)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.leading.equalToSuperview()
        }

        visibilityImage.snp.makeConstraints {
            $0.leading.equalTo(titleLabel.snp.trailing).offset(Spacing.base01)
            $0.centerY.equalTo(titleLabel)
            $0.size.equalTo(Layout.VisibilityImage.size)
        }

        withdrawCurrency.snp.makeConstraints {
            $0.leading.bottom.equalToSuperview()
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base00)
        }

        withdrawValue.snp.makeConstraints {
            $0.leading.equalTo(withdrawCurrency.snp.trailing).offset(Spacing.base00)
            $0.centerY.equalTo(withdrawCurrency)
        }

        withdrawLine.snp.makeConstraints {
            $0.leading.trailing.centerY.equalTo(withdrawValue)
            $0.height.equalTo(Layout.VisibilityLine.height)
        }
    }
}

// MARK: - Internal Methods

extension WalletBalanceWithdrawView {
    func setCurrency(_ currency: String?) {
        withdrawCurrency.text = currency
    }
    
    func setVisibility(isHidden: Bool) {
        visibilityImage.image = isHidden ? Assets.visibilityOff.image : Assets.visibilityOn.image
    }
    
    func setAlpha(labelAlpha: CGFloat, lineAlpha: CGFloat) {
        withdrawValue.alpha = labelAlpha
        withdrawLine.alpha = lineAlpha
    }
    
    func setValue(_ value: String) {
        withdrawValue.text = value
    }
}
