# Wallet Balance Component
Componente para apresentação do saldo disponível para saque.

## Requisitos
- iOS 10.3 or later
- Xcode 10.0 or later

## Como usar

```swift

let walletBalance = WalletBalanceFactory.make()
addChild(walletBalance)
view.addSubview(walletBalance.view)
view.sendSubviewToBack(walletBalance.view)
walletBalance.didMove(toParent: self)
walletBalance.view.translatesAutoresizingMaskIntoConstraints = false
NSLayoutConstraint.leadingTrailing(equalTo: view, for: [walletBalance.view])
walletBalance.view.topAnchor.constraint(equalTo: view.topAnchor, constant: -131).isActive = true
walletBalance.view.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
walletBalance.view.heightAnchor.constraint(equalToConstant: 131).isActive = true

```

Para utilizar no topo de uma tableView, ajustar o content inset conforme abaixo:
```swift 

tableView.contentInset = UIEdgeInsets(top: 131, left: 0, bottom: 0, right: 0)

``` 

## License
WalletBalance é liberado para uso interno da [PicPay](https://www.picpay.com/site)

