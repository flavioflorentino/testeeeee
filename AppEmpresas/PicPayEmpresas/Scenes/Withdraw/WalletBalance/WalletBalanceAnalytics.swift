import AnalyticsModule

enum WalletBalanceAnalytics: AnalyticsKeyProtocol {
    case withdrawIntention(sellerId: Int)
    case showBalanceStatus(_ balance: String, sellerId: Int, action: Bool)
    
    enum BalanceStatus: String {
        case show, hide
    }
    
    private var name: String {
        switch self {
        case .withdrawIntention:
            return "withdraw_intention"
        case .showBalanceStatus:
            return "Account Balance - Show Values Status"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .withdrawIntention(sellerId):
            return ["user_id": "\(sellerId)"]
        case let .showBalanceStatus(balance, sellerId, action):
            return propertiesFor(balance, sellerId, action)
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
    
    private func propertiesFor(_ balance: String, _ sellerId: Int, _ action: Bool) -> [String: String] {
        var properties: [String: String] = [:]
        properties["seller_id"] = "\(sellerId)"
        properties["value"] = balance
        let status: BalanceStatus = action ? .hide : .show
        properties["action"] = status.rawValue
        return properties
    }
}
