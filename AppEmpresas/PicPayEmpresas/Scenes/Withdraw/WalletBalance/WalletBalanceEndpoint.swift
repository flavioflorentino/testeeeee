import Core

enum WalletBalanceEndpoint {
    case getBalance
}

extension WalletBalanceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .getBalance:
            return "/seller/wallet-balance"
        }
    }
    
    var method: HTTPMethod {
        .get
    }
}
