import Foundation

protocol WalletBalancePresenting: AnyObject {
    var viewController: WalletBalanceDisplay? { get set }
    func didNextStep(action: WalletBalanceAction)
    func presentBalance(_ balance: WalletBalanceItem)
    func presentCurrency(_ symbol: String?)
    func withdrawValues(isHidden: Bool, animated: Bool)
    func showError()
}

final class WalletBalancePresenter: WalletBalancePresenting {
    private let coordinator: WalletBalanceCoordinating
    weak var viewController: WalletBalanceDisplay?

    init(coordinator: WalletBalanceCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: WalletBalanceAction) {
        coordinator.perform(action: action)
    }
    
    func presentBalance(_ balance: WalletBalanceItem) {
        let available = balance.available.currency(hideSymbol: true)
        let withhed = balance.withheld.currency(hideSymbol: true)
        viewController?.display(available, withheld: withhed)
        
        if let blockedBalance = balance.blocked?.currency(hideSymbol: true) {
            viewController?.displayBlockedBalance(blockedBalance)
        }

        if let number = balance.available.bindToNumber() {
            viewController?.newWithdrawalView(enabled: number.floatValue > 0)
        }
    }

    func presentCurrency(_ symbol: String?) {
        viewController?.setupCurrency(symbol)
    }
  
    func withdrawValues(isHidden: Bool, animated: Bool) {
        viewController?.setupVisibility(isHidden, animated: animated)
    }
    
    func showError() {
        viewController?.displayErrorView()
        viewController?.newWithdrawalView(enabled: false)
    }
}
