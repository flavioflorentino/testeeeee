import UIKit

enum BizErrorViewAction {
    case buttonAction
    case closeAction
}

protocol BizErrorViewCoordinating: AnyObject {
    var errorCoordinatorDelegate: BizErrorViewCoordinatorDelegate? { get set }
    func perform(action: BizErrorViewAction)
}

final class BizErrorViewCoordinator: BizErrorViewCoordinating {
    weak var errorCoordinatorDelegate: BizErrorViewCoordinatorDelegate?
    
    func perform(action: BizErrorViewAction) {
        switch action {
        case .buttonAction:
            errorCoordinatorDelegate?.buttonAction()
        case .closeAction:
            errorCoordinatorDelegate?.closeAction()
        }
    }
}
