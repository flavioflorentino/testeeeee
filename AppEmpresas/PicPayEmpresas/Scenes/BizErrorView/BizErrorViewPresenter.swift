import Foundation

protocol BizErrorViewPresenting: AnyObject {
    var viewController: BizErrorViewDisplay? { get set }
    func setupView()
    func didNextStep(action: BizErrorViewAction)
}

final class BizErrorViewPresenter {
    private let coordinator: BizErrorViewCoordinating
    private let errorView: BizErrorViewLayout
    weak var viewController: BizErrorViewDisplay?

    init(coordinator: BizErrorViewCoordinating, errorView: BizErrorViewLayout) {
        self.coordinator = coordinator
        self.errorView = errorView
    }
}

// MARK: - BizErrorViewPresenting
extension BizErrorViewPresenter: BizErrorViewPresenting {
    func setupView() {
        viewController?.setTitle(errorView.title)
        viewController?.setMessage(errorView.message)
        viewController?.setImage(errorView.image)
        viewController?.setButtonTitle(errorView.buttonTitle)
    }
    
    func didNextStep(action: BizErrorViewAction) {
        coordinator.perform(action: action)
    }
}
