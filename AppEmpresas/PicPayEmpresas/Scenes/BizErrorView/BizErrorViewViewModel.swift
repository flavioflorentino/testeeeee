import Foundation

protocol BizErrorViewViewModelInputs: AnyObject {
    func setupView()
    func closeScreen()
    func buttonAction()
}

final class BizErrorViewViewModel {
    private let presenter: BizErrorViewPresenting

    init(presenter: BizErrorViewPresenting) {
        self.presenter = presenter
    }
}

// MARK: - BizErrorViewViewModelInputs
extension BizErrorViewViewModel: BizErrorViewViewModelInputs {
    func setupView() {
        presenter.setupView()
    }
    
    func closeScreen() {
        presenter.didNextStep(action: .closeAction)
    }
    
    func buttonAction() {
        presenter.didNextStep(action: .buttonAction)
    }
}
