import Foundation
import UIKit

struct BizErrorViewLayout: Equatable {
    var title: String
    var message: String
    var image: UIImage
    var buttonTitle: String = Strings.ErrorView.ok
    
    static func == (lhs: BizErrorViewLayout, rhs: BizErrorViewLayout) -> Bool {
        lhs.title == rhs.title &&
        lhs.message == rhs.message &&
        lhs.image.isEqual(rhs.image) &&
        lhs.buttonTitle == rhs.buttonTitle
    }
}
