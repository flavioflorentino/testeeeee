import Foundation

protocol BizErrorViewCoordinatorDelegate: AnyObject {
    func buttonAction()
    func closeAction()
}

enum BizErrorViewFactory {
    static func make(errorView: BizErrorViewLayout, errorCoordinatorDelegate: BizErrorViewCoordinatorDelegate?) -> BizErrorViewViewController {
        let coordinator: BizErrorViewCoordinating = BizErrorViewCoordinator()
        let presenter: BizErrorViewPresenting = BizErrorViewPresenter(coordinator: coordinator, errorView: errorView)
        let viewModel = BizErrorViewViewModel(presenter: presenter)
        let viewController = BizErrorViewViewController(viewModel: viewModel)
        
        coordinator.errorCoordinatorDelegate = errorCoordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
