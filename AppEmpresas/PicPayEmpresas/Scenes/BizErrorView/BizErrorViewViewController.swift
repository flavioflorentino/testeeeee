import UI
import UIKit

protocol BizErrorViewDisplay: AnyObject {
    func setTitle(_ title: String?)
    func setMessage(_ message: String?)
    func setImage(_ image: UIImage?)
    func setButtonTitle(_ buttonTitle: String?)
}

private extension BizErrorViewViewController.Layout {
    enum ImageView {
        static let size = CGSize(width: 72, height: 72)
    }
    
    enum CloseButton {
        static let size = CGSize(width: 32, height: 32)
    }
}

final class BizErrorViewViewController: ViewController<BizErrorViewViewModelInputs, UIView> {
    private typealias Localizable = Strings.ErrorView
    
    fileprivate enum Layout { }
    
    // MARK: - Private Properties
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setBackgroundImage(Assets.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var imageView = UIImageView(image: Assets.Emoji.iconBad.image)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        label.text = Localizable.anErrorOcurred
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        label.text = Localizable.weReSorry
        return label
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTouchConfirmButton), for: .touchUpInside)
        button.setTitle(Localizable.ok, for: .normal)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.setupView()
    }
    
    override func setupConstraints() {
        contentView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.bottom.equalToSuperview().offset(-Spacing.base02)
        }
        
        closeButton.snp.makeConstraints {
            $0.leading.top.equalTo(contentView).offset(Spacing.base02)
            $0.size.equalTo(Layout.CloseButton.size)
        }
        
        imageView.snp.makeConstraints {
            $0.height.equalTo(Layout.ImageView.size.height)
            $0.width.equalTo(Layout.ImageView.size.width)
            $0.bottom.equalTo(titleLabel.snp.top).offset(-Spacing.base04)
            $0.centerX.equalTo(contentView)
        }
        
        titleLabel.snp.makeConstraints {
            $0.leading.equalTo(contentView).offset(Spacing.base02)
            $0.trailing.equalTo(contentView).offset(-Spacing.base02)
            $0.bottom.equalTo(messageLabel.snp.top).offset(-Spacing.base02)
        }
        
        messageLabel.snp.makeConstraints {
            $0.top.equalTo(view.snp.centerY)
            $0.leading.equalTo(contentView).offset(Spacing.base02)
            $0.trailing.equalTo(contentView).offset(-Spacing.base02)
            $0.bottom.lessThanOrEqualTo(confirmButton).offset(-Spacing.base02)
        }
        
        confirmButton.snp.makeConstraints {
            $0.leading.equalTo(contentView).offset(Spacing.base02)
            $0.trailing.equalTo(contentView).offset(-Spacing.base02)
            $0.bottom.equalTo(contentView).offset(-Spacing.base03)
        }
    }
    
    override func buildViewHierarchy() {
        view.addSubview(contentView)
        contentView.addSubviews(closeButton, imageView, titleLabel, messageLabel, confirmButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: BizErrorViewDisplay
extension BizErrorViewViewController: BizErrorViewDisplay {
    func setTitle(_ title: String?) {
        titleLabel.text = title
    }
    
    func setMessage(_ message: String?) {
        messageLabel.text = message
    }
    
    func setImage(_ image: UIImage?) {
        imageView.image = image
    }
    
    func setButtonTitle(_ buttonTitle: String?) {
        confirmButton.setTitle(buttonTitle, for: .normal)
    }
}

@objc extension BizErrorViewViewController {
    func didTouchConfirmButton() {
        viewModel.buttonAction()
    }
    
    func didTapCloseButton() {
        viewModel.closeScreen()
    }
}
