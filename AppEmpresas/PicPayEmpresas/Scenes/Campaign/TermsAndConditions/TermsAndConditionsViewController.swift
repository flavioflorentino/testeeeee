import UI
import UIKit
import WebKit

protocol TermsAndConditionsDisplay: AnyObject, LoadingViewProtocol {
    func displayWebPage(_ request: URLRequest)
}

final class TermsAndConditionsViewController: ViewController<TermsAndConditionsInteracting, UIView> {
    private typealias Localizable = Strings.CampaignTermsAndConditions
    fileprivate enum Layout { }
    
    private lazy var webView: WKWebView = {
        let webView = WKWebView()
        webView.isMultipleTouchEnabled = true
        webView.navigationDelegate = self
        return webView
    }()
    
    lazy var loadingView = LoadingView()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadWebPage()
    }

     override func buildViewHierarchy() {
        view.addSubview(webView)
    }
    
     override func setupConstraints() {
        webView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

     override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        title = Localizable.viewTitle
    }
}

// MARK: TermsAndConditionsDisplay
extension TermsAndConditionsViewController: TermsAndConditionsDisplay {
    func displayWebPage(_ request: URLRequest) {
        webView.load(request)
    }
}

extension TermsAndConditionsViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation?) {
        interactor.webViewDidFinishLoading(webView)
    }

    func webView(
        _ webView: WKWebView,
        didReceive challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    ) {
        interactor.webViewDidReceiveChallenge(challenge, completionHandler: completionHandler)
    }
}
