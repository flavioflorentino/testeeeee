import Foundation

protocol TermsAndConditionsPresenting: AnyObject {
    var viewController: TermsAndConditionsDisplay? { get set }
    func loadWebPage(for request: URLRequest)
    func webPageFinishedLoading()
}

final class TermsAndConditionsPresenter {
    weak var viewController: TermsAndConditionsDisplay?
}

// MARK: - TermsAndConditionsPresenting
extension TermsAndConditionsPresenter: TermsAndConditionsPresenting {
    func loadWebPage(for request: URLRequest) {
        viewController?.startLoadingView()
        viewController?.displayWebPage(request)
    }
    
    func webPageFinishedLoading() {
        viewController?.stopLoadingView()
    }
}
