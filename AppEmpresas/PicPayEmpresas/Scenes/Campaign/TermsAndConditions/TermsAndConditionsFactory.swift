import Foundation
import UIKit

enum TermsAndConditionsFactory {
    static func make() -> TermsAndConditionsViewController {
        let presenter: TermsAndConditionsPresenting = TermsAndConditionsPresenter()
        let interactor = TermsAndConditionsInteractor(presenter: presenter)
        let viewController = TermsAndConditionsViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
