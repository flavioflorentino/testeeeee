import Core
import LegacyPJ
import WebKit
import Foundation

protocol TermsAndConditionsInteracting: AnyObject {
    func loadWebPage()
    func webViewDidFinishLoading(_ webView: WKWebView)
    func webViewDidReceiveChallenge(
        _ challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    )
}

final class TermsAndConditionsInteractor {
    private let presenter: TermsAndConditionsPresenting

    init(presenter: TermsAndConditionsPresenting) {
        self.presenter = presenter
    }
}

// MARK: - TermsAndConditionsInteracting
extension TermsAndConditionsInteractor: TermsAndConditionsInteracting {
    func loadWebPage() {
        guard
            let termsUrl = URL(string: Endpoint.campaignUrlTermsAndConditions())
            else {
                return
        }
        presenter.loadWebPage(for: URLRequest(url: termsUrl))
    }

    func webViewDidFinishLoading(_ webView: WKWebView) {
        presenter.webPageFinishedLoading()
    }
    
    func webViewDidReceiveChallenge(
        _ challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    ) {
        let pinningHandler = PinningHandler()
        pinningHandler.handle(challenge: challenge, completionHandler: completionHandler)
    }
}
