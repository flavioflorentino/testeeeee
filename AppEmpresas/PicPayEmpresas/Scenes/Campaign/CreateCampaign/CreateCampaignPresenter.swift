import Foundation

protocol CreateCampaignPresenting: AnyObject {
    var viewController: CreateCampaignDisplay? { get set }
    func presentDurationOptions()
    func didNextStep(action: CreateCampaignAction)
    func enableContinueButton(_ isEnabled: Bool)
    func presentPercentageError()
    func presentMaxValueError()
    func presentPercentageItemDefaultState()
    func presentMaxValueItemDefaultState()
}

final class CreateCampaignPresenter {
    private let coordinator: CreateCampaignCoordinating
    weak var viewController: CreateCampaignDisplay?

    init(coordinator: CreateCampaignCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - CreateCampaignPresenting

extension CreateCampaignPresenter: CreateCampaignPresenting {
    func presentDurationOptions() {
        let options = [
            DurationOptions(
                rowTitle: Strings.CreateCampaign.sevenDays,
                rowValue: Strings.CreateCampaign.sevenDays),
            DurationOptions(
                rowTitle: Strings.CreateCampaign.fifteenDays,
                rowValue: Strings.CreateCampaign.fifteenDays),
            DurationOptions(
                rowTitle: Strings.CreateCampaign.thirtyDays,
                rowValue: Strings.CreateCampaign.thirtyDays)
        ]
        
        viewController?.setDurationOptions(options)
    }
    
    func didNextStep(action: CreateCampaignAction) {
        coordinator.perform(action: action)
    }
    
    func enableContinueButton(_ isEnabled: Bool) {
        viewController?.enableContinueButton(isEnabled)
    }
    
    func presentPercentageError() {
        viewController?.showPercentageError()
    }
    
    func presentMaxValueError() {
        viewController?.showMaxValueError()
    }
    
    func presentPercentageItemDefaultState() {
        viewController?.setPercentageItemDefaultState()
    }
    
    func presentMaxValueItemDefaultState() {
        viewController?.setMaxValueItemDefaultState()
    }
}
