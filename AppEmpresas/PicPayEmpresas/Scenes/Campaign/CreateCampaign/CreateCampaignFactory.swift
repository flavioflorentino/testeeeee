import Foundation

enum CreateCampaignFactory {
    static func make(_ type: CampaignType) -> CreateCampaignViewController {
        let coordinator: CreateCampaignCoordinating = CreateCampaignCoordinator()
        let presenter: CreateCampaignPresenting = CreateCampaignPresenter(coordinator: coordinator)
        let interactor = CreateCampaignInteractor(presenter: presenter, campaignType: type)
        let viewController = CreateCampaignViewController(viewModel: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
