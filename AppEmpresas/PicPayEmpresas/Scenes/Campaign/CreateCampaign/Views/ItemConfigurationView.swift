import SnapKit
import UI
import UIKit

enum ItemFieldType {
    case percentage
    case currency
}

private extension ItemConfigurationView.Layout {
    enum TextField {
        static let size = CGSize(width: 100, height: 50)
        static let insets = UIEdgeInsets(
            top: 0,
            left: Spacing.base01,
            bottom: 0,
            right: Spacing.base04
        )
    }
    
    enum ErrorIcon {
        static let size = CGSize(width: 20, height: 20)
    }
}

protocol ItemConfigurationViewDelegate: AnyObject {
    func textDidChanged()
}

final class ItemConfigurationView: UIView {
    fileprivate enum Layout {}
    
    var textChanged: String?
    private var fieldType: ItemFieldType = .percentage
    weak var delegate: ItemConfigurationViewDelegate?
    
    private lazy var itemTitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .grayscale950())
        
        return label
    }()
    
    private lazy var itemField: TextFieldWithPadding = {
        let field = TextFieldWithPadding()
        field.keyboardType = .numberPad
        field.viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.border, .light(color: .grayscale200()))
        field.addPadding(Layout.TextField.insets)
        field.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        
        if fieldType == .percentage {
            field.delegate = self
        }
        
        return field
    }()
    
    private lazy var errorIconImage: UIImageView = {
        let image = UIImageView(image: Assets.errorIcon.image)
        image.isHidden = true
        return image
    }()
    
    private lazy var errorMessageLabel: UILabel = {
        let label = UILabel()
        label.isHidden = true
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .critical600())
        return label
    }()
    
    private lazy var itemDescriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale500())
        
        return label
    }()
    
    // MARK: - Inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(fieldType: ItemFieldType) {
        self.init()
        self.fieldType = fieldType
    }
    
    // MARK: - Public Methods
    
    func setContent(title: String, description: String, placeholder: String) {
        itemTitleLabel.text = title
        itemDescriptionLabel.text = description
        itemField.placeholder = placeholder
    }
    
    func setTextFieldDelegate(_ delegate: ItemConfigurationViewDelegate) {
        self.delegate = delegate
    }
    
    @objc
    func textFieldDidChanged(_ textField: UITextField) {
        switch fieldType {
        case .currency:
            let amountString = textField.text?.currencyInputFormatting()
            textField.text = amountString
            
        case .percentage:
            let amountString = textField.text?.rateInputFormatting()
            textField.text = amountString
        }
        
        textChanged = textField.text
        delegate?.textDidChanged()
    }
}

extension ItemConfigurationView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(itemTitleLabel)
        addSubview(itemField)
        addSubview(errorIconImage)
        addSubview(errorMessageLabel)
        addSubview(itemDescriptionLabel)
    }
    
    func setupConstraints() {
        itemTitleLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        itemField.snp.makeConstraints {
            $0.top.equalTo(itemTitleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.lessThanOrEqualToSuperview().inset(Spacing.base02)
            $0.width.greaterThanOrEqualTo(Layout.TextField.size.width)
            $0.height.equalTo(Layout.TextField.size.height)
        }
        
        errorIconImage.snp.makeConstraints {
            $0.centerY.equalTo(itemField)
            $0.trailing.equalTo(itemField).inset(Spacing.base01)
            $0.size.equalTo(Layout.ErrorIcon.size)
        }
        
        errorMessageLabel.snp.makeConstraints {
            $0.top.equalTo(itemField.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(itemField).offset(Spacing.base02)
        }
        
        itemDescriptionLabel.snp.makeConstraints {
            $0.top.equalTo(itemField.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
}

extension ItemConfigurationView {
    func showError(message: String) {
        errorIconImage.isHidden = false
        errorMessageLabel.isHidden = false
        errorMessageLabel.text = message
        
        itemField
            .viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.border, .light(color: .critical600()))
        
        itemDescriptionLabel.snp.remakeConstraints {
            $0.top.equalTo(itemField.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func setDefaultState() {
        errorIconImage.isHidden = true
        errorMessageLabel.isHidden = true
        errorMessageLabel.text = ""
        
        itemField
            .viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.border, .light(color: .grayscale200()))
        
        itemDescriptionLabel.snp.remakeConstraints {
            $0.top.equalTo(itemField.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
}

extension ItemConfigurationView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if fieldType == .percentage {
            handlePercentBackSpace(textField, replacementString: string)
        }
        
        delegate?.textDidChanged()
        return true
    }
    
    private func handlePercentBackSpace(_ textField: UITextField, replacementString string: String) {
        if string.isBackspace {
            let text = textField.text ?? ""
            let textDouble = Double(text.replacingOccurrences(of: "%", with: "")) ?? 0.0
            let doubleAdjusted = textDouble / 10.0
            
            let formatter = NumberFormatter()
            formatter.roundingMode = .down
            
            let num = NSNumber(value: doubleAdjusted)
            let newText = formatter.string(from: num) ?? ""
            textChanged = newText
            textField.text = "\(newText)%"
        }
        
        delegate?.textDidChanged()
    }
}
