import UI

protocol CreateCampaignDisplay: AnyObject {
    func setDurationOptions(_ options: [DurationOptions])
    func enableContinueButton(_ isEnable: Bool)
    func showPercentageError()
    func showMaxValueError()
    func setPercentageItemDefaultState()
    func setMaxValueItemDefaultState()
}

private extension CreateCampaignViewController.Layout {
    enum TextField {
        static let height: CGFloat = 50.0
        static let insets = UIEdgeInsets(
            top: 0,
            left: Spacing.base01,
            bottom: 0,
            right: 0
        )
    }
    
    enum Arrow {
        static let size = CGSize(width: 12, height: 6)
    }
}

final class CreateCampaignViewController: ViewController<CreateCampaignInteractorInputs, UIView> {
    private typealias Localizable = Strings.CreateCampaign
    fileprivate enum Layout { }
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private lazy var percentageItemView: ItemConfigurationView = {
        let view = ItemConfigurationView(fieldType: .percentage)
        view.setContent(
            title: Localizable.cashbackPercentage,
            description: Localizable.cashbackPercentageDescription,
            placeholder: "%")
        view.setTextFieldDelegate(self)
        return view
    }()
    
    private lazy var valueItemView: ItemConfigurationView = {
        let view = ItemConfigurationView(fieldType: .currency)
        view.setContent(
            title: Localizable.maxValue,
            description: Localizable.maxValueDescription,
            placeholder: "R$")
        view.setTextFieldDelegate(self)
        return view
    }()
    
    private lazy var durationLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.campaignDuration
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .grayscale950())
        
        return label
    }()
    
    private lazy var durationField: TextFieldWithPadding = {
        let field = TextFieldWithPadding()
        field.inputView = durationInputView
        field.placeholder = Localizable.select
        field.inputAccessoryView = formToolbar
        field.addPadding(Layout.TextField.insets)
        field
            .viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.border, .light(color: .grayscale200()))
        
        field.addTarget(self, action: #selector(durationTextFieldChanged), for: .editingDidEnd)
        
        return field
    }()
    
    private let arrow = UIImageView(image: Assets.filledDownArrow.image)
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.isEnabled = false
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.continueButtonTitle, for: .normal)
        button.addTarget(self, action: #selector(continueButtonPressed), for: .touchUpInside)
        
        return button
    }()
    
    private let durationInputView = PickerInputView()
    
    private let formToolbar = DoneToolBar(doneText: Localizable.ok)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.obtainCampaignDurationOptions()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(contentView)
        contentView.addSubview(percentageItemView)
        contentView.addSubview(valueItemView)
        contentView.addSubview(durationLabel)
        contentView.addSubview(durationField)
        
        durationField.addSubview(arrow)
        
        contentView.addSubview(continueButton)
    }
    
    override func setupConstraints() {
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        percentageItemView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
        
        valueItemView.snp.makeConstraints {
            $0.top.equalTo(percentageItemView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
        }
        
        durationLabel.snp.makeConstraints {
            $0.top.equalTo(valueItemView.snp.bottom)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        durationField.snp.makeConstraints {
            $0.top.equalTo(durationLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.TextField.height)
        }
        
        arrow.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        continueButton.snp.makeConstraints {
            $0.top.equalTo(durationField.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalTo(durationField)
        }
    }
    
    override func configureViews() {
        addGestures()
        view.backgroundColor = Colors.backgroundPrimary.color
        title = Localizable.viewTitle
    }
    
    private func addGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        tapGesture.delaysTouchesBegan = false
        tapGesture.delaysTouchesEnded = false
        view.addGestureRecognizer(tapGesture)
    }
}

// MARK: Objc exposed methods

@objc
private extension CreateCampaignViewController {
    func continueButtonPressed() {
        viewModel.continueToCreateCampaign(
            percentage: percentageItemView.textChanged ?? "",
            maxValue: valueItemView.textChanged ?? "",
            duration: durationField.text ?? "")
    }
}

// MARK: CreateCampaignDisplay

extension CreateCampaignViewController: CreateCampaignDisplay {
    func enableContinueButton(_ isEnabled: Bool) {
        continueButton.isEnabled = isEnabled
    }
    
    func setDurationOptions(_ options: [DurationOptions]) {
        durationInputView.options = options
        durationInputView.inputInterfaceElement = durationField
    }
    
    func showMaxValueError() {
        valueItemView.showError(message: Localizable.maxValueErrorMessage)
    }
    
    func showPercentageError() {
        percentageItemView.showError(message: Localizable.percentageLimitErrorMessage)
    }
    
    func setPercentageItemDefaultState() {
        percentageItemView.setDefaultState()
    }
    
    func setMaxValueItemDefaultState() {
        valueItemView.setDefaultState()
    }
}

@objc
private extension CreateCampaignViewController {
    func durationTextFieldChanged() {
        textDidChanged()
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension CreateCampaignViewController: ItemConfigurationViewDelegate {
    func textDidChanged() {
        viewModel.verifyFieldsContent(
            percentage: percentageItemView.textChanged ?? "",
            maxValue: valueItemView.textChanged ?? "",
            duration: durationField.text ?? "")
    }
}
