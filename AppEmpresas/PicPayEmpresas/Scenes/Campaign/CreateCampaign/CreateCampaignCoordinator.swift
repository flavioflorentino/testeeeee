import UIKit

enum CreateCampaignAction {
    case confirmCampaign(_ campaign: Campaign)
}

protocol CreateCampaignCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CreateCampaignAction)
}

final class CreateCampaignCoordinator: CreateCampaignCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: CreateCampaignAction) {
        guard case .confirmCampaign(let campaign) = action else {
            return
        }
        
        let controller = ConfirmCampaignFactory.make(campaign)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
