import UI
import Foundation

struct DurationOptions: PickerOption {
    var rowTitle: String?
    var rowValue: String?
}

protocol CreateCampaignInteractorInputs: AnyObject {
    func obtainCampaignDurationOptions()
    func verifyFieldsContent(percentage: String, maxValue: String, duration: String)
    func continueToCreateCampaign(percentage: String, maxValue: String, duration: String)
}

final class CreateCampaignInteractor {
    private let maxAllowedPercentage = 40
    private let maxAllowedValue = 50_000.0
    private let presenter: CreateCampaignPresenting
    private let campaignType: CampaignType
    
    init(presenter: CreateCampaignPresenting, campaignType: CampaignType) {
        self.presenter = presenter
        self.campaignType = campaignType
    }
}

// MARK: - CreateCampaignViewModelInputs
extension CreateCampaignInteractor: CreateCampaignInteractorInputs {
    func verifyFieldsContent(percentage: String, maxValue: String, duration: String) {
        let isUnderLimits = verifyLimitValues(
            percentage: Int(percentage.onlyNumbers) ?? 0,
            maxValue: Double(maxValue.onlyNumbers) ?? 0.0)
        
        presenter.enableContinueButton(!percentage.isEmpty && !maxValue.isEmpty && !duration.isEmpty && isUnderLimits)
    }
    
    func obtainCampaignDurationOptions() {
        presenter.presentDurationOptions()
    }
    
    func continueToCreateCampaign(percentage: String, maxValue: String, duration: String) {
        let campaign = Campaign(
            type: campaignType,
            cashback: percentage,
            maxValue: maxValue,
            expirationDate: duration)
        
        presenter.didNextStep(action: .confirmCampaign(campaign))
    }
    
    private func verifyLimitValues(percentage: Int, maxValue: Double) -> Bool {
        var isUnderLimits = true
        
        if percentage > maxAllowedPercentage {
            presenter.presentPercentageError()
            isUnderLimits = false
        } else {
            presenter.presentPercentageItemDefaultState()
        }
        
        if maxValue > maxAllowedValue {
            presenter.presentMaxValueError()
            isUnderLimits = false
        } else {
            presenter.presentMaxValueItemDefaultState()
        }
        
        return isUnderLimits
    }
}
