import Foundation

protocol CampaignHowWorksInteractorInputs: AnyObject {
    func createCampaign()
    func getFrequentQuestions()
}

final class CampaignHowWorksInteractor {
    private typealias Localizable = Strings.CampaignHowWorks
    private let presenter: CampaignHowWorksPresenting
    
    init(presenter: CampaignHowWorksPresenting) {
        self.presenter = presenter
    }
}

// MARK: - CampaignHowWorksViewModelInputs
extension CampaignHowWorksInteractor: CampaignHowWorksInteractorInputs {
    func getFrequentQuestions() {
        presenter.presentFrequentQuestions([
            FrequentQuestionsModel(
                title: Localizable.whatIsCashbackTitle,
                description: Localizable.whatIsCashbackDescription),
            
            FrequentQuestionsModel(
                title: Localizable.whoPaysForTheCostTitle,
                description: Localizable.whoPaysForTheCostDescription),
            
            FrequentQuestionsModel(
                title: Localizable.howTheCostIsPayedTitle,
                description: Localizable.howTheCostIsPayedDescription),
            
            FrequentQuestionsModel(
                title: Localizable.whoIsTheResponsibleTitle,
                description: Localizable.whoIsTheResponsibleDescription),
            
            FrequentQuestionsModel(
                title: Localizable.howTheUserRecievesCashbackTitle,
                description: Localizable.howTheUserRecievesCashbackDescription)
        ])
    }
    
    func createCampaign() {
        presenter.didNextStep(action: .createCampaign)
    }
}
