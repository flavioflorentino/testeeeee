import UIKit

enum CampaignHowWorksAction {
    case createCampaign
}

protocol CampaignHowWorksCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CampaignHowWorksAction)
}

final class CampaignHowWorksCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - CampaignHowWorksCoordinating
extension CampaignHowWorksCoordinator: CampaignHowWorksCoordinating {
    func perform(action: CampaignHowWorksAction) {
        guard case .createCampaign = action else {
            return
        }
        
        let campaignType = CampaignTypeFactory.make()
        viewController?.navigationController?.pushViewController(campaignType, animated: true)
    }
}
