import Foundation

protocol CampaignHowWorksDisplay: AnyObject {
    func showFrequentQuestions(_ questions: [FrequentQuestionsModel])
}
