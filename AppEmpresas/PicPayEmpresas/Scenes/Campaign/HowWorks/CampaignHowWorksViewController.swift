import UI
import UIKit

extension CampaignHowWorksViewController.Layout {
    enum Sizes {
        static let topViewHeight: CGFloat = 650
        static let centerViewHeight: CGFloat = 1_000
    }
}

final class CampaignHowWorksViewController: ViewController<CampaignHowWorksInteractorInputs, UIView> {
    private typealias Localizable = Strings.CampaignHowWorks
    fileprivate enum Layout { }
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private let topView = CampaingHowWorksTopView()
    private let centerView = CampaignHowWorksCenterView()
    private let bottomView = CampaignHowWorksBottomView()
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(topView)
        contentView.addSubview(centerView)
        contentView.addSubview(bottomView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalTo(view)
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalTo(view)
        }
        
        topView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Sizes.topViewHeight)
        }
        
        centerView.snp.makeConstraints {
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Sizes.centerViewHeight)
        }
        
        bottomView.snp.makeConstraints {
            $0.top.equalTo(centerView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        title = Localizable.viewTitle
        centerView.delegate = self
        bottomView.delegate = self
        
        viewModel.getFrequentQuestions()
    }
}

// MARK: CampaignHowWorksDisplay

extension CampaignHowWorksViewController: CampaignHowWorksDisplay {
    func showFrequentQuestions(_ questions: [FrequentQuestionsModel]) {
        bottomView.setFrequentQuestions(questions)
    }
}

extension CampaignHowWorksViewController: CampaingHowWorksCenterViewDelegate {
    func createCampaing() {
        viewModel.createCampaign()
    }
}

// MARK: CampaingHowWorksBottomViewDelegate

extension CampaignHowWorksViewController: CampaingHowWorksBottomViewDelegate {
    func sectionViewPressed() {
        let bottomOffset = CGPoint(x: 0, y: Layout.Sizes.topViewHeight + Layout.Sizes.centerViewHeight)
        self.scrollView.setContentOffset(bottomOffset, animated: true)
    }
}
