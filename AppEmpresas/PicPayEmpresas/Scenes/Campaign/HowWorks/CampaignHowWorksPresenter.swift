import Foundation

protocol CampaignHowWorksPresenting: AnyObject {
    var viewController: CampaignHowWorksDisplay? { get set }
    func presentFrequentQuestions(_ questions: [FrequentQuestionsModel])
    func didNextStep(action: CampaignHowWorksAction)
}

final class CampaignHowWorksPresenter {
    private let coordinator: CampaignHowWorksCoordinating
    weak var viewController: CampaignHowWorksDisplay?

    init(coordinator: CampaignHowWorksCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - CampaignHowWorksPresenting

extension CampaignHowWorksPresenter: CampaignHowWorksPresenting {
    func presentFrequentQuestions(_ questions: [FrequentQuestionsModel]) {
        viewController?.showFrequentQuestions(questions)
    }
    
    func didNextStep(action: CampaignHowWorksAction) {
        coordinator.perform(action: action)
    }
}
