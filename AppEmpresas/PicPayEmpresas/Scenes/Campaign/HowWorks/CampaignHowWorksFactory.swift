import Foundation

enum CampaignHowWorksFactory {
    static func make() -> CampaignHowWorksViewController {
        let coordinator: CampaignHowWorksCoordinating = CampaignHowWorksCoordinator()
        let presenter: CampaignHowWorksPresenting = CampaignHowWorksPresenter(coordinator: coordinator)
        let viewModel = CampaignHowWorksInteractor(presenter: presenter)
        let viewController = CampaignHowWorksViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
