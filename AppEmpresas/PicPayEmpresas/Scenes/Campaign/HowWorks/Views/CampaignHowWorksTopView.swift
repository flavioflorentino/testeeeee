import SnapKit
import UI
import UIKit
import AssetsKit

private extension CampaingHowWorksTopView.Layout {
    enum NumberIndicator {
        static let width: CGFloat = 26.0
    }
    
    enum StackView {
        static let height: CGFloat = 170.0
    }
}

final class CampaingHowWorksTopView: UIView {
    private typealias Localizable = Strings.CampaignHowWorks
    fileprivate enum Layout {}
    
    private lazy var howWorksTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.howWorksTitle
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .business900())
        
        return label
    }()
    
    private lazy var numberOneLabel: UILabel = {
        let label = UILabel()
        label.text = "1."
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .branding600())
        
        return label
    }()
    
    private lazy var firstBuyTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.firstBuyTitle
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .branding900())
        
        return label
    }()
    
    private lazy var firstBuyDescriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.firstBuyDescription
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale950())
        
        return label
    }()
    
    private lazy var numberTwoLabel: UILabel = {
        let label = UILabel()
        label.text = "2."
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .branding600())
        
        return label
    }()
    
    private lazy var fixCashbackTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.fixCashbackTitle
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .branding900())
        
        return label
    }()
    
    private lazy var fixCashbackDescriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.fixCashbackDescription
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale950())
        
        return label
    }()
    
    private lazy var configDetailsSubtitleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.configDetailsSubtitle
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textAlignment, .center)
            .with(\.textColor, .business900())
        
        return label
    }()
    
    private lazy var detailsIconStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var valueDetailsView: CampaignDetailsIconView = {
        let detailsView = CampaignDetailsIconView()
        
        detailsView.setDetailsContent(
            image: Resources.Icons.icoValue.image,
            title: Localizable.valueTitle,
            description: Localizable.valueDescription)
        
        return detailsView
    }()
    
    private lazy var durationDetailsView: CampaignDetailsIconView = {
        let detailsView = CampaignDetailsIconView()
        
        detailsView.setDetailsContent(
            image: Resources.Icons.icoClock.image,
            title: Localizable.durationTitle,
            description: Localizable.durationDescription)
        
        return detailsView
    }()
    
    // MARK: - Inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration

extension CampaingHowWorksTopView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(howWorksTitleLabel)
        addSubview(numberOneLabel)
        addSubview(firstBuyTitleLabel)
        addSubview(firstBuyDescriptionLabel)
        addSubview(numberTwoLabel)
        addSubview(fixCashbackTitleLabel)
        addSubview(fixCashbackDescriptionLabel)
        addSubview(configDetailsSubtitleLabel)
        addSubview(detailsIconStackView)
        
        detailsIconStackView.addArrangedSubview(valueDetailsView)
        detailsIconStackView.addArrangedSubview(durationDetailsView)
    }
    
    func setupConstraints() {
        howWorksTitleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        numberOneLabel.snp.makeConstraints {
            $0.top.equalTo(howWorksTitleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.width.equalTo(Layout.NumberIndicator.width)
        }
        
        firstBuyTitleLabel.snp.makeConstraints {
            $0.top.equalTo(numberOneLabel)
            $0.leading.equalTo(numberOneLabel.snp.trailing).offset(Spacing.base03)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        firstBuyDescriptionLabel.snp.makeConstraints {
            $0.top.equalTo(firstBuyTitleLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalTo(firstBuyTitleLabel)
        }
        
        numberTwoLabel.snp.makeConstraints {
            $0.top.equalTo(firstBuyDescriptionLabel.snp.bottom).offset(Spacing.base04)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.width.equalTo(Layout.NumberIndicator.width)
        }
        
        fixCashbackTitleLabel.snp.makeConstraints {
            $0.top.equalTo(numberTwoLabel)
            $0.leading.equalTo(numberTwoLabel.snp.trailing).offset(Spacing.base03)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        fixCashbackDescriptionLabel.snp.makeConstraints {
            $0.top.equalTo(fixCashbackTitleLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalTo(fixCashbackTitleLabel)
        }
        
        configDetailsSubtitleLabel.snp.makeConstraints {
            $0.top.equalTo(fixCashbackDescriptionLabel.snp.bottom).offset(Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        detailsIconStackView.snp.makeConstraints {
            $0.top.equalTo(configDetailsSubtitleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.height.equalTo(Layout.StackView.height)
        }
    }
}
