import SnapKit
import UI
import UIKit

private extension CampaignDetailsIconView.Layout {
    enum Icon {
        static let size = CGSize(width: 52, height: 52)
    }
}

final class CampaignDetailsIconView: UIView {
    fileprivate enum Layout {}
    
    private lazy var detailsImageView: UIImageView = {
        let image = UIImageView()
        image.clipsToBounds = true
        
        return image
    }()
    
    private lazy var detailsTitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .center)
            .with(\.textColor, .business900())
        
        return label
    }()
    
    private lazy var detailsDescriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale950())
        
        return label
    }()
    
    // MARK: - Inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Public Methods

extension CampaignDetailsIconView {
    func setDetailsContent(image: UIImage, title: String, description: String) {
        detailsImageView.image = image
        detailsTitleLabel.text = title
        detailsDescriptionLabel.text = description
    }
}

// MARK: - ViewConfiguration

extension CampaignDetailsIconView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(detailsImageView)
        addSubview(detailsTitleLabel)
        addSubview(detailsDescriptionLabel)
    }
    
    func setupConstraints() {
        detailsImageView.snp.makeConstraints {
            $0.top.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Icon.size)
        }
        
        detailsTitleLabel.snp.makeConstraints {
            $0.top.equalTo(detailsImageView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        detailsDescriptionLabel.snp.makeConstraints {
            $0.top.equalTo(detailsTitleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
}
