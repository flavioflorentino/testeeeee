import SnapKit
import UI
import UIKit

struct FrequentQuestionsModel {
    var title: String
    var description: String
}

protocol CampaingHowWorksBottomViewDelegate: AnyObject {
    func sectionViewPressed()
}

private extension CampaignHowWorksBottomView.Layout {
    enum Separator {
        static let height: CGFloat = 1.0
    }
    
    enum QuestionsView {
        static let height: Int = 690
    }
    
    enum Arrow {
        static let size = CGSize(width: 10.54, height: 6.25)
    }
    
    enum Section {
        static let height: CGFloat = 56.0
    }
}

final class CampaignHowWorksBottomView: UIView {
    private typealias Localizable = Strings.CampaignHowWorks
    fileprivate enum Layout {}
    
    weak var delegate: CampaingHowWorksBottomViewDelegate?
    
    private var questionsStackViewHeight = 0
    
    private let sectionView = UIView()
    
    private lazy var topSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()
    
    private lazy var bottomSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.frequentQuestions
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .branding600())
        
        return label
    }()
    
    private let arrowImageView = UIImageView(image: Assets.icoArrowDown.image)
    
    private lazy var questionsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }()
    
    // MARK: - Inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

@objc
private extension CampaignHowWorksBottomView {
    func sectionViewTouched() {
        questionsStackViewHeight = questionsStackViewHeight == 0 ? Layout.QuestionsView.height : 0
        
        questionsStackView.snp.remakeConstraints {
            $0.top.equalTo(bottomSeparatorView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
            $0.height.equalTo(questionsStackViewHeight)
        }
        
        self.layoutIfNeeded()
        delegate?.sectionViewPressed()
    }
}

// MARK: - Public Methods

extension CampaignHowWorksBottomView {
    func setFrequentQuestions(_ model: [FrequentQuestionsModel]) {
        model.forEach {
            let questionView = CampaignFrequentQuestionView()
            questionView.setViewContent($0.title, $0.description)
            questionsStackView.addArrangedSubview(questionView)
        }
    }
}

// MARK: - ViewConfiguration

extension CampaignHowWorksBottomView: ViewConfiguration {
    func configureViews() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(sectionViewTouched))
        sectionView.addGestureRecognizer(tap)
    }
    
    func buildViewHierarchy() {
        addSubview(sectionView)
        
        sectionView.addSubview(topSeparatorView)
        sectionView.addSubview(titleLabel)
        sectionView.addSubview(arrowImageView)
        sectionView.addSubview(bottomSeparatorView)
        
        addSubview(questionsStackView)
    }
    
    func setupConstraints() {
        sectionView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Section.height)
        }
        
        topSeparatorView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Separator.height)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(topSeparatorView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalTo(sectionView).inset(Spacing.base02)
        }
        
        arrowImageView.snp.makeConstraints {
            $0.centerY.equalTo(titleLabel)
            $0.trailing.equalTo(sectionView.snp.trailing).inset(Spacing.base03)
            $0.size.equalTo(Layout.Arrow.size)
        }
        
        bottomSeparatorView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Separator.height)
        }
        
        questionsStackView.snp.makeConstraints {
            $0.top.equalTo(bottomSeparatorView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
            $0.height.equalTo(questionsStackViewHeight)
        }
    }
}
