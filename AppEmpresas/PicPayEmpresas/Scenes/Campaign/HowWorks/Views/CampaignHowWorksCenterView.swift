import SnapKit
import UI
import UIKit
import AssetsKit

private extension CampaignHowWorksCenterView.Layout {
    enum NumberIndicator {
        static let with: CGFloat = 26.0
    }
    
    enum StackView {
        static let height: CGFloat = 170.0
    }
    
    enum DeviceImage {
        static let size = CGSize(width: 300, height: 600)
    }
}

protocol CampaingHowWorksCenterViewDelegate: AnyObject {
    func createCampaing()
}

final class CampaignHowWorksCenterView: UIView {
    private typealias Localizable = Strings.CampaignHowWorks
    fileprivate enum Layout {}
    
    weak var delegate: CampaingHowWorksCenterViewDelegate?
    
    private lazy var greenBackgroundView: UIView = {
       let view = UIView()
        view.backgroundColor = Colors.branding900.color
        return view
    }()
    
    private lazy var topLabel: UILabel = {
       let label = UILabel()
        label.text = Localizable.waitConfirmationEmail
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .white())
            .with(\.textAlignment, .center)
        
        return label
    }()
    
    private let deviceImage = UIImageView(image: Assets.iosDeviceImg.image)
    
    private lazy var bottomLabel: UILabel = {
       let label = UILabel()
        label.text = Localizable.highlightYourBusiness
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .business900())
            .with(\.textAlignment, .center)
        
        return label
    }()
    
    private lazy var craeateCampaignButton: UIButton = {
        let button = UIButton()
        
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.createCampaign, for: .normal)
        button.addTarget(self, action: #selector(createCampaingPressed), for: .touchUpInside)
        
        return button
    }()
    
    // MARK: - Inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Private Methods

@objc
private extension CampaignHowWorksCenterView {
    func createCampaingPressed() {
        delegate?.createCampaing()
    }
}

// MARK: - ViewConfiguration

extension CampaignHowWorksCenterView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(greenBackgroundView)
        addSubview(topLabel)
        addSubview(deviceImage)
        addSubview(bottomLabel)
        addSubview(craeateCampaignButton)
    }
    
    func setupConstraints() {
        topLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base04)
        }
        
        deviceImage.snp.makeConstraints {
            $0.top.equalTo(topLabel.snp.bottom).offset(Spacing.base04)
            $0.size.equalTo(Layout.DeviceImage.size)
            $0.centerX.equalToSuperview()
        }
        
        bottomLabel.snp.makeConstraints {
            $0.top.equalTo(deviceImage.snp.bottom).offset(Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        craeateCampaignButton.snp.makeConstraints {
            $0.top.equalTo(bottomLabel.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base08)
        }
        
        greenBackgroundView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(deviceImage.snp.bottom).inset(Spacing.base09)
        }
    }
}
