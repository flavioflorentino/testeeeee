import Core
import Foundation

protocol ActiveCampaignInteracting: AnyObject {
    func fillCampaignContent()
    func confirmDeleteCampaign()
}

final class ActiveCampaignInteractor {
    private let service: ActiveCampaignServicing
    private let presenter: ActiveCampaignPresenting
    private var campaignData: CampaignResponse
    
    init(service: ActiveCampaignServicing,
         presenter: ActiveCampaignPresenting,
         campaign: CampaignResponse
    ) {
        self.service = service
        self.presenter = presenter
        campaignData = campaign
    }
}

// MARK: - ActiveCampaignInteracting
extension ActiveCampaignInteractor: ActiveCampaignInteracting {
    func fillCampaignContent() {
        presenter.presentCampaignData(campaignData: campaignData)
    }
    
    func confirmDeleteCampaign() {
        self.presenter.startLoading()
        
        service.deletCampaign(campaignData.id) { [weak self] result in
            self?.presenter.stopLoading()
        
            switch result {
            case let .failure(apiError):
                if let requestError = apiError.requestError {
                    self?.presenter.showError(requestError: requestError)
                } else {
                    self?.presenter.showError(apiError: apiError)
                }
            case .success:
                self?.presenter.didNextStep(action: .popToHome)
            }
        }
    }
}
