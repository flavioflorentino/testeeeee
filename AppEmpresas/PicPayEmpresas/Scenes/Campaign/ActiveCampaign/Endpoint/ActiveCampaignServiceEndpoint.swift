import Core
import Foundation

enum ActiveCampaignServiceEndpoint {
    case deleteCampaign(_ campaignId: String)
}

extension ActiveCampaignServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case let .deleteCampaign(campaignId):
            return "/campaigns/\(campaignId)/inactive"
        }
    }
    
    var method: HTTPMethod {
        .get
    }
}
