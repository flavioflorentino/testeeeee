import Foundation

enum ActiveCampaignFactory {
    static func make(_ campaign: CampaignResponse) -> ActiveCampaignViewController {
        let container = DependencyContainer()
        let service: ActiveCampaignServicing = ActiveCampaignService(dependencies: container)
        let coordinator: ActiveCampaignCoordinating = ActiveCampaignCoordinator()
        let presenter: ActiveCampaignPresenting = ActiveCampaignPresenter(coordinator: coordinator)
        let interactor = ActiveCampaignInteractor(service: service, presenter: presenter, campaign: campaign)
        let viewController = ActiveCampaignViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
