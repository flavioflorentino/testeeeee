import UI
import Core
import AssetsKit
import Foundation

protocol ActiveCampaignPresenting: AnyObject {
    var viewController: ActiveCampaignDisplay? { get set }
    func startLoading()
    func stopLoading()
    func showError(apiError: ApiError)
    func showError(requestError: RequestError)
    func presentCampaignData(campaignData: CampaignResponse)
    func didNextStep(action: ActiveCampaignAction)
}

private extension ActiveCampaignPresenter.Layout {
    enum IconSize {
        static let `default` = CGSize(width: 27, height: 27)
        static let maxValue = CGSize(width: 30.0, height: 19.0)
    }
    
    enum ModalImage {
        static let size = CGSize(width: 80, height: 80)
    }
}

final class ActiveCampaignPresenter {
    private typealias Localizable = Strings.ActiveCampaign
    fileprivate enum Layout { }
    
    private let coordinator: ActiveCampaignCoordinating
    weak var viewController: ActiveCampaignDisplay?
    
    init(coordinator: ActiveCampaignCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ActiveCampaignPresenting
extension ActiveCampaignPresenter: ActiveCampaignPresenting {
    func showError(apiError: ApiError) {
        showError(message: apiError.localizedDescription)
    }
    
    func showError(requestError: RequestError) {
        showError(message: requestError.message, title: requestError.title)
    }
    
    func startLoading() {
        viewController?.startLoadingView()
    }
    
    func stopLoading() {
        viewController?.stopLoadingView()
    }
    
    func presentCampaignData(campaignData: CampaignResponse) {
        let campaignTypeItem = CampaignItem(icon: Resources.Icons.icoTag.image,
                                            title: Localizable.campaignType,
                                            iconSize: Layout.IconSize.default,
                                            description: campaignData.type)
        
        let percentageItem = CampaignItem(icon: Resources.Icons.icoCurrency.image,
                                          title: Localizable.cashbackValue,
                                          iconSize: Layout.IconSize.default,
                                          description: "\(campaignData.cashback)")
        
        let maxValueItem = CampaignItem(icon: Resources.Icons.icoMoney.image,
                                        title: Localizable.maxPayment,
                                        iconSize: Layout.IconSize.maxValue,
                                        description: "\(campaignData.maxValue)")
        
        let durationItem = CampaignItem(icon: Resources.Icons.icoWatch.image,
                                        title: Localizable.campaignDuration,
                                        iconSize: Layout.IconSize.default,
                                        description: campaignData.expirationDate)
        
        viewController?.displayCampaignData(items: [
            campaignTypeItem,
            percentageItem,
            maxValueItem,
            durationItem
        ])
    }
    
    func didNextStep(action: ActiveCampaignAction) {
        coordinator.perform(action: action)
    }
}

// MARK: - Private Methods
private extension ActiveCampaignPresenter {
    func showError(message: String, title: String? = nil) {
        let modalImage = AlertModalInfoImage(
            image: Assets.Emoji.iconBad.image,
            size: Layout.ModalImage.size
        )
        
        let modalInfo = AlertModalInfo(
            imageInfo: modalImage,
            title: title,
            subtitle: message,
            buttonTitle: Localizable.modalButtonTitle
        )
        
        coordinator.perform(action: .deleteCampaignError(modalInfo: modalInfo))
    }
}
