import Core
import Foundation

protocol ActiveCampaignServicing {
    func deletCampaign(_ campaignId: String, completion: @escaping (Result<CampaignResponse?, ApiError>) -> Void)
}

final class ActiveCampaignService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ActiveCampaignServicing
extension ActiveCampaignService: ActiveCampaignServicing {
    func deletCampaign(_ campaignId: String, completion: @escaping (Result<CampaignResponse?, ApiError>) -> Void) {
        let endpoint = ActiveCampaignServiceEndpoint.deleteCampaign(campaignId)

        BizApi<CampaignResponse>(endpoint: endpoint).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }
}
