import UI
import UIKit
import AssetsKit

protocol ActiveCampaignDisplay: AnyObject, LoadingViewProtocol {
    func displayCampaignData(items: [CampaignItem])
}

private extension ActiveCampaignViewController.Layout {
    enum Image {
        static let size = CGSize(width: 47, height: 47)
    }
}

final class ActiveCampaignViewController: ViewController<ActiveCampaignInteracting, UIView> {
    private typealias Localizable = Strings.ActiveCampaign
    fileprivate enum Layout { }
    
    lazy var loadingView = LoadingView()
    
    private lazy var deleteButtonItem = UIBarButtonItem(image: Resources.Icons.icoTrash.image,
                                                        style: .plain,
                                                        target: self,
                                                        action: #selector(deleteButtonTapped))
    
    private lazy var successImage = UIImageView(image: Resources.Icons.icoCheck.image)
    
    private lazy var successMessage: UILabel = {
        let label = UILabel()
        label.text = Localizable.successMessage
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale950())
            .with(\.textAlignment, .center)
        
        return label
    }()
    
    private lazy var itemsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        
        return stackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fillCampaignContent()
    }
    
    override func buildViewHierarchy() {
        view.addSubviews(successImage,
                         successMessage,
                         itemsStackView)
    }
    
    override func setupConstraints() {
        successImage.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base04)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Image.size)
        }
        
        successMessage.snp.makeConstraints {
            $0.top.equalTo(successImage.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        itemsStackView.snp.makeConstraints {
            $0.top.equalTo(successMessage.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        title = Localizable.viewTitle
        navigationItem.rightBarButtonItem = deleteButtonItem
    }
}

// MARK: ActiveCampaignDisplay
extension ActiveCampaignViewController: ActiveCampaignDisplay {
    func displayCampaignData(items: [CampaignItem]) {
        for item in items {
            itemsStackView.addArrangedSubview(ConfirmCampaignItem(item))
        }
    }
}

@objc
private extension ActiveCampaignViewController {
    func deleteButtonTapped() {
        showConfirmationDeleteDialog()
    }
    
    func showConfirmationDeleteDialog() {
        let popup = UI.PopupViewController(
            title: Localizable.cancelCampaign,
            description: Localizable.cancelCampaignDialogDescription,
            preferredType: .business)
        
        let confirmation = PopupAction(
            title: Localizable.buttonTitleYes,
            style: .fill
        ) {
            self.interactor.confirmDeleteCampaign()
        }
        
        let cancel = PopupAction(
            title: Localizable.buttonTitleNo,
            style: .simpleText
        ) {
            self.dismiss(animated: true)
        }
        
        popup.addAction(confirmation)
        popup.addAction(cancel)
        self.showPopup(popup)
    }
}
