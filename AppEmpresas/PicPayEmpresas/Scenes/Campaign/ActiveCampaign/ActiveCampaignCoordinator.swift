import UIKit

enum ActiveCampaignAction {
    case popToHome
    case deleteCampaignError(modalInfo: AlertModalInfo)
}

protocol ActiveCampaignCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ActiveCampaignAction)
}

final class ActiveCampaignCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - ActiveCampaignCoordinating
extension ActiveCampaignCoordinator: ActiveCampaignCoordinating {
    func perform(action: ActiveCampaignAction) {
        switch action {
        case .deleteCampaignError(let modalInfo):
            showModalAlert(modalInfo: modalInfo)
        case .popToHome:
            viewController?.navigationController?.popToRootViewController(animated: true)
        }
    }
}

private extension ActiveCampaignCoordinator {
    func showModalAlert(modalInfo: AlertModalInfo) {
        let alertPopup = AlertModalFactory.make(alertModalInfo: modalInfo)
        alertPopup.touchButtonAction = { alert in
            alert.dismiss(animated: true)
        }
        showAlert(contentController: alertPopup)
    }
    
    func showAlert(contentController: UIViewController) {
        let popup = PopupViewController()
        popup.contentController = contentController
        viewController?.showPopup(popup)
    }
}
