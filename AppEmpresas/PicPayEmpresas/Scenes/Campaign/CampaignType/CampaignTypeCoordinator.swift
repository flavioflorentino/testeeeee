import UIKit

enum CampaignTypeAction {
    case configCampaign(type: CampaignType)
}

protocol CampaignTypeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CampaignTypeAction)
}

final class CampaignTypeCoordinator: CampaignTypeCoordinating {
    var viewController: UIViewController?
    
    func perform(action: CampaignTypeAction) {
        switch action {
        case let .configCampaign(type):
            let controller = CreateCampaignFactory.make(type)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
