import SnapKit
import UI
import UIKit

private extension CampaingTypeCardView.Layout {
    enum ArrowImage {
        static let size = CGSize(width: 12, height: 20)
    }
}

final class CampaingTypeCardView: UIView {
    fileprivate enum Layout {}
    
    // MARK: - Private Properties
    
    private lazy var cardContainerView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle())
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.border, .light(color: .grayscale100()))
            .with(\.cornerRadius, .medium)
    
        return view
    }()

    private lazy var cardTitle: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .branding600())
            .with(\.textAlignment, .left)
        return label
    }()

    private lazy var cardDescription: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
            .with(\.textAlignment, .left)
        
        return label
    }()

    private lazy var cardArrowImage: UIImageView = {
        let imageView = UIImageView()
        imageView.size = Layout.ArrowImage.size
        imageView.image = Assets.icoArrow.image
        return imageView
    }()
    
    // MARK: - Inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public Methods
    
    func setCardContent(title: String, description: String) {
        cardTitle.text = title
        cardDescription.text = description
    }
}

// MARK: - ViewConfiguration

extension CampaingTypeCardView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(cardContainerView)
        cardContainerView.addSubview(cardTitle)
        cardContainerView.addSubview(cardDescription)
        cardContainerView.addSubview(cardArrowImage)
    }
    
    func setupConstraints() {
        cardContainerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        cardTitle.snp.makeConstraints {
            $0.top.equalTo(cardContainerView.snp.top).offset(Spacing.base03)
            $0.leading.equalTo(cardContainerView.snp.leading).offset(Spacing.base02)
            $0.trailing.equalTo(cardContainerView.snp.trailing).inset(Spacing.base02)
        }
        
        cardArrowImage.snp.makeConstraints {
            $0.size.equalTo(Layout.ArrowImage.size)
            $0.centerY.equalTo(cardContainerView.snp.centerY)
            $0.trailing.equalTo(cardContainerView.snp.trailing).inset(Spacing.base03)
        }
        
        cardDescription.snp.makeConstraints {
            $0.top.equalTo(cardTitle.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(cardContainerView.snp.leading).offset(Spacing.base02)
            $0.trailing.equalTo(cardArrowImage.snp.leading).offset(-Spacing.base03)
            $0.bottom.equalTo(cardContainerView.snp.bottom).inset(Spacing.base03)
        }
    }
}
