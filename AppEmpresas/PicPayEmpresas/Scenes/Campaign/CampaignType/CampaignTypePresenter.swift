import Foundation

protocol CampaignTypePresenting: AnyObject {
    var viewController: CampaignTypeViewController? { get set }
    func didNextStep(action: CampaignTypeAction)
}

final class CampaignTypePresenter {
    private let coordinator: CampaignTypeCoordinating
    weak var viewController: CampaignTypeViewController?

    init(coordinator: CampaignTypeCoordinating) {
        self.coordinator = coordinator
    }
}

extension CampaignTypePresenter: CampaignTypePresenting {
    func didNextStep(action: CampaignTypeAction) {
        coordinator.perform(action: action)
    }
}
