import UI

private extension CampaignTypeViewController.Layout {
    enum ArrowImage {
        static let size = CGSize(width: 12, height: 20)
    }
}

final class CampaignTypeViewController: ViewController<CampaignTypeViewModelInputs, UIView> {
    private typealias Localizable = Strings.CampaignType
    fileprivate enum Layout { }
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private lazy var campaignTypeLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.campaignType
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .black())
            .with(\.textAlignment, .left)
        return label
    }()
    
    private lazy var firstCard: CampaingTypeCardView = {
        let view = CampaingTypeCardView()
        
        view.setCardContent(
            title: Localizable.firstBuyCardTitle,
            description: Localizable.firstBuyCardDescription)
        
        let tapAction = UITapGestureRecognizer(target: self, action: #selector(firsCardTouched))
        view.addGestureRecognizer(tapAction)
        
        return view
    }()
    
    private lazy var secondCard: CampaingTypeCardView = {
        let view = CampaingTypeCardView()
        
        view.setCardContent(
            title: Localizable.fixCashbackCardTitle,
            description: Localizable.fixCashbackCardDescription)
        
        let tapAction = UITapGestureRecognizer(target: self, action: #selector(secondCardTouched))
        view.addGestureRecognizer(tapAction)
        
        return view
    }()
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        title = Localizable.viewTitle
    }
    
    override func buildViewHierarchy() {
        view.addSubview(contentView)
        
        contentView.addSubview(campaignTypeLabel)
        contentView.addSubview(firstCard)
        contentView.addSubview(secondCard)
    }
    
    override func setupConstraints() {
        contentView.snp.makeConstraints {
            $0.edges.equalTo(view)
        }
        
        campaignTypeLabel.snp.makeConstraints {
            $0.top.equalTo(contentView.snp.top).offset(Spacing.base03)
            $0.leading.equalTo(contentView.snp.leading).offset(Spacing.base02)
            $0.trailing.equalTo(contentView.snp.trailing).inset(Spacing.base02)
        }
        
        firstCard.snp.makeConstraints {
            $0.top.equalTo(campaignTypeLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalTo(contentView.snp.leading).offset(Spacing.base02)
            $0.trailing.equalTo(contentView.snp.trailing).inset(Spacing.base02)
        }
        
        secondCard.snp.makeConstraints {
            $0.top.equalTo(firstCard.snp.bottom).offset(Spacing.base02)
            $0.leading.equalTo(contentView.snp.leading).offset(Spacing.base02)
            $0.trailing.equalTo(contentView.snp.trailing).inset(Spacing.base02)
        }
    }
}

@objc
private extension CampaignTypeViewController {
    func firsCardTouched() {
        viewModel.createFirstBuyCampaign()
    }
    
    func secondCardTouched() {
        viewModel.createFixCashbackCampaign()
    }
}
