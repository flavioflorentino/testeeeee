import Foundation

enum CampaignTypeFactory {
    static func make() -> CampaignTypeViewController {
        let coordinator: CampaignTypeCoordinating = CampaignTypeCoordinator()
        let presenter: CampaignTypePresenting = CampaignTypePresenter(coordinator: coordinator)
        let viewModel = CampaignTypeViewModel(presenter: presenter)
        let viewController = CampaignTypeViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
