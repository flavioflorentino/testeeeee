import Foundation

protocol CampaignTypeViewModelInputs: AnyObject {
    func createFirstBuyCampaign()
    func createFixCashbackCampaign()
}

final class CampaignTypeViewModel {
    private let presenter: CampaignTypePresenting

    init(presenter: CampaignTypePresenting) {
        self.presenter = presenter
    }
}

// MARK: - CampaignTypeViewModelInputs
extension CampaignTypeViewModel: CampaignTypeViewModelInputs {
    func createFirstBuyCampaign() {
        presenter.didNextStep(action: .configCampaign(type: .firstBuy))
    }
    
    func createFixCashbackCampaign() {
        presenter.didNextStep(action: .configCampaign(type: .fixCashback))
    }
}
