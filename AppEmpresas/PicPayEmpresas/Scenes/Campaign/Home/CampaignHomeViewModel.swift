import Core
import Foundation

protocol CampaignHomeViewModelInput: AnyObject {
    func howCampaignWorks()
    func getCampaigns()
    func createCampaign()
}

final class CampaignHomeViewModel {
    private let service: CampaignHomeServicing
    private let presenter: CampaignHomePresenting
    
    init(service: CampaignHomeServicing, presenter: CampaignHomePresenting) {
        self.service = service
        self.presenter = presenter
    }
}

extension CampaignHomeViewModel: CampaignHomeViewModelInput {
    func getCampaigns() {
        presenter.setLoading(isLoading: true)
        
        service.getCampaigns { [weak self] result in
            self?.presenter.setLoading(isLoading: false)
            
            switch result {
            case let .failure(apiError):
                if let requestError = apiError.requestError {
                    self?.presenter.showError(requestError: requestError)
                } else {
                    self?.presenter.showError(apiError: apiError)
                }
            case let .success(campaigns):
                guard let campaignsResponse = campaigns else {
                    self?.presenter.showError(apiError: ApiError.bodyNotFound)
                    return
                }
                
                self?.handleCampaignResult(campaignsResponse)
            }
        }
    }
    
    func howCampaignWorks() {
        presenter.didNextStep(action: .howCampaignWorks)
    }
    
    func createCampaign() {
        presenter.didNextStep(action: .createCampaign)
    }
    
    private func handleCampaignResult(_ campaigns: [CampaignResponse]) {
        let campaignResponse = campaigns.first { $0.active == true }
        
        guard let campaign = campaignResponse else {
            return
        }
        
        self.presenter.didNextStep(action: .goToCampaign(campaign))
    }
}
