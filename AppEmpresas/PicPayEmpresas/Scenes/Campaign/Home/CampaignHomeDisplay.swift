import Foundation

protocol CampaignHomeDisplay: AnyObject {
    func showLoading()
    func hideLoading()
}
