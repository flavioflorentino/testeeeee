import Core
import LegacyPJ

protocol CampaignHomeServicing {
    typealias GetCampaignsCompletionBlock = (Result<[CampaignResponse]?, ApiError>) -> Void
    func getCampaigns(completion: @escaping GetCampaignsCompletionBlock)
}

final class CampaignHomeService {
    typealias Dependencies = HasMainQueue & HasAuthManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension CampaignHomeService: CampaignHomeServicing {
    func getCampaigns(completion: @escaping GetCampaignsCompletionBlock) {
        let endpoint = CompaignHomeServiceEndpoint.getCampaigns
        BizApi<[CampaignResponse]>(endpoint: endpoint).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }
}
