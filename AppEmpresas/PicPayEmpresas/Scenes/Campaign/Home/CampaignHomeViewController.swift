import Foundation
import UI
import SnapKit

extension CampaignHomeViewController.Layout {
    enum ShopImage {
        static let size = CGSize(width: 200, height: 203)
    }
}

final class CampaignHomeViewController: ViewController<CampaignHomeViewModelInput, UIView> {
    private typealias Localizable = Strings.CampaignHome
    fileprivate enum Layout { }
    
    lazy var loadingView = LoadingView()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.isUserInteractionEnabled = true
        return scrollView
    }()
    
    private lazy var shopImage: UIImageView = {
        let imageView = UIImageView(image: Assets.shopIcon.image)
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var screenTitle: UILabel = {
        let label = UILabel()
        label.text = Localizable.campaignHomeTitle
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        
        return label
    }()
    
    private lazy var screenDescription: UILabel = {
        let label = UILabel()
        label.text = Localizable.campaignHomeDescription
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
            .with(\.textAlignment, .center)
        
        return label
    }()
    
    private lazy var createCampaign: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(
            Localizable.createCampaign,
            for: .normal)
        button.addTarget(self, action: #selector(createCampaingPressed), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var howWorks: UIButton = {
        let button = UIButton()
        button.buttonStyle(SecondaryButtonStyle())
        button.setTitle(
            Localizable.howWorks,
            for: .normal)
        button.addTarget(self, action: #selector(howWorksPressed), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getCampaigns()
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        title = Localizable.viewTitle
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(shopImage)
        contentView.addSubview(screenTitle)
        contentView.addSubview(screenDescription)
        contentView.addSubview(createCampaign)
        contentView.addSubview(howWorks)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalTo(view)
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalTo(view)
            $0.height.greaterThanOrEqualTo(view)
        }
        
        screenTitle.snp.makeConstraints {
            $0.centerY.equalTo(contentView).inset(Spacing.base08)
            $0.leading.equalTo(contentView).offset(Spacing.base03)
            $0.trailing.equalTo(contentView).inset(Spacing.base03)
        }
        
        screenDescription.snp.makeConstraints {
            $0.top.equalTo(screenTitle.snp.bottom).offset(Spacing.base02)
            $0.leading.equalTo(contentView).offset(Spacing.base03)
            $0.trailing.equalTo(contentView).inset(Spacing.base03)
        }
        
        shopImage.snp.makeConstraints {
            $0.bottom.equalTo(screenTitle.snp.top).offset(-Spacing.base04)
            $0.size.equalTo(Layout.ShopImage.size)
            $0.centerX.equalTo(contentView.snp.centerX)
        }
        
        howWorks.snp.makeConstraints {
            $0.bottom.equalTo(contentView.snp.bottom).inset(Spacing.base05)
            $0.leading.trailing.equalTo(createCampaign)
        }
        
        createCampaign.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(screenDescription.snp.bottom).offset(Spacing.base06)
            $0.bottom.equalTo(howWorks.snp.top).offset(-Spacing.base02)
            $0.leading.trailing.equalTo(screenDescription)
        }
    }
}

@objc
extension CampaignHomeViewController {
    private func createCampaingPressed() {
        viewModel.createCampaign()
    }
    
    private func howWorksPressed() {
        viewModel.howCampaignWorks()
    }
}

extension CampaignHomeViewController: CampaignHomeDisplay {
    func showLoading() {
        startLoadingView()
    }
    
    func hideLoading() {
        stopLoadingView()
    }
}

extension CampaignHomeViewController: LoadingViewProtocol { }
