import Foundation

enum CampaignHomeFactory {
    static func make() -> CampaignHomeViewController {
        let container = DependencyContainer()
        let service: CampaignHomeServicing = CampaignHomeService(dependencies: container)
        let coordinator: CampaignHomeCoordinating = CampaignHomeCoordinator()
        let presenter: CampaignHomePresenting = CampaignHomePresenter(coordinator: coordinator)
        let viewModel = CampaignHomeViewModel(service: service, presenter: presenter)
        
        let viewController = CampaignHomeViewController(viewModel: viewModel)
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
