import Core

enum CompaignHomeServiceEndpoint {
    case getCampaigns
}

extension CompaignHomeServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .getCampaigns:
            return "/campaigns"
        }
    }
    
    var method: HTTPMethod {
        .get
    }
}
