import Core
import Foundation
import UIKit

protocol CampaignHomePresenting: AnyObject {
    var viewController: CampaignHomeDisplay? { get set }
    
    func setLoading(isLoading: Bool)
    func showError(apiError: ApiError)
    func showError(requestError: RequestError)
    func didNextStep(action: CampaignHomeAction)
}

final class CampaignHomePresenter: CampaignHomePresenting {
    private typealias Localizable = Strings.CampaignHome
    private let coordinator: CampaignHomeCoordinating
    var viewController: CampaignHomeDisplay?
    
    init(coordinator: CampaignHomeCoordinating) {
        self.coordinator = coordinator
    }
    
    func setLoading(isLoading: Bool) {
         isLoading ? viewController?.showLoading() : viewController?.hideLoading()
    }
    
    func showError(apiError: ApiError) {
        showError(message: apiError.localizedDescription)
    }
    
    func showError(requestError: RequestError) {
        showError(message: requestError.message, title: requestError.title)
    }
    
    func didNextStep(action: CampaignHomeAction) {
        coordinator.perform(action: action)
    }
}

extension CampaignHomePresenter {
    func showError(message: String, title: String? = nil) {
           let modalImage = AlertModalInfoImage(
               image: Assets.Emoji.iconBad.image,
               size: CGSize(width: 80, height: 80)
           )
           
           let modalInfo = AlertModalInfo(
               imageInfo: modalImage,
               title: title,
               subtitle: message,
               buttonTitle: Localizable.okGotIt
           )
           
        coordinator.perform(action: .showErrorAlert(modalInfo: modalInfo))
       }
}
