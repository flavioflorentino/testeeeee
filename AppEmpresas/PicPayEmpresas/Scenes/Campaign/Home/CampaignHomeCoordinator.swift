import Foundation
import UIKit

enum CampaignHomeAction {
    case howCampaignWorks
    case goToCampaign(_ campaign: CampaignResponse)
    case createCampaign
    case showErrorAlert(modalInfo: AlertModalInfo)
}

protocol CampaignHomeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CampaignHomeAction)
}

private extension CampaignHomeCoordinator.Layout {
    enum ModalInfo {
        static let size = CGSize(width: 52, height: 52)
    }
}

final class CampaignHomeCoordinator: CampaignHomeCoordinating {
    fileprivate enum Layout { }
    weak var viewController: UIViewController?
    
    func perform(action: CampaignHomeAction) {
        switch action {
        case let .showErrorAlert(modal):
            showModalAlert(modalInfo: modal)
        case .howCampaignWorks:
            let howWorks = CampaignHowWorksFactory.make()
            viewController?.navigationController?.pushViewController(howWorks, animated: true)
        case .createCampaign:
            let campaignType = CampaignTypeFactory.make()
            viewController?.navigationController?.pushViewController(campaignType, animated: true)
        case let .goToCampaign(campaign):
            let controller = ActiveCampaignFactory.make(campaign)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

extension CampaignHomeCoordinator {
    func showModalAlert(modalInfo: AlertModalInfo) {
        let alertPopup = AlertModalFactory.make(alertModalInfo: modalInfo)
        alertPopup.touchButtonAction = { alert in
            alert.dismiss(animated: true)
        }
        showAlert(contentController: alertPopup)
    }
    
    func showAlert(contentController: UIViewController) {
        let popup = PopupViewController()
        popup.contentController = contentController
        viewController?.present(popup, animated: true)
    }
}
