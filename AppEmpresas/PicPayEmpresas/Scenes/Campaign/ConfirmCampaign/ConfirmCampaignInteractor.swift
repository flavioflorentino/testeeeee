import Core
import Foundation

protocol ConfirmCampaignInteracting: AnyObject {
    func showTermsAndConditions()
    func checkboxSelected(_ isSelected: Bool)
    func setCampaignData()
    func createCampaign()
}

final class ConfirmCampaignInteractor {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    
    private let service: ConfirmCampaignServicing
    private let presenter: ConfirmCampaignPresenting
    private var campaignData: Campaign
    
    init(service: ConfirmCampaignServicing, presenter: ConfirmCampaignPresenting, dependencies: Dependencies, campaign: Campaign) {
        self.service = service
        self.presenter = presenter
        self.campaignData = campaign
        self.dependencies = dependencies
    }
}

// MARK: - ConfirmCampaignInteracting
extension ConfirmCampaignInteractor: ConfirmCampaignInteracting {
    func checkboxSelected(_ isSelected: Bool) {
        presenter.enableCreateButton(isSelected)
    }
    
    func showTermsAndConditions() {
        presenter.didNextStep(action: .goToTermsAndConditions)
    }
    
    func setCampaignData() {
        presenter.presentCampaignData(campaignData)
    }
    
    func createCampaign() {
        presenter.startLoading()
        let maxValue = brazilianCurrencyToNumber(campaignData.maxValue)
        
        campaignData.expirationDate = formatExpirationDate(days: campaignData.expirationDate.onlyNumbers)
        campaignData.maxValue = "\(maxValue ?? 0.0)"
        
        service.createCampaign(campaignData) { [weak self] result in
            self?.presenter.stopLoading()
        
            switch result {
            case .failure(let apiError):
                if let requestError = apiError.requestError {
                    self?.presenter.showError(requestError: requestError)
                } else {
                    self?.presenter.showError(apiError: apiError)
                }
            case let .success(campaignResponse):
                guard let campaign = campaignResponse else {
                    self?.presenter.showError(apiError: ApiError.bodyNotFound)
                    return
                }
                self?.presenter.didNextStep(action: .goToCampaign(campaign))
            }
        }
    }
    
    private func formatExpirationDate(days: String) -> String {
        var dayComponent = DateComponents()
        dayComponent.day = Int(days)
        let calendar = Calendar.current
        
        guard let expirationDate = calendar.date(byAdding: dayComponent, to: Date())
            else {
                return days
        }
         
        return DateFormatter.custom(value: expirationDate, format: .dateAndTime)
    }
    
    private func brazilianCurrencyToNumber(_ currency: String) -> Double? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "pt_BR")
        let number = formatter.number(from: currency)
        
        return number?.doubleValue
    }
}
