import Foundation

enum ConfirmCampaignFactory {
    static func make(_ model: Campaign) -> ConfirmCampaignViewController {
        let container = DependencyContainer()
        let service: ConfirmCampaignServicing = ConfirmCampaignService(dependencies: container)
        let coordinator: ConfirmCampaignCoordinating = ConfirmCampaignCoordinator()
        let presenter: ConfirmCampaignPresenting = ConfirmCampaignPresenter(coordinator: coordinator)
        let interactor = ConfirmCampaignInteractor(
            service: service,
            presenter: presenter,
            dependencies: container,
            campaign: model)
        
        let viewController = ConfirmCampaignViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
