import Core
import Foundation

protocol ConfirmCampaignServicing {
    func createCampaign(_ campaign: Campaign, completion: @escaping (Result<CampaignResponse?, ApiError>) -> Void)
}

final class ConfirmCampaignService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ConfirmCampaignServicing
extension ConfirmCampaignService: ConfirmCampaignServicing {
    func createCampaign(_ campaign: Campaign, completion: @escaping (Result<CampaignResponse?, ApiError>) -> Void) {
        let endpoint = ConfirmCampaignServiceEndpoint.createCampaign(campaign)
        
        BizApi<CampaignResponse>(endpoint: endpoint).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }
}
