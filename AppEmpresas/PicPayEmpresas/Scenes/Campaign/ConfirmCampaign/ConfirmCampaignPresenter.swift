import Core
import Foundation
import UIKit

private extension ConfirmCampaignPresenter.Layout {
    enum ModalImage {
        static let size = CGSize(width: 80, height: 80)
    }
}

protocol ConfirmCampaignPresenting: AnyObject {
    var viewController: ConfirmCampaignDisplay? { get set }
    func enableCreateButton(_ isEnabled: Bool)
    func didNextStep(action: ConfirmCampaignAction)
    func presentCampaignData(_ campaign: Campaign)
    func startLoading()
    func stopLoading()
    func showError(apiError: ApiError)
    func showError(requestError: RequestError)
}

final class ConfirmCampaignPresenter {
    private typealias Localizable = Strings.ConfirmCampaign
    fileprivate enum Layout { }
    
    private let coordinator: ConfirmCampaignCoordinating
    weak var viewController: ConfirmCampaignDisplay?
    
    init(coordinator: ConfirmCampaignCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ConfirmCampaignPresenting

extension ConfirmCampaignPresenter: ConfirmCampaignPresenting {
    func presentCampaignData(_ campaign: Campaign) {
        var campaignToConfirm = campaign
        campaignToConfirm.expirationDate = formatDurationDate(campaign.expirationDate.onlyNumbers)
        
        viewController?.displayCampaignData(campaignToConfirm)
    }
    
    func enableCreateButton(_ isEnabled: Bool) {
        viewController?.displayCreateButtonEnabled(isEnabled)
    }
    
    func didNextStep(action: ConfirmCampaignAction) {
        coordinator.perform(action: action)
    }
    
    private func formatDurationDate(_ duration: String) -> String {
        var dayComponent = DateComponents()
        dayComponent.day = Int(duration)
        let calendar = Calendar.current
        
        guard let expirationDate = calendar.date(byAdding: dayComponent, to: Date())
            else {
                return duration
        }
        
        let dateFormatted = DateFormatter.custom(value: expirationDate, format: .dayAndMonth)
        
        return "\(duration) \(Strings.CreateCampaign.days) (\(Strings.CreateCampaign.until) \(dateFormatted))"
    }
    
    func startLoading() {
        viewController?.startLoadingView()
    }
    
    func stopLoading() {
        viewController?.stopLoadingView()
    }
    
    func showError(apiError: ApiError) {
        showError(message: apiError.localizedDescription)
    }
    
    func showError(requestError: RequestError) {
        showError(message: requestError.message, title: requestError.title)
    }
}

// MARK: - Private Methods
private extension ConfirmCampaignPresenter {
    func showError(message: String, title: String? = nil) {
        let modalImage = AlertModalInfoImage(
            image: Assets.Emoji.iconBad.image,
            size: Layout.ModalImage.size
        )
        
        let modalInfo = AlertModalInfo(
            imageInfo: modalImage,
            title: title,
            subtitle: message,
            buttonTitle: Localizable.modalButtonTitle
        )
        
        coordinator.perform(action: .createCampaignError(modalInfo: modalInfo))
    }
}
