import Core
import Foundation

enum ConfirmCampaignServiceEndpoint {
    case createCampaign(_ campaign: Campaign)
}

extension ConfirmCampaignServiceEndpoint: ApiEndpointExposable {
    var path: String {
        "/campaigns"
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var body: Data? {
        switch self {
        case let .createCampaign(campaign):
            return
                [
                    "type": campaign.type.rawValue,
                    "cashback": campaign.cashback.onlyNumbers,
                    "max_value": campaign.maxValue,
                    "expiration_date": campaign.expirationDate
                ].toData()
        }
    }
}
