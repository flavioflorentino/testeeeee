import UIKit

enum ConfirmCampaignAction {
    case goToTermsAndConditions
    case goToCampaign(_ campaign: CampaignResponse)
    case createCampaignError(modalInfo: AlertModalInfo)
}

protocol ConfirmCampaignCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ConfirmCampaignAction)
}

final class ConfirmCampaignCoordinator: ConfirmCampaignCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: ConfirmCampaignAction) {
        switch action {
        case .goToTermsAndConditions:
            let controller = TermsAndConditionsFactory.make()
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .createCampaignError(let modalInfo):
            showModalAlert(modalInfo: modalInfo)
        case let .goToCampaign(campaign):
            let controller = ActiveCampaignFactory.make(campaign)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

private extension ConfirmCampaignCoordinator {
    func showModalAlert(modalInfo: AlertModalInfo) {
        let alertPopup = AlertModalFactory.make(alertModalInfo: modalInfo)
        alertPopup.touchButtonAction = { alert in
            alert.dismiss(animated: true)
        }
        showAlert(contentController: alertPopup)
    }
    
    func showAlert(contentController: UIViewController) {
        let popup = PopupViewController()
        popup.contentController = contentController
        viewController?.present(popup, animated: true)
    }
}
