import Foundation

enum CampaignType: String {
    case firstBuy = "first_buy"
    case fixCashback = "general"
}

struct Campaign {
    let type: CampaignType
    let cashback: String
    var maxValue: String
    var expirationDate: String
}
