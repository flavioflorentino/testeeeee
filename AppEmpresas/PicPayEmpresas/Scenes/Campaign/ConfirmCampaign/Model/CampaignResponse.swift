import Foundation

struct CampaignResponse: Decodable {
    let id, sellerId, type: String
    let active: Bool
    let cashback: Int
    let maxValue: Float
    let expirationDate: String
}
