import SnapKit
import UI
import UIKit

struct CampaignItem {
    let icon: UIImage
    let title: String
    let iconSize: CGSize
    let description: String
}

private extension ConfirmCampaignItem.Layout {
    enum Icon {
        static let size = CGSize(width: 27, height: 27)
    }
    
    enum CampaignItem {
        static let height: CGFloat = 72.0
    }
}

final class ConfirmCampaignItem: UIView {
    fileprivate enum Layout {}
    
    // MARK: - Private Properties
    private lazy var containerView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle())
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.border, .light(color: .grayscale100()))
            .with(\.cornerRadius, .medium)
        
        return view
    }()
    
    private lazy var iconImage = UIImageView()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale950())
        
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale950())
        
        return label
    }()
    
    // MARK: - Inits
    convenience init(_ item: CampaignItem) {
        self.init()
        setContent(icon: item.icon, title: item.title, description: item.description, iconSize: item.iconSize)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public Methods
    func setContent(icon: UIImage, title: String, description: String, iconSize: CGSize = Layout.Icon.size) {
        self.iconImage.image = icon
        self.titleLabel.text = title
        self.descriptionLabel.text = description
        
        iconImage.snp.updateConstraints {
            $0.size.equalTo(iconSize)
        }
    }
}

// MARK: - ViewConfiguration
extension ConfirmCampaignItem: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(containerView)
        containerView.addSubview(iconImage)
        containerView.addSubview(titleLabel)
        containerView.addSubview(descriptionLabel)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Layout.CampaignItem.height)
        }
        
        iconImage.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.Icon.size)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.equalTo(iconImage.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalTo(titleLabel)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
}
