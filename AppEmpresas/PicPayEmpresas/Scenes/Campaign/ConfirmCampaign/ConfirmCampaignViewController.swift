import UI
import UIKit
import AssetsKit

protocol ConfirmCampaignDisplay: AnyObject, LoadingViewProtocol {
    func displayCampaignData(_ data: Campaign)
    func displayCreateButtonEnabled(_ isEnabled: Bool)
}

private extension ConfirmCampaignViewController.Layout {
    enum ConfirmCampaignItem {
        static let height: CGFloat = 72.0
    }
    
    enum DurationIcon {
        static let size = CGSize(width: 30.0, height: 19.0)
    }
}

final class ConfirmCampaignViewController: ViewController<ConfirmCampaignInteracting, UIView> {
    private typealias Localizable = Strings.ConfirmCampaign
    fileprivate enum Layout { }
    
    lazy var loadingView = LoadingView()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.confirmTitle
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .grayscale950())
        
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.confirmDescription
        label
            .labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale500())
        
        return label
    }()
    
    private lazy var campaignTypeItem = ConfirmCampaignItem()
    private lazy var percentageItem = ConfirmCampaignItem()
    private lazy var maxValueItem = ConfirmCampaignItem()
    private lazy var durationItem = ConfirmCampaignItem()
    
    private lazy var termsAndConditionsTextView: UITextView = {
        let textView = TextView()
        textView.isScrollEnabled = false
        textView.dataDetectorTypes = [.link]
        textView.isEditable = false
        textView.backgroundColor = .clear
        textView.delegate = self
        textView.linkTextAttributes = [
            .foregroundColor: Colors.branding600.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        textView.attributedText = getAttributedTerms()
        return textView
    }()
    
    private lazy var checkboxButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.Checkbox.checkboxUncheck.image, for: .normal)
        button.setImage(Assets.Checkbox.checkboxCheck.image, for: .selected)
        button.setContentHuggingPriority(.required, for: .horizontal)
        button.isSelected = false
        button.addTarget(self, action: #selector(termsAndConditionsCheckboxButtonPressed), for: .touchUpInside)
        return button
    }()
    
    private lazy var termsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [checkboxButton, termsAndConditionsTextView])
        stackView.spacing = Spacing.base03
        stackView.alignment = .top
        
        return stackView
    }()
    
    private lazy var createButton: UIButton = {
        let button = UIButton(type: .custom)
        button.isEnabled = false
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.createCampaign, for: .normal)
        button.addTarget(self, action: #selector(createButtonPressed), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setCampaignData()
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        title = Localizable.viewTitle
    }
    
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(campaignTypeItem)
        view.addSubview(percentageItem)
        view.addSubview(maxValueItem)
        view.addSubview(durationItem)
        view.addSubview(termsStackView)
        view.addSubview(createButton)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        campaignTypeItem.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.ConfirmCampaignItem.height)
        }
        
        percentageItem.snp.makeConstraints {
            $0.top.equalTo(campaignTypeItem.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.ConfirmCampaignItem.height)
        }
        
        maxValueItem.snp.makeConstraints {
            $0.top.equalTo(percentageItem.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.ConfirmCampaignItem.height)
        }
        
        durationItem.snp.makeConstraints {
            $0.top.equalTo(maxValueItem.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.ConfirmCampaignItem.height)
        }
        
        termsStackView.snp.makeConstraints {
            $0.top.equalTo(durationItem.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        createButton.snp.makeConstraints {
            $0.bottom.equalToSuperview().inset(Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
}

private extension ConfirmCampaignViewController {
    func getAttributedTerms() -> NSMutableAttributedString {
        let legalTermsText = Localizable.termsAndConditions as NSString
        let termsAndConditionsRange = legalTermsText.range(of: Localizable.termsAndConditionsLink)
        let attributedString = NSMutableAttributedString(
            string: Localizable.termsAndConditions,
            attributes: [.font: Typography.bodySecondary().font(), .foregroundColor: Colors.grayscale600.color]
        )
        
        attributedString.addAttribute(.link, value: "", range: termsAndConditionsRange)
        
        return attributedString
    }
}

@objc
private extension ConfirmCampaignViewController {
    func createButtonPressed() {
        interactor.createCampaign()
    }
    func termsAndConditionsCheckboxButtonPressed() {
        let isSelected = !checkboxButton.isSelected
        interactor.checkboxSelected(isSelected)
    }
}

// MARK: ConfirmCampaignDisplay
extension ConfirmCampaignViewController: ConfirmCampaignDisplay {
    func displayCampaignData(_ data: Campaign) {
        campaignTypeItem.setContent(
            icon: Resources.Icons.icoTag.image,
            title: Localizable.campaignType,
            description: data.type == .firstBuy ? Localizable.firstBuy : Localizable.fixCashback)
        
        percentageItem.setContent(
            icon: Resources.Icons.icoCurrency.image,
            title: Localizable.cashbackValue,
            description: data.cashback)
        
        maxValueItem.setContent(
            icon: Resources.Icons.icoMoney.image,
            title: Localizable.maxPayment,
            description: data.maxValue,
            iconSize: Layout.DurationIcon.size)
        
        durationItem.setContent(
            icon: Resources.Icons.icoWatch.image,
            title: Localizable.campaignDuration,
            description: data.expirationDate)
    }
    
    func displayCreateButtonEnabled(_ isEnabled: Bool) {
        checkboxButton.isSelected = isEnabled
        createButton.isEnabled = isEnabled
    }
}

extension ConfirmCampaignViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if textView == termsAndConditionsTextView {
            interactor.showTermsAndConditions()
        }
        return false
    }
}
