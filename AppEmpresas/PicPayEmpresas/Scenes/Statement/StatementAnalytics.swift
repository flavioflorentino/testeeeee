import AnalyticsModule
import Foundation

enum StatementAnalytics: AnalyticsKeyProtocol {
    case pixTouched(sellerID: String, companyName: String)

    private var name: String {
        switch self {
        case .pixTouched:
            return "Pix - Payment Receipt Options"
        }
    }

    private var properties: [String: Any] {
        switch self {
        case let .pixTouched(sellerID, companyName):
            return [
                "seller_id": sellerID,
                "user_name": companyName
            ]
        }
    }

    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
