import SnapKit
import UI
import UIKit

protocol StatementCellDisplay: AnyObject {
    func setup(title: String, description: String, balance: String)
    func setupIcon(_ icon: UIImage)
    func setupArrowLabel(isHidden: Bool)
    func hideStatus()
    func showStatus(text: String, color: UIColor)
}

private extension StatementCell.Layout {
    enum Icon {
        static let size = CGSize(width: 24, height: 24)
    }

    enum AccessoryImage {
        static let size = CGSize(width: 8, height: 16)
    }
}

final class StatementCell: UITableViewCell {
    // MARK: - Private Properties

    private typealias Localizable = Strings.Statement
    fileprivate enum Layout {}

    private lazy var iconImageView = UIImageView()

    private lazy var titleDescriptionStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, descriptionLabel])
        stackView.spacing = Spacing.base00
        stackView.axis = .vertical
        stackView.alignment = .leading
        return stackView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
        label.numberOfLines = 1
        label.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        label.numberOfLines = 1
        label.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        return label
    }()

    private lazy var statusPriceStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [statusLabel, balanceLabel])
        stackView.spacing = Spacing.base00
        stackView.axis = .vertical
        return stackView
    }()

    private lazy var statusLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .right)
        label.numberOfLines = 1
        return label
    }()

    private lazy var balanceLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale400())
            .with(\.textAlignment, .right)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var arrowLabel: UILabel = {
        let label = UILabel()
        label.text = Iconography.angleRightB.rawValue
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, Colors.branding600.color)
        return label
    }()

    private lazy var lineShape: CAShapeLayer = {
        let shape = CAShapeLayer()
        shape.lineWidth = 1
        shape.strokeColor = Colors.grayscale200.color.cgColor
        return shape
    }()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func draw(_ rect: CGRect) {
        drawLine()
    }
}

// MARK: - StatementCellDisplay

extension StatementCell: StatementCellDisplay {
    func setup(title: String, description: String, balance: String) {
        titleLabel.text = title
        descriptionLabel.text = description
        balanceLabel.text = balance
    }
    
    func setupIcon(_ icon: UIImage) {
        iconImageView.image = icon
    }
    
    func setupArrowLabel(isHidden: Bool) {
        arrowLabel.isHidden = isHidden
    }
    
    func hideStatus() {
        statusLabel.isHidden = true
        statusLabel.text = ""
    }
    
    func showStatus(text: String, color: UIColor) {
        statusLabel.isHidden = false
        statusLabel.text = text
        statusLabel.textColor = color
    }
}

// MARK: - Private Methods

private extension StatementCell {
    func drawLine() {
        contentView.layer.addSublayer(lineShape)

        let centerX = iconImageView.center.x
        let path = UIBezierPath()
        path.move(to: CGPoint(x: centerX, y: contentView.frame.minY))
        path.addLine(to: CGPoint(x: centerX, y: iconImageView.frame.minY))
        path.move(to: CGPoint(x: centerX, y: iconImageView.frame.maxY))
        path.addLine(to: CGPoint(x: centerX, y: contentView.frame.maxY))
        path.close()

        lineShape.path = path.cgPath
    }
}

// MARK: - ViewConfiguration

extension StatementCell: ViewConfiguration {
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }

    func setupConstraints() {
        iconImageView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.centerY.equalToSuperview()
            $0.size.equalTo(Layout.Icon.size)
        }

        titleDescriptionStackView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
        }

        statusPriceStackView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.greaterThanOrEqualTo(titleDescriptionStackView.snp.trailing).offset(Spacing.base01)
        }
        
        arrowLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(statusPriceStackView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base01)
        }
    }

    func buildViewHierarchy() {
        contentView.addSubview(iconImageView)
        contentView.addSubview(titleDescriptionStackView)
        contentView.addSubview(statusPriceStackView)
        contentView.addSubview(arrowLabel)
    }
}
