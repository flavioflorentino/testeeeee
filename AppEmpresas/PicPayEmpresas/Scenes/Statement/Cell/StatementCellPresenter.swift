import UI
import UIKit
import Foundation

protocol StatementCellPresenting: AnyObject {
    var cell: StatementCellDisplay? { get set }
    
    func show(item: StatementItem)
}

final class StatementCellPresenter: StatementCellPresenting {
    private typealias Localizable = Strings.Statement
    
    weak var cell: StatementCellDisplay?
    
    func show(item: StatementItem) {
        let balance = item.value.toCurrencyString() ?? ""
        cell?.setup(title: item.title, description: item.description, balance: balance)
        
        setupAccessoryImageView(typeTransaction: item.typeTransaction)
        setupIcon(itemType: item.typeItem, typeTransaction: item.typeTransaction)
        setupStatus(item.status)
    }
}

private extension StatementCellPresenter {
    func setupIcon(itemType: StatementItem.TypeItem, typeTransaction: StatementItem.TypeTransaction?) {
        let isInput = itemType == .input
        var icon = isInput ? Assets.Statements.iconCashin.image : Assets.Statements.iconCashout.image
        
        let isAutomaticWithdraw = typeTransaction == .automaticWithdrawal
        if isAutomaticWithdraw {
            icon = Assets.Statements.iconAutomaticWithdrawal.image
        }
        
        cell?.setupIcon(icon)
    }
    
    func setupAccessoryImageView(typeTransaction: StatementItem.TypeTransaction?) {
        guard let typeTransaction = typeTransaction else {
            cell?.setupArrowLabel(isHidden: true)
            return
        }
        
        var availableTypes: [StatementItem.TypeTransaction] = [.salesRecipe, .judicialBlockTransferred]
        availableTypes.append(contentsOf: [.pixRefund, .pixReceive, .pixTransfer, .pixTransferRefund])
        
        let shouldHide = !availableTypes.contains(typeTransaction)
        cell?.setupArrowLabel(isHidden: shouldHide)
    }
    
    func setupStatus(_ status: StatementItem.Status) {
        switch status {
        case .approved, .finished:
            cell?.hideStatus()
        case .processing:
            cell?.showStatus(text: Localizable.processing, color: Colors.neutral800.color)
        case .cancelled:
            cell?.showStatus(text: Localizable.canceled, color: Colors.notification600.color)
        }
    }
}
