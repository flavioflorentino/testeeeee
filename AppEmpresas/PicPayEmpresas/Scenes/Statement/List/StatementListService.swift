import Core
import Foundation

protocol StatementListServicing {
    func fetchStatement(_ pagination: StatementPagination?, completion: @escaping(Result<Statement?, ApiError>) -> Void)
    func fetchTax(of day: String, completion: @escaping(Result<StatementTax?, ApiError>) -> Void)
    func fetchBalance(_ completion: @escaping(Result<WalletBalanceItem?, ApiError>) -> Void)
}

final class StatementListService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - StatementListServicing
extension StatementListService: StatementListServicing {
    func fetchStatement(_ pagination: StatementPagination? = nil, completion: @escaping(Result<Statement?, ApiError>) -> Void) {
        let endpoint = StatementListEndpoint.fetchStatement(pagination)
        BizApi<Statement>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }

    func fetchTax(of day: String, completion: @escaping(Result<StatementTax?, ApiError>) -> Void) {
        let endpoint = StatementListEndpoint.statementTax(of: day)
        BizApi<StatementTax>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }

    func fetchBalance(_ completion: @escaping(Result<WalletBalanceItem?, ApiError>) -> Void) {
        BizApi<WalletBalanceItem>(endpoint: WalletBalanceEndpoint.getBalance).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }
}
