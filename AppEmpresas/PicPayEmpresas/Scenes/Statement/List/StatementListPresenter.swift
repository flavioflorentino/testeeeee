import Foundation

protocol StatementListPresenting: AnyObject {
    var viewController: StatementListDisplay? { get set }
    func didNextStep(action: StatementListAction)
    func showInitialStatements(_ statement: Statement)
    func showFutureAmount(_ amount: String, date: Date)
    func showNoFutureAmount()
    func showMoreStatements(_ statementItens: [StatementSection])
    func setLoading(_ isLoading: Bool)
    func setApolloLoading(_ isLoading: Bool)
    func setActivity(_ isLoading: Bool)
    func showSalesRecipeTaxesModal(taxes: Double)
    func showTaxPopUpError()
    func showEmptyState()
    func showInitialError()
    func showTryAgainComponent()
    func showBalance(_ balance: String)
    func showBlockedBalance(_ blockedBalance: String)
    func showBalancePopUpError()
    func removeErrorViewController()
}

final class StatementListPresenter: StatementListPresenting {
    private typealias Localizable = Strings.Statement
    
    private let coordinator: StatementListCoordinating
    weak var viewController: StatementListDisplay?

    init(coordinator: StatementListCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - StatementListPresenting

extension StatementListPresenter {
    func didNextStep(action: StatementListAction) {
        coordinator.perform(action: action)
    }

    func showInitialStatements(_ statement: Statement) {
        viewController?.displayInitialSections(sections: statement.extract)
    }
    
    func showFutureAmount(_ amount: String, date: Date) {
        viewController?.displayFutureAmount(amount, date: date)
    }
    
    func showNoFutureAmount() {
        viewController?.displayNoFutureBalance()
    }

    func showMoreStatements(_ statementItens: [StatementSection]) {
        viewController?.displayInitialSections(sections: statementItens)
    }

    func setLoading(_ isLoading: Bool) {
        isLoading ? viewController?.showLoading() : viewController?.hideLoading()
    }
    
    func setApolloLoading(_ isLoading: Bool) {
        isLoading ? viewController?.showApolloLoading() : viewController?.hideApolloLoading()
    }
    
    func setActivity(_ isLoading: Bool) {
        isLoading ? viewController?.showActivityIndicator() : viewController?.hideActivityIndicator()
    }

    func showSalesRecipeTaxesModal(taxes: Double) {
        let title = Localizable.details
        let taxesCurrency = taxes.toCurrencyString() ?? ""
        let text = Localizable.salesRecipeModalText(taxesCurrency)
        let buttonText = Localizable.modalButtonTitle
        let alertModalInfo = AlertModalInfo(title: title, subtitle: text, buttonTitle: buttonText)
        coordinator.perform(action: .salesRecipe(modalInfo: alertModalInfo))
    }

    func showTaxPopUpError() {
        let title = Localizable.taxErrorTitle
        let message = Localizable.taxErrorMessage
        let buttonText = Localizable.modalButtonTitle
        let info = AlertModalInfo(title: title, subtitle: message, buttonTitle: buttonText)
        coordinator.perform(action: .popUpError(modalInfo: info))
    }

    func showEmptyState() {
        viewController?.displayEmptyState()
    }

    func showInitialError() {
        viewController?.displayErrorView()
    }

    func showTryAgainComponent() {
        viewController?.showTryAgainComponent()
    }

    func showBalance(_ balance: String) {
        viewController?.displayAvailableAmount(balance.currency())
    }
    
    func showBlockedBalance(_ blockedBalance: String) {
        viewController?.displayBlockedBalance(blockedBalance.currency())
    }

    func showBalancePopUpError() {
        let title = Localizable.balanceErrorTitle
        let message = Localizable.balanceErrorMessage
        let buttonText = Localizable.modalButtonTitle
        let info = AlertModalInfo(title: title, subtitle: message, buttonTitle: buttonText)
        coordinator.perform(action: .popUpError(modalInfo: info))
    }
    
    func removeErrorViewController() {
        viewController?.removeErrorViewController()
    }
}
