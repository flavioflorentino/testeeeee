import SnapKit
import UI

protocol StatementListDisplay: AnyObject {
    func showLoading()
    func hideLoading()
    func showApolloLoading()
    func hideApolloLoading()
    func displayAvailableAmount(_ value: String)
    func displayFutureAmount(_ amount: String, date: Date)
    func displayNoFutureBalance()
    func displayBlockedBalance(_ value: String)
    func displayInitialSections(sections: [StatementSection])
    func displayEmptyState()
    func displayErrorView()
    func showActivityIndicator()
    func hideActivityIndicator()
    func showTryAgainComponent()
    func removeErrorViewController()
}

private extension StatementListViewController.Layout {
    enum TableView {
        static let estimatedRowHeight: CGFloat = 160
    }

    enum SectionHeader {
        static let height: CGFloat = 32
    }
}

final class StatementListViewController: ViewController<StatementListInteractorInputs, UIView> {
    private typealias Localizable = Strings.Statement

    fileprivate enum Layout { }

    lazy var loadingView = LoadingView()

    private lazy var errorViewController: ApolloFeedbackViewController = {
        let content = ApolloFeedbackViewContent(
            image: Assets.Statements.statementError.image,
            title: Localizable.firstErrorTitle,
            description: NSAttributedString(string: Localizable.firstErrorMsg),
            primaryButtonTitle: Localizable.firstErrorButtonMsg
        )
        let errorViewController = ApolloFeedbackViewController(content: content)
        errorViewController.didTapPrimaryButton = interactor.retryFirstPage
        return errorViewController
    }()

    private lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(refreshStatement), for: .valueChanged)
        return control
    }()

    private lazy var emptyView = StatementListEmptyView()
    private lazy var headerView = StatementListHeaderView()
    private lazy var footerView = StatementListFooterView()

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.estimatedRowHeight = Layout.TableView.estimatedRowHeight
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = Layout.SectionHeader.height
        tableView.sectionFooterHeight = CGFloat.leastNormalMagnitude
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = footerView
        tableView.refreshControl = refreshControl
        tableView.register(
            StatementCell.self,
            forCellReuseIdentifier: StatementCell.identifier)
        tableView.register(
            StatementListSectionHeaderView.self,
            forHeaderFooterViewReuseIdentifier: StatementListSectionHeaderView.identifier
        )
        return tableView
    }()

    private var dataSource: TableViewHandler<StatementHeader, StatementItem, StatementCell>?
    
    // MARK: - Inits
    
    init(interactor: StatementListInteractorInputs, headerViewDelegate: StatementListHeaderViewDelegate) {
        super.init(interactor: interactor)
        headerView.delegate = headerViewDelegate
    }

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.initialFetch()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateTableViewHeaderAndFooter()
    }

    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }

    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        title = Localizable.statementsTitle
        footerView.delegate = self
        setupNavigationBar()
    }
}

// MARK: - Private Methods

private extension StatementListViewController {
    func setupNavigationBar() {
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceDefault())
        let image = Assets.Statements.iconExport.image
        let item = UIBarButtonItem(image: image, style: .done, target: self, action: #selector(exportStatements))
        navigationItem.rightBarButtonItem = item
    }

    @objc
    func exportStatements() {
        interactor.exportStatements()
    }

    @objc
    func refreshStatement() {
        interactor.refresh()
    }

    func bindCell(_ row: Int, _ model: StatementItem, _ cell: StatementCell) {
        let cellPresenter: StatementCellPresenting = StatementCellPresenter()
        cellPresenter.cell = cell
        cellPresenter.show(item: model)
    }

    func bindSection(_ section: Int, _ model: Section<StatementHeader, StatementItem>) -> Header? {
        let identifier = StatementListSectionHeaderView.identifier
        guard let header = model.header,
            let headerView = tableView
                .dequeueReusableHeaderFooterView(withIdentifier: identifier) as? StatementListSectionHeaderView
            else {
                return nil
        }
        headerView.setup(header)
        return .view(headerView)
    }

    func didSelectRow(_ indexPath: IndexPath, _ model: StatementItem) {
        tableView.deselectRow(at: indexPath, animated: true)
        interactor.statementItemSelected(statementItem: model)
    }

    func willDisplayCell(_ indexPath: IndexPath, _ model: StatementItem) {
        interactor.fetchMoreItensIfNeeded(indexPath)
    }

    func updateTableViewHeaderAndFooter() {
        updateView(view: headerView)
        tableView.tableHeaderView = headerView

        updateView(view: footerView)
        tableView.tableFooterView = footerView
    }

    func updateView(view: UIView) {
        let viewHeight = view.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        if viewHeight != view.frame.size.height {
            view.frame.size.height = viewHeight
        }
    }
}

// MARK: StatementListDisplay

extension StatementListViewController: StatementListDisplay {
    func showLoading() {
        startLoadingView()
    }

    func hideLoading() {
        refreshControl.endRefreshing()
        stopLoadingView()
    }
    
    func showApolloLoading() {
        startApolloLoader()
    }
    
    func hideApolloLoading() {
        refreshControl.endRefreshing()
        stopApolloLoader()
    }

    func showActivityIndicator() {
        footerView.showLoading()
        updateTableViewHeaderAndFooter()
    }

    func hideActivityIndicator() {
        footerView.hideLoading()
    }

    func displayAvailableAmount(_ value: String) {
        headerView.setAvailableBalance(value)
    }

    func displayFutureAmount(_ amount: String, date: Date) {
        headerView.setFutureBalance(amount, date: date)
    }

    func displayNoFutureBalance() {
        headerView.setNoFutureBalance()
    }
    
    func displayBlockedBalance(_ value: String) {
        headerView.setBlockedBalance(value: value)
    }

    func displayInitialSections(sections: [StatementSection]) {
        let data: [Section<StatementHeader, StatementItem>] = sections.map { Section(header: $0.header, items: $0.itens) }
        dataSource = TableViewHandler(
            data: data,
            cellType: StatementCell.self,
            configureCell: bindCell,
            configureSection: bindSection,
            configureDidSelectRow: didSelectRow,
            willDisplayCellHandler: willDisplayCell
        )
        tableView.delegate = dataSource
        tableView.dataSource = dataSource
        tableView.reloadData()
    }

    func displayEmptyState() {
        headerView.showEmptyState()
        view.addSubview(emptyView)
        view.bringSubviewToFront(emptyView)
        
        emptyView.snp.makeConstraints {
            $0.top.equalTo(headerView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }

    func displayErrorView() {
        addChild(errorViewController)
        view.addSubview(errorViewController.view)
        errorViewController.didMove(toParent: self)
        view.bringSubviewToFront(errorViewController.view)
        errorViewController.view.snp.makeConstraints {
            $0.edges.equalTo(view.compatibleLayoutMargins.asUIEdgeInsets)
        }
    }
    
    func removeErrorViewController() {
        errorViewController.view.removeFromSuperview()
        errorViewController.removeFromParent()
    }

    func showTryAgainComponent() {
        footerView.showTryAgainComponent()
        updateTableViewHeaderAndFooter()
    }
}

// MARK: - FooterViewDelegate

extension StatementListViewController: StatementListFooterViewDelegate {
    func didTapTryAgain() {
        interactor.retryNextPage()
    }
}

// MARK: - LoadingViewProtocol

extension StatementListViewController: LoadingViewProtocol { }

extension StatementListViewController: ApolloLoadable { }
