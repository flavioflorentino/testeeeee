import AnalyticsModule
import Core
import Foundation
import LegacyPJ

protocol StatementListInteractorInputs: AnyObject {
    func initialFetch()
    func exportStatements()
    func statementItemSelected(statementItem: StatementItem)
    func fetchMoreItensIfNeeded(_ indexPath: IndexPath)
    func retryNextPage()
    func refresh()
    func retryFirstPage()
}

final class StatementListInteractor {
    typealias Dependencies = HasMainQueue & HasAnalytics & HasAuthManager & HasDispatchGroup

    private let dependencies: Dependencies
    private let service: StatementListServicing
    private let presenter: StatementListPresenting

    private var statementSections: [StatementSection] = []
    private var fetchingNewStatements = false
    private var pagination: StatementPagination?

    init(service: StatementListServicing, presenter: StatementListPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - StatementListViewModelInputs

extension StatementListInteractor: StatementListInteractorInputs {
    func initialFetch() {
        var statement: Statement?
        var balance: WalletBalanceItem?
        let group = dependencies.dispatchGroup
        
        presenter.setLoading(true)
        
        group.enter()
        fetchInitialStatements { fetchedStatement in
            statement = fetchedStatement
            group.leave()
        }
        
        group.enter()
        fetchWalletBalance { fetchedBalance in
            balance = fetchedBalance
            group.leave()
        }
        
        group.notify(queue: dependencies.mainQueue) {
            self.presenter.setLoading(false)
            self.show(statement: statement, balance: balance)
        }
    }

    func exportStatements() {
        presenter.didNextStep(action: .exportStatements)
    }

    func fetchMoreItensIfNeeded(_ indexPath: IndexPath) {
        let lastSection = statementSections.count - 1
        guard var lastRow = statementSections.last?.itens.count else { return }
        lastRow -= 1
        
        // Check if we're in the last section and in the last row
        if indexPath.section == lastSection, indexPath.row == lastRow {
            fetchOthersPages()
        }
    }

    func retryNextPage() {
        fetchOthersPages()
    }
    
    func retryFirstPage() {
        presenter.removeErrorViewController()
        refresh()
    }

    func statementItemSelected(statementItem: StatementItem) {
        switch statementItem.typeTransaction {
        case .salesRecipe:
            loadTax(forItem: statementItem)
        case .judicialBlockTransferred:
            presenter.didNextStep(action: .blockedTransferBalance)
        case .pixRefund, .pixTransfer, .pixTransferRefund:
            navigateToPixReceipt(item: statementItem, isRefundAllowed: false)
        case .pixReceive:
            navigateToPixReceipt(item: statementItem, isRefundAllowed: true)
        default:
            break
        }
    }

    func refresh() {
        pagination = nil
        initialFetch()
    }
}

// MARK: - Private

private extension StatementListInteractor {
    func fetchWalletBalance(completion: @escaping(WalletBalanceItem?) -> Void) {
        service.fetchBalance { result in
            switch result {
            case .success(let balance):
                completion(balance)
            case .failure:
                completion(nil)
            }
        }
    }
    
    func fetchInitialStatements(completion: @escaping(Statement?) -> Void) {
        fetch { result in
            switch result {
            case .success(let statement):
                completion(statement)
            case .failure:
                completion(nil)
            }
        }
    }

    func fetchOthersPages() {
        if fetchingNewStatements || pagination == nil || pagination?.nextPage == nil {
            return
        }
        presenter.setActivity(true)

        fetch(pagination) { [weak self] result in
            guard let self = self else { return }
            
            self.presenter.setActivity(false)
            switch result {
            case .success(let statement):
                self.statementSections = self.mergeStatements(statement.extract)
                self.presenter.showMoreStatements(self.statementSections)
            case .failure:
                self.presenter.showTryAgainComponent()
            }
        }
    }

    func fetch(_ pagination: StatementPagination? = nil, completion: @escaping (Result<Statement, Error>) -> Void) {
        fetchingNewStatements = true

        service.fetchStatement(pagination) { [weak self] result in
            self?.fetchingNewStatements = false
            switch result {
            case .success(let statement):
                guard let statement = statement else {
                    completion(.failure(LegacyPJError.requestError))
                    return
                }
                self?.pagination = statement.pagination
                completion(.success(statement))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func mergeStatements(_ sections: [StatementSection]) -> [StatementSection] {
        guard
            let newSectionDay = sections.first?.itens.first?.day,
            let lastSectionDay = statementSections.last?.itens.last?.day,
            newSectionDay == lastSectionDay else {
                return statementSections + sections
        }

        var currentSections = statementSections
        guard let lastSection = currentSections.popLast() else {
            return statementSections
        }

        var newSections = sections
        let firstNewSections = newSections.removeFirst()
        let newItens = lastSection.itens + firstNewSections.itens
        let mergedSection = [StatementSection(header: lastSection.header, itens: newItens)]

        return currentSections + mergedSection + newSections
    }
    
    func show(statement: Statement?, balance: WalletBalanceItem?) {
        guard let statement = statement else {
            presenter.showInitialError()
            return
        }
        
        showStatement(statement)

        guard let balance = balance else {
            presenter.showBalancePopUpError()
            return
        }

        showBalance(balance)
    }
    
    func showStatement(_ statement: Statement) {
        if statement.extract.isEmpty {
            presenter.showEmptyState()
        } else {
            statementSections = statement.extract
            presenter.showInitialStatements(statement)
        }
        
        guard let amount = statement.futureReleases?.value.toCurrencyString(),
            let date = statement.futureReleases?.date else {
            presenter.showNoFutureAmount()
            return
        }
        
        presenter.showFutureAmount(amount, date: date)
    }
    
    func showBalance(_ balance: WalletBalanceItem) {
        presenter.showBalance(balance.available)

        if let blockedBalance = balance.blocked {
            presenter.showBlockedBalance(blockedBalance)
        }
    }
    
    func loadTax(forItem item: StatementItem) {
        presenter.setApolloLoading(true)

        service.fetchTax(of: item.day) { [weak self] result in
            self?.presenter.setApolloLoading(false)
            switch result {
            case .success(let tax):
                guard let tax = tax else {
                    self?.presenter.showTaxPopUpError()
                    return
                }

                self?.presenter.showSalesRecipeTaxesModal(taxes: tax.tax)
            case .failure:
                self?.presenter.showTaxPopUpError()
            }
        }
    }
    
    func navigateToPixReceipt(item: StatementItem, isRefundAllowed: Bool) {
        let user = dependencies.authManager.user
        let event = StatementAnalytics.pixTouched(sellerID: "\(user?.id ?? 0)", companyName: user?.name ?? "")
        dependencies.analytics.log(event)
        presenter.didNextStep(action: .pixReceipt(movementCode: item.movementCode, transactionId: item.id, isRefundAllowed: isRefundAllowed))
    }
}

// MARK: - StatementListHeaderViewDelegate

extension StatementListInteractor: StatementListHeaderViewDelegate {
    func didTapHeaderView() {
        presenter.didNextStep(action: .futureMovements)
    }
    
    func didTapBlockedBalance() {
        presenter.didNextStep(action: .blockedBalance)
    }
}
