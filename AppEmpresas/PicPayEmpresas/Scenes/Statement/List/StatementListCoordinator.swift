import CustomerSupport
import UI
import UIKit
import PIX
import FeatureFlag

enum StatementListAction: Equatable {
    case exportStatements
    case salesRecipe(modalInfo: AlertModalInfo)
    case popUpError(modalInfo: AlertModalInfo)
    case futureMovements
    case blockedBalance
    case blockedTransferBalance
    case pixReceipt(movementCode: Int?, transactionId: String, isRefundAllowed: Bool)
}

protocol StatementListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: StatementListAction)
}

final class StatementListCoordinator {
    private typealias BlockedLocalizable = Strings.JudicialBlockade
    typealias Dependencies = HasFeatureManager
    
    weak var viewController: UIViewController?
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - StatementListCoordinating

extension StatementListCoordinator: StatementListCoordinating {
    func perform(action: StatementListAction) {
        switch action {
        case .exportStatements:
            let exportMovementsViewController: ExportMovementsViewController = ViewsManager.instantiateViewController(.movement)
            exportMovementsViewController.hidesBottomBarWhenPushed = true
            viewController?.navigationController?.pushViewController(exportMovementsViewController, animated: true)
        case let .salesRecipe(modalInfo), let .popUpError(modalInfo):
            let alertController = AlertModalFactory.make(alertModalInfo: modalInfo)
            alertController.touchButtonAction = { alert in
                alert.dismiss(animated: true)
            }
            let popup = PopupViewController()
            popup.hasCloseButton = false
            popup.contentController = alertController
            viewController?.navigationController?.present(popup, animated: true)
        case .futureMovements:
            let controller: MovementsTableViewController = ViewsManager.instantiateViewController(.movement)
            controller.setup(model: MovementListFutureViewModel())
            controller.title = Strings.Statement.futureBalance
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .blockedBalance:
            showModal(title: BlockedLocalizable.blockedModalTitle, text: BlockedLocalizable.blockedModalText)
        case .blockedTransferBalance:
            showModal(title: BlockedLocalizable.transferModalTitle, text: BlockedLocalizable.transferModalText)
        case let .pixReceipt(movementCode, transactionId, isRefundAllowed):
            navigateToPixReceipt(movementCode: movementCode, transactionId: transactionId, isRefundAllowed: isRefundAllowed)
        }
    }
}

// MARK: - Private Methods

private extension StatementListCoordinator {
    func showModal(title: String, text: String) {
        let controller = UI.PopupViewController(
            title: title,
            description: text,
            preferredType: .business
        )
        
        let confirmAction = PopupAction(title: Strings.JudicialBlockade.modalButtonTitle, style: .fill)
        
        let understandAction = PopupAction(title: Strings.JudicialBlockade.modalSecondaryButtonTitle, style: .link) {
            self.navigateToJudicialBlockadeDetails()
        }
        
        controller.addAction(confirmAction)
        controller.addAction(understandAction)
        
        viewController?.showPopup(controller)
    }
    
    func navigateToJudicialBlockadeDetails() {
        let faqID = dependencies.featureManager.text(.faqArticleJudicialBlockade)
        let options = FAQOptions.article(id: faqID)
        let faqViewController = FAQFactory.make(option: options)
        let navController = UINavigationController(rootViewController: faqViewController)
        viewController?.navigationController?.present(navController, animated: true, completion: nil)
    }
    
    func navigateToPixReceipt(movementCode: Int?, transactionId: String, isRefundAllowed: Bool) {
        guard let navigation = viewController?.navigationController else { return }
        
        let coordinator = PIXBizFlowCoordinator(with: navigation)
        coordinator.perform(flow: .newReceipt(movementCode: movementCode, transactionId: transactionId, isRefundAllowed: isRefundAllowed))
    }
}
