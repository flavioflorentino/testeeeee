import Core

enum StatementListEndpoint {
    case fetchStatement(_ pagination: StatementPagination?)
    case statementTax(of: String)
}

extension StatementListEndpoint: ApiEndpointExposable {
    var method: HTTPMethod {
        switch self {
        case .fetchStatement:
            return .get
        case .statementTax:
            return .post
        }
    }

    var path: String {
        switch self {
        case .fetchStatement:
            return "/extract"
        case .statementTax:
            return "/extract/sales-detail"
        }
    }

    var parameters: [String: Any] {
        guard
            case let StatementListEndpoint.fetchStatement(nextPage) = self,
            let pagination = nextPage,
            let page = pagination.nextPage,
            let lastDate = pagination.lastDate
            else {
                return [:]
        }

        return [
            Parameters.lastDate: lastDate,
            Parameters.perPage: pagination.perPage,
            Parameters.page: "\(page)"
        ]
    }

    var body: Data? {
        switch self {
        case .statementTax(let day):
            return [Parameters.day: day].toData()
        default:
            return nil
        }
    }
}

private extension StatementListEndpoint {
    enum Parameters {
        static let lastDate = "last_date"
        static let perPage = "per_page"
        static let page = "page"
        static let day = "day"
    }
}
