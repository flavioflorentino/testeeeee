import Foundation

enum StatementListFactory {
    static func make() -> StatementListViewController {
        let container = DependencyContainer()
        let service: StatementListServicing = StatementListService(dependencies: container)
        let coordinator: StatementListCoordinating = StatementListCoordinator(dependencies: container)
        let presenter: StatementListPresenting = StatementListPresenter(coordinator: coordinator)
        let interactor = StatementListInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = StatementListViewController(interactor: interactor, headerViewDelegate: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
