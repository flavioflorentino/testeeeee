import Foundation

struct Statement: Decodable {
    private enum CodingKeys: String, CodingKey {
        case extract
        case futureReleases = "future_releases"
        case pagination
    }

    let extract: [StatementSection]
    let futureReleases: StatementFutureReleases?
    let pagination: StatementPagination
}

struct StatementSection: Decodable {
    let header: StatementHeader
    let itens: [StatementItem]
}

struct StatementHeader: Decodable {
    let balance: Double
    let date: Date
}

struct StatementFutureReleases: Decodable {
    let date: Date
    let value: Double
}

struct StatementItem: Decodable {
    private enum CodingKeys: String, CodingKey {
        case day
        case description
        case id
        case status
        case title
        case typeItem = "type_item"
        case typeTransaction = "type_transaction"
        case value
        case movementCode = "movement_code_id"
    }

    enum TypeItem: String, Decodable {
        case output
        case input
    }

    enum TypeTransaction: String, Decodable {
        case automaticWithdrawal = "automatic_withdrawal"
        case bankWithdrawal = "bank_withdrawal"
        case bills
        case picpayWithdrawal = "picpay_withdrawal"
        case salesRecipe = "sales_recipe"
        case salesCancel = "sales_cancel"
        case refundChargeback = "refund_chargeback"
        case judicialBlockTransferred = "judicial_block_transferred"
        case recharge
        case pixReceive = "pix_receivement"
        case pixRefund = "pix_refund_receivement"
        case pixTransfer = "pix_transfer"
        case pixTransferRefund = "pix_refund_transfer"
        case unknown

        init(from decoder: Decoder) throws {
            let containter = try decoder.singleValueContainer()
            let rawValue = try containter.decode(RawValue.self)
            self = TypeTransaction(rawValue: rawValue) ?? .unknown
        }
    }

    enum Status: String, Decodable {
        case approved
        case cancelled
        case finished
        case processing
    }

    let day: String
    let description: String
    let id: String
    let status: Status
    let title: String
    let typeItem: TypeItem
    let typeTransaction: TypeTransaction
    let value: Double
    let movementCode: Int?
}

struct StatementPagination: Decodable {
    private enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case lastDate = "last_date"
        case nextPage = "next_page"
        case nextPageUrl = "next_page_url"
        case perPage = "per_page"
    }

    let currentPage: Int
    let lastDate: String?
    let nextPage: Int?
    let nextPageUrl: String?
    let perPage: Int
}

struct StatementTax: Decodable {
    let date: String
    let tax: Double
    let value: Double
}
