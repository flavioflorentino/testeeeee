import SnapKit
import UI

protocol StatementListHeaderViewDelegate: AnyObject {
    func didTapHeaderView()
    func didTapBlockedBalance()
}

extension StatementListHeaderView.Layout {
    enum ContentView {
        static let borderWidth: CGFloat = 1
    }
}

final class StatementListHeaderView: UIView {
    private typealias Localizable = Strings.Statement
    fileprivate enum Layout { }

    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.white.color
        view.layer.borderWidth = Layout.ContentView.borderWidth
        view.layer.borderColor = Colors.grayscale100.color.cgColor
        view.layer.cornerRadius = CornerRadius.medium
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapHeaderView))
        view.addGestureRecognizer(tapGesture)
        
        return view
    }()

    private lazy var titleDetailStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, detailLabel])
        stackView.spacing = Spacing.base01
        stackView.axis = .vertical
        return stackView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        label.text = Localizable.futureBalance
        return label
    }()

    private lazy var detailLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        label.numberOfLines = 1
        label.minimumScaleFactor = 0.7
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    private lazy var fundsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [availableStackView, blockedStackView])
        stackView.axis = .vertical
        stackView.alignment = .trailing
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var availableStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [availableFundsLabel, availableFundsValue])
        stackView.spacing = Spacing.base01
        stackView.distribution = .fill
        return stackView
    }()

    private lazy var availableFundsLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        label.numberOfLines = 1
        return label
    }()

    private lazy var availableFundsValue: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var blockedStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [blockedFundsLabel, blockedFundsValue])
        stackView.spacing = Spacing.base01
        stackView.distribution = .fill
        stackView.isHidden = true
        return stackView
    }()
    
    private lazy var blockedFundsLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale600())
        label.text = Localizable.blockedBalance
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var blockedFundsValue: UILabel = {
        let label = UILabel()
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        label.numberOfLines = 1
        label.isUserInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapBlockedBalance))
        label.addGestureRecognizer(tapGesture)
        
        return label
    }()

    private lazy var accessoryImageView = UIImageView(image: Assets.accessoryArrow.image)

    weak var delegate: StatementListHeaderViewDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("not implemented yet!!!")
    }
}

extension StatementListHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        containerView.addSubview(titleDetailStackView)
        containerView.addSubview(accessoryImageView)
        addSubview(containerView)
        addSubview(fundsStackView)
    }

    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview().inset(Spacing.base02).priority(.high)
            $0.bottom.equalToSuperview().inset(Spacing.base02).priority(.low)
        }

        titleDetailStackView.snp.makeConstraints {
            $0.leading.top.equalToSuperview().offset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
        }

        accessoryImageView.snp.makeConstraints {
            $0.leading.greaterThanOrEqualTo(titleDetailStackView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerY.equalTo(containerView)
        }
        
        fundsStackView.snp.makeConstraints {
            $0.top.equalTo(containerView.snp.bottom).offset(Spacing.base02)
            $0.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }

    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - Internal Methods

extension StatementListHeaderView {
    func setAvailableBalance(_ value: String) {
        availableFundsLabel.text = Localizable.availableFunds
        availableFundsValue.text = value
    }

    func setFutureBalance(_ amount: String, date: Date) {
        let dateFormatted = DateFormatter.custom(value: date)
        detailLabel.text = Localizable.futureBalanceDetail(amount, dateFormatted)
    }

    func setNoFutureBalance() {
        detailLabel.text = Localizable.emptyHeaderText
    }
    
    func setBlockedBalance(value: String) {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.linkSecondary().font(),
            .foregroundColor: Colors.notification600.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        blockedFundsValue.attributedText = NSAttributedString(string: value, attributes: attributes)
        blockedStackView.isHidden = false
    }

    func showEmptyState() {
        detailLabel.text = Localizable.emptyHeaderText
        availableFundsLabel.text = ""
        availableFundsValue.text = ""
    }
}

@objc
private extension StatementListHeaderView {
    func didTapHeaderView() {
        delegate?.didTapHeaderView()
    }
    
    func didTapBlockedBalance() {
        delegate?.didTapBlockedBalance()
    }
}
