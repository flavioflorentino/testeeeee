import UI

extension StatementListEmptyView.Layout {
    enum IconImageView {
        static let size = CGSize(width: 140, height: 140)
    }
}

final class StatementListEmptyView: UIView {
    private typealias Localizable = Strings.Statement
    fileprivate enum Layout { }
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [iconImageView, titleLabel, subtitleLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        stackView.alignment = .center
        return stackView
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView(image: Assets.Statements.statementsEmptyState.image)
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .grayscale800())
            .with(\.textAlignment, .center)
        label.text = Localizable.emptyListTitle
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale600())
            .with(\.textAlignment, .center)
        label.text = Localizable.emptyListSubtitle
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension StatementListEmptyView: ViewConfiguration {
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        iconImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.IconImageView.size)
        }
    }
    
    func buildViewHierarchy() {
        addSubview(stackView)
    }
}
