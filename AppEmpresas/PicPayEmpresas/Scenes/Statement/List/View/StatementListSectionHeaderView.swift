import UI
import UIKit

private extension StatementListSectionHeaderView.Layout {
    enum StackView {
        static let margins = UIEdgeInsets(
            top: Spacing.base01,
            left: Spacing.base02,
            bottom: Spacing.base01,
            right: Spacing.base02
        )
    }
}

final class StatementListSectionHeaderView: UITableViewHeaderFooterView {
    // MARK: - Private Properties

    fileprivate enum Layout { }

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [dateLabel, totalLabel])
        stackView.spacing = Spacing.base00
        return stackView
    }()

    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        label.numberOfLines = 1
        return label
    }()

    private lazy var totalLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .black())
            .with(\.textAlignment, .right)
        label.numberOfLines = 1
        return label
    }()

    // MARK: - Inits

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Internal Methods

extension StatementListSectionHeaderView {
    func setup(_ item: StatementHeader) {
        dateLabel.text = DateFormatter.custom(value: item.date)
        totalLabel.text = item.balance.toCurrencyString()
    }
}

// MARK: - ViewConfiguration

extension StatementListSectionHeaderView: ViewConfiguration {
    func configureViews() {
        contentView.backgroundColor = Colors.backgroundPrimary.color
    }

    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Layout.StackView.margins).priority(.high)
        }
    }

    func buildViewHierarchy() {
        contentView.addSubview(stackView)
    }
}
