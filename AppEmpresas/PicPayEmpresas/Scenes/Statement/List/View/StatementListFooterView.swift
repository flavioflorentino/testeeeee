import SnapKit
import UI

protocol StatementListFooterViewDelegate: AnyObject {
    func didTapTryAgain()
}

final class StatementListFooterView: UIView {
    private typealias Localizable = Strings.Statement

    lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .white)
        activityIndicator.color = Colors.black.color
        return activityIndicator
    }()

    private lazy var infoCircleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, .critical600())
            .with(\.textAlignment, .center)
        label.numberOfLines = 1
        label.text = Iconography.infoCircle.rawValue
        return label
    }()

    private lazy var tryAgainTitle: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .black())
        label.numberOfLines = 1
        label.text = Localizable.tryAgainTitle
        return label
    }()

    private lazy var tryAgainDescription: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        label.numberOfLines = 1
        label.text = Localizable.tryAgainDescription
        return label
    }()

    private lazy var tryAgainButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.tryAgainAction, for: .normal)
        button.buttonStyle(LinkButtonStyle(size: .small, icon: (name: .refresh, alignment: .right)))
        button.addTarget(self, action: #selector(tryAgainButtonTapped), for: .touchUpInside)
        return button
    }()

    weak var delegate: StatementListFooterViewDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("not implemented yet!!!")
    }
}

// MARK: - ViewConfiguration

extension StatementListFooterView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(activityIndicator)
    }

    func setupConstraints() {
        activityIndicator.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02).priority(.low)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
    }

    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - Internal Methods

extension StatementListFooterView {
    func showLoading() {
        activityIndicator.startAnimating()
        tryAgainComponent(isHidden: true)
    }

    func hideLoading() {
        activityIndicator.stopAnimating()
    }

    func showTryAgainComponent() {
        addSubview(infoCircleLabel)
        addSubview(tryAgainTitle)
        addSubview(tryAgainDescription)
        addSubview(tryAgainButton)

        infoCircleLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().offset(Spacing.base01)
            $0.leading.equalToSuperview().inset(Spacing.base02)
            $0.width.equalTo(Spacing.base03)
        }

        tryAgainTitle.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.leading.equalTo(infoCircleLabel.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }

        tryAgainDescription.snp.makeConstraints {
            $0.top.equalTo(tryAgainTitle.snp.bottom).offset(Spacing.base00)
            $0.leading.equalTo(tryAgainTitle)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }

        tryAgainButton.snp.makeConstraints {
            $0.top.equalTo(tryAgainDescription.snp.bottom).offset(Spacing.base00)
            $0.leading.equalTo(tryAgainTitle)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Spacing.base03)
        }
    }
}

@objc
private extension StatementListFooterView {
    func tryAgainButtonTapped() {
        delegate?.didTapTryAgain()
    }

    func tryAgainComponent(isHidden: Bool) {
        infoCircleLabel.removeFromSuperview()
        tryAgainTitle.removeFromSuperview()
        tryAgainDescription.removeFromSuperview()
        tryAgainButton.removeFromSuperview()
    }
}
