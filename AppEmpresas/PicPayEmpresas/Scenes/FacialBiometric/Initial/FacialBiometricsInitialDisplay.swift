import Foundation

protocol FacialBiometricsInitialDisplay: AnyObject {
    func startLoading()
}
