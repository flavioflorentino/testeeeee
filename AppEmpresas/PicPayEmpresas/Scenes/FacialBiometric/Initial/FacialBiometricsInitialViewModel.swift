import Foundation
import LegacyPJ

protocol FacialBiometricsInitialViewModelInputs: AnyObject {
    func verifyStatus()
}

final class FacialBiometricsInitialViewModel {
    typealias Dependencies = HasAuthManager
    private let dependencies: Dependencies

    private let service: FacialBiometricsInitialServicing
    private let presenter: FacialBiometricsInitialPresenting

    init(service: FacialBiometricsInitialServicing, presenter: FacialBiometricsInitialPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension FacialBiometricsInitialViewModel: FacialBiometricsInitialViewModelInputs {
    func verifyStatus() {
        presenter.startLoading()
        service.status { result in
            switch result {
            case .success(let response):
                self.updateStatus(response.biometry)
                self.verify(status: response.biometry)
            case .failure:
                self.verify(status: .statusError)
            }
        }
    }
    
    private func updateStatus(_ status: FacialBiometricsStatus) {
        let authManager = dependencies.authManager
        guard var auth = authManager.userAuth?.auth, let user = authManager.user else {
            return
        }
        auth.biometry = status
        authManager.updateUser(auth: auth, user: user)
    }
    
    private func verify(status: FacialBiometricsStatus) {
        let action: FacialBiometricsInitialAction
        switch status {
        case .required:
            action = .main
        default:
            action = .status(status)
        }
        presenter.didNextStep(action: action)
    }
}
