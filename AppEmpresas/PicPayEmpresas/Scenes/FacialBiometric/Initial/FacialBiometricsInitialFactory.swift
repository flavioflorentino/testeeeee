import Foundation

enum FacialBiometricsInitialFactory {
    static func make() -> FacialBiometricsInitialViewController {
        let container = DependencyContainer()
        let service: FacialBiometricsInitialServicing = FacialBiometricsInitialService(dependencies: container)
        let coordinator: FacialBiometricsInitialCoordinating = FacialBiometricsInitialCoordinator()
        let presenter: FacialBiometricsInitialPresenting = FacialBiometricsInitialPresenter(coordinator: coordinator)
        let viewModel = FacialBiometricsInitialViewModel(service: service, presenter: presenter, dependencies: container)
        let viewController = FacialBiometricsInitialViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
