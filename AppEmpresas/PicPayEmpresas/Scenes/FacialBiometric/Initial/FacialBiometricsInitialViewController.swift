import UI
import UIKit

final class FacialBiometricsInitialViewController: ViewController<FacialBiometricsInitialViewModelInputs, UIView> {
    fileprivate typealias Localizable = Strings.FacialBiometric
    fileprivate enum Layout {}
    
    private lazy var loader: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView()
        loader.color = Colors.black.lightColor
        // We can't change the size with constraints, so we need to scale the size
        loader.transform = CGAffineTransform(scaleX: 2, y: 2)
        return loader
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.selfieLoadingTitle
        label.typography = .bodyPrimary()
        label.textAlignment = .center
        label.textColor = Colors.grayscale700.lightColor
        label.numberOfLines = 2
        return label
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel.verifyStatus()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(loader)
        view.addSubview(titleLabel)
    }
    
    override func configureViews() {
        view.backgroundColor = .white(.light)
    }
    
    override func setupConstraints() {
        loader.layout {
            $0.centerX == view.centerXAnchor
            $0.centerY == view.centerYAnchor
        }
        
        titleLabel.layout {
            $0.leading == view.leadingAnchor + Spacing.base04
            $0.top == loader.bottomAnchor + Spacing.base03
            $0.trailing == view.trailingAnchor - Spacing.base04
        }
    }
}

// MARK: View Model Outputs
extension FacialBiometricsInitialViewController: FacialBiometricsInitialDisplay {
    func startLoading() {
        loader.startAnimating()
    }
}
