import LegacyPJ
import UIKit

enum FacialBiometricsInitialAction: Equatable {
    case main
    case status(_ status: FacialBiometricsStatus)
}

protocol FacialBiometricsInitialCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: FacialBiometricsInitialAction)
}

final class FacialBiometricsInitialCoordinator: FacialBiometricsInitialCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: FacialBiometricsInitialAction) {
        switch action {
        case .main:
            let controller = FacialBiometricsMainFactory.make()
            viewController?.pushViewController(controller)
        case .status(let status):
            let controller = FacialBiometricsStatusFactory.make(with: status)
            viewController?.pushViewController(controller)
        }
    }
}
