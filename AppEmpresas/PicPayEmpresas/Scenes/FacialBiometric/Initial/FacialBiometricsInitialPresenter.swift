import Core
import Foundation

protocol FacialBiometricsInitialPresenting: AnyObject {
    var viewController: FacialBiometricsInitialDisplay? { get set }
    func didNextStep(action: FacialBiometricsInitialAction)
    func startLoading()
}

final class FacialBiometricsInitialPresenter: FacialBiometricsInitialPresenting {
    private let coordinator: FacialBiometricsInitialCoordinating
    weak var viewController: FacialBiometricsInitialDisplay?

    init(coordinator: FacialBiometricsInitialCoordinating) {
        self.coordinator = coordinator
    }

    func didNextStep(action: FacialBiometricsInitialAction) {
        coordinator.perform(action: action)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
}
