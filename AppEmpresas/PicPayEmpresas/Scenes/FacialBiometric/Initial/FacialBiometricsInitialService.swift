import Core

protocol FacialBiometricsInitialServicing {
    func status(completion: @escaping (Result<FacialBiometricsStatusResponse, ApiError>) -> Void)
}

final class FacialBiometricsInitialService: FacialBiometricsInitialServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func status(completion: @escaping (Result<FacialBiometricsStatusResponse, ApiError>) -> Void) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase

        let endpoint = FacialBiometricsServiceEndPoint.status
        Core.Api<FacialBiometricsStatusResponse>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
