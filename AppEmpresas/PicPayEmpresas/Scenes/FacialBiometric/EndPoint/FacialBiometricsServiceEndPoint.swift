import Core
import UIKit

enum FacialBiometricsServiceEndPoint {
    case status
    case upload(image: UIImage, boundary: String)
}

extension FacialBiometricsServiceEndPoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .status:
            return "/company/create/user-biometry-status"
        case .upload:
            return "/company/create/user-biometry"
        }
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var contentType: ContentType {
        switch self {
        case .status:
            return .applicationJson
        case .upload:
            return .multipartFormData()
        }
    }
    
    var customHeaders: [String: String] {
        guard case .upload(_, let boundary) = self else {
            return [:]
        }
        return ["Content-Type": "boundary=\(boundary)"]
    }
    
    var body: Data? {
        let imageSize = CGSize(width: 720, height: 1_280)
        guard
            case let .upload(image, boundary) = self,
            let imageForUpload = image.resizedImageWithinRect(rectSize: imageSize, scale: 1),
            let data = imageForUpload.jpegData(compressionQuality: 0.85)
            else {
                return nil
        }
        return MultipartBody(boundary)
                .append(media: .init(data: data, name: "photo", filename: "IMG_\(Date().timeIntervalSince1970).jpg"))
                .build()
    }
}
