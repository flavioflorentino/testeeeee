import Foundation

final class MultipartBody {
    private let boundary: String
    private var body = Data()
    private var boundaryPrefix: String {
        "\(doubleDash)\(boundary)\(lineBreak)"
    }
    private let doubleDash = "--"
    private let lineBreak = "\r\n"
    
    init(_ boundary: String) {
        self.boundary = boundary
    }
    
    static func generateBoundary() -> String {
        "Boundary-\(UUID().uuidString)"
    }
    
    @discardableResult
    func append(media: MultipartMedia) -> Self {
        body.append("\(doubleDash)\(boundary)\(lineBreak)")
        body.append("Content-Disposition: form-data; name=\"\(media.name)\"; filename=\"\(media.filename)\"\(lineBreak)")
        body.append("Content-Type: \(media.mimeType)\(lineBreak)\(lineBreak)")
        body.append(media.data)
        body.append(lineBreak)

        return self
    }
    
    @discardableResult
    func append(medias: [MultipartMedia]) -> Self {
        for media in medias {
            append(media: media)
        }

        return self
    }
    
    @discardableResult
    func append(name: String, value: String) -> Self {
        body.append("\(doubleDash)\(boundary)\(lineBreak)")
        body.append("Content-Disposition: form-data; name=\"\(name)\"\(lineBreak)\(lineBreak)")
        body.append("\(value)\(lineBreak)")
        
        return self
    }
    
    @discardableResult
    func append(parameters: [String: String]) -> Self {
        for (name, value) in parameters {
            append(name: name, value: value)
        }
        
        return self
    }

    func build() -> Data {
        body.append("\(doubleDash)\(boundary)\(doubleDash)")
        return body as Data
    }
}

fileprivate extension Data {
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}
