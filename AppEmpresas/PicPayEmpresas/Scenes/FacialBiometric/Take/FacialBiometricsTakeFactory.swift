import Foundation

enum FacialBiometricsTakeFactory {
    static func make() -> FacialBiometricsTakeViewController {
        let coordinator: FacialBiometricsTakeCoordinating = FacialBiometricsTakeCoordinator()
        let presenter: FacialBiometricsTakePresenting = FacialBiometricsTakePresenter(coordinator: coordinator)
        let viewModel = FacialBiometricsTakeViewModel(presenter: presenter)
        let viewController = FacialBiometricsTakeViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
