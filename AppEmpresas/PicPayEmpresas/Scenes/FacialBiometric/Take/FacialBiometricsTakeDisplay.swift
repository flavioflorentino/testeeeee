import Foundation

protocol FacialBiometricsTakeDisplay: AnyObject {
    func showComponents()
    func hideComponents()
}
