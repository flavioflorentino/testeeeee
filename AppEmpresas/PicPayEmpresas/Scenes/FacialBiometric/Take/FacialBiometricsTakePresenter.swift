import Core
import Foundation

protocol FacialBiometricsTakePresenting: AnyObject {
    var viewController: FacialBiometricsTakeDisplay? { get set }
    func didNextStep(action: FacialBiometricsTakeAction)
    func hideComponents()
    func showComponents()
}

final class FacialBiometricsTakePresenter: FacialBiometricsTakePresenting {
    private let coordinator: FacialBiometricsTakeCoordinating
    weak var viewController: FacialBiometricsTakeDisplay?

    init(coordinator: FacialBiometricsTakeCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: FacialBiometricsTakeAction) {
        coordinator.perform(action: action)
    }
    
    func hideComponents() {
        viewController?.hideComponents()
    }
    
    func showComponents() {
        viewController?.showComponents()
    }
}
