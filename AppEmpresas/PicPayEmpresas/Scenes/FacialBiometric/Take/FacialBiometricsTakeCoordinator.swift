import LegacyPJ
import UIKit

enum FacialBiometricsTakeAction: Equatable {
    case dismiss
    case check(_ image: UIImage)
    case error
}

protocol FacialBiometricsTakeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: FacialBiometricsTakeAction)
}

final class FacialBiometricsTakeCoordinator: FacialBiometricsTakeCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: FacialBiometricsTakeAction) {
        switch action {
        case .dismiss:
            viewController?.dismiss(animated: true)
        case .check(let image):
            let controller = FacialBiometricsCheckingFactory.make(with: image)
            viewController?.navigationController?.pushViewController(controller, animated: false)
        case .error:
            let controller = FacialBiometricsStatusFactory.make(with: .uploadError)
            viewController?.pushViewController(controller)
        }
    }
}
