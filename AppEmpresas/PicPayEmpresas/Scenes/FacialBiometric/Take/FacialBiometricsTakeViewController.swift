import UI

extension FacialBiometricsTakeViewController.Layout {
    enum Buttons {
        static let closeSize: CGSize = .init(width: 32, height: 32)
        static let shotSize: CGSize = .init(width: 64, height: 64)
        static var shotCornerRadius: CGFloat {
            shotSize.height / 2
        }
    }

    enum Others {
        // This value was taken from the division of height by the width of the mask in the figma
        static let multiplier: CGFloat = 1.333_321_505_9 / 1
    }
}

final class FacialBiometricsTakeViewController: ViewController<FacialBiometricsTakeViewModelInputs, UIView> {
    fileprivate typealias Localizable = Strings.FacialBiometric
    fileprivate struct Layout {}
    
    private lazy var centerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()

    private lazy var transparentView = UIView()
    
    private lazy var closeButton: UIButton = {
        let image = Assets.icoClose.image

        let button = UIButton()
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        return button
    }()

    private lazy var tipLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.selfieTakePictureTitle
        label.typography = .bodyPrimary()
        label.textAlignment = .center
        label.textColor = Colors.white.lightColor
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var shotButton: UIButton = {
        let image = Assets.icoCamera.image

        let button = UIButton()
        button.setImage(image, for: .normal)
        button.backgroundColor = Colors.branding600.color
        button.layer.cornerRadius = Layout.Buttons.shotCornerRadius
        button.addTarget(self, action: #selector(didTapTake), for: .touchUpInside)
        return button
    }()
    
    private var cameraManager = CameraManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        requestCameraPermission()

        viewModel.sendLogTrack()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // We must insert the overlay after the constraints are set because we need the correct mask frame.
        setupOverlayLayout()
        cameraManager.resumeCamera()
        shotButton.isEnabled = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        cameraManager.stopCamera()
    }

    override func buildViewHierarchy() {
        view.addSubview(centerView)
        view.addSubview(transparentView)
        view.addSubview(closeButton)
        view.addSubview(tipLabel)
        view.addSubview(shotButton)
    }

    override func configureViews() {
        view.backgroundColor = .black(.light)
    }
    
    override func setupConstraints() {
        centerView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
        }

        transparentView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base06)
            $0.centerY.equalTo(centerView.snp.centerY)
            $0.height.equalTo(transparentView.snp.width).multipliedBy(Layout.Others.multiplier)
        }

        closeButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base03)
            $0.size.equalTo(Layout.Buttons.closeSize)
        }

        tipLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base04)
            $0.top.equalTo(closeButton.snp.bottom)
            $0.bottom.equalTo(centerView.snp.top)
        }

        shotButton.snp.makeConstraints {
            $0.top.equalTo(centerView.snp.bottom)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base06)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Buttons.shotSize)
        }
    }

    private func requestCameraPermission() {
        viewModel.hideComponents()
        PermissionManager.shared.askForCameraPermission { [weak self] isAllowed in
            if isAllowed {
                self?.setupCamera()
            } else {
                self?.presentSettingsAlert()
            }
        }
    }
    
    private func presentSettingsAlert() {
        let alert = UIAlertController(title: Localizable.selfiePermissionTitle,
                                      message: Localizable.selfiePermissionMessage,
                                      preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: Localizable.selfiePermissionSettings, style: .default, handler: alertSettingsHandler(_:))
        alert.addAction(settingsAction)
        alert.preferredAction = settingsAction
        
        let cancelAction = UIAlertAction(title: Localizable.selfiePermissionCancel, style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true)
    }

    private func setupOverlayLayout() {
        // We must guarantee which does not have overlay in sublayers because this can duplicate
        guard view.layer.sublayers?.contains(where: { $0 is CAShapeLayer }) == false else {
            return
        }

        view.layoutIfNeeded()

        // Creating mask
        let maskPath = UIBezierPath(rect: transparentView.frame)
        maskPath.cgPath = CGPath(ellipseIn: transparentView.frame, transform: nil)

        // Creating overlay
        let overlayPath = UIBezierPath(rect: view.bounds)
        overlayPath.append(maskPath)
        overlayPath.usesEvenOddFillRule = true

        // Creating the position
        let position: UInt32 = 1

        // Inserting overlay to view
        let fillLayer = CAShapeLayer()
        fillLayer.path = overlayPath.cgPath
        fillLayer.fillRule = .evenOdd
        fillLayer.fillColor = Colors.black.lightColor.withAlphaComponent(0.75).cgColor
        view.layer.insertSublayer(fillLayer, at: position)
    }

    private func setupCamera() {
        viewModel.showComponents()
        cameraManager.setupCamera(on: view, delegate: viewModel, position: .front)
    }
}

// MARK: UI Target Methods
@objc
private extension FacialBiometricsTakeViewController {
    private func didTapClose() {
        viewModel.dismiss()
    }

    private func didTapTake() {
        cameraManager.takePicture()
        shotButton.isEnabled = false
    }

    private func alertSettingsHandler(_ action: UIAlertAction) {
        guard
            let url = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(url)
            else {
                return
        }
        UIApplication.shared.open(url)
    }
}

// MARK: View Model Outputs
extension FacialBiometricsTakeViewController: FacialBiometricsTakeDisplay {
    func showComponents() {
        shouldHideComponents(false)
    }
    
    func hideComponents() {
        shouldHideComponents(true)
    }
    
    private func shouldHideComponents(_ isHidden: Bool) {
        closeButton.isHidden = isHidden
        tipLabel.isHidden = isHidden
        shotButton.isHidden = isHidden
    }
}
