import AnalyticsModule
import UIKit

protocol FacialBiometricsTakeViewModelInputs: CameraManagerDelegate {
    func dismiss()
    func sendLogTrack()
    func hideComponents()
    func showComponents()
}

final class FacialBiometricsTakeViewModel {
    private let presenter: FacialBiometricsTakePresenting
    private let dependencies: HasAnalytics = DependencyContainer()

    init(presenter: FacialBiometricsTakePresenting) {
        self.presenter = presenter
    }
}

extension FacialBiometricsTakeViewModel: FacialBiometricsTakeViewModelInputs {
    func dismiss() {
        presenter.didNextStep(action: .dismiss)
    }
    
    func sendLogTrack() {
        dependencies.analytics.log(FacialBiometricsAnalytics.cameraActivated)
    }

    func hideComponents() {
        presenter.hideComponents()
    }
    
    func showComponents() {
        presenter.showComponents()
    }
}

extension FacialBiometricsTakeViewModel: CameraManagerDelegate {
    func didTakePhoto(_ photo: UIImage) {
        presenter.didNextStep(action: .check(photo))
    }
    
    func errorWhileTryingToTakePhoto(_ error: Error) {
        // This error is not being used because the error screen we have for this flow is generic
        presenter.didNextStep(action: .error)
    }
}
