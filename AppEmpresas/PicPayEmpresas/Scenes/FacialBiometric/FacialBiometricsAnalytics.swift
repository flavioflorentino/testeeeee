import AnalyticsModule
import LegacyPJ

enum FacialBiometricsAnalytics: String, AnalyticsKeyProtocol {
    case takeSelfie = "Security - Take Selfie"
    case pictureInstructions = "Security - Picture Instructions"
    case cameraActivated = "Security - Camera Activated"
    case pictureTaken = "Security - Picture Taken"
    case pictureDenied = "Security - Picture Denied"
    case accessDenied = "Security - Access Denied"
    case accessOK = "Security - Access OK"
    case analyzingPicture = "Security - Analyzing Picture"
        
    func event() -> AnalyticsEventProtocol {
        var properties: [String: Any] = [:]
        if
            let user = AuthManager.shared.user,
            let userId = user.id,
            let name = user.name,
            let email = user.email,
            let phone = user.phone
        {
            properties = [
                "user_id": userId,
                "nome": name,
                "email": email,
                "telefone": phone
            ]
        }
        
        return AnalyticsEvent(rawValue, properties: properties, providers: [.mixPanel])
    }
}
