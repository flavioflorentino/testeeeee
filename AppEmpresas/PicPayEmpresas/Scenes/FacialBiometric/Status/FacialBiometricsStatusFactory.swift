import Foundation
import LegacyPJ

enum FacialBiometricsStatusFactory {
    static func make(with status: FacialBiometricsStatus) -> FacialBiometricsStatusViewController {
        let container = DependencyContainer()
        let coordinator: FacialBiometricsStatusCoordinating = FacialBiometricsStatusCoordinator(dependencies: container)
        let presenter: FacialBiometricsStatusPresenting = FacialBiometricsStatusPresenter(coordinator: coordinator)
        let viewModel = FacialBiometricsStatusViewModel(presenter: presenter, dependencies: container, status: status)
        let viewController = FacialBiometricsStatusViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
