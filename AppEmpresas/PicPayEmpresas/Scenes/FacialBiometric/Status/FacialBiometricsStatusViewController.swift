import UI
import UIKit

protocol FacialBiometricsStatusDisplay: AnyObject {
    func displayComponents(_ feedback: FacialBiometricsFeedback)
}

struct FacialBiometricsFeedback: Equatable {
    var hasCloseButton: Bool = true
    var image: UIImage?
    var title: String
    var subtitle: String?
    var description: String?
    var tips: [FacialBiometrisFeedbackTip] = []
    var actionTitle: String?
}

struct FacialBiometrisFeedbackTip: Equatable {
    let icon: UIImage
    let description: String
}

extension FacialBiometricsStatusViewController.Layout {
    enum Images {
        static let height: CGFloat = 112
    }

    enum Views {
        static let spacingHeight: CGFloat = 0.15
    }
    
    enum Buttons {
        static let closeSize = CGSize(width: 32, height: 32)
        static let height: CGFloat = 48
        static let cornerRadius: CGFloat = 24
    }
    
    enum Fonts {
        static let semibold = UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
}

final class FacialBiometricsStatusViewController: ViewController<FacialBiometricsStatusViewModelInputs, UIView> {
    fileprivate typealias Localizable = Strings.FacialBiometric
    fileprivate enum Layout {}
    
    private lazy var closeButton: UIButton = {
        let image = Assets.icoClose.image

        let button = UIButton()
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        return button
    }()
    
    private lazy var facialImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.setContentCompressionResistancePriority(
            .defaultLow,
            for: .vertical
        )
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.typography = .title(.xLarge)
        label.textAlignment = .center
        label.textColor = Colors.black.lightColor
        label.numberOfLines = 0
        return label
    }()

    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.selfieTipsTitle
        label.typography = .title(.small)
        label.textAlignment = .center
        label.textColor = Colors.grayscale700.lightColor
        label.numberOfLines = 3
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.typography = .bodyPrimary()
        label.textAlignment = .center
        label.textColor = Colors.grayscale700.lightColor
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var tipView = FacialBiometricsTipView()
    
    private lazy var spacingView = UIView()
    
    private lazy var verticalContainer: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            facialImageView,
            titleLabel,
            subtitleLabel,
            descriptionLabel,
            tipView,
            spacingView
        ])
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var horizontalContainer: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            verticalContainer
        ])
        stackView.axis = .horizontal
        stackView.alignment = .center
        return stackView
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.selfieCheckingGood, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.retrieveStatus()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(closeButton)
        view.addSubview(horizontalContainer)
        view.addSubview(actionButton)
    }
    
    override func configureViews() {
        view.backgroundColor = .white(.light)
    }
    
    override func setupConstraints() {
        closeButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base03)
            $0.size.equalTo(Layout.Buttons.closeSize)
        }

        facialImageView.snp.makeConstraints {
            $0.height.lessThanOrEqualTo(Layout.Images.height)
        }

        spacingView.snp.makeConstraints {
            $0.height.equalToSuperview().multipliedBy(Layout.Views.spacingHeight)
        }
        
        horizontalContainer.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base04)
            $0.top.equalTo(closeButton.snp.bottom)
            $0.bottom.equalTo(actionButton.snp.top).offset(-Spacing.base02)
        }

        tipView.snp.makeConstraints {
            $0.width.equalTo(horizontalContainer.snp.width)
        }

        actionButton.layout {
            $0.leading == view.leadingAnchor + Spacing.base03
            $0.trailing == view.trailingAnchor - Spacing.base03
            $0.bottom == view.compatibleSafeAreaLayoutGuide.bottomAnchor - Spacing.base04
            $0.height == Layout.Buttons.height
        }
    }
}

// MARK: UI Target Methods
@objc
private extension FacialBiometricsStatusViewController {
    func didTapButton() {
        viewModel.didNextStep()
    }
    
    func didTapClose() {
        viewModel.shouldLogout()
    }
}

// MARK: View Model Outputs
extension FacialBiometricsStatusViewController: FacialBiometricsStatusDisplay {
    func displayComponents(_ feedback: FacialBiometricsFeedback) {
        horizontalContainer.alignment = feedback.tips.isEmpty ? .center : .top
        closeButton.isHidden = !feedback.hasCloseButton
        titleLabel.text = feedback.title

        facialImageView.isHidden = feedback.image == nil
        facialImageView.image = feedback.image

        subtitleLabel.isHidden = feedback.subtitle == nil
        subtitleLabel.text = feedback.subtitle

        descriptionLabel.isHidden = feedback.description == nil
        descriptionLabel.text = feedback.description

        actionButton.isHidden = feedback.actionTitle == nil
        actionButton.setTitle(feedback.actionTitle, for: .normal)

        tipView.isHidden = feedback.tips.isEmpty
        feedback.tips.forEach { tip in
            tipView.addTip(icon: tip.icon, description: tip.description)
        }
    }
}
