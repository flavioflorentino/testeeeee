import AnalyticsModule
import Foundation
import LegacyPJ

protocol FacialBiometricsStatusViewModelInputs: AnyObject {
    func didNextStep()
    func retrieveStatus()
    func shouldLogout()
}

final class FacialBiometricsStatusViewModel {
    typealias Dependencies = HasAuthManager & HasAnalytics
    private let dependencies: Dependencies

    private let presenter: FacialBiometricsStatusPresenting
    private let status: FacialBiometricsStatus

    init(presenter: FacialBiometricsStatusPresenting, dependencies: Dependencies, status: FacialBiometricsStatus) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.status = status
    }
}

extension FacialBiometricsStatusViewModel: FacialBiometricsStatusViewModelInputs {
    func didNextStep() {
        switch status {
        case .accepted:
            conclude()
        case .retry, .uploadError, .statusError:
            retry()
        case .denied, .pending:
            shouldLogout()
        default:
            // Does not have
            break
        }
    }
    
    func retrieveStatus() {
        sendLogEvent()
        presenter.displayStatus(status)
    }
    
    func shouldLogout() {
        guard let userAuth = dependencies.authManager.userAuth else {
            return
        }

        dependencies.authManager.logout(userAuth: userAuth, changeAccount: true) { [weak self] _ in
            let nextStep: FacialBiometricsStatusAction =
                self?.dependencies.authManager.authenticatedUsers.isEmpty ?? true
                    ? .unlogged
                    : .authenticated
            self?.presenter.didNextStep(action: nextStep)
        }
    }

    private func conclude() {
        presenter.didNextStep(action: .conclude)
    }
    
    private func retry() {
        presenter.didNextStep(action: .retry)
    }
    
    private func sendLogEvent() {
        switch status {
        case .accepted:
            dependencies.analytics.log(FacialBiometricsAnalytics.accessOK)
        case .retry:
            dependencies.analytics.log(FacialBiometricsAnalytics.pictureDenied)
        case .denied:
            dependencies.analytics.log(FacialBiometricsAnalytics.accessDenied)
        case .pending:
            dependencies.analytics.log(FacialBiometricsAnalytics.analyzingPicture)
        default:
            break
        }
    }
}
