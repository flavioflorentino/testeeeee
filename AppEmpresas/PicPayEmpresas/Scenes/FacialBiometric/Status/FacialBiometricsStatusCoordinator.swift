import UIKit
import FeatureFlag

enum FacialBiometricsStatusAction {
    case conclude
    case unlogged
    case authenticated
    case retry
}

protocol FacialBiometricsStatusCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: FacialBiometricsStatusAction)
}

final class FacialBiometricsStatusCoordinator: FacialBiometricsStatusCoordinating {
    typealias Dependencies = HasAppManager & HasAppCoordinator & HasFeatureManager
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: FacialBiometricsStatusAction) {
        switch action {
        case .conclude, .authenticated:
            if dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
                dependencies.appCoordinator.displayAuthenticated()
            } else {
                dependencies.appManager.presentAuthenticatedViewController()
            }
        case .unlogged:
            if dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
                dependencies.appCoordinator.displayWelcome()
            } else {
                dependencies.appManager.presentUnloggedViewController()
            }
        case .retry:
            let controllers = viewController?.navigationController?.viewControllers
            if controllers?.contains(where: { $0 is FacialBiometricsTakeViewController }) == true {
                viewController?.navigationController?.popToRootViewController(animated: true)
            } else {
                let controller = FacialBiometricsTakeFactory.make()
                let navigation = UINavigationController(rootViewController: controller)
                navigation.setNavigationBarHidden(true, animated: false)
                navigation.modalPresentationStyle = .fullScreen
                viewController?.navigationController?.present(navigation, animated: true)
            }
        }
    }
}
