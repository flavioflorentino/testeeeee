import Core
import Foundation
import LegacyPJ
import UIKit

protocol FacialBiometricsStatusPresenting: AnyObject {
    var viewController: FacialBiometricsStatusDisplay? { get set }
    func didNextStep(action: FacialBiometricsStatusAction)
    func displayStatus(_ status: FacialBiometricsStatus)
}

final class FacialBiometricsStatusPresenter: FacialBiometricsStatusPresenting {
    fileprivate typealias Localizable = Strings.FacialBiometric

    private let coordinator: FacialBiometricsStatusCoordinating
    weak var viewController: FacialBiometricsStatusDisplay?

    init(coordinator: FacialBiometricsStatusCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: FacialBiometricsStatusAction) {
        coordinator.perform(action: action)
    }

    func displayStatus(_ status: FacialBiometricsStatus) {
        let feedback = FacialBiometricsFeedback(
            hasCloseButton: hasCloseButton(for: status),
            image: image(for: status),
            title: title(for: status),
            subtitle: subtitle(for: status),
            description: description(for: status),
            tips: tips(for: status),
            actionTitle: actionTitle(for: status)
        )
        viewController?.displayComponents(feedback)
    }

    private func hasCloseButton(for status: FacialBiometricsStatus) -> Bool {
        switch status {
        case .accepted:
            return false
        default:
            return true
        }
    }

    private func image(for status: FacialBiometricsStatus) -> UIImage? {
        switch status {
        case .accepted:
            return Assets.imgLike.image
        case .denied:
            return Assets.imgMail.image
        case .pending:
            return Assets.imgClock.image
        case .statusError:
            return Assets.imgTool.image
        case .uploadError:
            return Assets.imgTool.image
        default:
            return nil
        }
    }

    private func title(for status: FacialBiometricsStatus) -> String {
        switch status {
        case .accepted:
            return Localizable.selfieStatusApprovedTitle
        case .retry:
            return Localizable.selfieStatusRetryTitle
        case .denied:
            return Localizable.selfieStatusDeniedTitle
        case .pending:
            return Localizable.selfieStatusManualTitle
        case .statusError:
            return Localizable.selfieLoadingErrorTitle
        case .uploadError:
            return Localizable.selfieLoadingErrorTitle
        default:
            return ""
        }
    }

    private func subtitle(for status: FacialBiometricsStatus) -> String? {
        switch status {
        case .retry:
            return Localizable.selfieStatusRetrySubtitle
        default:
            return nil
        }
    }

    private func description(for status: FacialBiometricsStatus) -> String? {
        switch status {
        case .accepted:
            return Localizable.selfieStatusApprovedMessage
        case .denied:
            return Localizable.selfieStatusDeniedMessage
        case .pending:
            return Localizable.selfieStatusManualMessage
        case .statusError:
            return Localizable.selfieLoadingErrorMessage
        case .uploadError:
            return Localizable.selfieCheckingErrorMessage
        default:
            return nil
        }
    }

    private func tips(for status: FacialBiometricsStatus) -> [FacialBiometrisFeedbackTip] {
        switch status {
        case .retry:
            let tips = [
                FacialBiometrisFeedbackTip(icon: Assets.icoLamp.image, description: Localizable.selfieTipsBright),
                FacialBiometrisFeedbackTip(icon: Assets.icoAvoidAccessories.image, description: Localizable.selfieTipsAvoidAccessories),
                FacialBiometrisFeedbackTip(icon: Assets.icoNeutralExpression.image, description: Localizable.selfieTipsNeutralExpression),
                FacialBiometrisFeedbackTip(icon: Assets.iluPersonPlaceholder.image, description: Localizable.selfieTipsOwner)
            ]
            return tips
        default:
            return []
        }
    }

    private func actionTitle(for status: FacialBiometricsStatus) -> String? {
        switch status {
        case .accepted:
            return Localizable.selfieStatusApprovedConclude
        case .retry:
            return Localizable.selfieStatusRetryTakeAnother
        case .denied:
            return Localizable.selfieTake
        case .statusError:
            return Localizable.selfieLoadingErrorTryAgain
        case .uploadError:
            return Localizable.selfieStatusRetryTakeAnother
        default:
            return nil
        }
    }
}
