import Foundation

enum FacialBiometricsMainFactory {
    static func make() -> FacialBiometricsMainViewController {
        let container = DependencyContainer()
        let coordinator: FacialBiometricsMainCoordinating = FacialBiometricsMainCoordinator()
        let presenter: FacialBiometricsMainPresenting = FacialBiometricsMainPresenter(coordinator: coordinator)
        let viewModel = FacialBiometricsMainViewModel(presenter: presenter, dependencies: container)
        let viewController = FacialBiometricsMainViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
