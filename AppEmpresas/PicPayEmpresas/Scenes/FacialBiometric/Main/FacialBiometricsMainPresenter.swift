import Core
import Foundation

protocol FacialBiometricsMainPresenting: AnyObject {
    var viewController: FacialBiometricsMainDisplay? { get set }
    func didNextStep(action: FacialBiometricsMainAction)
}

final class FacialBiometricsMainPresenter: FacialBiometricsMainPresenting {
    private let coordinator: FacialBiometricsMainCoordinating
    weak var viewController: FacialBiometricsMainDisplay?

    init(coordinator: FacialBiometricsMainCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: FacialBiometricsMainAction) {
        coordinator.perform(action: action)
    }
}
