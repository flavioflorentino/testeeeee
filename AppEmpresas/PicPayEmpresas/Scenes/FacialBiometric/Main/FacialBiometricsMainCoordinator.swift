import UIKit
import FeatureFlag

enum FacialBiometricsMainAction {
    case tips
    case unlogged
    case authenticated
}

protocol FacialBiometricsMainCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: FacialBiometricsMainAction)
}

final class FacialBiometricsMainCoordinator: FacialBiometricsMainCoordinating {
    typealias Dependecies = HasAppCoordinator & HasFeatureManager
    weak var viewController: UIViewController?
    private var dependencies: Dependecies

    init(dependencies: Dependecies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func perform(action: FacialBiometricsMainAction) {
        switch action {
        case .tips:
            let controller = FacialBiometricsTipFactory.make()
            viewController?.pushViewController(controller)
        case .unlogged:
            if dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
                dependencies.appCoordinator.displayWelcome()
            } else {
                AppManager.shared.presentUnloggedViewController()
            }
        case .authenticated:
            if dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
                dependencies.appCoordinator.displayAuthenticated()
            } else {
                AppManager.shared.presentAuthenticatedViewController()
            }
        }
    }
}
