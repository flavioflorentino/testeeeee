import SnapKit
import UI
import UIKit

extension FacialBiometricsMainViewController.Layout {
    enum Images {
        static let height: CGFloat = 142
    }
    
    enum Buttons {
        static let closeSize = CGSize(width: 32, height: 32)
        static let height: CGFloat = 48
        static var cornerRadius: CGFloat {
            height / 2
        }
    }
    
    enum Fonts {
        static let semibold = UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
}

final class FacialBiometricsMainViewController: ViewController<FacialBiometricsMainViewModelInputs, UIView> {
    fileprivate typealias Localizable = Strings.FacialBiometric
    fileprivate struct Layout {}
    
    private lazy var closeButton: UIButton = {
        let image = Assets.icoClose.image

        let button = UIButton()
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        return button
    }()

    private lazy var facialImageView: UIImageView = {
        let image = Assets.imgSelfie.image
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.selfieTitle
        label.typography = .title(.medium)
        label.textAlignment = .center
        label.textColor = Colors.black.lightColor
        label.numberOfLines = 0
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.selfieDescription
        label.typography = .bodyPrimary()
        label.textAlignment = .center
        label.textColor = Colors.grayscale700.lightColor
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var selfieButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.selfieTake, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapTake), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.sendLogTrack()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(closeButton)
        view.addSubview(facialImageView)
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(selfieButton)
    }
    
    override func configureViews() {
        view.backgroundColor = .white(.light)
    }
    
    override func setupConstraints() {
        closeButton.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.Buttons.closeSize)
        }
        
        facialImageView.snp.makeConstraints {
            $0.bottom.equalTo(titleLabel.snp.top).offset(-Spacing.base04)
            $0.centerX.equalToSuperview()
            $0.height.equalTo(Layout.Images.height)
        }
        
        titleLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview().offset(-Spacing.base06)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.lessThanOrEqualTo(selfieButton.snp.top).inset(Spacing.base03)
        }
        
        selfieButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(Spacing.base04)
            $0.height.equalTo(Layout.Buttons.height)
        }
    }
}

// MARK: UI Target Methods
@objc
private extension FacialBiometricsMainViewController {
    func didTapTake() {
        viewModel.didNextStep()
    }
    
    func didTapClose() {
        viewModel.shouldLogout()
    }
}

// MARK: View Model Outputs
extension FacialBiometricsMainViewController: FacialBiometricsMainDisplay {}
