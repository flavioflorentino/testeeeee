import AnalyticsModule
import Foundation
import LegacyPJ

protocol FacialBiometricsMainViewModelInputs: AnyObject {
    func didNextStep()
    func shouldLogout()
    func sendLogTrack()
}

final class FacialBiometricsMainViewModel {
    typealias Dependencies = HasAuthManager & HasAnalytics
    private let dependencies: Dependencies

    private let presenter: FacialBiometricsMainPresenting

    init(presenter: FacialBiometricsMainPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension FacialBiometricsMainViewModel: FacialBiometricsMainViewModelInputs {
    func didNextStep() {
        presenter.didNextStep(action: .tips)
    }
    
    func shouldLogout() {
        guard let userAuth = dependencies.authManager.userAuth else {
            return
        }

        dependencies.authManager.logout(userAuth: userAuth, changeAccount: true) { [weak self] _ in
            if AuthManager.shared.authenticatedUsers.isEmpty {
                self?.presenter.didNextStep(action: .unlogged)
            } else {
                self?.presenter.didNextStep(action: .authenticated)
            }
        }
    }
    
    func sendLogTrack() {
        dependencies.analytics.log(FacialBiometricsAnalytics.takeSelfie)
    }
}
