import Foundation
import LegacyPJ

struct FacialBiometricsUploadResponse: Decodable {
    let status: FacialBiometricsStatus
}
