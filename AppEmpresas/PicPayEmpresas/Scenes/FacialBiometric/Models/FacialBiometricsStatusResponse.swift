import Foundation
import LegacyPJ

struct FacialBiometricsStatusResponse: Decodable {
    let biometry: FacialBiometricsStatus
}
