import LegacyPJ
import UIKit

enum FacialBiometricsCheckingAction: Equatable {
    case dismiss
    case status(_ status: FacialBiometricsStatus)
}

protocol FacialBiometricsCheckingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: FacialBiometricsCheckingAction)
}

final class FacialBiometricsCheckingCoordinator: FacialBiometricsCheckingCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: FacialBiometricsCheckingAction) {
        switch action {
        case .dismiss:
            viewController?.navigationController?.popViewController(animated: false)
        case .status(let status):
            let controller = FacialBiometricsStatusFactory.make(with: status)
            viewController?.pushViewController(controller)
        }
    }
}
