import AnalyticsModule
import LegacyPJ
import UIKit

protocol FacialBiometricsCheckingViewModelInputs: AnyObject {
    func dismiss()
    func retrieveImage()
    func sendImage()
}

final class FacialBiometricsCheckingViewModel {
    typealias Dependencies = HasAuthManager & HasAnalytics
    private let dependencies: Dependencies

    private let service: FacialBiometricsCheckingServicing
    private let presenter: FacialBiometricsCheckingPresenting
    private let image: UIImage

    init(
        service: FacialBiometricsCheckingServicing,
        presenter: FacialBiometricsCheckingPresenting,
        dependencies: Dependencies,
        image: UIImage
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.image = image
    }
}

extension FacialBiometricsCheckingViewModel: FacialBiometricsCheckingViewModelInputs {
    func dismiss() {
        presenter.didNextStep(action: .dismiss)
    }
    
    func retrieveImage() {
        dependencies.analytics.log(FacialBiometricsAnalytics.pictureTaken)
        presenter.displayImage(image)
    }
    
    func sendImage() {
        presenter.displayLoader()
        service.sendPicture(image: image) { [weak self] result in
            let status: FacialBiometricsStatus
            switch result {
            case .success(let response):
                self?.updateStatus(response.status)
                status = response.status
            case .failure:
                status = .uploadError
            }
            self?.presenter.dismissLoader()
            self?.presenter.didNextStep(action: .status(status))
        }
    }
    
    private func updateStatus(_ status: FacialBiometricsStatus) {
        let authManager = dependencies.authManager
        guard var auth = authManager.userAuth?.auth, let user = authManager.user else {
            return
        }
        auth.biometry = status
        authManager.updateUser(auth: auth, user: user)
    }
}
