import Core
import UIKit

protocol FacialBiometricsCheckingServicing {
    func sendPicture(image: UIImage, completion: @escaping (Result<FacialBiometricsUploadResponse, ApiError>) -> Void)
}

final class FacialBiometricsCheckingService: FacialBiometricsCheckingServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func sendPicture(image: UIImage, completion: @escaping (Result<FacialBiometricsUploadResponse, ApiError>) -> Void) {
        DispatchQueue.global(qos: .background).async {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase

            let boundary = MultipartBody.generateBoundary()
            let endpoint = FacialBiometricsServiceEndPoint.upload(image: image, boundary: boundary)
            Core.Api<FacialBiometricsUploadResponse>(endpoint: endpoint).execute { [weak self] result in
                let mappedResult = result.map { $0.model }
                self?.dependencies.mainQueue.async {
                    completion(mappedResult)
                }
            }
        }
    }
}
