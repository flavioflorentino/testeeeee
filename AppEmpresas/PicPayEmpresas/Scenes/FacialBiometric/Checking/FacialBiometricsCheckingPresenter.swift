import Core
import UIKit

protocol FacialBiometricsCheckingPresenting: AnyObject {
    var viewController: FacialBiometricsCheckingDisplay? { get set }
    func displayImage(_ image: UIImage)
    func didNextStep(action: FacialBiometricsCheckingAction)
    func displayLoader()
    func dismissLoader()
}

final class FacialBiometricsCheckingPresenter: FacialBiometricsCheckingPresenting {
    private let coordinator: FacialBiometricsCheckingCoordinating
    weak var viewController: FacialBiometricsCheckingDisplay?

    init(coordinator: FacialBiometricsCheckingCoordinating) {
        self.coordinator = coordinator
    }
    
    func displayImage(_ image: UIImage) {
        viewController?.displayImage(image)
    }
    
    func didNextStep(action: FacialBiometricsCheckingAction) {
        coordinator.perform(action: action)
    }
    
    func displayLoader() {
        viewController?.displayLoader()
    }
    
    func dismissLoader() {
        viewController?.dismissLoader()
    }
}
