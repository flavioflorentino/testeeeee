import UI
import UIKit

final class FacialBiometricsLoadingView: UIView, ViewConfiguration {
    fileprivate typealias Localizable = Strings.FacialBiometric
    fileprivate struct Layout {}
    
    private lazy var blurView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .dark)
        let visualEffect = UIVisualEffectView(effect: blurEffect)
        return visualEffect
    }()
    
    private lazy var loader: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView(style: .white)
        // We can't change the size with constraints, so we need to scale the size
        loader.transform = CGAffineTransform(scaleX: 2, y: 2)
        return loader
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.selfieSendingTitle
        label.typography = .bodyPrimary()
        label.textColor = Colors.white.lightColor
        return label
    }()

    init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(blurView)
        addSubview(loader)
        addSubview(titleLabel)
    }
    
    func setupConstraints() {
        blurView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        loader.layout {
            $0.centerX == centerXAnchor
            $0.centerY == centerYAnchor
        }
        
        titleLabel.layout {
            $0.top == loader.bottomAnchor + Spacing.base03
            $0.centerX == centerXAnchor
        }
    }
    
    func startLoading() {
        loader.startAnimating()
    }
    
    func stopLoading() {
        loader.stopAnimating()
    }
}
