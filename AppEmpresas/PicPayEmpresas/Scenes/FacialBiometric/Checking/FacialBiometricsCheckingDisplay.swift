import UIKit

protocol FacialBiometricsCheckingDisplay: AnyObject {
    func displayImage(_ image: UIImage)
    func displayLoader()
    func dismissLoader()
}
