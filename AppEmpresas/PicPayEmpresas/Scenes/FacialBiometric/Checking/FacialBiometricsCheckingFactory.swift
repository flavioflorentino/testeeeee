import UIKit

enum FacialBiometricsCheckingFactory {
    static func make(with image: UIImage) -> FacialBiometricsCheckingViewController {
        let container = DependencyContainer()
        let service: FacialBiometricsCheckingServicing = FacialBiometricsCheckingService(dependencies: container)
        let coordinator: FacialBiometricsCheckingCoordinating = FacialBiometricsCheckingCoordinator()
        let presenter: FacialBiometricsCheckingPresenting = FacialBiometricsCheckingPresenter(coordinator: coordinator)
        let viewModel = FacialBiometricsCheckingViewModel(service: service, presenter: presenter, dependencies: container, image: image)
        let viewController = FacialBiometricsCheckingViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
