import UI
import UIKit

extension FacialBiometricsErrorView.Layout {
    enum Buttons {
        static let size: CGFloat = 16.0
    }
}

final class FacialBiometricsErrorView: UIView, ViewConfiguration {
    fileprivate struct Layout {}
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.notification600.color
        view.cornerRadius = .light
        return view
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.typography = .caption()
        label.numberOfLines = 0
        label.textColor = Colors.white.lightColor
        return label
    }()
    
    private lazy var closeButton: UIButton = {
        let image = Assets.icoClose.image.withRenderingMode(.alwaysTemplate)
        let button = UIButton()
        button.setImage(image, for: .normal)
        button.tintColor = Colors.white.lightColor
        button.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        return button
    }()
    
    private var handle: (UIView) -> Void = { _ in }

    init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        self.addSubview(containerView)
        containerView.addSubview(messageLabel)
        containerView.addSubview(closeButton)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        messageLabel.layout {
            $0.leading == containerView.leadingAnchor + Spacing.base02
            $0.top >= containerView.topAnchor + Spacing.base02
            $0.trailing == closeButton.leadingAnchor - Spacing.base02
            $0.bottom >= containerView.bottomAnchor - Spacing.base02
            $0.centerY == containerView.centerYAnchor
        }
        
        closeButton.layout {
            $0.trailing == containerView.trailingAnchor - Spacing.base01
            $0.centerY == containerView.centerYAnchor
            $0.height == Layout.Buttons.size
            $0.width == Layout.Buttons.size
        }
    }
    
    func setup(with message: String, action completion: @escaping (UIView) -> Void) {
        messageLabel.text = message
        handle = completion
    }
    
    @objc
    private func didTapClose() {
        handle(self)
    }
}
