import UI
import UIKit

extension FacialBiometricsCheckingViewController.Layout {
    enum Buttons {
        static let height: CGFloat = 40
    }
}

final class FacialBiometricsCheckingViewController: ViewController<FacialBiometricsCheckingViewModelInputs, UIView> {
    fileprivate typealias Localizable = Strings.FacialBiometric
    fileprivate struct Layout {}
    
    private lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    private lazy var overlay: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.black.lightColor.withAlphaComponent(0.75)
        return view
    }()
    
    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            takeAnotherButton,
            goodButton
        ])
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        stackView.spacing = Spacing.base03
        
        return stackView
    }()
    
    private lazy var bottomContainer: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            confirmLabel,
            buttonsStackView
        ])
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = Spacing.base03

        return stackView
    }()

    private lazy var confirmLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.selfieCheckingTitle
        label.typography = .bodyPrimary()
        label.textAlignment = .center
        label.textColor = Colors.white.lightColor
        return label
    }()

    private lazy var takeAnotherButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.selfieCheckingTakeAnother, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTapTakeAnother), for: .touchUpInside)
        return button
    }()

    private lazy var goodButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.selfieCheckingGood, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapGood), for: .touchUpInside)
        return button
    }()
    
    private lazy var loaderView: FacialBiometricsLoadingView = {
        let view = FacialBiometricsLoadingView()
        view.isHidden = true
        return view
    }()
    
    private lazy var errorView = FacialBiometricsErrorView()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.retrieveImage()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(imageView)
        view.addSubview(overlay)
        overlay.addSubview(bottomContainer)
        view.addSubview(loaderView)
    }
    
    override func configureViews() {
        view.backgroundColor = .white(.light)
    }

    override func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.edges.equalTo(view)
        }
        
        overlay.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
        }

        takeAnotherButton.snp.makeConstraints {
            $0.height.equalTo(Layout.Buttons.height)
        }

        goodButton.snp.makeConstraints {
            $0.height.equalTo(Layout.Buttons.height)
        }

        bottomContainer.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.top.equalTo(overlay.snp.top).offset(Spacing.base03)
            $0.bottom.equalTo(overlay.compatibleSafeArea.bottom).offset(-Spacing.base03)
        }
        
        loaderView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

// MARK: UI Target Methods
@objc
private extension FacialBiometricsCheckingViewController {
    func didTapTakeAnother() {
        viewModel.dismiss()
    }

    func didTapGood() {
        viewModel.sendImage()
    }
}

// MARK: View Model Outputs
extension FacialBiometricsCheckingViewController: FacialBiometricsCheckingDisplay {
    func displayImage(_ image: UIImage) {
        imageView.image = image
    }

    func displayLoader() {
        isLoading(true)
    }
    
    func dismissLoader() {
        isLoading(false)
    }
    
    private func isLoading(_ bool: Bool) {
        overlay.isHidden = bool
        loaderView.isHidden = !bool
        bool
            ? loaderView.startLoading()
            : loaderView.stopLoading()
    }
}
