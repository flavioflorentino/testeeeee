import Foundation
import UIKit

protocol FacialBiometricsTipDisplay: AnyObject {
    func displayTip(icon: UIImage?, description: String)
}
