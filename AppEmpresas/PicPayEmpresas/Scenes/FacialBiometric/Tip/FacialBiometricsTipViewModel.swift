import AnalyticsModule
import LegacyPJ
import Foundation

protocol FacialBiometricsTipViewModelInputs: AnyObject {
    func didNextStep()
    func shouldLogout()
    func displayTips()
    func sendLogTrack()
}

final class FacialBiometricsTipViewModel {
    typealias Dependencies = HasAuthManager & HasAnalytics
    private let dependencies: Dependencies

    private let presenter: FacialBiometricsTipPresenting

    init(presenter: FacialBiometricsTipPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension FacialBiometricsTipViewModel: FacialBiometricsTipViewModelInputs {
    func didNextStep() {
        presenter.didNextStep(action: .camera)
    }

    func shouldLogout() {
        guard let userAuth = dependencies.authManager.userAuth else {
            return
        }

        dependencies.authManager.logout(userAuth: userAuth, changeAccount: true) { [weak self] _ in
            if AuthManager.shared.authenticatedUsers.isEmpty {
                self?.presenter.didNextStep(action: .unlogged)
            } else {
                self?.presenter.didNextStep(action: .authenticated)
            }
        }
    }

    func displayTips() {
        presenter.displayTips()
    }
    
    func sendLogTrack() {
        dependencies.analytics.log(FacialBiometricsAnalytics.pictureInstructions)
    }
}
