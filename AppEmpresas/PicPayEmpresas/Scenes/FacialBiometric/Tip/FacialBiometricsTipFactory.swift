import Foundation

enum FacialBiometricsTipFactory {
    static func make() -> FacialBiometricsTipViewController {
        let container = DependencyContainer()
        let coordinator: FacialBiometricsTipCoordinating = FacialBiometricsTipCoordinator()
        let presenter: FacialBiometricsTipPresenting = FacialBiometricsTipPresenter(coordinator: coordinator)
        let viewModel = FacialBiometricsTipViewModel(presenter: presenter, dependencies: container)
        let viewController = FacialBiometricsTipViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
