import Core
import Foundation

protocol FacialBiometricsTipPresenting: AnyObject {
    var viewController: FacialBiometricsTipDisplay? { get set }
    func didNextStep(action: FacialBiometricsTipAction)
    func displayTips()
}

final class FacialBiometricsTipPresenter: FacialBiometricsTipPresenting {
    fileprivate typealias Localizable = Strings.FacialBiometric
    private let coordinator: FacialBiometricsTipCoordinating
    weak var viewController: FacialBiometricsTipDisplay?

    init(coordinator: FacialBiometricsTipCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: FacialBiometricsTipAction) {
        coordinator.perform(action: action)
    }

    func displayTips() {
        viewController?.displayTip(icon: Assets.icoLamp.image, description: Localizable.selfieTipsBright)
        viewController?.displayTip(icon: Assets.icoAvoidAccessories.image, description: Localizable.selfieTipsAvoidAccessories)
        viewController?.displayTip(icon: Assets.icoNeutralExpression.image, description: Localizable.selfieTipsNeutralExpression)
        viewController?.displayTip(icon: nil, description: Localizable.selfieTipsHead(Localizable.selfieTipsOwner))
    }
}
