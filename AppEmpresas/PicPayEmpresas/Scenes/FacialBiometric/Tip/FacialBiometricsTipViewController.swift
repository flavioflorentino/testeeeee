import UI

extension FacialBiometricsTipViewController.Layout {
    enum Buttons {
        static let closeSize = CGSize(width: 32, height: 32)
        static let height: CGFloat = 48
        static var cornerRadius: CGFloat {
            height / 2
        }
    }
    
    enum Fonts {
        static let semibold = UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
}

final class FacialBiometricsTipViewController: ViewController<FacialBiometricsTipViewModelInputs, UIView> {
    fileprivate typealias Localizable = Strings.FacialBiometric
    fileprivate struct Layout {}

    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        return button
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.selfieTipsTitle
        label.typography = .title(.small)
        label.textAlignment = .center
        label.textColor = Colors.grayscale700.lightColor
        label.numberOfLines = 3
        return label
    }()
    
    private lazy var tipView = FacialBiometricsTipView()
    
    private lazy var letsGoButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.selfieTipsLetsGo, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapLetsGo), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.displayTips()
        viewModel.sendLogTrack()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(closeButton)
        view.addSubview(titleLabel)
        view.addSubview(tipView)
        view.addSubview(letsGoButton)
    }
    
    override func configureViews() {
        view.backgroundColor = .white(.light)
    }
    
    override func setupConstraints() {
        closeButton.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.Buttons.closeSize)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base09)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        tipView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base05)
            $0.centerY.equalToSuperview().priority(.low)
        }
        
        letsGoButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base04)
            $0.height.equalTo(Layout.Buttons.height)
        }
    }
}

// MARK: UI Target Methods
@objc
private extension FacialBiometricsTipViewController {
    func didTapLetsGo() {
        viewModel.didNextStep()
    }

    func didTapClose() {
        viewModel.shouldLogout()
    }
}

// MARK: View Model Outputs
extension FacialBiometricsTipViewController: FacialBiometricsTipDisplay {
    func displayTip(icon: UIImage?, description: String) {
        tipView.addTip(icon: icon, description: description)
    }
}
