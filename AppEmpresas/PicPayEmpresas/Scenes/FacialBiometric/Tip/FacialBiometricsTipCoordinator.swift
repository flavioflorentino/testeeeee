import UIKit
import FeatureFlag

enum FacialBiometricsTipAction {
    case camera
    case unlogged
    case authenticated
}

protocol FacialBiometricsTipCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: FacialBiometricsTipAction)
}

final class FacialBiometricsTipCoordinator {
    typealias Dependecies = HasAppCoordinator & HasFeatureManager
    weak var viewController: UIViewController?
    private var dependencies: Dependecies

    init(dependencies: Dependecies = DependencyContainer()) {
        self.dependencies = dependencies
    }
}

extension FacialBiometricsTipCoordinator: FacialBiometricsTipCoordinating {
    func perform(action: FacialBiometricsTipAction) {
        switch action {
        case .camera:
            let controller = FacialBiometricsTakeFactory.make()
            let navigation = UINavigationController(rootViewController: controller)
            navigation.setNavigationBarHidden(true, animated: false)
            navigation.modalPresentationStyle = .fullScreen
            viewController?.present(navigation, animated: true)
        case .unlogged:
            if dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
                dependencies.appCoordinator.displayWelcome()
            } else {
                AppManager.shared.presentUnloggedViewController()
            }
        case .authenticated:
            if dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
                dependencies.appCoordinator.displayAuthenticated()
            } else {
                AppManager.shared.presentAuthenticatedViewController()
            }
        }
    }
}
