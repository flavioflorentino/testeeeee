import UI
import UIKit

extension FacialBiometricsTipView.Layout {
    enum Images {
        static let size = CGSize(width: 28, height: 28)
    }
}

final class FacialBiometricsTipView: UIView, ViewConfiguration {
    fileprivate enum Layout {}

    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = Spacing.base05
        return stackView
    }()

    init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(containerStackView)
    }
    
    func setupConstraints() {
        containerStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    func addTip(icon: UIImage?, description: String) {
        let horizontalStackView = defaultRow()
        if let icon = icon {
            let imageView = defaultIcon(icon)
            horizontalStackView.addArrangedSubview(imageView)
        }
        let descriptionLabel = defaultLabel(description)
        horizontalStackView.addArrangedSubview(descriptionLabel)
        containerStackView.addArrangedSubview(horizontalStackView)
    }

    private func defaultRow() -> UIStackView {
        let stackView = UIStackView()
        stackView.alignment = .top
        stackView.spacing = Spacing.base02
        return stackView
    }

    private func defaultIcon(_ icon: UIImage) -> UIImageView {
        let imageView = UIImageView(image: icon)
        imageView.contentMode = .scaleAspectFit
        imageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Images.size)
        }
        return imageView
    }

    private func defaultLabel(_ description: String) -> UILabel {
        let label = UILabel()
        label.text = description
        label.typography = .bodySecondary()
        label.textColor = Colors.grayscale700.lightColor
        label.numberOfLines = 4
        label.setContentHuggingPriority(
            .required,
            for: .vertical
        )
        return label
    }
}
