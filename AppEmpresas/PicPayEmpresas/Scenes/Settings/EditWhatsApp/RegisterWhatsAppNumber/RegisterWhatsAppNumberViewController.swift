import UI
import UIKit
import Validator
import AssetsKit

protocol RegisterWhatsAppNumberDisplaying: AnyObject {
    func displayLinkText(linkText: NSAttributedString, linkAttributes: [NSAttributedString.Key: Any])
    func setRules(_ rules: ValidationRuleSet<String>)
    func enableSaveButton(enable: Bool)
    func textFieldSuccess()
    func textFieldError(message: String)
    func showError()
    func updateTextFieldAndSwitch(_ whatsApp: WhatsAppAccount)
}

private extension RegisterWhatsAppNumberViewController.Layout {
    enum Size {
        static let maxWidthLabel = CGFloat(0.8 * UIScreen.main.bounds.size.width)
    }

    enum Length {
        static let numberText = 15
    }
}

final class RegisterWhatsAppNumberViewController: ViewController<RegisterWhatsAppNumberInteracting, UIView> {
    fileprivate enum Layout { }
    private typealias Localizable = Strings.RegisterWhatsAppNumber

    private lazy var provideSwitchLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.switchText
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale950.color)
        label.preferredMaxLayoutWidth = Layout.Size.maxWidthLabel

        return label
    }()

    private lazy var provideSwitch: UISwitch = {
        let provideSwitch = UISwitch()
        provideSwitch.addTarget(self, action: #selector(switchDidChangeValue), for: .valueChanged)
        return provideSwitch
    }()

    private lazy var provideStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [provideSwitchLabel, provideSwitch])
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.spacing = Spacing.base00
        stack.alignment = .center

        return stack
    }()

    private lazy var numberTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.placeHolderText
        textField.placeholderColor = Colors.grayscale300.color
        textField.keyboardType = .numberPad
        textField.addTarget(self, action: #selector(changeTextField), for: .editingChanged)
        textField.delegate = self
        textField.title = ""

        return textField
    }()

    private lazy var cellPhoneNumberMaks: TextFieldMasker = {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cellPhoneWithDDD)
        return TextFieldMasker(textMask: mask)
    }()

    private lazy var alertLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.message
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale500.color)

        return label
    }()

    private lazy var descriptionText: UITextView = {
        let textView = UITextView()
        textView.text = Localizable.description
        textView.font = Typography.bodySecondary().font()
        textView.textColor = Colors.grayscale500.color
        textView.isEditable = false
        textView.isSelectable = true
        textView.isScrollEnabled = false
        textView.backgroundColor = .clear
        textView.textContainerInset = .zero
        textView.textContainer.lineFragmentPadding = 0
        textView.delegate = self

        return textView
    }()

    private lazy var saveButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.buttonTitleSave, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapButtonSave(_:)), for: .touchUpInside)
        button.isEnabled = false

        return button
    }()

    override func buildViewHierarchy() {
        view.addSubviews(provideStackView, numberTextField, alertLabel, descriptionText, saveButton)
    }

    override func setupConstraints() {
        provideStackView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base06)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        numberTextField.snp.makeConstraints {
            $0.top.equalTo(provideStackView).offset(Spacing.base06)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        alertLabel.snp.makeConstraints {
            $0.top.equalTo(numberTextField.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalTo(numberTextField)
        }

        descriptionText.snp.makeConstraints {
            $0.top.equalTo(alertLabel.snp.bottom).offset(Spacing.base02)
            $0.trailing.leading.equalTo(alertLabel)
        }

        saveButton.snp.makeConstraints {
            $0.top.equalTo(descriptionText.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalTo(descriptionText)
        }
    }

    override func configureViews() {
        title = Localizable.title
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        cellPhoneNumberMaks.bind(to: numberTextField)
        numberTextField.isEnabled = false
        interactor.configRules()
    }

    // MARK: - Super Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.setupLink()
        interactor.requestFetchWhatsAppAccount()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.navigationStyle(StandardNavigationStyle())
    }
}

extension RegisterWhatsAppNumberViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.count == Layout.Length.numberText && string.isNotEmpty {
            return false
        }

        return true
    }
}

// MARK: - Methods
extension RegisterWhatsAppNumberViewController {
    @objc
    private func switchDidChangeValue(_ sender: UISwitch) {
        if sender.isOn {
            numberTextField.isEnabled = true
            numberTextField.becomeFirstResponder()
        } else {
            numberTextField.text = ""
            numberTextField.isEnabled = false
        }
    }

    @objc
    private func changeTextField() {
        guard let text = numberTextField.text else {
            return
        }

        interactor.validateTextField(result: numberTextField.validate(), text: text)
    }

    @objc
    private func didTapButtonSave(_ sender: UIButton) {
        guard let text = numberTextField.text else {
            return
        }

        interactor.requestUpdateWhatsAppAccount(number: text, active: provideSwitch.isOn)
    }
}

extension RegisterWhatsAppNumberViewController: UITextViewDelegate {
    func textView(_ textView: UITextView,
                  shouldInteractWith URL: URL,
                  in characterRange: NSRange,
                  interaction: UITextItemInteraction) -> Bool {
        interactor.open(url: URL)

        return false
    }
}

// MARK: - RegisterWhatsAppNumberDisplaying
extension RegisterWhatsAppNumberViewController: RegisterWhatsAppNumberDisplaying {
    func updateTextFieldAndSwitch(_ whatsApp: WhatsAppAccount) {
        numberTextField.text = whatsApp.phoneNumber
        provideSwitch.isOn = whatsApp.active
    }
    
    func showError() {
        let primaryAlertButton = ApolloAlertAction(title: Localizable.titlePrimaryButtonAlert, dismissOnCompletion: true) {
            self.interactor.requestFetchWhatsAppAccount()
        }

        let secodaryAlertButton = ApolloAlertAction(title: Localizable.titleSecondaryButtonAlert, dismissOnCompletion: true) {
            self.dismiss(animated: true)
        }

        showApolloAlert(image: Resources.Illustrations.iluConstruction.image,
                        title: Localizable.errorAlertTitle,
                        subtitle: Localizable.errorAlertMessage,
                        primaryButtonAction: primaryAlertButton,
                        linkButtonAction: secodaryAlertButton)
    }
    
    func textFieldError(message: String) {
        numberTextField.errorMessage = " "

        alertLabel.text = message
        alertLabel.textColor = Colors.critical600.color
    }

    func textFieldSuccess() {
        numberTextField.lineColor = Colors.branding400.color
        numberTextField.errorMessage = ""

        alertLabel.text = Localizable.message
        alertLabel.textColor = Colors.grayscale500.color
    }

    func displayLinkText(linkText: NSAttributedString, linkAttributes: [NSAttributedString.Key: Any]) {
        descriptionText.attributedText = linkText
        descriptionText.linkTextAttributes = linkAttributes
    }

    func setRules(_ rules: ValidationRuleSet<String>) {
        numberTextField.validationRules = rules
    }

    func enableSaveButton(enable: Bool) {
        saveButton.isEnabled = enable
    }
}
