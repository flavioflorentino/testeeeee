import Foundation
import Core

enum RegisterWhatsAppNumberEndpoint {
    case fetchWhatsAppNumber(Int)
    case updateWhatsAppNumber(id: Int, updateNumber: UpdateWhatsAppAccount)
}

extension RegisterWhatsAppNumberEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case let .fetchWhatsAppNumber(id), let .updateWhatsAppNumber(id, _):
            return "/seller/\(id)/whatsapp_number"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .fetchWhatsAppNumber:
            return .get
        case .updateWhatsAppNumber:
            return .post
        }
    }

    var body: Data? {
        switch self {
        case let .updateWhatsAppNumber(_, updateNumber):
            return try? JSONEncoder().encode(updateNumber)
        case .fetchWhatsAppNumber:
            return nil
        }
    }
}
