import Foundation
import Core

protocol RegisterWhatsAppNumberServicing {
    func fetchWhatsAppAccount(_ sellerId: Int, completion: @escaping(Result<WhatsAppAccount, ApiError>) -> Void)
    func updateWhatsAppAccount(_ sellerId: Int,
                               _ updateAccount: UpdateWhatsAppAccount,
                               completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

final class RegisterWhatsAppNumberService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RegisterWhatsAppNumberServicing
extension RegisterWhatsAppNumberService: RegisterWhatsAppNumberServicing {
    func updateWhatsAppAccount(_ sellerId: Int,
                               _ updateAccount: UpdateWhatsAppAccount,
                               completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = RegisterWhatsAppNumberEndpoint.updateWhatsAppNumber(id: sellerId, updateNumber: updateAccount)
        BizApi<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                BizApiHelper.unwrapBizResult(result: result, completion: completion)
            }
        }
    }

    func fetchWhatsAppAccount(_ sellerId: Int, completion: @escaping (Result<WhatsAppAccount, ApiError>) -> Void) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase

        let endpoint = RegisterWhatsAppNumberEndpoint.fetchWhatsAppNumber(sellerId)
        BizApiV2<WhatsAppAccount>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }
}
