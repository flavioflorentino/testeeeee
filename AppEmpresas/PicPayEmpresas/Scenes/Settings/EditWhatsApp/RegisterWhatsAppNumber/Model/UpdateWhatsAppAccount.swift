import Foundation

struct UpdateWhatsAppAccount: Encodable {
    let phoneNumber: String
    let active: Bool
    let hash2fa: String

    private enum CodingKeys: String, CodingKey {
        case phoneNumber = "phone_number"
        case active
        case hash2fa
    }
}
