import Foundation

struct WhatsAppAccount: Decodable, Equatable {
    let phoneNumber: String
    let active: Bool
}
