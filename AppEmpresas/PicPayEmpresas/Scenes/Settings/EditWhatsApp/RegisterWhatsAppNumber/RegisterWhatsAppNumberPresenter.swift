import Foundation
import UI
import Validator
import FeatureFlag

protocol RegisterWhatsAppNumberPresenting: AnyObject {
    var viewController: RegisterWhatsAppNumberDisplaying? { get set }
    func presentLink()
    func open(url: URL)
    func prepareRules(_ rules: ValidationRuleSet<String>)
    func didNextStep(action: RegisterWhatsAppNumberAction)
    func updateTextFieldState(_ message: String?)
    func shouldUpdateTextFieldAndSwitch(_ whatsApp: WhatsAppAccount)
    func presentError()
}

final class RegisterWhatsAppNumberPresenter {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    private typealias Localizable = Strings.RegisterWhatsAppNumber

    private let coordinator: RegisterWhatsAppNumberCoordinating
    weak var viewController: RegisterWhatsAppNumberDisplaying?

    init(coordinator: RegisterWhatsAppNumberCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

extension RegisterWhatsAppNumberPresenter: AttributedTextURLDelegate {
    func getUrlInfo(linkType: String) -> AttributedTextURLInfo {
        AttributedTextURLInfo(
            urlAddress: dependencies.featureManager.text(.urlWhatsAppExplanationFaq),
            text: Localizable.descriptionLink
        )
    }
}

// MARK: - RegisterWhatsAppNumberPresenting
extension RegisterWhatsAppNumberPresenter: RegisterWhatsAppNumberPresenting {
    func presentError() {
        viewController?.showError()
    }

    func shouldUpdateTextFieldAndSwitch(_ whatsApp: WhatsAppAccount) {
        viewController?.updateTextFieldAndSwitch(whatsApp)
    }

    func presentLink() {
        let attributedText = AttributedTextURLHelper.createAttributedTextWithLinks(
            text: Localizable.description,
            attributedTextURLDelegate: self,
            linkTypes: [Localizable.descriptionLink]
        )

        viewController?.displayLinkText(linkText: attributedText, linkAttributes: AttributedTextURLHelper.linkAttributes)
    }

    func open(url: URL) {
        coordinator.perform(action: .open(url: url))
    }

    func prepareRules(_ rules: ValidationRuleSet<String>) {
        viewController?.setRules(rules)
    }
    
    func didNextStep(action: RegisterWhatsAppNumberAction) {
        coordinator.perform(action: action)
    }

    func updateTextFieldState(_ message: String?) {
        guard let message = message else { return }
        
        if message.isEmpty {
            viewController?.textFieldSuccess()
            viewController?.enableSaveButton(enable: true)
        } else {
            viewController?.textFieldError(message: message)
            viewController?.enableSaveButton(enable: false)
        }
    }
}
