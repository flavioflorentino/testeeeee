import Foundation
import LegacyPJ
import Validator

protocol RegisterWhatsAppNumberInteracting: AnyObject {
    func setupLink()
    func open(url: URL)
    func configRules()
    func validateTextField(result: ValidationResult, text: String)
    func requestFetchWhatsAppAccount()
    func requestUpdateWhatsAppAccount(number: String, active: Bool)
}

enum PhoneValidationError: String, CustomStringConvertible, Error, ValidationError {
    case phoneNumberRequired

    var message: String {
        Strings.RegisterWhatsAppNumber.errorMessage
    }

    var description: String {
        Strings.RegisterWhatsAppNumber.errorMessage
    }
}

final class RegisterWhatsAppNumberInteractor {
    typealias Dependencies = HasAuthManager
    private let dependencies: Dependencies

    private let service: RegisterWhatsAppNumberServicing
    private let presenter: RegisterWhatsAppNumberPresenting
    private let lengthNumber = 15
    private let sellerID: Int?

    init(service: RegisterWhatsAppNumberServicing, presenter: RegisterWhatsAppNumberPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.sellerID = self.dependencies.authManager.user?.id
    }
}

// MARK: - RegisterWhatsAppNumberInteracting
extension RegisterWhatsAppNumberInteractor: RegisterWhatsAppNumberInteracting {
    func requestUpdateWhatsAppAccount(number: String, active: Bool) {
        guard let sellerID = sellerID else {
            return // TODO: Estudar colocar um log para o NewRelic
        }

        // TODO: Proximos PRs, chamar o fluxo do 2fa

        let whatsApp = UpdateWhatsAppAccount(phoneNumber: number, active: active, hash2fa: "")
        service.updateWhatsAppAccount(sellerID, whatsApp) { [weak self] result in
            guard let self = self else {
                return
            }

            switch result {
            case .success:
                self.presenter.didNextStep(action: .goToNextStep)
            case .failure:
                self.presenter.presentError()
            }
        }
    }

    func requestFetchWhatsAppAccount() {
        guard let sellerID = sellerID else {
            return // TODO: Estudar colocar um log para o NewRelic
        }

        service.fetchWhatsAppAccount(sellerID) { [weak self] result in
            guard let self = self else {
                return
            }

            switch result {
            case .success(let number):
                self.presenter.shouldUpdateTextFieldAndSwitch(number)
            case .failure(let error):
                switch error {
                case .notFound:
                    self.presenter.shouldUpdateTextFieldAndSwitch(WhatsAppAccount(phoneNumber: "", active: false))
                default:
                    self.presenter.presentError()
                }
            }
        }
    }

    func setupLink() {
        presenter.presentLink()
    }

    func open(url: URL) {
        presenter.open(url: url)
    }

    func configRules() {
        presenter.prepareRules(createRules())
    }

    func validateTextField(result: ValidationResult, text: String) {
        let message: String? = {
            switch result {
            case .invalid(let error):
                return error.first?.message
            default:
                return ""
            }
        }()

        if text.count > 4, !verifyDDD(text: text) {
            presenter.updateTextFieldState(Strings.RegisterWhatsAppNumber.errorMessage)
            return
        }

        presenter.updateTextFieldState(message)
    }
}

// MARK: - Private Methods
private extension RegisterWhatsAppNumberInteractor {
    func createRules() -> ValidationRuleSet<String> {
        var phoneRules = ValidationRuleSet<String>()
        phoneRules.add(rule: ValidationRuleLength(min: lengthNumber,
                                                  max: lengthNumber,
                                                  lengthType: .characters,
                                                  error: PhoneValidationError.phoneNumberRequired))
        phoneRules.add(rule: ValidationRuleRequired(error: PhoneValidationError.phoneNumberRequired))

        return phoneRules
    }

    func verifyDDD(text: String) -> Bool {
        guard let ddd = text.split(separator: ")").first?.split(separator: "(").first, let dddInt = Int(ddd) else {
           return false
        }

        return dddInt > 0
    }
}
