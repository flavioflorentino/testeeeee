import UIKit

enum RegisterWhatsAppNumberAction: Equatable {
    case open(url: URL)
    case goToNextStep
}

protocol RegisterWhatsAppNumberCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegisterWhatsAppNumberAction)
}

final class RegisterWhatsAppNumberCoordinator {
    weak var viewController: UIViewController?
    let deeplinkHelper: DeeplinkHelper

    init(deeplinkHelper: DeeplinkHelper) {
        self.deeplinkHelper = deeplinkHelper
    }
}

// MARK: - RegisterWhatsAppNumberCoordinating
extension RegisterWhatsAppNumberCoordinator: RegisterWhatsAppNumberCoordinating {
    func perform(action: RegisterWhatsAppNumberAction) {
        switch action {
        case .open(let url):
            guard let navController = viewController?.navigationController else {
                return
            }

            deeplinkHelper.open(url: url, from: navController)
        case .goToNextStep:
            // TODO: Proximos PRs, concluir o fluxo em caso de sucesso, conferir fluxo verificar se faz sentido a nomenclatura desse case.
            break
        }
    }
}
