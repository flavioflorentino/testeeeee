import UIKit

enum RegisterWhatsAppNumberFactory {
    static func make() -> RegisterWhatsAppNumberViewController {
        let container = DependencyContainer()
        let service: RegisterWhatsAppNumberServicing = RegisterWhatsAppNumberService(dependencies: container)
        let coordinator: RegisterWhatsAppNumberCoordinating =
            RegisterWhatsAppNumberCoordinator(deeplinkHelper: DeeplinkHelper(dependencies: container))
        let presenter: RegisterWhatsAppNumberPresenting = RegisterWhatsAppNumberPresenter(coordinator: coordinator,
                                                                                          dependencies: container)
        let interactor = RegisterWhatsAppNumberInteractor(service: service,
                                                          presenter: presenter,
                                                          dependencies: container)
        let viewController = RegisterWhatsAppNumberViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
