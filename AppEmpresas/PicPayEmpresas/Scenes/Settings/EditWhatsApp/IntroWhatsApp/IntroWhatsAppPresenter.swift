import Foundation

protocol IntroWhatsAppPresenting: AnyObject {
    var viewController: IntroWhatsAppDisplaying? { get set }
    func didNextStep(action: IntroWhatsAppAction)
    func open(url: URL)
    func presentError()
}

final class IntroWhatsAppPresenter {
    private let coordinator: IntroWhatsAppCoordinating
    weak var viewController: IntroWhatsAppDisplaying?

    init(coordinator: IntroWhatsAppCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - IntroWhatsAppPresenting
extension IntroWhatsAppPresenter: IntroWhatsAppPresenting {
    func presentError() {
        viewController?.displayError()
    }

    func open(url: URL) {
        coordinator.perform(action: .open(url: url))
    }

    func didNextStep(action: IntroWhatsAppAction) {
        coordinator.perform(action: action)
    }
}
