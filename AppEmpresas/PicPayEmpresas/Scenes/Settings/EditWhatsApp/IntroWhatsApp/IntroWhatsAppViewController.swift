import UI
import AssetsKit
import UIKit

protocol IntroWhatsAppDisplaying: AnyObject {
    func displayError()
}

private extension IntroWhatsAppViewController.Layout {
    enum Size {
        static let image: CGFloat = 160.0
    }
}

final class IntroWhatsAppViewController: ViewController<IntroWhatsAppInteracting, UIView> {
    // MARK: - Properties
    fileprivate enum Layout { }
    private typealias Localizable = Strings.IntroWhatsApp

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: Resources.Illustrations.iluAgenda.image)
        imageView.contentMode = .scaleAspectFit

        return imageView
    }()

    private lazy var stackViewTexts: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [labelTitle, labelDescription])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base06

        return stackView
    }()

    private lazy var labelTitle: UILabel = {
        let label = UILabel()
        label.text = Localizable.contentTitle
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.grayscale600.color)

        return label
    }()

    private lazy var labelDescription: UILabel = {
        let label = UILabel()
        label.text = Localizable.contentDescription
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.grayscale600.color)

        return label
    }()

    private lazy var stackViewButtons: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [buttonWhatsApp, buttonHowWorks])
        stack.axis = .vertical
        stack.distribution = .equalSpacing
        stack.spacing = Spacing.base01

        return stack
    }()

    private lazy var buttonWhatsApp: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.buttonTitleWhatsApp, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapButtonWhatsApp(_:)), for: .touchUpInside)

        return button
    }()

    private lazy var buttonHowWorks: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.buttonTitleHowWorks, for: .normal)
        button.buttonStyle(SecondaryButtonStyle())
        button.addTarget(self, action: #selector(didTapButtonHowWorks(_:)), for: .touchUpInside)

        return button
    }()

    // MARK: - Configuration Methods
    override func buildViewHierarchy() {
        view.addSubview(imageView)
        view.addSubview(stackViewTexts)
        view.addSubview(stackViewButtons)
    }

    override func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base05)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.image)
        }

        stackViewTexts.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }

        stackViewButtons.snp.makeConstraints {
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }

    override func configureViews() {
        title = Localizable.title
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }

    // MARK: - Super Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.navigationStyle(StandardNavigationStyle())
    }
}

// MARK: - Methods
private extension IntroWhatsAppViewController {
    @objc
    func didTapButtonWhatsApp(_ sender: UIButton) {
        interactor.buttonWhatsAppTapped()
    }

    @objc
    func didTapButtonHowWorks(_ sender: UIButton) {
        interactor.buttonHowWorksTapped()
    }
}

// MARK: - IntroWhatsAppDisplaying
extension IntroWhatsAppViewController: IntroWhatsAppDisplaying {
    func displayError() {
        let primaryAlertButton = ApolloAlertAction(title: Localizable.titlePrimaryButtonAlert, dismissOnCompletion: true) {
            self.interactor.buttonHowWorksTapped()
        }

        let secodaryAlertButton = ApolloAlertAction(title: Localizable.titleSecondaryButtonAlert, dismissOnCompletion: true) {
            self.dismiss(animated: true)
        }

        showApolloAlert(image: Resources.Illustrations.iluConstruction.image,
                        title: Localizable.errorAlertTitle,
                        subtitle: Localizable.errorAlertMessage,
                        primaryButtonAction: primaryAlertButton,
                        linkButtonAction: secodaryAlertButton)
    }
}
