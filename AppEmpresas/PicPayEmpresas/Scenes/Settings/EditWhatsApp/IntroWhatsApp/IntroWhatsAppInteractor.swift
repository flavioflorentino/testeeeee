import Foundation
import FeatureFlag

protocol IntroWhatsAppInteracting: AnyObject {
    func buttonWhatsAppTapped()
    func buttonHowWorksTapped()
}

final class IntroWhatsAppInteractor {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    private let presenter: IntroWhatsAppPresenting

    init(presenter: IntroWhatsAppPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - IntroWhatsAppInteracting
extension IntroWhatsAppInteractor: IntroWhatsAppInteracting {
    func buttonWhatsAppTapped() {
        presenter.didNextStep(action: .buttonWhatsApp)
    }

    func buttonHowWorksTapped() {
        guard let url = URL(string: dependencies.featureManager.text(.urlWhatsAppExplanationWebview)) else {
            presenter.presentError()
            return
        }

        presenter.open(url: url)
    }
}
