import UIKit

enum IntroWhatsAppAction: Equatable {
    case buttonWhatsApp
    case open(url: URL)
}

protocol IntroWhatsAppCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: IntroWhatsAppAction)
}

final class IntroWhatsAppCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - IntroWhatsAppCoordinating
extension IntroWhatsAppCoordinator: IntroWhatsAppCoordinating {
    func perform(action: IntroWhatsAppAction) {
        switch action {
        case .buttonWhatsApp:
            let controller = RegisterWhatsAppNumberFactory.make()
            viewController?.pushViewController(controller)
        case .open(let url):
            let controller = ViewsManager.webViewController(url: url.absoluteString,
                                                            title: Strings.IntroWhatsApp.buttonTitleHowWorks)
            viewController?.pushViewController(controller)
        }
    }
}
