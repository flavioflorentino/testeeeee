import UIKit

enum IntroWhatsAppFactory {
    static func make() -> IntroWhatsAppViewController {
        let container = DependencyContainer()
        let coordinator: IntroWhatsAppCoordinating = IntroWhatsAppCoordinator()
        let presenter: IntroWhatsAppPresenting = IntroWhatsAppPresenter(coordinator: coordinator)
        let interactor = IntroWhatsAppInteractor(presenter: presenter, dependencies: container)
        let viewController = IntroWhatsAppViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
