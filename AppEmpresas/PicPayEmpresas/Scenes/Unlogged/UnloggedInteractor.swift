import Core
import Foundation
import AnalyticsModule
import FeatureFlag
import LegacyPJ

protocol UnloggedInteracting: AnyObject {
    func showNextScreen()
    func showLogin()
    func showHelp()
    func checkRegisterProgress()
    func checkAuthentication()
}

final class UnloggedInteractor {
    typealias Dependencies = HasAuthManager & HasAnalytics & HasFeatureManager & HasKeychainManager
    
    private enum Event {
        static let beginRegistration = "begin_registration"
    }
    
    private let service: UnloggedServicing
    private let presenter: UnloggedPresenting
    private let dependencies: Dependencies
    private let isRoot: Bool
    
    init(service: UnloggedServicing, presenter: UnloggedPresenting, dependencies: Dependencies, isRoot: Bool) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.isRoot = isRoot
    }
    
    private func trackingRegister() {
        dependencies.analytics.log(NewRegistrationAnalyticsFirebase.beginRegistration)
        
        let isAuthenticated = dependencies.authManager.isAuthenticated
        let log = isAuthenticated ? NewRegistrationAnalytics.obMultRegister : NewRegistrationAnalytics.obRegister
        
        dependencies.analytics.log(log)
    }
}

extension UnloggedInteractor: UnloggedInteracting {
    func showNextScreen() {
        guard dependencies.featureManager.isActive(.featureRegistrationIntroPresenting) else {
            trackingRegister()
            presenter.didNextStep(action: .register)
            return
        }
        dependencies.analytics.log(WelcomeRegistrationAnalytics.onboarding)
        presenter.didNextStep(action: .welcome)
    }
    
    func showLogin() {
        TrackingManager.trackOneTimeEvent(TrackingManager.TrackingManagerEvents.ftLogin, properties: nil)
        presenter.didNextStep(action: .login(isRoot))
    }
    
    func showHelp() {
        presenter.didNextStep(action: .help)
    }
    
    func checkRegisterProgress() {
        guard let hash = dependencies.keychain.getData(key: KeychainKeyPJ.registerStatus) else {
            return
        }
        
        let data = Data(hash.utf8)
        let decoder = JSONDecoder()
        
        guard let model = try? decoder.decode(RegistrationStatus.self, from: data) else {
            return
        }
        
        presenter.configureViewKindOfContinueRegister(model: model)
    }

    func checkAuthentication() {
        guard isRoot else { return }
        presenter.didNextStep(action: .start)
    }
}
