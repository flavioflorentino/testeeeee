import Foundation

protocol UnloggedPresenting: AnyObject {
    var viewController: UnloggedDisplay? { get set }
    func didNextStep(action: UnloggedAction)
    func configureViewKindOfContinueRegister(model: RegistrationStatus)
}

final class UnloggedPresenter: UnloggedPresenting {
    private let coordinator: UnloggedCoordinating
    weak var viewController: UnloggedDisplay?

    init(coordinator: UnloggedCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: UnloggedAction) {
        coordinator.perform(action: action)
    }
    
    func configureViewKindOfContinueRegister(model: RegistrationStatus) {
        viewController?.configureViewContinueRegisterStyle(with: model.name)
    }
}
