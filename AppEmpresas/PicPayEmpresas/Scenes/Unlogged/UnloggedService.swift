import Core
import Foundation
import LegacyPJ

protocol UnloggedServicing {
    func userISAuthenticated() -> Bool
    func localRegistrationStatus() -> RegistrationStatus?
}

final class UnloggedService: UnloggedServicing {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    
    let authManager: AuthManager
    init(authManager: AuthManager) {
        self.authManager = authManager
    }
    
    func userISAuthenticated() -> Bool {
        authManager.isAuthenticated
    }
    
    func localRegistrationStatus() -> RegistrationStatus? {
        guard let resultString = dependencies.keychain.getData(key: KeychainKeyPJ.registerStatus) else {
            return nil
        }
        
        let data = Data(resultString.utf8)
        let decoder = JSONDecoder()
        let model = try? decoder.decode(RegistrationStatus.self, from: data)
        
        return model
    }
}

struct RegistrationStatus: Codable {
    let completed: Bool
    let step: String?
    let sequence: [String?]
    let name: String
}
