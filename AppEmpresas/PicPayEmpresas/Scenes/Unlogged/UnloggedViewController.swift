import AssetsKit
import SnapKit
import UI
import UIKit
import FeatureFlag

protocol UnloggedDisplay: AnyObject {
    func configureViewContinueRegisterStyle(with name: String)
}

private extension UnloggedViewController.Layout {
    enum Button {
        static let cornerRadius = heightButton / 2
        static let heightButton: CGFloat = 44
    }
    
    enum Margins {
        static let borderOffset = Spacing.base02
        static let bottomOffsetBettwenButtons = Spacing.base01
        static let loginButtonOffsetBottom = Spacing.base06
    }
}

final class UnloggedViewController: ViewController<UnloggedInteracting, UIView> {
    typealias Dependecies = HasFeatureManager
    private typealias Localizable = Strings.Unlogged
    private var dependencies: Dependecies = DependencyContainer()
    
    // MARK: - Private Properties
    
    fileprivate struct Layout { }
    
    private lazy var backgroundImage: UIImageView = {
        let imageView = UIImageView(image: Assets.Background.pjBgUnloggedDefault.image)
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, Colors.white.color)
        label.text = Localizable.titleDefault
        return label
    }()
    
    private lazy var registerButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.registerButtonTile, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(showNextScreen), for: .touchUpInside)
        return button
    }()
    
    private lazy var continueRegisterButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.continueRegisterButtonTitle, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(showNextScreen), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private lazy var loginButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.loginButtonTitle, for: .normal)
        button
            .buttonStyle(SecondaryButtonStyle())
            .with(\.textColor, (.branding600(), .normal))
        button.addTarget(self, action: #selector(showLogin), for: .touchUpInside)
        return button
    }()
    
    private lazy var newRegisterButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.newRegisterButtonTitle, for: .normal)
        button
            .buttonStyle(SecondaryButtonStyle())
            .with(\.textColor, (.branding600(), .normal))
        button.addTarget(self, action: #selector(showNextScreen), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private lazy var needHelpButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.helpButtonTitle, for: .normal)
        button
            .buttonStyle(LinkButtonStyle())
            .with(\.typography, .icons(.small))
            .with(\.textAttributedColor, (.branding600(), .normal))
        
        button.addTarget(self, action: #selector(showHelp), for: .touchUpInside)
        return button
    }()
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.checkRegisterProgress()
        interactor.checkAuthentication()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceNewTransparent())
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let navigationBarAppearance: NavigationBarAppearance = dependencies.featureManager.isActive(.isFastAccessAvailable)
            ? NavigationBarAppearanceDefault()
            : NavigationBarAppearanceNewTransparent()
        navigationController?.navigationBar.setup(appearance: navigationBarAppearance)
    }
 
    override func buildViewHierarchy() {
        view.addSubviews(
            backgroundImage,
            titleLabel,
            registerButton,
            continueRegisterButton,
            loginButton,
            newRegisterButton,
            needHelpButton
        )
    }
    
    override func setupConstraints() {
        backgroundImage.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        registerButton.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Layout.Margins.borderOffset)
            $0.trailing.equalToSuperview().inset(Layout.Margins.borderOffset)
        }
        
        continueRegisterButton.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Layout.Margins.borderOffset)
            $0.trailing.equalToSuperview().inset(Layout.Margins.borderOffset)
        }

        loginButton.snp.makeConstraints {
            $0.top.equalTo(registerButton.snp.bottom).offset(Layout.Margins.bottomOffsetBettwenButtons)
            $0.leading.equalToSuperview().offset(Layout.Margins.borderOffset)
            $0.trailing.equalToSuperview().inset(Layout.Margins.borderOffset)
        }
        
        newRegisterButton.snp.makeConstraints {
            $0.top.equalTo(registerButton.snp.bottom).offset(Layout.Margins.bottomOffsetBettwenButtons)
            $0.leading.equalToSuperview().offset(Layout.Margins.borderOffset)
            $0.trailing.equalToSuperview().inset(Layout.Margins.borderOffset)
        }

        needHelpButton.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(loginButton.snp.bottom).offset(Layout.Margins.loginButtonOffsetBottom)
            $0.bottom.equalToSuperview().inset(Layout.Margins.borderOffset)
        }
    }
}

// MARK: - Actions

@objc
private extension UnloggedViewController {
    func showNextScreen() {
        viewModel.showNextScreen()
    }
    
    func showLogin() {
        viewModel.showLogin()
    }
    
    func showHelp() {
        viewModel.showHelp()
    }
}

// MARK: View Model Outputs

extension UnloggedViewController: UnloggedDisplay {
    func configureViewContinueRegisterStyle(with name: String) {
        backgroundImage.image = Assets.bgUnloggedContinueRegister.image
        titleLabel.text = Localizable.titleContinueRegister(name)
        registerButton.isHidden = true
        loginButton.isHidden = true
        continueRegisterButton.isHidden = false
        newRegisterButton.isHidden = false
    }
}
