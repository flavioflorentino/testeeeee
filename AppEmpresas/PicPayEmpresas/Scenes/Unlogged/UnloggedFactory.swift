import Foundation
import LegacyPJ

enum UnloggedFactory {
    static func make(isRoot: Bool) -> UnloggedViewController {
        let container = DependencyContainer()
        let service: UnloggedServicing = UnloggedService(authManager: AuthManager.shared)
        let coordinator: UnloggedCoordinating = UnloggedCoordinator(dependencies: container)
        let presenter: UnloggedPresenting = UnloggedPresenter(coordinator: coordinator)
        let interactor = UnloggedInteractor(service: service, presenter: presenter, dependencies: container, isRoot: isRoot)
        let viewController = UnloggedViewController(viewModel: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
