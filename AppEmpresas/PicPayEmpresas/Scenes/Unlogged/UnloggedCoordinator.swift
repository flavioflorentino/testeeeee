import CustomerSupport
import UIKit
import AuthenticationPJ
import LegacyPJ
import FeatureFlag

enum UnloggedAction: Equatable {
    case register
    case welcome
    case login(_ isRoot: Bool)
    case help
    case start
}

protocol UnloggedCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: UnloggedAction)
}

final class UnloggedCoordinator: UnloggedCoordinating {
    typealias Dependencies = HasAuthManager & HasAppCoordinator & HasFeatureManager
    weak var viewController: UIViewController?
    private let dependencies: Dependencies
    private lazy var authenticationCoordinator: AuthenticationCoordinator? = {
        guard let navigationController = viewController?.navigationController else {
            return nil
        }
        return AuthenticationCoordinator(from: navigationController, delegate: self)
    }()

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    private lazy var registerViewController = RegistrationPersonalInfoFactory.make(model: RegistrationViewModel())
    private lazy var welcomeViewController = WelcomeFactory.make()
    private lazy var loginViewController: LoginCnpjViewController = ViewsManager.instantiateViewController(.main)
    private lazy var customerSupportManager = BIZCustomerSupportManager()
    
    func perform(action: UnloggedAction) {
        switch action {
        case .register:
            viewController?.navigationController?.pushViewController(registerViewController, animated: true)
        case .welcome:
            viewController?.navigationController?.pushViewController(welcomeViewController, animated: true)
        case let .login(isRoot):
            if dependencies.featureManager.isActive(.isFastAccessAvailable) {
                authenticationCoordinator?.login(isAddingAccount: !isRoot)
            } else {
                viewController?.navigationController?.pushViewController(loginViewController, animated: true)
            }
        case .help:
            guard let viewController = viewController else { return }
            customerSupportManager.presentFAQ(from: viewController)
        case .start:
            guard dependencies.featureManager.isActive(.isFastAccessAvailable) else {
                return
            }
            authenticationCoordinator?.start()
        }
    }
}

extension UnloggedCoordinator: AuthenticationDelegate {
    func authenticationWasCompleted(_ navigation: UINavigationController, userAuthResponse: UserAuthResponse) {
        guard let authResponse = userAuthResponse.auth,
              let userResponse = userAuthResponse.user else {
            return
        }

        guard authResponse.completed else {
            showUserNeedsCompleteAccountAlert()
            return
        }

        let userAuth = makeUserAuth(authResponse: authResponse,
                                    userResponse: userResponse)
        dependencies.authManager.storeUserAuth(userAuth: userAuth)
        dependencies.appCoordinator.displayAuthenticated()
    }

    func authenticationDidTapFAQ(_ navigation: UINavigationController) {
        customerSupportManager.presentFAQ(from: navigation)
    }

    func authenticationDidTapForgotPassword(_ navigation: UINavigationController) {
        let title = Strings.Default.forgotPasswordTitle
        let controller = ViewsManager.webViewController(url: urlForgetPassword, title: title)
        controller.addScript(file: "Script")
        navigation.pushViewController(controller, animated: true)
    }

    func authenticationDidTapRegister(_ navigation: UINavigationController) {
        navigation.pushViewController(registerViewController, animated: true)
    }
}

private extension UnloggedCoordinator {
    func makeUserAuth(authResponse: AuthResponse, userResponse: UserResponse) -> UserAuth {
        let auth = makeAuth(authResponse: authResponse)
        let user = makeUser(userResponse: userResponse)

        return UserAuth(auth: auth, user: user)
    }

    func makeUser(userResponse: UserResponse) -> User {
        let superId = (userResponse.superId as NSNumber).boolValue
        let role = (userResponse.role as NSNumber).boolValue
        let isAdmin = superId || role
        return User(id: userResponse.id,
                    name: userResponse.name,
                    username: userResponse.username,
                    isAdmin: isAdmin,
                    phone: userResponse.mobilePhone,
                    email: userResponse.email,
                    imageUrl: userResponse.imageUrl,
                    cpf: userResponse.cnpj,
                    birthDate: userResponse.birthDateFormated)
    }

    func makeAuth(authResponse: AuthResponse) -> Auth {
        let biometry = FacialBiometricsStatus(rawValue: authResponse.biometry)
        return Auth(accessToken: authResponse.accessToken,
                    tokenType: authResponse.tokenType,
                    expires: authResponse.expires,
                    refreshToken: authResponse.refreshToken,
                    biometry: biometry,
                    completed: authResponse.completed)
    }

    func showUserNeedsCompleteAccountAlert() {
        let alertController = UIAlertController(
            title: Strings.Default.oops,
            message: Strings.Default.accountCompletionMessage,
            preferredStyle: .alert
        )

        let completeAction = UIAlertAction(title: Strings.Default.accountCompletionAction, style: .default) { _ in
            self.completeSignUp()
        }

        alertController.addAction(completeAction)

        viewController?.present(alertController, animated: true)
    }

    func completeSignUp() {
        guard let url = URL(string: dependencies.featureManager.text(.urlCompleteAccount)) else {
            return
        }
        UIApplication.shared.open(url)
    }
}
