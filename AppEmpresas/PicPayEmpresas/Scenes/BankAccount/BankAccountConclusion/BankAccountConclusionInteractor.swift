import Foundation

protocol BankAccountConclusionInteracting: AnyObject {
    func backAction()
}

final class BankAccountConclusionInteractor {
    private let presenter: BankAccountConclusionPresenting

    init(presenter: BankAccountConclusionPresenting) {
        self.presenter = presenter
    }
}

// MARK: - BankAccountConclusionInteracting
extension BankAccountConclusionInteractor: BankAccountConclusionInteracting {
    func backAction() {
        presenter.didNextStep(action: .back)
    }
}
