import UIKit

enum BankAccountConclusionFactory {
    static func make() -> BankAccountConclusionViewController {
        let coordinator: BankAccountConclusionCoordinating = BankAccountConclusionCoordinator()
        let presenter: BankAccountConclusionPresenting = BankAccountConclusionPresenter(coordinator: coordinator)
        let interactor = BankAccountConclusionInteractor(presenter: presenter)
        let viewController = BankAccountConclusionViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
