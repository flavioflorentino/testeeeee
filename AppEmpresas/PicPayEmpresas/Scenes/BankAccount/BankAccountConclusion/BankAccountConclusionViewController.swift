import AssetsKit
import SnapKit
import UI
import UIKit

protocol BankAccountConclusionDisplaying: AnyObject {
}

private extension BankAccountConclusionViewController.Layout {
    enum BackButton {
        static let height: CGFloat = 48
    }
}

final class BankAccountConclusionViewController: ViewController<BankAccountConclusionInteracting, UIView> {
    fileprivate typealias Localizable = Strings.BankAccount.Conclusion
    fileprivate enum Layout {}
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleSubtitleStackView, imageContainerView, backButton])
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        return stackView
    }()
    
    private lazy var titleSubtitleStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, Colors.black.color)
        label.text = Localizable.title
        return label
    }()

    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale600.color)
        label.text = Localizable.subtitle
        return label
    }()
    
    private lazy var imageContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: Resources.Illustrations.iluSuccess.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var backButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.Button.back, for: .normal)
        button.addTarget(self, action: #selector(didTapBack), for: .touchUpInside)
        return button
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
 
    override func buildViewHierarchy() {
        imageContainerView.addSubview(imageView)
        
        view.addSubview(stackView)
    }
    
    override func configureViews() {
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
    
    override func setupConstraints() {
        stackView.snp.makeConstraints {
            if #available(iOS 11, *) {
                $0.top.equalTo(view.safeAreaLayoutGuide).offset(Spacing.base04)
                $0.bottom.equalTo(view.safeAreaLayoutGuide).inset(Spacing.base02)
            } else {
                $0.top.equalToSuperview().offset(Spacing.base04)
                $0.bottom.equalToSuperview().inset(Spacing.base02)
            }
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        imageView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base06)
            $0.height.equalTo(imageView.snp.width)
        }
        
        backButton.snp.makeConstraints {
            $0.height.equalTo(Layout.BackButton.height)
        }
    }
}

// MARK: UI Target Methods
@objc
private extension BankAccountConclusionViewController {
    func didTapBack() {
        interactor.backAction()
    }
}

extension BankAccountConclusionViewController: BankAccountConclusionDisplaying { }
