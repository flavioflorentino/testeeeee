import UIKit

enum BankAccountConclusionAction {
    case back
}

protocol BankAccountConclusionCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BankAccountConclusionAction)
}

final class BankAccountConclusionCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - BankAccountConclusionCoordinating
extension BankAccountConclusionCoordinator: BankAccountConclusionCoordinating {
    func perform(action: BankAccountConclusionAction) {
        switch action {
        case .back:
            viewController?.navigationController?.popToRootViewController(animated: true)
        }
    }
}
