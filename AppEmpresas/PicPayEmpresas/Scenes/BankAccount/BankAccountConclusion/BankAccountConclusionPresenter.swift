import Foundation

protocol BankAccountConclusionPresenting: AnyObject {
    var viewController: BankAccountConclusionDisplaying? { get set }
    func didNextStep(action: BankAccountConclusionAction)
}

final class BankAccountConclusionPresenter {
    private let coordinator: BankAccountConclusionCoordinating
    weak var viewController: BankAccountConclusionDisplaying?

    init(coordinator: BankAccountConclusionCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - BankAccountConclusionPresenting
extension BankAccountConclusionPresenter: BankAccountConclusionPresenting {
    func didNextStep(action: BankAccountConclusionAction) {
        coordinator.perform(action: action)
    }
}
