import UIKit

protocol BankAccountDisplay: AnyObject {
    func bankAccount(_ bankAccount: BankAccountItem)
    func prepareViewForPresent()
    func prepareViewForEdit()
    func operationField(hidden: Bool)
    func personalDocumentField(_ cpf: String)
    func companyDocumentField(_ cnpj: String)
    func showErrorMessage(_ message: String)
    func showPopUp(with message: String)
    func didChangeBank(_ bank: BankItem)
    func startLoadingAnimation()
    func stopLoadingAnimation()
    func showLockedBankAccountMessage()
    func showAccountLockedPopUp()
    func clearMessageText()
}
