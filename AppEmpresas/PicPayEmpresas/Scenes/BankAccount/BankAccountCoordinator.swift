import UIKit
import CustomerSupport

enum BankAccountAction {
    case listBank(_ delegate: BankAccountDelegate)
    case accountType(_ delegate: BankAccountDelegate, seller: Seller?)
    case dismiss
    case openFAQ
}

protocol BankAccountCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BankAccountAction)
}

final class BankAccountCoordinator: BankAccountCoordinating {
    weak var viewController: UIViewController?
    let deeplinkHelper: DeeplinkHelper
    
    init(deeplinkHelper: DeeplinkHelper) {
        self.deeplinkHelper = deeplinkHelper
    }
    
    func perform(action: BankAccountAction) {
        switch action {
        case .listBank(let delegate):
            let controller: BankSelectFormViewController = ViewsManager.instantiateViewController(.settings)
            controller.setup(delegate: delegate)
            viewController?.pushViewController(controller)
        case let .accountType(delegate, seller):
            let controller = RegistrationAccountTypeFactory.makeForSettings(delegate, seller: seller)
            viewController?.pushViewController(controller)
        case .dismiss:
            viewController?.navigationController?.popViewController(animated: true)
        case .openFAQ:
            guard
                let navVC = viewController?.navigationController,
                let url = URL(string: "picpaybiz://picpaybiz/helpcenter/article/360049891151")
            else { return }
            deeplinkHelper.open(url: url, from: navVC)
        }
    }
}
