import AnalyticsModule

enum BankAccountAnalytics: AnalyticsKeyProtocol {
    case invalidDocument(_ sellerId: Int? = nil)
    case finishEdition(_ sellerId: Int? = nil)
    case residenceLockErrorViewed
    case residenceLockTryAgainViewed
    case residenceLockNotNowClicked
    case notificationResidenceLockActiveViewed
    case notificationResidenceLockActiveClicked
    case residenceLockStatusLabelViewed
    case changeAccountClicked
    case residenceUnlockFailedViewed
    case residenceUnlockFailedOkClicked
    case residenceUnlockFailedNeedHelpClicked
    case faqAccessed
    case notificationResidenceUnlockedViewed
    case notificationResidenceUnlockedClicked

    var providers: [AnalyticsProvider] {
        switch self {
        case .invalidDocument, .finishEdition:
            return [.appsFlyer]
        default:
            return [.mixPanel, .appsFlyer, .firebase]
        }
    }

    var properties: [String: Any] {
        switch self {
        case .invalidDocument(let sellerId),
             .finishEdition(let sellerId):
            return ["seller_id": "\(sellerId ?? 0)"]
        default:
            return [:]
        }
    }

    var name: String {
        switch self {
        case .invalidDocument:
            return "Invalid Bank Account CPF"
        case .finishEdition:
            return "Bank Account Edited"
        case .residenceLockErrorViewed:
            return "Erro - Trava Domicílio (view)"
        case .residenceLockTryAgainViewed:
            return "Erro trava - Tentar novamente (click)"
        case .residenceLockNotNowClicked:
            return "Erro trava - Agora não (click)"
        case .notificationResidenceLockActiveViewed:
            return "Notificação - Trava ativada (View)"
        case .notificationResidenceLockActiveClicked:
            return "Notificação - Trava ativada (Click)"
        case .residenceLockStatusLabelViewed:
            return "Aviso trava bancária (View)"
        case .changeAccountClicked:
            return "Alterar conta (click)"
        case .residenceUnlockFailedViewed:
            return "Alerta - Destrava domicílio (view)"
        case .residenceUnlockFailedOkClicked:
            return "Alerta trava - Ok, entendi (click)"
        case .residenceUnlockFailedNeedHelpClicked:
            return "Alerta trava - Preciso de ajuda (click)"
        case .faqAccessed:
            return "Acesso à FAQ"
        case .notificationResidenceUnlockedViewed:
            return "Notificação - Trava desativada (View)"
        case .notificationResidenceUnlockedClicked:
            return "Notificação - Trava desativada (Click)"
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
