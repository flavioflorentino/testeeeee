import InputMask
import AnalyticsModule
import CoreSellerAccount
import LegacyPJ

protocol BankAccountPresenting: AnyObject {
    var viewController: BankAccountDisplay? { get set }
    func willFetchBankAccount()
    func willSaveBankAccount()
    func didFetchBankAccount(_ bankAccount: BankAccount, of type: CompanyType?, isLocked: Bool)
    func didChangeBank(_ bank: Bank)
    func bankAccountUpdated()
    func showError(_ error: LegacyPJError)
    func showPopUp(_ error: LegacyPJError)
    func clearErrorMessage()
    func didNextStep(action: BankAccountAction)
    func didCancelUpdate()
    func didSelectPersonalAccount(_ cpf: String)
    func didSelectCompanyAccount(_ cnpj: String?)
    func didFailToUnlockBankAccount()
}

final class BankAccountPresenter: BankAccountPresenting {
    typealias Dependencies = HasAnalytics
    private let coordinator: BankAccountCoordinating
    private let dependencies: Dependencies
    var viewController: BankAccountDisplay?
    let caixaCode = "104"
    let cnpjMask = "[00].[000].[000]/[0000]-[00]"
    let cpfMask = "[000].[000].[000]-[00]"

    init(coordinator: BankAccountCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func willFetchBankAccount() {
        viewController?.startLoadingAnimation()
    }
    
    func willSaveBankAccount() {
        viewController?.prepareViewForPresent()
        viewController?.startLoadingAnimation()
    }
    
    func didFetchBankAccount(_ bankAccount: BankAccount, of type: CompanyType?, isLocked: Bool) {
        var document = String()
        var accountType = String()
        
        if let cnpj = bankAccount.cnpj {
            document = applyMask(cnpjMask, to: cnpj)
        }
        
        if let cpf = bankAccount.cpf {
            document = applyMask(cpfMask, to: cpf)
        }
        
        switch bankAccount.type {
        case "C":
            accountType = Strings.Default.currentAccount
        case "P":
            accountType = Strings.Default.savingAccount
        default:
            break
        }
        
        var bankName = String()
        
        if let code = bankAccount.bank?.code, let name = bankAccount.bank?.name {
            bankName = code + " - " + name
        }
    
        let bank = BankAccountItem(
            bankName: bankName,
            bankImageUrl: bankAccount.bank?.image ?? String(),
            branch: bankAccount.branch,
            branchDigit: bankAccount.branchDigit,
            account: bankAccount.account,
            accountDigit: bankAccount.accountDigit,
            type: accountType,
            operation: bankAccount.operation,
            document: document
        )
        
        viewController?.prepareViewForPresent()
        viewController?.operationField(hidden: bankAccount.bank?.code != caixaCode)
        viewController?.bankAccount(bank)
        viewController?.stopLoadingAnimation()
        guard isLocked else { return }
        viewController?.showLockedBankAccountMessage()
    }
    
    func didChangeBank(_ bank: Bank) {
        let bankName = bank.code + " - " + bank.name
        let bankItem = BankItem(name: bankName, imgUrl: bank.image ?? "")
        
        viewController?.prepareViewForEdit()
        viewController?.operationField(hidden: bank.code != caixaCode)
        viewController?.didChangeBank(bankItem)
    }
    
    func bankAccountUpdated() {
        coordinator.perform(action: .dismiss)
    }
    
    func showError(_ error: LegacyPJError) {
        viewController?.prepareViewForEdit()
        viewController?.stopLoadingAnimation()
        viewController?.showErrorMessage(error.localizedDescription)
    }
    
    func showPopUp(_ error: LegacyPJError) {
        viewController?.prepareViewForEdit()
        viewController?.stopLoadingAnimation()
        viewController?.showPopUp(with: error.localizedDescription)
    }
    
    func didNextStep(action: BankAccountAction) {
        coordinator.perform(action: action)
    }
    
    func didCancelUpdate() {
        viewController?.prepareViewForEdit()
        viewController?.stopLoadingAnimation()
    }
    
    func didSelectPersonalAccount(_ cpf: String) {
        viewController?.personalDocumentField(cpf)
    }
    
    func didSelectCompanyAccount(_ cnpj: String?) {
        guard let cnpj = cnpj else {
            return
        }
        let cnpjMasked = applyMask(cnpjMask, to: cnpj)
        viewController?.companyDocumentField(cnpjMasked)
    }
    
    func didFailToUnlockBankAccount() {
        viewController?.showAccountLockedPopUp()
        dependencies.analytics.log(BankAccountAnalytics.residenceUnlockFailedViewed)
    }
    
    func clearErrorMessage() {
        viewController?.clearMessageText()
    }
}

private extension BankAccountPresenter {
    func applyMask(_ format: String, to value: String) -> String {
        guard let mask = try? Mask(format: format) else {
            return String()
        }
        let result = mask.apply(toText: CaretString(string: value))
        return result.formattedText.string
    }
}
