import Foundation
import Core

enum BankAccountFactory {
    static func make(_ seller: Seller?) -> BankAccountViewController {
        let container = DependencyContainer()
        let service: BankAccountServicing = BankAccountService(dependencies: container)
        let coordinator: BankAccountCoordinating = BankAccountCoordinator(deeplinkHelper: DeeplinkHelper(dependencies: container))
        let presenter: BankAccountPresenting = BankAccountPresenter(coordinator: coordinator, dependencies: container)
        let viewModel = BankAccountViewModel(service: service, presenter: presenter, seller: seller, dependencies: container)
        let viewController = BankAccountViewController(viewModel: viewModel)
        viewController.hidesBottomBarWhenPushed = true

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
