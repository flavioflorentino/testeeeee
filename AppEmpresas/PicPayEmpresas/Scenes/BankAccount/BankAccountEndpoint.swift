import Core

enum UnlockBankAcountEndpoint {
    case unlock
    case checkLockStatus
}

extension UnlockBankAcountEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .unlock:
            return "/bank-residence-lock/unlock"
        case .checkLockStatus:
            return "/bank-residence-lock/check"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .unlock:
            return .post
        case .checkLockStatus:
            return .get
        }
    }
}

struct UnlockResponse: Decodable {
    let success: Bool
}

struct LockStatusResponse: Decodable {
    let isLocked: Bool
}
