import Foundation
import Core
import CoreSellerAccount
import LegacyPJ

typealias BankResidenceLockStatusCompletion = (Result<LockStatusResponse, ApiError>) -> Void
typealias BankResidenceUnlockCompletion = (Result<UnlockResponse, ApiError>) -> Void

protocol BankAccountServicing {
    func getBankAccount(_ completion: @escaping (LegacyPJError?, BankAccount?) -> Void)
    func getBankResidenceLockStatus(completion: @escaping BankResidenceLockStatusCompletion)
    func unlockBankAcount(completion: @escaping BankResidenceUnlockCompletion)
    func updateBankAccount(_ pin: String, bankAccount: BankAccount, completion: @escaping BaseCompletion)
    func verifyCompanyType(
        _ cnpj: String,
        completion: @escaping (_ error: LegacyPJError?, _ type: CompanyType?) -> Void
    )
    func verifyDocumentRelation(
        _ cnpj: String,
        cpf: String,
        completion: @escaping (_ error: LegacyPJError?, _ valid: Bool) -> Void
    )
}

final class BankAccountService: BankAccountServicing {
    private let apiSeller: ApiSeller
    private let apiSignUp: ApiSignUp

    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies, apiSeller: ApiSeller = ApiSeller(), apiSignUp: ApiSignUp = ApiSignUp()) {
        self.dependencies = dependencies
        self.apiSeller = apiSeller
        self.apiSignUp = apiSignUp
    }
    
    func verifyCompanyType(
        _ cnpj: String,
        completion: @escaping (_ error: LegacyPJError?, _ type: CompanyType?) -> Void
    ) {
        apiSignUp.verifySeller(cnpj) { result in
            switch result {
            case .success(let companyInfo):
                guard let companyInfo = companyInfo else {
                    completion(LegacyPJError.invalidCnpj, nil)
                    return
                }
                let flagValue = companyInfo.individual
                completion(nil, flagValue ? .individual : .multiPartners)
            case .failure:
                completion(LegacyPJError.invalidCnpj, nil)
            }
        }
    }
    
    func verifyDocumentRelation(
        _ cnpj: String,
        cpf: String,
        completion: @escaping (_ error: LegacyPJError?, _ valid: Bool) -> Void
    ) {
        apiSignUp.verifyCPFIntegrity(cnpj, cpf: cpf) { [weak self] error, valid in
            self?.dependencies.mainQueue.async {
                completion(error, valid)
            }
        }
    }
    
    func getBankAccount(_ completion: @escaping (LegacyPJError?, BankAccount?) -> Void) {
        apiSeller.bankAccount { [weak self] bankAccount, error in
            self?.dependencies.mainQueue.async {
                completion(error, bankAccount)
            }
        }
    }
    
    func updateBankAccount(_ pin: String, bankAccount: BankAccount, completion: @escaping BaseCompletion) {
        apiSeller.updateBankAccount(pin: pin, bankAccount: bankAccount) { [weak self] error in
            self?.dependencies.mainQueue.async {
                completion(error)
            }
        }
    }
    
    /// Check bank account lock status
    func getBankResidenceLockStatus(completion: @escaping BankResidenceLockStatusCompletion) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        Core.Api<LockStatusResponse>(endpoint: UnlockBankAcountEndpoint.checkLockStatus).execute(jsonDecoder: decoder, { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        })
    }
    
    /// Tries to unlock the bank account if the same is locked
    func unlockBankAcount(completion: @escaping BankResidenceUnlockCompletion) {
        Core.Api<UnlockResponse>(endpoint: UnlockBankAcountEndpoint.unlock).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
}
