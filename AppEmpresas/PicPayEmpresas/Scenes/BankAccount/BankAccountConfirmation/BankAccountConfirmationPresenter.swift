import Foundation

protocol BankAccountConfirmationPresenting: AnyObject {
    var viewController: BankAccountConfirmationDisplay? { get set }
    func showAccountInfo(_ accountItem: AccountConfirmationItem)
    func showWrongPassword(_ message: String)
    func showError(_ message: String)
    func didNextStep(action: BankAccountConfirmationAction)
    func perform(_ viewAction: BankAccountConfirmationPresenter.ViewAction)
    func updateSuccess()
}

final class BankAccountConfirmationPresenter {
    private typealias Localizable = Strings.BankAccountConfirmation

    private let coordinator: BankAccountConfirmationCoordinating
    weak var viewController: BankAccountConfirmationDisplay?

    enum ViewAction {
        case startLoading
        case stopLoading
    }

    init(coordinator: BankAccountConfirmationCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - BankAccountConfirmationPresenting
extension BankAccountConfirmationPresenter: BankAccountConfirmationPresenting {
    func perform(_ viewAction: ViewAction) {
        switch viewAction {
        case .startLoading:
            viewController?.startLoading()
        case .stopLoading:
            viewController?.stopLoading()
        }
    }

    func showAccountInfo(_ accountItem: AccountConfirmationItem) {
        viewController?.displayAccountItem(accountItem)
    }

    func updateSuccess() {
        let modalInfo = AlertModalInfo(
            title: Localizable.updateSuccess,
            buttonTitle: Localizable.ok
        )
        
        didNextStep(action: .updateSuccess(modalInfo))
    }

    func showWrongPassword(_ message: String) {
        didNextStep(action: .wrongPasswordModal(message))
    }

    func showError(_ message: String) {
        didNextStep(action: .showError(message))
    }
    
    func didNextStep(action: BankAccountConfirmationAction) {
        coordinator.perform(action: action)
    }
}
