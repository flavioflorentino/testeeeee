import AnalyticsModule
import Foundation

enum BankAccountConfirmationAnalytics: AnalyticsKeyProtocol {
    case toCorrect(_ bankAccount: CoreBankAccount)
    case confirmed(_ bankAccount: CoreBankAccount)
    case password(_ bankAccount: CoreBankAccount)
    case updated(_ bankAccount: CoreBankAccount)

    private var name: String {
        switch self {
        case .toCorrect:
            return "Bank Account - To Correct"
        case .confirmed:
            return "Bank Account - Confirmed"
        case .password:
            return "Bank Account - Password"
        case .updated:
            return "Bank Account - Updated"
        }
    }

    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }

    private var properties: [String: String] {
        switch self {
        case let .toCorrect(bankAccount), 
            let .confirmed(bankAccount), 
            let .password(bankAccount), 
            let .updated(bankAccount):
            return [
                "bank_id": bankAccount.bankId,
                "bank_name": bankAccount.bank?.name ?? "",
                "bank_account_type": bankAccount.type?.rawValue ?? "",
                "bank_account_value": bankAccount.value ?? ""
            ]
        }
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
