import UI
import UIKit
import FeatureFlag

enum BankAccountConfirmationAction {
    case dismiss
    case updateSuccess(_ info: AlertModalInfo)
    case proceedRegistration(_ viewModel: RegistrationViewModel)
    case wrongPasswordModal(_ message: String)
    case showError(_ message: String)
}

protocol BankAccountConfirmationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BankAccountConfirmationAction)
}

final class BankAccountConfirmationCoordinator {
    private typealias Localizable = Strings.BankAccountConfirmation
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - BankAccountConfirmationCoordinating
extension BankAccountConfirmationCoordinator: BankAccountConfirmationCoordinating {
    func perform(action: BankAccountConfirmationAction) {
        switch action {
        case .dismiss:
            viewController?.navigationController?.popViewController(animated: true)
        case .updateSuccess:
            let controller = BankAccountConclusionFactory.make()
            viewController?.pushViewController(controller)
        case .proceedRegistration(let registrationViewModel):
            guard dependencies.featureManager.isActive(.registrationTaxZero) else {
                let controller: RegistrationDaysStepViewController = ViewsManager.instantiateViewController(.registration)
                controller.setup(model: registrationViewModel)
                viewController?.pushViewController(controller)
                return
            }
            
            let controller = RegistrationTaxZeroFactory.make(model: registrationViewModel)
            viewController?.pushViewController(controller)
        case let .wrongPasswordModal(message):
            errorPopUp(with: Localizable.Error.WrongPassword.title, message: message)
        case let .showError(message):
            errorPopUp(with: Localizable.Error.message, message: message)
        }
    }
}

private extension BankAccountConfirmationCoordinator {
    func errorPopUp(with title: String, message: String) {
        let action = PopupAction(title: Localizable.ok, style: .fill)
        let infoPopUp = UI.PopupViewController(
            title: title,
            description: message,
            image: Assets.errorIcon.image
        )

        presentPopUp(infoPopUp, actions: [action])
    }
    
    func presentPopUp(_ popUp: UI.PopupViewController, actions: [PopupAction] = []) {
        popUp.hideCloseButton = true
        actions.forEach { popUp.addAction($0) }
        viewController?.showPopup(popUp)
    }
}
