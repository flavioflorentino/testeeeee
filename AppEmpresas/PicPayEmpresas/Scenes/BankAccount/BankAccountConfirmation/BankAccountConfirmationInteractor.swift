import AnalyticsModule
import InputMask
import Foundation
import LegacyPJ

protocol BankAccountConfirmationInteracting: AnyObject {
    func loadInfo()
    func saveBankAccount()
    func reviewAccountInfo()
}

struct AccountConfirmationItem {
    enum DocumentType {
        case cpf, cnpj
    }

    let imageUrl: String
    let bank: String
    let branch: String
    let account: String
    let document: String
    let documentType: DocumentType
    let type: String

    private let cnpjMask = "[00].[000].[000]/[0000]-[00]"
    private let cpfMask = "[000].[000].[000]-[00]"

    var formattedDoc: String {
        let mask = documentType == .cpf ? cpfMask : cnpjMask
        return Mask.maskString(input: document, mask: mask)
    }
}

final class BankAccountConfirmationInteractor {
    private typealias Localizable = Strings.BankAccountConfirmation
    private typealias Analytics = BankAccountConfirmationAnalytics

    typealias Dependencies = HasAuthManager & HasAnalytics
    private let dependencies: Dependencies

    private let service: BankAccountConfirmationServicing
    private let presenter: BankAccountConfirmationPresenting
    private let flow: BankAccountConfirmationFactory.ConfirmationFlow

    init(
        _ flow: BankAccountConfirmationFactory.ConfirmationFlow, 
        service: BankAccountConfirmationServicing,
        presenter: BankAccountConfirmationPresenting, 
        dependencies: Dependencies
    ) {
        self.flow = flow
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - BankAccountConfirmationInteracting
extension BankAccountConfirmationInteractor: BankAccountConfirmationInteracting {
    func loadInfo() {
        switch flow {
        case .registration(let registrationViewModel):
            presenter.showAccountInfo(registrationViewModel.account.confirmationItem)
        case .settings(let bankAccount):
            presenter.showAccountInfo(bankAccount.confirmationItem)
        }
    }

    func saveBankAccount() {
        switch flow {
        case .registration(let registrationViewModel):
            saveToViewModelAndGoToNextStep(registrationViewModel)
        case .settings(let bankAccount):
            requestUserPassword(bankAccount)
            logEvent(.confirmed(bankAccount))
        }
    }

    func reviewAccountInfo() {
        if case let .settings(bankAccount) = flow {
            logEvent(.toCorrect(bankAccount))
        }

        presenter.didNextStep(action: .dismiss)
    }
}

// MARK: - Private Methods
private extension BankAccountConfirmationInteractor {
    func requestUserPassword(_ bankAccount: CoreBankAccount) {
        dependencies.authManager.performActionWithAuthorization { [weak self] result in
            switch result {
            case .success(let password):
                self?.logEvent(.password(bankAccount))
                self?.updateBankAccount(password, bankAccount: bankAccount)
            case .failure(let error):
                self?.presenter.showError(error.localizedDescription)
            case .canceled:
                break
            }
        }
    }

    func updateBankAccount(_ pin: String, bankAccount: CoreBankAccount) {
        presenter.perform(.startLoading)
        service.updateBankAccount(pin: pin, bankAccount: bankAccount) { [weak self] result in 
            self?.presenter.perform(.stopLoading)
            switch result {
            case .success(let response):
                guard response.success else {
                    self?.presenter.showError(Localizable.Error.message)
                    return
                }
                self?.logEvent(.updated(bankAccount))
                self?.presenter.updateSuccess()
                NotificationCenter.default.post(name: Notification.Name.Settings.updated, object: nil)
            case let .failure(.badRequest(error)):
                self?.presenter.showError(ErrorBiz.errorMessage(error.jsonData))
            case let .failure(.unauthorized(error)):
                self?.presenter.showWrongPassword(ErrorBiz.errorMessage(error.jsonData))
            case .failure:
                self?.presenter.showError(LegacyPJError.requestError.message)
            }
        }
    }

    func saveToViewModelAndGoToNextStep(_ registrationViewModel: RegistrationViewModel) {
        presenter.didNextStep(action: .proceedRegistration(registrationViewModel))
    }

    func logEvent(_ event: BankAccountConfirmationAnalytics) {
        dependencies.analytics.log(event)
    }
}
