import UI
import UIKit

protocol BankAccountConfirmationDisplay: AnyObject {
    func displayAccountItem(_ accountItem: AccountConfirmationItem)
    func startLoading()
    func stopLoading()
}

final class BankAccountConfirmationViewController: ViewController<BankAccountConfirmationInteracting, UIView> {
    private typealias Localizable = Strings.BankAccountConfirmation

    fileprivate enum Layout { }

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .large))
        label.text = Localizable.View.title
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
        label.text = Localizable.View.description
        return label
    }()

    private lazy var bankAccountInfoView = BankAccountConfirmationView()

    private lazy var confirmationButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.PrimaryButton.title, for: .normal)
        button
            .buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(confirmationButtonTapped), for: .touchUpInside)
        return button
    }()

    private lazy var changeButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.SecondaryButton.title, for: .normal)
        button
            .buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(changeButtonTapped), for: .touchUpInside)
        return button
    }()

    lazy var loadingView: LoadingView = {
        let loadingView = LoadingView()
        loadingView.alpha = 0.8
        return loadingView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadInfo()
    }

    override func buildViewHierarchy() { 
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(bankAccountInfoView)
        view.addSubview(confirmationButton)
        view.addSubview(changeButton)
    }
    
    override func setupConstraints() { 
        titleLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        bankAccountInfoView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
        }

        changeButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }

        confirmationButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(changeButton.snp.top).offset(-Spacing.base01)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.white.color
    }
}

// MARK: BankAccountConfirmationDisplay
extension BankAccountConfirmationViewController: BankAccountConfirmationDisplay, LoadingViewProtocol {
    func displayAccountItem(_ accountItem: AccountConfirmationItem) {
        bankAccountInfoView.setupAccount(accountItem)
    }

    func startLoading() {
        startLoadingView()
    }

    func stopLoading() {
        stopLoadingView()
    }
}

@objc
private extension BankAccountConfirmationViewController {
    func confirmationButtonTapped() {
        interactor.saveBankAccount()
    }

    func changeButtonTapped() {
        interactor.reviewAccountInfo()
    }
}
