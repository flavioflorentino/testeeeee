import Core
import Foundation

protocol BankAccountConfirmationServicing {
    func updateBankAccount(
        pin: String, 
        bankAccount: CoreBankAccount, 
        completion: @escaping (Result<UpdateBankAccountResponse, ApiError>) -> Void
    )
}

struct UpdateBankAccountResponse: Decodable {
    let success: Bool
}

final class BankAccountConfirmationService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - BankAccountConfirmationServicing
extension BankAccountConfirmationService: BankAccountConfirmationServicing {
    func updateBankAccount(
        pin: String, 
        bankAccount: CoreBankAccount, 
        completion: @escaping (Result<UpdateBankAccountResponse, ApiError>) -> Void
    ) {
        let endpoint = BankAccountConfirmationEndpoint.updateBankAccount(pin, bankAccount: bankAccount)

        BizApiV2<UpdateBankAccountResponse>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }
}
