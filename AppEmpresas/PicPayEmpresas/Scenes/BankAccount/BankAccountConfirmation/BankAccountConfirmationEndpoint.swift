import Core

enum BankAccountConfirmationEndpoint {
    case updateBankAccount(_ pin: String, bankAccount: CoreBankAccount)
}

extension BankAccountConfirmationEndpoint: ApiEndpointExposable {
    var path: String {
        "/v2/bank-account"
    }
    
    var method: HTTPMethod {
        .put
    }

    var body: Data? {
        switch self {
        case let .updateBankAccount(_, bankAccount):
            return try? JSONEncoder().encode(bankAccount)
        }
    }
    
    var customHeaders: [String: String] {
        switch self {
        case let .updateBankAccount(pin, _):
            return headerWith(pin: pin)
        }
    }
}
