import Foundation
import UIKit

enum BankAccountConfirmationFactory {
    enum ConfirmationFlow {
        case registration(_ viewModel: RegistrationViewModel)
        case settings(_ bankAccount: CoreBankAccount)
    }

    static func make(
        _ flow: ConfirmationFlow
    ) -> BankAccountConfirmationViewController {
        let container = DependencyContainer()
        let service: BankAccountConfirmationServicing = BankAccountConfirmationService(dependencies: container)
        let coordinator: BankAccountConfirmationCoordinating = BankAccountConfirmationCoordinator(dependencies: container)
        let presenter: BankAccountConfirmationPresenting = BankAccountConfirmationPresenter(coordinator: coordinator)
        let interactor = BankAccountConfirmationInteractor(
            flow, 
            service: service, 
            presenter: presenter, 
            dependencies: container
        )
        let viewController = BankAccountConfirmationViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
