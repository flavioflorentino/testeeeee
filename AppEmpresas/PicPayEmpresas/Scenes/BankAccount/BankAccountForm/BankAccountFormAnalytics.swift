import AnalyticsModule

enum BankAccountFormAnalytics: AnalyticsKeyProtocol {
    case invalidDocument
    case help
    case needMoreHelp
    case issueResolved
    case edited

    var name: String {
        switch self {
        case .help:
            return "Bank Account - Help"
        case .needMoreHelp:
            return "Bank Account - Need More Help"
        case .issueResolved:
            return "Bank Account - Issue Resolved"
        case .invalidDocument:
            return "Bank Account - Invalid CPF"
        case .edited:
            return "Bank Account - Edited"
        }
    }

    var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: [:], providers: providers)
    }
}
