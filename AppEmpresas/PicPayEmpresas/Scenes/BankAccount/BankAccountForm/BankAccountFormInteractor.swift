import AnalyticsModule
import AssetsKit
import Core
import CoreSellerAccount
import Foundation
import LegacyPJ

protocol BankAccountFormInteracting: AnyObject {
    func loadViewInfo()
    func openListBank()
    func didChangeData(_ item: BankAccountItem)
    func listAccountTypes()
    func didSelectAccountType(_ index: Int)
    func saveNewAccount()
    func didTapExplanation()
}

struct ValidateQSAResponse: Decodable {
    let valid: Bool
}

final class BankAccountFormInteractor {
    typealias Dependencies = HasMainQueue & HasAnalytics
    private typealias Localizable = Strings.BankAccount

    enum ErrorField: String {
        case wrongDocument = "error_cpf_bank_account"
    }

    private let service: BankAccountFormServicing
    private let presenter: BankAccountFormPresenting
    private let dependencies: Dependencies

    private var bankAccount: CoreBankAccount?
    private var accountTypes: [CoreAccountType] = []
    private var bankAccountError: BankAccountValidation?
    private var companyType: CompanyType?
    private var bank: CoreBank?
    private var changedBankAccountItem: BankAccountItem?
    private var selectedAccountType: CoreAccountType?
    private var currentAccountType: CoreAccountType? {
        bankAccount?.bank?.accountTypes.first(where: { $0.selected })
    }

    private var newBankAccount: CoreBankAccount? {
        var cpf: String?

        if companyType == .individual && selectedAccountType?.type == .individual {
            cpf = changedBankAccountItem?.document
        }

        return CoreBankAccount(
            account: changedBankAccountItem?.account ?? "",
            accountDigit: changedBankAccountItem?.accountDigit ?? "",
            bank: bank,
            bankId: bank?.code ?? "",
            branch: changedBankAccountItem?.branch ?? "",
            branchDigit: changedBankAccountItem?.branchDigit ?? "",
            cnpj: bankAccount?.cnpj,
            cpf: cpf,
            id: 0,
            value: selectedAccountType?.value ?? "",
            type: selectedAccountType?.type ?? .company,
            label: selectedAccountType?.label ?? ""
        )
    }

    var seller: Seller?

    init(
        service: BankAccountFormServicing, 
        presenter: BankAccountFormPresenting, 
        dependencies: Dependencies, 
        companyType: CompanyType
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.companyType = companyType
    }
}

// MARK: - BankAccountFormInteracting
extension BankAccountFormInteractor: BankAccountFormInteracting {
    func loadViewInfo() {
        presenter.perform(.startLoading)
        let group = DispatchGroup()

        group.enter()
        loadBankAndAccountType(group)

        group.enter()
        loadBankAccountStatus(group)

        group.notify(queue: dependencies.mainQueue) { [weak self] in
            self?.handleResult()
        }
    }

    func openListBank() {
        presenter.didNextStep(action: .listBank(delegate: self))
    }

    func didChangeData(_ item: BankAccountItem) {
        guard item.branch == bankAccount?.branch,
            item.branchDigit == (bankAccount?.branchDigit ?? ""),
            item.account == bankAccount?.account,
            item.accountDigit == bankAccount?.accountDigit,
            selectedAccountType == currentAccountType, 
            bank?.code == bankAccount?.bankId else {
                presenter.changeButton(enabled: true)
                changedBankAccountItem = item
                return
            }

        presenter.changeButton(enabled: false)
    }

    func listAccountTypes() {
        presenter.listAccountTypes(accountTypes)
    }

    func didSelectAccountType(_ index: Int) {
        selectedAccountType = accountTypes[index]

        if companyType == .individual, selectedAccountType?.type == .individual {
            presenter.changeDocumentField(enabled: true, value: nil)
        } else {
            presenter.changeDocumentField(enabled: false, value: seller?.cnpj)
        }
    }

    func saveNewAccount() {
        guard let item = changedBankAccountItem, validateAccount(item) else { return }

        if companyType == .individual, selectedAccountType?.type == .individual {
            validateCPFAssociation(cpf: item.document, cnpj: seller?.cnpj ?? "")
            return
        }

        proceedToConfirmation()
    }

    func didTapExplanation() {
        guard let companyType = companyType else { return }
        let description: String
        switch companyType {
        case .individual:
            description = Localizable.BankAccountTips.HelpModal.Individual.subtitle
        case .multiPartners:
            description = Localizable.BankAccountTips.HelpModal.Multi.subtitle
        }
        
        presenter.didNextStep(action: .helpModal(description, seller: seller))
        trackHelpEvent()
    }
}

private extension BankAccountFormInteractor {
    func validateAccount(_ bankAccount: BankAccountItem) -> Bool {
        guard selectedAccountType != nil else {
            presenter.validationError(Localizable.accountTypeInvalid)
            return false
        }

        if bankAccount.branch.count < 3 {
            presenter.validationError(Localizable.branchInvalid)
            return false
        }

        if bankAccount.account.count < 3 {
            presenter.validationError(Localizable.accountInvalid)
            return false
        }
        
        if bankAccount.accountDigit.isEmpty {
            presenter.validationError(Localizable.accountDigitInvalid)
            return false
        }

        if companyType == .individual, selectedAccountType?.type == .individual {
            if bankAccount.document.isEmpty {
                presenter.validationError(Localizable.documentInvalid)
                return false
            }
            
            if !bankAccount.document.onlyNumbers.validCpf {
                presenter.validationError(Localizable.cpfInvalid)
                return false
            }
        }
        presenter.validationError("")

        return true
    }

    func loadBankAndAccountType(_ group: DispatchGroup) {
        DispatchQueue.global().async { [weak self] in
            let semaphore = DispatchSemaphore(value: 0)

            self?.loadBankAccount(group, semaphore: semaphore)

            _ = semaphore.wait(timeout: .now() + 2)

            self?.loadAccountType(group)
        }
    }

    func loadBankAccount(_ group: DispatchGroup, semaphore: DispatchSemaphore) {
        service.getBankAccount { [weak self] result in
            switch result {
            case .success(let account):
                self?.bankAccount = account
                self?.bank = account.bank
            case .failure(.badRequest(let error)):
                self?.handleLoadingInfoError(ErrorBiz.errorMessage(error.jsonData))
            case .failure:
                self?.handleLoadingInfoError()
            }

            semaphore.signal()
        }
    }

    func loadAccountType(_ group: DispatchGroup) {
        service.getAccountType(bank?.code ?? "", cnpj: seller?.cnpj ?? "") { [weak self] result in
            switch result {
            case .success(let accountTypes):
                self?.accountTypes = accountTypes
            case .failure(.badRequest(let error)):
                self?.handleLoadingInfoError(ErrorBiz.errorMessage(error.jsonData))
            case .failure:
                self?.handleLoadingInfoError()
            }

            group.leave()
        }
    }

    func loadBankAccountStatus(_ group: DispatchGroup) {
        service.checkBankAccount { [weak self] result in
            switch result {
            case .success(let accountError):
                self?.bankAccountError = accountError
            case .failure:
                self?.handleLoadingInfoError()
            }

            group.leave()
        }
    }

    func handleLoadingInfoError(_ message: String? = nil) {
        presenter.somenthingWentWrong(message) { [weak self] in
            self?.loadViewInfo()
        }
    }

    func handleResult() {
        presenter.perform(.stopLoading)

        if companyType == .individual {
            presenter.displayPersonalExplanation()
        }

        if let bankAccountError = bankAccountError {
            presenter.showAccountErrorComponent(bankAccountError)
        }

        selectedAccountType = currentAccountType
        presenter.bankAccount(bankAccount, of: companyType)
    } 

    func validateCPFAssociation(cpf: String, cnpj: String) {
        presenter.perform(.startLoading)
        service.validateDocuments(cpf: cpf, cnpj: cnpj) { [weak self] result in
            self?.presenter.perform(.stopLoading)
            switch result {
            case .success:
                self?.proceedToConfirmation()
            case .failure(.badRequest(let error)):
                guard let type = error.field, type == ErrorField.wrongDocument.rawValue else {
                    self?.handleValidationError()
                    return
                }

                self?.dependencies.analytics.log(BankAccountFormAnalytics.invalidDocument)
                self?.presenter.showWrongDocumentModal()
            case .failure:
                self?.handleValidationError()
            }
        }
    }

    func handleValidationError() {
        presenter.somenthingWentWrong(nil, completion: nil)
    }

    func proceedToConfirmation() {
        guard let account = newBankAccount else { return }
        presenter.didNextStep(action: .confirmChanges(account))
        trackAccountEditedEvent()
    }

    func loadNewAccountTypes() {
        presenter.perform(.startLoading)
        let group = DispatchGroup()

        group.enter()
        loadAccountType(group)

        group.notify(queue: dependencies.mainQueue) { [weak self] in
            self?.presenter.perform(.stopLoading)
        }
    }
}

private extension BankAccountFormInteractor {
    func trackHelpEvent() {
        dependencies.analytics.log(BankAccountFormAnalytics.help)
    }

    func trackAccountEditedEvent() {
        dependencies.analytics.log(BankAccountFormAnalytics.edited)
    }
}

extension BankAccountFormInteractor: BankSelectFormViewControllerDelegate {
    func didSelectBank(_ newBank: Bank) {
        let coreBank = CoreBank(newBank)
        let bankDidChange = coreBank.code != bank?.code

        presenter.changeButton(enabled: bankDidChange)
        bank = coreBank
        presenter.showBank(coreBank)

        if bankDidChange {
            selectedAccountType = nil
            loadNewAccountTypes()
        }
    }
    func shouldHidePJAccountDisclaimer() -> Bool { true } 
}
