import InputMask
import UI
import UIKit

protocol BankAccountFormDisplay: AnyObject {
    func changeActionButton(enabled: Bool)
    func bankChanged(_ bank: BankItem)
    func bankAccount(_ account: BankAccountItem)
    func bankAccountValidationError(_ message: String) 
    func startLoading()
    func stopLoading()
    func displayPersonalExplanation()
    func presentAccountTypes(_ itens: [AccountTypeItem])
    func didTapExplanationLabel()
    func validationMessage(_ message: String)
    func changeDocumentField(enabled: Bool, value: String?)
    func enableDocumentField()
}

private extension BankAccountFormViewController.Layout {
    enum Fonts {
        static let textField = Typography.bodyPrimary().font()
        static let messageError = Typography.caption().font()
    }
    
    enum Insets {
        static let bankTitle = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 24)
        static let accountTypeTitle = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        static let keyboard = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
    }
    
    enum Colors {
        static let enabledTextField = UI.Colors.black.color
        static let disabledTextField = UI.Colors.grayscale400.color
        static let backgroundTextField = UI.Colors.white.color
    }
    
    enum Spacing {
        static let horizontal: CGFloat = 8
        static let vertcal: CGFloat = 12
        static let `default`: CGFloat = 20
        static let messageError: CGFloat = 4
    }
    
    enum Mask {
        static let cpf = "[000].[000].[000]-[00]"
    }
    
    enum DigitTextField {
        static let width: CGFloat = 80
    }
    
    enum BankImage {
        static let leading: CGFloat = 24
        static let size = CGSize(width: 25, height: 25)
    }
    
    enum TextFields {
        static let height: CGFloat = 40
    }
    
    enum Others {
        static let buttonCornerRadius: CGFloat = 20
    }
}

final class BankAccountFormViewController: ViewController<BankAccountFormInteracting, UIView> {
    private typealias Localizable = Strings.BankAccount
    
    fileprivate enum Layout { }

    private lazy var bankButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.bankPlaceholder, for: .normal)
        button.setTitleColor(Layout.Colors.enabledTextField, for: .normal)
        button.backgroundColor = Layout.Colors.backgroundTextField
        button.titleLabel?.font = Layout.Fonts.textField
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.minimumScaleFactor = 0.6
        button.titleEdgeInsets = Layout.Insets.bankTitle
        button.layer.cornerRadius = Layout.Others.buttonCornerRadius
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector(listBank), for: .touchUpInside)
        return button
    }()

    private lazy var bankArrowImage = UIImageView(image: Assets.Commom.arrow.image)

    private lazy var bankImageView: UIImageView = {
        let imageView = UIImageView(image: Assets.icoBankPlaceholder.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var accountTypeButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.accountTypePlaceholder, for: .normal)
        button.setTitleColor(Layout.Colors.enabledTextField, for: .normal)
        button.backgroundColor = Layout.Colors.backgroundTextField
        button.titleLabel?.font = Layout.Fonts.textField
        button.titleEdgeInsets = Layout.Insets.accountTypeTitle
        button.layer.cornerRadius = Layout.Others.buttonCornerRadius
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector(listAccountTypes), for: .touchUpInside)
        return button
    }()
    
    private lazy var accountTypeArrowImage: UIImageView = {
        let imageView = UIImageView(image: Assets.Commom.arrow.image)
        imageView.transform = imageView.transform.rotated(by: CGFloat(Double.pi / 2))
        return imageView
    }()

    private lazy var documentTextField: UIRoundedTextField = {
        let textField = UIRoundedTextField()
        textField.placeholder = Localizable.companyDocumentPlaceholder
        textField.textColor = Layout.Colors.disabledTextField
        textField.backgroundColor = Layout.Colors.backgroundTextField
        textField.font = Layout.Fonts.textField
        textField.keyboardType = .numberPad
        textField.isEnabled = false
        textField.delegate = self
        textField.layer.borderWidth = Border.none
        textField.addTarget(self, action: #selector(valueDidChange), for: .editingChanged)
        return textField
    }()
    
    private lazy var branchTextField: UIRoundedTextField = {
        let textField = UIRoundedTextField()
        textField.placeholder = Localizable.branchPlaceholder
        textField.textColor = Layout.Colors.enabledTextField
        textField.backgroundColor = Layout.Colors.backgroundTextField
        textField.font = Layout.Fonts.textField
        textField.keyboardType = .numberPad
        textField.delegate = self
        textField.layer.borderWidth = Border.none
        textField.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        textField.addTarget(self, action: #selector(valueDidChange), for: .editingChanged)
        return textField
    }()
    
    private lazy var branchDigitTextField: UIRoundedTextField = {
        let textField = UIRoundedTextField()
        textField.placeholder = Localizable.digitPlaceholder
        textField.textColor = Layout.Colors.enabledTextField
        textField.backgroundColor = Layout.Colors.backgroundTextField
        textField.font = Layout.Fonts.textField
        textField.delegate = self
        textField.layer.borderWidth = Border.none
        textField.textAlignment = .center
        textField.addTarget(self, action: #selector(valueDidChange), for: .editingChanged)
        return textField
    }()
    
    private lazy var accountTextField: UIRoundedTextField = {
        let textField = UIRoundedTextField()
        textField.placeholder = Localizable.accountPlaceholder
        textField.textColor = Layout.Colors.enabledTextField
        textField.backgroundColor = Layout.Colors.backgroundTextField
        textField.font = Layout.Fonts.textField
        textField.keyboardType = .numberPad
        textField.delegate = self
        textField.layer.borderWidth = Border.none
        textField.addTarget(self, action: #selector(valueDidChange), for: .editingChanged)
        return textField
    }()
    
    private lazy var accountDigitTextField: UIRoundedTextField = {
        let textField = UIRoundedTextField()
        textField.placeholder = Localizable.digitPlaceholder
        textField.textColor = Layout.Colors.enabledTextField
        textField.backgroundColor = Layout.Colors.backgroundTextField
        textField.font = Layout.Fonts.textField
        textField.layer.borderWidth = Border.none
        textField.textAlignment = .center
        textField.addTarget(self, action: #selector(valueDidChange), for: .editingChanged)
        return textField
    }()
    
    private lazy var branchStackView: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Layout.Spacing.horizontal
        stack.axis = .horizontal
        stack.addArrangedSubview(branchTextField)
        stack.addArrangedSubview(branchDigitTextField)
        return stack
    }()
    
    private lazy var accountStackView: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Layout.Spacing.horizontal
        stack.axis = .horizontal
        stack.addArrangedSubview(accountTextField)
        stack.addArrangedSubview(accountDigitTextField)
        return stack
    }()
    
    private lazy var allFieldsStackView: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Layout.Spacing.vertcal
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.addArrangedSubview(bankButton)
        stack.addArrangedSubview(accountTypeButton)
        stack.addArrangedSubview(documentTextField)
        stack.addArrangedSubview(branchStackView)
        stack.addArrangedSubview(accountStackView)
        return stack
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.changeAccount, for: .normal)
        button
            .buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapChangeAccount), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()

    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.critical900.color)
        return label
    }()

    private lazy var explanationLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.grayscale600.color)
        let attrText = NSMutableAttributedString()
        attrText.append(createDefaultText(Localizable.BankAccountForm.explanationTip))
        attrText.append(createUnderlineText(Localizable.BankAccountForm.explanationCompany))
        label.attributedText = attrText
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapExplanationLabel))
        label.addGestureRecognizer(tapGesture)
        label.isUserInteractionEnabled = true
        return label
    }()

    private lazy var contentView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.isUserInteractionEnabled = true
        return scrollView
    }()

    lazy var loadingView: LoadingView = {
        let loadingView = LoadingView()
        loadingView.alpha = 0.8
        return loadingView
    }()

    private var bankAccountItem: BankAccountItem {
        BankAccountItem(
            bankName: bankButton.title(for: .normal) ?? "",
            bankImageUrl: "",
            branch: branchTextField.text ?? "",
            branchDigit: branchDigitTextField.text ?? "",
            account: accountTextField.text ?? "",
            accountDigit: accountDigitTextField.text ?? "",
            type: "",
            operation: "",
            document: documentTextField.text ?? ""
        )
    }

    private enum VisualRule {
        static let branchMaxChars = 4
        static let accountMaxChars = 12
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadViewInfo()
    }

    override func buildViewHierarchy() { 
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(allFieldsStackView)
        contentView.addSubview(messageLabel)
        contentView.addSubview(explanationLabel)
        contentView.addSubview(actionButton)
        contentView.addSubview(bankImageView)
        bankButton.addSubview(bankArrowImage)
        accountTypeButton.addSubview(accountTypeArrowImage)
    }
    
    override func setupConstraints() { 
        setupScrollViewConstraints()
        setupBodyConstraints()
        setupBankDetailsConstraints()
    }

    override func configureViews() { 
        view.backgroundColor = Palette.ppColorGrayscale100.color
        title = Localizable.viewTitle

        configureKeyboardBehaviours()
    }
    
    override func configureStyles() { }
}

extension BankAccountFormViewController: LoadingViewProtocol {
    func setupScrollViewConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.size.equalTo(view)
        }
    }
    
    func setupBodyConstraints() {
        allFieldsStackView.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(contentView).inset(Spacing.base02)
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
        }
        
        messageLabel.snp.makeConstraints {
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02) 
            $0.top.equalTo(allFieldsStackView.snp.bottom).offset(Spacing.base02)
        }
        
        explanationLabel.snp.makeConstraints {
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
            $0.top.equalTo(messageLabel.snp.bottom).offset(Spacing.base02)
        }

        actionButton.snp.makeConstraints {
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
            $0.top.equalTo(explanationLabel.snp.bottom).offset(Spacing.base01)
        }
    }
    
    func setupBankDetailsConstraints() {
        branchTextField.snp.makeConstraints {
            $0.height.equalTo(Layout.TextFields.height)
        }
        
        branchDigitTextField.snp.makeConstraints {
            $0.height.equalTo(Layout.TextFields.height)
            $0.width.equalTo(Layout.DigitTextField.width)
        }
        
        accountDigitTextField.snp.makeConstraints {
            $0.height.equalTo(Layout.TextFields.height)
            $0.width.equalTo(Layout.DigitTextField.width)
        }
        
        bankButton.snp.makeConstraints {
            $0.height.equalTo(Layout.TextFields.height)
        }

        bankArrowImage.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        accountTextField.snp.makeConstraints {
            $0.height.equalTo(Layout.TextFields.height)
        }
        
        accountTypeArrowImage.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        accountTypeButton.snp.makeConstraints {
            $0.height.equalTo(Layout.TextFields.height)
        }
        
        documentTextField.snp.makeConstraints {
            $0.height.equalTo(Layout.TextFields.height)
        }
        
        bankImageView.snp.makeConstraints {
            $0.leading.equalTo(contentView).offset(Layout.BankImage.leading)
            $0.centerY.equalTo(bankButton)
            $0.size.equalTo(Layout.BankImage.size)
        }
    }
}

@objc
extension BankAccountFormViewController {
    func keyboardWillShow(_ notification: NSNotification) {
        scrollView.contentInset = Layout.Insets.keyboard
    }
    
    func keyboardWillHide(_ notification: NSNotification) {
        scrollView.contentInset = .zero
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }

    func listBank() {
        interactor.openListBank()
    }

    func listAccountTypes() {
        interactor.listAccountTypes()
    }

    func valueDidChange() {
        interactor.didChangeData(bankAccountItem)
    }

    func didTapChangeAccount() {
        interactor.saveNewAccount()
    }

    func didTapExplanationLabel() {
        interactor.didTapExplanation()
    }
}

extension BankAccountFormViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            return true
        }
        
        switch textField {
        case branchTextField:
            return textField.text?.count != VisualRule.branchMaxChars
        case accountTextField:
            return textField.text?.count != VisualRule.accountMaxChars
        case documentTextField:
            let value: String? = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
            let text: String?
            do {
                let mask = try Mask(format: Layout.Mask.cpf)
                let result: Mask.Result = mask.apply(toText: CaretString(string: value ?? ""), autocomplete: true)
                text = result.formattedText.string
            } catch {
                text = value
            }
            textField.text = text
            textField.sendActions(for: .editingChanged)
            return false
        default:
            return true
        }
    }
}

extension BankAccountFormViewController: BankAccountFormDisplay {
    func changeActionButton(enabled: Bool) {
        actionButton.isEnabled = enabled
    }

    func bankChanged(_ bank: BankItem) {
        bankButton.setTitle(bank.name, for: .normal)
        bankImageView.sd_setImage(with: URL(string: bank.imgUrl), placeholderImage: Assets.icoBankPlaceholder.image)
        accountTypeButton.setTitle(Localizable.accountTypePlaceholder, for: .normal)
        valueDidChange()
    }

    func bankAccount(_ account: BankAccountItem) {
        let bankItem = BankItem(name: account.bankName, imgUrl: account.bankImageUrl)
        bankButton.setTitle(bankItem.name, for: .normal)
        bankImageView.sd_setImage(with: URL(string: bankItem.imgUrl), placeholderImage: Assets.icoBankPlaceholder.image)

        accountTypeButton.setTitle(account.type, for: .normal)
        branchTextField.text = account.branch
        branchDigitTextField.text = account.branchDigit
        accountTextField.text = account.account
        accountDigitTextField.text = account.accountDigit
        documentTextField.text = account.document
    }

    func bankAccountValidationError(_ message: String) {
        let feedbackCard = ApolloFeedbackCard(
            description: message,
            iconType: .error,
            layoutType: .onlyText,
            emphasisType: .high
        )
        
        contentView.addSubview(feedbackCard)
        
        feedbackCard.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(bankButton.snp.top).offset(-Spacing.base02)
        }
    }

    func startLoading() {
        startLoadingView()
    }

    func stopLoading() {
        stopLoadingView()
    }

    func displayPersonalExplanation() {
        let attrText = NSMutableAttributedString()
        attrText.append(createDefaultText(Localizable.BankAccountForm.explanationTip))
        attrText.append(createUnderlineText(Localizable.BankAccountForm.explanationPersonal))
        explanationLabel.attributedText = attrText
    }
    
    func presentAccountTypes(_ itens: [AccountTypeItem]) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        itens.forEach { item in
            actionSheet.addAction(title: item.label, style: .default, isEnabled: true) { [weak self] _ in
                guard let self = self else { return }
                self.accountTypeButton.setTitle(item.label, for: .normal)
                self.interactor.didSelectAccountType(item.value)
                self.interactor.didChangeData(self.bankAccountItem)
            }
        }
        
        actionSheet.addAction(title: Strings.Default.cancel, style: .cancel, isEnabled: true, handler: nil)
        
        present(actionSheet, animated: true, completion: nil)
    }

    func validationMessage(_ message: String) {
        messageLabel.text = message
    }

    func changeDocumentField(enabled: Bool, value: String?) {
        documentTextField.text = value
        documentTextField.isEnabled = enabled

        if enabled {
            documentTextField.placeholder = Localizable.personDocumentPlaceholder
            documentTextField.textColor = Layout.Colors.enabledTextField
        } else {
            documentTextField.placeholder = Localizable.companyDocumentPlaceholder
            documentTextField.textColor = Layout.Colors.disabledTextField
        }
    }

    func enableDocumentField() {
        documentTextField.isEnabled = true
        documentTextField.textColor = Layout.Colors.enabledTextField
    }
}

private extension BankAccountFormViewController {
    func configureKeyboardBehaviours() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        tapGesture.delaysTouchesBegan = false
        tapGesture.delaysTouchesEnded = false
        view.addGestureRecognizer(tapGesture)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }

    func createDefaultText(_ text: String) -> NSAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.caption().font(),
            .foregroundColor: Colors.black.color
        ]
        
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    func createUnderlineText(_ text: String) -> NSAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.caption().font(),
            .foregroundColor: Colors.branding600.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        return NSAttributedString(string: text, attributes: attributes)
    }
}
