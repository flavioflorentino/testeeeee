import Core
import Foundation

enum BankAccountFormEndpoint {
    case getBankAccount
    case getAccountType(_ bankId: String, cnpj: String)
    case getCompanyInfo(_ cnpj: String)
    case verifyIntegrity(cpf: String, cnpj: String)
    case checkBankAccount
}

extension BankAccountFormEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .getBankAccount:
            return "/v2/bank-account"
        case let .getAccountType(bankId, cnpj):
            return "/v2/bank/account-types/\(bankId)/\(cnpj)"
        case .getCompanyInfo(let cnpj):
            return "/seller/validate/\(cnpj)"
        case .verifyIntegrity:
            return "/account/validate-qsa"
        case .checkBankAccount:
            return "/v2/bank-account/check"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getBankAccount, .getCompanyInfo, .getAccountType, .checkBankAccount:
            return .get
        case .verifyIntegrity:
            return .post
        }
    }
    
    var body: Data? {
        switch self {
        case let .verifyIntegrity(cpf, cnpj):
            return [
                "cpf": cpf.onlyNumbers,
                "cnpj": cnpj
            ].toData()
        default:
            return nil
        }
    }
}
