import Core
import Foundation

protocol BankAccountFormServicing {
    func getBankAccount(_ completion: @escaping (Result<CoreBankAccount, ApiError>) -> Void)
    func getAccountType(_ bankCode: String, cnpj: String, completion: @escaping (Result<[CoreAccountType], ApiError>) -> Void)
    func validateDocuments(cpf: String, cnpj: String, completion: @escaping (Result<ValidateQSAResponse?, ApiError>) -> Void)
    func checkBankAccount(completion: @escaping(Result<BankAccountValidation?, ApiError>) -> Void)
}

final class BankAccountFormService {
    typealias Dependencies = HasMainQueue 
    private typealias Endpoint = BankAccountFormEndpoint
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - BankAccountFormServicing
extension BankAccountFormService: BankAccountFormServicing {
    func getBankAccount(_ completion: @escaping (Result<CoreBankAccount, ApiError>) -> Void) {
        BizApiV2<CoreBankAccount>(endpoint: Endpoint.getBankAccount).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }

    func getAccountType(_ bankCode: String, cnpj: String, completion: @escaping (Result<[CoreAccountType], ApiError>) -> Void) {
        BizApiV2<[CoreAccountType]>(endpoint: Endpoint.getAccountType(bankCode, cnpj: cnpj)).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }

    func validateDocuments(cpf: String, cnpj: String, completion: @escaping (Result<ValidateQSAResponse?, ApiError>) -> Void) {
        let endpoint = Endpoint.verifyIntegrity(cpf: cpf, cnpj: cnpj)
        BizApi<ValidateQSAResponse>(endpoint: endpoint).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }

    func checkBankAccount(completion: @escaping(Result<BankAccountValidation?, ApiError>) -> Void) {
        let endPoint = HomeEndpoint.checkBankAccount
        BizApi<BankAccountValidation>(endpoint: endPoint).bizExecute {[weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model.data)
                completion(mappedResult)
            }
        }
    }
}
