import CoreSellerAccount
import Foundation
import InputMask
import UI

protocol BankAccountFormPresenting: AnyObject {
    var viewController: BankAccountFormDisplay? { get set }
    func didNextStep(action: BankAccountFormAction)
    func perform(_ viewAction: BankAccountFormPresenter.ViewAction)
    func changeButton(enabled: Bool)
    func showBank(_ bank: CoreBank)
    func bankAccount(_ account: CoreBankAccount?, of companyType: CompanyType?)
    func listAccountTypes(_ types: [CoreAccountType])
    func displayPersonalExplanation()
    func showWrongDocumentModal()
    func somenthingWentWrong(_ message: String?, completion: (() -> Void)?)
    func validationError(_ message: String)
    func changeDocumentField(enabled: Bool, value: String?)
    func showAccountErrorComponent(_ error: BankAccountValidation)
}

struct BankItem {
    let name: String
    let imgUrl: String
}

struct BankAccountItem {
    let bankName: String
    let bankImageUrl: String
    let branch: String
    let branchDigit: String
    let account: String
    let accountDigit: String
    let type: String
    let operation: String
    let document: String
}

struct AccountTypeItem {
    let label: String
    let value: Int
}

final class BankAccountFormPresenter {
    private typealias Localizable = Strings.BankAccount
    private let coordinator: BankAccountFormCoordinating
    weak var viewController: BankAccountFormDisplay? 

    private let cnpjMask = "[00].[000].[000]/[0000]-[00]"
    private let cpfMask = "[000].[000].[000]-[00]"

    enum ViewAction {
        case startLoading
        case stopLoading
    }

    init(coordinator: BankAccountFormCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - BankAccountFormPresenting
extension BankAccountFormPresenter: BankAccountFormPresenting {
    func didNextStep(action: BankAccountFormAction) {
        coordinator.perform(action: action)
    }

    func perform(_ viewAction: ViewAction) {
        switch viewAction {
        case .startLoading:
            viewController?.startLoading()
        case .stopLoading:
            viewController?.stopLoading()
        }
    }

    func changeButton(enabled: Bool) {
        viewController?.changeActionButton(enabled: enabled)
    }

    func showBank(_ bank: CoreBank) {
        let bankItem = BankItem(
            name: "\(bank.code) - \(bank.name)",
            imgUrl: bank.image
        )
        viewController?.bankChanged(bankItem)
    }

    func bankAccount(_ account: CoreBankAccount?, of companyType: CompanyType?) {
        guard let account = account, let bank = account.bank else { return } 
        var document = String()

        if let cnpj = account.cnpj, cnpj.isNotEmpty {
            document = Mask.maskString(input: cnpj, mask: cnpjMask)
        }
        
        if companyType == .individual, let cpf = account.cpf, cpf.isNotEmpty {
            document = Mask.maskString(input: cpf, mask: cpfMask)
            viewController?.enableDocumentField()
        }

        let bankName = "\(bank.code) - \(bank.name)"
        let accountItem = BankAccountItem(
            bankName: bankName,
            bankImageUrl: bank.image,
            branch: account.branch,
            branchDigit: account.branchDigit ?? "",
            account: account.account,
            accountDigit: account.accountDigit,
            type: bank.accountTypes.first(where: { $0.selected })?.label ?? Localizable.accountTypePlaceholder,
            operation: "",
            document: document
        )

        viewController?.bankAccount(accountItem)
    }

    func showAccountErrorComponent(_ error: BankAccountValidation) {
        guard let text = error.validationIssue?.message else { return }
        
        viewController?.bankAccountValidationError(text)
    }

    func displayPersonalExplanation() {
        viewController?.displayPersonalExplanation()
    }

    func showWrongDocumentModal() {
        didNextStep(action: .wrongDocumentModal)
    }

    func listAccountTypes(_ types: [CoreAccountType]) {
        var accountTypes: [AccountTypeItem] = []
        for (index, accountType) in types.enumerated() {
            let item = AccountTypeItem(label: accountType.label, value: index)
            accountTypes.append(item)
        }

        viewController?.presentAccountTypes(accountTypes)
    }

    func somenthingWentWrong(
        _ message: String? = Localizable.BankAccountForm.Error.message,
        completion: (() -> Void)? 
    ) {
        didNextStep(action: .errorModal(message ?? "", completion: completion))
    }

    func validationError(_ message: String) {
        viewController?.validationMessage(message)
    }

    func changeDocumentField(enabled: Bool, value: String?) {
        var formattedValue: String?

        if !enabled, let cnpj = value {
            formattedValue = Mask.maskString(input: cnpj, mask: cnpjMask)
        }

        viewController?.changeDocumentField(enabled: enabled, value: formattedValue)
    }
}
