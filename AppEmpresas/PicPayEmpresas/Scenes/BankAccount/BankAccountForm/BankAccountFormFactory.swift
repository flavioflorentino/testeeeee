import CoreSellerAccount
import Foundation
import UIKit

enum BankAccountFormFactory {
    static func make(_ seller: Seller?, companyType: CompanyType) -> BankAccountFormViewController {
        let container = DependencyContainer()
        let service: BankAccountFormServicing = BankAccountFormService(dependencies: container)
        let coordinator: BankAccountFormCoordinating = BankAccountFormCoordinator(dependencies: container)
        let presenter: BankAccountFormPresenting = BankAccountFormPresenter(coordinator: coordinator)
        let interactor = BankAccountFormInteractor(
            service: service, 
            presenter: presenter, 
            dependencies: container,
            companyType: companyType
        )
        let viewController = BankAccountFormViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController
        interactor.seller = seller

        return viewController
    }
}
