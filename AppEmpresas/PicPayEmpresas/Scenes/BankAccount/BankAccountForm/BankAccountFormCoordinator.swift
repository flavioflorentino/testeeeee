import AnalyticsModule
import AssetsKit
import LegacyPJ
import UI
import UIKit

enum BankAccountFormAction {
    case listBank(delegate: BankSelectFormViewControllerDelegate)
    case confirmChanges(_ bankAccount: CoreBankAccount)
    case wrongDocumentModal
    case helpModal(_ description: String, seller: Seller?)
    case errorModal(_ message: String, completion: (() -> Void)? = nil)
}

protocol BankAccountFormCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BankAccountFormAction)
}

final class BankAccountFormCoordinator {
    typealias Dependencies = HasAuthManager & HasAnalytics
    private typealias LocalizableHM = Strings.BankAccount.BankAccountTips.HelpModal
    private typealias LocalizableFE = Strings.BankAccount.BankAccountForm.Error
    private typealias LocalizableWD = Strings.BankAccount.BankAccountForm.WrongDocument
    
    weak var viewController: UIViewController?
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - BankAccountFormCoordinating
extension BankAccountFormCoordinator: BankAccountFormCoordinating {
    func perform(action: BankAccountFormAction) {
        switch action {
        case .listBank(let delegate):
            let controller: BankSelectFormViewController = ViewsManager.instantiateViewController(.settings)
            controller.setup(delegate: delegate)
            viewController?.pushViewController(controller)
        case .confirmChanges(let bankAccount):
            let controller = BankAccountConfirmationFactory.make(.settings(bankAccount))
            viewController?.pushViewController(controller)
        case .wrongDocumentModal:
            wrongDocumentPopUp()
        case let .helpModal(description, seller):
            infoPopUp(with: description, seller: seller)
        case let .errorModal(message, completion):
            errorPopUp(message, completion: completion)
        }
    }
}

private extension BankAccountFormCoordinator {
    func wrongDocumentPopUp() {
        let action = PopupAction(title: LocalizableWD.buttonTitle, style: .fill)
        let infoPopUp = UI.PopupViewController(
            title: LocalizableWD.title,
            description: LocalizableWD.message,
            image: Assets.errorIcon.image
        )

        presentPopUp(infoPopUp, actions: [action])
    }

    func infoPopUp(with description: String, seller: Seller?) {
        let firstAction = PopupAction(title: LocalizableHM.buttonTitle, style: .fill) { [weak self] in
            self?.dependencies.analytics.log(BankAccountFormAnalytics.issueResolved)
        }

        let secondAction = PopupAction(title: LocalizableHM.secondaryButtonTitle, style: .link) { [weak self] in
            self?.navigateToChat(seller: seller)
            self?.dependencies.analytics.log(BankAccountFormAnalytics.needMoreHelp)
        }

        let infoPopUp = UI.PopupViewController(
            title: LocalizableHM.title,
            description: description,
            image: Resources.Icons.icoBlueInfo.image
        )

        presentPopUp(infoPopUp, actions: [firstAction, secondAction])
    }

    func errorPopUp(_ message: String, completion: (() -> Void)?) {
        let firstAction = PopupAction(title: LocalizableFE.tryAgain, style: .tryAgain) {
            completion?()
        }
        let secondAction = PopupAction(title: LocalizableFE.cancelEdition, style: .link) { [weak self] in
            self?.viewController?.navigationController?.popViewController(animated: true)
        }
        let infoPopUp = UI.PopupViewController(
            title: LocalizableFE.title,
            description: message,
            image: Assets.errorIcon.image
        )

        presentPopUp(infoPopUp, actions: [firstAction, secondAction])
    }

    func presentPopUp(_ popUp: UI.PopupViewController, actions: [PopupAction] = []) {
        popUp.hideCloseButton = true
        actions.forEach { popUp.addAction($0) }
        viewController?.showPopup(popUp)
    }
    
    func navigateToChat(seller: Seller?) {
        guard let viewController = viewController else { return }
        let token = dependencies.authManager.userAuth?.auth.accessToken
        BIZCustomerSupportManager(accessToken: token).presentFAQ(from: viewController, with: seller)
    }
}
