import Foundation
import LegacyPJ

struct CoreBankAccount: Codable {
    let account: String
    let accountDigit: String
    let bank: CoreBank?
    let bankId: String
    let branch: String
    let branchDigit: String?
    let cnpj: String?
    let cpf: String?
    let id: Int
    let value: String?
    let type: CoreAccountType.RawType?
    let label: String?

    enum CodingKeys: String, CodingKey {
        case account 
        case accountDigit = "account_digit"
        case bank
        case bankId = "bank_id"
        case branch 
        case branchDigit = "branch_digit"
        case cnpj
        case cpf
        case id
        case value
        case type
        case label
    }
}

extension CoreBankAccount {
    var confirmationItem: AccountConfirmationItem {
        let document: String
        let documentType: AccountConfirmationItem.DocumentType 
        let digit = branchDigit ?? ""

        switch type {
        case .individual:
            document = cpf ?? ""
            documentType = .cpf
        case .company, nil:
            document = cnpj ?? ""
            documentType = .cnpj
        }

        return AccountConfirmationItem(
            imageUrl: bank?.image ?? "", 
            bank: bank?.name ?? "",
            branch: digit.isEmpty ? "\(branch)" : "\(branch)-\(digit)",
            account: "\(account)-\(accountDigit)", 
            document: document,
            documentType: documentType,
            type: label ?? ""
        )
    }
}

struct CoreBank: Codable {
    let code: String
    let name: String
    let image: String
    let accountTypes: [CoreAccountType]
    
    enum CodingKeys: String, CodingKey { 
        case code = "id"
        case name
        case image
        case accountTypes = "account_types"
    }
}

extension CoreBank {
    init(_ bank: Bank) {
        self.code = bank.code
        self.name = bank.name
        self.image = bank.image ?? ""
        self.accountTypes = []
    }
}

struct CoreAccountType: Codable {
    enum RawType: String, Codable, Equatable {
        case company, individual
    }

    enum CodingKeys: String, CodingKey { 
        case label, value, type, selected
    }

    let label: String
    let value: String
    let type: RawType
    let selected: Bool

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.label = try container.decode(String.self, forKey: .label)
        self.value = try container.decode(String.self, forKey: .value)
        self.type = try container.decode(RawType.self, forKey: .type)
        self.selected = try container.decodeIfPresent(Bool.self, forKey: .selected) ?? false
    }
}

extension CoreAccountType: Equatable {
    static func == (lhs: CoreAccountType, rhs: CoreAccountType) -> Bool {
        lhs.value == rhs.value && lhs.type == rhs.type
    }
}
