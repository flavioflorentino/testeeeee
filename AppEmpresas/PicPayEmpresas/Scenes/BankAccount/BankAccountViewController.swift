import AssetsKit
import InputMask
import SnapKit
import UI
import UIKit

// MARK: Layout Extension
private extension BankAccountViewController.Layout {
    enum Fonts {
        static let textField = UIFont.systemFont(ofSize: 14)
        static let messageError = UIFont.systemFont(ofSize: 12)
    }
    
    enum Insets {
        static let bankTitle = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 0)
        static let accountTypeTitle = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        static let keyboard = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
    }
    
    enum Colors {
        static let textTextField = Palette.ppColorGrayscale600.color
        static let disabledTextField = Palette.ppColorGrayscale400.color
        static let backgroundTextField = Palette.ppColorGrayscale000.color
    }
    
    enum Spacing {
        static let horizontal: CGFloat = 8
        static let vertcal: CGFloat = 12
        static let `default`: CGFloat = 20
        static let messageError: CGFloat = 4
    }
    
    enum Mask {
        static let cpf = "[000].[000].[000]-[00]"
    }
    
    enum DigitTextField {
        static let width: CGFloat = 50
    }
    
    enum BankImage {
        static let leading: CGFloat = 30
        static let size: CGFloat = 25
    }
    
    enum TextFields {
        static let height: CGFloat = 40
    }
    
    enum Others {
        static let buttonCornerRadius: CGFloat = 20
    }
}

final class BankAccountViewController: ViewController<BankAccountViewModelInputs, UIView> {
    private typealias Localizable = Strings.BankAccount
    
    fileprivate enum Layout { }
    
    private lazy var bankButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.bankPlaceholder, for: .normal)
        button.setTitleColor(Layout.Colors.disabledTextField, for: .normal)
        button.backgroundColor = Layout.Colors.backgroundTextField
        button.titleLabel?.font = Layout.Fonts.textField
        button.titleEdgeInsets = Layout.Insets.bankTitle
        button.layer.cornerRadius = Layout.Others.buttonCornerRadius
        button.contentHorizontalAlignment = .left
        return button
    }()
    
    private lazy var bankImageView: UIImageView = {
        let imageView = UIImageView(image: Assets.icoBankPlaceholder.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var branchTextField: UIRoundedTextField = {
        let textField = UIRoundedTextField()
        textField.placeholder = Localizable.branchPlaceholder
        textField.textColor = Layout.Colors.textTextField
        textField.backgroundColor = Layout.Colors.backgroundTextField
        textField.font = Layout.Fonts.textField
        textField.keyboardType = .numberPad
        textField.delegate = self
        textField.layer.borderWidth = Border.none
        textField.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        return textField
    }()
    
    private lazy var branchDigitTextField: UIRoundedTextField = {
        let textField = UIRoundedTextField()
        textField.placeholder = Localizable.digitPlaceholder
        textField.textColor = Layout.Colors.textTextField
        textField.backgroundColor = Layout.Colors.backgroundTextField
        textField.font = Layout.Fonts.textField
        textField.keyboardType = .numberPad
        textField.delegate = self
        textField.layer.borderWidth = Border.none
        return textField
    }()
    
    private lazy var accountTextField: UIRoundedTextField = {
        let textField = UIRoundedTextField()
        textField.placeholder = Localizable.accountPlaceholder
        textField.textColor = Layout.Colors.textTextField
        textField.backgroundColor = Layout.Colors.backgroundTextField
        textField.font = Layout.Fonts.textField
        textField.keyboardType = .numberPad
        textField.delegate = self
        textField.layer.borderWidth = Border.none
        return textField
    }()
    
    private lazy var accountDigitTextField: UIRoundedTextField = {
        let textField = UIRoundedTextField()
        textField.placeholder = Localizable.digitPlaceholder
        textField.textColor = Layout.Colors.textTextField
        textField.backgroundColor = Layout.Colors.backgroundTextField
        textField.font = Layout.Fonts.textField
        textField.keyboardType = .numberPad
        textField.layer.borderWidth = Border.none
        textField.textAlignment = .center
        return textField
    }()
    
    private lazy var operationAccountTextField: UIRoundedTextField = {
        let textField = UIRoundedTextField()
        textField.placeholder = Localizable.operationAccountPlaceholder
        textField.textColor = Layout.Colors.textTextField
        textField.backgroundColor = Layout.Colors.backgroundTextField
        textField.font = Layout.Fonts.textField
        textField.keyboardType = .numberPad
        textField.layer.borderWidth = Border.none
        return textField
    }()
    
    private lazy var accountTypeButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.accountTypePlaceholder, for: .normal)
        button.setTitleColor(Layout.Colors.disabledTextField, for: .normal)
        button.backgroundColor = Layout.Colors.backgroundTextField
        button.titleLabel?.font = Layout.Fonts.textField
        button.titleEdgeInsets = Layout.Insets.accountTypeTitle
        button.layer.cornerRadius = Layout.Others.buttonCornerRadius
        button.contentHorizontalAlignment = .left
        return button
    }()
    
    private lazy var documentTextField: UIRoundedTextField = {
        let textField = UIRoundedTextField()
        textField.placeholder = Localizable.companyDocumentPlaceholder
        textField.textColor = Layout.Colors.disabledTextField
        textField.backgroundColor = Layout.Colors.backgroundTextField
        textField.font = Layout.Fonts.textField
        textField.keyboardType = .numberPad
        textField.isEnabled = false
        textField.delegate = self
        textField.layer.borderWidth = Border.none
        return textField
    }()
    
    private lazy var accountStackView: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Layout.Spacing.horizontal
        stack.axis = .horizontal
        stack.addArrangedSubview(branchTextField)
        stack.addArrangedSubview(branchDigitTextField)
        stack.addArrangedSubview(accountTextField)
        stack.addArrangedSubview(accountDigitTextField)
        return stack
    }()
    
    private lazy var allFieldsStackView: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Layout.Spacing.vertcal
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.addArrangedSubview(bankButton)
        stack.addArrangedSubview(accountStackView)
        stack.addArrangedSubview(accountTypeButton)
        stack.addArrangedSubview(operationAccountTextField)
        stack.addArrangedSubview(documentTextField)
        return stack
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.messageError
        label.textColor = Colors.critical900.color
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private lazy var actionButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(Localizable.changeAccount, for: .normal)
        button.normalTitleColor = Palette.white.color
        button.normalBackgrounColor = Palette.ppColorBranding300.color
        button.highlightedBackgrounColor = Palette.ppColorBranding400.color
        button.cornerRadius = Layout.Others.buttonCornerRadius
        button.addTarget(self, action: #selector(didTapChangeAccount(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.isUserInteractionEnabled = true
        return scrollView
    }()
    
    private var loadingView: UIView?
    private let branchMaxChars = 4
    private let accountMaxChars = 12
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getBankAccount()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadingView?.frame.size = view.frame.size
        loadingView?.layoutIfNeeded()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(allFieldsStackView)
        contentView.addSubview(messageLabel)
        contentView.addSubview(actionButton)
        contentView.addSubview(bankImageView)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
        title = Localizable.viewTitle
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        tapGesture.delaysTouchesBegan = false
        tapGesture.delaysTouchesEnded = false
        view.addGestureRecognizer(tapGesture)
    }
    
    override func setupConstraints() {
        setupScrollViewConstraints()
        setupBodyConstraints()
        setupBankDetailsConstraints()
    }
}

// MARK: - BankAccountViewController

private extension BankAccountViewController {
    func setupScrollViewConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.size.equalTo(view)
        }
    }
    
    func setupBodyConstraints() {
        allFieldsStackView.layout {
            $0.leading == contentView.leadingAnchor + Layout.Spacing.default
            $0.trailing == contentView.trailingAnchor - Layout.Spacing.default
            $0.top == contentView.topAnchor + Layout.Spacing.default
        }
        
        messageLabel.layout {
            $0.top == allFieldsStackView.bottomAnchor + Layout.Spacing.vertcal
            $0.leading == contentView.leadingAnchor + Layout.Spacing.default
            $0.trailing == contentView.trailingAnchor - Layout.Spacing.default
        }
        
        actionButton.layout {
            $0.leading == contentView.leadingAnchor + Layout.Spacing.default
            $0.trailing == contentView.trailingAnchor - Layout.Spacing.default
            $0.height == Layout.TextFields.height
            $0.top == messageLabel.bottomAnchor + Layout.Spacing.default
        }
    }
    
    func setupBankDetailsConstraints() {
        branchTextField.layout {
            $0.height == Layout.TextFields.height
        }
        
        branchDigitTextField.layout {
            $0.height == Layout.TextFields.height
            $0.width == Layout.DigitTextField.width
        }
        
        accountDigitTextField.layout {
            $0.height == Layout.TextFields.height
            $0.width == Layout.DigitTextField.width
        }
        
        bankButton.layout {
            $0.height == Layout.TextFields.height
        }
        
        accountTextField.layout {
            $0.height == Layout.TextFields.height
        }
        
        accountTypeButton.layout {
            $0.height == Layout.TextFields.height
        }
        
        operationAccountTextField.layout {
            $0.height == Layout.TextFields.height
        }
        
        documentTextField.layout {
            $0.height == Layout.TextFields.height
        }
        
        bankImageView.layout {
            $0.leading == contentView.leadingAnchor + Layout.BankImage.leading
            $0.centerY == bankButton.centerYAnchor
            $0.height == Layout.BankImage.size
            $0.width == Layout.BankImage.size
        }
    }
    
    func changeTextFieldsStatus(enable: Bool) {
        branchTextField.isEnabled = enable
        branchDigitTextField.isEnabled = enable
        accountTextField.isEnabled = enable
        accountDigitTextField.isEnabled = enable
        operationAccountTextField.isEnabled = enable
    }
    
    func applyColorToTextFields(_ color: UIColor) {
        bankButton.setTitleColor(color, for: .normal)
        accountTypeButton.setTitleColor(color, for: .normal)
        branchTextField.textColor = color
        branchDigitTextField.textColor = color
        accountTextField.textColor = color
        accountDigitTextField.textColor = color
        operationAccountTextField.textColor = color
    }
}

// MARK: UI Target Methods
private extension BankAccountViewController {
    @objc
    func didTapBankButton(_ sender: UITextField) {
        viewModel.selectBank()
    }
    
    @objc
    func didTapAccountType(_ sender: UITextField) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let currentAccount = Strings.Default.currentAccount
        let savingAccount = Strings.Default.savingAccount
        
        actionSheet.addAction(title: currentAccount, style: .default, isEnabled: true) { [weak self] _ in
            self?.accountTypeButton.setTitle(currentAccount, for: .normal)
        }
        actionSheet.addAction(title: savingAccount, style: .default, isEnabled: true) { [weak self] _ in
            self?.accountTypeButton.setTitle(savingAccount, for: .normal)
        }
        actionSheet.addAction(title: Strings.Default.cancel, style: .cancel, isEnabled: true, handler: nil)
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    @objc
    func didTapChangeAccount(_ sender: UIButton) {
        let bank = BankAccountItem(
            bankName: String(),
            bankImageUrl: String(),
            branch: branchTextField.text ?? String(),
            branchDigit: branchDigitTextField.text ?? String(),
            account: accountTextField.text ?? String(),
            accountDigit: accountDigitTextField.text ?? String(),
            type: accountTypeButton.titleLabel?.text ?? String(),
            operation: operationAccountTextField.text ?? String(),
            document: documentTextField.text ?? String()
        )
        viewModel.updateBankAccount(bank)
    }
    
    // MARK: - Keyboard Events Handler
    @objc
    func keyboardWillShow(_ notification: NSNotification) {
        scrollView.contentInset = Layout.Insets.keyboard
    }
    
    @objc
    func keyboardWillHide(_ notification: NSNotification) {
        scrollView.contentInset = .zero
    }
    
    @objc
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

// MARK: View Model Outputs
extension BankAccountViewController: BankAccountDisplay {
    func bankAccount(_ bankAccount: BankAccountItem) {
        bankButton.setTitle(bankAccount.bankName, for: .normal)
        accountTypeButton.setTitle(bankAccount.type, for: .normal)
        branchTextField.text = bankAccount.branch
        branchDigitTextField.text = bankAccount.branchDigit
        accountTextField.text = bankAccount.account
        accountDigitTextField.text = bankAccount.accountDigit
        operationAccountTextField.text = bankAccount.operation
        documentTextField.text = bankAccount.document
        
        bankImageView.sd_setImage(with: URL(string: bankAccount.bankImageUrl), placeholderImage: Assets.icoBankPlaceholder.image)
    }
    
    func didChangeBank(_ bank: BankItem) {
        bankButton.setTitle(bank.name, for: .normal)
        bankImageView.sd_setImage(with: URL(string: bank.imgUrl), placeholderImage: Assets.icoBankPlaceholder.image)
    }
    
    func prepareViewForPresent() {
        changeTextFieldsStatus(enable: false)
        applyColorToTextFields(Layout.Colors.disabledTextField)
        actionButton.setTitle(Localizable.changeAccount, for: .normal)
        bankButton.removeTarget(self, action: #selector(didTapBankButton(_:)), for: .touchUpInside)
        accountTypeButton.removeTarget(self, action: #selector(didTapAccountType(_:)), for: .touchUpInside)
    }
    
    func prepareViewForEdit() {
        changeTextFieldsStatus(enable: true)
        applyColorToTextFields(Layout.Colors.textTextField)
        actionButton.setTitle(Strings.Default.continue, for: .normal)
        bankButton.addTarget(self, action: #selector(didTapBankButton(_:)), for: .touchUpInside)
        accountTypeButton.addTarget(self, action: #selector(didTapAccountType(_:)), for: .touchUpInside)
    }
    
    func operationField(hidden: Bool) {
        operationAccountTextField.isHidden = hidden
    }
    
    func showErrorMessage(_ message: String) {
        messageLabel.textColor = Colors.critical900.color
        messageLabel.text = message
    }
    
    func showLockedBankAccountMessage() {
        messageLabel.text = Strings.BankAccount.LockedStatus.changeNotAllowed
        messageLabel.textColor = Colors.grayscale700.color
    }
    
    func clearMessageText() {
        messageLabel.text = ""
    }
    
    func startLoadingAnimation() {
        loadingView?.removeFromSuperview()
        
        let animationView = LoadingHeaderView(autoLayout: { })
        animationView.backgroundColor = Palette.ppColorGrayscale100.color
        animationView.alpha = 0.7
        loadingView = animationView
        
        view.addSubview(animationView)
        animationView.snp.makeConstraints {
            $0.edges.equalTo(view)
        }
    }
    
    func stopLoadingAnimation() {
        UIView.animate(
            withDuration: 0.3,
            animations: {
                self.loadingView?.alpha = 0.0
            },
            completion: { finished in
                if finished {
                    self.loadingView?.removeFromSuperview()
                }
            }
        )
    }
    
    func showPopUp(with message: String) {
        let popup = PopupViewController()
        let alertPopup = AlertPopupController()
        alertPopup.popupTitle = Strings.Default.invalidCpfAccount
        alertPopup.popupText = message
        popup.contentController = alertPopup
        present(popup, animated: true, completion: nil)
    }
    
    func showAccountLockedPopUp() {
        let popupViewController = UI.PopupViewController(
            title: Strings.BankAccount.LockedStatus.title,
            description: Strings.BankAccount.LockedStatus.subtitle,
            preferredType: .image(Assets.iluInformation.image)
        )

        let confirmAction = PopupAction(title: Strings.BankAccount.LockedStatus.confirm, style: .fill) { [weak self] in
            self?.viewModel.selectUnderstandLocked()
        }
        let needHelpAction = PopupAction(title: Strings.BankAccount.LockedStatus.needHelp, style: .link, completion: { [weak self] in
            self?.viewModel.selectNeedHelp()
        })
        popupViewController.addAction(confirmAction)
        popupViewController.addAction(needHelpAction)
        present(popupViewController, animated: true, completion: nil)
    }
    
    func personalDocumentField(_ cpf: String) {
        documentTextField.text = cpf
        documentTextField.isEnabled = true
        documentTextField.placeholder = Localizable.personDocumentPlaceholder
        documentTextField.textColor = Layout.Colors.textTextField
    }
    
    func companyDocumentField(_ cnpj: String) {
        documentTextField.text = cnpj
        documentTextField.isEnabled = false
        documentTextField.placeholder = Localizable.companyDocumentPlaceholder
        documentTextField.textColor = Layout.Colors.disabledTextField
    }
}

extension BankAccountViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            return true
        }
        
        switch textField {
        case branchTextField:
            return textField.text?.count != branchMaxChars
        case accountTextField:
            return textField.text?.count != accountMaxChars
        case documentTextField:
            let textFieldText = textField.text ?? ""
            let value: String = (textFieldText as NSString).replacingCharacters(in: range, with: string)
            let text: String
            do {
                let mask = try Mask(format: Layout.Mask.cpf)
                let result: Mask.Result = mask.apply(toText: CaretString(string: value), autocomplete: true)
                text = result.formattedText.string
            } catch {
                text = value
            }
            textField.text = text
            textField.sendActions(for: .editingChanged)
            return false
        default:
            return true
        }
    }
}
