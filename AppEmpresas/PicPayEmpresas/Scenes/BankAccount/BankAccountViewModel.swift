import AnalyticsModule
import CoreSellerAccount
import Foundation
import FeatureFlag
import LegacyPJ

protocol BankAccountViewModelInputs: AnyObject {
    func getBankAccount()
    func updateBankAccount(_ bankAccountItem: BankAccountItem)
    func selectBank()
    func selectNeedHelp()
    func selectUnderstandLocked()
}

typealias BankAccountDelegate = BankSelectFormViewControllerDelegate & RegistrationAccountTypeProtocol

final class BankAccountViewModel {
    private typealias Localizable = Strings.BankAccount
    typealias Dependencies = HasFeatureManager & HasAnalytics
    
    private enum ViewStates {
        case viewing, editing
    }
    
    private let service: BankAccountServicing
    private let presenter: BankAccountPresenting
    private let dependencies: Dependencies
    private let caixaCode = "104"
    
    private var viewState: ViewStates
    private var bankAccount: BankAccount?
    private var bank: Bank?
    private var companyType: CompanyType?
    private var accountTypeSelected: AccountType?
    private let seller: Seller?
    var isAccountLocked = false

    init(service: BankAccountServicing, presenter: BankAccountPresenting, seller: Seller?, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.viewState = .viewing
        self.seller = seller
        self.dependencies = dependencies
    }
    
    private func checkResidenceLockStatus(for serverBankAccount: BankAccount, of type: CompanyType?) {
        service.getBankResidenceLockStatus(completion: { [weak self] result in
            if case .success(let response) = result {
                self?.isAccountLocked = response.isLocked
                self?.presenter.didFetchBankAccount(serverBankAccount, of: type, isLocked: response.isLocked)
                self?.dependencies.analytics.log(BankAccountAnalytics.residenceLockStatusLabelViewed)
            } else {
                self?.dependencies.analytics.log(BankAccountAnalytics.residenceLockErrorViewed)
                self?.presenter.didFetchBankAccount(serverBankAccount, of: type, isLocked: false)
            }
        })
    }
}

extension BankAccountViewModel: BankAccountViewModelInputs {
    func getBankAccount() {
        presenter.willFetchBankAccount()
        service.getBankAccount { [weak self] error, serverBankAccount in
            guard let self = self else { return }
            if let error = error {
                self.presenter.showError(error)
                return
            }
            guard let serverBankAccount = serverBankAccount else { return }
            
            self.bankAccount = serverBankAccount
            self.bank = serverBankAccount.bank
            self.service.verifyCompanyType(serverBankAccount.cnpj ?? "") { error, type in
                if let error = error {
                    self.presenter.showError(error)
                    return
                }
                self.companyType = type
                
                guard self.dependencies.featureManager.isActive(.featureCheckResidenceLock) else {
                    self.presenter.didFetchBankAccount(serverBankAccount, of: type, isLocked: false)
                    return
                }
                self.checkResidenceLockStatus(for: serverBankAccount, of: type)
            }
        }
    }
    
    func updateBankAccount(_ bankAccountItem: BankAccountItem) {
        dependencies.analytics.log(BankAccountAnalytics.changeAccountClicked)
        guard dependencies.featureManager.isActive(.featureResidenceChangeLock) else {
            handleBankAccountUpdate(bankAccountItem)
            return
        }
        service.unlockBankAcount { [weak self] result in
            switch result {
            case .success(let response):
                guard response.success else {
                    self?.presenter.didFailToUnlockBankAccount()
                    return
                }
                self?.handleBankAccountUpdate(bankAccountItem)
            case .failure:
                self?.presenter.didFailToUnlockBankAccount()
            }
        }
    }
    
    func selectBank() {
        presenter.didNextStep(action: .listBank(self))
    }
    
    func selectNeedHelp() {
        presenter.didNextStep(action: .openFAQ)
        dependencies.analytics.log(BankAccountAnalytics.residenceUnlockFailedNeedHelpClicked)
    }
    
    func selectUnderstandLocked() {
        dependencies.analytics.log(BankAccountAnalytics.residenceUnlockFailedOkClicked)
    }
    
    private func handleBankAccountUpdate(_ bankAccountItem: BankAccountItem) {
        presenter.clearErrorMessage()
        switch viewState {
        case .viewing:
            let individual = companyType == .individual
            let action: BankAccountAction = individual ? .accountType(self, seller: seller) : .listBank(self)
            presenter.didNextStep(action: action)
        case .editing:
            validateAndSave(bankAccountItem)
        }
    }
    
    private func validateAndSave(_ bankAccountItem: BankAccountItem) {
        guard validAccount(bankAccountItem) else {
            return
        }
        
        presenter.willSaveBankAccount()
        
        if companyType == .individual, bankAccountItem.document.onlyNumbers.count <= 11 {
            let cnpj = bankAccount?.cnpj ?? ""
            service.verifyDocumentRelation(cnpj, cpf: bankAccountItem.document) { [weak self] error, _ in
                guard let self = self else {
                    return
                }
                if let error = error {
                    self.dependencies.analytics.log(BankAccountAnalytics.invalidDocument(self.seller?.id))
                    self.presenter.showPopUp(error)
                    return
                }
                
                self.saveBankAccount(self.parseBankAccount(bankAccountItem))
            }
        } else {
            saveBankAccount(parseBankAccount(bankAccountItem))
        }
    }
    
    private func validAccount(_ bankAccountItem: BankAccountItem) -> Bool {
        if bankAccountItem.branch.count < 3 {
            presenter.showError(LegacyPJError(message: Localizable.branchInvalid))
            return false
        }
        
        if bankAccountItem.account.count < 3 {
            presenter.showError(LegacyPJError(message: Localizable.accountInvalid))
            return false
        }
        
        if bankAccountItem.accountDigit.isEmpty {
            presenter.showError(LegacyPJError(message: Localizable.accountDigitInvalid))
            return false
        }
        
        if bank?.code == caixaCode, bankAccountItem.operation.isEmpty {
            presenter.showError(LegacyPJError(message: Localizable.operationInvalid))
            return false
        }
        
        if accountTypeSelected == .personal {
            if bankAccountItem.document.isEmpty {
                presenter.showError(LegacyPJError(message: Localizable.documentInvalid))
                return false
            }
            
            if !bankAccountItem.document.onlyNumbers.validCpf {
                presenter.showError(LegacyPJError(message: Localizable.cpfInvalid))
                return false
            }
        }
        
        return true
    }
    
    private func parseBankAccount(_ bankAccountItem: BankAccountItem) -> BankAccount {
        var bankAccount = BankAccount()
        bankAccount.bank = bank
        bankAccount.branch = bankAccountItem.branch
        bankAccount.branchDigit = bankAccountItem.branchDigit
        bankAccount.account = bankAccountItem.account
        bankAccount.accountDigit = bankAccountItem.accountDigit
        bankAccount.type = bankAccountItem.type
        bankAccount.operation = String()
        
        if bank?.code == caixaCode {
            bankAccount.operation = bankAccountItem.operation
        }
        
        let document = bankAccountItem.document.onlyNumbers
        
        if document.count <= 11 {
            bankAccount.cpf = document
        } else {
            bankAccount.cnpj = document
        }
        
        return bankAccount
    }
    
    private func saveBankAccount(_ bankAccount: BankAccount) {
        AuthManager.shared.performActionWithAuthorization { [weak self] result in
            switch result {
            case .success(let password):
                self?.service.updateBankAccount(password, bankAccount: bankAccount) { error in
                    if let error = error {
                        self?.presenter.showError(error)
                        return
                    }
                    self?.dependencies.analytics.log(BankAccountAnalytics.finishEdition(self?.seller?.id))
                    self?.presenter.bankAccountUpdated()
                    NotificationCenter.default.post(name: Notification.Name.Settings.updated, object: nil)
                }
            case .canceled:
                self?.presenter.didCancelUpdate()
            default:
                break
            }
        }
    }
}

extension BankAccountViewModel: RegistrationAccountTypeProtocol {
    func didSelect(_ accountType: AccountType) {
        accountTypeSelected = accountType
    }
}

extension BankAccountViewModel: BankSelectFormViewControllerDelegate {
    func didSelectBank(_ bank: Bank) {
        self.bank = bank
        viewState = .editing
        presenter.didChangeBank(bank)
        
        if accountTypeSelected == .personal {
            presenter.didSelectPersonalAccount(bankAccount?.cpf ?? String())
        } else {
            presenter.didSelectCompanyAccount(bankAccount?.cnpj)
        }
    }
    
    func shouldHidePJAccountDisclaimer() -> Bool {
        companyType == .individual && accountTypeSelected == .personal
    }
}
