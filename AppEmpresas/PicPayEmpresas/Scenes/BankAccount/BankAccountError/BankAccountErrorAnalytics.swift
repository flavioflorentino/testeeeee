import AnalyticsModule

enum BankAccountErrorAnalytics: AnalyticsKeyProtocol {
    case home(errorType: String)
    case homeToCheck(errorType: String)
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    var params: [String: String] {
        switch self {
        case let .home(errorType), let .homeToCheck(errorType):
            return ["error_type": errorType]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .home:
            return AnalyticsEvent("Bank Account Home", properties: params, providers: providers)
        case .homeToCheck:
            return AnalyticsEvent("Bank Account Home - To Check", properties: params, providers: providers)
        }
    }
}
