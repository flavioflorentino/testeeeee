import SnapKit
import UI
import UIKit

extension BankAccountConfirmationView.Layout {
    enum BankImageView {
        static let size = CGSize(width: 40, height: 40)
    }
}

final class BankAccountConfirmationView: UIView {
    private typealias Localizable = Strings.BankAccountConfirmation
    
    fileprivate enum Layout { }

    private lazy var contentView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = CornerRadius.medium
        return view
    }()
    
    private lazy var bankImageView = UIImageView(image: Assets.icoBankPlaceholder.image)

    private lazy var bankNameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.numberOfLines, 1)
        return label
    }()

    private lazy var bankStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [bankImageView, bankNameLabel])
        stackView.alignment = .leading
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var branchLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.numberOfLines, 1)
        label.text = Localizable.AccountInfo.branch
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        return label
    }()
    
    private lazy var branchNumberLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.numberOfLines, 1)
        return label
    }()

    private lazy var branchStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [branchLabel, branchNumberLabel])
        stackView.alignment = .leading
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var accountLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.numberOfLines, 1)
        label.text = Localizable.AccountInfo.account
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        return label
    }()
    
    private lazy var accountNumberLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.numberOfLines, 1)
        return label
    }()

    private lazy var accountStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [accountLabel, accountNumberLabel])
        stackView.alignment = .leading
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var documentLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.numberOfLines, 1)
        label.text = Localizable.Document.company
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        return label
    }()
    
    private lazy var documentNumberLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.numberOfLines, 1)
        return label
    }()

    private lazy var documentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [documentLabel, documentNumberLabel])
        stackView.alignment = .leading
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var accountTypeLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.numberOfLines, 1)
        return label
    }()

    // MARK: - Inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        buildLayout()
    }
}

// MARK: - ViewConfiguration

extension BankAccountConfirmationView: ViewConfiguration {
    func setupConstraints() {
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        bankImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.BankImageView.size)
        }

        bankNameLabel.snp.makeConstraints {
            $0.centerY.equalTo(bankImageView)
        }

        bankStackView.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview().inset(Spacing.base02)
        }

        branchStackView.snp.makeConstraints {
            $0.leading.trailing.equalTo(bankStackView)
            $0.top.equalTo(bankStackView.snp.bottom).offset(Spacing.base02)
        }

        accountStackView.snp.makeConstraints {
            $0.leading.trailing.equalTo(bankStackView)
            $0.top.equalTo(branchStackView.snp.bottom).offset(Spacing.base01)
        }

        documentStackView.snp.makeConstraints {
            $0.leading.trailing.equalTo(bankStackView)
            $0.top.equalTo(accountStackView.snp.bottom).offset(Spacing.base01)
        }

        accountTypeLabel.snp.makeConstraints {
            $0.bottom.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(documentStackView.snp.bottom).offset(Spacing.base01)
        }
    }
    
    func buildViewHierarchy() {
        contentView.addSubview(bankStackView)
        contentView.addSubview(branchStackView)
        contentView.addSubview(accountStackView)
        contentView.addSubview(documentStackView)
        contentView.addSubview(accountTypeLabel)
        addSubview(contentView)
    }

    func configureViews() {
        contentView.backgroundColor = Colors.grayscale050.color
        backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - Public Methods

extension BankAccountConfirmationView {
    func setupAccount(_ accountItem: AccountConfirmationItem) {
        bankImageView.sd_setImage(with: URL(string: accountItem.imageUrl), placeholderImage: Assets.icoBankPlaceholder.image)
        bankNameLabel.text = accountItem.bank
        branchNumberLabel.text = accountItem.branch
        accountNumberLabel.text = accountItem.account
        documentNumberLabel.text = accountItem.formattedDoc
        accountTypeLabel.text = accountItem.type
        documentLabel.text = accountItem.documentType == .cpf ? Localizable.Document.personal : Localizable.Document.company
    }
}
