import SnapKit
import UI
import UIKit

protocol RegistrationTaxZeroViewDelegate: AnyObject {
    func moreInfoAction()
}

extension RegistrationTaxZero.Layout {
    enum Constraints {
        static let linkButtonTopOffset: CGFloat = 20
        static let dayBottomOffset: CGFloat = 20
    }
}

final class RegistrationTaxZero: UIView, ViewConfiguration {
    private typealias Localizable = Strings.RegistrationTaxZero
    fileprivate enum Layout {}
    private lazy var titleBar: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.Business.bluishGray400.color
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.subTitleTax
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.textColor = Palette.white.color
        return label
    }()
    
    private lazy var linkLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.typography, .bodySecondary())
            .with(\.textAlignment, .right)
        label.text = Localizable.labelLinkTitle
        return label
    }()
    
    private lazy var linkButton: UIButton = {
        let button = UIButton()
        let attText = NSAttributedString(
            string: Localizable.buttonLinkTitle,
            attributes: [
                .underlineStyle: NSUnderlineStyle.single.rawValue,
                .foregroundColor: Colors.branding600.color
            ]
        )
        button.setAttributedTitle(attText, for: .normal)
        button.titleLabel?.font = Typography.bodySecondary().font()
        button.addTarget(self, action: #selector(didTapMoreInfoAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var slider: UISlider = {
        let slider = UISlider()
        slider.isEnabled = false
        return slider
    }()
    
    private lazy var pinImage = UIImageView(image: Assets.iconPinTaxZero.image)
    
    private lazy var firstDay: UILabel = {
        let label = UILabel()
        label.text = Localizable.oneDay
        label.textColor = Palette.ppColorGrayscale400.color
        label.font = UIFont.systemFont(ofSize: 10, weight: .bold)
        return label
    }()
    
    private lazy var lastDay: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale400.color
        label.font = UIFont.systemFont(ofSize: 10, weight: .bold)
        label.text = Localizable.oneDay
        return label
    }()
    
    private lazy var firstPercent: UILabel = {
        let label = UILabel()
        label.text = Localizable.zeroPercent
        label.textColor = Palette.ppColorBranding300.color
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        return label
    }()
    
    private lazy var lastPercent: UILabel = {
        let label = UILabel()
        label.text = Localizable.zeroPercent
        label.textColor = Palette.ppColorBranding300.color
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        return label
    }()
    
    weak var delegate: RegistrationTaxZeroViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        titleBar.snp.makeConstraints {
            $0.top.leading.trailing.equalTo(self)
            $0.height.equalTo(Spacing.base06)
        }
        
        titleLabel.snp.makeConstraints {
            $0.center.equalTo(titleBar)
        }
        
        linkLabel.snp.makeConstraints {
            $0.centerY.equalTo(linkButton.snp.centerY)
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        linkButton.snp.makeConstraints {
            $0.top.equalTo(titleBar.snp.bottom).offset(Layout.Constraints.linkButtonTopOffset)
            $0.leading.equalTo(linkLabel.snp.trailing).offset(Spacing.base00)
            $0.trailing.equalTo(self).offset(-Spacing.base06)
        }
        
        firstDay.snp.makeConstraints {
            $0.leading.equalTo(self).offset(Spacing.base02)
            $0.bottom.equalTo(slider.snp.top).offset(-Layout.Constraints.dayBottomOffset)
        }
        
        lastDay.snp.makeConstraints {
            $0.trailing.equalTo(self).offset(-Spacing.base02)
            $0.bottom.equalTo(slider.snp.top).offset(-Layout.Constraints.dayBottomOffset)
        }
        
        slider.snp.makeConstraints {
            $0.leading.equalTo(self).offset(Spacing.base08)
            $0.trailing.equalTo(self).offset(-Spacing.base08)
            $0.centerY.equalTo(self).offset(Spacing.base07)
        }
        
        firstPercent.snp.makeConstraints {
            $0.top.equalTo(slider.snp.bottom).offset(Spacing.base03)
            $0.leading.equalTo(self).offset(Spacing.base02)
        }
        
        lastPercent.snp.makeConstraints {
            $0.top.equalTo(slider.snp.bottom).offset(Spacing.base03)
            $0.trailing.equalTo(self).offset(-Spacing.base02)
        }
        
        pinImage.snp.makeConstraints {
            $0.width.equalTo(Spacing.base06)
            $0.height.equalTo(Spacing.base07)
            $0.leading.equalTo(self).offset(Spacing.base07)
            $0.centerY.equalTo(self)
        }
    }
    
    func buildViewHierarchy() {
        addSubview(titleBar)
        titleBar.addSubview(titleLabel)
        addSubview(linkLabel)
        addSubview(linkButton)
        addSubview(firstDay)
        addSubview(slider)
        addSubview(lastDay)
        addSubview(firstPercent)
        addSubview(lastPercent)
        addSubview(pinImage)
    }
    
    @objc
    private func didTapMoreInfoAction() {
        delegate?.moreInfoAction()
    }
}
