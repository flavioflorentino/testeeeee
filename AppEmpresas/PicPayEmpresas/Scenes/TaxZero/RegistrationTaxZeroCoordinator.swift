import UIKit
import FeatureFlag

enum RegistrationTaxZeroAction {
    case terms
    case nextStepRegistration(model: RegistrationViewModel)
    case showPopUp(popup: PopupViewController)
}

protocol RegistrationTaxZeroCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationTaxZeroAction)
}

final class RegistrationTaxZeroCoordinator: RegistrationTaxZeroCoordinating {
    typealias Dependencies = HasFeatureManager
    weak var viewController: UIViewController?
    let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: RegistrationTaxZeroAction) {
        switch action {
        case .terms:
            let controller = ViewsManager.webViewController(
                url: FeatureManager.shared.text(.urlTerms),
                title: Strings.Default.navTermsOfUseTitle
            )
            
            viewController?.pushViewController(controller)
        case .nextStepRegistration(let model):
            guard dependencies.featureManager.isActive(.opsNewFlowPasswordScenePJ) else {
                push(to: RegistrationPasswordStepViewController.self, model: model)
                return
            }
            let controller = RegistrationPasswordFactory.make(model: model)
            viewController?.pushViewController(controller)
        case .showPopUp(let popup):
            viewController?.present(popup, animated: true)
        }
    }
}

private extension RegistrationTaxZeroCoordinator {
    func push<T: RegistrationStepViewController>(to: T.Type, model: RegistrationViewModel) {
        let controller: T = ViewsManager.instantiateViewController(.registration)
        controller.setup(model: model)
        viewController?.pushViewController(controller)
    }
}
