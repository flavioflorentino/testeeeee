import Core
import Foundation
import UI

protocol RegistrationTaxZeroPresenting: AnyObject {
    var viewController: RegistrationTaxZeroDisplay? { get set }
    func didNextStep(action: RegistrationTaxZeroAction)
    func showPopUp(model: RegistrationViewModel)
}

final class RegistrationTaxZeroPresenter: RegistrationTaxZeroPresenting {
    private let coordinator: RegistrationTaxZeroCoordinating
    weak var viewController: RegistrationTaxZeroDisplay?

    init(coordinator: RegistrationTaxZeroCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: RegistrationTaxZeroAction) {
        coordinator.perform(action: action)
    }
    
    func showPopUp(model: RegistrationViewModel) {
        let popup = PopupViewController()
        
        let configuration = AlertPopupConfiguration(
            titleButton: Strings.RegistrationTaxZero.popUpButtonOK,
            backgroundButton: Palette.ppColorBranding300.color,
            higlightBackgroundButton: Palette.ppColorBranding400.color,
            titleColorButton: Palette.white.color,
            highlightTitleColor: Palette.white.color
        )
        
        let alertPopup = AlertPopupController(with: configuration)
        alertPopup.popupTitle = Strings.RegistrationTaxZero.popUpTitle
        alertPopup.popupText = Strings.RegistrationTaxZero.popUpDescription
        alertPopup.buttonAction = { [weak self] in
            self?.coordinator.perform(action: .nextStepRegistration(model: model))
        }
        
        popup.contentController = alertPopup
        coordinator.perform(action: .showPopUp(popup: popup))
    }
}
