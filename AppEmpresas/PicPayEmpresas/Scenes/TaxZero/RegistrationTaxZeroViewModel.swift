import Foundation

protocol RegistrationTaxZeroViewModelInputs: AnyObject {
    func showMoreInfo()
    func okAction()
}

final class RegistrationTaxZeroViewModel {
    private let presenter: RegistrationTaxZeroPresenting
    private let model: RegistrationViewModel

    init(presenter: RegistrationTaxZeroPresenting, model: RegistrationViewModel) {
        self.presenter = presenter
        self.model = model
    }
}

extension RegistrationTaxZeroViewModel: RegistrationTaxZeroViewModelInputs {
    func okAction() {
        model.account.days = 1
        model.feeDefaultDay = "1"
        presenter.showPopUp(model: model)
    }
    
    func showMoreInfo() {
        presenter.didNextStep(action: .terms)
    }
}
