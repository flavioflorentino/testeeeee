import SnapKit
import UI
import UIKit

extension RegistrationTaxZeroViewController.Layout {
    enum Size {
        static let shadowHeightOffset = 2
        static let sliderViewHeight: CGFloat = 288
        static let buttonRadius: CGFloat = 24
    }
}

final class RegistrationTaxZeroViewController: ViewController<RegistrationTaxZeroViewModelInputs, UIView> {
    private typealias Localizable = Strings.RegistrationTaxZero
    fileprivate enum Layout {}

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.titleTax
        label.textAlignment = .center
        label.font = Typography.title(.small).font()
        label.textColor = Colors.black.color
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.descriptionTax
        label.textAlignment = .center
        label.numberOfLines = 4
        label.font = Typography.caption().font()
        label.textColor = Colors.grayscale600.color
        return label
    }()
    
    private lazy var shadowView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowColor = Palette.ppColorGrayscale400.color.cgColor
        view.layer.shadowRadius = CornerRadius.medium
        view.layer.shadowOffset = CGSize(width: 0, height: Layout.Size.shadowHeightOffset)
        view.layer.shadowOpacity = 1
        view.layer.cornerRadius = CornerRadius.medium
        return view
    }()
    
    private lazy var sliderView: RegistrationTaxZero = {
        let view = RegistrationTaxZero()
        view.delegate = self
        view.layer.cornerRadius = CornerRadius.medium
        view.layer.masksToBounds = true
        return view
    }()
    
    private lazy var okButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(Localizable.buttonOkTitle, for: .normal)
        button.normalBackgrounColor = Palette.Business.positive300.color
        button.highlightedBackgrounColor = Palette.Business.positive300.color.withAlphaComponent(0.6)
        button.normalTitleColor = Palette.white.color
        button.highlightedTitleColor = Palette.white.color
        button.cornerRadius = Layout.Size.buttonRadius
        button.addTarget(self, action: #selector(didTapButtonAction), for: .touchUpInside)
        return button
    }()
 
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(shadowView)
        view.addSubview(sliderView)
        view.addSubview(okButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.grayscale050.color
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        shadowView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.Size.sliderViewHeight)
        }
        
        sliderView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.Size.sliderViewHeight)
        }
        
        okButton.snp.makeConstraints {
            $0.top.equalTo(sliderView.snp.bottom).offset(Spacing.base04)
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().inset(Spacing.base03)
            $0.height.equalTo(Spacing.base06)
        }
    }
    
    @objc
    private func didTapButtonAction() {
        viewModel.okAction()
    }
}

// MARK: View Model Outputs

extension RegistrationTaxZeroViewController: RegistrationTaxZeroDisplay {}

// MARK: - RegistrationTaxZeroViewDelegate

extension RegistrationTaxZeroViewController: RegistrationTaxZeroViewDelegate {
    func moreInfoAction() {
        viewModel.showMoreInfo()
    }
}
