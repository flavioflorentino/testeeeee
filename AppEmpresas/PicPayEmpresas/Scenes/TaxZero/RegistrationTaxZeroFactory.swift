import Foundation

enum RegistrationTaxZeroFactory {
    static func make(model: RegistrationViewModel) -> RegistrationTaxZeroViewController {
        let container = DependencyContainer()
        let coordinator: RegistrationTaxZeroCoordinating = RegistrationTaxZeroCoordinator(dependencies: container)
        let presenter: RegistrationTaxZeroPresenting = RegistrationTaxZeroPresenter(coordinator: coordinator)
        let viewModel = RegistrationTaxZeroViewModel(presenter: presenter, model: model)
        let viewController = RegistrationTaxZeroViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
