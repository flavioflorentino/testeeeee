import SnapKit
import UI
import UIKit

protocol AccountViewDelegate: AnyObject {
    func didTapChangeAccountButton()
}

extension PicPayAccountView.Layout {
    enum LogoImageView {
        static let size = CGSize(width: 32, height: 32)
        static let leading: CGFloat = 20
    }

    enum UsernameLabel {
        static let top: CGFloat = 14
        static let leading: CGFloat = 10
    }

    enum DescriptionLabel {
        static let leading: CGFloat = 10
    }

    enum ChangeButton {
        static let top: CGFloat = 12
        static let size = CGSize(width: 80, height: 40)
        static let cornerRadius: CGFloat = 20
    }

    enum LineView {
        static let height: CGFloat = 1
    }

    enum Fonts {
        static let smallRegular = UIFont.systemFont(ofSize: 14, weight: .regular)
        static let smallMedium = UIFont.systemFont(ofSize: 14, weight: .bold)
    }
}

final class PicPayAccountView: UIView {
    // MARK: - Private Properties

    fileprivate enum Layout { }

    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView(image: Assets.picpaySmall.image)
        imageView.layer.cornerRadius = CornerRadius.strong
        imageView.clipsToBounds = true
        return imageView
    }()

    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.smallMedium
        label.textColor = Palette.ppColorGrayscale600.color
        label.textAlignment = .left
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Default.picpayTransfer
        label.font = Layout.Fonts.smallRegular
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .left
        return label
    }()

    private lazy var changeAccountButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(Strings.Default.change, for: .normal)
        button.titleLabel?.font = Layout.Fonts.smallMedium
        button.normalTitleColor = Palette.ppColorBranding300.color
        button.normalBackgrounColor = Palette.white.color
        button.cornerRadius = Layout.ChangeButton.cornerRadius
        button.isHidden = true
        button.addTarget(self, action: #selector(didTapChangeButton), for: .touchUpInside)
        return button
    }()

    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale300.color
        return view
    }()

    weak var delegate: AccountViewDelegate?

    // MARK: - Inits

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Internal Methods

extension PicPayAccountView {
    func setupAccount(_ accountItem: PicpayAccountModel) {
        usernameLabel.text = accountItem.username
    }
}

// MARK: - Private Methods

private extension PicPayAccountView {
    func setup() {
        buildLayout()
    }
    
    @objc
    func didTapChangeButton() {
        delegate?.didTapChangeAccountButton()
    }
}

// MARK: - ViewConfiguration

extension PicPayAccountView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(logoImageView)
        addSubview(usernameLabel)
        addSubview(descriptionLabel)
        addSubview(changeAccountButton)
        addSubview(lineView)
    }

    func setupConstraints() {
        logoImageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Layout.LogoImageView.leading)
            $0.size.equalTo(Layout.LogoImageView.size)
        }
        
        usernameLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Layout.UsernameLabel.top)
            $0.leading.equalTo(logoImageView.snp.trailing).offset(Layout.UsernameLabel.leading)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(usernameLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.equalTo(logoImageView.snp.trailing).offset(Layout.DescriptionLabel.leading)
        }
        
        lineView.snp.makeConstraints {
            $0.top.equalTo(logoImageView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview()
            $0.height.equalTo(Layout.LineView.height)
        }
        
        changeAccountButton.snp.makeConstraints {
            $0.size.equalTo(Layout.ChangeButton.size)
            $0.top.equalToSuperview().offset(Layout.ChangeButton.top)
            $0.leading.equalTo(usernameLabel.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }

    func configureViews() {
        backgroundColor = Palette.white.color
    }
}
