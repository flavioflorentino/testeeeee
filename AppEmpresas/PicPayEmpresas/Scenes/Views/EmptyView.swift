import UI
import UIKit

extension EmptyView.Layout {
    enum Size {
        static let imageWidth: CGFloat = 185
    }
    
    enum Constraints {
        static let centerOffset: CGFloat = 100
    }
}

struct EmptyViewConfiguration {
    let image: UIImage
    let title: String
    let info: String
    let refreshButtonTitle: String?
    let buttonAction: (() -> Void)?
}

final class EmptyView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private var completionAction: (() -> Void)?
    
    private lazy var imageView = UIImageView()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.grayscale700.color
        label.font = Typography.bodyPrimary().font()
        label.textAlignment = .center
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.grayscale500.color
        label.font = Typography.bodySecondary().font()
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var refreshButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.addTarget(self, action: #selector(refreshAction), for: .touchUpInside)
        return button
    }()
    
    // MARK: - life cycle
    
    init(configuration: EmptyViewConfiguration) {
        super.init(frame: .zero)
        setup(configuration: configuration)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = Colors.white.color
    }
    
    func setupConstraints() {
        imageView.layout {
            $0.centerY == centerYAnchor - Layout.Constraints.centerOffset
            $0.centerX == centerXAnchor
            $0.width == Layout.Size.imageWidth
        }
                
        titleLabel.layout {
            $0.top == imageView.bottomAnchor + Spacing.base04
            $0.centerX == centerXAnchor
        }

        infoLabel.layout {
            $0.top == titleLabel.bottomAnchor + Spacing.base01
            $0.centerX == centerXAnchor
        }

        refreshButton.layout {
            $0.top == infoLabel.bottomAnchor + Spacing.base04
            $0.centerX == centerXAnchor
        }
    }
    
    func buildViewHierarchy() {
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(infoLabel)
        addSubview(refreshButton)
    }
    
    // MARK: - private functions
    
    private func setup(configuration: EmptyViewConfiguration) {
        imageView.image = configuration.image
        titleLabel.text = configuration.title
        infoLabel.text = configuration.info
        
        guard let refreshButtonTitle = configuration.refreshButtonTitle else {
            refreshButton.isHidden = true
            return
        }
        
        refreshButton.isHidden = false
        refreshButton.setTitle(refreshButtonTitle, for: .normal)
        completionAction = configuration.buttonAction
    }
    
    @objc
    private func refreshAction() {
        completionAction?()
    }
}
