import Foundation

protocol AlertModalDisplay: AnyObject {
    func presentImageInfo(_ imageInfo: AlertModalInfoImage)
    func presentTitle(_ title: String)
    func presentSubtitle(_ subtitle: String)
    func presentButtonTitle(_ buttonTitle: String)
    func presentSecondaryButtonTitle(_ secondaryButtonTitle: String)
}
