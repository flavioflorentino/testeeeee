import Core
import Foundation

protocol AlertModalPresenting: AnyObject {
    var viewController: AlertModalDisplay? { get set }
    func displayModalInfo(modalInfo: AlertModalInfo)
}

final class AlertModalPresenter: AlertModalPresenting {
    weak var viewController: AlertModalDisplay?
    
    func displayModalInfo(modalInfo: AlertModalInfo) {
        if let imageInfo = modalInfo.imageInfo {
            viewController?.presentImageInfo(imageInfo)
        }
        
        if let title = modalInfo.title {
            viewController?.presentTitle(title)
        }
        
        if let subtitle = modalInfo.subtitle {
            viewController?.presentSubtitle(subtitle)
        }
        
        if let buttonTitle = modalInfo.buttonTitle {
            viewController?.presentButtonTitle(buttonTitle)
        }
        
        if let secondaryButtonTitle = modalInfo.secondaryButtonTitle {
            viewController?.presentSecondaryButtonTitle(secondaryButtonTitle)
        }
    }
}
