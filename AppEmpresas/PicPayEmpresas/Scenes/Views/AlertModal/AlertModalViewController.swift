import SnapKit
import UI
import UIKit

private extension AlertModalViewController.Layout {
    enum ConfirmButton {
        static let height: CGFloat = 48
    }
    
    enum SecondaryButton {
        static let height: CGFloat = 48
    }
}

final class AlertModalViewController: ViewController<AlertModalViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    // MARK: - Private Properties
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        stackView.alignment = .center
        return stackView
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.black.color)
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        return stackView
    }()
    
    private lazy var button: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTouchButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var secondaryButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTouchSecondaryButton), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Internal Properties
    
    var touchButtonAction: ((AlertModalViewController) -> Void)?
    var touchSecondaryButtonAction: ((AlertModalViewController) -> Void)?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadModalInfo()
    }
    
    override func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base03)
            $0.leading.trailing.equalToSuperview()
        }
    }
    
    override func buildViewHierarchy() {
        view.addSubview(stackView)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - Private Methods

private extension AlertModalViewController {
    @objc
    func didTouchButton() {
        touchButtonAction?(self)
    }
    
    @objc
    func didTouchSecondaryButton() {
        touchSecondaryButtonAction?(self)
    }
}

// MARK: - View Model Outputs

extension AlertModalViewController: AlertModalDisplay {
    func presentImageInfo(_ imageInfo: AlertModalInfoImage) {
        stackView.addArrangedSubview(imageView)
        imageView.image = imageInfo.image
        imageView.snp.makeConstraints {
            $0.size.equalTo(imageInfo.size)
        }
    }
    
    func presentTitle(_ title: String) {
        if !stackView.arrangedSubviews.contains(textStackView) {
            stackView.addArrangedSubview(textStackView)
        }
        
        textStackView.addArrangedSubview(titleLabel)
        titleLabel.text = title
        titleLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base04)
        }
    }
    
    func presentSubtitle(_ subtitle: String) {
        if !stackView.arrangedSubviews.contains(textStackView) {
            stackView.addArrangedSubview(textStackView)
        }
        
        textStackView.addArrangedSubview(subtitleLabel)
        subtitleLabel.text = subtitle
        subtitleLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base04)
        }
    }
    
    func presentButtonTitle(_ buttonTitle: String) {
        if !stackView.arrangedSubviews.contains(buttonsStackView) {
            stackView.addArrangedSubview(buttonsStackView)
        }
        
        buttonsStackView.addArrangedSubview(button)
        button.setTitle(buttonTitle, for: .normal)
        button.snp.makeConstraints {
            $0.leading.equalTo(stackView).offset(Spacing.base04)
            $0.height.equalTo(Layout.ConfirmButton.height)
        }
    }
    
    func presentSecondaryButtonTitle(_ secondaryButtonTitle: String) {
        if !stackView.arrangedSubviews.contains(buttonsStackView) {
            stackView.addArrangedSubview(buttonsStackView)
        }
        
        buttonsStackView.addArrangedSubview(secondaryButton)
        secondaryButton.setTitle(secondaryButtonTitle, for: .normal)
        secondaryButton.buttonStyle(LinkButtonStyle())
        secondaryButton.snp.makeConstraints {
            $0.leading.equalTo(stackView).offset(Spacing.base04)
            $0.height.equalTo(Layout.SecondaryButton.height)
        }
    }
}
