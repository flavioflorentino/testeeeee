import Foundation

enum AlertModalFactory {
    static func make(alertModalInfo: AlertModalInfo) -> AlertModalViewController {
        let presenter: AlertModalPresenting = AlertModalPresenter()
        let viewModel = AlertModalViewModel(presenter: presenter, modalInfo: alertModalInfo)
        let viewController = AlertModalViewController(viewModel: viewModel)
        
        presenter.viewController = viewController

        return viewController
    }
}
