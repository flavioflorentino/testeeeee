import Foundation
import UIKit

struct AlertModalInfo: Equatable {
    let imageInfo: AlertModalInfoImage?
    let title: String?
    let subtitle: String?
    let buttonTitle: String?
    let secondaryButtonTitle: String?
    
    init(
        imageInfo: AlertModalInfoImage? = nil,
        title: String? = nil,
        subtitle: String? = nil,
        buttonTitle: String? = nil,
        secondaryButtonTitle: String? = nil
    ) {
        self.imageInfo = imageInfo
        self.title = title
        self.subtitle = subtitle
        self.buttonTitle = buttonTitle
        self.secondaryButtonTitle = secondaryButtonTitle
    }
}

struct AlertModalInfoImage: Equatable {
    let image: UIImage
    let size: CGSize
    
    init(image: UIImage, size: CGSize = CGSize(width: 80, height: 80)) {
        self.image = image
        self.size = size
    }
}

protocol AlertModalViewModelInputs: AnyObject {
    func loadModalInfo()
}

final class AlertModalViewModel {
    // MARK: - Private Properties
    
    private let presenter: AlertModalPresenting
    private let modalInfo: AlertModalInfo
    
    // MARK: Inits

    init(presenter: AlertModalPresenting, modalInfo: AlertModalInfo) {
        self.presenter = presenter
        self.modalInfo = modalInfo
    }
}

// MARK: - ViewModelInputs

extension AlertModalViewModel: AlertModalViewModelInputs {
    func loadModalInfo() {
        presenter.displayModalInfo(modalInfo: modalInfo)
    }
}
