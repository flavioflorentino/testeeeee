import SnapKit
import UI
import UIKit

extension BankAccountView.Layout {
    enum BankNameLabel {
        static let top: CGFloat = 17
        static let leading: CGFloat = 10
    }
    
    enum BankImage {
        static let size: CGFloat = 32
        static let top: CGFloat = 16
        static let leading: CGFloat = 20
    }
    
    enum BranchAccountStack {
        static let spacing: CGFloat = 4
        static let top: CGFloat = 4
        static let leading: CGFloat = 10
    }
    
    enum ChangeButton {
        static let top: CGFloat = 12
        static let size = CGSize(width: 80, height: 40)
        static let cornerRadius: CGFloat = 20
    }
    
    enum LineView {
        static let top: CGFloat = 16
        static let height: CGFloat = 1
    }
    
    enum Colors {
        static let label = Palette.ppColorGrayscale500.color
        static let value = Palette.ppColorGrayscale600.color
    }
    
    enum Fonts {
        static let label = UIFont.systemFont(ofSize: 14, weight: .regular)
        static let caption = UIFont.systemFont(ofSize: 14, weight: .medium)
    }
}

final class BankAccountView: UIView {
    // MARK: - Private Properties
    
    fileprivate enum Layout { }
    
    private lazy var bankImageView = UIImageView(image: Assets.icoBankPlaceholder.image)
    
    private lazy var bankNameLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.caption
        label.textColor = Layout.Colors.label
        label.textAlignment = .left
        return label
    }()
    
    private lazy var branchLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Default.branch + ":"
        label.font = Layout.Fonts.label
        label.textColor = Layout.Colors.label
        label.textAlignment = .left
        return label
    }()
    
    private lazy var branchNumberLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.caption
        label.textColor = Layout.Colors.label
        label.textAlignment = .left
        return label
    }()
    
    private lazy var accountLabel: UILabel = {
        let label = UILabel()
        label.text = "| " + Strings.Default.account + ":"
        label.font = Layout.Fonts.label
        label.textColor = Layout.Colors.label
        label.textAlignment = .left
        return label
    }()
    
    private lazy var accountNumberLabel: UILabel = {
        let label = UILabel()
        label.setContentHuggingPriority(.defaultLow, for: .horizontal)
        label.font = Layout.Fonts.caption
        label.textColor = Layout.Colors.label
        label.textAlignment = .left
        return label
    }()
    
    private lazy var branchAccountStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fillProportionally
        stack.spacing = Layout.BranchAccountStack.spacing
        stack.addArrangedSubview(branchLabel)
        stack.addArrangedSubview(branchNumberLabel)
        stack.addArrangedSubview(accountLabel)
        stack.addArrangedSubview(accountNumberLabel)
        return stack
    }()
    
    private lazy var changeAccountButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(Strings.Default.change, for: .normal)
        button.titleLabel?.font = Layout.Fonts.caption
        button.normalTitleColor = Palette.ppColorBranding300.color
        button.normalBackgrounColor = Palette.white.color
        button.cornerRadius = Layout.ChangeButton.cornerRadius
        button.isHidden = true
        button.addTarget(self, action: #selector(didTapChangeButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale300.color
        return view
    }()
    
    weak var delegate: AccountViewDelegate?
    
    // MARK: - Inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
}

// MARK: - Public Methods

extension BankAccountView {
    func setupAccount(_ accountItem: WithdrawAccountItem, hideChangeButton hide: Bool = false) {
        bankNameLabel.text = accountItem.bankName
        branchNumberLabel.text = accountItem.branch
        accountNumberLabel.text = accountItem.account
        bankImageView.sd_setImage(with: URL(string: accountItem.imageUrl), placeholderImage: Assets.icoBankPlaceholder.image)
    }
}

// MARK: - Private Methods

private extension BankAccountView {
    func setup() {
        buildLayout()
    }
}

@objc
private extension BankAccountView {
    func didTapChangeButton() {
        delegate?.didTapChangeAccountButton()
    }
}

// MARK: - ViewConfiguration

extension BankAccountView: ViewConfiguration {
    func setupConstraints() {
        bankImageView.snp.makeConstraints {
            $0.top.equalTo(self).offset(Layout.BankImage.top)
            $0.leading.equalTo(self).offset(Layout.BankImage.leading)
            $0.size.equalTo(Layout.BankImage.size)
        }
        
        bankNameLabel.snp.makeConstraints {
            $0.top.equalTo(self).offset(Layout.BankNameLabel.top)
            $0.leading.equalTo(bankImageView.snp.trailing).offset(Layout.BankNameLabel.leading)
        }
        
        branchAccountStack.snp.makeConstraints {
            $0.top.equalTo(bankNameLabel.snp.bottom).offset(Layout.BranchAccountStack.top)
            $0.leading.equalTo(bankImageView.snp.trailing).offset(Layout.BranchAccountStack.leading)
        }
        
        lineView.snp.makeConstraints {
            $0.top.equalTo(bankImageView.snp.bottom).offset(Layout.LineView.top)
            $0.leading.trailing.bottom.equalTo(self)
            $0.height.equalTo(Layout.LineView.height)
        }
        
        changeAccountButton.snp.makeConstraints {
            $0.size.equalTo(Layout.ChangeButton.size)
            $0.top.equalTo(self).offset(Layout.ChangeButton.top)
            $0.leading.equalTo(bankNameLabel.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalTo(self).offset(-Spacing.base02)
        }
    }
    
    func buildViewHierarchy() {
        addSubview(bankImageView)
        addSubview(bankNameLabel)
        addSubview(branchAccountStack)
        addSubview(changeAccountButton)
        addSubview(lineView)
    }
    
    func configureViews() {
        backgroundColor = Palette.white.color
    }
}
