import Foundation
import InputMask
import UI

extension BusinessTextField.Layout {
    enum Measures {
        static let textfieldHeight: CGFloat = 24
        static let underlineHeight: CGFloat = 1
    }
}

final class BusinessTextField: UIStackView {
    fileprivate enum Layout {}

    var isRequired: Bool = false
    var isValid: Bool = true
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale300())
        return label
    }()
    
    private lazy var textField: UITextField = {
        let textField = UITextField()
        textField.textColor = Colors.grayscale700.color
        textField.font = Typography.bodyPrimary().font()
        textField.layer.borderWidth = Border.none
        textField.borderStyle = .none
        textField.layer.addSublayer(underlineLayer)
        
        return textField
    }()
    
    private lazy var errorLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .critical600())
        label.isHidden = true
        return label
    }()
    
    private lazy var underlineLayer: CALayer = {
        let underlineLayer = CALayer()
        underlineLayer.backgroundColor = Colors.branding400.color.cgColor
        return underlineLayer
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        axis = .vertical
        spacing = Spacing.base00
        buildLayout()
    }

    @available(*, unavailable)
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let frame = CGRect(
            x: 0,
            y: Layout.Measures.textfieldHeight - Layout.Measures.underlineHeight,
            width: textField.frame.width,
            height: Layout.Measures.underlineHeight
        )
        underlineLayer.frame = frame
    }
    
    func set(
        title: String,
        textFieldDelegate: UITextFieldDelegate,
        keyboardType: UIKeyboardType = .default,
        isRequired: Bool = false
    ) {
        titleLabel.text = title
        textField.keyboardType = keyboardType
        textField.delegate = textFieldDelegate
        self.isRequired = isRequired
    }
    
    func updateStatus(
        validField: Bool,
        text: String = Strings.MaterialSolicitation.addressCardRequiredField
    ) {
        underlineLayer.backgroundColor = validField ? Colors.branding400.color.cgColor : Colors.critical600.color.cgColor
        errorLabel.isHidden = validField
        isValid = validField
        errorLabel.text = text
    }
    
    func updateTextField(with text: String?, mask: String? = nil) {
        if let mask = mask, let text = text {
            textField.text = maskString(input: text, mask: mask)
        } else {
            textField.text = text
        }
    }
    
    func equalsTextField(to textField: UITextField) -> Bool {
        self.textField === textField
    }
    
    func textFieldValue() -> String? {
        textField.text
    }
    
    private func maskString(input: String, mask: String) -> String {
        do {
            let mask: Mask = try Mask(format: mask)
            let result: Mask.Result = mask.apply(
                toText: CaretString(string: input),
                autocomplete: true
            )
            return result.formattedText.string
        } catch {
            return ""
        }
    }
}

extension BusinessTextField: ViewConfiguration {
    func buildViewHierarchy() {
        addArrangedSubview(titleLabel)
        addArrangedSubview(textField)
        addArrangedSubview(errorLabel)
    }

    func setupConstraints() {
        textField.snp.makeConstraints {
            $0.height.equalTo(Layout.Measures.textfieldHeight)
        }
    }
}
