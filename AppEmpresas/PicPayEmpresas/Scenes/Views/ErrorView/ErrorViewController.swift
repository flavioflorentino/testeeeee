import UI

protocol ErrorViewDelegate: AnyObject {
    func didTouchConfirmButton()
}

fileprivate extension ErrorViewController.Layout {
    enum ImageView {
        static let size = CGSize(width: 72, height: 72)
    }
}

final class ErrorViewController: UIViewController {
    typealias Localizable = Strings.ErrorView
    
    fileprivate enum Layout { }
    
    // MARK: - Public Properties
    
    weak var delegate: ErrorViewDelegate?
    
    // MARK: - Private Properties
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var imageView = UIImageView(image: Assets.Emoji.iconBad.image)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        label.text = Localizable.anErrorOcurred
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        label.text = Localizable.weReSorry
        return label
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTouchConfirmButton), for: .touchUpInside)
        button.setTitle(Localizable.ok, for: .normal)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
    }
}

// MARK: - Private Methods

private extension ErrorViewController {
    @objc
    func didTouchConfirmButton() {
        delegate?.didTouchConfirmButton()
    }
}

// MARK: - Public Methods

extension ErrorViewController {
    func setErrorView(
        title: String = Localizable.anErrorOcurred,
        message: String = Localizable.weReSorry,
        image: UIImage = Assets.Emoji.iconBad.image,
        buttonTitle: String = Localizable.ok
    ) {
        imageView.image = image
        titleLabel.text = title
        descriptionLabel.text = message
        confirmButton.setTitle(buttonTitle, for: .normal)
    }
    
    func setTitleStyle(_ style: Typography.Style.Title) {
        titleLabel.labelStyle(TitleLabelStyle(type: style))
    }
}

// MARK: - ViewConfiguration

extension ErrorViewController: ViewConfiguration {
    func setupConstraints() {
        contentView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.bottom.equalToSuperview().offset(-Spacing.base02)
        }
        
        imageView.snp.makeConstraints {
            $0.height.equalTo(Layout.ImageView.size.height)
            $0.width.equalTo(Layout.ImageView.size.width)
            $0.top.equalTo(contentView).offset(Spacing.base04)
            $0.centerX.equalTo(contentView)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base04)
            $0.leading.equalTo(contentView).offset(Spacing.base02)
            $0.trailing.equalTo(contentView).offset(-Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalTo(contentView).offset(Spacing.base02)
            $0.trailing.equalTo(contentView).offset(-Spacing.base02)
            $0.bottom.lessThanOrEqualTo(confirmButton).offset(-Spacing.base02)
        }
        
        confirmButton.snp.makeConstraints {
            $0.leading.equalTo(contentView).offset(Spacing.base02)
            $0.trailing.equalTo(contentView).offset(-Spacing.base02)
            $0.bottom.equalTo(contentView).offset(-Spacing.base03)
        }
    }
    
    func buildViewHierarchy() {
        view.addSubview(contentView)
        
        contentView.addSubview(imageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(confirmButton)
    }
    
    func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}
