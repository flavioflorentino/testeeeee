import Core
import FeatureFlag
import protocol InAppReview.InAppReviewServicing

enum AccountSellerType {
    case payment
    case liquidation
}

final class InAppReviewService: InAppReviewServicing {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    private let sellerType: AccountSellerType
    
    init(dependencies: Dependencies, sellerType: AccountSellerType) {
        self.dependencies = dependencies
        self.sellerType = sellerType
    }
    
    private func prepareTags(_ stringTags: String) -> [String] {
        let tags = stringTags
            .chopPrefix()
            .chopSuffix()
            .replacingOccurrences(of: "\"", with: "")
            .components(separatedBy: ",")
        return tags
    }
    
    func getTags() -> [String] {
        let tags: String
        switch sellerType {
        case .payment:
            tags = dependencies.featureManager.text(.inAppReviewTagsPayment)
        case .liquidation:
            tags = dependencies.featureManager.text(.inAppReviewTagsLiquidation)
        }
        return prepareTags(tags)
    }
    
    func sendSelected(tags: [String], rating: Int) {
        // TODO: Enviar avaliação no proximo PR
    }
}
