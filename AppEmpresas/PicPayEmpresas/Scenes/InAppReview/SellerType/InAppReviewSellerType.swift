enum InAppReviewSellerType {
    static func isPaymentAccount(seller: Seller?) -> Bool {
        guard let seller = seller else { return false }
        return
            seller.digitalAccount &&
            seller.biometry
    }
}
