import Foundation
import CustomerSupport
import LegacyPJ
import PIX
import FeatureFlag

enum RouterApp: String {
    typealias Dependecies = HasAppCoordinator & HasFeatureManager
    private var dependencies: Dependecies { DependencyContainer() }

    case home = ""
    case transactions = "transactions"
    case sale = "sale"
    case extract = "extract"
    case settings = "settings"
    case history = "history"
    case notifications = "notifications"
    case operators = "settings/operators"
    case installments = "settings/installments"
    case businessConditions = "settings/businessConditions"
    case automaticWithdrawal = "settings/automaticWithdrawal"
    case whatsApp = "settings/whatsapp"
    case introWhatsApp = "settings/introWhatsapp"
    case boardActivation = "boardActivation"
    case webViewAction = "webview"
    case pix = "pix"
    case helpcenter = "helpcenter"
    case printQR = "settings/printqr"
    case requestMaterial = "settings/requestMaterial"
    
    // swiftlint:disable cyclomatic_complexity function_body_length
    func match(navigation: UINavigationController?, parameters: String? = nil, url: URL? = nil) {
        guard AuthManager.shared.isAuthenticated else {
            if dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
                dependencies.appCoordinator.displayWelcome()
            } else {
                AppManager.shared.presentUnloggedViewController()
            }
            return
        }
        
        switch self {
        case .home:
            if dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
                dependencies.appCoordinator.displayRoot()
                break
            }

            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                let controller = AppManager.shared.initialRootViewController()
                appDelegate.window?.rootViewController = controller
            }
        case .transactions:
            loadMainViewWith(label: 0, controller: navigation?.topViewController)
        case .extract:
            loadMainViewWith(label: 1, controller: navigation?.topViewController)
        case .sale:
            loadMainViewWith(label: 2, controller: navigation?.topViewController)
        case .notifications:
            loadMainViewWith(label: 3, controller: navigation?.topViewController)
        case .settings:
            loadMainViewWith(label: 4, controller: navigation?.topViewController)
        case .history:
            let historyController: SummaryViewController = ViewsManager.instantiateViewController(.transaction)
            push(navigation: navigation, controller: historyController)
        case .operators:
            let operatorController: OperatorTableViewController = ViewsManager.instantiateViewController(.settings)
            push(navigation: navigation, controller: operatorController)
        case .installments:
            let installmentsController: InstallmentsTableViewController = ViewsManager.instantiateViewController(.settings)
            push(navigation: navigation, controller: installmentsController)
        case .businessConditions:
            let businessController: DayFormViewController = ViewsManager.instantiateViewController(.settings)
            let model = UpdateDayFormViewModel()
            businessController.setup(model: model)
            push(navigation: navigation, controller: businessController)
        case .automaticWithdrawal:
            let controller = AutomaticWithdrawalFactory.make()
            push(navigation: navigation, controller: controller)
        case .whatsApp:
            let controller = RegisterWhatsAppNumberFactory.make()
            push(navigation: navigation, controller: controller)
        case .introWhatsApp:
            let controller = IntroWhatsAppFactory.make()
            push(navigation: navigation, controller: controller)
        case .boardActivation:
            if FeatureManager.shared.isActive(.allowApplyMaterial) {
                let controller = DeliveryDetailFactory.make(state: .boardActivation)
                let navigationController = UINavigationController(rootViewController: controller)
                navigation?.present(navigationController, animated: true)
            } else {
                loadMainViewWith(label: 0, controller: navigation?.topViewController)
            }
        case .webViewAction:
            guard let parameters = parameters else { return }
            let controller = ViewsManager.webViewController(url: parameters)
            push(navigation: navigation, controller: controller)
        case .pix:
            switchToTransactionsIfNotNotifications(with: navigation)
            routeToPix(with: navigation, and: url)
        case .helpcenter:
            guard let url = url, let navigation = navigation else { return }
            let deeplinkResolver = HelpcenterDeeplinkResolver()
            _ = deeplinkResolver.open(url: url, isAuthenticated: true, from: navigation)
        case .printQR:
            let controller = ChooseQRCodePrintFactory.make()
            push(navigation: navigation, controller: controller)
        case .requestMaterial:
            let controller = MaterialFlowControlFactory.make()
            let navigationController = UINavigationController(rootViewController: controller)
            navigation?.present(navigationController, animated: true)
        }
    }
    // swiftlint:enable cyclomatic_complexity function_body_length
    
    private func loadMainViewWith(label index: Int, controller: UIViewController?) {
        guard AuthManager.shared.isAuthenticated else { return }
        
        var mainController: MainTabBarController?
        
        if let controller = controller as? MainTabBarController {
            mainController = controller
        } else {
            mainController = getMainTabControllerOnRoot()
        }
        
        mainController?.selectedIndex = index
    }
    
    private func push(navigation: UINavigationController?, controller: UIViewController) {
        navigation?.pushViewController(controller, animated: true)
        navigation?.setNavigationBarHidden(false, animated: true)
    }
    
    private func getMainTabControllerOnRoot() -> MainTabBarController? {
        let delegate = UIApplication.shared.delegate
        if let rootNav = delegate?.window??.rootViewController as? UINavigationController {
            return rootNav.topViewController as? MainTabBarController
        }
        
        return delegate?.window??.rootViewController as? MainTabBarController
    }

    private func switchToTransactionsIfNotNotifications(with navigation: UINavigationController?) {
        if let tabBarController = navigation?.topViewController as? UITabBarController,
           let navigationController = tabBarController.selectedViewController as? UINavigationController,
           navigationController.topViewController is NotificationCenterViewController == false {
            loadMainViewWith(label: 0, controller: navigation?.topViewController)
        }
    }

    private func routeToPix(with navigation: UINavigationController?, and url: URL?) {
        let api = ApiSeller()
        api.data { result in
            switch result {
            case .success(let seller):
                let dependencies: HasFeatureManager = DependencyContainer()
                guard seller.biometry == true,
                      seller.digitalAccount == true,
                      dependencies.featureManager.isActive(.releasePIXPresenting) else {
                    return
                }

                TrackingManager.setupMixpanel(sellerID: seller.id)

                if let mainTabBar = navigation?.topViewController as? MainTabBarController,
                   let controller = mainTabBar.selectedViewController as? UINavigationController,
                   let url = url {
                    _ = PIXBizDeeplinkResolver(dependencies: DependencyContainer(), navigation: controller).open(url: url, isAuthenticated: true)
                }
            case .failure:
                break
            }
        }
    }
}
