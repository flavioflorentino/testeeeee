import Alamofire
import Core
import Foundation
import LegacyPJ
import SwiftyJSON

protocol ApiSellerProtocol {
    func data(_ completion: @escaping(Swift.Result<Seller, LegacyPJError>) -> Void)
}

final class ApiSeller: LegacyPJ.Api, ApiSellerProtocol {
    private let dependencies: HasKVStore = DependencyContainer()
    
    // MARK: - Public Methods
    
    /// retrive the data of the authenticated seller
    /// - parameter lastId: last loaded id
    func data(_ completion: @escaping(Swift.Result<Seller, LegacyPJError>) -> Void) {
        let endpoint = Endpoint.sellerAuthenticated()
        requestManager.apiRequest(urlString: endpoint, method: .get).responseApi { response in
            switch response.result {
            case .success(let json):
                guard let seller = Seller(json: json) else {
                    completion(.failure(LegacyPJError(message: "")))
                    return
                }
                completion(.success(seller))
            case .failure:
                completion(.failure(response.result.picpayError ?? LegacyPJError(message: "")))
            }
        }
    }
    
    /// Edit the data of the authenticated seller
    /// - parameter pin: User password
    /// - parameter seller: Seller data to update
    func update(pin: String, seller: Seller, _ completion: @escaping((_ seller: Seller?, _ error: LegacyPJError?) -> Void) ) {
        let p: Parameters = seller.toDictionary()
        
        requestManager.apiRequest(urlString: Endpoint.updateSeller(), method: .put, parameters: p, pin: pin)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    
                    let seller = Seller(json: json)
                    completion(seller, nil)
                    
                    return
                case .failure:
                    completion(nil, response.result.picpayError)
                    return
                }
            }
    }
    
    /// Update the seller logo
    /// - parameter updateModel: Model containing password, data to update, Device token, Image data
    func updateLogo(
        updateModel: SellerLogoUpdateModel,
        uploadProgress: UploadProgress? = nil,
        _ completion: @escaping((_ url: String?, _ error: LegacyPJError?) -> Void)
    ) {
        let p: Parameters = ["cnpj": updateModel.seller.cnpj ?? "", "device_token": updateModel.deviceToken]
        let file = FileUpload(key: "logo", data: updateModel.image, fileName: "logo.jpeg", mimeType: "image/jpeg")
        
        requestManager.apiUpload(
            Endpoint.updateLogo(),
            method: .post,
            files: [file],
            uploadProgress: uploadProgress,
            parameters: p,
            pin: updateModel.pin
        ) { response in
            switch response.result {
            case .success(let json):
                if let url = json["image"].string {
                    completion(url, nil)
                } else {
                    completion(nil, nil)
                }
            case .failure:
                completion(nil, response.result.picpayError)
            }
        }
    }
    
    /// Load the seller bank account
    func bankAccount(_ completion: @escaping((_ bankAccount: BankAccount?, _ error: LegacyPJError?) -> Void) ) {
        requestManager.apiRequest(urlString: Endpoint.bankAccount(), method: .get)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    let bankAccount = BankAccount(json: json)
                    completion(bankAccount, nil)
                    
                    return
                case .failure:
                    completion(nil, response.result.picpayError)
                    return
                }
            }
    }
    
    /// Update the seller banck account
    /// - parameter bankAccount: seller bank account
    func updateBankAccount(pin: String, bankAccount: BankAccount, _ completion: @escaping BaseCompletion ) {
        let p: Parameters = bankAccount.toDictionary()
        
        requestManager.apiRequest(urlString: Endpoint.updateBankAccount(), method: .put, parameters: p, pin: pin)
            .responseApiBaseCompletion(completionHandler: completion)
    }
    
    /// retrive the data of the authenticated seller
    func ppcode(_ completion: @escaping((_ code: CodePicPay?, _ error: LegacyPJError?) -> Void) ) {
        requestManager.apiRequest(urlString: Endpoint.ppcode(), method: .get)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    if let ppcode = CodePicPay(json: json) {
                        completion(ppcode, nil)
                    } else {
                        completion(nil, LegacyPJError(message: Strings.Default.errorDecodeObject))
                    }
                
                    return
                case .failure:
                    completion(nil, response.result.picpayError)
                    return
                }
            }
    }
    
    /// Retrive the list of operator
    /// - parameter lastId: last loaded id
    func operatorsList(_ lastId: Int?, _ completion: @escaping((Swift.Result<(operators: [Operator], pag: Pagination), LegacyPJError>) -> Void)) {
        var p: Parameters = [:]
        if let id = lastId {
            p["last_id"] = id
        }
        
        requestManager.apiRequest(urlString: Endpoint.operatorList(), method: .get, parameters: p).responseApi { response in
            switch response.result {
            case .success(let json):
                let list: [Operator] = json["list"].array?.compactMap { Operator(json: $0) } ?? []
                let pag = Pagination(json: json)
                completion(.success((list, pag)))
            case .failure:
                guard let error = response.result.picpayError else {
                    completion(.failure(LegacyPJError.requestError))
                    return
                }
                completion(.failure(error))
            }
        }
    }
    
    /// Retrive the list of operator
    /// - parameter operatorData: operator data for create
    /// - parameter image: Image data for update
    func createOperator(pin: String, operatorData: Operator, image: Data?, uploadProgress:( (_ progress: Double) -> Void)?, completion: @escaping((_ error: LegacyPJError?) -> Void) ) {
        let p: Parameters = [
            "username": operatorData.username ?? "",
            "password": operatorData.password ?? ""
        ]
        var files: [FileUpload] = []
        if let image = image {
            let file = FileUpload(key: "logo", data: image, fileName: "logo.jpeg", mimeType: "image/jpeg")
            files.append(file)
        }
        
        requestManager.apiUpload(Endpoint.operatorCreate(), method: .post, files: files, uploadProgress: uploadProgress, parameters: p, pin: pin) { response in
            switch response.result {
            case .success:
                completion(nil)
                return
            case .failure:
                completion(response.result.picpayError)
                return
            }
        }
    }
    
    /// Retrive the list of operator
    /// - parameter operatorData: operator data for update
    /// - parameter image: Image data for update
    func updateOperator(pin: String, operatorData: Operator, image: Data?, uploadProgress:( (_ progress: Double) -> Void)?, completion: @escaping((_ error: LegacyPJError?) -> Void) ) {
        let p: Parameters = [
            "user_seller_id": operatorData.id ?? 0,
            "username": operatorData.username ?? "",
            "password": operatorData.password ?? ""
        ]
        
        var files: [FileUpload] = []
        if let image = image {
            let file = FileUpload(key: "image", data: image, fileName: "logo.jpeg", mimeType: "image/jpeg")
            files.append(file)
        }
        
        requestManager.apiUpload(Endpoint.operatorUpdate(), method: .post, files: files, uploadProgress: uploadProgress, parameters: p, pin: pin) { response in
            switch response.result {
            case .success:
                completion(nil)
                
                return
            case .failure:
                completion(response.result.picpayError)
                return
            }
        }
    }
    
    /// Delete operator
    /// - parameter pin: Password
    /// - parameter userSellerId: user seller id
    func deleteOperator(pin: String, userSellerId: Int, _ completion: @escaping((_ error: LegacyPJError?) -> Void) ) {
        let p: Parameters = [:]
        requestManager.apiRequest(urlString: Endpoint.operatorDelete(id: userSellerId), method: .delete, parameters: p, pin: pin).responseApi { response in
            switch response.result {
            case .success:
                completion(nil)
                return
            case .failure:
                completion(response.result.picpayError)
                return
            }
        }
    }
    
    /// Update the plan fee
    /// - parameter pin: User password
    /// - parameter daysToReleaseCc: days
    func updatePlanFee(pin: String, daysToReleaseCc: Int, _ completion: @escaping BaseCompletion) {
        let p: Parameters = ["days_to_release_cc": daysToReleaseCc ]
        requestManager.apiRequest(urlString: Endpoint.updatePlanFee(), method: .put, parameters: p, pin: pin)
            .responseApiBaseCompletion(completionHandler: completion)
    }
    
    /// Update the address
    /// - parameter pin: User password
    /// - parameter daysToReleaseCc: days
    func updateAddress(pin: String, address: Address, _ completion: @escaping BaseCompletion) {
        let p: Parameters = address.toDictionary()
        requestManager.apiRequest(urlString: Endpoint.updateAddress(), method: .put, parameters: p, pin: pin)
            .responseApiBaseCompletion(completionHandler: completion)
    }
    
    /// Update the seller display name
    /// - parameter pin: User password
    /// - parameter name: display name
    func updateDisplayName(pin: String, name: String, _ completion: @escaping BaseCompletion) {
        let p: Parameters = ["name": name]
        requestManager.apiRequest(urlString: Endpoint.updateDisplayName(), method: .put, parameters: p, pin: pin)
            .responseApiBaseCompletion(completionHandler: completion)
    }
    
    /// Update the restricat scanner payment config
    /// - parameter pin: User password
    /// - parameter restrictToScanner: restrict to scanner
    func updateRestrictToScanner(pin: String, restrictToScanner: Bool, _ completion: @escaping BaseCompletion) {
        let p: Parameters = ["restrict_to_scanner": restrictToScanner]
        requestManager.apiRequest(urlString: Endpoint.updateRestrictScannerPayment(), method: .put, parameters: p, pin: pin)
            .responseApiBaseCompletion(completionHandler: completion)
    }
    
    /// Update the restricat scanner payment config
    /// - parameter pin: User password
    /// - parameter userPhone: user phone
    /// - parameter storePhone: store phone
    func updatePhones(pin: String, userPhone: String, storePhone: String, _ completion: @escaping BaseCompletion) {
        let p: Parameters = ["store_phone": storePhone.onlyNumbers, "user_phone": userPhone.onlyNumbers]
        requestManager.apiRequest(urlString: Endpoint.updatePhone(), method: .put, parameters: p, pin: pin)
            .responseApiBaseCompletion(completionHandler: completion)
    }
   
    /// Update the address
    /// - parameter pin: User password
    /// - parameter daysToReleaseCc: days
    func monitorAuthorize(token: String, _ completion: @escaping BaseCompletion) {
        let p: Parameters = ["token": token]
        requestManager.apiRequest(urlString: Endpoint.webMonitor(), method: .post, parameters: p)
            .responseApiBaseCompletion(completionHandler: completion)
    }
    
    func usePromoCode(code: String, _ completion: @escaping ((_ result: PromoCodeResult?, _ error: LegacyPJError?) -> Void) ) {
        var p: Parameters = [:]
        p["code"] = code
        
        requestManager.apiRequest(urlString: Endpoint.promocode(), method: .post, parameters: p)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    let result = PromoCodeResult(json: json)
                    completion(result, nil)
                    return
                case .failure:
                    completion(nil, response.result.picpayError)
                    return
                }
            }
    }
}

struct PromoCodeResult {
    var title: String = ""
    var subtitle: String = ""
    var text: String = ""
    
    init(json: JSON) {
        title = json["title"].string ?? ""
        subtitle = json["subtitle"].string ?? ""
        text = json["text"].string ?? ""
    }
}

struct SellerLogoUpdateModel {
    let pin: String
    let seller: Seller
    let deviceToken: String
    let image: Data
}
