import Alamofire
import Core
import Foundation
import LegacyPJ
import SwiftyJSON
import FeatureFlag

struct SignUpAddress {
    let cep: String
    let address: String
    let number: String
    let district: String
    let city: String
    let state: String
}

enum SignUpEndpoint {
    case verifySeller(cnpj: String)
}

extension SignUpEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .verifySeller(let cnpj):
            return "/seller/validate/\(cnpj)"
        }
    }
    
    var isTokenNeeded: Bool {
        false
    }
}

protocol ApiSignUpProtocol {
    func verifySeller(_ cnpj: String, completion: @escaping ((Swift.Result<CompanyInfo?, ApiError>) -> Void))
}

final class ApiSignUp: LegacyPJ.Api, ApiSignUpProtocol {
    typealias Dependencies = HasMainQueue & HasFeatureManager & HasKVStore
    typealias FeeListClosure = (_ firstDay: String?, _ lastDay: String?, _ defaultDay: String?, _ items: [String: String]?, _ error: LegacyPJError?) -> Void
    
    private let dependencies: Dependencies = DependencyContainer()
    
    // MARK: - Public Methods
    
    /// Check duplicates and send sms with device verification code
    ///
    /// - parameter phone: complete phone
    /// - parameter closure: callback.
    func verifyMobilePhone(phone: String, _ completion: @escaping((_ smsLock: Date?, _ error: LegacyPJError?) -> Void) ) {
        // sanitize the phone
        let phoneSanitize = phone.replacingOccurrences(of: "-", with: "")

        let deviceToken: String?

        if dependencies.featureManager.isActive(.isAppLocalDataUnavailable) {
            deviceToken = dependencies.kvStore.stringFor(BizKvKey.deviceToken)
        } else {
            deviceToken = AppManager.shared.localData.deviceToken
        }
        let p: Parameters = [
            "phone": phoneSanitize,
            "device_token": deviceToken ?? ""
        ]
        
        requestManager.apiPublicRequest(urlString: Endpoint.accountVerifyMobilePhone(), method: .post, parameters: p)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    
                    var smsLock: Date?
                    if let lock = json["lock"].int {
                        smsLock = Date(timeIntervalSinceNow: TimeInterval(lock))
                    }
                    
                    completion(smsLock, nil)
                case .failure:
                    completion(nil, response.result.picpayError)
                }
            }
    }
    
    /// Validate the SMS code sent to the phone by (SMS or Call)
    ///
    /// - parameter code: code
    /// - parameter closure: callback.
    func verifyMobilePhoneCode(code: String, phone: String, _ completion: @escaping((_ error: LegacyPJError?) -> Void) ) {
        let p: Parameters = [
                "code": code,
                "phone": phone
        ]
        
        requestManager.apiPublicRequest(urlString: Endpoint.accountVerifyMobilePhoneCode(), method: .post, parameters: p)
            .responseApi { response in
                switch response.result {
                case .success:
                    completion(nil)
                case .failure:
                    completion(response.result.picpayError)
                }
            }
    }
    
    /// Get the address from cep
    ///
    /// - parameter cep: cep
    /// - parameter closure: callback.
    func verifyCEP(cep: String, _ completion: @escaping((_ address: Address?, _ error: LegacyPJError?) -> Void) ) {
        let p: Parameters = ["cep": cep.onlyNumbers]
        
        requestManager.apiPublicRequest(urlString: Endpoint.verifyCep(), method: .post, parameters: p)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    
                    if let address = Address(json: json) {
                        completion(address, nil)
                        return
                    }
                    
                    completion(nil, nil)
                case .failure:
                    completion(nil, response.result.picpayError)
                }
            }
    }
    
    /// Get the address from cep
    ///
    /// - parameter address: Address
    /// - parameter closure: callback.
    func mapCoordinate(
        address: SignUpAddress,
        _ completion: @escaping((_ lat: Double?, _ lon: Double?, _ error: LegacyPJError?) -> Void)
    ) {
        let p: Parameters = [
            "cep": address.cep,
            "address": address.address,
            "number": address.number,
            "district": address.district,
            "city": address.city,
            "state": address.state
        ]
        
        requestManager.apiPublicRequest(urlString: Endpoint.coordinateForAddress(), method: .post, parameters: p)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    
                    var lat: Double = 0.0
                    var lon: Double = 0.0
                    
                    if let lt = json["lat"].double {
                        lat = lt
                    }
                    if let ln = json["lon"].double {
                        lon = ln
                    }
                    
                    completion(lat, lon, nil)
                case .failure:
                    completion(nil, nil, response.result.picpayError)
                }
            }
    }
    
    /// Check for duplicates
    ///
    /// - parameter cnpj: complete phone
    /// - parameter closure: callback.
    func validateCNPJ(cnpj: String, _ completion: @escaping((_ error: LegacyPJError?) -> Void) ) {
        // sanitize the phone
        let cnpjSanitize = cnpj.onlyNumbers
        
        let p: Parameters = ["cnpj": cnpjSanitize]
        
        requestManager.apiPublicRequest(urlString: Endpoint.signUpValidateCnpj(), method: .post, parameters: p)
            .responseApi { response in
                switch response.result {
                case .success:
                    completion(nil)
                case .failure:
                    completion(response.result.picpayError)
                }
            }
    }
    
    /// Check if CPF matches with a shareholder CPF.
    /// - Parameters:
    ///   - cnpj: Company CNPJ
    ///   - cpf: User CPF
    ///   - name: User name
    ///   - completion: Calls with nil if success.
    func link(cnpj: String, cpf: String, name: String, completion: @escaping((_ error: LegacyPJError?) -> Void)) {
        let p: Parameters = [
            "cnpj": cnpj.onlyNumbers,
            "cpf": cpf.onlyNumbers,
            "name": name
        ]
        
        requestManager.apiPublicRequest(
            urlString: Endpoint.linkCompanyCNPJUserCPF(),
            method: .get,
            parameters: p
        )
        .responseAuthApi { response in
            switch response.result {
            case .success:
                completion(nil)
            case .failure:
                completion(response.result.picpayError)
            }
        }
    }
    
    /// Check for duplicates
    ///
    /// - parameter cnpj: complete phone
    /// - parameter closure: callback.
    func verifyDuplicatedUser(cnpj: String, cpf: String, email: String, _ completion: @escaping((_ error: LegacyPJError?) -> Void) ) {
        let cnpjSanitize = cnpj.onlyNumbers
        let cpfSanitize = cpf.onlyNumbers
        
        let p: Parameters = ["cnpj": cnpjSanitize, "cpf": cpfSanitize, "email": email]
        
        requestManager.apiPublicRequest(urlString: Endpoint.verifyDuplicatedUser(), method: .post, parameters: p)
            .responseApi { response in
                switch response.result {
                case .success:
                    completion(nil)
                case .failure:
                    completion(response.result.picpayError)
                }
            }
    }
    
    /// Get the list of fee
    ///
    /// - parameter closure: callback.
    func feeList(_ completion: @escaping(FeeListClosure)) {
        requestManager.apiPublicRequest(urlString: Endpoint.receiptFeeList(), method: .get)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    self.parseFeeList(json: json, completion: completion)
                case .failure:
                    completion(nil, nil, nil, nil, response.result.picpayError)
                }
            }
    }
    
    /// Get the address from cep
    ///
    /// - parameter data: image data
    /// - parameter closure: callback.
    func uploadLogoImage(cnpj: String, deviceToken: String, image: Data, uploadProgress: ((Double) -> Void)?, _ completion: @escaping((_ url: String?, _ error: LegacyPJError?) -> Void) ) {
        let p = [
            "cnpj": cnpj.onlyNumbers,
            "device_token": deviceToken
        ]
        
        let file = FileUpload(key: "logo", data: image, fileName: "logo.jpeg", mimeType: "image/jpeg")
        
        requestManager.apiPublicUpload(Endpoint.signupUploadLogo(), method: .post, files: [file], parameters: p, uploadProgress: uploadProgress) { response in
            switch response.result {
            case .success(let json):
                if let url = json["image"].string {
                    completion(url, nil)
                } else {
                    completion(nil, nil)
                }
            case .failure:
                completion(nil, response.result.picpayError)
            }
        }
    }
    
    /// Create a new account
    ///
    /// - parameter account: accoun form data
    /// - parameter closure: callback.
    func createAccount(_ account: SignUpAccount, _ completion: @escaping((_ auth: Auth?, _ error: LegacyPJError?) -> Void) ) {
        let p: Parameters = getParameters(account: account)
        
        requestManager.apiPublicRequest(urlString: Endpoint.createAccount(), method: .post, parameters: p)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    if let auth = Auth(json: json["access"]) {
                        completion(auth, nil)
                    } else {
                        completion(nil, nil)
                    }
                case .failure:
                    completion(nil, response.result.picpayError)
                }
            }
    }
    
    func verifySeller(_ cnpj: String, completion: @escaping ((Swift.Result<CompanyInfo?, ApiError>) -> Void)) {
        BizApi<CompanyInfo>(endpoint: SignUpEndpoint.verifySeller(cnpj: cnpj.onlyNumbers)).bizExecute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }
    
    func verifyCPFIntegrity(_ cnpj: String, cpf: String, completion: @escaping (_ error: LegacyPJError?, _ valid: Bool) -> Void) {
        let param: Parameters = [
            "cnpj": cnpj.onlyNumbers,
            "cpf": cpf.onlyNumbers
        ]
        requestManager.apiPublicRequest(urlString: Endpoint.verifyAccountIntegrity(), method: .post, parameters: param)
            .responseApi { response in
                switch response.result {
                case .success(let json):
                    guard let success = json["valid"].bool else {
                        completion(response.result.picpayError, false)
                        return
                    }
                    completion(nil, success)
                case .failure:
                    completion(response.result.picpayError, false)
                }
            }
    }
}

private extension ApiSignUp {
    func getParameters(account: SignUpAccount) -> Parameters {
        var parameters: [String: Any] = [
            "cnpj": account.cnpj.onlyNumbers,
            "razao_social": account.companyName,
            "cep": account.cep.onlyNumbers,
            "address": account.address,
            "state": account.state,
            "city": account.city,
            "district": account.district,
            "complement": account.complement,
            "number": account.addressNumber,
            "lat": account.lat,
            "lon": account.lon,
            "display_name": account.displayName,
            "image": account.imageUrl,
            "mobile_phone": (account.mobilePhoneDdd + account.mobilePhone).onlyNumbers,
            "phone": (account.ddd + account.phone).onlyNumbers,
            "name": account.partner.name,
            "email": account.partner.email,
            "cpf": account.partner.cpf.onlyNumbers,
            "birth_date": account.partner.birthday,
            "partner": partnerDict(account: account),
            "password": account.password,
            "admin": true,
            "agencia": account.branch,
            "banco_id": account.bank?.code ?? "",
            "agencia_dv": account.branchDigit,
            "conta": account.account,
            "conta_dv": account.accountDigit,
            "operacao": account.accountOperation,
            "tipo": account.legacyAccountType.rawValue,
            "days_to_release_cc": account.days,
            "distinct_id": account.distinctId,
            "document_bank": account.documentBank,
            "terms_accepted": account.termsAccepted,
            "without_address": account.withoutAddress
        ]
        
        if let accountValue = account.accountValue, let accountType = account.accountType {
            parameters["value"] = accountValue.rawValue
            parameters["type"] = accountType.rawValue
        }
        
        return parameters
    }
    
    func partnerDict(account: SignUpAccount) -> Parameters {
        [
            "name": account.partner.name,
            "email": account.partner.email,
            "cpf": account.partner.cpf.onlyNumbers,
            "birth_date": account.partner.birthday,
            "mother_name": account.partner.motherName,
            "address": [
                "street": account.partner.address.street,
                "number": account.partner.address.addressNumber,
                "district": account.partner.address.district,
                "complement": account.partner.address.complement,
                "city": account.partner.address.city,
                "state": account.partner.address.state,
                "postal_code": account.partner.address.cep
            ]
        ]
    }
    
    func parseFeeList(json: JSON, completion: FeeListClosure) {
        var list = [String: String]()
        var firstDay = ""
        var lastDay = ""
        var defaultDay = ""
        
        // adjust the list of fee
        if let items = json["list"].array {
            for i in items {
                if
                    let dic = i.dictionary,
                    let key = dic["day"]?.string,
                    let value = dic["percentage_str"]?.string {
                    list[key] = value
                }
            }
            
            if let d = items.first?["day"] {
                firstDay = "\(d)"
            }
            
            if let d = items.last?["day"] {
                lastDay = "\(d)"
            }
        }
        
        if let d = json["default_day"].int {
            defaultDay = "\(d)"
        }
        
        completion(firstDay, lastDay, defaultDay, list, nil)
    }
}
