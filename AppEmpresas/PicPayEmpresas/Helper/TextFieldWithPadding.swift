import UIKit
import Foundation

class TextFieldWithPadding: UITextField {
    private var textPadding: UIEdgeInsets = .zero
    
    func addPadding(_ padding: UIEdgeInsets) {
        textPadding = padding
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.textRect(forBounds: bounds)
        return rect.inset(by: textPadding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.editingRect(forBounds: bounds)
        return rect.inset(by: textPadding)
    }
}
