import Core
import Foundation
import LegacyPJ

final class KeychainMigration {
    typealias Dependencies = HasKeychainManager
    private let dependencies = DependencyContainer()
    
    /*
     We had to migrate Keychain because we were using a pod and now we are using our implementation
     because that would cause an impact in the user experience we had to implement this class.
     After some publications (enough number that could already have finished migrating)  we can delete these methods.
     */
    func migrate() {
        guard
            let apiAuth = getValues(for: KeychainKeyLegacyPJ.apiAuth.rawValue),
            let index = getValues(for: KeychainKeyLegacyPJ.apiAuthIndex.rawValue)
            else {
                return
        }
        
        debugPrint("======== Migrating keychain values ========")
        dependencies.keychain.set(key: KeychainKeyLegacyPJ.apiAuth, value: apiAuth)
        dependencies.keychain.set(key: KeychainKeyLegacyPJ.apiAuthIndex, value: index)
        
        clearOlderKeychain()
    }
    
    private func getValues(for key: String) -> String? {
        let query = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrService: "com.picpay.PicPayEmpresas",
            kSecReturnData: kCFBooleanTrue as Any,
            kSecMatchLimit: kSecMatchLimitOne,
            kSecAttrAccount: key
        ] as CFDictionary
        
        var dataTypeRef: AnyObject?
        let status = SecItemCopyMatching(query, &dataTypeRef)
        if status == noErr, let dataTypeRefData = dataTypeRef as? Data, let value = String(data: dataTypeRefData, encoding: .utf8) {
            return value
        }
        return nil
    }
    
    private func clearOlderKeychain() {
        let query = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrService: "com.picpay.PicPayEmpresas"
        ] as CFDictionary
        let result = SecItemDelete(query) == noErr
        debugPrint("========  Have clean the older Keychain:  \(result)    ========")
    }
}
