import Core
import Foundation

enum KeychainKeyPJ: String, KeychainKeyable {
    case registerStatus = "register_status"
}
