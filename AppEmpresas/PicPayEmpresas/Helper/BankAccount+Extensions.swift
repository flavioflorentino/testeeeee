import LegacyPJ

extension BankAccount {
    var confirmationItem: AccountConfirmationItem {
        let personalDocument = cpf ?? ""
        let companyDocument = cnpj ?? ""
        let documentType: AccountConfirmationItem.DocumentType = personalDocument.isEmpty ? .cnpj : .cpf
        let accountType = documentType == .cnpj ?
            Strings.BankAccountConfirmation.companyAccount : Strings.BankAccountConfirmation.personalAccount
        let type: String

        switch self.type {
        case "C":
            type = "\(Strings.Default.currentAccount) \(accountType)"
        case "P":
            type = "\(Strings.Default.savingAccount) \(accountType)"
        default:
            type = ""
        }

        return AccountConfirmationItem(
            imageUrl: bank?.image ?? "",
            bank: bank?.name ?? "",
            branch: branchDigit.isEmpty ? branch : "\(branch)-\(branchDigit)",
            account: "\(account)-\(accountDigit)",
            document: documentType == .cnpj ? companyDocument : personalDocument,
            documentType: documentType,
            type: type
        )
    }
}
