import AudioToolbox
import Core
import LegacyPJ
import UIKit

// MARK: - Methods
public extension UIAlertController {
    /// SwifterSwift: Present alert view controller in the current view controller.
    ///
    /// - Parameters:
    ///   - animated: set true to animate presentation of alert controller (default is true).
    ///   - vibrate: set true to vibrate the device while presenting the alert (default is false).
    ///   - completion: an optional completion handler to be called after presenting alert controller (default is nil).
    func show(animated: Bool = true, vibrate: Bool = false, completion: (() -> Void)? = nil) {
        guard let window = UIApplication.shared.keyWindow, let rootViewController = window.rootViewController else {
            return
        }

        var topController = rootViewController

        while let newTopController = topController.presentedViewController {
            topController = newTopController
        }
        
        topController.present(self, animated: animated, completion: completion)
        if vibrate {
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        }
    }
    
    /// SwifterSwift: Add an action to Alert
    ///
    /// - Parameters:
    ///   - title: action title
    ///   - style: action style (default is UIAlertActionStyle.default)
    ///   - isEnabled: isEnabled status for action (default is true)
    ///   - handler: optional action handler to be called when button is tapped (default is nil)
    /// - Returns: action created by this method
    @discardableResult
    func addAction(title: String, style: UIAlertAction.Style = .default, isEnabled: Bool = true, handler: ((UIAlertAction) -> Void)? = nil) -> UIAlertAction {
        let action = UIAlertAction(title: title, style: style, handler: handler)
        action.isEnabled = isEnabled
        self.addAction(action)
        return action
    }
}

extension UIAlertController {
    // MARK: - Initializers
    
    /// SwifterSwift: Create new alert view controller with default OK action.
    ///
    /// - Parameters:
    ///   - title: alert controller's title.
    ///   - message: alert controller's message (default is nil).
    ///   - defaultActionButtonTitle: default action button title (default is "OK")
    ///   - tintColor: alert controller's tint color (default is nil)
    public convenience init(title: String, message: String? = nil, defaultActionButtonTitle: String = "OK", tintColor: UIColor? = nil) {
        self.init(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: defaultActionButtonTitle, style: .default, handler: nil)
        self.addAction(defaultAction)
        if let color = tintColor {
            self.view.tintColor = color
        }
    }
    
    /// SwifterSwift: Create new error alert view controller from Error with default OK action and optional custom actions.
    ///
    /// - Parameters:
    ///   - error: error with localizedDescription to set alert controller's message and optional custom actions.
    public convenience init(error: Error) {
        var errorMessage: String = error.localizedDescription
        
        if let requestError = error as? RequestError {
            errorMessage = requestError.message
        }
        
        self.init(title: Strings.Default.oops,
                  message: errorMessage,
                  preferredStyle: .alert)
        
        if let error = error as? LegacyPJError, error.isPermissionDenied {
            addTalkToUsAction()
        }
        
        let okAction = addOkAction()
        if hasMoreThanOneAction() {
            preferredAction = okAction
        }
    }
    
    private func hasMoreThanOneAction() -> Bool {
        actions.count > 1
    }
    
    private func addTalkToUsAction() {
        addAction(title: "Fale conosco", style: .default, isEnabled: true) { _ in
            guard let topController = UIApplication.topViewController() else {
                return
            }
            BIZCustomerSupportManager(accessToken: AuthManager.shared.userAuth?.auth.accessToken).presentTicketList(from: topController)
        }
    }
    
    private func addOkAction() -> UIAlertAction {
        let action = UIAlertAction(title: Strings.Default.ok, style: .default, handler: nil)
        addAction(action)
        return action
    }
}
