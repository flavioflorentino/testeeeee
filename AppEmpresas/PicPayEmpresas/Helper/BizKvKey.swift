import Core

enum BizKvKey: String, KVKeyContract {
    var description: String { rawValue }
    
    case globalTrackValues
    case individualCompany
    case isBalanceHidden
    case didInteractTransactionCarouselPayBillet
    case didInteractTransactionCarouselTransfer
    case didInteractCarouselCreateCampaign
    case didInteractTransactionCarouselMaterial
    case didInteractTransactionCarouselPrint
    case didPresentAdvertisingPix
    case deviceToken = "deviceID"
    case phoneValidationNumber
    case needShowStartPromoCodePopup
}
