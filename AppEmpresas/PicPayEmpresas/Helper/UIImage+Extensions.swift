import Foundation
import UIKit

extension UIImage {
    /// Returns a image that fills in newSize
    func resizedImage(newSize: CGSize, scale: CGFloat) -> UIImage? {
        guard size != newSize else {
            return self
        }
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, scale)
        let newRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        draw(in: newRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func resizedImageForUpload() -> UIImage? {
        let uploadImageSize = CGSize(width: 400, height: 400)
        return resizedImageWithinRect(rectSize: uploadImageSize)
    }
    
    /// Returns a resized image that fits in rectSize, keeping it's aspect ratio
    /// Note that the new image size is not rectSize, but within it.
    func resizedImageWithinRect(rectSize: CGSize, scale: CGFloat = 0) -> UIImage? {
        let widthFactor = size.width / rectSize.width
        let heightFactor = size.height / rectSize.height
        let resizeFactor = max(heightFactor, widthFactor)
        let newWidth = size.width / resizeFactor
        let newHeight = size.height / resizeFactor
        let newSize = CGSize(width: newWidth, height: newHeight)
        let resized = resizedImage(newSize: newSize, scale: scale)
        return resized
    }
}
