import AnalyticsModule
import Core
import CoreSellerAccount
import Foundation
import FeatureFlag
import LegacyPJ

typealias AppDependencies =
    HasNoDependency &
    HasMainQueue &
    HasAuthManager &
    HasApiSeller &
    HasApiSignUp &
    HasLocationManager &
    HasAppManager &
    HasEnvVariable &
    HasFeatureManager &
    HasAnalytics &
    HasKeychainManager &
    HasKVStore &
    HasSellerInfo &
    HasAppCoordinator &
    HasDispatchGroup

final class DependencyContainer: AppDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var locationManager: LocationManagerContract = LocationManager.shared
    lazy var authManager: AuthManagerProtocol = AuthManager.shared
    lazy var apiSeller: ApiSellerProtocol = ApiSeller()
    lazy var apiSignUp: ApiSignUpProtocol = ApiSignUp()
    lazy var appManager: AppManagerProtocol = AppManager.shared
    lazy var envVariable: EnvVariableProtocol = EnvVariable()
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var keychain: KeychainManagerContract = KeychainManager()
    lazy var kvStore: KVStoreContract = KVStore()
    lazy var sellerInfo: SellerInfoManagerProtocol = SellerInfoManager.shared
    lazy var appCoordinator: AppCoordinating = AppCoordinator()
    lazy var dispatchGroup = DispatchGroup()
}
