import Foundation
import LegacyPJ

protocol HasNoDependency {}

protocol HasApiSeller {
    var apiSeller: ApiSellerProtocol { get }
}

protocol HasApiSignUp {
    var apiSignUp: ApiSignUpProtocol { get }
}

protocol HasAppManager {
    var appManager: AppManagerProtocol { get }
}

protocol HasEnvVariable {
    var envVariable: EnvVariableProtocol { get }
}

protocol HasAppCoordinator {
    var appCoordinator: AppCoordinating { get }
}
