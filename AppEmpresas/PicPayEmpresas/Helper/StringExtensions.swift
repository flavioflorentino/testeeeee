import Foundation
import UI
import UIKit

// MARK: - Properties

extension String {
    /// SwifterSwift: Date object from "yyyy-MM-dd HH:mm:ss.SSSSSS" formatted string.
    var dateTimeFull: Date? {
        let selfLowercased = self.trimmed.lowercased()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS"
        return formatter.date(from: selfLowercased)
    }
    
    var transactionTimestamp: Date? {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return formatter.date(from: self)
    }
    
    /// SwifterSwift: String with no spaces or new lines in beginning and end.
    var trimmed: String {
        trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    var onlyNumbers: String {
        components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
    
    var validCpf: Bool {
        let cpf = onlyNumbers
        guard cpf.count == 11 else {
            return false
        }
        
        let index1 = cpf.index(cpf.startIndex, offsetBy: 9)
        let index2 = cpf.index(cpf.startIndex, offsetBy: 10)
        let index3 = cpf.index(cpf.startIndex, offsetBy: 11)
        let originalInitialDac = Int(cpf[index1..<index2])
        let originalFinalDac = Int(cpf[index2..<index3])
        
        var calcInitialDac = 0, calcFinalDac = 0
        
        for i in 0...8 {
            let start = cpf.index(cpf.startIndex, offsetBy: i)
            let end = cpf.index(cpf.startIndex, offsetBy: i + 1)
            if let char = Int(cpf[start..<end]) {
                calcInitialDac += char * (10 - i)
                calcFinalDac += char * (11 - i)
            }
        }
        
        calcInitialDac %= 11
        calcInitialDac = calcInitialDac < 2 ? 0 : 11 - calcInitialDac
        
        calcFinalDac += calcInitialDac * 2
        calcFinalDac %= 11
        calcFinalDac = calcFinalDac < 2 ? 0 : 11 - calcFinalDac
        
        return calcInitialDac == originalInitialDac && calcFinalDac == originalFinalDac
    }
    
    var html2AttributedString: NSAttributedString? {
        guard let data = data(using: String.Encoding.utf8) else {
            return nil
        }
        
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    
    var isBackspace: Bool {
        if canBeConverted(to: String.Encoding.utf8) {
            let char = cString(using: String.Encoding.utf8)
            return strcmp(char, "\\b") == -92
        } else {
            return false
        }
    }
    
    /// SwifterSwift: Random string of given length.
    ///
    /// - Parameter ofLength: number of characters in string.
    /// - Returns: random string of given length.
    static func random(ofLength: Int) -> String {
        var string = ""
        guard ofLength > 0 else {
            return string
        }
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        for _ in 0..<ofLength {
            let randomIndex = Int.random(in: 0..<base.count)
            string += "\(base[base.index(base.startIndex, offsetBy: IndexDistance(randomIndex))])"
        }
        return string
    }
    
    func currency(hideSymbol hide: Bool = false) -> String {
        guard
            let number = bindToNumber(),
            let formatted = bindToCurrency(number, hideSymbol: hide)
            else {
                return String()
        }
        
        return formatted
    }
    
    func bindToNumber() -> NSNumber? {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.numberStyle = .decimal
        return formatter.number(from: self)
    }
    
    private func bindToCurrency(_ number: NSNumber, hideSymbol: Bool = false) -> String? {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "pt_BR")
        formatter.numberStyle = hideSymbol ? .decimal : .currency
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        return formatter.string(from: number)
    }
    
    func currencyInputFormatting() -> String {
        var amountWithPrefix = self
        
        let regex = try? NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex?.stringByReplacingMatches(
            in: amountWithPrefix,
            options: NSRegularExpression.MatchingOptions(rawValue: 0),
            range: NSRange(location: 0, length: self.count),
            withTemplate: ""
        ) ?? ""
        
        let double = (amountWithPrefix as NSString).doubleValue
        let number = NSNumber(value: (double / 100))
        
        guard
            number != 0 as NSNumber,
            let formatted = bindToCurrency(number, hideSymbol: false)
            else {
                return ""
        }
        
        return formatted
    }
    
    func rateInputFormatting() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumIntegerDigits = 2
        formatter.minimum = 0

        let amountWithPrefix = replacingOccurrences(of: "%", with: "")

        let double = (amountWithPrefix as NSString).doubleValue
        let number = NSNumber(value: (double / 100))
        
        guard number != 0 as NSNumber else {
            return ""
        }

        return formatter.string(from: number) ?? ""
    }
    
    func removingWhiteSpaces() -> String {
        replacingOccurrences(of: " ", with: "")
    }
    
    func chopPrefix(_ count: Int = 1) -> String {
        String(suffix(from: index(startIndex, offsetBy: count)))
    }
    
    func chopSuffix(_ count: Int = 1) -> String {
        String(prefix(upTo: index(endIndex, offsetBy: -count)))
    }
    
    func billetMask() -> CustomStringMask {
        CustomStringMask(mask: self[0] == "8"
            ? "00000000000-0 00000000000-0 00000000000-0 00000000000-0"
            : "00000.00000 00000.000000 00000.000000 0 00000000000000")
    }
}

// MARK: - Subscript

extension String {
    /// SwifterSwift: Safely subscript string with index.
    ///
    /// - Parameter i: index.
    subscript(i: Int) -> String? {
        guard i >= 0 && i < count else {
            return nil
        }
        return String(self[index(startIndex, offsetBy: i)])
    }

    subscript(bounds: CountableRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start..<end])
    }

    /// SwifterSwift: Safely subscript string within a half-open range.
    ///
    /// - Parameter range: Half-open range.
    subscript(range: CountableRange<Int>) -> String? {
        guard let lowerIndex = index(startIndex, offsetBy: max(0, range.lowerBound), limitedBy: endIndex) else {
            return nil
        }
        guard let upperIndex = index(lowerIndex, offsetBy: range.upperBound - range.lowerBound, limitedBy: endIndex) else {
            return nil
        }
        return String(self[lowerIndex..<upperIndex])
    }
}
