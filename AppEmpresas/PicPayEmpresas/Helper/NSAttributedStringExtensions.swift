import Foundation
import UIKit

public extension NSMutableAttributedString {
    @discardableResult
    func setAsLink(_ textToFind: String, linkURL: String) -> Bool {
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSAttributedString.Key.link, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
}

public extension NSAttributedString {
    /**
     Set Attributed string apply the picpay font
     */
    func attributedPicpayFont(_ fontSize: CGFloat = 12.0, weight: CGFloat = UIFont.Weight.light.rawValue) -> NSAttributedString? {
        let attrs = NSMutableAttributedString(attributedString: self)
        
        attrs.enumerateAttribute(NSAttributedString.Key.font, in: NSRange(location: 0, length: attrs.length), options: .longestEffectiveRangeNotRequired, using: { value, range, _ in
            if let oldFont = value as? UIFont {
                var newFont = UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight(rawValue: weight))
                
                if oldFont.fontName == "TimesNewRomanPS-BoldMT" {
                    newFont = UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold)
                }
                
                attrs.removeAttribute(NSAttributedString.Key.font, range: range)
                attrs.addAttribute(NSAttributedString.Key.font, value: newFont, range: range)
            }
        })
        return attrs
    }
}
