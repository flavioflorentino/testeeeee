import Core

public typealias BizApi<T: Decodable> = Core.Api<ResponseBiz<T>>
public typealias BizApiV2<T: Decodable> = Core.Api<ResponseBizV2<T>>

public struct ErrorBiz: Decodable {
    let error: RequestError

    static func errorMessage(_ data: Data?) -> String {
        guard let data = data, let decoded = try? JSONDecoder().decode(ErrorBiz.self, from: data) else {
            return RequestError().message
        }

        return decoded.error.message
    }
}

public struct ResponseBizV2<T: Decodable>: Decodable {
    let data: T
}

public struct ResponseBiz<T: Decodable>: Decodable {
    let meta: Meta?
    let data: T?
}

public struct Meta: Decodable {
    private enum CodingKeys: String, CodingKey {
        case code
        case errorType = "error_type"
        case errorTitle = "error_title"
        case message
        case errorMessage = "error_message"
    }

    let code: Int?
    let errorType: String?
    let errorTitle: String?
    let errorMessage: String?

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        code = try container.decodeIfPresent(Int.self, forKey: .code)
        errorType = try container.decodeIfPresent(String.self, forKey: .errorType)
        errorTitle = try container.decodeIfPresent(String.self, forKey: .errorTitle)

        if let message = try container.decodeIfPresent(String.self, forKey: .message) {
            errorMessage = message
        } else {
            errorMessage = try container.decodeIfPresent(String.self, forKey: .errorMessage)
        }
    }
}

struct ServerTimestamp: Decodable, Equatable {
    enum CodingKeys: String, CodingKey {
        case date, timezone, timezoneType = "timezone_type"
    }

    let stringDate: String
    let timezone: String
    let timezoneType: Int
    let date: Date?

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        stringDate = try container.decode(String.self, forKey: .date)
        timezone = try container.decode(String.self, forKey: .timezone)
        timezoneType = try container.decode(Int.self, forKey: .timezoneType)
        date = stringDate.dateTimeFull
    }
}

enum BizApiHelper {
    static func unwrapBizResult<T: Decodable>(
        result: Result<(model: ResponseBiz<T>, data: Data?), ApiError>,
        completion: @escaping(Result<T, ApiError>) -> Void
    ) {
        let mappedResult = result.map { $0.model.data }
        
        switch mappedResult {
        case .success(let model):
            guard let model = model else {
                completion(.failure(ApiError.bodyNotFound))
                return
            }
            completion(.success(model))
        case .failure(let error):
            completion(.failure(error))
        }
    }
}
