import UI
import UIKit

extension UINavigationBar {
    func setup(appearance: NavigationBarAppearance) {
        shadowImage = appearance.shadowImage
        tintColor = appearance.tintColor
        barTintColor = appearance.barTintColor
        barStyle = appearance.barStyle
        isTranslucent = appearance.isTranslucent
        titleTextAttributes = appearance.titleTextAttributes
        setBackgroundImage(appearance.backgroundImage, for: .default)
    }
}

protocol NavigationBarAppearance {
    var shadowImage: UIImage? { get }
    var tintColor: UIColor { get }
    var barTintColor: UIColor { get }
    var barStyle: UIBarStyle { get }
    var isTranslucent: Bool { get }
    var titleTextAttributes: [NSAttributedString.Key: Any] { get }
    var backgroundImage: UIImage? { get }
}

extension NavigationBarAppearance {
    var shadowImage: UIImage? {
        nil
    }
    
    var tintColor: UIColor {
        Palette.white.color
    }
    
    var barTintColor: UIColor {
        Palette.Business.grayscale600.color
    }
    
    var barStyle: UIBarStyle {
        .default
    }
    
    var isTranslucent: Bool {
        true
    }
    
    var titleTextAttributes: [NSAttributedString.Key: Any] {
        [:]
    }
    
    var backgroundImage: UIImage? {
        nil
    }
}

struct NavigationBarAppearanceOldRegistration: NavigationBarAppearance {
    let tintColor: UIColor = Palette.black.color
    let barTintColor: UIColor = #colorLiteral(red: 0.94, green: 0.94, blue: 0.94, alpha: 1.00)
    let barStyle: UIBarStyle = .default
    let isTranslucent: Bool = false
}

struct NavigationBarAppearanceDefault: NavigationBarAppearance {
    let barTintColor: UIColor = Colors.white.lightColor
    let tintColor: UIColor = Colors.branding600.lightColor
    let isTranslucent: Bool = false
    let titleTextAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: Colors.black.lightColor]
    let shadowImage: UIImage? = UIImage()
    let backgroundImage: UIImage? = UIImage()
}

struct NavigationBarAppearanceLegacy: NavigationBarAppearance {
    let shadowImage: UIImage? = UIImage()
    let tintColor: UIColor = Palette.white.color
    let barTintColor: UIColor = Palette.Business.grayscale600.color
    let barStyle: UIBarStyle = .black
    let isTranslucent: Bool = false
    let titleTextAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: Palette.white.color]
}

struct NavigationBarAppearanceTransparent: NavigationBarAppearance {
    let shadowImage: UIImage? = UIImage()
    let isTranslucent: Bool = true
    let backgroundImage: UIImage? = UIImage()
    let tintColor: UIColor = Colors.branding600.color
}

struct NavigationBarAppearanceNewTransparent: NavigationBarAppearance {
    let shadowImage: UIImage? = UIImage()
    let isTranslucent: Bool = true
    let backgroundImage: UIImage? = UIImage()
    let tintColor: UIColor = Colors.white.lightColor
    let barStyle: UIBarStyle = .black
}
