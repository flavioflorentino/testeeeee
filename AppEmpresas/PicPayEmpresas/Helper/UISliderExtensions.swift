import UIKit

// MARK: - Methods
extension UISlider {
    /// Return the center off the thumb
    func thumbCenterX() -> CGFloat {
        let _trackRect = trackRect(forBounds: self.bounds)
        let _thumbRect = thumbRect(forBounds: self.bounds, trackRect: _trackRect, value: value)
        return _thumbRect.origin.x + _thumbRect.width / 2
    }
}
