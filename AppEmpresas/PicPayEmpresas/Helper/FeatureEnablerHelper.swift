import FeatureFlag
enum FeatureEnablerHelper {
    static func shouldEnablePIX(seller: Seller?, isAdmin: Bool) -> Bool {
        guard let seller = seller else { return false }
        return FeatureManager.shared.isActive(.releasePIXPresenting) &&
            seller.digitalAccount &&
            seller.biometry &&
            isAdmin
    }
}
