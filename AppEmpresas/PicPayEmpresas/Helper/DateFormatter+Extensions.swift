extension DateFormatter {
    enum Format: String {
        case `default` = "dd/MM/yyyy"
        case monthName = "dd MMM/yy"
        case dayAndMonth = "dd/MM"
        case dateAndTime = "dd/MM/yyyy HH:mm"
        case timeAndDate = "HH:mm dd/MM/yy"
    }

    /// Format date as "dd MMM/yyyy"
    static func custom(value: Date, format: Format = .monthName) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format.rawValue
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "pt_BR_POSIX")
        return formatter.string(from: value)
    }
}
