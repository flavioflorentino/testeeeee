import Foundation

protocol EnvVariableProtocol {
    var isAnalyticsLogEnabled: Bool { get }
}

final class EnvVariable: EnvVariableProtocol {
    private enum Keys: String {
        case analyticsLog = "ANALYTICS_LOG"
    }
    
    private let appMode: AppMode
    private let environment: [String: String]
    
    var isAnalyticsLogEnabled: Bool {
        let stringValue = environment[Keys.analyticsLog.rawValue]
        let isVariableEnabled = stringValue == "enable"
        let isAppModeDebug = appMode == .debug
        return isAppModeDebug && isVariableEnabled
    }
    
    init(
        appMode: AppMode = AppMode.current,
        environment: [String: String] = ProcessInfo.processInfo.environment
    ) {
        self.appMode = appMode
        self.environment = environment
    }
}
