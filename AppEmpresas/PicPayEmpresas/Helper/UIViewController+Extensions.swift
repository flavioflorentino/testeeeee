import UIKit

extension UIViewController {
    func setupRightNavigationBarButton(_ button: UIButton) {
        let button = UIBarButtonItem(customView: button)
        navigationItem.rightBarButtonItem = button
    }
}
