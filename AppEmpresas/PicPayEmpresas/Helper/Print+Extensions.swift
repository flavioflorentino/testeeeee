import Foundation

// Override the swift print function to prevent logging on release version
public func debugPrint(_ items: @autoclosure () -> Any, separator: String = " ", terminator: String = "\n") {
    if AppMode.current == .debug {
        Swift.debugPrint(items(), separator: separator, terminator: terminator)
    }
}

func print(_ items: @autoclosure () -> Any, separator: String = " ", terminator: String = "\n") {
    if AppMode.current == .debug {
        Swift.print(items(), separator: separator, terminator: terminator)
    }
}
