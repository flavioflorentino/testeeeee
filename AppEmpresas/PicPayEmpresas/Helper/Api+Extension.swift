import Core
import LegacyPJ

extension ApiEndpointExposable {
    func headerWith(pin: String) -> [String: String] {
        ["pin": pin]
    }
}

extension Core.Api {
    func bizExecute<T: Decodable>(
        jsonDecoder: JSONDecoder = JSONDecoder(),
        completion: @escaping Completion
    ) where E == ResponseBiz<T> {
        execute(jsonDecoder: jsonDecoder) { result in
            switch result {
            case .success(let response):
                self.handleBizSuccess(response: response, completion: completion)
            case .failure(let apiError):
                self.handleApiError(apiError: apiError, completion: completion)
            }
        }
    }
}

private extension Core.Api {
    func handleBizSuccess<T: Decodable>(response: Success, completion: Completion) where E == ResponseBiz<T> {
        if let meta = response.model.meta, let code = meta.code, code != HTTPStatusCode.ok.rawValue {
            let requestError = RequestError(meta, data: response.data)
            completion(.failure(.badRequest(body: requestError)))
            return
        }
        
        completion(.success(response))
    }
    
    func handleApiError(apiError: ApiError, completion: Completion) {
        if let data = apiError.requestError?.jsonData,
            let errorBiz = try? JSONDecoder().decode(ErrorBiz.self, from: data) {
            completion(.failure(.badRequest(body: errorBiz.error)))
            return
        }

        guard let data = apiError.requestError?.jsonData,
            let errorResponse = try? JSONDecoder().decode(ResponseBiz<NoContent>.self, from: data),
            let meta = errorResponse.meta else {
            completion(.failure(apiError))
            return
        }
        
        let requestError = RequestError(meta, data: data)
        completion(.failure(.badRequest(body: requestError)))
    }
}

extension RequestError {
    init(_ meta: Meta, data: Data? = nil) {
        self.init()
        if let code = meta.code {
            self.code = "\(code)"
        }
        
        if let title = meta.errorTitle {
            self.title = title
        }
        
        if let message = meta.errorMessage {
            self.message = message
        }

        if let errorType = meta.errorType {
            self.field = errorType
        }
        
        jsonData = data
    }
    
    init(picPayError: LegacyPJError?) {
        self.init()
        
        guard let picPayError = picPayError else {
            return
        }
        
        code = "\(picPayError.code)"
        message = picPayError.message
        
        if let title = picPayError.title {
            self.title = title
        }
    }
}
