import Foundation

extension Notification.Name {
    enum Settings {
        static let updated = Notification.Name(rawValue: "SettingsHasUpdate")
    }
    
    enum User {
        static let updated = Notification.Name(rawValue: "UserHasUpdate")
    }
    
    enum Operator {
        static let listUpdated = Notification.Name(rawValue: "OperatorHasUpdatedList")
    }
    
    enum Remote {
        static let newTransaction = Notification.Name(rawValue: "NotificationNewTransaction")
    }
    
    enum Network {
        static let statusChanged = Notification.Name(rawValue: "NetworkStatusChanged")
    }
}
