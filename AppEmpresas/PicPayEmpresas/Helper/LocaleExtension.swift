extension Locale {
    static func currencySymbol(withIdentifier identifier: String = "pt_BR") -> String? {
        Locale(identifier: identifier).currencySymbol
    }
}
