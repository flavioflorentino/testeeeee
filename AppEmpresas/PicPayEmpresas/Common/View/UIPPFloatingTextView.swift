import UI
import UIKit

protocol MaskTextField {
    var maskBlock: ((_ oldText: String, _ newText: String) -> String)? { get set }
}

private extension UIPPFloatingTextView.Layout {
    enum Fonts {
        static let textView = UIFont.systemFont(ofSize: 16)
        static let topLabel = UIFont.systemFont(ofSize: 13)
    }
    
    enum Measures {
        static let `default`: CGFloat = 20
        static let lineHeight: CGFloat = 1
        static let topTextHeight: CGFloat = 17
        static let lineSelectedHeight: CGFloat = 2
    }
    
    enum Insets {
        static let textView = UIEdgeInsets(top: 3, left: 0, bottom: 3, right: 0)
    }
}

final class UIPPFloatingTextView: UIControl, MaskTextField {
    fileprivate enum Layout { }
    
    // MARK: Private
    override var isSelected: Bool {
        didSet {
            updateBottomLine()
        }
    }
    
    private var lineHeightContraint: CGFloat = 1
    private let bottomLine = UIView()
    private let topLabel = UILabel()
    private let infoButton = UIButton(type: .infoLight)
    
    // MARK: Public
    var selectedLineColor = Palette.ppColorBranding300.color
    var lineColor = Palette.ppColorGrayscale400.color
    var topText = "" {
        didSet {
            updateTopLabel()
        }
    }
    
    var errorMessage: String? = "" {
        didSet {
            guard hasError else {
                updateTopLabel()
                updateBottomLine()
                return
            }
            topLabel.text = errorMessage
            bottomLine.backgroundColor = Palette.ppColorNegative400.color
            topLabel.textColor = Palette.ppColorNegative400.color
        }
    }
    
    var hasError: Bool {
        !(errorMessage?.isEmpty ?? true)
    }
    
    var maskBlock: ((String, String) -> String)?
    let textView = UITextView()
    
    var infoButtonAction: (() -> Void)? {
        didSet {
            infoButton.isHidden = infoButtonAction == nil
        }
    }
    
    weak var delegate: UITextViewDelegate?
    
    // MARK: Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Helpers
    func setupBilletMask() {
        self.maskBlock = { oldValue, newValue in
            guard newValue.isNotEmpty else {
                return newValue
            }
            // Handling user input and programmatically updated text field
            let mask = newValue.billetMask()
            
            let nValue = mask.unmaskedText(from: newValue)
            return mask.maskedText(from: nValue) ?? oldValue
        }
    }
    
    private func updateTopLabel() {
        topLabel.text = topText
        topLabel.textColor = Palette.ppColorGrayscale400.color
    }
    
    private func updateBottomLine() {
        let lineThickness = isSelected ? Layout.Measures.lineSelectedHeight : Layout.Measures.lineHeight
        lineHeightContraint = lineThickness
        bottomLine.backgroundColor = isSelected ? selectedLineColor : lineColor
        if hasError {
            bottomLine.backgroundColor = Palette.ppColorNegative400.color
        }
    }
    
    // MARK: Actions
    @objc
    private func didTapInfo() {
        infoButtonAction?()
    }
}

extension UIPPFloatingTextView: ViewConfiguration {
    func configureViews() {
        backgroundColor = .clear
        
        infoButton.addTarget(self, action: #selector(didTapInfo), for: .touchUpInside)
        
        bottomLine.backgroundColor = Palette.ppColorGrayscale400.color
        
        textView.backgroundColor = .clear
        textView.textContainerInset = Layout.Insets.textView
        textView.isScrollEnabled = false
        textView.font = Layout.Fonts.textView
        textView.delegate = self
        textView.keyboardType = .numberPad
        
        topLabel.font = Layout.Fonts.topLabel
        topLabel.textColor = Palette.ppColorGrayscale400.color
    }
    
    func setupConstraints() {
        topLabel.layout {
            $0.top == topAnchor
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.height == Layout.Measures.topTextHeight
        }
        
        bottomLine.layout {
            lineHeightContraint = Layout.Measures.lineHeight
            $0.height == lineHeightContraint
            $0.bottom == bottomAnchor
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
        }
        
        textView.layout {
            $0.top == topLabel.bottomAnchor
            $0.bottom == bottomLine.topAnchor
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
        }
        
        infoButton.layout {
            $0.leading == topLabel.trailingAnchor
            $0.height == Layout.Measures.default
            $0.width == Layout.Measures.default
            $0.centerY == topLabel.centerYAnchor
        }
    }
    
    func buildViewHierarchy() {
        addSubview(textView)
        addSubview(bottomLine)
        addSubview(topLabel)
        addSubview(infoButton)
    }
}

extension UIPPFloatingTextView: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        delegate?.textViewDidBeginEditing?(textView)
        isSelected = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        delegate?.textViewDidEndEditing?(textView)
        isSelected = false
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if !isEnabled {
            return false
        }
        
        errorMessage = nil
        
        return delegate?.textView?(textView, shouldChangeTextIn: range, replacementText: text) ?? true
    }
}
