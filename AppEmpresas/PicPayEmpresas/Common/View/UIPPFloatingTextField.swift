import SkyFloatingLabelTextField
import UI
import UIKit

final class UIPPFloatingTextField: SkyFloatingLabelTextField {
    var alwaysShowPlaceholder = false

    /// float textfield title formatter
    var picpayTitleFormatter: ((String) -> String) = { (text: String) -> String in
        text
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.titleFormatter = picpayTitleFormatter
        setupColors()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func isTitleVisible() -> Bool {
        alwaysShowPlaceholder ? alwaysShowPlaceholder : super.isTitleVisible()
    }
    
    private func setupColors() {
        tintColor = Palette.ppColorBranding300.color
        textColor = Palette.ppColorGrayscale600.color
        placeholderColor = Palette.ppColorGrayscale400.color
        lineColor = Palette.ppColorGrayscale400.color
        titleColor = Palette.ppColorGrayscale400.color
        selectedLineColor = Palette.ppColorPositive300.color
        disabledColor = Palette.ppColorGrayscale400.color
        selectedTitleColor = Palette.ppColorGrayscale400.color(withCustomDark: .ppColorGrayscale000)
    }
}
