import CustomerSupport
import Foundation
import SupportSDK
import ZendeskCoreSDK

struct CustomerSupportSDKWrapper {
    private var url: String {
        if AppMode.current == .debug {
            return "https://picpay21589831114.zendesk.com"
        } else {
            return "https://ajudaempresas.picpay.com"
        }
    }
    
    var sellerID: String?
    
    private var mainRequestConfigutation: RequestUiConfiguration {
        let configuration = RequestUiConfiguration()
        var customFields: [CustomField] = []
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            customFields.append(CustomField(fieldId: CustomFieldIds.appVersion, value: version))
        }
        customFields.append(CustomField(fieldId: CustomFieldIds.osVersion, value: UIDevice.current.systemVersion))
        customFields.append(CustomField(fieldId: CustomFieldIds.platform, value: "iOS"))
        if let modelName = UIDevice.current.modelName {
            customFields.append(CustomField(fieldId: CustomFieldIds.model, value: "\(UIDevice.current.tradeName) | \(modelName)"))
        } else {
            customFields.append(CustomField(fieldId: CustomFieldIds.model, value: "\(UIDevice.current.tradeName) | Modelo indefinido"))
        }
        configuration.customFields = customFields
        
        configuration.tags = ["clientepj__motivo__outro"]
        
        return configuration
    }
    
    init(sellerID: String? = nil) {
        self.sellerID = sellerID
    }
    
    func setLocalizableFile(fileName: String) {
        ZDKLocalization.registerTableName(fileName)
    }
}

private extension CustomerSupportSDKWrapper {
    // swiftlint:disable number_separator
    enum CustomFieldIds {
        static var appVersion: Int64 {
            if AppMode.current == .debug {
                return 360031753511
            } else {
                return 360031611992
            }
        }
        
        static var osVersion: Int64 {
            if AppMode.current == .debug {
                return 360031684052
            } else {
                return 360031612012
            }
        }
        
        static var platform: Int64 {
            if AppMode.current == .debug {
                return 360031684032
            } else {
                return 360031685411
            }
        }
        
        static var model: Int64 {
            if AppMode.current == .debug {
                return 360031753891
            } else {
                return 360031685431
            }
        }
    }
    // swiftlint:enable number_separator
}

extension CustomerSupportSDKWrapper: ThirdPartySupportSDKContract {
    func activate(appId: String, clientId: String) {
        Zendesk.initialize(
            appId: appId,
            clientId: clientId,
            zendeskUrl: url
        )
        Support.initialize(withZendesk: Zendesk.instance)
    }
    
    func registerJWT(by userId: String) {
        if AppMode.current == .debug {
            // SANDBOX Credentials to Zendesk JWT authentication
            activate(
                appId: "b2345148ee1bf8054d8103054cc55ccb2aad616d287ce6f4",
                clientId: "mobile_sdk_client_c9e65207f0ba881fd2ba"
            )
        }
        let identity = Identity.createJwt(token: "biz_\(userId)")
        Zendesk.instance?.setIdentity(identity)
    }
    
    func registerAnonymous() {
        if AppMode.current == .debug {
            // SANDBOX Credentials to Zendesk Anonymous authentication
            activate(
                appId: "cb645a4c64dbcfb21e05e710580c3ad78ced64eabe44e941",
                clientId: "mobile_sdk_client_828b29cbd1c7bdafa43d"
            )
        }
        let identity = Identity.createAnonymous()
        Zendesk.instance?.setIdentity(identity)
    }
    
    func register(notificationToken: String) {
        guard let zendeskInstance = Zendesk.instance else {
            assertionFailure("Zendesk Instance is null")
            return
        }
        let locale = NSLocale.preferredLanguages.first ?? "pt-BR"
        ZDKPushProvider(zendesk: zendeskInstance).register(deviceIdentifier: "biz_\(notificationToken)", locale: locale) { _, error in
            if let error = error {
                assertionFailure("Couldn't register device: \(notificationToken). Error: \(error.localizedDescription)")
            }
        }
    }
    
    func setDebugMode(enabled: Bool) {
        CoreLogger.enabled = enabled
    }
    
    // swiftlint:disable discouraged_optional_collection
    /// Create a Ticket
    /// - Parameters:
    ///   - title: Ticket title
    ///   - message: message to be inside the ticket
    ///   - tags: Array of tags which can be used in a ticket
    ///   - extraCustomFields: Array of custom fields which can be used in a ticket
    ///   - completion: result from request with an only error parameter which if is null there is no error.
    public func requestTicket(
        title: String,
        message: String,
        tags: [String],
        extraCustomFields: [TicketCustomField],
        completion: @escaping (Result<Void, Error>) -> Void
    ) {
        let request = ZDKCreateRequest()
        request.subject = title
        request.requestDescription = message
        request.tags = tags

        let mappedCustomFields = extraCustomFields.compactMap { CustomField(fieldId: Int64($0.id), value: $0.value) }
        request.customFields = mappedCustomFields

        let provider = ZDKRequestProvider()
        provider.createRequest(request) { _, error in
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(()))
            }
        }
    }
    
    /// Verify if is any ticket open, based on *subjects*
    /// - Parameters:
    ///   - subjects: A list of subjects which a ticket can be inserted it
    ///   - completion: result with ticketId. If the result is nil. It was not any Ticket found it.
    public func isAnyTicketOpen(_ subjects: [String], completion: @escaping (_ ticketId: String?) -> Void) {
        let provider = ZDKRequestProvider()
        let notSolvedStatus = ["new", "open", "pending", "hold"]
        provider.getAllRequests { response, _ in
            let firstTicket = response?.requests.first { ticket in
                // Verifica se o ticket cumpri com os status
                guard notSolvedStatus.contains(ticket.status) else {
                    return false
                }

                guard !subjects.isEmpty else {
                    // Se a lista de Subjects ta vazia é considerado que se quer todos os assuntos, por isso ja retorna verdadeiro.
                    return true
                }

                guard let subject = ticket.subject else {
                    return false
                }

                return subjects.contains(subject)
            }
            completion(firstTicket?.requestId)
        }
    }
    
    func search(_ searchArticle: ArticlesResearch, completion: @escaping (Result<ResultArticlesResearch, Error>) -> Void) {
        let helpCenterProvider = ZDKHelpCenterProvider()
        let search = ZDKHelpCenterSearch()
        search.query = searchArticle.query
        search.page = NSNumber(value: searchArticle.page)
        helpCenterProvider.searchArticles(search) { arrayObjectResult, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let zendeskArticles = arrayObjectResult as? [ZDKHelpCenterArticle] else {
                let result = ResultArticlesResearch(articles: [], page: search.page.intValue)
                completion(.success(result))
                return
            }
            
            let articles = zendeskArticles.map {
                Article(id: $0.identifier.intValue, title: $0.title, description: $0.articleParents)
            }
            
            let result = ResultArticlesResearch(articles: articles, page: search.page.intValue)
            completion(.success(result))
        }
    }

    func articles(labels: [String], completion: @escaping (Result<ResultArticlesResearch, Error>) -> Void) {
        let helpCenterProvider = ZDKHelpCenterProvider()
        helpCenterProvider.getArticlesByLabels(labels) { arrayObjectResult, error in
            if let error = error {
                completion(.failure(error))
                return
            }

            guard let zendeskArticles = arrayObjectResult as? [ZDKHelpCenterArticle] else {
                let result = ResultArticlesResearch(articles: [], page: 0)
                completion(.success(result))
                return
            }

            let articles = zendeskArticles.map {
                Article(id: $0.identifier.intValue, title: $0.title, description: $0.articleParents)
            }

            let result = ResultArticlesResearch(articles: articles, page: 0)
            completion(.success(result))
        }
    }
    
    // MARK: FAQ
    
    func buildFAQController(option: FAQOptions) -> UIViewController {
        switch option {
        case .home:
            return HelpCenterUi.buildHelpCenterOverviewUi()
        case .section(let id):
            let helpCenterConfiguration = HelpCenterUiConfiguration()
            helpCenterConfiguration.groupType = .section
            return faqController(configuration: helpCenterConfiguration, id: id)
        case .category(let id):
            let helpCenterConfiguration = HelpCenterUiConfiguration()
            helpCenterConfiguration.groupType = .category
            return faqController(configuration: helpCenterConfiguration, id: id)
        case .article(let id):
            return HelpCenterUi.buildHelpCenterArticleUi(withArticleId: id)
        }
    }
    
    private func faqController(configuration: HelpCenterUiConfiguration, id: String) -> UIViewController {
        guard let intId = Int(id) else {
            return HelpCenterUi.buildHelpCenterOverviewUi()
        }
        let numberId = NSNumber(value: intId)
        configuration.groupIds = [numberId]
        return HelpCenterUi.buildHelpCenterOverviewUi(withConfigs: [mainRequestConfigutation, configuration])
    }
    
    // MARK: TicketList
    func buildTicketListController(ticketId: String? = nil) -> UIViewController {
        RequestUi.buildRequestList(with: [mainRequestConfigutation])
    }
    
    func buildFAQController() -> UIViewController {
        HelpCenterUi.buildHelpCenterOverviewUi(withConfigs: [mainRequestConfigutation])
    }
    
    func buildTicketListController() -> UIViewController {
        RequestUi.buildRequestList(with: [mainRequestConfigutation])
    }

    func articleBy(query: String) -> FAQOptions {
        .home
    }
}
