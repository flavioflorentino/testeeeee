import LegacyPJ

struct AuthManagerTrackingWrapper: AuthManagerTrackingWrapperProtocol {
    func userDidLogin(_ userId: String) {
        TrackingManager.userDidLogin(userId)
    }
    
    func createAliasForUser(_ userId: String) {
        TrackingManager.createAliasForUser(userId)
    }
    
    func trackLogout() {
        TrackingManager.trackLogout()
    }
    
    func updateDistinticId() {
        TrackingManager.updateDistinticId()
    }
}
