import FeatureFlag
import FirebaseRemoteConfig
import AnalyticsModule

public struct RemoteConfigWrapper: ThirdPartyRemoteConfigContract {
    public let instance: RemoteConfig
    
    public func activate() {
        instance.activate(completion: nil)
    }
    
    public func fetch(withExpirationDuration: TimeInterval, successHandler: @escaping () -> Void) {
        instance.fetch(withExpirationDuration: withExpirationDuration) { status, _ in
            guard status == .success else { return }
            successHandler()
        }
    }
    
    public func object<T>(type: T.Type, for key: String) -> T? {
        switch type {
        case is Bool.Type:
            return instance[key].boolValue as? T
        case is String.Type:
            return instance[key].stringValue as? T
        case is NSNumber.Type:
            return instance[key].numberValue as? T
        case is Int.Type:
            return instance[key].numberValue.intValue as? T
        default:
            return instance[key] as? T
        }
    }
}
