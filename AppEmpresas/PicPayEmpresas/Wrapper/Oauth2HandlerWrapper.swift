import LegacyPJ

final class OAuth2HandlerWrapper: OAuth2HandlerWrapperProtocol {
    func logoutUserForInvalidToken() {
        AppManager.shared.logoutUserForInvalidToken()
    }
    
    func displayWelcome() {
        AppCoordinator().displayWelcome()
    }
}
