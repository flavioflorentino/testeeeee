import AccountManagementPJ
import Foundation
import LegacyPJ

struct AccountUserDataCoordinatorWrapper {
    private let seller: Seller
    private let user: User
    private let address: Address
    
    private weak var navigationController: UINavigationController?
    
    init(seller: Seller, user: User, address: Address, navigationController: UINavigationController?) {
        self.seller = seller
        self.user = user
        self.address = address
        self.navigationController = navigationController
    }
}

// MARK: - AccountUserDataCoordinatorWrapping

extension AccountUserDataCoordinatorWrapper: AccountUserDataCoordinatorWrapping {
    func navigateToName() {
        let controller: PersonalFormViewController = ViewsManager.instantiateViewController(.settings)
        pushViewController(controller)
    }
    
    func navigateToEmail() {
        let controller: EmailFormViewController = ViewsManager.instantiateViewController(.settings)
        let model = UpdateEmailFormViewModel()
        model.user = user
        controller.setup(model: model)
        pushViewController(controller)
    }
    
    func navigateToPhone() {
        let controller: PhoneFormViewController = ViewsManager.instantiateViewController(.settings)
        let model = UpdatePhoneFormViewModel(user: user, seller: seller)
        controller.setup(model: model)
        pushViewController(controller)
    }
    
    func navigateToAddress() {
        let controller: AddressFormViewController = ViewsManager.instantiateViewController(.settings)
        let model = UpdateAddressFormViewModel()
        model.address = address
        model.address.isResidentialAddress = true
        controller.setup(model: model)
        pushViewController(controller)
    }
    
    func navigateToCompanyAddress() {
        // To be added
    }
}

// MARK: - Private Methods

private extension AccountUserDataCoordinatorWrapper {
    func pushViewController(_ controller: UIViewController, hidesBottomBar: Bool = true) {
        controller.hidesBottomBarWhenPushed = hidesBottomBar
        navigationController?.pushViewController(controller, animated: true)
    }
}
