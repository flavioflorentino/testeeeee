import Core
import AnalyticsModule
import AppsFlyerLib
import Mixpanel
import EventTracker
import FirebaseAnalytics
import LegacyPJ

private protocol AnalyticsDefaultable { }

extension AnalyticsDefaultable {
    private var defaultProperties: [String: Any] {
        ["installation_id": UIDevice.current.identifierForVendor?.uuidString ?? 0]
    }
    
    func mergingDefaults(with properties: [String: Any]) -> [String: Any] {
        properties.merging(defaultProperties) { first, _ in first }
    }
}

struct AppsFlyerWrapper: AppsFlyerContract {
    private typealias Dependencies = HasKVStore
    private let container: Dependencies = DependencyContainer()
    let instance: AppsFlyerLib
    
    func track(_ event: String, properties: [String: Any]) {
        var allProperties = container.kvStore.dictionaryFor(BizKvKey.globalTrackValues) ?? [:] as [String: Any]
        allProperties.merge(properties, uniquingKeysWith: { _, current in
            current
        })
        instance.logEvent(event, withValues: allProperties)
    }
}

struct MixPanelWrapper: MixPanelContract {
    private typealias Dependencies = HasAuthManager
    private let container: Dependencies = DependencyContainer()
    let instance: MixpanelInstance

    func track(_ event: String, properties: [String: Any]) {
        updateDistinctIdIfNeeded()
        let properties = properties as? Properties
        instance.track(event: event, properties: properties)
    }
    
    func time(_ event: String) {
        updateDistinctIdIfNeeded()
        instance.time(event: event)
    }

    func reset() {
        instance.reset()
    }

    private func updateDistinctIdIfNeeded() {
        guard !container.authManager.isAuthenticated,
              instance.distinctId != EventTrackerManager.tracker.distinctId else { return }

        instance.identify(distinctId: EventTrackerManager.tracker.distinctId)
    }
}

struct EventTrackerWrapper: EventTrackerContract {
    let instance: EventTracking

    func track(_ event: String, properties: [String: Any]) {
        instance.track(eventName: event, properties: properties.mapValues(AnyCodable.init))
    }

    func reset() {
        instance.reset()
    }
}

struct FirebaseWrapper: FirebaseContract, AnalyticsDefaultable {
    func track(_ event: String, properties: [String: Any]) {
        FirebaseAnalytics.Analytics.logEvent(event, parameters: mergingDefaults(with: properties))
    }
}
