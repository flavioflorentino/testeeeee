import AnalyticsModule
enum SettingsAnalytics: AnalyticsKeyProtocol {
    case addBusiness
    case editLogo
    case logoUpdated
    case editName
    case nameUpdated
    case editAddress
    case updatedAddress
    case paymentType
    case scannerActivated(activated: Bool)
    case splitConditions
    case updatedSplit(condition: Int)
    case fee
    case manageOperators
    case bankAccount
    case bankAccountUpdated
    case businessData
    case businessAdded
    case phone
    case email
    case personalData
    case password
    case forgotPassword
    case webMonitor
    case promoCode
    case feeUpdated(_ days: String, fee: Double)
    case operatorAdded(_ userName: String)
    case operatorRemoved(_ userName: String)
    case webMonitorToken

    var name: String {
        switch self {
        case .addBusiness:
            return "Settings - Add Business"
        case .editLogo:
            return "Settings - Edit Logo"
        case .logoUpdated:
            return "Settings - Logo Updated"
        case .editName:
            return "Settings - Edit Name"
        case .nameUpdated:
            return "Settings - Name Updated"
        case .editAddress:
            return "Settings - Edit Address"
        case .updatedAddress:
            return "Settings - Address Updated"
        case .paymentType:
            return "Settings - Payments Type"
        case .scannerActivated:
            return "Settings - Scanner Only Activated"
        case .splitConditions:
            return "Settings - Split Conditions"
        case .updatedSplit(let condition):
            return "Settings - Split Condition \(condition)x Updated"
        case .fee:
            return "Settings - Fee"
        case .manageOperators:
            return "Settings - Manage Operators"
        case .bankAccount:
            return "Settings - Bank Account"
        case .bankAccountUpdated:
            return "Settings - Bank Account Updated"
        case .businessData:
            return "Settings - Bank Business Data"
        case .businessAdded:
            return "Settings - Business Added"
        case .phone:
            return "Settings - Phone"
        case .email:
            return "Settings - Email"
        case .personalData:
            return "Settings - Personal Data"
        case .password:
            return "Settings - Change Password"
        case .forgotPassword:
            return "Settings - Recover Password"
        case .webMonitor:
            return "Settings - Monitor Web"
        case .promoCode:
            return "Settings - Add Promo Code"
        case .feeUpdated:
            return "Fee-Settings Updated"
        case .operatorAdded:
            return "Operator Added"
        case .operatorRemoved:
            return "Operator Removed"
        case .webMonitorToken:
            return "Web Monitor Token Inserted"
        }
    }

    private var properties: [String: Any] {
        switch self {
        case .scannerActivated(let activated):
            return ["activated": activated]
        case let .feeUpdated(days, fee):
            let stringFee = String(format: "%.2f", fee)
            return ["days": days, "fee": stringFee]
        case let .operatorAdded(userName):
            return ["username": userName]
        case let .operatorRemoved(userName):
            return ["username": userName]
        case .forgotPassword:
            return [
                "recovery_pass_flow": "logged_in",
                "recovery_pass_method": "email"
            ]
        default:
            return [:]
        }
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
    }
}
