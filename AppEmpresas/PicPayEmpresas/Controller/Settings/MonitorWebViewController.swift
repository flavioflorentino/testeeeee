import AnalyticsModule
import UIKit

final class MonitorWebViewController: FormViewController {
    var model: WebMonitorViewModel?
    private let dependencies: HasAnalytics = DependencyContainer()
    
    @IBOutlet private var code: UIRoundedTextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        code.becomeFirstResponder()
        dependencies.analytics.log(SettingsAnalytics.webMonitor)
    }
    
    @IBAction func accessAction(_ sender: Any) {
        if let _code = self.code.text {
            if _code.isNotEmpty {
                self.startLoading()
                self.model?.authorize(token: _code, { [weak self] error in
                    if let error = error {
                        UIAlertController(error: error).show()
                    } else {
                        UIAlertController(title: "Monitor vinculado com sucesso").show()
                        self?.dependencies.analytics.log(SettingsAnalytics.webMonitorToken)
                    }
                    self?.code?.text = ""
                    self?.stopLoading()
                    self?.code?.becomeFirstResponder()
                })
            }
        }
    }
    
    func setup(model: WebMonitorViewModel) {
        self.model = model
    }
}
