import AccountManagementPJ
import AnalyticsModule
import InputMask
import FeatureFlag
import LegacyPJ
import SDWebImage
import SVProgressHUD
import PIX
import UI
import UIKit

extension SettingsTableViewController.Layout {
    enum Footer {
        static let height: CGFloat = 20.0
    }
}

final class SettingsTableViewController: BaseTableViewController, UINavigationControllerDelegate {
    private typealias Localizable = Strings.LegacySettingsTableView
    private typealias PixLocalizable = Strings.SettingsTableView.Pix
    typealias Dependecies = HasFeatureManager & HasAnalytics & HasAppCoordinator
    fileprivate enum Layout {}
    
    // MARK: - Public Properties
    var viewModel: SettingsViewModel?
    
    // MARK: - Private Properties
    private var tableHeaderView: UIView?
    private var sections: [SettingsSection] = [SettingsSection]()
    private var automaticWithdrawalRow: SettingsRow?
    private var dependencies: Dependecies = DependencyContainer()
    
    // MARK: - IB Outlets
    @IBOutlet var logoImage: UIImageView!
    @IBOutlet var logoActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var companyNameLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        configureTableView()
        configureLegacySections()
        setupClosures()
        setupNotifications()
        
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceLegacy())
    }
    
    @IBAction func changeAccount(_ sender: Any) {
        let controller: AccountTableViewController = ViewsManager.instantiateViewController(.settings)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - Dependence Injection
    override func configureDependencies() {
        setup(viewModel: SettingsDataViewModel(dependencies: DependencyContainer()))
        super.configureDependencies()
    }
    
    func setup(viewModel: SettingsViewModel) {
        self.viewModel = viewModel
        isReady = true
    }
    
    // MARK: - User Actions
    @objc
    func showUploadLogoOptions() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(title: "Escolher na biblioteca", style: .default, isEnabled: true) { [weak self] _ in
            // open the image picker
            let imagePicker = UIImagePickerController()
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            
            self?.present(imagePicker, animated: true, completion: nil)
        }
        alert.addAction(title: Strings.Default.cancel, style: .cancel, isEnabled: true) { _ in }
        present(alert, animated: true, completion: nil)
        dependencies.analytics.log(SettingsAnalytics.editLogo)
    }
    
    /// Logout account
    func logout(_ userAuth: UserAuth) {
        let actionSheet = UIAlertController(title: nil, message: "Desconectar da sua conta?", preferredStyle: .actionSheet)
        actionSheet.addAction(title: "Desconectar", style: .destructive, isEnabled: true) { _ in
            SVProgressHUD.show()
            AuthManager.shared.logout(userAuth: userAuth) { _ in
                if AuthManager.shared.authenticatedUsers.isEmpty {
                    if self.dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
                        self.dependencies.appCoordinator.displayWelcome()
                    } else {
                        AppManager.shared.presentUnloggedViewController()
                    }
                } else {
                    if self.dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
                        self.dependencies.appCoordinator.displayAuthenticated()
                    } else {
                        AppManager.shared.presentAuthenticatedViewController()
                    }
                }
                SVProgressHUD.dismiss()
            }
        }
        
        actionSheet.addAction(title: Strings.Default.cancel, style: .cancel, isEnabled: true, handler: nil)
        present(actionSheet, animated: true, completion: nil)
    }
    
    /// Open an string
    // swiftlint:disable:next cyclomatic_complexity function_body_length
    func open(_ screen: SettingsRow.Screen) {
        switch screen {
        case .operatorManagement:
            let controller: OperatorTableViewController = ViewsManager.instantiateViewController(.settings)
            pushViewController(controller)
        case .editAddress:
            let controller: AddressFormViewController = ViewsManager.instantiateViewController(.settings)
            let model = UpdateAddressFormViewModel()
            model.address = self.viewModel?.seller.store?.address ?? Address()
            controller.setup(model: model)
            pushViewController(controller)
        case .editName:
            let controller: NameFormViewController = ViewsManager.instantiateViewController(.settings)
            let model = UpdateNameFormViewModel()
            model.seller = self.viewModel?.seller ?? Seller()
            controller.setup(model: model)
            pushViewController(controller)
        case .editReceiveDay:
            let controller: DayFormViewController = ViewsManager.instantiateViewController(.settings)
            let model = UpdateDayFormViewModel()
            model.feeSelectedDay = self.viewModel?.seller.daysToReleaseCc ?? 0
            controller.setup(model: model)
            pushViewController(controller)
        case .editCnpjCompanyName:
            let controller: CnpjFormViewController = ViewsManager.instantiateViewController(.settings)
            let model = UpdateCnpjFormViewModel(dependencies: DependencyContainer(), seller: self.viewModel?.seller ?? Seller())
            controller.setup(model: model)
            pushViewController(controller)
        case .editBankAccount:
            guard let viewModel = viewModel else {
                return
            }

            let controller: UIViewController

            if dependencies.featureManager.isActive(.releaseBankAccountWarning) {
                controller = BankAccountTipsFactory.makeEditTips(companyType: viewModel.companyType, seller: viewModel.seller)
                dependencies.analytics.log(BankAccountTipsAnalytics.toCheck)
            } else {
                controller = BankAccountFactory.make(viewModel.seller)
            }

            pushViewController(controller)
        case .editAccountInfo:
            guard
                let seller = viewModel?.seller,
                let user = viewModel?.user,
                let address = viewModel?.seller.store?.address,
                let navigationController = navigationController
            else {
                return
            }
            let wrapper = AccountUserDataCoordinatorWrapper(
                seller: seller,
                user: user,
                address: address,
                navigationController: navigationController
            )
            AccountManagementPJ.FlowCoordinator.start(
                navigationController: navigationController,
                userDataCoordinatorWrapper: wrapper
            )
        case .editPersonalData:
            let controller: PersonalFormViewController = ViewsManager.instantiateViewController(.settings)
            pushViewController(controller)
        case .editPassword:
            let controller: PasswordFormViewController = ViewsManager.instantiateViewController(.settings)
            if let user = viewModel?.user {
                controller.setup(model: UpdatePasswordFormViewModel(user: user))
            }
            pushViewController(controller)
        case .editEmail:
            let controller: EmailFormViewController = ViewsManager.instantiateViewController(.settings)
            let model = UpdateEmailFormViewModel()
            if let user = self.viewModel?.user {
                model.user = user
            }
            controller.setup(model: model)
            pushViewController(controller)
        case .monitorWeb:
            let controller: MonitorWebViewController = ViewsManager.instantiateViewController(.settings)
            let model = WebMonitorImpViewModel()
            controller.setup(model: model)
            pushViewController(controller)
        case .editPhone:
            let controller: PhoneFormViewController = ViewsManager.instantiateViewController(.settings)
            guard let user = viewModel?.user, let seller = viewModel?.seller else {
                return
            }
            
            let model = UpdatePhoneFormViewModel(user: user, seller: seller)
            controller.setup(model: model)
            pushViewController(controller)
        case .configPicpayPayment:
            guard let seller = viewModel?.seller else {
                return
            }
            let controller: PaymentConfigViewController = ViewsManager.instantiateViewController(.settings)
            let model = PaymentConfigImpViewModel(seller: seller)
            controller.setup(model: model)
            pushViewController(controller)
        case .campaign:
            let controller = CampaignHomeFactory.make()
            pushViewController(controller)
        case .configInstallments:
            let controller: InstallmentsTableViewController = ViewsManager.instantiateViewController(.settings)
            pushViewController(controller)
        case .termsOfUse:
            let controller = ViewsManager.webViewController(url: Endpoint.urlTermsOfUse(), title: Strings.Default.navTermsOfUseTitle)
            pushViewController(controller)
        case .privacyPolicy:
            let controller = ViewsManager.webViewController(url: "https://www.picpay.com/br/politica-de-privacidade", title: Strings.Default.navPrivacyPolicyTitle)
            pushViewController(controller)
        case .addAccount:
            if dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
                guard let navigationController = navigationController else {
                    return
                }
                dependencies.appCoordinator.displayAccountAddition(with: navigationController)
            } else {
                pushViewController(UnloggedFactory.make(isRoot: false))
            }
        case .helpFAQ:
            BIZCustomerSupportManager(accessToken: AuthManager.shared.userAuth?.auth.accessToken).pushFAQ(from: self)
        case .helpContactUs:
            BIZCustomerSupportManager(accessToken: AuthManager.shared.userAuth?.auth.accessToken).pushTicketList(from: self)
        case .promoCode:
            let controller: PromoCodeFormViewController = ViewsManager.instantiateViewController(.settings)
            pushViewController(controller)
        case .automaticWithdrawal:
            guard let viewModel = viewModel else { return }
            let status = viewModel.seller.automaticWithdrawal
            let controller = AutomaticWithdrawalFactory.make(initialEnabled: status, viewModelDelegate: viewModel)
            pushViewController(controller)
        case .printQRCode:
            let controller = ChooseQRCodePrintFactory.make()
            pushViewController(controller)
        case .applyMaterial:
            let controller = MaterialFlowControlFactory.make()
            let navigationController = UINavigationController(rootViewController: controller)
            present(navigationController, animated: true)
        case .myPixKeys:
            let flow = PIXBizFlowCoordinator(with: navigationController ?? UINavigationController())
            flow.perform(flow: .hub)
        case .myPixLimit:
            let controller = DailyLimitFactory.makePJ()
            pushViewController(controller)
        case .editWhatsApp:
            let controller = RegisterWhatsAppNumberFactory.make()
            pushViewController(controller)
        #if DEBUG
        case .featureFlagControl:
            guard let controller = FeatureFlagControlHelper.isEnabled ? FeatureFlagControl.shared.controller() : nil else {
                return
            }
            pushViewController(controller)
        #endif
        }
    }
    
    override func refresh(_ control: UIRefreshControl?) {
        control?.beginRefreshing()
        self.loadData(animateLoading: false)
    }
    
    override func stopLoading() {
        tableView.tableHeaderView = UIView()
        tableView.tableFooterView = UIView()
        configureAppVersionOnTableFooter()
    }
    
    override func startLoading() {
        tableView.tableHeaderView = self.loadingView
    }
}

// MARK: - UITableViewDataSource & UITableViewDelegate

extension SettingsTableViewController {
    // ------ Table Cell --------
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = sections[indexPath.section].rows[indexPath.row]
        
        switch row.cell {
        case .normal:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: row.cell.rawValue) as? SettingsTableViewCell else { break }
            cell.configureCell(row)
            row.cellConfigureAdpter?.configureCellAdpter(cell: cell, data: row.data)
            return cell
            
        case .subtitle:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: row.cell.rawValue) as? SettingsDetailTableViewCell else { break }
            cell.configureCell(row)
            row.cellConfigureAdpter?.configureCellAdpter(cell: cell, data: row.data)
            return cell
           
        case .logout:
            let cell = tableView.dequeueReusableCell(withIdentifier: row.cell.rawValue) as? SettingsLogoutTableViewCell
            cell?.configureCell(row)
            if let cell = cell {
                return cell
            }
            
        default:
            break
        }
        
        return UITableViewCell()
    }
        
    override func numberOfSections(in tableView: UITableView) -> Int {
        self.sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard section < sections.count else {
            return 0
        }
        return sections[section].rows.count
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if !sections[section].title.isEmpty {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 30))
            view.backgroundColor = UIColor.clear
            
            let label = UILabel(frame: CGRect(x: 20, y: 0, width: tableView.frame.size.width - 40, height: 30))
            label.font = UIFont.systemFont(ofSize: 11)
            label.textColor = #colorLiteral(red: 0.4470588235, green: 0.4470588235, blue: 0.4470588235, alpha: 1)
            label.text = sections[section].title
            
            view.addSubview(label)
            
            return view
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = sections[indexPath.section].rows[indexPath.row]
        switch row.action {
        case .open(let screen):
            self.open(screen)
        
        case .logout(let authUser):
            self.logout(authUser)
           
        case .uploadLogo:
            self.showUploadLogoOptions()
            
        default:
            return
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        sections[section].title.isEmpty ? 0.0 : 30.0
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 20))
        view.backgroundColor = UIColor.clear
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        Layout.Footer.height
    }
}

// MARK: Private Methods
private extension SettingsTableViewController {
    func setupNotifications() {
        let center = NotificationCenter.default
        let settings = Notification.Name.Settings.updated
        let user = Notification.Name.User.updated
        
        center.addObserver(self, selector: #selector(loadData), name: settings, object: nil)
        center.addObserver(self, selector: #selector(loadUserData), name: user, object: nil)
    }
    
    private func configureView() {
        tableHeaderView = tableView.tableHeaderView
        tableView.tableHeaderView = nil
        
        logoImage.isUserInteractionEnabled = true
        logoImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showUploadLogoOptions)))
    }
    
    private func setupClosures() {
        viewModel?.didUpdateAutomaticWithdrawalStatus = { [weak self] status in
            self?.automaticWithdrawalRow?.value = status
            self?.tableView.reloadData()
        }
    }
    
    private func configureData() {
        if let image = viewModel?.seller.imageUrl {
            logoImage.sd_setImage(with: URL(string: image), placeholderImage: Assets.iluSellerPlaceholder.image)
        } else {
            logoImage.image = Assets.iluSellerPlaceholder.image
        }
        
        logoImage.layer.cornerRadius = 35.0
        logoImage.clipsToBounds = true
        
        companyNameLabel.text = viewModel?.seller.name
        
        populateAddress()
    }
    
    /// Load all settings data
    @objc
    private func loadData(animateLoading: Bool = true) {
        if animateLoading {
            startLoading()
        }
        
        viewModel?.loadSettingsData { [weak self] error in
            self?.refreshControl?.endRefreshing()
            self?.stopLoading()
            
            if let error = error {
                UIAlertController(error: error).show()
            } else {
                self?.tableView.tableHeaderView = self?.tableHeaderView
                self?.configureTableView()
                self?.configureData()
                self?.tableView.reloadData()
            }
        }
    }
    
    /// Load only user data
    @objc
    private func loadUserData() {
        viewModel?.loadSettingsUserData { [weak self] _ in
            self?.configureTableView()
            self?.configureData()
            self?.tableView.reloadData()
            self?.refreshControl?.endRefreshing()
        }
    }

    private func populateAddress() {
        let address = viewModel?.seller.store?.address
        
        var addressString = address?.address ?? ""
        addressString += address?.number == nil ? "" : ", \(address?.number ?? "")."
        addressString += (address?.complement?.isEmpty ?? true) ? "" : "\n\(address?.complement ?? "")"
        addressString += "\n"
        addressString += address?.district == nil ? "" : "\(address?.district ?? ""), "
        
        if let city = viewModel?.seller.store?.address?.city, let state = viewModel?.seller.store?.address?.state {
            addressString += "\(city) - \(state)"
        }
        
        addressLabel.text = addressString
    }
    
    /// Configure the sections, rowns and table view isues
    private func configureTableView() {
        registerCell(SettingsRow.Cell.normal.rawValue, nib: "SettingsTableViewCell")
        registerCell(SettingsRow.Cell.subtitle.rawValue, nib: "SettingsDetailTableViewCell")
        registerCell(SettingsRow.Cell.logout.rawValue, nib: "SettingsLogoutTableViewCell")
        registerCell(SettingsRow.Cell.configSwitch.rawValue, nib: "SettingsSwitchTableViewCell")
        configureLegacySections()
    }
    
    private func setupProfileSection() -> SettingsSection {
        var profileRows: [SettingsRow] = [
            SettingsRow(Localizable.changeCompanyLogo, cell: .normal, action: .uploadLogo),
            SettingsRow(Localizable.changeName, cell: .normal, action: .open(.editName)),
            SettingsRow(Localizable.changeAddress, cell: .normal, action: .open(.editAddress))
        ]
        
        if dependencies.featureManager.isActive(.isBusinessWhatsappAvailable) {
            profileRows.append(SettingsRow(Localizable.editWhatsApp, cell: .normal, action: .open(.editWhatsApp)))
        }
        
        return SettingsSection(Localizable.perfil, rows: profileRows)
    }

    private func setupPixSection() -> SettingsSection {
        var rows: [SettingsRow] = []

        let pixRow = SettingsRow(PixLocalizable.myKeys, cell: .normal, action: .open(.myPixKeys))
        rows.append(pixRow)

        if dependencies.featureManager.isActive(.isPixDailyLimitAvailablePJ) {
            rows.append(SettingsRow(PixLocalizable.myDailyLimit, cell: .normal, action: .open(.myPixLimit)))
        }

        return SettingsSection(PixLocalizable.title, rows: rows)
    }
    
    private func setupStoresSection() -> SettingsSection {
        let hasBankError = viewModel?.hasBankError ?? false
        let bankIcon: UIImage? = hasBankError ? Assets.errorIcon.image : nil
        
        var storeRowsArray: [SettingsRow] = [
            SettingsRow(Localizable.paymentOptions, cell: .normal, action: .open(.configPicpayPayment)),
            SettingsRow(Localizable.installmentInterests, cell: .normal, action: .open(.configInstallments)),
            SettingsRow(Localizable.comercialConditions, cell: .normal, action: .open(.editReceiveDay)),
            SettingsRow(Localizable.manageOperators, cell: .normal, action: .open(.operatorManagement)),
            SettingsRow(Localizable.bankAccount, icon: bankIcon, cell: .normal, action: .open(.editBankAccount))
        ]
        
        let isEnabled = viewModel?.seller.automaticWithdrawal == true
        let isEnabledText = isEnabled ? Localizable.enabled : Localizable.disabled
        let automaticWithdrawalRow = SettingsRow(
            Localizable.automaticWithdrawal,
            value: isEnabledText,
            cell: .normal,
            action: .open(.automaticWithdrawal)
        )
        self.automaticWithdrawalRow = automaticWithdrawalRow
        storeRowsArray.append(automaticWithdrawalRow)
        
        let companyRow = SettingsRow(Localizable.companyData, subtitle: Localizable.cnpjAndSocialName, cell: .normal, action: .open(.editCnpjCompanyName))
        storeRowsArray.append(companyRow)
        
        storeRowsArray.append(SettingsRow(Localizable.addAccount, cell: .normal, action: .open(.addAccount)))
        
        if dependencies.featureManager.isActive(.releaseCashbackPresentingBool) {
            let campaignRow = SettingsRow(Localizable.campaigns, cell: .normal, action: .open(.campaign))
            storeRowsArray.insert(campaignRow, at: 0)
        }
        
        return SettingsSection(Localizable.storeOptions, rows: storeRowsArray)
    }
    
    private func setupPersonalSection() -> SettingsSection {
        var phoneWithMask = Mask.maskString(input: viewModel?.user.phone ?? "", mask: "([00]) [00009]-[0000]")
        var emailWithMask = viewModel?.user.email ?? ""
        
        if dependencies.featureManager.isActive(.featureAppSecObfuscationMask) {
            if let phone = StringObfuscationMasker.mask(phone: viewModel?.user.phone) {
                phoneWithMask = phone
            }
            if let email = StringObfuscationMasker.mask(email: viewModel?.user.email) {
                emailWithMask = email
            }
        }

        let personalSection = SettingsSection(Localizable.personalConfigurations, rows: [
            SettingsRow(Localizable.myPhone, value: phoneWithMask, cell: .normal, action: .open(.editPhone)),
            SettingsRow(Localizable.myEmail, value: emailWithMask, cell: .normal, action: .open(.editEmail)),
            SettingsRow(Localizable.personalData, subtitle: Localizable.nameCPFBirthdate, cell: .subtitle, action: .open(.editPersonalData)),
            SettingsRow(Localizable.changePassword, cell: .normal, action: .open(.editPassword))
        ])
        
        return personalSection
    }
    
    private func setupMonitorSection() -> SettingsSection {
        SettingsSection(
            Localizable.computerTransactions,
            rows: [SettingsRow(Localizable.webMonitor, cell: .normal, action: .open(.monitorWeb))])
    }
    
    private func setupMoreSection() -> SettingsSection {
        var sectionRows: [SettingsRow] = [
            SettingsRow(
                Localizable.faq, 
                cell: .normal, 
                action: .open(.helpFAQ)),
            SettingsRow(
                Localizable.contactUs, 
                cell: .normal, 
                action: .open(.helpContactUs), 
                data: nil, 
                cellAdpter: ContactUsCellAdapter()),
            SettingsRow(
                Localizable.useTerms, 
                cell: .normal, 
                action: .open(.termsOfUse))
        ]

        if dependencies.featureManager.isActive(.isAccountShutdownAvailable) {
            let accountShutDownRow = SettingsRow(Localizable.accountShutDown, cell: .normal)
            sectionRows.append(accountShutDownRow)
        }
        return SettingsSection(Localizable.more, rows: sectionRows)
    }
    
    private func setupLogoutSection() -> SettingsSection? {
        // Logout for all authenticated users
        if AuthManager.shared.authenticatedUsers.count > 1 {
            var rows: [SettingsRow] = []
            for authUser in AuthManager.shared.authenticatedUsers {
                var username = authUser.user.username ?? ""
                username = authUser.user.name ?? username
                
                rows.append(SettingsRow(Localizable.disconnect + username, cell: .logout, action: .logout(authUser)))
            }
            
            return SettingsSection("", rows: rows)
        } else if let authUser = AuthManager.shared.userAuth {
            let logoutRows = [SettingsRow(Localizable.disconnect, cell: .logout, action: .logout(authUser))]
            return SettingsSection("", rows: logoutRows)
        }
        
        return nil
    }
    
    private func setupMaterialSections() -> SettingsSection {
        var rows: [SettingsRow] = []
        
        if dependencies.featureManager.isActive(.allowApplyMaterial) {
            let applyMaterialRow = SettingsRow(Localizable.applyMaterial, cell: .normal, action: .open(.applyMaterial))
            rows.append(applyMaterialRow)
        }
        
        if dependencies.featureManager.isActive(.allowQRCodeFlow) {
            let printQRCodeRow = SettingsRow(Localizable.printQRCode, cell: .normal, action: .open(.printQRCode))
            rows.append(printQRCodeRow)
        }
        
        return SettingsSection(Localizable.materialTitleSection, rows: rows)
    }
    
    private func configureLegacySections() {
        guard !dependencies.featureManager.isActive(.isNewAccountManagementAvailable) else {
            configureSections()
            return
        }

        sections.removeAll()
        
        if viewModel?.seller.name != nil {
            sections.append(setupProfileSection())
            sections.append(setupStoresSection())
        }
        let shouldEnablePixSection = FeatureEnablerHelper.shouldEnablePIX(
            seller: viewModel?.seller,
            isAdmin: viewModel?.user.isAdmin ?? false
        )
        if shouldEnablePixSection {
            sections.append(setupPixSection())
        }

        sections.append(setupMaterialSections())
        if viewModel?.user.id != nil {
            sections.append(setupPersonalSection())
        }
        
        if viewModel?.seller.name != nil {
            sections.append(setupMonitorSection())
        }
      
        let promoSection = SettingsSection(Localizable.promotions, rows: [
            SettingsRow(Localizable.addPromotionalCode, cell: .normal, action: .open(.promoCode) )
        ])
        sections.append(promoSection)
        
        sections.append(setupMoreSection())

        #if DEBUG
        if FeatureFlagControlHelper.isEnabled {
            sections.append(SettingsSection(String(), rows: instantiateFeatureFlagControl()))
        }
        #endif
        
        if let logoutSection = setupLogoutSection() {
            sections.append(logoutSection)
        }
    }

    #if DEBUG
    private func instantiateFeatureFlagControl() -> [SettingsRow] {
        [SettingsRow(Localizable.featureFlagControl, cell: .logout, action: .open(.featureFlagControl))]
    }
    #endif
    
    private func configureSections() {
        sections = viewModel?.setupSections() ?? []
    }

    private func configureAppVersionOnTableFooter() {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 40))
        let versionLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 40))
        versionLabel.font = UIFont.systemFont(ofSize: 11.0)
        versionLabel.textColor = UIColor.lightGray
        versionLabel.textAlignment = .center
        
        if let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String {
            versionLabel.text = Strings.Default.appVersion(versionNumber)
        }
        footerView.addSubview(versionLabel)
        
        tableView.tableFooterView = footerView
    }
}

// MARK: - TableViewCellConfigureAdpter
extension ContactUsCellAdapter.Layout {
    enum View {
        static let tag: Int = 88_425
    }
    
    enum Badge {
        static let size = CGRect(x: 115, y: 15, width: 15, height: 15)
    }
}

final class ContactUsCellAdapter: TableViewCellConfigureAdpter {
    enum Layout {}
    func configureCellAdpter(cell: UITableViewCell, data: Any?) {
        guard let numberOfNotification = data as? Int else {
            return
        }
        
        if let badge = cell.viewWithTag(Layout.View.tag) as?  UINotificationBadge {
            badge.text = "\(numberOfNotification)"
        } else {
            let badge = UINotificationBadge(frame: Layout.Badge.size)
            badge.text = "\(numberOfNotification)"
            badge.sizeToFit()
            badge.tag = Layout.View.tag
            cell.addSubview(badge)
        }
    }
}
