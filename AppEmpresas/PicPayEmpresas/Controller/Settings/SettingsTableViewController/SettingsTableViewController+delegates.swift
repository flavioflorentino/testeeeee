import AnalyticsModule
import Core
import Foundation
import LegacyPJ
import UIKit

// MARK: - UIImagePickerControllerDelegate

extension SettingsTableViewController: UIImagePickerControllerDelegate {
    private func setupLogo(image: UIImage) {
        let dependencies: HasAnalytics = DependencyContainer()
        logoImage.image = image
        logoImage.layer.opacity = 0.5
        logoActivityIndicator.startAnimating()
        dependencies.analytics.log(SettingsAnalytics.logoUpdated)
    }
    
    private func uploadLogo(_ image: UIImage) {
        let resizedImage = image.resizedImageForUpload()
        guard let resized = resizedImage, let imageData = resized.jpegData(compressionQuality: 0.8) else {
            return
        }
        
        DispatchQueue.main.async {
            // Ask for user password
            AuthManager.shared.performActionWithAuthorization { [weak self] result in
                guard case let .success(password) = result else {
                    return
                }
                let currentLogo = self?.logoImage.image
                self?.setupLogo(image: image)
                
                self?.viewModel?.uploadLogo(password: password, image: imageData, { error in
                    self?.logoActivityIndicator.stopAnimating()
                    self?.logoImage.layer.opacity = 1.0
                    if let error = error {
                        self?.logoImage.image = currentLogo
                        UIAlertController(error: error).show()
                    }
                })
            }
        }
    }

    @objc
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        dismiss(animated: true) {
            if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
                self.uploadLogo(pickedImage)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
