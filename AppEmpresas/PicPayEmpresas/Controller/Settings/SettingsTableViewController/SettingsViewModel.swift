import AccountManagementPJ
import Core
import CoreSellerAccount
import FeatureFlag
import Foundation
import InputMask
import LegacyPJ
import UI

protocol SettingsViewModel: AutomaticWithdrawalViewModelDelegate {
    // MARK: Properties
    var seller: Seller { get }
    var user: User { get }
    var didUpdateAutomaticWithdrawalStatus: ((String) -> Void)? { get set }
    var hasBankError: Bool { get }
    var companyType: CompanyType { get }
    
    // MARK: Methods
    
    /// Load all Settings Data
    func loadSettingsData(_ completion:@escaping( (_ error: LegacyPJError?) -> Void))
    
    /// Load only user data
    func loadSettingsUserData(_ completion:@escaping( (_ error: LegacyPJError?) -> Void))
    
    /// Upload the seller logo
    func uploadLogo(password: String, image: Data, _ completion:@escaping( (_ error: LegacyPJError?) -> Void))
    func setupSections() -> [SettingsSection]
}

final class SettingsDataViewModel: SettingsViewModel {
    typealias Dependecies = HasAuthManager & HasKVStore & HasFeatureManager
    typealias Localizable = Strings.LegacySettingsTableView
    private typealias ProfileLocalizable = Strings.SettingsTableView.ProfileSection
    private typealias ReceivementsLocalizable = Strings.SettingsTableView.ReceivementsSection
    private typealias AccountInfoLocalizable = Strings.SettingsTableView.AccountInfoSection
    private typealias PromotionsLocalizalble = Strings.SettingsTableView.PromotionsSection
    private typealias HelpLocalizable = Strings.SettingsTableView.HelpSection
    private typealias DisconnectLocalizable = Strings.SettingsTableView
    private typealias PixLocalizable = Strings.SettingsTableView.Pix
    
    // MARK: - Properties
    
    private(set) var seller: Seller
    private(set) var user: User
    private(set) var hasBankError = false

    var companyType: CompanyType {
        let isIndividualCompany = dependencies.kvStore.boolFor(BizKvKey.individualCompany)
        return isIndividualCompany ? .individual : .multiPartners
    }
    
    var didUpdateAutomaticWithdrawalStatus: ((String) -> Void)?
    var apiSeller: ApiSeller
    var apiUser: ApiUser
    
    private let homeService = HomeService(dependencies: DependencyContainer())
    private let dependencies: Dependecies
    
    init(dependencies: Dependecies, apiSeller: ApiSeller = ApiSeller(), apiUser: ApiUser = ApiUser()) {
        self.apiSeller = apiSeller
        self.apiUser = apiUser
        self.dependencies = dependencies
        
        self.seller = Seller()
        self.user = User()
    }
    
    // MARK: - Public Methdos
    
    /// Load all Settings Data
    /// - Load the seller data
    /// - Load the user data
    func loadSettingsData(_ completion:@escaping( (_ error: LegacyPJError?) -> Void)) {
        let group = DispatchGroup()
        
        var error: LegacyPJError?
        
        group.enter()
        fetchData { fetchedError in
            error = fetchedError
            group.leave()
        }
        
        group.enter()
        checkBankAccountErrors {
            group.leave()
        }
        
        group.notify(queue: .main) { [weak self] in
            if let error = error {
                completion(error)
                return
            }
            
            self?.loadSettingsUserData(completion)
        }
    }
    
    func loadSettingsUserData(_ completion:@escaping( (_ error: LegacyPJError?) -> Void)) {
        apiUser.data { [weak self] user, error in
            if let user = user {
                self?.user = user
            }
            
            completion(error)
        }
    }
    
    /// Upload the seller logo
    func uploadLogo(password: String, image: Data, _ completion:@escaping( (_ error: LegacyPJError?) -> Void)) {
        let deviceToken: String?

        if dependencies.featureManager.isActive(.isAppLocalDataUnavailable) {
            deviceToken = dependencies.kvStore.stringFor(BizKvKey.deviceToken)
        } else {
            deviceToken = AppManager.shared.localData.deviceToken
        }

        let updateModel = SellerLogoUpdateModel(
            pin: password,
            seller: seller,
            deviceToken: deviceToken ?? "",
            image: image
        )
        
        apiSeller.updateLogo(updateModel: updateModel) { _, error in
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
    
    func successOnUpdateAutomaticWithdrawalStatus(status: Bool) {
        let value = status ? Localizable.enabled : Localizable.disabled
        seller.automaticWithdrawal = status
        didUpdateAutomaticWithdrawalStatus?(value)
    }

    func setupSections() -> [SettingsSection] {
        guard seller.name != nil else {
            return [setupPromotionsSection(), setupHelpSection(), setupDisconnectSection()]
        }

        var sections: [SettingsSection] = []
        sections.append(setupProfileSection())
        sections.append(setupReceivementsSection())
        let shouldEnablePixSection = FeatureEnablerHelper.shouldEnablePIX(
            seller: seller,
            isAdmin: user.isAdmin
        )
        if shouldEnablePixSection {
            sections.append(setupPixSection())
        }
        sections.append(setupAccountInfoSection())
        sections.append(setupPromotionsSection())
        sections.append(setupHelpSection())

        #if DEBUG
        if FeatureFlagControlHelper.isEnabled {
            sections.append(setupFeatureFlag())
        }
        #endif

        sections.append(setupDisconnectSection())

        return sections
    }

    private func setupProfileSection() -> SettingsSection {
        var rows: [SettingsRow] = [
            SettingsRow(ProfileLocalizable.changeImage, cell: .normal, action: .uploadLogo),
            SettingsRow(ProfileLocalizable.changeName, cell: .normal, action: .open(.editName)),
            SettingsRow(ProfileLocalizable.changeAddress, cell: .normal, action: .open(.editAddress))
        ]
        
        if dependencies.featureManager.isActive(.isBusinessWhatsappAvailable) {
            rows.append(SettingsRow(ProfileLocalizable.editWhatsApp, cell: .normal, action: .open(.editWhatsApp)))
        }
        
        return SettingsSection(ProfileLocalizable.title, rows: rows)
    }

    private func setupPixSection() -> SettingsSection {
        var rows: [SettingsRow] = []

        rows.append(SettingsRow(PixLocalizable.myKeys, cell: .normal, action: .open(.myPixKeys)))

        if dependencies.featureManager.isActive(.isPixDailyLimitAvailablePJ) {
            rows.append(SettingsRow(PixLocalizable.myDailyLimit, cell: .normal, action: .open(.myPixLimit)))
        }

        return SettingsSection(PixLocalizable.title, rows: rows)
    }

    private func setupReceivementsSection() -> SettingsSection {
        var rows: [SettingsRow] = []
        
        if dependencies.featureManager.isActive(.allowApplyMaterial) {
            let applyMaterialRow = SettingsRow(ReceivementsLocalizable.applyMaterial, cell: .normal, action: .open(.applyMaterial))
            rows.append(applyMaterialRow)
        }
        
        let bankIcon: UIImage? = hasBankError ? Assets.errorIcon.image : nil
        
        rows += [
            SettingsRow(ReceivementsLocalizable.installmentInterests, cell: .normal, action: .open(.configInstallments)),
            SettingsRow(ReceivementsLocalizable.comercialConditions, cell: .normal, action: .open(.editReceiveDay)),
            SettingsRow(ReceivementsLocalizable.bankAccount, icon: bankIcon, cell: .normal, action: .open(.editBankAccount))
        ]
        
        let isEnabledText = seller.automaticWithdrawal == true ? Localizable.enabled : Localizable.disabled
        let automaticWithdrawalRow = SettingsRow(
            ReceivementsLocalizable.automaticWithdrawal,
            value: isEnabledText,
            cell: .normal,
            action: .open(.automaticWithdrawal)
        )
        rows.append(automaticWithdrawalRow)

        rows.append(SettingsRow(ReceivementsLocalizable.paymentOptions, cell: .normal, action: .open(.configPicpayPayment)))
        
        if dependencies.featureManager.isActive(.allowQRCodeFlow) {
            let printQRCodeRow = SettingsRow(ReceivementsLocalizable.printQRCode, cell: .normal, action: .open(.printQRCode))
            rows.append(printQRCodeRow)
        }

        if dependencies.featureManager.isActive(.releaseCashbackPresentingBool) {
            rows.append(SettingsRow(ReceivementsLocalizable.campaigns, cell: .normal, action: .open(.campaign)))
        }
        
        return SettingsSection(ReceivementsLocalizable.title, rows: rows)
    }

    private func setupAccountInfoSection() -> SettingsSection {
        var warningIcon: UIImage?
        if dependencies.featureManager.isActive(.isAccountInfoRenewalAvailable), seller.renewalRequired {
            warningIcon = UIImage(
                iconName: Iconography.exclamationTriangle.rawValue,
                font: Typography.icons(.large).font(),
                color: Colors.warning600.color,
                size: CGSize(width: 32, height: 32)
            )
        }
        
        let rows: [SettingsRow] = [
            SettingsRow(AccountInfoLocalizable.accountInfo, icon: warningIcon, cell: .normal, action: .open(.editAccountInfo)),
            SettingsRow(AccountInfoLocalizable.changePassword, cell: .normal, action: .open(.editPassword)),
            SettingsRow(AccountInfoLocalizable.manageOperators, cell: .normal, action: .open(.operatorManagement)),
            SettingsRow(AccountInfoLocalizable.webMonitor, cell: .normal, action: .open(.monitorWeb)),
            SettingsRow(AccountInfoLocalizable.addAccount, cell: .normal, action: .open(.addAccount))
        ]
        
        return SettingsSection(AccountInfoLocalizable.title, rows: rows)
    }

    private func setupPromotionsSection() -> SettingsSection {
        let rows: [SettingsRow] = [
            SettingsRow(PromotionsLocalizalble.addPromotionalCode, cell: .normal, action: .open(.promoCode))
        ]
        return SettingsSection(PromotionsLocalizalble.title, rows: rows)
    }

    private func setupHelpSection() -> SettingsSection {
        var rows: [SettingsRow] = [
            SettingsRow(HelpLocalizable.faq, cell: .normal, action: .open(.helpFAQ)),
            SettingsRow(
                HelpLocalizable.contactUs, 
                cell: .normal, 
                action: .open(.helpContactUs), 
                data: nil, 
                cellAdpter: ContactUsCellAdapter()),
            SettingsRow(HelpLocalizable.useTerms, cell: .normal, action: .open(.termsOfUse))
        ]

        if dependencies.featureManager.isActive(.isAccountShutdownAvailable) {
            let accountShutDownRow = SettingsRow(Localizable.accountShutDown, cell: .normal)
            rows.append(accountShutDownRow)
        }

        return SettingsSection(HelpLocalizable.title, rows: rows)
    }

    private func setupDisconnectSection() -> SettingsSection {
        let moreUsersConnected = dependencies.authManager.authenticatedUsers.count > 1 
        let localizablePrefix = moreUsersConnected ? DisconnectLocalizable.disconnectMore : DisconnectLocalizable.disconnectOne
        var rows: [SettingsRow] = []
        
        for authUser in AuthManager.shared.authenticatedUsers {
            let username = (authUser.user.name ?? authUser.user.username) ?? ""
            let cellTitle = moreUsersConnected ? localizablePrefix + username : localizablePrefix

            rows.append(SettingsRow(cellTitle, cell: .logout, action: .logout(authUser)))
        }

        return SettingsSection("", rows: rows)
    }
}

private extension SettingsDataViewModel {
    func fetchData(_ completion: @escaping((_ error: LegacyPJError?) -> Void)) {
        apiSeller.data { [weak self] result in
            switch result {
            case .success(let seller):
                self?.seller = seller
                
                guard let token = self?.dependencies.authManager.userAuth?.auth.accessToken else { return }
                let sellerId = seller.id
                let bizCustomerSupportManager = BIZCustomerSupportManager(accessToken: token)
                bizCustomerSupportManager.registerUser()
                bizCustomerSupportManager.registerNotification(sellerID: "\(sellerId)")
                completion(nil)
                
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    func checkBankAccountErrors(_ completion: @escaping() -> Void) {
        guard dependencies.featureManager.isActive(.releaseBankAccountWarning) else {
            completion()
            return
        }
        
        homeService.checkBankAccount { result in
            switch result {
            case .success(let account):
                self.hasBankError = account?.validationIssue != nil
            case .failure:
                break
            }
            completion()
        }
    }
}

#if DEBUG
extension SettingsViewModel {
    private typealias FeatureFlagLocalizable = Strings.SettingsTableView.FeatureFlagSection

    func setupFeatureFlag() -> SettingsSection {
        let rows = [SettingsRow(FeatureFlagLocalizable.featureFlagControl, cell: .logout, action: .open(.featureFlagControl))]
        return SettingsSection(FeatureFlagLocalizable.title, rows: rows)
    }
}
#endif
