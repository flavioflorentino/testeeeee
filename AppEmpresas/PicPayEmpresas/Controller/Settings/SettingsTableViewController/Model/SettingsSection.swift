final class SettingsSection {
    let title: String
    let rows: [SettingsRow]

    init(_ title: String, rows: [SettingsRow]) {
        self.title = title
        self.rows = rows
    }
}
