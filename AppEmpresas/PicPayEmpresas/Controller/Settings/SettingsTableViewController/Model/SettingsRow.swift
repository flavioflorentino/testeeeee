import LegacyPJ
import UIKit

final class SettingsRow {
    enum Cell: String {
        case normal
        case subtitle
        case logout
        case configSwitch
    }
    
    enum Action {
        case none
        case uploadLogo
        case open(Screen)
        case custom(String)
        case logout(UserAuth)
    }
    
    enum Screen {
        case operatorManagement
        case editAddress
        case editName
        case editReceiveDay
        case editCnpjCompanyName
        case editBankAccount
        case editAccountInfo
        case editPersonalData
        case editPassword
        case editEmail
        case editPhone
        case monitorWeb
        case campaign
        case configPicpayPayment
        case configInstallments
        case termsOfUse
        case privacyPolicy
        case addAccount
        case helpFAQ
        case helpContactUs
        case promoCode
        case automaticWithdrawal
        case printQRCode
        case applyMaterial
        case myPixKeys
        case myPixLimit
        case editWhatsApp
        #if DEBUG
        case featureFlagControl
        #endif
    }
    
    let title: String
    let subtitle: String
    var value: String?
    let icon: UIImage?
    let cell: Cell
    let action: Action
    let data: Any?
    let cellConfigureAdpter: TableViewCellConfigureAdpter?
    
    init(
        _ title: String,
        subtitle: String = "",
        value: String? = nil,
        icon: UIImage? = nil,
        cell: Cell = .normal,
        action: Action = .none,
        data: Any? = nil,
        cellAdpter: TableViewCellConfigureAdpter? = nil
    ) {
        self.title = title
        self.subtitle = subtitle
        self.value = value
        self.icon = icon
        self.cell = cell
        self.action = action
        self.data = data
        self.cellConfigureAdpter = cellAdpter
    }
}
