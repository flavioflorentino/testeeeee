import AnalyticsModule
import LegacyPJ
import SVProgressHUD
import UIKit
import FeatureFlag

final class AccountTableViewController: BaseTableViewController {
    // MARK: - Public Properties
    
    var model: AccountListViewModel?
    
    // MARK: - Private Properties
    
    private let dependencies: HasAnalytics & HasAppCoordinator & HasFeatureManager = DependencyContainer()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        
        isPullRefresh = false
        dependencies.analytics.log(SettingsAnalytics.addBusiness)
    }
    
    // MARK: User Actions
    
    @IBAction func addAccount(_ sender: Any) {
        if dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
            guard let navigationController = navigationController else {
                return
            }
            dependencies.appCoordinator.displayAccountAddition(with: navigationController)
        } else {
            pushViewController(UnloggedFactory.make(isRoot: false))
        }
    }
    
    // MARK: - Dependence Injection
    
    override func configureDependencies() {
        self.setup(model: AccountDataListViewModel())
        super.configureDependencies()
    }
    
    func setup(model: AccountListViewModel) {
        self.model = model
        isReady = true
    }
    
    func isAuthenticatedUser(indexPath: IndexPath) -> Bool {
        guard let model = model else {
            return false
        }
        
        return model.userAuthForRow(at: indexPath) == model.authenticatedUser()
    }
    
    func logoutUser(at indexPath: IndexPath) {
        guard let userAuth = model?.userAuthForRow(at: indexPath) else { return }
        
        let message = Strings.Default.disconnectMessage(userAuth.user.username ?? Strings.Default.disconnectUser)
        
        let actionSheet = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        actionSheet.addAction(title: Strings.Default.disconnect, style: .destructive, isEnabled: true) { [weak self] _ in
            SVProgressHUD.show()
            AuthManager.shared.logout(userAuth: userAuth) { _ in
                if AuthManager.shared.authenticatedUsers.isEmpty {
                    if self?.dependencies.featureManager.isActive(.isAppCoordinatorAvailable) == true {
                        self?.dependencies.appCoordinator.displayWelcome()
                    } else {
                        AppManager.shared.presentUnloggedViewController()
                    }
                }
                self?.tableView.reloadData()
                SVProgressHUD.dismiss()
            }
        }
        
        actionSheet.addAction(title: Strings.Default.cancel, style: .cancel, isEnabled: true, handler: nil)
        present(actionSheet, animated: true, completion: nil)
    }
    
    // MARK: - Internal Methods
    func configureTableView() {
        registerCell("account", nib: "AccountTableViewCell")
        tableView.rowHeight = 64.0
        
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9607843137, alpha: 1)
    }
    
    // MARK: - TableViewDelegate / TableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model?.numberOfRows() ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = model else {
            return UITableViewCell()
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "account") as? AccountTableViewCell {
            let userAuth = model.userAuthForRow(at: indexPath)
            cell.configureCell(userAuth)
            cell.accessoryType = .none
            if let authenticatedUser = model.authenticatedUser(), userAuth == authenticatedUser {
                cell.accessoryType = .checkmark
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let userAuth = model?.userAuthForRow(at: indexPath) else {
            return
        }
        
        if let authenticatedUser = model?.authenticatedUser(), userAuth != authenticatedUser {
            SVProgressHUD.show(withStatus: "Trocando de conta")
            
            let currentAuthUser = AuthManager.shared.userAuth
            
            AuthManager.shared.changeCurrentAccount(userAuth: userAuth) { [weak self] error in
                if let error = error {
                    UIAlertController(error: error).show()
                    // If response is 401 it means the operator was deleted from another device
                    if error.code == 401 {
                        AuthManager.shared.removeUserAuth(userAuth: userAuth)
                        tableView.reloadData()
                    }
                    // Re auth the current user
                    if let user = currentAuthUser {
                        AuthManager.shared.changeCurrentAccount(userAuth: user, completion: { _ in
                        })
                    }
                } else {
                    self?.navigationController?.dismiss(animated: true, completion: nil)
                    if self?.dependencies.featureManager.isActive(.isAppCoordinatorAvailable) == true {
                        self?.dependencies.appCoordinator.displayAuthenticated()
                    } else {
                        AppManager.shared.presentAuthenticatedViewController()
                    }
                }
                SVProgressHUD.dismiss()
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        !isAuthenticatedUser(indexPath: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            logoutUser(at: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        Strings.Default.disconnect
    }
}
