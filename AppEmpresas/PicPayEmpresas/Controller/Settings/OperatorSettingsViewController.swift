import Core
import InputMask
import LegacyPJ
import SDWebImage
import SVProgressHUD
import UIKit
import FeatureFlag
import AnalyticsModule

final class OperatorSettingsViewController: BaseTableViewController, UINavigationControllerDelegate {
    private typealias Dependencies = HasAppCoordinator & HasFeatureManager & HasAnalytics
    // MARK: - Public Properties
    
    var model: SettingsViewModel?
    
    // MARK: - Private Properties
    
    private var tableHeaderView: UIView?
    private var sections: [SettingsSection] = [SettingsSection]()
    private let dependencies: Dependencies = DependencyContainer()

    // MARK: - IB Outlets
    @IBOutlet var imageView: UIImageView?
    @IBOutlet var imageActivityIndicator: UIActivityIndicatorView?
    @IBOutlet var nameLabel: UILabel!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    @IBAction func changeAccount(_ sender: Any) {
        let controller: AccountTableViewController = ViewsManager.instantiateViewController(.settings)
        pushViewController(controller)
    }
    
    // MARK: - Dependence Injection
    override func configureDependencies() {
        self.setup(model: SettingsDataViewModel(dependencies: DependencyContainer()))
        super.configureDependencies()
    }
    
    func setup(model: SettingsViewModel) {
        self.model = model
        isReady = true
    }
    
    func configureData() {
        // logo
        if let image = self.model?.seller.imageUrl {
            imageView?.sd_setImage(with: URL(string: image), placeholderImage: Assets.iluOperatorPlaceholder.image)
        } else {
            imageView?.image = Assets.iluOperatorPlaceholder.image
        }
        
        imageView?.layer.cornerRadius = 35.0
        imageView?.clipsToBounds = true
        
        // data
        if let user = AuthManager.shared.userAuth?.user {
            var name = user.name ?? ""
            if let username = user.username {
                name = username
            }
            nameLabel.text = name
        }
        
        // Activate Push
        guard let sellerId = model?.seller.id else { return }
        BIZCustomerSupportManager().registerNotification(sellerID: "\(sellerId)")
    }
    
    /// Configure the sections, rowns and table view isues
    func configureTableView() {
        registerCell(SettingsRow.Cell.normal.rawValue, nib: "SettingsTableViewCell")
        registerCell(SettingsRow.Cell.subtitle.rawValue, nib: "SettingsDetailTableViewCell")
        registerCell(SettingsRow.Cell.logout.rawValue, nib: "SettingsLogoutTableViewCell")
        registerCell(SettingsRow.Cell.configSwitch.rawValue, nib: "SettingsSwitchTableViewCell")
        configureSections()
    }
    
    func configureSections() {
        sections.removeAll()
        
        let moreSection = SettingsSection("", rows: [
            SettingsRow("Adicionar conta", cell: .normal, action: .open(.addAccount)),
            SettingsRow("Perguntas frequentes", cell: .normal, action: .open(.helpFAQ)),
            SettingsRow("Fale conosco", cell: .normal, action: .open(.helpContactUs), data: nil, cellAdpter: ContactUsCellAdapter())
        ])
        sections.append(moreSection)
        
        let monitorWebSection = SettingsSection("VER TRANSAÇÕES NO COMPUTADOR", rows: [
            SettingsRow("Monitor Web", cell: .normal, action: .open(.monitorWeb))
        ])
        sections.append(monitorWebSection)
        
        // Logout for all authenticated users
        if AuthManager.shared.authenticatedUsers.count > 1 {
            var rows: [SettingsRow] = []
            for authUser in AuthManager.shared.authenticatedUsers {
                var username = authUser.user.username ?? ""
                username = authUser.user.name ?? username
                
                rows.append( SettingsRow("Desconectar: " + username, cell: .logout, action: .logout(authUser) ))
            }
            
            let logoutSection = SettingsSection("", rows: rows)
            sections.append(logoutSection)
        } else {
            if let authUser = AuthManager.shared.userAuth {
                let logoutSection = SettingsSection("", rows: [
                    SettingsRow("Desconectar", cell: .logout, action: .logout(authUser) )
                ])
                sections.append(logoutSection)
            }
        }
    }
    
    func loadData() {
        configureTableView()
        configureData()
        tableView.reloadData()
        refreshControl?.endRefreshing()
        configureTableFooter()
    }
    
    // MARK: - User Actions
    
    /// Logout account
    func logout(_ userAuth: UserAuth) {
        let actionSheet = UIAlertController(title: nil, message: "Desconectar da sua conta?", preferredStyle: .actionSheet)
        actionSheet.addAction(title: "Desconectar", style: .destructive, isEnabled: true) { _ in
            SVProgressHUD.show()
            AuthManager.shared.logout(userAuth: userAuth) { _ in
                self.dependencies.analytics.log(LoginAnalytics.userSignedOut)
                if AuthManager.shared.authenticatedUsers.isEmpty {
                    if self.dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
                        self.dependencies.appCoordinator.displayWelcome()
                    } else {
                        AppManager.shared.presentUnloggedViewController()
                    }
                } else {
                    if self.dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
                        self.dependencies.appCoordinator.displayAuthenticated()
                    } else {
                        AppManager.shared.presentAuthenticatedViewController()
                    }
                }
                SVProgressHUD.dismiss()
            }
        }
        
        actionSheet.addAction(title: Strings.Default.cancel, style: .cancel, isEnabled: true, handler: nil)
        present(actionSheet, animated: true, completion: nil)
    }
    
    /// Open an string
    func open(_ screen: SettingsRow.Screen) {
        switch screen {
        case .addAccount:
            if dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
                guard let navigationController = navigationController else {
                    return
                }
                dependencies.appCoordinator.displayAccountAddition(with: navigationController)
            } else {
                pushViewController(UnloggedFactory.make(isRoot: false))
            }
        case .helpFAQ:
            BIZCustomerSupportManager(accessToken: AuthManager.shared.userAuth?.auth.accessToken).presentFAQ(from: self, with: model?.seller)
            
        case .helpContactUs:
            BIZCustomerSupportManager(accessToken: AuthManager.shared.userAuth?.auth.accessToken).presentTicketList(from: self, with: model?.seller)
            
        case .monitorWeb:
            let controller: MonitorWebViewController = ViewsManager.instantiateViewController(.settings)
            controller.hidesBottomBarWhenPushed = true
            let model = WebMonitorImpViewModel()
            controller.setup(model: model)
            navigationController?.pushViewController(controller, animated: true)
            
        default:
            break
        }
    }
    
    override func refresh(_ control: UIRefreshControl?) {
        control?.beginRefreshing()
        self.loadData()
    }
    
    func configureTableFooter() {
        // Add app version on tableviewer footer
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40))
        
        let versionLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 40))
        versionLabel.font = UIFont.systemFont(ofSize: 11.0)
        versionLabel.textColor = UIColor.lightGray
        versionLabel.textAlignment = .center
        
        if let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String {
            versionLabel.text = Strings.Default.appVersion(versionNumber)
        }
        view.addSubview(versionLabel)
        
        tableView.tableFooterView = view
    }
    
    override func stopLoading() {
        tableView.tableFooterView = UIView()
        configureTableFooter()
    }
    
    /// Upload the new seller logo
    func uploadLogo(_ image: UIImage) {
        let resizedImage = image.resizedImageForUpload()
        guard let resized = resizedImage, let imageData = resized.jpegData(compressionQuality: 1.0) else {
            return
        }
        
        DispatchQueue.main.async {
            // Ask for user password
            AuthManager.shared.performActionWithAuthorization { [weak self] result in
                switch result {
                case .success(let password):
                    
                    // update the view with the selected image
                    let currentLogo = self?.imageView?.image
                    self?.imageView?.image = image
                    self?.imageView?.layer.opacity = 0.5
                    self?.imageActivityIndicator?.startAnimating()
                    
                    self?.model?.uploadLogo(password: password, image: imageData, { error in
                        self?.imageActivityIndicator?.stopAnimating()
                        self?.imageView?.layer.opacity = 1.0
                        if let error = error {
                            self?.imageView?.image = currentLogo
                            UIAlertController(error: error).show()
                        }
                    })
                default:
                    break
                }
            }
        }
    }
    
    // MARK: - UITableViewDataSource / UITableViewDelegate
    // ------ Table Cell --------
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = sections[indexPath.section].rows[indexPath.row]
        switch row.cell {
        case .normal:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: row.cell.rawValue) as? SettingsTableViewCell else { break }
            cell.configureCell(row)
            row.cellConfigureAdpter?.configureCellAdpter(cell: cell, data: row.data)
            return cell
            
        case .subtitle:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: row.cell.rawValue) as? SettingsDetailTableViewCell else { break }
            cell.configureCell(row)
            row.cellConfigureAdpter?.configureCellAdpter(cell: cell, data: row.data)
            return cell
            
        case .logout:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: row.cell.rawValue) as? SettingsLogoutTableViewCell
                else {
                    break
                }
            cell.configureCell(row)
            return cell
            
        default:
            break
        }
        
        return UITableViewCell()
    }
    
    // ------ Table Counts --------
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        sections[section].rows.count
    }
    
    // ------  Sections Header -------
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if !sections[section].title.isEmpty {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 30))
            view.backgroundColor = UIColor.clear
            
            let label = UILabel(frame: CGRect(x: 20, y: 0, width: tableView.frame.size.width - 40, height: 30))
            label.font = UIFont.systemFont(ofSize: 11)
            label.textColor = #colorLiteral(red: 0.4470588235, green: 0.4470588235, blue: 0.4470588235, alpha: 1)
            label.text = sections[section].title
            
            view.addSubview(label)
            
            return view
        }
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 20))
        view.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9529411765, blue: 0.9568627451, alpha: 1)
        return view
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = sections[indexPath.section].rows[indexPath.row]
        switch row.action {
        case .open(let screen):
            self.open(screen)
        case .logout(let authUser):
            self.logout(authUser)
        default:
            return
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        sections[section].title.isEmpty ? 0.0 : 30.0
    }
    
    // ------- Section Footer ---------
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 20))
        view.backgroundColor = UIColor.clear
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        20.0
    }
}

extension OperatorSettingsViewController: UIImagePickerControllerDelegate {
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        dismiss(animated: true) {
            if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
                self.uploadLogo(pickedImage)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
