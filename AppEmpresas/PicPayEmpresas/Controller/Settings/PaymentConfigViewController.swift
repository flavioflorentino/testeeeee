import AnalyticsModule
import LegacyPJ
import SVProgressHUD
import UIKit

final class PaymentConfigViewController: BaseViewController {
    // MARK: - Public Properties
       
    var model: PaymentConfigViewModel?
    private let dependencies: HasAnalytics = DependencyContainer()
    
    // MARK: - IBOutlet
    
    @IBOutlet private var configSwitch: UISwitch!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        guard let model = model else {
            return
        }
        
        super.viewDidLoad()
        
        configSwitch.addTarget(self, action: #selector(changeConfig), for: .valueChanged)
        configSwitch.isOn = model.restrictToScanner
        dependencies.analytics.log(SettingsAnalytics.paymentType)
    }
    
    @objc
    func changeConfig() {
        if configSwitch.isOn {
            let alert = UIAlertController(title: Strings.Default.alertSettingsPaymentConfigTitle, message: Strings.Default.alertSettingsPaymentConfigMsg, preferredStyle: .alert)
            alert.addAction(title: Strings.Default.cancel, style: .cancel, isEnabled: true) { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.configSwitch.isOn.toggle()
            }
            alert.addAction(title: Strings.Default.yes, style: .default, isEnabled: true) { [weak self] _ in
                self?.performChangeConfig()
            }
            present(alert, animated: true, completion: nil)
        } else {
            performChangeConfig()
        }
    }
    
    // MARK: - Dependence Injection
    
    override func configureDependencies() {
        self.setup(model: PaymentConfigImpViewModel())
        super.configureDependencies()
    }
    
    func setup(model: PaymentConfigViewModel) {
        self.model = model
        isReady = true
    }
    
    // MARK: - Internal Methods
    
    // MARK: - User Actions
    
    func performChangeConfig() {
        AuthManager.shared.performActionWithAuthorization { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            
            switch result {
            case .success(let password):
                SVProgressHUD.show()
                strongSelf.model?.savePaymentConfig(password: password, isOn: strongSelf.configSwitch.isOn, {[weak self] error in
                    guard let strongSelf = self else {
                        return
                    }
                    
                    if let error = error {
                        UIAlertController(error: error).show()
                        strongSelf.configSwitch.isOn.toggle()
                    } else {
                        strongSelf.dependencies.analytics.log(SettingsAnalytics.scannerActivated(activated: strongSelf.configSwitch.isOn))
                        NotificationCenter.default.post(name: Notification.Name.Settings.updated, object: nil)
                    }
                    SVProgressHUD.dismiss()
                })
            default:
                strongSelf.configSwitch.isOn.toggle()
                SVProgressHUD.dismiss()
            }
        }
    }
}
