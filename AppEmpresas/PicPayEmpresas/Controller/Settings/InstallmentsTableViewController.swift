import AnalyticsModule
import LegacyPJ
import SVProgressHUD
import UIKit

final class InstallmentsTableViewController: BaseTableViewController {
    private typealias Localizable = Strings.LegacySettingsTableView
    
    // MARK: - Properties
    
    var rowSelected: Int?
    
    var model: InstallmentViewModel?
    private let dependencies: HasAnalytics = DependencyContainer()
    
    // Overlay para imitar o comportamento do SVProgressHUD, o default não cobre toda tableView
    lazy var overlay: UIView = {
        [weak self] in
        let _view = UIView()
        _view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4)
        _view.isHidden = true
        _view.frame = self?.view.frame ?? CGRect.zero
        self?.view.addSubview(_view)
        return _view
    }()
    
    var isSaveButtomEnabled: Bool {
        get {
            navigationController?.navigationBar.topItem?.rightBarButtonItem?.isEnabled ?? false
        }
        set {
            if let btnSalvar = self.navigationController?.navigationBar.topItem?.rightBarButtonItem {
                btnSalvar.isEnabled = newValue
            }
        }
    }
    
    var tableViewHeaderHeight: CGFloat? {
        get {
            tableView.tableHeaderView?.frame.height
        }
        set {
            guard let headerView = self.tableView.tableHeaderView else {
                return
            }
            headerView.frame = CGRect(
                origin: headerView.frame.origin,
                size: CGSize(width: headerView.frame.width, height: newValue ?? 0)
            )
        }
    }
    
    @IBOutlet private var lblAvisoTaxa: UILabel!
    
    // MARK: - View Life Cycle

    override func viewDidLoad() {
        self.isPullRefresh = false
        SVProgressHUD.setDefaultMaskType(.none)
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableViewHeaderHeight = nil
        super.viewDidLoad()
        self.retrieveOptions()
        dependencies.analytics.log(SettingsAnalytics.splitConditions)
        title = Localizable.installmentInterests
    }
    
    // MARK: - Actions
    
    @IBAction func salvar(_ sender: UIBarButtonItem) {
        guard let row = self.rowSelected else {
            let alert = UIAlertController(title: "Opção inválida", message: "Selecione uma opção.", preferredStyle: .alert)
            alert.addAction(title: "OK")
            alert.show()
            return
        }

        DispatchQueue.main.async {
            AuthManager.shared.performActionWithAuthorization { [weak self] result in
                switch result {
                case .success(let password):
                    self?.saveOptionWithPassword(row: row, password: password)
                    
                default:
                    break
                }
            }
        }
    }
    
    // MARK: - Dependence Injection
    
    override func configureDependencies() {
        self.setup(model: InstallmentViewModel())
        super.configureDependencies()
    }
    
    // MARK: - Internal Methods

    private func working(_ needsWork: Bool) {
        self.view.isUserInteractionEnabled = !needsWork
        self.overlay.isHidden = !needsWork
        if needsWork {
            // FIXME: SVProgress dont calculate center of screen properly after keyboard dismiss.
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                SVProgressHUD.show()
            }
        } else {
            SVProgressHUD.dismiss()
        }
    }
    
    func setup(model: InstallmentViewModel) {
        self.model = model
        self.rowSelected = model.rowSaved
        self.lblAvisoTaxa.text = model.header
    }
    
    func retrieveOptions() {
        startLoading()
        
        model?.loadInstallmentsList { [weak self] error in
            guard let self = self else { return }
            self.stopLoading()
            
            if let error = error {
                UIAlertController(error: error).show()
                return
            }
            
            self.rowSelected = self.model?.rowSaved
            self.isSaveButtomEnabled = false
            self.lblAvisoTaxa.text = self.model?.header ?? ""
            // Ajuste do height do tableViewHeader, de acordo com o texto recebido do servidor
            self.lblAvisoTaxa.sizeToFit()
            
            let height = self.lblAvisoTaxa.frame.height
            let y = self.lblAvisoTaxa.frame.origin.y
            let withPadding = y * 2 + height
            self.tableViewHeaderHeight = withPadding
            self.tableView.reloadData()
            // Ajuste do tamanho da view de overlay para o novo tamanho do conteudo da tableview
            self.overlay.frame = CGRect(origin: .zero, size: self.tableView.contentSize)
        }
    }

    func saveOptionWithPassword(row: Int, password: String) {
        self.working(true)
        self.isSaveButtomEnabled = false
        self.model?.saveInstallment(rowSelected: row, password: password) { [weak self] error in
            self?.working(false)
            
            if let error = error {
                self?.isSaveButtomEnabled = true
                UIAlertController(error: error).show()
                return
            }
            self?.navigationController?.popViewController(animated: true)
        }
    }

    // MARK: - TableViewDelegate / TableViewDataSource

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model?.numberOfRows ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        let data = self.model?.dataForRow(row)
        let selectedCellIdentifierPlaceholder: String = self.rowSelected == row ? "Selected" : ""
        var cell: UITableViewCell?
        
        if let subtitle = data?.subtitle {
            cell = tableView.dequeueReusableCell(withIdentifier: "Subtitle\(selectedCellIdentifierPlaceholder)Cell")
            cell?.detailTextLabel?.text = subtitle
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "Basic\(selectedCellIdentifierPlaceholder)Cell")
        }
        
        cell?.textLabel?.text = data?.title
        
        cell?.accessoryType = selectedCellIdentifierPlaceholder.isEmpty ? .none : .checkmark
        
        return cell ?? UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.rowSelected = indexPath.row
        
        self.isSaveButtomEnabled = self.rowSelected != self.model?.rowSaved

        tableView.reloadData()
    }
}
