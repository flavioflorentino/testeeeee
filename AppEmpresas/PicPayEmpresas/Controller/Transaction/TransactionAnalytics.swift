import AnalyticsModule
import LegacyPJ

enum TransactionAnalytics: AnalyticsKeyProtocol {
    case transactionDetails(_ transaction: Transaction)
    case transactionReturned(_ transaction: Transaction, origin: TransactionOrigin?)
    case historyOpen(_ range: HistoryRange)
    case historyDetail(_ range: HistoryRange, date: String, position: Int)
    case historyTransactionsDetails(_ transaction: Transaction)
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .transactionDetails(let transaction):
            return AnalyticsEvent("Transaction Details Viewed", properties: properties(from: transaction), providers: [.mixPanel])
        case let .transactionReturned(transaction, origin):
            var finalProperties = properties(from: transaction)
            if let origin = origin {
                finalProperties["origin"] = origin.rawValue
            }
            
            return AnalyticsEvent("Payment Returned", properties: finalProperties, providers: [.mixPanel])
            
        case let .historyOpen(range):
            return AnalyticsEvent("Sales-History Accessed", properties: ["type": range.rawValue.uppercased()], providers: [.mixPanel])
            
        case let .historyDetail(range, date, position):
            var properties = [String: String]()
            properties["type"] = range.rawValue.uppercased()
            properties["date"] = date
            properties["cell_position"] = String(position)
            
            return AnalyticsEvent("Sales-History Details Accessed", properties: properties, providers: [.mixPanel])
            
        case let .historyTransactionsDetails(transaction):
            return AnalyticsEvent("Sales-History - Transaction Details Accessed", properties: properties(from: transaction), providers: [.mixPanel])
        }
    }
    
    private func properties(from transaction: Transaction) -> [String: String] {
        var properties = [String: String]()
        properties["customer_username"] = transaction.consumer?.username ?? ""
        properties["value"] = transaction.siteValue
        properties["date"] = transaction.rawDate
        return properties
    }
}

enum HistoryRange: String {
    case daily
    case monthly
}

enum TransactionOrigin: String {
    case transaction = "TRANSACOES"
    case history = "EXTRATO_HISTORICO"
}
