import AnalyticsModule
import CoreSellerAccount
import LegacyPJ
import PIX
import SnapKit
import UI
import UIKit
import FeatureFlag

private extension TransactionTableViewController.Layout {
    enum StackView {
        static let layoutMargins = UIEdgeInsets(top: 16, left: 0, bottom: 16, right: 0)
    }
}

class TransactionTableViewController: BaseTableViewController {
    // MARK: Private Properties
    fileprivate enum Layout {}
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = Layout.StackView.layoutMargins
        return stackView
    }()
    
    private let dependencies: HasAnalytics & HasFeatureManager = DependencyContainer()
    private var feedbackContentView: UIView?
    
    // MARK: - Internal Properties
    
    var model: TransactionListViewModel?
    var origin: TransactionOrigin?
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        
        if let title = model?.transactionTitle {
            navigationItem.title = title
        }
        
        configureTableView()
        loadTransactions()
        
        model?.loadSellerAndUserStatus { [weak self] in
            guard let self = self else {
                return
            }
            self.didLoadSeller()
            self.model?.presentAdvertising(self.showAdvertising(with:))
        }
        
        tableView.tableHeaderView = stackView
        
        // Observers
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotificationNewTransaction), name: Notification.Name.Remote.newTransaction, object: nil)
    }
    
    private func configureNavigation() {
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
        if dependencies.featureManager.isActive(.releaseNewHome) && origin == .history {
            navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceDefault())
        } else { 
            navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceLegacy())
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Colors.white.lightColor]
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        configureNavigation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureNavigation()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateTableHeaderHeight()
    }
    
    // MARK: - User Actions
    
    @IBAction func showHistory(_ sender: Any) {
        let controller: SummaryViewController = ViewsManager.instantiateViewController(.transaction)
        pushViewController(controller)
    }
    
    @IBAction func refreshButtonTap(_ sender: Any) {
        forceRefresh()
    }
    
    @objc
    func handleNotificationNewTransaction() {
        forceRefresh()
    }
    
    // MARK: - Override Methods
    
    override func shouldLoadMoreRows() -> Bool {
        model?.shouldLoadMoreItems() ?? false
    }
    
    override func loadMoreRows() {
        guard let model = model else {
            return
        }
        
        startLoading()
        model.loadMoreTransactions { [weak self] error in
            self?.stopLoading()
            self?.tableView.reloadData()
            
            if let error = error {
                UIAlertController(error: error).show()
            }
        }
    }
    
    override func stopLoading() {
        tableView.tableFooterView = UIView()
    }
    
    override func handleRefresh(_ control: UIRefreshControl) {
        control.beginRefreshing()
        loadTransactions(loading: false)
    }
}

// MARK: - Internal Methods

extension TransactionTableViewController {
    func setup(model: TransactionListViewModel, origin: TransactionOrigin = .transaction) {
        self.model = model
        self.origin = origin
        isReady = true
    }
    
    func configureTableView() {
        let transactionNib = UINib(nibName: TransactionTableViewCell.identifier, bundle: .main)
        let operatorNib = UINib(nibName: TransactionOperatorTableViewCell.identifier, bundle: .main)
        tableView.register(transactionNib, forCellReuseIdentifier: TransactionTableViewCell.identifier)
        tableView.register(operatorNib, forCellReuseIdentifier: TransactionOperatorTableViewCell.identifier)
        
        tableView.rowHeight = 60
        tableView.sectionHeaderHeight = 0.0
        automaticallyAdjustsScrollViewInsets = false
        tableView.backgroundColor = #colorLiteral(red: 0.95, green: 0.95, blue: 0.96, alpha: 1.0)
    }
    
    func loadTransactions(loading: Bool = true) {
        guard let model = model else { return }
        
        if loading {
            startLoading()
        }
        
        model.loadTransactions { [weak self] error in
            self?.stopLoading()
            self?.tableView.reloadData()
            self?.refreshControl?.endRefreshing()
            self?.checkIsTableEmpty(error)
            if let error = error {
                UIAlertController(error: error).show()
            }
        }
    }
    
    func forceRefresh() {
        refreshControl?.endRefreshing()
        refreshControl?.beginRefreshing()
        
        // animate table view to show the refresh control
        if fabsf(Float(tableView.contentOffset.y)) < .ulpOfOne {
            UIView.animate(
                withDuration: 0.25,
                delay: 0,
                options: .beginFromCurrentState,
                animations: {
                    if let height = self.refreshControl?.frame.size.height {
                        self.tableView.contentOffset = CGPoint(x: 0, y: -height)
                    }
                })
        }
        
        loadTransactions(loading: false)
    }
    
    func cancelTransaction(_ transaction: Transaction, indexPath: IndexPath) {
        AuthManager.shared.performActionWithAuthorization { [weak self] result in
            switch result {
            case .success(let password):
                self?.performCancelTransaction(password: password, transaction: transaction, indexPath: indexPath)
            default:
                break
            }
        }
    }
    
    func performCancelTransaction(password: String, transaction: Transaction, indexPath: IndexPath) {
        model?.cancelTransaction(password: password, transaction: transaction, indexPath: indexPath, { [weak self] error in
            if let error = error {
                UIAlertController(error: error).show()
            }
            self?.dependencies.analytics.log(TransactionAnalytics.transactionReturned(transaction, origin: self?.origin))
            self?.tableView.reloadRows(at: [indexPath], with: .fade)
        })
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
}

// MARK: - Private Methods

private extension TransactionTableViewController {
    func didLoadSeller() {
        guard let seller = model?.seller else { return }
        
        configureAccountErrorComponent(bankAccountError: model?.bankAccountError)
        configureMaterialHeader(hasDigitalAccount: seller.digitalAccount, userStatus: model?.userStatus)
        configureWalletComponent(hasDigitalAccount: seller.digitalAccount)
        configureCarouselComponent(
            hasDigitalAccount: seller.digitalAccount,
            hasBiometry: seller.biometry,
            seller: seller,
            userStatus: model?.userStatus
        )
        configurePixTransactionsComponent(seller: seller)
        updateTableHeaderHeight()
    }
    
    func configureAccountErrorComponent(bankAccountError: BankAccountValidation?) {
        guard let validation = bankAccountError?.validationIssue, let action = validation.action else { return }

        if let feedbackContentView = feedbackContentView {
            feedbackContentView.removeFromSuperview()
            self.feedbackContentView = nil
        }
        
        let apolloAction = ApolloFeedbackCardButtonAction(title: action.message, action: accountErrorAction)
        let layoutType: ApolloFeedbackCardViewLayoutType = .oneAction(apolloAction)
        
        let text = validation.title ?? validation.message
        
        let feedbackCard = ApolloFeedbackCard(
            description: text,
            iconType: .error,
            layoutType: layoutType,
            emphasisType: .high
        )
        
        let contentView = UIView()
        contentView.addSubview(feedbackCard)
        stackView.addArrangedSubview(contentView)
        
        feedbackCard.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
        }
        
        feedbackContentView = contentView
        
        let event = BankAccountErrorAnalytics.home(errorType: validation.reason.rawValue)
        dependencies.analytics.log(event)
    }
    
    func configureWalletComponent(hasDigitalAccount: Bool) {
        guard hasDigitalAccount else { return }
        
        var bankError: WithdrawalOptionsBankError?
        if
            let error = model?.bankAccountError,
            let seller = model?.seller,
            let individual = model?.companyInfo?.ableToDigitalAccount
        {
            let companyType: CompanyType = individual ? .individual : .multiPartners
            bankError = WithdrawalOptionsBankError(bankError: error, seller: seller, companyType: companyType)
        }
        
        let walletController = WalletBalanceFactory.make(bankError: bankError)
        addChild(walletController)
        stackView.addArrangedSubview(walletController.view)
        walletController.didMove(toParent: self)
    }
    
    func configureCarouselComponent(hasDigitalAccount: Bool, hasBiometry: Bool, seller: Seller, userStatus: UserStatus?) {
        guard hasDigitalAccount else { return }
        
        let carouselController = TransactionCarouselFactory.make(hasBiometry: hasBiometry, userStatus: userStatus, seller: seller)
        addChild(carouselController)
        stackView.addArrangedSubview(carouselController.view)
        carouselController.didMove(toParent: self)
    }
    
    func configurePixTransactionsComponent(seller: Seller) {
        let shouldEnablePix = FeatureEnablerHelper.shouldEnablePIX(seller: seller, isAdmin: model?.user?.isAdmin ?? false)
        let shouldEnableTransactionsPixButton = model?.isPixTransactionsButtonEnabled() ?? false
        if shouldEnablePix && shouldEnableTransactionsPixButton {
            let transactionButton = TransactionsPIXButtonView(didTap: didTapPixTransactionsButton)
            stackView.addArrangedSubview(transactionButton)
        }
    }
    
    func configureMaterialHeader(hasDigitalAccount: Bool, userStatus: UserStatus?) {
        guard let model = userStatus,
              FeatureManager.shared.isActive(.allowApplyMaterial),
              !hasDigitalAccount,
              !model.boardRequestRefused else {
            return
        }
        
        let materialController = MaterialSolicitationHeaderFactory.make(status: model)
        materialController.delegate = self
        addChild(materialController)
        stackView.addArrangedSubview(materialController.view)
        materialController.didMove(toParent: self)
    }
    
    func updateTableHeaderHeight() {
        guard let headerView = tableView.tableHeaderView else { return }
        let availableSize = CGSize(width: view.frame.width, height: 0)
        let height = headerView.systemLayoutSizeFitting(availableSize).height
        var headerFrame = headerView.frame
        
        // Comparison necessary to avoid infinite loop
        if height != headerFrame.size.height {
            headerFrame.size.height = height
            headerView.frame = headerFrame
            tableView.tableHeaderView = headerView
        }
    }
    
    /// Check if the table is Empty to show the onboard screen
    func checkIsTableEmpty(_ error: LegacyPJError?) {
        guard error == nil && model?.numberOfRowsInSection(section: 0) == 0 else {
            navigationItem.rightBarButtonItem?.isEnabled = true
            return
        }
        
        createEmptyTableWithQRCodeHeader()
        navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    func createEmptyTableWithQRCodeHeader() {
        let emptyView = createEmptyView(frame: tableView.frame)
        tableView.tableFooterView = emptyView
        tableView.tableHeaderView = stackView
    }
    
    func createEmptyView(frame: CGRect) -> UIView {
        let emptyView = TransactionEmptyListView(frame: frame)
        emptyView.delegate = self
        emptyView.actionButton.isHidden = false
        emptyView.setActionButtonTitle(title: Strings.Transaction.emptyTitle)
        return emptyView
    }
    
    func showAdvertising(with modalInfo: AlertModalInfo) {
        let alertPopup = AlertModalFactory.make(alertModalInfo: modalInfo)
        
        alertPopup.touchButtonAction = { [weak self] alert in
            guard let self = self else {
                return
            }
            alert.dismiss(animated: true) {
                self.model?.trackPixRegister()
                self.navigateToPIX()
            }
        }
        
        alertPopup.touchSecondaryButtonAction = { [weak self] alert in
            guard let self = self else {
                return
            }
            self.model?.trackPixNotNow()
            alert.dismiss(animated: true)
        }
        
        let popup = PopupViewController()
        popup.hasCloseButton = false
        popup.contentController = alertPopup
        present(popup, animated: true, completion: nil)
    }
    
    func navigateToPIX() {
        guard let navigationController = navigationController,
              let seller = model?.seller,
              let user = model?.user else {
            return
        }
        
        let userInfo = KeyManagerBizUserInfo(
            name: seller.razaoSocial ?? "",
            cnpj: seller.cnpj ?? "",
            mail: seller.email ?? "",
            phone: user.phone ?? "",
            userId: String(seller.id)
        )
        
        let coordinator = PIXBizFlowCoordinator(with: navigationController, userInfo: userInfo)
        
        let welcomePresented = model?.mustPresentWelcome() == true
        if model?.mustPresentHub() == true {
            let flow: PIXBizFlow = welcomePresented ? .hub : .welcomePage
            coordinator.perform(flow: flow)
            return
        }
        
        coordinator.perform(
            flow: .optin(action: welcomePresented
                            ? .keyManager(input: .none)
                            : .onboarding)
        )
    }
    
    func createTransactionAlert(with transaction: Transaction, at indexPath: IndexPath) {
        let alert = UIAlertController(title: "\n\n\n", message: nil, preferredStyle: .actionSheet)
        
        // Configure cell size
        let transactionView = TransactionView(frame: CGRect(x: 0.0, y: 10.0, width: view.frame.size.width - 20, height: 60))
        transactionView.backgroundColor = UIColor.clear
        transactionView.layer.cornerRadius = 10.0
        alert.view.addSubview(transactionView)
        transactionView.configure(transaction)
        let isRefundAllowed = transaction.status != .canceled
        if transaction.isPix {
            alert.addAction(title: Strings.Transaction.Alert.Button.receipt,
                            style: .default,
                            isEnabled: true) { [weak self] _ in
                self?.openPixReceipt("\(transaction.id)", isRefundAllowed: isRefundAllowed)
            }
            if isRefundAllowed {
                alert.addAction(title: Strings.Transaction.Alert.Button.returnPix,
                                style: .destructive,
                                isEnabled: true) { [weak self] _ in
                    self?.returnPixPayment("\(transaction.id)")
                }
            }
        } else {
            alert.addAction(title: Strings.Transaction.Alert.Button.return,
                            style: .destructive,
                            isEnabled: (transaction.status != .canceled),
                            handler: { [weak self] _ in
                self?.cancelTransaction(transaction, indexPath: indexPath)
            })
        }
        
        alert.addAction(title: Strings.Default.cancel, style: .cancel, isEnabled: true)
        present(alert, animated: true)
        alert.view.setNeedsLayout()
    }
    
    func openPixReceipt( _ transactionId: String, isRefundAllowed: Bool) {
        guard let navigationController = navigationController else {
            return
        }
        let coordinator = PIXBizFlowCoordinator(with: navigationController)
        
        coordinator.perform(
            flow: .receipt(transactionId: transactionId, isRefundAllowed: isRefundAllowed)
        )
    }
    
    func didTapPixTransactionsButton() {
        let controller = TransactionsListFactory.make(type: .pix)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func returnPixPayment( _ transactionId: String) {
        guard let navigationController = navigationController else {
            return
        }
        let coordinator = PIXBizFlowCoordinator(with: navigationController)
        
        coordinator.perform(
            flow: .cashout(action: .refund(transactionId: transactionId))
        )
    }
    
    @objc
    func accountErrorAction() {
        guard let seller = model?.seller, let companyInfo = model?.companyInfo else { return }
        
        let companyType: CompanyType = companyInfo.individual ? .individual : .multiPartners
        let controller = BankAccountTipsFactory.makeEditTips(companyType: companyType, seller: seller)
        pushViewController(controller)
    }
}

// MARK: UITableViewDataSource

extension TransactionTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model?.numberOfRowsInSection(section: section) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = model, let transaction = model.transationForRow(indexPath) else {
            return UITableViewCell()
        }
        
        var cell: TransactionTableViewCell
        
        switch transaction.type {
        case .transaction:
            let identifier = TransactionTableViewCell.identifier
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TransactionTableViewCell ?? TransactionTableViewCell()
        case .collector:
            let identifier = TransactionOperatorTableViewCell.identifier
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TransactionOperatorTableViewCell ?? TransactionTableViewCell()
        default:
            return UITableViewCell()
        }
        
        cell.configureCell(transaction)
        
        model.isLoading(indexPath: indexPath) ? cell.startLoading() : cell.stopLoading()
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let model = model else { return }
        
        if let transaction = model.transationForRow(indexPath) {
            // Create a cell to insert on action sheet
            switch origin ?? .transaction {
            case .transaction:
                dependencies.analytics.log(TransactionAnalytics.transactionDetails(transaction))
            case .history:
                dependencies.analytics.log(TransactionAnalytics.historyTransactionsDetails(transaction))
            }
            
            createTransactionAlert(with: transaction, at: indexPath)
        }
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
// MARK: - TransactionEmptyListViewDelegate

extension TransactionTableViewController: TransactionEmptyListViewDelegate {
    func transactionEmptyListViewDidTapActionButton() {
        let popup = PopupViewController()
        let promoCodePopup = PromoCodePopupController()
        popup.contentController = promoCodePopup
        self.parent?.parent?.present(popup, animated: true, completion: nil)
    }
}

// MARK: - MaterialSolicitationHeaderDelegate

extension TransactionTableViewController: MaterialSolicitationHeaderDelegate {
    func updateTable() {
        updateTableHeaderHeight()
    }
}
