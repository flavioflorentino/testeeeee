import AnalyticsModule
import LegacyPJ
import UIKit

protocol SummaryTableViewDelegate: AnyObject {
    func summaryTableDidSelectedItem(item: SummaryItem)
}

final class SummaryTableViewController: BaseTableViewController {
    // MARK: Private Properties
    
    enum Cell: String {
        case summary
    }
    
    weak var summaryDelegate: SummaryTableViewDelegate?
    
    private let dependencies: HasAnalytics = DependencyContainer()
    private var model: SummaryListViewModel?
    private var range: HistoryRange?
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        
        configureTableView()
        loadItems()
    }
    
    // MARK: - Dependence Injections
    
    func setup(model: SummaryListViewModel, range: HistoryRange = .daily) {
        self.model = model
        self.range = range
        isReady = true
    }
    
    // MARK: - Internal Methods
    
    func configureTableView() {
        self.registerCell(Cell.summary.rawValue, nib: "SummaryItemTableViewCell")
        tableView.rowHeight = 44.0
        tableView.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9607843137, alpha: 1)
    }
    
    func configureView() {
        self.tableView.reloadData()
    }
    
    func loadItems() {
        guard let model = model else { return }
        
        startLoading()
        model.loadSummaryItems { [weak self] error in
            self?.configureView()
            self?.stopLoading()
            self?.refreshControl?.endRefreshing()
            if let error = error {
                UIAlertController(error: error).show()
            }
        }
    }
    
    override func shouldLoadMoreRows() -> Bool {
        guard let model = model else {
            return false
        }
        
        return model.shouldLoadMoreItems()
    }
    
    override func loadMoreRows() {
        guard let model = model else {
            return
        }
        
        startLoading()
        
        model.loadMoreSummaryItems { [weak self] error in
            self?.stopLoading()
            
            self?.configureView()
            
            if let error = error {
                UIAlertController(error: error).show()
            }
        }
    }
    
    // MARK: Event Handler
    
    override func handleRefresh(_ control: UIRefreshControl) {
        control.beginRefreshing()
        loadItems()
    }
    
    // MARK: UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model?.numberOfRowsInSection(section: section) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let model = model,
            let summaryItem = model.summaryItemForRow(indexPath),
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.summary.rawValue) as? SummaryItemTableViewCell
            else {
                return UITableViewCell()
        }
        
        cell.configureCell(summaryItem)
        return cell
    }
    
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let model = model else { return }
        
        if let summaryItem = model.summaryItemForRow(indexPath) {
            summaryDelegate?.summaryTableDidSelectedItem(item: summaryItem)
            dependencies.analytics.log(TransactionAnalytics.historyDetail(range ?? .daily, date: summaryItem.dateId, position: indexPath.row))
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
