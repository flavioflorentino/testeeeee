import AnalyticsModule
import LegacyPJ
import UI
import UIKit
import FeatureFlag

final class SummaryViewController: BaseViewController {
    enum Segment: Int {
        case day = 0
        case month = 1
    }
    
    // MARK: - Properties
    
    private lazy var dayTableViewController: SummaryTableViewController = {
        let controller: SummaryTableViewController = ViewsManager.instantiateViewController(.transaction)
        controller.setup(model: SummaryListByDayViewModel(), range: .daily)
        controller.summaryDelegate = self
        return controller
    }()
    
    private lazy var monthTableViewController: SummaryTableViewController = {
        let controller: SummaryTableViewController = ViewsManager.instantiateViewController(.transaction)
        controller.setup(model: SummaryListByMonthViewModel(), range: .monthly)
        controller.summaryDelegate = self
        return controller
    }()
    
    private let dependencies: HasAnalytics & HasFeatureManager = DependencyContainer()
    
    // MARK: - IB Outlet
    @IBOutlet private var headerView: UIView!
    @IBOutlet private var segmentedControl: UISegmentedControl!
    @IBOutlet private var containerView: UIView!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSegmentControl()
        displayContentController(controller: dayTableViewController)
        dependencies.analytics.log(TransactionAnalytics.historyOpen(.daily))
        if dependencies.featureManager.isActive(.releaseNewHome) {
            navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceDefault())
        } else {
            navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceLegacy())
        }
    }
    
    // MARK: - Dependence Injections
    
    override func configureDependencies() {
        isReady = true
        super.configureDependencies()
    }

    // MARK: - Internal Methods
    
    func configureSegmentControl() {
        headerView.backgroundColor = Colors.white.lightColor
        segmentedControl.addTarget(self, action: #selector(handlerSegmentedChange(_:)), for: .valueChanged)
    }
    
    @objc
    func handlerSegmentedChange(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == Segment.day.rawValue {
            dependencies.analytics.log(TransactionAnalytics.historyOpen(.daily))
            changeContentController(old: monthTableViewController, new: dayTableViewController )
        } else {
            dependencies.analytics.log(TransactionAnalytics.historyOpen(.monthly))
            changeContentController(old: dayTableViewController, new: monthTableViewController)
        }
    }
    
    func displayContentController(controller: UITableViewController) {
        // Add the view controller
        self.addChild(controller)
        controller.view.frame = containerView.frame
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(controller.view)
        
        // Apply the contrants to present the corret size
        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            controller.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            controller.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            controller.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
        
        // move controller
        controller.didMove(toParent: self)
    }
    
    func changeContentController(old: UIViewController, new: UIViewController) {
        // Prepare the two view controllers for the change.
        old.willMove(toParent: nil)
        self.addChild(new)
        
        new.view.frame = old.view.frame
        
        transition(from: old, to: new, duration: 0.25, options: .showHideTransitionViews, animations: nil, completion: { _ in
            old.removeFromParent()
            
            NSLayoutConstraint.activate([
                new.view.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor),
                new.view.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor),
                new.view.topAnchor.constraint(equalTo: self.containerView.topAnchor),
                new.view.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor)
            ])
            
            new.didMove(toParent: self)
        })
    }
}

extension SummaryViewController: SummaryTableViewDelegate {
    // MARK: - SummaryTableViewDelegate
    
    func summaryTableDidSelectedItem(item: SummaryItem) {
        let model = SummaryDetailDateViewModel(summaryItem: item)
        
        let controller: SummaryDetailViewController = ViewsManager.instantiateViewController(.transaction)
        controller.setup(model: model, origin: .history)
        navigationController?.pushViewController(controller, animated: true)
    }
}
