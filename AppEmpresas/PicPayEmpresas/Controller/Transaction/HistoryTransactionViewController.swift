import UIKit

final class HistoryTransactionViewController: BaseTableViewController {
    enum Cell: String {
        case history
    }
    
    var model: HistoryTransactionListViewModel?
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        
        configureTableView()
        loadHistoryItems()
    }
    
    // MARK: - Internal Methods
    
    func configureTableView() {
        self.registerCell(Cell.history.rawValue, nib: "HistoryItemTableViewCell")
        
        tableView.rowHeight = 44.0
        tableView.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9607843137, alpha: 1)
        
        if let name = self.model?.operatorr.username {
            navigationItem.title = name
        }
    }
    
    func configureView() {
        self.tableView.reloadData()
    }
    
    func loadHistoryItems() {
        guard let model = model else { return }
        
        startLoading()
        model.loadHistoryItems { [weak self] error in
            self?.configureView()
            self?.stopLoading()
            self?.refreshControl?.endRefreshing()
            if let error = error {
                UIAlertController(error: error).show()
            }
        }
    }
    
    override func shouldLoadMoreRows() -> Bool {
        guard let model = model else {
            return false
        }
        
        return model.shouldLoadMoreItems()
    }
    
    override func loadMoreRows() {
        guard let model = model else { return }
        
        startLoading()
        
        model.loadMoreHistoryItems { [weak self] error in
            self?.stopLoading()
            
            self?.configureView()
            
            if let error = error {
                UIAlertController(error: error).show()
            }
        }
    }
    
    // MARK: Event Handler
    
    override func handleRefresh(_ control: UIRefreshControl) {
        control.beginRefreshing()
        loadHistoryItems()
    }
    
    // MARK: UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model?.numberOfRowsInSection(section: section) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let model = model,
            let historyItem = model.historyItemForRow(indexPath),
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.history.rawValue) as? HistoryItemTableViewCell
            else {
                return UITableViewCell()
            }
                
        cell.configureCell(historyItem)
        return cell
    }
    
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let historyItem = model?.historyItemForRow(indexPath) else { return }
        
        let controller: OperatorReceiptViewController = ViewsManager.instantiateViewController(.transaction)
        let model = OperatorReeiptViewModel(historyItem: historyItem)
        controller.setup(model: model)
        navigationController?.pushViewController(controller, animated: true)
    }
}
