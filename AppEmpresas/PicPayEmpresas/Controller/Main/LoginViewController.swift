import AnalyticsModule
import LegacyPJ
import UI
import UIKit
import FeatureFlag

final class LoginViewController: FormViewController {
    typealias Dependencies = HasAppManager & HasAnalytics & HasAppCoordinator & HasFeatureManager
    private var dependencies: Dependencies?
    
    // MARK: - Properties
    var model: LoginViewModel?
    
    // MARK: - IB Outlet
    
    @IBOutlet var usernameTextField: UIRoundedTextField!
    @IBOutlet var passwordTextField: UIRoundedTextField!

    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        model?.delegate = self
        configureInputs()
        view.backgroundColor = Palette.Business.grayscale600.color
    }
    
    // MARK: - User Actions
    
    @IBAction func login(_ sender: UIButton?) {
        startLoading()
        populateForm()
        model?.login()
    }
    
    @IBAction func resetPassword(_ sender: UIButton?) {
        let title = Strings.Default.forgotPasswordTitle
        let controller = ViewsManager.webViewController(url: urlForgetPassword + (model?.cnpj ?? ""), title: title)
        controller.addScript(file: "Script")
        navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - Dependencie Injection
    
    override func configureDependencies() {
        self.setup(model: LoginImpViewModel(), dependencies: DependencyContainer())
        super.configureDependencies()
    }
    
    func setup(model: LoginViewModel, dependencies: Dependencies) {
        self.model = model
        self.dependencies = dependencies
        isReady = true
    }
    
    // MARK: - Internal Methods
    
    private func configureInputs() {
        // add text fields to the controls list
        self.controls.append(usernameTextField)
        self.controls.append(passwordTextField)
        
        // set delegate
        
        self.usernameTextField.delegate = self
        self.passwordTextField.delegate = self
    }
    
    private func populateForm() {
        if let username = usernameTextField.text {
            model?.username = username
        }
        if let password = passwordTextField.text {
            model?.password = password
        }
    }
    
    /// Move the cursor to next field
    /// If the field is the last field try to finish the step
    func goToNextField(from: UITextField) {
        if from == self.usernameTextField {
            self.passwordTextField.becomeFirstResponder()
        } else {
            self.login(nil)
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.goToNextField(from: textField)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        true
    }
}

extension LoginViewController: LoginViewModelDelegate {
    func loginDidSuccess() {
        dependencies?.analytics.log(LoginAnalytics.userSignedIn)
        if dependencies?.featureManager.isActive(.isAppCoordinatorAvailable) == true {
            dependencies?.appCoordinator.displayAuthenticated()
        } else {
            dependencies?.appManager.presentAuthenticatedViewController()
        }
    }
    
    func loginDidFail(error: LegacyPJError) {
        UIAlertController(error: error).show()
    }
    
    func userNeedsCompleteAccount() {
        let alertController = UIAlertController(
            title: Strings.Default.oops,
            message: Strings.Default.accountCompletionMessage,
            preferredStyle: .alert
        )
        
        let completeAction = UIAlertAction(title: Strings.Default.accountCompletionAction, style: .default) { _ in
            self.model?.completeSignUp()
        }
        
        alertController.addAction(completeAction)
        
        present(alertController, animated: true)
    }
    
    func loginDidFinish() {
        stopLoading()
    }
}
