import SnapKit
import UI
import UIKit

extension MainTabBar.Layout {
    enum SaleButton {
        static let size = 40
    }
}

final class MainTabBar: UITabBar, ViewConfiguration {
    fileprivate enum Layout {}
    
    override var isHidden: Bool {
        didSet {
            saleButton.isHidden = isHidden
            saleButton.isEnabled = !isHidden
        }
    }
    
    private lazy var saleButton = UISaleTabButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        saleButton.snp.makeConstraints {
            $0.size.equalTo(Layout.SaleButton.size)
            $0.centerX.equalToSuperview()
            $0.bottom.equalTo(compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }
    
    func buildViewHierarchy() {
        insertSubview(saleButton, at: 1)
    }
    
    func setButtonStatus(selected: Bool) {
        saleButton.setBackgroundStatus(selected: selected)
    }
}
