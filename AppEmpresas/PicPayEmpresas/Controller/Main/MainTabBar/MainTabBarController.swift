import AnalyticsModule
import AssetsKit
import LegacyPJ
import UI
import UIKit
import FeatureFlag

final class MainTabBarController: UITabBarController {
    typealias Dependencies = HasFeatureManager & HasAnalytics
    private typealias Localizable = Strings.MainTabBar
    
    enum MainTabBarItens: Int {
        case TransactionsTab = 1, MovementsTab, ReceiveTab, NotificationsTab, SettingTab
    }
    
    private let container: Dependencies
    
    private var previousViewController = ""
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    init(container: Dependencies) {
        self.container = container
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setValue(MainTabBar(frame: tabBar.frame), forKey: "tabBar")
        
        configureTabBar()
        
        PermissionManager.shared.askRemoteNotificationPermission()
        setNeedsStatusBarAppearanceUpdate()
        TrackingManager.setupMixpanel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        delegate = self
        setupTabBarStyle(isNewHomeEnabled: container.featureManager.isActive(.releaseNewHome))
    }
}

// MARK: - Internal Methods

extension MainTabBarController {
    func configureTabBar() {
        if let isAdmin = AuthManager.shared.userAuth?.user.isAdmin, isAdmin {
            configureAdminTabBar()
        } else {
            configureOperatorTabBar()
        }
    }
    
    func select(viewController: UIViewController?, tag: Int) {
        guard let viewController = viewController,
              let index = tabBar.items?.firstIndex(where: { $0.tag == tag })
        else {
            return
        }
        
        selectedIndex = index
        tabBarController(self, didSelect: viewController)
    }
}

// MARK: - Private Methods
private extension MainTabBarController {
    func setupTabBarStyle(isNewHomeEnabled: Bool) {
        if isNewHomeEnabled {
            tabBar.backgroundColor = Colors.grayscale050.color
            tabBar.tintColor = Colors.branding900.color
            
            let unselectedAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: Colors.grayscale700.color]
            UITabBarItem.appearance().setTitleTextAttributes(unselectedAttributes, for: .normal)
            
            let selectedAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: Colors.branding900.color]
            UITabBarItem.appearance().setTitleTextAttributes(selectedAttributes, for: .selected)
        } else {
            tabBar.isTranslucent = false
            tabBar.isOpaque = true
            tabBar.barTintColor = Colors.backgroundTertiary.color
            tabBar.backgroundImage = Assets.bgNavMain.image
            tabBar.tintColor = Colors.branding400.color
        }
    }
    
    func configureOperatorTabBar() {
        let transactionNavigationController = createOperatorTransactions()
        let receiveNavigationController = createReceive()
        let settingsNavigationViewController = createSettings(isAdmin: false)
        
        viewControllers = [transactionNavigationController, receiveNavigationController, settingsNavigationViewController]
        selectedIndex = 0
    }
    
    func configureAdminTabBar() {
        let transactionNavigationController = createHome()
        let statementsNavigationController = createStatements()
        let receiveNavigationController = createReceive()
        let notificationNavigationController = createNotifications()
        let settingsNavigationViewController = createSettings(isAdmin: true)
        
        viewControllers = [
            transactionNavigationController,
            statementsNavigationController,
            receiveNavigationController,
            notificationNavigationController,
            settingsNavigationViewController
        ]
        selectedIndex = 0
    }
    
    func setMainButtonStatus(selected: Int?) {
        guard let mainTabBar = tabBar as? MainTabBar, let selected = selected else { return }
        let isReceive = selected == MainTabBarItens.ReceiveTab.rawValue
        mainTabBar.setButtonStatus(selected: isReceive)
    }
    
    func createNavigationController(root: UIViewController, tabBarItem: UITabBarItem) -> UINavigationController {
        let navController = UINavigationController(rootViewController: root)
        navController.tabBarItem = tabBarItem
        return navController
    }
    
    func createHome() -> UINavigationController {
        if container.featureManager.isActive(.releaseNewHome) {
            let title = Localizable.tabHome
            let image = Resources.Icons.icoPjTabBarHome.image
            let selectedImage = Resources.Icons.icoPjTabBarHomeFill.image
            let item = UITabBarItem(title: title, image: image, selectedImage: selectedImage)
            item.tag = 1
            return createNavigationController(root: HomeFactory.make(), tabBarItem: item)
        } else {
            return createTransactions()
        }
    }
    
    func createTransactions() -> UINavigationController {
        let container = DependencyContainer()
        let service = UserStatusService(dependencies: container)
        let model = TransactionListAllViewModel(container: container, service: service)
        let transactionController: TransactionTableViewController = ViewsManager.instantiateViewController(.transaction)
        transactionController.setup(model: model, origin: .transaction)
        
        let title = Localizable.tabTransactions
        let image = Assets.icoTabTransaction.image
        let item = UITabBarItem(title: title, image: image, tag: 1)
        return createNavigationController(root: transactionController, tabBarItem: item)
    }
    
    func createOperatorTransactions() -> UINavigationController {
        let transactionController: OperatorTransactionTableViewController = ViewsManager.instantiateViewController(.transaction)
        
        var title = Localizable.tabTransactions
        let tag = 1
        let item: UITabBarItem
        
        if container.featureManager.isActive(.releaseNewHome) {
            let image = Resources.Icons.icoPjTabBarOperatorTransactions.image
            title = Localizable.tabOperatorTransactions
            let selectedImage = Resources.Icons.icoPjTabBarOperatorTransactionsFill.image
            item = UITabBarItem(title: title, image: image, selectedImage: selectedImage)
        } else {
            let image = Assets.icoTabTransaction.image
            item = UITabBarItem(title: title, image: image, tag: tag)
        }
        
        return createNavigationController(root: transactionController, tabBarItem: item)
    }
    
    func createStatements() -> UINavigationController {
        let statementsViewController = StatementListFactory.make()
        let title = Localizable.tabExtract
        let image = Resources.Icons.icoPjTabBarStatement.image
        
        let item: UITabBarItem
        
        if container.featureManager.isActive(.releaseNewHome) {
            let selectedImage = Resources.Icons.icoPjTabBarStatementFill.image
            item = UITabBarItem(title: title, image: image, selectedImage: selectedImage)
            item.tag = 2
        } else {
            item = UITabBarItem(title: title, image: image, tag: 2)
        }
        
        return createNavigationController(root: statementsViewController, tabBarItem: item)
    }
    
    func createReceive() -> UINavigationController {
        let receiveController: UIViewController
        
        if FeatureManager.shared.isActive(.allowNewReceive) {
            receiveController = ReceiveFactory.make()
        } else {
            let oldReceiveController: PPCodeViewController = ViewsManager.instantiateViewController(.main)
            receiveController = oldReceiveController as PPCodeViewController
        }
        
        let item = UITabBarItem(title: "Cobrar", image: nil, tag: 3)
        return createNavigationController(root: receiveController, tabBarItem: item)
    }
    
    func createNotifications() -> UINavigationController {
        let notificationViewController: NotificationCenterViewController = NotificationCenterFactory.make()
        let title = Localizable.tabNotification
        
        let item: UITabBarItem
        if container.featureManager.isActive(.releaseNewHome) {
            let image = Resources.Icons.icoPjTabBarNotification.image
            let selectedImage = Resources.Icons.icoPjTabBarNotificationFill.image
            item = UITabBarItem(title: title, image: image, selectedImage: selectedImage)
            item.tag = 4
        } else {
            let image = Assets.icoTabNotifications.image
            item = UITabBarItem(title: title, image: image, tag: 4)
        }
        
        return createNavigationController(root: notificationViewController, tabBarItem: item)
    }
    
    func createSettings(isAdmin: Bool) -> UINavigationController {
        let settingsViewController: UIViewController
        if isAdmin {
            let adminController: SettingsTableViewController = ViewsManager.instantiateViewController(.settings)
            settingsViewController = adminController
        } else {
            let operatorController: OperatorSettingsViewController = ViewsManager.instantiateViewController(.settings)
            settingsViewController = operatorController
        }
        
        let title = Localizable.tabSettings
        
        let item: UITabBarItem
        if container.featureManager.isActive(.releaseNewHome) {
            let image = Resources.Icons.icoPjTabBarSettings.image
            let selectedImage = Resources.Icons.icoPjTabBarSettingsFill.image
            item = UITabBarItem(title: title, image: image, selectedImage: selectedImage)
            item.tag = 5
        } else {
            let image = Assets.icoTabSettings.image
            item = UITabBarItem(title: title, image: image, tag: 5)
        }
        
        return createNavigationController(root: settingsViewController, tabBarItem: item)
    }
    
    func logTabBarSelected(tag: Int) {
        switch tag {
        case MainTabBarItens.TransactionsTab.rawValue:
            container.analytics.log(MainTabBarAnalytics.tabBarSelected(.transactions, origin: previousViewController))
            TrackingManager.trackOneTimeEvent(.ftTransacoesTab, properties: nil)
        case MainTabBarItens.ReceiveTab.rawValue:
            guard FeatureManager.shared.isActive(.allowNewReceive) else {
                container.analytics.log(MainTabBarAnalytics.tabBarSelected(.receive, origin: previousViewController))
                TrackingManager.trackOneTimeEvent(.ftReceberTab, properties: nil)
                return
            }
            
            container.analytics.log(MainTabBarAnalytics.tabBarSelected(.newReceive, origin: previousViewController))
            TrackingManager.trackOneTimeEvent(.ftCobrarTab, properties: nil)
        case MainTabBarItens.MovementsTab.rawValue:
            container.analytics.log(MainTabBarAnalytics.tabBarSelected(.movements, origin: previousViewController))
            TrackingManager.trackOneTimeEvent(.ftExtratoTab, properties: nil)
        case MainTabBarItens.SettingTab.rawValue:
            container.analytics.log(MainTabBarAnalytics.tabBarSelected(.settings, origin: previousViewController))
            TrackingManager.trackOneTimeEvent(.ftAjustesTab, properties: nil)
        case MainTabBarItens.NotificationsTab.rawValue:
            container.analytics.log(MainTabBarAnalytics.tabBarSelected(.notifications, origin: previousViewController))
            TrackingManager.trackOneTimeEvent(.ftNotificationsTab, properties: nil)
        default:
            break
        }
    }
}

// MARK: - UITabBarControllerDelegate

extension MainTabBarController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if
            let baseNavigation = tabBarController.selectedViewController as? UINavigationController,
            let controller = baseNavigation.topViewController {
            previousViewController = String(describing: controller.classForCoder)
        } else if let controller = tabBarController.selectedViewController {
            previousViewController = String(describing: controller.classForCoder)
        } else {
            previousViewController = ""
        }
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        setMainButtonStatus(selected: tabBarController.tabBar.selectedItem?.tag)
        
        guard let tag = tabBarController.tabBar.selectedItem?.tag else {
            return
        }
        
        logTabBarSelected(tag: tag)
    }
}
