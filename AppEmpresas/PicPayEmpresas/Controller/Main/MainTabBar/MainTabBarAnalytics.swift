import AnalyticsModule

enum MainTabBarAnalytics: AnalyticsKeyProtocol {
    case tabBarSelected(_ tabSelected: MainTabBarSelection, origin: String?)
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case let .tabBarSelected(selection, origin):
            return AnalyticsEvent("Navigation -  Tab Selected", properties: ["tab": selection.rawValue, "origin": origin ?? ""], providers: [.mixPanel])
        }
    }
}

enum MainTabBarSelection: String {
    case transactions = "TRANSACOES"
    case receive = "RECEBER"
    case newReceive = "COBRAR"
    case movements = "EXTRATO"
    case settings = "AJUSTES"
    case notifications = "NOTIFICACOES"
}
