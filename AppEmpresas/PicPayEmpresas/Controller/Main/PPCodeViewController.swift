import CoreImage
import UIKit

final class PPCodeViewController: BaseViewController {
    // MARK: - Properties
    
    var model: PPCodeViewModel?
    
    // MARK: - Private Properties
    
    private var timer: Timer?
    
    // MARK: - IB Outlet
    
    @IBOutlet private var codeImage: UIImageView!
    @IBOutlet private var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadCode()
        
        // add Reload action to QR Code
        codeImage.isUserInteractionEnabled = true
        codeImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(reloadCode)))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(loadCode), userInfo: nil, repeats: true)
    }
    
    // MARK: - Dependencie Injection
    
    override func configureDependencies() {
        setup(model: PPCodeImpViewModel())
        super.configureDependencies()
    }
    
    func setup(model: PPCodeViewModel) {
        self.model = model
        isReady = true
    }
    
    // MARK: - Internal Methods
    
    @objc
    func reloadCode() {
        loadCode(true)
    }
    
    /// Load the code
    /// OBS.: Load a new code only if the current code is not valid
    @objc
    func loadCode(_ force: Bool = false) {
        guard let model = model else {
            return
        }
        
        if model.codeIsValid() == false || force {
            startLoading()
            model.loadCodePicPay { [weak self] error in
                if error == nil {
                    if let code = model.code {
                        self?.generateCode(code: code.key)
                        self?.stopLoading()
                    }
                }
            }
        }
    }
    
    func generateCode(code: String) {
        let filter = CIFilter(name: "CIQRCodeGenerator")
        
        filter?.setValue(code.data(using: String.Encoding.utf8), forKey: "inputMessage")
        filter?.setValue("H", forKey: "inputCorrectionLevel")
        
        if let image = filter?.outputImage {
            let scaleX = codeImage.frame.size.width / image.extent.size.width
            let scaleY = codeImage.frame.size.height / image.extent.size.height
        
            let transformedImage = image.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
        
            codeImage.image = UIImage(ciImage: transformedImage)
        }
    }
    
    func startLoading() {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        codeImage.isHidden = true
    }
    
    func stopLoading() {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        codeImage.isHidden = false
    }
}
