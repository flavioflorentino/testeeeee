import AnalyticsModule

enum LoginAnalytics: AnalyticsKeyProtocol {
    case cnpjInserted(valid: Bool)
    case userSignedIn
    case userSignedOut

    var name: String {
        switch self {
        case .cnpjInserted:
            return "Login - CNPJ Inserted"
        case .userSignedIn:
            return "User Signed In"
        case .userSignedOut:
            return "User Signed Out"
        }
    }

    var properties: [String: Any] {
        switch self {
        case .cnpjInserted(let isValid):
            return [
                "CNPJ_validation": isValid ? "VALID" : "INVALID"
            ]
        default:
            return [:]
        }
    }

    var providers: [AnalyticsProvider] {
        switch self {
        case .cnpjInserted:
            return [.mixPanel]
        default:
            return [.eventTracker]
        }
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
