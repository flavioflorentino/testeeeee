import AnalyticsModule

enum AppAnalytics: AnalyticsKeyProtocol {
    case openApp
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .openApp:
            return AnalyticsEvent("OpenApp", providers: [.mixPanel, .appsFlyer, .firebase])
        }
    }
}
