import AnalyticsModule
import UI
import UIKit

final class LoginCnpjViewController: FormViewController {
    // MARK: - Properties
    
    var model: LoginViewModel?
    private let dependencies: HasAnalytics = DependencyContainer()
    
    // MARK: - IB Outlet
    
    @IBOutlet private var cnpjTextField: UIRoundedTextField!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureInputs()
        view.backgroundColor = Palette.Business.grayscale600.color
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    // MARK: - User Actions
    
    @IBAction func validate(_ sender: UIButton?) {
        startLoading()
        populateForm()
        model?.validate({ [weak self] error in
            self?.stopLoading()
            self?.dependencies.analytics.log(LoginAnalytics.cnpjInserted(valid: error == nil))
            if let error = error {
                UIAlertController(error: error).show()
            } else {
                self?.presentLogin()
            }
        })
    }
    
    // MARK: - Dependencie Injection
    
    override func configureDependencies() {
        setup(model: LoginImpViewModel())
        super.configureDependencies()
    }
    
    func setup(model: LoginViewModel) {
        self.model = model
        isReady = true
    }
    
    // MARK: - Internal Methods
    
    private func configureInputs() {
        controls.append(cnpjTextField)
    
        cnpjTextField.delegate = self
    }
    
    /// Show the login view controller
    private func presentLogin() {
        guard let model = model else {
            return
        }
        let loginController: LoginViewController = ViewsManager.instantiateViewController(.main)
        loginController.setup(model: model, dependencies: DependencyContainer())
        navigationController?.pushViewController(loginController, animated: true)
    }
    
    private func populateForm() {
        if let cnpj = cnpjTextField.text {
            model?.cnpj = cnpj
        }
    }
}

extension LoginCnpjViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        validate(nil)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if textField == cnpjTextField {
            if string.isEmpty {
                return true
            }
            
            let v: String = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = self.maskString(input: v, mask: "[00].[000].[000]/[0000]-[00]")
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        return true
    }
}
