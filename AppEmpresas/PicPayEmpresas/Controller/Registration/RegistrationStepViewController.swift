import InputMask
import UIKit
import Validator

class RegistrationStepViewController: BaseViewController {
    // MARK: - Public Properties
    var valid: Bool = true
    var loading: Bool = false
    var model: RegistrationViewModel?
    
    lazy var controls: [UIControl] = {
        [UIControl]()
    }()
    
    /// Enable the validation when the user touch on contorller
    var validateOnInputChange: Bool = false {
        didSet {
            // Validate all elements
            for obj in self.controls {
                if let validatable = obj as? UITextField {
                    if validatable.validationRules != nil {
                        validatable.validateOnInputChange(enabled: validateOnInputChange)
                    }
                }
            }
        }
    }
    
    private var nextButtonTitle: String = ""
    
    // MARK: - IB Outlets
    @IBOutlet var nextButton: UIPPButton?
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var textLabel: UILabel?
    @IBOutlet private var activityIndicator: UIActivityIndicatorView?
    @IBOutlet private var messageLabel: UILabel?
    @IBOutlet private var contentView: UIView?
    @IBOutlet private var bottomConstraint: NSLayoutConstraint?
    @IBOutlet private var scrollView: UIScrollView?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityIndicator?.isHidden = true
        self.messageLabel?.text = ""
        
        if let title = nextButton?.titleLabel?.text {
            self.nextButtonTitle = title
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        tapGesture.delaysTouchesBegan = false
        tapGesture.delaysTouchesEnded = false
        self.view.addGestureRecognizer(tapGesture)
        
        // Add observer to keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillOpen), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // MARK: - Setup Dependency
    override func configureDependencies() {
        self.setup(model: RegistrationViewModel())
        super.configureDependencies()
    }
    
    /// Setup the model
    func setup(model: RegistrationViewModel) {
        self.model = model
        isReady = true
    }
    
    // MARK: - Internal Methods
    @discardableResult
    func validate(_ showMessage: Bool = false) -> ValidationResult {
        var results: [ValidationResult] = [ValidationResult]()
        
        // Validate all elements
        for obj in self.controls {
            if let validatable = obj as? UIRoundedTextField {
                if validatable.validationRules != nil {
                    let result = validatable.validate()
                    results.append(result)
                    validatable.hasError = !result.isValid
                }
            }
        }
        
        // Merge all results
        let result = ValidationResult.merge(results: results)
        self.showValidationResult(result: result)
        self.valid = result.isValid

        return result
    }
    
    // show the validation results
    func showValidationResult(result: ValidationResult, showMessage: Bool = true) {
        switch result {
        case .invalid(let errors):
            if showMessage {
                var message = ""
                for error in errors {
                    message += error.message + "\n"
                }
                self.showMessage(text: message)
            }
        default:
            self.showMessage(text: "")
        }
    }
    
    func startLoading() {
        loading = true
        if let title = self.nextButton?.titleLabel?.text {
            nextButtonTitle = title
        }
        self.activityIndicator?.isHidden = false
        self.activityIndicator?.startAnimating()
        self.nextButton?.setTitle("", for: .disabled)
        self.nextButton?.isEnabled = false
        
        for control in controls {
            control.isEnabled = false
        }
        UIView.animate(withDuration: 0.50, animations: {
            for control in self.controls {
                control.layer.opacity = 0.35
            }
        })
    }
    
    func stopLoading() {
        loading = false
        nextButton?.isEnabled = true
        nextButton?.setTitle(nextButtonTitle, for: .disabled)
        activityIndicator?.stopAnimating()
        activityIndicator?.isHidden = true
        
        controls.forEach { $0.isEnabled = true }
        UIView.animate(withDuration: 0.50, animations: {
            for control in self.controls {
                control.layer.opacity = 1.0
            }
        })
    }
    
    @objc
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    /// show the messagem on screen
    func showMessage(text: String, show: Bool = true) {
        if show {
            messageLabel?.isHidden = false
            messageLabel?.text = text + (text.isEmpty ? "" : "\n")
        }
    }
    
    func hideMessage() {
        messageLabel?.text = ""
        messageLabel?.isHidden = true
    }
    
    /// Apply the mask on the string
    func maskString(input: String, mask: String) -> String {
        do {
            let mask = try Mask(format: mask)
            let result: Mask.Result = mask.apply(toText: CaretString(string: input), autocomplete: true)
            return result.formattedText.string
        } catch {
            return input
        }
    }
    
    // MARK: - Keyboard Events Handler
    
    @objc
    func keyboardWillOpen(keyboardNotification: NSNotification) {
        if let animationDuration = ((keyboardNotification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey]) as? NSNumber)?.doubleValue {
            if let keyboardSize = keyboardNotification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect {
                // animate the view to adjust height with the opened keyboard
                self.view.layoutIfNeeded()
                self.bottomConstraint?.constant = keyboardSize.size.height
                
                UIView.animate(withDuration: animationDuration) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    @objc
    func keyboardWillHide(keyboardNotification: NSNotification) {
        if let animationDuration = ((keyboardNotification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey]) as? NSNumber)?.doubleValue {
            // animate the view to adjust height with the opened keyboard
            self.view.layoutIfNeeded()
            self.bottomConstraint?.constant = 0
            
            UIView.animate(withDuration: animationDuration) {
                self.view.layoutIfNeeded()
            }
        }
        
        self.scrollView?.setContentOffset(.zero, animated: true)
    }
    
    // MARK: - Default TextFieldDelegate
    
    @objc
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.scrollView?.setContentOffset(CGPoint(x: 0, y: textField.frame.origin.y - 100), animated: true)
    }
    
    func push<T: RegistrationStepViewController>(to: T.Type, model: RegistrationViewModel) {
        let controller: T = ViewsManager.instantiateViewController(.registration)
        controller.setup(model: model)
        navigationController?.pushViewController(controller, animated: true)
    }
}
