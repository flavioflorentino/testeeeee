import UIKit
import Validator

final class RegistrationPhoneViewControllerOld: RegistrationStepViewController {
    // MARK: - Private Properties
    
    enum ValidateError: String, ValidationError {
        case dddInvalid = "O ddd deve ser preenchido corretamente"
        case phoneInvalid = "O telefone deve preenchido corretamente"
        
        var message: String {
            rawValue
        }
    }
    
    @IBOutlet private var dddTextField: UIRoundedTextField!
    @IBOutlet private var phoneTextField: UIRoundedTextField!
    @IBOutlet private var mobileDddTextField: UIRoundedTextField!
    @IBOutlet private var mobileTextField: UIRoundedTextField!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.controls.append(mobileTextField)
        self.controls.append(mobileDddTextField)
        self.controls.append(dddTextField)
        self.controls.append(phoneTextField)
        
        self.mobileDddTextField.delegate = self
        self.mobileTextField.delegate = self
        self.dddTextField.delegate = self
        self.phoneTextField.delegate = self
        
        var dddTypeRules = ValidationRuleSet<String>()
        dddTypeRules.add(rule: ValidationRuleLength(min: 2, error: ValidateError.dddInvalid))
        mobileDddTextField.validationRules = dddTypeRules
        
        var phoneTypeRules = ValidationRuleSet<String>()
        phoneTypeRules.add(rule: ValidationRuleLength(min: 9, error: ValidateError.phoneInvalid))
        mobileTextField.validationRules = phoneTypeRules
        
        self.validateOnInputChange = false
		
		TrackingManager.trackEventPublicAuth(.obPhoneNumbers, authEventName: .obMultPhoneNumbers, properties: nil)
    }
    
    /// Save data and finish the step
    @IBAction func finishStep(_ sender: UIButton?) {
        self.validate(true)
        
        if valid {
            self.saveFormData()
            self.goToNextStep()
        }
    }
    
    // MARK: - Internal Methods
    
    /// Send to the next screen of the registration form
    func goToNextStep() {
        guard let model = model else { return }
        
        let properties = [
            TrackingManager.TrackingManagerProperties.ddd.rawValue: model.account.mobilePhoneDdd,
            TrackingManager.TrackingManagerProperties.landline.rawValue: model.account.phone.isEmpty ? "NO" : "YES"
        ]
		
		TrackingManager.trackEventPublicAuth(.obSettedPhoneNumbers, authEventName: .obMultSettedPhoneNumbers, properties: properties)
        
        let addressViewController: RegistrationAddressStepViewController = ViewsManager.instantiateViewController(.registration)
        addressViewController.addressType = .personal
        addressViewController.setup(model: model)
        navigationController?.pushViewController(addressViewController, animated: true)
    }
    
    /// save form data
    func saveFormData() {
        if let ddd = dddTextField.text {
            self.model?.account.ddd = ddd
        }
        
        if let phone = phoneTextField.text {
            self.model?.account.phone = phone
        }
        
        if let ddd = mobileDddTextField.text {
            self.model?.account.mobilePhoneDdd = ddd
        }
        
        if let phone = mobileTextField.text {
            self.model?.account.mobilePhone = phone
        }
    }
    
    // MARK: - User Actions
    
    /// Move the cursor to next field
    /// If the field is the last field try to finish the step
    func goToNextField(from: UITextField) {
        if from == self.dddTextField {
            self.phoneTextField.becomeFirstResponder()
        } else {
            self.finishStep(nil)
        }
    }
}

extension RegistrationPhoneViewControllerOld: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        goToNextField(from: textField)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if string.isEmpty { return true }
        let v: String = (text as NSString).replacingCharacters(in: range, with: string)
        
        if textField == self.dddTextField {
            if v.count > 2 {
                phoneTextField.becomeFirstResponder()
                return false
            }
            textField.text = self.maskString(input: v, mask: "[00]")
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        if textField == self.mobileDddTextField {
            if v.count > 2 {
                mobileTextField.becomeFirstResponder()
                return false
            }
            textField.text = self.maskString(input: v, mask: "[00]")
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        if textField == phoneTextField || textField == mobileTextField {
            textField.text = self.maskString(input: v, mask: "[00009]-[0000]")
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        return true
    }
}
