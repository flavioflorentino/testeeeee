import AnalyticsModule

enum BankAccountAnalyticsType: String {
    case pf = "PF"
    case pj = "PJ"
}

enum RegistrationAnalytics: AnalyticsKeyProtocol {
    case identifyProfile
    case typeBusiness
    case insertCNPJ
    case daysToWithdraw
    case typePRO
    case settedPassword(properties: [String: Any])
    case finished(_ type: BankAccountAnalyticsType)
    case invalidDocument
    case addressType
    case newAccountRegister
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .identifyProfile:
            return AnalyticsEvent("OB Business Type", properties: [:], providers: [.appsFlyer, .mixPanel])
        case .typeBusiness:
            return AnalyticsEvent("OB Business Type Business", properties: [:], providers: [.appsFlyer, .mixPanel])
        case .insertCNPJ:
            return AnalyticsEvent("OB Your Business", properties: [:], providers: [.mixPanel, .appsFlyer])
        case .daysToWithdraw:
            return AnalyticsEvent("OB Days to Withdrawal", properties: [:], providers: [.mixPanel])
        case .typePRO:
            return AnalyticsEvent("OB Business Type PRO", properties: [:], providers: [.appsFlyer, .mixPanel])
        case .settedPassword(let properties):
            return AnalyticsEvent("OB Setted Password", properties: properties, providers: [.mixPanel])
        case .finished(let type):
            return AnalyticsEvent("OB Registration Completed", properties: ["bank_account_type": type.rawValue], providers: [.appsFlyer, .mixPanel])
        case .invalidDocument:
            return AnalyticsEvent("OB Invalid Bank Account CPF", properties: [:], providers: [.appsFlyer, .mixPanel])
        case .addressType:
            return AnalyticsEvent("OB Address Type", properties: [:], providers: [.mixPanel])
        case .newAccountRegister:
            return AnalyticsEvent("OB New Account Registered", properties: [:], providers: [.mixPanel, .appsFlyer, .firebase])
        }
    }
}
