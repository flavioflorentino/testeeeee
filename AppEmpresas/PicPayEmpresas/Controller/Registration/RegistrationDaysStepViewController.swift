import AnalyticsModule
import UI
import UIKit
import FeatureFlag
import LegacyPJ

final class RegistrationDaysStepViewController: RegistrationStepViewController {
    var timer: Timer?
    private let dependencies: HasAnalytics & HasFeatureManager = DependencyContainer()

    // MARK: - IB Outlet
    
    @IBOutlet private var daysToolTipView: UIView!
    @IBOutlet private var daysToolTipLabel: UILabel!
    @IBOutlet private var daysSlider: UISlider!
    @IBOutlet private var daysToolTipTextLabel: UILabel!
    
    @IBOutlet private var feeLabel: UILabel!
    @IBOutlet private var feeStartLabel: UILabel!
    @IBOutlet private var feeEndLabel: UILabel!
    
    @IBOutlet private var daysStartLabel: UILabel!
    @IBOutlet private var daysEndLabel: UILabel!
    @IBOutlet private var linksTextView: UITextView!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        daysToolTipView.isHidden = true
        daysToolTipLabel.isHidden = true
        daysSlider.isHidden = true
        daysToolTipTextLabel.isHidden = true
        configureFee()
        configureLinks()
        
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(loadFeeList), userInfo: nil, repeats: true)
        
        loadFeeList()
        daysSlider.addTarget(self, action: #selector(handlerSliderChange), for: UIControl.Event.valueChanged)
		
        dependencies.analytics.log(RegistrationAnalytics.daysToWithdraw)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        daysSlider.setNeedsLayout()
        
        self.updateDayToolTip()
        self.updateFee()
    }
    
    // MARK: - User Events
    
    /// Save data and finish the step
    @IBAction func finishStep(_ sender: UIButton?) {
        self.startLoading()
        self.saveFormData()
        self.model?.finishTaxDay(callback: { [weak self] error in
            self?.stopLoading()
            if let error = error {
                self?.showMessage(text: error.localizedDescription)
                return
            }
            self?.goToNextStep()
        })
    }
    
    // MARK: - Private Methods
    
    @objc
    func loadFeeList() {
        guard let model = model else {
            return
        }
        
        startLoading()
        model.loadFeeList { [weak self] _ in
            self?.configureFee()
            self?.updateDayToolTip()
            self?.updateFee()
            self?.stopLoading()
        }
        
        if model.feeListItems.isNotEmpty {
            timer?.invalidate()
        }
    }
    
    /// configure the view with fee data
    private func configureLinks() {
        if let attr: NSMutableAttributedString = linksTextView?.attributedText.mutableCopy() as? NSMutableAttributedString {
            attr.setAsLink("Termos de Uso", linkURL: Endpoint.urlTermsOfUse())
            linksTextView.attributedText = attr
            linksTextView.delegate = self
            linksTextView.isUserInteractionEnabled = true
            linksTextView.delaysContentTouches = false
            linksTextView.isSelectable = true
            linksTextView.linkTextAttributes = [NSAttributedString.Key.foregroundColor: Colors.branding300.color]
        }
    }
    
    private func configureFee() {
        guard let model = model else {
            return
        }
        
        if let firstDay = Float(model.feeFirstDay) {
            daysSlider.minimumValue = firstDay
            daysStartLabel.text = model.feeFirstDay + " " + (firstDay > 1 ? "dias" : "dia")
            
            if let fee = model.feeByDay(day: model.feeFirstDay) {
                feeStartLabel.text = fee
            }
        }
        if let lastDay = Float(model.feeLastDay) {
            daysSlider.maximumValue = lastDay
            daysEndLabel.text = model.feeLastDay + " dias"
            
            if let fee = model.feeByDay(day: model.feeLastDay) {
                feeEndLabel.text = fee
            }
        }
        
        if let selectedDay = Float(model.feeDefaultDay) {
            if selectedDay >= daysSlider.minimumValue && selectedDay <= daysSlider.maximumValue {
                daysSlider.value = selectedDay
            }
        }
        
        daysToolTipView.isHidden = false
        daysToolTipLabel.isHidden = false
        daysSlider.isHidden = false
        daysToolTipTextLabel.isHidden = false
    }
    
    /// Update the days on tooltip and position the view at the slider's thumb position
    private func updateDayToolTip() {
        let days = Int(daysSlider.value)
        daysToolTipLabel.text = "\(days)"
        
        daysToolTipTextLabel.text = days == 1 ? "dia" : "dias"
        
        var frame = daysToolTipLabel.frame
        frame.origin.x = daysSlider.thumbCenterX() + 10
        frame.origin.y = self.daysSlider.frame.origin.y - 47
        daysToolTipView.frame = frame
    }
    
    /// Update the fee value according to the selected number of days
    private func updateFee() {
        guard let model = model else {
            return
        }
        let day = Int(daysSlider.value)
        
        if let fee = model.feeByDay(day: "\(day)") {
            feeLabel.text = fee
        }
    }
    
    /// Send to the next screen of the registration form
    func goToNextStep() {
        guard let model = model else {
            return
        }
        
		TrackingManager.trackEventPublicAuth(
            .obSettedDaysToWithdrawal,
            authEventName: .obMultSettedDaysToWithdrawal,
            properties: [TrackingManager.TrackingManagerProperties.numberOfDays.rawValue: model.account.days])
        
        guard dependencies.featureManager.isActive(.opsNewFlowPasswordScenePJ) else {
            push(to: RegistrationPasswordStepViewController.self, model: model)
            return
        }
        
        let controller = RegistrationPasswordFactory.make(model: model)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    /// save form data
    private func saveFormData() {
        guard let model = model else {
            return
        }
        
        model.account.days = Int(daysSlider.value)
        model.feeDefaultDay = "\(Int(daysSlider.value))"
    }
    
    // MARK: - Events Handler
    
    @objc
    func handlerSliderChange() {
        self.updateDayToolTip()
        self.updateFee()
    }
}

extension RegistrationDaysStepViewController: UITextViewDelegate {
    // MARK: - UITextViewDelegate
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        shouldInteractWithURL(URL: URL)
    }
    
    @available(iOS 10.0, *)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        shouldInteractWithURL(URL: URL)
    }
    
    func shouldInteractWithURL(URL: URL) -> Bool {
        let controller = ViewsManager.webViewController(url: URL.absoluteString)
        navigationController?.pushViewController(controller, animated: true)
        return false
    }
}
