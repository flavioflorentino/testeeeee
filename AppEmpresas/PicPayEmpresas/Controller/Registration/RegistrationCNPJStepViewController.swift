import AnalyticsModule
import Foundation
import Validator

final class RegistrationCNPJStepViewController: RegistrationStepViewController {
    // MARK: - Private Properties
    
    enum ValidateError: String, ValidationError {
        case cpnjInvalid = "CNPJ inválido"
        case companyName = "A razão social deve ser preenchida corretamente"
        
        var message: String {
            rawValue
        }
    }
    
    private let dependencies: HasAnalytics = DependencyContainer()
    
    // MARK: - IB Outlets
    
    @IBOutlet private var cnpjTextField: UIRoundedTextField!
    @IBOutlet private var companyNameTextField: UIRoundedTextField!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureInputs()
        dependencies.analytics.log(RegistrationAnalytics.insertCNPJ)
    }
    
    /// Save data and finish the step
    @IBAction func finishStep(_ sender: UIButton?) {
        self.validateOnInputChange = true

        if self.validate(true).isValid {
            self.saveFormData()

            self.startLoading()
            self.model?.finishCnpjStep(callback: { [weak self] error in
                self?.stopLoading()
                
                if let error = error {
                    self?.showMessage(text: error.localizedDescription)
                    return
                }
                self?.goToNextStep()
            })
        }
    }
    
    // MARK: - Internal Methods
    
    /// Configure the step inputs
    func configureInputs() {
        // add text fields to the controls list
        controls.append(cnpjTextField)
        controls.append(companyNameTextField)
        
        // set delegate
        cnpjTextField.delegate = self
        companyNameTextField.delegate = self
        
        // Configure validations
        var cnpjRules = ValidationRuleSet<String>()
        cnpjRules.add(rule: ValidationRuleLength(min: 18, error: ValidateError.cpnjInvalid))
        cnpjTextField.validationRules = cnpjRules
        
        var companyNameRules = ValidationRuleSet<String>()
        companyNameRules.add(rule: ValidationRuleLength(min: 5, error: ValidateError.companyName))
        companyNameTextField.validationRules = companyNameRules
        
        self.validateOnInputChange = false
    }
    
    /// Send to the next screen of the registration form
    func goToNextStep() {
        guard let model = model else { return }
        let controller = RegistrationFixedLocationFactory.make(model: model)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func saveFormData() {
        if let cnpj = cnpjTextField.text {
            model?.account.cnpj = cnpj
        }
        if let companyName = companyNameTextField.text {
            model?.account.companyName = companyName
        }
    }
    
    // MARK: - User Actions
    
    /// Move the cursor to next field
    /// If the field is the last field try to finish the step
    func goToNextField(from: UITextField) {
        if from == self.cnpjTextField {
            companyNameTextField.becomeFirstResponder()
        } else {
            finishStep(nil)
        }
    }
}

extension RegistrationCNPJStepViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.goToNextField(from: textField)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        
        if textField == cnpjTextField {
            if string.isEmpty {
                return true
            }
            
            let v: String = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = self.maskString(input: v, mask: "[00].[000].[000]/[0000]-[00]")
            return false
        }
        
        return true
    }
}
