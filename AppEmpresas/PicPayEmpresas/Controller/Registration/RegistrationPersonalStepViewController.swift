import UIKit
import Validator

final class RegistrationPersonalStepViewController: RegistrationStepViewController {
    // MARK: - Private Properties
    
    enum ValidateError: String, ValidationError {
        case nameInvalid = "Nome completo obrigatório"
        case emailInvalid = "O email deve preenchida corretamente"
        case cpfInvalid = "O CPF deve ser preenchido corretamente"
        case birthdayInvalid = "A data de nascimento deve ser preenchida corretamente"
        case motherNameInvalid = "Nome completo da mãe obrigatório"
        
        var message: String {
            rawValue
        }
    }
    
    // MARK: - IB Outlets
    
    @IBOutlet private var nameTextField: UIRoundedTextField!
    @IBOutlet private var emailTextField: UIRoundedTextField!
    @IBOutlet private var cpfTextField: UIRoundedTextField!
    @IBOutlet private var birthdayTextField: UIRoundedTextField!
    @IBOutlet private var motherNameTextField: UIRoundedTextField!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureInputs()
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceOldRegistration())
		TrackingManager.trackEventPublicAuth(.obPersonalData, authEventName: .obMultPersonalData, properties: nil)
    }
    
    /// Save data and finish the step
    @IBAction func finishStep(_ sender: UIButton?) {
        self.validateOnInputChange = true
        self.validate(true)
        
        guard valid else { return }
        
        self.saveFormData()

        self.startLoading()
        self.model?.finishPersonalStep(callback: { [weak self] error in
            self?.stopLoading()
            if let error = error {
                self?.showMessage(text: error.localizedDescription)
                return
            }
            self?.goToNextStep()
        })
    }
    
    // MARK: - Internal Methods
    
    /// Configure the step inputs
    func configureInputs() {
        // add text fields to the controls list
        controls.append(nameTextField)
        controls.append(emailTextField)
        controls.append(cpfTextField)
        controls.append(birthdayTextField)
        controls.append(motherNameTextField)
        
        // set delegate
        nameTextField.delegate = self
        emailTextField.delegate = self
        cpfTextField.delegate = self
        birthdayTextField.delegate = self
        motherNameTextField.delegate = self
        
        // Configure validations
        var nameRules = ValidationRuleSet<String>()
        nameRules.add(rule: ValidationRuleLength(min: 5, error: ValidateError.nameInvalid))
        nameTextField.validationRules = nameRules
        
        var emailRules = ValidationRuleSet<String>()
        emailRules.add(rule: ValidationRulePattern(pattern: EmailValidationPattern.standard,
                                                   error: ValidateError.emailInvalid))
        emailTextField.validationRules = emailRules
        
        var cpfRules = ValidationRuleSet<String>()
        cpfRules.add(rule: ValidationRuleLength(min: 14, error: ValidateError.cpfInvalid))
        cpfTextField.validationRules = cpfRules
        
        var birthdayRules = ValidationRuleSet<String>()
        birthdayRules.add(rule: ValidationRuleLength(min: 10, error: ValidateError.birthdayInvalid))
        birthdayTextField.validationRules = birthdayRules
        
        var motherNameRules = ValidationRuleSet<String>()
        motherNameRules.add(rule: ValidationRuleLength(min: 5, error: ValidateError.motherNameInvalid))
        motherNameTextField.validationRules = motherNameRules
        
        self.validateOnInputChange = false
    }
    
    /// Send to the next screen of the registration form
    func goToNextStep() {
        guard let model = model else { return }
        
        let phoneViewController: RegistrationPhoneViewControllerOld = ViewsManager.instantiateViewController(.registration)
        
        phoneViewController.setup(model: model)
        navigationController?.pushViewController(phoneViewController, animated: true)
    }
    
    /// save form data
    func saveFormData() {
        if let name = nameTextField.text {
            model?.account.partner.name = name
        }
        
        if let email = emailTextField.text {
            model?.account.partner.email = email
        }
        
        if let cpf = cpfTextField.text {
            model?.account.partner.cpf = cpf
        }
        
        if let birthday = birthdayTextField.text {
            model?.account.partner.birthday = birthday
        }
        
        if let motherName = motherNameTextField.text {
            model?.account.partner.motherName = motherName
        }
    }
    
    // MARK: - User Actions
    
    /// Move the cursor to next field
    /// If the field is the last field try to finish the step
    func goToNextField(from: UITextField) {
        if from == self.nameTextField {
            emailTextField.becomeFirstResponder()
        } else if from == self.emailTextField {
            cpfTextField.becomeFirstResponder()
        } else if from == self.cpfTextField {
            birthdayTextField.becomeFirstResponder()
        } else if from == self.birthdayTextField {
            motherNameTextField.becomeFirstResponder()
        } else {
            finishStep(nil)
        }
    }
}

// MARK: - UITextFieldDelegate
extension RegistrationPersonalStepViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.goToNextField(from: textField)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        
        if string.isEmpty {
            return true
        }
        
        let v: String = (text as NSString).replacingCharacters(in: range, with: string)
        
        if textField == cpfTextField {
            textField.text = self.maskString(input: v, mask: "[000].[000].[000]-[00]")
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        if textField == birthdayTextField {
            textField.text = self.maskString(input: v, mask: "[00]/[00]/[0000]")
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        return true
    }
}
