import UIKit

final class RegistrationProViewController: BaseViewController {
    @IBOutlet private var bizLabel: UILabel!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bizLabel.isUserInteractionEnabled = true
        bizLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(signUp(_:))))
    }
    
    // MARK: - User Action
    
    @IBAction func signUp(_ sender: Any) {
		TrackingManager.trackEventPublicAuth(
            .obProSuggestion,
            authEventName: .obMultProSuggestion,
            properties: [TrackingManager.TrackingManagerProperties.cto.rawValue: "Back to Business"]
        )
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func installPicPayApp(_ sender: Any) {
		TrackingManager.trackEventPublicAuth(.obProSuggestion, authEventName: .obMultProSuggestion, properties: [TrackingManager.TrackingManagerProperties.cto.rawValue: "PicPay Download"])
        let picpayUrl = "https://picpay.onelink.me/R25q/96102a7b"
        guard let url = URL(string: picpayUrl), UIApplication.shared.canOpenURL(url) else {
            return
        }
        UIApplication.shared.open(url, options: [:])
    }
}
