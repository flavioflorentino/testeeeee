import MapKit
import UIKit

final class RegistrationMapStepViewController: RegistrationStepViewController {
    // MARK: - IB Outlets
    
    @IBOutlet private var mapView: MKMapView!
    @IBOutlet private var addressLabel: UILabel!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.populateAddress()
        self.centerMapAtAddress()
		
		TrackingManager.trackEventPublicAuth(.obMap, authEventName: .obMultMap, properties: nil)
    }
    
    // MARK: - User Actions
    
    /// Save data and finish the step
    @IBAction func finishStep(_ sender: UIButton?) {
        guard let model = model else {
            return
        }
        
        model.account.lat = self.mapView.centerCoordinate.latitude
        model.account.lon = self.mapView.centerCoordinate.longitude
        self.goToNextStep()
    }
    
    @IBAction func back(_ sender: UIButton?) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Internal Methods
    
    /// Center the map at adress
    /// Try to find the location from string address
    func centerMapAtAddress() {
        guard let model = model else {
            return
        }
        
        let coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(model.account.lat), longitude: CLLocationDegrees(model.account.lon))
        
        let zoomLevel = 16
        let span = MKCoordinateSpan(latitudeDelta: 0, longitudeDelta: 360 / pow(2, Double(zoomLevel)) * Double(self.view.frame.size.width) / 256)
        self.mapView.setRegion(MKCoordinateRegion(center: coordinate, span: span), animated: true)
        
        /*let address = "\(self.model!.addressNumber) \(self.model!.addressNumber), \(self.model!.district), \(self.model!.city) \(self.model!.state)"
        
        // Get the geolocation for the address
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(address) { [weak self] (placemarks, error) in
            if error == nil {
                if placemarks?.count != 0 {
                    if let coordinate = placemarks?[0].location?.coordinate {
                        let zoomLevel = 16
                        let span = MKCoordinateSpanMake(0, 360 / pow(2, Double(zoomLevel)) * Double((self?.view.frame.size.width)!) / 256)
                        self?.mapView.setRegion(MKCoordinateRegionMake(coordinate, span), animated: true)
                    }
                }
            }
        }*/
    }
    
    /// Populate the screen address with the data typed in the back screen
    func populateAddress() {
        guard let model = model else {
            return
        }
        
        var address = ""
        address += "\(model.account.address), \(model.account.addressNumber) \n"
        address += "\(model.account.district)\n"
        address += "\(model.account.city) - \(model.account.state)\n"
        address += "\(model.account.cep)"
        
        self.addressLabel.text = address
    }
    
    /// Send to the next screen
    func goToNextStep() {
        guard let model = model else {
            return
        }
        
        let controller: RegistrationImageStepViewController = ViewsManager.instantiateViewController(.registration)
        controller.setup(model: model)
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
