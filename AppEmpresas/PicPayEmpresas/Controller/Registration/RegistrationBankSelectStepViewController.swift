import LegacyPJ
import SDWebImage
import UIKit
import FeatureFlag

final class RegistrationBankSelectStepViewController: RegistrationStepViewController {
    // MARK: Private Properties
    
    private var loadingView: LoadingHeaderView?
    private var timer: Timer?
    
    private let dependencies: HasFeatureManager = DependencyContainer()
    
    private lazy var activityView: UIActivityIndicatorView = {
        if #available(iOS 13.0, *) {
            return UIActivityIndicatorView(style: .large)
        } else {
            return UIActivityIndicatorView(style: .gray)
        }
    }()
    
    // MARK: IB Outlet
    
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var accountDisclaimerLabel: UILabel!
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingView = LoadingHeaderView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100))
        loadBanks()
        timer = Timer.scheduledTimer(
            timeInterval: 5,
            target: self,
            selector: #selector(loadBanks),
            userInfo: nil,
            repeats: true
        )
  	    TrackingManager.trackEventPublicAuth(.obBankSelection, authEventName: .obMultBankSelection, properties: nil)
    }
    
    override func startLoading() {
        view.addSubview(activityView)
        activityView.center = view.center
        activityView.startAnimating()
    }
    
    override func stopLoading() {
        activityView.stopAnimating()
        activityView.removeFromSuperview()
    }
}

// MARK: Private Methods

private extension RegistrationBankSelectStepViewController {
    // Load the list of banks
    @objc
    func loadBanks() {
        guard let model = model else { return }
        
        setTableViewLoading(isLoading: true)
        
        model.loadBankList { [weak self] in
            self?.tableView.reloadData()
            self?.setTableViewLoading(isLoading: false)
        }
        
        if model.banks.isNotEmpty {
            timer?.invalidate()
        }
        
        if model.bankAccountType == .personal || dependencies.featureManager.isActive(.releaseBankAccountWarning) {
            accountDisclaimerLabel?.text = ""
        }
    }
    
    func didSelectBank(_ bank: Bank) {
        guard let model = model else { return }
        
        guard dependencies.featureManager.isActive(.releaseBankAccountWarning) else {
            goToNextStep(bank)
            return
        }
        
        startLoading()
        view.isUserInteractionEnabled = false
        model.fetchBankAccountTypes(bankID: bank.code) { [weak self] error in
            self?.stopLoading()
            self?.view.isUserInteractionEnabled = true
            guard error == nil else {
                self?.presentAccountTypesError()
                return
            }
            self?.goToNextStep(bank)
        }
    }
    
    /// Send to the next screen of the registration form
    func goToNextStep(_ bank: Bank) {
        guard let model = model else { return }
        
        model.account.bank = bank
        
        let controller: RegistrationBankStepViewController = ViewsManager.instantiateViewController(.registration)
        controller.setup(model: model)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func presentAccountTypesError() {
        let popup = PopupViewController()
        let alertPopup = AlertPopupController()
        alertPopup.popupTitle = "Ocorreu um erro"
        alertPopup.popupText = "Por favor, tente novamente mais tarde"
        popup.contentController = alertPopup
        present(popup, animated: true, completion: nil)
    }
    
    func setTableViewLoading(isLoading: Bool) {
        tableView.tableFooterView = isLoading ? loadingView : UIView()
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension RegistrationBankSelectStepViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - UITableViewDelegate
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = model else { return UITableViewCell() }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        if let cell = cell as? BankTableViewCell {
            let bank = model.banks[indexPath.row]
            cell.configureCell(bank)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let model = model else { return }
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let bank = model.banks[indexPath.row]
        didSelectBank(bank)
    }

    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model?.banks.count ?? 0
    }
}
