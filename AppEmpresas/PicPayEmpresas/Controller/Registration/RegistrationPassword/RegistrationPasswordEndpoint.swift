import Core
import FeatureFlag

enum RegistrationPasswordEndpoint {
    case useTerms
    case contract
}

extension RegistrationPasswordEndpoint {
    var url: String {
        switch self {
        case .useTerms:
            return FeatureManager.shared.text(.urlTerms)
        case .contract:
            return FeatureManager.shared.text(.urlWallet)
        }
    }
}
