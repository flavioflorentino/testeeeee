import AnalyticsModule

enum RegistrationPasswordAnalytics: AnalyticsKeyProtocol {
    case useTerms
    case contract
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .useTerms:
            return getEventsFor(name: "FT Terms Of Use")
            
        case .contract:
            return getEventsFor(name: "FT Account Contract")
        }
    }
}

private extension RegistrationPasswordAnalytics {
    func getEventsFor(name: String) -> AnalyticsEvent {
        AnalyticsEvent(name, providers: [.mixPanel])
    }
}
