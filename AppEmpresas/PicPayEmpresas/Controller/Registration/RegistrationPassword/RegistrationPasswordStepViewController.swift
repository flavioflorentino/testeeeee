import AnalyticsModule
import LegacyPJ
import UI
import UIKit
import Validator
import FeatureFlag

final class RegistrationPasswordStepViewController: RegistrationStepViewController {
    typealias Localizable = Strings.RegistrationPassword
    
    enum ValidateError: ValidationError {
        case passwordInvalid
        case termsNotChecked
        
        var rawValue: String {
            switch self {
            case .passwordInvalid:
                return Localizable.passwordMinimumCharacters
                
            case .termsNotChecked:
                return Localizable.acceptTermsToContinue
            }
        }
        
        var message: String {
            rawValue
        }
    }
    
    // MARK: - Private Properties
    
    private var showPasswordTrackEventProperty: Bool {
        !passwordTextField.isSecureTextEntry
    }
    
    private let dependencies: HasAnalytics & HasAppCoordinator & HasFeatureManager = DependencyContainer()
    
    // MARK: - IB Outlets
    
    @IBOutlet private var passwordTextField: UIRoundedTextField!
    @IBOutlet private var showPasswordImage: UIImageView!
    @IBOutlet private var termsCheckbox: UIButton!
    @IBOutlet private var termsTextView: UITextView!
    @IBOutlet private var termsErrorLabel: UILabel!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nextButton?.isEnabled = false
        termsErrorLabel.text = ValidateError.termsNotChecked.rawValue
        setupPasswordTextField()
        setupTermsTextView()
        setupCheckbox()
        TrackingManager.trackEventPublicAuth(.obPassword, authEventName: .obMultPassword, properties: nil)
    }
    
    // MARK: - Actions
    
    @IBAction private func finishStep(_ sender: UIButton?) {
        validateOnInputChange = true
        validate(true)
        
        if valid {
            savePassword()
        }
    }
    
    @IBAction private func termsCheckboxTouched(_ sender: Any) {
        termsCheckbox.isSelected.toggle()
        nextButton?.isEnabled = termsCheckbox.isSelected
    }
}

// MARK: - Private Methods

private extension RegistrationPasswordStepViewController {
    func setupCheckbox() {
        termsCheckbox.setImage(Assets.Checkbox.checkboxCheck.image, for: .selected)
        termsCheckbox.setImage(Assets.Checkbox.checkboxUncheck.image, for: .normal)
    }
    
    func setupTermsTextView() {
        termsTextView.delegate = self
        termsTextView.linkTextAttributes = [NSAttributedString.Key.foregroundColor: Palette.ppColorBranding300.color]
        
        let text = NSMutableAttributedString()
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 6
        
        let defaultStyle: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12),
            NSAttributedString.Key.paragraphStyle: paragraphStyle
        ]
        
        var textStyle = defaultStyle
        textStyle[NSAttributedString.Key.foregroundColor] = Palette.Business.grayscale400.color
        
        var useTermsLinkStyle = defaultStyle
        useTermsLinkStyle[NSAttributedString.Key.link] = RegistrationPasswordEndpoint.useTerms.url
        
        text.append(NSAttributedString(string: Localizable.agreeWith, attributes: textStyle))
        text.append(NSAttributedString(string: Localizable.useTerms, attributes: useTermsLinkStyle))
        
        if model?.ableToDigitalAccount == true {
            var contractTermsLinkStyle = defaultStyle
            contractTermsLinkStyle[NSAttributedString.Key.link] = RegistrationPasswordEndpoint.contract.url
            
            text.append(NSAttributedString(string: Localizable.acceptThe, attributes: textStyle))
            text.append(NSAttributedString(string: Localizable.contract, attributes: contractTermsLinkStyle))
        }
        
        termsTextView.attributedText = text
    }
    
    func setupPasswordTextField() {
        controls.append(passwordTextField)
        passwordTextField.delegate = self
        
        passwordTextField.isSecureTextEntry = true
        toggleShowPassword()
        
        var passwordRules = ValidationRuleSet<String>()
        passwordRules.add(rule: ValidationRuleLength(min: 6, error: ValidateError.passwordInvalid))
        passwordTextField.validationRules = passwordRules
        
        validateOnInputChange = false
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(toggleShowPassword))
        showPasswordImage.addGestureRecognizer(tapGesture)
        showPasswordImage.isUserInteractionEnabled = true
    }
    
    func savePassword() {
        saveFormData()
        startLoading()
        
        let analyticsProperties: [String: Any] = [
            TrackingManager.TrackingManagerProperties.openPasswordTextField.rawValue: showPasswordTrackEventProperty ? "YES" : "NO"
        ]
        dependencies.analytics.log(RegistrationAnalytics.settedPassword(properties: analyticsProperties))
        
        model?.finishRegistration(callback: { [weak self] error in
            self?.stopLoading()
            if let error = error {
                self?.showMessage(text: error.localizedDescription)
                return
            }
            
            let type = self?.model?.bankAccountType == .personal ?
                BankAccountAnalyticsType(rawValue: "PF") :
                BankAccountAnalyticsType(rawValue: "PJ")
            self?.dependencies.analytics.log(RegistrationAnalytics.finished(type ?? BankAccountAnalyticsType.pf))
            self?.goToNextStep()
        })
    }
    
    func goToNextStep() {
        if dependencies.featureManager.isActive(.isAppCoordinatorAvailable) {
            dependencies.appCoordinator.displayAuthenticated()
        } else {
            AppManager.shared.presentAuthenticatedViewController()
        }
        dependencies.analytics.log(RegistrationAnalytics.newAccountRegister)
        TrackingManager.trackFirebaseEvent("complete_registration")
    }
    
    func saveFormData() {
        guard let password = passwordTextField.text else {
            return
        }
        
        model?.account.password = password
    }
    
    /// Toggle the password visibility
    @objc
    func toggleShowPassword() {
        if passwordTextField.isSecureTextEntry {
            passwordTextField.isSecureTextEntry = false
            showPasswordImage.image = Assets.iconPasswordEyeBold.image
        } else {
            passwordTextField.isSecureTextEntry = true
            showPasswordImage.image = Assets.iconPasswordEye.image
        }
    }
}

// MARK: - UITextFieldDelegate

extension RegistrationPasswordStepViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        finishStep(nil)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        true
    }
}

// MARK: - UITextViewDelegate

extension RegistrationPasswordStepViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if URL.absoluteString == RegistrationPasswordEndpoint.useTerms.url {
            dependencies.analytics.log(RegistrationPasswordAnalytics.useTerms)
        } else {
            dependencies.analytics.log(RegistrationPasswordAnalytics.contract)
        }
        
        let controller = ViewsManager.webViewController(url: URL.absoluteString)
        navigationController?.pushViewController(controller, animated: true)
        return false
    }
}
