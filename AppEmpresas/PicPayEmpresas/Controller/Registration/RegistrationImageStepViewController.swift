import Core
import UIKit
import Validator
import FeatureFlag

final class RegistrationImageStepViewController: RegistrationStepViewController, UINavigationControllerDelegate {
    typealias Dependencies = HasFeatureManager
    
    // MARK: - Private Properties
    
    enum ValidateError: String, ValidationError {
        case nameInvalid = "O nome de exibição deve ser preenchido corretamente."
        case imageInvalid = "Envie o logotipo da sua loja."
        
        var message: String {
            rawValue
        }
    }
    
    private let dependencies: Dependencies = DependencyContainer()
    
    // MARK: - IB Outlets
    
    @IBOutlet private var imageView: UICircularImageView!
    @IBOutlet private var imageButton: UIButton!
    @IBOutlet private var nameTextField: UIRoundedTextField!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureInputs()
		TrackingManager.trackEventPublicAuth(.obNameLogo, authEventName: .obMultNameLogo, properties: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceOldRegistration())
    }
    
    /// Save data and finish the step
    @IBAction func finishStep(_ sender: UIButton?) {
        self.validateOnInputChange = true
        
        guard validate(true).isValid else { return }
        
        self.saveFormData()
        
        self.model?.finishImageStep(callback: { [weak self] error in
            guard let self = self else { return }
            
            if let error = error {
                self.showMessage(text: error.localizedDescription)
                return
            }
            
            let logoUpload = self.model?.account.image != nil ? "YES" : "NO"
            TrackingManager.trackEventPublicAuth(
                .obSettedName,
                authEventName: .obMultSettedName,
                properties: [TrackingManager.TrackingManagerProperties.logoUpload.rawValue: logoUpload]
            )
            self.goToNextStep()
        })
    }
    
    /// Select a new image
    @IBAction func selectImage(_sender: UIButton?) {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        
        present(imagePicker, animated: true)
    }
    
    // MARK: - Internal Methods
    
    /// Configure the step inputs
    func configureInputs() {
        // add text fields to the controls list
        self.controls.append(nameTextField)
        self.controls.append(imageButton)
        
        // set delegate
        self.nameTextField.delegate = self
        
        // Configure validations
        var nameRules = ValidationRuleSet<String>()
        nameRules.add(rule: ValidationRuleLength(min: 4, error: ValidateError.nameInvalid))
        nameTextField.validationRules = nameRules
        
        self.validateOnInputChange = false
    }
    
    /// Send to the next screen of the registration form
    func goToNextStep() {
        guard let model = model else { return }
        
        guard dependencies.featureManager.isActive(.releaseBankAccountWarning) else {
            switch model.companyType {
            case .individual:
                let accountTypeViewController = RegistrationAccountTypeFactory.make(model: model)
                navigationController?.pushViewController(accountTypeViewController, animated: true)
            case .multiPartners:
                pushBankSelectionView()
            }
            return
        }
        
        navigationController?.navigationBar.setup(appearance: NavigationBarAppearanceDefault())
        let viewController = BankAccountTipsFactory.makeRegisterTips(companyType: model.companyType, registrationModel: model)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func pushBankSelectionView() {
        guard let model = model else { return }
        
        let bankViewController: RegistrationBankSelectStepViewController =
            ViewsManager.instantiateViewController(.registration)
        bankViewController.setup(model: model)
        navigationController?.pushViewController(bankViewController, animated: true)
    }
    
    /// save form data
    func saveFormData() {
        if let displayName = nameTextField.text {
            self.model?.account.displayName = displayName
        }
    }
    
    /// Validate de form
    @discardableResult
    override func validate(_ showMessage: Bool) -> ValidationResult {
        let result = super.validate(showMessage)
        
        self.valid = result.isValid
        return result
    }
}

// MARK: - UIImagePickerControllerDelegate Methods
extension RegistrationImageStepViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        guard let pickedImage = info[.editedImage] as? UIImage else {
            dismiss(animated: true, completion: nil)
            return
        }
        
        if let resizedImage = pickedImage.resizedImageForUpload() {
            imageView.image = pickedImage
            self.model?.account.image = resizedImage.jpegData(compressionQuality: 0.8)
        }
        
        DispatchQueue.main.async {
            self.imageButton.setAttributedTitle(NSAttributedString(string: ""), for: .normal)
            
            // clear image validation
            self.imageView.layer.borderColor = UIColor.clear.cgColor
            self.imageView.layer.borderWidth = 0.0
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - UITextFieldDelegate
extension RegistrationImageStepViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        finishStep(nil)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        true
    }
}
