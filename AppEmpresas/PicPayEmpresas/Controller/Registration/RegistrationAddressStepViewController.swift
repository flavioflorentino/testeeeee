import Foundation
import InputMask
import LegacyPJ
import UIKit
import Validator

final class RegistrationAddressStepViewController: RegistrationStepViewController {
    // MARK: - Private Properties
    
    enum ValidateError: String, ValidationError {
        case cepInvalid = "CEP inválido"
        case streetInvalid = "A rua deve preenchida corretamente"
        case numberInvalid = "O número deve ser preenchido corretamente"
        case districtInvalid = "O bairro deve ser preenchido corretamente"
        case cityInvalid = "A cidade deve ser preenchida corretamente"
        case stateInvalid = "O estado deve ser preenchido corretamente"
        
        var message: String {
            rawValue
        }
    }
    
    // MARK: - Public Properties
    
    var addressType: RegistrationType = .company
    
    // MARK: - IB Outlets
    
    @IBOutlet private var cepTextField: UIRoundedTextField!
    @IBOutlet private var streetTextField: UIRoundedTextField!
    @IBOutlet private var numberTextField: UIRoundedTextField!
    @IBOutlet private var complementTextField: UIRoundedTextField!
    @IBOutlet private var districtTextField: UIRoundedTextField!
    @IBOutlet private var cityTextField: UIRoundedTextField!
    @IBOutlet private var stateTextField: UIRoundedTextField!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureInputs()
        configureViewByAddressType()
        
        if addressType == .company {
            TrackingManager.trackEventPublicAuth(.obBusinessAddress, authEventName: .obMultBusinessAddress, properties: nil)
        } else {
            TrackingManager.trackEventPublicAuth(.obHomeAddress, authEventName: .obMultHomeAddress, properties: nil)
        }
    }
    
    /// Save data and finish the step
    @IBAction func finishStep(_ sender: UIButton?) {
        self.validateOnInputChange = true
        self.validate(true)
        
        guard valid else { return }
        self.saveFormData()
        
        self.startLoading()
        self.model?.finishAddressStep(callback: { [weak self] error in
            self?.stopLoading()
            if let error = error {
                self?.showMessage(text: error.localizedDescription)
                return
            }
            self?.goToNextStep()
        })
    }
    
    // MARK: - Internal Methods
    
    /// Configure the step inputs
    func configureInputs() {
        // add text fields to the controls list
        controls.append(cepTextField)
        controls.append(streetTextField)
        controls.append(numberTextField)
        controls.append(complementTextField)
        controls.append(districtTextField)
        controls.append(cityTextField)
        controls.append(stateTextField)
        
        // set delegate
        cepTextField.delegate = self
        streetTextField.delegate = self
        numberTextField.delegate = self
        complementTextField.delegate = self
        districtTextField.delegate = self
        cityTextField.delegate = self
        stateTextField.delegate = self
        
        // Configure validations
        var cepRules = ValidationRuleSet<String>()
        cepRules.add(rule: ValidationRuleLength(min: 9, error: ValidateError.cepInvalid))
        cepTextField.validationRules = cepRules
        
        var streetRules = ValidationRuleSet<String>()
        streetRules.add(rule: ValidationRuleLength(min: 4, error: ValidateError.streetInvalid))
        streetTextField.validationRules = streetRules
        
        var numberRules = ValidationRuleSet<String>()
        numberRules.add(rule: ValidationRuleLength(min: 1, error: ValidateError.numberInvalid))
        numberTextField.validationRules = numberRules
        
        var districtRules = ValidationRuleSet<String>()
        districtRules.add(rule: ValidationRuleLength(min: 4, error: ValidateError.districtInvalid))
        districtTextField.validationRules = districtRules
        
        var cityRules = ValidationRuleSet<String>()
        cityRules.add(rule: ValidationRuleLength(min: 4, error: ValidateError.cityInvalid))
        cityTextField.validationRules = cityRules
        
        var stateRules = ValidationRuleSet<String>()
        stateRules.add(rule: ValidationRuleLength(min: 2, error: ValidateError.stateInvalid))
        stateTextField.validationRules = stateRules
        
        validateOnInputChange = false
    }
    
    func configureViewByAddressType() {
        if addressType == .company {
            titleLabel?.text = Strings.Default.companyAddressScreenTitle
            textLabel?.text = Strings.Default.companyAddressScreenDescription
        } else {
            titleLabel?.text = Strings.Default.partnerAddressScreenTitle
            textLabel?.text = Strings.Default.partnerAddressScreenDescription
        }
    }
    
    /// Send to the next screen of the registration form
    func goToNextStep() {        
        guard let model = model else { return }
        
        let controller = chooseDestiny(
            addressType: addressType,
            model: model
        )
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func chooseDestiny(addressType: RegistrationType, model: RegistrationViewModel) -> UIViewController {
        if addressType == . personal {
            let controller = RegistrationChooseProfileFactory.make(model: model)
            return controller
        }
        
        let mapStepViewController: RegistrationMapStepViewController = ViewsManager.instantiateViewController(.registration)
        mapStepViewController.setup(model: model)
        return mapStepViewController
    }
    
    /// Load and populate the address according to cep
    func loadAddressByCep() {
        guard
            let cep = self.cepTextField.text,
            let model = model
            else {
            return
        }
        
        let result = cepTextField.validate()
        if result.isValid {
            self.startLoading()
            model.loadAddressByCep(cep: cep, callback: { [weak self] address, _ in
                if let add = address {
                    self?.streetTextField.text = add.address
                    self?.districtTextField.text = add.district
                    self?.cityTextField.text = add.city
                    self?.stateTextField.text = add.state
                }
                
                self?.stopLoading()
            })
        }
    }
    
    /// save form data
    func saveFormData() {
        var address: [String: String] = [:]
        
        if let cep = cepTextField.text {
            address["cep"] = cep
        }
        
        if let street = streetTextField.text {
            address["street"] = street
        }
        
        if let city = cityTextField.text {
            address["city"] = city
        }
        
        if let complement = complementTextField.text {
            address["complement"] = complement
        }
        
        if let district = districtTextField.text {
            address["district"] = district
        }
        
        if let number = numberTextField.text {
            address["number"] = number
        }
        
        if let state = stateTextField.text {
            address["state"] = state
        }
        
        model?.setAddress(type: addressType, address: address)
    }
    
    // MARK: - User Actions
    
    /// Move the cursor to next field
    /// If the field is the last field try to finish the step
    func goToNextField(from: UITextField) {
        if from == self.cepTextField {
            self.loadAddressByCep()
        } else if from == self.streetTextField {
            self.numberTextField.becomeFirstResponder()
        } else if from == self.numberTextField {
            self.complementTextField.becomeFirstResponder()
        } else if from == self.complementTextField {
            self.districtTextField.becomeFirstResponder()
        } else if from == self.districtTextField {
            self.cityTextField.becomeFirstResponder()
        } else if from == self.cityTextField {
            self.stateTextField.becomeFirstResponder()
        } else {
            self.finishStep(nil)
        }
    }
}

extension RegistrationAddressStepViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.goToNextField(from: textField)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if string.isEmpty { return true }
        let v: String = (text as NSString).replacingCharacters(in: range, with: string).replacingOccurrences(of: "-", with: "")
        
        if textField == cepTextField {
            textField.text = self.maskString(input: v, mask: "[00000]-[000]")
            self.loadAddressByCep()
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        if textField == stateTextField {
            textField.text = self.maskString(input: v, mask: "[AA]")
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        return true
    }
}
