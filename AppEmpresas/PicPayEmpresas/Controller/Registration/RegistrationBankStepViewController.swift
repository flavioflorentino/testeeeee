import AnalyticsModule
import AssetsKit
import SDWebImage
import UI
import UIKit
import Validator
import FeatureFlag
import LegacyPJ

enum BankName: String {
    case caixa
}

enum LegacyBankAccountType: String {
    private typealias Localizable = Strings.BankAccount
    
    case checking = "c"
    case savings = "p"
    
    var title: String {
        switch self {
        case .checking:
            return Localizable.Current.Legacy.title
        case .savings:
            return Localizable.Savings.Legacy.title
        }
    }
}

final class RegistrationBankStepViewController: RegistrationStepViewController {
    private typealias Localizable = Strings.BankAccount
    private typealias LocalizableWD = Strings.BankAccount.BankAccountForm.WrongDocument
    private typealias BankTipsLocalizable = Strings.BankAccount.BankAccountTips
    private typealias Dependencies = HasFeatureManager & HasAnalytics
    
    // MARK: - Private Properties
    
    enum ValidateError: String, ValidationError {
        case branchInvalid = "A agência deve ser informada corretamente"
        case branchDigitInvalid = "O dígito da agência deve ser preenchido corretamente"
        case accountInvalid = "O número da conta deve ser preenchido corretamente"
        case accountDigitInvalid = "O dígito da conta deve ser preenchido corretamente"
        case accountTypeInvalid = "Selecione o tipo de conta"
        case operationInvalid = "A operação deve ser informada corretamente"
        case cpfInvalid = "CPF inválido"
        case checkCPF = "Verifique o CPF informado"
        
        var message: String {
            rawValue
        }
    }
    
    private let dependencies: Dependencies = DependencyContainer()
    
    // MARK: - IB Outlet
    
    @IBOutlet private var bankImageView: UIImageView!
    @IBOutlet private var bankTextField: UIRoundedTextField!
    @IBOutlet private var branchTextField: UIRoundedTextField!
    @IBOutlet private var branchDigitTextField: UIRoundedTextField!
    @IBOutlet private var accountTextField: UIRoundedTextField!
    @IBOutlet private var accountDigitTextField: UIRoundedTextField!
    @IBOutlet private var typeTextField: UIRoundedTextField!
    @IBOutlet private var operationTextField: UIRoundedTextField!
    @IBOutlet private var documentTextField: UIRoundedTextField!
    @IBOutlet private var requirementsLabel: UILabel!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureInputs()
        setupRequirementsLabel()
		TrackingManager.trackEventPublicAuth(.obBankAccountForm, authEventName: .obMultBankAccountForm, properties: nil)
    }
    
    @IBAction func finishStep(_ sender: UIButton?) {
        validateOnInputChange = true
        
        guard validate(true).isValid else { return }
        
        documentTextField.hasError = false

        // CPF Validation
        if model?.bankAccountType == .personal, let text = documentTextField.text, !text.validCpf {
            documentTextField.hasError = true
            showMessage(text: ValidateError.cpfInvalid.message)
            return
        }
        
        saveBankAccount()
    }
}
    
// MARK: - UITextFieldDelegate

extension RegistrationBankStepViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        guard textField === typeTextField else {
            return true
        }
        
        view.endEditing(true)
        showAccountTypes()
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        goToNextField(from: textField)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            return true
        }
        
        if textField === branchTextField || textField === accountTextField {
            let character = string.replacingOccurrences(of: "[^0-9]", with: "", options: String.CompareOptions.regularExpression)
            textField.text?.append(character)
            return false
        }
        
        if textField === branchDigitTextField {
            return string.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) == nil
        }
        
        if textField === documentTextField {
            let text = (textField.text ?? "") as NSString
            let v: String = text.replacingCharacters(in: range, with: string)
            textField.text = self.maskString(input: v, mask: "[000].[000].[000]-[00]")
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        return true
    }
}

// MARK: - Private Methods

private extension RegistrationBankStepViewController {
    func configureInputs() {
        setupBankTextField()
        setupControls()
        setupDelegate()
        configureInputRules()
        setupLegacyDocumentTextField()
        setupOperationTextField()
    }
    
    func setupRequirementsLabel() {
        let text = NSMutableAttributedString()
        
        let defaultStyle: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font: Typography.caption().font(),
            NSAttributedString.Key.foregroundColor: Colors.black.color
        ]
        
        let underlineAttributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.foregroundColor: Colors.branding300.color,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        text.append(NSAttributedString(string: Localizable.Requirements.default, attributes: defaultStyle))
        switch model?.companyType {
        case .individual:
            text.append(NSAttributedString(string: Localizable.Requirements.personal, attributes: underlineAttributes))
        case .multiPartners, nil:
            text.append(NSAttributedString(string: Localizable.Requirements.company, attributes: underlineAttributes))
        }
        
        requirementsLabel.attributedText = text
        
        requirementsLabel.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(openAccountRequirementsModal))
        requirementsLabel.addGestureRecognizer(tapGesture)
    }
    
    func setupBankTextField() {
        guard let bank = model?.account.bank else { return }
        
        bankTextField.isEnabled = false
        bankTextField.text = "\(bank.code) - \(bank.name)"

        if let imageUrl = bank.image {
            bankImageView.sd_setImage(with: URL(string: imageUrl))
        } else {
            bankImageView.image = Assets.icoBankPlaceholder.image
        }
    }
    
    func setupControls() {
        controls.append(bankTextField)
        controls.append(branchTextField)
        controls.append(branchDigitTextField)
        controls.append(accountTextField)
        controls.append(accountDigitTextField)
        controls.append(typeTextField)
    }
    
    func setupDelegate() {
        branchTextField.delegate = self
        branchDigitTextField.delegate = self
        accountTextField.delegate = self
        accountDigitTextField.delegate = self
        typeTextField.delegate = self
        operationTextField.delegate = self
        documentTextField.delegate = self
    }
    
    func configureInputRules() {
        var branchRules = ValidationRuleSet<String>()
        branchRules.add(rule: ValidationRuleLength(min: 3, error: ValidateError.branchInvalid))
        branchTextField.validationRules = branchRules
       
        var accountRules = ValidationRuleSet<String>()
        accountRules.add(rule: ValidationRuleLength(min: 3, error: ValidateError.accountInvalid))
        accountTextField.validationRules = accountRules

        var accountDigitRules = ValidationRuleSet<String>()
        accountDigitRules.add(rule: ValidationRuleLength(min: 1, error: ValidateError.accountDigitInvalid))
        accountDigitTextField.validationRules = accountDigitRules

        var accountTypeRules = ValidationRuleSet<String>()
        accountTypeRules.add(rule: ValidationRuleLength(min: 2, error: ValidateError.accountTypeInvalid))
        typeTextField.validationRules = accountTypeRules
        
        validateOnInputChange = false
    }
    
    // Remove this method and the OperationTextField when releaseBankAccountWarning is enabled
    func setupOperationTextField() {
        guard let model = model else { return }
        
        if model.account.bank?.form == BankName.caixa.rawValue, !dependencies.featureManager.isActive(.releaseBankAccountWarning) {
            operationTextField.isHidden = false
            var operationTypeRules = ValidationRuleSet<String>()
            operationTypeRules.add(rule: ValidationRuleLength(min: 1, error: ValidateError.operationInvalid))
            operationTextField.validationRules = operationTypeRules
            controls.append(operationTextField)
        } else {
            operationTextField.text = ""
            operationTextField.isHidden = true
        }
    }
    
    // Remove this when releaseBankAccountWarning is enabled
    func setupLegacyDocumentTextField() {
        guard let model = model, !dependencies.featureManager.isActive(.releaseBankAccountWarning) else { return }
        
        let type = model.bankAccountType
        setupDocumentTextField(registrationType: type)
    }
    
    func goToNextStep() {
        guard let model = model else { return }
        
        TrackingManager.trackEventPublicAuth(
            .obSettedBankAccount,
            authEventName: .obMultBankAccountForm,
            properties: [TrackingManager.TrackingManagerProperties.bankId.rawValue: (model.account.bank?.code) ?? ""]
        )
        
        if dependencies.featureManager.isActive(.releaseBankAccountWarning) {
            let controller = BankAccountConfirmationFactory.make(.registration(model))
            navigationController?.pushViewController(controller, animated: true)
            return
        }
        
        if dependencies.featureManager.isActive(.registrationTaxZero) {
            let controller = RegistrationTaxZeroFactory.make(model: model)
            navigationController?.pushViewController(controller, animated: true)
            return
        }
        
        let controller: RegistrationDaysStepViewController = ViewsManager.instantiateViewController(.registration)
        controller.setup(model: model)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func goToNextField(from textField: UITextField) {
        guard let model = model else { return }
        
        if textField === branchTextField {
            branchDigitTextField.becomeFirstResponder()
            return
        }
        
        if textField === branchDigitTextField {
            accountTextField.becomeFirstResponder()
            return
        }
        
        if textField === accountDigitTextField {
            typeTextField.becomeFirstResponder()
            return
        }
        
        if model.account.bank?.form == BankName.caixa.rawValue, !dependencies.featureManager.isActive(.releaseBankAccountWarning) {
            operationTextField.becomeFirstResponder()
            return
        }
        
        finishStep(nil)
    }
    
    func showAccountTypes() {
        guard let model = model else { return }
        
        guard dependencies.featureManager.isActive(.releaseBankAccountWarning) else {
            let types: [LegacyBankAccountType] = [.checking, .savings]
            showLegacyAccountTypesAlert(with: types)
            return
        }
        
        // Show from back end
        showAccountTypesAlert(with: model.bankAccountTypes)
    }
    
    func showLegacyAccountTypesAlert(with types: [LegacyBankAccountType]) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for type in types {
            let action = UIAlertAction(title: type.title, style: .default) { [weak self] _ in
                self?.didSelectLegacyAccountType(type)
            }
            actionSheet.addAction(action)
        }
        
        actionSheet.addAction(title: "Cancelar", style: .cancel, isEnabled: true)
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    func showAccountTypesAlert(with types: [BankAccountType]) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for type in types {
            let action = UIAlertAction(title: type.label, style: .default) { [weak self] _ in
                self?.didSelectAccountType(type)
            }
            actionSheet.addAction(action)
        }
        
        actionSheet.addAction(title: Strings.Default.cancel, style: .cancel, isEnabled: true)
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    func didSelectLegacyAccountType(_ type: LegacyBankAccountType) {
        model?.account.legacyAccountType = type
        typeTextField.text = type.title
    }
    
    func didSelectAccountType(_ type: BankAccountType) {
        model?.account.accountType = type.type
        model?.account.accountValue = type.value
        
        typeTextField.text = type.label
        
        switch type.type {
        case .company:
            model?.bankAccountType = .company
            setupDocumentTextField(registrationType: .company)
        case .individual:
            model?.bankAccountType = .personal
            setupDocumentTextField(registrationType: .personal)
        }
        
        model?.bankAccountValue = type.value
    }
    
    func setupDocumentTextField(registrationType: RegistrationType?) {
        guard let model = model, let registrationType = registrationType else {
            documentTextField.text = ""
            documentTextField.isHidden = true
            documentTextField.hasError = false
            documentTextField.textColor = UI.Colors.grayscale600.color
            return
        }
        
        documentTextField.isHidden = false
        
        switch registrationType {
        case .company:
            documentTextField.text = model.account.cnpj
            documentTextField.isEnabled = false
            documentTextField.textColor = UI.Colors.grayscale400.color
        case .personal:
            documentTextField.text = ""
            documentTextField.placeholder = Localizable.personDocumentPlaceholder
            documentTextField.textColor = UI.Colors.black.color
            documentTextField.isEnabled = true
        }
    }
    
    func saveBankAccount() {
        saveFormData()
        startLoading()
        
        model?.finishBankStep { [weak self] error in
            self?.stopLoading()
            if error != nil {
                self?.presentPopUp()
                return
            }
            self?.goToNextStep()
        }
    }
    
    func saveFormData() {
        if let branch = branchTextField.text {
            model?.account.branch = branch
        }
        
        if let branchDigit = branchDigitTextField.text {
            model?.account.branchDigit = branchDigit
        }
        
        if let account = accountTextField.text {
            model?.account.account = account
        }
        
        if let accountDigit = accountDigitTextField.text {
            model?.account.accountDigit = accountDigit
        }
        
        if let operation = operationTextField.text {
            model?.account.accountOperation = operation
        }
        
        if let document = documentTextField.text {
            model?.account.documentBank = document
        }
    }
    
    func presentPopUp() {
        let action = PopupAction(title: LocalizableWD.buttonTitle, style: .fill)
        let infoPopUp = UI.PopupViewController(
            title: LocalizableWD.title,
            description: LocalizableWD.message,
            image: Assets.errorIcon.image
        )

        infoPopUp.hideCloseButton = true
        infoPopUp.addAction(action)
        showPopup(infoPopUp)
    }
    
    func showModal(info: AlertModalInfo) {
        let alertPopup = AlertModalFactory.make(alertModalInfo: info)
        
        alertPopup.touchButtonAction = { alert in
            alert.dismiss(animated: true)
        }
        
        alertPopup.touchSecondaryButtonAction = { [weak self] alert in
            alert.dismiss(animated: true) {
                guard let self = self else { return }
                BIZCustomerSupportManager().presentFAQ(from: self)
            }
        }
        
        let popup = PopupViewController()
        popup.contentController = alertPopup
        popup.hasCloseButton = false
        present(popup, animated: true, completion: nil)
    }
}

// MARK: - OBJC Actions

@objc
private extension RegistrationBankStepViewController {
    func openAccountRequirementsModal() {
        guard let model = model else { return }
        let subtitle: String
        switch model.companyType {
        case .individual:
            subtitle = BankTipsLocalizable.HelpModal.Individual.subtitle
        case .multiPartners:
            subtitle = BankTipsLocalizable.HelpModal.Multi.subtitle
        }
        
        let imageInfo = AlertModalInfoImage(image: Resources.Icons.icoBlueInfo.image)
        let info = AlertModalInfo(
            imageInfo: imageInfo,
            title: BankTipsLocalizable.HelpModal.title,
            subtitle: subtitle,
            buttonTitle: BankTipsLocalizable.HelpModal.buttonTitle,
            secondaryButtonTitle: BankTipsLocalizable.HelpModal.secondaryButtonTitle
        )
        showModal(info: info)
    }
}

// MARK: - BankSelectFormViewControllerDelegate

extension RegistrationBankStepViewController: BankSelectFormViewControllerDelegate {
    func didSelectBank(_ bank: Bank) {
        typeTextField.text = ""
        model?.account.bank = bank
        setupBankTextField()
        setupOperationTextField()
        
        if dependencies.featureManager.isActive(.releaseBankAccountWarning) {
            setupDocumentTextField(registrationType: nil)
        }
    }
    
    func shouldHidePJAccountDisclaimer() -> Bool {
        true
    }
}
