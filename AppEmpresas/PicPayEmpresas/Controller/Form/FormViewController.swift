import InputMask
import UIKit
import Validator

class FormViewController: BaseViewController {
    enum LoadingType {
        case button
        case overlay
    }

    // MARK: - Public Properties
    lazy var controls: [UIControl] = {
        [UIControl]()
    }()
    
    private var nextButtonTitle: String = ""
    var loadingView: LoadingHeaderView?
    
    /// Enable the validation when the user touch on contorller
    var validateOnInputChange: Bool = false {
        didSet {
            // Validate all elements
            for obj in self.controls {
                if let validatable = obj as? UITextField {
                    if validatable.validationRules != nil {
                        validatable.validateOnInputChange(enabled: validateOnInputChange)
                    }
                }
            }
        }
    }
    
    // MARK: - IB Outlets
    @IBOutlet private var activityIndicator: UIActivityIndicatorView?
    @IBOutlet var saveButton: UIPPButton?
    @IBOutlet private var messageLabel: UILabel?
    @IBOutlet private var titleLabel: UILabel?
    @IBOutlet private var textLabel: UILabel?
    @IBOutlet private var contentView: UIView?
    @IBOutlet private var bottomConstraint: NSLayoutConstraint?
    @IBOutlet private var scrollView: UIScrollView?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        self.activityIndicator?.isHidden = true
        self.messageLabel?.text = ""
        
        if let title = saveButton?.titleLabel?.text {
            self.nextButtonTitle = title
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        tapGesture.delaysTouchesBegan = false
        tapGesture.delaysTouchesEnded = false
        self.view.addGestureRecognizer(tapGesture)
        
        // Add observer to keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillOpen), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        super.viewDidLoad()
    }
    
    // MARK: - Internal Methods
    @discardableResult
    func validate(_ showMessage: Bool = false) -> ValidationResult {
        var results: [ValidationResult] = [ValidationResult]()
        
        // Validate all elements
        for obj in self.controls {
            if let validatable = obj as? UIRoundedTextField {
                if validatable.validationRules != nil {
                    let result = validatable.validate()
                    results.append(result)
                    validatable.hasError = !result.isValid
                }
            }
        }
        
        // Merge all results
        let result = ValidationResult.merge(results: results)
        self.showValidationResult(result: result)
        
        return result
    }
    
    // show the validation results
    func showValidationResult(result: ValidationResult, showMessage: Bool = true) {
        switch result {
        case .invalid(let errors):
            if showMessage {
                var message = ""
                for error in errors {
                    message += error.message + "\n"
                }
                self.showMessage(text: message)
            }
        default:
            self.showMessage(text: "")
        }
    }
    
    func startLoading(_ type: LoadingType = LoadingType.button) {
        switch type {
        case .button:
            if let title = saveButton?.titleLabel?.text {
                nextButtonTitle = title
            }
            activityIndicator?.isHidden = false
            activityIndicator?.startAnimating()
            saveButton?.setTitle("", for: .disabled)
            saveButton?.isEnabled = false
            
            disableAllControls()
            
            UIView.animate(withDuration: 0.50, animations: {
                for control in self.controls {
                    control.layer.opacity = 0.35
                }
            })
        case .overlay:
            loadingView?.removeFromSuperview()
            var frame = view.frame
            frame.origin.y = 0
            frame.origin.x = 0
            let loadingView = LoadingHeaderView(frame: frame)
            self.loadingView = loadingView
            loadingView.backgroundColor = UIColor.white
            view.addSubview(loadingView)
        }
    }
    
    func stopLoading(_ type: LoadingType = LoadingType.button) {
        if type == .button {
            saveButton?.isEnabled = true
            saveButton?.setTitle(nextButtonTitle, for: .disabled)
            self.activityIndicator?.stopAnimating()
            self.activityIndicator?.isHidden = true
            
            for control in controls {
                control.isEnabled = true
            }
            UIView.animate(withDuration: 0.50, animations: {
                for control in self.controls {
                    control.layer.opacity = 1.0
                }
            })
        } else if type == .overlay || self.loadingView != nil {
            UIView.animate(
                withDuration: 0.5,
                animations: {
                    self.loadingView?.layer.opacity = 0
                },
                completion: { _ in
                    self.loadingView?.removeFromSuperview()
                }
            )
        }
    }
    
    func disableAllControls() {
        for control in controls {
            control.isEnabled = false
        }
    }
    
    @objc
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    /// show the messagem on screen
    func showMessage(text: String) {
        messageLabel?.isHidden = false
        messageLabel?.text = text + ( text.isEmpty ? "" : "\n")
    }
    
    /// Apply the mask on the string
    func maskString(input: String, mask: String) -> String {
        do {
            let mask = try Mask(format: mask)
            let result: Mask.Result = mask.apply(toText: CaretString(string: input), autocomplete: true)
            return result.formattedText.string
        } catch {
            return input
        }
    }
    
    // MARK: - Keyboard Events Handler
    @objc
    func keyboardWillOpen(keyboardNotification: NSNotification) {
        if let animationDuration = ((keyboardNotification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey]) as? NSNumber)?.doubleValue {
            if let keyboardSize = keyboardNotification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect {
                // animate the view to adjust height with the opened keyboard
                self.view.layoutIfNeeded()
                // workaround tabbar - 60pt
                self.bottomConstraint?.constant = keyboardSize.size.height - 60.0

                UIView.animate(withDuration: animationDuration) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    @objc
    func keyboardWillHide(keyboardNotification: NSNotification) {
        if let animationDuration = ((keyboardNotification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey]) as? NSNumber)?.doubleValue {
            // animate the view to adjust height with the opened keyboard
            self.view.layoutIfNeeded()
            self.bottomConstraint?.constant = 0
            
            UIView.animate(withDuration: animationDuration) {
                self.view.layoutIfNeeded()
            }
        }
        
        self.scrollView?.setContentOffset(.zero, animated: true)
    }
}
