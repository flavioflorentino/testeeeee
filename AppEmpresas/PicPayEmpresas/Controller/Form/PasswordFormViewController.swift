import AnalyticsModule
import LegacyPJ
import SDWebImage
import UI
import UIKit
import Validator

final class PasswordFormViewController: FormViewController {
    enum ValidateError: String, ValidationError {
        case currentPassordInvalid
        case passwordInvalid

        var message: String {
            FormLocalizable(rawValue: self.rawValue)?.text ?? ""
        }
    }
    
    // MARK: - Properties
    
    var model: PasswordFormViewModel?
    private let dependencies: HasAnalytics = DependencyContainer()
    
    // MARK: - IB Outlet
    
    @IBOutlet private var currentPasswordTextField: UIRoundedTextField!
    @IBOutlet private var passwordTextField: UIRoundedTextField!
    @IBOutlet private var showPasswordImage: UIImageView!
    @IBOutlet private var forgotPasswordButton: UIButton! {
        didSet {
            let title = FormLocalizable.forgotPassword.text
            forgotPasswordButton.setTitle(title, for: .normal)
            forgotPasswordButton.buttonStyle(LinkButtonStyle())
        }
    }

    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureInputs()
        dependencies.analytics.log(SettingsAnalytics.password)
    }
    
    // MARK: - User Actions
    
    @IBAction func save(_ sender: UIButton?) {
        if self.validate().isValid {
            startLoading()
            saveFormData()
            self.model?.savePassword({ [weak self] error in
                if let error = error {
                    UIAlertController(error: error).show()
                } else {
                    _ = self?.navigationController?.popViewController(animated: true)
                }
                self?.stopLoading()
            })
        }
    }

    @IBAction func didTapForgotPassword(_ sender: Any) {
        dependencies.analytics.log(SettingsAnalytics.forgotPassword)
        let title = Strings.Default.forgotPasswordTitle
        let controller = ViewsManager.webViewController(url: urlForgetPassword, title: title)
        controller.addScript(file: "Script")
        navigationController?.pushViewController(controller, animated: true)
    }

    // MARK: - Dependence Injection
    override func configureDependencies() {
        self.setup(model: UpdatePasswordFormViewModel(user: User()))
        super.configureDependencies()
    }
    
    func setup(model: PasswordFormViewModel) {
        self.model = model
        isReady = true
    }
    
    // MARK: - Internal Methods
    
    /// Configure the step inputs
    private func configureInputs() {
        // add text fields to the controls list
        self.controls.append(passwordTextField)
        self.controls.append(currentPasswordTextField)
        
        // set delegate
        self.passwordTextField.delegate = self
        self.currentPasswordTextField.delegate = self
        
        // default password visibility
        passwordTextField.isSecureTextEntry = true
        toggleShowPassword()
        
        // Configure validations
        var passwordRules = ValidationRuleSet<String>()
        passwordRules.add(rule: ValidationRuleLength(min: 6, error: ValidateError.passwordInvalid))
        passwordTextField.validationRules = passwordRules
        
        var currentRules = ValidationRuleSet<String>()
        currentRules.add(rule: ValidationRuleLength(min: 3, error: ValidateError.currentPassordInvalid))
        currentPasswordTextField.validationRules = currentRules
        
        self.validateOnInputChange = false
        
        // Add a gesture to enable the user to view the password
        self.showPasswordImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(toggleShowPassword)))
        self.showPasswordImage.isUserInteractionEnabled = true
    }
    
    /// save form data
    private func saveFormData() {
        if let current = currentPasswordTextField.text {
            self.model?.currentPassword = current
        }
        if let password = passwordTextField.text {
            self.model?.password = password
        }
    }
    
    /// Move the cursor to next field
    /// If the field is the last field try to finish the step
    func goToNextField(from: UITextField) {
        if from == self.currentPasswordTextField {
            self.passwordTextField.becomeFirstResponder()
        } else {
            self.save(nil)
        }
    }
    
    /// Toggle the password visibility
    @objc
    func toggleShowPassword() {
        if passwordTextField.isSecureTextEntry {
            passwordTextField.isSecureTextEntry = false
            showPasswordImage.image = Assets.iconPasswordEyeBold.image
        } else {
            passwordTextField.isSecureTextEntry = true
            showPasswordImage.image = Assets.iconPasswordEye.image
        }
    }
}

extension PasswordFormViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.goToNextField(from: textField)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        true
    }
}
