import AnalyticsModule
import LegacyPJ
import UI
import UIKit
import FeatureFlag

final class DayFormViewController: FormViewController {
    // MARK: - Public Properties
    
    var model: DayFormViewModel?
    
    private let dependencies: HasAnalytics = DependencyContainer()

    // MARK: - IB Outlet
    
    @IBOutlet private var daysToolTipView: UIView!
    @IBOutlet private var daysToolTipLabel: UILabel!
    @IBOutlet private var infoTaxZeroView: UIView!
    @IBOutlet private var daysSlider: UISlider!
    @IBOutlet private var daysToolTipTextLabel: UILabel!
    
    @IBOutlet private var feeLabel: UILabel!
    @IBOutlet private var feeStartLabel: UILabel!
    @IBOutlet private var feeEndLabel: UILabel!
    
    @IBOutlet private var daysStartLabel: UILabel!
    @IBOutlet private var daysEndLabel: UILabel!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureFee()
        
        daysSlider.addTarget(self, action: #selector(handlerSliderChange), for: UIControl.Event.valueChanged)
        
        guard let model = model else {
            return
        }
        
        self.startLoading(.overlay)
        model.loadFeeList({ [weak self] error in
            guard let model = self?.model else {
                return
            }
            
            if error == nil {
                if model.dayHasInitialLoad == true {
                    self?.configureFee()
                    self?.loadData()
                    self?.configureZeroFee()
                } else {
                    self?.configureFee()
                    if let day = model.feeDefaultDay {
                        self?.daysSlider.value = Float(day)
                    }
                    self?.stopLoading(.overlay)
                }
            }
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.setNeedsLayout()
        
        updateDayToolTip()
        updateFee()
        dependencies.analytics.log(SettingsAnalytics.fee)
    }
    
    // MARK: - User Events
    
    /// Save data and finish the step
    @IBAction func finishStep(_ sender: UIButton?) {
        self.saveFormData()
        
        AuthManager.shared.performActionWithAuthorization { [weak self] result in
            switch result {
            case .success(let password):
                self?.startLoading()
                self?.model?.saveFeeDay(password: password) { [weak self] error in
                    self?.stopLoading()
                    if let error = error {
                        self?.showMessage(text: error.localizedDescription)
                        return
                    }
                    
                    NotificationCenter.default.post(name: Notification.Name.Settings.updated, object: nil)
                    _ = self?.navigationController?.popViewController(animated: true)
                    if let day = self?.model?.feeSelectedDay, let fee = self?.model?.rawFeeByDay(day: "\(day)") {
                        self?.dependencies.analytics.log(SettingsAnalytics.feeUpdated("\(day)", fee: fee))
                    }
                }
            case .failure(let error):
                UIAlertController(error: error).show()
            default:
                break
            }
        }
    }
    
    @IBAction func termsInteraction(_sender: UIButton?) {
        let controller = ViewsManager.webViewController(url: FeatureManager.shared.text(.urlTerms))
        navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - Dependence Injection
    override func configureDependencies() {
        self.setup(model: UpdateDayFormViewModel())
        super.configureDependencies()
    }
    
    func setup(model: DayFormViewModel) {
        self.model = model
        isReady = true
    }
    
    // MARK: - Events Handler
    
    @objc
    func handlerSliderChange() {
        self.updateDayToolTip()
        self.updateFee()
    }
}

private extension DayFormViewController {
    func configureZeroFee() {
        guard let model = model, model.zeroFee else {
            return
        }
        
        infoTaxZeroView.isHidden = false
        daysSlider.isEnabled = false
        saveButton?.isEnabled = false
        feeLabel.text = FormLocalizable.taxZeroFeeLabel.text
        feeStartLabel.text = FormLocalizable.taxZeroStartPercent.text
    }
    
    /// Load the selected day
    func loadData() {
        self.startLoading(.overlay)
        self.model?.loadFeeDay({ [weak self] error in
            if error == nil {
                if let day = self?.model?.feeDefaultDay {
                    self?.daysSlider.value = Float(day)
                }
                
                if let day = self?.model?.feeSelectedDay {
                    self?.daysSlider.value = Float(day)
                }
                
                self?.stopLoading(.overlay)
                self?.daysSlider.setNeedsLayout()
                
                self?.updateDayToolTip()
                self?.updateFee()
            }
        })
    }
    
    /// configure the view with fee data
    func configureFee() {
        guard let model = model else {
            return
        }
        
        if let firstDay = Float(model.feeFirstDay) {
            daysSlider.minimumValue = firstDay
            daysStartLabel.text = "\(model.feeFirstDay) \(FormDictLocalizable.numberOfDays.plural(Int(firstDay)))"
            
            let fee = model.feeByDay(day: model.feeFirstDay)
            feeStartLabel.text = fee
        }
        if let lastDay = Float(model.feeLastDay) {
            daysSlider.maximumValue = lastDay
            daysEndLabel.text = "\(model.feeLastDay) \(FormDictLocalizable.numberOfDays.plural(Int(lastDay)))"
            
            let fee = model.feeByDay(day: model.feeLastDay)
            feeEndLabel.text = fee
        }
        
        if let day = model.feeDefaultDay {
            let selectedDay = Float(day)
            if selectedDay >= daysSlider.minimumValue && selectedDay <= daysSlider.maximumValue {
                daysSlider.value = selectedDay
            }
        }
    }
    
    /// Update the days on tooltip and position the view at the slider's thumb position
    func updateDayToolTip() {
        let days = Int(daysSlider.value)
        daysToolTipLabel.text = "\(days)"

        daysToolTipTextLabel.text = FormDictLocalizable.numberOfDays.plural(days)
        
        var frame = daysToolTipLabel.frame
        frame.origin.x = daysSlider.thumbCenterX() + 10
        frame.origin.y = self.daysSlider.frame.origin.y - 47
        daysToolTipView.frame = frame
    }
    
    /// Update the fee value according to the selected number of days
    func updateFee() {
        let day = Int(daysSlider.value)
        
        if let fee = self.model?.feeByDay(day: "\(day)") {
            feeLabel.text = fee
        } else {
            feeLabel.text = "Taxa por transação 0.00%"
        }
    }
    
    func saveFormData() {
        guard var model = model else {
            return
        }
        
        model.feeSelectedDay = Int(daysSlider.value)
        model.feeDefaultDay = Int(daysSlider.value)
    }
}
