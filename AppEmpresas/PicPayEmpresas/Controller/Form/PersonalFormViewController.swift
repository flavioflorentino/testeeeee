import AccountManagementPJ
import AnalyticsModule
import LegacyPJ
import UIKit
import Validator

final class PersonalFormViewController: FormViewController {
    enum ValidateError: String, ValidationError {
        case personalNameInvalid
        case personalCpfInvalid
        case personalBirthdayInvalid

        var message: String {
            FormLocalizable(rawValue: rawValue)?.text ?? ""
        }
    }
    
    // MARK: - Private Properties
    
    private var model: PersonalFormViewModel = UpdatePersonalFormViewModel()
    private let dependencies: HasAnalytics = DependencyContainer()
    
    // MARK: - IB Outlets
    
    @IBOutlet private var nameTextField: UIRoundedTextField!
    @IBOutlet private var cpfTextField: UIRoundedTextField!
    @IBOutlet private var birthdayTextField: UIRoundedTextField!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureInputs()
        loadPersonalData()
        dependencies.analytics.log(SettingsAnalytics.personalData)
    }
    
    // MARK: - User Actions
    
    @IBAction func save(_ sender: UIButton?) {
        validateOnInputChange = true
        
        guard validate().isValid else {
            return
        }
        
        startLoading()
        updateFormData()
        requestAuthorization()
    }
    
    // MARK: - Dependence Injection
    
    override func configureDependencies() {
        isReady = true
        super.configureDependencies()
    }
}

// MARK: - Private Methods

private extension PersonalFormViewController {
    func configureInputs() {
        controls.append(nameTextField)
        controls.append(cpfTextField)
        controls.append(birthdayTextField)
        
        nameTextField.delegate = self
        cpfTextField.delegate = self
        birthdayTextField.delegate = self
        
        configureValidations()
    }
    
    func configureValidations() {
        var nameRules = ValidationRuleSet<String>()
        nameRules.add(rule: ValidationRuleLength(min: 5, error: ValidateError.personalNameInvalid))
        nameTextField.validationRules = nameRules
        
        var cpfRules = ValidationRuleSet<String>()
        cpfRules.add(rule: ValidationRuleLength(min: 14, error: ValidateError.personalCpfInvalid))
        cpfTextField.validationRules = cpfRules
        
        var birthdayRules = ValidationRuleSet<String>()
        birthdayRules.add(rule: ValidationRuleLength(min: 10, error: ValidateError.personalBirthdayInvalid))
        birthdayTextField.validationRules = birthdayRules
        
        validateOnInputChange = false
    }
    
    func loadPersonalData() {
        guard model.personalHasInitialLoad else {
            populateForm()
            return
        }
        
        startLoading(.overlay)
        
        model.loadPersonalData({ [weak self] error in
            self?.stopLoading(.overlay)
            
            if let error = error {
                self?.showMessage(text: error.localizedDescription)
                return
            }
            
            self?.populateForm()
        })
    }
    
    func populateForm() {
        if let name = model.personalData.name {
            nameTextField.text = name
        }
        
        if let cpf = model.personalData.cpf {
            cpfTextField.text = maskString(input: cpf, mask: "[000].[000].[000]-[00]")
        }
        
        if let birthday = model.personalData.birthDate {
            birthdayTextField.text = birthday
        }
    }
    
    /// save form data
    func updateFormData() {
        if let name = nameTextField.text {
            model.personalData.name = name
        }
        
        if let cpf = cpfTextField.text {
            model.personalData.cpf = cpf
        }
        
        if let birthday = birthdayTextField.text {
            model.personalData.birthDate = birthday
        }
    }
    
    func requestAuthorization() {
        AuthManager.shared.performActionWithAuthorization { [weak self] result in
            switch result {
            case .success(let password):
                self?.savePersonalData(password: password)
                
            case .failure(let error):
                self?.stopLoading()
                UIAlertController(error: error).show()
                
            default:
                self?.stopLoading()
            }
        }
    }
    
    func savePersonalData(password: String) {
        model.savePersonalData(password: password) { [weak self] error in
            self?.stopLoading()
            if let error = error {
                UIAlertController(error: error).show()
                return
            }
            
            let name = Notification.Name.User.updated
            NotificationCenter.default.post(name: name, object: nil)
            AccountUserDataNotification.updated(.admin)
            self?.navigationController?.popViewController(animated: true)
        }
    }
    
    /// Move the cursor to next field
    /// If the field is the last field try to finish the step
    func goToNextField(from: UITextField) {
        if from == nameTextField {
            cpfTextField.becomeFirstResponder()
            return
        }
        
        if from == cpfTextField {
            birthdayTextField.becomeFirstResponder()
            return
        }
        
        save(nil)
    }
}

// MARK: - UITextFieldDelegate

extension PersonalFormViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        goToNextField(from: textField)
        return true
    }
    
    func textField(
        _ textField: UITextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String
    ) -> Bool {
        guard let text = textField.text else { return true }
        if string.isEmpty { return true }
        let v: String = (text as NSString).replacingCharacters(in: range, with: string)
        
        if textField == cpfTextField {
            textField.text = maskString(input: v, mask: "[000].[000].[000]-[00]")
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        if textField == birthdayTextField {
            textField.text = maskString(input: v, mask: "[00]/[00]/[0000]")
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        return true
    }
}
