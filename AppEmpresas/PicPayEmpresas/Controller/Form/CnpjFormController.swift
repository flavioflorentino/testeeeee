import AnalyticsModule
import CustomerSupport
import UIKit
import Validator

final class CnpjFormViewController: FormViewController {
    enum ValidateError: String, ValidationError {
        case cpnjInvalid
        case cnpjCompanyNameInvalid

        var message: String {
            FormLocalizable(rawValue: self.rawValue)?.text ?? ""
        }
    }
    
    // MARK: - Properties
    
    var model: CnpjFormViewModel?
    
    private let dependencies: HasAnalytics = DependencyContainer()
    
    // MARK: - IB Outlets
    
    @IBOutlet private var cnpjTextField: UIRoundedTextField!
    @IBOutlet private var companyNameTextField: UIRoundedTextField!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureInputs()
        
        guard let model = model else {
            return
        }
        
        // Load form data
        if model.cnpjHasInitialLoad {
            startLoading()
            
            self.model?.loadCnpj({ [weak self] error in
                self?.stopLoading()
                if error == nil {
                    self?.populateForm()
                    self?.disableAllControls()
                } else {
                    if let text = error?.localizedDescription {
                        self?.showMessage(text: text)
                    }
                }
            })
        }
        
        dependencies.analytics.log(SettingsAnalytics.businessData)
    }
    
    // MARK: - User Actions
    
    @IBAction func save(_ sender: UIButton?) {
        let bizManager = BIZCustomerSupportManager(accessToken: model?.accessToken)
        bizManager.presentTicketList(from: self, with: model?.seller)
        
        guard let sellerId = model?.seller.id else {
            return
        }
        
        bizManager.registerNotification(sellerID: "\(sellerId)")
    }
    
    // MARK: - Dependence Injection
    
    func setup(model: CnpjFormViewModel) {
        self.model = model
        isReady = true
    }
    
    // MARK: - Internal Methods
    
    /// Configure the step inputs
    private func configureInputs() {
        // add text fields to the controls list
        self.controls.append(cnpjTextField)
        self.controls.append(companyNameTextField)
        
        // set delegate
        self.cnpjTextField.delegate = self
        self.companyNameTextField.delegate = self
        
        // Configure validations
        var cnpjRules = ValidationRuleSet<String>()
        cnpjRules.add(rule: ValidationRuleLength(min: 18, error: ValidateError.cpnjInvalid))
        cnpjTextField.validationRules = cnpjRules
        
        var companyNameRules = ValidationRuleSet<String>()
        companyNameRules.add(rule: ValidationRuleLength(min: 5, error: ValidateError.cnpjCompanyNameInvalid))
        companyNameTextField.validationRules = companyNameRules
        
        self.validateOnInputChange = false
        
        cnpjTextField.isEnabled = false
        companyNameTextField.isEnabled = false
    }
    
    /// Populate the for with the
    private func populateForm() {
        if let cnpj = self.model?.seller.cnpj {
            cnpjTextField.text = maskString(input: cnpj, mask: "[00].[000].[000]/[0000]-[00]")
        }
        if let companyName = self.model?.seller.razaoSocial {
            companyNameTextField.text = companyName
        }
    }
    
    /// Move the cursor to next field
    /// If the field is the last field try to finish the step
    func goToNextField(from: UITextField) {
        if cnpjTextField == from {
            companyNameTextField.becomeFirstResponder()
        } else {
            self.save(nil)
        }
    }
}

extension CnpjFormViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.goToNextField(from: textField)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        false
    }
}
