import AccountManagementPJ
import AnalyticsModule
import FeatureFlag
import LegacyPJ
import MapKit
import UIKit

final class MapFormViewController: FormViewController {
    // MARK: - Public Properties
    
    var model: AddressFormViewModel?
    typealias Dependecies = HasFeatureManager & HasAnalytics 
    
    private let dependencies: Dependecies = DependencyContainer()
    
    // MARK: - IB Outlets
    
    @IBOutlet private var mapView: MKMapView!
    @IBOutlet private var addressLabel: UILabel!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.populateAddress()
        self.centerMapAtAddress()
    }
    
    // MARK: - User Actions
    
    /// Save data and finish the step
    @IBAction func finishStep(_ sender: UIButton?) {
        guard var model = model else {
            return
        }
        model.address.lat = self.mapView.centerCoordinate.latitude
        model.address.lon = self.mapView.centerCoordinate.longitude
        
        AuthManager.shared.performActionWithAuthorization { [weak self] result in
            switch result {
            case .success(let password):
                self?.startLoading()
                self?.model?.saveAddress(password: password) { [weak self] error in
                    self?.stopLoading()
                    if let error = error {
                        self?.showMessage(text: error.localizedDescription)
                        return
                    }
                    self?.dependencies.analytics.log(SettingsAnalytics.updatedAddress)
                    NotificationCenter.default.post(name: Notification.Name.Settings.updated, object: nil)
                    if self?.dependencies.featureManager.isActive(.isAccountInfoRenewalAvailable) ?? false {
                        self?.popToUserDataViewController()
                        return
                    }
                    _ = self?.navigationController?.popToRootViewController(animated: true)
                }
            case .failure(let error):
                UIAlertController(error: error).show()
                
            default:
                break
            }
        }
    }

    private func popToUserDataViewController() {
        AccountUserDataNotification.updated(.address)
        FlowCoordinator.popToUserData(navigationController)
    }
    
    @IBAction func back(_ sender: UIButton?) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Dependence Injection
    
    override func configureDependencies() {
        self.setup(model: UpdateAddressFormViewModel())
        super.configureDependencies()
    }
    
    func setup(model: AddressFormViewModel) {
        self.model = model
        isReady = true
    }
    
    // MARK: - Internal Methods
    
    /// Center the map at adress
    /// Try to find the location from string address
    func centerMapAtAddress() {
        guard let lat = self.model?.address.lat,
            let lon = self.model?.address.lon else { return }
        
        let coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lon))
        
        let zoomLevel = 16
        let span = MKCoordinateSpan(latitudeDelta: 0, longitudeDelta: 360 / pow(2, Double(zoomLevel)) * Double(self.view.frame.size.width) / 256)
        self.mapView.setRegion(MKCoordinateRegion(center: coordinate, span: span), animated: true)
    }
    
    /// Populate the screen address with the data typed in the back screen
    func populateAddress() {
        guard let model = model else {
            return
        }
        
        var address = ""
        
        if let street = model.address.address {
            address += "\(street)"
        }
        if let number = model.address.number {
            address += ", - \(number) \n"
        } else {
            address += "\n"
        }
        
        if let complement = model.address.complement, !complement.isEmpty {
            address += "\(complement)\n"
        }
        
        if let district = model.address.district {
            address += "\(district)\n"
        }
        
        if let city = model.address.city {
            if let state = model.address.state {
                address += "\(city) - \(state)\n"
            }
        }
        
        if let cep = model.address.cep {
            address += "\(cep)"
        }
        
        self.addressLabel.text = address
    }
}
