import AnalyticsModule
import LegacyPJ
import UIKit
import Validator

final class PromoCodeFormViewController: FormViewController {
    enum ValidateError: String, ValidationError {
        case promoCodeInvalid

        var message: String {
            FormLocalizable(rawValue: self.rawValue)?.text ?? ""
        }
    }
    
    // MARK: - Properties
    
    var model: PromoCodeFormViewModelType?
    private let dependencies: HasAnalytics = DependencyContainer()
    
    // MARK: - IB Outlet
    
    @IBOutlet private var codeTextField: UIRoundedTextField!

    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureInputs()
        dependencies.analytics.log(SettingsAnalytics.promoCode)
    }
    
    // MARK: - User Actions
    
    @IBAction func save(_ sender: UIButton?) {
        guard let code = codeTextField.text else {
            return
        }
        
        if self.validate().isValid {
            self.startLoading()
            
            model?.useCode(code: code) { [weak self] result, error in
                self?.stopLoading()
                if error == nil {
                    self?.codeTextField.text = ""
                    self?.saveButton?.isEnabled = false
                    
                    let popup = PopupViewController()
                    let promoCodePopup = PromoResultPopupController()
                    promoCodePopup.data = result
                    popup.contentController = promoCodePopup
                    self?.present(popup, animated: true, completion: nil)
                } else {
                    if let error = error {
                        UIAlertController(error: error).show()
                    }
                }
            }
        }
    }
    
    // MARK: - Dependence Injection
    override func configureDependencies() {
        self.setup(model: PromoCodeFormViewModel())
        super.configureDependencies()
    }
    
    func setup(model: PromoCodeFormViewModelType) {
        self.model = model
        isReady = true
    }
    
    // MARK: - Internal Methods
    
    /// Configure the step inputs
    private func configureInputs() {
        // add text fields to the controls list
        self.controls.append(codeTextField)
        
        // set delegate
        self.codeTextField.delegate = self
        
        // Configure validations
        var codeRules = ValidationRuleSet<String>()
        codeRules.add(rule: ValidationRuleLength(min: 2, max: 100, error: ValidateError.promoCodeInvalid))
        codeTextField.validationRules = codeRules
        
        self.validateOnInputChange = false
    }
    
    /// Move the cursor to next field
    /// If the field is the last field try to finish the step
    func goToNextField() {
        self.save(nil)
    }
}

extension PromoCodeFormViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.goToNextField()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        true
    }
}
