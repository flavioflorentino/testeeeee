import AnalyticsModule
import LegacyPJ
import SDWebImage
import UIKit
import Validator

final class OperatorFormViewController: FormViewController, UINavigationControllerDelegate {
    enum ValidateError: String, ValidationError {
        case operatorNameInvalid
        case operatorPasswordInvalid

        var message: String {
            FormLocalizable(rawValue: self.rawValue)?.text ?? ""
        }
    }
    
    // MARK: - Properties
    
    var model: OperatorFormModel?
    private let dependencies: HasAnalytics = DependencyContainer()
    
    // MARK: - IB Outlet
    
    @IBOutlet private var imageView: UICircularImageView?
    @IBOutlet private var nameTextField: UIRoundedTextField!
    @IBOutlet private var passwordTextField: UIRoundedTextField!
    @IBOutlet private var showPasswordImage: UIImageView!
    @IBOutlet private var sendImageButton: UIButton?
    @IBOutlet private var progressView: UIProgressView?
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        guard let model = model else {
            return
        }
        
        super.viewDidLoad()
        self.configureView()
        self.configureInputs()
        
        // Load form data
        if model.hasInitialLoad {
            startLoading()
            
            model.loadData({ [weak self] error in
                self?.stopLoading()
                if error == nil {
                    self?.populateForm()
                } else {
                    if let text = error?.localizedDescription {
                        self?.showMessage(text: text)
                    }
                }
            })
        }
    }
    
        // MARK: - User Actions
    
    @IBAction func save(_ sender: UIButton?) {
        if self.validate().isValid {
            self.saveFormData()
            AuthManager.shared.performActionWithAuthorization { [weak self] result in
                switch result {
                case .success(let password):
                    self?.startLoading()
                    self?.model?.save(
                        pin: password,
                        uploadProgress: { [weak self] progress in
                            DispatchQueue.main.async {
                                self?.progressView?.isHidden = false
                                self?.progressView?.progress = Float(progress)
                            }
                        }, { [weak self] error in
                            self?.progressView?.isHidden = true
                            if let error = error {
                                UIAlertController(error: error).show()
                            } else {
                                NotificationCenter.default.post(name: Notification.Name.Operator.listUpdated, object: nil)
                                _ = self?.navigationController?.popViewController(animated: true)
                                self?.dependencies.analytics.log(SettingsAnalytics.operatorAdded(self?.model?.data.username ?? ""))
                            }
                            self?.stopLoading()
                        }
                    )
                default:
                    break
                }
            }
        }
    }
    
    /// Select a new image
    @IBAction func selectImage(sender: UIButton?) {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - Dependence Injection
    override func configureDependencies() {
        self.setup(model: AddOperatorFormViewModel())
        super.configureDependencies()
    }
    
    func setup(model: OperatorFormModel) {
        self.model = model
        isReady = true
    }
    
    // MARK: - Internal Methods
    
    private func configureView() {
        guard let model = model else {
            return
        }
        
        self.saveButton?.setTitle(model.saveButtonLabel, for: .normal)
    }
    
    /// Configure the step inputs
    private func configureInputs() {
        guard let model = model else {
            return
        }
        
        self.progressView?.isHidden = true
        
        // add text fields to the controls list
        self.controls.append(passwordTextField)
        self.controls.append(nameTextField)
        
        // set delegate
        self.passwordTextField.delegate = self
        self.nameTextField.delegate = self
        
        // Configure validations
        if model.formType == .insert {
            var passwordRules = ValidationRuleSet<String>()
            passwordRules.add(rule: ValidationRuleLength(min: 6, error: ValidateError.operatorPasswordInvalid))
            passwordTextField.validationRules = passwordRules
        }
        
        var nameRules = ValidationRuleSet<String>()
        nameRules.add(rule: ValidationRuleLength(min: 3, error: ValidateError.operatorNameInvalid))
        nameTextField.validationRules = nameRules
        
        self.validateOnInputChange = false
        
        // Add a gesture to enable the user to view the password
        self.showPasswordImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(toggleShowPassword)))
        self.showPasswordImage.isUserInteractionEnabled = true
    }
    
    /// Populate the for with the
    private func populateForm() {
        if let name = self.model?.data.username {
            nameTextField.text = name
        }
        if let image = self.model?.data.imageUrl {
            imageView?.sd_setImage(with: URL(string: image))
            self.sendImageButton?.setAttributedTitle(NSAttributedString(string: ""), for: .normal)
        }
    }
    
    /// save form data
    private func saveFormData() {
        if let password = passwordTextField.text {
            self.model?.data.password = password
        }
        
        if let name = nameTextField.text {
            self.model?.data.username = name
        }
    }
    
    /// Move the cursor to next field
    /// If the field is the last field try to finish the step
    func goToNextField(from: UITextField) {
        if from == self.nameTextField {
            self.passwordTextField.becomeFirstResponder()
        } else {
            self.save(nil)
        }
    }
    
    /// Toggle the password visibility
    @objc
    func toggleShowPassword() {
        if passwordTextField.isSecureTextEntry {
            passwordTextField.isSecureTextEntry = false
            showPasswordImage.image = Assets.iconPasswordEyeBold.image
        } else {
            passwordTextField.isSecureTextEntry = true
            showPasswordImage.image = Assets.iconPasswordEye.image
        }
    }
}

extension OperatorFormViewController: UIImagePickerControllerDelegate {
    // MARK: - UIImagePickerControllerDelegate Methods
    
    private func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [String: Any]
    ) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage.rawValue] as? UIImage {
            imageView?.image = pickedImage
            
            // populate the model
            self.model?.image = pickedImage.jpegData(compressionQuality: 1.0)
            DispatchQueue.main.async {
                self.sendImageButton?.setAttributedTitle(NSAttributedString(string: ""), for: .normal)
                
                // clear image validation
                self.imageView?.layer.borderColor = UIColor.clear.cgColor
                self.imageView?.layer.borderWidth = 0.0
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension OperatorFormViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.goToNextField(from: textField)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == nameTextField {
            let acceptableCharacters = "abcdefghijklmnopqrstuvwxyz0123456789_."
            let cs = NSCharacterSet(charactersIn: acceptableCharacters).inverted
            let filtered = (string.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
            
            return string == filtered
        }
        
        return true
    }
}
