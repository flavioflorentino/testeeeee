import AccountManagementPJ
import AnalyticsModule
import LegacyPJ
import UIKit
import Validator

final class EmailFormViewController: FormViewController {
    enum ValidateError: String, ValidationError {
        case emailInvalid
        
        var message: String {
            FormLocalizable(rawValue: self.rawValue)?.text ?? ""
        }
    }

    // MARK: - Properties
    
    var model: EmailFormViewModel?
    private let dependencies: HasAnalytics = DependencyContainer()

    // MARK: - IB Outlet
    
    @IBOutlet private var emailTextField: UIRoundedTextField!
            
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureInputs()
        populateForm()
        
        dependencies.analytics.log(SettingsAnalytics.email)
    }
    
    // MARK: - User Actions
    
    @IBAction func save(_ sender: UIButton?) {
        if self.validate().isValid {
            saveFormData()
            AuthManager.shared.performActionWithAuthorization { [weak self] result in
                switch result {
                case .success(let password):
                    self?.startLoading()
                    self?.model?.saveEmail(password: password) { [weak self] error in
                        if let error = error {
                            UIAlertController(error: error).show()
                        } else {
                            NotificationCenter.default.post(name: Notification.Name.User.updated, object: nil)
                            AccountUserDataNotification.updated(.email)
                            _ = self?.navigationController?.popViewController(animated: true)
                        }
                        self?.stopLoading()
                    }
                    
                case .failure(let error):
                    self?.stopLoading()
                    UIAlertController(error: error).show()
                    
                default:
                    self?.stopLoading()
                }
            }
        }
    }
    
    // MARK: - Dependence Injection
    override func configureDependencies() {
        self.setup(model: UpdateEmailFormViewModel())
        super.configureDependencies()
    }
    
    func setup(model: EmailFormViewModel) {
        self.model = model
        isReady = true
    }
    
    /// Move the cursor to next field
    /// If the field is the last field try to finish the step
    func goToNextField() {
        self.save(nil)
    }
}

private extension EmailFormViewController {
    func configureInputs() {
        self.controls.append(emailTextField)
        
        self.emailTextField.delegate = self
        
        var emailRules = ValidationRuleSet<String>()
        emailRules.add(rule: ValidationRulePattern(pattern: EmailValidationPattern.standard, error: ValidateError.emailInvalid))
        emailTextField.validationRules = emailRules
        
        self.validateOnInputChange = false
    }
    
    func populateForm() {
        if let email = self.model?.user.email {
            emailTextField.text = email
        }
    }
    
    func saveFormData() {
        if let email = emailTextField.text {
            self.model?.user.email = email
        }
    }
}

extension EmailFormViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.goToNextField()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        true
    }
}
