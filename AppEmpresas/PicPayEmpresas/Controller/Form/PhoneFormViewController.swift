import AccountManagementPJ
import AnalyticsModule
import LegacyPJ
import UIKit
import Validator

final class PhoneFormViewController: FormViewController {
    enum ValidateError: String, ValidationError {
        case dddInvalid
        case phoneInvalid

        var message: String {
            FormLocalizable(rawValue: self.rawValue)?.text ?? ""
        }
    }
    
    // MARK: - Private Properties
    var model: PhoneFormViewModel?
    private let dependencies: HasAnalytics = DependencyContainer()
    
    // MARK: - IB Outlets
    @IBOutlet private var dddTextField: UIRoundedTextField!
    @IBOutlet private var phoneTextField: UIRoundedTextField!
    @IBOutlet private var mobileDddTextField: UIRoundedTextField!
    @IBOutlet private var mobileTextField: UIRoundedTextField!

    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureInputs()
        
        // Load form data
        populateForm()
        dependencies.analytics.log(SettingsAnalytics.phone)
    }
    
    // MARK: - User Actions
    
    @IBAction func save(_ sender: UIButton?) {
        self.validateOnInputChange = true
        
        if self.validate().isValid {
            saveFormData()
            AuthManager.shared.performActionWithAuthorization { [weak self] result in
                switch result {
                case .success(let password):
                    self?.startLoading()
                    self?.model?.savePhoneData(password: password) { [weak self] error in
                        if let error = error {
                            UIAlertController(error: error).show()
                        } else {
                            NotificationCenter.default.post(name: Notification.Name.Settings.updated, object: nil)
                            AccountUserDataNotification.updated(.phone)
                            _ = self?.navigationController?.popViewController(animated: true)
                        }
                        self?.stopLoading()
                    }
                default:
                    self?.stopLoading()
                }
            }
        }
    }
    
    // MARK: - Dependence Injection
    
    func setup(model: PhoneFormViewModel) {
        self.model = model
        isReady = true
    }
    
    // MARK: - Internal Methods
    
    /// Configure the step inputs
    func configureInputs() {
        self.controls.append(mobileTextField)
        self.controls.append(mobileDddTextField)
        self.controls.append(dddTextField)
        self.controls.append(phoneTextField)
        
        self.mobileDddTextField.delegate = self
        self.mobileTextField.delegate = self
        self.dddTextField.delegate = self
        self.phoneTextField.delegate = self
        
        var dddTypeRules = ValidationRuleSet<String>()
        dddTypeRules.add(rule: ValidationRuleLength(min: 2, error: ValidateError.dddInvalid))
        mobileDddTextField.validationRules = dddTypeRules
        
        var phoneTypeRules = ValidationRuleSet<String>()
        phoneTypeRules.add(rule: ValidationRuleLength(min: 9, error: ValidateError.phoneInvalid))
        mobileTextField.validationRules = phoneTypeRules
        
        self.validateOnInputChange = false
    }
    
    private func populateForm() {
        if let phone = self.model?.seller.store?.phone, phone.count > 1 {
            dddTextField.text = phone[0..<2]
            if let phone = phone[2..<phone.count] {
                phoneTextField.text = self.maskString(input: phone, mask: "[0000]-[0000]")
            }
        }
        if let mobile = self.model?.user.phone, mobile.count > 1 {
            mobileDddTextField.text = mobile[0..<2]
            if let phone = mobile[2..<mobile.count] {
                mobileTextField.text = self.maskString(input: phone, mask: "[00009]-[0000]")
            }
        }
    }
    
    /// save form data
    func saveFormData() {
        var phone = ""
        if let ddd = dddTextField.text {
            phone = ddd
        }
        
        if let phoneNumber = phoneTextField.text {
            phone += phoneNumber
        }
        model?.seller.store?.phone = phone
        
        var mobile = ""
        if let ddd = mobileDddTextField.text {
            mobile = ddd
        }
        
        if let phone = mobileTextField.text {
            mobile += phone
        }
        model?.user.phone = mobile
    }
    
    // MARK: - User Actions
    
    /// Move the cursor to next field
    /// If the field is the last field try to finish the step
    func goToNextField(from: UITextField) {
        if from == self.dddTextField {
            self.phoneTextField.becomeFirstResponder()
        } else {
            self.save(nil)
        }
    }
}

extension PhoneFormViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.goToNextField(from: textField)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if string.isEmpty { return true }
        let v: String = (text as NSString).replacingCharacters(in: range, with: string)
        
        if textField == self.dddTextField {
            if v.count > 2 {
                phoneTextField.becomeFirstResponder()
                return false
            }
            textField.text = self.maskString(input: v, mask: "[00]")
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        if textField == self.mobileDddTextField {
            if v.count > 2 {
                mobileTextField.becomeFirstResponder()
                return false
            }
            textField.text = self.maskString(input: v, mask: "[00]")
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        if textField == mobileTextField {
            textField.text = self.maskString(input: v, mask: "[00009]-[0000]")
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        if textField == phoneTextField {
            textField.text = self.maskString(input: v, mask: "[0000]-[0000]")
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        return true
    }
}
