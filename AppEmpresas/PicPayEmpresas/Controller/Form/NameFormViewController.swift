import AnalyticsModule
import LegacyPJ
import UIKit
import Validator

final class NameFormViewController: FormViewController, UINavigationControllerDelegate {
    enum ValidateError: String, ValidationError {
        case nameInvalid

        var message: String {
            FormLocalizable(rawValue: self.rawValue)?.text ?? ""
        }
    }
    
    // MARK: - Properties
    
    var model: NameFormViewModel?
    private let dependencies: HasAnalytics = DependencyContainer()
    
    // MARK: - IB Outlet

    @IBOutlet private var nameTextField: UIRoundedTextField!
    @IBOutlet private var imageView: UICircularImageView!
    @IBOutlet private var imageButton: UIButton!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        guard let model = model else {
            return
        }
        
        super.viewDidLoad()
        self.configureInputs()
        
        // Load form data
        if model.nameHasInitialLoad {
            startLoading()
            
            model.loadName({ [weak self] error in
                self?.stopLoading()
                if error == nil {
                    self?.populateForm()
                } else {
                    if let text = error?.localizedDescription {
                        self?.showMessage(text: text)
                    }
                }
            })
        }
        
        dependencies.analytics.log(SettingsAnalytics.editName)
    }
    
    // MARK: - User Actions
    
    @IBAction func save(_ sender: UIButton?) {
        if self.validate().isValid {
            saveFormData()
            AuthManager.shared.performActionWithAuthorization { [weak self] result in
                switch result {
                case .success(let password):
                    self?.startLoading()
                    self?.model?.saveName(password: password) { [weak self] error in
                        if let error = error {
                            UIAlertController(error: error).show()
                        } else {
                            self?.dependencies.analytics.log(SettingsAnalytics.nameUpdated)
                            NotificationCenter.default.post(name: Notification.Name.Settings.updated, object: nil)
                            _ = self?.navigationController?.popViewController(animated: true)
                        }
                        self?.stopLoading()
                    }
                case .failure(let error):
                    UIAlertController(error: error).show()
                    
                default:
                    break
                }
            }
        }
    }
    
    /// Select a new image
    @IBAction func selectImage(_sender: UIButton?) {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - Dependence Injection
    
    override func configureDependencies() {
        self.setup(model: UpdateNameFormViewModel())
        super.configureDependencies()
    }
    
    func setup(model: NameFormViewModel) {
        self.model = model
        isReady = true
    }
    
    // MARK: - Internal Methods
    
    /// Configure the step inputs
    private func configureInputs() {
        // add text fields to the controls list
        self.controls.append(nameTextField)
        
        // set delegate
        self.nameTextField.delegate = self
        
        // Configure validations
        
        var nameRules = ValidationRuleSet<String>()
        nameRules.add(rule: ValidationRuleLength(min: 3, error: ValidateError.nameInvalid))
        nameTextField.validationRules = nameRules
        
        self.validateOnInputChange = false
    }
    
    /// Populate the for with the
    private func populateForm() {
        if let name = self.model?.seller.name {
            nameTextField.text = name
        }
    }
    
    /// save form data
    private func saveFormData() {
        if let name = nameTextField.text {
            self.model?.seller.name = name
        }
    }
    
    /// Move the cursor to next field
    /// If the field is the last field try to finish the step
    func goToNextField() {
        self.save(nil)
    }
}

extension NameFormViewController: UIImagePickerControllerDelegate {
    // MARK: - UIImagePickerControllerDelegate Methods
    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage.rawValue] as? UIImage {
            imageView.image = pickedImage
            
            DispatchQueue.main.async {
                self.imageView.layer.borderColor = UIColor.clear.cgColor
                self.imageView.layer.borderWidth = 0.0
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension NameFormViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.goToNextField()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        true
    }
}
