import UIKit
import LegacyPJ

protocol BankSelectFormViewControllerDelegate: AnyObject {
    func didSelectBank(_ bank: Bank)
    func shouldHidePJAccountDisclaimer() -> Bool
}

final class BankSelectFormViewController: BaseTableViewController {
    var model: BankSelectFormViewModel?
    weak var delegate: BankSelectFormViewControllerDelegate?
    
    @IBOutlet var disclaimerLabel: UILabel!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadBanks()
        disclaimerLabel.isHidden = delegate?.shouldHidePJAccountDisclaimer() ?? false
    }
    
    // MARK: - Dependence Injection
    
    func setup(delegate: BankSelectFormViewControllerDelegate?, model: BankSelectFormViewModel = BankSelectListFormViewModel() ) {
        self.model = model
        self.delegate = delegate
        isReady = true
    }
    
    // MARK: - Internal Methods
    
    // Load the list of banks
    func loadBanks(loading: Bool = true) {
        if loading {
            startLoading()
        }
        model?.loadBanks({ [weak self] _ in
            self?.tableView.reloadData()
            self?.stopLoading()
            self?.refreshControl?.endRefreshing()
        })
    }
    
    // MARK: Event Handler
    
    override func handleRefresh(_ control: UIRefreshControl) {
        control.beginRefreshing()
        loadBanks(loading: false)
    }
    
    // MARK: UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model?.numberOfRowsInSection(section: section) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = model else {
            return UITableViewCell()
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.separatorInset = UIEdgeInsets.zero
        
        if let cell = cell as? BankTableViewCell {
            let bank = model.bankForRow(indexPath)
            cell.configureCell(bank)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let model = model else {
            return
        }
        
        let bank = model.bankForRow(indexPath)
        delegate?.didSelectBank(bank)
        _ = navigationController?.popViewController(animated: true)
    }
}
