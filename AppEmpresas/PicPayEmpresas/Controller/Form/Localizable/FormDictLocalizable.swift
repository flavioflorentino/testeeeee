import Foundation

enum FormDictLocalizable: String, Localizable {
    // DayForm
    case numberOfDays
    
    var key: String {
        self.rawValue
    }
    var file: LocalizableFile {
        .formDict
    }
}
