import Foundation

enum FormLocalizable: String, Decodable, Localizable {
    // Address
    case addressCepInvalid
    case addressStreetInvalid
    case addressNumberInvalid
    case addressDistrictInvalid
    case addressCityInvalid
    case addressStateInvalid
    // CNPJ
    case cpnjInvalid
    case cnpjCompanyNameInvalid
    // Email
    case emailInvalid
    // Name
    case nameInvalid
    // Operator
    case operatorNameInvalid
    case operatorPasswordInvalid
    // Password
    case currentPassordInvalid
    case passwordInvalid
    case forgotPassword
    // Personal
    case personalNameInvalid
    case perosnalCpfInvalid
    case personalBirthdayInvalid
    // Phone
    case dddInvalid
    case phoneInvalid
    // Promo
    case promoCodeInvalid
    
    case taxZeroStartPercent
    case taxZeroFeeLabel

    var key: String {
        self.rawValue
    }
    var file: LocalizableFile {
        .form
    }
}
