import AnalyticsModule
import UIKit
import Validator

final class AddressFormViewController: FormViewController {
    private typealias Localizable = Strings.SettingsTableView

    enum ValidateError: String, ValidationError {
        case addressCepInvalid
        case addressStreetInvalid
        case addressNumberInvalid
        case addressDistrictInvalid
        case addressCityInvalid
        case addressStateInvalid
        
        var message: String {
            FormLocalizable(rawValue: self.rawValue)?.text ?? ""
        }
    }
    
    // MARK: - Public Properties
    
    var model: AddressFormViewModel?
    
    // MARK: - IB Outlets
    @IBOutlet private var cepTextField: UIRoundedTextField!
    @IBOutlet private var streetTextField: UIRoundedTextField!
    @IBOutlet private var numberTextField: UIRoundedTextField!
    @IBOutlet private var districtTextField: UIRoundedTextField!
    @IBOutlet private var cityTextField: UIRoundedTextField!
    @IBOutlet private var stateTextField: UIRoundedTextField!
    @IBOutlet private var complementTextField: UIRoundedTextField!
    
    private let dependencies: HasAnalytics = DependencyContainer()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureInputs()
        self.loadInitialData()
        dependencies.analytics.log(SettingsAnalytics.editAddress)
    
        title = (model?.address.isResidentialAddress ?? false)
            ? Localizable.EditAddress.residential
            : Localizable.EditAddress.company
    }
    
    /// Save data and finish the step
    @IBAction func finishStep(_ sender: UIButton?) {
        self.validateOnInputChange = true
        
        if self.validate(true).isValid {
            self.saveFormData()
            
            self.startLoading()
            self.model?.loadAddressCoordinate { [weak self] _, _, _ in
                self?.stopLoading()
                self?.goToNextStep()
            }
        }
    }
    
    // MARK: - Dependence Injection
    override func configureDependencies() {
        self.setup(model: UpdateAddressFormViewModel())
        super.configureDependencies()
    }
    
    func setup(model: AddressFormViewModel) {
        self.model = model
        isReady = true
    }
    
    // MARK: - Internal Methods
    
    /// Configure the step inputs
    func configureInputs() {
        // add text fields to the controls list
        self.controls.append(cepTextField)
        self.controls.append(streetTextField)
        self.controls.append(numberTextField)
        self.controls.append(districtTextField)
        self.controls.append(cityTextField)
        self.controls.append(stateTextField)
        
        // set delegate
        self.cepTextField.delegate = self
        self.streetTextField.delegate = self
        self.numberTextField.delegate = self
        self.districtTextField.delegate = self
        self.cityTextField.delegate = self
        self.stateTextField.delegate = self
        
        // Configure validations
        var cepRules = ValidationRuleSet<String>()
        cepRules.add(rule: ValidationRuleLength(min: 9, error: ValidateError.addressCepInvalid))
        cepTextField.validationRules = cepRules
        
        var streetRules = ValidationRuleSet<String>()
        streetRules.add(rule: ValidationRuleLength(min: 4, error: ValidateError.addressStreetInvalid))
        streetTextField.validationRules = streetRules
        
        var numberRules = ValidationRuleSet<String>()
        numberRules.add(rule: ValidationRuleLength(min: 1, error: ValidateError.addressNumberInvalid))
        numberTextField.validationRules = numberRules
        
        var districtRules = ValidationRuleSet<String>()
        districtRules.add(rule: ValidationRuleLength(min: 4, error: ValidateError.addressDistrictInvalid))
        districtTextField.validationRules = districtRules
        
        var cityRules = ValidationRuleSet<String>()
        cityRules.add(rule: ValidationRuleLength(min: 4, error: ValidateError.addressCityInvalid))
        cityTextField.validationRules = cityRules
        
        var stateRules = ValidationRuleSet<String>()
        stateRules.add(rule: ValidationRuleLength(min: 2, error: ValidateError.addressStateInvalid))
        stateTextField.validationRules = stateRules
        
        self.validateOnInputChange = false
    }
    
    //
    func loadInitialData() {
        // Load form data
        
        guard let model = model else {
            return
        }
        
        if model.addressHasInitialLoad {
            startLoading()
            
            model.loadAddress({ [weak self] error in
                self?.stopLoading()
                if error == nil {
                    self?.populateForm()
                } else {
                    if let text = error?.localizedDescription {
                        self?.showMessage(text: text)
                    }
                }
            })
        }
    }
    
    /// Send to the next screen of the registration form
    func goToNextStep() {
        guard let model = model else {
            return
        }
        
        let controller: MapFormViewController = ViewsManager.instantiateViewController(.settings)
        controller.setup(model: model)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    /// Load and populate the address according to cep
    func loadAddressByCep() {
        guard let cep = self.cepTextField.text else {
            return
        }
        
        let result = cepTextField.validate()
        if result.isValid {
            self.startLoading()
            guard let model = model else {
                return
            }
            
            model.loadAddressByCep(cep: cep, callback: { [weak self] address, _ in
                if let add = address {
                    self?.streetTextField.text = add.address
                    self?.districtTextField.text = add.district
                    self?.cityTextField.text = add.city
                    self?.stateTextField.text = add.state
                }
                self?.stopLoading()
            })
        }
    }
    
    /// save form data
    func saveFormData() {
        if let cep = cepTextField.text {
            self.model?.address.cep = cep
        }
        
        if let street = streetTextField.text {
            self.model?.address.address = street
        }
        
        if let city = cityTextField.text {
            self.model?.address.city = city
        }
        
        if let district = districtTextField.text {
            self.model?.address.district = district
        }
        
        if let number = numberTextField.text {
            self.model?.address.number = number
        }
        
        if let state = stateTextField.text {
            self.model?.address.state = state
        }
    }
    
    /// Populate the for with the
    private func populateForm() {
        if let cep = self.model?.address.cep {
            cepTextField.text = self.maskString(input: cep, mask: "[00000]-[000]")
        }
        
        if let street = self.model?.address.address {
            streetTextField.text = street
        }
        
        if let city = self.model?.address.city {
            cityTextField.text = city
        }
        
        if let district = self.model?.address.district {
            districtTextField.text = district
        }
        
        if let number = self.model?.address.number {
            numberTextField.text = number
        }
        
        if let state = self.model?.address.state {
            stateTextField.text = state
        }
    }
    
    // MARK: - User Actions
    
    /// Move the cursor to next field
    /// If the field is the last field try to finish the step
    func goToNextField(from: UITextField) {
        if from == self.cepTextField {
            self.loadAddressByCep()
        } else if from == self.streetTextField {
            self.numberTextField.becomeFirstResponder()
        } else if from == self.numberTextField {
            self.districtTextField.becomeFirstResponder()
        } else if from == self.districtTextField {
            self.cityTextField.becomeFirstResponder()
        } else if from == self.cityTextField {
            self.stateTextField.becomeFirstResponder()
        } else {
            self.finishStep(nil)
        }
    }
}

extension AddressFormViewController: UITextFieldDelegate {
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.goToNextField(from: textField)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if string.isEmpty { return true }
        let v: String = (text as NSString).replacingCharacters(in: range, with: string)
        
        if textField == cepTextField {
            textField.text = self.maskString(input: v, mask: "[00000]-[000]")
            self.loadAddressByCep()
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        if textField == stateTextField {
            textField.text = self.maskString(input: v, mask: "[AA]")
            textField.sendActions(for: .editingChanged)
            return false
        }
        
        return true
    }
}
