import Core
import Foundation
import UIKit
import WebKit

final class WebViewController: UIViewController, WKNavigationDelegate {
    enum NameScripts: String {
        case recoveryPassword = "recoveryPasswordSuccess"
    }

    // MARK: Properties
    var webView: WKWebView
    var url: String?
    var navigationTitle: String?

    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var webViewContainer: UIView!

    required init?(coder aDecoder: NSCoder) {
        self.webView = WKWebView()
        super.init(coder: aDecoder)
    }

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        title = navigationTitle
        configureWebView()
    }

    func addScript(file: String) {
        let source = bundleLoad(file: file)
        let content = WKUserContentController()
        let script = WKUserScript(source: source, injectionTime: .atDocumentEnd, forMainFrameOnly: true)

        content.addUserScript(script)
        content.add(self, name: NameScripts.recoveryPassword.rawValue)

        let config = WKWebViewConfiguration()
        config.userContentController = content

        self.webView = WKWebView(frame: UIScreen.main.bounds, configuration: config)
    }

    private func configureWebView() {
        view.layoutIfNeeded()

        webView.navigationDelegate = self
        webView.allowsBackForwardNavigationGestures = true
        webView.translatesAutoresizingMaskIntoConstraints = false
        webViewContainer.addSubview(webView)

        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: webViewContainer.topAnchor),
            webView.bottomAnchor.constraint(equalTo: webViewContainer.bottomAnchor),
            webView.leadingAnchor.constraint(equalTo: webViewContainer.leadingAnchor),
            webView.trailingAnchor.constraint(equalTo: webViewContainer.trailingAnchor)
        ])

        // Load url
        if let url = url, let formattedUrl = URL(string: url) {
            webView.load(URLRequest(url: formattedUrl))
        }
    }

    private func bundleLoad(file: String) -> String {
        guard
            let path = Bundle.main.path(forResource: file, ofType: "js"),
            let sourceScript = try? String(contentsOfFile: path)
            else {
                return ""
        }

        return sourceScript
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation) {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }

    func webView(
        _ webView: WKWebView,
        didReceive challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    ) {
        let pinningHandler = PinningHandler()
        pinningHandler.handle(challenge: challenge, completionHandler: completionHandler)
    }

    func webView(_ webView: WKWebView,
                 decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let url = navigationAction.request.url, let navController = navigationController else {
            return
        }

        let container = DependencyContainer()
        let deeplinkHelper = DeeplinkHelper(dependencies: container)
        deeplinkHelper.open(url: url, from: navController)

        decisionHandler(.allow)
    }
}

extension WebViewController: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == NameScripts.recoveryPassword.rawValue {
            navigationController?.popViewController(animated: true)
        }
    }
}
