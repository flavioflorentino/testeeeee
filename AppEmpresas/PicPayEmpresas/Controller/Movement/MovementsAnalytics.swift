import AnalyticsModule

enum MovementsAnalytics: AnalyticsKeyProtocol {
    case futureTransactions
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .futureTransactions:
            return AnalyticsEvent("Sales - Future Transactions", properties: [:], providers: [.mixPanel])
        }
    }
}
