import AnalyticsModule
import Foundation
import LegacyPJ
import UI
import UIKit

final class ExportMovementsViewController: BaseViewController, UITextFieldDelegate {
    private typealias Localizable = Strings.Movement
    
    private let initialDatePicker = UIDatePicker()
    private let finalDatePicker = UIDatePicker()
    
    var initialDate: Date?
    var finalDate: Date?
    var fileFormat: Int = 0
    
    fileprivate let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM,yyyy"
        return formatter
    }()
    
    // MARK: - Properties
    var model: ExportMovementViewModel?
    
    private let dependencies: HasAnalytics = DependencyContainer()
    
    @IBOutlet private var fromDateTextField: UIRoundedTextField!
    @IBOutlet private var toDateTextField: UIRoundedTextField!
    @IBOutlet private var pdfStackView: UIStackView!
    @IBOutlet private var csvStackView: UIStackView!
    @IBOutlet private var pdfRadioButton: UIRadioButton!
    @IBOutlet private var csvRadioButton: UIRadioButton!
    @IBOutlet private var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private var sendEmailButton: UIPPButton!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setInitialDatePicker()
        setFinalDatePicker()
        setRadioButtons()
        activityIndicator.isHidden = true
        sendEmailButton.isEnabled = false
        pdfRadioButton.isSelected = true
        csvRadioButton.isSelected = false
    }
    
    @IBAction func sendMovementByEmailAction(_ sender: UIPPButton) {
        guard let initialDate = initialDate, let finalDate = finalDate else { return }
        sendEmailButton.isEnabled = false
        sendEmailButton.startLoadingAnimating()
        
        let exportDateFormatter = DateFormatter()
        exportDateFormatter.dateFormat = "yyy-MM-dd"
        model?.fromDate = exportDateFormatter.string(from: initialDate)
        model?.toDate = exportDateFormatter.string(from: finalDate)
        model?.type = fileFormat == 0 ? "PDF" : "CSV"
        
        model?.exportMovement({ [weak self] email, error in
            self?.sendEmailButton.isEnabled = true
            self?.sendEmailButton.stopLoadingAnimating()
            
            if let emailStr = email {
                self?.dependencies.analytics.log(ExportMovementsAnalytics.exportMovement(
                    type: self?.model?.type ?? "",
                    startDate: self?.model?.fromDate ?? "",
                    endDate: self?.model?.toDate ?? ""))
                
                let popup = PopupViewController()
                let alertPopup = AlertPopupController()
                alertPopup.popupTitle = Localizable.sentTitle
                alertPopup.popupText = Localizable.sentMessage(emailStr)
                popup.contentController = alertPopup
                self?.present(popup, animated: true, completion: nil)
            }
            
            if let error = error {
                UIAlertController(error: error).show()
            }
        })
    }
    
    // MARK: - Dependencie Injection
    
    override func configureDependencies() {
        self.setup(model: ExportMovementImpViewModel())
        super.configureDependencies()
    }
    
    func setup(model: ExportMovementViewModel) {
        self.model = model
        isReady = true
    }
    
    func setRadioButtons() {
        let tapPdfGesture = UITapGestureRecognizer(target: self, action: #selector(self.checkPdfRadioButton))
        pdfStackView.addGestureRecognizer(tapPdfGesture)
        
        let tapCsvGesture = UITapGestureRecognizer(target: self, action: #selector(self.checkCsvRadioButton))
        csvStackView.addGestureRecognizer(tapCsvGesture)
    }
    
    @objc
    func checkPdfRadioButton() {
        pdfRadioButton.isSelected = true
        csvRadioButton.isSelected = false
        fileFormat = 0
    }
    
    @objc
    func checkCsvRadioButton() {
        pdfRadioButton.isSelected = false
        csvRadioButton.isSelected = true
        fileFormat = 1
    }
    
    func setInitialDatePicker() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: Localizable.select, style: .plain, target: self, action: #selector(doneInitialDatePicker))
        doneButton.tintColor = Colors.branding500.color
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Localizable.cancel, style: .plain, target: self, action: #selector(cancelDatePicker))
        cancelButton.tintColor = Colors.grayscale500.color
        toolbar.setItems([doneButton, spaceButton, cancelButton], animated: false)

        fromDateTextField.inputAccessoryView = toolbar
        initialDatePicker.datePickerMode = .date
        initialDatePicker.maximumDate = Date()
        fromDateTextField.inputView = initialDatePicker
        if #available(iOS 13.4, *) {
            initialDatePicker.preferredDatePickerStyle = .wheels
        }
    }
    
    func setFinalDatePicker() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: Localizable.select, style: .plain, target: self, action: #selector(doneFinalDatePicker))
        doneButton.tintColor = Colors.branding500.color
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: Localizable.cancel, style: .plain, target: self, action: #selector(cancelDatePicker))
        cancelButton.tintColor = Colors.grayscale500.color
        toolbar.setItems([doneButton, spaceButton, cancelButton], animated: false)
        
        toDateTextField.inputAccessoryView = toolbar
        finalDatePicker.datePickerMode = .date
        finalDatePicker.maximumDate = Date()
        toDateTextField.inputView = finalDatePicker
        if #available(iOS 13.4, *) {    
            finalDatePicker.preferredDatePickerStyle = .wheels
        }
    }
    
    @objc
    func doneInitialDatePicker() {
        initialDate = initialDatePicker.date
        finalDatePicker.minimumDate = initialDate
        fromDateTextField.text = "\(Localizable.from): \(dateFormatter.string(from: initialDatePicker.date).capitalized)"
        self.view.endEditing(true)
        updateSendMovementByEmailButton()
    }
    
    @objc
    func doneFinalDatePicker() {
        finalDate = finalDatePicker.date
        initialDatePicker.maximumDate = finalDate
        toDateTextField.text = "\(Localizable.until): \(dateFormatter.string(from: finalDatePicker.date).capitalized)"
        self.view.endEditing(true)
        updateSendMovementByEmailButton()
    }
    
    @objc
    func cancelDatePicker() {
        self.view.endEditing(true)
    }
    
    func updateSendMovementByEmailButton() {
        if initialDate != nil, finalDate != nil {
            self.sendEmailButton.isEnabled = true
        } else {
            self.sendEmailButton.isEnabled = false
        }
    }
}
