import AnalyticsModule
import LegacyPJ
import UIKit

final class MovementsViewController: BaseViewController {
    enum Segment: Int {
        case movements = 0
        case futureMovements = 1
    }
    
    // MARK: - Properties
    
    private lazy var movementsTableViewController: MovementsTableViewController = {
        let controller: MovementsTableViewController = ViewsManager.instantiateViewController(.movement)
        controller.setup(model: MovementListNormalViewModel())
        return controller
    }()
    
    private lazy var futureMovementsTableViewController: MovementsTableViewController = {
        let controller: MovementsTableViewController = ViewsManager.instantiateViewController(.movement)
        controller.setup(model: MovementListFutureViewModel())
        return controller
    }()
    
    // MARK: - IB Outlet
    @IBOutlet private var segmentedControl: UISegmentedControl!
    @IBOutlet private var containerView: UIView!
    @IBOutlet private var exportBarButton: UIBarButtonItem!
    private let dependencies: HasAnalytics = DependencyContainer()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSegmentControl()
        
        displayContentController(controller: movementsTableViewController)
    }
    
    @IBAction func handlerExportButtonClick(_ sender: Any) {
        let exportMovementsViewController: ExportMovementsViewController = ViewsManager.instantiateViewController(.movement)
        exportMovementsViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(exportMovementsViewController, animated: true)
    }
    
    // MARK: - Dependence Injections
    
    override func configureDependencies() {
        isReady = true
        super.configureDependencies()
    }
    
    // MARK: - Internal Methods
    
    func configureSegmentControl() {
        if #available(iOS 13.0, *) {
            let selectedColor = #colorLiteral(red: 0.1294117647, green: 0.7607843137, blue: 0.368627451, alpha: 1)
            segmentedControl.selectedSegmentTintColor = selectedColor
            segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: selectedColor], for: .normal)
            segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)], for: .selected)
        }
        segmentedControl.addTarget(self, action: #selector(self.handlerSegmentedChange), for: .valueChanged)
    }
    
    @objc
    func handlerSegmentedChange(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == Segment.movements.rawValue {
            changeContentController(old: futureMovementsTableViewController, new: movementsTableViewController)
        } else {
            dependencies.analytics.log(MovementsAnalytics.futureTransactions)
            changeContentController(old: movementsTableViewController, new: futureMovementsTableViewController )
        }
    }
    
    func displayContentController(controller: UITableViewController) {
        // Add the view controller
        self.addChild(controller)
        controller.view.frame = containerView.frame
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(controller.view)
        
        // Apply the contrants to present the corret size
        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            controller.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            controller.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            controller.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
        
        // move controller
        controller.didMove(toParent: self)
    }
    
    func changeContentController(old: UIViewController, new: UIViewController) {
        // Prepare the two view controllers for the change.
        old.willMove(toParent: nil)
        self.addChild(new)
        
        new.view.frame = old.view.frame
        
        transition(
            from: old,
            to: new,
            duration: 0.25,
            options: .showHideTransitionViews,
            animations: {},
            completion: { _ in
                old.removeFromParent()
            
                NSLayoutConstraint.activate([
                    new.view.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor),
                    new.view.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor),
                    new.view.topAnchor.constraint(equalTo: self.containerView.topAnchor),
                    new.view.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor)
                ])
            
                new.didMove(toParent: self)
            }
        )
    }
}
