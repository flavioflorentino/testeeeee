import UIKit
import LegacyPJ

final class MovementsTableViewController: BaseTableViewController {
    // MARK: Private Properties
    
    enum Cell: String {
        case movement = "movements"
    }
    
    private var model: MovementListViewModel?
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        
        configureTableView()
        loadItems()
    }
    
    // MARK: - Dependence Injections
    
    func setup(model: MovementListViewModel) {
        self.model = model
        isReady = true
    }
    
    // MARK: - Internal Methods
    
    func configureTableView() {
        self.registerCell(Cell.movement.rawValue, nib: "MovementItemTableViewCell")
        tableView.backgroundColor = #colorLiteral(red: 0.95, green: 0.95, blue: 0.96, alpha: 1.0)
        tableView.tableHeaderView = UIView()
        tableView.tableFooterView = UIView()
    }
    
    func configureView() {
        self.tableView.reloadData()
    }
    
    func configureTableHeader(header: TableHeader?) {
        guard let header = header else {
            self.tableView.tableHeaderView = nil
            return
        }
        
        let label = UILabel(frame: CGRect(x: 16, y: 10, width: self.view.frame.size.width - 32.0, height: 100))
        label.text = header.text
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .white
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.sizeToFit()
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: label.frame.size.height + 20))
        headerView.backgroundColor = #colorLiteral(red: 0.27, green: 0.31, blue: 0.33, alpha: 1.00)
        headerView.addSubview(label)
        self.tableView.tableHeaderView = headerView
    }
    
    func loadItems() {
        guard let model = model else { return }
        
        startLoading()
        model.loadMovementItems { [weak self] response in
            self?.configureView()
            self?.stopLoading()
            self?.refreshControl?.endRefreshing()
            self?.checkIsTableEmpty(response.error)
            self?.configureTableHeader(header: response.header)
            if let error = response.error {
                UIAlertController(error: error).show()
            }
        }
    }
    
    override func shouldLoadMoreRows() -> Bool {
        guard let model = model else {
            return false
        }
        
        return model.shouldLoadMoreItems()
    }
    
    override func loadMoreRows() {
        guard let model = model else { return }
        
        startLoading()
        
        model.loadMoreMovementItems { [weak self] response in
            self?.stopLoading()
            
            self?.configureView()
            
            if let error = response.error {
                UIAlertController(error: error).show()
            }
        }
    }
    
    /// Check if the table is Empty to show the onboard screen
    func checkIsTableEmpty(_ error: LegacyPJError?) {
        guard let model = model else {
            return
        }
        
        if error == nil && model.numberOfSections() == 0 {
            let onboard = OnboardView(frame: tableView.frame)
            onboard.ilustrationImage.image = nil
            onboard.titleLabel.text = model.onboardTitle
            onboard.descriptionLabel.text = model.onboardText
            
            tableView.tableFooterView = onboard
        } else {
            tableView.tableFooterView = UIView()
        }
    }
    
    // MARK: Event Handler
    
    override func handleRefresh(_ control: UIRefreshControl) {
        control.beginRefreshing()
        loadItems()
    }

    // MARK: UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        model?.numberOfSections() ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model?.numberOfRowsInSection(section: section) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let model = model,
            let movementItem = model.movementItemForRow(indexPath),
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.movement.rawValue) as? MovementItemTableViewCell
            else {
                return UITableViewCell()
            }
        
        cell.configureCell(movementItem)
        return cell
    }
    
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let model = model else {
            return
        }
        
        if let movementDay = model.movementItemForRow(indexPath) {
            movementDay.expanded.toggle()
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let item = model?.movementItemForRow(indexPath) {
            return MovementItemTableViewCell.calculatedHeight(item)
        }
        return 44.0
    }
    
    // ------  Sections Header -------
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let movementItem = model?.movementItemForSection(section: section) {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 30))
            view.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9529411765, blue: 0.9568627451, alpha: 1)
            
            let label = UILabel(frame: CGRect(x: 20, y: 0, width: tableView.frame.size.width - 40, height: 30))
            label.font = UIFont.systemFont(ofSize: 11)
            label.textColor = #colorLiteral(red: 0.4470588235, green: 0.4470588235, blue: 0.4470588235, alpha: 1)
            label.text = movementItem.label
            
            view.addSubview(label)
            
            return view
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        30.0
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 20))
        view.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9529411765, blue: 0.9568627451, alpha: 1)
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        20.0
    }
}
