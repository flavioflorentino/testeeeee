import AnalyticsModule

enum ExportMovementsAnalytics: AnalyticsKeyProtocol {
    case exportMovement(type: String, startDate: String, endDate: String)
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case let .exportMovement(type, startDate, endDate):
            var properties = [String: String]()
            properties["type"] = type
            properties["start_date"] = startDate
            properties["end_date"] = endDate
            
            return AnalyticsEvent("Payments Statement Exported", properties: properties, providers: [.mixPanel])
        }
    }
}
