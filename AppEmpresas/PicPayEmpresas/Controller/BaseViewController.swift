import SVProgressHUD
import UIKit

class BaseViewController: UIViewController {
    var isReady: Bool = false
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if the dependencies were injected
        // if necessary call the method to configure the dependencies
        if !isReady {
            self.configureDependencies()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // Ensuring that SVProgress will disappear and return maskType to App's default
        SVProgressHUD.dismiss()
        SVProgressHUD.setDefaultMaskType(.black)
        super.viewWillDisappear(animated)
    }
    
    // MARK: Dependece Injection
    
    func configureDependencies() {
        isReady = true
    }
}

extension UIViewController {
    /// Push the view controller on the navigation controller
    ///
    /// - Parameters:
    ///   - controller: view controller to push
    ///   - hidesBottomBar: hidesBottomBarWhenPushed
    func pushViewController(_ controller: UIViewController, hidesBottomBar: Bool = true) {
        controller.hidesBottomBarWhenPushed = hidesBottomBar
        navigationController?.pushViewController(controller, animated: true)
    }
}
