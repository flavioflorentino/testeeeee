import UIKit

final class PromoCodePopupController: BaseViewController, PopupPresentable {
    weak var popup: PopupViewController?
    var model: PromoCodeFormViewModelType?
    
    @IBOutlet private var codeTextField: UIRoundedTextField!
    @IBOutlet private var sendButton: UIPPButton!
    
    // MARK: - Initialiazer
    
    init() {
        super.init(nibName: "PromoCodePopupController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        codeTextField.layer.borderColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        codeTextField.layer.borderWidth = 1.0
    }
    
    // MARK: - User Actions
    
    @IBAction func sendCode(_ sender: Any) {
        guard let code = codeTextField.text else {
            return
        }
        
        sendButton.isEnabled = false
        sendButton.startLoadingAnimating()
        
        model?.useCode(code: code) { [weak self] result, error in
            self?.sendButton.isEnabled = true
            self?.sendButton.stopLoadingAnimating()
            
            if error == nil {
                let popup = PopupViewController()
                let promoCodePopup = PromoResultPopupController()
                promoCodePopup.data = result
                popup.contentController = promoCodePopup
            
                let presenting = self?.presentingViewController
                self?.dismiss(animated: true, completion: {
                    presenting?.present(popup, animated: true, completion: nil)
                })
            } else {
                if let error = error {
                    let alert = UIAlertController(error: error)
                    self?.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func dismiss(_ sender: Any) {
        popup?.dismiss(animated: true)
    }
    
    // MARK: - Dependence Injection
    
    override func configureDependencies() {
        setup(model: PromoCodeFormViewModel())
    }
    
    func setup(model: PromoCodeFormViewModelType) {
        self.model = model
    }
}
