import UIKit

final class PromoResultPopupController: BaseViewController, PopupPresentable {
    weak var popup: PopupViewController?
    
    var data: PromoCodeResult?
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    @IBOutlet var textLabel: UILabel!
    @IBOutlet var actionButton: UIPPButton!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let data = data {
            titleLabel.text = data.title
            let subtitle = "<center>" + data.subtitle + "</center>"
            if let attributeString = subtitle.html2AttributedString?.attributedPicpayFont(16, weight: UIFont.Weight.regular.rawValue) {
                subtitleLabel.attributedText = attributeString
            }
            textLabel.text = data.text
        }
    }

    // MARK: - User Actions
    
    @IBAction func close(_ sender: Any) {
        popup?.dismiss(animated: true, completion: nil)
    }
}
