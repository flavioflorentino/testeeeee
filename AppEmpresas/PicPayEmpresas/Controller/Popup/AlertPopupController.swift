import UI
import UIKit

struct AlertPopupConfiguration {
    let titleButton: String
    let backgroundButton: UIColor
    let higlightBackgroundButton: UIColor
    let titleColorButton: UIColor
    let highlightTitleColor: UIColor
}

extension AlertPopupController.Layout {
    enum Size {
        static let buttonRadius: CGFloat = 22
    }
}

final class AlertPopupController: BaseViewController, PopupPresentable {
    fileprivate enum Layout {}
    
    weak var popup: PopupViewController?
    
    var popupTitle: String?
    var popupSubtitle: String?
    var popupText: String?
    var buttonText: String?
    var buttonAction: (() -> Void)?
    
    private var configuration: AlertPopupConfiguration?
    
    // MARK: - IBOutlets
    
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var subtitleLabel: UILabel!
    @IBOutlet private var textLabel: UILabel!
    @IBOutlet private var actionButton: UIPPButton!
    
    // MARK: - View Life Cycle
    init(with configuration: AlertPopupConfiguration? = nil) {
        self.configuration = configuration
        super.init(nibName: String(describing: AlertPopupController.self), bundle: Bundle.main)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLabelText(label: titleLabel, text: popupTitle)
        setLabelText(label: subtitleLabel, text: popupSubtitle)
        setLabelText(label: textLabel, text: popupText)
        
        if let buttonText = buttonText {
            actionButton.setTitle(buttonText, for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        buttonConfiguration()
    }
    
    // MARK: - User Actions
    
    @IBAction func close(_ sender: Any) {
        popup?.dismiss(animated: true, completion: { [weak self] in
            self?.buttonAction?()
        })
    }
    
    // MARK: - Public Methods
    
    func setLabelText (label: UILabel, text: String?) {
        if let text = text {
            label.text = text
        } else {
            label.isHidden = true
        }
    }
    
    private func buttonConfiguration() {
        guard let configuration = configuration else {
            return
        }
        
        actionButton.setTitle(configuration.titleButton, for: .normal)
        actionButton.normalBackgrounColor = configuration.backgroundButton
        actionButton.highlightedBackgrounColor = configuration.higlightBackgroundButton
        actionButton.normalTitleColor = configuration.titleColorButton
        actionButton.highlightedTitleColor = configuration.highlightTitleColor
        
        actionButton.borderColor = .clear
        actionButton.cornerRadius = Layout.Size.buttonRadius
        actionButton.layer.masksToBounds = true
    }
}
