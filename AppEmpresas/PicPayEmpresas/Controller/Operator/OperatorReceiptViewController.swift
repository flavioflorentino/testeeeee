import SDWebImage
import UIKit

final class OperatorReceiptViewController: BaseViewController {
    // MARK: - Properties
    
    private var loadingView: LoadingHeaderView?
    var model: HistoryItemViewModel?
    
    // MARK: - IB Outlet
    
    @IBOutlet var photoImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var totalReceiptsLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadItem()
    }
    
    // MARK: - Dependence Injections
    
    func setup(model: HistoryItemViewModel) {
        self.model = model
        isReady = true
    }
    
    // MARK: - Internal Methods
    
    private func loadItem() {
        startLoading()
        self.model?.loadHistoryItem({ [weak self] error in
            if let error = error {
                UIAlertController(error: error).show()
            } else {
                self?.configureView()
                self?.stopLoading()
            }
        })
    }
    
    private func configureView() {
        guard let model = model else {
            return
        }
        
        if let op = model.historyItem.operatorr {
            if let imageUrl = op.imageUrl {
                photoImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: Assets.iluOperatorPlaceholder.image)
            }
            if let name = op.username {
                nameLabel.text = name
            }
        }
        
        dateLabel.text = "\(model.historyItem.date) - \(model.historyItem.time)"
        valueLabel.text = model.historyItem.totalReceived
        if let receipts = model.historyItem.totalReceipts {
            totalReceiptsLabel.text = "\(receipts)"
        }
    }
    
    private func startLoading() {
        loadingView?.removeFromSuperview()
        let loadingView = LoadingHeaderView(frame: view.frame)
        self.loadingView = loadingView
        loadingView.backgroundColor = .white
        view.addSubview(loadingView)
    }
    
    private func stopLoading() {
        UIView.animate(
            withDuration: 0.5,
            animations: {
                self.loadingView?.layer.opacity = 0
            }, completion: { _ in
                self.loadingView?.removeFromSuperview()
            }
        )
    }
}
