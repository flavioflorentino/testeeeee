import AnalyticsModule
import Foundation
import LegacyPJ
import SVProgressHUD
import UI
import UIKit

final class OperatorTableViewController: BaseTableViewController {
    private typealias Localizable = Strings.Default
    
    // MARK: Private Properties
    
    private var model: OperatorListViewModel?
    private let dependencies: HasAnalytics = DependencyContainer()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        loadOperators()
        setupNotifications()
        
        dependencies.analytics.log(SettingsAnalytics.manageOperators)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = Localizable.navOperatorListTitle
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = nil
    }
    
    // MARK: - User Actions
    
    @IBAction func addOperator(_ sender: Any) {
        let controller: OperatorFormViewController = ViewsManager.instantiateViewController(.settings)
        controller.title = Localizable.addOperator
        controller.setup(model: AddOperatorFormViewModel())
        navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - Override
    
    override func configureDependencies() {
        setup(model: OperatorListAllViewModel())
        super.configureDependencies()
    }
    
    /// refresh the table data
    override func refresh(_ control: UIRefreshControl?) {
        loadOperators()
    }
    
    override func shouldLoadMoreRows() -> Bool {
        guard let model = model else {
            return false
        }
        
        return model.shouldLoadMoreItems()
    }
    
    override func loadMoreRows() {
        guard let model = model else { return }
        
        startLoading()
        
        model.loadMoreOperators { [weak self] error in
            self?.stopLoading()
            
            self?.tableView.reloadData()
            
            if let error = error {
                UIAlertController(error: error).show()
            }
        }
    }
    
    // MARK: - Event Handler
    
    override func handleRefresh(_ control: UIRefreshControl) {
        control.beginRefreshing()
        loadOperators(loading: false)
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model?.numberOfRowsInSection(section: section) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let model = model,
            let op = model.operatorForRow(indexPath),
            let cell = tableView.dequeueReusableCell(withIdentifier: OperatorTableViewCell.identifier) as? OperatorTableViewCell
            else {
                return UITableViewCell()
            }
    
        cell.configureCell(op)
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let model = model else { return }
        
        if let op = model.operatorForRow(indexPath) {
            showOperatorOptions(op: op)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - Internal Methods

extension OperatorTableViewController {
    func setup(model: OperatorListViewModel) {
        self.model = model
        isReady = true
    }
}

// MARK: - Private Methods

private extension OperatorTableViewController {
    func setupNotifications() {
        let notification = Notification.Name.Operator.listUpdated
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(loadOperators), name: notification, object: nil)
    }
    
    func configureTableView() {
        tableView.tableFooterView = UIView()
        let nib = UINib(nibName: OperatorTableViewCell.identifier, bundle: .main)
        tableView.register(nib, forCellReuseIdentifier: OperatorTableViewCell.identifier)
        tableView.rowHeight = 60.0
    }
    
    @objc
    func loadOperators(loading: Bool = true) {
        guard let model = model else { return }
        
        if loading {
            startLoading()
        }
        
        model.loadOperators { [weak self] error in
            self?.stopLoading()
            self?.tableView.reloadData()
            self?.refreshControl?.endRefreshing()
            self?.checkIsTableEmpty(error)
            if let error = error {
                UIAlertController(error: error).show()
            }
        }
    }
    
    /// Check if the table is Empty to show the onboard screen
    func checkIsTableEmpty(_ error: LegacyPJError?) {
        guard let model = model else { return }
        
        guard error == nil, model.numberOfRowsInSection(section: 0) == 0 else {
            tableView.tableHeaderView = nil
            return
        }
        
        let onboard = OnboardView(frame: tableView.frame)
        onboard.ilustrationImage.image = Assets.iluOperatorEmpty.image
        onboard.titleLabel.text = Localizable.operatorNotAdded
        onboard.descriptionLabel.text = ""
        onboard.setupActionButton(title: Localizable.understandHowItWorks, action: { [weak self] in
            self?.presentAboutOperatorsAlert()
        })
        
        tableView.tableHeaderView = onboard
        tableView.tableFooterView = UIView()
    }
    
    func presentAboutOperatorsAlert() {
        let confirmAction = PopupAction(title: Localizable.operatorAboutAlertConfirm, style: .fill) {
            self.addOperator(self)
        }
        
        let cancelAction = PopupAction(title: Localizable.operatorAboutAlertCancel, style: .simpleText)
        
        let popup = UI.PopupViewController(
            title: nil,
            description: Localizable.operatorAboutAlertMsg,
            image: Assets.iluOperatorPlaceholder.image
        )
        
        popup.hideCloseButton = true
        popup.addAction(confirmAction)
        popup.addAction(cancelAction)
        showPopup(popup)
    }
    
    /// Open the operator form
    func editOperator(_ op: Operator) {
        let controller: OperatorFormViewController = ViewsManager.instantiateViewController(.settings)
        let model = UpdateOperatorFormViewModel(operatorr: op)
        controller.title = Localizable.changeOperator
        controller.setup(model: model)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    /// Show the current operator transaction
    func openCurrentOperatorTransactions(_ op: Operator) {
        let controller: OperatorTransactionsViewController = ViewsManager.instantiateViewController(.settings)
        controller.model = OperatorCurrentTransactionsAllViewModel(userSeller: op)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    /// Show the history screen
    func openHistory(_ op: Operator) {
        let controller: HistoryTransactionViewController = ViewsManager.instantiateViewController(.transaction)
        controller.model = HistoryTransactionListAllViewModel(operatorr: op)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func deleteOperator(_ op: Operator) {
        let alert = UIAlertController(
            title: Localizable.operatorDeleteAlertTitle,
            message: Localizable.operatorDeleteAlertMsg,
            preferredStyle: .alert
        )
        alert.addAction(title: Localizable.cancel, style: .cancel, isEnabled: true, handler: nil)
        alert.addAction(title: Localizable.delete, style: .destructive, isEnabled: true) { [weak self] _ in
            self?.performDeleteOperator(op)
        }
        present(alert, animated: true, completion: nil)
    }
    
    func performDeleteOperator(_ op: Operator) {
        guard let id = op.id else { return }
        
        AuthManager.shared.performActionWithAuthorization { [weak self] result in
            switch result {
            case .success(let password):
                SVProgressHUD.show()
                self?.model?.deleteOperator(password: password, userSellerId: id, { [weak self] error in
                    SVProgressHUD.dismiss()
                    if let error = error {
                        UIAlertController(error: error).show()
                        return
                    }
                    
                    self?.dependencies.analytics.log(SettingsAnalytics.operatorRemoved(op.username ?? ""))
                    self?.loadOperators(loading: false)
                    // Also remove from the list of logged users
                    AuthManager.shared.removeUserAuth(username: op.username)
                })
            default:
                break
            }
        }
    }
    
    /// Show an action sheep with the operator optios
    func showOperatorOptions(op: Operator) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let transactionsAction = UIAlertAction(title: Localizable.operatorOptCurrentTransactions, style: .default) { [weak self] _ in
            self?.openCurrentOperatorTransactions(op)
        }
        let historyAction = UIAlertAction(title: Localizable.operatorOptHistory, style: .default) { [weak self] _ in
            self?.openHistory(op)
        }
        let editAction = UIAlertAction(title: Localizable.operatorOptEdit, style: .default) { [weak self] _ in
            self?.editOperator(op)
        }
        let deleteAction = UIAlertAction(title: Localizable.delete, style: .destructive) { [weak self] _ in
            self?.deleteOperator(op)
        }
        let cancelAction = UIAlertAction(title: Localizable.cancel, style: .cancel) {  _ in
        }
        actionSheet.addAction(transactionsAction)
        actionSheet.addAction(historyAction)
        actionSheet.addAction(editAction)
        actionSheet.addAction(deleteAction)
        actionSheet.addAction(cancelAction)
        
        present(actionSheet, animated: true, completion: nil)
    }
}
