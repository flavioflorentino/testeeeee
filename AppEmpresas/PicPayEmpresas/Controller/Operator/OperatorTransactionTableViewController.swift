import LegacyPJ
import UIKit

final class OperatorTransactionTableViewController: BaseTableViewController {
    enum Cell: String {
        case transaction
    }
    
    // MARK: Private Properties
    var model: OperatorTransactionListViewModel?
    var headerView: UIView?
    
    // MARK: - IB Outlet
    @IBOutlet private var totalLabel: UILabel!
    @IBOutlet private var closeCashierButton: UIPPButton!
    
    // MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        headerView = tableView.tableHeaderView
        
        configureTableView()
        loadTransactions()
    }
    
    // MARK: - User Actions
    @IBAction func showHistory(_ sender: Any) {
        if let user = AuthManager.shared.userAuth?.user {
            let op = Operator(user: user)
            
            let controller: HistoryTransactionViewController = ViewsManager.instantiateViewController(.transaction)
            controller.model = HistoryTransactionListAllViewModel(operatorr: op)
            pushViewController(controller)
        }
    }
    
    /// Close transaction (Make a history item)
    @IBAction func cashierClose(_ sender: Any) {
        let alert = UIAlertController(title: Strings.Default.operatorCashierCloseAlertTitle,
                                      message: Strings.Default.operatorCashierCloseAlertMsg,
                                      preferredStyle: .alert)
        alert.addAction(title: Strings.Default.cancel, style: .default, isEnabled: true, handler: nil)
        alert.addAction(title: Strings.Default.continue, style: .default, isEnabled: true) { [weak self] _ in
            self?.performCashierClose()
        }
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Dependence Injections
    override func configureDependencies() {
        self.setup(model: OperatorTransactionAllListViewModel())
        super.configureDependencies()
    }
    
    func setup(model: OperatorTransactionListViewModel) {
        self.model = model
        isReady = true
    }
    
    // MARK: - Internal Methods
    func configureTableView() {
        self.registerCell(Cell.transaction.rawValue, nib: "TransactionTableViewCell")
        
        tableView.rowHeight = 60.0
        tableView.backgroundColor = #colorLiteral(red: 0.95, green: 0.95, blue: 0.96, alpha: 1.0)
    }
    
    func configureView() {
        guard let model = model else { return }
        
        self.totalLabel.text = model.totalValue
        tableView.reloadData()
        tableView.allowsSelection = false
    }
    
    func loadTransactions() {
        guard let model = model else { return }
        
        startLoading()
        model.loadTransactions { [weak self] error in
            self?.configureView()
            self?.stopLoading()
            self?.refreshControl?.endRefreshing()
            self?.checkIsTableEmpty(error)
            if let error = error {
                UIAlertController(error: error).show()
            }
        }
    }
    
    override func shouldLoadMoreRows() -> Bool {
        guard let model = model else {
            return false
        }
        
        return model.shouldLoadMoreItems()
    }
    
    override func loadMoreRows() {
        guard let model = model else { return }
        
        startLoading()
        
        model.loadMoreTransactions { [weak self] error in
            self?.stopLoading()
            
            self?.configureView()
            
            if let error = error {
                UIAlertController(error: error).show()
            }
        }
    }
    
    /// Check if the table is Empty to show the onboard screen
    func checkIsTableEmpty(_ error: LegacyPJError?) {
        guard let model = model else { return }
        
        if error == nil && model.numberOfRowsInSection(section: 0) == 0 {
            tableView.tableHeaderView = TransactionEmptyListView(frame: tableView.frame)
            tableView.tableFooterView = UIView()
        } else {
            tableView.tableHeaderView = headerView
        }
    }
    
    /// Show the close action receipt
    func showCasierClose(historyItem: HistoryItem) {
        let controller: OperatorReceiptViewController = ViewsManager.instantiateViewController(.transaction)
        let model = OperatorReeiptViewModel(historyItem: historyItem)
        controller.setup(model: model)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func performCashierClose() {
        self.closeCashierButton.isEnabled = false
        self.closeCashierButton.startLoadingAnimating()
        
        AuthManager.shared.performActionWithAuthorization { [weak self] result in
            switch result {
            case .success(let password):
                
                self?.model?.chashierClose(password: password ) { [weak self] historyItem, error in
                    if let error = error {
                        UIAlertController(error: error).show()
                    } else {
                        if let historyItem = historyItem {
                            self?.showCasierClose(historyItem: historyItem)
                            self?.loadTransactions()
                        }
                    }
                    
                    self?.closeCashierButton.stopLoadingAnimating()
                    self?.closeCashierButton.isEnabled = true
                }
            default:
                self?.closeCashierButton.isEnabled = true
                self?.closeCashierButton.stopLoadingAnimating()
            }
        }
    }
    
    // MARK: Event Handler
    override func handleRefresh(_ control: UIRefreshControl) {
        control.beginRefreshing()
        loadTransactions()
    }
    
    // MARK: UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model?.numberOfRowsInSection(section: section) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let model = model,
            let transaction = model.transationForRow(indexPath),
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.transaction.rawValue) as? TransactionTableViewCell
            else {
                return UITableViewCell()
            }
        
        cell.configureCell(transaction)
        return cell
    }
}
