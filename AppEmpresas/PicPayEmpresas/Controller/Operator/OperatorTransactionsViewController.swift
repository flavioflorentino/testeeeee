import UIKit

final class OperatorTransactionsViewController: BaseTableViewController {
    enum Cell: String {
        case transaction
    }
    
    var model: OperatorCurrentTransactionsViewModel?
    
    // MARK: - IB Outlet
    @IBOutlet private var totalLabel: UILabel!
    @IBOutlet private var nameLabel: UILabel!
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        configureView()
        loadTransactions()
    }
    
    // MARK: - Internal Methods
    
    func configureTableView() {
        registerCell(Cell.transaction.rawValue, nib: "TransactionTableViewCell")
        tableView.rowHeight = 60.0
        self.tableView.tableFooterView = UIView()
    }
    
    func configureView() {
        guard let model = model else { return }
        
        // Populate title total value and operator name
        totalLabel.text = model.totalValue
        nameLabel.text = model.userSeller.username
        
        // load table view data
        tableView.reloadData()
        tableView.allowsSelection = false
    }
    
    func loadTransactions() {
        guard let model = model else { return }
        
        startLoading()
        model.loadTransactions { [weak self] error in
            self?.configureView()
            self?.stopLoading()
            self?.refreshControl?.endRefreshing()
            if let error = error {
                UIAlertController(error: error).show()
            }
        }
    }
    
    override func shouldLoadMoreRows() -> Bool {
        model?.shouldLoadMoreItems() ?? false
    }
    
    override func loadMoreRows() {
        guard let model = model else { return }
        
        startLoading()
        
        model.loadMoreTransactions { [weak self] error in
            self?.stopLoading()
            self?.configureView()
            
            if let error = error {
                UIAlertController(error: error).show()
            }
        }
    }
    
    // MARK: - User Actions
    
    // MARK: Event Handler
    
    override func handleRefresh(_ control: UIRefreshControl) {
        control.beginRefreshing()
        loadTransactions()
    }
    
    // MARK: UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let model = model else {
            return 0
        }
        
        return model.numberOfRowsInSection(section: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let model = model,
            let transaction = model.transationForRow(indexPath),
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.transaction.rawValue) as? TransactionTableViewCell
            else {
                return UITableViewCell()
            }
        
        cell.configureCell(transaction)
        return cell
    }
}
