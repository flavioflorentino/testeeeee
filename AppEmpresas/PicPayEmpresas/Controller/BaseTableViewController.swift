import SVProgressHUD
import UIKit

class BaseTableViewController: UITableViewController {
    var isReady: Bool = false
    
    lazy var loadingView: LoadingHeaderView = { [weak self] in
        LoadingHeaderView(frame: CGRect(x: 0, y: 0, width: self?.view.frame.size.width ?? 0.0, height: 80))
    }()
    
    var isPullRefresh: Bool = true
    var checkInfiteScroll: Bool = true
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if the dependencies were injected
        // if necessary call the method to configure the dependencies
        if !isReady {
            self.configureDependencies()
        }

        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        if isPullRefresh {
            refreshControl = UIRefreshControl()
            refreshControl?.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControl.Event.valueChanged)
        }

        view.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9529411765, blue: 0.9568627451, alpha: 1)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // Ensuring that SVProgress will disappear and return maskType to App's default
        SVProgressHUD.dismiss()
        SVProgressHUD.setDefaultMaskType(.black)
        super.viewWillDisappear(animated)
    }
    
    // MARK: Dependece Injection
    
    func configureDependencies() {
        isReady = true
    }
    
    // MARK: - Data Manager
    
    /// Check if the table should load more content
    func shouldLoadMoreRows() -> Bool {
        false
    }
    
    /// Load more data for table
    func loadMoreRows() {
    }
    
    /// refresh the table data
    func refresh(_ control: UIRefreshControl?) {
    }
    
    // MARK: Event Handler
    
    @objc
    func handleRefresh(_ control: UIRefreshControl) {
        self.refresh(control)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // calculates where the user is in the y-axis
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height {
            if checkInfiteScroll && self.shouldLoadMoreRows() {
                checkInfiteScroll = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: { 
                    self.loadMoreRows()
                    self.checkInfiteScroll = true
                })
            }
        }
    }
    
    // MARK: - Utils
    
    func registerCell(_ identifier: String, nib: String, bundle: Bundle = Bundle.main) {
        let nibStore = UINib(nibName: nib, bundle: bundle)
        tableView.register(nibStore, forCellReuseIdentifier: identifier)
    }
    
    func startLoading() {
        tableView.tableFooterView = self.loadingView
    }
    
    func stopLoading() {
        tableView.tableFooterView = UIView()
    }
}
