protocol LoggerProtocol {
    static func nonFatalLog(_ key: LoggerKeyProtocol)
}

protocol LoggerKeyProtocol {
    func event() -> String
}
