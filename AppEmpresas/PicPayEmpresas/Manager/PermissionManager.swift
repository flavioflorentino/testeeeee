import AVFoundation
import Foundation
import UIKit
import UserNotifications

final class PermissionManager {
    // MARK: - Singleton
    
    static let shared: PermissionManager = {
        let instance = PermissionManager()
        return instance
    }()
    
    func askRemoteNotificationPermission() {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.sound, .alert, .badge]) { _, error in
            guard error == nil else {
                return
            }
            
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func askForCameraPermission(_ completion: @escaping (Bool) -> Void) {
        AVCaptureDevice.requestAccess(for: .video) { allowedAccess -> Void in
            DispatchQueue.main.async {
                completion(allowedAccess)
            }
        }
    }
}
