import CustomerSupport
import Foundation

protocol BizCustomerSupportManagerDelegate: AnyObject {
    func setup()
    func registerUser()
    func registerNotification(sellerID: String)
    func pushFAQ(from controller: UIViewController, with seller: Seller?)
    func pushTicketList(from controller: UIViewController, with seller: Seller?)
    func presentFAQ(from controller: UIViewController, with seller: Seller?)
    func presentTicketList(from controller: UIViewController, with seller: Seller?)
}

final class BIZCustomerSupportManager: BizCustomerSupportManagerDelegate {
    var accessToken: String?
    
    init(accessToken: String? = nil) {
        self.accessToken = accessToken
    }
    
    func setup() {
        CustomerSupportManager.shared.inject(instance: CustomerSupportSDKWrapper())
        CustomerSupportManager.shared.setDebugMode(enabled: AppMode.current == .debug)
        CustomerSupportSDKWrapper().setLocalizableFile(fileName: "Localizable-Zendesk")
    }
    
    func registerUser() {
        guard let accessToken = accessToken else {
            CustomerSupportManager.shared.registerAnonymousUserCredentials()
            return
        }

        CustomerSupportManager.shared.registerUserLoggedCredentials(accessToken: accessToken)
    }
    
    func registerNotification(sellerID: String) {
        CustomerSupportSDKWrapper().register(notificationToken: sellerID)
    }

    func pushFAQ(from controller: UIViewController, with seller: Seller? = nil) {
        registerUser()
        let faqController = CustomerSupportSDKWrapper().buildFAQController()
        controller.navigationController?.pushViewController(faqController, animated: true)
    }
    
    func pushTicketList(from controller: UIViewController, with seller: Seller? = nil) {
        registerUser()
        let ticketListController = CustomerSupportSDKWrapper().buildTicketListController()
        controller.navigationController?.pushViewController(ticketListController, animated: true)
    }
    
    func presentFAQ(from controller: UIViewController, with seller: Seller? = nil) {
        registerUser()
        let faqController = CustomerSupportSDKWrapper().buildFAQController()
        let navigationController = UINavigationController(rootViewController: faqController)
        controller.present(navigationController, animated: true, completion: nil)
    }
    
    func presentTicketList(from controller: UIViewController, with seller: Seller? = nil) {
        registerUser()
        let ticketListController = CustomerSupportSDKWrapper().buildTicketListController()
        let navigationController = UINavigationController(rootViewController: ticketListController)
        controller.present(navigationController, animated: true, completion: nil)
    }
}
