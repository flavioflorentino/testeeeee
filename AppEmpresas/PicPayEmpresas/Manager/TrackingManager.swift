import AnalyticsModule
import AppsFlyerLib
import FirebaseAnalytics
import Foundation
import Mixpanel
import EventTracker
import LegacyPJ
import Core
import CoreSellerAccount
import FeatureFlag

private struct TrackingManagerKVKey: KVKeyContract {
    let description: String
}

enum TrackingManager {
    private typealias Dependencies = HasEnvVariable &
        HasKVStore &
        HasAnalytics &
        HasAuthManager &
        HasSellerInfo &
        HasFeatureManager

	// The prefix FT means that the event can be tracked once
	enum TrackingManagerEvents: String {
		case appLaunch = "App Launch"
		case ftOpen = "FT Open"
		case obRegister = "OB Register"
		case obMultRegister = "OBM Register"
		case ftLogin = "FT Login"
		case obBusinessType = "OB Business Type"
        case obMultBusinessType = "OBM Business Type"
		case ftTermsOfUse = "FT Terms Of Use"
		case obProSuggestion = "OB PRO Suggestion"
        case obMultProSuggestion = "OBM PRO Suggestion"
		case obHomeAddress = "OB Home Address"
        case obBusinessAddress = "OB Business Address"
        case obMultBusinessAddress = "OBM Business Address"
        case obMultHomeAddress = "OBM Home Address"
		case obMap = "OB Map"
        case obMultMap = "OBM Map"
		case obNameLogo = "OB Name + Logo"
        case obMultNameLogo = "OBM Name + Logo"
		case obSettedName = "OB Setted Name"
        case obMultSettedName = "OBM Setted Name"
		case obBankSelection = "OB Bank Selection"
        case obMultBankSelection = "OBM Bank Selection"
		case obBankAccountForm = "OB Bank Account Form"
        case obMultBankAccountForm = "OBM Bank Account Form"
		case obSettedBankAccount = "OB Setted Bank Account"
        case obMultDaysToWithdrawal = "OBM Days to Withdrawal"
		case obSettedDaysToWithdrawal = "OB Setted Days to Withdrawal"
        case obMultSettedDaysToWithdrawal = "OBM Setted Days to Withdrawal"
		case obPersonalData = "OB Personal Data"
        case obMultPersonalData = "OBM Personal Data"
		case obPhoneNumbers = "OB Phone Numbers"
        case obMultPhoneNumbers = "OBM Phone Numbers"
		case obSettedPhoneNumbers = "OB Setted Phone Numbers"
        case obMultSettedPhoneNumbers = "OBM Setted Phone Numbers"
		case obPassword = "OB Password"
        case obMultPassword = "OBM Password"
        case obMultSettedPassword = "OBM Setted Password"
		case ftCancelTransaction = "FT Cancel Transaction"
		case ftTransacoesTab = "FT Transacoes Tab"
		case ftTransacoesTabMonthly = "FT Transacoes Tab (Monthly)"
		case ftReceberTab = "FT Receber Tab"
        case ftCobrarTab = "FT Cobrar Tab"
		case ftExtratoTab = "FT Extrato Tab"
		case ftExtratoTabFutureMovements = "FT Extrato Tab (Future Movements)"
		case ftAjustesTab = "FT Ajustes Tab"
        case ftNotificationsTab = "FT Notificacoes Tab"
	}
	
	enum TrackingManagerProperties: String {
		case userRole = "User Role"
		case type = "Type"
		case cto = "CTO"
		case logoUpload = "Logo Upload"
		case bankId = "Bank ID"
		case numberOfDays = "Number of Days"
		case ddd = "DDD"
		case landline = "Landline"
		case openPasswordTextField = "Open Password Text Field"
	}
    
    private enum UserRole: String {
        case unlogged = "Unlogged"
        case admin = "Admin"
        case user = "User"
    }
    
    private static let container: Dependencies = DependencyContainer()
    
    private static var userRole: UserRole {
        guard container.authManager.isAuthenticated else {
            return .unlogged
        }
        
        guard let isAdmin = container.authManager.userAuth?.user.isAdmin, isAdmin else {
            return .user
        }
        
        return .admin
    }
}

// MARK: - Internal Methods

extension TrackingManager {
    static func createAliasForUser(_ userId: String) {
        let mixpanel = Mixpanel.mainInstance()
        mixpanel.createAlias(userId, distinctId: mixpanel.distinctId)
    }
    
    static func trackLogout() {
        container.analytics.reset()
    }
    
    static func setupDeviceToken(deviceToken: Data) {
        Mixpanel.mainInstance().people.addPushDeviceToken(deviceToken)
        AppsFlyerLib.shared().registerUninstall(deviceToken)
    }
    
    static func userDidLogin(_ userId: String) {
        EventTrackerManager.tracker.identify(with: .sellerId(userId))
        Mixpanel.mainInstance().identify(distinctId: userId)
        NewRelic.setUserId(userId)
        createAliasForUser(userId)
    }
    
    static func setupMixpanel(sellerID: Int? = nil) {
        guard container.authManager.isAuthenticated, let auth = container.authManager.userAuth else { return }
        
        var properties: [String: String] = [
            "user_id": String(auth.user.id ?? 0),
            "name": auth.user.name ?? "",
            "phone": auth.user.phone ?? "",
            "email": auth.user.email ?? ""
        ]
        
        guard let sellerID = sellerID else {
            setGlobal(properties: properties)
            return
        }
        
        properties["sellerID"] = String(sellerID)
        
        guard container.featureManager.isActive(.isReceivementAccountAvailable) else {
            setGlobal(properties: properties)
            setMixpanelPeople(userAuth: auth, sellerID: sellerID)
            return
        }
        
        container.sellerInfo.accountDetails { result in
            if case .success(let accountDetails) = result {
                properties["biz_account_type"] = accountDetails.bizAccountType.rawValue
            }
            setGlobal(properties: properties)
            setMixpanelPeople(userAuth: auth, sellerID: sellerID)
        }
    }
    
    static func trackAppLaunch() {
        AppsFlyerLib.shared().start()
    }
    
    static func trackFirebaseEvent(_ name: String) {
        guard AppMode.current == .debug else {
            Analytics.logEvent(name, parameters: nil)
            return
        }
        
        if container.envVariable.isAnalyticsLogEnabled {
            print("Firebase event: \(name), \("parameters: \([:])")")
        }
    }

    /// Track the event only once over the lifetime of the app.
    ///
    /// - Parameters:
    ///   - eventName: the event
    ///   - properties: event properties
    static func trackOneTimeEvent(_ eventName: TrackingManagerEvents, properties: Properties?) {
        if AppMode.current == .debug {
            if container.envVariable.isAnalyticsLogEnabled {
                print("trackOneTimeEvent:", eventName.rawValue)
            }
        } else {
            let event = TrackingManagerKVKey(description: eventName.rawValue)
            guard !container.kvStore.boolFor(event) else { return }
            TrackingManager.trackEvent(eventName, properties: properties)
            container.kvStore.set(value: true, with: event)
        }
    }
    
    static func trackEvent(_ eventName: TrackingManagerEvents, properties: Properties?) {
        if AppMode.current == .debug {
            if container.envVariable.isAnalyticsLogEnabled {
                print("trackEvent: \(eventName.rawValue) | properties: \(String(describing: properties))")
            }
        } else {
            Mixpanel.mainInstance().track(event: eventName.rawValue, properties: properties)
        }
    }
    
    /// Track the correct event for authenticated user and unloged user
    ///
    /// - Parameters:
    ///   - eventName: Event name for unloged user
    ///   - authEventName: Event name for authenticated user
    ///   - properties: event properties
    static func trackEventPublicAuth(_ eventName: TrackingManagerEvents, authEventName: TrackingManagerEvents, properties: Properties?) {
        if container.authManager.isAuthenticated {
            Mixpanel.mainInstance().track(event: authEventName.rawValue, properties: properties)
        } else {
            Mixpanel.mainInstance().track(event: eventName.rawValue, properties: properties)
        }
    }
    
    /// Update the distinct id on server
    static func updateDistinticId() {
        if container.authManager.isAuthenticated {
            let userApi = ApiUser()
            userApi.updateMixpanelId(Mixpanel.mainInstance().distinctId, { _ in })
        }
    }
    
    static func trackAppStart() {
        let trackingProperties = [TrackingManagerProperties.userRole.rawValue: userRole.rawValue]
        trackEvent(TrackingManagerEvents.appLaunch, properties: trackingProperties)
        updateDistinticId()
        trackOneTimeEvent(.ftOpen, properties: nil)
        container.analytics.log(AppAnalytics.openApp)
    }
}

// MARK: - Private Methods

private extension TrackingManager {
    static func setGlobal(properties: [String: String]) {
        container.kvStore.set(value: properties, with: BizKvKey.globalTrackValues)
        
        for provider in AnalyticsProvider.allCases {
            switch provider {
            case .mixPanel:
                Mixpanel.mainInstance().registerSuperProperties(properties)
            default:
                break
            }
        }
    }
    
    static func setMixpanelPeople(userAuth: UserAuth, sellerID: Int) {
        let mixpanel = Mixpanel.mainInstance()
        mixpanel.people.set(property: "$email", to: userAuth.user.email ?? "")
        mixpanel.people.set(property: "$phone", to: userAuth.user.phone ?? "")
        mixpanel.people.set(property: "$name", to: userAuth.user.name ?? "")
        mixpanel.people.set(property: "$id", to: userAuth.user.id ?? 0)
        mixpanel.people.set(property: "$sellerId", to: sellerID)
    }
}
