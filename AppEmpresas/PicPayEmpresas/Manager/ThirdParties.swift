import AnalyticsModule
import AppsFlyerLib
import Core
import EventTracker
import FeatureFlag
import FirebaseRemoteConfig
import FirebaseCore
import Foundation
import LegacyPJ
import Mixpanel

struct ThirdParties {
    private typealias Dependencies = HasEnvVariable &
        HasAnalytics &
        HasFeatureManager &
        HasAuthManager &
        HasKVStore
    
    private let container: Dependencies = DependencyContainer()
    
    func setup() {
        setupAppTrackingPermission()
        setupFirebase()
        #if DEBUG
        setupFeatureFlagControl()
        #endif
        setupTracking()
        setupBizConsumerSupport()
        setupRechability()
        setupLegacy()
    }
}

// MARK: - Private setup functions -
private extension ThirdParties {
    func setupFirebase() {
        let googleServicePlist: String
        if AppMode.current == .debug {
            googleServicePlist = "GoogleService-Info-Dev"
        } else {
            googleServicePlist = "GoogleService-Info"
        }
        
        let filePath = Bundle.main.path(forResource: googleServicePlist, ofType: "plist")
        if let path = filePath, let opts = FirebaseOptions(contentsOfFile: path) {
            FirebaseApp.configure(options: opts)
        }
        
        let config = RemoteConfig.remoteConfig()
        config.configSettings = RemoteConfigSettings()
        config.setDefaults(fromPlist: "FeatureToggles")
        RemoteConfigSetup.inject(instance: RemoteConfigWrapper(instance: config))
        Analytics.shared.register(tracker: FirebaseWrapper(), for: .firebase, with: FirebaseSanitizer())
        FeatureManager.shared.update()
    }
    
    func setupBizConsumerSupport() {
        BIZCustomerSupportManager().setup()
    }
    
    func setupRechability() {
        ReachabilityManager.shared.setupReachability()
    }
    
    func setupLegacy() {
        container.authManager.trackingWrapper = AuthManagerTrackingWrapper()
        setupRequestManager()
    }
    
    func setupRequestManager() {
        guard let apiURL = Environment.apiUrl else { return }
        let properties = OAuth2HandlerProperties(apiUrl: apiURL)
        
        let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        let buildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String

        let deviceToken: String?

        if container.featureManager.isActive(.isAppLocalDataUnavailable) {
            deviceToken = container.kvStore.stringFor(BizKvKey.deviceToken)
        } else {
            deviceToken = AppManager.shared.localData.deviceToken
        }
        let headers: [String: String] = [
            "app_version": versionNumber ?? "",
            "app_version_b": buildNumber ?? "",
            "device_token": deviceToken ?? ""
        ]
        RequestManager.setup(properties: properties, headers: headers, wrapper: OAuth2HandlerWrapper())
    }
    
    func setupTracking() {
        setupEventTracker()
        setupMixpanel()
        setupAppsFlyer()
        setupNewRelic()
        
        TrackingManager.trackAppStart()
    }
    
    func setupMixpanel() {
        let mixpanel = Mixpanel.initialize(token: Credentials.Mixpanel.token)
        mixpanel.loggingEnabled = container.envVariable.isAnalyticsLogEnabled

        let wrapper = MixPanelWrapper(instance: mixpanel)
        container.analytics.register(tracker: wrapper, for: .mixPanel)
    }

    func setupAppsFlyer() {
        let appsFlyer = AppsFlyerLib.shared()
        appsFlyer.appsFlyerDevKey = Credentials.AppsFlyer.devKey
        appsFlyer.appleAppID = "1208271019"
        appsFlyer.isDebug = container.envVariable.isAnalyticsLogEnabled

        if let userId = container.authManager.userAuth?.user.id {
            appsFlyer.customerUserID = "\(userId)"
        }

        let wrapper = AppsFlyerWrapper(instance: appsFlyer)
        container.analytics.register(tracker: wrapper, for: .appsFlyer)
    }
    
    func setupNewRelic() {
        if !container.envVariable.isAnalyticsLogEnabled {
            NRLogger.setLogLevels(NRLogLevelNone.rawValue)
        }

        NewRelic.disableFeatures(.NRFeatureFlag_WebViewInstrumentation)
        NewRelic.start(withApplicationToken: Credentials.NewRelic.applicationToken)
    }
    
    func setupEventTracker() {
        let apiToken = container.featureManager.isActive(.isEventTrackerProxyEnabled)
            ? Credentials.EventTracker.proxyApiToken
            : Credentials.EventTracker.apiToken

        EventTrackerManager.initialize(apiToken: .seller(apiToken))
        var tracker = EventTrackerManager.tracker
        tracker.contextPropertyProviders.append(GeoLocationContextPropertyProvider())
        tracker.contextPropertyProviders.append(AppsFlyerContextPropertyProvider())

        let wrapper = EventTrackerWrapper(instance: tracker)
        container.analytics.register(tracker: wrapper, for: .eventTracker)
    }
    
    func setupAppTrackingPermission() {
        AppTracking.requestAuthorization { status in
            guard case AppTrackingAuthorizationStatus.authorized = status else {
                AppsFlyerLib.shared().disableAdvertisingIdentifier = true
                return
            }
        }
    }

    #if DEBUG
    private func setupFeatureFlagControl() {
        if FeatureFlagControlHelper.isEnabled {
            FeatureFlagControlHelper.start()
            RemoteConfigSetup.inject(instance: FeatureFlagControl.shared.remoteConfigMock)
        }
    }
    #endif
}

private struct GeoLocationContextPropertyProvider: ContextPropertyProviding {
    private let container: HasLocationManager = DependencyContainer()
    
    var properties: [String: AnyCodable] {
        guard let location = container.locationManager.lastAvailableLocation else {
            return [:]
        }

        return [
            "latitude": .init(value: location.coordinate.latitude),
            "longitude": .init(value: location.coordinate.longitude)
        ]
    }
}

private struct AppsFlyerContextPropertyProvider: ContextPropertyProviding {
    var properties: [String: AnyCodable] {
        [
            "appsflyer_id": .init(value: AppsFlyerLib.shared().getAppsFlyerUID()),
            "advertising_id": .init(value: AppsFlyerLib.shared().advertisingIdentifier)
        ]
    }
}
