import Core
import Foundation
import Reachability
import UI
import UIKit

final class ReachabilityManager {
    static let shared = ReachabilityManager()
    
    private let reachability: Reachability? = {
        try? Reachability()
    }()
    
    func setupReachability() {
        guard let reachability = reachability else { return }
        
        reachability.whenReachable = { [weak self] _ in
            self?.sendReachabilityNotification(hasConnection: true)
        }
        
        reachability.whenUnreachable = { [weak self] _ in
            self?.sendReachabilityNotification(hasConnection: false)
        }
        
        startReachabilityNotifier()
    }
}

private extension ReachabilityManager {
    func startReachabilityNotifier() {
        try? reachability?.startNotifier()
    }
    
    func sendReachabilityNotification(hasConnection: Bool) {
        if hasConnection { return }
        showNetworkStatusView()
    }
    
    func showNetworkStatusView() {
        guard let topController = UIApplication.topViewController() else { return }
        
        let snackbar = ApolloSnackbar(text: Strings.Default.noConnection, iconType: .error, emphasis: .high)
        topController.showSnackbar(snackbar, duration: 2)
    }
}
