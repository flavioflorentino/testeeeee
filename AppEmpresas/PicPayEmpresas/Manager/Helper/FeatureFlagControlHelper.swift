import FeatureFlag
import Foundation

#if DEBUG
public enum FeatureFlagControlHelper {
    static var isEnabled: Bool {
        let isRunningTests = NSClassFromString("XCTestCase") != nil
        return !isRunningTests
    }

    static func start() {
        let dict = FeatureFlagControlHelper.dictFromFeatureTogglesPlist()
        guard !dict.isEmpty else { return }

        FeatureFlagControl.shared.buildSwitches(from: dict)
        FeatureFlagControl.shared.activateCache()
    }

    static func dictFromFeatureTogglesPlist() -> [String: Any] {
        guard let path = Bundle.main.path(forResource: "FeatureToggles", ofType: "plist") else {
            return [:]
        }
        return NSDictionary(contentsOfFile: path) as? [String: Any] ?? [:]
    }
}
#endif
