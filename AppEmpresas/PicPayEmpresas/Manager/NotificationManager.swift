import Foundation
import LegacyPJ
import UIKit
import FeatureFlag

final class NotificationManager: NSObject {
    // MARK: - Singleton
    
    static let shared = NotificationManager()
}

// MARK: - Internal Methods

extension NotificationManager {
    /// Update the notification token
    func updateToken(_ token: String) {
        if AuthManager.shared.isAuthenticated {
            let api = ApiUser()
            api.updateNotificationToken(token) { _ in }
        }
    }
}

// MARK: - Private Methods

private extension NotificationManager {
    func handleNotification(userInfo: [AnyHashable: Any]) {
        if FeatureManager.shared.isActive(.allowDeepLinkRouter) {
            handleDeeplink(userInfo: userInfo)
        }
        
        NotificationCenter.default.post(name: Notification.Name.Remote.newTransaction, object: nil)
    }
    
    func handleDeeplink(userInfo: [AnyHashable: Any]) {
        let decoder = JSONDecoder()
        let separator = "picpaybiz://picpaybiz/"
        
        guard
            let root = UIApplication.shared.delegate?.window??.rootViewController as? UINavigationController,
            let data = userInfo["data"] as? [String: Any],
            let infoData = data.toData(),
            let notification = try? decoder.decode(Message.self, from: infoData),
            let deepLink = notification.deeplink?.components(separatedBy: separator).last
            else {
                return
        }
        
        if deepLink.split(separator: "/").first == "pix" {
            let router = RouterApp.pix
            router.match(navigation: root, url: URL(string: separator + deepLink))
            return
        }
        
        let route = RouterApp(rawValue: deepLink)
        route?.match(navigation: root)
    }
}

// MARK: - UNUserNotificationCenterDelegate

extension NotificationManager: UNUserNotificationCenterDelegate {
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification,
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void
    ) {
        let userInfo = notification.request.content.userInfo
        handleNotification(userInfo: userInfo)
        completionHandler([])
    }
    
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void
    ) {
        let userInfo = response.notification.request.content.userInfo
        if isPixDeeplink(userInfo: userInfo) {
            handleNotification(userInfo: userInfo)
        }
        completionHandler()
    }
    
    private func isPixDeeplink(userInfo: [AnyHashable: Any]) -> Bool {
        if let data = userInfo["data"] as? NSDictionary,
           let deeplink = data["deeplink"] as? String,
           deeplink.contains("pix") {
                    return true
        }
        return false
    }
}
