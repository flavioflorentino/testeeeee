import UIKit

final class ViewsManager: NSObject {
    /// Instantiate a new view controler base on storyboard
    /// OBS.: The storyboard need to have a UIViewController Identifier with the same name of the class
    ///
    /// - parameter storyboard: storyboard where the the viewcontroller will be instantiate
    /// - return T: The class that called this method
    static func instantiateViewController<T: StoryboardIdentifiable>(_ storyboard: UIStoryboard.Storyboard,
                                                                     bundle: Bundle? = nil) -> T {
        let board = UIStoryboard(storyboard: storyboard, bundle: bundle)
        guard let viewController = board.instantiateViewController(withIdentifier: T.storyboardIdentifier) as? T else {
            fatalError("Couldn't instantiate view controller with identifier \(T.storyboardIdentifier) ")
        }
    
        return viewController
    }
    
    /// Instantiate a new web view controller base on url
    static func webViewController(url: String, title: String = "") -> WebViewController {
        let controller: WebViewController = ViewsManager.instantiateViewController(.main)
        controller.url = url
        controller.navigationTitle = title
        return controller
    }
}

/// Exntend the UIStoryboard to enable create a storeboard base on existent storeboard on project
extension UIStoryboard {
    enum Storyboard: String {
        case main
        case registration
        case transaction
        case settings
        case movement
        var filename: String {
            rawValue.capitalized
        }
    }
    
    /// Shortuc to enable
    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.filename, bundle: bundle)
    }
}

protocol StoryboardIdentifiable {
    static var storyboardIdentifier: String { get }
}

/// We’ve now made it so every UIViewController within our project conforms to the StoryboardIdentifiable
extension StoryboardIdentifiable where Self: UIViewController {
    static var storyboardIdentifier: String {
        String(describing: self)
    }
}

/// Make a global UIViewController conformance with StoryboardIdentifiable
extension UIViewController: StoryboardIdentifiable { }
