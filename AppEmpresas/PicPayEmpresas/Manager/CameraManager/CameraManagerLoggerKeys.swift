enum CameraManagerLoggerKeys: String, Error, LoggerKeyProtocol {
    case withoutCameraDevice = "Camera Manager - The device does not have a camera"
    case cannotAddInput = "Camera Manager - Cannot add an input"
    case containsNoInputToRemove = "Camera Manager - Contains no input to remove"
    case cannotAddOutput = "Camera Manager - Cannot add an output"
    case containsNoOutputToRemove = "Camera Manager - Contains no output to remove"
    case sessionIsAlreadyRunning = "Camera Manager - Session is already running"
    case sessionIsNotRunning = "Camera Manager - Session is not running"
    case containNoOutputs = "Camera Manager - Contain no outputs"
    case noCamerasAvailable = "Camera Manager - There is no camera available"
    case withoutData = "Camera Manager - Without data"
    case cannotCast = "Camera Manager - Can not cast"
    case cannotConvertToImage = "Camera Manager - Can not convert to image"
    case cannotFlip = "Camera Manager - Can not flip the image"
    
    var userShouldBeNotified: Bool {
        !(self == .sessionIsAlreadyRunning || self == .sessionIsNotRunning || self == .containsNoInputToRemove)
    }

    func event() -> String {
        rawValue
    }
}
