import AVFoundation
import Foundation
import UIKit

public protocol CameraManagerDelegate: AnyObject {
    func didTakePhoto(_ photo: UIImage)
    func errorWhileTryingToTakePhoto(_ error: Error)
}

public final class CameraManager: NSObject {
    // MARK: - Variables

    private var captureSession = AVCaptureSession()
    private var pictureOutput: AVCapturePhotoOutput?
    private var pictureInput: AVCaptureDeviceInput?
    private weak var delegate: CameraManagerDelegate?

    /// Know if the camera is available
    ///
    /// - Returns: A boolean indicating that the camera is available
    public var isCameraAvailable: Bool {
        AVCaptureDevice.isCameraAvailable
    }

    /// Configure camera to fill all screen
    ///
    /// - Parameter view: view to show the camera
    /// - Parameter delegate: used to communicate
    /// - Parameter position: camera to be used
    public func setupCamera(on view: UIView, delegate: CameraManagerDelegate, position: AVCaptureDevice.Position = .front) {
        self.delegate = delegate
        captureSession.sessionPreset = .photo

        setupInput(with: position)
        setupOutput()
        
        setupPreview(on: view)
        resumeCamera()
    }

    /// Configure the input
    ///
    /// - Parameter position: position of the camera that should present to user
    private func setupInput(with position: AVCaptureDevice.Position) {
        guard let captureDevice = position.device(),
            let input = try? AVCaptureDeviceInput(device: captureDevice) else {
                logAndDisplayError(.withoutCameraDevice)
                return
        }
        
        guard captureSession.canAddInput(input) else {
            logAndDisplayError(.cannotAddInput)
            return
        }
        
        captureSession.addInput(input)
        pictureInput = input
    }

    private func setupOutput() {
        let output = AVCapturePhotoOutput()
        guard captureSession.canAddOutput(output) else {
            logAndDisplayError(.cannotAddOutput)
            return
        }
        captureSession.addOutput(output)
        pictureOutput = output
    }
    
    private func removeInput() {
        guard let input = pictureInput else {
            logAndDisplayError(.containsNoInputToRemove)
            return
        }
        captureSession.removeInput(input)
    }

    private func removeOutput() {
        guard let output = pictureOutput else {
            logAndDisplayError(.containsNoOutputToRemove)
            return
        }
        captureSession.removeOutput(output)
    }

    /// Setup the Preview
    ///
    /// - Parameter controller: controller to show the camera
    private func setupPreview(on view: UIView) {
        let layer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        layer.videoGravity = .resizeAspectFill
        layer.connection?.videoOrientation = .portrait
        layer.frame = view.layer.bounds
        
        view.layer.insertSublayer(layer, at: 0)
    }
    
    /// Switch camera device
    ///
    /// - Parameter position: position of the camera that should present to user
    public func switchCamera(to postion: AVCaptureDevice.Position) {
        removeInput()
        setupInput(with: postion)
    }

    public func resumeCamera() {
        guard isCameraAvailable else {
            logAndDisplayError(.noCamerasAvailable)
            return
        }
        guard !captureSession.isRunning else {
            return
        }
        captureSession.startRunning()
    }

    public func stopCamera() {
        guard isCameraAvailable else {
            logAndDisplayError(.noCamerasAvailable)
            return
        }
        guard captureSession.isRunning else {
            return
        }
        captureSession.stopRunning()
    }

    public func takePicture() {
        guard self.isCameraAvailable else {
            self.logAndDisplayError(.noCamerasAvailable)
            return
        }
        guard self.captureSession.isRunning else {
            self.logAndDisplayError(.sessionIsNotRunning)
            return
        }
        guard self.captureSession.outputs.isNotEmpty else {
            self.logAndDisplayError(.containNoOutputs)
            self.setupOutput()
            return
        }
        
        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecJPEG])
        self.pictureOutput?.capturePhoto(with: settings, delegate: self)
    }
}

// MARK: - AVCaptureMetadataOutputObjectsDelegate
extension CameraManager: AVCapturePhotoCaptureDelegate {
    @available(iOS 11.0, *)
    public func photoOutput(
        _ output: AVCapturePhotoOutput,
        didFinishProcessingPhoto photo: AVCapturePhoto,
        error: Error?
    ) {
        guard let data = photo.fileDataRepresentation() else {
            self.logAndDisplayError(.withoutData)
            return
        }
        
        self.output(with: data)
    }
    
    // swiftlint:disable function_parameter_count
    public func photoOutput(
        _ output: AVCapturePhotoOutput,
        didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?,
        previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?,
        resolvedSettings: AVCaptureResolvedPhotoSettings,
        bracketSettings: AVCaptureBracketedStillImageSettings?,
        error: Error?
    ) {
        guard
            let buffer = photoSampleBuffer,
            let data = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: buffer, previewPhotoSampleBuffer: nil)
            else {
                self.logAndDisplayError(.cannotCast)
                return
        }

        self.output(with: data)
    }
    
    private func flipPhoto(_ data: Data) -> UIImage? {
        guard
            let image = UIImage(data: data),
            let cgImage = image.cgImage
            else {
                self.logAndDisplayError(.cannotFlip)
                return nil
        }
        
        let ciImage = CIImage(cgImage: cgImage).oriented(forExifOrientation: 6)
        let flippedImage = ciImage.transformed(by: CGAffineTransform(scaleX: -1, y: 1))
        
        return convertToImage(flippedImage)
    }
    
    private func convertToImage(_ image: CIImage) -> UIImage? {
        let context = CIContext(options: nil)
        guard
            let cgImage = context.createCGImage(image, from: image.extent)
            else {
                self.logAndDisplayError(.cannotConvertToImage)
                return nil
        }
        return UIImage(cgImage: cgImage)
    }
    
    private func output(with data: Data) {
        guard let image = flipPhoto(data) else {
            self.logAndDisplayError(.cannotFlip)
            return
        }
        delegate?.didTakePhoto(image)
        stopCamera()
    }
}

private extension CameraManager {
    func logAndDisplayError(_ error: CameraManagerLoggerKeys) {
        if error.userShouldBeNotified {
            delegate?.errorWhileTryingToTakePhoto(error)
        }
    }
}

private extension AVCaptureDevice {
    static var isCameraAvailable: Bool {
        let hasFrontDevice = Position.front.device() != nil
        let hasBackDevice = Position.back.device() != nil
        let hasDevice = hasFrontDevice || hasBackDevice
        return hasDevice ? true : false
    }
}

private extension AVCaptureDevice.Position {
    func device() -> AVCaptureDevice? {
        guard self != .unspecified else {
            return nil
        }
        return AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: self)
    }
}
