import LegacyPJ
import UIKit
import FeatureFlag

protocol AppManagerProtocol: AnyObject {
    var localData: AppManager.AppLocalData { get set }

    func initialRootViewController() -> UIViewController
    func presentAuthenticatedViewController()
    func presentUnloggedViewController()
    func logoutUserForInvalidToken()
}

@available(*, deprecated, message: "Use AppCoordinator instead")
final class AppManager: AppManagerProtocol {
    // MARK: - Internal Data Struct

    @available(*, deprecated, message: "Use KVStore with BizKvKey instead")
    struct AppLocalData {
        /// Token Device
        var deviceToken: String {
            get {
                if let deviceId = UserDefaults.standard.string(forKey: "deviceID") {
                    return deviceId
                }
                return ""
            }
            set(value) {
                UserDefaults.standard.set(value, forKey: "deviceID")
            }
        }
        
        /// Last phone sent for validation
        var phoneValidationNumber: String {
            get {
                UserDefaults.standard.string(forKey: "phoneValidationNumber") ?? ""
            }
            set(value) {
                UserDefaults.standard.set(value, forKey: "phoneValidationNumber")
            }
        }
        
        /// Flag for show signup promo code popup
        var needShowStartPromoCodePopup: Bool {
            get {
                UserDefaults.standard.bool(forKey: "needShowStartPromoCodePopup")
            }
            set(value) {
                UserDefaults.standard.set(value, forKey: "needShowStartPromoCodePopup")
            }
        }
    }

    private var mustPresentBiometry: Bool {
        FeatureManager.shared.isActive(.featureFacialBiometry)
            && AuthManager.shared.userAuth?.auth.biometry != .accepted
            && AuthManager.shared.isAuthenticated
    }
    
    // MARK: - Singleton
    
    static let shared: AppManagerProtocol = {
        let instance = AppManager()
        return instance
    }()
    
    // MARK: - Public Local Data
    
    var localData: AppLocalData
    
    init() {
        self.localData = AppLocalData()
        if self.localData.deviceToken.isEmpty {
            localData.deviceToken = String.random(ofLength: 20)
        }
    }
        
    // MARK: - Public Methods
    
    /// Load the corret initial root view controller according to current user session context
    func initialRootViewController() -> UIViewController {
        if mustPresentBiometry {
            return facialBiometryController()
        }

        if AuthManager.shared.isAuthenticated {
            let controller = MainTabBarController(container: DependencyContainer())
            let navigation = UINavigationController(rootViewController: controller)
            navigation.setNavigationBarHidden(true, animated: false)
            return navigation
        }

        let controller = UnloggedFactory.make(isRoot: true)
        let navigation = UINavigationController(rootViewController: controller)
        return navigation
    }
    
    /// Change the root view controller to first autenticated view controller
    func presentAuthenticatedViewController() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }

        if mustPresentBiometry {
            appDelegate.changeRootViewController(facialBiometryController())
            return
        }

        let controller = MainTabBarController(container: DependencyContainer())
        appDelegate.changeRootViewController(controller)
    }
    
    /// Change the root view controller to first unlogged view controller
    func presentUnloggedViewController() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let controller: UnloggedViewController = UnloggedFactory.make(isRoot: true)
            let navigation = UINavigationController(rootViewController: controller)
            appDelegate.changeRootViewController(navigation)
        }
    }
    
    func logoutUserForInvalidToken() {
        presentUnloggedViewController()
    }

    // MARK: - Private Methods

    private func facialBiometryController() -> UIViewController {
        let controller = FacialBiometricsInitialFactory.make()
        let navigation = UINavigationController(rootViewController: controller)
        navigation.setNavigationBarHidden(true, animated: false)
        return navigation
    }
}
