import LegacyPJ
import UIKit
import FeatureFlag

protocol AppCoordinating: AnyObject {
    func displayRoot()
    func displayAuthenticated()
    func displayWelcome()
    func displayAccountAddition(with navigation: UINavigationController)
}

final class AppCoordinator: AppCoordinating {
    // MARK: - Private vars

    // MARK: Validations
    private var mustPresentBiometry: Bool {
        FeatureManager.shared.isActive(.featureFacialBiometry)
            && AuthManager.shared.userAuth?.auth.biometry != .accepted
            && AuthManager.shared.isAuthenticated
    }
    private var mustPresentAuthenticated: Bool {
        AuthManager.shared.isAuthenticated
    }

    // MARK: Helpers
    private var appDelegate: AppDelegate? {
        UIApplication.shared.delegate as? AppDelegate
    }

    // MARK: Instances
    private lazy var welcomeController = UnloggedFactory.make(isRoot: false)
    private lazy var welcomeRootNavigationController = UINavigationController(rootViewController: UnloggedFactory.make(isRoot: true))
    private lazy var mainTabBarController: UIViewController = {
        let controller = MainTabBarController(container: DependencyContainer())
        let navigation = UINavigationController(rootViewController: controller)
        navigation.setNavigationBarHidden(true, animated: false)
        return navigation
    }()
    private lazy var facialBiometryController: UIViewController = {
        let controller = FacialBiometricsInitialFactory.make()
        let navigation = UINavigationController(rootViewController: controller)
        navigation.setNavigationBarHidden(true, animated: false)
        return navigation
    }()

    // MARK: - Internal methods
    func displayRoot() {
        if mustPresentBiometry {
            appDelegate?.window?.rootViewController = facialBiometryController
        } else if mustPresentAuthenticated {
            appDelegate?.window?.rootViewController = mainTabBarController
        } else {
            appDelegate?.window?.rootViewController = welcomeRootNavigationController
        }
    }

    func displayAuthenticated() {
        if mustPresentBiometry {
            appDelegate?.changeRootViewController(facialBiometryController)
            return
        }

        appDelegate?.changeRootViewController(mainTabBarController)
    }

    func displayWelcome() {
        appDelegate?.changeRootViewController(welcomeRootNavigationController)
    }

    func displayAccountAddition(with navigation: UINavigationController) {
        let controller = welcomeController
        controller.hidesBottomBarWhenPushed = true
        navigation.pushViewController(controller, animated: true)
    }
}
