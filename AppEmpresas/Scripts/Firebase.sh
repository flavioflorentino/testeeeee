PATH_TO_GOOGLE_PLISTS="${PROJECT_DIR}/PicPayEmpresas/Supporting Files"
NEW_PLIST_PATH="${BUILT_PRODUCTS_DIR}/${PRODUCT_NAME}.app/GoogleService-Info.plist"

case "${CONFIGURATION}" in
   "Debug" | "Homolog" )
        cp -v -f "$PATH_TO_GOOGLE_PLISTS/GoogleService-Info-Dev.plist" "${NEW_PLIST_PATH}" ;;

   "Release")
        cp -v -f "$PATH_TO_GOOGLE_PLISTS/GoogleService-Info-Release.plist" "${NEW_PLIST_PATH}" ;;
    *)
        ;;
esac
