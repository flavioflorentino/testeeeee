SCRIPT=`/usr/bin/find "${SRCROOT}/.." -name newrelic_postbuild.sh | head -n 1`

if [[ $CONFIGURATION == 'Release' ]]; then
    echo "PicPay Release"
    /bin/sh "${SCRIPT}" "AAefe2fa83d81a146444df1de747aa2670e19afc07-NRMA"
else
    echo "PicPay Debug | Homolog"
    /bin/sh "${SCRIPT}" "AAd425e054e59ad14eb7f4fd3d6bf381be40902067-NRMA"
fi
