<!--- This is a history changelog prior to git generated changelog. -->
<!--- !!! DO NOT USE THIS FILE !!! -->

### Added
- **HouseKeeping:** [JIRA-XXX] Inserção do AnalyticsModule ([#478](https://github.com/PicPay/business-ios/issues/478))
- **engajamento_pj:** [ENPJ-87] - Layout da tela de tipo campanha ([#480](https://github.com/PicPay/business-ios/issues/480))
- **engajamento_pj:** [ENPJ-82] Implementa a estrutura pra servico de obter campanhas ([#475](https://github.com/PicPay/business-ios/issues/475))

### Changed
- **ativaçaoPJ:** [B2B-695] Ajustando trackings do cadastro ([#486](https://github.com/PicPay/business-ios/issues/486))
- **ativaçaoPJ:** [B2B-741] Mudando background color no cadastro ([#485](https://github.com/PicPay/business-ios/issues/485))
- **enterprise_security:** [ES-65] 2 Factor Authentication - Adição de logs e correções ([#489](https://github.com/PicPay/business-ios/issues/489))

### Fixed
- **Transactions:** [TRAN-42] Fix Loading do Formulário de Pagamento de Boleto ([#484](https://github.com/PicPay/business-ios/issues/484))


<a name="v1.2.11"></a>
## [v1.2.11] - 2020-06-30
### Added
- **Ativacao:** [B2B-573] Adicionando fluxo de solicitação de material ([#426](https://github.com/PicPay/business-ios/issues/426))
- **ContaPJ:** [COPJ-508] Ajuste de design do componente de wallet ([#481](https://github.com/PicPay/business-ios/issues/481))
- **ContaPJ:** [COPJ-505] Exportando Extrato ([#482](https://github.com/PicPay/business-ios/issues/482))
- **ContaPJ:** [COPJ-505] Correção no Header de Extrato e novo status ([#483](https://github.com/PicPay/business-ios/issues/483))
- **ContaPJ:** [COPJ-505] Integrando carregamento inicial de extrato ([#479](https://github.com/PicPay/business-ios/issues/479))
- **empresas:** [B2B-669] Refatoração da tela de telefone usada no cadastro ([#466](https://github.com/PicPay/business-ios/issues/466))
- **empresas:** [B2B-668] Finalizando a refatoração da tela de informações pessoais ([#383](https://github.com/PicPay/business-ios/issues/383))

### Changed
- **empresas:** [B2B-700] Altera o fluxo do onboard ([#409](https://github.com/PicPay/business-ios/issues/409))


<a name="v1.2.10"></a>
## [v1.2.10] - 2020-06-23
### Added
- **ContaPJ:** [COPJ-479] Integrando componentes de Extrato ([#454](https://github.com/PicPay/business-ios/issues/454))
- **ContaPJ:** [COPJ-488] Header do Extrato com saldo disponível ([#459](https://github.com/PicPay/business-ios/issues/459))
- **Housekeeping:** Unifica os mocks dos controllers ([#441](https://github.com/PicPay/business-ios/issues/441))
- **Housekeeping:** Corrigindo Footer da tela de notificações ([#456](https://github.com/PicPay/business-ios/issues/456))
- **engajamento_pj:** [ENPJ-82] layout tela inicial do fluxo de criar campanha ([#453](https://github.com/PicPay/business-ios/issues/453))
- **enterprise_security:** [ES-65] 2 Factor Authentication - Part 5 ([#461](https://github.com/PicPay/business-ios/issues/461))
- **enterprise_security:** [ES-65] 2 Factor Authentication - Part 4 ([#460](https://github.com/PicPay/business-ios/issues/460))
- **enterprise_security:** [ES-65] 2 Factor Authentication - Ultimo ([#468](https://github.com/PicPay/business-ios/issues/468))
- **enterprise_security:** [ES-65] 2 Factor Authentication - Part 6 ([#462](https://github.com/PicPay/business-ios/issues/462))

### Fixed
- **ExpAtendimento:** [XPAT-XXX] Arruma as chamadas do CustomerSupportManager ([#476](https://github.com/PicPay/business-ios/issues/476))
- **ExpAtendimento:** [XPAT-XXX] Adiciona a nova função do contrato de dependência do CustomerSupportModule ([#474](https://github.com/PicPay/business-ios/issues/474))
- **ExpAtendimento:** [XPAT-216] Arruma campos customizados dos tickets ([#464](https://github.com/PicPay/business-ios/issues/464))
- **ExpAtendimento:** [XPAT-216] Arruma as chamadas que foram alteradas no verdinho ([#463](https://github.com/PicPay/business-ios/issues/463))


<a name="v1.2.9"></a>
## [v1.2.9] - 2020-06-16
### Added
- **ContaPJ:** [COPJ-488] Implementação da nova header view de Lançamentos Futuros ([#449](https://github.com/PicPay/business-ios/issues/449))
- **ContaPJ:** [COPJ-489] Layout do Header de seção do Extrato ([#446](https://github.com/PicPay/business-ios/issues/446))
- **Housekeeping:** Removendo SellerID vazio ([#442](https://github.com/PicPay/business-ios/issues/442))
- **Transactions:** [TRAN-16] Passagem de Código para a Próxima Tela e Testes do Presenter ([#438](https://github.com/PicPay/business-ios/issues/438))
- **empresas:** [TRAN-16] Adicionando Verificador do código de barras e testes do ViewModel ([#434](https://github.com/PicPay/business-ios/issues/434))
- **empresas:** [ES-65] 2 Factor Authentication - Part3 ([#443](https://github.com/PicPay/business-ios/issues/443))

### Fixed
- **ativaçãoPJ:** [B2B-740] Arruma o SellerID que é enviado para o MixPanel ([#451](https://github.com/PicPay/business-ios/issues/451))
- **enterprise_security:** Ajuste no fluxo de Biometria Facial ([#448](https://github.com/PicPay/business-ios/issues/448))


<a name="v1.2.8"></a>
## [v1.2.8] - 2020-06-10
### Added
- **ContaPJ:** [COPJ-479] Adicionando estrutura básica de extrato ([#432](https://github.com/PicPay/business-ios/issues/432))
- **ContaPJ:** [COPJ-486] Criando Layout de célula de Extrato ([#440](https://github.com/PicPay/business-ios/issues/440))
- **ContaPJ:** [COPJ-479] Adicionando FeatureToggle do Novo Extrato ([#439](https://github.com/PicPay/business-ios/issues/439))
- **ContaPJ:** [COPJ-473] Atualizando textos do TransactionCarousel ([#430](https://github.com/PicPay/business-ios/issues/430))
- **ContaPJ:** [COPJ-483] Corrigindo bug no PersonalDataViewController ([#429](https://github.com/PicPay/business-ios/issues/429))
- **ContaPJ:** [COPJ-479] Novo estilo de NavigationBar ([#433](https://github.com/PicPay/business-ios/issues/433))
- **ExpAtendimento:** [XPAT-112] Atualiza os textos do SDK do Zendesk ([#436](https://github.com/PicPay/business-ios/issues/436))
- **Housekeeping:** Corrigindo warnings ([#437](https://github.com/PicPay/business-ios/issues/437))
- **Housekeeping:** Mergeando as extensões de UIImage ([#424](https://github.com/PicPay/business-ios/issues/424))
- **Housekeeping:** Removendo pinEdges ([#420](https://github.com/PicPay/business-ios/issues/420))
- **empresas:** [TRAN-16] Adicionando View de Scanner no ScannerViewController  ([#419](https://github.com/PicPay/business-ios/issues/419))
- **empresas:** [COPJ-466] Nova opção para ocultar valor do Saldo na Home ([#402](https://github.com/PicPay/business-ios/issues/402))

### Changed
- **empresas:** Ajuste do StorageManager ([#423](https://github.com/PicPay/business-ios/issues/423))

### Fixed
- **ExpAtendimento:** [XPAT-093] Arruma o registro de notificação  ([#425](https://github.com/PicPay/business-ios/issues/425))
- **empresas:** Hide TabBar when pushing View Controller ([#427](https://github.com/PicPay/business-ios/issues/427))


<a name="v1.2.7"></a>
## [v1.2.7] - 2020-06-03
### Added
- **ExpAtendimento:** Adiciona o Zendesk no BIZ ([#412](https://github.com/PicPay/business-ios/issues/412))
- **Housekeeping:** Removendo TODO antigos ([#405](https://github.com/PicPay/business-ios/issues/405))
- **Housekeeping:** Limpando Helpshift ([#397](https://github.com/PicPay/business-ios/issues/397))
- **Housekeeping:** Renomeando GoogleService.plist ([#403](https://github.com/PicPay/business-ios/issues/403))
- **empresas:** [TRAN-16] Testes Unitários da Tela de Opções de Pagamento ([#414](https://github.com/PicPay/business-ios/issues/414))
- **empresas:** [ES-65] 2 Factor Authentication - Part 2 ([#406](https://github.com/PicPay/business-ios/issues/406))
- **empresas:** [B2B-679] Cria tela de escolha de escolha se tem ponto fixo ([#404](https://github.com/PicPay/business-ios/issues/404))
- **empresas:** [TRAN-16] Adicionando Tela de Opções de Pagamento de Boleto ([#394](https://github.com/PicPay/business-ios/issues/394))
- **empresas:** [TRAN-16] Arquivos da Scene da Tela de Scanner ([#399](https://github.com/PicPay/business-ios/issues/399))
- **empresas:** [ES-65] 2 Factor Authentication - Part 1 ([#395](https://github.com/PicPay/business-ios/issues/395))

### Changed
- **plataforma:** [PLAIOS-167] altera como que é a nomenclatura do build number. ([#415](https://github.com/PicPay/business-ios/issues/415))

### Fixed
- **Empresas:** Tratando notificação do Helpshift conforme a documentação ([#418](https://github.com/PicPay/business-ios/issues/418))
- **Empresas:** Removendo NavBar oculta quando deslogado ([#410](https://github.com/PicPay/business-ios/issues/410))
- **Empresas:** Corrigindo Constraints de Escolha de CNPJ ([#408](https://github.com/PicPay/business-ios/issues/408))
- **empresas:** Removido template de pull request da raiz do projeto ([#400](https://github.com/PicPay/business-ios/issues/400))
- **plataforma:** Correção migração keychain [#411](https://github.com/PicPay/business-ios/issues/411)
- **plataforma:** Correção migração keychain

### 

- Ao clicar deve abrir uma tela de FAQ 
- No menu de ajustes na opção "Perguntas Frequentes"

- Ao clicar deve abrir uma tela de FAQ 
- No menu de ajustes na opção "Fale Conosco"

- Ao clicar deve abrir uma tela de Tickets 

# Outras observações:

<!--- Outras observações relevantes que não se encaixam nos campos acima -->


<a name="v1.2.4"></a>
## [v1.2.4] - 2020-05-27
### Added
- **Housekeeping:** Corrigindo Warning de Notificações ([#378](https://github.com/PicPay/business-ios/issues/378))
- **Housekeeping:** Limpando RequestManager ([#391](https://github.com/PicPay/business-ios/issues/391))
- **Housekeeping:** Removendo GoogleService-Info.plist do target ([#390](https://github.com/PicPay/business-ios/issues/390))
- **Housekeeping:** Removendo NSLayoutContraint+Extensions ([#374](https://github.com/PicPay/business-ios/issues/374))
- **Housekeeping:** Corrigindo Constraints da UnloggedViewController ([#388](https://github.com/PicPay/business-ios/issues/388))
- **Housekeeping:** Corrigindo erros do Lint ([#392](https://github.com/PicPay/business-ios/issues/392))
- **Housekeeping:** Limpando AppDelegate ([#382](https://github.com/PicPay/business-ios/issues/382))
- **Housekeeping:** Removendo Warnings ([#369](https://github.com/PicPay/business-ios/issues/369))
- **Housekeeping:** Tratando notificações com UNUserNotificationCenterDelegate ([#381](https://github.com/PicPay/business-ios/issues/381))
- **Housekeeping:** Usando SnapKit em Views antigas ([#379](https://github.com/PicPay/business-ios/issues/379))
- **Housekeeping:** Removendo TODO de Saque ([#373](https://github.com/PicPay/business-ios/issues/373))
- **empresas:** [COPJ-441] Eventos restantes de boleto ([#389](https://github.com/PicPay/business-ios/issues/389))
- **empresas:** [B2B-638] Adiciona valor no tracking de compartilhamento de cobrança ([#370](https://github.com/PicPay/business-ios/issues/370))
- **empresas:** [COPJ-424] Testes para a tela de input de valor de boleto ([#384](https://github.com/PicPay/business-ios/issues/384))
- **empresas:** Criando Variáveis de Scheme ([#360](https://github.com/PicPay/business-ios/issues/360))
- **empresas:** [B2B-674] Refatoração da RegistrationIdentifyProfileViewController ([#337](https://github.com/PicPay/business-ios/issues/337))
- **housekeeping:** Ajustando tamanho da AlertModalView ([#368](https://github.com/PicPay/business-ios/issues/368))
- **housekeeping:** Removendo BaseResponse ([#352](https://github.com/PicPay/business-ios/issues/352))
- **plataforma:** [PLAIOS-162] adiciona New Relic no aplicativo ([#387](https://github.com/PicPay/business-ios/issues/387))

### Changed
- **APPSEC:** [APPSEC-306] Retornando o código do pinning ([#380](https://github.com/PicPay/business-ios/issues/380))
- **APPSEC:** [APPSEC-303] Substituição do pod KeychainAccess por uma solução nossa ([#355](https://github.com/PicPay/business-ios/issues/355))
- **empresas:** [B2B-587] Adicionando erro para o CameraManager ([#366](https://github.com/PicPay/business-ios/issues/366))
- **plataforma:** atualização Cocoapods ([#393](https://github.com/PicPay/business-ios/issues/393))

### Fixed
- **empresas:** Bill Module Rename ([#386](https://github.com/PicPay/business-ios/issues/386))
- **empresas:** Adicionado senha no Pagamento de Boleto ([#372](https://github.com/PicPay/business-ios/issues/372))

### Partial
- **empresas:** [B2B-668] Implementando o service da refatoração do RegistrationPersonalStepViewController ([#359](https://github.com/PicPay/business-ios/issues/359))
- **empresas:** [B2B-668] Refatoração da tela de informações pessoais ([#350](https://github.com/PicPay/business-ios/issues/350))


<a name="v1.2.3"></a>
## [v1.2.3] - 2020-05-13
### Added
- **Housekeeping:** Usando SnapKit em telas antigas ([#342](https://github.com/PicPay/business-ios/issues/342))
- **Housekeeping:** Removendo Development App Mode ([#343](https://github.com/PicPay/business-ios/issues/343))
- **Housekeeping:** Removendo xib de PopUpViewController ([#341](https://github.com/PicPay/business-ios/issues/341))
- **empresas:** [B2B-587] Adicionando testes para cena biometria facial checking ([#313](https://github.com/PicPay/business-ios/issues/313))
- **empresas:** [ENPJ-55] Adicionando Cobrança sem Valor Prefixado ([#351](https://github.com/PicPay/business-ios/issues/351))
- **empresas:** [B2B-587] Adicionando testes para cena biometria facial initial ([#314](https://github.com/PicPay/business-ios/issues/314))
- **empresas:** [COPJ-435] Ajustes na integracao da leitura de boleto ([#346](https://github.com/PicPay/business-ios/issues/346))
- **empresas:** [COPJ-259] Serviço Pagamento de Boleto ([#345](https://github.com/PicPay/business-ios/issues/345))
- **empresas:** [B2B-587] Adicionando testes para cena biometria facial tip ([#318](https://github.com/PicPay/business-ios/issues/318))
- **empresas:** [B2B-587] Adicionando testes para cena biometria facial take ([#317](https://github.com/PicPay/business-ios/issues/317))
- **empresas:** [B2B-587] Adicionando testes para cena biometria facial status ([#316](https://github.com/PicPay/business-ios/issues/316))
- **empresas:** [B2B-587] Adicionando testes para cena biometria facial main ([#315](https://github.com/PicPay/business-ios/issues/315))
- **housekeeping:** Removing Snapkit ([#356](https://github.com/PicPay/business-ios/issues/356))
- **plataforma:** adicionado arquivo de configuração do kodiak [#348](https://github.com/PicPay/business-ios/issues/348)

### Fixed
- **empresas:** Sincroniza uso de classe de mascara e alinhamento de texto ([#349](https://github.com/PicPay/business-ios/issues/349))
- **empresas:** [B2B-650] Cria empty state para central de notificações ([#323](https://github.com/PicPay/business-ios/issues/323))

### Partial
- **empresas:** [COPJ-257] - Analytics fluxo de pagamento boleto ([#363](https://github.com/PicPay/business-ios/issues/363))
- **empresas:** [COPJ-440] Transaction Carousel Skip Modal ([#362](https://github.com/PicPay/business-ios/issues/362))
- **empresas:** Ajustes de layout no preenchimento do boleto ([#361](https://github.com/PicPay/business-ios/issues/361))
- **empresas:** [COPJ-407] Ajustes gerais e tela de erro ([#358](https://github.com/PicPay/business-ios/issues/358))
- **empresas:** Integrating Billet Scenes ([#357](https://github.com/PicPay/business-ios/issues/357))
- **empresas:** [COPJ-407] Integração Serviço de Boleto ([#347](https://github.com/PicPay/business-ios/issues/347))
- **empresas:** [COPJ-416] Tansaction Carousel Biometry ([#338](https://github.com/PicPay/business-ios/issues/338))
- **empresas:** [COPJ-392] Implementa serviço de verificação de boleto ([#306](https://github.com/PicPay/business-ios/issues/306))
- **empresas:** Navegação do carrossel da Home ([#333](https://github.com/PicPay/business-ios/issues/333))
- **empresas:** [B2B-256] Layout célula de boleto na Home ([#332](https://github.com/PicPay/business-ios/issues/332))

### 

<img src="https://user-images.githubusercontent.com/13156884/81764529-2a1a3a80-94a8-11ea-8da5-aa3d3f91a998.png" width="200" height="400" />

<img src="https://user-images.githubusercontent.com/13156884/81764541-30101b80-94a8-11ea-990a-d0a1b29408bb.png" width="200" height="400" />
</p>





## Type of change

<!--- Please delete options that are not relevant. -->

- New feature (non-breaking change which adds functionality)

# Checklist:

- [x] My code follows the style guidelines of this project (Swift Lint)
- [x] My changes generate no new warnings
- [x] I have added tests that prove my fix is effective or that my feature works
- [x] I have performed a self exploratory manual test 
- [x] The changes passed in the exploratory manual test (Q.A.)


<a name="v1.2.2"></a>
## [v1.2.2] - 2020-05-06
### Added
- **empresas:** [ENPJ-51] Altera o QR code da tela Cobrar para o mesmo código da impressão ([#324](https://github.com/PicPay/business-ios/issues/324))
- **empresas:** [B2B-587] Adiciona o ponto de entrada para o fluxo de biometria facial ([#295](https://github.com/PicPay/business-ios/issues/295))
- **empresas:** Adding badges for transaction carousel ([#300](https://github.com/PicPay/business-ios/issues/300))
- **empresas:** Nova Modal Genérica ([#297](https://github.com/PicPay/business-ios/issues/297))

### Fixed
- **plataforma:** remoção do keychain junto com pinning por motivo de crash em algumas versões [#326](https://github.com/PicPay/business-ios/issues/326)
- **plataforma:** Correção no corrido do feature manager [#304](https://github.com/PicPay/business-ios/issues/304)


<a name="v1.2.1"></a>
## [v1.2.1] - 2020-05-05
### Fixed
- **plataforma:** remoção do keychain junto com pinning por motivo de crash em algumas versões [#325](https://github.com/PicPay/business-ios/issues/325)


<a name="v1.2.0"></a>
## [v1.2.0] - 2020-04-29
### Added
- **Empresas:** [B2B-613] Implementa a chamada para fazer o match entre CPF e CNPJ ([#230](https://github.com/PicPay/business-ios/issues/230))
- **empresas:** Removendo TODO de WithdrawalOptions ([#289](https://github.com/PicPay/business-ios/issues/289))
- **empresas:**  [COPJ-413] - Tela input valor boleto ([#284](https://github.com/PicPay/business-ios/issues/284))
- **empresas:** Removing warnings ([#277](https://github.com/PicPay/business-ios/issues/277))
- **empresas:** [B2B-623] Adiciona redirecionamento no Login caso necessite completar sign up ou validar a biometria ([#254](https://github.com/PicPay/business-ios/issues/254))
- **empresas:** Cleaning AppDelegate warnings ([#276](https://github.com/PicPay/business-ios/issues/276))

### Changed
- **APPSEC:** [APPSEC-278] Implementação do pinning e adição dos certificados. ([#270](https://github.com/PicPay/business-ios/issues/270))
- **plataforma:** [PLAIOS-92] trocando script swift de upload de Changelog pelo script em python [#280](https://github.com/PicPay/business-ios/issues/280)

### Fixed
- **empresas:** [B2B-646] Adiciona fluxo de erro na impressão de material ([#294](https://github.com/PicPay/business-ios/issues/294))
- **empresas:** [B2B-647] Adiciona load na impressão de material ([#290](https://github.com/PicPay/business-ios/issues/290))
- **empresas:** [B2B-631] Adicionando load na tela de notificações ([#288](https://github.com/PicPay/business-ios/issues/288))
- **empresas:** [ENPJ-21] Adicionando load na tela de valor ([#281](https://github.com/PicPay/business-ios/issues/281))
- **empresas:** [B2B-xxx] Ajuste da tabBar quando usuário é operador ([#265](https://github.com/PicPay/business-ios/issues/265))
- **plataforma:** Correção no corrido do feature manager [#303](https://github.com/PicPay/business-ios/issues/303)

### Partial
- **empresas:** [COPJ-403] Transaction Carousel Tests ([#275](https://github.com/PicPay/business-ios/issues/275))
- **empresas:** [COPJ-253] Layout de conferência de boleto ([#274](https://github.com/PicPay/business-ios/issues/274))
- **empresas:** [COPJ-254] Layout tela de recibo de pagamento de boleto ([#268](https://github.com/PicPay/business-ios/issues/268))


<a name="v1.1.18"></a>
## [v1.1.18] - 2020-04-22
### Added
- **empresas:** [ENPJ-21] Fluxo novo de cobrar ([#262](https://github.com/PicPay/business-ios/issues/262))
- **empresas:** Replacing class with AnyObject ([#247](https://github.com/PicPay/business-ios/issues/247))

### Fixed
- **empresas:** [ENPJ-21] Ajustando a cor da seleção do botão da tabBar  ([#264](https://github.com/PicPay/business-ios/issues/264))
- **empresas:** B2B Ajustando nome do titulo da tela de Notificações ([#239](https://github.com/PicPay/business-ios/issues/239))

### Partial
- **empresas:** [ENPJ-21] Testes Unitarios da tela de input de valor ([#263](https://github.com/PicPay/business-ios/issues/263))
- **empresas:** [ENPJ-21] Tracking Analytics ([#261](https://github.com/PicPay/business-ios/issues/261))
- **empresas:** [COPJ-251] Transaction Carousel ([#252](https://github.com/PicPay/business-ios/issues/252))
- **empresas:** [COPJ-362] Layout pagamento boleto ([#231](https://github.com/PicPay/business-ios/issues/231))


<a name="v1.1.17"></a>
## [v1.1.17] - 2020-04-15
### Added
- **empresas:** [COPJ-368] Habilita Saques Conta Bancaria e Conta PicPay PF para novos usuários MEI ([#240](https://github.com/PicPay/business-ios/issues/240))

### Changed
- **Business:** [B2B-XXX] Colocar novo sign up em uma feature flag ([#238](https://github.com/PicPay/business-ios/issues/238))

### Partial
- **empresas:** [COPJ-331] Saque para PicPayAccount ([#233](https://github.com/PicPay/business-ios/issues/233))


<a name="v1.1.16"></a>
## [v1.1.16] - 2020-04-09
### Added
- **empresas:** [B2B-607] Tela nova para ação taxa 0 ([#224](https://github.com/PicPay/business-ios/issues/224))
- **empresas:** [COPJ-357] Endereço Pessoal e Nome da mãe no cadastro ([#228](https://github.com/PicPay/business-ios/issues/228))

### Changed
- version update [#216](https://github.com/PicPay/business-ios/issues/216)

### Fixed
- **empresas:** [B2B-585] Adiciona tela de erro para central de notificações ([#226](https://github.com/PicPay/business-ios/issues/226))

### Partial
- **empresas:** [B2B-606] Adicionando funções ao layout da taxa 0 ([#223](https://github.com/PicPay/business-ios/issues/223))
- **empresas:** [B2B-605] Visual da tela de Taxa 0 no cadastro ([#221](https://github.com/PicPay/business-ios/issues/221))
- **empresas:** [COPJ-338] Analytics Opções de Saque ([#227](https://github.com/PicPay/business-ios/issues/227))
- **empresas:** [COPJ-264] Testes Opções de Saque ([#222](https://github.com/PicPay/business-ios/issues/222))
- **empresas:** Correções no fluxo de saque manual ([#220](https://github.com/PicPay/business-ios/issues/220))
- **empresas:** Navegação de Opções de Saque ([#219](https://github.com/PicPay/business-ios/issues/219))
- **empresas:** [COPJ-264] Serviços Opções de Saque ([#218](https://github.com/PicPay/business-ios/issues/218))
- **empresas:** [COPJ-310] Layout Opções de Saque ([#202](https://github.com/PicPay/business-ios/issues/202))


<a name="1.1.14"></a>
## [1.1.14] - 2020-04-01
### Changed
- version update [#215](https://github.com/PicPay/business-ios/issues/215)


<a name="v1.1.13"></a>
## [v1.1.13] - 2020-04-01
### Added
- **empresas:** [B2B-341] Central de Notificações ([#205](https://github.com/PicPay/business-ios/issues/205))

### Partial
- **empresas:** [B2B-540] Alterando endPoints de notificações  ([#204](https://github.com/PicPay/business-ios/issues/204))
- **empresas:** Termos de aceite usando URLs do firebase ([#198](https://github.com/PicPay/business-ios/issues/198))
- **empresas:** [COPJ-329] Componente Saque PicPay pessoal ([#199](https://github.com/PicPay/business-ios/issues/199))
- **empresas:** [COPJ-296] Verifica disponibilidade de saque ([#194](https://github.com/PicPay/business-ios/issues/194))


<a name="v1.1.12"></a>
## [v1.1.12] - 2020-03-25
### Added
- **plataforma:** [PLAIOS-102] adicionado configuração do PicPay Empresas Homolog no Firebase ([#195](https://github.com/PicPay/business-ios/issues/195))

### Changed
- **plataforma:** [PLAIOS-117] remove Fabric e Crashlytics e adicionar Firebase Crashlytics ([#197](https://github.com/PicPay/business-ios/issues/197))

### Partial
- **empresas:** ErrorView de AutomaticWithdrawal ([#196](https://github.com/PicPay/business-ios/issues/196))
- **empresas:** [COPJ-237] Inclusão de senha no request de saque automatico ([#188](https://github.com/PicPay/business-ios/issues/188))
- **empresas:** [B2B-553] Adiciona nas configurações solicitação de material ([#189](https://github.com/PicPay/business-ios/issues/189))


<a name="v1.1.11"></a>
## [v1.1.11] - 2020-03-18
### Added
- **empresas:** Unificando mudanças da NavigationBar ([#175](https://github.com/PicPay/business-ios/issues/175))

### Changed
- **empresas:** Ajuste template de CHANGELOG ([#181](https://github.com/PicPay/business-ios/issues/181))

### Partial
- **empresas:** [COPJ-299] Conecta fluxos de Saque Manual e Habilitar Saque Automatico ([#180](https://github.com/PicPay/business-ios/issues/180))
- **empresas:** [COPJ-127] Eventos de saque manual ([#179](https://github.com/PicPay/business-ios/issues/179))
- **empresas:** [B2B-535] Adiciona Analytics na tabBar no botao novo de notificacoes ([#185](https://github.com/PicPay/business-ios/issues/185))
- **empresas:** [B2B-558] Arruma bug do botão receber que não saia da tela ([#186](https://github.com/PicPay/business-ios/issues/186))
- **empresas:** [B2B-510] Tela de central de notificacoes ([#177](https://github.com/PicPay/business-ios/issues/177))
- **empresas:** [COPJ-205] Termos de aceite ([#184](https://github.com/PicPay/business-ios/issues/184))


<a name="v1.1.10"></a>
## [v1.1.10] - 2020-03-11
### Added
- **empresas:** [B2B-508]Cria botão de venda na tabBar ([#169](https://github.com/PicPay/business-ios/issues/169))
- **empresas:** [COPJ-245] Analytics de Saque Automático ([#176](https://github.com/PicPay/business-ios/issues/176))
- **empresas:** [COPJ-244] Atualizando menu após trocar o estado do Saque Automático ([#171](https://github.com/PicPay/business-ios/issues/171))
- **empresas:** [B2B-479] Testes unitários do fluxo de impressão do QRCode Part2 ([#160](https://github.com/PicPay/business-ios/issues/160))


<a name="v1.1.9"></a>
## [v1.1.9] - 2020-03-04
### Added
- **empresas:** [COPJ-237] Implementando serviço de saque automático ([#168](https://github.com/PicPay/business-ios/issues/168))
- **empresas:** [B2B-479] Testes unitários do fluxo de impressão do QRCode Part1 ([#159](https://github.com/PicPay/business-ios/issues/159))
- **empresas:** [COPJ-237] Saque automatico testes ([#161](https://github.com/PicPay/business-ios/issues/161))


<a name="v1.1.8"></a>
## [v1.1.8] - 2020-02-27
### Added
- **empresas:** [B2B-378] Adiciona novos deeplinks ([#147](https://github.com/PicPay/business-ios/issues/147))
- **empresas:** [COPJ-122] Solicitação de saque ([#154](https://github.com/PicPay/business-ios/issues/154))
- **empresas:** Adiciona envio de e-mail do QRCode ([#150](https://github.com/PicPay/business-ios/issues/150))
- **empresas:** [COPJ-135] - Tela de solicitação de saque ([#146](https://github.com/PicPay/business-ios/issues/146))
- **empresas:** [B2B-404] Baixar e compartilhar PDF ([#144](https://github.com/PicPay/business-ios/issues/144))
- **empresas:** [COPJ-120] - Conta PJ Wallet Balance Component ([#145](https://github.com/PicPay/business-ios/issues/145))
- **empresas:** [B2B-389] Fluxo de impressão de QRCode (Apenas visual) ([#141](https://github.com/PicPay/business-ios/issues/141))

### Fixed
- **empresas:** [B2B-502] Devolve campo digito de agencia ([#158](https://github.com/PicPay/business-ios/issues/158))
- **empresas:** Ajuste no fluxo de impressão de QRCode ([#149](https://github.com/PicPay/business-ios/issues/149))


<a name="v1.1.7"></a>
## [v1.1.7] - 2020-02-06
### Added
- **empresas:** Incluido Container de dependencias ([#137](https://github.com/PicPay/business-ios/issues/137))
- **empresas:** Nova tela de edição de contas ([#128](https://github.com/PicPay/business-ios/issues/128))


<a name="v1.1.6"></a>
## [v1.1.6] - 2020-01-29
### Changed
- **empresas:** B2B-138 Adiciona novos tracks ([#125](https://github.com/PicPay/business-ios/issues/125))


<a name="v1.1.5"></a>
## [v1.1.5] - 2020-01-24
### Changed
- **empresas:** B2B-382 Adiciona validacao em agencia e conta bancaria ([#127](https://github.com/PicPay/business-ios/issues/127))

### Fixed
- **empresas:** B2B-386 Ajuste no fluxo que chamam UnloggedViewController ([#126](https://github.com/PicPay/business-ios/issues/126))
- **plataforma:** [PLAIOS-51] ajusta a lane do fastlane que estava falhando no momento de deploy ([#124](https://github.com/PicPay/business-ios/issues/124))


<a name="v1.1.4"></a>
## [v1.1.4] - 2020-01-08
### Added
- **empresas:** B2B-303 Adiciona ChangeLog Automatico   ([#119](https://github.com/PicPay/business-ios/issues/119))
- **empresas:** [B2B-325] Adiciona estrutura de deeplinks ([#114](https://github.com/PicPay/business-ios/issues/114))

### Changed
- **empresas:** [B2B-345] Refatoração da classe UnloggedViewController ([#117](https://github.com/PicPay/business-ios/issues/117))


<a name="v1.1.3"></a>
## [v1.1.3] - 2019-12-30
### Added
- **empresas:** [B2B-258] Permitir conta PF no registro de empresas individuais ([#108](https://github.com/PicPay/business-ios/issues/108))


[Unreleased]: https://github.com/PicPay/business-ios/compare/v1.2.12...HEAD
[pj@v1.2.12]: https://github.com/PicPay/business-ios/compare/v1.2.11...v1.2.12
[v1.2.12]: https://github.com/PicPay/business-ios/compare/v1.2.11...v1.2.12
[v1.2.11]: https://github.com/PicPay/business-ios/compare/v1.2.10...v1.2.11
[v1.2.10]: https://github.com/PicPay/business-ios/compare/v1.2.9...v1.2.10
[v1.2.9]: https://github.com/PicPay/business-ios/compare/v1.2.8...v1.2.9
[v1.2.8]: https://github.com/PicPay/business-ios/compare/v1.2.7...v1.2.8
[v1.2.7]: https://github.com/PicPay/business-ios/compare/v1.2.4...v1.2.7
[v1.2.4]: https://github.com/PicPay/business-ios/compare/v1.2.3...v1.2.4
[v1.2.3]: https://github.com/PicPay/business-ios/compare/v1.2.2...v1.2.3
[v1.2.2]: https://github.com/PicPay/business-ios/compare/v1.2.1...v1.2.2
[v1.2.1]: https://github.com/PicPay/business-ios/compare/v1.2.0...v1.2.1
[v1.2.0]: https://github.com/PicPay/business-ios/compare/v1.1.18...v1.2.0
[v1.1.18]: https://github.com/PicPay/business-ios/compare/v1.1.17...v1.1.18
[v1.1.17]: https://github.com/PicPay/business-ios/compare/v1.1.16...v1.1.17
[v1.1.16]: https://github.com/PicPay/business-ios/compare/1.1.14...v1.1.16
[1.1.14]: https://github.com/PicPay/business-ios/compare/v1.1.13...1.1.14
[v1.1.13]: https://github.com/PicPay/business-ios/compare/v1.1.12...v1.1.13
[v1.1.12]: https://github.com/PicPay/business-ios/compare/v1.1.11...v1.1.12
[v1.1.11]: https://github.com/PicPay/business-ios/compare/v1.1.10...v1.1.11
[v1.1.10]: https://github.com/PicPay/business-ios/compare/v1.1.9...v1.1.10
[v1.1.9]: https://github.com/PicPay/business-ios/compare/v1.1.8...v1.1.9
[v1.1.8]: https://github.com/PicPay/business-ios/compare/v1.1.7...v1.1.8
[v1.1.7]: https://github.com/PicPay/business-ios/compare/v1.1.6...v1.1.7
[v1.1.6]: https://github.com/PicPay/business-ios/compare/v1.1.5...v1.1.6
[v1.1.5]: https://github.com/PicPay/business-ios/compare/v1.1.4...v1.1.5
[v1.1.4]: https://github.com/PicPay/business-ios/compare/v1.1.3...v1.1.4
[v1.1.3]: https://github.com/PicPay/business-ios/compare/v1.1.2...v1.1.3


## [v1.1.2] - 2019-12-09
### Added
 - B2B-107 Criado fluxo de recuperação de senha
 - B2B-254 Alterado link para download do app PicPay

## [v1.1.1] - 2019-12-04
### Changed
- B2B-256 Alterar comportamento do botão "Continuar"

### Fixed
- Warings de SwiftLint resolvidos
- B2B-256 Correção de layout na aba extrato 

## [v1.1.0] - 2019-11-22
### Fixed
- Ajusta signing pra manual

### Added
- B2B-42 Habilita recebimento de push notification do Mixpanel
- B2B-48 Implementa eventos no Appsflyer
- Adicionado template de pull requests
- Corrige scripts de CI
- PM-1003 Adicionando arquivo de setup para o projeto

### Changed
- PM-1001 Removendo os Pods do projeto

## [v1.0.29] - 2019-05-10
### Fixed
- PM-1140 Alteração de conta bancária no PicPay Empresas

## [v1.0.27] - 2018-12-04

### Added
- PM-454 Mensagem quando o usuário BIZ está bloqueado. Acrescentar um botão de fale conosco.

## [v1.0.26] - 2018-10-17

### Added
- Opção de exportar extrato em formato PDF ou CSV para envio por email
- PM-326 Ajustes para IPhoneX e Swift 4.2

### Changed
- PM-514 Change Regex para permitir somente números no campos da agencia e conta
- PM-511 Remover campo CNPJ (Cadastro do usuário e Ajuste)

## [v1.0.21] - 2017-09-11
### Fixed
- URL do painel do empresas empresas.picpay.com

## [v1.0.20] - 2017-09-06
### Added
- Opção de Monitor Web na tela de ajustes

### Changed
- Endpoint para salvar token de push do firebase

## [v1.0.17] - 2017-08-09
### Added
- Popup para inserção do código promocional
- Opção de códigos promocionais na tela de ajustes
- Header com texto na tela de extrato (Exibido apenas se api enviar os dados da header)

## [v1.0.16](https://github.com/PicPay/business-ios/compare/v1.0.15...v1.0.16) - 2017-07-11
### Added
- Nova tela de parcelamento sem juros
- Ao deletar operador, apagar conta da lista de contas logadas.
- Saiba mais na tela de operadores vazia

### Changed

### Removed

### Fixed
-  Fixed

[Unreleased]: https://github.com/PicPay/business-ios/compare/v1.0.17...HEAD
[v1.0.17]: https://github.com/PicPay/business-ios/compare/v1.0.16...v1.0.17
[v1.0.16]: https://github.com/PicPay/business-ios/compare/v1.0.15...v1.0.16
[v1.0.20]: https://github.com/PicPay/business-ios/compare/v1.0.20...v1.0.16
[v1.0.16]: https://github.com/PicPay/business-ios/compare/v1.0.21...v1.0.21
[v1.0.26]: https://github.com/PicPay/business-ios/compare/v1.0.26...v1.0.21
[v1.0.21]: https://github.com/PicPay/business-ios/compare/v1.0.27...v1.0.27
[v1.0.29]: https://github.com/PicPay/business-ios/compare/v1.0.27...v1.0.29
