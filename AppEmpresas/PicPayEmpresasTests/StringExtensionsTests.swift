import XCTest
@testable import PicPayEmpresas

final class StringExtensionsTests: XCTestCase {

    func testOnlyNumbers() {
        XCTAssertEqual("123.123.123-12".onlyNumbers, "12312312312")
        XCTAssertEqual("abc123def456".onlyNumbers, "123456")
        XCTAssertEqual("stringDeTeste2".onlyNumbers, "2")
        XCTAssertNotEqual("123.123.asd".onlyNumbers, "123.123.asd")
    }
    
    func testCpfValidation() {
        XCTAssert("517.188.214-25".validCpf)
        XCTAssertFalse("317.188.214-25".validCpf)
        XCTAssertFalse("517.188.214-00".validCpf)
        XCTAssert("822.516.585-30".validCpf)
        XCTAssert("486.272.888-01".validCpf)
        XCTAssert("150.542.259-00".validCpf)
    }
}
