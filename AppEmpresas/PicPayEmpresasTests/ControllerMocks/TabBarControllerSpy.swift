import UIKit
@testable import PicPayEmpresas

final class TabBarControllerSpy: UITabBarController {
    private(set) var presentCount: Int = 0
    private(set) var presentViewController: UIViewController?

    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        super.present(viewControllerToPresent, animated: flag, completion: completion)
        presentViewController = viewControllerToPresent
        presentCount += 1
    }
}

