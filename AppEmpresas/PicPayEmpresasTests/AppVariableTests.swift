import XCTest
@testable import PicPayEmpresas

final class AppVariableTests: XCTestCase {
    private var appMode: AppMode = .debug
    private var environment: [String: String] = [:]
    
    private lazy var sut: EnvVariable = EnvVariable(appMode: appMode, environment: environment)
    
    func testIsAnalyticsLogEnabled_WhenIsDebugAndEnabled_ShouldReturnTrue() {
        appMode = .debug
        environment = ["ANALYTICS_LOG": "enable"]
        
        XCTAssertTrue(sut.isAnalyticsLogEnabled)
    }
    
    func testIsAnalyticsLogEnabled_WhenIsDebugAndNotEnabled_ShouldReturnFalse() {
        appMode = .debug
        environment = ["ANALYTICS_LOG": "another value"]
        
        XCTAssertFalse(sut.isAnalyticsLogEnabled)
    }
    
    func testIsAnalyticsLogEnabled_WhenIsDebugAndEmpty_ShouldReturnFalse() {
        appMode = .debug
        environment = [:]
        
        XCTAssertFalse(sut.isAnalyticsLogEnabled)
    }
    
    func testIsAnalyticsLogEnabled_WhenIsNotDebugAndEnabled_ShouldReturnFalse() {
        appMode = .release
        environment = ["ANALYTICS_LOG": "enable"]
        
        XCTAssertFalse(sut.isAnalyticsLogEnabled)
    }
}
