import XCTest
import Core
import LegacyPJ
@testable import PicPayEmpresas

class KeychainKeyTests: XCTestCase {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainerMock(KeychainManagerMock())
        
    func testSetApiAuthIndex() {
        dependencies.keychain.set(key: KeychainKeyLegacyPJ.apiAuthIndex, value: "12")
        XCTAssertEqual(dependencies.keychain.getData(key: KeychainKeyLegacyPJ.apiAuthIndex), "12")
    }
    
    func testNullifyToken() {
        XCTAssertTrue(dependencies.keychain.clearValue(key: KeychainKeyLegacyPJ.apiAuthIndex))
        //Setting nil to a dictionary value on keychain is returning NSNull instead of nil
        XCTAssertNil(dependencies.keychain.getData(key: KeychainKeyLegacyPJ.apiAuthIndex))
    }
    
    func testSetUserApiAuth() {
        dependencies.keychain.set(key: KeychainKeyLegacyPJ.apiAuth, value: "picpayTest")
        XCTAssertEqual(dependencies.keychain.getData(key: KeychainKeyLegacyPJ.apiAuth), "picpayTest")
    }
    
    func testNullifyUserId() {
        XCTAssertTrue(dependencies.keychain.clearValue(key: KeychainKeyLegacyPJ.apiAuth))
        XCTAssertNil(dependencies.keychain.getData(key: KeychainKeyLegacyPJ.apiAuth))
    }
}
