import XCTest
@testable import PicPayEmpresas

final class CampaignHowWorksCoordinatorTests: XCTestCase {
    private let viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    private lazy var sut: CampaignHowWorksCoordinator = {
        let coordinator = CampaignHowWorksCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()

    func testPerformAction_WhenActionCreateCampaing_ShouldShowCreateCampaignScreen() {
        sut.perform(action: .createCampaign)
        
        XCTAssertTrue(navigationSpy.currentViewController is CampaignTypeViewController)
    }
}
