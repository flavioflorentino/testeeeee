import XCTest
@testable import PicPayEmpresas

private final class CampaignHowWorksPresenterSpy: CampaignHowWorksPresenting {
    weak var viewController: CampaignHowWorksDisplay?
    
    private(set) var didShowCreateCampaign: Int = 0
    private(set) var didShowFrequentQuestions: Int = 0
    private(set) var campaignHowWorksAction: CampaignHowWorksAction?
    private(set) var frequentQuestions: [FrequentQuestionsModel] = []
    
    func didNextStep(action: CampaignHowWorksAction) {
        guard case .createCampaign = action else {
            XCTFail()
            return
        }
        
        campaignHowWorksAction = action
        didShowCreateCampaign += 1
    }
    
    func presentFrequentQuestions(_ questions: [FrequentQuestionsModel]) {
        didShowFrequentQuestions += 1
        frequentQuestions = questions
    }
}


final class CampaignHowWorksInteractorTests: XCTestCase {
    private let presenterSpy = CampaignHowWorksPresenterSpy()
    
    private lazy var sut = CampaignHowWorksInteractor(presenter: presenterSpy)

    func testCreateCampaignButton_WhenTouched_ShouldShowCreateCampaignScreen() {
        sut.createCampaign()
        
        XCTAssertNotNil(presenterSpy.campaignHowWorksAction)
        XCTAssertEqual(presenterSpy.didShowCreateCampaign, 1)
    }
    
    func testGetFrequentQuestions_WhenViewDidLoad_ShouldShowFrequentQuestions() {
        sut.getFrequentQuestions()
        
        XCTAssertEqual(presenterSpy.frequentQuestions.count, 5)
        XCTAssertEqual(presenterSpy.frequentQuestions.first?.title, "O que é cashback?")
        XCTAssertEqual(presenterSpy.didShowFrequentQuestions, 1)
    }
}
