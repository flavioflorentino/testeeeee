import XCTest
@testable import PicPayEmpresas

private final class CampaignHowWorksCoordinatorMock: CampaignHowWorksCoordinating {
    weak var viewController: UIViewController?
    
    private(set) var goToCreateCampaignScreen = 0
    private(set) var campaignHowWorksAction: CampaignHowWorksAction?
    
    func perform(action: CampaignHowWorksAction) {
        guard case .createCampaign = action else {
            XCTFail()
            return
        }
        
        campaignHowWorksAction = action
        goToCreateCampaignScreen += 1
    }
}

final class CampaignHowWorksPresenterTests: XCTestCase {
    private let coordinatorMock = CampaignHowWorksCoordinatorMock()
    
    private lazy var sut = CampaignHowWorksPresenter(coordinator: coordinatorMock)
    
    func testDidNextStep_WhenActionCreateCampaign_ShouldGoToCreateCampaign() {
        sut.didNextStep(action: .createCampaign)
        
        XCTAssertNotNil(coordinatorMock.campaignHowWorksAction)
        XCTAssertEqual(coordinatorMock.goToCreateCampaignScreen, 1)
    }
}
