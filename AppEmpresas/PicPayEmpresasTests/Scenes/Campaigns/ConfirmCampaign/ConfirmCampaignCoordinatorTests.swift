import XCTest
@testable import PicPayEmpresas

final class ConfirmCampaignCoordinatorTests: XCTestCase {
    private let viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    
    private lazy var sut: ConfirmCampaignCoordinator = {
        let coordinator = ConfirmCampaignCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testPerformAction_WhenActionGoToTerms_ShouldShowTermsAndConditionsScreen() {
        sut.perform(action: .goToTermsAndConditions)
        
        XCTAssertTrue(navigationSpy.currentViewController is TermsAndConditionsViewController)
    }
    
    func testPerformAction_WhenActionCampaignError_ShouldShowCampaignError() {
        sut.perform(action: .createCampaignError(modalInfo: AlertModalInfo()))
        
        XCTAssertTrue(viewControllerSpy.presentViewController is PopupViewController)
    }
    
    func testPerformAction_WhenActionGoToCampaign_ShouldShowCampaignScreen() {
        let campaignResponse = CampaignResponse(
                   id: "5f10be77ff7fe50044454872",
                   sellerId: "1",
                   type: "first_buy",
                   active: true,
                   cashback: 10,
                   maxValue: 30.5,
                   expirationDate: "25/12/2020 13:30")
        
        sut.perform(action: .goToCampaign(campaignResponse))
        
        XCTAssertTrue(navigationSpy.currentViewController is ActiveCampaignViewController)
    }
}
