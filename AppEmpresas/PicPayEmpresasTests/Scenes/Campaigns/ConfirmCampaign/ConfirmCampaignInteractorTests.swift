import Core
import XCTest
@testable import PicPayEmpresas

private enum ConfirmCampaignServiceResponse {
    case success
    case failure
    case invalidResponse
}

private final class ConfirmCampaignPresenterSpy: ConfirmCampaignPresenting {
    var viewController: ConfirmCampaignDisplay?
    
    private(set) var didEnableCreateButton: Int = 0
    private(set) var didShowNextStep: Int = 0
    private(set) var didShowCampaignData: Int = 0
    private(set) var didShowStartLoading: Int = 0
    private(set) var didShowStopLoading: Int = 0
    private(set) var didShowError: Int = 0
    private(set) var didShowCampaign: Int = 0
    private(set) var didShowTermsAndConditions: Int = 0
    private(set) var didShowCampaignError: Int = 0
    private(set) var confirmCampaignAction: ConfirmCampaignAction?
    private(set) var isCreateButtonEnabled: Bool = false
    private(set) var campaign: Campaign?
    private(set) var campaignResponse: CampaignResponse?
    private(set) var errorMessage: String = ""
    
    func enableCreateButton(_ isEnabled: Bool) {
        didEnableCreateButton += 1
        isCreateButtonEnabled = isEnabled
    }
    
    func didNextStep(action: ConfirmCampaignAction) {
        didShowNextStep += 1
        confirmCampaignAction = action
        
        switch action {
        case .goToTermsAndConditions:
            didShowTermsAndConditions += 1
        case .createCampaignError:
            didShowCampaignError += 1
        case let .goToCampaign(campaign):
            didShowCampaign += 1
            campaignResponse = campaign
        }
    }
    
    func presentCampaignData(_ campaign: Campaign) {
        didShowCampaignData += 1
        self.campaign = campaign
    }
    
    func startLoading() {
        didShowStartLoading += 1
    }
    
    func stopLoading() {
        didShowStopLoading += 1
    }
    
    func showError(apiError: ApiError) {
        didShowError += 1
    }
    
    func showError(requestError: RequestError) {
        didShowError += 1
    }
}

private final class ConfirmCampaignServiceMock: ConfirmCampaignServicing {
    var response: ConfirmCampaignServiceResponse = .success
    
    let campaignResponse = CampaignResponse(
        id: "5f10be77ff7fe50044454872",
        sellerId: "1",
        type: "first_buy",
        active: true,
        cashback: 10,
        maxValue: 30.5,
        expirationDate: "25/12/2020 13:30")

    func createCampaign(_ campaign: Campaign, completion: @escaping (Result<CampaignResponse?, ApiError>) -> Void) {
        switch response {
        case .failure:
            completion(.failure(.serverError))
        case .success:
            completion(.success(campaignResponse))
        case .invalidResponse:
            completion(.success(nil))
        }
    }
}


final class ConfirmCampaignInteractorTests: XCTestCase {
    private let container = DependencyContainerMock()
    private let presenterSpy = ConfirmCampaignPresenterSpy()
    private let serviceMock = ConfirmCampaignServiceMock()
    private let campaign = Campaign(type: .firstBuy,
                                    cashback: "10",
                                    maxValue: "R$ 10,00",
                                    expirationDate: "15 dias")
    
    private lazy var sut = ConfirmCampaignInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        dependencies: container,
        campaign: campaign)
    
 
    func testShowTermsAndConditions_WhenSuccess_ShouldShowTermsAndConditions() {
        sut.showTermsAndConditions()
        
        XCTAssertEqual(presenterSpy.didShowNextStep, 1)
        XCTAssertNotNil(presenterSpy.confirmCampaignAction)
        XCTAssertEqual(presenterSpy.didShowTermsAndConditions, 1)
    }
    
    func testCheckboxSelected_WhenSelected_ShouldEnableCreateButton() {
        sut.checkboxSelected(true)
        
        XCTAssertEqual(presenterSpy.didEnableCreateButton, 1)
        XCTAssertTrue(presenterSpy.isCreateButtonEnabled)
    }
    
    func testCheckboxSelected_WhenNotSelected_ShouldDisableCreateButton() {
        sut.checkboxSelected(false)
        
        XCTAssertEqual(presenterSpy.didEnableCreateButton, 1)
        XCTAssertFalse(presenterSpy.isCreateButtonEnabled)
    }
    
    func testSetCampaignData_WhenSuccess_ShouldShowCampaignData() {
        sut.setCampaignData()
        
        XCTAssertEqual(presenterSpy.didShowCampaignData, 1)
        XCTAssertNotNil(presenterSpy.campaign)
        XCTAssertEqual(presenterSpy.campaign?.type, .firstBuy)
        XCTAssertEqual(presenterSpy.campaign?.cashback, "10")
        XCTAssertEqual(presenterSpy.campaign?.maxValue, "R$ 10,00")
        XCTAssertEqual(presenterSpy.campaign?.expirationDate, "15 dias")
    }
    
    func testCreateCampaign_WhenSuccess_ShouldShowCampaign() {
        serviceMock.response = .success
        
        sut.createCampaign()
        
        XCTAssertEqual(presenterSpy.didShowStartLoading, 1)
        XCTAssertEqual(presenterSpy.didShowNextStep, 1)
        XCTAssertNotNil(presenterSpy.confirmCampaignAction)
        XCTAssertEqual(presenterSpy.didShowCampaign, 1)
        XCTAssertEqual(presenterSpy.didShowStopLoading, 1)
        
        XCTAssertEqual(presenterSpy.campaignResponse?.id, "5f10be77ff7fe50044454872")
        XCTAssertEqual(presenterSpy.campaignResponse?.type, "first_buy")
        XCTAssertEqual(presenterSpy.campaignResponse?.cashback, 10)
        XCTAssertEqual(presenterSpy.campaignResponse?.maxValue, 30.5)
    }
    
    func testCreateCampaign_WhenFailure_ShouldCallError() {
        serviceMock.response = .failure
        
        sut.createCampaign()
        
        XCTAssertEqual(presenterSpy.didShowStartLoading, 1)
        XCTAssertEqual(presenterSpy.didShowError, 1)
        XCTAssertEqual(presenterSpy.didShowStopLoading, 1)
    }
    
    func testCreateCampaign_WhenInvalidResponse_ShouldCallError() {
        serviceMock.response = .invalidResponse
        
        sut.createCampaign()
        
        XCTAssertEqual(presenterSpy.didShowStartLoading, 1)
        XCTAssertEqual(presenterSpy.didShowError, 1)
        XCTAssertEqual(presenterSpy.didShowStopLoading, 1)
    }
}
