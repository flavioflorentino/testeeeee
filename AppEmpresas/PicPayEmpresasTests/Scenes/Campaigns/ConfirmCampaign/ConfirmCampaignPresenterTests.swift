import UI
import Core
import UIKit
import XCTest
@testable import PicPayEmpresas

private final class ConfirmCampaignCoordinatorMock: ConfirmCampaignCoordinating {
    var viewController: UIViewController?
    
    private(set) var goToTermsAndConditionsScreen = 0
    private(set) var goToCampaignScreen = 0
    private(set) var showCampaignError = 0
    private(set) var campaignData: CampaignResponse?
    
    func perform(action: ConfirmCampaignAction) {
        switch action {
        case .goToTermsAndConditions:
            goToTermsAndConditionsScreen += 1
        case let .goToCampaign(campaignResponse):
            goToCampaignScreen += 1
            campaignData = campaignResponse
        case .createCampaignError:
            showCampaignError += 1
        }
    }
}

private final class ConfirmCampaignViewControllerSpy: UIViewController, ConfirmCampaignDisplay {
    var loadingView = LoadingView()
    
    private(set) var didShowCampaignData = 0
    private(set) var didEnableCreateButton = 0
    private(set) var campaignData: Campaign?
    private(set) var isCreateButtonEnable: Bool = false
    
    func displayCampaignData(_ data: Campaign) {
        campaignData = data
        didShowCampaignData += 1
    }
    
    func displayCreateButtonEnabled(_ isEnabled: Bool) {
        isCreateButtonEnable = isEnabled
        didEnableCreateButton += 1
    }
}


final class ConfirmCampaignPresenterTests: XCTestCase {
    private let coordinatorMock = ConfirmCampaignCoordinatorMock()
    private let viewControllerSpy = ConfirmCampaignViewControllerSpy()
    
    private lazy var sut: ConfirmCampaignPresenter = {
        let presenter = ConfirmCampaignPresenter(coordinator: coordinatorMock)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidNextStep_WhenActionGoToTermsAndConditions_ShouldGoToTermsScreen() {
        sut.didNextStep(action: .goToTermsAndConditions)
        
        XCTAssertEqual(coordinatorMock.goToTermsAndConditionsScreen, 1)
    }
    
    func testDidNextStep_WhenActionGoToCampaign_ShouldGoToCampaignScreen() {
        let campaign = CampaignResponse(
            id: "5f10be77ff7fe50044454872",
            sellerId: "1",
            type: "first_buy",
            active: true,
            cashback: 10,
            maxValue: 30.5,
            expirationDate: "25/12/2020 13:30")
        
        sut.didNextStep(action: .goToCampaign(campaign))
        
        XCTAssertNotNil(coordinatorMock.campaignData)
        XCTAssertEqual(coordinatorMock.goToCampaignScreen, 1)
        XCTAssertEqual(coordinatorMock.campaignData?.id, "5f10be77ff7fe50044454872")
        XCTAssertEqual(coordinatorMock.campaignData?.type, "first_buy")
        XCTAssertEqual(coordinatorMock.campaignData?.cashback, 10)
        XCTAssertEqual(coordinatorMock.campaignData?.maxValue, 30.5)
        XCTAssertEqual(coordinatorMock.campaignData?.expirationDate, "25/12/2020 13:30")
    }
    
    func testDidNextStep_WhenCreateCampaignError_ShouldShowErrorModal() {
        let modalInfo = AlertModalInfo()
        
        sut.didNextStep(action: .createCampaignError(modalInfo: modalInfo))
        
        XCTAssertEqual(coordinatorMock.showCampaignError, 1)
    }
    
    func testEnableCreateButton_WhenTrue_ShouldEnableCreateButton() {
        sut.enableCreateButton(true)
        
        guard let viewController = sut.viewController as? ConfirmCampaignViewControllerSpy else {
            XCTFail("Fail trying to cast viewController spy.")
            return
        }
        
        XCTAssertEqual(viewController.didEnableCreateButton, 1)
        XCTAssertTrue(viewController.isCreateButtonEnable)
    }
    
    func testEnableCreateButton_WhenFalse_ShouldDisableCreateButton() {
        sut.enableCreateButton(false)
        
        guard let viewController = sut.viewController as? ConfirmCampaignViewControllerSpy else {
            XCTFail("Fail trying to cast viewController spy.")
            return
        }
        
        XCTAssertEqual(viewController.didEnableCreateButton, 1)
        XCTAssertFalse(viewController.isCreateButtonEnable)
    }
    
    func testPresentCampaignData_WhenSuccess_ShouldPresentCampaignData() {
        let campaign = Campaign(type: .firstBuy,
                                cashback: "10",
                                maxValue: "R$ 10,00",
                                expirationDate: "15 dias")
        
        sut.presentCampaignData(campaign)
        
        guard let viewController = sut.viewController as? ConfirmCampaignViewControllerSpy else {
            XCTFail("Fail trying to cast viewController spy.")
            return
        }
        
        XCTAssertNotNil(viewController.campaignData)
        XCTAssertEqual(viewController.didShowCampaignData, 1)
        XCTAssertEqual(viewController.campaignData?.type, .firstBuy)
        XCTAssertEqual(viewController.campaignData?.cashback, "10")
        XCTAssertEqual(viewController.campaignData?.maxValue, "R$ 10,00")
    }
    
    func testShouwError_whenApiError_ShouldPresentModalError() {
        sut.showError(apiError: ApiError.bodyNotFound)
        
        XCTAssertEqual(coordinatorMock.showCampaignError, 1)
    }
    
    func testShouwError_whenRequestError_ShouldPresentModalError() {
        sut.showError(requestError: RequestError())
        
        XCTAssertEqual(coordinatorMock.showCampaignError, 1)
    }
}
