import Foundation
import XCTest
@testable import PicPayEmpresas

private final class CampaignTypePresenterSpy: CampaignTypePresenting {
    weak var viewController: CampaignTypeViewController?
    
    private(set) var didShowConfigCampaign: Int = 0
    private(set) var campaignType: CampaignType = .firstBuy
    private(set) var campaignTypeAction: CampaignTypeAction?
    
    func didNextStep(action: CampaignTypeAction) {
        campaignTypeAction = action
        
        switch action {
        case let .configCampaign(type):
            campaignType = type
            didShowConfigCampaign += 1
        }
    }
}

final class CampaignTypeViewModelTests: XCTestCase {
    private let presenterSpy = CampaignTypePresenterSpy()
    
    private lazy var sut: CampaignTypeViewModel = {
        let viewModel = CampaignTypeViewModel(presenter: presenterSpy)
        return viewModel
    }()
    
    func testCreateFirstBuyCampaign_WhenCardPressed_ShouldGoToConfigCampaign() {
        sut.createFirstBuyCampaign()
        
        XCTAssertNotNil(presenterSpy.campaignTypeAction)
        XCTAssertEqual(presenterSpy.campaignType, .firstBuy)
        XCTAssertEqual(presenterSpy.didShowConfigCampaign, 1)
        
    }
    
    func testcCreateFixCashbackCampaign_WhenCardPressed_ShouldGoToConfigCampaign() {
        sut.createFixCashbackCampaign()
        
        XCTAssertNotNil(presenterSpy.campaignTypeAction)
        XCTAssertEqual(presenterSpy.campaignType, .fixCashback)
        XCTAssertEqual(presenterSpy.didShowConfigCampaign, 1)
        
    }
}
