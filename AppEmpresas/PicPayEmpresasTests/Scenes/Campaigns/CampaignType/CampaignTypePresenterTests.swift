import XCTest
@testable import PicPayEmpresas

private final class CampaignTypeCoordinatorMock: CampaignTypeCoordinating {
    weak var viewController: UIViewController?
    
    private(set) var goToConfigCampaignScreen = 0
    private(set) var campaignType: CampaignType = .firstBuy
    private(set) var campaignTypeAction: CampaignTypeAction?
    
    func perform(action: CampaignTypeAction) {
        campaignTypeAction = action
        
        switch action {
        case let .configCampaign(type):
            campaignType = type
            goToConfigCampaignScreen += 1
        }
    }
}

class CampaignTypePresenterTests: XCTestCase {
     private let coordinatorMock = CampaignTypeCoordinatorMock()
     
     private lazy var sut = CampaignTypePresenter(coordinator: coordinatorMock)
    
    func testDidNextStep_WhenActionConfigCampaignFirstBuy_ShouldGoToConfigCampaign() {
        sut.didNextStep(action: .configCampaign(type: .firstBuy))
        
        XCTAssertNotNil(coordinatorMock.campaignTypeAction)
        XCTAssertEqual(coordinatorMock.goToConfigCampaignScreen, 1)
        XCTAssertEqual(coordinatorMock.campaignType, .firstBuy)
    }
    
    func testDidNextStep_WhenActionConfigCampaignFixCashback_ShouldGoToConfigCampaign() {
        sut.didNextStep(action: .configCampaign(type: .fixCashback))
        
        XCTAssertNotNil(coordinatorMock.campaignTypeAction)
        XCTAssertEqual(coordinatorMock.goToConfigCampaignScreen, 1)
        XCTAssertEqual(coordinatorMock.campaignType, .fixCashback)
    }
}
