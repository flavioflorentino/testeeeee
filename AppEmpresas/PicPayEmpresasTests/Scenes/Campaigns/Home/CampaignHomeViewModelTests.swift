import Core
import XCTest

@testable import PicPayEmpresas

private enum CampaignHomeServiceResponse {
    case empty
    case success
    case failure
    case invalidResponse
}

private final class CampaignHomePresenterSpy: CampaignHomePresenting {
    weak var viewController: CampaignHomeDisplay?
    
    private(set) var didShowError: Int = 0
    private(set) var isLoading: Bool = false
    private(set) var didShowHowWorks: Int = 0
    private(set) var didShowCampaign: Int = 0
    private(set) var didLoadingCalled: Int = 0
    private(set) var didShowAlertError: Int = 0
    private(set) var didShowCreateCampaign: Int = 0
    private(set) var campaignHomeAction: CampaignHomeAction?
    private(set) var campaignResponse: CampaignResponse?
    
    func setLoading(isLoading: Bool) {
        didLoadingCalled += 1
        self.isLoading = isLoading
    }
    
    func showError(apiError: ApiError) {
        didShowError += 1
    }
    
    func showError(requestError: RequestError) {
        didShowError += 1
    }
    
    func didNextStep(action: CampaignHomeAction) {
        campaignHomeAction = action
        
        switch action {
        case .createCampaign:
            didShowCreateCampaign += 1
        case let .goToCampaign(campaign):
            campaignResponse = campaign
            didShowCampaign += 1
        case .howCampaignWorks:
            didShowHowWorks += 1
        case .showErrorAlert:
            didShowAlertError += 1
        }
    }
}

private final class CampaignHomeServiceMock: CampaignHomeServicing {
    var response: CampaignHomeServiceResponse = .success
    
    private(set) var didShowSuccess: Int = 0
    private(set) var didShowFailure: Int = 0
    private(set) var didShowInvalidResponse: Int = 0
    
    let campaignResponse = CampaignResponse(
        id: "5f10be77ff7fe50044454872",
        sellerId: "1",
        type: "first_buy",
        active: true,
        cashback: 10,
        maxValue: 30.5,
        expirationDate: "25/12/2020 13:30")
    
    let campaignResponseInactive = CampaignResponse(
        id: "5f10be77ff7fe50044454872",
        sellerId: "1",
        type: "first_buy",
        active: false,
        cashback: 10,
        maxValue: 30.5,
        expirationDate: "25/12/2020 13:30")
    
    func getCampaigns(completion: @escaping (Result<[CampaignResponse]?, ApiError>) -> Void) {
        switch response {
        case .failure:
            didShowFailure += 1
            completion(.failure(.serverError))
        case .success:
            didShowSuccess += 1
            completion(.success([campaignResponse]))
        case .invalidResponse:
            didShowInvalidResponse += 1
            completion(.failure(.badRequest(body: RequestError.init())))
        case .empty:
            didShowSuccess += 1
            completion(.success([campaignResponseInactive]))
        }
    }
}

final class CampaignHomeViewModelTests: XCTestCase {
    private let serviceMock = CampaignHomeServiceMock()
    private let presenterSpy = CampaignHomePresenterSpy()
    
    private lazy var sut = CampaignHomeViewModel(service: serviceMock,
                                                 presenter: presenterSpy)

    func testCreateCampaignButton_WhenTouched_ShouldShowCreateCampaignScreen() {
        sut.createCampaign()
        
        XCTAssertEqual(presenterSpy.didShowCreateCampaign, 1)
    }
    
    func testHowWorksButton_WhenTouched_ShouldShowHowWorksScreen() {
        sut.howCampaignWorks()
        
        XCTAssertEqual(presenterSpy.didShowHowWorks, 1)
    }
    
    func testGetCampaigns_WhenSuccesAndAlreadyHaveCampaign_ShouldShowCampaign() {
        serviceMock.response = .success
        
        sut.getCampaigns()
        
        XCTAssertEqual(presenterSpy.didLoadingCalled, 2)
        XCTAssertEqual(presenterSpy.didShowCampaign, 1)
        XCTAssertEqual(serviceMock.didShowSuccess, 1)
        XCTAssertNotNil(presenterSpy.campaignResponse)
        XCTAssertEqual(presenterSpy.campaignResponse?.active, true)
        XCTAssertEqual(presenterSpy.campaignResponse?.id, "5f10be77ff7fe50044454872")
        XCTAssertEqual(presenterSpy.campaignResponse?.type, "first_buy")
        XCTAssertEqual(presenterSpy.campaignResponse?.expirationDate, "25/12/2020 13:30")
    }
    
    func testGetCampaigns_WhenSuccessAndDontHaveCampaign_ShouldShowCampaignsHome() {
        serviceMock.response = .empty
        
        sut.getCampaigns()
        
        XCTAssertEqual(presenterSpy.didLoadingCalled, 2)
        XCTAssertEqual(serviceMock.didShowSuccess, 1)
        XCTAssertNil(presenterSpy.campaignResponse)
    }
    
    func testGetCampaigns_WhenFails_ShouldShowError() {
        serviceMock.response = .failure
        
        sut.getCampaigns()
        
        XCTAssertEqual(presenterSpy.didLoadingCalled, 2)
        XCTAssertEqual(presenterSpy.didShowError, 1)
        XCTAssertEqual(serviceMock.didShowFailure, 1)
    }
}
