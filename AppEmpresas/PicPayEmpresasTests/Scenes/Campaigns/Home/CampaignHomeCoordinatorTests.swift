import XCTest
@testable import PicPayEmpresas

final class CampaignHomeCoordinatorTests: XCTestCase {
    private let viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    private lazy var sut: CampaignHomeCoordinator = {
        let coordinator = CampaignHomeCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testPerformAction_WhenActionShowAlert_ShouldShowAlert() {
        sut.perform(action: .showErrorAlert(modalInfo: AlertModalInfo()))
        
        XCTAssertTrue(viewControllerSpy.presentViewController is PopupViewController)
    }
    
    func testPerformAction_WhenActionCreateCampaing_ShouldShowCreateCampaignScreen() {
        sut.perform(action: .createCampaign)
        
        XCTAssertTrue(navigationSpy.currentViewController is CampaignTypeViewController)
    }
    
    func testPerformAction_WhenActionHowWorks_ShouldShowHowWorksScreen() {
        // TODO: implementar os testes quando as próximas telas do fluxo forem criadas.
    }
    
    func testPerformAction_WhenActionGoToCampaign_ShouldShowCampaignScreen() {
        // TODO: implementar os testes quando as próximas telas do fluxo forem criadas.
    }
    
}
