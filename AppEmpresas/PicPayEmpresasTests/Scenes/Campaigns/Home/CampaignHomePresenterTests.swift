import XCTest
@testable import PicPayEmpresas

private final class CampaignHomeCoordinatorMock: CampaignHomeCoordinating {
    weak var viewController: UIViewController?
    
    private(set) var showAlert = 0
    private(set) var goToCampaignScreen = 0
    private(set) var goToHowWorksScreen = 0
    private(set) var goToCreateCampaignScreen = 0
    private(set) var campaignHomeAction: CampaignHomeAction?
    
    func perform(action: CampaignHomeAction) {
        campaignHomeAction = action
        
        switch action {
        case .createCampaign:
            goToCreateCampaignScreen += 1
        case .goToCampaign:
            goToCampaignScreen += 1
        case .howCampaignWorks:
            goToHowWorksScreen += 1
        case .showErrorAlert:
            showAlert += 1
        }
    }
}

private final class CampaignHomeViewControllerSpy: UIViewController, CampaignHomeDisplay {
    private(set) var didStoptLoading = 0
    private(set) var didStartLoading = 0
    
    func showLoading() {
        didStartLoading += 1
    }
    
    func hideLoading() {
        didStoptLoading += 1
    }
}

final class CampaignHomePresenterTests: XCTestCase {
    private let viewControllerSpy = CampaignHomeViewControllerSpy()
    private let coordinatorMock = CampaignHomeCoordinatorMock()
    
    private lazy var sut: CampaignHomePresenter = {
        let presenter = CampaignHomePresenter(coordinator: coordinatorMock)
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testIsLoading_WhenTrue_ShouldPresentLoading() {
        sut.setLoading(isLoading: true)
        
        XCTAssertEqual(viewControllerSpy.didStartLoading, 1)
    }
    
    func testIsLoading_WhenFalse_ShouldHideLoading() {
        sut.setLoading(isLoading: false)
        
        XCTAssertEqual(viewControllerSpy.didStoptLoading, 1)
    }
    
    func testDidNextStep_WhenActionCreateCampaign_ShouldGoToCreateCampaign() {
        sut.didNextStep(action: .createCampaign)
        
        XCTAssertNotNil(coordinatorMock.campaignHomeAction)
        XCTAssertEqual(coordinatorMock.goToCreateCampaignScreen, 1)
    }
    
    func testDidNextStep_WhenActionGoToCampaign_ShouldGoToCampaign() {
        let campaignResponse = CampaignResponse(
               id: "5f10be77ff7fe50044454872",
               sellerId: "1",
               type: "first_buy",
               active: true,
               cashback: 10,
               maxValue: 30.5,
               expirationDate: "25/12/2020 13:30")
        
        sut.didNextStep(action: .goToCampaign(campaignResponse))
        
        XCTAssertNotNil(coordinatorMock.campaignHomeAction)
        XCTAssertEqual(coordinatorMock.goToCampaignScreen, 1)
    }
    
    func testDidNextStep_WhenActionHowWorks_ShouldGoToHowWorks() {
        sut.didNextStep(action: .howCampaignWorks)
        
        XCTAssertNotNil(coordinatorMock.campaignHomeAction)
        XCTAssertEqual(coordinatorMock.goToHowWorksScreen, 1)
    }
}
