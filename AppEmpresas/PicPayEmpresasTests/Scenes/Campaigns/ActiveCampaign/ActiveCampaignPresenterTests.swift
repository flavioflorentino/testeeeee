import UI
import Core
import UIKit
import XCTest
@testable import PicPayEmpresas

private final class ActiveCampaignCoordinatorMock: ActiveCampaignCoordinating {
    var viewController: UIViewController?
    
    private(set) var goToCampaignHome = 0
    private(set) var showCampaignError = 0
    
    func perform(action: ActiveCampaignAction) {
        switch action {
        case .deleteCampaignError:
            showCampaignError += 1
        case .popToHome:
            goToCampaignHome += 1
        }
    }
}

private final class ActiveCampaignViewControllerSpy: UIViewController, ActiveCampaignDisplay {
    var loadingView = LoadingView()
    
    private(set) var didDisplayCampaignData: Int = 0
    private(set) var campaignItems: [CampaignItem] = []
    
    func displayCampaignData(items: [CampaignItem]) {
        didDisplayCampaignData += 1
        campaignItems = items
    }
}

final class ActiveCampaignPresenterTests: XCTestCase {
    private let coordinatorMock = ActiveCampaignCoordinatorMock()
    private let viewControllerSpy = ActiveCampaignViewControllerSpy()
    
    private lazy var sut: ActiveCampaignPresenter = {
        let presenter = ActiveCampaignPresenter(coordinator: coordinatorMock)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentCampaignData_ShouldDisplayCampaignItems() {
        let campaignResponse = CampaignResponse(
            id: "5f10be77ff7fe50044454872",
            sellerId: "1",
            type: "first_buy",
            active: true,
            cashback: 10,
            maxValue: 30.5,
            expirationDate: "25/12/2020 13:30")
        
        sut.presentCampaignData(campaignData: campaignResponse)
        
        guard let viewController = sut.viewController as? ActiveCampaignViewControllerSpy else {
            XCTFail("Fail trying to cast viewController spy.")
            return
        }
        
        XCTAssertEqual(viewController.campaignItems.count, 4)
        XCTAssertEqual(viewController.didDisplayCampaignData, 1)
    }
    
    func testDidNextStep_WhenActionPopToHome_ShouldPopToCampaignHome() {
        sut.didNextStep(action: .popToHome)
        
        XCTAssertEqual(coordinatorMock.goToCampaignHome, 1)
    }
    
    func testShowError_WhenApiError_ShouldShowError() {
        sut.showError(apiError: .bodyNotFound)
        
        XCTAssertEqual(coordinatorMock.showCampaignError, 1)
    }
    
    func testShowError_WhenRequestError_ShouldShowError() {
        sut.showError(requestError: RequestError.init())
        
        XCTAssertEqual(coordinatorMock.showCampaignError, 1)
    }
}
