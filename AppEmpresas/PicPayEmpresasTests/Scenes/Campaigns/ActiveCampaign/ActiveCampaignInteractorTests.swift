import Core
import XCTest
@testable import PicPayEmpresas

private enum ActiveCampaignServiceResponse {
    case success
    case failure
    case invalidResponse
}

private final class ActiveCampaignPresenterSpy: ActiveCampaignPresenting {
    var viewController: ActiveCampaignDisplay?
    
    private(set) var didShowStatLoading: Int = 0
    private(set) var didShowStopLoading: Int = 0
    private(set) var didShowNextStep: Int = 0
    private(set) var didShowApiError: Int = 0
    private(set) var didShowRequestError: Int = 0
    private(set) var didShowCampaign: Int = 0
    private(set) var campaignResponse: CampaignResponse?
    private(set) var activeCampaignAction: ActiveCampaignAction?
    
    func startLoading() {
        didShowStatLoading += 1
    }
    
    func stopLoading() {
        didShowStopLoading += 1
    }
    
    func showError(apiError: ApiError) {
        didShowApiError += 1
    }
    
    func showError(requestError: RequestError) {
        didShowRequestError += 1
    }
    
    func presentCampaignData(campaignData: CampaignResponse) {
        didShowCampaign += 1
        campaignResponse = campaignData
    }
    
    func didNextStep(action: ActiveCampaignAction) {
        didShowNextStep += 1
        activeCampaignAction = action
    }
}

private final class ActiveCampaignServiceMock: ActiveCampaignServicing {
    var response: ActiveCampaignServiceResponse = .success
    
    let campaignResponse = CampaignResponse(
        id: "5f10be77ff7fe50044454872",
        sellerId: "1",
        type: "first_buy",
        active: true,
        cashback: 10,
        maxValue: 30.5,
        expirationDate: "25/12/2020 13:30")
    
    func deletCampaign(_ campaignId: String, completion: @escaping (Result<CampaignResponse?, ApiError>) -> Void) {
        switch response {
        case .failure:
            completion(.failure(.serverError))
        case .invalidResponse:
            completion(.failure(.badRequest(body: RequestError.init())))
        case .success:
            completion(.success(campaignResponse))
        }
    }
}

final class ActiveCampaignInteractorTests: XCTestCase {
    private let presenterSpy = ActiveCampaignPresenterSpy()
    private let serviceMock = ActiveCampaignServiceMock()
    
    let campaignResponse = CampaignResponse(
        id: "5f10be77ff7fe50044454872",
        sellerId: "1",
        type: "first_buy",
        active: true,
        cashback: 10,
        maxValue: 30.5,
        expirationDate: "25/12/2020 13:30")
    
    private lazy var sut = ActiveCampaignInteractor(service: serviceMock,
                                                    presenter: presenterSpy,
                                                    campaign: campaignResponse)

    func testConfirmDeleteCampaign_WhenSucces_ShouldDeleteCampaign() {
        serviceMock.response = .success
        
        sut.confirmDeleteCampaign()
        
        XCTAssertNotNil(presenterSpy.activeCampaignAction)
        XCTAssertEqual(presenterSpy.didShowStatLoading, 1)
        XCTAssertEqual(presenterSpy.didShowNextStep, 1)
        XCTAssertEqual(presenterSpy.didShowStopLoading, 1)
    }
    
    func testConfirmDeleteCampaign_WhenFailure_ShouldShowError() {
        serviceMock.response = .failure
        
        sut.confirmDeleteCampaign()
        
        XCTAssertEqual(presenterSpy.didShowStatLoading, 1)
        XCTAssertEqual(presenterSpy.didShowApiError, 1)
        XCTAssertEqual(presenterSpy.didShowStopLoading, 1)
    }
    
    func testConfirmDeleteCampaign_WhenInvalidResponse_ShouldShowError() {
        serviceMock.response = .invalidResponse
        
        sut.confirmDeleteCampaign()
        
        XCTAssertEqual(presenterSpy.didShowStatLoading, 1)
        XCTAssertEqual(presenterSpy.didShowRequestError, 1)
        XCTAssertEqual(presenterSpy.didShowStopLoading, 1)
    }
    
    func testFillCampaignContent_ShouldPresentCampaignData() {
        sut.fillCampaignContent()
        
        XCTAssertNotNil(presenterSpy.campaignResponse)
        XCTAssertEqual(presenterSpy.didShowCampaign, 1)
        XCTAssertEqual(presenterSpy.campaignResponse?.id, "5f10be77ff7fe50044454872")
        XCTAssertEqual(presenterSpy.campaignResponse?.type, "first_buy")
        XCTAssertEqual(presenterSpy.campaignResponse?.cashback, 10)
        XCTAssertEqual(presenterSpy.campaignResponse?.maxValue, 30.5)
        XCTAssertEqual(presenterSpy.campaignResponse?.expirationDate, "25/12/2020 13:30")
    }
}
