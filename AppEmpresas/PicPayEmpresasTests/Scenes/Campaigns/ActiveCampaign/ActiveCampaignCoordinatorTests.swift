import XCTest
@testable import PicPayEmpresas

final class ActiveCampaignCoordinatorTests: XCTestCase {
    private let viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    
    private lazy var sut: ActiveCampaignCoordinator = {
        let coordinator = ActiveCampaignCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testPerformAction_WhenActionGoToTerms_ShouldShowTermsAndConditionsScreen() {
        sut.perform(action: .popToHome)
        
        XCTAssertEqual(navigationSpy.popToRoot, 1)
    }
    
    func testPerformAction_WhenActionCampaignError_ShouldShowCampaignError() {
        sut.perform(action: .deleteCampaignError(modalInfo: AlertModalInfo()))
        
        XCTAssertTrue(viewControllerSpy.presentViewController is PopupViewController)
    }
}
