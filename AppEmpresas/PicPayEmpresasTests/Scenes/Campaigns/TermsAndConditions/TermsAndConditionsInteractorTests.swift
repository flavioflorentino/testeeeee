import WebKit
import XCTest
@testable import PicPayEmpresas

private final class TermsAndConditionsPresenterSpy: TermsAndConditionsPresenting {
    var viewController: TermsAndConditionsDisplay?
    private(set) var didLoadWebPage = 0
    private(set) var didWebPageFinishedLoading = 0
    private(set) var request: URLRequest?

    func loadWebPage(for request: URLRequest) {
        didLoadWebPage += 1
        self.request = request
    }

    func webPageFinishedLoading() {
        didWebPageFinishedLoading += 1
    }
}

final class TermsAndConditionsInteractorTests: XCTestCase {
    private let presenterSpy = TermsAndConditionsPresenterSpy()

    private lazy var sut = TermsAndConditionsInteractor(presenter: presenterSpy)

    func testLoadWebPage_WhenSuccess_ShouldLoadWebPage() {
        sut.loadWebPage()

        XCTAssertEqual(presenterSpy.didLoadWebPage, 1)
    }

    func testLoadWebPage_WhenSuccess_ShouldCreateURLResquest() {
        sut.loadWebPage()

        XCTAssertEqual(presenterSpy.didLoadWebPage, 1)
    }

    func testWebViewDidFinishLoading_WhenSuccess_ShouldWebPageFinishedLoading() {
        sut.webViewDidFinishLoading(WKWebView())

        XCTAssertEqual(presenterSpy.didWebPageFinishedLoading, 1)
    }
}
