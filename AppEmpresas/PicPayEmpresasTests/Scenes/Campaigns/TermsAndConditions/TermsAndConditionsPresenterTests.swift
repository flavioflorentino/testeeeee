import UI
import XCTest
@testable import PicPayEmpresas

private final class TermsAndConditionsDisplaySpy: TermsAndConditionsDisplay {
    var loadingView: LoadingView = LoadingView()

    private(set) var didStartLoadingView = 0
    private(set) var didDisplayWebPage = 0
    private(set) var didStopLoadingView = 0

    func startLoadingView() {
        didStartLoadingView += 1
    }

    func displayWebPage(_ request: URLRequest) {
        didDisplayWebPage += 1
    }

    func stopLoadingView() {
        didStopLoadingView += 1
    }
}

final class TermsAndConditionsPresenterTests: XCTestCase {
    private let viewControllerSpy = TermsAndConditionsDisplaySpy()

    private lazy var sut: TermsAndConditionsPresenter = {
        let presenter = TermsAndConditionsPresenter()
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testLoadWebPage_WhenSuccess_ShouldStartLoadingView() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        sut.loadWebPage(for: URLRequest(url: url))

        XCTAssertEqual(viewControllerSpy.didStartLoadingView, 1)
    }

    func testLoadWebPage_WhenSuccess_ShouldDisplayWebPage() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        sut.loadWebPage(for: URLRequest(url: url))

        XCTAssertEqual(viewControllerSpy.didDisplayWebPage, 1)
    }

    func testWebPageFinishedLoading_WhenSuccess_ShouldStopLoadingView() {
        sut.webPageFinishedLoading()

        XCTAssertEqual(viewControllerSpy.didStopLoadingView, 1)
    }
}
