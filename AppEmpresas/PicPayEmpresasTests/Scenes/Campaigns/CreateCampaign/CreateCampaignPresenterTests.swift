import XCTest
@testable import PicPayEmpresas

private final class CreateCampaignCoordinatorMock: CreateCampaignCoordinating {
    var viewController: UIViewController?
    
    private(set) var goToConfirmCampaignScreen = 0
    private(set) var createCampaignAction: CreateCampaignAction?
    
    func perform(action: CreateCampaignAction) {
        guard case .confirmCampaign = action else {
            XCTFail()
            return
        }
        
        createCampaignAction = action
        goToConfirmCampaignScreen += 1
    }
}

final class CreateCampaignPresenterTests: XCTestCase {
    private let coordinatorMock = CreateCampaignCoordinatorMock()
    
    private lazy var sut = CreateCampaignPresenter(coordinator: coordinatorMock)
    
    func testDidNextStep_WhenActionConfirmCampaign_ShouldGoToConfirmCampaign() {
        let campaign = Campaign(type: .firstBuy, cashback: "", maxValue: "", expirationDate: "")
        
        sut.didNextStep(action: .confirmCampaign(campaign))
        
        XCTAssertNotNil(coordinatorMock.createCampaignAction)
        XCTAssertEqual(coordinatorMock.goToConfirmCampaignScreen, 1)
    }
}
