import XCTest
@testable import PicPayEmpresas

private final class CreateCampaignPresenterSpy: CreateCampaignPresenting {
    var viewController: CreateCampaignDisplay?
    
    private(set) var didShowDurationOptions: Int = 0
    private(set) var didShowNextStep: Int = 0
    private(set) var didShowEnableContinueButton = 0
    private(set) var didShowPercentError: Int = 0
    private(set) var didShowMaxValueError: Int = 0
    private(set) var didShowPercentDefaultState: Int = 0
    private(set) var didShowMaxValueDefaultState: Int = 0
    private(set) var durationOptions: [DurationOptions] = []
    private(set) var createCampaignAction: CreateCampaignAction?
    private(set) var isContinueButtonEnabled = false
    
    func presentDurationOptions() {
        let options = [
            DurationOptions(
                rowTitle: Strings.CreateCampaign.sevenDays,
                rowValue: Strings.CreateCampaign.sevenDays),
            DurationOptions(
                rowTitle: Strings.CreateCampaign.fifteenDays,
                rowValue: Strings.CreateCampaign.fifteenDays),
            DurationOptions(
                rowTitle: Strings.CreateCampaign.thirtyDays,
                rowValue: Strings.CreateCampaign.thirtyDays)
        ]
        didShowDurationOptions += 1
        durationOptions = options
    }
    
    func didNextStep(action: CreateCampaignAction) {
        didShowNextStep += 1
        createCampaignAction = action
    }
    
    func enableContinueButton(_ isEnabled: Bool) {
        didShowEnableContinueButton += 1
        isContinueButtonEnabled = isEnabled
    }
    
    func presentPercentageError() {
        didShowPercentError += 1
    }
    
    func presentMaxValueError(){
        didShowMaxValueError += 1
    }
    
    func presentPercentageItemDefaultState() {
        didShowPercentDefaultState += 1
    }
    
    func presentMaxValueItemDefaultState() {
        didShowMaxValueDefaultState += 1
    }
}

final class CreateCampaignInteractorTests: XCTestCase {
    private let presenterSpy = CreateCampaignPresenterSpy()
    
    private lazy var sut = CreateCampaignInteractor(presenter: presenterSpy, campaignType: .firstBuy)

    func testGetCampaignDurationOptions_WhenSuccess_ShouldReturnCampaignDurationOptions() {
        sut.obtainCampaignDurationOptions()
        
        XCTAssertEqual(presenterSpy.didShowDurationOptions, 1)
        XCTAssertEqual(presenterSpy.durationOptions.count, 3)
        XCTAssertEqual(presenterSpy.durationOptions.first?.rowTitle, "7 dias")
        XCTAssertEqual(presenterSpy.durationOptions.last?.rowTitle, "30 dias")
    }
    
    func testVerifyFieldsContent_WhenMaxPercentageReached_ShouldPresentPercentageError() {
        sut.verifyFieldsContent(percentage: "41", maxValue: "", duration: "")
        
        XCTAssertEqual(presenterSpy.didShowPercentError, 1)
        XCTAssertEqual(presenterSpy.isContinueButtonEnabled, false)
    }
    
    func testVerifyFieldsContent_WhenMaxValueReached_ShouldPresentMaxValueError() {
        sut.verifyFieldsContent(percentage: "", maxValue: "50100", duration: "")
        
        XCTAssertEqual(presenterSpy.didShowMaxValueError, 1)
        XCTAssertEqual(presenterSpy.isContinueButtonEnabled, false)
    }
    
    func testVerifyFieldsContent_WhenPercentageUnderLimits_ShouldPresentPercentageDefaultState() {
        sut.verifyFieldsContent(percentage: "41", maxValue: "", duration: "")
        
        XCTAssertEqual(presenterSpy.didShowPercentError, 1)
        
        sut.verifyFieldsContent(percentage: "40", maxValue: "", duration: "")
        
        XCTAssertEqual(presenterSpy.didShowPercentDefaultState, 1)
        XCTAssertEqual(presenterSpy.isContinueButtonEnabled, false)
    }
    
    func testVerifyFieldsContent_WhenMaxValueUnderLimits_ShouldPresentMaxValueDefaultState() {
        sut.verifyFieldsContent(percentage: "", maxValue: "50100", duration: "")
        
        XCTAssertEqual(presenterSpy.didShowMaxValueError, 1)
        
        sut.verifyFieldsContent(percentage: "", maxValue: "50000", duration: "")
        
        XCTAssertEqual(presenterSpy.didShowMaxValueDefaultState, 1)
        XCTAssertEqual(presenterSpy.isContinueButtonEnabled, false)
    }
    
    func testVerifyFieldsContent_WhenSuccess_ShouldEnableContinueButton() {
        sut.verifyFieldsContent(percentage: "40", maxValue: "50000", duration: "7 dias")
        
        XCTAssertEqual(presenterSpy.didShowEnableContinueButton, 1)
        XCTAssertEqual(presenterSpy.isContinueButtonEnabled, true)
    }
    
    func testContinueToCreateCampaign_WhenButtonPressed_ShouldGoToConfirmCampaign() {
        sut.continueToCreateCampaign(percentage: "", maxValue: "", duration: "")
        
        XCTAssertNotNil(presenterSpy.createCampaignAction)
        XCTAssertEqual(presenterSpy.didShowNextStep, 1)
    }
}
