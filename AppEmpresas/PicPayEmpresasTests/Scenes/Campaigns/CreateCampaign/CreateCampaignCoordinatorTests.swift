import XCTest
@testable import PicPayEmpresas

final class CreateCampaignCoordinatorTests: XCTestCase {
    private let viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    
    private lazy var sut: CreateCampaignCoordinator = {
        let coordinator = CreateCampaignCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testPerformAction_WhenActionConfirmCampaing_ShouldShowConfirmCampaignScreen() {
        let campaign = Campaign(type: .firstBuy,
                                        cashback: "10",
                                        maxValue: "R$ 10,00",
                                        expirationDate: "15 dias")
        
        sut.perform(action: .confirmCampaign(campaign))
        
        XCTAssertTrue(navigationSpy.currentViewController is ConfirmCampaignViewController)
    }
}
