import XCTest
@testable import PicPayEmpresas

final private class FacialBiometricsTakePresenterSpy: FacialBiometricsTakePresenting {
    var viewController: FacialBiometricsTakeDisplay?
    
    private(set) var actionSelected: FacialBiometricsTakeAction?
    private(set) var actionCount = 0
    private(set) var handleHideComponents = 0
    private(set) var handleShowComponents = 0
    
    func didNextStep(action: FacialBiometricsTakeAction) {
        actionSelected = action
        actionCount += 1
    }
    
    func hideComponents() {
        handleHideComponents += 1
    }
    
    func showComponents() {
        handleShowComponents += 1
    }
}

final class FacialBiometricsTakeViewModelTests: XCTestCase {
    private let presenterSpy = FacialBiometricsTakePresenterSpy()
    private lazy var sut = FacialBiometricsTakeViewModel(presenter: presenterSpy)
    
    func testDismiss_WhenReceiveDismissFromViewController_ShouldDismissViewController() {
        sut.dismiss()
        
        XCTAssertEqual(presenterSpy.actionSelected, .dismiss)
        XCTAssertEqual(presenterSpy.actionCount, 1)
    }
    
    func testShowComponents_WhenReceiveShowComponentsFromViewController_ShouldShowComponents() {
        sut.showComponents()
        
        XCTAssertEqual(presenterSpy.handleShowComponents, 1)
    }
    
    func testHideComponents_WhenReceiveHideComponentsFromViewController_ShouldHideComponents() {
        sut.hideComponents()
        
        XCTAssertEqual(presenterSpy.handleHideComponents, 1)
    }
}
