import XCTest
@testable import PicPayEmpresas

private final class FacialBiometricsTakeCoordinatorSpy: FacialBiometricsTakeCoordinating {
    
    var viewController: UIViewController?
    
    private(set) var actionSelected: FacialBiometricsTakeAction?
    private(set) var actionCount = 0
    
    func perform(action: FacialBiometricsTakeAction) {
        actionSelected = action
        actionCount += 1
    }
}

private final class FacialBiometricsTakeDisplaySpy: FacialBiometricsTakeDisplay {
    
    private(set) var handleShowComponents = 0
    private(set) var handleHideComponents = 0
    
    func showComponents() {
        handleShowComponents += 1
    }
    
    func hideComponents() {
        handleHideComponents += 1
    }
}

final class FacialBiometricsTakePresenterTests: XCTestCase {
    private let coordinatorSpy =  FacialBiometricsTakeCoordinatorSpy()
    private let displaySpy = FacialBiometricsTakeDisplaySpy()
    
    private lazy var sut: FacialBiometricsTakePresenter = {
        let presenter = FacialBiometricsTakePresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testDidNextStep_WhenReceiveCheckFromViewModel_ShouldPushNewViewController(){
        let imageMock = UIImage()
        sut.didNextStep(action: .check(imageMock))
        
        XCTAssertEqual(coordinatorSpy.actionSelected, .check(imageMock))
        XCTAssertEqual(coordinatorSpy.actionCount, 1)
    }
    
    func testDidNextStep_WhenReceiveDismissFromViewModel_ShouldDismissViewController(){
        sut.didNextStep(action: .dismiss)
        
        XCTAssertEqual(coordinatorSpy.actionSelected, .dismiss)
        XCTAssertEqual(coordinatorSpy.actionCount, 1)
    }
    
    func testShowComponents_WhenReceiveShowComponentsFromViewModel_ShouldShowComponents() {
        sut.showComponents()
        
        XCTAssertEqual(displaySpy.handleShowComponents, 1)
    }
    
    func testHideComponents_WhenReceiveHideComponentsFromViewModel_ShouldHideComponents() {
        sut.hideComponents()
        
        XCTAssertEqual(displaySpy.handleHideComponents, 1)
    }
}
