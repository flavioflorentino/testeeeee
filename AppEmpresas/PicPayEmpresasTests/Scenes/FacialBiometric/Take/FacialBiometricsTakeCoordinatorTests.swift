import UIKit
import XCTest

@testable import PicPayEmpresas

final class FacialBiometricsTakeCoordinatorTests: XCTestCase {
    private var viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    
    private lazy var sut: FacialBiometricsTakeCoordinator = {
        let coordinator = FacialBiometricsTakeCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenReceiveCheckFromPresenter_ShouldPushViewController() {
        let imageMock = UIImage()
        sut.perform(action: .check(imageMock))
        
        XCTAssertTrue(navigationSpy.currentViewController is FacialBiometricsCheckingViewController)
        // Must be 2 because pushViewController is called when we set rootViewController for the navigationController
        XCTAssertEqual(navigationSpy.pushedCount, 2)
    }
    
    func testPerform_WhenReceiveDismissFromPresenter_ShouldDismissViewController() {
        sut.perform(action: .dismiss)
        
        XCTAssertEqual(viewControllerSpy.dismissCount, 1)
    }
    
    func testPerform_WhenReceiveErrorFromPresenter_ShouldPushViewController() {
        sut.perform(action: .error)
        
        XCTAssertTrue(navigationSpy.currentViewController is FacialBiometricsStatusViewController)
        // Must be 2 because pushViewController is called when we set rootViewController for the navigationController
        XCTAssertEqual(navigationSpy.pushedCount, 2)
    }
}
