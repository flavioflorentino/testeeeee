import XCTest
@testable import PicPayEmpresas

final class FacialBiometricsCheckingMock {
    func getUploadAcceped() -> FacialBiometricsUploadResponse? {
        LocalMock<FacialBiometricsUploadResponse>().pureLocalMock("biometry_upload_accepted")
    }

    func getUploadDenied() -> FacialBiometricsUploadResponse? {
        LocalMock<FacialBiometricsUploadResponse>().pureLocalMock("biometry_upload_denied")
    }

    func getUploadRequired() -> FacialBiometricsUploadResponse? {
        LocalMock<FacialBiometricsUploadResponse>().pureLocalMock("biometry_upload_required")
    }

    func getUploadRetry() -> FacialBiometricsUploadResponse? {
        LocalMock<FacialBiometricsUploadResponse>().pureLocalMock("biometry_upload_retry")
    }

    func getUploadPending() -> FacialBiometricsUploadResponse? {
        LocalMock<FacialBiometricsUploadResponse>().pureLocalMock("biometry_upload_pending")
    }

    func getUploadStatusError() -> FacialBiometricsUploadResponse? {
        LocalMock<FacialBiometricsUploadResponse>().pureLocalMock("biometry_upload_status_error")
    }

    func getUploadError() -> FacialBiometricsUploadResponse? {
        LocalMock<FacialBiometricsUploadResponse>().pureLocalMock("biometry_upload_error")
    }
}
