import XCTest
@testable import PicPayEmpresas

final private class FacialBiometricsCheckingCoordinatorSpy: FacialBiometricsCheckingCoordinating {
    var viewController: UIViewController?

    private(set) var actionSelected: FacialBiometricsCheckingAction?
    private(set) var actionCount = 0

    func perform(action: FacialBiometricsCheckingAction) {
        actionSelected = action
        actionCount += 1
    }
}

final private class FacialBiometricsCheckingDispaySpy: FacialBiometricsCheckingDisplay {

    private(set) var handleDisplayLoader = 0
    private(set) var handleDismissLoader = 0
    private(set) var image: UIImage?
    private(set) var imageCount = 0

    func displayImage(_ image: UIImage) {
        self.image = image
        imageCount += 1
    }

    func displayLoader() {
        handleDisplayLoader += 1
    }

    func dismissLoader() {
        handleDismissLoader += 1
    }
}

final class FacialBiometricsCheckingPresenterTests: XCTestCase {
    private let coordinatorSpy =  FacialBiometricsCheckingCoordinatorSpy()
    private let displaySpy = FacialBiometricsCheckingDispaySpy()

    private lazy var sut: FacialBiometricsCheckingPresenter = {
        let presenter = FacialBiometricsCheckingPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()

    func testDidNextStep_WhenReceiveDismissFromViewModel_ShouldPopNavigationController() {
        stepStatus(.dismiss)
    }

    func testDidNextStep_WhenReceiveStatusAcceptedFromViewModel_ShouldPushNewViewController() {
        stepStatus(.status(.accepted))
    }

    func testDidNextStep_WhenReceiveStatusDeniedFromViewModel_ShouldPushNewViewController() {
        stepStatus(.status(.denied))
    }

    func testDidNextStep_WhenReceiveStatusPendingFromViewModel_ShouldPushNewViewController() {
        stepStatus(.status(.pending))
    }

    func testDidNextStep_WhenReceiveStatusRequiredFromViewModel_ShouldPushNewViewController() {
        stepStatus(.status(.required))
    }

    func testDidNextStep_WhenReceiveStatusRetryFromViewModel_ShouldPushNewViewController() {
        stepStatus(.status(.retry))
    }

    private func stepStatus(_ status: FacialBiometricsCheckingAction) {
        sut.didNextStep(action: status)

        XCTAssertEqual(coordinatorSpy.actionSelected, status)
        XCTAssertEqual(coordinatorSpy.actionCount, 1)
    }

    func testGeneratedQrImageFailure_WhenNotReceiveCodeFromViewModel_ShouldUIImageIsNil() {
        let imageMock = UIImage()
        sut.displayImage(imageMock)

        XCTAssertEqual(displaySpy.image, imageMock)
        XCTAssertEqual(displaySpy.imageCount, 1)
    }

    func testGeneratedQrImage_WhenReceiveCodeFromViewModel_ShouldReturnUIImage() {
        sut.dismissLoader()

        XCTAssertEqual(displaySpy.handleDismissLoader, 1)
    }

    func testHandleError_WhenReceiveErrorFromViewModel_ShouldShowErrorQRView() {
        sut.displayLoader()

        XCTAssertEqual(displaySpy.handleDisplayLoader, 1)
    }
}
