import Core
import LegacyPJ
import XCTest
@testable import PicPayEmpresas

final private class FacialBiometricsCheckingPresenterSpy: FacialBiometricsCheckingPresenting {
    var viewController: FacialBiometricsCheckingDisplay?

    private(set) var actionSelected: FacialBiometricsCheckingAction?
    private(set) var actionCount = 0
    private(set) var displayedImage: UIImage?
    private(set) var displayedImageCount = 0
    private(set) var handleDisplayLoader = 0
    private(set) var handleDismissLoader = 0

    func displayImage(_ image: UIImage) {
        displayedImage = image
        displayedImageCount += 1
    }

    func didNextStep(action: FacialBiometricsCheckingAction) {
        actionSelected = action
        actionCount += 1
    }

    func displayLoader() {
        handleDisplayLoader += 1
    }

    func dismissLoader() {
        handleDismissLoader += 1
    }
}

final private class FacialBiometricsCheckingServiceMock: FacialBiometricsCheckingServicing {
    var isSuccess = true
    var status: FacialBiometricsStatus = .accepted

    func sendPicture(image: UIImage, completion: @escaping (Result<FacialBiometricsUploadResponse, ApiError>) -> Void) {
        guard isSuccess, let mock = status.mock else {
            completion(.failure(ApiError.serverError))
            return
        }

        completion(.success(mock))
    }
}

private extension FacialBiometricsStatus {
    var mock: FacialBiometricsUploadResponse? {
        let mock = FacialBiometricsCheckingMock()
        switch self {
        case .required:
            return mock.getUploadRequired()
        case .accepted:
            return mock.getUploadAcceped()
        case .retry:
            return mock.getUploadRetry()
        case .denied:
            return mock.getUploadDenied()
        case .pending:
            return mock.getUploadPending()
        case .statusError:
            return mock.getUploadStatusError()
        case .uploadError:
            return mock.getUploadError()
        }
    }
}


final class FacialBiometricsCheckingViewModelTests: XCTestCase {
    private let serviceMock = FacialBiometricsCheckingServiceMock()
    private let presenterSpy = FacialBiometricsCheckingPresenterSpy()
    private let imageMock = UIImage()
    private lazy var sut = FacialBiometricsCheckingViewModel(service: serviceMock, presenter: presenterSpy, dependencies: DependencyContainerMock(), image: imageMock)

    func testDismiss_WhenReceiveDismissFromViewController_ShouldPopNavigationController() {
        sut.dismiss()

        XCTAssertEqual(presenterSpy.actionSelected, .dismiss)
        XCTAssertEqual(presenterSpy.actionCount, 1)
    }

    func testRetrieveImage_WhenReceiveRetrieveImageFromViewController_ShouldReturnAnImage() {
        sut.retrieveImage()

        XCTAssertEqual(presenterSpy.displayedImage, imageMock)
        XCTAssertEqual(presenterSpy.displayedImageCount, 1)
    }

    func testCallUploadImage_WhenReceiveCallFromViewController_ShouldSendToStatusAccepted() {
        callUploadImage(with: .accepted, action: .status(.accepted))
    }

    func testCallUploadImage_WhenReceiveCallFromViewController_ShouldSendToStatusDenied() {
        callUploadImage(with: .denied, action: .status(.denied))
    }

    func testCallUploadImage_WhenReceiveCallFromViewController_ShouldSendToStatusRequired() {
        callUploadImage(with: .required, action: .status(.required))
    }

    func testCallUploadImage_WhenReceiveCallFromViewController_ShouldSendToStatusRetry() {
        callUploadImage(with: .retry, action: .status(.retry))
    }

    func testCallUploadImage_WhenReceiveCallFromViewController_ShouldSendToStatusPending() {
        callUploadImage(with: .pending, action: .status(.pending))
    }

    func testCallUploadImage_WhenReceiveCallFromViewController_ShouldSendToStatusError() {
        callUploadImage(with: .statusError, action: .status(.statusError))
    }

    func testCallUploadImage_WhenReceiveCallFromViewController_ShouldSendToStatusUploadError() {
        callUploadImage(with: .uploadError, action: .status(.uploadError))
    }

    func callUploadImage(with status: FacialBiometricsStatus, action: FacialBiometricsCheckingAction) {
        serviceMock.isSuccess = true
        serviceMock.status = status

        sut.sendImage()

        XCTAssertEqual(presenterSpy.handleDisplayLoader, 1)
        XCTAssertEqual(presenterSpy.handleDismissLoader, 1)
        XCTAssertEqual(presenterSpy.actionSelected, action)
        XCTAssertEqual(presenterSpy.actionCount, 1)
    }

    func testCallUploadImage_WhenReceiveCallFromViewController_ShouldFailure() {
        serviceMock.isSuccess = false

        sut.sendImage()

        XCTAssertEqual(presenterSpy.handleDisplayLoader, 1)
        XCTAssertEqual(presenterSpy.handleDismissLoader, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .status(.uploadError))
        XCTAssertEqual(presenterSpy.actionCount, 1)
    }
}
