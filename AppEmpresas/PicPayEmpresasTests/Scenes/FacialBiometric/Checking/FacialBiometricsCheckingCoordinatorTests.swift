import LegacyPJ
import XCTest
@testable import PicPayEmpresas

final class FacialBiometricsCheckingCoordinatorTests: XCTestCase {
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: UIViewController())

    private lazy var sut: FacialBiometricsCheckingCoordinator = {
        let coordinator = FacialBiometricsCheckingCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()

    func testPerform_WhenReceiveDismissFromPresenter_ShouldPopNavigationController() {
        sut.perform(action: .dismiss)

        XCTAssertEqual(navigationSpy.popedViewControllerCount, 1)
    }

    func testPerform_WhenReceiveStatusAcceptedFromPresenter_ShouldPushViewController() {
        performStatus(.accepted)
    }

    func testPerform_WhenReceiveStatusDeniedFromPresenter_ShouldPushViewController() {
        performStatus(.denied)
    }

    func testPerform_WhenReceiveStatusPendingFromPresenter_ShouldPushViewController() {
        performStatus(.pending)
    }

    func testPerform_WhenReceiveStatusRequiredFromPresenter_ShouldPushViewController() {
        performStatus(.required)
    }

    func testPerform_WhenReceiveStatusRetryFromPresenter_ShouldPushViewController() {
        performStatus(.retry)
    }

    private func performStatus(_ status: FacialBiometricsStatus) {
        sut.perform(action: .status(status))

        XCTAssertTrue(navigationSpy.currentViewController is FacialBiometricsStatusViewController)
        // Must be 2 because pushViewController is called when we set rootViewController for the navigationController
        XCTAssertEqual(navigationSpy.pushedCount, 2)
    }
}
