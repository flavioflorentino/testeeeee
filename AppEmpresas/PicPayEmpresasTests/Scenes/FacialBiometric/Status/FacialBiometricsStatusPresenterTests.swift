import LegacyPJ
import XCTest
@testable import PicPayEmpresas

final private class FacialBiometricsStatusCoordinatorSpy: FacialBiometricsStatusCoordinating {
    var viewController: UIViewController?

    private(set) var actionSelected: FacialBiometricsStatusAction?
    private(set) var actionCount = 0

    func perform(action: FacialBiometricsStatusAction) {
        actionSelected = action
        actionCount += 1
    }
}

final private class FacialBiometricsStatusDisplaySpy: FacialBiometricsStatusDisplay {
    private(set) var feedback: FacialBiometricsFeedback?
    private(set) var handleDisplayComponentsCount = 0

    func displayComponents(_ feedback: FacialBiometricsFeedback) {
        self.feedback = feedback
        handleDisplayComponentsCount += 1
    }
}

final class FacialBiometricsStatusPresenterTests: XCTestCase {
    private let coordinatorSpy =  FacialBiometricsStatusCoordinatorSpy()
    private let displaySpy = FacialBiometricsStatusDisplaySpy()

    private lazy var sut: FacialBiometricsStatusPresenter = {
        let presenter = FacialBiometricsStatusPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()

    func testDidNextStep_WhenReceiveActionFromViewModel_ShouldPopToRootController() {
        sut.didNextStep(action: .retry)

        XCTAssertEqual(coordinatorSpy.actionSelected, .retry)
        XCTAssertEqual(coordinatorSpy.actionCount, 1)
    }

    func testDisplay_WhenReceiveActionFromViewModel_ShouldDisplayStatusAccepted() {
        let feedbackMock = FacialBiometricsFeedback(
            hasCloseButton: false,
            image: Assets.imgLike.image,
            title: "Sua conta foi criada",
            description: "Tudo certo com sua foto e com os dados cadastrados. Obrigado por escolher o PicPay.",
            actionTitle: "Acessar minha conta"
        )

        testDisplayStatus(status: .accepted, feedback: feedbackMock)
    }

    func testDisplay_WhenReceiveActionFromViewModel_ShouldDisplayStatusRetry() {
        let tipsMock = [
            FacialBiometrisFeedbackTip(icon: Assets.icoLamp.image, description: "Fique em um local bem iluminado para a foto sair com qualidade."),
            FacialBiometrisFeedbackTip(icon: Assets.icoAvoidAccessories.image, description: "Evite usar acessórios como óculos, bonés e chapéus."),
            FacialBiometrisFeedbackTip(icon: Assets.icoNeutralExpression.image, description: "Lembre-se de ficar com uma expressão neutra, sem fazer caretas."),
            FacialBiometrisFeedbackTip(icon: Assets.iluPersonPlaceholder.image, description: "A foto precisa ser da pessoa responsável pela empresa, a mesma que teve os dados cadastrados")
        ]
        let feedbackMock = FacialBiometricsFeedback(
            title: "Opa, a foto não ficou boa",
            subtitle: "Veja as dicas e tire mais uma foto:",
            tips: tipsMock,
            actionTitle: "Tirar outra foto"
        )

        testDisplayStatus(status: .retry, feedback: feedbackMock)
    }

    func testDisplay_WhenReceiveActionFromViewModel_ShouldDisplayStatusDenied() {
        let feedbackMock = FacialBiometricsFeedback(
            image: Assets.imgMail.image,
            title: "O cadastro de sua empresa foi recusado",
            description: "Encontramos algumas divergências nos dados e, infelizmente, não será possível prosseguir com o cadastro de sua empresa.",
            actionTitle: "Ok, entedi"
        )

        testDisplayStatus(status: .denied, feedback: feedbackMock)
    }

    func testDisplay_WhenReceiveActionFromViewModel_ShouldDisplayStatusPending() {
        let feedbackMock = FacialBiometricsFeedback(
            image: Assets.imgClock.image,
            title: "Analisando seus dados",
            description: "Não se preocupe, já recebemos sua foto e seus dados. Assim que nossa equipe finalizar a análise, você receberá uma notificação."
        )

        testDisplayStatus(status: .pending, feedback: feedbackMock)
    }

    func testDisplay_WhenReceiveActionFromViewModel_ShouldDisplayStatusError() {
        let feedbackMock = FacialBiometricsFeedback(
            image: Assets.imgTool.image,
            title: "Opa! Erramos por aqui",
            description: "Tivemos uma falha no sistema e não conseguimos gerar o processo de validação da sua identidade.",
            actionTitle: "Tentar novamente"
        )

        testDisplayStatus(status: .statusError, feedback: feedbackMock)
    }

    func testDisplay_WhenReceiveActionFromViewModel_ShouldDisplayUploadError() {
        let feedbackMock = FacialBiometricsFeedback(
            image: Assets.imgTool.image,
            title: "Opa! Erramos por aqui",
            description: "Tivemos uma falha no sistema e não conseguimos validar sua identidade.",
            actionTitle: "Tirar outra foto"
        )

        testDisplayStatus(status: .uploadError, feedback: feedbackMock)
    }

    private func testDisplayStatus(status: FacialBiometricsStatus,
                                   feedback: FacialBiometricsFeedback) {
        sut.displayStatus(status)

        XCTAssertEqual(displaySpy.handleDisplayComponentsCount, 1)
        XCTAssertNotNil(displaySpy.feedback)
        XCTAssertEqual(displaySpy.feedback, feedback)
        XCTAssertNil(coordinatorSpy.actionSelected)
    }
}
