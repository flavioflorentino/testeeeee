import LegacyPJ
import XCTest
@testable import PicPayEmpresas

final private class FacialBiometricsStatusPresenterSpy: FacialBiometricsStatusPresenting {
    var viewController: FacialBiometricsStatusDisplay?

    private(set) var actionSelected: FacialBiometricsStatusAction?
    private(set) var actionCount = 0
    private(set) var statusSelected: FacialBiometricsStatus?
    private(set) var statusCount = 0

    func didNextStep(action: FacialBiometricsStatusAction) {
        actionSelected = action
        actionCount += 1
    }

    func displayStatus(_ status: FacialBiometricsStatus) {
        statusSelected = status
        statusCount += 1
    }
}

final class FacialBiometricsStatusViewModelTests: XCTestCase {
    private let presenterSpy = FacialBiometricsStatusPresenterSpy()
    private lazy var sutAccepted = FacialBiometricsStatusViewModel(
        presenter: presenterSpy,
        dependencies: DependencyContainerMock(),
        status: .accepted
    )
    private lazy var sutDenied = FacialBiometricsStatusViewModel(
        presenter: presenterSpy,
        dependencies: DependencyContainerMock(),
        status: .denied
    )
    private lazy var sutRetry = FacialBiometricsStatusViewModel(
        presenter: presenterSpy,
        dependencies: DependencyContainerMock(),
        status: .retry
    )
    private lazy var sutPending = FacialBiometricsStatusViewModel(
        presenter: presenterSpy,
        dependencies: DependencyContainerMock(),
        status: .pending
    )
    private lazy var sutRequired = FacialBiometricsStatusViewModel(
        presenter: presenterSpy,
        dependencies: DependencyContainerMock(),
        status: .required
    )

    func testDidNextStep_WhenReceiveAcceptedFromViewController_ShouldPushNewViewController() {
        sutAccepted.didNextStep()

        XCTAssertEqual(presenterSpy.actionSelected, .conclude)
        XCTAssertEqual(presenterSpy.actionCount, 1)
    }

    func testDidNextStep_WhenReceiveRetryFromViewController_ShouldPushNewViewController() {
        sutRetry.didNextStep()

        XCTAssertEqual(presenterSpy.actionSelected, .retry)
        XCTAssertEqual(presenterSpy.actionCount, 1)
    }

    func testDidNextStep_WhenReceivePendingFromViewController_ShouldDoNothing() {
        sutPending.didNextStep()

        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.actionCount, 0)
    }

    func testDidNextStep_WhenReceiveRequiredFromViewController_ShouldDoNothing() {
        sutRequired.didNextStep()

        XCTAssertNil(presenterSpy.actionSelected)
        XCTAssertEqual(presenterSpy.actionCount, 0)
    }

    func testDisplayStatusAccepted_WhenReceiveActionFromViewController_ShouldDisplayInView() {
        sutAccepted.retrieveStatus()

        XCTAssertEqual(presenterSpy.statusSelected, .accepted)
        XCTAssertEqual(presenterSpy.statusCount, 1)
    }

    func testDisplayStatusRetry_WhenReceiveActionFromViewController_ShouldDisplayInView() {
        sutRetry.retrieveStatus()

        XCTAssertEqual(presenterSpy.statusSelected, .retry)
        XCTAssertEqual(presenterSpy.statusCount, 1)
    }

    func testDisplayStatusPending_WhenReceiveActionFromViewController_ShouldDisplayInView() {
        sutPending.retrieveStatus()

        XCTAssertEqual(presenterSpy.statusSelected, .pending)
        XCTAssertEqual(presenterSpy.statusCount, 1)
    }

    func testDisplayStatusRequired_WhenReceiveActionFromViewController_ShouldDoNothing() {
        sutRequired.retrieveStatus()

        XCTAssertEqual(presenterSpy.statusSelected, .required)
        XCTAssertEqual(presenterSpy.statusCount, 1)
    }
}
