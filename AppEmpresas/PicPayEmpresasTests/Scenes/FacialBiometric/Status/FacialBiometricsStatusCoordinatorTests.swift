import XCTest
@testable import PicPayEmpresas

final class FacialBiometricsStatusCoordinatorTests: XCTestCase {
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: UIViewController())

    private lazy var sut: FacialBiometricsStatusCoordinator = {
        let coordinator = FacialBiometricsStatusCoordinator(dependencies: DependencyContainerMock())
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()

    func testPerform_WhenReceiveValueScreenFromPresenter_ShouldPopToRoot() {
        navigationSpy.insertCameraController()
        
        sut.perform(action: .retry)

        XCTAssertEqual(navigationSpy.popToRoot, 1)
    }

    func testPerform_WhenReceiveValueScreenFromPresenter_ShouldPresentViewController() {
        sut.perform(action: .retry)

        XCTAssertTrue(navigationSpy.presentViewController is FacialBiometricsTakeViewController)
        XCTAssertEqual(navigationSpy.presentCount, 1)
    }
}
