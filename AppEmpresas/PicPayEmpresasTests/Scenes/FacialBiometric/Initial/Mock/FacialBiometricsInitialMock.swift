import Core
import XCTest
@testable import PicPayEmpresas

final class FacialBiometricsInitialMock {
    func getStatusAcceped() -> FacialBiometricsStatusResponse? {
        LocalMock<FacialBiometricsStatusResponse>().pureLocalMock("biometry_status_accepted")
    }

    func getStatusDenied() -> FacialBiometricsStatusResponse? {
        LocalMock<FacialBiometricsStatusResponse>().pureLocalMock("biometry_status_denied")
    }

    func getStatusRequired() -> FacialBiometricsStatusResponse? {
        LocalMock<FacialBiometricsStatusResponse>().pureLocalMock("biometry_status_required")
    }

    func getStatusRetry() -> FacialBiometricsStatusResponse? {
        LocalMock<FacialBiometricsStatusResponse>().pureLocalMock("biometry_status_retry")
    }

    func getStatusPending() -> FacialBiometricsStatusResponse? {
        LocalMock<FacialBiometricsStatusResponse>().pureLocalMock("biometry_status_pending")
    }

    func getStatusError() -> FacialBiometricsStatusResponse? {
        LocalMock<FacialBiometricsStatusResponse>().pureLocalMock("biometry_status_error")
    }

    func getStatusUploadError() -> FacialBiometricsStatusResponse? {
        LocalMock<FacialBiometricsStatusResponse>().pureLocalMock("biometry_status_upload_error")
    }
}
