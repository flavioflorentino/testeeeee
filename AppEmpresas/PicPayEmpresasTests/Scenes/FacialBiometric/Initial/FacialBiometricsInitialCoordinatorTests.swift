import LegacyPJ
import UIKit
import XCTest
@testable import PicPayEmpresas

final class FacialBiometricsInitialCoordinatorTests: XCTestCase {
    private lazy var navigationMock = NavigationControllerSpy(rootViewController: UIViewController())

    private lazy var sut: FacialBiometricsInitialCoordinator = {
        let coordinator = FacialBiometricsInitialCoordinator()
        coordinator.viewController = navigationMock.topViewController
        return coordinator
    }()

    func testPerform_WhenReceiveMainFromPresenter_ShouldPushViewController() {
        sut.perform(action: .main)

        XCTAssertTrue(navigationMock.currentViewController is FacialBiometricsMainViewController)
        // Must be 2 because pushViewController is called when we set rootViewController for the navigationController
        XCTAssertEqual(navigationMock.pushedCount, 2)
    }

    func testPerform_WhenReceiveStatusAcceptedFromPresenter_ShouldPushViewController() {
        performStatus(with: .accepted)
    }

    func testPerform_WhenReceiveStatusDeniedFromPresenter_ShouldPushViewController() {
        performStatus(with: .denied)
    }

    func testPerform_WhenReceiveStatusRequiredFromPresenter_ShouldPushViewController() {
        performStatus(with: .required)
    }

    func testPerform_WhenReceiveStatusRetryFromPresenter_ShouldPushViewController() {
        performStatus(with: .retry)
    }

    func testPerform_WhenReceiveStatusPendingFromPresenter_ShouldPushViewController() {
        performStatus(with: .pending)
    }

    func performStatus(with status: FacialBiometricsStatus) {
        sut.perform(action: .status(status))

        XCTAssertTrue(navigationMock.currentViewController is FacialBiometricsStatusViewController)
        // Must be 2 because pushViewController is called when we set rootViewController for the navigationController
        XCTAssertEqual(navigationMock.pushedCount, 2)
    }
}
