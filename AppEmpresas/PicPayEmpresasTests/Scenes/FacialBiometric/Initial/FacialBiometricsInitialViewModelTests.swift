import Core
import LegacyPJ
import XCTest
@testable import PicPayEmpresas

final private class FacialBiometricsInitialPresenterSpy: FacialBiometricsInitialPresenting {
    var viewController: FacialBiometricsInitialDisplay?

    private(set) var actionSelected: FacialBiometricsInitialAction?
    private(set) var actionCount = 0
    private(set) var handleLoading = 0

    func didNextStep(action: FacialBiometricsInitialAction) {
        actionSelected = action
        actionCount += 1
    }

    func startLoading() {
        handleLoading += 1
    }
}

final private class FacialBiometricsInitialServiceMock: FacialBiometricsInitialServicing {

    var isSuccess = true
    var status: FacialBiometricsStatus = .accepted

    func status(completion: @escaping (Result<FacialBiometricsStatusResponse, ApiError>) -> Void) {
        guard isSuccess, let status = status.mock else {
            completion(.failure(ApiError.serverError))
            return
        }

        completion(.success(status))
    }
}

private extension FacialBiometricsStatus {
    var mock: FacialBiometricsStatusResponse? {
        let mock = FacialBiometricsInitialMock()
        switch self {
        case .required:
            return mock.getStatusRequired()
        case .accepted:
            return mock.getStatusAcceped()
        case .retry:
            return mock.getStatusRetry()
        case .denied:
            return mock.getStatusDenied()
        case .pending:
            return mock.getStatusPending()
        case .statusError:
            return mock.getStatusError()
        case .uploadError:
            return mock.getStatusUploadError()
        }
    }
}

final class FacialBiometricsInitialViewModelTests: XCTestCase {
    private let serviceMock = FacialBiometricsInitialServiceMock()
    private let presenterSpy = FacialBiometricsInitialPresenterSpy()
    private let container = DependencyContainerMock()
    private lazy var sut = FacialBiometricsInitialViewModel(service: serviceMock, presenter: presenterSpy, dependencies: container)

    func testCallVerifyStatus_WhenReceiveActionFromViewController_ShouldStartLoading() {
        sut.verifyStatus()

        XCTAssertEqual(presenterSpy.handleLoading, 1)
    }

    func testCallVerifyStatus_WhenReceiveCallFromViewController_ShouldSendToStatusAccepted() {
        callVerifyStatus(with: .accepted, action: .status(.accepted))
    }

    func testCallVerifyStatus_WhenReceiveCallFromViewController_ShouldSendToStatusDenied() {
        callVerifyStatus(with: .denied, action: .status(.denied))
    }

    func testCallVerifyStatus_WhenReceiveCallFromViewController_ShouldSendToStatusRequired() {
        callVerifyStatus(with: .required, action: .main)
    }

    func testCallVerifyStatus_WhenReceiveCallFromViewController_ShouldSendToStatusRetry() {
        callVerifyStatus(with: .retry, action: .status(.retry))
    }

    func callVerifyStatus(with status: FacialBiometricsStatus, action: FacialBiometricsInitialAction) {
        serviceMock.isSuccess = true
        serviceMock.status = status

        sut.verifyStatus()

        XCTAssertEqual(presenterSpy.handleLoading, 1)
        XCTAssertEqual(presenterSpy.actionSelected, action)
        XCTAssertEqual(presenterSpy.actionCount, 1)
        XCTAssertEqual(serviceMock.status, status)
    }

    func testCallVerifyStatus_WhenReceiveCallFromViewController_ShouldOccurAnError() {
        serviceMock.isSuccess = false

        sut.verifyStatus()

        XCTAssertEqual(presenterSpy.handleLoading, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .status(.statusError))
        XCTAssertEqual(presenterSpy.actionCount, 1)
    }
}
