import LegacyPJ
import XCTest
@testable import PicPayEmpresas

private final class FacialBiometricsInitialCoordinatorSpy: FacialBiometricsInitialCoordinating {
    var viewController: UIViewController?

    private(set) var action: FacialBiometricsInitialAction?
    private(set) var performCount: Int = 0

    func perform(action: FacialBiometricsInitialAction) {
        self.action = action
        performCount += 1
    }
}

private final class FacialBiometricsInitialDispaySpy: FacialBiometricsInitialDisplay {

    private(set) var handleLoading = 0

    func startLoading() {
        handleLoading += 1
    }
}

final class FacialBiometricsInitialPresenterTests: XCTestCase {
    private let coordinatorSpy = FacialBiometricsInitialCoordinatorSpy()
    private let displaySpy = FacialBiometricsInitialDispaySpy()

    private lazy var sut: FacialBiometricsInitialPresenter = {
        let presenter = FacialBiometricsInitialPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()

    func testDidNextStep_WhenReceiveMainFromViewModel_ShouldPushWebViewController() {
        sut.didNextStep(action: .main)

        XCTAssertEqual(coordinatorSpy.action, .main)
        XCTAssertEqual(coordinatorSpy.performCount, 1)
    }

    func testDidNextStep_WhenReceiveStatusAcceptedFromViewModel_ShouldPushViewController() {
        didNextStep_Status(with: .accepted)
    }

    func testDidNextStep_WhenReceiveStatusDeniedFromViewModel_ShouldPushViewController() {
        didNextStep_Status(with: .denied)
    }

    func testDidNextStep_WhenReceiveStatusRequiredFromViewModel_ShouldPushViewController() {
        didNextStep_Status(with: .required)
    }

    func testDidNextStep_WhenReceiveStatusRetryFromViewModel_ShouldPushViewController() {
        didNextStep_Status(with: .retry)
    }

    func testDidNextStep_WhenReceiveStatusPendingFromViewModel_ShouldPushViewController() {
        didNextStep_Status(with: .pending)
    }

    func testDidNextStep_WhenReceiveStatusErrorFromViewModel_ShouldPushViewController() {
        didNextStep_Status(with: .statusError)
    }

    func testDidNextStep_WhenReceiveUploadErrorFromViewModel_ShouldPushViewController() {
        didNextStep_Status(with: .uploadError)
    }

    func testRetrieveStatus_WhenReceiveStartLoadingFromViewModel() {
        sut.startLoading()

        XCTAssertEqual(displaySpy.handleLoading, 1)
    }

    func didNextStep_Status(with status: FacialBiometricsStatus) {
        sut.didNextStep(action: .status(status))

        XCTAssertEqual(coordinatorSpy.action, .status(status))
        XCTAssertEqual(coordinatorSpy.performCount, 1)
    }
}
