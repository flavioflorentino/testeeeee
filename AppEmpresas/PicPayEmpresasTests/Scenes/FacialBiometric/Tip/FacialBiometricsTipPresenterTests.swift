import XCTest
@testable import PicPayEmpresas

private final class FacialBiometricsTipCoordinatorSpy: FacialBiometricsTipCoordinating {
    var viewController: UIViewController?
    
    private(set) var actionSelected: FacialBiometricsTipAction?
    private(set) var actionCount = 0
    
    func perform(action: FacialBiometricsTipAction) {
        actionSelected = action
        actionCount += 1
    }
}

private final class FacialBiometricsTipDisplaySpy: FacialBiometricsTipDisplay {
    private(set) var icon: UIImage?
    private(set) var description: String?

    func displayTip(icon: UIImage?, description: String) {
        self.icon = icon
        self.description = description
    }
}

final class FacialBiometricsTipPresenterTests: XCTestCase {
    private let coordinatorSpy =  FacialBiometricsTipCoordinatorSpy()
    private let displaySpy = FacialBiometricsTipDisplaySpy()
    
    private lazy var sut: FacialBiometricsTipPresenter = {
        let presenter = FacialBiometricsTipPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testDidNextStep_WhenReceiveCameraFromViewModel_ShouldPresentNewViewController() {
        sut.didNextStep(action: .camera)
        
        XCTAssertEqual(coordinatorSpy.actionSelected, .camera)
        XCTAssertEqual(coordinatorSpy.actionCount, 1)
    }
}
