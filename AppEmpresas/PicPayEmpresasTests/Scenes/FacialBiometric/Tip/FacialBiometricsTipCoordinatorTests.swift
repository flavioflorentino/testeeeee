import XCTest
@testable import PicPayEmpresas

final class FacialBiometricsTipCoordinatorTests: XCTestCase {
    private lazy var viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    
    private lazy var sut: FacialBiometricsTipCoordinator = {
        let coordinator = FacialBiometricsTipCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenReceiveCameraFromPresenter_ShouldPushViewController() {
        sut.perform(action: .camera)
        
        XCTAssertTrue((viewControllerSpy.presentViewController as? UINavigationController)?.topViewController is FacialBiometricsTakeViewController)
        XCTAssertEqual(navigationSpy.pushedCount, 1)
    }
}
