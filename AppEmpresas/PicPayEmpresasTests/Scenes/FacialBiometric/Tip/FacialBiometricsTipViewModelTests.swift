import XCTest
@testable import PicPayEmpresas

private final class FacialBiometricsTipPresenterSpy: FacialBiometricsTipPresenting {
    var viewController: FacialBiometricsTipDisplay?
    
    private(set) var actionSelected: FacialBiometricsTipAction?
    private(set) var actionCount = 0
    private(set) var displayTipsCount = 0
    
    func didNextStep(action: FacialBiometricsTipAction) {
        actionSelected = action
        actionCount += 1
    }

    func displayTips() {
        displayTipsCount += 1
    }
}

final class FacialBiometricsTipViewModelTests: XCTestCase {
    private let presenterSpy = FacialBiometricsTipPresenterSpy()
    private let container = DependencyContainerMock()
    private lazy var sut = FacialBiometricsTipViewModel(presenter: presenterSpy, dependencies: container)
    
    func testDidNextStep_WhenReceiveDidNextStepFromViewController_ShouldPresentNewViewController() {
        sut.didNextStep()
        
        XCTAssertEqual(presenterSpy.actionSelected, .camera)
        XCTAssertEqual(presenterSpy.actionCount, 1)
    }
    
}
