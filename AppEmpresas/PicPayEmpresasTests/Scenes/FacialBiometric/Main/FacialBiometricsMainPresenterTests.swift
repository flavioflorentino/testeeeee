import XCTest
@testable import PicPayEmpresas

final private class FacialBiometricsMainCoordinatorSpy: FacialBiometricsMainCoordinating {
    var viewController: UIViewController?
    
    private(set) var actionSelected: FacialBiometricsMainAction?
    private(set) var actionCount = 0
    
    func perform(action: FacialBiometricsMainAction) {
        actionSelected = action
        actionCount += 1
    }
}

final private class FacialBiometricsMainDisplaySpy: FacialBiometricsMainDisplay {}

final class FacialBiometricsMainPresenterTests: XCTestCase {
    private let coordinatorSpy =  FacialBiometricsMainCoordinatorSpy()
    private let displaySpy = FacialBiometricsMainDisplaySpy()
    
    private lazy var sut: FacialBiometricsMainPresenter = {
        let presenter = FacialBiometricsMainPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testDidNextStep_WhenReceiveTipsFromViewModel_ShouldPushNewViewController() {
        sut.didNextStep(action: .tips)
        
        XCTAssertEqual(coordinatorSpy.actionSelected, .tips)
        XCTAssertEqual(coordinatorSpy.actionCount, 1)
    }
}
