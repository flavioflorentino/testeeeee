import XCTest
@testable import PicPayEmpresas

final class FacialBiometricsMainCoordinatorTests: XCTestCase {
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: UIViewController())
    
    private lazy var sut: FacialBiometricsMainCoordinator = {
        let coordinator = FacialBiometricsMainCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenReceiveTipsFromPresenter_ShouldPushViewController() {
        sut.perform(action: .tips)
        
        XCTAssertTrue(navigationSpy.currentViewController is FacialBiometricsTipViewController)
        // Must be 2 because pushViewController is called when we set rootViewController for the navigationController
        XCTAssertEqual(navigationSpy.pushedCount, 2)
    }
}
