import XCTest
@testable import PicPayEmpresas

final private class FacialBiometricsMainPresenterSpy: FacialBiometricsMainPresenting {
    var viewController: FacialBiometricsMainDisplay?
    
    private(set) var actionSelected: FacialBiometricsMainAction?
    private(set) var actionCount = 0
    
    func didNextStep(action: FacialBiometricsMainAction) {
        actionSelected = action
        actionCount += 1
    }
}

final class FacialBiometricsMainViewModelTests: XCTestCase {
    private let presenterSpy = FacialBiometricsMainPresenterSpy()
    private lazy var sut = FacialBiometricsMainViewModel(presenter: presenterSpy, dependencies: DependencyContainerMock())

    func testNextStep_WhenReceiveDidNextStepFromViewController_ShouldPushNewViewController(){
        sut.didNextStep()
        
        XCTAssertEqual(presenterSpy.actionSelected, .tips)
        XCTAssertEqual(presenterSpy.actionCount, 1)
    }
}
