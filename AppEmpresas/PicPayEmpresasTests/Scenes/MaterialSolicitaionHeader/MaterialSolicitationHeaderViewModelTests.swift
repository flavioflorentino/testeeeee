import XCTest
import AnalyticsModule
@testable import PicPayEmpresas

final class MaterialSolicitationHeaderViewModelTests: XCTestCase {
    private lazy var dependency = DependencyContainerMock()
    private lazy var spy = MaterialSolicitationHeaderPresenterSpy()
    private lazy var model = UserStatus(
        boardRequestRefused: false,
        boardStep: "bloqueado",
        boardRequested: true,
        config: UserStatusConfig(blocked: nil),
        sellerID: 1
    )
    
    private lazy var sut = MaterialSolicitationHeaderViewModel(presenter: spy, model: model, dependencies: dependency)
    
    func testMaterialSolicitation_WhenReceiveActionFromViewController_ShouldCallPresenterNextAction() {
        sut.materialSolicitation()
        
        XCTAssertNotNil(spy.currentAction)
        XCTAssertEqual(spy.currentAction, MaterialSolicitationHeaderAction.optinMaterialSolicitation)
    }
    
    func testPrintMaterialSolicitation_WhenReceiveActionFromViewController_ShouldCallPresenterNextAction() {
        sut.printMaterialSolicitation()
        
        XCTAssertNotNil(spy.currentAction)
        XCTAssertEqual(spy.currentAction, MaterialSolicitationHeaderAction.printMaterialSolicitation)
    }
    
    func testIsCheckStatus_WhenReceiveModelWithStateSolicitationFromViewController_ShouldIncreaseCountRequested() {
        model = UserStatus(boardRequestRefused: false,
        boardStep: "solicitacao",
        boardRequested: true,
        config: UserStatusConfig(blocked: nil),
        sellerID: 1)
        
        sut.isCheckShowStatus()
        
        XCTAssertEqual(spy.currentStateRequested, .solicitation)
        XCTAssertEqual(spy.countRequested, 1)
    }
    
    func testIsCheckStatus_WhenReceiveModelWithStateActivationFromViewController_ShouldIncreaseCountRequested() {
        model = UserStatus(boardRequestRefused: false,
        boardStep: "ativacao",
        boardRequested: true,
        config: UserStatusConfig(blocked: nil),
        sellerID: 1)
        
        sut.isCheckShowStatus()
        
        XCTAssertEqual(spy.currentStateRequested, .activation)
        XCTAssertEqual(spy.countRequested, 1)
    }
    
    func testIsCheckStatus_WhenReceiveModelWithStateBrCodeFromViewController_ShouldIncreaseCountRequested() {
        model = UserStatus(boardRequestRefused: false,
        boardStep: "brCode",
        boardRequested: true,
        config: UserStatusConfig(blocked: nil),
        sellerID: 1)
        
        sut.isCheckShowStatus()
        
        XCTAssertEqual(spy.currentStateRequested, .brCode)
        XCTAssertEqual(spy.countRequested, 1)
    }
    
    func testIsCheckStatus_WhenReceiveModelWithStateBlockedFromViewController_ShoulIncreaseCountRequested() {
        model = UserStatus(
            boardRequestRefused: false,
            boardStep: "bloqueia",
            boardRequested: false,
            config: UserStatusConfig(blocked: nil),
            sellerID: 1
        )
        
        sut.isCheckShowStatus()
        
        XCTAssertEqual(spy.currentStateRequested, .thirdSolicitation)
        XCTAssertEqual(spy.countRequested, 1)
    }
}

private final class MaterialSolicitationHeaderPresenterSpy: MaterialSolicitationHeaderPresenting {
    var viewController: MaterialSolicitationHeaderDisplay?
    
    private(set) var currentAction: MaterialSolicitationHeaderAction?
    private(set) var currentStateRequested: MaterialFlowStep?
    private(set) var countRequested = 0
    
    func didNextStep(action: MaterialSolicitationHeaderAction) {
        currentAction = action
    }
    
    func controlViewLoad(state: MaterialFlowStep) {
        currentStateRequested = state
        countRequested += 1
    }
}
