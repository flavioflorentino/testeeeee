import XCTest
@testable import PicPayEmpresas

final class MaterialSolicitationHeaderPresenterTests: XCTestCase {
    private lazy var displaySpy = MaterialSolicitationHeaderDisplaySpy()
    private lazy var spy = MaterialSolicitationHeaderCoordinatorSpy()
    private lazy var sut: MaterialSolicitationHeaderPresenter = {
        let presenter = MaterialSolicitationHeaderPresenter(coordinator: spy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testDidNextStep_WhenReceiveActionOptinMaterialSolicitationFromViewModel_ShouldCallCoordinatorNextAction() {
        sut.didNextStep(action: .optinMaterialSolicitation)
        
        XCTAssertNotNil(spy.currentAction)
        XCTAssertEqual(spy.currentAction, MaterialSolicitationHeaderAction.optinMaterialSolicitation)
    }
    
    func testDidNextStep_WhenReceiveActionPrintMaterialSolicitationFromViewModel_ShouldCallCoordinatorNextAction() {
        sut.didNextStep(action: .printMaterialSolicitation)
        
        XCTAssertNotNil(spy.currentAction)
        XCTAssertEqual(spy.currentAction, MaterialSolicitationHeaderAction.printMaterialSolicitation)
    }
    
    
    func testControlViewLoad_WhenReceiveSolicitationStateFromViewModel_ShouldIncreaseHeaderMaterialCount() {
        sut.controlViewLoad(state: .solicitation)
        
        XCTAssertEqual(displaySpy.printHeaderCount, 0)
        XCTAssertEqual(displaySpy.materialHeaderCount, 1)
    }
    
    func testControlViewLoad_WhenReceiveActivationStateFromViewModel_ShouldIncreaseHeaderMaterialCount() {
        sut.controlViewLoad(state: .activation)
        
        XCTAssertEqual(displaySpy.printHeaderCount, 0)
        XCTAssertEqual(displaySpy.materialHeaderCount, 1)
    }
    
    func testControlViewLoad_WhenReceiveBrCodeStateFromViewModel_ShouldIncreaseHeaderMaterialCount() {
        sut.controlViewLoad(state: .brCode)
        
        XCTAssertEqual(displaySpy.printHeaderCount, 0)
        XCTAssertEqual(displaySpy.materialHeaderCount, 1)
    }
    
    func testControlViewLoad_WhenReceiveThirdSolicitationStateFromViewModel_ShoulIncreasePrintCount() {
        sut.controlViewLoad(state: .thirdSolicitation)
        
        XCTAssertEqual(displaySpy.printHeaderCount, 1)
        XCTAssertEqual(displaySpy.materialHeaderCount, 0)
    }
}

private final class MaterialSolicitationHeaderCoordinatorSpy: MaterialSolicitationHeaderCoordinating {
    var viewController: UIViewController?
    
    private(set) var currentAction: MaterialSolicitationHeaderAction?
    
    func perform(action: MaterialSolicitationHeaderAction) {
        currentAction = action
    }
}

private final class MaterialSolicitationHeaderDisplaySpy: MaterialSolicitationHeaderDisplay {
    private(set) var materialHeaderCount = 0
    private(set) var printHeaderCount = 0
    
    func insertHeader(material: MaterialSolicitationHeaderView) {
        materialHeaderCount += 1
    }
    
    func insertHeader(print: QRCodePrintView) {
        printHeaderCount += 1
    }
}
