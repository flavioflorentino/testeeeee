import XCTest
@testable import PicPayEmpresas

final class MaterialSolicitationHeaderCoordinatorTests: XCTestCase {
    private lazy var controller = ViewControllerSpy()
    private lazy var navigation = NavigationControllerSpy(rootViewController: controller)
    
    private lazy var sut: MaterialSolicitationHeaderCoordinator = {
        let coordinator = MaterialSolicitationHeaderCoordinator()
        coordinator.viewController = navigation.topViewController
        return coordinator
    }()
    
    func testPerform_whenReceiveOptinMaterialSolicitationFromPresenter_shouldCallOptinApplyMaterialViewController() {
        sut.perform(action: .optinMaterialSolicitation)
        XCTAssertTrue(controller.presentViewController is UINavigationController)
    }
    
    func testPerform_whenReceivePrintMaterialSolicitationFromPresenter_shouldCallChooseQRCodePrintViewController() {
        sut.perform(action: .printMaterialSolicitation)
        XCTAssertTrue(navigation.currentViewController is ChooseQRCodePrintViewController)
    }
}
