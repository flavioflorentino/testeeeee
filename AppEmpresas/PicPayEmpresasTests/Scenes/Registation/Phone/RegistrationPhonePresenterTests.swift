import XCTest
@testable import PicPayEmpresas

final class RegistrationPhonePresenterTests: XCTestCase {
    private lazy var coordinatorSpy = RegistrationPhoneCoordinatorSpy()
    private lazy var displaySpy = RegistrationPhoneDisplaySpy()
    private lazy var sut: RegistrationPhonePresenter = {
        let presenter = RegistrationPhonePresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testDidNextStep_WhenReceiveNextScreenActionFromViewModel_ShouldCallNextStepInCoordinator() {
        sut.didNextStep(action: .nextScreen)
        XCTAssertEqual(coordinatorSpy.actionSelected, RegistrationPhoneAction.nextScreen)
    }

    func testValidateSubmit_WhenReceiveSuccesValueFromViewModel_ShouldCallSubmitValueCount() {
        sut.validateSubmit(value: true)

        XCTAssertEqual(displaySpy.submitValuesCount, 1)
    }

    func testValidateSubmit_WhenReceiveFailureValueFromViewModel_ShouldCallSubmitValueCount() {
        sut.validateSubmit(value: false)

        XCTAssertEqual(displaySpy.submitValuesCount, 0)
    }

    func testUpdateValues_WhenReceiveAllTextFieldsWithErrorsFromViewModel_ShouldUpdateFieldsWithMessageError() {
        let list = [
            ("generic error", UIPPFloatingTextField()),
            ("generic error 2", UIPPFloatingTextField()),
            ("", UIPPFloatingTextField())
        ]

        sut.updateFields(fields: list)

        XCTAssertNotNil(displaySpy.listUpdateFields)
        XCTAssertEqual(displaySpy.listUpdateFields?.first?.message, "generic error")
    }

    func testUpdateValues_WhenReceiveAllTextFieldsFromViewModel_ShouldUpdateFieldsWithOutMessageError() {
        let list = [
            ("", UIPPFloatingTextField()),
            ("", UIPPFloatingTextField()),
            ("", UIPPFloatingTextField())
        ]

        sut.updateFields(fields: list)

        XCTAssertNotNil(displaySpy.listUpdateFields)
        XCTAssertEqual(displaySpy.listUpdateFields?.first?.message, "")
    }
}

private class RegistrationPhoneCoordinatorSpy: RegistrationPhoneCoordinating {
    var viewController: UIViewController?
    private(set) var actionSelected: RegistrationPhoneAction?

    func perform(action: RegistrationPhoneAction) {
        actionSelected = action
    }
}

private class RegistrationPhoneDisplaySpy: RegistrationPhoneDisplay {
    private(set) var listUpdateFields: [(message: String?, textField: UIPPFloatingTextField)]?
    private(set) var submitValuesCount = 0
    
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)]) {
        listUpdateFields = fields
    }
    
    func submitValues() {
        submitValuesCount += 1
    }
}
