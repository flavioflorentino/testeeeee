import XCTest
import Validator
@testable import PicPayEmpresas

final class RegistrationPhoneViewModelTests: XCTestCase {
    private lazy var mock = RegistrationPhoneServiceMock()
    private lazy var spy = RegistrationPhonePresenterSpy()
    private lazy var sut = RegistrationPhoneViewModel(service: mock, presenter: spy)
    
    func testUpdateValues_WhenReceiveAllTextFieldsWithErrorsFromViewController_ShouldUpdateFieldsWithMessageError() {
        let list = createPPFloatingFields(with: false)
        sut.updateFields(textFields: list)

        XCTAssertNotNil(spy.listUpdateFields)
        XCTAssertEqual(spy.listUpdateFields?.first?.message, "generec error")
    }

    func testUpdateValues_WhenReceiveAllTextFieldsFromViewController_ShouldUpdateFieldsWithOutMessageError() {
        let list = createPPFloatingFields(with: true)

        sut.updateFields(textFields: list)

        XCTAssertNotNil(spy.listUpdateFields)
        XCTAssertEqual(spy.listUpdateFields?.first?.message, "")
    }
    
    func testValidateSubmitSuccess_WhenReceiveAllTextFieldsFormViewController_ShouldCallValidateSubmitInPresenter() {
        let list = createPPFloatingFields(with: true)

        sut.validateSubmit(textFields: list)

        XCTAssertEqual(spy.isValidated, true)
    }
    
    func testValidateSubmitError_WhenReceiveAllTextFieldsFormViewController_ShouldCallValidateSubmitInPresenter() {
         let list = createPPFloatingFields(with: false)
         sut.validateSubmit(textFields: list)

         XCTAssertEqual(spy.isValidated, false)
     }
    
    func testSubmitValue_WhenSubmitIsSuccessFromViewController_ShouldCallNextStep() {
        sut.submitValues(ddd: "(11)", phone: "91234-5678", dddOpcional: nil, phoneOpcional: nil)
        
        XCTAssertEqual(spy.actionSelected, RegistrationPhoneAction.nextScreen)
        XCTAssertNotNil(mock.model)
    }
}

// MARK: - Private functios used by configuration
private extension RegistrationPhoneViewModelTests {
    private enum TestsValidateError: String, ValidationError {
        case generic = "generec error"

        var message: String {
            rawValue
        }
    }

    private func createPPFloatingFields(with text: Bool) -> [UIPPFloatingTextField] {
        let dddTextField = UIPPFloatingTextField()
        let phoneTextField = UIPPFloatingTextField()
        let dddOpcionalTextField = UIPPFloatingTextField()
        let phoneOpcionalTextField = UIPPFloatingTextField()

        if text {
            dddTextField.text = "(11)"
            phoneTextField.text = "91234-5678"
            dddOpcionalTextField.text = nil
            phoneOpcionalTextField.text = nil
        }

        let list = [dddTextField, phoneTextField, dddOpcionalTextField, phoneOpcionalTextField]
        configureValidatorsFields(inputs: list)

        return list
    }

    private func configureValidatorsFields(inputs: [UIPPFloatingTextField]) {
        var dddRule = ValidationRuleSet<String>()
        var dddInput = inputs[0]
        dddRule.add(rule: ValidationRuleLength(min: 2, error: TestsValidateError.generic))
        dddInput.validationRules = dddRule

        var cellPhoneRules = ValidationRuleSet<String>()
        var phoneInput = inputs[1]
        cellPhoneRules.add(rule: ValidationRuleLength(min: 9, error: TestsValidateError.generic))
        phoneInput.validationRules = cellPhoneRules
    }
}

private final class RegistrationPhoneServiceMock: RegistrationPhoneServicing {
    private(set) var model: PhoneRegisterModel?
    func registerPhone(model: PhoneRegisterModel) {
        self.model = model
    }
}

private final class RegistrationPhonePresenterSpy: RegistrationPhonePresenting {
    var viewController: RegistrationPhoneDisplay?
    private(set) var actionSelected: RegistrationPhoneAction?
    private(set) var listUpdateFields: [(message: String?, textField: UIPPFloatingTextField)]?
    private(set) var isValidated: Bool?
    
    func didNextStep(action: RegistrationPhoneAction) {
        actionSelected = action
    }
    
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)]) {
        listUpdateFields = fields
    }
    
    func validateSubmit(value: Bool) {
        isValidated = value
    }
}
