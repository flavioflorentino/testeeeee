import XCTest
@testable import PicPayEmpresas

final class RegistrationPhoneCoordinatorTests: XCTestCase {
    private lazy var controller = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: controller)
    private lazy var sut: RegistrationPhoneCoordinator = {
        let cooordinator = RegistrationPhoneCoordinator(model: RegistrationViewModel())
        cooordinator.viewController = navigationSpy.topViewController
        return cooordinator
    }()
    
    func testDidNextStep_WhenReceiveToProFromPresenter_ShouldPushAddressViewController() {
        sut.perform(action: .nextScreen)
        XCTAssertTrue(navigationSpy.currentViewController is RegistrationAddressStepViewController)
    }
}
