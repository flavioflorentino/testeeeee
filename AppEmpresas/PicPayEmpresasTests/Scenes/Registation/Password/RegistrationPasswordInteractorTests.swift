import XCTest
import Core
import Validator
import AnalyticsModule
@testable import PicPayEmpresas

final class RegistrationPasswordInteractorTests: XCTestCase {
    private lazy var spy = RegistrationPasswordPresenterSpy()
    private lazy var mock = RegistrationPasswordServiceMock()
    lazy var sut = RegistrationPasswordInteractor(
        model: RegistrationViewModel(),
        service: mock,
        presenter: spy,
        dependencies: DependencyContainerMock()
    )
    
    func testConfigureTerms_WhenReceiveActionFromViewController_ShouldPresenterConfigureAttributeText() {
        sut.configureTerms()
        XCTAssertEqual(spy.configureTermsCount, 1)
    }
    
    func testOpenUrl_WhenReceiveURLFromViewController_ShouldCallDidNextStepCount() {
        sut.openURL(url: "http://picpay.com")
        XCTAssertEqual(spy.didNextStepActionCount, 1)
        XCTAssertEqual(spy.action, RegistrationPasswordAction.openURL(url: "http://picpay.com"))
    }
    
    func testUpdateField_WhenReceiveFieldsIsInvalid_ShouldResultHaveMessageError() {
        let list = createPPFloatingFields(with: false)
        sut.updateFields(inputs: list)
        
        XCTAssertNotNil(spy.listUpdateFields)
        XCTAssertEqual(spy.listUpdateFields?.first?.message, "generic error")
    }
    
    func testUpdateField_WhenReceiveFieldsIsValid_ShouldResultNoHaveMessageError() {
        let list = createPPFloatingFields(with: true)
        sut.updateFields(inputs: list)
        
        XCTAssertNotNil(spy.listUpdateFields)
        XCTAssertNil(spy.listUpdateFields?.first?.message)
    }
    
    func testSubmitPassword_WhenReceiveNotEqualValues_ShouldCallShowMessageErroInSpy() {
        let list = createPPFloatingFields(with: true, equalText: false)
        sut.submitPassword(inputs: list)
        
        XCTAssertEqual(spy.showMessageErrorCount, 1)
        XCTAssertEqual(spy.messageError, "O campo de confirmação de senha é inválido.")
    }
    
    func testSubmitPassword_WhenReceiveEqualValuesAndServiceIsSuccess_ShouldCallNextStepAction() {
        mock.isSucess = true
        let list = createPPFloatingFields(with: true, equalText: true)
        sut.submitPassword(inputs: list)
        
        XCTAssertEqual(spy.didNextStepActionCount, 1)
        XCTAssertEqual(spy.action, .nextStep)
    }
    
    func testSubmitPassword_WhenReceiveEqualValuesAndServiceIsFailure_ShouldCallHandleErrorAction() {
        mock.isSucess = false
        let list = createPPFloatingFields(with: true, equalText: true)
        sut.submitPassword(inputs: list)
        
        XCTAssertEqual(spy.didNextStepActionCount, 1)
        XCTAssertEqual(spy.action, .handleError)
    }
}

private final class RegistrationPasswordPresenterSpy: RegistrationPasswordPresenting {
    var viewController: RegistrationPasswordDisplaying?
    private(set) var configureTermsCount = 0
    private(set) var didNextStepActionCount = 0
    private(set) var action: RegistrationPasswordAction?
    private(set) var listUpdateFields: [(message: String?, textField: UIPPFloatingTextField?)]?
    private(set) var updateFieldsCount = 0
    private(set) var showMessageErrorCount = 0
    private(set) var messageError: String?

    
    func configureTerms() {
        configureTermsCount += 1
    }
    
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField?)]) {
        listUpdateFields = fields
        updateFieldsCount += 1
    }
    
    func showMessageError(error: String) {
        showMessageErrorCount += 1
        messageError = error
    }
    
    func didNextStep(action: RegistrationPasswordAction) {
        self.action = action
        didNextStepActionCount += 1
    }
}

private final class RegistrationPasswordServiceMock: RegistrationPasswordServicing {
    var isSucess = true
    
    func createAccount(model: RegistrationViewModel, _ completion: @escaping (Result<RegistrationPasswordAccess?, ApiError>) -> Void) {
        guard isSucess else {
            completion(.failure(ApiError.serverError))
            return
        }
        
        let accessModel = Access(
            accessToken: "iOGagYGpyMVQOmKmkxmjdVRU8iAAxoaCQofJQWMj",
            tokenType: "Bearer",
            expiresIn: 15552000,
            refreshToken: "BrGkMYv9GNcw9qPuZbj49yP64zQdjVGIa2mpVyo6",
            biometry: "accepted",
            completed: true
        )
        
        let successModel = RegistrationPasswordAccess(access: accessModel, createAccount: true, completed: true)
        completion(.success(successModel))
    }
}

private extension RegistrationPasswordInteractorTests {
    private enum TestsValidateError: String, ValidationError {
        case generic = "generic error"
        
        var message: String {
            rawValue
        }
    }
    
    private func createPPFloatingFields(with text: Bool, equalText: Bool = true) -> [UIPPFloatingTextField] {
        let passwordTextField = UIPPFloatingTextField()
        let confirmPasswordTextField = UIPPFloatingTextField()
        
        if text && equalText{
            passwordTextField.text = "12345678"
            confirmPasswordTextField.text = "12345678"
        } else if text && !equalText {
            passwordTextField.text = "11569678"
            confirmPasswordTextField.text = "12345678"
        }
        
        let list = [passwordTextField, confirmPasswordTextField]
        configureValidatorsFields(inputs: list)
        
        return list
    }
    
    private func configureValidatorsFields(inputs: [UIPPFloatingTextField]) {
        var passwordRules = ValidationRuleSet<String>()
        var passwordInput = inputs[0]
        var confirmInput = inputs[1]
        
        passwordRules.add(rule: ValidationRuleLength(min: 8, max: 8, error: TestsValidateError.generic))
        passwordInput.validationRules = passwordRules
        confirmInput.validationRules = passwordRules
    }
}
