import XCTest
import Core
@testable import PicPayEmpresas

final class RegistrationPasswordPresenterTests: XCTestCase {
    private lazy var spy = RegistrationPasswordCoordinatorSpy()
    private lazy var displaySpy = RegistrationPasswordDisplaySpy()
    lazy var sut: RegistrationPasswordPresenter = {
        let presenter = RegistrationPasswordPresenter(coordinator: spy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testConfigureTerms_WhenReceiveActionFromInteractor_ShouldDisplayConfigureAttributeText() {
        sut.configureTerms()
        XCTAssertEqual(displaySpy.setAttributeTextCount, 1)
    }
    
    func testDidNextStep_WhenReceiveActionOpenURLFromInteractor_ShouldCallPerformCount() {
        sut.didNextStep(action: .openURL(url: "http://www.picpay.com"))
        
        XCTAssertEqual(spy.currenAction, RegistrationPasswordAction.openURL(url: "http://www.picpay.com"))
        XCTAssertEqual(spy.performCountAction, 1)
    }
    
    func testDidNextStep_WhenReceiveActionNexStepFromInteractor_ShouldCallPerformCount() {
        sut.didNextStep(action: .nextStep)
        
        XCTAssertEqual(spy.currenAction, RegistrationPasswordAction.nextStep)
        XCTAssertEqual(spy.performCountAction, 1)
    }
    
    func testDidNextStep_WhenReceiveActionHandleErrorFromInteractor_ShouldCallPerformCount() {
        sut.didNextStep(action: .handleError)
        
        XCTAssertEqual(spy.currenAction, RegistrationPasswordAction.handleError)
        XCTAssertEqual(spy.performCountAction, 1)
    }
    
    func testUpdateFields_WhenReceiveAllTextFieldsWithErrorsFromInteractor_ShouldUpdateFieldsWithMessageError() {
        let list = [
            ("generic error", UIPPFloatingTextField()),
            ("generic error 2", UIPPFloatingTextField())
        ]
        
        sut.updateFields(fields: list)
        
        XCTAssertNotNil(displaySpy.listUpdateFields)
        XCTAssertEqual(displaySpy.listUpdateFields?.first?.message, "generic error")
        XCTAssertEqual(displaySpy.listUpdateFields?.last?.message, "generic error 2")
    }
    
    func testUpdateFields_WhenReceiveAllTextFieldsFromInteractor_ShouldUpdateFieldsWithOutMessageError() {
        let list: [(String?, UIPPFloatingTextField?)] = [
            (nil, UIPPFloatingTextField()),
            (nil, UIPPFloatingTextField())
        ]
        
        sut.updateFields(fields: list)
        
        XCTAssertNotNil(displaySpy.listUpdateFields)
        XCTAssertNil(displaySpy.listUpdateFields?.first?.message)
    }
    
    func testShowMessageError_WhenReceiveErrorFromInteractor_ShouldCallMessageErrorCount() {
        sut.showMessageError(error: "generic error")
        
        XCTAssertEqual(displaySpy.messageErrorCount, 1)
        XCTAssertEqual(displaySpy.error, "generic error")
    }
}

private class RegistrationPasswordCoordinatorSpy: RegistrationPasswordCoordinating {
    var viewController: UIViewController?
    
    private(set) var currenAction: RegistrationPasswordAction?
    private(set) var performCountAction = 0
    
    func perform(action: RegistrationPasswordAction) {
        currenAction = action
        performCountAction += 1
    }
}

private class RegistrationPasswordDisplaySpy: RegistrationPasswordDisplaying {
    private(set) var setAttributeTextCount = 0
    private(set) var listUpdateFields: [(message: String?, textField: UIPPFloatingTextField?)]?
    private(set) var error: String?
    private(set) var messageErrorCount = 0
    
    func setAgreementTextView(linkText: NSAttributedString, linkAttributes: [NSAttributedString.Key : Any]) {
        setAttributeTextCount += 1
    }
    
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField?)]) {
        listUpdateFields = fields
    }
    
    func showMessageError(error: String) {
        messageErrorCount += 1
        self.error = error
    }
}
