import XCTest
import Core
@testable import PicPayEmpresas

final class RegistrationPasswordCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy(rootViewController: UIViewController())
    
    private lazy var sut: RegistrationPasswordCoordinator = {
        let coordinator = RegistrationPasswordCoordinator(dependencies: DependencyContainerMock())
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testDidNextStep_WhenReceiveOpenURLFromPresenter_ShouldPushWebViewController() {
        sut.perform(action: .openURL(url: "http://www.picpay.com"))
        XCTAssertTrue(navigationSpy.currentViewController is WebViewController)
    }
    
    func testDidNextStep_WhenReceiveNextStepFromPresenter_ShouldCallHomeApp() {
        sut.perform(action: .nextStep)
        XCTAssertNotNil(navigationSpy.currentViewController)
    }
    
    func testDidNextStep_WhenReceiveHandleErrorFromPresenter_ShouldPresentBizErrorViewController() {
        sut.perform(action: .handleError)
        XCTAssertTrue(navigationSpy.presentViewController is BizErrorViewViewController)
    }
}
