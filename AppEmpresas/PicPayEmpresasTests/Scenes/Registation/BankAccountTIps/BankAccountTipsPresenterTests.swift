import XCTest
@testable import PicPayEmpresas

private final class BankAccountTipsCoordinatorSpy: BankAccountTipsCoordinating {
    var viewController: UIViewController?
    private(set) var action: BankAccountTipsAction?
    private(set) var performActionCount = 0
    
    private(set) var navigateToBankSelectionCount = 0
    private(set) var navigateToBankSelectionModel: RegistrationViewModel?

    func perform(action: BankAccountTipsAction) {
        self.action = action
        performActionCount += 1
    }
    
    func navigateToBankSelection(registrationModel: RegistrationViewModel) {
        navigateToBankSelectionCount += 1
        navigateToBankSelectionModel = registrationModel
    }
}

private final class BankAccountTipsDisplaySpy: BankAccountTipsDisplay {
    private(set) var displayTitleCount = 0
    private(set) var displayTitle: String?
    
    private(set) var displayTextCount = 0
    private(set) var displayText: String?
    
    private(set) var displayTipsCount = 0
    private(set) var displayTips: [BankAccountTip]?

    private(set) var displayFAQTitleCount = 0
    private(set) var displayFAQTitle: String?

    func displayTitle(_ title: String) {
        displayTitleCount += 1
        displayTitle = title
    }
    
    func displayText(_ text: String) {
        displayTextCount += 1
        displayText = text
    }
    
    func displayTips(_ tips: [BankAccountTip]) {
        displayTipsCount += 1
        displayTips = tips
    }

    func displayFAQTitle(_ title: String) {
        displayFAQTitleCount += 1
        displayFAQTitle = title
    }
}

final class BankAccountTipsPresenterTests: XCTestCase {
    private let coordinatorSpy = BankAccountTipsCoordinatorSpy()
    private let displaySpy = BankAccountTipsDisplaySpy()

    lazy var sut: BankAccountTipsPresenter? = {
        let presenter = BankAccountTipsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testDisplayTitle_ShouldCallDisplayTitle() {
        let title = "Irmãos Karamazov"
        sut?.displayTitle(title)
        
        XCTAssertEqual(1, displaySpy.displayTitleCount)
        XCTAssertEqual(title, displaySpy.displayTitle)
    }
    
    func testDisplayText_ShouldCallDisplayText() {
        let text = "Turma da Mônica"
        sut?.displayText(text)
        
        XCTAssertEqual(1, displaySpy.displayTextCount)
        XCTAssertEqual(text, displaySpy.displayText)
    }
    
    func testDisplayTips_WhenIndividual_ShouldCallDisplayIndividualTips() throws {
        sut?.displayTips(companyType: .individual)
        
        XCTAssertEqual(1, displaySpy.displayTipsCount)
        XCTAssertEqual(2, displaySpy.displayTips?.count)
    }
    
    func testDisplayTips_WhenMulti_ShouldCallDisplayMultiTips() throws {
        sut?.displayTips(companyType: .multiPartners)
        
        XCTAssertEqual(1, displaySpy.displayTipsCount)
        XCTAssertEqual(2, displaySpy.displayTips?.count)
    }
    
    func testDidNextStep_WhenEditBankAccount_ShouldCallPerformAction() {
        let action: BankAccountTipsAction = .editBankAccount(seller: nil, companyType: .individual)
        sut?.didNextStep(action: action)
        
        XCTAssertEqual(1, coordinatorSpy.performActionCount)
        XCTAssertEqual(action, coordinatorSpy.action)
    }
    
    func testDidNextStep_WhenHelpModal_ShouldCallPerformAction() {
        let action: BankAccountTipsAction = .helpModal(info: AlertModalInfo(), seller: nil)
        sut?.didNextStep(action: action)
        
        XCTAssertEqual(1, coordinatorSpy.performActionCount)
        XCTAssertEqual(action, coordinatorSpy.action)
    }
    
    func testNavigateToBankSelection_ShouldCallNavigateToBankSelectionWithRegistrationModel() {
        sut?.navigateToBankSelection(registrationModel: RegistrationViewModel())
        
        XCTAssertEqual(1, coordinatorSpy.navigateToBankSelectionCount)
        XCTAssertNotNil(coordinatorSpy.navigateToBankSelectionModel)
    }
}
