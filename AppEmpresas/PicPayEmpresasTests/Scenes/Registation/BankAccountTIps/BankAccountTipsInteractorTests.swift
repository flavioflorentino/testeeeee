import AssetsKit
import Core
import CoreSellerAccount
import XCTest
@testable import PicPayEmpresas

private final class BankAccountTipsPresenterSpy: BankAccountTipsPresenting {
    var viewController: BankAccountTipsDisplay?
    
    private(set) var displayTitleCount = 0
    private(set) var displayTitle: String?
    
    private(set) var displayTextCount = 0
    private(set) var displayText: String?
    
    private(set) var displayTipsCount = 0
    private(set) var displayTipsCompany: CompanyType?
    
    private(set) var displayNextStepCount = 0
    private(set) var displayNextStepAction: BankAccountTipsAction?
    
    private(set) var displayBankSelectionCount = 0
    private(set) var displayBankSelectionModel: RegistrationViewModel?
    
    private(set) var displayFAQTitleCount = 0
    private(set) var displayFAQTitle: String?

    func displayTitle(_ title: String) {
        displayTitleCount += 1
        displayTitle = title
    }
    
    func displayText(_ text: String) {
        displayTextCount += 1
        displayText = text
    }
    
    func displayTips(companyType: CompanyType) {
        displayTipsCount += 1
        displayTipsCompany = companyType
    }
    
    func didNextStep(action: BankAccountTipsAction) {
        displayNextStepCount += 1
        displayNextStepAction = action
    }
    
    func navigateToBankSelection(registrationModel: RegistrationViewModel) {
        displayBankSelectionCount += 1
        displayBankSelectionModel = registrationModel
    }

    func displayFAQTitle(_ title: String) {
        displayFAQTitleCount += 1
        displayFAQTitle = title
    }
}

final class BankAccountTipsInteractorTests: XCTestCase {
    private typealias Localizable = Strings.BankAccount.BankAccountTips
    
    private lazy var container = DependencyContainerMock()
    private lazy var presenterSpy = BankAccountTipsPresenterSpy()
    private lazy var registrationModel = RegistrationViewModel()
    private lazy var seller = Seller()
    
    func testInitialLoad_WhenRegister_ShouldDisplayRegisterTitleAndText() {
        let sut = BankAccountTipsInteractor(
            presenter: presenterSpy,
            dependencies: container,
            status: .register(registrationModel: registrationModel),
            companyType: .individual
        )
        sut.initialLoad()
        
        XCTAssertEqual(presenterSpy.displayTitleCount, 1)
        XCTAssertEqual(presenterSpy.displayTitle, Localizable.Register.title)
        XCTAssertEqual(presenterSpy.displayTextCount, 1)
        XCTAssertEqual(presenterSpy.displayText, Localizable.Register.text)
    }
    
    func testInitialLoad_WhenEdit_ShouldDisplayEditTitleAndText() {
        let sut = BankAccountTipsInteractor(
            presenter: presenterSpy,
            dependencies: container,
            status: .edit(seller: seller),
            companyType: .individual
        )
        sut.initialLoad()
        
        XCTAssertEqual(presenterSpy.displayTitleCount, 1)
        XCTAssertEqual(presenterSpy.displayTitle, Localizable.Edit.title)
        XCTAssertEqual(presenterSpy.displayTextCount, 1)
        XCTAssertEqual(presenterSpy.displayText, Localizable.Edit.text)
    }
    
    func testInitialLoad_WhenIndividual_ShouldDisplayIndividualTips() {
        let sut = BankAccountTipsInteractor(
            presenter: presenterSpy,
            dependencies: container,
            status: .edit(seller: seller),
            companyType: .individual
        )
        sut.initialLoad()
        
        XCTAssertEqual(presenterSpy.displayTipsCount, 1)
        XCTAssertEqual(presenterSpy.displayTipsCompany, .individual)
    }
    
    func testInitialLoad_WhenMulti_ShouldDisplayMultiTips() {
        let sut = BankAccountTipsInteractor(
            presenter: presenterSpy,
            dependencies: container,
            status: .edit(seller: seller),
            companyType: .multiPartners
        )
        sut.initialLoad()
        
        XCTAssertEqual(presenterSpy.displayTipsCount, 1)
        XCTAssertEqual(presenterSpy.displayTipsCompany, .multiPartners)
    }
    
    func testTouchContinueButton_WhenEdit_ShouldCallEditAction() {
        let sut = BankAccountTipsInteractor(
            presenter: presenterSpy,
            dependencies: container,
            status: .edit(seller: seller),
            companyType: .multiPartners
        )
        
        sut.continueAction()
        
        XCTAssertEqual(presenterSpy.displayNextStepCount, 1)
        XCTAssertEqual(presenterSpy.displayNextStepAction, .editBankAccount(seller: seller, companyType: .multiPartners))
    }
    
    func testTouchContinueButton_WhenRegister_ShouldCallRegisterAction() {
        let sut = BankAccountTipsInteractor(
            presenter: presenterSpy,
            dependencies: container,
            status: .register(registrationModel: registrationModel),
            companyType: .multiPartners
        )
        
        sut.continueAction()
        
        XCTAssertEqual(presenterSpy.displayBankSelectionCount, 1)
        // We cannot validate the value here because RegistrationViewModel is legacy and not equatable
        XCTAssertNotNil(registrationModel)
    }
    
    func testNoAccountButton_WhenIndividual_ShouldCallHelpModalIndividualAction() {
        testHelpModal(subtitle: Localizable.HelpModal.Individual.subtitle, companyType: .individual)
    }
    
    func testNoAccountButton_WhenMulti_ShouldCallHelpModalMultiAction() {
        testHelpModal(subtitle: Localizable.HelpModal.Multi.subtitle, companyType: .multiPartners)
    }
}

private extension BankAccountTipsInteractorTests {
    func testHelpModal(subtitle: String, companyType: CompanyType) {
        let imageInfo = AlertModalInfoImage(image: Resources.Icons.icoBlueInfo.image)
        let info = AlertModalInfo(
            imageInfo: imageInfo,
            title: Localizable.HelpModal.title,
            subtitle: subtitle,
            buttonTitle: Localizable.HelpModal.buttonTitle,
            secondaryButtonTitle: Localizable.HelpModal.secondaryButtonTitle
        )
        
        let sut = BankAccountTipsInteractor(
            presenter: presenterSpy,
            dependencies: container,
            status: .register(registrationModel: registrationModel),
            companyType: companyType
        )
        
        sut.noAccountAction()
        
        XCTAssertEqual(presenterSpy.displayNextStepCount, 1)
        XCTAssertEqual(presenterSpy.displayNextStepAction, .helpModal(info: info, seller: nil))
    }
}
