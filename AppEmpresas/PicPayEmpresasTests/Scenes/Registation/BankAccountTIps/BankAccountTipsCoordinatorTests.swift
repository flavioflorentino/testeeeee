import XCTest
@testable import PicPayEmpresas

final private class BankAccountTipsCoordinatorTests: XCTestCase {
    private lazy var viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    
    private lazy var sut: BankAccountTipsCoordinating = {
        let coordinator = BankAccountTipsCoordinator(container: DependencyContainerMock())
        coordinator.viewController = navigationSpy.currentViewController
        return coordinator
    }()
    
    func testPerform_EditBankAccount_ShouldPushBankAccountViewController() {
        sut.perform(action: .editBankAccount(seller: nil, companyType: .individual))
        
        XCTAssertTrue(navigationSpy.currentViewController is BankAccountFormViewController)
    }
    
    func testPerform_RegisterBankAccount_ShouldPushBankSelectViewController() {
        let registrationModel = RegistrationViewModel()
        sut.navigateToBankSelection(registrationModel: registrationModel)
        
        XCTAssertTrue(navigationSpy.currentViewController is RegistrationBankSelectStepViewController)
    }
    
    func testPerform_WhenHelpModal_ShouldPresentHelpModal() {
        let alertModalInfo = AlertModalInfo()
        
        sut.perform(action: .helpModal(info: alertModalInfo, seller: nil))
        
        XCTAssertEqual(1, viewControllerSpy.presentViewControllerCounter)
        XCTAssertTrue(viewControllerSpy.presentViewController is PopupViewController)
    }
}
