import XCTest
@testable import PicPayEmpresas

final class RegistrationChooseProfileViewModelTests: XCTestCase {
    private let spy = RegistrationChooseProfilePresenterSpy()
    private lazy var sut = RegistrationChooseProfileViewModel(presenter: spy)
    
    func testGoToProScreen_WhenReceiveActionFromViewController_ShouldReceiveToProValue() {
        sut.dontHaveCNPJ()
        
        XCTAssertNotNil(spy.actionSelected)
        XCTAssertEqual(spy.actionSelected, RegistrationChooseProfileAction.toPro)
    }
    
    func testContinueRegister_WhenReceiveActionFromViewController_ShouldReceiveNextScreenValue() {
        sut.nextStep()
        
        XCTAssertNotNil(spy.actionSelected)
        XCTAssertEqual(spy.actionSelected, RegistrationChooseProfileAction.nextScreen)
    }
}

private final class RegistrationChooseProfilePresenterSpy: RegistrationChooseProfilePresenting {
    var viewController: RegistrationChooseProfileDisplay?
    
    private(set) var actionSelected: RegistrationChooseProfileAction?
    
    func didNextStep(action: RegistrationChooseProfileAction) {
        actionSelected = action
    }
}
