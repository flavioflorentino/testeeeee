import XCTest
@testable import PicPayEmpresas

final class RegistrationChooseProfileCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy(rootViewController: UIViewController())
    
    private lazy var sut: RegistrationChooseProfileCoordinator = {
        let coordinator = RegistrationChooseProfileCoordinator(model: RegistrationViewModel())
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testDidNextStep_WhenReceiveToProFromPresenter_ShouldPushProViewController() {
        sut.perform(action: .toPro)
        
        XCTAssertTrue(navigationSpy.currentViewController is RegistrationProViewController)
    }
    
    func testDidNextStep_WhenReceiveNextScreenFromPresenter_ShouldPushViewController() {
        sut.perform(action: .nextScreen)
        
        XCTAssertTrue(navigationSpy.currentViewController is RegistrationCNPJStepViewController)
    }
}
