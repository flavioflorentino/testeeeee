import XCTest
@testable import PicPayEmpresas

final class RegistrationChooseProfilePresenterTests: XCTestCase {
    private let spy = RegistrationChooseProfileCoodirnatorSpy()
    private lazy var sut = RegistrationChooseProfilePresenter(coordinator: spy)
    
    func testDidNextStep_WhenReceiveToProValueFromViewModel_ShouldReceiveToProValueInCoordinator() {
        sut.didNextStep(action: .toPro)
        
        XCTAssertNotNil(spy.actionSelected)
        XCTAssertEqual(spy.actionSelected, RegistrationChooseProfileAction.toPro)
    }
    
    func testDidNextStep_WhenReceiveNextScreenValueFromViewModel_ShouldReceiveNextScreenValueInCoordinator() {
        sut.didNextStep(action: .nextScreen)
        
        XCTAssertNotNil(spy.actionSelected)
        XCTAssertEqual(spy.actionSelected, RegistrationChooseProfileAction.nextScreen)
    }
}

private final class RegistrationChooseProfileCoodirnatorSpy: RegistrationChooseProfileCoordinating {
    var viewController: UIViewController?
    
    private(set) var actionSelected: RegistrationChooseProfileAction?
    
    func perform(action: RegistrationChooseProfileAction) {
        actionSelected = action
    }
}
