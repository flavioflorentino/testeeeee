import XCTest
@testable import PicPayEmpresas

private final class RegistrationWelcomeCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy(rootViewController: ViewControllerSpy())

    private lazy var sut: WelcomeCoordinating = {
        let coordinator = WelcomeCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()

    func testPerform_ShouldPresentRegistrationScreen() {
        sut.perform(action: .rules)
        XCTAssertEqual(navigationSpy.pushedCount, 2)
        XCTAssertTrue(navigationSpy.currentViewController is RegistrationRulesViewController)
    }
}
