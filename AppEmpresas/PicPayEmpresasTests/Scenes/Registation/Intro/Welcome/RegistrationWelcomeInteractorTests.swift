import XCTest
import FeatureFlag
@testable import PicPayEmpresas

private final class WelcomePresenterSpy: WelcomePresenting {
    var viewController: WelcomeDisplaying?

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [WelcomeAction] = []

    //MARK: - hideZeroTax
    private(set) var hideZeroTaxCallsCount = 0
    private(set) var hideZeroTaxReceivedInvocations: [Bool] = []

    func didNextStep(action: WelcomeAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }

    func hideZeroTax(_ shouldHide: Bool) {
        hideZeroTaxCallsCount += 1
        hideZeroTaxReceivedInvocations.append(shouldHide)
    }
}

private final class RegistrationWelcomeInteractorTests: XCTestCase {
    let presenterSpy = WelcomePresenterSpy()

    let featureManagerMock = FeatureManagerMock()
    lazy var dependenciesMock = DependencyContainerMock(featureManagerMock)

    lazy var sut: WelcomeInteracting = WelcomeInteractor(presenter: presenterSpy, dependencies: dependenciesMock)

    func testLoadRules_ShouldGoToRulesScreen() {
        sut.loadRules()

        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.count, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.first, .rules)
    }

    func testHideZeroTax_WhenFeatureFlagFalse_ShouldHideZeroTaxDescription() {
        featureManagerMock.override(keys: .registrationTaxZero, with: false)

        sut.hideZeroTax()

        XCTAssertEqual(presenterSpy.hideZeroTaxCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideZeroTaxReceivedInvocations.count, 1)
        XCTAssertEqual(presenterSpy.hideZeroTaxReceivedInvocations.first, true)
    }

    func testHideZeroTax_WhenFeatureFlagTrue_ShouldShowZeroTaxDescription() {
        featureManagerMock.override(keys: .registrationTaxZero, with: true)

        sut.hideZeroTax()

        XCTAssertEqual(presenterSpy.hideZeroTaxCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideZeroTaxReceivedInvocations.count, 1)
        XCTAssertEqual(presenterSpy.hideZeroTaxReceivedInvocations.first, false)
    }
}
