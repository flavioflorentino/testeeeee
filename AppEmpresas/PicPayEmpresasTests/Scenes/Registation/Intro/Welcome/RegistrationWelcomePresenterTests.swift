import XCTest
@testable import PicPayEmpresas

private final class WelcomeViewControllerSpy: WelcomeDisplaying {

    //MARK: - hideZeroTax
    private(set) var hideZeroTaxCallsCount = 0
    private(set) var hideZeroTaxReceivedInvocations: [Bool] = []

    func hideZeroTax(_ shouldHide: Bool) {
        hideZeroTaxCallsCount += 1
        hideZeroTaxReceivedInvocations.append(shouldHide)
    }
}

private final class WelcomeCoordinatorSpy: WelcomeCoordinating {
    var viewController: UIViewController?

    //MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [WelcomeAction] = []

    func perform(action: WelcomeAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

private final class RegistrationWelcomePresenterTests: XCTestCase {
    let viewControllerSpy = WelcomeViewControllerSpy()
    let coordinatorSpy = WelcomeCoordinatorSpy()

    lazy var sut: WelcomePresenting = {
        let presenter = WelcomePresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testDidNextStep_ShouldGoToRulesScreen() {
        sut.didNextStep(action: .rules)

        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations.count, 1)
        XCTAssertTrue(coordinatorSpy.performActionReceivedInvocations.contains(.rules))
    }

    func testHideZeroTax_WhenFalse_ShouldShowZeroTaxDescription() {
        sut.hideZeroTax(false)

        XCTAssertEqual(viewControllerSpy.hideZeroTaxCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.hideZeroTaxReceivedInvocations.count, 1)
        XCTAssertEqual(viewControllerSpy.hideZeroTaxReceivedInvocations.first, false)
    }

    func testHideZeroTax_WhenTrue_ShouldHideZeroTaxDescription() {
        sut.hideZeroTax(true)

        XCTAssertEqual(viewControllerSpy.hideZeroTaxCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.hideZeroTaxReceivedInvocations.count, 1)
        XCTAssertEqual(viewControllerSpy.hideZeroTaxReceivedInvocations.first, true)
    }
}
