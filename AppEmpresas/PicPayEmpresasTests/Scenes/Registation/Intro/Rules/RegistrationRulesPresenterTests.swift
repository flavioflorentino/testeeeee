import XCTest
@testable import PicPayEmpresas

private final class RegistrationRulesCoordinatorSpy: RegistrationRulesCoordinating {
    var viewController: UIViewController?

    //MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [RegistrationRulesAction] = []

    func perform(action: RegistrationRulesAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

private final class RegistrationRulesViewControllerSpy: RegistrationRulesDisplay {

    //MARK: - setAgreementTextView
    private(set) var setAgreementTextViewLinkTextLinkAttributesCallsCount = 0
    private(set) var setAgreementTextViewLinkTextLinkAttributesReceivedInvocations: [(linkText: NSAttributedString, linkAttributes: [NSAttributedString.Key: Any])] = []

    func setAgreementTextView(
        linkText: NSAttributedString,
        linkAttributes: [NSAttributedString.Key: Any]
    ) {
        setAgreementTextViewLinkTextLinkAttributesCallsCount += 1
        setAgreementTextViewLinkTextLinkAttributesReceivedInvocations.append((linkText: linkText, linkAttributes: linkAttributes))
    }
}

private final class RegistrationRulesPresenterTests: XCTestCase {
    let coordinatorSpy = RegistrationRulesCoordinatorSpy()
    let viewControllerSpy = RegistrationRulesViewControllerSpy()

    lazy var sut: RegistrationRulesPresenting = {
        let presenter = RegistrationRulesPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testSetupLinks_ShouldSetupLinks() {
        sut.setupLinks()

        XCTAssertEqual(viewControllerSpy.setAgreementTextViewLinkTextLinkAttributesCallsCount, 1)
        XCTAssertNotNil(viewControllerSpy.setAgreementTextViewLinkTextLinkAttributesReceivedInvocations)
    }
    
    func testDidNextStep_ShouldGoToRulesScreen() {
        sut.didNextStep(action: .register)

        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations.count, 1)
        XCTAssertTrue(coordinatorSpy.performActionReceivedInvocations.contains(.register))
    }
    
    func testOpen_ShouldOpenURL() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        sut.open(url: url)

        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations.count, 1)
        XCTAssertTrue(coordinatorSpy.performActionReceivedInvocations.contains(.open(url: url)))
    }
}
