import XCTest
import SafariServices
@testable import PicPayEmpresas

private final class RegistrationRulesCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy(rootViewController: ViewControllerSpy())

    private lazy var sut: RegistrationRulesCoordinating = {
        let coordinator = RegistrationRulesCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()

    func testPerformRegister_ShouldPushRegistrationScreen() {
        sut.perform(action: .register)
        XCTAssertEqual(navigationSpy.pushedCount, 2)
        XCTAssertTrue(navigationSpy.currentViewController is RegistrationPersonalInfoViewController)
    }
    
    func testPerformOpenURL_ShouldPresentOpenURLInSafari() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        sut.perform(action: .open(url: url))
        XCTAssertEqual(navigationSpy.presentCount, 1)
        XCTAssertTrue(navigationSpy.presentViewController is SFSafariViewController)
    }
}
