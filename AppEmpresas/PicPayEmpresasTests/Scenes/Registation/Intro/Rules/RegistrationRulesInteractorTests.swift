import LegacyPJ
import XCTest
@testable import PicPayEmpresas

private final class RegistrationRulesPresenterSpy: RegistrationRulesPresenting {
    var viewController: RegistrationRulesDisplay?

    //MARK: - setupLinks
    private(set) var setupLinksCallsCount = 0

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [RegistrationRulesAction] = []

    //MARK: - open
    private(set) var openUrlCallsCount = 0
    private(set) var openUrlReceivedInvocations: [URL] = []

    func setupLinks() {
        setupLinksCallsCount += 1
    }

    func didNextStep(action: RegistrationRulesAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }

    func open(url: URL) {
        openUrlCallsCount += 1
        openUrlReceivedInvocations.append(url)
    }
}

private final class RegistrationRulesInteractorTests: XCTestCase {
    let presenterSpy = RegistrationRulesPresenterSpy()
    
    let authManagerMock = AuthManagerMock()
    let analyticsMock = AnalyticsMock()
    lazy var dependenciesMock = DependencyContainerMock(authManagerMock, analyticsMock)

    lazy var sut: RegistrationRulesInteracting = RegistrationRulesInteractor(
        presenter: presenterSpy,
        dependencies: dependenciesMock
    )
    
    func testSetupLinks_ShouldSetupLinks() {
        sut.setupLinks()

        XCTAssertEqual(presenterSpy.setupLinksCallsCount, 1)
    }

    func testLoadRegistration_WhenUserAuthenticated_ShouldGoToRegistration() throws {
        authManagerMock.isAuthenticatedUser = true
        sut.loadRegistration()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.count, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.first, .register)
    }
    
    func testLoadRegistration_WhenUserNotAuthenticated_ShouldGoToRegistration() throws {
        authManagerMock.isAuthenticatedUser = false
        sut.loadRegistration()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.count, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.first, .register)
    }
    
    func testOpenURL_ShouldOpenURL() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        sut.open(url: url)

        XCTAssertEqual(presenterSpy.openUrlCallsCount, 1)
        XCTAssertEqual(presenterSpy.openUrlReceivedInvocations.count, 1)
        XCTAssertEqual(presenterSpy.openUrlReceivedInvocations.first, url)
    }
}
