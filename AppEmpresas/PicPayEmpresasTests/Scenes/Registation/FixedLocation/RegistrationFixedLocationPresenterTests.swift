import XCTest
@testable import PicPayEmpresas

final class RegistrationFixedLocationPresenterTests: XCTestCase {
    private let spy = RegistrationFixedLocationCoordinatorSpy()
    private lazy var sut = RegistrationFixedLocationPresenter(coordinator: spy)
    
    func testDidNextStep_WhenReceiveDontHaveLocationValueFromViewModel_ShouldReceiveActionInCoordinator() {
        sut.didNextStep(action: .without(model: RegistrationViewModel()))
        
        XCTAssertNotNil(spy.actionSelected)
    }
    
    func testDidNextStep_WhenReceiveNextScreenValueFromViewModel_ShouldReceiveActionInCoordinator() {
        sut.didNextStep(action: .nextScreen(model: RegistrationViewModel()))
        
        XCTAssertNotNil(spy.actionSelected)
    }
}

private final class RegistrationFixedLocationCoordinatorSpy: RegistrationFixedLocationCoordinating {
    var viewController: UIViewController?
    
    private(set) var actionSelected: RegistrationFixedLocationAction?
    
    func perform(action: RegistrationFixedLocationAction) {
        actionSelected = action
    }
}
