import XCTest
@testable import PicPayEmpresas

final class RegistrationFixedLocationCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy(rootViewController: UIViewController())
    
    private lazy var sut: RegistrationFixedLocationCoordinator = {
        let coordinator = RegistrationFixedLocationCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testDidNextStep_WhenReceiveToProFromPresenter_ShouldPushProViewController() {
        sut.perform(action: .without(model: RegistrationViewModel()))
        
        XCTAssertTrue(navigationSpy.currentViewController is RegistrationImageStepViewController)
        XCTAssertEqual(navigationSpy.viewControllers.count, 1)
    }
    
    func testDidNextStep_WhenReceiveNextScreenFromPresenter_ShouldPushViewController() {
        sut.perform(action: .nextScreen(model: RegistrationViewModel()))
        
        XCTAssertTrue(navigationSpy.currentViewController is RegistrationAddressStepViewController)
        XCTAssertEqual(navigationSpy.viewControllers.count, 1)
    }
}
