import XCTest
@testable import PicPayEmpresas

final class RegistrationFixedLocationViewModelTests: XCTestCase {
    private let spy = RegistrationFixedLocationPresenterSpy()
    private let mock = RegistrationFixedLocationServiceMock()
    private lazy var sut = RegistrationFixedLocationViewModel(service: mock, presenter: spy, model: RegistrationViewModel())
    
    func testwithoutLocation_WhenReceiveActionFromViewController_ShouldSetFalseValueInModel() {
        sut.withoutLocation()
        
        XCTAssertNotNil(spy.actionSelected)
        XCTAssertEqual(mock.mockModel?.account.withoutAddress, true)
    }
    
    func testContinueRegister_WhenReceiveActionFromViewController_ShouldSetTrueValueInModel() {
        sut.nextStep()
        
        XCTAssertNotNil(spy.actionSelected)
        XCTAssertEqual(mock.mockModel?.account.withoutAddress, false)
    }
}

private final class RegistrationFixedLocationPresenterSpy: RegistrationFixedLocationPresenting {
    var viewController: RegistrationChooseProfileDisplay?
    private(set) var actionSelected: RegistrationFixedLocationAction?
    
    func didNextStep(action: RegistrationFixedLocationAction) {
        actionSelected = action
    }
}

private final class RegistrationFixedLocationServiceMock: RegistrationFixedLocationServicing {
    private(set) var mockModel: RegistrationViewModel?
    
    func registerCompletedStep(model: RegistrationViewModel) {
        mockModel = model
    }
}
