import XCTest
import Validator
@testable import PicPayEmpresas

final class RegistrationPersonalInfoPresenterTests: XCTestCase {
    private lazy var coordinator = RegistrationPersonalInfoCoordinatorSpy()
    private lazy var display = RegistrationPersonalInfoDisplaySpy()
    private lazy var sut: RegistrationPersonalInfoPresenter = {
        let presenter = RegistrationPersonalInfoPresenter(coordinator: coordinator)
        presenter.viewController = display
        return presenter
    }()
    
    func testHandleError_WhenReceiveActionFromViewModel_ShouldCallHandleError() {
        sut.handleError(message: "generic error")
        
        XCTAssertEqual(display.handleErrorCount, 1)
        XCTAssertEqual(display.error, "generic error")
    }
    
    func testDidNextStep_WhenReceiveNextScreenActionFromViewModel_ShouldCallNextStepInCoordinator() {
        sut.didNextStep(action: .nextScreen)
        
        XCTAssertEqual(coordinator.actionSelected, RegistrationPersonalInfoAction.nextScreen)
    }
    
    func testValidateSubmit_WhenReceiveSuccesValueFromViewModel_ShouldCallSubmitValueCount() {
        sut.validateSubmit(value: true)
        
        XCTAssertEqual(display.submitValuesCount, 1)
    }
    
    func testValidateSubmit_WhenReceiveFailureValueFromViewModel_ShouldCallHandleError() {
        sut.validateSubmit(value: false)
        
        XCTAssertEqual(display.handleErrorCount, 1)
        XCTAssertNil(display.error)
    }
    
    func testUpdateValues_WhenReceiveAllTextFieldsWithErrorsFromViewModel_ShouldUpdateFieldsWithMessageError() {
        let list = [
            ("generic error", UIPPFloatingTextField()),
            ("generic error 2", UIPPFloatingTextField()),
            ("", UIPPFloatingTextField())
        ]
        
        sut.updateFields(fields: list)
        
        XCTAssertNotNil(display.listUpdateFields)
        XCTAssertEqual(display.listUpdateFields?.first?.message, "generic error")
    }
    
    func testUpdateValues_WhenReceiveAllTextFieldsFromViewModel_ShouldUpdateFieldsWithOutMessageError() {
        let list = [
            ("", UIPPFloatingTextField()),
            ("", UIPPFloatingTextField()),
            ("", UIPPFloatingTextField())
        ]
        
        sut.updateFields(fields: list)
        
        XCTAssertNotNil(display.listUpdateFields)
        XCTAssertEqual(display.listUpdateFields?.first?.message, "")
    }
}

private class RegistrationPersonalInfoCoordinatorSpy: RegistrationPersonalInfoCoordinating {
    var viewController: UIViewController?
    private(set) var actionSelected: RegistrationPersonalInfoAction?
    
    func perform(action: RegistrationPersonalInfoAction) {
        actionSelected = action
    }
}

private class RegistrationPersonalInfoDisplaySpy: RegistrationPersonalInfoDisplay {
    private(set) var error: String?
    private(set) var handleErrorCount = 0
    private(set) var submitValuesCount = 0
    private(set) var listUpdateFields: [(message: String?, textField: UIPPFloatingTextField)]?
    
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)]) {
        listUpdateFields = fields
    }
    
    func submitValues() {
        submitValuesCount += 1
    }
    
    func handleError(error: String?) {
        handleErrorCount += 1
        self.error = error
    }
}
