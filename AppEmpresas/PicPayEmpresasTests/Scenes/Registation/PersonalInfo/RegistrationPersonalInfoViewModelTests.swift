import XCTest
import Core
import Validator
@testable import PicPayEmpresas

final class RegistrationPersonalInfoViewModelTests: XCTestCase {
    private let service = RegistrationPersonalInfoServiceMock()
    private let presenter = RegistrationPersonalInfoPresenterSpy()
    private lazy var sut = RegistrationPersonalInfoViewModel(service: service, presenter: presenter)
    
    func testSubmitValue_WhenSubmitIsFailureFromViewController_ShouldCallHandleError() {
        service.isSuccess = false
        
        sut.submitValues(
            name: "nome usuario",
            mail: "nome@usuario.com",
            cpf: "12345678900",
            birthday: "10/10/1990",
            motherName: "nome mae usuario"
        )
        
        XCTAssertEqual(presenter.handleErrorCount, 1)
        XCTAssertEqual(presenter.error, "verifyDuplciateUserError")
    }
    
    func testSubmitValue_WhenSubmitIsSuccessFromViewController_ShouldCallNextStep() {
        service.isSuccess = true
        
        sut.submitValues(
            name: "nome usuario",
            mail: "nome@usuario.com",
            cpf: "12345678900",
            birthday: "10/10/1990",
            motherName: "nome mae usuario"
        )
        
        XCTAssertEqual(presenter.actionSelected, RegistrationPersonalInfoAction.nextScreen)
    }
    
    func testValidateSubmitSuccess_WhenReceiveAllTextFieldsFormViewController_ShouldCallValidateSubmitInPresenter() {
        let list = createPPFloatingFields(with: true)
        
        sut.validateSubmit(textFields: list)
        
        XCTAssertEqual(presenter.isValidated, true)
    }
    
    func testValidateSubmitError_WhenReceiveAllTextFieldsFormViewController_ShouldCallValidateSubmitInPresenter() {
        let list = createPPFloatingFields(with: false)
        sut.validateSubmit(textFields: list)
        
        XCTAssertEqual(presenter.isValidated, false)
    }
    
    func testUpdateValues_WhenReceiveAllTextFieldsWithErrorsFromViewController_ShouldUpdateFieldsWithMessageError() {
        let list = createPPFloatingFields(with: false)
        sut.updateFields(textFields: list)
        
        XCTAssertNotNil(presenter.listUpdateFields)
        XCTAssertEqual(presenter.listUpdateFields?.first?.message, "generec error")
    }
    
    func testUpdateValues_WhenReceiveAllTextFieldsFromViewController_ShouldUpdateFieldsWithOutMessageError() {
        let list = createPPFloatingFields(with: true)
        
        sut.updateFields(textFields: list)
        
        XCTAssertNotNil(presenter.listUpdateFields)
        XCTAssertEqual(presenter.listUpdateFields?.first?.message, "")
    }
}

// MARK: - Private functios used by configuration
private extension RegistrationPersonalInfoViewModelTests {
    private enum TestsValidateError: String, ValidationError {
        case generic = "generec error"
        
        var message: String {
            rawValue
        }
    }
    
    private func createPPFloatingFields(with text: Bool) -> [UIPPFloatingTextField] {
        let nameTextField = UIPPFloatingTextField()
        let mailTextField = UIPPFloatingTextField()
        let cpfTextField = UIPPFloatingTextField()
        let birthdayTextField = UIPPFloatingTextField()
        let motherNameTextField = UIPPFloatingTextField()
        
        if text {
            nameTextField.text = "Nome usuário teste"
            mailTextField.text = "email@usuario.com"
            cpfTextField.text = "123.456.789-00"
            birthdayTextField.text = "10/10/1990"
            motherNameTextField.text = "Nome mãe usuário"
        }
        
        let list = [nameTextField, mailTextField, cpfTextField, birthdayTextField, motherNameTextField]
        configureValidatorsFields(inputs: list)
        
        return list
    }
    
    private func configureValidatorsFields(inputs: [UIPPFloatingTextField]) {
        
        var nameRule = ValidationRuleSet<String>()
        var nameInput = inputs[0]
        nameRule.add(rule: ValidationRuleLength(min: 10, error: TestsValidateError.generic))
        nameInput.validationRules = nameRule

        var emailRules = ValidationRuleSet<String>()
        var mailInput = inputs[1]
        emailRules.add(rule: ValidationRulePattern(pattern: EmailValidationPattern.standard, error: TestsValidateError.generic))
        mailInput.validationRules = emailRules

        var cpfRules = ValidationRuleSet<String>()
        var cpfInput = inputs[2]
        cpfRules.add(rule: ValidationRuleLength(min: 14, error: TestsValidateError.generic))
        cpfInput.validationRules = cpfRules

        var birthdayRules = ValidationRuleSet<String>()
        var birthdayInput = inputs[3]
        birthdayRules.add(rule: ValidationRuleLength(min: 10, error: TestsValidateError.generic))
        birthdayInput.validationRules = birthdayRules

        var motherNameRules = ValidationRuleSet<String>()
        var motherNameInput = inputs[4]
        motherNameRules.add(rule: ValidationRuleLength(min: 5, error: TestsValidateError.generic))
        motherNameInput.validationRules = motherNameRules
    }
}

// MARK: - Mock Class
private final class RegistrationPersonalInfoServiceMock: RegistrationPersonalInfoServicing {
    var isSuccess = true
    
    func registerPersonalInfo(model: PersonalInfoModel, _ completion: @escaping (Result<VerifyDuplicatedUser?, ApiError>) -> Void) {
        guard isSuccess else {
            completion(.failure(ApiError.serverError))
            return
        }
        
        let model = VerifyDuplicatedUser(verify: true)
        completion(.success(model))
    }
}

// MARK: - Spy Class
private final class RegistrationPersonalInfoPresenterSpy: RegistrationPersonalInfoPresenting {
    var viewController: RegistrationPersonalInfoDisplay?
    
    private(set) var actionSelected: RegistrationPersonalInfoAction?
    private(set) var error: String?
    private(set) var handleErrorCount = 0
    private(set) var listUpdateFields: [(message: String?, textField: UIPPFloatingTextField)]?
    private(set) var isValidated: Bool?
    
    func updateFields(fields: [(message: String?, textField: UIPPFloatingTextField)]) {
        listUpdateFields = fields
    }
    
    func didNextStep(action: RegistrationPersonalInfoAction) {
        actionSelected = action
    }
    
    func validateSubmit(value: Bool) {
        isValidated = value
    }
    
    func handleError(message: String) {
        handleErrorCount += 1
        error = message
    }
}
