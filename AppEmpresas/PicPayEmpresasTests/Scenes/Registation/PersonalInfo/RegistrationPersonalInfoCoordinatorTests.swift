import XCTest
@testable import PicPayEmpresas

final class RegistrationPersonalInfoCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy(rootViewController: UIViewController())
    
    private lazy var sut: RegistrationPersonalInfoCoordinator = {
        let coordinator = RegistrationPersonalInfoCoordinator(model: RegistrationViewModel())
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testDidNextStep_WhenReceiveToProFromPresenter_ShouldPushAddressViewController() {
        sut.perform(action: .nextScreen)
        
        XCTAssertTrue(navigationSpy.currentViewController is RegistrationPhoneViewControllerOld)
    }
}
