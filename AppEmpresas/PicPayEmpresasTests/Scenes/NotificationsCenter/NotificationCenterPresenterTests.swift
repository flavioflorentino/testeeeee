import XCTest
import Core
import UI
@testable import PicPayEmpresas

final class NotificationCenterPresenterTests: XCTestCase {
    private let viewControllerSpy = NotificationDisplaySpy()
    private let coordinator = NotificationCenterCoordinator()
    
    private lazy var sut: NotificationCenterPresenter = {
        let sut = NotificationCenterPresenter(coordinator: coordinator)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func test_updateTable_whencalled_fromViewModel_shouldReturnNotificationsList() {
        let first = Message(
            deeplink: "picpayempresas://extract",
            appVersion: "1.0.0",
            template: "temp",
            message: "message",
            created: "10-20-19",
            seen: false
        )
        
        let last = Message(
            deeplink: "picpayempresas://sale",
            appVersion: "1.0.0",
            template: "temp",
            message: "message",
            created: "10-20-19",
            seen: true
        )
        
        let dataSource: [Message] = [first, last]
        sut.updateTable(notifications: dataSource)
        
        XCTAssertNotNil(viewControllerSpy.data)
        XCTAssertEqual(viewControllerSpy.data?.count, 2)
    }
    
    func test_handleError_whenCalled_fromViewModel_shouldPresentErrorView() {
        let error = ApiError.serverError
        sut.handleError(error: error.localizedDescription)
        
        XCTAssertEqual(viewControllerSpy.callErrorCount, 1)
    }
    
    func testStartLoad_whenCalledFromViewModel_ShouldStartLoadAnimation() {
        sut.startLoad()
        
        XCTAssertEqual(viewControllerSpy.startLoadCount, 1)
    }
    
    func testStopLoad_whenCalledFromViewModel_ShouldStartLoadAnimation() {
        sut.stopLoad()
        
        XCTAssertEqual(viewControllerSpy.stopLoadCount, 1)
    }
}

private final class NotificationDisplaySpy: NotificationCenterDisplay {
    private(set) var data: [NotificationCenterViewCellPresenting]?
    private(set) var callErrorCount: Int = 0
    private(set) var startLoadCount = 0
    private(set) var stopLoadCount = 0
    
    func updateTable(notifications: [NotificationCenterViewCellPresenting]) {
        data = notifications
    }
    
    func showError() {
        callErrorCount += 1
    }
    
    func startLoad() {
        startLoadCount += 1
    }
    
    func stopLoad() {
        stopLoadCount += 1
    }
}
