import XCTest
import Core
@testable import PicPayEmpresas

final class NotificationCenterViewModelTests: XCTestCase {
    private let service = NotificationCenterServiceMock()
    private let presenter = NotificationCenterPresenterSpy()
    private lazy var sut = NotificationCenterViewModel(service: service, presenter: presenter)
    
    func test_getNotification_whenCalledFromRefreshControl_shouldNotStartLoading() {
        service.isSuccess = true
        
        sut.loadNotifications(wasCalledFromRefreshControl: true)
        XCTAssertEqual(presenter.startLoadCount, 0)
        XCTAssertEqual(presenter.stopLoadCount, 1)
    }
    
    func test_getNotification_whenNotCalledFromRefreshControl_shouldStartLoading() {
        service.isSuccess = true
        
        sut.loadNotifications(wasCalledFromRefreshControl: false)
        XCTAssertEqual(presenter.startLoadCount, 1)
        XCTAssertEqual(presenter.stopLoadCount, 1)
    }
    
    func test_getNotification_whenReceiveSuccess_fromService_shouldReturnNotificationList() {
        service.isSuccess = true
        
        sut.loadNotifications(wasCalledFromRefreshControl: false)
        
        XCTAssertNotNil(presenter.notificationsReceived)
        XCTAssertEqual(presenter.notificationsReceived?.count, 2)
    }
    
    func test_getNotification_whenReceiveError_fromService_shouldReturnMessageError() {
        service.isSuccess = false
        
        sut.loadNotifications(wasCalledFromRefreshControl: true)
        XCTAssertEqual(presenter.handleErrorCount, 1)
        XCTAssertNotNil(presenter.error)
    }
    
    func test_didNextStep_whenReciveActionTapCellActionFromViewController_ShouldCallNextStepFromPresenter() {
        sut.loadNotifications(wasCalledFromRefreshControl: true)
        sut.didTapCell(at: 0)
        
        XCTAssertNotNil(presenter.currentAction)
    }
}

private final class NotificationCenterServiceMock: NotificationCenterServicing {
    var isSuccess = true
    
    func getNotifications(completion: @escaping (Result<Notifications, ApiError>) -> Void) {
        guard isSuccess else {
            completion(.failure(ApiError.serverError))
            return
        }
        
        let first = Message(
            deeplink: "picpayempresas://extract",
            appVersion: "1.0.0",
            template: "temp",
            message: "message",
            created: "10-20-19",
            seen: false
        )
        
        let last = Message(
            deeplink: "picpayempresas://sale",
            appVersion: "1.0.0",
            template: "temp",
            message: "message",
            created: "10-20-19",
            seen: true
        )
        
        let notifications = Notifications(notifications: [first, last])
        
        completion(.success(notifications))
    }
}

private final class NotificationCenterPresenterSpy: NotificationCenterPresenting {
    var viewController: NotificationCenterDisplay?
    
    private(set) var notificationsReceived: [Message]?
    private(set) var error: String?
    private(set) var handleErrorCount = 0
    private(set) var startLoadCount = 0
    private(set) var stopLoadCount = 0
    private(set) var currentAction: NotificationCenterAction?
    
    func updateTable(notifications: [Message]) {
        notificationsReceived = notifications
    }
    
    func handleError(error: String) {
        handleErrorCount += 1
        self.error = error
    }
    
    func startLoad() {
        startLoadCount += 1
    }
    
    func stopLoad() {
        stopLoadCount += 1
    }
    
    func didNextStep(action: NotificationCenterAction) {
        currentAction = action
    }
}
