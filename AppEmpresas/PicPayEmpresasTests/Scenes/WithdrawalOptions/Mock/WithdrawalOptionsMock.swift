import XCTest
@testable import PicPayEmpresas

final class WithdrawalOptionsMock {
    func getAccounts() -> [WithdrawAccount]? {
        LocalMock<[WithdrawAccount]>().localMock("withdrawAccounts")
    }
    
    func getValidPicPayAccounts() -> [PicpayAccountModel]? {
        LocalMock<[PicpayAccountModel]>().localMock("validPicpayAccounts")
    }
    
    func getInvalidPicPayAccounts() -> [PicpayAccountModel]? {
        LocalMock<[PicpayAccountModel]>().localMock("invalidPicpayAccounts")
    }
    
    func getBankError() -> WithdrawalOptionsBankError? {
        guard let error = LocalMock<BankAccountValidation>().localMock("BankAccountValidationInvalid") else { return nil }
        let seller = Seller()
        return WithdrawalOptionsBankError(bankError: error, seller: seller, companyType: .individual)
    }
}
