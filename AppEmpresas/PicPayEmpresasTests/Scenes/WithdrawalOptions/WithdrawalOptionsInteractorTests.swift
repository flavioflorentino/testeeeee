import Core
import CoreSellerAccount
import XCTest
import FeatureFlag
@testable import PicPayEmpresas

private final class WithdrawalOptionsPresenterSpy: WithdrawalOptionsPresenting {
    var viewController: WithdrawalOptionsDisplay?
    
    var setOptionsCompletion: (() -> Void)?
    var presentErrorViewCompletion: (() -> Void)?

    private (set) var didPresentPicpayAccountInactive = 0
    private (set) var didPresentNoPicPayAccount = 0
    private (set) var didShowErrorView = 0
    private (set) var setLoadingCount = 0
    private (set) var isLoading: Bool = false
    private (set) var options: [WithdrawalOptionsInfo]?
    private (set) var withdrawAccount: WithdrawAccount?
    private (set) var picPayAccount: PicpayAccountModel?
    private (set) var balanceItem: WalletBalanceItem?
    private (set) var presentBankErrorCount = 0
    private (set) var bankError: WithdrawalOptionsBankError?

    func setLoading(isLoading: Bool) {
        self.isLoading = isLoading
        setLoadingCount += 1
    }
    
    func setWithdrawalOptions(_ options: [WithdrawalOptionsInfo]) {
        self.options = options
        setOptionsCompletion?()
    }
    
    func presentBankWithdrawal(account: WithdrawAccount, balanceItem: WalletBalanceItem) {
        withdrawAccount = account
        self.balanceItem = balanceItem
    }
    
    func presentPicpayWithdrawal(account: PicpayAccountModel, balanceItem: WalletBalanceItem) {
        picPayAccount = account
        self.balanceItem = balanceItem
    }
    
    func presentPicpayAccountInactive() {
        didPresentPicpayAccountInactive += 1
    }
    
    func presentNoPicpayAccount() {
        didPresentNoPicPayAccount += 1
    }
    
    func showErrorView() {
        didShowErrorView += 1
        presentErrorViewCompletion?()
    }
    
    func presentBankError(bankError: WithdrawalOptionsBankError) {
        self.bankError = bankError
        presentBankErrorCount += 1
    }
}

private final class WithdrawalOptionsServiceMock: WithdrawalOptionsServicing {
    private let mocks = WithdrawalOptionsMock()
    
    lazy var getAccountsResult: Result<[WithdrawAccount], ApiError> = {
        guard let response = mocks.getAccounts() else {
            XCTFail("Missing getAccounts")
            return .failure(.connectionFailure)
        }
        return .success(response)
    }()
    
    lazy var getPicPayResult: Result<[PicpayAccountModel], ApiError> = {
        guard let response = mocks.getValidPicPayAccounts() else {
            XCTFail("Missing getValidPicPayAccounts")
            return .failure(.connectionFailure)
        }
        return .success(response)
    }()
    
    func getAccounts(_ completion: @escaping (Result<[WithdrawAccount], ApiError>) -> Void) {
        completion(getAccountsResult)
    }
    
    func getPicpayUsers(_ completion: @escaping (Result<[PicpayAccountModel], ApiError>) -> Void) {
        completion(getPicPayResult)
    }
}


final class WithdrawalOptionsViewModelTests: XCTestCase {
    private let mocks = WithdrawalOptionsMock()
    private let timeout: Double = 2
    private let serviceMock = WithdrawalOptionsServiceMock()
    private let presenterSpy = WithdrawalOptionsPresenterSpy()
    
    private let featureManager: FeatureManagerMock = {
        let featureManager = FeatureManagerMock()
        featureManager.override(key: .isReceivementAccountAvailable, with: true)
        return featureManager
    }()
    
    private let sellerInfoManager: SellerInfoManagerMock = {
        let sellerInfoManager = SellerInfoManagerMock()
        let accountDetails = AccountDetails(
            bizAccountType: .receivementAccount,
            companyType: .individual,
            companyNature: .ei,
            hasPartners: false
        )
        sellerInfoManager.accountDetailsResult = .success(accountDetails)
        return sellerInfoManager
    }()
    
    private var bankError: WithdrawalOptionsBankError?
    
    private lazy var sut: WithdrawalOptionsInteractor? = {
        guard let walletBalanceItem = WalletBalanceItemMock().balanceItem() else {
            XCTFail()
            return nil
        }
        
        let viewModel = WithdrawalOptionsViewModel(balanceItem: walletBalanceItem, bankAccountError: bankError)
        
        let container = DependencyContainerMock(featureManager, sellerInfoManager)
        let interactor = WithdrawalOptionsInteractor(
                service: serviceMock,
                presenter: presenterSpy,
                dependencies: container,
                viewModel: viewModel
        )
        return interactor
    }()
    
    func testLoadOptions_WhenAllSuccess_ShouldHaveAllAccountOptions() throws {
        let expectation = self.expectation(description: "WithdrawalOptions were loaded")
        
        let mainAccount = try XCTUnwrap(mocks.getAccounts()?.first(where: { $0.isPrimary }))
        let mainPicPayAccount = try XCTUnwrap(mocks.getValidPicPayAccounts()?.first)
        
        let bankAccountOption = BankAccountWithdrawOption(bankAccount: mainAccount)
        let picpayWithdrawalOption = PicPayWithdrawOption(picpayAccount: mainPicPayAccount)
        
        presenterSpy.setOptionsCompletion = {
            expectation.fulfill()
        }
        
        sut?.loadOptions()
        
        waitForExpectations(timeout: timeout) { _ in
            XCTAssertEqual(self.presenterSpy.options?.count, 2)
            
            let presenterAccount = self.presenterSpy.options?.last as? BankAccountWithdrawOption
            XCTAssertEqual(presenterAccount, bankAccountOption)
            
            let presenterPicPayAccount = self.presenterSpy.options?.first as? PicPayWithdrawOption
            XCTAssertEqual(presenterPicPayAccount, picpayWithdrawalOption)
        }
    }
    
    func testLoadOptions_ShouldStopLoading() {
        let expectation = self.expectation(description: "WithdrawalOptions were loaded")
        
        presenterSpy.setOptionsCompletion = {
            expectation.fulfill()
        }
        
        sut?.loadOptions()
        
        waitForExpectations(timeout: timeout) { _ in
            XCTAssertFalse(self.presenterSpy.isLoading)
            XCTAssertEqual(2, self.presenterSpy.setLoadingCount)
        }
    }
        
    func testLoadOptions_WhenFailGetBankAccounts_ShouldNotHaveOptionsAndPresentError() {
        let expectation = self.expectation(description: "WithdrawalOptions were loaded with error")
        serviceMock.getAccountsResult = Result.failure(.badRequest(body: RequestError()))
        
        presenterSpy.presentErrorViewCompletion = {
            expectation.fulfill()
        }
        
        sut?.loadOptions()
        
        waitForExpectations(timeout: timeout) { _ in
            XCTAssertNil(self.presenterSpy.options)
            XCTAssertEqual(1, self.presenterSpy.didShowErrorView)
        }
    }
    
    func testLoadOptions_WhenFailGetPicPayAccounts_ShouldNotHaveOptionsAndPresentError() {
        let expectation = self.expectation(description: "WithdrawalOptions were loaded with error")
        serviceMock.getPicPayResult = Result.failure(.badRequest(body: RequestError()))
        
        presenterSpy.presentErrorViewCompletion = {
            expectation.fulfill()
        }
        
        sut?.loadOptions()
        
        waitForExpectations(timeout: timeout) { _ in
            XCTAssertNil(self.presenterSpy.options)
            XCTAssertEqual(1, self.presenterSpy.didShowErrorView)
        }
    }
    
    func testLoadOptions_WhenReceivementAccountAndLTDA_ShouldNotLoadPicPayAccount() throws {
        let accountDetails = AccountDetails(bizAccountType: .receivementAccount, companyType: .multiPartners, companyNature: .ltda, hasPartners: true)
        sellerInfoManager.accountDetailsResult = .success(accountDetails)
        
        let mainAccount = try XCTUnwrap(mocks.getAccounts()?.first(where: { $0.isPrimary }))
        let bankAccountOption = BankAccountWithdrawOption(bankAccount: mainAccount)
        
        let expectation = self.expectation(description: "Load only BankAccount")
        
        presenterSpy.setOptionsCompletion = {
            expectation.fulfill()
        }
        
        sut?.loadOptions()
        
        waitForExpectations(timeout: timeout) { _ in
            XCTAssertEqual(self.presenterSpy.options?.count, 1)
            
            let presenterAccount = self.presenterSpy.options?.first as? BankAccountWithdrawOption
            XCTAssertEqual(presenterAccount, bankAccountOption)
        }
    }
    
    func testLoadOptions_WhenNotReceivementAccountOrNotLTDA_ShouldLoadPicPayAccount() throws {
        let expectation = self.expectation(description: "WithdrawalOptions were loaded")
        
        let mainAccount = try XCTUnwrap(mocks.getAccounts()?.first(where: { $0.isPrimary }))
        let mainPicPayAccount = try XCTUnwrap(mocks.getValidPicPayAccounts()?.first)
        
        let bankAccountOption = BankAccountWithdrawOption(bankAccount: mainAccount)
        let picpayWithdrawalOption = PicPayWithdrawOption(picpayAccount: mainPicPayAccount)
        
        presenterSpy.setOptionsCompletion = {
            expectation.fulfill()
        }
        
        sut?.loadOptions()
        
        waitForExpectations(timeout: timeout) { _ in
            XCTAssertEqual(self.presenterSpy.options?.count, 2)
            
            let presenterAccount = self.presenterSpy.options?.last as? BankAccountWithdrawOption
            XCTAssertEqual(presenterAccount, bankAccountOption)
            
            let presenterPicPayAccount = self.presenterSpy.options?.first as? PicPayWithdrawOption
            XCTAssertEqual(presenterPicPayAccount, picpayWithdrawalOption)
        }
    }
    
    func testSelectPicPayAccount_WhenValid_ShouldNavigateToPicPayWithdrawal() throws {
        let expectation = self.expectation(description: "WithdrawalOptions were loaded")
        let indexPath = IndexPath(item: 0, section: 0)
        
        let mainPicPayAccount = try XCTUnwrap(mocks.getValidPicPayAccounts()?.first)
        let walletBalanceItem = try XCTUnwrap(WalletBalanceItemMock().balanceItem())
        
        presenterSpy.setOptionsCompletion = {
            expectation.fulfill()
        }
        
        sut?.loadOptions()
        
        waitForExpectations(timeout: timeout) { _ in
            self.sut?.selectIndexPath(indexPath: indexPath)
            
            XCTAssertEqual(self.presenterSpy.picPayAccount, mainPicPayAccount)
            XCTAssertEqual(self.presenterSpy.balanceItem, walletBalanceItem)
        }
    }
    
    func testSelectPicPayAccount_WhenInvalid_ShouldNavigateToInvalidPicPayAccount() throws {
        let expectation = self.expectation(description: "WithdrawalOptions were loaded")
        let indexPath = IndexPath(item: 0, section: 0)
        let response = try XCTUnwrap(mocks.getInvalidPicPayAccounts())
        serviceMock.getPicPayResult = Result.success(response)
        
        presenterSpy.setOptionsCompletion = {
            expectation.fulfill()
        }
        
        sut?.loadOptions()
        
        waitForExpectations(timeout: timeout) { _ in
            self.sut?.selectIndexPath(indexPath: indexPath)
            
            XCTAssertEqual(1, self.presenterSpy.didPresentPicpayAccountInactive)
            XCTAssertNil(self.presenterSpy.picPayAccount)
        }
    }
    
    func testSelectPicPayAccount_WhenNone_ShouldNavigateToNoPicPayAccount() {
        let expectation = self.expectation(description: "WithdrawalOptions were loaded")
        let indexPath = IndexPath(item: 0, section: 0)
        serviceMock.getPicPayResult = Result.success([])
        
        presenterSpy.setOptionsCompletion = {
            expectation.fulfill()
        }
        
        sut?.loadOptions()
        
        waitForExpectations(timeout: timeout) { _ in
            self.sut?.selectIndexPath(indexPath: indexPath)
            
            XCTAssertNil(self.presenterSpy.picPayAccount)
            XCTAssertEqual(1, self.presenterSpy.didPresentNoPicPayAccount)
        }
    }
    
    func testSelectBankAccount_ShouldNavigateToBankAccount() throws {
        let expectation = self.expectation(description: "WithdrawalOptions were loaded")
        let indexPath = IndexPath(item: 1, section: 0)
        let mainAccount = try XCTUnwrap(mocks.getAccounts()?.first(where: { $0.isPrimary }))
        let walletBalanceItem = try XCTUnwrap(WalletBalanceItemMock().balanceItem())
        
        presenterSpy.setOptionsCompletion = {
            expectation.fulfill()
        }
        
        sut?.loadOptions()
        
        waitForExpectations(timeout: timeout) { _ in
            self.sut?.selectIndexPath(indexPath: indexPath)
            
            XCTAssertEqual(mainAccount, self.presenterSpy.withdrawAccount)
            XCTAssertEqual(self.presenterSpy.balanceItem, walletBalanceItem)
        }
    }
    
    func testSelectBankAccount_WhenHasError_ShouldPresentBankErrorModal() throws {
        bankError = mocks.getBankError()
        let expectation = self.expectation(description: "WithdrawalOptions were loaded")
        let indexPath = IndexPath(item: 1, section: 0)
        
        presenterSpy.setOptionsCompletion = {
            expectation.fulfill()
        }
        
        sut?.loadOptions()
        
        waitForExpectations(timeout: timeout) { _ in
            self.sut?.selectIndexPath(indexPath: indexPath)
            
            XCTAssertEqual(self.presenterSpy.presentBankErrorCount, 1)
            XCTAssertEqual(self.presenterSpy.bankError, self.bankError)
        }
    }
}
