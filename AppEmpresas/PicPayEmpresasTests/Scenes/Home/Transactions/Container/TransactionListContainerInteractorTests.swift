import Foundation
import XCTest
@testable import PicPayEmpresas

private class TransactionListContainerPresentingSpy: TransactionListContainerPresenting {
    var viewController: TransactionListContainerDisplaying?

    //MARK: - presentSegmentedControl
    private(set) var presentSegmentedControlCallsCount = 0

    func presentSegmentedControl() {
        presentSegmentedControlCallsCount += 1
    }

    //MARK: - presentActiveList
    private(set) var presentActiveListCallsCount = 0
    private(set) var presentActiveListReceivedInvocations: [TransactionTabOption] = []

    func presentActiveList(_ tabOption: TransactionTabOption) {
        presentActiveListCallsCount += 1
        presentActiveListReceivedInvocations.append(tabOption)
    }

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [TransactionListContainerAction] = []

    func didNextStep(action: TransactionListContainerAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }
}

final private class TransactionListContainerInteractorTests: XCTestCase {
    private lazy var presentingSpy = TransactionListContainerPresentingSpy()
    private lazy var container = DependencyContainerMock()
    private lazy var descriptor = TransactionsDescriptor(
        displayHeader: .title, 
        seller: Seller(), 
        selectedTab: .picPayTransactions
    )

    private lazy var sut = TransactionListContainerInteractor(
        presenter: presentingSpy, 
        descriptor: descriptor, 
        dependencies: container
    )

    func testSetupHeader_WhenTypeIsEqualTitle_ShouldNotPresentSegmentedControl() {
        descriptor = TransactionsDescriptor(
            displayHeader: .title,
            seller: Seller(), 
            selectedTab: .picPayTransactions
        )

        sut.setupHeader()

        XCTAssertEqual(presentingSpy.presentSegmentedControlCallsCount, 0)
    }

    func testSetupHeader_WhenTypeIsEqualTabs_ShouldPresentSegmentedControl() {
        descriptor = TransactionsDescriptor(
            displayHeader: .tabs,
            seller: Seller(), 
            selectedTab: .picPayTransactions
        )

        sut.setupHeader()

        XCTAssertEqual(presentingSpy.presentSegmentedControlCallsCount, 1)
    }
    
    func testSetupActiveList_WhenUser_ShouldPresentUserActiveList() {
        descriptor = TransactionsDescriptor(
            displayHeader: .tabs,
            seller: Seller(), 
            selectedTab: .picPayTransactions
        )
        
        sut.setupActiveList()
        
        let invocation = presentingSpy.presentActiveListReceivedInvocations.last
        XCTAssertEqual(invocation, .picPayTransactions)
        XCTAssertEqual(presentingSpy.presentActiveListCallsCount, 1)
    }
    
    func testSetupActiveList_WhenPix_ShouldPresentPixActiveList() {
        descriptor = TransactionsDescriptor(
            displayHeader: .tabs,
            seller: Seller(), 
            selectedTab: .pixTransactions
        )
        
        sut.setupActiveList()
        
        let invocation = presentingSpy.presentActiveListReceivedInvocations.last
        XCTAssertEqual(invocation, .pixTransactions)
        XCTAssertEqual(presentingSpy.presentActiveListCallsCount, 1)
    }
    
    func testHistoryAction_ShouldCallDidNextStepWithHistoryAction() {
        sut.historyAction()
        
        let invocation = presentingSpy.didNextStepActionReceivedInvocations.last
        XCTAssertEqual(invocation, .history)
        XCTAssertEqual(presentingSpy.didNextStepActionCallsCount, 1)
    }
}
