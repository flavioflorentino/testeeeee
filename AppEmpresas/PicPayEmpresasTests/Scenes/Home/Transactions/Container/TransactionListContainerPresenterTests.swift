import Foundation
import XCTest
@testable import PicPayEmpresas

private class TransactionListContainerDisplayingSpy: TransactionListContainerDisplaying {

    //MARK: - displayActiveList
    private(set) var displayActiveListCallsCount = 0
    private(set) var displayActiveListReceivedInvocations: [TransactionTabOption] = []

    func displayActiveList(_ tabOption: TransactionTabOption) {
        displayActiveListCallsCount += 1
        displayActiveListReceivedInvocations.append(tabOption)
    }

    //MARK: - displaySegmentedControl
    private(set) var displaySegmentedControlCallsCount = 0

    func displaySegmentedControl() {
        displaySegmentedControlCallsCount += 1
    }
}

private class TransactionListContainerCoordinatingSpy: TransactionListContainerCoordinating {
    var viewController: UIViewController?

    //MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [TransactionListContainerAction] = []

    func perform(action: TransactionListContainerAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

final class TransactionListContainerPresenterTests: XCTestCase {
    private let coordinatorSpy = TransactionListContainerCoordinatingSpy()
    private let displaySpy = TransactionListContainerDisplayingSpy()

    lazy var sut: TransactionListContainerPresenting = {
        let presenter = TransactionListContainerPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testPresentActiveList_WhenUser_ShouldCallDisplayActiveListWithUser() {
        sut.presentActiveList(.picPayTransactions)
        
        let invocation = displaySpy.displayActiveListReceivedInvocations.last
        XCTAssertEqual(invocation, .picPayTransactions)
        XCTAssertEqual(displaySpy.displayActiveListCallsCount, 1)
    }
    
    func testPresentActiveList_WhenPix_ShouldCallDisplayActiveListWithPix() {
        sut.presentActiveList(.pixTransactions)
        
        let invocation = displaySpy.displayActiveListReceivedInvocations.last
        XCTAssertEqual(invocation, .pixTransactions)
        XCTAssertEqual(displaySpy.displayActiveListCallsCount, 1)
    }
    
    func testDidNextStep_WhenHistory_ShouldCallCoordinatorNextStepWithHistory() {
        sut.didNextStep(action: .history)
        
        let invocation = coordinatorSpy.performActionReceivedInvocations.last
        XCTAssertEqual(invocation, .history)
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
    }
}
