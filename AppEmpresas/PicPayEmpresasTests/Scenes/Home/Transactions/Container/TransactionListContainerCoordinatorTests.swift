import Foundation
import XCTest
@testable import PicPayEmpresas

final private class TransactionListContainerCoordinatorTests: XCTestCase {
    private lazy var viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    
    private lazy var sut: TransactionListContainerCoordinating = {
        let coordinator = TransactionListContainerCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenHistory_ShouldPushSummaryViewController() {
        sut.perform(action: .history)
        XCTAssertTrue(navigationSpy.currentViewController is SummaryViewController)
    }
}
