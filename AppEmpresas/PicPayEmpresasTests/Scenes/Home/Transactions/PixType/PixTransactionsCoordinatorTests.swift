import XCTest
import FeatureFlag
import PIX
@testable import PicPayEmpresas

final private class PixTransactionsCoordinatorTests: XCTestCase {
    private let viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var container = DependencyContainerMock(featureManagerMock)
    
    private lazy var sut: PixTransactionsCoordinating = {
        let coordinator = PixTransactionsCoordinator(dependencies: container)
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testPerformActionShowAllTransactions_ShouldPushTransactionListContainer() {
        let transactionsDescriptor = TransactionsDescriptor(
            displayHeader: .tabs,
            seller: Seller(),
            selectedTab: .pixTransactions
        )
        
        sut.perform(action: .showAllTransactions(transactionsDescriptor: transactionsDescriptor))

        XCTAssertTrue(navigationSpy.currentViewController is TransactionListContainerViewController)
    }
    
    func testPerformActionPresentPixReceipt_ShouldPresentPIXBIZFlowCoordinatorWithReceiptAction() {
        sut.perform(action: .presentPixReceipt(1, isRefundAllowed: false))

        XCTAssertTrue(navigationSpy.presentViewController is ReceiptBizViewController)
    }
}
