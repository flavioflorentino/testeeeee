import LegacyPJ
import XCTest
@testable import PicPayEmpresas

private final class PixTransactionsCoordinatorSpy: PixTransactionsCoordinating {
    var viewController: UIViewController?
    
    private(set) var performCallsCount = 0
    private(set) var performedAction: PixTransactionsAction?
    
    func perform(action: PixTransactionsAction) {
        performCallsCount += 1
        performedAction = action
    }
}

private final class PixTransactionsViewControllerSpy: PixTransactionsDisplaying {
    func enableScroll() {
        // TODO
    }
    
    func setupPagination() {
        // TODO
    }
    
    // MARK: - SetupTransactions
    private(set) var setupTransactionsListViewCallsCount = 0

    func setupTransactionsListView() {
        setupTransactionsListViewCallsCount += 1
    }

    // MARK: - Display
    private(set) var displayTransactionsCallsCount = 0
    private(set) var displayTransactionsReceivedInvocations: [[TransactionViewModel]] = []

    func display(transactions: [TransactionViewModel]) {
        displayTransactionsCallsCount += 1
        displayTransactionsReceivedInvocations.append(transactions)
    }

    // MARK: - SetupButton
    private(set) var setupButtonCallsCount = 0

    func setupButton() {
        setupButtonCallsCount += 1
    }

    // MARK: - SetupLoadingView
    private(set) var startLoadingCallsCount = 0

    func startLoading() {
        startLoadingCallsCount += 1
    }

    // MARK: - HideLoading
    private(set) var stopLoadingCallsCount = 0

    func stopLoading() {
        stopLoadingCallsCount += 1
    }

    // MARK: - DisplayEmptyView
    private(set) var displayEmptyViewCallsCount = 0

    func displayEmptyView() {
        displayEmptyViewCallsCount += 1
    }

    // MARK: - DisplayErrorView
    private(set) var displayErrorViewCallsCount = 0

    func displayErrorView() {
        displayErrorViewCallsCount += 1
    }

    // MARK: - ResetViews
    private(set) var resetViewsCallsCount = 0

    func resetViews() {
        resetViewsCallsCount += 1
    }

    // MARK: - DisplayAlert
    private(set) var displayAlertForIndexPathCallsCount = 0
    private(set) var displayAlertForIndexPathReceivedInvocations: [(transaction: Transaction, indexPath: IndexPath)] = []

    func displayAlert(for transaction: Transaction, indexPath: IndexPath) {
        displayAlertForIndexPathCallsCount += 1
        displayAlertForIndexPathReceivedInvocations.append((transaction: transaction, indexPath: indexPath))
    }
}

final class PixTransactionsPresenterTests: XCTestCase {
    private let coordinatorSpy = PixTransactionsCoordinatorSpy()
    private let viewControllerSpy = PixTransactionsViewControllerSpy()
    
    private lazy var sut: PixTransactionsPresenter = {
        let presenter = PixTransactionsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testShowTransactions_WhenShowButtonIsFalse_ShouldSetupAndShowTransactionsWithAHiddenButton() {
        sut.present(transactions: [], showButton: false)
        
        XCTAssertEqual(viewControllerSpy.resetViewsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.setupTransactionsListViewCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayTransactionsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.setupButtonCallsCount, 0)
    }
    
    func testShowTransactions_WhenShowButtonIsTrue_ShouldSetupAndShowTransactionsWithButton() {
        sut.present(transactions: [], showButton: true)
        
        XCTAssertEqual(viewControllerSpy.resetViewsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.setupTransactionsListViewCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayTransactionsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.setupButtonCallsCount, 1)
    }
    
    func testShowAlert_ShouldDisplayAlert() {
        sut.presentAlert(for: Transaction(), indexPath: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(viewControllerSpy.displayAlertForIndexPathCallsCount, 1)
    }
    
    func testShowLoading_ShouldResetViewsAndSetupLoadingView() {
        sut.showLoading()
        
        XCTAssertEqual(viewControllerSpy.resetViewsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.startLoadingCallsCount, 1)
    }
    
    func testHideLoading_ShouldHideLoading() {
        sut.hideLoading()
        
        XCTAssertEqual(viewControllerSpy.stopLoadingCallsCount, 1)
    }
    
    func testShowError_ShouldResetViewsAndSetupErrorView() {
        sut.presentErrorState()
        
        XCTAssertEqual(viewControllerSpy.resetViewsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayErrorViewCallsCount, 1)
    }
    
    func testShowEmpty_ShouldResetViewsAndSetupEmptyView() {
        sut.presentEmptyState()
        
        XCTAssertEqual(viewControllerSpy.resetViewsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayEmptyViewCallsCount, 1)
    }
    
    func testDidNextStep_ShouldGoToNextStep() {
        sut.didNextStep(action: .presentPixReceipt(1, isRefundAllowed: false))
        
        XCTAssertEqual(coordinatorSpy.performedAction, .presentPixReceipt(1, isRefundAllowed: false))
        XCTAssertEqual(coordinatorSpy.performCallsCount, 1)
    }
}
