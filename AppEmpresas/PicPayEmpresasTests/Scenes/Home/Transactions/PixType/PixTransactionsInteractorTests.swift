import XCTest
import SwiftyJSON
import LegacyPJ
import AnalyticsModule
@testable import PicPayEmpresas

private final class PixTransactionsServiceSpy: PixTransactionsServicing {
    private(set) var fetchTransactionsCallsCount = 0
    
    var transactions = [Transaction(), Transaction(), Transaction(), Transaction(), Transaction()]
    
    lazy var response: Result<TransactionListResponse, LegacyPJError> = .success(TransactionListResponse(pagination: Pagination(), list: transactions))
    
    func fetchTransactions(page: Int, completion: @escaping (Result<TransactionListResponse, LegacyPJError>) -> Void) {
        fetchTransactionsCallsCount += 1
        completion(response)
    }
}

private final class PixTransactionsPresenterSpy: PixTransactionsPresenting {
    func setupPagination() {
        // TODO
    }
    
    var viewController: PixTransactionsDisplaying?

    // MARK: - Present
    private(set) var presentTransactionsShowButtonCallsCount = 0
    private(set) var presentTransactionsShowButtonReceivedInvocations: [(transactions: [Transaction], showButton: Bool)] = []

    func present(transactions: [Transaction], showButton: Bool) {
        presentTransactionsShowButtonCallsCount += 1
        presentTransactionsShowButtonReceivedInvocations.append((transactions: transactions, showButton: showButton))
    }

    // MARK: - PresentAlert
    private(set) var presentAlertForIndexPathCallsCount = 0
    private(set) var presentAlertForIndexPathReceivedInvocations: [(transaction: Transaction, indexPath: IndexPath)] = []

    func presentAlert(for transaction: Transaction, indexPath: IndexPath) {
        presentAlertForIndexPathCallsCount += 1
        presentAlertForIndexPathReceivedInvocations.append((transaction: transaction, indexPath: indexPath))
    }

    // MARK: - ShowLoading
    private(set) var showLoadingCallsCount = 0

    func showLoading() {
        showLoadingCallsCount += 1
    }

    // MARK: - HideLoading
    private(set) var hideLoadingCallsCount = 0

    func hideLoading() {
        hideLoadingCallsCount += 1
    }

    // MARK: - PresentErrorState
    private(set) var presentErrorStateCallsCount = 0

    func presentErrorState() {
        presentErrorStateCallsCount += 1
    }

    // MARK: - PresentEmptyState
    private(set) var presentEmptyStateCallsCount = 0

    func presentEmptyState() {
        presentEmptyStateCallsCount += 1
    }

    // MARK: - DidNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [PixTransactionsAction] = []

    func didNextStep(action: PixTransactionsAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }
}

final class PixTransactionsInteractorTests: XCTestCase {
    private let analyticsMock = AnalyticsSpy()
    
    private let serviceSpy = PixTransactionsServiceSpy()
    private let presenterSpy = PixTransactionsPresenterSpy()
    
    private let seller = Seller()
    
    private lazy var dependencies = DependencyContainerMock(analyticsMock)
    
    private lazy var sut: PixTransactionsInteracting = {
        let interactor = PixTransactionsInteractor(
            seller: seller,
            service: serviceSpy,
            presenter: presenterSpy,
            dependencies: dependencies,
            type: .compact
        )
        return interactor
    }()
    
    func testFetchTransactions_WhenSuccededWithResults_ShouldShowTransactions() {
        sut.fetchTransactions(at: 0)
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentTransactionsShowButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentEmptyStateCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorStateCallsCount, 0)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
    }
    
    func testFetchTransactions_WhenDoNotSuccededWithResults_ShouldShowError() {
        serviceSpy.response = .failure(.requestError)
        
        sut.fetchTransactions(at: 0)
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentTransactionsShowButtonCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentEmptyStateCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorStateCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
    }
    
    func testFetchTransactions_WhenSuccededWithNoResults_ShouldShowEmpty() {
        serviceSpy.response = .success(TransactionListResponse(pagination: Pagination(), list: []))
        sut.fetchTransactions(at: 0)
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentTransactionsShowButtonCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentEmptyStateCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorStateCallsCount, 0)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
    }
    
    func testFetchTransactions_WhenSuccededWithMoreThanFiveResults_ShouldShowFiveResultsOnly() throws {
        serviceSpy.transactions.append(Transaction())
        
        sut.fetchTransactions(at: 0)
        
        XCTAssertEqual(presenterSpy.presentTransactionsShowButtonCallsCount, 1)
        let transactionsCount = try XCTUnwrap(presenterSpy.presentTransactionsShowButtonReceivedInvocations.first?.transactions.count)
        XCTAssertEqual(transactionsCount, 5)
    }
    
    func testFetchTransactions_WhenSuccededWithFiveResults_ShouldShowFiveResults() throws {
        sut.fetchTransactions(at: 0)
        
        XCTAssertEqual(presenterSpy.presentTransactionsShowButtonCallsCount, 1)
        let transactionsCount = try XCTUnwrap(presenterSpy.presentTransactionsShowButtonReceivedInvocations.first?.transactions.count)
        XCTAssertEqual(transactionsCount, 5)
    }
    
    func testFetchTransactions_WhenSuccededWithFiveResults_ShouldNotShowMoreButton() throws {
        sut.fetchTransactions(at: 0)
        
        XCTAssertEqual(presenterSpy.presentTransactionsShowButtonCallsCount, 1)
        let showButton = try XCTUnwrap(presenterSpy.presentTransactionsShowButtonReceivedInvocations.first?.showButton)
        XCTAssertEqual(showButton, false)
    }
    
    func testFetchTransactions_WhenSuccededWithMoreThanFiveResults_ShouldShowMoreButton() throws {
        serviceSpy.transactions.append(Transaction())
        
        sut.fetchTransactions(at: 0)
        
        XCTAssertEqual(presenterSpy.presentTransactionsShowButtonCallsCount, 1)
        let showButton = try XCTUnwrap(presenterSpy.presentTransactionsShowButtonReceivedInvocations.first?.showButton)
        XCTAssertEqual(showButton, true)
    }
    
    func testDidSelectTransaction_WhenTapTransaction_ShouldSendAnAnalyticsEvent() throws {
        sut.fetchTransactions(at: 0)
        
        sut.didSelect(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(analyticsMock.logCalledCount, 1)
        XCTAssertTrue(analyticsMock.equals(to: TransactionAnalytics.transactionDetails(Transaction()).event()))
    }
    
    func testDidSelectTransaction_WhenTapTransaction_ShouldShowAnAlert() throws {
        sut.fetchTransactions(at: 0)
        
        sut.didSelect(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.presentAlertForIndexPathCallsCount, 1)
    }
    
    func testShowPixHub_WhenCalled_ShouldGoToPixHub() {
        sut.showPixHub()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
    }
    
    func testOpenPixReceipt_WhenCalledWithACanceledTransaction_ShouldGoToPixReceiptNotAllowingRefund() {
        let canceledTransaction = Transaction()
        canceledTransaction.id = 1
        canceledTransaction.status = .canceled
        
        sut.openPixReceipt(canceledTransaction)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.first, PixTransactionsAction.presentPixReceipt(1, isRefundAllowed: false))
    }
    
    func testOpenPixReceipt_WhenCalled_ShouldGoToPixReceipt() {
        let transaction = Transaction()
        transaction.id = 1
        transaction.status = .unknown
        
        sut.openPixReceipt(transaction)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.first, PixTransactionsAction.presentPixReceipt(1, isRefundAllowed: true))
    }
    
    func testReturnPixPayment_WhenCalled_ShouldGoToReturnPixPayment() {
        let transaction = Transaction()
        transaction.id = 1
        
        sut.openPixPayment(transaction)
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.first, PixTransactionsAction.returnPixPayment(1))
    }
    
    func testDidNextStep_WhenCalled_ShouldShowAllTransactions() {
        let transactionsDescriptor = TransactionsDescriptor(
            displayHeader: .tabs,
            seller: seller,
            selectedTab: .pixTransactions
        )
        
        sut.didNextStep()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.first, PixTransactionsAction.showAllTransactions(transactionsDescriptor: transactionsDescriptor))
    }
}
