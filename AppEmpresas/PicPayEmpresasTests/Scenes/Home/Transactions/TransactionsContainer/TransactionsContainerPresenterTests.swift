import XCTest
@testable import PicPayEmpresas

private final class TransactionsContainerViewControllerSpy: TransactionsContainerDisplaying {
    // MARK: - SetupPicPayTransactions
    private(set) var setupPicPayTransactionsViewControllerCallsCount = 0
    private(set) var setupPicPayTransactionsViewControllerReceivedInvocations: [PicPayTransactionsViewController] = []

    func setupPicPayTransactions(viewController: PicPayTransactionsViewController) {
        setupPicPayTransactionsViewControllerCallsCount += 1
        setupPicPayTransactionsViewControllerReceivedInvocations.append(viewController)
    }

    // MARK: - SetupPixTransactions
    private(set) var setupPixTransactionsViewControllerCallsCount = 0
    private(set) var setupPixTransactionsViewControllerReceivedInvocations: [PixTransactionsViewController] = []

    func setupPixTransactions(viewController: PixTransactionsViewController) {
        setupPixTransactionsViewControllerCallsCount += 1
        setupPixTransactionsViewControllerReceivedInvocations.append(viewController)
    }

    // MARK: - SetupLinkTransactions
    private(set) var setupLinkTransactionsViewControllerCallsCount = 0
    private(set) var setupLinkTransactionsViewControllerReceivedInvocations: [LinkTransactionsViewController] = []

    func setupLinkTransactions(viewController: LinkTransactionsViewController) {
        setupLinkTransactionsViewControllerCallsCount += 1
        setupLinkTransactionsViewControllerReceivedInvocations.append(viewController)
    }

    // MARK: - DisplayActiveList
    private(set) var displayActiveListCallsCount = 0
    private(set) var displayActiveListReceivedInvocations: [TransactionTabOption] = []

    func displayActiveList(_ tabOption: TransactionTabOption) {
        displayActiveListCallsCount += 1
        displayActiveListReceivedInvocations.append(tabOption)
    }

    // MARK: - SetupSegmentedControl
    private(set) var setupSegmentedControlCallsCount = 0
    private(set) var setupSegmentedControlReceivedInvocations: [[String]] = []

    func setupSegmentedControl(_ tabs: [String]) {
        setupSegmentedControlCallsCount += 1
        setupSegmentedControlReceivedInvocations.append(tabs)
    }

    // MARK: - DisplayTitle
    private(set) var displayTitleCallsCount = 0

    func displayTitle() {
        displayTitleCallsCount += 1
    }

    // MARK: - DisplaySegmentedControl
    private(set) var displaySegmentedControlCallsCount = 0

    func displaySegmentedControl() {
        displaySegmentedControlCallsCount += 1
    }

    // MARK: - BringPicPayToFront
    private(set) var bringPicPayToFrontCallsCount = 0

    func bringPicPayToFront() {
        bringPicPayToFrontCallsCount += 1
    }

    // MARK: - BringPixToFront
    private(set) var bringPixToFrontCallsCount = 0

    func bringPixToFront() {
        bringPixToFrontCallsCount += 1
    }

    // MARK: - ShowPicPayTransactions
    private(set) var showPicPayTransactionsCallsCount = 0
    private(set) var showPicPayTransactionsReceivedInvocations: [Bool] = []

    func showPicPayTransactions(_ show: Bool) {
        showPicPayTransactionsCallsCount += 1
        showPicPayTransactionsReceivedInvocations.append(show)
    }

    // MARK: - ShowPixTransactions
    private(set) var showPixTransactionsCallsCount = 0
    private(set) var showPixTransactionsReceivedInvocations: [Bool] = []

    func showPixTransactions(_ show: Bool) {
        showPixTransactionsCallsCount += 1
        showPixTransactionsReceivedInvocations.append(show)
    }

    // MARK: - ReloadPicPayTransactions
    private(set) var reloadPicPayTransactionsCallsCount = 0

    func reloadPicPayTransactions() {
        reloadPicPayTransactionsCallsCount += 1
    }

    // MARK: - ReloadPixTransactions
    private(set) var reloadPixTransactionsCallsCount = 0

    func reloadPixTransactions() {
        reloadPixTransactionsCallsCount += 1
    }
}

final class TransactionsContainerPresenterTests: XCTestCase {
    private let viewController = TransactionsContainerViewControllerSpy()
    
    private lazy var sut: TransactionsContainerPresenting = {
        let presenter = TransactionsContainerPresenter()
        presenter.viewController = viewController
        return presenter
    }()
    
    func testSetupPicPayTransactions_WhenPicPayTransactionsIsNil_ShouldSetupPicPayTransactions() {
        sut.setupPicPayTransactions(seller: Seller(), type: .compact)
        
        XCTAssertEqual(viewController.setupPicPayTransactionsViewControllerCallsCount, 1)
        XCTAssertTrue(viewController.setupPicPayTransactionsViewControllerReceivedInvocations.isNotEmpty)
    }
    
    func testSetupPicPayTransactions_WhenPicPayTransactionsIsNotNil_ShouldSkipNewSetup() {
        sut.setupPicPayTransactions(seller: Seller(), type: .compact)
        sut.setupPicPayTransactions(seller: Seller(), type: .compact)
        
        XCTAssertEqual(viewController.setupPicPayTransactionsViewControllerCallsCount, 1)
        XCTAssertTrue(viewController.setupPicPayTransactionsViewControllerReceivedInvocations.isNotEmpty)
    }
    
    func testSetupPixTransactions_WhenPixTransactionsIsNil_ShouldSetupPixTransactions() {
        sut.setupPixTransactions(seller: Seller(), type: .compact)
        
        XCTAssertEqual(viewController.setupPixTransactionsViewControllerCallsCount, 1)
        XCTAssertTrue(viewController.setupPixTransactionsViewControllerReceivedInvocations.isNotEmpty)
    }
    
    func testSetupPixTransactions_WhenPixTransactionsIsNotNil_ShouldSkipNewSetup() {
        sut.setupPixTransactions(seller: Seller(), type: .compact)
        sut.setupPixTransactions(seller: Seller(), type: .compact)
        
        XCTAssertEqual(viewController.setupPixTransactionsViewControllerCallsCount, 1)
        XCTAssertTrue(viewController.setupPixTransactionsViewControllerReceivedInvocations.isNotEmpty)
    }
    
    func testPresentTitle_ShouldPresentTitle() {
        sut.presentTitle()
        
        XCTAssertEqual(viewController.displayTitleCallsCount, 1)
    }
    
    func testPresentSegmentedControl_WhenPicPayTabAndPixTab_ShouldPresentPicPayTabAndPixTab() {
        let tabs: [TransactionTabOption] = [.picPayTransactions, .pixTransactions]
        sut.presentSegmentedControl(tabs)
        
        XCTAssertEqual(viewController.setupSegmentedControlCallsCount, 1)
        XCTAssertEqual(viewController.setupSegmentedControlReceivedInvocations.first, [TransactionTabOption.picPayTransactions.description, TransactionTabOption.pixTransactions.description])
        XCTAssertEqual(viewController.displaySegmentedControlCallsCount, 1)
    }
    
    func testPresentSegmentedControl_WhenPicPayTabAndLinkTab_ShouldPresentPicPayTabAndLinkTab() {
        let tabs: [TransactionTabOption] = [.picPayTransactions, .linkTransactions]
        sut.presentSegmentedControl(tabs)
        
        XCTAssertEqual(viewController.setupSegmentedControlCallsCount, 1)
        XCTAssertEqual(viewController.setupSegmentedControlReceivedInvocations.first, [TransactionTabOption.picPayTransactions.description, TransactionTabOption.linkTransactions.description])
        XCTAssertEqual(viewController.displaySegmentedControlCallsCount, 1)
    }
    
    func testHideAll_ShouldHideAllTabs() {
        sut.hideAll()
        
        XCTAssertEqual(viewController.showPicPayTransactionsCallsCount, 1)
        XCTAssertEqual(viewController.showPicPayTransactionsReceivedInvocations.first, false)
        
        XCTAssertEqual(viewController.showPixTransactionsCallsCount, 1)
        XCTAssertEqual(viewController.showPixTransactionsReceivedInvocations.first, false)
    }
    
    func testPresentActiveList_WhenTabIsPicPay_ShouldPresentPicPayTab() {
        sut.presentActiveList(.picPayTransactions)
        
        XCTAssertEqual(viewController.displayActiveListCallsCount, 1)
        XCTAssertEqual(viewController.displayActiveListReceivedInvocations.first, .picPayTransactions)
    }
    
    func testPresentActiveList_WhenTabIsPix_ShouldPresentPixTab() {
        sut.presentActiveList(.pixTransactions)
        
        XCTAssertEqual(viewController.displayActiveListCallsCount, 1)
        XCTAssertEqual(viewController.displayActiveListReceivedInvocations.first, .pixTransactions)
    }
    
    func testPresentActiveList_WhenTabIsLink_ShouldPresentLinkTab() {
        sut.presentActiveList(.linkTransactions)
        
        XCTAssertEqual(viewController.displayActiveListCallsCount, 1)
        XCTAssertEqual(viewController.displayActiveListReceivedInvocations.first, .linkTransactions)
    }
    
    func testShowPicPayList_ShouldShowPicPayList() {
        sut.showPicPayList()
        
        XCTAssertEqual(viewController.bringPicPayToFrontCallsCount, 1)
        XCTAssertEqual(viewController.showPicPayTransactionsCallsCount, 1)
        XCTAssertEqual(viewController.showPicPayTransactionsReceivedInvocations.first, true)
    }
    
    func testShowPixList_ShouldShowPixList() {
        sut.showPixList()
        
        XCTAssertEqual(viewController.bringPixToFrontCallsCount, 1)
        XCTAssertEqual(viewController.showPixTransactionsCallsCount, 1)
        XCTAssertEqual(viewController.showPixTransactionsReceivedInvocations.first, true)
    }
    
    func testReloadPicPayTransactions_ShouldReloadPicPayTransactions() {
        sut.reloadPicPayTransactions()
        
        XCTAssertEqual(viewController.reloadPicPayTransactionsCallsCount, 1)
    }
    
    func testReloadPixTransactions_ShouldReloadPixTransactions() {
        sut.reloadPixTransactions()
        
        XCTAssertEqual(viewController.reloadPixTransactionsCallsCount, 1)
    }
}
