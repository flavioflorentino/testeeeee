@testable import PicPayEmpresas
@testable import FeatureFlag
import LegacyPJ
import XCTest

private final class TransactionsContainerPresenterSpy: TransactionsContainerPresenting {
    var viewController: TransactionsContainerDisplaying?

    // MARK: - SetupPicPayTransactions
    private(set) var setupPicPayTransactionsSellerCallsCount = 0
    private(set) var setupPicPayTransactionsSellerReceivedInvocations: [Seller] = []

    func setupPicPayTransactions(seller: Seller, type: TransactionsListType) {
        setupPicPayTransactionsSellerCallsCount += 1
        setupPicPayTransactionsSellerReceivedInvocations.append(seller)
    }

    // MARK: - SetupPixTransactions
    private(set) var setupPixTransactionsSellerCallsCount = 0
    private(set) var setupPixTransactionsSellerReceivedInvocations: [Seller] = []

    func setupPixTransactions(seller: Seller, type: TransactionsListType) {
        setupPixTransactionsSellerCallsCount += 1
        setupPixTransactionsSellerReceivedInvocations.append(seller)
    }

    // MARK: - SetupLinkTransactions
    private(set) var setupLinkTransactionsSellerCallsCount = 0
    private(set) var setupLinkTransactionsSellerReceivedInvocations: [Seller] = []

    func setupLinkTransactions(seller: Seller) {
        setupLinkTransactionsSellerCallsCount += 1
        setupLinkTransactionsSellerReceivedInvocations.append(seller)
    }

    // MARK: - PresentTitle
    private(set) var presentTitleCallsCount = 0

    func presentTitle() {
        presentTitleCallsCount += 1
    }

    // MARK: - PresentSegmentedControl
    private(set) var presentSegmentedControlCallsCount = 0
    private(set) var presentSegmentedControlReceivedInvocations: [[TransactionTabOption]] = []

    func presentSegmentedControl(_ tabs: [TransactionTabOption]) {
        presentSegmentedControlCallsCount += 1
        presentSegmentedControlReceivedInvocations.append(tabs)
    }

    // MARK: - HideAll
    private(set) var hideAllCallsCount = 0

    func hideAll() {
        hideAllCallsCount += 1
    }

    // MARK: - PresentActiveList
    private(set) var presentActiveListCallsCount = 0
    private(set) var presentActiveListReceivedInvocations: [TransactionTabOption] = []

    func presentActiveList(_ tabOption: TransactionTabOption) {
        presentActiveListCallsCount += 1
        presentActiveListReceivedInvocations.append(tabOption)
    }

    // MARK: - ShowPicPayList
    private(set) var showPicPayListCallsCount = 0

    func showPicPayList() {
        showPicPayListCallsCount += 1
    }

    // MARK: - ShowPixList
    private(set) var showPixListCallsCount = 0

    func showPixList() {
        showPixListCallsCount += 1
    }

    // MARK: - ShowLinkList
    private(set) var showLinkListCallsCount = 0

    func showLinkList() {
        showLinkListCallsCount += 1
    }

    // MARK: - ReloadPicPayTransactions
    private(set) var reloadPicPayTransactionsCallsCount = 0

    func reloadPicPayTransactions() {
        reloadPicPayTransactionsCallsCount += 1
    }

    // MARK: - ReloadPixTransactions
    private(set) var reloadPixTransactionsCallsCount = 0

    func reloadPixTransactions() {
        reloadPixTransactionsCallsCount += 1
    }

    // MARK: - ReloadLinkTransactions
    private(set) var reloadLinkTransactionsCallsCount = 0

    func reloadLinkTransactions() {
        reloadLinkTransactionsCallsCount += 1
    }
}

final class TransactionsContainerInteractorTests: XCTestCase {
    private let authManager = AuthManagerMock()
    private let featureManager: FeatureManagerMock = {
        let featureManager = FeatureManagerMock()
        featureManager.override(keys: .releasePIXPresenting, with: true)
        return featureManager
    }()
    
    private lazy var seller: Seller = {
        var seller = Seller()
        seller.digitalAccount = true
        seller.biometry = true
        return seller
    }()
    
    private lazy var user: User = {
        var user = User()
        user.isAdmin = true
        authManager.user = user
        return user
    }()
    
    private lazy var dependencies = DependencyContainerMock(authManager, featureManager)
    private let presenter = TransactionsContainerPresenterSpy()

    private lazy var sut: TransactionsContainerInteracting = {
        let _ = user
        let interactor = TransactionsContainerInteractor(
            presenter: presenter,
            dependencies: dependencies,
            seller: seller, type: .compact
        )
        return interactor
    }()
    
    func testSetupPicPayTransactions_ShouldSetupPicPayTransactions() {
        sut.setupPicPayTransactions()
        
        XCTAssertEqual(presenter.setupPicPayTransactionsSellerCallsCount, 1)
        XCTAssertEqual(presenter.setupPicPayTransactionsSellerReceivedInvocations.first, seller)
        XCTAssertEqual(presenter.showPicPayListCallsCount, 1)
    }
    
    func testSetupHeader_WhenPixDisabled_ShouldSetupTabsHeader() {
        featureManager.override(key: .releasePIXPresenting, with: false)
        
        sut.setupHeader()
        
        XCTAssertEqual(presenter.presentTitleCallsCount, 1)
        XCTAssertEqual(presenter.presentSegmentedControlCallsCount, 0)
    }
    
    func testSetupHeader_WhenAllEnabled_ShouldSetupTabsHeader() {
        sut.setupHeader()
        
        XCTAssertEqual(presenter.presentTitleCallsCount, 0)
        XCTAssertEqual(presenter.presentSegmentedControlCallsCount, 1)
        XCTAssertEqual(presenter.presentSegmentedControlReceivedInvocations.first, [.picPayTransactions, .pixTransactions])
    }
    
    func testSetupActiveList_WhenPicPayTab_ShouldPresentPicPayTab() {
        sut.setupActiveList(transactionTab: 0)
        
        XCTAssertEqual(presenter.presentActiveListCallsCount, 1)
        XCTAssertEqual(presenter.presentActiveListReceivedInvocations.first, .picPayTransactions)
    }
    
    func testSetupActiveList_WhenPixTab_ShouldPresentPixTab() {
        sut.setupHeader()
        
        sut.setupActiveList(transactionTab: 1)
        
        XCTAssertEqual(presenter.presentActiveListCallsCount, 1)
        XCTAssertEqual(presenter.presentActiveListReceivedInvocations.first, .pixTransactions)
    }
    
    func testChangedTab_WhenPicPayTab_ShouldShowPicPayList() {
        sut.setupHeader()
        
        sut.changeTab(to: 0)
        
        XCTAssertEqual(presenter.setupPicPayTransactionsSellerCallsCount, 1)
        XCTAssertEqual(presenter.setupPicPayTransactionsSellerReceivedInvocations.first, seller)
        XCTAssertEqual(presenter.showPicPayListCallsCount, 1)
    }
    
    func testChangedTab_WhenPixTab_ShouldShowPixList() {
        sut.setupHeader()
        
        sut.changeTab(to: 1)
        
        XCTAssertEqual(presenter.setupPixTransactionsSellerCallsCount, 1)
        XCTAssertEqual(presenter.setupPixTransactionsSellerReceivedInvocations.first, seller)
        XCTAssertEqual(presenter.showPixListCallsCount, 1)
    }
    
    func testReloadTab_WhenPicPayTab_ShouldReloadPicPayList() {
        sut.reload(transactionTab: 0)
        
        XCTAssertEqual(presenter.reloadPicPayTransactionsCallsCount, 1)
        XCTAssertEqual(presenter.showPicPayListCallsCount, 1)
    }
    
    func testReloadTab_WhenPixTab_ShouldReloadPixList() {
        sut.setupHeader()
        
        sut.reload(transactionTab: 1)
        
        XCTAssertEqual(presenter.reloadPixTransactionsCallsCount, 1)
        XCTAssertEqual(presenter.showPixListCallsCount, 1)
    }
}
