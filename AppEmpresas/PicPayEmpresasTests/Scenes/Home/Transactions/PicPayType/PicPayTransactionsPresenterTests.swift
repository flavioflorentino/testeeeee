@testable import PicPayEmpresas
import LegacyPJ
import XCTest

private final class PicPayTransactionsCoordinatorSpy: PicPayTransactionsCoordinating {
    var viewController: UIViewController?

    // MARK: - Perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [PicPayTransactionsAction] = []

    func perform(action: PicPayTransactionsAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}


private final class PicPayTransactionsViewControllerSpy: PicPayTransactionsDisplaying {
    func enableScroll() {
        // TODO
    }
    
    func setupPagination() {
        // TODO
    }
    
    // MARK: - SetupTransactions
    private(set) var setupTransactionsListViewCallsCount = 0
    
    func setupTransactionsListView() {
        setupTransactionsListViewCallsCount += 1
    }
    
    // MARK: - Display
    private(set) var displayTransactionsCallsCount = 0
    private(set) var displayTransactionsReceivedInvocations: [[TransactionViewModel]] = []
    
    func display(transactions: [TransactionViewModel]) {
        displayTransactionsCallsCount += 1
        displayTransactionsReceivedInvocations.append(transactions)
    }
    
    // MARK: - Update
    private(set) var updateTransactionCallsCount = 0
    private(set) var updateTransactionReceivedInvocations: [TransactionViewModel] = []
    
    func update(transaction: TransactionViewModel) {
        updateTransactionCallsCount += 1
        updateTransactionReceivedInvocations.append(transaction)
    }
    
    // MARK: - SetupButton
    private(set) var setupButtonCallsCount = 0
    
    func setupButton() {
        setupButtonCallsCount += 1
    }
    
    // MARK: - HideButton
    private(set) var hideButtonCallsCount = 0
    
    func hideButton() {
        hideButtonCallsCount += 1
    }
    
    // MARK: - SetupLoadingView
    private(set) var startLoadingCallsCount = 0
    
    func startLoading() {
        startLoadingCallsCount += 1
    }
    
    // MARK: - HideLoading
    private(set) var stopLoadingCallsCount = 0
    
    func stopLoading() {
        stopLoadingCallsCount += 1
    }
    
    // MARK: - StartLoading
    private(set) var startLoadingAtCallsCount = 0
    private(set) var startLoadingAtReceivedInvocations: [IndexPath] = []
    
    func startLoading(at indexPath: IndexPath) {
        startLoadingAtCallsCount += 1
        startLoadingAtReceivedInvocations.append(indexPath)
    }
    
    // MARK: - StopLoading
    private(set) var stopLoadingAtCallsCount = 0
    private(set) var stopLoadingAtReceivedInvocations: [IndexPath] = []
    
    func stopLoading(at indexPath: IndexPath) {
        stopLoadingAtCallsCount += 1
        stopLoadingAtReceivedInvocations.append(indexPath)
    }
    
    func startPaginatingLoader() {
        // TODO
    }
    
    func stopPaginatingLoader() {
        // TODO
    }
    
    // MARK: - DisplayEmptyView
    private(set) var displayEmptyViewCallsCount = 0
    
    func displayEmptyView() {
        displayEmptyViewCallsCount += 1
    }
    
    // MARK: - DisplayErrorView
    private(set) var displayErrorViewCallsCount = 0
    
    func displayErrorView() {
        displayErrorViewCallsCount += 1
    }
    
    // MARK: - ResetViews
    private(set) var resetViewsCallsCount = 0
    
    func resetViews() {
        resetViewsCallsCount += 1
    }
    
    // MARK: - DisplayAlert
    private(set) var displayAlertIndexPathCallsCount = 0
    private(set) var displayAlertIndexPathReceivedInvocations: [(transaction: Transaction, indexPath: IndexPath)] = []
    
    func displayAlert(_ transaction: Transaction, indexPath: IndexPath) {
        displayAlertIndexPathCallsCount += 1
        displayAlertIndexPathReceivedInvocations.append((transaction: transaction, indexPath: indexPath))
    }
}

final class PicPayTransactionsPresenterTests: XCTestCase {
    private let coordinator = PicPayTransactionsCoordinatorSpy()
    private let viewController = PicPayTransactionsViewControllerSpy()
    
    private lazy var sut: PicPayTransactionsPresenting = {
        let presenter = PicPayTransactionsPresenter(coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()
    
    func testShowTransactions_WhenShowButtonIsTrue_ShouldShowTransactionsAndButton() {
        let transactions = [Transaction(), Transaction()]
        sut.present(transactions: transactions, showButton: true)
        
        XCTAssertEqual(viewController.resetViewsCallsCount, 1)
        XCTAssertEqual(viewController.setupTransactionsListViewCallsCount, 1)
        XCTAssertEqual(viewController.displayTransactionsCallsCount, 1)
        XCTAssertEqual(viewController.setupButtonCallsCount, 1)
        XCTAssertEqual(viewController.hideButtonCallsCount, 0)
    }
    
    func testShowTransactions_WhenShowButtonIsFalse_ShouldShowTransactionsAndHideButton() {
        let transactions = [Transaction(), Transaction()]
        sut.present(transactions: transactions, showButton: false)
        
        XCTAssertEqual(viewController.resetViewsCallsCount, 1)
        XCTAssertEqual(viewController.setupTransactionsListViewCallsCount, 1)
        XCTAssertEqual(viewController.displayTransactionsCallsCount, 1)
        XCTAssertEqual(viewController.setupButtonCallsCount, 0)
    }
    
    func testShowErrorView_ShouldShowErrorView() {
        sut.presentErrorState()
        
        XCTAssertEqual(viewController.resetViewsCallsCount, 1)
        XCTAssertEqual(viewController.displayErrorViewCallsCount, 1)
    }
    
    func testShowErrorPopUp_ShouldPerformErrorPopUp() {
        let message = "mensagem de erro"
        sut.presentErrorPopUp(with: message)
        
        XCTAssertEqual(coordinator.performActionCallsCount, 1)
        XCTAssertEqual(coordinator.performActionReceivedInvocations.first, .modalError(message))
    }
    
    func testShowAlert_ShouldShowAlert() {
        sut.presentAlert(for: Transaction(), indexPath: IndexPath())
        
        XCTAssertEqual(viewController.displayAlertIndexPathCallsCount, 1)
    }
    
    func testShowLoading_ShouldShowLoading() {
        sut.startLoading()
        
        XCTAssertEqual(viewController.resetViewsCallsCount, 1)
        XCTAssertEqual(viewController.startLoadingCallsCount, 1)
    }
    
    func testHideLoading_ShouldHideLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(viewController.stopLoadingCallsCount, 1)
    }
    
    func testStartLoadingAtTransaction_ShouldStartLoadingAtSameTransaction() {
        let index = IndexPath()
        sut.startLoading(at: index)
        
        XCTAssertEqual(viewController.startLoadingAtCallsCount, 1)
        XCTAssertEqual(viewController.startLoadingAtReceivedInvocations.first, index)
    }
    
    func testStopLoadingAtTransaction_ShouldStopLoadingAtSameTransaction() {
        let index = IndexPath()
        sut.stopLoading(at: index)
        
        XCTAssertEqual(viewController.stopLoadingAtCallsCount, 1)
        XCTAssertEqual(viewController.stopLoadingAtReceivedInvocations.first, index)
    }
    
    func testShowEmpty_ShouldShowEmptyView() {
        sut.presentEmptyState()
        
        XCTAssertEqual(viewController.resetViewsCallsCount, 1)
        XCTAssertEqual(viewController.displayEmptyViewCallsCount, 1)
    }
    
    func testUpdateTransaction_ShouldUpdateSameTransaction() {
        let transaction = Transaction()
        sut.update(transaction: transaction)
        
        XCTAssertEqual(viewController.updateTransactionCallsCount, 1)
    }
    
    func testDidNextStep_ShouldGoToNextStepWithSameAction() {
        let action: PicPayTransactionsAction = .modalError("mensagem de erro")
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinator.performActionCallsCount, 1)
        XCTAssertEqual(coordinator.performActionReceivedInvocations.first, action)
    }
}
