@testable import PicPayEmpresas
import UI
import XCTest

final class PicPayTransactionsCoordinatorTests: XCTestCase {
    private let viewController = ViewControllerSpy()
    private lazy var navigationController = NavigationControllerSpy(rootViewController: viewController)
    
    private lazy var dependencies = DependencyContainerMock()
    
    private lazy var sut: PicPayTransactionsCoordinating = {
        let coordinator = PicPayTransactionsCoordinator(dependencies: dependencies)
        coordinator.viewController = navigationController.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionShowAllTransactions_ShouldGoToAllTransactionsScreen() {
        let transactionsDescriptor = TransactionsDescriptor(
            displayHeader: .tabs,
            seller: Seller(),
            selectedTab: .picPayTransactions
        )
        sut.perform(action: .showAllTransactions(transactionsDescriptor: transactionsDescriptor))
        
        XCTAssertEqual(navigationController.pushedCount, 2)
        XCTAssertTrue(navigationController.currentViewController is TransactionListContainerViewController)
    }
    
    func testPerform_WhenActionModalError_ShouldShowPopUp() {
        let message = "mensagem de erro"
        sut.perform(action: .modalError(message))
        
        XCTAssertEqual(viewController.presentViewControllerCounter, 1)
        XCTAssertTrue(viewController.presentViewController is UI.PopupViewController)
    }
}
