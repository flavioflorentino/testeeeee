@testable import PicPayEmpresas
import XCTest
import AnalyticsModule
import LegacyPJ

private final class PicPayTransactionsServiceMock: PicPayTransactionsServicing {
    private(set) var fetchTransactionsCompletionCallsCount = 0

    var transactions = [
        Transaction(),
        Transaction(),
        Transaction(),
        Transaction()
    ]
    
    lazy var response: Result<TransactionListResponse, LegacyPJError> = .success(TransactionListResponse(pagination: Pagination(), list: transactions))
    
    func fetchTransactions(page: Int, completion: @escaping (Result<TransactionListResponse, LegacyPJError>) -> Void) {
        fetchTransactionsCompletionCallsCount += 1
        completion(response)
    }

    // MARK: - Authenticate
    private(set) var authenticateCallsCount = 0
    private(set) var authenticateReceivedInvocations: [((_ result: AuthManager.PerformActionResult) -> Void)] = []
    var authenticateClosure: ((@escaping (_ result: AuthManager.PerformActionResult) -> Void) -> Void)?

    func authenticate(_ completion: @escaping (_ result: AuthManager.PerformActionResult) -> Void) {
        authenticateCallsCount += 1
        authenticateReceivedInvocations.append(completion)
        authenticateClosure?(completion)
    }

    // MARK: - CancelTransaction
    private(set) var cancelTransactionIdCompletionCallsCount = 0
    private(set) var cancelTransactionIdCompletionReceivedInvocations: [(pin: String, id: Int, completion: (Result<Void, LegacyPJError>) -> Void)] = []
    var cancelTransactionIdCompletionClosure: ((String, Int, @escaping (Result<Void, LegacyPJError>) -> Void) -> Void)?

    func cancelTransaction(_ pin: String, id: Int, completion: @escaping (Result<Void, LegacyPJError>) -> Void) {
        cancelTransactionIdCompletionCallsCount += 1
        cancelTransactionIdCompletionReceivedInvocations.append((pin: pin, id: id, completion: completion))
        cancelTransactionIdCompletionClosure?(pin, id, completion)
    }
}

private final class PicPayTransactionsPresenterSpy: PicPayTransactionsPresenting {
    var viewController: PicPayTransactionsDisplaying?

    func setupPagination() {
        // TODO
    }
    
    // MARK: - Present
    private(set) var presentTransactionsShowButtonCallsCount = 0
    private(set) var presentTransactionsShowButtonReceivedInvocations: [(transactions: [Transaction], showButton: Bool)] = []

    func present(transactions: [Transaction], showButton: Bool) {
        presentTransactionsShowButtonCallsCount += 1
        presentTransactionsShowButtonReceivedInvocations.append((transactions: transactions, showButton: showButton))
    }

    // MARK: - PresentErrorState
    private(set) var presentErrorStateCallsCount = 0

    func presentErrorState() {
        presentErrorStateCallsCount += 1
    }

    // MARK: - PresentErrorPopUp
    private(set) var presentErrorPopUpWithCallsCount = 0
    private(set) var presentErrorPopUpWithReceivedInvocations: [String] = []

    func presentErrorPopUp(with error: String) {
        presentErrorPopUpWithCallsCount += 1
        presentErrorPopUpWithReceivedInvocations.append(error)
    }

    // MARK: - PresentAlert
    private(set) var presentAlertForIndexPathCallsCount = 0
    private(set) var presentAlertForIndexPathReceivedInvocations: [(transaction: Transaction, indexPath: IndexPath)] = []

    func presentAlert(for transaction: Transaction, indexPath: IndexPath) {
        presentAlertForIndexPathCallsCount += 1
        presentAlertForIndexPathReceivedInvocations.append((transaction: transaction, indexPath: indexPath))
    }
    
    func willStartPaginating() {
        // TODO
    }
    
    func didEndPaginating() {
        // TODO
    }
    
    private(set) var showLoadingCallsCount = 0
    
    func startLoading() {
        showLoadingCallsCount += 1
    }
    
    private(set) var stopLoadingCallsCount = 0
    
    func stopLoading() {
        stopLoadingCallsCount += 1
    }

    // MARK: - StartLoading
    private(set) var startLoadingAtCallsCount = 0
    private(set) var startLoadingAtReceivedInvocations: [IndexPath] = []

    func startLoading(at indexPath: IndexPath) {
        startLoadingAtCallsCount += 1
        startLoadingAtReceivedInvocations.append(indexPath)
    }

    // MARK: - StopLoading
    private(set) var stopLoadingAtCallsCount = 0
    private(set) var stopLoadingAtReceivedInvocations: [IndexPath] = []

    func stopLoading(at indexPath: IndexPath) {
        stopLoadingAtCallsCount += 1
        stopLoadingAtReceivedInvocations.append(indexPath)
    }

    // MARK: - PresentEmptyState
    private(set) var presentEmptyStateCallsCount = 0

    func presentEmptyState() {
        presentEmptyStateCallsCount += 1
    }

    // MARK: - Update
    private(set) var updateTransactionCallsCount = 0
    private(set) var updateTransactionReceivedInvocations: [Transaction] = []

    func update(transaction: Transaction) {
        updateTransactionCallsCount += 1
        updateTransactionReceivedInvocations.append(transaction)
    }

    // MARK: - DidNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [PicPayTransactionsAction] = []

    func didNextStep(action: PicPayTransactionsAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }
}

final class PicPayTransactionsInteractorTests: XCTestCase {
    private let service = PicPayTransactionsServiceMock()
    private let presenter = PicPayTransactionsPresenterSpy()
    
    private let analytics = AnalyticsSpy()
    private let seller = Seller()
    
    private lazy var dependencies = DependencyContainerMock(analytics)
    
    private lazy var sut: PicPayTransactionsInteracting = {
        let interactor = PicPayTransactionsInteractor(
            seller: seller,
            service: service,
            presenter: presenter,
            dependencies: dependencies,
            type: .compact
        )
        return interactor
    }()
    
    func testFetchTransactions_WhenSuccessResultAndHasTransactionsAndMoreThan5_ShouldShow5TransactionsWithButton() throws {
        service.transactions.append(Transaction())
        service.transactions.append(Transaction())
        
        sut.fetchTransactions(at: 0)
        
        XCTAssertEqual(presenter.presentTransactionsShowButtonCallsCount, 1)
        let invocation = try XCTUnwrap(presenter.presentTransactionsShowButtonReceivedInvocations.first)
        XCTAssertTrue(invocation.showButton)
        XCTAssertEqual(invocation.transactions.count, 5)
    }
    
    func testFetchTransactions_WhenSuccessResultAndHasTransactionsAndLessThan5_ShouldShowLessThan5TransactionsWithoutButton() throws {
        sut.fetchTransactions(at: 0)
        
        XCTAssertEqual(presenter.presentTransactionsShowButtonCallsCount, 1)
        let invocation = try XCTUnwrap(presenter.presentTransactionsShowButtonReceivedInvocations.first)
        XCTAssertFalse(invocation.showButton)
        XCTAssertEqual(invocation.transactions.count, 4)
    }
    
    func testFetchTransactions_WhenSuccessResultAndHasTransactionsAndEqualTo5_ShouldShow5TransactionsWithoutButton() throws {
        service.transactions.append(Transaction())
        
        sut.fetchTransactions(at: 0)
        
        XCTAssertEqual(presenter.presentTransactionsShowButtonCallsCount, 1)
        let invocation = try XCTUnwrap(presenter.presentTransactionsShowButtonReceivedInvocations.first)
        XCTAssertFalse(invocation.showButton)
        XCTAssertEqual(invocation.transactions.count, 5)
    }
    
    func testFetchTransactions_WhenSuccessResultAndHasNotTransactions_ShouldShowEmptyView() {
        service.response = .success(TransactionListResponse(pagination: Pagination(), list: []))
        
        sut.fetchTransactions(at: 0)
        
        XCTAssertEqual(presenter.presentEmptyStateCallsCount, 1)
    }
    
    func testFetchTransactions_WhenFailureResult_ShouldShowErrorView() {
        service.response = .failure(.requestError)
        
        sut.fetchTransactions(at: 0)
        
        XCTAssertEqual(presenter.presentErrorStateCallsCount, 1)
    }
    
    func testDidSelectTransaction_ShouldShowAlert() throws {
        sut.fetchTransactions(at: 0)
        sut.didSelect(at: IndexPath(row: 0, section: 0))
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertTrue(analytics.equals(to: TransactionAnalytics.transactionDetails(Transaction()).event()))
        XCTAssertEqual(presenter.presentAlertForIndexPathCallsCount, 1)
    }
    
    func testAuthenticateUser_WhenFailure_ShouldShowErrorPopUp() {
        service.authenticateClosure = { completion in
            completion(.failure(LegacyPJError(message: "mensagem de erro")))
        }
        sut.authenticateUser(Transaction(), indexPath: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenter.presentErrorPopUpWithCallsCount, 1)
    }
    
    func testAuthenticateUserAndCancelTransaction_WhenAuthenticatedAndCancelWithSuccess_ShouldUpdateTransaction() throws {
        service.authenticateClosure = { completion in
            completion(.success("pin"))
        }
        service.cancelTransactionIdCompletionClosure = { pin, id, completion in
            completion(.success(()))
        }
        let index = IndexPath()
        let transaction = Transaction()
        transaction.id = 123
        sut.authenticateUser(transaction, indexPath: index)
        
        XCTAssertEqual(presenter.startLoadingAtCallsCount, 1)
        XCTAssertEqual(presenter.startLoadingAtReceivedInvocations.first, index)
        XCTAssertEqual(presenter.stopLoadingAtCallsCount, 1)
        XCTAssertEqual(presenter.stopLoadingAtReceivedInvocations.first, index)
        XCTAssertEqual(presenter.updateTransactionCallsCount, 1)
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertTrue(analytics.equals(to: TransactionAnalytics.transactionReturned(transaction, origin: .transaction).event()))
    }
    
    func testAuthenticateUserAndCancelTransaction_WhenAuthenticatedAndCancelWithFailure_ShouldShowErrorPopUp() throws {
        service.authenticateClosure = { completion in
            completion(.success("pin"))
        }
        service.cancelTransactionIdCompletionClosure = { pin, id, completion in
            completion(.failure(.requestError))
        }
        let index = IndexPath(row: 0, section: 0)
        let transaction = Transaction()
        transaction.id = 123
        sut.authenticateUser(transaction, indexPath: index)
        
        XCTAssertEqual(presenter.startLoadingAtCallsCount, 1)
        XCTAssertEqual(presenter.startLoadingAtReceivedInvocations.first, index)
        XCTAssertEqual(presenter.stopLoadingAtCallsCount, 1)
        XCTAssertEqual(presenter.stopLoadingAtReceivedInvocations.first, index)
        XCTAssertEqual(presenter.presentErrorPopUpWithCallsCount, 1)
    }
    
    func testDidNextStep_ShouldGoToNextStepWithTransactionsDescriptor() {
        let transactionsDescriptor = TransactionsDescriptor(
            displayHeader: .tabs,
            seller: seller,
            selectedTab: .picPayTransactions
        )
        sut.didNextStep()
        
        XCTAssertEqual(presenter.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenter.didNextStepActionReceivedInvocations.first, .showAllTransactions(transactionsDescriptor: transactionsDescriptor))
    }
}
