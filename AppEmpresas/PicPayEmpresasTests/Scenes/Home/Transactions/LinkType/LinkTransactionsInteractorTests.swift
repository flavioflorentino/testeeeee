import XCTest
import LegacyPJ
import Core
@testable import PicPayEmpresas

private final class LinkTransactionsServiceSpy: LinkTransactionsServicing {
    private(set) var fetchCallsCount = 0
    private(set) var authenticateCallsCount = 0
    private(set) var refundTransactionCallsCount = 0
    
    var transaction = LinkTransaction(
        id: 6,
        code: "RQEQTNH36R",
        status: .approved,
        value: 0.53,
        valueToSettle: nil,
        consumer: LinkConsumer(
            name: "Everton",
            email: nil,
            cpf: nil,
            cnpj: nil
        ),
        createdDate: "2020-08-12 09:30:40",
        updatedDate: "2020-08-12 09:30:40"
    )
    
    lazy var transactions = [transaction]
    
    lazy var response: Result<LinkTransactionResponse, ApiError> = .success(
        LinkTransactionResponse(
            data: transactions,
            meta: LinkTransactionMeta(
                pagination:
                    PaginationResponse(currentPage: 0, numberOfPages: 1)
            )
        )
    )
    
    func fetch(sellerID: Int, page: Int?, perPage: Int, completion: @escaping (Result<LinkTransactionResponse, ApiError>) -> Void) {
        fetchCallsCount += 1
        completion(response)
    }
    
    func authenticate(_ completion: @escaping (AuthManager.PerformActionResult) -> Void) {
        authenticateCallsCount += 1
    }
    
    func refundTransaction(id: Int, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        refundTransactionCallsCount += 1
    }
}

private final class LinkTransactionsPresenterSpy: LinkTransactionsPresenting {
    var viewController: LinkTransactionsDisplay?
    
    private(set) var didNextStepCallsCount = 0
    private(set) var presentCallsCount = 0
    private(set) var presentDetailsCallsCount = 0
    private(set) var presentErrorAlertCallsCount = 0
    private(set) var startLoadingCallsCount = 0
    private(set) var stopLoadingCallsCount = 0
    private(set) var startLoadingAtCallsCount = 0
    private(set) var stopLoadingAtCallsCount = 0
    private(set) var presentEmptyStateCallsCount = 0
    private(set) var presentErrorCallsCount = 0
    
    private(set) var presentedTransactions: [LinkTransaction] = []
    
    func didNextStep(action: LinkTransactionsAction) {
        didNextStepCallsCount += 1
    }
    
    func present(transactions: [LinkTransaction], showButton: Bool) {
        presentCallsCount += 1
        presentedTransactions = transactions
    }
    
    func presentDetails(of transaction: LinkTransaction, at indexPath: IndexPath) {
        presentDetailsCallsCount += 1
    }
    
    func presentErrorAlert() {
        presentErrorAlertCallsCount += 1
    }
    
    func startLoading() {
        startLoadingCallsCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallsCount += 1
    }
    
    func setupPagination() {
        // TODO
    }
    
    func willStartPaginating() {
        // TODO
    }
    
    func didEndPaginating() {
        // TODO
    }
    
    func startLoading(at indexPath: IndexPath) {
        startLoadingAtCallsCount += 1
    }
    
    func stopLoading(at indexPath: IndexPath) {
        stopLoadingAtCallsCount += 1
    }
    
    func presentEmptyState() {
        presentEmptyStateCallsCount += 1
    }
    
    func presentErrorState() {
        presentErrorCallsCount += 1
    }
}

final class LinkTransactionsInteractorTests: XCTestCase {
    private let serviceSpy = LinkTransactionsServiceSpy()
    private let presenterSpy = LinkTransactionsPresenterSpy()
    
    private let seller = Seller()
    
    private lazy var dependencies = DependencyContainerMock()
    
    private lazy var sut: LinkTransactionsInteracting = {
        let interactor = LinkTransactionsInteractor(
            seller: seller,
            dependencies: dependencies,
            service: serviceSpy,
            presenter: presenterSpy,
            type: .full)
        return interactor
    }()
    
    func testFetchTransactions_WhenSuccededWithResults_ShouldShowTransactions() {
        sut.initialFetch()
        
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentEmptyStateCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 0)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
    }
    
    func testFetchTransactions_WhenSuccededWithNoResults_ShouldShowEmptyView() {
        serviceSpy.response = .success(
            LinkTransactionResponse(
                data: [],
                meta:
                    LinkTransactionMeta(pagination: PaginationResponse(currentPage: 0, numberOfPages: 1)
                    )
            )
        )
        
        sut.initialFetch()
        
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentEmptyStateCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 0)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
    }
    
    func testFetchTransactions_WhenResultsError_ShouldShowErrorView() {
        serviceSpy.response = .failure(.serverError)
        
        sut.fetchTransactions(at: 0)
        
        XCTAssertEqual(presenterSpy.presentCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentEmptyStateCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
    }
}
