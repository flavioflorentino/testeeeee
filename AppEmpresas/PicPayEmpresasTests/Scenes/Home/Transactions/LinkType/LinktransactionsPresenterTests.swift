import LegacyPJ
import UI
import XCTest
@testable import PicPayEmpresas

private final class LinkTransactionsCoordinatorSpy: LinkTransactionsCoordinating {
    var viewController: UIViewController?
    
    private(set) var performCallsCount = 0
    private(set) var performedAction: LinkTransactionsAction?
    
    func perform(action: LinkTransactionsAction) {
        performCallsCount += 1
        performedAction = action
    }
}

private final class LinkTransactionsViewControllerSpy: LinkTransactionsDisplay {
    private(set) var setupTransactionsCallsCount = 0
    private(set) var displayCallsCount = 0
    private(set) var displayDetailsCallsCount = 0
    private(set) var setupButtonCallsCount = 0
    private(set) var startLoadingCallsCount = 0
    private(set) var stopLoadingCallsCount = 0
    private(set) var startPaginationLoadingCallsCount = 0
    private(set) var stopPaginationLoadingCallsCount = 0
    private(set) var displayEmptyViewCallsCount = 0
    private(set) var displayErrorViewCallsCount = 0
    private(set) var displayErrorAlertCallsCount = 0
    private(set) var resetViewsCallsCount = 0
    
    private(set) var displayedDetailsConfigurator: TransactionDetailsAlertConfigurator?
    
    func enableScroll() {
        // TODO
    }
    
    func setupTransactionsListView() {
        setupTransactionsCallsCount += 1
    }
    
    func display(transactions: [TransactionViewModel]) {
        displayCallsCount += 1
    }
    
    func displayDetails(with configurator: TransactionDetailsAlertConfigurator) {
        displayDetailsCallsCount += 1
        displayedDetailsConfigurator = configurator
    }
    
    func setupButton() {
        setupButtonCallsCount += 1
    }
    
    func startLoading() {
        startLoadingCallsCount += 1
    }
    
    func stopLoading() {
        stopLoadingCallsCount += 1
    }
    
    func startPaginatingLoader() {
        startPaginationLoadingCallsCount += 1
    }
    
    func stopPaginatingLoader() {
        stopPaginationLoadingCallsCount += 1
    }
    
    func startLoading(at indexPath: IndexPath) {
        startLoadingCallsCount += 1
    }
    
    func stopLoading(at indexPath: IndexPath) {
        stopLoadingCallsCount += 1
    }
    
    func displayEmptyView() {
        displayEmptyViewCallsCount += 1
    }
    
    func displayErrorView() {
        displayErrorViewCallsCount += 1
    }
    
    func displayErrorAlert() {
        displayErrorAlertCallsCount += 1
    }
    
    func resetViews() {
        resetViewsCallsCount += 1
    }
}

final class LinkTransactionsPresenterTests: XCTestCase {
    private let coordinatorSpy = LinkTransactionsCoordinatorSpy()
    private let viewControllerSpy = LinkTransactionsViewControllerSpy()
    
    private lazy var sut: LinkTransactionsPresenter = {
        let presenter = LinkTransactionsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentTransactions_WhenShowButtonIsFalse_ShouldSetupAndShowTransactionsWithAHiddenButton() {
        sut.present(transactions: [], showButton: false)
        
        XCTAssertEqual(viewControllerSpy.resetViewsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.setupTransactionsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.setupButtonCallsCount, 0)
    }
    
    func testPresentTransactions_WhenShowButtonIsTrue_ShouldSetupAndShowTransactionsWithAButton() {
        sut.present(transactions: [], showButton: true)
        
        XCTAssertEqual(viewControllerSpy.resetViewsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.setupTransactionsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.setupButtonCallsCount, 1)
    }
    
    func testPresentDetailsConfigurator_WhenTransactionDetailsIsPresented_ShouldCallDisplayDetails() {
        let transaction = createTransaction(with: .approved)
        sut.presentDetails(of: transaction, at: IndexPath(row: 0, section: 0))
        XCTAssertEqual(viewControllerSpy.displayDetailsCallsCount, 1)
    }
    
    func testPresentDetailsConfigurator_WhenTransactionDetailsIsPresented_ConfiguratorShouldBeInitializedCorrectly() {
        let indexPath = IndexPath(row: 0, section: 0)
        let transaction = createTransaction(with: .canceled)
        
        sut.presentDetails(of: transaction, at: indexPath)
        
        XCTAssertEqual(viewControllerSpy.displayedDetailsConfigurator?.indexPath, indexPath)
        XCTAssertEqual(viewControllerSpy.displayedDetailsConfigurator?.photoUrl, nil)
        XCTAssertEqual(viewControllerSpy.displayedDetailsConfigurator?.name, "Everton")
        XCTAssertEqual(viewControllerSpy.displayedDetailsConfigurator?.date, "09:30 12/08/20")
        XCTAssertEqual(viewControllerSpy.displayedDetailsConfigurator?.value, "R$ 0,53")
        XCTAssertEqual(viewControllerSpy.displayedDetailsConfigurator?.isRefundAvailable, false)
    }
    
    func testPresentDetails_WhenCanceledTransactionDetailsIsPresented_IsRefundAvailableShouldBeFalseAndStatusColorSouldBeCritical600() throws {
        let expectedStatusColor = Colors.critical600.color.cgColor
        let expectedStatusText = "Cancelada"
        
        let transaction = createTransaction(with: .canceled)
        
        sut.presentDetails(of: transaction, at: IndexPath(row: 0, section: 0))
        
        let status = try XCTUnwrap(viewControllerSpy.displayedDetailsConfigurator?.status)
        
        let statusColor = try XCTUnwrap(
            status.attribute(
                NSAttributedString.Key.foregroundColor,
                at: 0,
                effectiveRange: nil
            ) as? UIColor
        )
        
        XCTAssertEqual(statusColor.cgColor, expectedStatusColor)
        XCTAssertEqual(status.string, expectedStatusText)
        
        XCTAssertEqual(viewControllerSpy.displayedDetailsConfigurator?.isRefundAvailable, false)
    }
    
    func testPresentDetails_WhenApprovedTransactionDetailsIsPresented_IsRefundAvailableShouldBeTrueAndStatusColorSouldBeBranding700() throws {
        let expectedStatusColor = Colors.branding700.color.cgColor
        let expectedStatusText = "Aprovada"
        
        let transaction = createTransaction(with: .approved)
        
        sut.presentDetails(of: transaction, at: IndexPath(row: 0, section: 0))
        
        let status = try XCTUnwrap(viewControllerSpy.displayedDetailsConfigurator?.status)
        
        let statusColor = try XCTUnwrap(
            status.attribute(
                NSAttributedString.Key.foregroundColor,
                at: 0,
                effectiveRange: nil
            ) as? UIColor
        )
        
        XCTAssertEqual(statusColor.cgColor, expectedStatusColor)
        XCTAssertEqual(status.string, expectedStatusText)
        
        XCTAssertEqual(viewControllerSpy.displayedDetailsConfigurator?.isRefundAvailable, true)
    }
    
    func testPresentDetails_WhenCompletedTransactionDetailsIsPresented_IsRefundAvailableShouldBeTrueAndStatusColorSouldBeBranding700() throws {
        let expectedStatusColor = Colors.branding700.color.cgColor
        let expectedStatusText = "Completada"
        
        let transaction = createTransaction(with: .completed)
        
        sut.presentDetails(of: transaction, at: IndexPath(row: 0, section: 0))
        
        let status = try XCTUnwrap(viewControllerSpy.displayedDetailsConfigurator?.status)
        
        let statusColor = try XCTUnwrap(
            status.attribute(
                NSAttributedString.Key.foregroundColor,
                at: 0,
                effectiveRange: nil
            ) as? UIColor
        )
        
        XCTAssertEqual(statusColor.cgColor, expectedStatusColor)
        XCTAssertEqual(status.string, expectedStatusText)
        
        XCTAssertEqual(viewControllerSpy.displayedDetailsConfigurator?.isRefundAvailable, true)
    }
    
    func testPresentDetails_WhenReturnedTransactionDetailsIsPresented_IsRefundAvailableShouldBeTrueAndStatusColorSouldBeBranding700() throws {
        let expectedStatusColor = Colors.branding700.color.cgColor
        let expectedStatusText = "Devolvida"
        
        let transaction = createTransaction(with: .returned)
        
        sut.presentDetails(of: transaction, at: IndexPath(row: 0, section: 0))
        
        let status = try XCTUnwrap(viewControllerSpy.displayedDetailsConfigurator?.status)
        
        let statusColor = try XCTUnwrap(
            status.attribute(
                NSAttributedString.Key.foregroundColor,
                at: 0,
                effectiveRange: nil
            ) as? UIColor
        )
        
        XCTAssertEqual(statusColor.cgColor, expectedStatusColor)
        XCTAssertEqual(status.string, expectedStatusText)
        
        XCTAssertEqual(viewControllerSpy.displayedDetailsConfigurator?.isRefundAvailable, true)
    }
    
    func testPresentError_WhenPresentErrorIsCalled_ShouldResetViewAndShowError() {
        sut.presentErrorState()
        
        XCTAssertEqual(viewControllerSpy.resetViewsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayErrorViewCallsCount, 1)
    }
    
    func testPresentErrorAlert_WhenPresentErrorAlertIsCalled_ShouldShowAlertError() {
        sut.presentErrorAlert()
        
        XCTAssertEqual(viewControllerSpy.displayErrorAlertCallsCount, 1)
    }
    
    func testShowLoading_WhenShowLoadingIsCalled_ShouldResetViewsAndSetupLoadingView() {
        sut.startLoading()
        
        XCTAssertEqual(viewControllerSpy.resetViewsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.startLoadingCallsCount, 1)
    }
    
    func testShowLoading_WhenHideLoadingIsCalled_ShouldHideLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(viewControllerSpy.stopLoadingCallsCount, 1)
    }
    
    func testStartLoading_WhenStartLoadingIsCalled_ShouldStartLoading() {
        sut.startLoading(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(viewControllerSpy.startLoadingCallsCount, 1)
    }
    
    func testPresentEmptyState_WhenPresentEmptyStateIsCalled_ShouldResetViewsAndPresentEmptyView() {
        sut.presentEmptyState()
        
        XCTAssertEqual(viewControllerSpy.resetViewsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayEmptyViewCallsCount, 1)
    }
    
    private func createTransaction(with status: TransactionStatus) -> LinkTransaction {
        LinkTransaction(
            id: 6,
            code: "RQEQTNH36R",
            status: status,
            value: 0.53,
            valueToSettle: nil,
            consumer: LinkConsumer(
                name: "Everton",
                email: nil,
                cpf: nil,
                cnpj: nil
            ),
            createdDate: "2020-08-12 09:30:40",
            updatedDate: "2020-08-12 09:30:40"
        )
    }
}
