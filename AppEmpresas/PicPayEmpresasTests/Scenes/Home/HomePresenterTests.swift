import AssetsKit
import XCTest
@testable import PicPayEmpresas

private final class HomeCoordinatorSpy: HomeCoordinating {
    var viewController: UIViewController?
    private(set) var action: HomeAction?

    func perform(action: HomeAction) {
        self.action = action
    }
}

private class HomeDisplayingSpy: HomeDisplaying {

    //MARK: - startLoading
    private(set) var startLoadingCallsCount = 0

    func startLoading() {
        startLoadingCallsCount += 1
    }

    //MARK: - stopLoading
    private(set) var stopLoadingCallsCount = 0

    func stopLoading() {
        stopLoadingCallsCount += 1
    }

    //MARK: - endRefreshing
    private(set) var endRefreshingCallsCount = 0

    func endRefreshing() {
        endRefreshingCallsCount += 1
    }

    //MARK: - displayAccountErrorComponent
    private(set) var displayAccountErrorComponentTextButtonTitleCallsCount = 0
    private(set) var displayAccountErrorComponentTextButtonTitleReceivedInvocations: [(text: String, buttonTitle: String?)] = []

    func displayAccountErrorComponent(text: String, buttonTitle: String?) {
        displayAccountErrorComponentTextButtonTitleCallsCount += 1
        displayAccountErrorComponentTextButtonTitleReceivedInvocations.append((text: text, buttonTitle: buttonTitle))
    }

    //MARK: - displayWalletComponent
    private(set) var displayWalletComponentBankErrorCallsCount = 0
    private(set) var displayWalletComponentBankErrorReceivedInvocations: [WithdrawalOptionsBankError?] = []

    func displayWalletComponent(bankError: WithdrawalOptionsBankError?) {
        displayWalletComponentBankErrorCallsCount += 1
        displayWalletComponentBankErrorReceivedInvocations.append(bankError)
    }

    //MARK: - displayCarouselComponent
    private(set) var displayCarouselComponentHasBiometrySellerUserStatusCallsCount = 0
    private(set) var displayCarouselComponentHasBiometrySellerUserStatusReceivedInvocations: [(hasBiometry: Bool, seller: Seller, userStatus: UserStatus?)] = []

    func displayCarouselComponent(hasBiometry: Bool, seller: Seller, userStatus: UserStatus?) {
        displayCarouselComponentHasBiometrySellerUserStatusCallsCount += 1
        displayCarouselComponentHasBiometrySellerUserStatusReceivedInvocations.append((hasBiometry: hasBiometry, seller: seller, userStatus: userStatus))
    }

    //MARK: - displayMaterialHeader
    private(set) var displayMaterialHeaderUserStatusCallsCount = 0
    private(set) var displayMaterialHeaderUserStatusReceivedInvocations: [UserStatus] = []

    func displayMaterialHeader(userStatus: UserStatus) {
        displayMaterialHeaderUserStatusCallsCount += 1
        displayMaterialHeaderUserStatusReceivedInvocations.append(userStatus)
    }

    //MARK: - displayTransactionComponent
    private(set) var displayTransactionComponentSellerCallsCount = 0
    private(set) var displayTransactionComponentSellerReceivedInvocations: [Seller] = []

    func displayTransactionComponent(seller: Seller) {
        displayTransactionComponentSellerCallsCount += 1
        displayTransactionComponentSellerReceivedInvocations.append(seller)
    }
    
    private(set) var displayNewTransactionComponentSellerCallsCount = 0
    private(set) var displayNewTransactionComponentSellerReceivedInvocations: [Seller] = []
    
    func displayNewTransactionComponent(seller: Seller) {
        displayNewTransactionComponentSellerCallsCount += 1
        displayNewTransactionComponentSellerReceivedInvocations.append(seller)
    }

    //MARK: - displayAdvertising
    private(set) var displayAdvertisingWithCallsCount = 0
    private(set) var displayAdvertisingWithReceivedInvocations: [AlertModalInfo] = []

    func displayAdvertising(with modalInfo: AlertModalInfo) {
        displayAdvertisingWithCallsCount += 1
        displayAdvertisingWithReceivedInvocations.append(modalInfo)
    }
}

final class HomePresenterTests: XCTestCase {
    private typealias TransactionLocalizable = Strings.Transaction
    private let coordinatorSpy = HomeCoordinatorSpy()
    private let displaySpy = HomeDisplayingSpy()

    lazy var sut: HomePresenter = {
        let presenter = HomePresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testPresentAccountError_ShouldDisplayAccountError() throws {
        let bankAccountError = try XCTUnwrap(BankAccountValidationMock().getBankAccountValidationInvalid())
        
        sut.presentAccountErrorComponent(bankAccountError: bankAccountError)
        
        XCTAssertEqual(displaySpy.displayAccountErrorComponentTextButtonTitleCallsCount, 1)
        
        let invocation = displaySpy.displayAccountErrorComponentTextButtonTitleReceivedInvocations.first
        XCTAssertEqual(invocation?.buttonTitle, bankAccountError.validationIssue?.action?.message)
        XCTAssertEqual(invocation?.text, bankAccountError.validationIssue?.message)
    }
    
    func testPresentWallet_ShouldDisplayWallet() throws {
        let bankAccountError = try XCTUnwrap(BankAccountValidationMock().getBankAccountValidationInvalid())
        
        let seller = Seller()
        let companyInfo = CompanyInfo(valid: true, individual: true, ableToDigitalAccount: true)
        
        sut.presentWalletComponent(bankAccountError: bankAccountError, seller: seller, companyInfo: companyInfo)
        
        XCTAssertEqual(displaySpy.displayWalletComponentBankErrorCallsCount, 1)
        
        let bankError = WithdrawalOptionsBankError(bankError: bankAccountError, seller: seller, companyType: .individual)
        
        let invocation = displaySpy.displayWalletComponentBankErrorReceivedInvocations.first
        XCTAssertEqual(invocation, bankError)
    }
    
    func testPresentCarousel_ShouldDisplayCarousel() {
        let seller = Seller()
        let userStatus = UserStatus(
            boardRequestRefused: true,
            boardStep: "",
            boardRequested: true,
            config: UserStatusConfig(blocked: nil),
            sellerID: 0
        )
        
        sut.presentCarouselComponent(seller: seller, userStatus: userStatus)
        
        XCTAssertEqual(displaySpy.displayCarouselComponentHasBiometrySellerUserStatusCallsCount, 1)
        
        let invocation = displaySpy.displayCarouselComponentHasBiometrySellerUserStatusReceivedInvocations.first
        XCTAssertEqual(invocation?.hasBiometry, seller.biometry)
        XCTAssertEqual(invocation?.seller, seller)
        XCTAssertEqual(invocation?.userStatus, userStatus)
    }
    
    func testPresentMaterialHeader_ShouldDisplayMaterialHeader() {
        let userStatus = UserStatus(
            boardRequestRefused: true,
            boardStep: "",
            boardRequested: true,
            config: UserStatusConfig(blocked: nil),
            sellerID: 0)
        
        sut.presentMaterialHeader(userStatus: userStatus)
        
        XCTAssertEqual(displaySpy.displayMaterialHeaderUserStatusCallsCount, 1)
        
        let invocation = displaySpy.displayMaterialHeaderUserStatusReceivedInvocations.first
        XCTAssertEqual(invocation, userStatus)
    }
    
    func testPresentTransaction_ShouldDisplayTransactionComponent() {
        sut.presentTransactionComponent(seller: Seller())
        XCTAssertEqual(1, displaySpy.displayTransactionComponentSellerCallsCount)
        XCTAssertTrue(displaySpy.displayTransactionComponentSellerReceivedInvocations.isNotEmpty)
    }
    
    func testFinishLoading_ShouldCallFinishLoading() {
        sut.endRefreshing()
        
        XCTAssertEqual(1, displaySpy.endRefreshingCallsCount)
    }
    
    func testPresentAdvertising_ShouldDisplayAdvertising() {
        sut.presentAdvertising()
        
        let size = CGSize(width: 150, height: 53)
        let modalInfoImage = AlertModalInfoImage(image: Resources.Logos.pixLogo.image, size: size)
        let info = AlertModalInfo(
            imageInfo: modalInfoImage,
            title: TransactionLocalizable.pixCarouselAlertTitle,
            subtitle: TransactionLocalizable.pixCarouselAlertDescription,
            buttonTitle: TransactionLocalizable.pixCarouselAlertButton,
            secondaryButtonTitle: TransactionLocalizable.pixCarouselAlertSecondaryButton
        )
        
        XCTAssertEqual(displaySpy.displayAdvertisingWithCallsCount, 1)
        XCTAssertEqual(displaySpy.displayAdvertisingWithReceivedInvocations.last, info)
    }
}
