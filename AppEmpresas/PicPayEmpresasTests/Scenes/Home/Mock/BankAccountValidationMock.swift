import Core
import XCTest
@testable import PicPayEmpresas

final class BankAccountValidationMock {
    func getBankAccountValidationInvalid() -> BankAccountValidation? {
        LocalMock<BankAccountValidation>().localMock("BankAccountValidationInvalid")
    }
}
