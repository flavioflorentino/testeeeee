import Core
import Foundation
import LegacyPJ
import PIX
import XCTest
@testable import PicPayEmpresas

private class HomePresentingSpy: HomePresenting {
    var viewController: HomeDisplaying?

    //MARK: - startLoading
    private(set) var startLoadingCallsCount = 0

    func startLoading() {
        startLoadingCallsCount += 1
    }

    //MARK: - stopLoading
    private(set) var stopLoadingCallsCount = 0

    func stopLoading() {
        stopLoadingCallsCount += 1
    }

    //MARK: - endRefreshing
    private(set) var endRefreshingCallsCount = 0
    var didCallEndRefreshing: (() -> Void)?

    func endRefreshing() {
        endRefreshingCallsCount += 1
        didCallEndRefreshing?()
    }

    //MARK: - presentAccountErrorComponent
    private(set) var presentAccountErrorComponentBankAccountErrorCallsCount = 0
    private(set) var presentAccountErrorComponentBankAccountErrorReceivedInvocations: [BankAccountValidation] = []

    func presentAccountErrorComponent(bankAccountError: BankAccountValidation) {
        presentAccountErrorComponentBankAccountErrorCallsCount += 1
        presentAccountErrorComponentBankAccountErrorReceivedInvocations.append(bankAccountError)
    }

    //MARK: - presentWalletComponent
    private(set) var presentWalletComponentBankAccountErrorSellerCompanyInfoCallsCount = 0
    private(set) var presentWalletComponentBankAccountErrorSellerCompanyInfoReceivedInvocations: [(bankAccountError: BankAccountValidation?, seller: Seller, companyInfo: CompanyInfo?)] = []

    func presentWalletComponent(bankAccountError: BankAccountValidation?, seller: Seller, companyInfo: CompanyInfo?) {
        presentWalletComponentBankAccountErrorSellerCompanyInfoCallsCount += 1
        presentWalletComponentBankAccountErrorSellerCompanyInfoReceivedInvocations.append((bankAccountError: bankAccountError, seller: seller, companyInfo: companyInfo))
    }

    //MARK: - presentCarouselComponent
    private(set) var presentCarouselComponentSellerUserStatusCallsCount = 0
    private(set) var presentCarouselComponentSellerUserStatusReceivedInvocations: [(seller: Seller, userStatus: UserStatus?)] = []

    func presentCarouselComponent(seller: Seller, userStatus: UserStatus?) {
        presentCarouselComponentSellerUserStatusCallsCount += 1
        presentCarouselComponentSellerUserStatusReceivedInvocations.append((seller: seller, userStatus: userStatus))
    }

    //MARK: - presentMaterialHeader
    private(set) var presentMaterialHeaderUserStatusCallsCount = 0
    private(set) var presentMaterialHeaderUserStatusReceivedInvocations: [UserStatus] = []

    func presentMaterialHeader(userStatus: UserStatus) {
        presentMaterialHeaderUserStatusCallsCount += 1
        presentMaterialHeaderUserStatusReceivedInvocations.append(userStatus)
    }

    //MARK: - presentTransactionComponent
    private(set) var presentTransactionComponentSellerCallsCount = 0
    private(set) var presentTransactionComponentSellerReceivedInvocations: [Seller] = []

    func presentTransactionComponent(seller: Seller) {
        presentTransactionComponentSellerCallsCount += 1
        presentTransactionComponentSellerReceivedInvocations.append(seller)
    }
    
    private(set) var presentNewTransactionComponentSellerCallsCount = 0
    private(set) var presentNewTransactionComponentSellerReceivedInvocations: [Seller] = []

    func presentNewTransactionComponent(seller: Seller) {
        presentNewTransactionComponentSellerCallsCount += 1
        presentNewTransactionComponentSellerReceivedInvocations.append(seller)
    }

    //MARK: - presentAdvertising
    private(set) var presentAdvertisingCallsCount = 0

    func presentAdvertising() {
        presentAdvertisingCallsCount += 1
    }

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [HomeAction] = []

    func didNextStep(action: HomeAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }
}

private final class HomeServiceMock: HomeServicing {
    var bankAccountWarningsEnabled = true
    var welcomePresented = false
    var pixPresentingEnabled = false
    var mustPresentHub = false
    var allowApplyMaterial = false
    var receiveAccountEnabled = false
    
    var checkStatusCompletion: Result<UserStatus?, ApiError>?
    func checkStatus(completion: @escaping (Result<UserStatus?, ApiError>) -> Void) {
        guard let checkStatusCompletion = checkStatusCompletion else {
            XCTFail("Missing checkStatusCompletion")
            return
        }
        completion(checkStatusCompletion)
    }
    
    //MARK: - checkBankAccount
    var checkBankAccountCompletion: Result<BankAccountValidation?, ApiError>?
    func checkBankAccount(completion: @escaping(Result<BankAccountValidation?, ApiError>) -> Void) {
        guard let checkBankAccountCompletion = checkBankAccountCompletion else {
            XCTFail("Missing checkBankAccountCompletion")
            return
        }
        completion(checkBankAccountCompletion)
    }
}

final class HomeInteractorTests: XCTestCase {
    private let bankAccountValidationMock = BankAccountValidationMock()
    private var serviceMock = HomeServiceMock()
    private var presenterSpy = HomePresentingSpy()
    private let timeout: TimeInterval = 2
    
    private lazy var apiSeller = ApiSellerMock()
    private lazy var apiSignUp = ApiSignUpMock()
    private lazy var kvStore = KVStoreMock()
    private lazy var analyticsMock = AnalyticsMock()
    private lazy var authManagerMock = AuthManagerMock()
    
    private lazy var dependencies: DependencyContainerMock = {
        return DependencyContainerMock(apiSeller, apiSignUp, kvStore, analyticsMock, authManagerMock)
    }()

    private lazy var sut: HomeInteracting = {
        let interactor = HomeInteractor(service: serviceMock, presenter: presenterSpy, dependencies: dependencies)
        return interactor
    }()
    
    private lazy var userStatusConfig = UserStatusConfig(blocked: nil)
    private lazy var defaultStatus = UserStatus(
        boardRequestRefused: true,
        boardStep: "",
        boardRequested: true,
        config: userStatusConfig,
        sellerID: 0
    )
    
    func testLoadHome_WhenSellerIsNil_ShouldNotShowHomeComponents() throws {
        apiSeller.dataCompletion = .failure(LegacyPJError(message: ""))
        serviceMock.checkStatusCompletion = .success(defaultStatus)
        try setupDefaultBankAccountValidation()
        
        loadHomeAndWait()
        
        XCTAssertEqual(presenterSpy.presentAccountErrorComponentBankAccountErrorCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentWalletComponentBankAccountErrorSellerCompanyInfoCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentCarouselComponentSellerUserStatusCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentMaterialHeaderUserStatusCallsCount, 0)
    }
    
    func testLoadHome_WhenHasBankAccountValidation_ShouldShowBankAccountValidationComponent() throws {
        apiSeller.dataCompletion = .success(Seller())
        let bankAccountValidation = try setupDefaultBankAccountValidation()
        serviceMock.checkStatusCompletion = .success(defaultStatus)
        
        loadHomeAndWait()
        
        XCTAssertEqual(presenterSpy.presentAccountErrorComponentBankAccountErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentAccountErrorComponentBankAccountErrorReceivedInvocations.count, 1)
        
        let invocations = presenterSpy.presentAccountErrorComponentBankAccountErrorReceivedInvocations
        XCTAssertEqual(invocations.first, bankAccountValidation)
    }
    
    func testLoadHome_WhenHasNoDigitalAccountAndReceiveAccountEnabled_ShouldShowWalletComponent() throws {
        let bankAccountValidation = try setupDefaultBankAccountValidation()
        serviceMock.checkBankAccountCompletion = .success(bankAccountValidation)
        
        var seller = Seller()
        seller.digitalAccount = false
        seller.cnpj = ""
        apiSeller.dataCompletion = .success(seller)
        
        let companyInfo = setupDefaultCompanyInfo()
        serviceMock.checkStatusCompletion = .success(defaultStatus)
        
        serviceMock.receiveAccountEnabled = true
        
        loadHomeAndWait()
        
        XCTAssertEqual(presenterSpy.presentWalletComponentBankAccountErrorSellerCompanyInfoCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentWalletComponentBankAccountErrorSellerCompanyInfoReceivedInvocations.count, 1)
        
        let invocation = presenterSpy.presentWalletComponentBankAccountErrorSellerCompanyInfoReceivedInvocations.first
        
        XCTAssertEqual(invocation?.bankAccountError, bankAccountValidation)
        XCTAssertEqual(invocation?.seller, seller)
        XCTAssertEqual(invocation?.companyInfo, companyInfo)
    }
    
    func testLoadHome_WhenHasNoDigitalAccountAndReceiveAccountDisabled_ShouldNotShowWalletComponent() throws {
        let bankAccountValidation = try setupDefaultBankAccountValidation()
        serviceMock.checkBankAccountCompletion = .success(bankAccountValidation)
        
        var seller = Seller()
        seller.digitalAccount = false
        seller.cnpj = ""
        apiSeller.dataCompletion = .success(seller)
        
        serviceMock.checkStatusCompletion = .success(defaultStatus)
        
        serviceMock.receiveAccountEnabled = false
        
        loadHomeAndWait()
        
        XCTAssertEqual(presenterSpy.presentWalletComponentBankAccountErrorSellerCompanyInfoCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentWalletComponentBankAccountErrorSellerCompanyInfoReceivedInvocations.count, 0)
    }
    
    func testLoadHome_WhenHasDigitalAccountAndReceiveAccountDisabled_ShouldShowWalletComponent() throws {
        let bankAccountValidation = try setupDefaultBankAccountValidation()
        serviceMock.checkBankAccountCompletion = .success(bankAccountValidation)
        
        var seller = Seller()
        seller.digitalAccount = true
        seller.cnpj = ""
        apiSeller.dataCompletion = .success(seller)
        
        let companyInfo = setupDefaultCompanyInfo()
        serviceMock.checkStatusCompletion = .success(defaultStatus)
        
        loadHomeAndWait()
        
        XCTAssertEqual(presenterSpy.presentWalletComponentBankAccountErrorSellerCompanyInfoCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentWalletComponentBankAccountErrorSellerCompanyInfoReceivedInvocations.count, 1)
        
        let invocation = presenterSpy.presentWalletComponentBankAccountErrorSellerCompanyInfoReceivedInvocations.first
        
        XCTAssertEqual(invocation?.bankAccountError, bankAccountValidation)
        XCTAssertEqual(invocation?.seller, seller)
        XCTAssertEqual(invocation?.companyInfo, companyInfo)
    }
    
    func testLoadHome_WhenHasDigitalAccount_ShouldShowCarouselComponent() {
        var seller = Seller()
        seller.digitalAccount = true
        apiSeller.dataCompletion = .success(seller)
        
        serviceMock.checkStatusCompletion = .success(defaultStatus)
        serviceMock.checkBankAccountCompletion = .failure(ApiError.cancelled)
        
        loadHomeAndWait()
        
        XCTAssertEqual(presenterSpy.presentCarouselComponentSellerUserStatusCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentCarouselComponentSellerUserStatusReceivedInvocations.count, 1)
        
        let invocation = presenterSpy.presentCarouselComponentSellerUserStatusReceivedInvocations.first
        
        XCTAssertEqual(invocation?.userStatus, defaultStatus)
        XCTAssertEqual(invocation?.seller, seller)
    }
    
    func testLoadHome_WhenNoDigitalAccountAndBoardRequestAccepted_ShouldShowMaterialComponent() {
        var seller = Seller()
        seller.digitalAccount = false
        apiSeller.dataCompletion = .success(seller)
        
        let boardRequestRefused = false
        let userStatus = UserStatus(
            boardRequestRefused: boardRequestRefused,
            boardStep: "",
            boardRequested: true,
            config: userStatusConfig,
            sellerID: 0
        )
        
        serviceMock.checkStatusCompletion = .success(userStatus)
        serviceMock.checkBankAccountCompletion = .failure(ApiError.cancelled)
        
        serviceMock.allowApplyMaterial = true
        
        loadHomeAndWait()
        
        XCTAssertEqual(self.presenterSpy.presentMaterialHeaderUserStatusCallsCount, 1)
        XCTAssertEqual(self.presenterSpy.presentMaterialHeaderUserStatusReceivedInvocations.count, 1)
        
        let invocation = self.presenterSpy.presentMaterialHeaderUserStatusReceivedInvocations.first
        XCTAssertEqual(invocation, userStatus)
    }
    
    func testLoadHome_WhenHasCnpj_ShouldSaveCompanyInfo() throws {
        kvStore.set(value: false, with: BizKvKey.individualCompany)
        var seller = Seller()
        seller.cnpj = ""
        apiSeller.dataCompletion = .success(seller)
        
        serviceMock.checkStatusCompletion = .success(defaultStatus)
        serviceMock.checkBankAccountCompletion = .failure(ApiError.cancelled)
        
        setupDefaultCompanyInfo()
        
        loadHomeAndWait()
        
        let value = kvStore.boolFor(BizKvKey.individualCompany)
        XCTAssertTrue(value)
    }
    
    func testLoadHome_ShouldShowTransactionsComponent() {
        var seller = Seller()
        seller.digitalAccount = false
        apiSeller.dataCompletion = .success(seller)
        
        serviceMock.checkStatusCompletion = .success(defaultStatus)
        serviceMock.checkBankAccountCompletion = .failure(ApiError.cancelled)
        
        loadHomeAndWait()
        
        XCTAssertEqual(presenterSpy.presentTransactionComponentSellerCallsCount, 1)
        XCTAssertTrue(presenterSpy.presentTransactionComponentSellerReceivedInvocations.isNotEmpty)
    }
    
    func testLoadHome_ShouldCallFinishLoading() {
        var seller = Seller()
        seller.digitalAccount = false
        apiSeller.dataCompletion = .success(seller)
        
        serviceMock.checkStatusCompletion = .success(defaultStatus)
        serviceMock.checkBankAccountCompletion = .failure(ApiError.cancelled)
        
        loadHomeAndWait()
        
        XCTAssertEqual(presenterSpy.endRefreshingCallsCount, 1)
    }
    
    func testSnackBarAction_ShouldCallDidNextStepEditAccount() throws {
        var seller = Seller()
        seller.cnpj = ""
        apiSeller.dataCompletion = .success(seller)
        
        setupDefaultCompanyInfo()
        try setupDefaultBankAccountValidation()
        serviceMock.checkStatusCompletion = .success(defaultStatus)
        
        loadHomeAndWait()
        
        sut.snackBarAction()
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        
        let invocations = presenterSpy.didNextStepActionReceivedInvocations
        XCTAssertEqual(invocations.first, .editBankAccount(.individual, seller))
    }
    
    func testLoadHome_ShouldPresentAdvertising() throws {
        var seller = Seller()
        seller.biometry = true
        seller.digitalAccount = true
        apiSeller.dataCompletion = .success(seller)
        
        try setupDefaultBankAccountValidation()
        serviceMock.checkStatusCompletion = .success(defaultStatus)
        
        serviceMock.pixPresentingEnabled = true
        kvStore.set(value: false, with: BizKvKey.didPresentAdvertisingPix)
        
        loadHomeAndWait()
        
        XCTAssertEqual(presenterSpy.presentAdvertisingCallsCount, 1)
        XCTAssertTrue(kvStore.boolFor(BizKvKey.didPresentAdvertisingPix))
    }
    
    func testLoadHome_WhenBiometryIsFalse_ShouldNotPresentAdvertising() throws {
        try test_ShouldNotDisplayAdvertising(biometry: false)
    }
    
    func testLoadHome_WhenDigitalAccountIsFalse_ShouldNotPresentAdvertising() throws {
        try test_ShouldNotDisplayAdvertising(digitalAccount: false)
    }
    
    func testLoadHome_WhenReleasePIXPresentingIsFalse_ShouldNotPresentAdvertising() throws {
        try test_ShouldNotDisplayAdvertising(releasePIXPresenting: false)
    }
    
    func testLoadHome_WhenDidPresentAdvertisingPixIsTrue_ShouldNotPresentAdvertising() throws {
        try test_ShouldNotDisplayAdvertising(didPresentAdvertisingPix: true)
    }
    
    func testTrackPixNotNow_ShouldLogModalNotNowPixEvent() throws {
        let id = 300
        let companyName = "Potato"
        
        apiSeller.dataCompletion = .success(Seller())
        try setupDefaultBankAccountValidation()
        serviceMock.checkStatusCompletion = .success(defaultStatus)
        
        loadHomeAndWait()
        
        sut.trackPixNotNow()
        
        try testPixLog(event: TransactionCarouselAnalytics.modalNotNowPixEvent(sellerID: id, companyName: companyName))
    }
    
    func testTrackPixNotNow_ShouldLogTrackShowPixEvent() throws {
        let id = 300
        let companyName = "Potato"
        
        apiSeller.dataCompletion = .success(Seller())
        try setupDefaultBankAccountValidation()
        serviceMock.checkStatusCompletion = .success(defaultStatus)
        
        loadHomeAndWait()
        
        sut.trackPixRegister()
        
        try testPixLog(event: TransactionCarouselAnalytics.modalShowPixEvent(sellerID: id, companyName: companyName))
    }
    
    func testPixAction_WhenSellerIsMissing_ShouldNotCallNextStep() throws {
        try testPixAction_WhenMissingInfo_ShouldNotCallNextStep(seller: nil, user: User())
    }
    
    func testPixAction_WhenSellerNameIsMissing_ShouldNotCallNextStep() throws {
        let seller = setupSeller(name: nil)
        try testPixAction_WhenMissingInfo_ShouldNotCallNextStep(seller: seller, user: User())
    }
    
    func testPixAction_WhenSellerCnpjIsMissing_ShouldNotCallNextStep() throws {
        let seller = setupSeller(cnpj: nil)
        try testPixAction_WhenMissingInfo_ShouldNotCallNextStep(seller: seller, user: User())
    }
    
    func testPixAction_WhenSellerEmailIsMissing_ShouldNotCallNextStep() throws {
        let seller = setupSeller(email: nil)
        try testPixAction_WhenMissingInfo_ShouldNotCallNextStep(seller: seller, user: User())
    }
    
    func testPixAction_WhenUserIsMissing_ShouldNotCallNextStep() throws {
        let seller = setupSeller()
        try testPixAction_WhenMissingInfo_ShouldNotCallNextStep(seller: seller, user: nil)
    }
    
    func testPixAction_WhenUserPhoneIsMissing_ShouldNotCallNextStep() throws {
        let seller = setupSeller()
        let user = User()
        try testPixAction_WhenMissingInfo_ShouldNotCallNextStep(seller: seller, user: user)
    }
    
    func testPixAction_WhenMustPresentHubAndWelcomePresented_ShouldCallNextStepWithPixHub() throws {
        try testPixAction(welcomePresented: true, mustPresentHub: true, flow: PIXBizFlow.hub)
    }
    
    func testPixAction_WhenMustPresentHubAndWelcomeNotPresented_ShouldCallNextStepWithPixWelcome() throws {
        try testPixAction(mustPresentHub: true, flow: .welcomePage)
    }
    
    func testPixAction_WhenMustNotPresentHubAndWelcomePresented_ShouldCallNextStepWithOptinKeyManager() throws {
        try testPixAction(welcomePresented: true, flow: .optin(action: .keyManager(input: .none)))
    }
    
    func testPixAction_WhenMustNotPresentHubAndWelcomeNotPresented_ShouldCallNextStepWithOptinOnboard() throws {
        try testPixAction(flow: .optin(action: .onboarding))
    }
}

private extension HomeInteractorTests {
    func loadHomeAndWait() {
        let expectation = XCTestExpectation()
        presenterSpy.didCallEndRefreshing = {
            expectation.fulfill()
        }
        
        sut.loadHome()
        
        wait(for: [expectation], timeout: timeout)
    }
    
    @discardableResult
    func setupDefaultBankAccountValidation() throws -> BankAccountValidation {
        let bankAccountValidation = try XCTUnwrap(bankAccountValidationMock.getBankAccountValidationInvalid())
        serviceMock.checkBankAccountCompletion = .success(bankAccountValidation)
        return bankAccountValidation
    }
    
    @discardableResult
    func setupDefaultCompanyInfo() -> CompanyInfo {
        let companyInfo = CompanyInfo(valid: true, individual: true, ableToDigitalAccount: true)
        apiSignUp.verifySellerCompletion = .success(companyInfo)
        return companyInfo
    }
    
    func test_ShouldNotDisplayAdvertising(
        biometry: Bool = true,
        digitalAccount: Bool = true,
        releasePIXPresenting: Bool = true,
        didPresentAdvertisingPix: Bool = false
    ) throws {
        var seller = Seller()
        seller.biometry = biometry
        seller.digitalAccount = digitalAccount
        apiSeller.dataCompletion = .success(seller)
        
        try setupDefaultBankAccountValidation()
        serviceMock.checkStatusCompletion = .success(defaultStatus)
        
        serviceMock.pixPresentingEnabled = releasePIXPresenting
        kvStore.set(value: didPresentAdvertisingPix, with: BizKvKey.didPresentAdvertisingPix)
        
        loadHomeAndWait()
        
        XCTAssertEqual(presenterSpy.presentAdvertisingCallsCount, 0)
        XCTAssertEqual(kvStore.boolFor(BizKvKey.didPresentAdvertisingPix), didPresentAdvertisingPix)
    }
    
    func testPixLog(event: TransactionCarouselAnalytics) throws {
        let logEvent = try XCTUnwrap(analyticsMock.logReceivedInvocations.last)
        XCTAssertEqual(logEvent.event().name, event.event().name)
    }
    
    func testPixAction_WhenMissingInfo_ShouldNotCallNextStep(seller: Seller? = nil, user: User? = nil) throws {
        if let seller = seller {
            apiSeller.dataCompletion = .success(seller)
        } else {
            apiSeller.dataCompletion = .failure(.requestError)
        }
        
        authManagerMock.user = user
        
        try setupDefaultBankAccountValidation()
        serviceMock.checkStatusCompletion = .success(defaultStatus)
        
        loadHomeAndWait()
        
        sut.pixAction()
        XCTAssertEqual(0, presenterSpy.didNextStepActionCallsCount)
    }
    
    func setupSeller(name: String? = "Hey", cnpj: String? = "1234", email: String? = "hey@hey.com") -> Seller {
        var seller = Seller()
        seller.razaoSocial = name
        seller.cnpj = cnpj
        seller.email = email
        return seller
    }
    
    func testPixAction(
        welcomePresented: Bool = false,
        mustPresentHub: Bool = false,
        flow: PIXBizFlow
    ) throws {
        let id = 24
        let name = "potato"
        let cnpj = "1234"
        let email = "hey@hey.com"
        
        var seller = setupSeller(name: name, cnpj: cnpj, email: email)
        seller.id = id
        apiSeller.dataCompletion = .success(seller)
        
        let phone = "1234"
        var user = User()
        user.phone = phone
        authManagerMock.user = user
        
        try setupDefaultBankAccountValidation()
        serviceMock.checkStatusCompletion = .success(defaultStatus)
        serviceMock.welcomePresented = welcomePresented
        serviceMock.mustPresentHub = mustPresentHub
        
        let userInfo = KeyManagerBizUserInfo(
            name: name,
            cnpj: cnpj,
            mail: email,
            phone: phone,
            userId: String(seller.id)
        )
        
        loadHomeAndWait()
        
        sut.pixAction()
        
        let action = presenterSpy.didNextStepActionReceivedInvocations.last
        let actionToCompare = HomeAction.pix(userInfo: userInfo, flow: flow)
        XCTAssertEqual(action, actionToCompare)
    }
}
