import XCTest
import Core
import LegacyPJ
@testable import PicPayEmpresas

private final class NewWithdrawPresentingSpy: NewWithdrawPresenting {
    var viewController: NewWithdrawDisplay?

    //MARK: - showBankAccount
    private(set) var showBankAccountCallsCount = 0

    func showBankAccount() {
        showBankAccountCallsCount += 1
    }

    //MARK: - showPicPayAccount
    private(set) var showPicPayAccountCallsCount = 0

    func showPicPayAccount() {
        showPicPayAccountCallsCount += 1
    }

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [NewWithdrawAction] = []

    func didNextStep(action: NewWithdrawAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }

    //MARK: - didReceivePicPayAccount
    private(set) var didReceivePicPayAccountCallsCount = 0
    private(set) var didReceivePicPayAccountReceivedInvocations: [PicpayAccountModel] = []

    func didReceivePicPayAccount(_ account: PicpayAccountModel) {
        didReceivePicPayAccountCallsCount += 1
        didReceivePicPayAccountReceivedInvocations.append(account)
    }

    //MARK: - didReceiveBankAccount
    private(set) var didReceiveBankAccountCallsCount = 0
    private(set) var didReceiveBankAccountReceivedInvocations: [WithdrawAccount] = []

    func didReceiveBankAccount(_ account: WithdrawAccount) {
        didReceiveBankAccountCallsCount += 1
        didReceiveBankAccountReceivedInvocations.append(account)
    }

    //MARK: - availableAmount
    private(set) var availableAmountCallsCount = 0
    private(set) var availableAmountReceivedInvocations: [String] = []

    func availableAmount(_ amount: String) {
        availableAmountCallsCount += 1
        availableAmountReceivedInvocations.append(amount)
    }

    //MARK: - enableButton
    private(set) var enableButtonCallsCount = 0
    private(set) var enableButtonReceivedInvocations: [Bool] = []

    func enableButton(_ state: Bool) {
        enableButtonCallsCount += 1
        enableButtonReceivedInvocations.append(state)
    }

    //MARK: - bankAccountSuccess
    private(set) var bankAccountSuccessCallsCount = 0
    private(set) var bankAccountSuccessReceivedInvocations: [String] = []

    func bankAccountSuccess(_ message: String) {
        bankAccountSuccessCallsCount += 1
        bankAccountSuccessReceivedInvocations.append(message)
    }

    //MARK: - picpayAccountSuccess
    private(set) var picpayAccountSuccessCallsCount = 0
    private(set) var picpayAccountSuccessReceivedInvocations: [String] = []

    func picpayAccountSuccess(_ message: String) {
        picpayAccountSuccessCallsCount += 1
        picpayAccountSuccessReceivedInvocations.append(message)
    }

    //MARK: - fail
    private(set) var failCallsCount = 0
    private(set) var failReceivedInvocations: [String] = []

    func fail(_ message: String) {
        failCallsCount += 1
        failReceivedInvocations.append(message)
    }

    //MARK: - rollBackFail
    private(set) var rollBackFailCallsCount = 0
    private(set) var rollBackFailReceivedInvocations: [LegacyPJError] = []

    func rollBackFail(_ error: LegacyPJError) {
        rollBackFailCallsCount += 1
        rollBackFailReceivedInvocations.append(error)
    }

    //MARK: - didCancelAuth
    private(set) var didCancelAuthCallsCount = 0

    func didCancelAuth() {
        didCancelAuthCallsCount += 1
    }

    //MARK: - pendingWithdraw
    private(set) var pendingWithdrawCallsCount = 0
    private(set) var pendingWithdrawReceivedInvocations: [UnavailableWithdrawItem] = []

    func pendingWithdraw(_ withdrawItem: UnavailableWithdrawItem) {
        pendingWithdrawCallsCount += 1
        pendingWithdrawReceivedInvocations.append(withdrawItem)
    }
    
    //MARK: - pendingWithdraw
    private(set) var startLoadingCallsCount = 0

    func startLoading() {
        startLoadingCallsCount += 1
    }
}

private final class NewWithdrawServiceMock: NewWithdrawServicing {
    var shouldFail = false
    var shouldCancel = false
    var shouldPending = false

    func authenticate(_ completion: @escaping (AuthManager.PerformActionResult) -> Void) {
        guard shouldCancel else {
            completion(.success(String()))
            return
        }

        completion(.canceled)
    }

    func requestBankAccountWithdraw(
        _ pin: String,
        account: WithdrawAccount,
        amount: Double,
        completion: @escaping (Result<BankWithdrawResponse?, ApiError>) -> Void
    ) {
        guard shouldFail else {
            completion(.success(BankWithdrawResponse(message: String())))
            return
        }

        completion(.failure(.badRequest(body: RequestError())))
    }

    func requestPicPayAccountWithdraw(
        _ pin: String,
        amount: Double,
        completion: @escaping (Result<PicPayWithdrawResponse?, ApiError>) -> Void
    ) {
        guard shouldFail else {
            completion(.success(
                PicPayWithdrawResponse(
                    id: String(),
                    status: String(),
                    value: 0.0,
                    balance: 0.0,
                    message: String())
                ))
            return
        }

        completion(.failure(.badRequest(body: RequestError())))
    }

    func checkAvailableWithdraw(_ completion: @escaping (Result<UnavailableWithdrawItem?, ApiError>) -> Void) {
        guard shouldFail else {
            if shouldPending {
                completion(.success(NewWithdrawMocks().pendingWithdraw()))
            } else {
                completion(.success(NewWithdrawMocks().noPendingWithdraw()))
            }
            return
        }

        completion(.failure(.badRequest(body: RequestError())))
    }
}

final class NewWithdrawViewModelTests: XCTestCase {
    private var serviceMock = NewWithdrawServiceMock()
    private var presenterSpy = NewWithdrawPresentingSpy()
    private var withdrawMocks = NewWithdrawMocks()
    private let upperLimit = 12.34
    private let lowerLimit = 0.00
    private var withdrawType: WithdrawType = .bankAccount
    private var bankAccount: WithdrawAccount? = NewWithdrawMocks().getAccounts()?.first
    private var picpayAccount: PicpayAccountModel? = NewWithdrawMocks().getPicPayAccounts()?.first

    lazy var balanceItem: WalletBalanceItem? = {
        return WalletBalanceItemMock().balanceItem()
    }()

    lazy var sut: NewWithdrawViewModel = {
        let dependencies = DependencyContainerMock()
        let viewModel = NewWithdrawViewModel(
            withdrawType,
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: dependencies)
        viewModel.balanceItem = balanceItem
        viewModel.mainAccount = bankAccount
        viewModel.picpayAccount = picpayAccount
        return viewModel
    }()

    func testGetAvailableAmount_WhenSucces_ShouldReturnWalletBalanceInstace() throws {
        // Given
        sut.loadView()

        // Then
        let balance = try XCTUnwrap(balanceItem)
        XCTAssertEqual(presenterSpy.availableAmountCallsCount, 1)
        XCTAssertEqual(presenterSpy.showBankAccountCallsCount, 1)
        XCTAssertEqual(presenterSpy.availableAmountReceivedInvocations.first, balance.available)
        XCTAssertEqual(presenterSpy.didReceiveBankAccountReceivedInvocations.first, withdrawMocks.getAccounts()?.first)
    }

    func testGetAvailableAmount_WhenFail_ShouldPresentRollbackFail() {
        //Given
        bankAccount = nil
        sut.loadView()

        //Then
        XCTAssertEqual(presenterSpy.rollBackFailCallsCount, 1)
    }

    func testGetAvailableAmountForPicPayAccount_WhenSucces_ShouldReturnWalletBalanceInstace() throws {
        // Given
        withdrawType = .picpay
        sut.loadView()

        // Then
        let balance = try XCTUnwrap(balanceItem)
        XCTAssertEqual(presenterSpy.availableAmountCallsCount, 1)
        XCTAssertEqual(presenterSpy.showPicPayAccountCallsCount, 1)
        XCTAssertEqual(presenterSpy.availableAmountReceivedInvocations.first, balance.available)
        XCTAssertEqual(presenterSpy.didReceivePicPayAccountReceivedInvocations.first, withdrawMocks.getPicPayAccounts()?.first)
    }

    func testGetAvailableAmountForPicPayAccount_WhenFail_ShouldPresentRollbackFail() {
        //Given
        picpayAccount = nil
        withdrawType = .picpay
        sut.loadView()

        //Then
        XCTAssertEqual(presenterSpy.rollBackFailCallsCount, 1)
    }

    func testNewAmountValue_WhenTypedZero_ShouldDisableConfirmButton() throws {
        // Given
        let value = lowerLimit

        // When
        sut.newAmountValue(value)

        // Then
        let unwrapValue = try XCTUnwrap(presenterSpy.enableButtonReceivedInvocations.first)
        XCTAssertFalse(unwrapValue)
    }

    func testNewAmountValue_WhenTypeSmallThanAvailableAmount_ShouldEnableConfirmButton() throws {
        // Given
        let value = Double.random(in: lowerLimit.nextUp..<upperLimit)

        // When
        sut.newAmountValue(value)

        // Then
        let unwrapValue = try XCTUnwrap(presenterSpy.enableButtonReceivedInvocations.first)
        XCTAssertTrue(unwrapValue)
    }

    func testNewAmountValue_WhenTypedEqualToAvailableAmount_ShouldEnableConfirmButton() throws {
        // Given
        let value = upperLimit

        // When
        sut.newAmountValue(value)

        // Then
        let unwrapValue = try XCTUnwrap(presenterSpy.enableButtonReceivedInvocations.first)
        XCTAssertTrue(unwrapValue)
    }

    func testNewAmountValue_WhenTypedGreaterThanAvailableAmount_ShouldDisableConfirmButton() throws {
        // Given
        let value = Double.random(in: upperLimit.nextUp..<Double.greatestFiniteMagnitude)

        // When
        sut.newAmountValue(value)

        // Then
        let unwrapValue = try XCTUnwrap(presenterSpy.enableButtonReceivedInvocations.first)
        XCTAssertFalse(unwrapValue)
    }
    
    func testRequestWithdraw_WhenSuccess_ShouldCallStartLoading() {
        // Given
        serviceMock.shouldFail = false

        // When
        sut.requestWithdraw(upperLimit)

        // Then
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 1)
    }
    
    func testRequestWithdraw_WhenSuccess_ShouldCallSuccessFunction() {
        // Given
        serviceMock.shouldFail = false

        // When
        sut.requestWithdraw(upperLimit)

        // Then
        XCTAssertEqual(presenterSpy.bankAccountSuccessCallsCount, 1)
        XCTAssertEqual(presenterSpy.failCallsCount, 0)
    }

    func testRequestPicPayAccountWithdraw_WhenSuccess_ShouldCallSuccessFunction() {
        // Given
        serviceMock.shouldFail = false

        // When
        withdrawType = .picpay
        sut.requestWithdraw(upperLimit)

        // Then
        XCTAssertEqual(presenterSpy.picpayAccountSuccessCallsCount, 1)
        XCTAssertEqual(presenterSpy.failCallsCount, 0)
    }

    func testRequestWithdraw_WhenFail_ShouldCallFailFunction() {
        // Given
        serviceMock.shouldFail = true

        // When
        sut.requestWithdraw(upperLimit)

        // Then
        XCTAssertEqual(presenterSpy.failCallsCount, 1)
        XCTAssertEqual(presenterSpy.bankAccountSuccessCallsCount, 0)
    }

    func testRequestWithdraw_WhenUserCancelAuth_ShouldCallCancelAuth() {
        // Given
        serviceMock.shouldCancel = true

        // When
        sut.requestWithdraw(upperLimit)

        // Then
        XCTAssertEqual(presenterSpy.didCancelAuthCallsCount, 1)
    }

    func testRequestWithdraw_WhenPendingWithdrawIsTrue_ShouldShowPendingWithdrawData() {
        // Given
        serviceMock.shouldPending = true

        // When
        sut.loadView()

        // Then
        XCTAssertEqual(presenterSpy.pendingWithdrawCallsCount, 1)
    }

    func testDismissAction_ShouldFireDismissAction() {
        // Given
        sut.dismiss()

        //Then
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.first, NewWithdrawAction.dismiss)
    }
}
