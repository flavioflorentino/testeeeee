import XCTest
@testable import PicPayEmpresas

final private class NewWithdrawCoordinatorTests: XCTestCase {
    private lazy var viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    private lazy var tabControllerSpy = TabBarControllerSpy()

    private lazy var sut: NewWithdrawCoordinating = {
        let coordinator = NewWithdrawCoordinator()
        tabControllerSpy.viewControllers = [navigationSpy]
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()

    func testPerformActionDismiss_ShouldDismissViewController() {
        sut.perform(action: .dismiss)

        XCTAssertEqual(navigationSpy.popedViewControllerCount, 1)
    }

    func testPerformSuccessBankAccountWithdraw_ShouldPresentSuccessWithdrawViewController() {
        sut.perform(action: .successBankAccount(message: ""))

        XCTAssertEqual(navigationSpy.presentCount, 1)
        XCTAssertEqual(navigationSpy.popToRoot, 1)
        XCTAssertTrue(navigationSpy.presentViewController is WithdrawSuccessViewController)
    }

    func testPerformSuccessPicPayWithdraw_ShouldPresentSuccessWithdrawViewController() {
        sut.perform(action: .successPicPayAccount(message: ""))

        XCTAssertEqual(navigationSpy.presentCount, 1)
        XCTAssertEqual(navigationSpy.popToRoot, 1)
        XCTAssertTrue(navigationSpy.presentViewController is WithdrawSuccessViewController)
    }

    func testPerformPendingWithdraw_ShouldPresentPendingWithdrawPopUp() {
        let withdrawItem = UnavailableWithdrawItem(pending: true, amount: nil, createdAt: nil, accountBank: nil)
        sut.perform(action: .pendingWithdraw(withdrawItem))

        XCTAssertEqual(tabControllerSpy.presentCount, 1)
        XCTAssertTrue(tabControllerSpy.presentViewController is UnavailableWithdrawViewController)
    }
}
