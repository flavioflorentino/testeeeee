import XCTest
@testable import PicPayEmpresas

final class NewWithdrawMocks {
    func getAccounts() -> [WithdrawAccount]? {
        LocalMock<[WithdrawAccount]>().localMock("withdrawAccounts")
    }

    func getPicPayAccounts() -> [PicpayAccountModel]? {
        LocalMock<[PicpayAccountModel]>().localMock("withdrawPicPayAccount")
    }

    func pendingWithdraw() -> UnavailableWithdrawItem? {
        LocalMock<UnavailableWithdrawItem>().localMock("pendingWithdraw")
    }

    func noPendingWithdraw() -> UnavailableWithdrawItem? {
        LocalMock<UnavailableWithdrawItem>().localMock("noPendingWithdraw")
    }
}
