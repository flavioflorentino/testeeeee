import LegacyPJ
import XCTest
@testable import PicPayEmpresas

private final class NewWithdrawCoordinatingSpy: NewWithdrawCoordinating {
    var viewController: UIViewController?

    // MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [NewWithdrawAction] = []

    func perform(action: NewWithdrawAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

private final class NewWithdrawDisplaySpy: NewWithdrawDisplay {
    // MARK: - perform
    private(set) var performCallsCount = 0
    private(set) var performReceivedInvocations: [NewWithdrawDisplayAction] = []

    func perform(_ displayAction: NewWithdrawDisplayAction) {
        performCallsCount += 1
        performReceivedInvocations.append(displayAction)
    }
    
    private(set) var startLoadingCallsCount = 0
    func startLoading() {
        startLoadingCallsCount += 1
    }
}

private final class NewWithdrawPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = NewWithdrawCoordinatingSpy()
    private lazy var viewControllerSpy = NewWithdrawDisplaySpy()
    private var bankAccount: WithdrawAccount? = NewWithdrawMocks().getAccounts()?.first
    private var picpayAccount: PicpayAccountModel? = NewWithdrawMocks().getPicPayAccounts()?.first
    private var pendingWithdraw: UnavailableWithdrawItem? = NewWithdrawMocks().pendingWithdraw()

    private lazy var sut: NewWithdrawPresenting = {
        let presenter = NewWithdrawPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testShowBankAccount_ShouldPresentBankAccountView() {
        // Given
        sut.showBankAccount()

        // Then
        XCTAssertEqual(viewControllerSpy.performCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.performReceivedInvocations.last, NewWithdrawDisplayAction.showBankAccount)
    }

    func testShowPicPayAccount_ShouldPresentPicPayAccountView() {
        // Given
        sut.showPicPayAccount()

        // Then
        XCTAssertEqual(viewControllerSpy.performCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.performReceivedInvocations.last, NewWithdrawDisplayAction.showPicPayAccount)
    }

    func testDidCancelAuthentication_ShouldCallStopLoadingAction() {
        // Given
        sut.didCancelAuth()

        // Then
        XCTAssertEqual(viewControllerSpy.performCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.performReceivedInvocations.last, NewWithdrawDisplayAction.stopLoading)
    }

    func testDidReceivePicPayAccount_ShouldPresentPicPayAccountInformation() throws {
        // Given
        let account = try XCTUnwrap(picpayAccount)
        sut.didReceivePicPayAccount(account)

        // Then
        let action = NewWithdrawDisplayAction.presentPicPayAccount(account)
        XCTAssertEqual(viewControllerSpy.performCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.performReceivedInvocations.last, action)
    }

    func testDidReceiveBankAccount_ShouldPresentBankAccountInformation() throws {
        // Given
        let account = try XCTUnwrap(bankAccount)
        let accountItem = WithdrawAccountItem(account)
        sut.didReceiveBankAccount(account)

        // Then
        let action = NewWithdrawDisplayAction.presentBankAccount(accountItem)
        XCTAssertEqual(viewControllerSpy.performCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.performReceivedInvocations.last, action)
    }

    func testAvailableAmount_ShouldPresentAmountAsCurrency() {
        let amount = "123,45"
        // Given
        sut.availableAmount(amount)

        // Then
        let action = NewWithdrawDisplayAction.availableAmount(amount.currency())
        XCTAssertEqual(viewControllerSpy.performCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.performReceivedInvocations.last, action)
    }

    func testEnableButton_WhenParameterIsTrue_ShouldEnableWithdrawButton() {
        // Given
        let flag = true

        // When
        sut.enableButton(flag)

        // Then
        let action = NewWithdrawDisplayAction.enableButton(true)
        XCTAssertEqual(viewControllerSpy.performCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.performReceivedInvocations.last, action)
    }

    func testEnableButton_WhenParameterIsFalse_ShouldEnableWithdrawButton() {
        // Given
        let flag = false

        // When
        sut.enableButton(flag)

        // Then
        let action = NewWithdrawDisplayAction.enableButton(false)
        XCTAssertEqual(viewControllerSpy.performCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.performReceivedInvocations.last, action)
    }

    func testBankAccountSuccess_ShouldStopLoadingAndInvokeSuccessBankAccountActionOnCoordinator() {
        // Given
        sut.bankAccountSuccess(String())

        // Then
        let coordinatorAction = NewWithdrawAction.successBankAccount(message: String())
        XCTAssertEqual(viewControllerSpy.performCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.performReceivedInvocations.last, NewWithdrawDisplayAction.stopLoading)
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations.last, coordinatorAction)
    }

    func testPicPayAccountSuccess_ShouldStopLoadingAndInvokeSuccessPicPayActionOnCoordinator() {
        // Given
        sut.picpayAccountSuccess(String())

        // Then
        let coordinatorAction = NewWithdrawAction.successPicPayAccount(message: String())
        XCTAssertEqual(viewControllerSpy.performCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.performReceivedInvocations.last, NewWithdrawDisplayAction.stopLoading)
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations.last, coordinatorAction)
    }

    func testPendingWithdraw_ShouldInvokePendingWithdrawActionOnCoordinator() throws {
        // Given
        let pending = try XCTUnwrap(pendingWithdraw)
        sut.pendingWithdraw(pending)

        // Then
        let coordinatorAction = NewWithdrawAction.pendingWithdraw(pending)
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations.last, coordinatorAction)
    }

    func testFailFunction_ShouldPresentValidationMessage() {
        // Given
        sut.fail(String())

        // Then
        let action = NewWithdrawDisplayAction.validationMessage(String())
        XCTAssertEqual(viewControllerSpy.performCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.performReceivedInvocations.last, action)
    }

    func testRollBackError_ShouldShowMessage() {
        // Given
        let error = LegacyPJError(message: String())
        sut.rollBackFail(error)

        // Then
        let action = NewWithdrawDisplayAction.rollBackError(error.message)
        XCTAssertEqual(viewControllerSpy.performCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.performReceivedInvocations.last, action)
    }
    
    func testStartLoading_ShouldCallStartLoading() {
        sut.startLoading()
        
        XCTAssertEqual(viewControllerSpy.startLoadingCallsCount, 1)
    }
}

extension NewWithdrawDisplayAction: Equatable {
    public static func == (lhs: NewWithdrawDisplayAction, rhs: NewWithdrawDisplayAction) -> Bool {
        switch (lhs, rhs) {
        case (.showBankAccount, .showBankAccount):
            return true
        case (.showPicPayAccount, .showPicPayAccount):
            return true
        case (.stopLoading, .stopLoading):
            return true
        case let (.presentPicPayAccount(a), .presentPicPayAccount(b)):
            return a.name == b.name &&
                a.email == b.email &&
                a.username == b.username &&
                a.verified == b.verified
        case let (.presentBankAccount(a), .presentBankAccount(b)):
            return a.bankName == b.bankName &&
                a.imageUrl == b.imageUrl &&
                a.branch == b.branch &&
                a.account == b.account
        case let (.availableAmount(a), .availableAmount(b)):
            return a == b
        case let (.enableButton(a), .enableButton(b)):
            return a == b
        case let (.validationMessage(a), .validationMessage(b)):
            return a == b
        case let (.rollBackError(a), .rollBackError(b)):
            return a == b
        default:
            return false
        }
    }
}
