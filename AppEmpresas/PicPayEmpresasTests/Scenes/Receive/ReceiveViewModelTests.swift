import Core
import XCTest
@testable import PicPayEmpresas

final private class ReceiveViewModelTests: XCTestCase {
    private let serviceMock = ReceiveServiceMock()
    private let presenterSpy = ReceivePresenterSpy()
    private lazy var sut = ReceiveViewModel(service: serviceMock, presenter: presenterSpy)

    func testCallLinkAction_WhenReceiveActionFromViewController_ShouldPushNewViewController(){
        sut.linkAction()
        
        XCTAssertNotNil(presenterSpy.actionSelected)
    }
    
    func testLoadQRCodeImage_WhenReceiveCallFromViewController_ShouldSendKeyForPresenter() {
        serviceMock.isSuccess = true
        
        sut.loadQrCodeImage()
        
        XCTAssertEqual(presenterSpy.qrCodeKey, "http://app.picpay.com/payment?type=store&sellerId=14581")
    }
    
    func testErrorOnLoadQRCodeImage_WhenReceiveCallFromViewController_ShouldShowErrorQRView() {
        serviceMock.isSuccess = false
        
        sut.loadQrCodeImage()
        
        XCTAssertTrue(presenterSpy.handleErrorCount > 0)
    }
}

final private class ReceivePresenterSpy: ReceivePresenting {
    var viewController: ReceiveDisplay?
    
    private(set) var actionSelected: ReceiveAction?
    private(set) var qrCodeKey: String?
    private(set) var handleErrorCount = 0
    
    func didNextStep(action: ReceiveAction) {
        actionSelected = action
    }
    
    func generatedQrImage(with code: String?) {
        qrCodeKey = code
    }
    
    func handleError() {
        handleErrorCount += 1
    }
}

final private class ReceiveServiceMock: ReceiveServicing {
    var isSuccess = true
    
    func loadQrCodeImage(_ completion: @escaping (Result<SellerQRCode?, ApiError>) -> Void) {
        guard isSuccess else {
            completion(.failure(ApiError.serverError))
            return
        }
        
        let mock = ReceiveMock()
        completion(.success(mock.getValidQRCode()))
    }
}
