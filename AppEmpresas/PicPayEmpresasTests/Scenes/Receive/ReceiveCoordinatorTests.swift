import UIKit
import XCTest
@testable import PicPayEmpresas

final private class ReceiveCoordinatorTests: XCTestCase {
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: UIViewController())
    
    private lazy var sut: ReceiveCoordinator = {
        let coordinator = ReceiveCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenReceiveValueScreenFromPresenter_ShouldPushViewController() {
        sut.perform(action: .valueScreen)
        
        XCTAssertTrue(navigationSpy.currentViewController is ReceiveInputValueViewController)
    }
}
