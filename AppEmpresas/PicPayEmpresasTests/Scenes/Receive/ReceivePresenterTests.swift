import XCTest
@testable import PicPayEmpresas

final private class ReceivePresenterTests: XCTestCase {
    private let coordinatorSpy =  ReceiveCoordinatorSpy()
    private let displaySpy = ReceiveDispaySpy()
    
    private lazy var sut: ReceivePresenter = {
        let presenter = ReceivePresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func tesdidNextStep_WhenReceiveActionFromViewModel_ShouldPushNewViewController(){
        sut.didNextStep(action: .valueScreen)
        
        XCTAssertNotNil(coordinatorSpy.actionSelected)
    }
    
    func testGeneratedQrImage_WhenReceiveCodeFromViewModel_ShouldReturnUIImage() {
        let mock = ReceiveMock()
        
        sut.generatedQrImage(with: mock.getValidQRCode()?.key)
        
        XCTAssertNotNil(displaySpy.qrImage)
    }
    
    func testGeneratedQrImageFailure_WhenNotReceiveCodeFromViewModel_ShouldUIImageIsNil() {
        sut.generatedQrImage(with: nil)
        
        XCTAssertNil(displaySpy.qrImage)
    }
    
    func testHandleError_WhenReceiveErrorFromViewModel_ShouldShowErrorQRView() {
        sut.handleError()
        
        XCTAssertTrue(displaySpy.handleErrorCount > 0)
    }
}

final private class ReceiveCoordinatorSpy: ReceiveCoordinating {
    var viewController: UIViewController?
    
    private(set) var actionSelected: ReceiveAction?
    
    func perform(action: ReceiveAction) {
        actionSelected = action
    }
}

final private class ReceiveDispaySpy: ReceiveDisplay {
    private(set) var handleErrorCount = 0
    private(set) var qrImage: UIImage?
    
    func updateQrImage(image: UIImage) {
        qrImage = image
    }
    
    func showQrError() {
        handleErrorCount += 1
    }
}
