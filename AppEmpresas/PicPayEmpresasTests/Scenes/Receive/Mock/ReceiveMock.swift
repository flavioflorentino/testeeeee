import Core
import XCTest
@testable import PicPayEmpresas

final class ReceiveMock {
    func getValidQRCode() -> SellerQRCode? {
        LocalMock<SellerQRCode>().localMock("validQRCode")
    }
    
    func getValidLinkToShare() -> SharePaymentLink? {
        LocalMock<SharePaymentLink>().localMock("validLinkShare")
    }
}
