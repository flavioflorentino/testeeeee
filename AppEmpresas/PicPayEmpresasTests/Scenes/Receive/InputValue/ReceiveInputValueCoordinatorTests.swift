import XCTest
@testable import PicPayEmpresas

final private class ReceiveInputValueCoordinatorTests: XCTestCase {
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: UIViewController())
    
    private lazy var sut: ReceiveInputValueCoordinator = {
        let coordinator = ReceiveInputValueCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testGoBackAction_WhenReceiveActionFromPresenter_ShouldPopToRootViewController() {
        sut.perform(action: .goBack)
        
        XCTAssertTrue(navigationSpy.popToRoot > 0)
    }
}
