import XCTest
@testable import PicPayEmpresas

final private class ReceiveInputValuePresenterTests: XCTestCase {
    private let coordinatorSpy = ReceiveInputCoordinatorSpy()
    private let displaySpy = ReceiveInputDispaySpy()
    
    private lazy var sut: ReceiveInputValuePresenter = {
        let sut = ReceiveInputValuePresenter(coordinator: coordinatorSpy)
        sut.viewController = displaySpy
        return sut
    }()
    
    func testDidNextStep_WhenReceiveGoBackActionFromViewModel_ShouldCoordinatorReceiveAnActionBack() {
        sut.didNextStep(action: .goBack)
        
        XCTAssertNotNil(coordinatorSpy.actionSelected)
    }
    
    func testButtonEnable_WhenReceiveActionFromViewModel_ShouldReturnTrueValue() throws {
        sut.changeEnableValueConfirmButton(enable: true)
        
        let resultUnWarrping = try XCTUnwrap(displaySpy.confirmButtonEnable)
        XCTAssertTrue(resultUnWarrping)
    }
    
    func testButtonDisable_WhenReceiveActionFromViewModel_ShouldReturnFalseValue() throws {
        sut.changeEnableValueConfirmButton(enable: false)
        
        let resultUnWarrping = try XCTUnwrap(displaySpy.confirmButtonEnable)
        XCTAssertFalse(resultUnWarrping)
    }
    
    func testShowShareActivity_WhenReceiveActionFromViewModel_ShouldDisplayLinkInActivity() throws {
        let url = ReceiveMock().getValidLinkToShare()?.url
        let urlUnwrap = try XCTUnwrap(url)
        sut.showShareActivity(link: urlUnwrap, value: 5.0)
        
        XCTAssertEqual(displaySpy.link, urlUnwrap)
    }
    
    func testHandleError_WhenReceiveActionFromViewModel_ShouldCountHandleError() {
        sut.handleError()
        
        XCTAssertEqual(displaySpy.handleErrorCount, 1)
    }
}

final private class ReceiveInputCoordinatorSpy: ReceiveInputValueCoordinating {
    var viewController: UIViewController?
    
    private(set) var actionSelected: ReceiveInputValueAction?
    
    func perform(action: ReceiveInputValueAction) {
        actionSelected = action
    }
}

final private class ReceiveInputDispaySpy: ReceiveInputValueDisplay {
    private(set) var handleErrorCount = 0
    private(set) var confirmButtonEnable: Bool?
    private(set) var link: String?
    
    func changeEnableValueConfirmButton(enable: Bool) {
        confirmButtonEnable = enable
    }
    
    func showActivityShare(link: String, value: Double?) {
        self.link = link
    }
    
    func showError() {
        handleErrorCount += 1
    }
}
