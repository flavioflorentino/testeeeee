import Core
import XCTest
@testable import PicPayEmpresas

final private class ReceiveInputValueViewModelTests: XCTestCase {
    private let presenterSpy = ReceiveInputPresenterSpy()
    private let serviceMock = ReceiveInputServiceMock()
    private let defaultValue: Double = 5000
    private lazy var sut = ReceiveInputValueViewModel(service: serviceMock, presenter: presenterSpy)

    func testButtonEnable_WhenReceiveActionFromViewController_ShouldReturnTrueValue() throws {
        sut.checkEnableConfirmButton(value: defaultValue)
        
        let resultUnWarrping = try XCTUnwrap(presenterSpy.confirmButtonEnable)
        XCTAssertTrue(resultUnWarrping)
    }
    
    func testButtonDisable_WhenReceiveActionFromViewController_ShouldReturnFalseValue() throws {
        sut.checkEnableConfirmButton(value: 0)
        
        let resultUnWarrping = try XCTUnwrap(presenterSpy.confirmButtonEnable)
        XCTAssertFalse(resultUnWarrping)
    }
    
    func testSuccessGeneratedLink_WhenReceiveRequestFromViewController_ShouldReturnALink() {
        serviceMock.response = .success
        
        sut.generatedLink(value: defaultValue)
        
        XCTAssertEqual(presenterSpy.link, "http://app.picpay.com/payment?type=store&sellerId=14650")
    }
    
    func testFailureGeneratedLink_WhenReceiveRequestFromViewController_ShouldCountHandleError() {
        serviceMock.response = .failure
        
        sut.generatedLink(value: defaultValue)
        
        XCTAssertEqual(presenterSpy.handleErrorCount, 1)
    }
    
    func testShowError_WhenReceiveActionFromViewController_ShouldCountHandleError() {
        sut.handleActivityResult(error: ApiError.bodyNotFound)
        
        XCTAssertEqual(presenterSpy.handleErrorCount, 1)
    }
    
    func testGoBack_WhenReceiveActionFromViewController_ShouldReceiveActionBack() {
        sut.handleActivityResult(error: nil)
        
        XCTAssertNotNil(presenterSpy.actionSelected)
    }
    
    func testGenerateLinkWithouValue_WhenReceivedSuccessResponse_ShouldReturnSharableLink() {
        sut.generatedLinkWithoutValue()
        
        XCTAssertEqual(serviceMock.didCallGetLink, 1)
        XCTAssertEqual(presenterSpy.link, "http://app.picpay.com/payment?type=store&sellerId=14650")
        XCTAssertEqual(presenterSpy.didCallShowShareActivity, 1)
    }
    
    func testGenerateLinkWithoutValue_WhenReceivedFailedResponse_ShouldHandleError() {
        serviceMock.response = .failure
        
        sut.generatedLinkWithoutValue()
        
        XCTAssertEqual(serviceMock.didCallGetLink, 1)
        XCTAssertNil(presenterSpy.link)
        XCTAssertEqual(presenterSpy.handleErrorCount, 1)
        XCTAssertNotEqual(presenterSpy.didCallShowShareActivity, 1)
    }
}

final private class ReceiveInputPresenterSpy: ReceiveInputValuePresenting {
    var viewController: ReceiveInputValueDisplay?
    
    private(set) var actionSelected: ReceiveInputValueAction?
    private(set) var link: String?
    private(set) var confirmButtonEnable: Bool?
    
    private(set) var handleErrorCount = 0
    private(set) var didCallShowShareActivity = 0
    
    func didNextStep(action: ReceiveInputValueAction) {
        actionSelected = action
    }
    
    func changeEnableValueConfirmButton(enable: Bool) {
        confirmButtonEnable = enable
    }
    
    func showShareActivity(link: String, value: Double?) {
        self.link = link
        didCallShowShareActivity += 1
    }
    
    func handleError() {
        handleErrorCount += 1
    }
}

private enum ReceiveInputServiceMockResponse {
    case success
    case failure
}

final private class ReceiveInputServiceMock: ReceiveInputValueServicing {
    var response: ReceiveInputServiceMockResponse = .success
    
    private(set) var didCallGetLink = 0
    
    private let successLinkWithouValueResponse = SharePaymentLink(url: "http://app.picpay.com/payment?type=store&sellerId=14650")
    
    func getLink(value: Int?, _ completion: @escaping LinkCompletionBlock) {
        didCallGetLink += 1
        switch response {
        case .success:
            completion(.success(successLinkWithouValueResponse))
        case .failure:
            completion(.failure(ApiError.connectionFailure))
        }
    }
}
