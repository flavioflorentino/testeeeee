import Core
import LegacyPJ
import XCTest
@testable import PicPayEmpresas

private final class AutomaticWithdrawalPresenterSpy: AutomaticWithdrawalPresenting {
    var viewController: AutomaticWithdrawalDisplay?

    private (set) var didReceiveMainAccount = 0
    private (set) var didSetAutomaticWithdraw = 0
    private (set) var didReceiveError = 0
    private (set) var didReverseSwitch = 0
    private (set) var didShowErrorView = 0
    private (set) var error: Error? = nil
    private (set) var isLoading: Bool = false

    func setLoading(isLoading: Bool) {
        self.isLoading = isLoading
    }

    func setAutomaticWithdrawalStatus(_ value: Bool) {
        didSetAutomaticWithdraw += 1
    }

    func showMainAccount(account: WithdrawAccount) {
        didReceiveMainAccount += 1
    }

    func reverseSwitch() {
        didReverseSwitch += 1
    }
    
    func showError(error: Error) {
        self.error = error
        didReceiveError += 1
    }
    
    func showErrorView() {
        didShowErrorView += 1
    }
}

private final class AutomaticWithdrawalServiceMock: AutomaticWithdrawalServicing {
    var getAccountsResult: Result<[WithdrawAccount], ApiError>?
    func getAccounts(_ completion: @escaping(Result<[WithdrawAccount], ApiError>) -> Void) {
        guard let getAccountsResult = getAccountsResult else {
            XCTFail("getAccountsResult missing")
            return
        }
        completion(getAccountsResult)
    }

    func getWithdrawalStatus(_ completion: @escaping(Result<Bool, ApiError>) -> Void) {
    }
    
    var updateAutomaticWithdrawalResult: Result<NoContent, ApiError>?
    func updateAutomaticWithdrawal(_ pin: String, accountID: Int, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let updateAutomaticWithdrawalResult = updateAutomaticWithdrawalResult else {
            XCTFail("updateAutomaticWithdrawalResult missing")
            return
        }
        completion(updateAutomaticWithdrawalResult)
    }
}

private final class AutomaticWithdrawalViewModelDelegateSpy: AutomaticWithdrawalViewModelDelegate {
    private(set) var updateAutomaticWithdrawalStatusSent: Bool?
    
    func successOnUpdateAutomaticWithdrawalStatus(status: Bool) {
        updateAutomaticWithdrawalStatusSent = status
    }
}


final class AutomaticWithdrawalViewModelTests: XCTestCase {
    private let serviceMock = AutomaticWithdrawalServiceMock()
    private let presenterSpy = AutomaticWithdrawalPresenterSpy()
    private let delegateSpy = AutomaticWithdrawalViewModelDelegateSpy()
    private let authManagerMock = AuthManagerMock()
    private var isAutomaticWithdrawalInitiallyEnabled: Bool? = true
    
    private lazy var sut: AutomaticWithdrawalViewModel = {
        let container = DependencyContainerMock(authManagerMock)
        let viewModel = AutomaticWithdrawalViewModel(
                service: serviceMock,
                presenter: presenterSpy,
                dependencies: container,
                isAutomaticWithdrawalEnabled: isAutomaticWithdrawalInitiallyEnabled
        )
        viewModel.delegate = delegateSpy
        return viewModel
    }()

    func testGetWithdrawStatus_WhenSuccess_ShouldReturnMainAccount() throws {
        let accounts = try XCTUnwrap(NewWithdrawMocks().getAccounts())
        serviceMock.getAccountsResult = .success(accounts)
        sut.loadData()
        isAutomaticWithdrawalInitiallyEnabled = nil
        
        XCTAssertEqual(presenterSpy.didSetAutomaticWithdraw, 1)
        XCTAssertEqual(presenterSpy.didReceiveError, 0)
    }

    func testGetMainAccount_WhenSuccess_ShouldReturnMainAccount() throws {
        let accounts = try XCTUnwrap(NewWithdrawMocks().getAccounts())
        serviceMock.getAccountsResult = .success(accounts)
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.didReceiveMainAccount, 1)
        XCTAssertEqual(presenterSpy.didReceiveError, 0)
    }
    
    func testGetMainAccount_WhenFail_ShouldReturnError() throws {
        serviceMock.getAccountsResult = .failure(ApiError.serverError)
        sut.loadData()
        
        XCTAssertEqual(presenterSpy.didShowErrorView, 1)
        XCTAssertEqual(presenterSpy.didReceiveMainAccount, 0)
    }
    
    func testUpdateAutomaticWithdrawalAuthenticate_WhenFail_ShouldStopLoadingWithErrorAndRevertSwitch() throws {
        let accounts = try XCTUnwrap(NewWithdrawMocks().getAccounts())
        serviceMock.getAccountsResult = .success(accounts)
        authManagerMock.actionResultToReturn = .failure(LegacyPJError(message: "Erro"))
        
        sut.loadData()
        sut.updateAutomaticWithdrawal(newStatus: true)
        
        XCTAssertEqual(presenterSpy.didReceiveError, 1)
        XCTAssertEqual(presenterSpy.didReverseSwitch, 1)
        XCTAssertNil(delegateSpy.updateAutomaticWithdrawalStatusSent)
        XCTAssertNotNil(presenterSpy.error)
        XCTAssertFalse(presenterSpy.isLoading)
    }
    
    func testUpdateAutomaticWithdrawalAuthenticate_WhenCancel_ShouldStopLoadingWithoutErrorAndRevertSwitch() throws {
        let accounts = try XCTUnwrap(NewWithdrawMocks().getAccounts())
        serviceMock.getAccountsResult = .success(accounts)
        authManagerMock.actionResultToReturn = .canceled

        sut.loadData()
        sut.updateAutomaticWithdrawal(newStatus: true)
        
        XCTAssertEqual(presenterSpy.didReceiveError, 0)
        XCTAssertEqual(presenterSpy.didReverseSwitch, 1)
        XCTAssertNil(delegateSpy.updateAutomaticWithdrawalStatusSent)
        XCTAssertNil(presenterSpy.error)
        XCTAssertFalse(presenterSpy.isLoading)
    }
    
    func testUpdateAutomaticWithdrawal_WhenSuccess_ShouldStopLoadingWithoutError() throws {
        let accounts = try XCTUnwrap(NewWithdrawMocks().getAccounts())
        serviceMock.getAccountsResult = .success(accounts)
        
        serviceMock.updateAutomaticWithdrawalResult = .success(NoContent())
        
        authManagerMock.actionResultToReturn = .success("Pin")

        sut.loadData()
        sut.updateAutomaticWithdrawal(newStatus: false)
        
        XCTAssertEqual(presenterSpy.didReceiveError, 0)
        XCTAssertEqual(presenterSpy.didReverseSwitch, 0)
        XCTAssert(delegateSpy.updateAutomaticWithdrawalStatusSent == false)
        XCTAssertNil(presenterSpy.error)
        XCTAssertFalse(presenterSpy.isLoading)
    }
    
    func testUpdateAutomaticWithdrawal_WhenFail_ShouldStopLoadingWithErrorAndRevertSwitch() throws {
        let accounts = try XCTUnwrap(NewWithdrawMocks().getAccounts())
        serviceMock.getAccountsResult = .success(accounts)
        authManagerMock.actionResultToReturn = .success("Pin")
        serviceMock.updateAutomaticWithdrawalResult = .failure(ApiError.serverError)
        
        sut.loadData()
        
        sut.updateAutomaticWithdrawal(newStatus: true)

        XCTAssertEqual(self.presenterSpy.didReceiveError, 1)
        XCTAssertEqual(self.presenterSpy.didReverseSwitch, 1)
        XCTAssertNil(delegateSpy.updateAutomaticWithdrawalStatusSent)
        XCTAssertNotNil(self.presenterSpy.error)
        XCTAssertFalse(self.presenterSpy.isLoading)
    }
}
