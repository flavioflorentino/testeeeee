import XCTest
import Validator
import Core
import LegacyPJ
import SwiftyJSON
@testable import PicPayEmpresas

private final class RegisterWhatsAppNumberPresenterSpy: RegisterWhatsAppNumberPresenting {
    weak var viewController: RegisterWhatsAppNumberDisplaying?

    //MARK: - setupLink
    private(set) var presentLinkCallsCount = 0

    func presentLink() {
        presentLinkCallsCount += 1
    }

    //MARK: - open
    private(set) var openUrlCallsCount = 0
    private(set) var openUrlReceivedInvocations: [URL] = []

    func open(url: URL) {
        openUrlCallsCount += 1
        openUrlReceivedInvocations.append(url)
    }

    //MARK: - setRules
    private(set) var prepareRulesCallsCount = 0
    private(set) var prepareRulesReceivedInvocations: [ValidationRuleSet<String>] = []

    func prepareRules(_ rules: ValidationRuleSet<String>) {
        prepareRulesCallsCount += 1
        prepareRulesReceivedInvocations.append(rules)
    }

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [RegisterWhatsAppNumberAction] = []

    func didNextStep(action: RegisterWhatsAppNumberAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }

    //MARK: - updateTextFieldState
    private(set) var updateTextFieldStateCallsCount = 0
    private(set) var updateTextFieldStateMessage: String?

    func updateTextFieldState(_ message: String?) {
        updateTextFieldStateCallsCount += 1
        updateTextFieldStateMessage = message
    }

    //MARK: - shouldUpdateTextFieldAndSwitch
    private(set) var shouldUpdateTextFieldAndSwitchCallsCount = 0
    private(set) var shouldUpdateTextFieldAndSwitchReceivedInvocations: [WhatsAppAccount] = []

    func shouldUpdateTextFieldAndSwitch(_ whatsApp: WhatsAppAccount) {
        shouldUpdateTextFieldAndSwitchCallsCount += 1
        shouldUpdateTextFieldAndSwitchReceivedInvocations.append(whatsApp)
    }

    //MARK: - presentError
    private(set) var presentErrorCallsCount = 0

    func presentError() {
        presentErrorCallsCount += 1
    }
}

private final class RegisterWhatsAppNumberServiceMock: RegisterWhatsAppNumberServicing {
    var fetchWhatsAppAccountResult: Result<WhatsAppAccount, ApiError> = .failure(ApiError.serverError)
    var updateWhatsAppAccountResult: Result<NoContent, ApiError> = .failure(ApiError.serverError)

    func fetchWhatsAppAccount(_ sellerId: Int, completion: @escaping (Result<WhatsAppAccount, ApiError>) -> Void) {
        completion(fetchWhatsAppAccountResult)
    }

    func updateWhatsAppAccount(_ sellerId: Int,
                              _ updateNumber: UpdateWhatsAppAccount,
                              completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        completion(updateWhatsAppAccountResult)
    }
}

// MARK: - Tests

final class RegisterWhatsAppNumberInteractorTests: XCTestCase {
    private let presenterSpy = RegisterWhatsAppNumberPresenterSpy()
    private let serviceMock = RegisterWhatsAppNumberServiceMock()
    lazy var dependenciesMock = DependencyContainerMock()
    private let mainQueue = DispatchQueue(label: "RegisterWhatsAppNumberInteractorTests")
    private let timeout: Double = 3.0
    private let userJson = """
                {
                  "id": 1,
                  "cpf": "37.442.929/0001-80",
                  "headOfficeId": 1,
                  "imgUrl": "https://images1.fanpop.com/images/image_uploads/Naruto-Sad-uzumaki-naruto-987855_620_465.jpg",
                  "mobilePhone": "11999999999",
                  "birthDateFormated": "31/03/1998",
                  "username": "username",
                  "super": 1,
                  "role": 1,
                  "email": "teste@picpay.com",
                  "name": "Uzumaki Naruto"
                }
            """

    private lazy var sut: RegisterWhatsAppNumberInteracting = {
        let interactor = RegisterWhatsAppNumberInteractor(service: serviceMock,
                                                          presenter: presenterSpy,
                                                          dependencies: dependenciesMock)

        return interactor
    }()

    func testSetupLink_WhenLoadView_ShouldPresentLink() {
        sut.setupLink()

        XCTAssertEqual(presenterSpy.presentLinkCallsCount, 1)
    }

    func testOpen_WhenUserTap_ShouldPresentWebView() throws {
        let url = try XCTUnwrap(URL(string: "https://www.google.com"))

        sut.open(url: url)

        XCTAssertEqual(presenterSpy.openUrlCallsCount, 1)
        XCTAssertTrue(presenterSpy.openUrlReceivedInvocations.isNotEmpty)
        XCTAssertTrue(presenterSpy.openUrlReceivedInvocations.contains(url))
    }

    func testConfigRules_WhenLoadView_ShouldPrepareRules() {
        sut.configRules()

        XCTAssertEqual(presenterSpy.prepareRulesCallsCount, 1)
        XCTAssertTrue(presenterSpy.prepareRulesReceivedInvocations.isNotEmpty)
    }

    func testValidateTextField_WhenInvalidCase_ShouldUpdateTextFieldState() throws {
        let text = "(11) 92233-"
        let validationError = PhoneValidationError.phoneNumberRequired
        let validationResult = ValidationResult.invalid([validationError])

        sut.validateTextField(result: validationResult, text: text)

        XCTAssertEqual(presenterSpy.updateTextFieldStateCallsCount, 1)
        XCTAssertEqual(presenterSpy.updateTextFieldStateMessage, Strings.RegisterWhatsAppNumber.errorMessage)
    }

    func testValidateTextField_WhenDDDInvalidCase_ShouldUpdateTextFieldState() throws {
        let text = "(00) 92233-2311"
        let validationError = PhoneValidationError.phoneNumberRequired
        let validationResult = ValidationResult.invalid([validationError])

        sut.validateTextField(result: validationResult, text: text)

        XCTAssertEqual(presenterSpy.updateTextFieldStateCallsCount, 1)
        XCTAssertEqual(presenterSpy.updateTextFieldStateMessage, Strings.RegisterWhatsAppNumber.errorMessage)
    }

    func testValidateTextField_WhenDDDValidCase_ShouldUpdateTextFieldState() throws {
        let text = "(11) 92233-2311"
        let validationResult = ValidationResult.valid

        sut.validateTextField(result: validationResult, text: text)

        XCTAssertEqual(presenterSpy.updateTextFieldStateCallsCount, 1)
        XCTAssertEqual(presenterSpy.updateTextFieldStateMessage, "")
    }

    func testUpdateWhatsAppAccount_WhenValid_ShouldGoToNextStep() throws {
        let authMock: AuthManagerMock = AuthManagerMock()
        let jsonData = try XCTUnwrap(userJson.data(using: .utf8))
        let json = try JSON(data: jsonData)
        let user = User(json: json)
        authMock.user = user
        dependenciesMock.authManager = authMock

        let whatsAppMock = WhatsAppAccount(phoneNumber: "(11) 92736-7623", active: true)
        serviceMock.updateWhatsAppAccountResult = .success(NoContent())

        sut.requestUpdateWhatsAppAccount(number: whatsAppMock.phoneNumber, active: whatsAppMock.active)

        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssert(presenterSpy.didNextStepActionReceivedInvocations.isNotEmpty)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(.goToNextStep))
    }

    func testUpdateWhatsAppAccount_WhenInvalid_ShouldPresentError() throws {
        let authMock: AuthManagerMock = AuthManagerMock()
        let jsonData = try XCTUnwrap(userJson.data(using: .utf8))
        let json = try JSON(data: jsonData)
        let user = User(json: json)
        authMock.user = user
        dependenciesMock.authManager = authMock
        serviceMock.updateWhatsAppAccountResult = .failure(ApiError.serverError)

        sut.requestUpdateWhatsAppAccount(number: "(01) 21323-", active: false)

        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 1)
    }

    func testFetchWhatsAppNumber_WhenInvalid_ShouldPresentError() throws {
        let authMock: AuthManagerMock = AuthManagerMock()
        let jsonData = try XCTUnwrap(userJson.data(using: .utf8))
        let json = try JSON(data: jsonData)
        let user = User(json: json)
        authMock.user = user
        dependenciesMock.authManager = authMock

        serviceMock.fetchWhatsAppAccountResult = .failure(ApiError.serverError)

        sut.requestFetchWhatsAppAccount()

        XCTAssertEqual(presenterSpy.presentErrorCallsCount, 1)
    }

    func testFetchWhatsAppNumber_WhenValid_ShouldUpdateTextFieldAndSwitch() throws {
        let authMock: AuthManagerMock = AuthManagerMock()
        let jsonData = try XCTUnwrap(userJson.data(using: .utf8))
        let json = try JSON(data: jsonData)
        let user = User(json: json)
        authMock.user = user
        dependenciesMock.authManager = authMock
        let whatsAppMock = WhatsAppAccount(phoneNumber: "(11) 92736-7623", active: true)

        serviceMock.fetchWhatsAppAccountResult = .success(whatsAppMock)

        sut.requestFetchWhatsAppAccount()

        XCTAssertEqual(presenterSpy.shouldUpdateTextFieldAndSwitchCallsCount, 1)
        XCTAssertTrue(presenterSpy.shouldUpdateTextFieldAndSwitchReceivedInvocations.isNotEmpty)
        XCTAssertEqual(presenterSpy.shouldUpdateTextFieldAndSwitchReceivedInvocations.first, whatsAppMock)
    }

    func testFetchWhatsAppNumber_WhenNotFoundResponse_ShouldUpdateTextFieldAndSwitch() throws {
        let authMock: AuthManagerMock = AuthManagerMock()
        let jsonData = try XCTUnwrap(userJson.data(using: .utf8))
        let json = try JSON(data: jsonData)
        let user = User(json: json)
        authMock.user = user
        dependenciesMock.authManager = authMock

        var error = RequestError()
        error.code = "404"

        serviceMock.fetchWhatsAppAccountResult = .failure(ApiError.notFound(body: error))

        sut.requestFetchWhatsAppAccount()

        XCTAssertEqual(presenterSpy.shouldUpdateTextFieldAndSwitchCallsCount, 1)
        XCTAssertTrue(presenterSpy.shouldUpdateTextFieldAndSwitchReceivedInvocations.isNotEmpty)
        let whatsAppAccount = try XCTUnwrap(presenterSpy.shouldUpdateTextFieldAndSwitchReceivedInvocations.first)
        XCTAssertFalse(whatsAppAccount.active)
        XCTAssertEqual(whatsAppAccount.phoneNumber, "")
    }
}
