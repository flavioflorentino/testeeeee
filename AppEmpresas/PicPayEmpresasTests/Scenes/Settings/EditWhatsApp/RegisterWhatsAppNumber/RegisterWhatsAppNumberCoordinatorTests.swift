import XCTest
@testable import PicPayEmpresas

final class RegisterWhatsAppNumberCoordinatorTests: XCTestCase {
    private lazy var viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    private lazy var containerMock = DependencyContainerMock()

    private lazy var sut: RegisterWhatsAppNumberCoordinating = {
        let coordinator = RegisterWhatsAppNumberCoordinator(deeplinkHelper: DeeplinkHelper(dependencies: containerMock))
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
}
