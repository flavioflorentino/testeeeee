import XCTest
import Validator
@testable import PicPayEmpresas

private class RegisterWhatsAppNumberDisplayingSpy: RegisterWhatsAppNumberDisplaying {
    //MARK: - displayLinkText
    private(set) var displayLinkTextLinkTextLinkAttributesCallsCount = 0
    private(set) var displayLinkTextLinkTextLinkAttributesReceivedInvocations:
        [(linkText: NSAttributedString, linkAttributes: [NSAttributedString.Key: Any])] = []

    func displayLinkText(linkText: NSAttributedString, linkAttributes: [NSAttributedString.Key: Any]) {
        displayLinkTextLinkTextLinkAttributesCallsCount += 1
        displayLinkTextLinkTextLinkAttributesReceivedInvocations.append((linkText: linkText, linkAttributes: linkAttributes))
    }

    //MARK: - setRules
    private(set) var setRulesCallsCount = 0
    private(set) var setRulesReceivedInvocations: [ValidationRuleSet<String>] = []

    func setRules(_ rules: ValidationRuleSet<String>) {
        setRulesCallsCount += 1
        setRulesReceivedInvocations.append(rules)
    }

    //MARK: - enableSaveButton
    private(set) var enableSaveButtonEnableCallsCount = 0
    private(set) var enableSaveButtonIsEnable = false

    func enableSaveButton(enable: Bool) {
        enableSaveButtonEnableCallsCount += 1
        enableSaveButtonIsEnable = enable
    }

    //MARK: - textFieldSuccess
    private(set) var textFieldSuccessCallsCount = 0

    func textFieldSuccess() {
        textFieldSuccessCallsCount += 1
    }

    //MARK: - textFieldError
    private(set) var textFieldErrorMessageCallsCount = 0
    private(set) var textFieldErrorMessageReceived: String = ""

    func textFieldError(message: String) {
        textFieldErrorMessageCallsCount += 1
        textFieldErrorMessageReceived = message
    }

    //MARK: - showError
    private(set) var showErrorCallsCount = 0

    func showError() {
        showErrorCallsCount += 1
    }

    //MARK: - updateTextFieldAndSwitch
    private(set) var updateTextFieldAndSwitchCallsCount = 0
    private(set) var updateTextFieldAndSwitchReceived: WhatsAppAccount = WhatsAppAccount(phoneNumber: "", active: false)

    func updateTextFieldAndSwitch(_ whatsApp: WhatsAppAccount) {
        updateTextFieldAndSwitchCallsCount += 1
        updateTextFieldAndSwitchReceived = whatsApp
    }
}

private class RegisterWhatsAppNumberCoordinatorSpy: RegisterWhatsAppNumberCoordinating {
    var viewController: UIViewController?

    //MARK: - perform
    private(set) var action: RegisterWhatsAppNumberAction?

    func perform(action: RegisterWhatsAppNumberAction) {
        self.action = action
    }
}

final class RegisterWhatsAppNumberPresenterTests: XCTestCase {
    private let displaySpy = RegisterWhatsAppNumberDisplayingSpy()
    private let coordinatorSpy = RegisterWhatsAppNumberCoordinatorSpy()
    private let dependenciesMock = DependencyContainerMock()

    private lazy var sut: RegisterWhatsAppNumberPresenting = {
        let presenter = RegisterWhatsAppNumberPresenter(coordinator: coordinatorSpy, dependencies: dependenciesMock)
        presenter.viewController = displaySpy

        return presenter
    }()

    func testPresentLink_WhenCalled_ShouldDisplayLinkText() {
        sut.presentLink()

        XCTAssertEqual(displaySpy.displayLinkTextLinkTextLinkAttributesCallsCount, 1)
        XCTAssertTrue(displaySpy.displayLinkTextLinkTextLinkAttributesReceivedInvocations.isNotEmpty)
    }

    func testOpen_WhenUserTap_ThenCallPresenter() throws {
        let url = try XCTUnwrap(URL(string: "https://www.google.com"))

        sut.open(url: url)

        XCTAssertEqual(coordinatorSpy.action, .open(url: url))
    }

    func testSetRules_WhenConfigRules_ShouldSetRules() {
        let rules = ValidationRuleSet<String>()
        
        sut.prepareRules(rules)

        XCTAssertEqual(displaySpy.setRulesCallsCount, 1)
        XCTAssertTrue(displaySpy.setRulesReceivedInvocations.isNotEmpty)
    }

    func testUpdateTextFieldState_WhenSuccessCase_ShouldUpdateTextFieldState() {
        sut.updateTextFieldState("")

        XCTAssertEqual(displaySpy.textFieldSuccessCallsCount, 1)
        XCTAssertEqual(displaySpy.enableSaveButtonEnableCallsCount, 1)
        XCTAssertTrue(displaySpy.enableSaveButtonIsEnable)
    }

    func testUpdateTextFieldState_WhenErrorCase_ShouldUpdateTextFieldState() {
        let message = "Error Message"
        sut.updateTextFieldState(message)

        XCTAssertEqual(displaySpy.textFieldErrorMessageCallsCount, 1)
        XCTAssertEqual(displaySpy.textFieldErrorMessageReceived, message)
        XCTAssertEqual(displaySpy.enableSaveButtonEnableCallsCount, 1)
        XCTAssertFalse(displaySpy.enableSaveButtonIsEnable)
    }

    func testDidNextStep_WhenOpenURL_ShouldOpenURL() throws {
        let url = try XCTUnwrap(URL(string: "https://www.google.com"))

        sut.didNextStep(action: .open(url: url))

        XCTAssertEqual(coordinatorSpy.action, .open(url: url))
    }

    func testPresentError_WhenErrorHappens_ShouldDisplayError() {
        sut.presentError()

        XCTAssertEqual(displaySpy.showErrorCallsCount, 1)
    }

    func testShouldUpdateTextFieldAndSwitch_WhenSuccessRequest_ShouldUpdateTexteFieldAndSwitch() {
        let whatsapp = WhatsAppAccount(phoneNumber: "(11) 92345-1234", active: true)

        sut.shouldUpdateTextFieldAndSwitch(whatsapp)

        XCTAssertEqual(displaySpy.updateTextFieldAndSwitchCallsCount, 1)
        XCTAssertEqual(displaySpy.updateTextFieldAndSwitchReceived, whatsapp)
    }
}
