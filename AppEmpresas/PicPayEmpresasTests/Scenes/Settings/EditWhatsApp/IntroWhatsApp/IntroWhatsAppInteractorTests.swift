import XCTest
import FeatureFlag
@testable import PicPayEmpresas

private final class IntroWhatsAppPresenterSpy: IntroWhatsAppPresenting {
    weak var viewController: IntroWhatsAppDisplaying?

    private(set) var presentErrorCalled = 0
    func presentError() {
        presentErrorCalled += 1
    }

    private(set) var openCalled = 0
    private(set) var openUrlReceived = URL(string: "")
    func open(url: URL) {
        openCalled += 1
        openUrlReceived = url
    }

    private(set) var nextStepCalled = 0
    func didNextStep(action: IntroWhatsAppAction) {
        nextStepCalled += 1
    }
}

final class IntroWhatsAppInteractorTests: XCTestCase {
    private lazy var presenterSpy = IntroWhatsAppPresenterSpy()
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var containerMock = DependencyContainerMock(featureManagerMock)

    private lazy var sut: IntroWhatsAppInteracting = {
        let interactor = IntroWhatsAppInteractor(presenter: presenterSpy, dependencies: containerMock)
        return interactor
    }()

    func testButtonWhatsAppTapped_WhenUserTap_ShouldGoToNextStep() {
        sut.buttonWhatsAppTapped()

        XCTAssertEqual(presenterSpy.nextStepCalled, 1)
    }

    func testButtonHowWorksTapped_WhenSuccessCase_ShouldOpenWebView() throws {
        let url = try XCTUnwrap(URL(string: "https://google.com"))
        featureManagerMock.override(key: .urlWhatsAppExplanationWebview, with: url.absoluteString)

        sut.buttonHowWorksTapped()

        XCTAssertEqual(presenterSpy.openCalled, 1)
        XCTAssertEqual(presenterSpy.openUrlReceived, url)
    }

    func testButtonHowWorksTapped_WhenFailureCase_ShouldOpenWebView() throws {
        featureManagerMock.override(key: .urlWhatsAppExplanationWebview, with: "")

        sut.buttonHowWorksTapped()

        XCTAssertEqual(presenterSpy.presentErrorCalled, 1)
    }
}
