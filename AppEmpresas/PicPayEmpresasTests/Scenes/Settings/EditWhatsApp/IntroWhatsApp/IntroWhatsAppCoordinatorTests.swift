import XCTest
@testable import PicPayEmpresas

final class IntroWhatsAppCoordinatorTests: XCTestCase {
    private lazy var viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)

    private lazy var sut: IntroWhatsAppCoordinating = {
        let coordinator = IntroWhatsAppCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()

    func testPerform_WhenButtonWhatsAppTapped_ShouldCallRegisterWhatsAppNumberController() {
        sut.perform(action: .buttonWhatsApp)

        XCTAssertEqual(navigationSpy.pushedCount, 2)
        XCTAssertTrue(navigationSpy.currentViewController is RegisterWhatsAppNumberViewController)
    }

    func testPerform_WhenOpenURL_ShouldOpenWebViewController() throws {
        let url = try XCTUnwrap(URL(string: "https://google.com"))

        sut.perform(action: .open(url: url))

        let viewController = navigationSpy.currentViewController as? WebViewController
        let webView = try XCTUnwrap(viewController)

        XCTAssertTrue(navigationSpy.currentViewController is WebViewController)
        XCTAssertEqual(webView.url, url.absoluteString)
    }
}
