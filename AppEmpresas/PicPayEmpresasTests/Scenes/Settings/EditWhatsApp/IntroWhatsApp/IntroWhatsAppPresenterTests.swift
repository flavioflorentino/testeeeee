import XCTest
@testable import PicPayEmpresas

private final class IntroWhatsAppCoordinatorSpy: IntroWhatsAppCoordinating {
    weak var viewController: UIViewController?
    private(set) var action: IntroWhatsAppAction?

    func perform(action: IntroWhatsAppAction) {
        self.action = action
    }
}

final class IntroWhatsAppPresenterTests: XCTestCase {
    private let coordinatorSpy = IntroWhatsAppCoordinatorSpy()

    private lazy var sut: IntroWhatsAppPresenting = {
        let presenter = IntroWhatsAppPresenter(coordinator: coordinatorSpy)
        return presenter
    }()

    func testDidNextStep_WhenButtonWhatsAppTapped_ShouldCallNextView() {
        sut.didNextStep(action: .buttonWhatsApp)

        XCTAssertEqual(coordinatorSpy.action, .buttonWhatsApp)
    }

    func testDidNextStep_WhenButtonHowWorksTapped_ShouldCallWebView() throws {
        let url = try XCTUnwrap(URL(string: "https://google.com"))

        sut.didNextStep(action: .open(url: url))

        XCTAssertEqual(coordinatorSpy.action, .open(url: url))
    }
}
