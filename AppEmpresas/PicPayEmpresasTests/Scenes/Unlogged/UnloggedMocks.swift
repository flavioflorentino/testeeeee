import Foundation
import XCTest
import AuthenticationPJ

enum UnloggedMocks {
    static func authResponseCompleted() throws -> AuthResponse {
        let authJson = """
                {
                 "data": {
                  "accessToken": "1234567890",
                  "tokenType": "tokenType",
                  "expires_in": 123,
                  "refreshToken": "refreshToken",
                  "biometry": "denied",
                  "completed": true
                 }
                }
            """

        let authData = try XCTUnwrap(authJson.data(using: .utf8))
        return try JSONDecoder(.convertFromSnakeCase).decode(AuthResponse.self, from: authData)
    }

    static func authResponseNotCompleted() throws -> AuthResponse {
        let authJson = """
                {
                 "data": {
                  "accessToken": "1234567890",
                  "tokenType": "tokenType",
                  "expires_in": 123,
                  "refreshToken": "refreshToken",
                  "biometry": "denied",
                  "completed": false
                 }
                }
            """

        let authData = try XCTUnwrap(authJson.data(using: .utf8))
        return try JSONDecoder(.convertFromSnakeCase).decode(AuthResponse.self, from: authData)
    }

    static func userResponse() throws -> UserResponse {
        let userJson = """
                {
                 "data": {
                  "id": 1,
                  "cpf": "37.442.929/0001-80",
                  "headOfficeId": 1,
                  "imgUrl": "https://images1.fanpop.com/images/image_uploads/Naruto-Sad-uzumaki-naruto-987855_620_465.jpg",
                  "mobilePhone": "11999999999",
                  "birthDateFormated": "31/03/1998",
                  "username": "username",
                  "super": 1,
                  "role": 1,
                  "email": "teste@picpay.com",
                  "name": "Uzumaki Naruto"
                 }
                }
            """

        let userData = try XCTUnwrap(userJson.data(using: .utf8))
        return try JSONDecoder(.convertFromSnakeCase).decode(UserResponse.self, from: userData)
    }
}
