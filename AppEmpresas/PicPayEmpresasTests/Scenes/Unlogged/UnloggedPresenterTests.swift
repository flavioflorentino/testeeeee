import XCTest
@testable import PicPayEmpresas

private final class UnloggedPresenterTests: XCTestCase {
    let coordiantor = MockUnloggedCoordinator()
    var sut: UnloggedPresenter?
    
    override func setUp() {
        sut = UnloggedPresenter(coordinator: coordiantor)
    }

    override func tearDown() {
        sut = nil
    }

    func testDidNextStepRegister() {
        sut?.didNextStep(action: .welcome)
        XCTAssertEqual(coordiantor.currentAction, .welcome)
    }
    
    func testDidNextStepLogin() {
        sut?.didNextStep(action: .login(true))
        XCTAssertEqual(coordiantor.currentAction, .login(true))
    }
    
    func testDidNextStepHelp() {
        sut?.didNextStep(action: .help)
        XCTAssertEqual(coordiantor.currentAction, .help)
    }
}

private final class MockUnloggedCoordinator: UnloggedCoordinating {
    var viewController: UIViewController?
    var currentAction: UnloggedAction?
    
    func perform(action: UnloggedAction) {
        currentAction = action
    }
}
