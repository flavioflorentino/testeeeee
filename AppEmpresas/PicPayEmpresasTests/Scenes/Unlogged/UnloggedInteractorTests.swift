import XCTest
import FeatureFlag
import Core
import LegacyPJ
@testable import PicPayEmpresas

private final class UnloggedInteractorTests: XCTestCase {
    let service = UnloggedService(authManager: AuthManager.shared)
    let presenter = MockUnloggedPresenter()
    
    let featureManagerMock = FeatureManagerMock()
    let authManagerMock = AuthManagerMock()
    let analyticsMock = AnalyticsMock()
    let keychainMock = KeychainManagerMock()
    
    lazy var dependenciesMock = DependencyContainerMock(featureManagerMock, authManagerMock, analyticsMock, keychainMock)
    lazy var sut: UnloggedInteractor = UnloggedInteractor(service: service, presenter: presenter, dependencies: dependenciesMock, isRoot: false)
    lazy var sutRoot: UnloggedInteractor = UnloggedInteractor(service: service, presenter: presenter, dependencies: dependenciesMock, isRoot: true)

    func testShowWelcome() {
        featureManagerMock.override(keys: .featureRegistrationIntroPresenting, with: true)
        sut.showNextScreen()
        XCTAssertEqual(presenter.currentAction, .welcome)
    }
    
    func testShowRegisterAuthenticated() throws {
        authManagerMock.isAuthenticatedUser = true
        featureManagerMock.override(keys: .featureRegistrationIntroPresenting, with: false)
        sut.showNextScreen()
        
        let logEvent = try XCTUnwrap(analyticsMock.logReceivedInvocations.last)
        XCTAssertEqual(presenter.currentAction, .register)
        XCTAssertEqual(analyticsMock.logCallsCount, 2)
        XCTAssertEqual(logEvent.event().name, NewRegistrationAnalytics.obMultRegister.rawValue)
    }
    
    func testShowRegisterNotAuthenticated() throws {
        authManagerMock.isAuthenticatedUser = false
        featureManagerMock.override(keys: .featureRegistrationIntroPresenting, with: false)
        sut.showNextScreen()
        
        let logEvent = try XCTUnwrap(analyticsMock.logReceivedInvocations.last)
        XCTAssertEqual(presenter.currentAction, .register)
        XCTAssertEqual(analyticsMock.logCallsCount, 2)
        XCTAssertEqual(logEvent.event().name, NewRegistrationAnalytics.obRegister.rawValue)
    }
    
    func testShowLogin_WhenReceiveShowLoginFromViewController_ShouldDisplayLoginWithoutRoot() {
        sut.showLogin()
        XCTAssertEqual(presenter.currentAction, .login(false))
    }

    func testShowLogin_WhenReceiveShowLoginFromViewController_ShouldDisplayLoginWithRoot() {
        sutRoot.showLogin()
        XCTAssertEqual(presenter.currentAction, .login(true))
    }
    
    func testShowHelp() {
        sut.showHelp()
        XCTAssertEqual(presenter.currentAction, .help)
    }
    
    func testIsHasRegisterProgress_WhenNoHaveProgressAndReceiveActionFromViewController_ShouldCallRegisterProgressCount() {
        dependenciesMock.keychain.clearValue(key: KeychainKeyPJ.registerStatus)
        sut.checkRegisterProgress()
        XCTAssertEqual(presenter.registerProgressCount, 0)
    }
    
    func testIsHasRegisterProgress_WhenHaveProgressAndReceiveActionFromViewController_ShouldCallRegisterProgressCount() {
        let status = RegistrationStatus(completed: false, step: "person", sequence: ["person", "phone"], name: "Nome Teste")
        dependenciesMock.keychain.set(key: KeychainKeyPJ.registerStatus, value: status)
        
        sut.checkRegisterProgress()
        XCTAssertEqual(presenter.registerProgressCount, 1)
    }

    func testCheckAuthentication_WhenReceiveCheckAuthenticationFromViewController_ShouldDoNothing() {
        sut.checkAuthentication()
        XCTAssertNil(presenter.currentAction)
        XCTAssertEqual(presenter.actionCallsCount, 0)
    }

    func testCheckAuthentication_WhenReceiveCheckAuthenticationFromViewController_ShouldCheckAuthentication() {
        sutRoot.checkAuthentication()
        XCTAssertEqual(presenter.currentAction, .start)
        XCTAssertEqual(presenter.actionCallsCount, 1)
    }
}

private final class MockUnloggedPresenter: UnloggedPresenting {
    var viewController: UnloggedDisplay?
    var currentAction: UnloggedAction?
    var registerProgressCount = 0
    private(set) var actionCallsCount = 0
    
    func didNextStep(action: UnloggedAction) {
        currentAction = action
        actionCallsCount += 1
    }
    
    func configureViewKindOfContinueRegister(model: RegistrationStatus) {
        registerProgressCount += 1
    }
}
