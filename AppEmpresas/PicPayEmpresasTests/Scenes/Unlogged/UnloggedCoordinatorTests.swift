import XCTest
import FeatureFlag
@testable import AuthenticationPJ
@testable import PicPayEmpresas

final class UnloggedCoordinatorTests: XCTestCase {
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var appCoordinatorMock = AppCoordinatorMock()
    private lazy var controller = ViewControllerSpy()
    private lazy var navigation = NavigationControllerSpy(rootViewController: controller)
    private lazy var dependencies = DependencyContainerMock(featureManagerMock, appCoordinatorMock)
    private lazy var sut: UnloggedCoordinator = {
        let cooordinator = UnloggedCoordinator(dependencies: dependencies)
        cooordinator.viewController = navigation.topViewController
        return cooordinator
    }()
    private lazy var sutControllerNil: UnloggedCoordinator = {
        let cooordinator = UnloggedCoordinator(dependencies: dependencies)
        return cooordinator
    }()

    func testPerform_WhenReceiveActionFromPresenter_ShouldPushRegistrationPersonalInfoViewController() {
        sut.perform(action: .register)

        XCTAssertEqual(navigation.pushedCount, 2)
        XCTAssertTrue(navigation.currentViewController is RegistrationPersonalInfoViewController)
    }

    func testPerform_WhenReceiveActionFromPresenter_ShouldPushWelcomeViewController() {
        sut.perform(action: .welcome)

        XCTAssertEqual(navigation.pushedCount, 2)
        XCTAssertTrue(navigation.currentViewController is WelcomeViewController)
    }
    
    func testLoginPerform() {
        sut.perform(action: .login(true))

        XCTAssertEqual(navigation.pushedCount, 2)
        XCTAssertTrue(navigation.currentViewController is LoginCnpjViewController)
    }

    func testPerform_WhenReceiveActionFromPresenter_ShouldCallAuthenticationPJ() {
        featureManagerMock.override(key: .isFastAccessAvailable, with: true)
        sut.perform(action: .login(true))

        XCTAssertTrue(navigation.currentViewController is FirstAccessViewController)
    }

    func testPerform_WhenReceiveActionFromPresenter_ShouldDoNothing() {
        featureManagerMock.override(key: .isFastAccessAvailable, with: false)
        sut.perform(action: .start)

        XCTAssertTrue(navigation.currentViewController is ViewControllerSpy)
    }

    func testPerform_WhenReceiveActionFromPresenter_ShouldStartAuthenticationPJ() {
        featureManagerMock.override(key: .isFastAccessAvailable, with: true)
        sut.perform(action: .start)

        XCTAssertTrue(navigation.currentViewController is ViewControllerSpy)
        XCTAssertEqual(navigation.popToRoot, 1)
    }

    func testPerform_WhenReceiveActionFromPresenter_ShouldCallHelpAndDoNothing() {
        sutControllerNil.perform(action: .help)

        XCTAssertTrue(navigation.currentViewController is ViewControllerSpy)
        XCTAssertEqual(controller.presentViewControllerCounter, 0)
    }

    func testPerform_WhenReceiveActionFromPresenter_ShouldCallHelpAndPresent() {
        sut.perform(action: .help)

        XCTAssertTrue(navigation.currentViewController is ViewControllerSpy)
        XCTAssertEqual(controller.presentViewControllerCounter, 1)
    }

    func testAuthenticationWasCompleted_ShouldDoNothing() {
        let userAuthResponse = UserAuthResponse()
        sut.authenticationWasCompleted(navigation, userAuthResponse: userAuthResponse)

        XCTAssertEqual(appCoordinatorMock.displayAuthenticatedCallsCount, 0)
    }

    func testAuthenticationWasCompleted_ShouldDisplayCompleteDialog() throws {
        let userAuthResponse = UserAuthResponse()
        userAuthResponse.auth = try UnloggedMocks.authResponseNotCompleted()
        userAuthResponse.user = try UnloggedMocks.userResponse()
        sut.authenticationWasCompleted(navigation, userAuthResponse: userAuthResponse)

        XCTAssertEqual(appCoordinatorMock.displayAuthenticatedCallsCount, 0)
    }

    func testAuthenticationWasCompleted_ShouldDisplayAuthentication() throws {
        let userAuthResponse = UserAuthResponse()
        userAuthResponse.auth = try UnloggedMocks.authResponseCompleted()
        userAuthResponse.user = try UnloggedMocks.userResponse()
        sut.authenticationWasCompleted(navigation, userAuthResponse: userAuthResponse)

        XCTAssertEqual(appCoordinatorMock.displayAuthenticatedCallsCount, 1)
    }

    func testAuthenticationDidTapFAQ_ShouldDisplayFAQ() {
        sut.authenticationDidTapFAQ(navigation)

        XCTAssertTrue(navigation.currentViewController is ViewControllerSpy)
        XCTAssertEqual(navigation.presentCount, 1)
    }

    func testAuthenticationDidTapForgotPassword_ShouldDisplayForgotPassword() {
        sut.authenticationDidTapForgotPassword(navigation)

        XCTAssertEqual(navigation.pushedCount, 2)
        XCTAssertTrue(navigation.currentViewController is WebViewController)
    }

    func testAuthenticationDidTapRegister_ShouldDisplayRegister() {
        sut.authenticationDidTapRegister(navigation)

        XCTAssertEqual(navigation.pushedCount, 2)
        XCTAssertTrue(navigation.currentViewController is RegistrationPersonalInfoViewController)
    }
}
