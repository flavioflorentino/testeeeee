import Core
import FeatureFlag
import XCTest
@testable import PicPayEmpresas

final private class WalletBalanceCoordinatorTests: XCTestCase {
    private let balanceItemMock = WalletBalanceItemMock()
    
    private lazy var viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    private lazy var dependencies = DependencyContainerMock()
    
    private lazy var sut: WalletBalanceCoordinating = {
        let coordinator = WalletBalanceCoordinator(dependencies: dependencies)
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenNewWithdrawalAndHasAutomaticWithdrawalDisabled_ShouldPushWithdrawalOptionsViewController() throws {
        let balance = try XCTUnwrap(balanceItemMock.balanceItem())
        sut.perform(action: .newWithdraw(balance))
        XCTAssertTrue(navigationSpy.currentViewController is WithdrawalOptionsViewController)
    }
    
    func testPerform_WhenNewWithdrawalAndHasBankError_ShouldPushWithdrawalOptionsViewController() throws {
        let balance = try XCTUnwrap(balanceItemMock.balanceItem())
        let bankError = WithdrawalOptionsMock().getBankError()
        sut.perform(action: .newWithdraw(balance, bankError: bankError))
        XCTAssertTrue(navigationSpy.currentViewController is WithdrawalOptionsViewController)
    }
}
