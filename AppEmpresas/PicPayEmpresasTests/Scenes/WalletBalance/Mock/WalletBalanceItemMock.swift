import XCTest
@testable import PicPayEmpresas

final class WalletBalanceItemMock {
    func balanceItem() -> WalletBalanceItem? {
        return LocalMock<WalletBalanceItem>().localMock("balance")
    }
    
    func balanceNotBlockedItem() -> WalletBalanceItem? {
        return LocalMock<WalletBalanceItem>().localMock("balance_not_blocked")
    }
}
