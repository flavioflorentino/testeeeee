import XCTest
@testable import PicPayEmpresas

final class WalletBalancePresenterTests: XCTestCase {
    private let balanceItemMock = WalletBalanceItemMock()
    
    private lazy var dependencies = DependencyContainerMock()
    private lazy var coordinator = WalletBalanceCoordinator(dependencies: dependencies)
    private lazy var viewController = WalletBalanceDisplaySpy()
    
    private lazy var sut: WalletBalancePresenting = {
        let sut = WalletBalancePresenter(coordinator: coordinator)
        sut.viewController = viewController
        return sut
    }()
    
    func testPresentBalance_WhenSuccess_ShouldDisplayBalance() throws {
        let balance = try XCTUnwrap(balanceItemMock.balanceItem())
        sut.presentBalance(balance)
        
        XCTAssertEqual(viewController.displayBalanceCount, 1)
        XCTAssertEqual(viewController.balance, "\(balance.available)".currency(hideSymbol: true))
        XCTAssertEqual(viewController.withheld, "\(balance.withheld)".currency(hideSymbol: true))
    }
    
    func testPresentBalance_WhenSuccessAndJudicialBlockadeEnabled_ShouldDisplayBlockedBalance() throws {
        let balance = try XCTUnwrap(balanceItemMock.balanceItem())
        sut.presentBalance(balance)
        
        let blockedBalance = try XCTUnwrap(balance.blocked)
        XCTAssertEqual(viewController.displayBlockedBalanceCount, 1)
        XCTAssertEqual(viewController.blocked, "\(blockedBalance)".currency(hideSymbol: true))
    }
    
    func testPresentBalance_WhenSuccess_ShouldEnableWithdrawalButton() throws {
        let balance = try XCTUnwrap(balanceItemMock.balanceItem())
        sut.presentBalance(balance)
        
        XCTAssertEqual(viewController.newWithdrawViewCount, 1)
        XCTAssertEqual(viewController.newWithdrawViewEnabled, true)
    }
    
    func testPresentCurrency_ShouldCallSetupCurrency() {
        let currency = "R$"
        sut.presentCurrency(currency)
        
        XCTAssertEqual(viewController.setupCurrencyCallsCount, 1)
        XCTAssertEqual(viewController.setupCurrencySymbol, currency)
    }
    
    func testWithdrawValues_ShouldCallSetupVisibility() {
        let isHidden = true
        let animated = false
        sut.withdrawValues(isHidden: isHidden, animated: animated)
        
        XCTAssertEqual(viewController.setupVisibilityCount, 1)
        XCTAssertEqual(viewController.setupVisibilityHidden, isHidden)
        XCTAssertEqual(viewController.setupVisibilityAnimated, animated)
    }
    
    func testShowError_ShouldDisplayError() {
        sut.showError()
        
        XCTAssertEqual(viewController.displayErrorCount, 1)
        XCTAssertEqual(viewController.newWithdrawViewCount, 1)
        XCTAssertEqual(viewController.newWithdrawViewEnabled, false)
    }
}

private final class WalletBalanceDisplaySpy: WalletBalanceDisplay {
    //MARK: - setupCurrency
    private(set) var setupCurrencyCallsCount = 0
    private(set) var setupCurrencySymbol: String?

    func setupCurrency(_ symbol: String?) {
        setupCurrencyCallsCount += 1
        setupCurrencySymbol = symbol
    }

    //MARK: - setupVisibility
    private(set) var setupVisibilityCount = 0
    private(set) var setupVisibilityHidden: Bool?
    private(set) var setupVisibilityAnimated: Bool?
    
    func setupVisibility(_ hidden: Bool, animated: Bool) {
        setupVisibilityCount += 1
        setupVisibilityHidden = hidden
        setupVisibilityAnimated = animated
    }

    //MARK: - display
    private(set) var displayBalanceCount = 0
    private(set) var balance: String?
    private(set) var withheld: String?
    private(set) var displayBlockedBalanceCount = 0
    private(set) var blocked: String?
    private(set) var displayErrorCount = 0

    func display(_ balance: String, withheld: String) {
        displayBalanceCount += 1
        self.withheld = withheld
        self.balance = balance
    }
    
    func displayBlockedBalance(_ blockedBalance: String) {
        displayBlockedBalanceCount += 1
        self.blocked = blockedBalance
    }
    
    func displayErrorView() {
        displayErrorCount += 1
    }

    //MARK: - withdrawButton
    private(set) var newWithdrawViewCount = 0
    private(set) var newWithdrawViewEnabled: Bool?

    func newWithdrawalView(enabled: Bool) {
        newWithdrawViewCount += 1
        newWithdrawViewEnabled = enabled
    }
}
