import XCTest
import Core
@testable import PicPayEmpresas

private final class WalletBalanceServiceMock: WalletBalanceServicing {
    var userBalanceCallback: Result<WalletBalanceItem?, ApiError> = {
        let balanceItem = WalletBalanceItemMock()
        return Result.success(balanceItem.balanceItem())
    }()
    
    func userBalance(_ completion: @escaping (Result<WalletBalanceItem?, ApiError>) -> Void) {
        completion(userBalanceCallback)
    }
}

private final class WalletBalancePresentingSpy: WalletBalancePresenting {
    var viewController: WalletBalanceDisplay?

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepAction: WalletBalanceAction?

    func didNextStep(action: WalletBalanceAction) {
        didNextStepActionCallsCount += 1
        didNextStepAction = action
    }

    //MARK: - presentBalance
    private(set) var presentBalanceCallsCount = 0
    private(set) var presentBalanceReceivedInvocations: [WalletBalanceItem] = []

    func presentBalance(_ balance: WalletBalanceItem) {
        presentBalanceCallsCount += 1
        presentBalanceReceivedInvocations.append(balance)
    }

    //MARK: - presentCurrency
    private(set) var presentCurrencyCallsCount = 0
    private(set) var presentCurrencyReceivedInvocations: [String?] = []

    func presentCurrency(_ symbol: String?) {
        presentCurrencyCallsCount += 1
        presentCurrencyReceivedInvocations.append(symbol)
    }

    //MARK: - withdrawValues
    private(set) var withdrawValuesShowAnimatedCallsCount = 0
    private(set) var withdrawValuesShowAnimatedReceivedInvocations: [(isHidden: Bool, animated: Bool)] = []

    func withdrawValues(isHidden: Bool, animated: Bool) {
        withdrawValuesShowAnimatedCallsCount += 1
        withdrawValuesShowAnimatedReceivedInvocations.append((isHidden: isHidden, animated: animated))
    }
    
    // MARK: - Show Error
    private(set) var showErrorCount = 0
    
    func showError() {
        showErrorCount += 1
    }
}


final class WalletBalanceViewModelTests: XCTestCase {
    private let service = WalletBalanceServiceMock()
    private let presenter = WalletBalancePresentingSpy()
    private let kvStore = KVStoreMock()
    
    private lazy var container = DependencyContainerMock(kvStore)
    
    private lazy var sut: WalletBalanceViewModel? = {
        WalletBalanceViewModel(service: service, presenter: presenter, dependencies: container)
    }()
    
    func testGetWalletBalance_WhenSuccess_ShouldCallPresentBalance() {
        sut?.getBalance()
        XCTAssertEqual(presenter.presentBalanceCallsCount, 1)
    }

    func testSetupView_WhenSuccess_ShouldCallWithdrawValuesAndPresentCurrency() {
        sut?.setupView()
        XCTAssertEqual(presenter.withdrawValuesShowAnimatedCallsCount, 1)
        XCTAssertEqual(presenter.presentCurrencyCallsCount, 1)
    }
    
    func testNewWithdrawal_ShouldCallNewWithdrawalAction() throws {
        let balance = try XCTUnwrap(WalletBalanceItemMock().balanceItem())
        sut = WalletBalanceViewModel(service: service, presenter: presenter, dependencies: container, balance: balance)
        sut?.newWithdraw()
        XCTAssertEqual(presenter.didNextStepAction, .newWithdraw(balance))
        XCTAssertEqual(presenter.didNextStepActionCallsCount, 1)
    }
    
    func testChangeVisibility_WhenTrue_ShouldCallWithdrawValuesWithFalse() {
        testChangeVisibility(isHidden: true)
    }
    
    func testChangeVisibility_WhenFalse_ShouldCallWithdrawValuesWithTrue() {
        testChangeVisibility(isHidden: false)
    }
    
    func testGetBalance_WhenError_ShouldDisplayErrorView() {
        service.userBalanceCallback = .failure(ApiError.timeout)
        sut?.getBalance()
        
        XCTAssertEqual(presenter.showErrorCount, 1)
    }
}

private extension WalletBalanceViewModelTests {
    func testChangeVisibility(isHidden: Bool) {
        kvStore.set(value: isHidden, with: BizKvKey.isBalanceHidden)
        sut?.changeVisibility()
        
        let withdrawValues = presenter.withdrawValuesShowAnimatedReceivedInvocations.first
        XCTAssertEqual(presenter.withdrawValuesShowAnimatedCallsCount, 1)
        XCTAssertEqual(presenter.withdrawValuesShowAnimatedReceivedInvocations.count, 1)
        XCTAssertEqual(withdrawValues?.isHidden, !isHidden)
        XCTAssertEqual(withdrawValues?.animated, true)
        
        
        let savedIsHidden = kvStore.boolFor(BizKvKey.isBalanceHidden)
        XCTAssertEqual(savedIsHidden, !isHidden)
    }
}
