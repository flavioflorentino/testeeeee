import XCTest
import Core
import LegacyPJ
@testable import PicPayEmpresas

private class AddressCardDisplaySpy: AddressCardDisplay {
    //MARK: - loadAddressByPostalCode
    private(set) var loadAddressByPostalCodeAddressCallsCount = 0
    private(set) var loadAddressByPostalCodeAddressReceivedInvocations: [Address] = []

    func loadAddressByPostalCode(address: Address) {
        loadAddressByPostalCodeAddressCallsCount += 1
        loadAddressByPostalCodeAddressReceivedInvocations.append(address)
    }

    //MARK: - startLoading
    private(set) var startLoadingCallsCount = 0

    func startLoading() {
        startLoadingCallsCount += 1
    }

    //MARK: - stopLoading
    private(set) var stopLoadingCallsCount = 0

    func stopLoading() {
        stopLoadingCallsCount += 1
    }

    //MARK: - dismissKeyboard
    private(set) var dismissKeyboardCallsCount = 0

    func dismissKeyboard() {
        dismissKeyboardCallsCount += 1
    }

    //MARK: - setup
    private(set) var setupAddressCallsCount = 0
    private(set) var setupAddressReceivedInvocations: [Address] = []

    func setup(address: Address) {
        setupAddressCallsCount += 1
        setupAddressReceivedInvocations.append(address)
    }

    //MARK: - setPostalCode
    private(set) var setPostalCodeCallsCount = 0
    private(set) var setPostalCodeReceivedInvocations: [String?] = []

    func setPostalCode(_ postalCode: String?) {
        setPostalCodeCallsCount += 1
        setPostalCodeReceivedInvocations.append(postalCode)
    }

    //MARK: - setFields
    private(set) var setFieldsWithCallsCount = 0
    private(set) var setFieldsWithReceivedInvocations: [Address] = []

    func setFields(with address: Address) {
        setFieldsWithCallsCount += 1
        setFieldsWithReceivedInvocations.append(address)
    }

    //MARK: - updateLayoutPostalCodeField
    private(set) var updateLayoutPostalCodeFieldIsValidMessageCallsCount = 0
    private(set) var updateLayoutPostalCodeFieldIsValidMessageReceivedInvocations: [(isValid: Bool, message: String)] = []

    func updateLayoutPostalCodeField(isValid: Bool, message: String) {
        updateLayoutPostalCodeFieldIsValidMessageCallsCount += 1
        updateLayoutPostalCodeFieldIsValidMessageReceivedInvocations.append((isValid: isValid, message: message))
    }

    //MARK: - updateLayoutAddressField
    private(set) var updateLayoutAddressFieldIsValidMessageCallsCount = 0
    private(set) var updateLayoutAddressFieldIsValidMessageReceivedInvocations: [(isValid: Bool, message: String)] = []

    func updateLayoutAddressField(isValid: Bool, message: String) {
        updateLayoutAddressFieldIsValidMessageCallsCount += 1
        updateLayoutAddressFieldIsValidMessageReceivedInvocations.append((isValid: isValid, message: message))
    }

    //MARK: - updateLayoutComplementField
    private(set) var updateLayoutComplementFieldIsValidMessageCallsCount = 0
    private(set) var updateLayoutComplementFieldIsValidMessageReceivedInvocations: [(isValid: Bool, message: String)] = []

    func updateLayoutComplementField(isValid: Bool, message: String) {
        updateLayoutComplementFieldIsValidMessageCallsCount += 1
        updateLayoutComplementFieldIsValidMessageReceivedInvocations.append((isValid: isValid, message: message))
    }

    //MARK: - updateLayoutNumberField
    private(set) var updateLayoutNumberFieldIsValidMessageCallsCount = 0
    private(set) var updateLayoutNumberFieldIsValidMessageReceivedInvocations: [(isValid: Bool, message: String)] = []

    func updateLayoutNumberField(isValid: Bool, message: String) {
        updateLayoutNumberFieldIsValidMessageCallsCount += 1
        updateLayoutNumberFieldIsValidMessageReceivedInvocations.append((isValid: isValid, message: message))
    }

    //MARK: - updateLayoutDistrictField
    private(set) var updateLayoutDistrictFieldIsValidMessageCallsCount = 0
    private(set) var updateLayoutDistrictFieldIsValidMessageReceivedInvocations: [(isValid: Bool, message: String)] = []

    func updateLayoutDistrictField(isValid: Bool, message: String) {
        updateLayoutDistrictFieldIsValidMessageCallsCount += 1
        updateLayoutDistrictFieldIsValidMessageReceivedInvocations.append((isValid: isValid, message: message))
    }

    //MARK: - updateLayoutCityField
    private(set) var updateLayoutCityFieldIsValidMessageCallsCount = 0
    private(set) var updateLayoutCityFieldIsValidMessageReceivedInvocations: [(isValid: Bool, message: String)] = []

    func updateLayoutCityField(isValid: Bool, message: String) {
        updateLayoutCityFieldIsValidMessageCallsCount += 1
        updateLayoutCityFieldIsValidMessageReceivedInvocations.append((isValid: isValid, message: message))
    }

    //MARK: - updateLayoutStateField
    private(set) var updateLayoutStateFieldIsValidMessageCallsCount = 0
    private(set) var updateLayoutStateFieldIsValidMessageReceivedInvocations: [(isValid: Bool, message: String)] = []

    func updateLayoutStateField(isValid: Bool, message: String) {
        updateLayoutStateFieldIsValidMessageCallsCount += 1
        updateLayoutStateFieldIsValidMessageReceivedInvocations.append((isValid: isValid, message: message))
    }

    //MARK: - confirmButtonDidTap
    private(set) var confirmButtonDidTapCallsCount = 0

    func confirmButtonDidTap() {
        confirmButtonDidTapCallsCount += 1
    }
}

final class AddressCardPresenterTests: XCTestCase {
    private let viewControllerSpy = AddressCardDisplaySpy()
    
    private lazy var sut: AddressCardPresenter = {
        let presenter = AddressCardPresenter()
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testLoadAddressByPostalCode_ShouldLoadAddress() {
        sut.loadAddressByPostalCode(address: Address())
        
        XCTAssertEqual(viewControllerSpy.loadAddressByPostalCodeAddressCallsCount, 1)
        XCTAssertNotNil(viewControllerSpy.loadAddressByPostalCodeAddressReceivedInvocations)
    }
    
    func testSetup_ShouldUpdateAddressWithPostalCodeChange() {
        var address = Address()
        address.cep = "05317-020"
        sut.setup(address: address)
        
        XCTAssertEqual(viewControllerSpy.setPostalCodeCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.setFieldsWithCallsCount, 1)
        XCTAssertTrue(viewControllerSpy.setPostalCodeReceivedInvocations.contains("05317-020"))
        XCTAssertEqual(viewControllerSpy.setFieldsWithCallsCount, 1)
    }
    
    func testSetup_ShouldUpdateAddressWithoutPostalCodeChange() {
        sut.setup(address: Address())
        
        XCTAssertEqual(viewControllerSpy.setPostalCodeCallsCount, 0)
        XCTAssertEqual(viewControllerSpy.setFieldsWithCallsCount, 1)
        XCTAssertNotNil(viewControllerSpy.setPostalCodeReceivedInvocations)
        XCTAssertNotNil(viewControllerSpy.setFieldsWithReceivedInvocations)
    }
    
    func testUpdateLayoutPostalCodeField_WhenValidPostalCode_ShouldUpdateFieldStatus() {
        sut.updateLayoutPostalCodeField(isValid: true, message: "teste")
        
        XCTAssertEqual(viewControllerSpy.updateLayoutPostalCodeFieldIsValidMessageCallsCount, 1)
        XCTAssertNotNil(viewControllerSpy.updateLayoutPostalCodeFieldIsValidMessageReceivedInvocations)
    }
    
    func testUpdateLayoutAddressField_WhenValidAddress_ShouldUpdateFieldStatus() {
        sut.updateLayoutAddressField(isValid: true, message: "teste")
        
        XCTAssertEqual(viewControllerSpy.updateLayoutAddressFieldIsValidMessageCallsCount, 1)
        XCTAssertNotNil(viewControllerSpy.updateLayoutAddressFieldIsValidMessageReceivedInvocations)
    }
    
    func testUpdateLayoutComplementField_WhenValidComplement_ShouldUpdateFieldStatus() {
        sut.updateLayoutComplementField(isValid: true, message: "teste")
        
        XCTAssertEqual(viewControllerSpy.updateLayoutComplementFieldIsValidMessageCallsCount, 1)
        XCTAssertNotNil(viewControllerSpy.updateLayoutComplementFieldIsValidMessageReceivedInvocations)
    }
    
    func testUpdateLayoutNumberField_WhenValidNumber_ShouldUpdateFieldStatus() {
        sut.updateLayoutNumberField(isValid: true, message: "teste")
        
        XCTAssertEqual(viewControllerSpy.updateLayoutNumberFieldIsValidMessageCallsCount, 1)
        XCTAssertNotNil(viewControllerSpy.updateLayoutNumberFieldIsValidMessageReceivedInvocations)
    }
    
    func testUpdateLayoutDistrictField_WhenValidDistrict_ShouldUpdateFieldStatus() {
        sut.updateLayoutDistrictField(isValid: true, message: "teste")
        
        XCTAssertEqual(viewControllerSpy.updateLayoutDistrictFieldIsValidMessageCallsCount, 1)
        XCTAssertNotNil(viewControllerSpy.updateLayoutNumberFieldIsValidMessageReceivedInvocations)
    }
    
    func testUpdateLayoutCityField_WhenValidCity_ShouldUpdateFieldStatus() {
        sut.updateLayoutCityField(isValid: true, message: "teste")
        
        XCTAssertEqual(viewControllerSpy.updateLayoutCityFieldIsValidMessageCallsCount, 1)
        XCTAssertNotNil(viewControllerSpy.updateLayoutCityFieldIsValidMessageReceivedInvocations)
    }
    
    func testUpdateLayoutStateField_WhenValidState_ShouldUpdateFieldStatus() {
        sut.updateLayoutStateField(isValid: true, message: "teste")
        
        XCTAssertEqual(viewControllerSpy.updateLayoutStateFieldIsValidMessageCallsCount, 1)
        XCTAssertNotNil(viewControllerSpy.updateLayoutStateFieldIsValidMessageReceivedInvocations)
    }
    
    func testConfirmButtonConfig_ShouldCallButtonTap() {
        sut.confirmButtonConfig()
        
        XCTAssertEqual(viewControllerSpy.confirmButtonDidTapCallsCount, 1)
    }
}
