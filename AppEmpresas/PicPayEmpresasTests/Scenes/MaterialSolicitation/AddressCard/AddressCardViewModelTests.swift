import LegacyPJ
import XCTest
@testable import PicPayEmpresas

private final class AddressCardServiceMock: AddressCardServicing {
    var addressExpectedResult: Result<Address, LegacyPJError>?
    
    func loadAddress(by postalCode: String, completion: @escaping(Result<Address, LegacyPJError>) -> Void) {
        guard let expectedResult = addressExpectedResult else {
            XCTFail("Expected result not defined")
            return
        }
        completion(expectedResult)
    }
}

private class AddressCardPresentingSpy: AddressCardPresenting {
    var viewController: AddressCardDisplay?

    //MARK: - loadAddressByPostalCode
    private(set) var loadAddressByPostalCodeAddressCallsCount = 0
    private(set) var loadAddressByPostalCodeAddressReceivedInvocations: [Address] = []

    func loadAddressByPostalCode(address: Address) {
        loadAddressByPostalCodeAddressCallsCount += 1
        loadAddressByPostalCodeAddressReceivedInvocations.append(address)
    }

    //MARK: - startLoading
    private(set) var startLoadingCallsCount = 0

    func startLoading() {
        startLoadingCallsCount += 1
    }

    //MARK: - setup
    private(set) var setupAddressCallsCount = 0
    private(set) var setupAddressReceivedInvocations: [Address] = []

    func setup(address: Address) {
        setupAddressCallsCount += 1
        setupAddressReceivedInvocations.append(address)
    }

    //MARK: - updateLayoutPostalCodeField
    private(set) var updateLayoutPostalCodeFieldIsValidMessageCallsCount = 0
    private(set) var updateLayoutPostalCodeFieldIsValidMessageReceivedInvocations: [(isValid: Bool, message: String)] = []

    func updateLayoutPostalCodeField(isValid: Bool, message: String) {
        updateLayoutPostalCodeFieldIsValidMessageCallsCount += 1
        updateLayoutPostalCodeFieldIsValidMessageReceivedInvocations.append((isValid: isValid, message: message))
    }

    //MARK: - updateLayoutAddressField
    private(set) var updateLayoutAddressFieldIsValidMessageCallsCount = 0
    private(set) var updateLayoutAddressFieldIsValidMessageReceivedInvocations: [(isValid: Bool, message: String)] = []

    func updateLayoutAddressField(isValid: Bool, message: String) {
        updateLayoutAddressFieldIsValidMessageCallsCount += 1
        updateLayoutAddressFieldIsValidMessageReceivedInvocations.append((isValid: isValid, message: message))
    }

    //MARK: - updateLayoutComplementField
    private(set) var updateLayoutComplementFieldIsValidMessageCallsCount = 0
    private(set) var updateLayoutComplementFieldIsValidMessageReceivedInvocations: [(isValid: Bool, message: String)] = []

    func updateLayoutComplementField(isValid: Bool, message: String) {
        updateLayoutComplementFieldIsValidMessageCallsCount += 1
        updateLayoutComplementFieldIsValidMessageReceivedInvocations.append((isValid: isValid, message: message))
    }

    //MARK: - updateLayoutNumberField
    private(set) var updateLayoutNumberFieldIsValidMessageCallsCount = 0
    private(set) var updateLayoutNumberFieldIsValidMessageReceivedInvocations: [(isValid: Bool, message: String)] = []

    func updateLayoutNumberField(isValid: Bool, message: String) {
        updateLayoutNumberFieldIsValidMessageCallsCount += 1
        updateLayoutNumberFieldIsValidMessageReceivedInvocations.append((isValid: isValid, message: message))
    }

    //MARK: - updateLayoutDistrictField
    private(set) var updateLayoutDistrictFieldIsValidMessageCallsCount = 0
    private(set) var updateLayoutDistrictFieldIsValidMessageReceivedInvocations: [(isValid: Bool, message: String)] = []

    func updateLayoutDistrictField(isValid: Bool, message: String) {
        updateLayoutDistrictFieldIsValidMessageCallsCount += 1
        updateLayoutDistrictFieldIsValidMessageReceivedInvocations.append((isValid: isValid, message: message))
    }

    //MARK: - updateLayoutCityField
    private(set) var updateLayoutCityFieldIsValidMessageCallsCount = 0
    private(set) var updateLayoutCityFieldIsValidMessageReceivedInvocations: [(isValid: Bool, message: String)] = []

    func updateLayoutCityField(isValid: Bool, message: String) {
        updateLayoutCityFieldIsValidMessageCallsCount += 1
        updateLayoutCityFieldIsValidMessageReceivedInvocations.append((isValid: isValid, message: message))
    }

    //MARK: - updateLayoutStateField
    private(set) var updateLayoutStateFieldIsValidMessageCallsCount = 0
    private(set) var updateLayoutStateFieldIsValidMessageReceivedInvocations: [(isValid: Bool, message: String)] = []

    func updateLayoutStateField(isValid: Bool, message: String) {
        updateLayoutStateFieldIsValidMessageCallsCount += 1
        updateLayoutStateFieldIsValidMessageReceivedInvocations.append((isValid: isValid, message: message))
    }

    //MARK: - confirmButtonConfig
    private(set) var confirmButtonConfigCallsCount = 0

    func confirmButtonConfig() {
        confirmButtonConfigCallsCount += 1
    }
}

private class ButtonDelegateOutputSpy: ButtonDelegateOutput {
    //MARK: - sendAddress
    private(set) var sendAddressAddressValidationCallsCount = 0
    private(set) var sendAddressAddressValidationReceivedInvocations: [AddressValidation] = []

    func sendAddress(addressValidation: AddressValidation) {
        sendAddressAddressValidationCallsCount += 1
        sendAddressAddressValidationReceivedInvocations.append(addressValidation)
    }
}

private final class AddressCardViewModelTests: XCTestCase {
    private typealias Localizable = Strings.MaterialSolicitation
    
    private let serviceMock = AddressCardServiceMock()
    private let presenterSpy = AddressCardPresentingSpy()
    private let buttonDelegate = ButtonDelegateOutputSpy()
    
    private lazy var sut: AddressCardViewModel = {
        let viewModel = AddressCardViewModel(service: serviceMock, presenter: presenterSpy)
        viewModel.buttonDelegate = buttonDelegate
        return viewModel
    }()
    
    // MARK: Load Address
    
    func testLoadAddress_WhenSuccess_ShouldStartLoadingAndLoadAddress() {
        serviceMock.addressExpectedResult = .success(Address())
        
        sut.loadAddress(by: "00000-000")
        
        XCTAssertEqual(presenterSpy.loadAddressByPostalCodeAddressCallsCount, 1)
        XCTAssertTrue(presenterSpy.loadAddressByPostalCodeAddressReceivedInvocations.isNotEmpty)
    }
    
    func testLoadAddress_WhenFailure_ShouldUpdateFieldsWithError() throws {
        serviceMock.addressExpectedResult = .failure(.invalidCnpj)
        
        sut.loadAddress(by: "00000-000")
        
        XCTAssertEqual(presenterSpy.updateLayoutPostalCodeFieldIsValidMessageCallsCount, 1)
        let isValidPostalCode = try XCTUnwrap(presenterSpy.updateLayoutPostalCodeFieldIsValidMessageReceivedInvocations.first?.isValid)
        XCTAssertFalse(isValidPostalCode)
    }
    
    // MARK: Send Address
    
    func testSendAddress_WhenAllFieldsOk_ShouldSendAddress() {
        let address = AddressValidation(
            postalCode: postalCodeAddressFieldValid(),
            address: streetAddressField(),
            district: neighborhoodAddressField(),
            city: cityAddressField(),
            state: stateAddressField(),
            number: numberAddressField(),
            complement: complementAddressField()
        )
        sut.sendAddress(addressValidation: address)
        
        XCTAssertEqual(buttonDelegate.sendAddressAddressValidationCallsCount, 1)
    }
    
    func testSendAddress_WhenAllFieldsNotOk_ShouldNotSendAddress() {
        let address = AddressValidation(
            postalCode: postalCodeAddressFieldInvalid(),
            address: streetAddressField(),
            district: neighborhoodAddressField(),
            city: cityAddressField(),
            state: stateAddressField(),
            number: numberAddressField(),
            complement: complementAddressField()
        )
        sut.sendAddress(addressValidation: address)
        
        XCTAssertEqual(buttonDelegate.sendAddressAddressValidationCallsCount, 0)
    }
    
    // MARK: Setup
    
    func testSetup_ShouldSetupAddressInTheFields() {
        sut.setup(Address())
        
        XCTAssertEqual(presenterSpy.setupAddressCallsCount, 1)
        XCTAssertTrue(presenterSpy.setupAddressReceivedInvocations.isNotEmpty)
    }
    
    func testSetErrorText_ShouldSetRequiredMessage() {
        let errorText = sut.errorTextConfigured()
        
        XCTAssertEqual(errorText, Localizable.addressCardRequiredField)
    }
    
    func testSetErrorText_WhenErrorMessageIsNotNil_ShouldSetValidationMessage() {
        let errorText = sut.errorTextConfigured(errorMessage: Localizable.addressCardPostalCodeError)
        
        XCTAssertEqual(errorText, Localizable.addressCardPostalCodeError)
    }
    
    // MARK: Validations - Required And Filled Field
    
    func testIsRequiredAndFilledField_WhenRequiredAndEmpty_ShouldReturnFalse() {
        let isValid = sut.isRequiredAndFilledField(addressField: genericAddressFieldUnfilled())
        
        XCTAssertFalse(isValid)
    }
    
    func testIsRequiredAndFilledField_WhenRequiredAndNotEmpty_ShouldReturnTrue() {
        let isValid = sut.isRequiredAndFilledField(addressField: genericAddressFieldValid())
        
        XCTAssertTrue(isValid)
    }
    
    // MARK: Validations - Valid Postal Code Field
    
    func testIsValidPostalCodeField_WhenValidFieldAndValidationIsOK_ShouldReturnTrue() {
        let isValidField = sut.isValidPostalCodeField(addressField: postalCodeAddressFieldValid(), validation: postalCodeValidation(text: "05317-020"))
        XCTAssertTrue(isValidField)
    }
    
    func testIsValidPostalCodeField_WhenInvalidFieldAndValidationIsOK_ShouldReturnFalse() {
        let isValidField = sut.isValidPostalCodeField(addressField: postalCodeAddressFieldInvalid(), validation: postalCodeValidation(text: "05317-020"))
        XCTAssertFalse(isValidField)
    }
    
    func testIsValidPostalCodeField_WhenValidFieldAndValidationIsNotOK_ShouldReturnFalse() {
        let isValidField = sut.isValidPostalCodeField(addressField: postalCodeAddressFieldValid(), validation: postalCodeValidation(text: "05317-02"))
        XCTAssertFalse(isValidField)
    }
    
    func testIsValidPostalCodeField_WhenInvalidFieldAndValidationIsNotOK_ShouldReturnFalse() {
        let isValidField = sut.isValidPostalCodeField(addressField: postalCodeAddressFieldInvalid(), validation: postalCodeValidation(text: "05317-02"))
        XCTAssertFalse(isValidField)
    }
    
    // MARK: Validations - Valid Postal Code
    
    func testIsValidPostalCode_WhenInvalidText_ShouldReturnFalse() {
        let isValidField = sut.isValidPostalCode(text: "text")
        XCTAssertFalse(isValidField)
    }
    
    func testIsValidPostalCode_WhenValidText_ShouldReturnFalse() {
        let isValidField = sut.isValidPostalCode(text: "05317-020")
        XCTAssertTrue(isValidField)
    }
    
    // MARK: Update Layout Fields
    
    func testUpdateLayoutAllFields_ShouldUpdateFieldsLayout() throws {
        let address = AddressValidation(
            postalCode: postalCodeAddressFieldValid(),
            address: streetAddressField(),
            district: neighborhoodAddressField(),
            city: cityAddressField(),
            state: stateAddressField(),
            number: numberAddressField(),
            complement: complementAddressField()
        )
        sut.updateLayoutAllFields(addressValidation: address)
        
        XCTAssertEqual(presenterSpy.updateLayoutPostalCodeFieldIsValidMessageCallsCount, 1)
        let isValidPostalCode = try XCTUnwrap(presenterSpy.updateLayoutPostalCodeFieldIsValidMessageReceivedInvocations.first?.isValid)
        XCTAssertTrue(isValidPostalCode)
        
        XCTAssertEqual(presenterSpy.updateLayoutAddressFieldIsValidMessageCallsCount, 1)
        let isValidAddress = try XCTUnwrap(presenterSpy.updateLayoutAddressFieldIsValidMessageReceivedInvocations.first?.isValid)
        XCTAssertTrue(isValidAddress)
        
        XCTAssertEqual(presenterSpy.updateLayoutComplementFieldIsValidMessageCallsCount, 1)
        let isValidComplement = try XCTUnwrap(presenterSpy.updateLayoutComplementFieldIsValidMessageReceivedInvocations.first?.isValid)
        XCTAssertTrue(isValidComplement)
        
        XCTAssertEqual(presenterSpy.updateLayoutCityFieldIsValidMessageCallsCount, 1)
        let isValidCity = try XCTUnwrap(presenterSpy.updateLayoutCityFieldIsValidMessageReceivedInvocations.first?.isValid)
        XCTAssertTrue(isValidCity)
        
        XCTAssertEqual(presenterSpy.updateLayoutStateFieldIsValidMessageCallsCount, 1)
        let isValidState = try XCTUnwrap(presenterSpy.updateLayoutStateFieldIsValidMessageReceivedInvocations.first?.isValid)
        XCTAssertTrue(isValidState)
        
        XCTAssertEqual(presenterSpy.updateLayoutNumberFieldIsValidMessageCallsCount, 1)
        let isValidNumber = try XCTUnwrap(presenterSpy.updateLayoutNumberFieldIsValidMessageReceivedInvocations.first?.isValid)
        XCTAssertTrue(isValidNumber)
        
        XCTAssertEqual(presenterSpy.updateLayoutDistrictFieldIsValidMessageCallsCount, 1)
        let isValidNeighborhood = try XCTUnwrap(presenterSpy.updateLayoutDistrictFieldIsValidMessageReceivedInvocations.first?.isValid)
        XCTAssertTrue(isValidNeighborhood)
    }
    
    func testUpdateLayoutPostalCodeField_WhenUnfilledField_ShouldUpdateInvalidField() throws {
        sut.updateLayoutPostalCodeField(genericAddressFieldUnfilled())
        
        XCTAssertEqual(presenterSpy.updateLayoutPostalCodeFieldIsValidMessageCallsCount, 1)
        let isValidPostalCode = try XCTUnwrap(presenterSpy.updateLayoutPostalCodeFieldIsValidMessageReceivedInvocations.first?.isValid)
        XCTAssertFalse(isValidPostalCode)
        XCTAssertEqual(presenterSpy.updateLayoutPostalCodeFieldIsValidMessageReceivedInvocations.first?.message, Localizable.addressCardRequiredField)
    }
    
    func testUpdateLayoutPostalCodeField_WhenInvalidField_ShouldUpdateInvalidField() throws {
        sut.updateLayoutPostalCodeField(postalCodeAddressFieldInvalid())
        
        XCTAssertEqual(presenterSpy.updateLayoutPostalCodeFieldIsValidMessageCallsCount, 1)
        let isValidPostalCode = try XCTUnwrap(presenterSpy.updateLayoutPostalCodeFieldIsValidMessageReceivedInvocations.first?.isValid)
        XCTAssertFalse(isValidPostalCode)
        XCTAssertEqual(presenterSpy.updateLayoutPostalCodeFieldIsValidMessageReceivedInvocations.first?.message, Localizable.addressCardPostalCodeError)
    }
    
    func testUpdateLayoutStreetField_WhenUnfilledField_ShouldUpdateUnfilledField() throws {
        sut.updateLayoutStreetField(genericAddressFieldUnfilled())
        
        XCTAssertEqual(presenterSpy.updateLayoutAddressFieldIsValidMessageCallsCount, 1)
        let isValidAddress = try XCTUnwrap(presenterSpy.updateLayoutAddressFieldIsValidMessageReceivedInvocations.first?.isValid)
        XCTAssertFalse(isValidAddress)
        XCTAssertEqual(presenterSpy.updateLayoutAddressFieldIsValidMessageReceivedInvocations.first?.message, Localizable.addressCardRequiredField)
    }
    
    func testUpdateLayoutComplementField_WhenUnfilledField_ShouldUpdateUnfilledField() throws {
        sut.updateLayoutComplementField(genericAddressFieldUnfilled())
        
        XCTAssertEqual(presenterSpy.updateLayoutComplementFieldIsValidMessageCallsCount, 1)
        let isValidComplement = try XCTUnwrap(presenterSpy.updateLayoutComplementFieldIsValidMessageReceivedInvocations.first?.isValid)
        XCTAssertFalse(isValidComplement)
        XCTAssertEqual(presenterSpy.updateLayoutComplementFieldIsValidMessageReceivedInvocations.first?.message, Localizable.addressCardRequiredField)
    }
    
    func testUpdateLayoutNumberField_WhenUnfilledField_ShouldUpdateUnfilledField() throws {
        sut.updateLayoutNumberField(genericAddressFieldUnfilled())
        
        XCTAssertEqual(presenterSpy.updateLayoutNumberFieldIsValidMessageCallsCount, 1)
        let isValidNumber = try XCTUnwrap(presenterSpy.updateLayoutNumberFieldIsValidMessageReceivedInvocations.first?.isValid)
        XCTAssertFalse(isValidNumber)
        XCTAssertEqual(presenterSpy.updateLayoutNumberFieldIsValidMessageReceivedInvocations.first?.message, Localizable.addressCardRequiredField)
    }
    
    func testUpdateLayoutDistrictField_WhenUnfilledField_ShouldUpdateUnfilledField() throws {
        sut.updateLayoutDistrictField(genericAddressFieldUnfilled())
        
        XCTAssertEqual(presenterSpy.updateLayoutDistrictFieldIsValidMessageCallsCount, 1)
        let isValidNeighborhood = try XCTUnwrap(presenterSpy.updateLayoutDistrictFieldIsValidMessageReceivedInvocations.first?.isValid)
        XCTAssertFalse(isValidNeighborhood)
        XCTAssertEqual(presenterSpy.updateLayoutDistrictFieldIsValidMessageReceivedInvocations.first?.message, Localizable.addressCardRequiredField)
    }
    
    func testUpdateLayoutCityField_WhenUnfilledField_ShouldUpdateUnfilledField() throws {
        sut.updateLayoutCityField(genericAddressFieldUnfilled())
        
        XCTAssertEqual(presenterSpy.updateLayoutCityFieldIsValidMessageCallsCount, 1)
        let isValidCity = try XCTUnwrap(presenterSpy.updateLayoutCityFieldIsValidMessageReceivedInvocations.first?.isValid)
        XCTAssertFalse(isValidCity)
        XCTAssertEqual(presenterSpy.updateLayoutCityFieldIsValidMessageReceivedInvocations.first?.message, Localizable.addressCardRequiredField)
    }
    
    func testUpdateLayoutStateField_WhenUnfilledField_ShouldUpdateUnfilledField() throws {
        sut.updateLayoutStateField(genericAddressFieldUnfilled())
        
        XCTAssertEqual(presenterSpy.updateLayoutStateFieldIsValidMessageCallsCount, 1)
        let isValidState = try XCTUnwrap(presenterSpy.updateLayoutStateFieldIsValidMessageReceivedInvocations.first?.isValid)
        XCTAssertFalse(isValidState)
        XCTAssertEqual(presenterSpy.updateLayoutStateFieldIsValidMessageReceivedInvocations.first?.message, Localizable.addressCardRequiredField)
    }
}

//MARK: - Helper methods

private extension AddressCardViewModelTests {
    func postalCodeValidation(text: String) -> Bool {
        text.count == 9
    }
    
    func postalCodeAddressFieldValid() -> AddressField {
        let text = "05317-020"
        return AddressField(
            text: text,
            isRequired: true,
            isValid: postalCodeValidation(text: text)
        )
    }
    
    func postalCodeAddressFieldInvalid() -> AddressField {
        let text = "05317-02"
        return AddressField(
            text: text,
            isRequired: true,
            isValid: text.count == 9
        )
    }
    
    func genericAddressFieldValid() -> AddressField {
        AddressField(
            text: "teste",
            isRequired: true,
            isValid: true
        )
    }
    
    func genericAddressFieldUnfilled() -> AddressField {
        AddressField(
            text: "",
            isRequired: true,
            isValid: true
        )
    }
    
    func streetAddressField() -> AddressField {
        AddressField(
            text: "street",
            isRequired: true
        )
    }
    
    func complementAddressField() -> AddressField {
        AddressField(
            text: "complement",
            isRequired: false
        )
    }
    
    func numberAddressField() -> AddressField {
        AddressField(
            text: "number",
            isRequired: true
        )
    }
    
    func neighborhoodAddressField() -> AddressField {
        AddressField(
            text: "neighborhood",
            isRequired: true
        )
    }
    
    func cityAddressField() -> AddressField {
        AddressField(
            text: "city",
            isRequired: true
        )
    }
    
    func stateAddressField() -> AddressField {
        AddressField(
            text: "state",
            isRequired: true
        )
    }
}
