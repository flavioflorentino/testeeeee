import XCTest
import Core
@testable import PicPayEmpresas

private final class MaterialFlowControlServiceMock: MaterialFlowControlServicing {

    //MARK: - loadNextStep
    private(set) var loadNextStepCompletionCallsCount = 0
    private(set) var loadNextStepCompletionReceivedInvocations: [(Result<MaterialFlowControlResponse?, ApiError>) -> Void] = []
    var loadNextStepCompletionClosure: Result<MaterialFlowControlResponse?, ApiError>?

    func loadNextStep(completion: @escaping(Result<MaterialFlowControlResponse?, ApiError>) -> Void) {
        loadNextStepCompletionCallsCount += 1
        loadNextStepCompletionReceivedInvocations.append(completion)
        
        guard let expectedResult = loadNextStepCompletionClosure else {
            XCTFail("Expected result not defined")
            return
        }
        completion(expectedResult)
    }
}

private final class MaterialFlowControlPresenterSpy: MaterialFlowControlPresenting {
    var viewController: MaterialFlowControlDisplay?

    //MARK: - displayThirdSolicitation
    private(set) var displayThirdSolicitationCallsCount = 0

    func displayThirdSolicitation() {
        displayThirdSolicitationCallsCount += 1
    }

    //MARK: - displayStepError
    private(set) var displayStepErrorCallsCount = 0

    func displayStepError() {
        displayStepErrorCallsCount += 1
    }

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [MaterialFlowControlAction] = []

    func didNextStep(action: MaterialFlowControlAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }

    //MARK: - startLoading
    private(set) var startLoadingCallsCount = 0

    func startLoading() {
        startLoadingCallsCount += 1
    }

    //MARK: - stopLoading
    private(set) var stopLoadingCallsCount = 0

    func stopLoading() {
        stopLoadingCallsCount += 1
    }
}

private final class MaterialFlowControlViewModelTests: XCTestCase {
    private let serviceMock = MaterialFlowControlServiceMock()
    private let presenterSpy = MaterialFlowControlPresenterSpy()
    lazy var sut = MaterialFlowControlViewModel(service: serviceMock, presenter: presenterSpy)
    
    func testLoadNextStep_WhenSolicitation_ShouldGoToNextStep() {
        let flowControlResponse = MaterialFlowControlResponse(boardStep: MaterialFlowStep.solicitation.rawValue)
        serviceMock.loadNextStepCompletionClosure = .success(flowControlResponse)
        
        sut.loadNextStep()
        
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.count, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(.solicitation))
    }
    
    func testLoadNextStep_WhenActivation_ShouldGoToActivationScreen() {
        let flowControlResponse = MaterialFlowControlResponse(boardStep: MaterialFlowStep.activation.rawValue)
        serviceMock.loadNextStepCompletionClosure = .success(flowControlResponse)
        
        sut.loadNextStep()
        
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.count, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(.activation))
    }
    
    func testLoadNextStep_WhenDeliveryForecast_ShouldGoToDeliveryForecastScreen() {
        let flowControlResponse = MaterialFlowControlResponse(boardStep: MaterialFlowStep.deliveryForecast.rawValue)
        serviceMock.loadNextStepCompletionClosure = .success(flowControlResponse)
        
        sut.loadNextStep()
        
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.count, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(.deliveryForecast))
    }
    
    func testLoadNextStep_WhenThirdSolicitation_ShouldGoToThirdSolicitationScreen() {
        let flowControlResponse = MaterialFlowControlResponse(boardStep: MaterialFlowStep.thirdSolicitation.rawValue)
        serviceMock.loadNextStepCompletionClosure = .success(flowControlResponse)
        
        sut.loadNextStep()
        
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayThirdSolicitationCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
    }
    
    func testLoadNextStep_WhenFailure_ShouldGoToErrorScreen() {
        serviceMock.loadNextStepCompletionClosure = .failure(.bodyNotFound)
        
        sut.loadNextStep()
        
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayStepErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
    }
    
    func testLoadNextStep_WhenStepNotFound_ShouldGoToErrorScreen() {
        serviceMock.loadNextStepCompletionClosure = .success(nil)
        
        sut.loadNextStep()
        
        XCTAssertEqual(presenterSpy.startLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.displayStepErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallsCount, 1)
    }
}
