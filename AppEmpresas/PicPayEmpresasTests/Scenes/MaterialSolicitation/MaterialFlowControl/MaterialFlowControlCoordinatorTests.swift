import XCTest
@testable import PicPayEmpresas

private final class MaterialFlowControlCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy(rootViewController: ViewControllerSpy())
    
    private lazy var sut: MaterialFlowControlCoordinator = {
        let coordinator = MaterialFlowControlCoordinator(dependencies: DependencyContainerMock())
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsSolicitation_ShouldPresentSolicitationFlow() {
        sut.perform(action: .solicitation)
        XCTAssertEqual(navigationSpy.pushedCount, 2)
        XCTAssertTrue(navigationSpy.currentViewController is OptinApplyMaterialViewController)
    }
    
    func testPerform_WhenActionIsActivation_ShouldPresentActivationFlow() {
        sut.perform(action: .activation)
        XCTAssertEqual(navigationSpy.pushedCount, 2)
        XCTAssertTrue(navigationSpy.currentViewController is DeliveryDetailViewController)
    }
    
    func testPerform_WhenActionIsDeliveryForecast_ShouldPresentDeliveryForecastScreen() {
        sut.perform(action: .deliveryForecast)
        XCTAssertEqual(navigationSpy.pushedCount, 2)
        XCTAssertTrue(navigationSpy.currentViewController is DeliveryDetailViewController)
    }
    
    func testPerform_WhenActionIsThirdSolicitation_ShouldPresentThirdSolicitationScreen() {
        let viewConfig = BizErrorViewLayout(
            title: "title",
            message: "message",
            image: Assets.Emoji.iconBad.image,
            buttonTitle: "button title"
        )
        sut.perform(action: .thirdSolicitation(viewConfig))
        XCTAssertEqual(navigationSpy.pushedCount, 2)
        XCTAssertTrue(navigationSpy.currentViewController is BizErrorViewViewController)
    }
    
    func testPerform_WhenActionIsError_ShouldPresentErrorScreen() {
        let viewConfig = BizErrorViewLayout(
            title: "title",
            message: "message",
            image: Assets.Emoji.iconBad.image,
            buttonTitle: "button title"
        )
        sut.perform(action: .error(viewConfig))
        XCTAssertEqual(navigationSpy.pushedCount, 2)
        XCTAssertTrue(navigationSpy.currentViewController is BizErrorViewViewController)
    }
}
