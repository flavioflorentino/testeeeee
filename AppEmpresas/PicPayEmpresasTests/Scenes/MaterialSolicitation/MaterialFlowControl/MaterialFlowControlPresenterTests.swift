import XCTest
@testable import PicPayEmpresas

private final class MaterialFlowControlCoordinatorSpy: MaterialFlowControlCoordinating {
    var viewController: UIViewController?

    //MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [MaterialFlowControlAction] = []

    func perform(action: MaterialFlowControlAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

private final class MaterialFlowControlViewControllerSpy: MaterialFlowControlDisplay {

    //MARK: - startLoading
    private(set) var startLoadingCallsCount = 0

    func startLoading() {
        startLoadingCallsCount += 1
    }

    //MARK: - stopLoading
    private(set) var stopLoadingCallsCount = 0

    func stopLoading() {
        stopLoadingCallsCount += 1
    }
}

private final class MaterialFlowControlPresenterTests: XCTestCase {
    private typealias Localizable = Strings.MaterialSolicitation
    
    private lazy var coordinatorSpy = MaterialFlowControlCoordinatorSpy()
    private lazy var viewControllerSpy = MaterialFlowControlViewControllerSpy()
    
    lazy var sut: MaterialFlowControlPresenting? = {
        let presenter = MaterialFlowControlPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDisplayThirdSolicitation_ShouldGoToThirdSolicitationScreen() {
        sut?.displayThirdSolicitation()
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        
        let viewConfig = BizErrorViewLayout(
            title: Localizable.thirdSolicitationTitle,
            message: Localizable.thirdSolicitationDescription,
            image: Assets.Emoji.iconBad.image,
            buttonTitle: Localizable.thirdSolicitationButton
        )
        XCTAssertTrue(coordinatorSpy.performActionReceivedInvocations.contains(.thirdSolicitation(viewConfig)))
    }
    
    func testDisplayStepError_ShouldGoToStepErrorScreen() {
        sut?.displayStepError()
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        
        let viewConfig = BizErrorViewLayout(
            title: Localizable.stepRequestErrorTitle,
            message: Localizable.stepRequestErrorDescription,
            image: Assets.Emoji.iconBad.image
        )
        XCTAssertTrue(coordinatorSpy.performActionReceivedInvocations.contains(.error(viewConfig)))
    }
    
    func testDidNextStep_ShouldGoToNextScreen() {
        sut?.didNextStep(action: .solicitation)
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
    }
    
    func testStartLoading_ShouldStartLoading() {
        sut?.startLoading()
        
        XCTAssertEqual(viewControllerSpy.startLoadingCallsCount, 1)
    }
    
    func testStopLoading_ShouldStopLoading() {
        sut?.stopLoading()
        
        XCTAssertEqual(viewControllerSpy.stopLoadingCallsCount, 1)
    }
}
