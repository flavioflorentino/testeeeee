import XCTest
import LegacyPJ
import Core
@testable import PicPayEmpresas

private class MaterialAddressCoordinatorSpy: MaterialAddressCoordinating {
    var viewController: UIViewController?

    //MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [MaterialAddressAction] = []

    func perform(action: MaterialAddressAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

private final class MaterialAddressViewControllerSpy: MaterialAddressDisplay {
    private(set) var startLoadingCount = 0
    private(set) var showAddressCount = 0
    private(set) var stopLoadingCount = 0
    private(set) var requiredFieldsUnfilledCount = 0
    
    var address: Address?
    
    func showAddress(address: Address) {
        showAddressCount += 1
        self.address = address
    }
    
    func startLoading() {
        startLoadingCount += 1
    }
    
    func stopLoading() {
        stopLoadingCount += 1
    }
    
    func requiredFieldsUnfilled() {
        requiredFieldsUnfilledCount += 1
    }
}

private final class MaterialAddressPresenterTests: XCTestCase {
    private let viewControllerSpy = MaterialAddressViewControllerSpy()
    private lazy var coordinatorSpy = MaterialAddressCoordinatorSpy()
    
    lazy var sut: MaterialAddressPresenter? = {
        let presenter = MaterialAddressPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testStartLoading_ShouldStartLoading() {
        sut?.startLoading()
        
        XCTAssertEqual(viewControllerSpy.startLoadingCount, 1)
    }
    
    func testDidNextStep_ShouldGoToNextScreen() {
        sut?.didNextStep(action: .solicitationSuccess)
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertTrue(coordinatorSpy.performActionReceivedInvocations.contains(.solicitationSuccess))
    }
    
    func testShowMaterialAddress_ShouldLoadAddressAndStopLoading() {
        let materialAddressResponse = MaterialAddressResponse(
            cep: "05317-020",
            address: "Av. Manuel Bandeira",
            district: "Vila Leopoldina",
            city: "São Paulo",
            state: "SP",
            number: "291",
            complement: "Bloco B - 3º Andar"
        )
        
        sut?.showMaterialAddress(materialAddressResponse: materialAddressResponse)
        
        XCTAssertEqual(viewControllerSpy.showAddressCount, 1)
        XCTAssertEqual(viewControllerSpy.stopLoadingCount, 1)
        XCTAssertNotNil(viewControllerSpy.address)
    }
    
    func testShowErrorView_ShouldGoToErrorScreen() {
        sut?.showErrorView()
        
        let viewConfig = BizErrorViewLayout(
            title: Strings.MaterialSolicitation.materialSolicitationErrorTitle,
            message: Strings.MaterialSolicitation.materialSolicitationErrorSubtitle,
            image: Assets.Emoji.iconBad.image
        )
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertTrue(coordinatorSpy.performActionReceivedInvocations.contains(.solicitationError(viewConfig)))
    }
}
