import XCTest
import Core
@testable import PicPayEmpresas

private final class MaterialAddressServiceMock: MaterialAddressServicing {
    var loadAddressExpectedResult: Result<MaterialAddressResponse?, ApiError>?
    var requestMaterialExpectedResult: Result<MaterialSolicitationResponse?, ApiError>?
    
    func loadAddress(completion: @escaping (Result<MaterialAddressResponse?, ApiError>) -> Void) {
        guard let expectedResult = loadAddressExpectedResult else {
            XCTFail("Expected result not defined")
            return
        }
        completion(expectedResult)
    }
    
    func requestMaterial(materialSolicitationRequest: MaterialSolicitationRequest, completion: @escaping (Result<MaterialSolicitationResponse?, ApiError>) -> Void) {
        guard let expectedResult = requestMaterialExpectedResult else {
            XCTFail("Expected result not defined")
            return
        }
        completion(expectedResult)
    }
}

private final class MaterialAddressPresenterSpy: MaterialAddressPresenting {
    var viewController: MaterialAddressDisplay? = nil
    
    private(set) var materialAddressResponse: MaterialAddressResponse?
    private(set) var action: MaterialAddressAction?
    
    private(set) var requiredFieldsUnfilledCount = 0
    private(set) var showMaterialAddressCount = 0
    private(set) var didNextStepCount = 0
    private(set) var showErrorSViewCount = 0
    
    func requiredFieldsUnfilled() {
        requiredFieldsUnfilledCount += 1
    }
    
    func showMaterialAddress(materialAddressResponse: MaterialAddressResponse?) {
        showMaterialAddressCount += 1
        self.materialAddressResponse = materialAddressResponse
    }
    
    func didNextStep(action: MaterialAddressAction) {
        didNextStepCount += 1
        self.action = action
    }
    
    func showErrorView() {
        showErrorSViewCount += 1
    }
    
    func startLoading() { }
}

private final class MaterialAddressViewModelTests: XCTestCase {
    private let serviceMock = MaterialAddressServiceMock()
    private let presenterSpy = MaterialAddressPresenterSpy()
    lazy var sut = MaterialAddressViewModel(service: serviceMock, presenter: presenterSpy)
    
    func testLoadAddress_WhenSuccess_ShouldLoadAddress() throws {
        let materialAddressResponse = MaterialAddressResponse(
            cep: "05317-020",
            address: "Av. Manuel Bandeira",
            district: "Vila Leopoldina",
            city: "São Paulo",
            state: "SP",
            number: "291",
            complement: "Bloco B - 3º Andar"
        )
        serviceMock.loadAddressExpectedResult = .success(materialAddressResponse)
        
        sut.loadAddress()
        
        XCTAssertEqual(presenterSpy.showMaterialAddressCount, 1)
        XCTAssertNotNil(presenterSpy.materialAddressResponse)
    }
    
    func testLoadAddress_WhenAddressNil_ShouldShowEmptyFields() throws {
        serviceMock.loadAddressExpectedResult = .success(nil)
        
        sut.loadAddress()
        
        XCTAssertEqual(presenterSpy.showMaterialAddressCount, 1)
        XCTAssertNil(presenterSpy.materialAddressResponse)
    }
    
    func testLoadAddress_WhenFailure_ShouldShowEmptyFields() throws {
        serviceMock.loadAddressExpectedResult = .failure(.bodyNotFound)
        
        sut.loadAddress()
        
        XCTAssertEqual(presenterSpy.showMaterialAddressCount, 0)
        XCTAssertNil(presenterSpy.materialAddressResponse)
    }
    
    func testValidateFields_WhenSuccess_ShouldRequestMaterial() throws {
        let materialSolicitationResponse = MaterialSolicitationResponse(
            postalCode: "05317-020",
            street: "Av. Manuel Bandeira",
            district: "Vila Leopoldina",
            city: "São Paulo",
            state: "SP",
            number: "291",
            complement: "Bloco B - 3º Andar",
            sellerId: 1,
            lastUpdate: "22/06/2020",
            createdDate: "01/06/2020",
            id: 1
        )
        
        serviceMock.requestMaterialExpectedResult = .success(materialSolicitationResponse)
        
        let address = AddressValidation(
            postalCode: AddressField(text: "05317-020", isRequired: true),
            address: AddressField(text: "Av. Manuel Bandeira", isRequired: true),
            district: AddressField(text: "Vila Leopoldina", isRequired: true),
            city: AddressField(text: "São Paulo", isRequired: true),
            state: AddressField(text: "SP", isRequired: true),
            number: AddressField(text: "291", isRequired: true),
            complement: AddressField(text: "Bloco B - 3º Andar", isRequired: true)
        )
        sut.requestMaterial(address: address)
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertNotNil(presenterSpy.action)
        XCTAssertEqual(presenterSpy.action, .solicitationSuccess)
    }
    
    func testValidateFields_WhenFailure_ShouldShowError() throws {
        serviceMock.requestMaterialExpectedResult = .failure(.bodyNotFound)
        
        let address = AddressValidation(
            postalCode: AddressField(text: "05317-020", isRequired: true),
            address: AddressField(text: "Av. Manuel Bandeira", isRequired: true),
            district: AddressField(text: "Vila Leopoldina", isRequired: true),
            city: AddressField(text: "São Paulo", isRequired: true),
            state: AddressField(text: "SP", isRequired: true),
            number: AddressField(text: "291", isRequired: true),
            complement: AddressField(text: "Bloco B - 3º Andar", isRequired: true)
        )
        sut.requestMaterial(address: address)
        
        XCTAssertEqual(presenterSpy.showErrorSViewCount, 1)
    }
}
