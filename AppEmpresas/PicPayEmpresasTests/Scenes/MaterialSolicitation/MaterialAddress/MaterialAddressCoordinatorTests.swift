import XCTest
@testable import PicPayEmpresas

private final class MaterialAddressCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy(rootViewController: ViewControllerSpy())
    private lazy var sut: MaterialAddressCoordinator = {
        let coordinator = MaterialAddressCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsSolicitationSuccess_ShouldPresentSuccessScreen() {
        sut.perform(action: .solicitationSuccess)
        
        XCTAssertEqual(navigationSpy.pushedCount, 2)
        XCTAssertTrue(navigationSpy.currentViewController is SolicitationSuccessViewController)
    }
    
    func testPerform_WhenActionIsSolicitationError_ShouldPresentErrorScreen() throws {
        let viewConfig = BizErrorViewLayout(
            title: "title",
            message: "message",
            image: Assets.Emoji.iconBad.image,
            buttonTitle: "button title"
        )
        sut.perform(action: .solicitationError(viewConfig))
        
        XCTAssertEqual(navigationSpy.presentCount, 1)
        XCTAssertTrue(navigationSpy.presentViewController is BizErrorViewViewController)
    }
}
