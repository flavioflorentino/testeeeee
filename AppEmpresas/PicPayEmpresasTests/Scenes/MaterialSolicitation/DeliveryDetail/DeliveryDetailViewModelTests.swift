import XCTest
@testable import PicPayEmpresas

private final class DeliveryDetailPresenterSpy: DeliveryDetailPresenting {
    var viewController: DeliveryDetailDisplay?

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [DeliveryDetailAction] = []

    func didNextStep(action: DeliveryDetailAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }

    //MARK: - setupDescriptionDeliveryForecast
    private(set) var setupDescriptionDeliveryForecastCallsCount = 0

    func setupDescriptionDeliveryForecast() {
        setupDescriptionDeliveryForecastCallsCount += 1
    }

    //MARK: - setupDescriptionBoardActivation
    private(set) var setupDescriptionBoardActivationCallsCount = 0

    func setupDescriptionBoardActivation() {
        setupDescriptionBoardActivationCallsCount += 1
    }
}

private final class DeliveryDetailViewModelTests: XCTestCase {
    private let presenterSpy = DeliveryDetailPresenterSpy()
    
    lazy var sutDeliveryForecast = DeliveryDetailViewModel(presenter: presenterSpy, state: .deliveryForecast)
    lazy var sutBoardActivation = DeliveryDetailViewModel(presenter: presenterSpy, state: .boardActivation)
    
    func testSetupView_WhenStateDeliveryForecast_ShouldSetupViewForDeliveryForecast() {
        sutDeliveryForecast.setupView()
        
        XCTAssertEqual(presenterSpy.setupDescriptionDeliveryForecastCallsCount, 1)
    }
    
    func testSetupView_WhenStateBoardActivation_ShouldSetupViewForBoardActivation() {
        sutBoardActivation.setupView()
        
        XCTAssertEqual(presenterSpy.setupDescriptionBoardActivationCallsCount, 1)
    }
    
    func testDismissScreen_ShouldGoToRootScreen() {
        sutDeliveryForecast.dismissScreen()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.count, 1)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(.rootScreen))
    }
    
    func testDidNextStep_WhenStateDeliveryForecast_ShouldGoToRootScreen() {
        sutDeliveryForecast.didNextStep()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.count, 1)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(.rootScreen))
    }
    
    func testDidNextStep_WhenStateBoardActivation_ShouldGoToQRCodeScreen() {
        sutBoardActivation.didNextStep()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.count, 1)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(.qrCodeScreen))
    }
    
    func testShowCustomerSupport_ShouldGoToCustomerSupportScreen() {
        sutBoardActivation.showCustomerSupport()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
    }
}
