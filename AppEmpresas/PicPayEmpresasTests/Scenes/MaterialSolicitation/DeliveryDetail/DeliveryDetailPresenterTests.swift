import XCTest
@testable import PicPayEmpresas

// swiftlint:disable large_tuple
private final class DeliveryDetailCoordinatorSpy: DeliveryDetailCoordinating {
    var viewController: UIViewController?
    var navigationController: UINavigationController?

    //MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [DeliveryDetailAction] = []

    func perform(action: DeliveryDetailAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

private final class DeliveryDetailViewControllerSpy: DeliveryDetailDisplay {
    //MARK: - setView
    private(set) var setViewScreenTitleDescriptionTextButtonTitleCallsCount = 0
    private(set) var setViewScreenTitleDescriptionTextButtonTitleReceivedInvocations: [(screenTitle: String, descriptionText: NSAttributedString, buttonTitle: String)] = []

    func setView(
        screenTitle: String,
        descriptionText: NSAttributedString,
        buttonTitle: String
    ) {
        setViewScreenTitleDescriptionTextButtonTitleCallsCount += 1
        setViewScreenTitleDescriptionTextButtonTitleReceivedInvocations.append((screenTitle: screenTitle, descriptionText: descriptionText, buttonTitle: buttonTitle))
    }
    
    //MARK: - setView
    private(set) var setViewScreenTitleButtonTitleLinkTextLinkAttributesCallsCount = 0
    private(set) var setViewScreenTitleButtonTitleLinkTextLinkAttributesReceivedInvocations: [(screenTitle: String, buttonTitle: String, linkText: NSAttributedString, linkAttributes: [NSAttributedString.Key: Any])] = []

    func setView(
        screenTitle: String,
        buttonTitle: String,
        linkText: NSAttributedString,
        linkAttributes: [NSAttributedString.Key: Any]
    ) {
        setViewScreenTitleButtonTitleLinkTextLinkAttributesCallsCount += 1
        setViewScreenTitleButtonTitleLinkTextLinkAttributesReceivedInvocations.append((screenTitle: screenTitle, buttonTitle: buttonTitle, linkText: linkText, linkAttributes: linkAttributes))
    }
}

private final class DeliveryDetailPresenterTests: XCTestCase {
    private let viewControllerSpy = DeliveryDetailViewControllerSpy()
    private lazy var coordinatorSpy = DeliveryDetailCoordinatorSpy()
    
    lazy var sut: DeliveryDetailPresenter? = {
        let presenter = DeliveryDetailPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidNextStep() {
        sut?.didNextStep(action: .rootScreen)
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations.count, 1)
        XCTAssertTrue(coordinatorSpy.performActionReceivedInvocations.contains(.rootScreen))
    }
    
    func testSetupDescriptionDeliveryForecast_ShouldSetupScreenAsDeliveryForecast() {
        sut?.setupDescriptionDeliveryForecast()
        
        XCTAssertEqual(viewControllerSpy.setViewScreenTitleDescriptionTextButtonTitleCallsCount, 1)
        XCTAssertTrue(viewControllerSpy.setViewScreenTitleDescriptionTextButtonTitleReceivedInvocations.isNotEmpty)
    }
    
    func testSetupDescriptionBoardActivation_ShouldSetupScreenAsBoardActivation() {
        sut?.setupDescriptionBoardActivation()
        
        XCTAssertEqual(viewControllerSpy.setViewScreenTitleButtonTitleLinkTextLinkAttributesCallsCount, 1)
        XCTAssertTrue(viewControllerSpy.setViewScreenTitleButtonTitleLinkTextLinkAttributesReceivedInvocations.isNotEmpty)
    }
}
