import XCTest
@testable import PicPayEmpresas

private final class DeliveryDetailCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy(rootViewController: ViewControllerSpy())
    private let customerSupport = BizCustomerSupportManagerSpy()
    
    private lazy var sut: DeliveryDetailCoordinating = {
        let coordinator = DeliveryDetailCoordinator(dependencies: DependencyContainerMock())
        coordinator.viewController = navigationSpy.topViewController
        coordinator.customerSupport = customerSupport
        return coordinator
    }()
    
    func testPerform_WhenActionIsRootScreen_ShouldPresentRootScreen() {
        sut.perform(action: .rootScreen)
        XCTAssertEqual(navigationSpy.popToRoot, 1)
        XCTAssertTrue(navigationSpy.topViewController is ViewControllerSpy)
    }
    
    func testPerform_WhenActionIsQrCodeScreen_ShouldPresentQrCodeScreen() {
        sut.perform(action: .qrCodeScreen)
        XCTAssertEqual(navigationSpy.pushedCount, 2)
        XCTAssertTrue(navigationSpy.currentViewController is MaterialCameraActivationViewController)
    }
    
    func testPerform_WhenActionIsCustomerSupport_ShouldOpenCustomerSupport() {
        sut.perform(action: .customerSupport)
        XCTAssertEqual(customerSupport.presentTicketListFromWithCallsCount, 1)
    }
}
