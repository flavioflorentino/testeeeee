import XCTest
@testable import PicPayEmpresas

private final class MaterialStepRequestErrorTests: XCTestCase {
    let viewController: UIViewController? = ViewControllerSpy()
    
    private lazy var sut: MaterialStepRequestError = {
        let errorView = MaterialStepRequestError(viewController: viewController)
        return errorView
    }()
    
    func testButtonAction_ShouldCloseScreen() throws {
        sut.buttonAction()
        let viewControllerSpy = try XCTUnwrap(viewController as? ViewControllerSpy)
        XCTAssertEqual(viewControllerSpy.dismissCount, 1)
    }
    
    func testCloseAction_ShouldCloseScreen() throws {
        sut.closeAction()
        let viewControllerSpy = try XCTUnwrap(viewController as? ViewControllerSpy)
        XCTAssertEqual(viewControllerSpy.dismissCount, 1)
    }
}

private final class ThirdSolicitationErrorTests: XCTestCase {
    let viewController: UIViewController? = ViewControllerSpy()
    let customerSupport = BizCustomerSupportManagerSpy()
    
    private lazy var sut: ThirdSolicitationError = {
        let errorView = ThirdSolicitationError(viewController: viewController, dependencies: DependencyContainerMock())
        errorView.customerSupport = customerSupport
        return errorView
    }()
    
    func testButtonAction_ShouldOpenZendesk() {
        sut.buttonAction()
        XCTAssertEqual(customerSupport.presentTicketListFromWithCallsCount, 1)
    }
    
    func testCloseAction_ShouldCloseScreen() throws {
        sut.closeAction()
        let viewControllerSpy = try XCTUnwrap(viewController as? ViewControllerSpy)
        XCTAssertEqual(viewControllerSpy.dismissCount, 1)
    }
}

private final class MaterialSolicitationErrorTests: XCTestCase {
    let viewController: UIViewController? = ViewControllerSpy()
    
    private lazy var sut: MaterialSolicitationError = {
        let errorView = MaterialSolicitationError(viewController: viewController)
        return errorView
    }()
    
    func testButtonAction_ShouldCloseScreen() throws {
        sut.buttonAction()
        let viewControllerSpy = try XCTUnwrap(viewController as? ViewControllerSpy)
        XCTAssertEqual(viewControllerSpy.dismissCount, 1)
    }
    
    func testCloseAction_ShouldCloseScreen() throws {
        sut.closeAction()
        let viewControllerSpy = try XCTUnwrap(viewController as? ViewControllerSpy)
        XCTAssertEqual(viewControllerSpy.dismissCount, 1)
    }
}
