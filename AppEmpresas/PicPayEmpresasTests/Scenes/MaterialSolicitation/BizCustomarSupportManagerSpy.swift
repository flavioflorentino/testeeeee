import UIKit
@testable import PicPayEmpresas

final class BizCustomerSupportManagerSpy: BizCustomerSupportManagerDelegate {
    //MARK: - setup
    private(set) var setupCallsCount = 0

    func setup() {
        setupCallsCount += 1
    }

    //MARK: - registerUser
    private(set) var registerUserCallsCount = 0

    func registerUser() {
        registerUserCallsCount += 1
    }

    //MARK: - registerNotification
    private(set) var registerNotificationSellerIDCallsCount = 0
    private(set) var registerNotificationSellerIDReceivedInvocations: [String] = []

    func registerNotification(sellerID: String) {
        registerNotificationSellerIDCallsCount += 1
        registerNotificationSellerIDReceivedInvocations.append(sellerID)
    }

    //MARK: - pushFAQ
    private(set) var pushFAQFromWithCallsCount = 0
    private(set) var pushFAQFromWithReceivedInvocations: [(controller: UIViewController, seller: Seller?)] = []

    func pushFAQ(from controller: UIViewController, with seller: Seller?) {
        pushFAQFromWithCallsCount += 1
        pushFAQFromWithReceivedInvocations.append((controller: controller, seller: seller))
    }

    //MARK: - pushTicketList
    private(set) var pushTicketListFromWithCallsCount = 0
    private(set) var pushTicketListFromWithReceivedInvocations: [(controller: UIViewController, seller: Seller?)] = []

    func pushTicketList(from controller: UIViewController, with seller: Seller?) {
        pushTicketListFromWithCallsCount += 1
        pushTicketListFromWithReceivedInvocations.append((controller: controller, seller: seller))
    }

    //MARK: - presentFAQ
    private(set) var presentFAQFromWithCallsCount = 0
    private(set) var presentFAQFromWithReceivedInvocations: [(controller: UIViewController, seller: Seller?)] = []

    func presentFAQ(from controller: UIViewController, with seller: Seller?) {
        presentFAQFromWithCallsCount += 1
        presentFAQFromWithReceivedInvocations.append((controller: controller, seller: seller))
    }

    //MARK: - presentTicketList
    private(set) var presentTicketListFromWithCallsCount = 0
    private(set) var presentTicketListFromWithReceivedInvocations: [(controller: UIViewController, seller: Seller?)] = []

    func presentTicketList(from controller: UIViewController, with seller: Seller?) {
        presentTicketListFromWithCallsCount += 1
        presentTicketListFromWithReceivedInvocations.append((controller: controller, seller: seller))
    }
}
