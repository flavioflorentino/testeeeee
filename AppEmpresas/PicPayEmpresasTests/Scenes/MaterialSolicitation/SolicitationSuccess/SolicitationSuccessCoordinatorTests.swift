import XCTest
@testable import PicPayEmpresas

class SolicitationSuccessCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy(rootViewController: ViewControllerSpy())
    
    private lazy var sut: SolicitationSuccessCoordinator = {
        let coordinator = SolicitationSuccessCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsDeliveryForecast_ShouldPresentDeliveryForecastScreen() {
        sut.perform(action: .deliveryForecast)
        XCTAssertEqual(navigationSpy.pushedCount, 2)
        XCTAssertTrue(navigationSpy.currentViewController is DeliveryDetailViewController)
    }
    
    func testPerform_WhenActionIsRootScreen_ShouldPresentRootScreen() throws {
        sut.perform(action: .rootScreen)
        
        let topViewController = try XCTUnwrap(navigationSpy.topViewController as? ViewControllerSpy)
        XCTAssertEqual(topViewController.dismissCount, 1)
        XCTAssertEqual(navigationSpy.popToRoot, 1)
    }
}
