import XCTest
@testable import PicPayEmpresas

private final class SolicitationSuccessCoordinatorSpy: SolicitationSuccessCoordinating {
    var viewController: UIViewController?

    //MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [SolicitationSuccessAction] = []

    func perform(action: SolicitationSuccessAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

private final class SolicitationSuccessViewControllerSpy: SolicitationSuccessDisplay {

    //MARK: - setView
    private(set) var setViewImageAssetTitleDescriptionButtonTitleCallsCount = 0
    private(set) var setViewImageAssetTitleDescriptionButtonTitleReceivedInvocations: [(
        imageAsset: ImageAsset,
        title: String,
        description: String,
        buttonTitle: String,
        state: SuccessFeedbackState
    )] = []

    func setView(imageAsset: ImageAsset, title: String, description: String, buttonTitle: String, state: SuccessFeedbackState) {
        setViewImageAssetTitleDescriptionButtonTitleCallsCount += 1
        setViewImageAssetTitleDescriptionButtonTitleReceivedInvocations.append(
            (imageAsset: imageAsset, title: title, description: description, buttonTitle: buttonTitle, state: state)
        )
    }
}

private final class SolicitationSuccessPresenterTests: XCTestCase {
    private let viewControllerSpy = SolicitationSuccessViewControllerSpy()
    private lazy var coordinatorSpy = SolicitationSuccessCoordinatorSpy()
    
    lazy var sut: SolicitationSuccessPresenter? = {
        let presenter = SolicitationSuccessPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidNextStep() {
        sut?.didNextStep(action: .rootScreen)
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations.count, 1)
        XCTAssertTrue(coordinatorSpy.performActionReceivedInvocations.contains(.rootScreen))
    }
    
    func testSetupSolicitation_ShouldSetupScreenAsSolicitation() {
        sut?.setupSolicitation()
        
        XCTAssertEqual(viewControllerSpy.setViewImageAssetTitleDescriptionButtonTitleCallsCount, 1)
        XCTAssertTrue(viewControllerSpy.setViewImageAssetTitleDescriptionButtonTitleReceivedInvocations.isNotEmpty)
    }
    
    func testSetupActivation_ShouldSetupScreenAsActivation() {
        sut?.setupActivation()
        
        XCTAssertEqual(viewControllerSpy.setViewImageAssetTitleDescriptionButtonTitleCallsCount, 1)
        XCTAssertTrue(viewControllerSpy.setViewImageAssetTitleDescriptionButtonTitleReceivedInvocations.isNotEmpty)
    }
}
