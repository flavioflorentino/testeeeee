import XCTest
@testable import PicPayEmpresas

private final class SolicitationSuccessPresenterSpy: SolicitationSuccessPresenting {
    var viewController: SolicitationSuccessDisplay?

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [SolicitationSuccessAction] = []

    func didNextStep(action: SolicitationSuccessAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }

    //MARK: - setupSolicitation
    private(set) var setupSolicitationCallsCount = 0

    func setupSolicitation() {
        setupSolicitationCallsCount += 1
    }

    //MARK: - setupActivation
    private(set) var setupActivationCallsCount = 0

    func setupActivation() {
        setupActivationCallsCount += 1
    }
}

private final class SolicitationSuccessViewModelTests: XCTestCase {
    private let presenterSpy = SolicitationSuccessPresenterSpy()
    
    lazy var sutSolicitation = SolicitationSuccessViewModel(presenter: presenterSpy, state: .solicitation)
    lazy var sutActivation = SolicitationSuccessViewModel(presenter: presenterSpy, state: .activation)
    
    func testSetupView_WhenStateSolicitation_ShouldSetupViewForSolicitation() {
        sutSolicitation.setupView()
        
        XCTAssertEqual(presenterSpy.setupSolicitationCallsCount, 1)
    }
    
    func testSetupView_WhenStateActivation_ShouldSetupViewForActivation() {
        sutActivation.setupView()
        
        XCTAssertEqual(presenterSpy.setupActivationCallsCount, 1)
    }
    
    func testDidNextStep_WhenStateSolicitation_ShouldGoToDeliveryForecastScreen() {
        sutSolicitation.didNextStep()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.count, 1)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(.deliveryForecast))
    }
    
    func testDidNextStep_WhenStateActivation_ShouldGoToRootScreen() {
        sutActivation.didNextStep()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.count, 1)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(.rootScreen))
    }
}
