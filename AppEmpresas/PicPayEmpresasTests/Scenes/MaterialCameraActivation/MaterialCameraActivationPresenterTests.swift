import XCTest
import UI
import UIKit
@testable import PicPayEmpresas

final class MaterialCameraActivationPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = MaterialCameraActivationCoordinatorSpy()
    private lazy var displaySpy = MaterialCameraActivationDisplaySpy()
    private lazy var sut: MaterialCameraActivationPresenter = {
        let presenter = MaterialCameraActivationPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testHandleError_WhenReciveErrorFromViewModel_ShouldActionPopUpError() {
        sut.handleError(error: "error")
        
        XCTAssertEqual(displaySpy.errorCount, 1)
    }
    
    func testDidNextStep_WhenReceiveActionSuccessFromViewModel_ShouldSendActionToCoordinator() {
        sut.didNextStep(action: .activationSuccess)
        
        XCTAssertEqual(coordinatorSpy.currentAction, .activationSuccess)
    }
    
    func testDidNextStep_WhenReceiveActionCloseFromViewModel_ShouldSendActionToCoordinator() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.currentAction, .close)
    }
    
    func testCreateCameraPermissionPopup_WhenReceiveActionFormViewModel_ShouldCallCreatePopUpDisplayFunction() {
        sut.createCameraPermissionPopup()
        XCTAssertEqual(displaySpy.createCamerePermissionCount, 1)
    }
}

final private class MaterialCameraActivationCoordinatorSpy: MaterialCameraActivationCoordinating {
    var viewController: UIViewController?
    private(set) var currentAction: MaterialCameraActivationAction?
    
    func perform(action: MaterialCameraActivationAction) {
        currentAction = action
    }
}

final private class MaterialCameraActivationDisplaySpy: MaterialCameraActivationDisplay {
    private(set) var errorCount = 0
    private(set) var createCamerePermissionCount = 0
    private(set) var startLoadCount = 0
    private(set) var stopLoadCount = 0
    
    func showError(error: String) {
        errorCount += 1
    }
    
    func createCameraPermissionPopUp() {
        createCamerePermissionCount += 1
    }
    
    func startApolloLoader(loadingText: String) {}
    func stopApolloLoader(completion: (() -> Void)?) {}
}
