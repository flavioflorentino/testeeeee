import XCTest
import Core
import UI
import UIKit

@testable import PicPayEmpresas

final class MaterialCameraActivationViewModelTests: XCTestCase {
    private lazy var mock = MaterialCameraActivationServiceMock()
    private lazy var spy = MaterialCameraActivationPresenterSpy()
    private lazy var sut = MaterialCameraActivationViewModel(service: mock, presenter: spy)
    
    func testValidateCode_WhenServiceReturnFalse_ShouldCallHandleError() {
        mock.isSuccess = false
        mock.payloadSuccess = false
        sut.validateCode(code: "B2G5xt")
        
        XCTAssertEqual(spy.errorCount, 1)
        XCTAssertEqual(spy.startLoadCount, 1)
    }
    
    func testValidateCode_WhenServiceReturnTrueWithErrorInPayLoad_ShouldCallHandleError() {
        mock.isSuccess = true
        mock.payloadSuccess = false
        sut.validateCode(code: "B2G5xt")
        
        XCTAssertEqual(spy.errorCount, 1)
        XCTAssertEqual(spy.startLoadCount, 1)
    }
    
    func testValidateCode_WhenServiceReturnTrueWithoutErrosInPayload_ShouldCallScreenSuccess() {
        mock.isSuccess = true
        mock.payloadSuccess = true
        sut.validateCode(code: "B2G5xt")
        
        XCTAssertNotNil(spy.currentAction)
        XCTAssertEqual(spy.currentAction, .activationSuccess)
        XCTAssertEqual(spy.startLoadCount, 1)
    }
    
    func testCreateCameraPopUP_WhenReceiveActionFromViewController_ShouldCallCreateCameraPopUpInPresenter() {
        sut.createPopupCameraPermission()
        
        XCTAssertEqual(spy.createCameraPopUpCount, 1)
    }
    
    func testDidNextStep_WhenReceiveGoToSettingsAction_ShouldPresenterReceiveSameAction() {
        sut.didNextStep(action: .goToSettings)
        XCTAssertEqual(spy.currentAction, .goToSettings)
    }
    
    func testDidNextStep_WhenReceiveDismissAction_ShouldPresenterReceiveSameAction() {
        sut.didNextStep(action: .dismiss)
        XCTAssertEqual(spy.currentAction, .dismiss)
    }
    
    func testDidNextStep_WhenReceiveCloseAction_ShouldPresenterReceiveSameAction() {
        sut.didNextStep(action: .close)
        XCTAssertEqual(spy.currentAction, .close)
    }
}

private final class MaterialCameraActivationServiceMock: MaterialCameraActivationServicing {
    var isSuccess = true
    var payloadSuccess = true
    func activateCode(code: String, completion: @escaping (Result<MaterialActivation?, ApiError>) -> Void) {
        guard isSuccess else {
            completion(.failure(ApiError.serverError))
            return
        }
        
        if payloadSuccess {
            let material = MaterialActivation(success: true, error: nil, data: "QR Code vinculado com sucesso!")
            completion(.success(material))
            return
        }
        
        let material = MaterialActivation(success: false, error: "Plaquinha já está vinculada à esse lojista", data: nil)
        completion(.success(material))
    }
}

private final class MaterialCameraActivationPresenterSpy: MaterialCameraActivationPresenting {
    var viewController: MaterialCameraActivationDisplay?
    
    private(set) var errorCount = 0
    private(set) var currentAction: MaterialCameraActivationAction?
    private(set) var createCameraPopUpCount = 0
    private(set) var startLoadCount = 0
    
    func didNextStep(action: MaterialCameraActivationAction) {
        currentAction = action
    }
    
    func handleError(error: String) {
        errorCount += 1
    }
    
    func createCameraPermissionPopup() {
        createCameraPopUpCount += 1
    }
    
    func startLoad() {
        startLoadCount += 1
    }
}
