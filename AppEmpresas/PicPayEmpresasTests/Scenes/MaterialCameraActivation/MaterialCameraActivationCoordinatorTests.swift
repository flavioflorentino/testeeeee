import XCTest
import UI
import UIKit

@testable import PicPayEmpresas

final class MaterialCameraActivationCoordinatorTests: XCTestCase {
    private lazy var controllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: controllerSpy)
    private lazy var sut: MaterialCameraActivationCoordinator = {
        let coordinator = MaterialCameraActivationCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenReceiveActionSuccessFromPresenter_ShouldPushedController() {
        sut.perform(action: .activationSuccess)
        
        XCTAssertTrue(navigationSpy.currentViewController is SolicitationSuccessViewController)
    }
    
    func testPerform_WhenReceiveActionCloseFromPresenter_ShouldPopController() {
        sut.perform(action: .close)
        
        XCTAssertEqual(navigationSpy.popedViewControllerCount, 1)
    }
    
    func testPerform_WhenReceiveActionPopupErrorFromPresenter_ShouldPresentController() {
        let popupError = UI.PopupViewController(title: "Titulo Teste")
        sut.perform(action: .showPopupError(controller: popupError))
        
        XCTAssertEqual(controllerSpy.presentViewControllerCounter, 1)
        XCTAssertEqual(controllerSpy.presentViewController, popupError)
    }
    
    func testPerform_WhenReceiveActionPopupFromPresenter_ShouldPresentController() {
        let popupError = UI.PopupViewController(title: "Titulo Teste")
        sut.perform(action: .showPopup(controller: popupError))
        
        XCTAssertEqual(controllerSpy.presentViewControllerCounter, 1)
        XCTAssertEqual(controllerSpy.presentViewController, popupError)
    }
    
    func testPerform_WhenReceiveActionDismissFromPresenter_ShouldDismissController() {
        sut.perform(action: .dismiss)
        XCTAssertEqual(controllerSpy.dismissCount, 1)
    }
}
