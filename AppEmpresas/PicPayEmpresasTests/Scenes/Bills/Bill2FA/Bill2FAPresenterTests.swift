import XCTest
@testable import PicPayEmpresas

final private class Bill2FACoordinatorSpy: Bill2FACoordinating {
    var viewController: UIViewController?

    private(set) var actionSelected: Bill2FAAction?
    private(set) var actionCount = 0

    func perform(action: Bill2FAAction) {
        actionSelected = action
        actionCount += 1
    }
}

final private class Bill2FADisplaySpy: Bill2FADisplay {
    private(set) var statusCount = 0
    private(set) var displayLoadingCount = 0
    private(set) var dismissLoadingCount = 0
    private(set) var displayResendLoadingText: NSAttributedString?
    private(set) var displayResendLoadingCount = 0
    private(set) var dismissResendLoadingCount = 0
    private(set) var displayTitleText: String?
    private(set) var displayTitleCount = 0
    private(set) var displayDescriptionText: NSAttributedString?
    private(set) var displayDescriptionCount = 0
    private(set) var displayTimeText: NSAttributedString?
    private(set) var displayTimeCount = 0
    private(set) var displayKeyboardCount = 0
    private(set) var enableResendCount = 0
    private(set) var displayError: String?
    private(set) var displayErrorCount = 0
    private(set) var dismissKeyboardCount = 0
    private(set) var withoutNetworkCount = 0
    
    func resetStatus() {
        statusCount += 1
    }
    
    func displayLoading() {
        displayLoadingCount += 1
    }
    
    func dismissLoading() {
        dismissLoadingCount += 1
    }
    
    func displayResendLoading(text: NSAttributedString) {
        displayResendLoadingText = text
        displayResendLoadingCount += 1
    }
    
    func dismissResendLoading() {
        dismissResendLoadingCount += 1
    }
    
    func displayTitle(title: String) {
        displayTitleText = title
        displayTitleCount += 1
    }
    
    func displayDescription(description: NSAttributedString) {
        displayDescriptionText = description
        displayDescriptionCount += 1
    }
    
    func displayTime(time: NSAttributedString) {
        displayTimeText = time
        displayTimeCount += 1
    }
    
    func displayKeyboard() {
        displayKeyboardCount += 1
    }
    
    func enableResend() {
        enableResendCount += 1
    }
    
    func displayError(_ message: String) {
        displayError = message
        displayErrorCount += 1
    }
    
    func dismissKeyboard() {
        dismissKeyboardCount += 1
    }
    
    func withoutNetwork() {
        withoutNetworkCount += 1
    }
}

final class Bill2FAPresenterTests: XCTestCase {
    private let coordinatorSpy =  Bill2FACoordinatorSpy()
    private let displaySpy = Bill2FADisplaySpy()
    private let textMock = ""

    private lazy var sut: Bill2FAPresenter = {
        let presenter = Bill2FAPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testResetStatus_WhenReceiveResetStatusFromViewModel_ShouldResetStatus() {
        sut.resetStatus()
        XCTAssertEqual(displaySpy.statusCount, 1)
    }
    
    func testDisplayLoading_WhenReceiveDisplayLoadingFromViewModel_ShouldDisplayLoading() {
        sut.displayLoading()
        XCTAssertEqual(displaySpy.displayLoadingCount, 1)
    }
    
    func testDismissLoading_WhenReceiveDismissLoadingFromViewModel_ShouldDismissLoading() {
        sut.dismissLoading()
        XCTAssertEqual(displaySpy.dismissLoadingCount, 1)
    }
    
    func testResendLoading_WhenReceiveResendLoadingFromViewModel_ShouldDisplayResendLoading() {
        let textMock = "Aguarde..."
        
        sut.displayResendLoading()
        XCTAssertEqual(displaySpy.displayResendLoadingCount, 1)
        XCTAssertEqual(displaySpy.displayResendLoadingText?.string, textMock)
    }
    
    func testDismissResendLoading_WhenReceiveDismissResendLoadingFromViewModel_ShouldDismissResendLoading() {
        sut.dismissResendLoading()
        XCTAssertEqual(displaySpy.dismissResendLoadingCount, 1)
    }
    
    func testDisplayError_WhenReceiveDisplayErrorFromViewModel_ShouldDisplayError() {
        sut.displayError()
        XCTAssertEqual(displaySpy.displayErrorCount, 1)
        XCTAssertEqual(displaySpy.displayError, "Opa! Erramos por aqui. Por favor, tente novamente.")
    }

    func testDisplayInvalid_WhenReceiveDisplayErrorFromViewModel_ShouldDisplayError() {
        sut.displayInvalid()
        XCTAssertEqual(displaySpy.displayErrorCount, 1)
        XCTAssertEqual(displaySpy.displayError, "O código informado não corresponde ao enviado por SMS para seu celular.")
    }
    
    func testDisplayTitle_WhenReceiveDisplayRegularFromViewModel_ShouldDisplayTitle() {
        let titleMock = "Digite o código de autorização"
        
        sut.displayRegular(textMock)
        XCTAssertEqual(displaySpy.displayTitleCount, 1)
        XCTAssertEqual(displaySpy.displayTitleText, titleMock)
    }
    
    func testDisplayDescription_WhenReceiveDisplayRegularFromViewModel_ShouldDisplayDescription() {
        let descriptionMock = "Digite o código de autorização"
        
        sut.displayRegular(textMock)
        XCTAssertEqual(displaySpy.displayTitleCount, 1)
        XCTAssertEqual(displaySpy.displayTitleText, descriptionMock)
    }
    
    func testDisplayTitle_WhenReceiveDisplayLastChanceFromViewModel_ShouldDisplayLastChanceTitle() {
        let titleMock = "Atenção! Última tentativa"
        
        sut.displayLastChance(textMock)
        XCTAssertEqual(displaySpy.displayTitleCount, 1)
        XCTAssertEqual(displaySpy.displayTitleText, titleMock)
    }
    
    func testDisplayDescription_WhenReceiveDisplayLastChanceFromViewModel_ShouldLastChanceDescription() {
        let descriptionMock = "Insira o código do SMS enviado para o número . Se você atingir o limite de tentativas, o pagamento de boletos será temporariamente bloqueado."
        
        sut.displayLastChance(textMock)
        XCTAssertEqual(displaySpy.displayDescriptionCount, 1)
        XCTAssertEqual(displaySpy.displayDescriptionText?.string, descriptionMock)
    }
    
    func testDisplayTime_WhenReceiveDisplayTimeFromViewModel_ShouldDisplayTime() {
        let timeMock = 60
        let messageMock = "Não recebeu? Aguarde 60s"
        
        sut.displayTime(timeMock)
        XCTAssertEqual(displaySpy.displayTimeCount, 1)
        XCTAssertEqual(displaySpy.displayTimeText?.string, messageMock)
    }
    
    func testDisplayKeyboard_WhenReceiveDisplayKeyboardFromViewModel_ShouldDisplayKeyboard() {
        sut.displayKeyboard()
        XCTAssertEqual(displaySpy.displayKeyboardCount, 1)
    }
    
    func testEnableResend_WhenReceiveEnableResendFromViewModel_ShouldEnableResend() {
        sut.enableResend()
        XCTAssertEqual(displaySpy.enableResendCount, 1)
    }
    
    func testNextStep_WhenReceiveDismissFromViewModel_ShouldDismissNavigationController() {
        sut.didNextStep(action: .dismiss)
        XCTAssertEqual(coordinatorSpy.actionCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .dismiss)
    }
    
    func testNextStep_WhenReceiveBlockedFromViewModel_ShouldPresentANavigationController() {
        sut.didNextStep(action: .blocked(delegate: nil))
        XCTAssertEqual(coordinatorSpy.actionCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .blocked(delegate: nil))
    }
    
    func testNextStep_WhenReceiveResendFromViewModel_ShouldPresentADialog() {
        let modalInfo = AlertModalInfo()
        sut.didNextStep(action: .resend(modalInfo: modalInfo))
        XCTAssertEqual(coordinatorSpy.actionCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .resend(modalInfo: modalInfo))
    }
    
    func testDismissKeyboard_WhenReceiveDismissKeyboardFromViewModel_ShouldDismissKeyboard() {
        sut.dismissKeyboard()
        XCTAssertEqual(displaySpy.dismissKeyboardCount, 1)
    }
    
    func testDisplayExpired_WhenReceiveDisplayExpiredFromViewModel_ShouldDisplayExpiredError() {
        let messageMock = "O código digitado está expirado. Aguarde a chegada de um novo código por SMS."
        
        sut.displayExpired()
        XCTAssertEqual(displaySpy.displayErrorCount, 1)
        XCTAssertEqual(displaySpy.displayError, messageMock)
    }
    
    func testDisplayResendCodeError_WhenReceiveDisplayResendCodeErrorFromViewModel_ShouldDisplayADialog() {
        let modalInfo = AlertModalInfo()
        
        sut.displayResendCodeError()
        XCTAssertEqual(coordinatorSpy.actionCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .resend(modalInfo: modalInfo))
    }

    func testShowApiError_WhenReceiveShowApiErrorFromViewModel_ShouldDisplayADialog() {
        let modalInfo = AlertModalInfo()

        sut.showApiError(title: "titulo", description: "descrição")
        XCTAssertEqual(coordinatorSpy.actionCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .resend(modalInfo: modalInfo))
    }

    func testShowApiError_WhenReceiveShowApiErrorWithoutDescriptionFromViewModel_ShouldDisplayADialog() {
        let modalInfo = AlertModalInfo()

        sut.showApiError(title: "titulo", description: nil)
        XCTAssertEqual(coordinatorSpy.actionCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .resend(modalInfo: modalInfo))
    }
    
    func testDisplayNetworkError_WhenReceiveDisplayNetworkErrorFromViewModel_ShouldDisplayAnAlert() {
        sut.displayNetworkError()
        XCTAssertEqual(displaySpy.withoutNetworkCount, 1)
    }
}
