import Core
import XCTest
@testable import PicPayEmpresas

final private class Bill2FAPresenterSpy: Bill2FAPresenting {
    var viewController: Bill2FADisplay?
    
    private(set) var actionSelected: Bill2FAAction?
    private(set) var actionCount = 0
    private(set) var resetStatusCount = 0
    private(set) var displayRegularNumber: String?
    private(set) var displayRegularCount = 0
    private(set) var displayLastChanceNumber: String?
    private(set) var displayLastChanceCount = 0
    private(set) var displayTimeNumber: Int?
    private(set) var displayTimeCount = 0
    private(set) var displayKeyboardCount = 0
    private(set) var displayLoadingCount = 0
    private(set) var dismissLoadingCount = 0
    private(set) var displayResendLoadingCount = 0
    private(set) var dismissResendLoadingCount = 0
    private(set) var displayErrorCount = 0
    private(set) var displayInvalidCount = 0
    private(set) var enableResendCount = 0
    private(set) var dismissKeyboardCount = 0
    private(set) var displayExpiredCount = 0
    private(set) var displayResendCodeErrorCount = 0
    private(set) var showApiErrorTitle: String?
    private(set) var showApiErrorDescription: String?
    private(set) var showApiErrorCount = 0
    private(set) var displayNetworkErrorCount = 0
    
    func didNextStep(action: Bill2FAAction) {
        actionSelected = action
        actionCount += 1
    }
    
    func resetStatus() {
        resetStatusCount += 1
    }
    
    func displayRegular(_ number: String) {
        displayRegularNumber = number
        displayRegularCount += 1
    }
    
    func displayLastChance(_ number: String) {
        displayLastChanceNumber = number
        displayLastChanceCount += 1
    }
    
    func displayTime(_ time: Int) {
        displayTimeNumber = time
        displayTimeCount += 1
    }
    
    func displayKeyboard() {
        displayKeyboardCount += 1
    }
    
    func displayLoading() {
        displayLoadingCount += 1
    }
    
    func dismissLoading() {
        dismissLoadingCount += 1
    }
    
    func displayResendLoading() {
        displayResendLoadingCount += 1
    }
    
    func dismissResendLoading() {
        dismissResendLoadingCount += 1
    }
    
    func displayError() {
        displayErrorCount += 1
    }

    func displayInvalid() {
        displayInvalidCount += 1
    }
    
    func enableResend() {
        enableResendCount += 1
    }
    
    func dismissKeyboard() {
        dismissKeyboardCount += 1
    }
    
    func displayExpired() {
        displayExpiredCount += 1
    }
    
    func displayResendCodeError() {
        displayResendCodeErrorCount += 1
    }
    
    func showApiError(title: String?, description: String?) {
        showApiErrorTitle = title
        showApiErrorDescription = description
        showApiErrorCount += 1
    }
    
    func displayNetworkError() {
        displayNetworkErrorCount += 1
    }
}

final private class Bill2FAServiceMock: Bill2FAServicing {
    
    var isSuccess = true
    
    var resultSuccess: Bill2FAMock.Success = .payBilletWithData
    var resultError: Bill2FAMock.Error = .verifyCode
    
    func sendSMS(completion: @escaping (Result<Bill2FAResponse, ApiError>) -> Void) {
        guard isSuccess else {
            completion(.failure(ApiError.serverError))
            return
        }

        completion(.success(resultSuccess.resendCode()))
    }
    
    func payBillet(params: PayBillet2FAParams, completion: @escaping PayBilletCompletion) {
        guard isSuccess else {
            completion(.failure(resultError.verifyCode()))
            return
        }

        completion(.success(resultSuccess.payBillet()))
    }
}

final class Bill2FADelegateMock: Bill2FADelegate {
    var factorAuthenticatorPasswordCount = 0
    var resultSMSFactorCount = 0
    var resultSMSFactorId: String?
    var finishFlowCount = 0
    
    func factorAuthenticatorPassword() {
        factorAuthenticatorPasswordCount += 1
    }
    
    func resultSMSFactor(id: String) {
        resultSMSFactorCount += 1
        resultSMSFactorId = id
    }
    
    func finishFlow() {
        finishFlowCount += 1
    }
}

final class Bill2FAViewModelTests: XCTestCase {
    private let serviceMock = Bill2FAServiceMock()
    private let presenterSpy = Bill2FAPresenterSpy()
    private let codeMock = "123456"
    private let phoneMock = "(1*) **** 2345"
    private let billetMock = BillPay(
        code: "",
        description: "",
        beneficiary: "",
        expirationDate: "",
        isExpired: false,
        initialValue: "",
        fee: "",
        totalValue: 0,
        instuctions: "",
        data: []
    )
    private let delegateMock = Bill2FADelegateMock()
    private lazy var sut = Bill2FAViewModel(
        service: serviceMock,
        presenter: presenterSpy,
        billet: billetMock,
        phone: phoneMock,
        delegate: delegateMock
    )
    
    func testResetStatus_WhenReceiveResetStatusFromViewController_ShouldResetStatus() {
        sut.resetStatus()
        XCTAssertEqual(presenterSpy.resetStatusCount, 1)
    }
    
    func testDisplayRegular_WhenReceivePrepareForReceiveCodeFromViewController_ShouldDisplayRegular() {
        sut.prepareForReceiveCode()
        XCTAssertEqual(presenterSpy.displayRegularCount, 1)
        XCTAssertEqual(presenterSpy.displayRegularNumber, phoneMock)
    }
    
    func testDisplayTime_WhenReceivePrepareForReceiveCodeFromViewController_ShouldDisplayTime() {
        sut.prepareForReceiveCode()
        XCTAssertEqual(presenterSpy.displayTimeCount, 1)
        XCTAssertEqual(presenterSpy.displayTimeNumber, 60)
    }
    
    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplaySuccess() {
        serviceMock.isSuccess = true
        serviceMock.resultSuccess = .payBilletWithData
        
        sut.verifyCode(codeMock)
        XCTAssertEqual(presenterSpy.displayLoadingCount, 1)
        XCTAssertEqual(presenterSpy.dismissLoadingCount, 1)
        XCTAssertEqual(presenterSpy.actionCount, 1)
        XCTAssertEqual(delegateMock.resultSMSFactorCount, 1)
        XCTAssertEqual(delegateMock.resultSMSFactorId, "")
        XCTAssertEqual(presenterSpy.actionSelected, .dismiss)
    }
    
    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplayErrorWhenReceiveDataNil() {
        serviceMock.isSuccess = true
        serviceMock.resultSuccess = .payBilletWithoutData
        
        sut.verifyCode(codeMock)
        XCTAssertEqual(presenterSpy.displayLoadingCount, 1)
        XCTAssertEqual(presenterSpy.dismissLoadingCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
    }
    
    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplayError() {
        serviceMock.isSuccess = false
        serviceMock.resultError = .verifyCode
        
        sut.verifyCode(codeMock)
        XCTAssertEqual(presenterSpy.displayLoadingCount, 1)
        XCTAssertEqual(presenterSpy.dismissLoadingCount, 1)
        XCTAssertEqual(presenterSpy.displayKeyboardCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
    }
    
    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplayNetworkErrorFromConnectionFailure() {
        serviceMock.isSuccess = false
        serviceMock.resultError = .verifyCodeConnectionFailure
        
        sut.verifyCode(codeMock)
        XCTAssertEqual(presenterSpy.displayLoadingCount, 1)
        XCTAssertEqual(presenterSpy.dismissLoadingCount, 1)
        XCTAssertEqual(presenterSpy.displayKeyboardCount, 1)
        XCTAssertEqual(presenterSpy.displayNetworkErrorCount, 1)
    }
    
    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplayNetworkErrorFromTimeout() {
        serviceMock.isSuccess = false
        serviceMock.resultError = .verifyCodeTimeout
        
        sut.verifyCode(codeMock)
        XCTAssertEqual(presenterSpy.displayLoadingCount, 1)
        XCTAssertEqual(presenterSpy.dismissLoadingCount, 1)
        XCTAssertEqual(presenterSpy.displayKeyboardCount, 1)
        XCTAssertEqual(presenterSpy.displayNetworkErrorCount, 1)
    }
    
    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplayErrorFromBadRequest() {
        serviceMock.isSuccess = false
        serviceMock.resultError = .verifyCodeBadRequest
        
        sut.verifyCode(codeMock)
        XCTAssertEqual(presenterSpy.displayLoadingCount, 1)
        XCTAssertEqual(presenterSpy.dismissLoadingCount, 1)
        XCTAssertEqual(presenterSpy.displayKeyboardCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
    }
    
    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplayApiErrorFromBadRequest() {
        serviceMock.isSuccess = false
        serviceMock.resultError = .verifyCodeBadRequestDecodable
        
        sut.verifyCode(codeMock)
        XCTAssertEqual(presenterSpy.displayLoadingCount, 1)
        XCTAssertEqual(presenterSpy.dismissLoadingCount, 1)
        XCTAssertEqual(presenterSpy.displayKeyboardCount, 1)
        XCTAssertEqual(presenterSpy.showApiErrorCount, 1)
    }
    
    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplayExpiredFromBadRequest() {
        serviceMock.isSuccess = false
        serviceMock.resultError = .verifyCodeBadRequestDecodableTokenExpired
        
        sut.verifyCode(codeMock)
        XCTAssertEqual(presenterSpy.displayLoadingCount, 1)
        XCTAssertEqual(presenterSpy.dismissLoadingCount, 1)
        XCTAssertEqual(presenterSpy.displayKeyboardCount, 1)
        XCTAssertEqual(presenterSpy.displayExpiredCount, 1)
        XCTAssertEqual(presenterSpy.dismissKeyboardCount, 1)
    }
    
    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplayInvalidFromBadRequest() {
        serviceMock.isSuccess = false
        serviceMock.resultError = .verifyCodeBadRequestDecodableTokenInvalid
        
        sut.verifyCode(codeMock)
        XCTAssertEqual(presenterSpy.displayLoadingCount, 1)
        XCTAssertEqual(presenterSpy.dismissLoadingCount, 1)
        XCTAssertEqual(presenterSpy.displayKeyboardCount, 1)
        XCTAssertEqual(presenterSpy.displayInvalidCount, 1)
    }
    
    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplayBlockedFromBadRequest() {
        serviceMock.isSuccess = false
        serviceMock.resultError = .verifyCodeBadRequestDecodableBlocked
        
        sut.verifyCode(codeMock)
        XCTAssertEqual(presenterSpy.displayLoadingCount, 1)
        XCTAssertEqual(presenterSpy.dismissLoadingCount, 1)
        XCTAssertEqual(presenterSpy.displayKeyboardCount, 1)
        XCTAssertEqual(presenterSpy.actionCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .blocked(delegate: nil))
    }
    
    func testVerifyCode_WhenReceiveVerifyCodeFromViewController_ShouldDisplayLastChanceFromBadRequest() {
        serviceMock.isSuccess = false
        serviceMock.resultError = .verifyCodeBadRequestDecodableLastChance
        
        sut.verifyCode(codeMock)
        XCTAssertEqual(presenterSpy.displayLoadingCount, 1)
        XCTAssertEqual(presenterSpy.dismissLoadingCount, 1)
        XCTAssertEqual(presenterSpy.displayKeyboardCount, 1)
        XCTAssertEqual(presenterSpy.displayLastChanceCount, 1)
        XCTAssertEqual(presenterSpy.displayLastChanceNumber, phoneMock)
    }

    func testResendCode_WhenReceiveResendCodeFromViewController_ShouldDisplaySuccess() {
        serviceMock.isSuccess = true
        serviceMock.resultSuccess = .resendCodeStatusSms
        
        sut.resendCode()
        XCTAssertEqual(presenterSpy.resetStatusCount, 1)
        XCTAssertEqual(presenterSpy.displayResendLoadingCount, 1)
        XCTAssertEqual(presenterSpy.dismissResendLoadingCount, 1)
        XCTAssertEqual(presenterSpy.displayRegularCount, 1)
        XCTAssertEqual(presenterSpy.displayRegularNumber, phoneMock)
        XCTAssertEqual(presenterSpy.displayTimeCount, 1)
        XCTAssertEqual(presenterSpy.displayTimeNumber, 60)
    }

    func testResendCode_WhenReceiveResendCodeFromViewController_ShouldDismissNavigationController() {
        serviceMock.isSuccess = true
        serviceMock.resultSuccess = .resendCodeStatusPassword
        
        sut.resendCode()
        XCTAssertEqual(presenterSpy.resetStatusCount, 1)
        XCTAssertEqual(presenterSpy.displayResendLoadingCount, 1)
        XCTAssertEqual(presenterSpy.dismissResendLoadingCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .dismiss)
        XCTAssertEqual(presenterSpy.actionCount, 1)
        XCTAssertEqual(delegateMock.factorAuthenticatorPasswordCount, 1)
    }

    func testFinishFlow_WhenReceiveFinishFlowFromViewController_ShouldDismissNavigationController() {
        sut.finish()

        XCTAssertEqual(presenterSpy.actionCount, 1)
        XCTAssertEqual(presenterSpy.actionSelected, .dismiss)
        XCTAssertEqual(delegateMock.finishFlowCount, 1)
    }

    func testResendCode_WhenReceiveResendCodeFromViewController_ShouldDisplayError() {
        serviceMock.isSuccess = false
        sut.resendCode()
        XCTAssertEqual(presenterSpy.resetStatusCount, 1)
        XCTAssertEqual(presenterSpy.displayResendLoadingCount, 1)
        XCTAssertEqual(presenterSpy.dismissResendLoadingCount, 1)
        XCTAssertEqual(presenterSpy.displayResendCodeErrorCount, 1)
        XCTAssertEqual(presenterSpy.enableResendCount, 1)
    }
}
