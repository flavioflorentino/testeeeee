import Core
import XCTest
@testable import PicPayEmpresas

enum Bill2FAMock {
    
    enum Error {
        case verifyCodeConnectionFailure
        case verifyCodeTimeout
        case verifyCodeBadRequest
        case verifyCodeBadRequestDecodable
        case verifyCodeBadRequestDecodableTokenExpired
        case verifyCodeBadRequestDecodableTokenInvalid
        case verifyCodeBadRequestDecodableBlocked
        case verifyCodeBadRequestDecodableLastChance
        case verifyCode
        
        func verifyCode() -> ApiError {
            switch self {
            case .verifyCodeConnectionFailure:
                return .connectionFailure
            case .verifyCodeTimeout:
                return .timeout
            case .verifyCodeBadRequest:
                return .badRequest(body: RequestError())
            case .verifyCodeBadRequestDecodable:
                var requestError = RequestError()
                requestError.jsonData = Mocker().data("bill_2fa_verify_code_bad_request")
                return .badRequest(body: requestError)
            case .verifyCodeBadRequestDecodableTokenExpired:
                var requestError = RequestError()
                requestError.jsonData = Mocker().data("bill_2fa_verify_code_token_expired")
                return .badRequest(body: requestError)
            case .verifyCodeBadRequestDecodableTokenInvalid:
                var requestError = RequestError()
                requestError.jsonData = Mocker().data("bill_2fa_verify_code_token_invalid")
                return .badRequest(body: requestError)
            case .verifyCodeBadRequestDecodableBlocked:
                var requestError = RequestError()
                requestError.jsonData = Mocker().data("bill_2fa_verify_code_blocked")
                return .badRequest(body: requestError)
            case .verifyCodeBadRequestDecodableLastChance:
                var requestError = RequestError()
                requestError.jsonData = Mocker().data("bill_2fa_verify_code_token_last_chance")
                return .badRequest(body: requestError)
            case .verifyCode:
                return .serverError
            }
        }
        
    }
    
    enum Success {
        case resendCodeStatusSms
        case resendCodeStatusPassword
        case payBilletWithData
        case payBilletWithoutData
        
        func resendCode() -> Bill2FAResponse {
            let detail: Bill2FADetailResponse
            switch self {
            case .resendCodeStatusSms:
                detail = Bill2FADetailResponse(mobilePhone: "(1*) **** 2345", method: .sms)
            case .resendCodeStatusPassword:
                detail = Bill2FADetailResponse(mobilePhone: "(1*) **** 2345", method: .password)
            default:
                detail = Bill2FADetailResponse(mobilePhone: "", method: .password)
            }
            return Bill2FAResponse(detail: detail, status: "", success: true)
        }
        
        func payBillet() -> ResponseBiz<BillPayResponse> {
            switch self {
            case .payBilletWithData:
                let data = BillPayResponse(id: "")
                return ResponseBiz<BillPayResponse>(meta: nil, data: data)
            default:
                return ResponseBiz<BillPayResponse>(meta: nil, data: nil)
            }
        }
    }
}
