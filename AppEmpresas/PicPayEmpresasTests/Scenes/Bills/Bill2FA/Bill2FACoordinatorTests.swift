import XCTest
@testable import PicPayEmpresas

final class Bill2FACoordinatorTests: XCTestCase {
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: UIViewController())
    
    private lazy var sut: Bill2FACoordinator = {
        let coordinator = Bill2FACoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testDismiss_WhenReceiveActionDismissFromPresenter_ShouldDismissNavigation() {
        sut.perform(action: .dismiss)
        XCTAssertEqual(navigationSpy.dismissedViewControllerCount, 1)
    }
    
    func testBlocked_WhenReceiveActionBlockedFromPresenter_ShouldPresentANavigationController() {
        sut.perform(action: .blocked(delegate: nil))
        XCTAssertEqual(navigationSpy.presentCount, 1)
    }
    
    func testResend_WhenReceiveActionResendFromPresenter_ShouldPresentADialog() {
        let alertModalInfoMock = AlertModalInfo()
        sut.perform(action: .resend(modalInfo: alertModalInfoMock))
        XCTAssertEqual(navigationSpy.presentCount, 1)
    }
}
