import XCTest
@testable import PicPayEmpresas

private final class BillPaymentMethodsPresenterSpy: BillPaymentMethodsPresenting {
    private(set) var didCallNextStepScanner = 0
    private(set) var didCallNextStepType = 0
    
    func didNextStep(action: BillPaymentMethodsAction) {
        switch action {
        case .scanner:
            didCallNextStepScanner += 1
        case .type:
            didCallNextStepType += 1
        }
    }
}

final class BillPaymentMethodsViewModelTests: XCTestCase {
    private let presenterSpy = BillPaymentMethodsPresenterSpy()
    private let container = DependencyContainerMock()
    
    private lazy var sut: BillPaymentMethodsViewModel = {
        let viewModel = BillPaymentMethodsViewModel(
            presenter: presenterSpy,
            dependencies: container
        )
        return viewModel
    }()
    
    func testOpenTypeMethod_WhenCalled_ShouldCallNextStep() {
        sut.openTypeMethod()
        XCTAssertEqual(presenterSpy.didCallNextStepType, 1)
    }
    
    func testOpenScannerMethod_WhenCalled_ShouldCallNextStep() {
        sut.openScannerMethod()
        XCTAssertEqual(presenterSpy.didCallNextStepScanner, 1)
    }
}
