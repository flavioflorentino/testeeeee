import XCTest
@testable import PicPayEmpresas

private final class BillPaymentMethodsCoordinatorSpy: BillPaymentMethodsCoordinating {
    var viewController: UIViewController?
    
    private(set) var didCallScanner = 0
    private(set) var didCallType = 0
    private(set) var action: BillPaymentMethodsAction?
    
    func perform(action: BillPaymentMethodsAction) {
        switch action {
        case .scanner:
            didCallScanner += 1
        case .type:
            didCallType += 1
        }
        self.action = action
    }
}

final class BillPaymentMethodsPresenterTests: XCTestCase {
    private let coordinatorSpy = BillPaymentMethodsCoordinatorSpy()
    private lazy var sut: BillPaymentMethodsPresenter = {
        let presenter = BillPaymentMethodsPresenter(coordinator: coordinatorSpy)
        return presenter
    }()
    
    func testDidNextStep_WhenCalledByPresenter_ShouldPresentScanner() {
        sut.didNextStep(action: .scanner)
        
        XCTAssertEqual(coordinatorSpy.didCallScanner, 1)
        XCTAssertNotNil(coordinatorSpy.action)
    }
    
    func testDidNextStep_WhenCalledByPresenter_ShouldPresentType() {
        sut.didNextStep(action: .type)
        
        XCTAssertEqual(coordinatorSpy.didCallType, 1)
        XCTAssertNotNil(coordinatorSpy.action)
    }
}
