import XCTest
@testable import PicPayEmpresas

final class FillableValueBilletCoordinatorTests: XCTestCase {
    private lazy var viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    private lazy var sut: BillCustomValueCoordinator = {
        let coordinator = BillCustomValueCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testCheckBillet_WhenReceiveActionFromPresenter_ShouldShowCheckBilletScreen() {
        let checkPaymentBillet = BillPay(
            code: "",
            description: "",
            beneficiary: "",
            expirationDate: "",
            isExpired: false,
            initialValue: "",
            fee: "",
            totalValue: 0,
            instuctions: "",
            data: [])
        
        sut.perform(action: .checkPayment(billet: checkPaymentBillet))
        
        XCTAssertTrue(navigationSpy.currentViewController is BillPayViewController)
    }
}
