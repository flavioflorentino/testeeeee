import XCTest
@testable import PicPayEmpresas

private final class BillCustomValueCoordinatorMock: BillCustomValueCoordinating {
    var viewController: UIViewController?
    private(set) var goToCheckPaymentScreen = 0
    
    func perform(action: BillCustomValueAction) {
        switch action {
        case .checkPayment(_):
            goToCheckPaymentScreen += 1
        }
    }
}

private final class BillCustomValueViewControllerSpy: UIViewController, BillCustomValueDisplay {
    private(set) var didChangeEnableValueConfirmButton = 0
    private(set) var confirmButtonEnable = false
    
    func changeEnableValueConfirmButton(enable: Bool) {
        didChangeEnableValueConfirmButton += 1
        confirmButtonEnable = enable
    }
}

final class FillableValueBilletPresenterTests: XCTestCase {
    private let viewControllerSpy = BillCustomValueViewControllerSpy()
    private let coordinatorMock = BillCustomValueCoordinatorMock()
    
    private lazy var sut: BillCustomValuePresenter = {
        let presenter = BillCustomValuePresenter(coordinator: coordinatorMock)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidTapNextStep_WhenSuccess_ShouldGoToCheckPaymentScreen() {
        let checkPaymentBillet = BillPay(
            code: "",
            description: "",
            beneficiary: "",
            expirationDate: "",
            isExpired: false,
            initialValue: "",
            fee: "",
            totalValue: 0,
            instuctions: "",
            data: [])
        
        sut.didNextStep(action: .checkPayment(billet: checkPaymentBillet))
        
        XCTAssertEqual(coordinatorMock.goToCheckPaymentScreen, 1)
    }
    
    func testChangeEnableValueConfirmButton_WhenSuccess_ShouldEnableConfirmButton() {
        sut.changeEnableValueConfirmButton(enable: true)
        
        guard let viewController = sut.viewController as? BillCustomValueViewControllerSpy else {
            XCTFail("Test should show invalid billet code.")
            return
        }
        
        XCTAssertEqual(viewController.didChangeEnableValueConfirmButton, 1)
        XCTAssertTrue(viewController.confirmButtonEnable)
    }
    
    
    func testChangeEnableValueConfirmButton_WhenFail_ShouldDisableConfirmButton() {
        sut.changeEnableValueConfirmButton(enable: false)
        
        guard let viewController = sut.viewController as? BillCustomValueViewControllerSpy else {
            XCTFail("Test should show invalid billet code.")
            return
        }
        
        XCTAssertEqual(viewController.didChangeEnableValueConfirmButton, 1)
        XCTAssertFalse(viewController.confirmButtonEnable)
    }
}
