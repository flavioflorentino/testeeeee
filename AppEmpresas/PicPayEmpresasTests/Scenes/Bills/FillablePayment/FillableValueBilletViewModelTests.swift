import XCTest
@testable import PicPayEmpresas

private final class BillCustomValuePresenterSpy: BillCustomValuePresenting {
    var viewController: BillCustomValueDisplay?
    
    private(set) var didChangeEnableValueConfirmButton = 0
    private(set) var didPresentBilletBlockMask = 0
    private(set) var confirmButtonEnable = false
    private(set) var didShowCheckPayment = 0
    
    func changeEnableValueConfirmButton(enable: Bool) {
        didChangeEnableValueConfirmButton += 1
        confirmButtonEnable = enable
    }
    
    func didNextStep(action: BillCustomValueAction) {
        switch action {
        case .checkPayment(_):
            didShowCheckPayment += 1
        }
    }
}

final class FillableValueBilletViewModelTests: XCTestCase {
    private let presenterSpy = BillCustomValuePresenterSpy()
    
    private lazy var sut: BillCustomValueViewModel = {
        let checkPaymentBillet = BillPay(
            code: "",
            description: "",
            beneficiary: "",
            expirationDate: "",
            isExpired: false,
            initialValue: "",
            fee: "",
            totalValue: 0,
            instuctions: "",
            data: [])
        
        return BillCustomValueViewModel(presenter: presenterSpy, billet: checkPaymentBillet, dependencies: DependencyContainerMock())
    }()
    
    func testChangeEnableValueConfirmButton_WhenSuccess_ShouldEnableConfirmButton() {
        sut.checkEnableConfirmButton(value: 1234.56)
        
        XCTAssertEqual(presenterSpy.didChangeEnableValueConfirmButton, 1)
        XCTAssertTrue(presenterSpy.confirmButtonEnable)
    }
    
    func testChangeEnableValueConfirmButton_WhenFail_ShouldDisableConfirmButton() {
        sut.checkEnableConfirmButton(value: 0.0)
        
        XCTAssertEqual(presenterSpy.didChangeEnableValueConfirmButton, 1)
        XCTAssertFalse(presenterSpy.confirmButtonEnable)
    }
    
    func testDidNextStep_WhenSuccess_ShouldGoToCheckPaymentViewController() {
        sut.confirmButtonTouched()
        
        XCTAssertEqual(presenterSpy.didShowCheckPayment, 1)
    }
}
