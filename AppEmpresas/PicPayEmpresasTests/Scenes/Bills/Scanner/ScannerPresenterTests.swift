import XCTest
@testable import PicPayEmpresas

private final class ScannerViewControllerSpy: ScannerDisplay {
    private(set) var didCallStartLoading = 0
    private(set) var didCallStopLoading = 0
    private(set) var didCallShowError = 0
    
    func startLoading() {
        didCallStartLoading += 1
    }
    
    func stopLoading() {
        didCallStopLoading += 1
    }
    
    func showError() {
        didCallShowError += 1
    }
}

private final class ScannerCoordinatorSpy: ScannerCoordinating {
    var viewController: UIViewController?
    
    private(set) var didCallFormAction = 0
    private(set) var didCallCloseAction = 0
    private(set) var code: String?
    
    func perform(action: ScannerAction) {
        switch action {
        case .form(let code):
            self.code = code
            didCallFormAction += 1
        case .close:
            didCallCloseAction += 1
        }
    }
    
}

final class ScannerPresenterTests: XCTestCase {
    private let viewControllerSpy = ScannerViewControllerSpy()
    private let coordinatorSpy = ScannerCoordinatorSpy()
    
    private lazy var sut: ScannerPresenter = {
        let presenter = ScannerPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testClose_WhenCalledByViewController_ShouldCallCoordinator() {
        
        sut.close()
        XCTAssertEqual(coordinatorSpy.didCallCloseAction, 1)
    }
    
    func testStartLoading_WhenCalledByViewModel_ShouldCAllViewController() {
        sut.startLoading()
        
        XCTAssertEqual(viewControllerSpy.didCallStartLoading, 1)
    }
    
    func testStopLoading_WhenCalledByViewModel_ShouldCAllViewController() {
        sut.stopLoading()
        
        XCTAssertEqual(viewControllerSpy.didCallStopLoading, 1)
    }
    
    func testGoToBillForm_WhenGivenCode_ShouldStopLoadingAndCallCoordinator() {
        let code = "84630000002049902962020061550800000189194665"
        sut.goToBillForm(lineCode: code)
        
        XCTAssertEqual(viewControllerSpy.didCallStopLoading, 1)
        XCTAssertEqual(coordinatorSpy.code, code)
        XCTAssertEqual(coordinatorSpy.didCallFormAction, 1)
    }
    
    func testShowError_WhenCalledByViewModel_ShouldCallViewController() {
        sut.showError()
        
        XCTAssertEqual(viewControllerSpy.didCallShowError, 1)
    }
}
