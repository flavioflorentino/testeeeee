import XCTest
@testable import PicPayEmpresas

private enum ScannerServiceResponse {
    case success
    case failure
    case invalidResponse
}

private final class ScannerServiceMock: ScannerServicing {
    var response: ScannerServiceResponse = .success
    
    let billetData = BilletData(
        billType: "Convenio",
        beneficiary: Beneficiary(
            code: nil,
            limitHour: "18:00",
            name: nil,
            fantasyName: "NET SERVICOS",
            imageUrl: nil
        ),
        amount: "204.99",
        amountNominal: nil,
        expired: false,
        paymentDate: "04/06/2020",
        dueDate: nil,
        fillable: false,
        barCode: "84630000002049902962020061550800000189194665",
        lineCode: "846300000029049902962023006155080002001891946657",
        codeType: "Linha Digitável",
        instructions: "A compensação pode ocorrer...",
        surcharge: "2.99",
        sellerId: 14316,
        cip: Cip(amount: 204.99,
                 interest: nil,
                 canReceiveOverdue: false,
                 paymentValue: nil,
                 dateSearch: "04/06/2020 09:06:57",
                 isRegisterValid: false
        )
    )
    
    func getLineCode(code: String, completion: @escaping BarcodeCompletionBlock) {
        switch response {
        case .failure:
            completion(.failure(.serverError))
        case .success:
            completion(.success(billetData))
        case .invalidResponse:
            completion(.success(nil))
        }
    }
}

private final class ScannerPresenterSpy: ScannerPresenting {
    weak var viewController: ScannerDisplay?
    private(set) var lineCode = ""
    
    private(set) var didCallGoToBillForm = 0
    private(set) var didCallStartLoading = 0
    private(set) var didCallStopLoading = 0
    private(set) var didCallShowError = 0
    private(set) var didCallClose = 0
    
    func goToBillForm(lineCode: String) {
        self.lineCode = lineCode
        didCallGoToBillForm += 1
    }
    
    func startLoading() {
        didCallStartLoading += 1
    }
    
    func stopLoading() {
        didCallStopLoading += 1
    }
    
    func close() {
        didCallClose += 1
    }
    
    func showError() {
        didCallShowError += 1
    }
}

final class ScannerViewModelTests: XCTestCase {
    private let container = DependencyContainerMock()
    private let serviceMock = ScannerServiceMock()
    private let presenterSpy = ScannerPresenterSpy()
    
    private lazy var sut = ScannerViewModel(service: serviceMock, presenter: presenterSpy, dependencies: container)
    
    func testGoToType_WhenButtonButtonTypeTapped_ShouldCallPresenter() {
        sut.goToType()
        
        XCTAssertEqual(presenterSpy.didCallGoToBillForm, 1)
    }
    
    func testVerifyCode_WhenResponseSuccess_ShouldGetLineCode() {
        serviceMock.response = .success
        
        sut.verifyCode(code: "84630000002049902962020061550800000189194665")
        
        XCTAssertEqual(presenterSpy.didCallGoToBillForm, 1)
    }
    
    func testVerifyCode_WhenResponseFail_ShouldGetLineCode() {
        serviceMock.response = .failure
        
        sut.verifyCode(code: "84630000002049902962020061550800000189194665")
        
        XCTAssertEqual(presenterSpy.didCallShowError, 1)
    }
    
    func testVerifyCode_WhenInvalidResponse_ShouldCallError() {
        serviceMock.response = .invalidResponse
        
        sut.verifyCode(code: "8463000000204990296202")
        
        XCTAssertEqual(presenterSpy.didCallShowError, 1)
    }
    
    func testVerifyCode_WhenNotPassingCode_ShouldNotProcceed() {
        serviceMock.response = .success
        
        sut.verifyCode(code: nil)
        
        XCTAssertEqual(presenterSpy.didCallShowError, 0)
        XCTAssertEqual(presenterSpy.didCallGoToBillForm, 0)
    }
}
