import Core
import XCTest
@testable import PicPayEmpresas

final class BillFormViewModelTests: XCTestCase {
    private let container = DependencyContainerMock()
    private let serviceMock = BillFormServiceMock()
    private let presenterSpy = BillFormPresenterSpy()
    private var isAutomaticWithdrawalInitiallyEnabled: Bool? = true
    
    private lazy var sut: BillFormViewModel = {
        let viewModel = BillFormViewModel(service: serviceMock, presenter: presenterSpy, dependencies: container, code: "")
        return viewModel
    }()
    
    func testValidateBillet_WhenTheBilletCodeFieldChanges_ShouldPresentValidCode() {
        sut.validateBillet(with: "846900000056049602962026004100880006002338502863", replacementText: "")
        
        XCTAssertEqual(presenterSpy.didValidateBilletCode, 1)
        XCTAssertTrue(presenterSpy.billetCodeValid)
    }
    
    func testValidateBillet_WhenTheBilletCodeFieldChanges_ShouldPresentInvalidCode() {
        sut.validateBillet(with: "72345763478523784523453728452387453845883452", replacementText: "")
        
        XCTAssertEqual(presenterSpy.didValidateBilletCode, 1)
        XCTAssertFalse(presenterSpy.billetCodeValid)
    }
    
    func testSetBilletMask_WhenTheBilletCodeFieldChanges_ShouldPresentBilletCodeWithBlockMask() {
        let textView = UIPPFloatingTextView()
        textView.setupBilletMask()
        
        let billetCode = "03399.81961 50840.600006 15914.501018 4 82210000057475"
        sut.setBilletMask(text: "", range: NSRange(), replacementText: "03399819615084060000615914501018482210000057475", superview: textView)
        
        XCTAssertEqual(presenterSpy.didPresentBilletBlockMask, 1)
        XCTAssertEqual(presenterSpy.billetCode, billetCode)
    }

    func testVerifyBilletPayment_WhenSuccessNotFillableAndNotExpired_ShouldReturnBillet() {
        serviceMock.shouldFail = false
        serviceMock.expired = false
        serviceMock.fillable = false
        
        sut.verifyBillForm(with: "", description: "")
        
        XCTAssertEqual(presenterSpy.didStartLoading, 1)
        XCTAssertEqual(presenterSpy.didStopLoading, 1)
        XCTAssertEqual(presenterSpy.didShowCheckPayment, 1)
    }
    
    func testVerifyBilletPayment_WhenSuccessFillableAndNotExpired_ShouldPresentFillableScreen() {
        serviceMock.shouldFail = false
        serviceMock.expired = false
        serviceMock.fillable = true
        
        sut.verifyBillForm(with: "", description: "")
        
        XCTAssertEqual(presenterSpy.didStartLoading, 1)
        XCTAssertEqual(presenterSpy.didStopLoading, 1)
        XCTAssertEqual(presenterSpy.didShowFillableBillet, 1)
    }
    
    func testVerifyBilletPayment_WhenSuccessNotFillableAndExpired_ShouldPresentExpiredScreen() {
        serviceMock.shouldFail = false
        serviceMock.expired = true
        serviceMock.fillable = false
    
        sut.verifyBillForm(with: "", description: "")
        
        XCTAssertEqual(presenterSpy.didStartLoading, 1)
        XCTAssertEqual(presenterSpy.didStopLoading, 1)
        XCTAssertEqual(presenterSpy.didPresentUnacceptedOverduePayment, 1)
    }
    
    func testVerifyBilletPayment_WhenFail_ShouldReturnError() {
        serviceMock.shouldFail = true
        serviceMock.expired = false
        serviceMock.fillable = false
        
        
        sut.verifyBillForm(with: "", description: "")
        
        XCTAssertEqual(presenterSpy.didStartLoading, 1)
        XCTAssertEqual(presenterSpy.didStopLoading, 1)
        XCTAssertEqual(presenterSpy.didPresentErrorVerifyingPayment, 1)
        XCTAssertEqual(presenterSpy.errorMessage, RequestError().message)
    }
    
    func testChangeActionButtonAvailability_WithSuccess_ShouldEnableActionButton() {
        sut.validateBillet(with: "846900000056049602962026004100880006002338502863", replacementText: "")
        
        XCTAssertEqual(presenterSpy.didChangeActionButtonAviability, 1)
        XCTAssertTrue(presenterSpy.actionButtonAvailable)
    }
    
    func testChangeActionButtonAvailability_WithFail_ShouldDisableActionButton() {
        sut.validateBillet(with: "84690000005604960296202600410088063", replacementText: "")
        
        XCTAssertEqual(presenterSpy.didChangeActionButtonAviability, 1)
        XCTAssertFalse(presenterSpy.actionButtonAvailable)
    }
}

private final class BillFormPresenterSpy: BillFormPresenting {
    var viewController: BillFormDisplay?
    
    private (set) var didValidateBilletCode = 0
    private (set) var didPresentBilletBlockMask = 0
    private (set) var didShowFillableBillet = 0
    private (set) var didShowCheckPayment = 0
    private (set) var didStartLoading = 0
    private (set) var didStopLoading = 0
    private (set) var didCallSetTextFieldCode = 0
    private (set) var didChangeActionButtonAviability = 0
    private (set) var didPresentUnacceptedOverduePayment = 0
    private (set) var didPresentErrorVerifyingPayment = 0
    private (set) var errorMessage: String = ""
    private (set) var billetCode: String = ""
    private (set) var billetCodeValid: Bool = false
    private (set) var billetDescription: String = ""
    private (set) var actionButtonAvailable: Bool = false
    
    func stopLoading() {
        didStopLoading += 1
    }
    
    func setTextFieldCode(code: String) {
        didCallSetTextFieldCode += 1
    }
    
    func startLoading() {
        didStartLoading += 1
    }
    
    func validatedBilletCode(_ billetValid: Bool) {
        didValidateBilletCode += 1
        billetCodeValid = billetValid
    }
    
    func presentBilletBlockMask(blockMask: String) {
        didPresentBilletBlockMask += 1
        billetCode = blockMask
    }
    
    func presentUnacceptedOverduePayment() {
        didPresentUnacceptedOverduePayment += 1
    }
    
    func presentErrorVerifyingPayment(message: String) {
        didPresentErrorVerifyingPayment += 1
        errorMessage = message
    }
    
    func didNextStep(action: BillFormAction) {
        switch action {
        case .fillableBillet(_):
            didShowFillableBillet += 1
        case .checkPayment(_):
            didShowCheckPayment += 1
        default:
            break
        }
    }
    
    func changeActionButtonAvailability(_ enable: Bool) {
        didChangeActionButtonAviability += 1
        actionButtonAvailable = enable
    }
}

private final class BillFormServiceMock: BillFormServicing {
    var shouldFail = false
    var expired = false
    var fillable = false
    
    func verifyPayment(code: String, billetDescription: String, completion: @escaping (Result<BilletData?, ApiError>) -> Void) {
        
        if shouldFail {
            completion(.failure(.badRequest(body: RequestError())))
        } else {
            let mock = CheckBilletPaymentMock()
            var mockBillet = mock.getBillet()
            
            if expired {
                mockBillet = mock.getExpiredBillet()
            }
            
            if fillable {
                mockBillet = mock.getFillableBillet()
            }
            
            guard let billet = mockBillet else {
                completion(.failure(.bodyNotFound))
                return
            }
            
            completion(.success(billet))
        }
    }
}
