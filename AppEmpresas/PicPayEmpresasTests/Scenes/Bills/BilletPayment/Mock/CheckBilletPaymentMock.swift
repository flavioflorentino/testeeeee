import XCTest
@testable import PicPayEmpresas

final class CheckBilletPaymentMock {
    func getBillet() -> BilletData? {
        LocalMock<BilletData>().localMock("billet")
    }
    
    func getFillableBillet() -> BilletData? {
        LocalMock<BilletData>().localMock("fillableBillet")
    }
    
    func getExpiredBillet() -> BilletData? {
        LocalMock<BilletData>().localMock("expiredBillet")
    }
}
