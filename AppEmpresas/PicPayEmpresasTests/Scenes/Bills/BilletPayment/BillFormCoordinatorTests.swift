import XCTest
@testable import PicPayEmpresas

final class BillFormCoordinatorTests: XCTestCase {
    private lazy var viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    private lazy var sut: BillFormCoordinator = {
        let coordinator = BillFormCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testShowAlert_WhenReceiveActionFromPresenter_ShouldShowAlert() {
        sut.perform(action: .showAlert(title: "", message: "", buttonTitle: ""))
        
        XCTAssertTrue(viewControllerSpy.presentViewController is PopupViewController)
    }
    
    func testShowErrorView_WhenReceiveActionFromPresenter_ShouldShowErrorView() {
        sut.perform(action: .showAlert(title: "", message: "", buttonTitle: ""))
        
        XCTAssertTrue(viewControllerSpy.presentViewController is PopupViewController)
    }
    
    func testShowFillableBillet_WhenReceiveActionFromPresenter_ShouldShowFillableBilletScreen() {
        let checkPaymentBillet = BillPay(
                                            code: "",
                                            description: "",
                                            beneficiary: "",
                                            expirationDate: "",
                                            isExpired: false,
                                            initialValue: "",
                                            fee: "",
                                            totalValue: 0,
                                            instuctions: "",
                                            data: [])
        
        sut.perform(action: .fillableBillet(billet: checkPaymentBillet))
        
        XCTAssertTrue(navigationSpy.currentViewController is BillCustomValueViewController)
    }
    
    func testCheckBillet_WhenReceiveActionFromPresenter_ShouldShowCheckBilletScreen() {
        let checkPaymentBillet = BillPay(
                                            code: "",
                                            description: "",
                                            beneficiary: "",
                                            expirationDate: "",
                                            isExpired: false,
                                            initialValue: "",
                                            fee: "",
                                            totalValue: 0,
                                            instuctions: "",
                                            data: [])
        
        sut.perform(action: .checkPayment(billet: checkPaymentBillet))
        
        XCTAssertTrue(navigationSpy.currentViewController is BillPayViewController)
    }
    
    func testBack_WhenReceiveActionFromPresenter_ShouldPopToRootViewController() {
        sut.perform(action: .back)
        
        XCTAssertEqual(navigationSpy.popedViewControllerCount, 1)
    }
}
