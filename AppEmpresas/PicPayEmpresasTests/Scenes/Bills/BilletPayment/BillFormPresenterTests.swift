import XCTest
@testable import PicPayEmpresas

final class BillFormPresenterTests: XCTestCase {
    private let viewControllerSpy = BillFormViewControllerSpy()
    private let coordinatorMock = BillFormCoordinatorMock()
    
    private lazy var sut: BillFormPresenter = {
        let presenter = BillFormPresenter(coordinator: coordinatorMock)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidTapBack_WhenSuccess_ShouldPopViewController() {
        sut.didNextStep(action: .dismiss)

        XCTAssertEqual(coordinatorMock.popViewController, 1)
    }
    
    func testDidTapDismiss_WhenSuccess_ShouldDismissViewController() {
        sut.didNextStep(action: .back)

        XCTAssertEqual(coordinatorMock.dismissViewController, 1)
    }

    func testDidTapNextStep_WhenSuccessNotFillableAndNotExpired_ShouldGoToCheckPaymentScreen() {
        let checkPaymentBillet = BillPay(
                                                code: "",
                                                description: "",
                                                beneficiary: "",
                                                expirationDate: "",
                                                isExpired: false,
                                                initialValue: "",
                                                fee: "",
                                                totalValue: 0,
                                                instuctions: "",
                                                data: [])
        
        sut.didNextStep(action: .checkPayment(billet: checkPaymentBillet))

        XCTAssertEqual(coordinatorMock.goToCheckPaymentScreen, 1)
    }
    
    func testDidTapNextStep_WhenSuccessFillableAndNotExpired_ShouldGoToCheckPaymentScreen() {
        let checkPaymentBillet = BillPay(
                                                code: "",
                                                description: "",
                                                beneficiary: "",
                                                expirationDate: "",
                                                isExpired: false,
                                                initialValue: "",
                                                fee: "",
                                                totalValue: 0,
                                                instuctions: "",
                                                data: [])
        
        sut.didNextStep(action: .fillableBillet(billet: checkPaymentBillet))

        XCTAssertEqual(coordinatorMock.goToFillableValueScreen, 1)
    }

    func testPresentInvalidBilletCode_WhenSuccess_ShouldShowInvalidBilletCode() {
        sut.validatedBilletCode(false)

        guard let viewController = sut.viewController as? BillFormViewControllerSpy else {
            XCTFail("Test should show invalid billet code.")
            return
        }
        
        XCTAssertEqual(viewController.didPresentInvalidBilletCode, 1)
        XCTAssertEqual(viewController.messageInvalidBilletCode, "Número errado, verifique o código digitado.")
    }
    
    func testPresentValidBilletCode_WhenSuccess_ShouldShowValidBilletCode() {
        sut.validatedBilletCode(true)

        guard let viewController = sut.viewController as? BillFormViewControllerSpy else {
            XCTFail("Test should show valid billet code.")
            return
        }
        
        XCTAssertEqual(viewController.didPresentValidBilletCode, 1)
    }
    
    func testPresnetBilletBlockMask_WhenSuccess_ShouldPresentBilletBlockMask() {
        sut.presentBilletBlockMask(blockMask: "blockMask")
        
        guard let viewController = sut.viewController as? BillFormViewControllerSpy else {
            XCTFail("Test should show block masked billet code.")
            return
        }
        
        XCTAssertEqual(viewController.didPresentBilletBlockMask, 1)
        XCTAssertEqual(viewController.maskedBlock, "blockMask")
    }
    
    func testPresentErrorVerifyingPayment_WhenSucess_ShouldShowErrorAlert() {
        sut.presentErrorVerifyingPayment(message: "Error verifying payment.")
        
        XCTAssertEqual(coordinatorMock.showAlert, 1)
    }
    
    func testPresentUnacceptedOverduePayment_WhenSuccess_ShouldPresentUnacceptedOverduePayment() {
        sut.presentUnacceptedOverduePayment()
        
        XCTAssertEqual(coordinatorMock.showAlert, 1)
    }
    
    func testStartLoading_WhenSucess_ShouldStartLoadingAnimation() {
        sut.startLoading()
        
        guard let viewController = sut.viewController as? BillFormViewControllerSpy else {
            XCTFail("Test should show invalid billet code.")
            return
        }
        
        XCTAssertEqual(viewController.didStartLoadingAnimation, 1)
    }
    
    func testStopLoading_WhenSucess_ShouldStopLoadingAnimation() {
        sut.stopLoading()
        
        guard let viewController = sut.viewController as? BillFormViewControllerSpy else {
            XCTFail("Test should show invalid billet code.")
            return
        }
        
        XCTAssertEqual(viewController.didStoptLoadingAnimation, 1)
    }
    
    func testEnableActionButton_WhenSucess_ShouldEnableActionButton() {
        sut.changeActionButtonAvailability(true)
        
        guard let viewController = sut.viewController as? BillFormViewControllerSpy else {
            XCTFail("Test should show invalid billet code.")
            return
        }
        
        XCTAssertEqual(viewController.didEnableActionButton, 1)
        XCTAssertTrue(viewController.isActionButtonEnabled)
    }
    
    func testEnableActionButton_WhenFail_ShouldDisableActionButton() {
        sut.changeActionButtonAvailability(false)
        
        guard let viewController = sut.viewController as? BillFormViewControllerSpy else {
            XCTFail("Test should show invalid billet code.")
            return
        }
        
        XCTAssertEqual(viewController.didEnableActionButton, 1)
        XCTAssertFalse(viewController.isActionButtonEnabled)
    }
}

private final class BillFormCoordinatorMock: BillFormCoordinating {
    var viewController: UIViewController?
    
    private(set) var goToCheckPaymentScreen = 0
    private(set) var goToFillableValueScreen = 0
    private(set) var popViewController = 0
    private(set) var dismissViewController = 0
    private(set) var showAlert = 0
    
    func perform(action: BillFormAction) {
        switch action {
        case .showAlert(_,_, _):
            showAlert += 1
        case .checkPayment:
            goToCheckPaymentScreen += 1
        case .fillableBillet:
            goToFillableValueScreen += 1
        case .dismiss:
            popViewController += 1
        case .back:
            dismissViewController += 1
        }
    }
}

private final class BillFormViewControllerSpy: UIViewController, BillFormDisplay {
    
    private(set) var didPresentInvalidBilletCode = 0
    private(set) var didPresentValidBilletCode = 0
    private(set) var didPresentBilletBlockMask = 0
    private(set) var didShowAlertError = 0
    private(set) var didStoptLoadingAnimation = 0
    private(set) var didStartLoadingAnimation = 0
    private(set) var didCallSetTextFieldCode = 0
    private(set) var didEnableActionButton = 0
    private(set) var messageInvalidBilletCode = ""
    private(set) var maskedBlock = ""
    private(set) var alertErrorMessage = ""
    private(set) var isActionButtonEnabled = false
    
    func prepareViewForPresent() { }
    
    func stopLoadingAnimation() {
        didStoptLoadingAnimation += 1
    }
    
    func setTextFieldCode(code: String) {
        didCallSetTextFieldCode += 1
    }
    
    func startLoadingAnimation() {
        didStartLoadingAnimation += 1
    }
    
    func presentInvalidBilletCode(_ message: String) {
        didPresentInvalidBilletCode += 1
        messageInvalidBilletCode = message
    }
    
    func presentValidBilletCode() {
        didPresentValidBilletCode += 1
    }
    
    func presentBilletBlockMask(maskedBlock: String) {
        didPresentBilletBlockMask += 1
        self.maskedBlock = maskedBlock
    }
    
    func enableActionButton(_ enable: Bool) {
        didEnableActionButton += 1
        isActionButtonEnabled = enable
    }
}
