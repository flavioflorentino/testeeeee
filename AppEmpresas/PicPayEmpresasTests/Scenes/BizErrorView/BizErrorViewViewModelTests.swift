import XCTest
@testable import PicPayEmpresas

private final class BizErrorViewPresenterSpy: BizErrorViewPresenting {
    var viewController: BizErrorViewDisplay?
    var errorView: BizErrorViewLayout?

    //MARK: - setupView
    private(set) var setupViewCallsCount = 0

    func setupView() {
        setupViewCallsCount += 1
    }

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [BizErrorViewAction] = []

    func didNextStep(action: BizErrorViewAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }
}

private final class BizErrorViewViewModelTests: XCTestCase {
    private let presenterSpy = BizErrorViewPresenterSpy()
    
    lazy var sut = BizErrorViewViewModel(presenter: presenterSpy)
    
    func testSetupView_ShouldCallViewSetup() {
        sut.setupView()
        
        XCTAssertEqual(presenterSpy.setupViewCallsCount, 1)
    }
    
    func testCloseScreen_ShouldCallCloseScreen() {
        sut.closeScreen()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(.closeAction))
    }
    
    func testButtonAction_ShouldCallButtonAction() {
        sut.buttonAction()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertTrue(presenterSpy.didNextStepActionReceivedInvocations.contains(.buttonAction))
    }
}
