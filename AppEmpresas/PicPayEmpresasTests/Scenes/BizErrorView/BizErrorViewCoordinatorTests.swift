import XCTest
@testable import PicPayEmpresas

private final class BizErrorViewCoordinatorDelegateSpy: BizErrorViewCoordinatorDelegate {

    //MARK: - buttonAction
    private(set) var buttonActionCallsCount = 0

    func buttonAction() {
        buttonActionCallsCount += 1
    }

    //MARK: - closeAction
    private(set) var closeActionCallsCount = 0

    func closeAction() {
        closeActionCallsCount += 1
    }
}

private final class BizErrorViewCoordinatorTests: XCTestCase {
    let errorCoordinatorDelegate = BizErrorViewCoordinatorDelegateSpy()
    
    lazy var sut: BizErrorViewCoordinating = {
        let coordinator = BizErrorViewCoordinator()
        coordinator.errorCoordinatorDelegate = errorCoordinatorDelegate
        return coordinator
    }()
    
    func testPerform_WhenActionButton_ShouldCallButtonClosure() {
        sut.perform(action: .buttonAction)
        
        XCTAssertEqual(errorCoordinatorDelegate.buttonActionCallsCount, 1)
    }
    
    func testPerform_WhenActionClose_ShouldCallCloseClosure() {
        sut.perform(action: .closeAction)
        
        XCTAssertEqual(errorCoordinatorDelegate.closeActionCallsCount, 1)
    }
}
