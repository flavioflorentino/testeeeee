import XCTest
@testable import PicPayEmpresas

private final class BizErrorViewCoordinatorSpy: BizErrorViewCoordinating {
    var errorCoordinatorDelegate: BizErrorViewCoordinatorDelegate?

    //MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [BizErrorViewAction] = []

    func perform(action: BizErrorViewAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

private final class BizErrorViewViewControllerSpy: BizErrorViewDisplay {
    //MARK: - setTitle
    private(set) var setTitleCallsCount = 0
    private(set) var setTitleReceivedInvocations: [String?] = []

    func setTitle(_ title: String?) {
        setTitleCallsCount += 1
        setTitleReceivedInvocations.append(title)
    }

    //MARK: - setMessage
    private(set) var setMessageCallsCount = 0
    private(set) var setMessageReceivedInvocations: [String?] = []

    func setMessage(_ message: String?) {
        setMessageCallsCount += 1
        setMessageReceivedInvocations.append(message)
    }

    //MARK: - setImage
    private(set) var setImageCallsCount = 0
    private(set) var setImageReceivedInvocations: [UIImage?] = []

    func setImage(_ image: UIImage?) {
        setImageCallsCount += 1
        setImageReceivedInvocations.append(image)
    }

    //MARK: - setButtonTitle
    private(set) var setButtonTitleCallsCount = 0
    private(set) var setButtonTitleReceivedInvocations: [String?] = []

    func setButtonTitle(_ buttonTitle: String?) {
        setButtonTitleCallsCount += 1
        setButtonTitleReceivedInvocations.append(buttonTitle)
    }
}

private final class BizErrorViewPresenterTests: XCTestCase {
    lazy var coordinatorSpy = BizErrorViewCoordinatorSpy()
    let viewControllerSpy = BizErrorViewViewControllerSpy()
    let errorView = BizErrorViewLayout(
        title: "title",
        message: "message",
        image: Assets.Emoji.iconBad.image,
        buttonTitle: "button title"
    )
    
    lazy var sut: BizErrorViewPresenting = {
        let presenter = BizErrorViewPresenter(coordinator: coordinatorSpy, errorView: errorView)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testSetupView_ShouldDisplayConfiguredView() {
        sut.setupView()
        
        let viewConfig = BizErrorViewLayout(
            title: "title",
            message: "message",
            image: Assets.Emoji.iconBad.image,
            buttonTitle: "button title"
        )
        
        XCTAssertEqual(viewControllerSpy.setTitleCallsCount, 1)
        XCTAssertTrue(viewControllerSpy.setTitleReceivedInvocations.contains(viewConfig.title))
        
        XCTAssertEqual(viewControllerSpy.setMessageCallsCount, 1)
        XCTAssertTrue(viewControllerSpy.setMessageReceivedInvocations.contains(viewConfig.message))
        
        XCTAssertEqual(viewControllerSpy.setImageCallsCount, 1)
        XCTAssertTrue(viewControllerSpy.setImageReceivedInvocations.contains(viewConfig.image))
        
        XCTAssertEqual(viewControllerSpy.setButtonTitleCallsCount, 1)
        XCTAssertTrue(viewControllerSpy.setButtonTitleReceivedInvocations.contains(viewConfig.buttonTitle))
    }
    
    func testDidNextStep_WhenAnyAction_ShouldPerformRespectiveActionInCoordinator() {
        sut.didNextStep(action: .closeAction)
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertTrue(coordinatorSpy.performActionReceivedInvocations.contains(.closeAction))
    }
}
