import XCTest
@testable import PicPayEmpresas

final class SuccessQRCodePrintCoordinatorTests: XCTestCase {
    private let navigationMock = NavigationControllerSpy(rootViewController: UIViewController())
    
    private lazy var sut: SuccessQRCodePrintCoordinator = {
        let coordinator = SuccessQRCodePrintCoordinator()
        coordinator.viewController = navigationMock.topViewController
        return coordinator
    }()
    
    func testPerformWhenCalledFromPresentWithBackActionShouldPopViewController() {
        sut.perform(action: .back)
        
        XCTAssertEqual(navigationMock.popedViewControllerCount, 1)
    }
    
    func testPerformWhenCalledFromPresentWithBackRootActionShouldPopRootViewController() {
        sut.perform(action: .backRoot)
        
        XCTAssertEqual(navigationMock.popToRoot, 1)
    }
}
