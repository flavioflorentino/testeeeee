import XCTest
@testable import PicPayEmpresas

final class SuccessQRCodePrintPresenterTests: XCTestCase {
    private let coordinatorSpy = SuccessCoordinatorSpy()
    
    private lazy var sut = SuccessQRCodePrintPresenter(coordinator: coordinatorSpy)
    
    func testDidNextStepWhenCalledFromViewModelWhitBackActionShouldCallCoordinatorAction() {
        sut.didNextStep(action: .back)
        
        XCTAssertNotNil(coordinatorSpy.action)
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
    }
    
    func testDidNextStepWhenCalledFromViewModelWhitBackRootActionShouldCallCoordinatorAction() {
        sut.didNextStep(action: .backRoot)
        
        XCTAssertNotNil(coordinatorSpy.action)
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
    }
}

private final class SuccessCoordinatorSpy: SuccessQRCodePrintCoordinating {
    var viewController: UIViewController?
    
    private(set) var action: SuccessQRCodePrintAction?
    private(set) var callPerformCount: Int = 0
    
    func perform(action: SuccessQRCodePrintAction) {
        self.action = action
        callPerformCount += 1
    }
}
