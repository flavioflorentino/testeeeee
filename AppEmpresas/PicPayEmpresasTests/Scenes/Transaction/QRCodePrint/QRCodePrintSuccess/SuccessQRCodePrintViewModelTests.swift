import XCTest
@testable import PicPayEmpresas

final class SuccessQRCodePrintViewModelTests: XCTestCase {
    private let service = SuccessQRCodePrintService()
    private let presenterSpy = SuccessPresenterSpy()
    
    private lazy var sut: SuccessQRCodePrintViewModel = {
        let viewModel = SuccessQRCodePrintViewModel(service: service, presenter: presenterSpy)
        return viewModel
    }()
    
    func testDidNextStepWhenCalledFromViewControllerWithBackActionShouldBackOneViewController() {
        sut.backScreen()
        
        XCTAssertEqual(presenterSpy.popViewControllerCount, 1)
    }
    
    func testDidNextStepWhenCalledFromViewControllerBackRootActionShouldBackAllViewControllers() {
        sut.backToRoot()
        
        XCTAssertEqual(presenterSpy.popRootViewControllerCount, 1)
    }
}

private final class SuccessPresenterSpy: SuccessQRCodePrintPresenting {
    var viewController: SuccessQRCodePrintDisplay?
    
    private(set) var popViewControllerCount: Int = 0
    private(set) var popRootViewControllerCount: Int = 0
    
    func didNextStep(action: SuccessQRCodePrintAction) {
        switch action {
        case .back:
            popViewControllerCount += 1
        case .backRoot:
            popRootViewControllerCount += 1
        }
    }
}
