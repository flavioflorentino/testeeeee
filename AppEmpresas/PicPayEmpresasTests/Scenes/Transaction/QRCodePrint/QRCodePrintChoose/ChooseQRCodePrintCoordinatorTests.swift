import XCTest
@testable import PicPayEmpresas

final class ChooseQRCodePrintCoordinatorTests: XCTestCase {
    private let navigationMock = NavigationControllerSpy(rootViewController: UIViewController())
    
    private lazy var sut: ChooseQRCodePrintCoordinator = {
        let coordinator = ChooseQRCodePrintCoordinator()
        coordinator.viewController = navigationMock.topViewController
        return coordinator
    }()
    
    func testPerformWhenCalledFromPresentWithSuccessShareActionShouldPushSuccessViewController() {
        sut.perform(action: .successShare)
        
        XCTAssertTrue(navigationMock.currentViewController is SuccessQRCodePrintViewController)
    }
    
    func testPerformWhenCalledFromPresentWithSuccessEmailActionShouldPushSuccessViewController() {
        sut.perform(action: .successEmail(email: "teste@picpay.com.br"))
        
        XCTAssertTrue(navigationMock.currentViewController is SuccessQRCodePrintViewController)
    }
}
