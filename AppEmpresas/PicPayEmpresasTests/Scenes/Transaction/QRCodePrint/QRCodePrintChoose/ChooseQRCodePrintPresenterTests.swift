import XCTest
import Core
@testable import PicPayEmpresas

final class ChooseQRCodePrintPresenterTests: XCTestCase {
    private let viewControllerSpy = ChooseDisplaySpy()
    private let coordinatorSpy = ChooseCoordinatorSpy()
    
    private lazy var sut: ChooseQRCodePrintPresenter = {
        let sut = ChooseQRCodePrintPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testOpenShareWhenCalledFromViewModelShouldReturnDataObjectNotNil() {
        let data = Data()
        
        sut.openShare(data: data)
        
        XCTAssertNotNil(viewControllerSpy.data)
    }
    
    func testHandleErrorWhenCalledFromViewModelShouldReturnMessageError() {
        let error = ApiError.serverError
        sut.handleError(error: error)
        
        XCTAssertNotNil(coordinatorSpy.action)
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
    }
    
    func testDidNextStepWithSuccessShareWhenCalledFormViewModelShouldCallCoordinatorAction() {
        sut.didNextStep(action: .successShare)
        
        XCTAssertNotNil(coordinatorSpy.action)
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
    }
    
    func testDidNextStepWithSuccessEmailWhenCalledFromViewModelShouldCallCoordinatorAction() {
        sut.didNextStep(action: .successEmail(email: "email@picpay.com.br"))
        
        XCTAssertNotNil(coordinatorSpy.action)
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
    }
    
    func testStartLoad_WhenCalledFromViewModel_ShouldShowLoadAnimation() {
        sut.startLoad()
        
        XCTAssertEqual(viewControllerSpy.startLoadCount, 1)
    }
}

private final class ChooseDisplaySpy: ChooseQRCodePrintDisplay {
    private(set) var callErrorCount = 0
    private(set) var error: String?
    private(set) var data: Data?
    private(set) var startLoadCount = 0
    private(set) var stopLoadCount = 0
    
    func openShare(data: Data) {
        self.data = data
    }
    
    func handleError(error: String) {
        self.error = error
        callErrorCount += 1
    }
    
    func startLoad() {
        startLoadCount += 1
    }
    
    func stopLoad() {
        stopLoadCount += 1
    }
}

private final class ChooseCoordinatorSpy: ChooseQRCodePrintCoordinating {
    var viewController: UIViewController?
    
    private(set) var action: ChooseQRCodePrintAction?
    private(set) var callPerformCount = 0
    
    func perform(action: ChooseQRCodePrintAction) {
        self.action = action
        callPerformCount += 1
    }
}
