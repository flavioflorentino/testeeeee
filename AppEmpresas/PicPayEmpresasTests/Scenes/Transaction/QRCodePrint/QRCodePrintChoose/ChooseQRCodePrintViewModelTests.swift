import XCTest
import Core
@testable import PicPayEmpresas

final class ChooseQRCodePrintViewModelTests: XCTestCase {
    private let service = ChooseQRCodePrintServiceMock()
    private let presenter = ChooseQRCodePrintPresenterSpy()
    private lazy var sut = ChooseQRCodePrintViewModel(service: service, presenter: presenter)
    
    func testSendEmailWhenReceiveSuccessFromServiceShouldReturnActionWithEmail() {
        service.isSuccess = true
        
        sut.didSelected(type: .email)
        
        XCTAssertNotNil(presenter.currentActionSelected)
        XCTAssertEqual(presenter.mailReceived, "mailteste@picpay.com")
        XCTAssertEqual(presenter.startLoadCount, 1)
        XCTAssertEqual(presenter.stopLoadCount, 1)
    }
    
    func testSendMailWhenReceiveFailureFromServiceShouldReturnAnApiError() {
        service.isSuccess = false
        
        sut.didSelected(type: .email)
        
        XCTAssertEqual(presenter.handleErrorCount, 1)
        XCTAssertNotNil(presenter.error)
        XCTAssertEqual(presenter.startLoadCount, 1)
        XCTAssertEqual(presenter.stopLoadCount, 1)
    }
    
    func testDownloadPDFWhenReceiveSuccessFromServiceShouldReturnADataObject() {
        service.isSuccess = true
        
        sut.didSelected(type: .share)
        
        XCTAssertNotNil(presenter.handleShareData)
        XCTAssertEqual(presenter.startLoadCount, 1)
        XCTAssertEqual(presenter.stopLoadCount, 1)
    }
    
    func testDownloadPDFWhenReceiveFailureFromServiceShouldReturnAnApiError() {
        service.isSuccess = false
        
        sut.didSelected(type: .share)
        XCTAssertEqual(presenter.handleErrorCount, 1)
        XCTAssertNotNil(presenter.error)
        XCTAssertEqual(presenter.startLoadCount, 1)
        XCTAssertEqual(presenter.stopLoadCount, 1)
    }
    
    func testOpenShareOptionWhenViewControllerCalledShouldReceiveShareAction() {
        sut.showShareScreenSuccess()
        
        XCTAssertNotNil(presenter.currentActionSelected)
    }
}

private final class ChooseQRCodePrintServiceMock: ChooseQRCodePrintServicing {    
    var isSuccess = true
    
    func downloadPDF(completion: @escaping (Result<Data, ApiError>) -> Void) {
        guard isSuccess else {
            completion(.failure(ApiError.serverError))
            return
        }
        
        completion(.success(Data()))
    }
    
    func sendEmail(completion: @escaping (Result<SendEmail?, ApiError>) -> Void) {
        guard isSuccess else {
            completion(.failure(ApiError.serverError))
            return
        }
        
        let mailModel = SendEmail(email: "mailteste@picpay.com")
        completion(.success(mailModel))
    }
}

private final class ChooseQRCodePrintPresenterSpy: ChooseQRCodePrintPresenting {
    var viewController: ChooseQRCodePrintDisplay?
    
    private(set) var currentActionSelected: ChooseQRCodePrintAction?
    private(set) var mailReceived: String?
    private(set) var handleShareData: Data?
    private(set) var error: ApiError?
    private(set) var handleErrorCount = 0
    private(set) var startLoadCount = 0
    private(set) var stopLoadCount = 0
    
    func didNextStep(action: ChooseQRCodePrintAction) {
        switch action {
        case .successEmail(let mail):
            self.mailReceived = mail
            self.currentActionSelected = action
        case .successShare, .error:
            self.currentActionSelected = action
        }
    }
    
    func openShare(data: Data) {
        self.handleShareData = data
    }
    
    func handleError(error: ApiError) {
        self.error = error
        self.handleErrorCount += 1
    }
    
    func startLoad() {
        startLoadCount += 1
    }
    
    func stopLoad() {
        stopLoadCount += 1
    }
}

