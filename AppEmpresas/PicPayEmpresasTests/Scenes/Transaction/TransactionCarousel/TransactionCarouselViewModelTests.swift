import XCTest
import Core
import FeatureFlag
@testable import PicPayEmpresas

private final class TransactionCarouselPresenterSpy: TransactionCarouselPresenting {
    var viewController: TransactionCarouselDisplay?
    
    private(set) var didCallSetTransactionItems = 0
    private(set) var transactionItems: [TransactionCarouselModel]?
    private(set) var didCallReloadCell = 0
    private(set) var reloadedIndexPath: IndexPath?
    private(set) var didCallShowPayBillet = 0
    private(set) var didShowCampaign = 0
    private(set) var didCallTranfer = 0
    private(set) var didCallPayBillet = 0
    private(set) var didCallShowPIX = 0
    private(set) var didCallMaterialSolicitation = 0
    private(set) var didCallPrintSolicitation = 0
    
    func setTransactionItems(_ items: [TransactionCarouselModel]) {
        didCallSetTransactionItems += 1
        transactionItems = items
    }
    
    func reloadCellAt(indexPath: IndexPath) {
        didCallReloadCell += 1
        reloadedIndexPath = indexPath
    }
    
    func showTransfer(hasBiometry: Bool) {
        didCallTranfer += 1
    }
    
    func showPayBillet(hasBiometry: Bool, firstInteraction: Bool, isBlocked: Bool) {
        didCallShowPayBillet += 1
    }
    
    func showPIX(seller: Seller, welcomePresented: Bool, pixAvailablePresented: Bool) {
        didCallShowPIX += 1
    }
    
    func showCreateCampaign() {
        didShowCampaign += 1
    }
    
    func showMaterialSolicitation() {
        didCallMaterialSolicitation += 1
    }
    
    func showPrintMaterial() {
        didCallPrintSolicitation += 1
    }
}

private final class TransactionCarouselServiceMock: TransactionCarouselServicing {
    var isPixAvaiablePresented = false
    var isWelcomePixPresented = false
    var isSuccess = true
    var isBlocked = false
    
    func isBlocked(completion: @escaping (Result<TransactionCarouselResponse, ApiError>) -> Void) {
        guard isSuccess else {
            completion(.failure(ApiError.serverError))
            return
        }

        let detail = TransactionCarouselDetailResponse(blocked: isBlocked)
        let response = TransactionCarouselResponse(detail: detail)
        completion(.success(response))
    }
}

final class TransactionCarouselViewModelTests: XCTestCase {
    private let userStatusModel = UserStatus(boardRequestRefused: false,
                                             boardStep: "teste",
                                             boardRequested: false,
                                             config: UserStatusConfig(blocked: nil),
                                             sellerID: 100)
    private let presenter = TransactionCarouselPresenterSpy()
    private let service = TransactionCarouselServiceMock()
    
    private let featureManagerMock: FeatureManagerMock = {
        let featureManagerMock = FeatureManagerMock()
        
        featureManagerMock.override(
            keys: .isBilletPaymentAvailable,
            .bill2FA,
            .allowQRCodeFlow,
            .allowApplyMaterial,
            .releaseCashbackPresentingBool,
            .isTransferCarouselButtonAvailable,
            with: false
        )
        
        return featureManagerMock
    }()
    
    private lazy var sut = TransactionCarouselViewModel(
        service: service,
        presenter: presenter,
        dependencies: DependencyContainerMock(featureManagerMock),
        hasBiometry: true,
        userStatus: userStatusModel,
        seller: seller
    )
    
    private lazy var seller: Seller = {
        var seller = Seller()
        seller.biometry = true
        seller.digitalAccount = true
        return seller
    }()
    
    func testLoadTransactionItems_WhenSuccess_ShouldPresentItems() {
        featureManagerMock.override(key: .isBilletPaymentAvailable, with: true)
        sut.loadTransactionItems()
        
        XCTAssertEqual(1, presenter.didCallSetTransactionItems)
        XCTAssertNotNil(presenter.transactionItems)
        XCTAssertEqual(1, presenter.transactionItems?.count)
        XCTAssertEqual(.payBillet, presenter.transactionItems?[0].type)
    }
    
    func testDidSelectCell_WhenIndex0_ShouldCallPayBilletAction() {
        featureManagerMock.override(key: .isBilletPaymentAvailable, with: true)
        let indexPath = IndexPath(row: 0, section: 0)
        sut.selectCellAt(indexPath: indexPath)
        
        XCTAssertEqual(1, presenter.didCallShowPayBillet)
        XCTAssertEqual(1, presenter.didCallReloadCell)
        XCTAssertEqual(indexPath, presenter.reloadedIndexPath)
    }

    func testFeatureFlagIsBilletPaymentAvailable_WhenIsEnable_ShouldShowBilletPaymentButton() throws {
        featureManagerMock.override(key: .isBilletPaymentAvailable, with: true)
        sut.loadTransactionItems()
        let transactionItems = try XCTUnwrap(presenter.transactionItems)
        XCTAssertTrue(transactionItems.contains { $0 is TransactionCarouselPayBillet })
    }

    func testFeatureFlagIsTransferCarouselButtonAvailable_WhenIsEnable_ShouldShowTransferButton() throws {
        featureManagerMock.override(key: .isTransferCarouselButtonAvailable, with: true)
        sut.loadTransactionItems()
        let transactionItems = try XCTUnwrap(presenter.transactionItems)
        XCTAssertTrue(transactionItems.contains { $0 is TransactionCarouselTransfer })
    }

    func testFeatureFlagIsTransferCarouselButtonAvailable_WhenIsDisable_ShouldHideTransferButton() throws {
        featureManagerMock.override(key: .isTransferCarouselButtonAvailable, with: false)
        sut.loadTransactionItems()
        let transactionItems = try XCTUnwrap(presenter.transactionItems)
        XCTAssertEqual(transactionItems.count, .zero)
    }
    
    func testFeatureFlagIsBilletPaymentAvailable_WhenIsDisableFlag_ShouldHideBilletPaymentButton() throws {
        featureManagerMock.override(key: .isBilletPaymentAvailable, with: false)
        sut.loadTransactionItems()
        let transactionItems = try XCTUnwrap(presenter.transactionItems)
        XCTAssertFalse(transactionItems.contains { $0 is TransactionCarouselPayBillet })
    }
    
    func testFeatureFlagIsBilletPaymentAvailable_WhenIsDisableBiometry_ShouldHideBilletPaymentButton() throws {
        featureManagerMock.override(key: .isBilletPaymentAvailable, with: true)
        seller.biometry = false
        seller.digitalAccount = true
        sut.loadTransactionItems()
        let transactionItems = try XCTUnwrap(presenter.transactionItems)
        XCTAssertFalse(transactionItems.contains { $0 is TransactionCarouselPayBillet })
    }
    
    func testFeatureFlagIsBilletPaymentAvailable_WhenIsDisableDigitalAccount_ShouldHideBilletPaymentButton() throws {
        featureManagerMock.override(key: .isBilletPaymentAvailable, with: true)
        seller.biometry = true
        seller.digitalAccount = false
        sut.loadTransactionItems()
        let transactionItems = try XCTUnwrap(presenter.transactionItems)
        XCTAssertFalse(transactionItems.contains { $0 is TransactionCarouselPayBillet })
    }
}
