import Core
import LegacyPJ
import SwiftyJSON
import XCTest
@testable import PicPayEmpresas

private class TransactionsListPresentingSpy: TransactionsListPresenting {
    var viewController: TransactionsListDisplaying?

    //MARK: - didFetchTransactions
    private(set) var didFetchTransactionsCallsCount = 0
    private(set) var didFetchTransactionsReceivedInvocations: [[Transaction]] = []

    func didFetchTransactions(_ transactions: [Transaction]) {
        didFetchTransactionsCallsCount += 1
        didFetchTransactionsReceivedInvocations.append(transactions)
    }

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [TransactionsListAction] = []

    func didNextStep(action: TransactionsListAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }

    //MARK: - prepareToReload
    private(set) var prepareToReloadCallsCount = 0

    func prepareToReload() {
        prepareToReloadCallsCount += 1
    }

    //MARK: - reloadTransactions
    private(set) var reloadTransactionsCallsCount = 0
    private(set) var reloadTransactionsReceivedInvocations: [[Transaction]] = []

    func reloadTransactions(_ transactions: [Transaction]) {
        reloadTransactionsCallsCount += 1
        reloadTransactionsReceivedInvocations.append(transactions)
    }

    //MARK: - showAlert
    private(set) var showAlertForIndexPathCallsCount = 0
    private(set) var showAlertForIndexPathReceivedInvocations: [(transaction: Transaction, indexPath: IndexPath)] = []

    func showAlert(for transaction: Transaction, indexPath: IndexPath) {
        showAlertForIndexPathCallsCount += 1
        showAlertForIndexPathReceivedInvocations.append((transaction: transaction, indexPath: indexPath))
    }

    //MARK: - fail
    private(set) var failCallsCount = 0
    private(set) var failReceivedInvocations: [String] = []

    func fail(_ message: String) {
        failCallsCount += 1
        failReceivedInvocations.append(message)
    }

    //MARK: - startLoading
    private(set) var startLoadingAtCallsCount = 0
    private(set) var startLoadingAtReceivedInvocations: [IndexPath] = []

    func startLoading(at indexPath: IndexPath) {
        startLoadingAtCallsCount += 1
        startLoadingAtReceivedInvocations.append(indexPath)
    }

    //MARK: - stopLoading
    private(set) var stopLoadingAtCallsCount = 0
    private(set) var stopLoadingAtReceivedInvocations: [IndexPath] = []

    func stopLoading(at indexPath: IndexPath) {
        stopLoadingAtCallsCount += 1
        stopLoadingAtReceivedInvocations.append(indexPath)
    }
}

private class TransactionsListServicingMock: TransactionsListServicing {

    //MARK: - fetchTransactions
    private(set) var fetchTransactionsPageCompletionCallsCount = 0
    private(set) var fetchTransactionsPageCompletionReceivedInvocations: [(page: Int?, completion: (Result<TransactionListResponse, LegacyPJError>) -> Void)] = []
    var fetchTransactionsPageCompletionClosure: ((Int?, @escaping (Result<TransactionListResponse, LegacyPJError>) -> Void) -> Void)?

    func fetchTransactions(page: Int?,
                           endpoint: TransactionsEndpoint,
                           completion: @escaping (Result<TransactionListResponse, LegacyPJError>) -> Void) {
        fetchTransactionsPageCompletionCallsCount += 1
        fetchTransactionsPageCompletionReceivedInvocations.append((page: page, completion: completion))
        fetchTransactionsPageCompletionClosure?(page, completion)
    }

    //MARK: - authenticate
    private(set) var authenticateCallsCount = 0
    private(set) var authenticateReceivedInvocations: [((_ result: AuthManager.PerformActionResult) -> Void)] = []
    var authenticateClosure: ((@escaping (_ result: AuthManager.PerformActionResult) -> Void) -> Void)?

    func authenticate(_ completion: @escaping (_ result: AuthManager.PerformActionResult) -> Void) {
        authenticateCallsCount += 1
        authenticateReceivedInvocations.append(completion)
        authenticateClosure?(completion)
    }

    //MARK: - cancelTransaction
    private(set) var cancelTransactionIdCompletionCallsCount = 0
    private(set) var cancelTransactionIdCompletionReceivedInvocations: [(pin: String, id: Int, completion: (Result<Void, LegacyPJError>) -> Void)] = []
    var cancelTransactionIdCompletionClosure: ((String, Int, @escaping (Result<Void, LegacyPJError>) -> Void) -> Void)?

    func cancelTransaction(_ pin: String, id: Int, completion: @escaping (Result<Void, LegacyPJError>) -> Void) {
        cancelTransactionIdCompletionCallsCount += 1
        cancelTransactionIdCompletionReceivedInvocations.append((pin: pin, id: id, completion: completion))
        cancelTransactionIdCompletionClosure?(pin, id, completion)
    }
}

final class TransactionsListInteractorTests: XCTestCase {
    private let mainQueue = DispatchQueue(label: "transactionListInteractorTests")
    private let timeout: Double = 3.0
    private let presenter = TransactionsListPresentingSpy()
    private let service = TransactionsListServicingMock()
    private lazy var sut = TransactionsListInteractor(
        service: service,
        presenter: presenter,
        dependencies: DependencyContainerMock(),
        type: .user
    )

    func testInitialFetch_WhenSuccess_ShouldListTransacitons() {
        service.fetchTransactionsPageCompletionClosure = { _, completion in
            self.mockTransactionsSuccess(completion)
        }

        sut.initialFetch()

        let expectation = self.expectation(description: "")
        mainQueue.asyncAfter(deadline: .now() + 0.1) {
            expectation.fulfill()
        }
        waitForExpectations(timeout: timeout)

        XCTAssertEqual(presenter.didFetchTransactionsCallsCount, 1)
        XCTAssertEqual(presenter.didFetchTransactionsReceivedInvocations.last?.count, 8)
    }

    func testRefresh_ShouldInvokePrepareToReloadFunctions() {
        sut.refresh()

        XCTAssertEqual(presenter.prepareToReloadCallsCount, 1)
    }

    func testeDidSelectTransaction_WhenIsNotPIX_ShouldPresentActionSheet() {
        service.fetchTransactionsPageCompletionClosure = { _, completion in
            self.mockTransactionsSuccess(completion)
        }

        sut.initialFetch()

        let expectation = self.expectation(description: "")
        mainQueue.asyncAfter(deadline: .now() + 0.1) {
            expectation.fulfill()
        }
        waitForExpectations(timeout: timeout)

        sut.didSelectTransaction(at: IndexPath(row: 0, section: 0))

        XCTAssertEqual(presenter.showAlertForIndexPathCallsCount, 1)
    }
    
    func testeDidSelectTransaction_WhenIsPIX_ShouldPresentReceipt() {
        service.fetchTransactionsPageCompletionClosure = { _, completion in
            self.mockTransactionsSuccess(completion)
        }

        sut.initialFetch()

        let expectation = self.expectation(description: "")
        mainQueue.asyncAfter(deadline: .now() + 0.1) {
            expectation.fulfill()
        }
        waitForExpectations(timeout: timeout)
        let transaction = Transaction()
        transaction.id = 2006501
        
        sut.openPixReceipt(transaction)

        let expectedAction = TransactionsListAction.presentPixReceipt(2006501, isRefundAllowed: true)
        XCTAssertEqual(presenter.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenter.didNextStepActionReceivedInvocations.first, expectedAction)
    }
}

private extension TransactionsListInteractorTests {
    func mockTransactionsSuccess(_ completion: @escaping (Result<TransactionListResponse, LegacyPJError>) -> Void) {
        guard let data = Mocker().data("transactions"),
        let json = try? JSON(data: data) else {
            return
        }

        var list: [Transaction] = [Transaction]()
        if let items = json["list"].array {
            for item in items {
                let transaction = Transaction(json: item)
                list.append(transaction)
            }
        }

        let pag = Pagination(json: json)

        completion(.success(TransactionListResponse(pagination: pag, list: list)))
    }
}
