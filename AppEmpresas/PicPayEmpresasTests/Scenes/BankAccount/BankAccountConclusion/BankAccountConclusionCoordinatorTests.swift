import Foundation
import XCTest
@testable import PicPayEmpresas

final private class BankAccountConclusionCoordinatorTests: XCTestCase {
    private lazy var viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    
    private lazy var sut: BankAccountConclusionCoordinating = {
        let sut = BankAccountConclusionCoordinator()
        sut.viewController = navigationSpy.topViewController
        return sut
    }()
    
    func testBackAction_ShouldPopViewController() {
        sut.perform(action: .back)
        
        XCTAssertEqual(navigationSpy.popToRoot, 1)
    }
}
