import Foundation
import XCTest
@testable import PicPayEmpresas

private class BankAccountConclusionCoordinatingSpy: BankAccountConclusionCoordinating {
    var viewController: UIViewController?

    //MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [BankAccountConclusionAction] = []

    func perform(action: BankAccountConclusionAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

private final class BankAccountConclusionPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = BankAccountConclusionCoordinatingSpy()
    
    private lazy var sut = BankAccountConclusionPresenter(coordinator: coordinatorSpy)
    
    func testDidNextStep_ShouldCallDidNextWithCorrectAction() {
        sut.didNextStep(action: .back)
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.performActionReceivedInvocations.first, .back)
    }
}
