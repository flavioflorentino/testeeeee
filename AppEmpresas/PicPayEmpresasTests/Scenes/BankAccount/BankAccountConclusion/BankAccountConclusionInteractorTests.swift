import Foundation
import XCTest
@testable import PicPayEmpresas

private class BankAccountConclusionPresentingSpy: BankAccountConclusionPresenting {
    var viewController: BankAccountConclusionDisplaying?

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [BankAccountConclusionAction] = []

    func didNextStep(action: BankAccountConclusionAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }
}

private final class BankAccountConclusionInteractorTests: XCTestCase {
    private lazy var presenterSpy = BankAccountConclusionPresentingSpy()
    
    private lazy var sut = BankAccountConclusionInteractor(presenter: presenterSpy)
    
    func testBackAction_ShouldCallDidNextStepBackAction() {
        sut.backAction()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.first, .back)
    }
}
