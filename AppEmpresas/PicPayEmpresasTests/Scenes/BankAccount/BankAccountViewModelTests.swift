import XCTest
import Core
import CoreSellerAccount
import FeatureFlag
import LegacyPJ
@testable import PicPayEmpresas

final private class BankAccountPresenterSpy: BankAccountPresenting {
    var viewController: BankAccountDisplay?
    
    private(set) var didFailToUnlockBankAccountCalledCount = 0
    private(set) var didFetchBankAccountCalledCount = 0
    private(set) var showErrorCalledCount = 0
    private(set) var isAccountLocked = false
    
    func willFetchBankAccount() {}
    func willSaveBankAccount() {}
    
    func didFetchBankAccount(_ bankAccount: BankAccount, of type: CompanyType?, isLocked: Bool) {
        didFetchBankAccountCalledCount += 1
        self.isAccountLocked = isLocked
    }
    
    func didChangeBank(_ bank: Bank) {}
    func bankAccountUpdated() {}
    func showError(_ error: LegacyPJError) {
        showErrorCalledCount += 1
    }
    func showPopUp(_ error: LegacyPJError) {}
    func didNextStep(action: BankAccountAction) {}
    func didCancelUpdate() {}
    func didSelectPersonalAccount(_ cpf: String) {}
    func didSelectCompanyAccount(_ cnpj: String?) {}
    func clearErrorMessage() {}
    
    func didFailToUnlockBankAccount() {
        didFailToUnlockBankAccountCalledCount += 1
    }
}

final private class BankAccountServiceMock: BankAccountServicing {
    
    var expectedResidenceLockResult: Result<UnlockResponse, ApiError> = .failure(ApiError.connectionFailure)
    var expectedResidenceLockStatusResult: Result<LockStatusResponse, ApiError> = .failure(ApiError.connectionFailure)
    
    var shouldFailRequest = false
    
    func getBankAccount(_ completion: @escaping (LegacyPJError?, BankAccount?) -> Void) {
        if shouldFailRequest {
            completion(.requestError, nil)
        } else {
            completion(nil, BankAccount())
        }
    }
    
    func getBankResidenceLockStatus(completion: @escaping BankResidenceLockStatusCompletion) {
        completion(expectedResidenceLockStatusResult)
    }
    
    func unlockBankAcount(completion: @escaping BankResidenceUnlockCompletion) {
        completion(expectedResidenceLockResult)
    }
    
    func updateBankAccount(_ pin: String, bankAccount: BankAccount, completion: @escaping BaseCompletion) {}
    
    func verifyCompanyType(
        _ cnpj: String,
        completion: @escaping (_ error: LegacyPJError?, _ type: CompanyType?) -> Void
    ) {
        if shouldFailRequest {
            completion(.requestError, nil)
        } else {
            completion(nil, CompanyType.individual)
        }
    }
    
    func verifyDocumentRelation(
        _ cnpj: String,
        cpf: String,
        completion: @escaping (_ error: LegacyPJError?, _ valid: Bool) -> Void
    ) {
        if shouldFailRequest {
            completion(.requestError, false)
        } else {
            completion(nil, true)
        }
    }
}

final class BankAccountViewModelTests: XCTestCase {
    private lazy var featureManagerMock = FeatureManagerMock()
    private let serviceMock = BankAccountServiceMock()
    private let presenterSpy = BankAccountPresenterSpy()
    private lazy var container = DependencyContainerMock(featureManagerMock)
    
    private lazy var sut = BankAccountViewModel(
        service: serviceMock,
        presenter: presenterSpy,
        seller: nil,
        dependencies: container
    )
    
    func testGetBankAccount_WhenSuccessfulRequest_ShouldCallPresenterCorrectly() {
        serviceMock.shouldFailRequest = false
        sut.getBankAccount()
        XCTAssertEqual(presenterSpy.didFetchBankAccountCalledCount, 1)
    }
    
    func testGetBankAccount_WhenFailedRequest_ShouldCallPresenterWithError() {
        serviceMock.shouldFailRequest = true
        sut.getBankAccount()
        XCTAssertEqual(presenterSpy.didFetchBankAccountCalledCount, 0)
        XCTAssertEqual(presenterSpy.showErrorCalledCount, 1)
    }
    
    func testGetBankAccount_WhenAccountUnlocked_ShouldSendCorrectStatusToPresenter() {
        let isAccountLocked = false
        serviceMock.expectedResidenceLockStatusResult = .success(.init(isLocked: isAccountLocked))
        sut.getBankAccount()
        XCTAssertEqual(presenterSpy.didFetchBankAccountCalledCount, 1)
        XCTAssertEqual(presenterSpy.isAccountLocked, isAccountLocked)
    }
        
    func testUpdateBankAccount_WhenFeatureFlagOff_ShouldNotCallPresenterWithFailToUnlockBankAccount() {
        featureManagerMock.override(keys: .featureResidenceChangeLock, with: false)
        sut.updateBankAccount(BankAccountItem(bankName: "", bankImageUrl: "", branch: "", branchDigit: "", account: "", accountDigit: "", type: "", operation: "", document: ""))
        
        XCTAssertEqual(presenterSpy.didFailToUnlockBankAccountCalledCount, 0)
    }
    
    func testUpdateBankAccount_WhenFeatureFlagOnAndAccountLocked_ShouldCallPresenterWithFailToUnlockBankAccount() {
        featureManagerMock.override(keys: .featureResidenceChangeLock, with: true)
        sut.isAccountLocked = true
        sut.updateBankAccount(BankAccountItem(bankName: "", bankImageUrl: "", branch: "", branchDigit: "", account: "", accountDigit: "", type: "", operation: "", document: ""))
        XCTAssertEqual(presenterSpy.didFailToUnlockBankAccountCalledCount, 1)
    }
    
    func testUpdateBankAccount_WhenFeatureFlagOffAndAccountLocked_ShouldNotCallPresenterWithFailToUnlockBankAccount() {
        featureManagerMock.override(keys: .featureResidenceChangeLock, with: false)
        sut.isAccountLocked = true
        sut.updateBankAccount(BankAccountItem(bankName: "", bankImageUrl: "", branch: "", branchDigit: "", account: "", accountDigit: "", type: "", operation: "", document: ""))
        XCTAssertEqual(presenterSpy.didFailToUnlockBankAccountCalledCount, 0)
    }
    
    func testUpdateBankAccount_WhenFeatureFlagOnAndAccountUnlocked_ShouldNotCallPresenterWithFailToUnlockBankAccount() {
        featureManagerMock.override(keys: .featureResidenceChangeLock, with: false)
        sut.isAccountLocked = true
        sut.updateBankAccount(BankAccountItem(bankName: "", bankImageUrl: "", branch: "", branchDigit: "", account: "", accountDigit: "", type: "", operation: "", document: ""))
        XCTAssertEqual(presenterSpy.didFailToUnlockBankAccountCalledCount, 0)
    }
}
