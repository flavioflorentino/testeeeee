import XCTest
import FeatureFlag
import LegacyPJ
@testable import PicPayEmpresas

private final class BankAccountCoordinatorMock: BankAccountCoordinating {
    var viewController: UIViewController?
    func perform(action: BankAccountAction) {}
}

private final class BankAccountViewControllerSpy: BankAccountDisplay {
   
    private(set) var showAccountLockedPopUpCalledCount = 0
    private(set) var showLockedBankAccountMessageCalledCount = 0
    private(set) var showErrorMessageCalledCount = 0
    
    func bankAccount(_ bankAccount: BankAccountItem) {}
    func prepareViewForPresent() {}
    func prepareViewForEdit() {}
    func operationField(hidden: Bool) {}
    func personalDocumentField(_ cpf: String) {}
    func companyDocumentField(_ cnpj: String) {}
    
    func showErrorMessage(_ message: String) {
        showErrorMessageCalledCount += 1
    }
    
    func showPopUp(with message: String) {}
    func didChangeBank(_ bank: BankItem) {}
    func startLoadingAnimation() {}
    func stopLoadingAnimation() {}
    func clearMessageText() {}
    
    func showLockedBankAccountMessage() {
        showLockedBankAccountMessageCalledCount += 1
    }
    
    func showAccountLockedPopUp() {
        showAccountLockedPopUpCalledCount += 1
    }
}


final class BankAccountPresenterTests: XCTestCase {
    
    private let coordinatorSpy = BankAccountCoordinatorMock()
    private let viewControllerSpy = BankAccountViewControllerSpy()
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var container = DependencyContainerMock(featureManagerMock)
    
    private lazy var sut: BankAccountPresenter = {
        let presenter = BankAccountPresenter(coordinator: coordinatorSpy, dependencies: container)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidFetchBankAccount_WhenAccountLocked_ShouldCallShowLockedBankAccountMessage() {
        sut.didFetchBankAccount(BankAccount(), of: .individual, isLocked: true)
        XCTAssertEqual(viewControllerSpy.showLockedBankAccountMessageCalledCount, 1)
    }
    
    func testDidFetchBankAccount_WhenAccountUnlocked_ShouldNotCallShowLockedBankAccountMessage() {
        sut.didFetchBankAccount(BankAccount(), of: .individual, isLocked: false)
        XCTAssertEqual(viewControllerSpy.showLockedBankAccountMessageCalledCount, 0)
    }
    
    func testDidFailToUnlockBankAccount_WhenCalled_ShouldCallShowAccountLockedPopup() {
        sut.didFailToUnlockBankAccount()
        XCTAssertEqual(viewControllerSpy.showAccountLockedPopUpCalledCount, 1)
    }
}
