import AssetsKit
import Core
import CoreSellerAccount
import XCTest
@testable import PicPayEmpresas

private class BankAccountFormPresentingSpy: BankAccountFormPresenting {
    var viewController: BankAccountFormDisplay?

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [BankAccountFormAction] = []

    func didNextStep(action: BankAccountFormAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }

    //MARK: - perform
    private(set) var performCallsCount = 0
    private(set) var performReceivedInvocations: [BankAccountFormPresenter.ViewAction] = []

    func perform(_ viewAction: BankAccountFormPresenter.ViewAction) {
        performCallsCount += 1
        performReceivedInvocations.append(viewAction)
    }

    //MARK: - changeButton
    private(set) var changeButtonEnabledCallsCount = 0
    private(set) var changeButtonEnabledReceivedInvocations: [Bool] = []

    func changeButton(enabled: Bool) {
        changeButtonEnabledCallsCount += 1
        changeButtonEnabledReceivedInvocations.append(enabled)
    }

    //MARK: - showBank
    private(set) var showBankCallsCount = 0
    private(set) var showBankReceivedInvocations: [CoreBank] = []

    func showBank(_ bank: CoreBank) {
        showBankCallsCount += 1
        showBankReceivedInvocations.append(bank)
    }

    //MARK: - bankAccount
    private(set) var bankAccountOfCallsCount = 0
    private(set) var bankAccountOfReceivedInvocations: [(account: CoreBankAccount?, companyType: CompanyType?)] = []

    func bankAccount(_ account: CoreBankAccount?, of companyType: CompanyType?) {
        bankAccountOfCallsCount += 1
        bankAccountOfReceivedInvocations.append((account: account, companyType: companyType))
    }

    //MARK: - listAccountTypes
    private(set) var listAccountTypesCallsCount = 0
    private(set) var listAccountTypesReceivedInvocations: [[CoreAccountType]] = []

    func listAccountTypes(_ types: [CoreAccountType]) {
        listAccountTypesCallsCount += 1
        listAccountTypesReceivedInvocations.append(types)
    }

    //MARK: - displayPersonalExplanation
    private(set) var displayPersonalExplanationCallsCount = 0

    func displayPersonalExplanation() {
        displayPersonalExplanationCallsCount += 1
    }

    //MARK: - showWrongDocumentModal
    private(set) var showWrongDocumentModalCallsCount = 0

    func showWrongDocumentModal() {
        showWrongDocumentModalCallsCount += 1
    }

    //MARK: - somenthingWentWrong
    private(set) var somenthingWentWrongCompletionCallsCount = 0
    private(set) var somenthingWentWrongCompletionReceivedInvocations: [(message: String?, completion: (() -> Void)?)] = []

    func somenthingWentWrong(_ message: String?, completion: (() -> Void)?) {
        somenthingWentWrongCompletionCallsCount += 1
        somenthingWentWrongCompletionReceivedInvocations.append((message: message, completion: completion))
    }

    //MARK: - validationError
    private(set) var validationErrorCallsCount = 0
    private(set) var validationErrorReceivedInvocations: [String] = []

    func validationError(_ message: String) {
        validationErrorCallsCount += 1
        validationErrorReceivedInvocations.append(message)
    }

    //MARK: - changeDocumentField
    private(set) var changeDocumentFieldEnabledValueCallsCount = 0
    private(set) var changeDocumentFieldEnabledValueReceivedInvocations: [(enabled: Bool, value: String?)] = []

    func changeDocumentField(enabled: Bool, value: String?) {
        changeDocumentFieldEnabledValueCallsCount += 1
        changeDocumentFieldEnabledValueReceivedInvocations.append((enabled: enabled, value: value))
    }

    //MARK: - showAccountErrorComponent
    private(set) var showAccountErrorComponentCallsCount = 0
    private(set) var showAccountErrorComponentReceivedInvocations: [BankAccountValidation] = []

    func showAccountErrorComponent(_ error: BankAccountValidation) {
        showAccountErrorComponentCallsCount += 1
        showAccountErrorComponentReceivedInvocations.append(error)
    }
}

private class BankAccountFormServicingMock: BankAccountFormServicing {

    //MARK: - getBankAccount
    private(set) var getBankAccountCallsCount = 0
    private(set) var getBankAccountReceivedInvocations: [((Result<CoreBankAccount, ApiError>) -> Void)] = []
    var getBankAccountClosure: ((@escaping (Result<CoreBankAccount, ApiError>) -> Void) -> Void)?

    func getBankAccount(_ completion: @escaping (Result<CoreBankAccount, ApiError>) -> Void) {
        getBankAccountCallsCount += 1
        getBankAccountReceivedInvocations.append(completion)
        getBankAccountClosure?(completion)
    }

    //MARK: - getAccountType
    private(set) var getAccountTypeCnpjCompletionCallsCount = 0
    private(set) var getAccountTypeCnpjCompletionReceivedInvocations: [(bankCode: String, cnpj: String, completion: (Result<[CoreAccountType], ApiError>) -> Void)] = []
    var getAccountTypeCnpjCompletionClosure: ((String, String, @escaping (Result<[CoreAccountType], ApiError>) -> Void) -> Void)?

    func getAccountType(_ bankCode: String, cnpj: String, completion: @escaping (Result<[CoreAccountType], ApiError>) -> Void) {
        getAccountTypeCnpjCompletionCallsCount += 1
        getAccountTypeCnpjCompletionReceivedInvocations.append((bankCode: bankCode, cnpj: cnpj, completion: completion))
        getAccountTypeCnpjCompletionClosure?(bankCode, cnpj, completion)
    }

    //MARK: - validateDocuments
    private(set) var validateDocumentsCpfCnpjCompletionCallsCount = 0
    private(set) var validateDocumentsCpfCnpjCompletionReceivedInvocations: [(cpf: String, cnpj: String, completion: (Result<ValidateQSAResponse?, ApiError>) -> Void)] = []
    var validateDocumentsCpfCnpjCompletionClosure: ((String, String, @escaping (Result<ValidateQSAResponse?, ApiError>) -> Void) -> Void)?

    func validateDocuments(cpf: String, cnpj: String, completion: @escaping (Result<ValidateQSAResponse?, ApiError>) -> Void) {
        validateDocumentsCpfCnpjCompletionCallsCount += 1
        validateDocumentsCpfCnpjCompletionReceivedInvocations.append((cpf: cpf, cnpj: cnpj, completion: completion))
        validateDocumentsCpfCnpjCompletionClosure?(cpf, cnpj, completion)
    }

    //MARK: - checkBankAccount
    private(set) var checkBankAccountCompletionCallsCount = 0
    private(set) var checkBankAccountCompletionReceivedInvocations: [((Result<BankAccountValidation?, ApiError>) -> Void)] = []
    var checkBankAccountCompletionClosure: ((@escaping(Result<BankAccountValidation?, ApiError>) -> Void) -> Void)?

    func checkBankAccount(completion: @escaping(Result<BankAccountValidation?, ApiError>) -> Void) {
        checkBankAccountCompletionCallsCount += 1
        checkBankAccountCompletionReceivedInvocations.append(completion)
        checkBankAccountCompletionClosure?(completion)
    }
}

final class BankAccountFormInteractorTests: XCTestCase {
    private let serviceMock = BankAccountFormServicingMock()
    private let presenterSpy = BankAccountFormPresentingSpy()
    private let mainQueue = DispatchQueue(label: "bankAccountFormInteractorTests")
    private let timeout: Double = 3.0

    private var companyType: CompanyType = .individual
    private var bankAccount: CoreBankAccount? {
        LocalMock<CoreBankAccount>().localMockV2("bankAccount") 
    }
    private var accountTypes: [CoreAccountType]? {
        LocalMock<[CoreAccountType]>().localMockV2("accountType")
    }
    private var accountValidation: BankAccountValidation? {
        LocalMock<BankAccountValidation>().localMockV2("bankAccountValidation")
    }

    lazy var sut = BankAccountFormInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        dependencies: DependencyContainerMock(mainQueue),
        companyType: companyType
    )

    func testLoadViewInfo_WhenCompanyTypeIsIndividualAndSuccess_ShouldPresentBankAccountAndAccountTypes() {
        companyType = .individual
        let expectationBank = self.expectation(description: "bank")
        serviceMock.getBankAccountClosure = { completion in
            guard let account = self.bankAccount else { 
                XCTFail("Bank Account mock is nil") 
                return
            }
            completion(.success(account))
            expectationBank.fulfill()
        }
        
        let expectationCheckBank = self.expectation(description: "checkank")
        serviceMock.checkBankAccountCompletionClosure = { completion in
            guard let accountValidation = self.accountValidation else { 
                XCTFail("Bank Account mock is nil") 
                return
            }

            completion(.success(accountValidation))
            expectationCheckBank.fulfill()
        }

        let expectationType = self.expectation(description: "type")
        serviceMock.getAccountTypeCnpjCompletionClosure = { _, _, completion in
            guard let types = self.accountTypes else { 
                XCTFail("Account Types mock is nil") 
                return
            }

            completion(.success(types))
            expectationType.fulfill()
        }
        
        sut.loadViewInfo()

        let expectationAfter = self.expectation(description: "after")
        mainQueue.asyncAfter(deadline: .now() + 0.1) {
            expectationAfter.fulfill()
        }
        
        wait(for: [expectationBank, expectationType, expectationCheckBank, expectationAfter], timeout: timeout)

        XCTAssertEqual(presenterSpy.performCallsCount, 2)
        XCTAssertEqual(presenterSpy.displayPersonalExplanationCallsCount, 1)
        XCTAssertEqual(presenterSpy.bankAccountOfCallsCount, 1)
    }

    func testLoadViewInfo_WhenCompanyTypeIsMultiPartnerAndSuccess_ShouldPresentBankAccountAndAccountTypes() {
        companyType = .multiPartners
        serviceMock.getBankAccountClosure = { completion in
            guard let account = self.bankAccount else { 
                XCTFail("Bank Account mock is nil") 
                return
            }

            completion(.success(account))
        }

        serviceMock.getAccountTypeCnpjCompletionClosure = { _, _, completion in
            guard let types = self.accountTypes else { 
                XCTFail("Account Types mock is nil") 
                return
            }

            completion(.success(types))
        }

        serviceMock.checkBankAccountCompletionClosure = { completion in
            guard let accountValidation = self.accountValidation else { 
                XCTFail("Bank Account mock is nil") 
                return
            }

            completion(.success(accountValidation))
        }

        sut.loadViewInfo()

        let expectation = self.expectation(description: "")
        mainQueue.asyncAfter(deadline: .now() + 0.1) {
            expectation.fulfill()
        }
        waitForExpectations(timeout: timeout)

        XCTAssertEqual(presenterSpy.performCallsCount, 2)
        XCTAssertEqual(presenterSpy.displayPersonalExplanationCallsCount, 0)
        XCTAssertEqual(presenterSpy.bankAccountOfCallsCount, 1)
    }

    func testOpenListBank_ShouldInvokeActionListBank() {
        sut.openListBank()

        let expectedAction = BankAccountFormAction.listBank(delegate: sut)

        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.last, expectedAction)
    }

    func testListAccountTypes_ShouldInvokeListAccount() {
        companyType = .individual
        serviceMock.getBankAccountClosure = { completion in
            guard let account = self.bankAccount else { 
                XCTFail("Bank Account mock is nil") 
                return
            }

            completion(.success(account))
        }

        serviceMock.getAccountTypeCnpjCompletionClosure = { _, _, completion in
            guard let types = self.accountTypes else { 
                XCTFail("Account Types mock is nil") 
                return
            }

            completion(.success(types))
        }

        sut.loadViewInfo()

        let expectation = self.expectation(description: "")
        mainQueue.asyncAfter(deadline: .now() + 0.1) {
            expectation.fulfill()
        }
        waitForExpectations(timeout: timeout)

        sut.listAccountTypes()

        XCTAssertEqual(presenterSpy.listAccountTypesCallsCount, 1)
        XCTAssertEqual(presenterSpy.listAccountTypesReceivedInvocations.last?.count, accountTypes?.count)
    }
}

extension BankAccountFormAction: Equatable {
    public static func == (lhs: BankAccountFormAction, rhs: BankAccountFormAction) -> Bool {
        switch (lhs, rhs) {
        case (.listBank, .listBank):
            return true
        case (.wrongDocumentModal, .wrongDocumentModal):
            return true
        case let (.helpModal(a, _), .helpModal(b, _)):
            return a == b
        case let (.errorModal(a, _), .errorModal(b, _)):
            return a == b
        default:
            return false
        }
    }
}
