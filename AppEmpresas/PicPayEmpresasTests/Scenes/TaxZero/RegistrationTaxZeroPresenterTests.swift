import XCTest
@testable import PicPayEmpresas

final class RegistrationTaxZeroPresenterTests: XCTestCase {
    private let coordinatorSpy = RegistrationTaxZeroCoordinatorSpy()
    
    private lazy var sut = RegistrationTaxZeroPresenter(coordinator: coordinatorSpy)
    
    func testDidNextStep_WhenReceiveTermsFromViewModel_ShouldPushWebViewController() {
        sut.didNextStep(action: .terms)
        
        XCTAssertNotNil(coordinatorSpy.action)
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
    }
    
    func testDidNextStep_WhenReceiveNextStepRegistrationFromViewModel_ShouldPushViewController() {
        let modelMock = RegistrationViewModel()
        
        sut.didNextStep(action: .nextStepRegistration(model: modelMock))
        
        XCTAssertNotNil(coordinatorSpy.action)
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
    }
    
    func testShowPopUp_WhenReceiveActionFromViewModel_ShouldConfigureAndPresentPopUp() {
        let modelMock = RegistrationViewModel()
        
        sut.showPopUp(model: modelMock)
        
        XCTAssertNotNil(coordinatorSpy.action)
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
    }
}

private final class RegistrationTaxZeroCoordinatorSpy: RegistrationTaxZeroCoordinating {
    var viewController: UIViewController?
    
    private (set) var action: RegistrationTaxZeroAction?
    private (set) var callPerformCount: Int = 0
    
    func perform(action: RegistrationTaxZeroAction) {
        self.action = action
        callPerformCount += 1
    }
}
