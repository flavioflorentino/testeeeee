import XCTest
import FeatureFlag
@testable import PicPayEmpresas

class RegistrationTaxZeroCoordinatorTest: XCTestCase {
    private let viewControllerMock = ViewControllerSpy()
    private lazy var navigationMock = NavigationControllerSpy(rootViewController: viewControllerMock)
    private lazy var featureMock = FeatureManagerMock()
    
    private lazy var sut: RegistrationTaxZeroCoordinator = {
        let coordinator = RegistrationTaxZeroCoordinator(dependencies: DependencyContainerMock(featureMock))
        coordinator.viewController = navigationMock.topViewController
        return coordinator
    }()
    
    func testPerform_WhenReceiveNextStepRegistrationFromPresenter_ShouldPushViewController() {
        let modelMock = RegistrationViewModel()
        featureMock.override(key: .opsNewFlowPasswordScenePJ, with: false)
        sut.perform(action: .nextStepRegistration(model: modelMock))
        XCTAssertTrue(navigationMock.currentViewController is RegistrationPasswordStepViewController)
    }
    
    func testPerform_WhenReceiveTermsFromPresenter_ShouldWebViewController() {
        sut.perform(action: .terms)
        
        XCTAssertTrue(navigationMock.currentViewController is WebViewController)
    }
    
    func testPerform_WhenReceiveShowPopUpFromPresenter_ShouldPresentPopUp() {
        let popUp = PopupViewController()
        sut.perform(action: .showPopUp(popup: popUp))
        
        XCTAssertTrue(viewControllerMock.presentViewController is PopupViewController)
    }
}
