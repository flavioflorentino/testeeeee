import XCTest
@testable import PicPayEmpresas

private class RegistrationTaxZeroViewModelTests: XCTestCase {
    private let presenter = RegistrationTaxZeroPresenterSpy()
    private let modelMock = RegistrationViewModel()
    private lazy var sut = RegistrationTaxZeroViewModel(presenter: presenter, model: modelMock)
    
    func testShowMoreInfo_WhenReceiveActionFromViewController_ShouldPushWebViewController() {
        sut.showMoreInfo()
        
        XCTAssertNotNil(presenter.actionSelected)
    }
    
    func testOkAction_WhenReceiveActionFromViewController_ShouldAddNumberOneInModelAndPresentPopUp() {
        sut.okAction()
        
        XCTAssertEqual(presenter.modelValueSelected, 1)
    }
}

private final class RegistrationTaxZeroPresenterSpy: RegistrationTaxZeroPresenting {
    var viewController: RegistrationTaxZeroDisplay?
    
    private(set) var actionSelected: RegistrationTaxZeroAction?
    private(set) var modelValueSelected: Int?
    
    func didNextStep(action: RegistrationTaxZeroAction) {
        actionSelected = action
    }
    
    func showPopUp(model: RegistrationViewModel) {
        modelValueSelected = model.account.days
    }
}
