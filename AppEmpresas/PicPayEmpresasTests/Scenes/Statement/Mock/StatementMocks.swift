import Core
@testable import PicPayEmpresas

final class StatementMocks {
    func fetchStatement() -> Statement? {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DecodableDateFormatter())
        return LocalMock<Statement>().pureLocalMock("statement", decoder: decoder)
    }

    func fetchEmtpyStatement() -> Statement? {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DecodableDateFormatter())
        return LocalMock<Statement>().pureLocalMock("empty_statement", decoder: decoder)
    }

    func fetchSalesRecipeStatement() -> StatementItem? {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DecodableDateFormatter())
        return LocalMock<StatementItem>().pureLocalMock("statement_sales_recipe", decoder: decoder)
    }
    
    func fetchBlockedTransferStatement() -> StatementItem? {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DecodableDateFormatter())
        return LocalMock<StatementItem>().pureLocalMock("statement_blocked_transfer", decoder: decoder)
    }
    
    func fetchPicPayWithdrawalProgressStatement() -> StatementItem? {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DecodableDateFormatter())
        return LocalMock<StatementItem>().pureLocalMock("statement_picpay_withdrawal_progress", decoder: decoder)
    }

    func fetchTaxItem() -> StatementTax? {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DecodableDateFormatter())
        return LocalMock<StatementTax>().pureLocalMock("statement_tax", decoder: decoder)
    }
    
    func fetchPixTransferStatement() -> StatementItem? {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DecodableDateFormatter())
        return LocalMock<StatementItem>().pureLocalMock("statement_pix_transfer", decoder: decoder)
    }
}
