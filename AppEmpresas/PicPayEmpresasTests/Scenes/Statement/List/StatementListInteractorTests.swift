import Core
import XCTest
@testable import PicPayEmpresas

private final class StatementListServiceMock: StatementListServicing {
    var fetchStatementReturn: (Result<Statement?, ApiError>)?
    var fetchTaxReturn: (Result<StatementTax?, ApiError>)?
    var fetchBalanceReturn: (Result<WalletBalanceItem?, ApiError>)?

    func fetchStatement(_ pagination: StatementPagination?, completion: @escaping(Result<Statement?, ApiError>) -> Void) {
        guard let fetchStatementReturn = fetchStatementReturn else {
            XCTFail()
            return
        }

        completion(fetchStatementReturn)
    }

    func fetchTax(of day: String, completion: @escaping(Result<StatementTax?, ApiError>) -> Void) {
        guard let fetchTaxReturn = fetchTaxReturn else {
            XCTFail()
            return
        }

        completion(fetchTaxReturn)
    }

    func fetchBalance(_ completion: @escaping(Result<WalletBalanceItem?, ApiError>) -> Void) {
        guard let fetchBalanceReturn = fetchBalanceReturn else {
            XCTFail()
            return
        }

        completion(fetchBalanceReturn)
    }
}

private final class StatementListPresenterSpy: StatementListPresenting {
    var viewController: StatementListDisplay?
    private(set) var didNextStepCount = 0
    private(set) var nextStepAction: StatementListAction?
    private(set) var showInitialStatementsCount = 0
    private(set) var initialStatement: Statement?
    private(set) var showFutureAmountCount = 0
    private(set) var futureAmount: String?
    private(set) var futureDate: Date?
    private(set) var noFutureAmountCount = 0
    private(set) var setLoadingCount = 0
    private(set) var setActivityCount = 0
    private(set) var setApolloLoadingCount = 0
    private(set) var showSalesRecipesCount = 0
    private(set) var salesTaxes: Double?
    private(set) var showEmptyStateCount = 0
    private(set) var showTaxPopUpCount = 0
    private(set) var showInitialErrorCount = 0
    private(set) var showMoreStatementsCount = 0
    private(set) var showTryAgainCount = 0
    private(set) var showBalanceCount = 0
    private(set) var showBalancePopUpCount = 0
    private(set) var balance: String?
    private(set) var showBlockedBalanceCount = 0
    private(set) var blockedBalance: String?
    private(set) var removeErrorViewControllerCount = 0
    
    func didNextStep(action: StatementListAction) {
        didNextStepCount += 1
        nextStepAction = action
    }

    func showInitialStatements(_ statement: Statement) {
        showInitialStatementsCount += 1
        initialStatement = statement
    }
    
    func showFutureAmount(_ amount: String, date: Date) {
        showFutureAmountCount += 1
        futureAmount = amount
        futureDate = date
    }
    
    func showNoFutureAmount() {
        noFutureAmountCount += 1
    }

    func showMoreStatements(_ statement: [StatementSection]) {
        showMoreStatementsCount += 1
    }

    func setLoading(_ isLoading: Bool) {
        setLoadingCount += 1
    }
    
    func setApolloLoading(_ isLoading: Bool) {
        setApolloLoadingCount += 1
    }

    func setActivity(_ isLoading: Bool) {
        setActivityCount += 1
    }

    func showSalesRecipeTaxesModal(taxes: Double) {
        showSalesRecipesCount += 1
        salesTaxes = taxes
    }

    func showEmptyState() {
        showEmptyStateCount += 1
    }

    func showTaxPopUpError() {
        showTaxPopUpCount += 1
    }

    func showInitialError() {
        showInitialErrorCount += 1
    }

    func showTryAgainComponent() {
        showTryAgainCount += 1
    }
    
    func showBalance(_ balance: String) {
        showBalanceCount += 1
        self.balance = balance
    }
    
    func showBlockedBalance(_ blockedBalance: String) {
        showBlockedBalanceCount += 1
        self.blockedBalance = blockedBalance
    }

    func showBalancePopUpError() {
        showBalancePopUpCount += 1
    }
    
    func removeErrorViewController() {
        removeErrorViewControllerCount += 1
    }
}

final class StatementListInteractorTests: XCTestCase {
    private let timeout: Double = 0.5
    private let mainQueue = DispatchQueue(label: "statementListViewModelTests")
    private let dispatchGroup = DispatchGroup()
    private let serviceMock = StatementListServiceMock()
    private let presenterSpy = StatementListPresenterSpy()
    
    lazy var sut: StatementListInteractor = {
        let dependencies = DependencyContainerMock(mainQueue, dispatchGroup)
        return StatementListInteractor(service: serviceMock, presenter: presenterSpy, dependencies: dependencies)
    }()

    func testInitialFetch_WhenSuccess_ShouldDisplayStatement() throws {
        let statement = try XCTUnwrap(StatementMocks().fetchStatement())
        let balance = try XCTUnwrap(WalletBalanceItemMock().balanceItem())
        serviceMock.fetchStatementReturn = .success(statement)
        serviceMock.fetchBalanceReturn = .success(balance)
        
        sut.initialFetch()
        waitForFetch()
        
        XCTAssertEqual(presenterSpy.showInitialStatementsCount, 1)
        XCTAssertEqual(presenterSpy.setLoadingCount, 2)
    }

    func testInitialFetch_WhenFailOnStatement_ShouldDisplayInitialError() throws {
        serviceMock.fetchStatementReturn = .failure(.badRequest(body: RequestError()))
        let balance = try XCTUnwrap(WalletBalanceItemMock().balanceItem())
        serviceMock.fetchBalanceReturn = .success(balance)
        
        sut.initialFetch()
        waitForFetch()
        
        XCTAssertEqual(presenterSpy.showInitialErrorCount, 1)
        XCTAssertEqual(presenterSpy.showInitialStatementsCount, 0)
        XCTAssertEqual(presenterSpy.setLoadingCount, 2)
    }

    func testInitialFetch_WhenEmpty_ShouldDisplayEmptyState() throws {
        let emtpyStatement = try XCTUnwrap(StatementMocks().fetchEmtpyStatement())
        serviceMock.fetchStatementReturn = .success(emtpyStatement)
        let balance = try XCTUnwrap(WalletBalanceItemMock().balanceItem())
        serviceMock.fetchBalanceReturn = .success(balance)
        
        sut.initialFetch()
        waitForFetch()
        
        XCTAssertEqual(presenterSpy.setLoadingCount, 2)
        XCTAssertEqual(presenterSpy.showEmptyStateCount, 1)
    }

    func testExportStatements_ShouldDisplayExportStatementsScene() {
        sut.exportStatements()

        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy.nextStepAction, .exportStatements)
    }

    func testStatementItemSelected_WhenSalesRecipe_ShouldDisplaySales() throws {
        let statementTax = try XCTUnwrap(StatementMocks().fetchTaxItem())
        serviceMock.fetchTaxReturn = .success(statementTax)
        let statementItem = try XCTUnwrap(StatementMocks().fetchSalesRecipeStatement())
        sut.statementItemSelected(statementItem: statementItem)

        XCTAssertEqual(presenterSpy.setApolloLoadingCount, 2)
        XCTAssertEqual(presenterSpy.showSalesRecipesCount, 1)
        XCTAssertEqual(presenterSpy.salesTaxes, statementTax.tax)
    }
    
    func testStatementItemSelected_WhenBlockedTransferBalance_ShouldDisplayBlockedTransferModal() throws {
        let statementItem = try XCTUnwrap(StatementMocks().fetchBlockedTransferStatement())
        sut.statementItemSelected(statementItem: statementItem)

        XCTAssertEqual(1, presenterSpy.didNextStepCount)
        XCTAssertEqual(.blockedTransferBalance, presenterSpy.nextStepAction)
    }
    
    func testStatementItemSelected_WhenPix_ShouldDisplayPixReceipt() throws {
        let statementItem = try XCTUnwrap(StatementMocks().fetchPixTransferStatement())
        sut.statementItemSelected(statementItem: statementItem)

        XCTAssertEqual(1, presenterSpy.didNextStepCount)
        XCTAssertEqual(.pixReceipt(movementCode: statementItem.movementCode, transactionId: statementItem.id, isRefundAllowed: false), presenterSpy.nextStepAction)
    }
    
    func testInitialFetch_WhenSuccess_ShouldDisplayBalance() throws {
        let statement = try XCTUnwrap(StatementMocks().fetchStatement())
        serviceMock.fetchStatementReturn = .success(statement)
        let walletBalance = try XCTUnwrap(WalletBalanceItemMock().balanceItem())
        serviceMock.fetchBalanceReturn = .success(walletBalance)
        
        sut.initialFetch()
        waitForFetch()
        
        XCTAssertEqual(presenterSpy.showBalanceCount, 1)
        
        let spyBalance = try XCTUnwrap(presenterSpy.balance)
        XCTAssertEqual(spyBalance, walletBalance.available)
    }
    
    func testInitialFetch_WhenSuccessAndHasBlockedBalance_ShouldDisplayBlockedBalance() throws {
        let statement = try XCTUnwrap(StatementMocks().fetchStatement())
        serviceMock.fetchStatementReturn = .success(statement)
        let walletBalance = try XCTUnwrap(WalletBalanceItemMock().balanceItem())
        serviceMock.fetchBalanceReturn = .success(walletBalance)
        
        sut.initialFetch()
        waitForFetch()
        
        let blockedBalance = try XCTUnwrap(walletBalance.blocked)
        let spyBalance = try XCTUnwrap(presenterSpy.blockedBalance)
        XCTAssertEqual(presenterSpy.showBlockedBalanceCount, 1)
        XCTAssertEqual(spyBalance, blockedBalance)
    }
    
    func testInitialFetch_WhenSuccessAndHasNoBlockedBalance_ShouldNotDisplayBlockedBalance() throws {
        let statement = try XCTUnwrap(StatementMocks().fetchStatement())
        serviceMock.fetchStatementReturn = .success(statement)
        
        let walletBalance = try XCTUnwrap(WalletBalanceItemMock().balanceNotBlockedItem())
        serviceMock.fetchBalanceReturn = .success(walletBalance)
        
        sut.initialFetch()
        waitForFetch()
        
        XCTAssertEqual(presenterSpy.showBlockedBalanceCount, 0)
        XCTAssertNil(presenterSpy.blockedBalance)
    }
    
    func testInitialFetch_WhenBalanceFailure_ShouldDisplayBalancePopUpError() throws {
        let statement = try XCTUnwrap(StatementMocks().fetchStatement())
        serviceMock.fetchStatementReturn = .success(statement)
        serviceMock.fetchBalanceReturn = .failure(.badRequest(body: RequestError()))
        
        sut.initialFetch()
        waitForFetch()
        
        XCTAssertEqual(presenterSpy.showBalancePopUpCount, 1)
    }
    
    func testRefresh_WhenSuccess_ShouldCallInitialStatements() throws {
        let statement = try XCTUnwrap(StatementMocks().fetchStatement())
        serviceMock.fetchStatementReturn = .success(statement)
        
        let walletBalance = try XCTUnwrap(WalletBalanceItemMock().balanceNotBlockedItem())
        serviceMock.fetchBalanceReturn = .success(walletBalance)
        
        sut.refresh()
        waitForFetch()
        
        XCTAssertEqual(presenterSpy.showInitialStatementsCount, 1)
    }
    
    func testRetryFirstPage_ShouldCallRemoveErrorViewController() throws {
        let statement = try XCTUnwrap(StatementMocks().fetchStatement())
        serviceMock.fetchStatementReturn = .success(statement)
        
        let walletBalance = try XCTUnwrap(WalletBalanceItemMock().balanceNotBlockedItem())
        serviceMock.fetchBalanceReturn = .success(walletBalance)
        
        sut.retryFirstPage()
        waitForFetch()
        
        XCTAssertEqual(presenterSpy.removeErrorViewControllerCount, 1)
    }
    
    func testRetryFirstPage_ShouldCallRefresh() throws {
        let statement = try XCTUnwrap(StatementMocks().fetchStatement())
        serviceMock.fetchStatementReturn = .success(statement)
        
        let walletBalance = try XCTUnwrap(WalletBalanceItemMock().balanceNotBlockedItem())
        serviceMock.fetchBalanceReturn = .success(walletBalance)
        
        sut.retryFirstPage()
        waitForFetch()
        
        XCTAssertEqual(presenterSpy.showInitialStatementsCount, 1)
    }
}

private extension StatementListInteractorTests {
    func waitForFetch() {
        let expectation = XCTestExpectation()
        dispatchGroup.notify(queue: mainQueue) {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: timeout)
    }
}
