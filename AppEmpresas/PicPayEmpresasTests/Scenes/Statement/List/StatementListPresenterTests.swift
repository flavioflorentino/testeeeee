import XCTest
@testable import PicPayEmpresas

private final class StatementListCoordinatorSpy: StatementListCoordinating {
    var viewController: UIViewController?
    private(set) var action: StatementListAction?

    func perform(action: StatementListAction) {
        self.action = action
    }
}

private final class StatementListDisplaySpy: StatementListDisplay {
    private(set) var showLoadingCount = 0
    private(set) var hideLoadingCount = 0
    private(set) var showApolloLoadingCount = 0
    private(set) var hideApolloLoadingCount = 0
    private(set) var showActivityCount = 0
    private(set) var hideActivityCount = 0
    private(set) var displayInitialSectionsCount = 0
    private(set) var sections: [StatementSection]?
    private(set) var displayEmptyStateCount = 0
    private(set) var displayErrorViewCount = 0
    private(set) var displayAvailableCount = 0
    private(set) var availableAmount: String?

    private(set) var displayFutureAmountCount = 0
    private(set) var futureAmount: String?
    private(set) var futureDate: Date?
    private(set) var displaynofuturebalancecount = 0
    private(set) var displayBlockedBalanceCount = 0
    private(set) var blockedBalance: String?
    private(set) var showTryAgainCount = 0
    private(set) var removeErrorViewControllerCount = 0

    func showLoading() {
        showLoadingCount += 1
    }

    func hideLoading() {
        hideLoadingCount += 1
    }
    
    func showApolloLoading() {
        showApolloLoadingCount += 1
    }

    func hideApolloLoading() {
        hideApolloLoadingCount += 1
    }

    func showActivityIndicator() {
        showActivityCount += 1
    }

    func hideActivityIndicator() {
        hideActivityCount += 1
    }

    func displayInitialSections(sections: [StatementSection]) {
        displayInitialSectionsCount += 1
        self.sections = sections
    }

    func displayEmptyState() {
        displayEmptyStateCount += 1
    }

    func displayErrorView() {
        displayErrorViewCount += 1
    }

    func displayAvailableAmount(_ value: String) {
        displayAvailableCount += 1
        availableAmount = value
    }

    func displayFutureAmount(_ amount: String, date: Date) {
        displayFutureAmountCount += 1
        futureAmount = amount
        futureDate = date
    }

    func displayNoFutureBalance() {
        displaynofuturebalancecount += 1
    }
    
    func displayBlockedBalance(_ value: String) {
        displayBlockedBalanceCount += 1
        blockedBalance = value
    }

    func showTryAgainComponent() {
        showTryAgainCount += 1
    }
    
    func removeErrorViewController() {
        removeErrorViewControllerCount += 1
    }
}

final class StatementListPresenterTests: XCTestCase {
    private let coordinatorSpy = StatementListCoordinatorSpy()
    private let displaySpy = StatementListDisplaySpy()

    lazy var sut: StatementListPresenter? = {
        let presenter = StatementListPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()

    func testSetLoading_WhenTrue_ShouldDisplayShowLoading() {
        sut?.setLoading(true)

        XCTAssertEqual(1, displaySpy.showLoadingCount)
    }

    func testSetLoading_WhenFalse_ShouldDisplayShowLoading() {
        sut?.setLoading(false)

        XCTAssertEqual(1, displaySpy.hideLoadingCount)
    }
    
    func testSetApolloLoading_WhenTrue_ShouldDisplayShowApolloLoading() {
        sut?.setApolloLoading(true)
        
        XCTAssertEqual(displaySpy.showApolloLoadingCount, 1)
    }
    
    func testSetApolloLoading_WhenFalse_ShouldDisplayShowApolloLoading() {
        sut?.setApolloLoading(false)
        
        XCTAssertEqual(displaySpy.hideApolloLoadingCount, 1)
    }

    func testShowInitialStatements_ShouldDisplaySectionsAndHeader() throws {
        let statement = try XCTUnwrap(StatementMocks().fetchStatement())
        sut?.showInitialStatements(statement)

        XCTAssertEqual(1, displaySpy.displayInitialSectionsCount)
        XCTAssertEqual(statement.extract.count, displaySpy.sections?.count)
    }
    
    func testShowFutureAmount_ShouldDisplayAndHeader() throws {
        let statement = try XCTUnwrap(StatementMocks().fetchStatement())
        let amount = try XCTUnwrap(statement.futureReleases?.value.toCurrencyString())
        let date = try XCTUnwrap(statement.futureReleases?.date)
        sut?.showFutureAmount(amount, date: date)

        XCTAssertEqual(1, displaySpy.displayFutureAmountCount)
        XCTAssertEqual(amount, displaySpy.futureAmount)
        XCTAssertEqual(date, displaySpy.futureDate)
    }

    func testDidNextStep_ShouldDisplayNextStep() {
        sut?.didNextStep(action: .exportStatements)

        XCTAssertEqual(.exportStatements, coordinatorSpy.action)
    }

    func testEmptyState() {
        sut?.showEmptyState()

        XCTAssertEqual(1, displaySpy.displayEmptyStateCount)
    }

    func testInitialErrorView() {
        sut?.showInitialError()

        XCTAssertEqual(1, displaySpy.displayErrorViewCount)
    }
    
    func testShowBalance_ShouldDisplayBalance() throws {
        let balance = try XCTUnwrap(WalletBalanceItemMock().balanceItem())
        sut?.showBalance(balance.available)
        
        XCTAssertEqual(displaySpy.displayAvailableCount, 1)
        XCTAssertEqual(displaySpy.availableAmount, balance.available.currency())
    }
    
    func testShowBlockedBalance_ShouldDisplayBlockedBalance() throws {
        let balance = try XCTUnwrap(WalletBalanceItemMock().balanceItem())
        let blockedBalance = try XCTUnwrap(balance.blocked)
        sut?.showBlockedBalance(blockedBalance)
        
        XCTAssertEqual(displaySpy.displayBlockedBalanceCount, 1)
        XCTAssertEqual(displaySpy.blockedBalance, balance.blocked?.currency())
    }
    
    func testRemoveErrorViewController_ShouldRemoveErrorViewController() {
        sut?.removeErrorViewController()
        XCTAssertEqual(displaySpy.removeErrorViewControllerCount, 1)
    }
}
