import XCTest
import FeatureFlag
@testable import PicPayEmpresas

final private class StatementListCoordinatorTests: XCTestCase {
    private lazy var viewControllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: viewControllerSpy)
    
    private lazy var sut: StatementListCoordinating = {
        let coordinator = StatementListCoordinator(dependencies: DependencyContainerMock())
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenExportStatements_ShouldPushStatementsViewController() {
        sut.perform(action: .exportStatements)
        
        XCTAssertTrue(navigationSpy.currentViewController is ExportMovementsViewController)
    }
    
    func testPerform_WhenSalesRecipe_ShouldPresentSalesRecipeModal() {
        let alertModalInfo = AlertModalInfo()
        
        sut.perform(action: .salesRecipe(modalInfo: alertModalInfo))
        
        XCTAssertEqual(1, navigationSpy.presentCount)
    }
    
    func testPerform_WhenBlockedBalance_ShouldPresentBlockedBalanceModal() {
        sut.perform(action: .blockedBalance)
        
        XCTAssertEqual(1, viewControllerSpy.presentViewControllerCounter)
    }
    
    func testPerform_WhenBlockedTransferBalance_ShouldPresentBlockedTransferBalanceModal() {
        sut.perform(action: .blockedTransferBalance)
        
        XCTAssertEqual(1, viewControllerSpy.presentViewControllerCounter)
    }
    
    func testPerform_WhenPix_ShouldPresentPixReceipt() throws {
        let statementItem = try XCTUnwrap(StatementMocks().fetchPixTransferStatement())
        
        sut.perform(action: .pixReceipt(movementCode: statementItem.movementCode, transactionId: statementItem.id, isRefundAllowed: true))
        XCTAssertEqual(1, navigationSpy.presentCount)
    }
}
