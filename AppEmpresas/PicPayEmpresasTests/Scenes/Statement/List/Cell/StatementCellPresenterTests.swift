import XCTest
@testable import PicPayEmpresas

private final class StatementCellDisplaySpy: StatementCellDisplay {
    private(set) var setupCount = 0
    private(set) var title: String?
    private(set) var description: String?
    private(set) var balance: String?
    
    private(set) var setupIconCount = 0
    
    private(set) var setupArrowLabelCount = 0
    private(set) var arrowLabelIsHidden: Bool?
    
    private(set) var hideStatusCount = 0
    
    private(set) var showStatusCount = 0
    
    func setup(title: String, description: String, balance: String) {
        setupCount += 1
        self.title = title
        self.description = description
        self.balance = balance
    }
    
    func setupIcon(_ icon: UIImage) {
        setupIconCount += 1
    }
    
    func setupArrowLabel(isHidden: Bool) {
        setupArrowLabelCount += 1
        arrowLabelIsHidden = isHidden
    }
    
    func hideStatus() {
        hideStatusCount += 1
    }
    
    func showStatus(text: String, color: UIColor) {
        showStatusCount += 1
    }
}

final class StatementCellPresenterTests: XCTestCase {
    private let displaySpy = StatementCellDisplaySpy()
    
    lazy var sut: StatementCellPresenting = {
        let presenter = StatementCellPresenter()
        presenter.cell = displaySpy
        return presenter
    }()
    
    func testShowItem_ShouldSetTitleDescriptionAndBalance() throws {
        let item = try XCTUnwrap(StatementMocks().fetchSalesRecipeStatement())
        sut.show(item: item)
        
        XCTAssertEqual(displaySpy.setupCount, 1)
        XCTAssertEqual(displaySpy.title, item.title)
        XCTAssertEqual(displaySpy.description, item.description)
        XCTAssertEqual(displaySpy.balance, item.value.toCurrencyString())
    }
    
    func testShowItem_ShouldCallSetupIcon() throws {
        let item = try XCTUnwrap(StatementMocks().fetchSalesRecipeStatement())
        sut.show(item: item)
        
        XCTAssertEqual(displaySpy.setupIconCount, 1)
    }
    
    func testShowItem_WhenSalesRecipe_ShouldShowAcessoryIcon() throws {
        let item = try XCTUnwrap(StatementMocks().fetchSalesRecipeStatement())
        sut.show(item: item)
        
        XCTAssertEqual(displaySpy.setupArrowLabelCount, 1)
        XCTAssertEqual(displaySpy.arrowLabelIsHidden, false)
    }
    
    func testShowItem_WhenJudicialBlock_ShouldShowAcessoryIcon() throws {
        let item = try XCTUnwrap(StatementMocks().fetchSalesRecipeStatement())
        sut.show(item: item)
        
        XCTAssertEqual(displaySpy.setupArrowLabelCount, 1)
        XCTAssertEqual(displaySpy.arrowLabelIsHidden, false)
    }
    
    func testShowItem_WhenPix_ShouldShowAcessoryIcon() throws {
        let item = try XCTUnwrap(StatementMocks().fetchPixTransferStatement())
        sut.show(item: item)
        
        XCTAssertEqual(displaySpy.setupArrowLabelCount, 1)
        XCTAssertEqual(displaySpy.arrowLabelIsHidden, false)
    }
    
    func testShowItem_WhenOthers_ShouldHideAcessoryIcon() throws {
        let item = try XCTUnwrap(StatementMocks().fetchPicPayWithdrawalProgressStatement())
        sut.show(item: item)
        
        XCTAssertEqual(displaySpy.setupArrowLabelCount, 1)
        XCTAssertEqual(displaySpy.arrowLabelIsHidden, true)
    }
    
    func testShowItem_WhenIsApprovedOrFinished_ShouldHideStatus() throws {
        let item = try XCTUnwrap(StatementMocks().fetchSalesRecipeStatement())
        sut.show(item: item)
        
        XCTAssertEqual(displaySpy.hideStatusCount, 1)
    }
    
    func testShowItem_WhenIsNotApprovedOrFinished_ShouldShowStatus() throws {
        let item = try XCTUnwrap(StatementMocks().fetchPicPayWithdrawalProgressStatement())
        sut.show(item: item)
        
        XCTAssertEqual(displaySpy.showStatusCount, 1)
    }
}
