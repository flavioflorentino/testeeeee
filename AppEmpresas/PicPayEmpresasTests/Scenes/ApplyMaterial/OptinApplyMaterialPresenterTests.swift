import XCTest
@testable import PicPayEmpresas

final class OptinApplyMaterialPresenterTests: XCTestCase {
    private lazy var displaySpy = OptinApplyMaterialDisplaySpy()
    
    lazy var sut: OptinApplyMaterialPresenter = {
        let presenter = OptinApplyMaterialPresenter(coordinator: OptinApplyMaterialCoordinator())
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testShowDeclinePopUp_WhenReceiveActionFromViewModel_ShouldCallPopupCount() {
        // Given
        sut.showDeclinePopUp()
        
        // Then
        XCTAssertEqual(displaySpy.popupViewCount, 1)
    }
    
    func testCreateErrorView_WhenReceiveActionFromViewModel_ShouldErrorCountInDisplay() {
        sut.createErrorView()
        
        XCTAssertEqual(displaySpy.errorViewCount, 1)
    }
}

fileprivate final class OptinApplyMaterialDisplaySpy: OptinApplyMaterialDisplay {
    private(set) var popupViewCount = 0
    private(set) var errorViewCount = 0
    
    func createPopUpError() {
        popupViewCount += 1
    }
    
    func createErrorView() {
        errorViewCount += 1
    }
}
