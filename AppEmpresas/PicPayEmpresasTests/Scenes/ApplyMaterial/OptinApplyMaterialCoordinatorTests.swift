import XCTest
import UI
@testable import PicPayEmpresas

private final class OptinApplyMaterialCoordinatorTests: XCTestCase {
    private lazy var controllerSpy = ViewControllerSpy()
    private lazy var navigationSpy = NavigationControllerSpy(rootViewController: controllerSpy)
    
    private lazy var sut: OptinApplyMaterialCoordinator = {
        let coordinator = OptinApplyMaterialCoordinator()
        coordinator.viewController = navigationSpy.topViewController
        return coordinator
    }()
    
    func testShowAlert_WhenReceiveActionFromPresenter_ShouldCallPresenterCount() {
        let popUp = UI.PopupViewController(title: "titulo teste")
        sut.perform(action: .showAlert(popUp))
        
        XCTAssertEqual(controllerSpy.presentViewControllerCounter, 1)
        XCTAssertTrue(controllerSpy.presentViewController is UI.PopupViewController)
    }
    
    func testShowErrorView_WhenReceiveActionFromPresenter_ShouldCallPresenterCount() {
        let controller = ErrorViewController()
        sut.perform(action: .showErrorView(controller))
        
        XCTAssertEqual(controllerSpy.presentViewControllerCounter, 1)
        XCTAssertTrue(controllerSpy.presentViewController is ErrorViewController)
    }
    
    func testDismissPresents_WhenReceiveActionFromPresenter_ShouldCallDismissCount() {
        sut.perform(action: .dismissAlert)
        
        XCTAssertEqual(controllerSpy.dismissCount, 1)
    }
    
    func testBackController_WhenReceiveActionFromPresenter_ShouldCallPopControllerCount() {
        sut.perform(action: .back)
        XCTAssertEqual(controllerSpy.dismissCount, 1)
        XCTAssertEqual(navigationSpy.popToRoot, 1)
    }
    
    func testAddressScreen_WhenReceiveActionFromPresenter_ShouldCallPushControllerCount() {
        sut.perform(action: .addressScreen)
        
        XCTAssertTrue(navigationSpy.currentViewController is MaterialAddressViewController)
    }
}
