import XCTest
import Core
@testable import PicPayEmpresas

private final class OptinApplyMaterialViewModelTests: XCTestCase {
    private let serviceMock = OptinApplyMaterialServiceMock()
    private let presenterMock = OptinApplyMaterialPresenterMock()
    lazy var sut: OptinApplyMaterialViewModel? = {
        return OptinApplyMaterialViewModel(service: serviceMock, presenter: presenterMock)
    }()
    
    func testActionButton_WhenButton_ShouldGoToAdressScreen() throws {
        sut?.actionButton(type: .button)
        
        XCTAssertEqual(presenterMock.didNextStepActionCallsCount, 1)
        XCTAssertTrue(presenterMock.didNextStepActionReceivedInvocations.contains(.addressScreen))
    }
    
    func testDeclinedPopUp_WhenSuccess_ShouldShowDeclinePopUp() throws {
        sut?.actionButton(type: .link)
        
        XCTAssertEqual(presenterMock.showDeclinePopUpCallsCount, 1)
    }
    
    func testCloseScreen_ShouldCloseSolicitationFlow() {
        sut?.closeScreen()
        
        XCTAssertEqual(presenterMock.closeScreenCallsCount, 1)
    }
    
    func testDeclineMaterial_WhenServiceReturnSuccess_ShouldEndFlow() {
        serviceMock.isSuccess = true
        sut?.declineMaterial()
        
        // didNextStep is called twice: one to dismiss alert and another to do back action
        XCTAssertEqual(presenterMock.didNextStepActionCallsCount, 2)
        XCTAssertTrue(presenterMock.didNextStepActionReceivedInvocations.contains(.dismissAlert))
        XCTAssertTrue(presenterMock.didNextStepActionReceivedInvocations.contains(.back))
    }
    
    func testDeclineMaterial_WhenServiceReturnFalse_ShouldCallCreateErrorView() {
        serviceMock.isSuccess = false
        sut?.declineMaterial()
        
        XCTAssertEqual(presenterMock.createErrorViewCallsCount, 1)
    }
    
    func testNextStep_ShouldDismissScreen() throws {
        sut?.nextStep(action: .dismissAlert)
        
        XCTAssertEqual(presenterMock.didNextStepActionCallsCount, 1)
        XCTAssertTrue(presenterMock.didNextStepActionReceivedInvocations.contains(.dismissAlert))
    }
}

private final class OptinApplyMaterialServiceMock: OptinApplyMaterialServicing {
    var isSuccess = true
    
    func checkOptOut(completion: @escaping (Result<OptoutMaterialSolicitation?, ApiError>) -> Void) {
        guard isSuccess else {
            completion(.failure(ApiError.connectionFailure))
            return
        }
        
        let material = OptoutMaterialSolicitation(
            dontWantBoard: true,
            sellerId: 0,
            updatedAt: "2020-08-08",
            createdAt: "2020-08-07",
            id: 1313
        )
        
        completion(.success(material))
    }
}

private final class OptinApplyMaterialPresenterMock: OptinApplyMaterialPresenting {
    var viewController: OptinApplyMaterialDisplay?

    //MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [OptinApplyMaterialAction] = []

    func didNextStep(action: OptinApplyMaterialAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }

    //MARK: - showDeclinePopUp
    private(set) var showDeclinePopUpCallsCount = 0

    func showDeclinePopUp() {
        showDeclinePopUpCallsCount += 1
    }

    //MARK: - closeScreen
    private(set) var closeScreenCallsCount = 0

    func closeScreen() {
        closeScreenCallsCount += 1
    }

    //MARK: - createErrorView
    private(set) var createErrorViewCallsCount = 0

    func createErrorView() {
        createErrorViewCallsCount += 1
    }
}
