import Foundation
@testable import PicPayEmpresas

final class EnvVariableMock: EnvVariableProtocol {
    let isAnalyticsLogEnabled: Bool
    
    init(isAnalyticsLogEnabled: Bool = false) {
        self.isAnalyticsLogEnabled = isAnalyticsLogEnabled
    }
}
