import AnalyticsModule
import Foundation
@testable import PicPayEmpresas

final class AnalyticsMock: AnalyticsProtocol {
    //MARK: - log
    private(set) var logCallsCount = 0
    private(set) var registerCallsCount = 0
    private(set) var resetCallCount = 0
    private(set) var logReceivedInvocations: [AnalyticsKeyProtocol] = []

    var logClosure: ((AnalyticsKeyProtocol) -> Void)?

    func log(_ event: AnalyticsKeyProtocol) {
        logCallsCount += 1
        logReceivedInvocations.append(event)
        logClosure?(event)
    }

    //MARK: - time
    private(set) var timeCallsCount = 0
    private(set) var timeReceivedInvocations: [AnalyticsKeyProtocol] = []
    var timeClosure: ((AnalyticsKeyProtocol) -> Void)?

    func time(_ event: AnalyticsKeyProtocol) {
        timeCallsCount += 1
        timeReceivedInvocations.append(event)
        timeClosure?(event)
    }

    func register(tracker: ThirdPartyTracker, for: AnalyticsProvider, with: EventSanitizing?) {
        registerCallsCount += 1
    }

    func reset() {
        resetCallCount += 1
    }
}
