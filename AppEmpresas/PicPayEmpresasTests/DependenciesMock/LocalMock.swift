import XCTest
@testable import PicPayEmpresas

final class LocalMock<T: Decodable> {
    func localMock(_ fileName: String, decoder: JSONDecoder = JSONDecoder()) -> T? {
        guard
            let data = Mocker().data(fileName),
            let item = try? decoder.decode(ResponseBiz<T>.self, from: data)
            else {
                return nil
        }
        return item.data
    }

    func localMockV2(_ fileName: String, decoder: JSONDecoder = JSONDecoder()) -> T? {
        guard
            let data = Mocker().data(fileName),
            let item = try? decoder.decode(ResponseBizV2<T>.self, from: data)
            else {
                return nil
        }
        return item.data
    }
    
    func pureLocalMock(_ fileName: String, decoder: JSONDecoder = JSONDecoder()) -> T? {
        guard let data = Mocker().data(fileName) else {
            return nil
        }
        
        do {
            let item = try decoder.decode(T.self, from: data)
            return item
        } catch let error {
            print(error)
            return nil
        }
    }
}

final class Mocker {
    func data(_ fileName: String) -> Data? {
        guard
            let file = Bundle(for: type(of: self)).path(forResource: fileName, ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: file))
            else {
                return nil
        }
        return data
    }
}
