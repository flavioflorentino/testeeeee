import AnalyticsModule
import Core
import CoreSellerAccount
import LegacyPJ
import XCTest
import FeatureFlag
@testable import PicPayEmpresas

/// Don't use this class for testing, use DependencyContainerMock
final class DependencyContainer {
    init() {
        fatalError("Use a DependencyContainerMock on test target")
    }
}

final class DependencyContainerMock: AppDependencies {
    lazy var authManager: AuthManagerProtocol = resolve()
    lazy var mainQueue: DispatchQueue = resolve()
    lazy var locationManager: LocationManagerContract = resolve()
    lazy var apiSeller: ApiSellerProtocol = resolve()
    lazy var apiSignUp: ApiSignUpProtocol = resolve()
    lazy var appManager: AppManagerProtocol = resolve()
    lazy var envVariable: EnvVariableProtocol = resolve()
    lazy var featureManager: FeatureManagerContract = resolve()
    lazy var analytics: AnalyticsProtocol = resolve()
    lazy var keychain: KeychainManagerContract = resolve()
    lazy var kvStore: KVStoreContract = resolve()
    lazy var sellerInfo: SellerInfoManagerProtocol = resolve()
    lazy var appCoordinator: AppCoordinating = resolve()
    lazy var dispatchGroup: DispatchGroup = resolve()
    
    private let dependencies: [Any]
    
    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
}

extension DependencyContainerMock {
    func resolve<T>() -> T {
        let resolved = dependencies.compactMap { $0 as? T }
                
        switch resolved.first {
        case .none:
            fatalError("DependencyContainerMock could not resolve dependency: \(T.self)\n")
        case .some where resolved.count > 1:
            fatalError("DependencyContainerMock resolved mutiple dependencies for: \(T.self)\n")
        case .some(let mock):
            return mock
        }
    }
    
    func resolve() -> DispatchQueue {
        let resolved = dependencies.compactMap { $0 as? DispatchQueue }.first
        return resolved ?? DispatchQueue(label: "DependencyContainerMock")
    }
    
    func resolve() -> AuthManagerProtocol {
        let resolved = dependencies.compactMap { $0 as? AuthManagerProtocol }.first
        return resolved ?? AuthManagerMock()
    }
    
    func resolve() -> EnvVariableProtocol {
        let resolved = dependencies.compactMap { $0 as? EnvVariableProtocol }.first
        return resolved ?? EnvVariableMock()
    }

    func resolve() -> FeatureManagerContract {
        let resolved = dependencies.compactMap { $0 as? FeatureManagerContract }.first
        return resolved ?? FeatureManagerMock()
    }
    
    func resolve() -> AnalyticsProtocol {
        let resolved = dependencies.compactMap { $0 as? AnalyticsProtocol }.first
        return resolved ?? AnalyticsMock()
    }
    
    func resolve() -> KVStoreContract {
        let resolved = dependencies.compactMap { $0 as? KVStoreContract }.first
        return resolved ?? KVStoreMock()
    }
    
    func resolve() -> ApiSellerProtocol {
        let resolved = dependencies.compactMap { $0 as? ApiSellerProtocol }.first
        return resolved ?? ApiSellerMock()
    }
    
    func resolve() -> ApiSignUpProtocol {
        let resolved = dependencies.compactMap { $0 as? ApiSignUpProtocol }.first
        return resolved ?? ApiSignUpMock()
    }
    
    func resolve() -> SellerInfoManagerProtocol {
        let resolved = dependencies.compactMap { $0 as? SellerInfoManagerProtocol }.first
        return resolved ?? SellerInfoManagerMock()
    }
    
    func resolve() -> AppManagerProtocol {
        let resolved = dependencies.compactMap { $0 as? AppManagerProtocol }.first
        return resolved ?? AppManagerMock()
    }

    func resolve() -> AppCoordinating {
        let resolved = dependencies.compactMap { $0 as? AppCoordinating }.first
        return resolved ?? AppCoordinatorMock()
    }
    
    func resolve() -> DispatchGroup {
        let resolved = dependencies.compactMap { $0 as? DispatchGroup }.first
        return resolved ?? DispatchGroup()
    }
}
