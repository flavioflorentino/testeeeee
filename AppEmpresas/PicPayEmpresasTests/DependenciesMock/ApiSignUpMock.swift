import Core
import Foundation
@testable import PicPayEmpresas

final class ApiSignUpMock: ApiSignUpProtocol {
    var verifySellerCompletion: Result<CompanyInfo?, ApiError> = .success(nil)
    
    func verifySeller(_ cnpj: String, completion: @escaping ((Result<CompanyInfo?, ApiError>) -> Void)) {
        completion(verifySellerCompletion)
    }
}
