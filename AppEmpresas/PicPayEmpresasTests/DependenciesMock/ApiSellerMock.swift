import Core
import Foundation
import LegacyPJ
@testable import PicPayEmpresas

final class ApiSellerMock: ApiSellerProtocol {
    var dataCompletion: Result<Seller, LegacyPJError> = .success(Seller())
    
    func data(_ completion: @escaping(Result<Seller, LegacyPJError>) -> Void) {
        completion(dataCompletion)
    }
}
