import Foundation
import UI
import UIKit
@testable import PicPayEmpresas

final class AppCoordinatorMock: AppCoordinating {
    private(set) var displayRootCallsCount = 0

    func displayRoot() {
        displayRootCallsCount += 1
    }

    private(set) var displayAuthenticatedCallsCount = 0

    func displayAuthenticated() {
        displayAuthenticatedCallsCount += 1
    }

    private(set) var displayWelcomeCallsCount = 0

    func displayWelcome() {
        displayWelcomeCallsCount += 1
    }

    private(set) var displayAccountAdditionCallsCount = 0

    func displayAccountAddition(with navigation: UINavigationController) {
        displayAccountAdditionCallsCount += 1
    }
}
