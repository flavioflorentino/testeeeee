import Foundation
import UI
import UIKit
@testable import PicPayEmpresas

final class AppManagerMock: AppManagerProtocol {
    var localData: AppManager.AppLocalData = AppManager.AppLocalData()

    func initialRootViewController() -> UIViewController {
        return UIViewController()
    }

    func presentAuthenticatedViewController() {
        // TODO: Must understand how it works to make a good test
    }

    func presentUnloggedViewController() {
        // TODO: Must understand how it works to make a good test
    }

    func logoutUserForInvalidToken() {
        // TODO: Must understand how it works to make a good test
    }

}
