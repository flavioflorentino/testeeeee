#!/bin/sh

#Verify if the environment variables exists and are not empty.
if [ -n "${IS_PROD_TARGET}" ] || [ -n "${IS_HOMOLOG_TARGET}" ]; then
     echo "Validando variáveis de ambiente"
    : ${PJ_NEWRELIC_APP_TOKEN:?}
    : ${PJ_MIXPANEL_TOKEN:?}
    : ${PJ_APPSFLYER_DEV_TOKEN:?}
    : ${PJ_EVENTTRACKER_API_TOKEN:?}
    : ${PJ_EVENTTRACKER_PROXY_API_TOKEN:?}
fi

if [ -n "${IS_PROD_TARGET}" ]; then
     echo "Gerando credenciais para o ambiente de produção da aplicação PJ"
     sourcery --config Setup/Sourcery/sourcery-prod.yml
 elif [ -n "${IS_HOMOLOG_TARGET}" ]; then
     echo "Gerando credenciais para o ambiente de homologação/QA da aplicação PJ"
     sourcery --config Setup/Sourcery/sourcery-homolog.yml
 else
     echo "Gerando credenciais para o ambiente de desenvolvimento da aplicação PJ"
     sourcery --config Setup/Sourcery/sourcery-develop.yml
fi
