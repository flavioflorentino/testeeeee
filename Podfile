# source 'https://github.com/CocoaPods/Specs.git'
source 'https://cdn.cocoapods.org/'

platform :ios, '10.3'
use_frameworks!

workspace 'PicPay'

def shared_pods
  alamofire_pods
  pod 'SDWebImage'
end

def ui_pods
  pod 'SnapKit', '~> 5.0.1'
  pod 'SkeletonView', '~> 1.7.0'
  pod 'SkyFloatingLabelTextField', '~> 3.0'
  pod 'lottie-ios'
end

def iq_keyboard_pods
  pod 'IQKeyboardManagerSwift', '~> 6.5.0'
end

def validators
  pod 'Validator', '~> 3.2.1'
end

def starscream_pod
  # TODO: - voltar com a versão do release assim que sair
  pod 'Starscream', '~> 4.0.4'
end

def security_pods
     pod 'CryptoSwift', '~> 1.3'
end

def stub_pods
  pod 'OHHTTPStubs/Swift', '~> 8.0'
end

def sendbird_pods
  pod 'SendBirdSDK', '3.0.218'
  pod 'SendBirdUIKit', '2.0.9'
end

def swifty_json_pods
  pod 'SwiftyJSON', '~> 4.0'
end

def alamofire_pods
  pod 'Alamofire', '~> 4.9'
end 

def core_pods
  pod 'KeychainAccess'
end

def new_relic
  pod 'NewRelicAgent', '~> 7.0.0'
end

def main_pods
  swifty_json_pods
  ui_pods
  validators
  iq_keyboard_pods

  pod 'MessageKit', '3.1.0'
  pod 'MQTTClient/Websocket'
  pod 'Atributika'
  pod 'CardIO', '~> 5.4.1'
  pod 'Texture'
  pod 'ZXingObjC', '~> 3.6.4'
  pod 'Mixpanel-swift'
  pod 'Cache'
  pod 'Charts'
  pod 'CarbonKit'
  pod 'AMPopTip'
  pod 'CropViewController', '2.5.3'
  pod 'AppsFlyerFramework'
  pod 'Adyen3DS2', '~> 2.1.0-rc.6'
  pod 'ZendeskSupportSDK', '~> 5.0'
  pod 'PINCache'
  pod 'MQTTClient/Websocket'

  starscream_pod
  security_pods
  core_pods
  new_relic
  
  #Firebase
  pod 'Firebase/Analytics'
  pod 'Firebase/RemoteConfig'
  pod 'Firebase/DynamicLinks'

  # Google
  pod 'GoogleConversionTracking', '~> 3.4.0'

  # Sendbird
  sendbird_pods

  # Other dependencies
  pod 'GZIP', '~> 1.2'
end

target 'PicPay' do
  project 'AppPicPay/PicPay.project'
  use_frameworks!

  shared_pods
  main_pods

  target 'PicPayRecentsExtension' do
    inherit! :search_paths
    use_frameworks!
    new_relic
    shared_pods
  end

  target 'PicPayTests' do
    inherit! :search_paths
    stub_pods
  end

  target 'PicPayFunctionalTests' do
    inherit! :search_paths
    stub_pods

    pod 'KIF'
  end
end

def biz_pods
  shared_pods
  ui_pods
  swifty_json_pods
  core_pods
  validators
  new_relic
  iq_keyboard_pods

  pod 'InputMask', '5.0.0'
  pod 'SVProgressHUD'
  pod 'ReachabilitySwift'

  #Analytics
  pod 'AppsFlyerFramework'
  pod 'Mixpanel-swift'

  #Firebase
  pod 'Firebase/Analytics'
  pod 'Firebase/RemoteConfig'

  #Support
  pod 'ZendeskSupportSDK', '~> 5.0'
end

target 'PicPayEmpresas' do
  project 'AppEmpresas/PicPayEmpresas.project'
  use_frameworks!

  biz_pods

  target 'PicPayEmpresasTests' do
    inherit! :search_paths
    use_frameworks!
  end
end

post_install do |installer|
  installer.pods_project.root_object.attributes['LastSwiftMigration'] = 9999
  installer.pods_project.root_object.attributes['LastSwiftUpdateCheck'] = 9999
  installer.pods_project.root_object.attributes['LastUpgradeCheck'] = 9999
    
  installer.pods_project.targets.each do |target|
    target.build_configurations.each do |config|
      # config.build_settings['SWIFT_VERSION'] = '5.0'
      config.build_settings['GCC_WARN_INHIBIT_ALL_WARNINGS'] = "YES"
      config.build_settings['SWIFT_SUPPRESS_WARNINGS'] = "YES"
      config.build_settings['LD_NO_PIE'] = 'NO'
      config.build_settings['CLANG_ENABLE_CODE_COVERAGE'] = 'NO'
      config.build_settings.delete 'IPHONEOS_DEPLOYMENT_TARGET'
      config.build_settings.delete 'SWIFT_VERSION'
    end
  end
  
  puts "Fix Xcode 12 build for simulator on M1"
  installer.pods_project.targets.each do |target|
    target.build_configurations.each do |config|
      config.build_settings["EXCLUDED_ARCHS[sdk=iphonesimulator*]"] = "arm64"
    end
  end
  
end

target 'Core' do
  project 'Modules/Core/Core.project'
  core_pods
  
  target 'CoreTests' do
    inherit! :search_paths
  end
  
  target 'CoreSample' do
    inherit! :complete
    platform :ios, '13.0'
  end
end

target 'Card' do
  project 'Modules/Card/Card.project'
  ui_pods
  security_pods
  core_pods
  validators
  iq_keyboard_pods

  target 'CardSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'CardTests' do
    inherit! :search_paths
  end
end

target 'PCI' do
  project 'Modules/PCI/PCI.project'
  core_pods
  
  target 'PCISample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'PCITests' do
    inherit! :search_paths
    stub_pods
  end
end

target 'UI' do
  project 'Modules/UI/UI.project'
  ui_pods
  core_pods

  target 'UISample' do
    inherit! :complete
    platform :ios, '14.0'
  end
end

target 'Loan' do
  project 'Modules/Loan/Loan.project'
  ui_pods
  core_pods

  target 'LoanSample' do
    inherit! :complete
    platform :ios, '13.0'
    ui_pods
  end

  target 'LoanTests' do
    inherit! :search_paths
  end
end

target 'FeatureFlag' do
  project 'Modules/FeatureFlag/FeatureFlag.project'
  core_pods
  
  target 'FeatureFlagTests' do
    inherit! :search_paths
  end

  target 'FeatureFlagSample' do
    inherit! :complete
    platform :ios, '13.0'
  end
end

target 'AnalyticsModule' do
  project 'Modules/AnalyticsModule/AnalyticsModule.project'

  target 'AnalyticsModuleTests' do
    inherit! :search_paths
  end
end
target 'VendingMachine' do
  project 'Modules/VendingMachine/VendingMachine.project'
  core_pods
  starscream_pod

  pod 'lottie-ios'

  target 'VendingMachineSample' do
    inherit! :complete
    platform :ios, '13.0'
    ui_pods
  end

  target 'VendingMachineTests' do
    inherit! :search_paths
  end
end

target 'Billet' do
  project 'Modules/Billet/Billet.project'
  ui_pods
  core_pods
  
  target 'BilletSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'BilletTests' do
    inherit! :search_paths
  end
end

target 'Advertising' do
  project 'Modules/Advertising/Advertising.project'
  core_pods
  
  target 'AdvertisingSample' do
    inherit! :complete
    platform :ios, '13.0'
    ui_pods
  end

  target 'AdvertisingTests' do
    inherit! :search_paths
  end
end

target 'P2PLending' do
  project 'Modules/P2PLending/P2PLending.project'
  core_pods
  ui_pods

  target 'P2PLendingSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'P2PLendingTests' do
    inherit! :search_paths
  end
end

target 'SecurityModule' do
  project 'Modules/SecurityModule/SecurityModule.project'

  ui_pods
  security_pods
  core_pods
  
  target 'SecurityModuleSample' do
    inherit! :complete
    platform :ios, '13.0'
    ui_pods
  end

  target 'SecurityModuleTests' do
    inherit! :search_paths
  end
end

target 'IdentityValidation' do
  project 'Modules/IdentityValidation/IdentityValidation.project'

  ui_pods
  core_pods
  
  target 'IdentityValidationSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'IdentityValidationTests' do
    inherit! :search_paths
  end
end

target 'CustomerSupport' do
  project 'Modules/CustomerSupport/CustomerSupport.project'

  ui_pods
  core_pods
  iq_keyboard_pods

  target 'CustomerSupportSample' do
    inherit! :complete
    platform :ios, '13.0'
  	pod 'ZendeskSupportSDK', '~> 5.0'
  end

  target 'CustomerSupportTests' do
    inherit! :search_paths
  end
end

target 'Registration' do
  project 'Modules/Registration/Registration.project'

  ui_pods
  security_pods
  core_pods
  
  target 'RegistrationSample' do
    inherit! :complete
    platform :ios, '13.0'

    iq_keyboard_pods
  end

  target 'RegistrationTests' do
    inherit! :search_paths
  end
end

target 'CoreSentinel' do
  project 'Modules/CoreSentinel/CoreSentinel.project'

  target 'CoreSentinelSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'CoreSentinelTests' do
    inherit! :search_paths
  end
end

target 'IssueReport' do
  project 'Modules/IssueReport/IssueReport.project'

  ui_pods
  core_pods
  
  target 'IssueReportSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'IssueReportTests' do
    inherit! :search_paths
  end
end

target 'Address' do
  project 'Modules/Address/Address.project'

  ui_pods
  core_pods

  target 'AddressSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'AddressTests' do
    inherit! :search_paths
  end
end

target 'TFA' do
  project 'Modules/TFA/TFA.project'

  ui_pods
  security_pods
  core_pods
  validators
  
  target 'TFASample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'TFATests' do
    inherit! :search_paths
  end
end

target 'Feed' do
  project 'Modules/Feed/Feed.project'

  ui_pods
  core_pods

  target 'FeedSample' do
    inherit! :complete
    platform :ios, '13.0'

    ui_pods
  end

  target 'FeedTests' do
    inherit! :search_paths
  end
end

target 'Statement' do
  project 'Modules/Statement/Statement.project'

  ui_pods
  core_pods

  target 'StatementSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'StatementTests' do
    inherit! :search_paths
  end
end

target 'Search' do
  project 'Modules/Search/Search.project'

  ui_pods
  core_pods

  target 'SearchSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'SearchTests' do
    inherit! :search_paths
  end
end

target 'Store' do
  project 'Modules/Store/Store.project'

  ui_pods
  core_pods
  new_relic
  
  target 'StoreSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'StoreTests' do
    inherit! :search_paths
  end
end

target 'AssetsKit' do
  project 'Modules/AssetsKit/AssetsKit.project'

  target 'AssetsKitSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'AssetsKitTests' do
    inherit! :search_paths
  end
end

target 'PasswordReset' do
  project 'Modules/PasswordReset/PasswordReset.project'

  ui_pods
  core_pods

  target 'PasswordResetSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'PasswordResetTests' do
    inherit! :search_paths
  end
end

target 'PermissionsKit' do
  project 'Modules/PermissionsKit/PermissionsKit.project'

  ui_pods
  core_pods

  target 'PermissionsKitSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'PermissionsKitTests' do
    inherit! :search_paths
  end
end

target 'CoreLegacy' do
  project 'Modules/CoreLegacy/CoreLegacy.project'

  ui_pods
  swifty_json_pods
  core_pods

  target 'CoreLegacySample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'CoreLegacyTests' do
    inherit! :search_paths
  end
end

target 'PIX' do
  project 'Modules/PIX/PIX.project'

  ui_pods
  shared_pods
  core_pods
  iq_keyboard_pods
  
  target 'PIXSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'PIXTests' do
    inherit! :search_paths
  end
end

target 'Validations' do
  project 'Modules/Validations/Validations.project'

  target 'ValidationsSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'ValidationsTests' do
    inherit! :search_paths
  end
end

target 'CorePayment' do
  project 'Modules/CorePayment/CorePayment.project'
  
  target 'CorePaymentTests' do
    inherit! :search_paths
  end
end

target 'Mobility' do
  project 'Modules/Mobility/Mobility.project'

  target 'MobilitySample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'MobilityTests' do
    inherit! :search_paths
  end
end

target 'FactorAuthentication' do
  project 'Modules/FactorAuthentication/FactorAuthentication.project'

  ui_pods
  core_pods

  target 'FactorAuthenticationSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'FactorAuthenticationTests' do
    inherit! :search_paths
  end
end

target 'DirectMessage' do
  project 'Modules/DirectMessage/DirectMessage.project'

  ui_pods
  core_pods

  pod 'MessageKit', '3.1.0'
  pod 'MQTTClient/Websocket'

  target 'DirectMessageSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'DirectMessageTests' do
    inherit! :search_paths
  end
end

target 'EventTracker' do
  project 'Modules/EventTracker/EventTracker.project'

  core_pods

  target 'EventTrackerSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'EventTrackerTests' do
    inherit! :search_paths
  end
end

target 'RegisterBiz' do
  project 'Modules/RegisterBiz/RegisterBiz.project'
  ui_pods
  core_pods
  validators

  target 'RegisterBizSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'RegisterBizTests' do
    inherit! :search_paths
  end
end

target 'DynamicRegistration' do
  project 'Modules/DynamicRegistration/DynamicRegistration.project'
  
  ui_pods
  core_pods
  validators

  target 'DynamicRegistrationSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'DynamicRegistrationTests' do
    inherit! :search_paths
  end
end

target 'Cashout' do
  project 'Modules/Cashout/Cashout.project'

  ui_pods
  core_pods

  target 'CashoutSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'CashoutTests' do
    inherit! :search_paths
  end
end

target 'Socket' do
  project 'Modules/Socket/Socket.project'

  pod 'MQTTClient/Websocket'

  target 'SocketSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'SocketTests' do
    inherit! :search_paths
  end
end

target 'CoreFormatter' do
  project 'Modules/CoreFormatter/CoreFormatter.project'

  target 'CoreFormatterSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'CoreFormatterTests' do
    inherit! :search_paths
  end
end

target 'CashIn' do
  project 'Modules/CashIn/CashIn.project'

  ui_pods
  core_pods

  target 'CashInSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'CashInTests' do
    inherit! :search_paths
  end
end

target 'AuthenticationPJ' do
  project 'Modules/AuthenticationPJ/AuthenticationPJ.project'

  ui_pods
  core_pods

  target 'AuthenticationPJSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'AuthenticationPJTests' do
    inherit! :search_paths
  end
end

target 'ReceiptKit' do
  project 'Modules/ReceiptKit/ReceiptKit.project'

  ui_pods
  core_pods

  target 'ReceiptKitSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'ReceiptKitTests' do
    inherit! :search_paths
  end
end

target 'SearchKit' do
  project 'Modules/SearchKit/SearchKit.project'

  ui_pods
  core_pods
  
  target 'SearchKitSample' do
    inherit! :complete
    platform :ios, '13.0'
    ui_pods
  end
  
  target 'SearchKitTests' do
    inherit! :search_paths
  end
end

target 'LoanOffer' do
  project 'Modules/LoanOffer/LoanOffer.project'
  ui_pods
  core_pods

  target 'LoanOfferSample' do
    inherit! :complete
    platform :ios, '13.0'
    ui_pods
  end

  target 'LoanOfferTests' do
    inherit! :search_paths
  end
end
  
target 'DirectMessageSB' do
  project 'Modules/DirectMessageSB/DirectMessageSB.project'
  ui_pods
  sendbird_pods
  core_pods

  target 'DirectMessageSBSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'DirectMessageSBTests' do
    inherit! :search_paths
  end
end

target 'SettingsBiz' do
  project 'Modules/SettingsBiz/SettingsBiz.project'

  ui_pods
  core_pods

  target 'SettingsBizSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'SettingsBizTests' do
    inherit! :search_paths
  end
end

target 'SearchCore' do
  project 'Modules/SearchCore/SearchCore.project'
  core_pods

  target 'SearchCoreSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'SearchCoreTests' do
    inherit! :search_paths
  end
end

target 'IncomeTaxReturn' do
  project 'Modules/IncomeTaxReturn/IncomeTaxReturn.project'
  ui_pods
  core_pods

  target 'IncomeTaxReturnSample' do
    inherit! :complete
    platform :ios, '13.0'
  end
  
  target 'IncomeTaxReturnTests' do
    inherit! :search_paths
  end
end

target 'AccountShutDownBiz' do
  project 'Modules/AccountShutDownBiz/AccountShutDownBiz.project'
  ui_pods
  core_pods

  target 'AccountShutDownBizSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'AccountShutDownBizTests' do
    inherit! :search_paths
  end
end

target 'AccessibilityKit' do
  project 'Modules/AccessibilityKit/AccessibilityKit.project'

  target 'AccessibilityKitSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'AccessibilityKitTests' do
    inherit! :search_paths
  end
end

target 'LegacyPJ' do
  project 'Modules/LegacyPJ/LegacyPJ.project'

  swifty_json_pods
  alamofire_pods
  core_pods
  
  target 'LegacyPJSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'LegacyPJTests' do
    inherit! :search_paths
  end
end

target 'NetworkPJ' do
  project 'Modules/NetworkPJ/NetworkPJ.project'
  
  core_pods

  target 'NetworkPJSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'NetworkPJTests' do
    inherit! :search_paths
  end
end

target 'LendingHub' do
  project 'Modules/LendingHub/LendingHub.project'
  ui_pods
  core_pods
  
  target 'LendingHubSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'LendingHubTests' do
    inherit! :search_paths
  end
end

target 'CoreSellerAccount' do
  project 'Modules/CoreSellerAccount/CoreSellerAccount.project'
  
  core_pods
  
  target 'CoreSellerAccountSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'CoreSellerAccountTests' do
    inherit! :search_paths
  end
end

target 'AccountManagementPJ' do
  project 'Modules/AccountManagementPJ/AccountManagementPJ.project'
  ui_pods
  core_pods
  iq_keyboard_pods

  target 'AccountManagementPJSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'AccountManagementPJTests' do
    inherit! :search_paths
  end
end

target 'LendingComponents' do
  project 'Modules/LendingComponents/LendingComponents.project'
  ui_pods
  core_pods
  
  target 'LendingComponentsSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'LendingComponentsTests' do
    inherit! :search_paths
  end
end

target 'P2PCollectiveLending' do
  project 'Modules/P2PCollectiveLending/P2PCollectiveLending.project'
  ui_pods
  core_pods

  target 'P2PCollectiveLendingSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'P2PCollectiveLendingTests' do
    inherit! :search_paths
  end
end

target 'InAppReview' do
  project 'Modules/InAppReview/InAppReview.project'

  ui_pods
  core_pods

  target 'InAppReviewSample' do
    inherit! :complete
    platform :ios, '13.0'
  end

  target 'InAppReviewTests' do
    inherit! :search_paths
  end
end
