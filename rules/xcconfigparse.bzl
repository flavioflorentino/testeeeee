def _replace_apple_variables(ctx):
    ctx.actions.expand_template(
        template = ctx.file.src,
        output = ctx.outputs.out,
        substitutions = ctx.attr.replacements,
    )

process_plist_vars = rule(
    attrs = dict(
        src = attr.label(mandatory = True, allow_single_file = [".plist"]),
        replacements = attr.string_dict(allow_empty = False),
    ),
    outputs = {"out": "%{name}-replaced.plist"},
    implementation = _replace_apple_variables,
)

process_entitlements_vars = rule(
    attrs = dict(
        src = attr.label(mandatory = True, allow_single_file = [".entitlements"]),
        replacements = attr.string_dict(allow_empty = False),
    ),
    outputs = {"out": "%{name}-replaced.entitlements"},
    implementation = _replace_apple_variables,
)