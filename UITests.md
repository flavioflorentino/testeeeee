# Testes de Funcionalidades (UITests)

Documentação para implementação e execução dos testes de interface/funcionalidade do picpay.

## Getting Started

Os testes funcionais implementados no projeto tem por finalidade testar as funcionalidades críticas do aplicativo, ou seja, aquelas que estejam diretamente relacionas aos négocio da empresa Ex: (Cadastro, Pagamento P2P, Cadastro de Cartão, etc).

Visando testar apenas as funcionalidades internas do aplicativo as chamadas a API foram mockeadas para remover as interferencias externas causadas pela relação de dependencia dos serviços web. Para cada teste a ser realizado existe um conjunto pre definido de retornos da API.
Para fornecer o serviço de API mockeado foi utilizado o **[Wiremock](http://wiremock.org)** rodando no modo **[Standalone](http://wiremock.org/docs/running-standalone/)**.
O wiremock está no repositório: **ApiMock/wiremock.jar**.

Para testar algumas funcionalidades no aplicativo é necessário simular o estado do usuário/dispositivo (ex.: usuário autênticado ) e para isso foram adicionados racks na inicialização do app para configurar propriedades que simulem o estado necessário.

### Bundle install

Execute `bundle install` na raiz do projeto.

### Java

O serviço de mock é escrito em java, então você vai precisar do Java SDK. [Install java](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)

## Implementação dos testes
Basicamente para implmentaçãos de todos os testes será necessário seguir as seguintes etapas:
* Verificar o estado do usuário/dispositivo necessário para a realização do teste. Caso haja, verificar se esse estado já pode ser simulado através de algum argumento/parametro de inicialização. Se não, esse argumento/parametro deve ser implementado ([como simular estado do usuário/dispositivo](#simulando-estado-do-usuáriodispositivo)).
* Verificar se existe interferencia da api na funcionalidade testada. Caso haja, um mock deverá ser criado para responder a essa chamada ([Criando mock para API](#criando-mock-para-api)).
* Adicionar no inicio da execução do teste o lauch do app com os parâmetros/argumentos necessários para simular o estado do app ([Simulando estado do usuário/dispositivo no aplicativo](#simulando-estado-do-usuáriodispositivo-no-aplicativo)).
* Configurar caso haja necessidade a url de mock da API.
* Escrever o teste gravando os passos de intereção com a interface do app.
* Adicionar os Asserts de validação para as informações/interface que deseja verificar.
* Testar o teste :).

### Simulando estado do usuário/dispositivo
Os racks para simular o estado podem ser passados para o app das seguintes formas:
* **Flag** - Utilizando o argumentos do processo do app. (Obs.: Utilizar o padrão CamelCase Ex.: `ClearUserDefaults`)
* **Valores** - Variáveis de ambiente. (Obs.: Utilizar letras maiúsculas com underscores Ex.: `API_URL`)

#### Simulando estado do usuário/dispositivo no aplicativo
No app os racks são adionados na classe **AppDelegate** no método **prepareForUITest**.

* **Flag** - Exemplo utilizando flag.
```Objective-C
[[[NSProcessInfo processInfo] arguments] containsObject:@"ClearUserDefaults"]
```

* **Valores** - Exemplo utilizando variáveis de ambiente com valores
```Objective-C
[[[NSProcessInfo processInfo] environment] valueForKey:@"API_URL"]
```

#### Simulando estado do usuário/dispositivo no teste
Para simular o estado através do teste será necessário inicializar o app passando os argumentos e variaveis de ambiente.

* **Flag** - Exemplo utilizando flag.
```Swift
let app = XCUIApplication()
app.launchArguments.append(contentsOf: ["UITests","NoAnimations"])`
app.launch()
```

* **Valores** - Exemplo utilizando variáveis de ambiente com valores
```Swift
let app = XCUIApplication()
app.launchEnvironment["API_URL"] = "http://localhost:8080/"
app.launch()
```
### Configurando URL do Mock usando variável de ambienteo
Para facilitar a configuração de argumetos no projeto de UITest na classe base de testes (*BaseTestCase*) foram criados dois métodos:
* **lauchApp(apiPrefix:)** - Método que inicia o app passando a URL da API apontando para o mock.
* **lauchAppAuth(apiPrefix:)** - Método que inicia o app passando a URL da API apontando para o mock e o argumento para força o estado de usuário logado.

### Criando mock para API
O mock da api deve ser criado no wiremock localizado na pasta **ApiMock/mappings**. [Veja a documentação do wiremock para mais detalhes](http://wiremock.org/docs/running-standalone/).
Para que mock funcione de forma especifica para cada teste é necessário adicionar um prefixo para as chamada realizadas durante o teste. Por exemplo: para o teste de login o app faz uma chamada para *#API_URL/userLogin.json*. Para o mock de teste onde o login será feito com sucesso a url do mock deverá possuir um prefix identificador único exemplo *#API_URL/**login_success**/userLogin.json*.

**`Importante!`** Para utilizar o prefixo do mock a variavel de ambiente `API_URL` deve ser configurada no inicio do teste. detalhes em [Simulando estado do usuário/dispositivo no teste](#simulando-estado-do-usuáriodispositivo-no-teste)

#### Gravando chamadas a API
Para facilitar a criação do mock o wiremock disponibiliza uma funcionalidade de *record* que permite fazer o proxy de todas as chamadas para o servidor real e salvar as chamada no formato de mock ao mesmo tempo. [Veja a documentação do wiremock para mais detalhes](http://wiremock.org/docs/running-standalone/).

Para iniciar o mock no mode de gravação basta executar o lane `mock_record` do fastlane.
**Comando:** `bundle exec fastlane mock_record` ou executar o commando manualmente no diretorio ApiMock: *java -jar wiremock.jar --proxy-all="http://appws.ppay.me/" --record-mappings --print-all-network-traffic*.

**`Obs.:`** A opção de gravação não funciona no modo de execução de test do xCode. (O modo de test do xcode está configurado para rodar o mock em outro formato)

## Executando Tests

Os testes podem ser executados diretamente no xcode no menu `*Product->Test* ou (command+U)` ou através do lane `uitest` commando: `bundle exec fastlane uitest`.
