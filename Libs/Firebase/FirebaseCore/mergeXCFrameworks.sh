#!/bin/bash
set -o pipefail
set -e

find . -iname "*.xcframework" -type d -depth 1 -exec bash -c '
	f="{}" 
	name=$(basename /"$f/" .xcframework)

	lipo \
	    -create $name.xcframework/ios-arm64_i386_x86_64-simulator/$name.framework/$name \
	    -output $name.xcframework/ios-arm64_armv7/$name.framework/$name

	cp -nfr ./$name.xcframework/ios-arm64_armv7/$name.framework ./
' - {} \;

rm -Rf *.xcframework