import Foundation

// swiftlint:disable convenience_type
final class <module_name>Resources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: <module_name>Resources.self).url(forResource: "<module_name>Resources", withExtension: "bundle") else {
            return Bundle(for: <module_name>Resources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: <module_name>Resources.self)
    }()
}
