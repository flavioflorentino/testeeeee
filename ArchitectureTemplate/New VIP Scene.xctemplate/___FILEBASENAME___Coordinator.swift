import UIKit

enum ___VARIABLE_moduleName___Action {
}

protocol ___VARIABLE_moduleName___Coordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ___VARIABLE_moduleName___Action)
}

final class ___VARIABLE_moduleName___Coordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ___VARIABLE_moduleName___Coordinating
extension ___VARIABLE_moduleName___Coordinator: ___VARIABLE_moduleName___Coordinating {
    func perform(action: ___VARIABLE_moduleName___Action) {
    }
}
