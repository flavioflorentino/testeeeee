import Core
import Foundation

protocol ___VARIABLE_moduleName___Servicing {
}

final class ___VARIABLE_moduleName___Service {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ___VARIABLE_moduleName___Servicing
extension ___VARIABLE_moduleName___Service: ___VARIABLE_moduleName___Servicing {
}
