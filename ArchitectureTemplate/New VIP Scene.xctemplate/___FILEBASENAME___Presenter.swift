import Foundation

protocol ___VARIABLE_moduleName___Presenting: AnyObject {
    var viewController: ___VARIABLE_moduleName___Displaying? { get set }
    func displaySomething()
    func didNextStep(action: ___VARIABLE_moduleName___Action)
}

final class ___VARIABLE_moduleName___Presenter {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: ___VARIABLE_moduleName___Coordinating
    weak var viewController: ___VARIABLE_moduleName___Displaying?

    init(coordinator: ___VARIABLE_moduleName___Coordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - ___VARIABLE_moduleName___Presenting
extension ___VARIABLE_moduleName___Presenter: ___VARIABLE_moduleName___Presenting {
    func displaySomething() {
        viewController?.displaySomething()
    }
    
    func didNextStep(action: ___VARIABLE_moduleName___Action) {
        coordinator.perform(action: action)
    }
}
