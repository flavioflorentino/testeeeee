# Make it more obvious that a PR is a work in progress and shouldn't be merged yet
warn("PR is classed as Work in Progress") if github.pr_title.include? "[WIP]"

# Warn when there is a big PR
warn("Big PR") if git.lines_of_code > 500

# Verify if PR title contains Jira task
tickets = github.pr_title.scan(/([A-Z][A-Z0-9]+-[0-9]+)/)
if tickets.empty?
  message('This PR does not include any JIRA tasks in the title. (e.g. [TICKET-1234])')
else
  ticket_urls = tickets.map do |ticket|
    "[#{ticket[0]}](https://picpay.atlassian.net/browse/#{ticket[0]})"
  end
  message("JIRA: " + ticket_urls.join(" "))
end

# SwiftLint configuration
`sed -i '' 's/#- todo/- todo/g' .swiftlint.yml`
swiftlint.config_file = '.swiftlint.yml'

# Set fail if a warning is found
swiftlint.strict = false 

# Run linter to show summary
swiftlint.lint_files(fail_on_error: true)

# Run linter to add comments inline
swiftlint.lint_files(inline_mode: true, fail_on_error: true)

# PR title validation to git-chglog pattern 
unless github.pr_title.match?('^(?i)(?:PJ|PF|ANY) - (\w*)(?:\(([\wÀ-ú\$\.\-\*\s]*)\))?\:\s(.*)$')
  warn "Please provide a Pull Request title conforming to the pattern: \n`XX - Type(Squad): [JIRA-001] Descrição da mudança em português` \nReplace `XX` with `PF`, `PJ` or `ANY`(for changes that affects both Apps) \nValid values of Type are: <Added|Changed|Deprecated|Removed|Fixed|Security|Partial>"
end
