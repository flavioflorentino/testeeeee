#!/bin/ksh

# Declaring variables for parameters received from Makefile
module=$1

# Making module name lowercase
lowerModule=$(tr '[A-Z]' '[a-z]' <<< $module)

# Check whether module is empty or PicPay
if [ -z "$module" ] || [ $lowerModule = "picpay" ]; then
    testTarget="//AppPicPay/PicPayTests:PicPayTests"
# Check whether module is PicPay Empresas
elif [ $lowerModule = "biz" ] || [ $lowerModule = "picpayempresas" ] || [ $lowerModule = "empresas" ]; then
    testTarget="//AppEmpresas/PicPayEmpresas:PicPayEmpresasTests"
else
# If module is not PicPay or PicPay Empresas it is a specific module
    testTarget="//Modules/$module:$module"
    testTarget+="Tests"
fi

# Running unit tests of given module
echo Running command: "'"set -o pipefail \&\& bazel test $testTarget"'"
set -o pipefail && bazel test $testTarget