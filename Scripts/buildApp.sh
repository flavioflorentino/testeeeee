#!/bin/sh

app=$1
scheme=$2

prefix="@//Modules/"
runningScheme="${scheme:-Debug}"

# Sanitizing App string to lower case
sanitizedApp=$(tr '[A-Z]' '[a-z]' <<< $app)

# Defining dependencies path and app to be built
if [ -z "$sanitizedApp" ] || [ $sanitizedApp = "picpay" ] || [ $sanitizedApp = "pf" ] ; then
    buildBazelFilePath="Modules/pfDeps.bzl"
    buildingApp="//AppPicPay/PicPay:$runningScheme"
else
    buildBazelFilePath="Modules/pjDeps.bzl"
    buildingApp="//AppEmpresas/PicPayEmpresas:$runningScheme"
fi

# Retrieving all module directories
while read line; do
    if [[ $line == *"#"* || $line == *"["* || $line == *"]"* ]]; then
        continue;
    fi
    
    allModules+="$line "
done < $buildBazelFilePath

# Sanitizing Modules string
modules="${allModules//$prefix/}"
modules="${modules//\"/}"
modules="${modules//\,/}"

# Converting string to array
modules=($modules)

# Creating new string with all module test targets to be tested
for module in "${modules[@]}"; do
    buildTargets+="//Modules/$module "
done

# Running unit tests of all modules and main apps
echo Building App and ${#modules[@]} modules
echo Running command: "'"bazel build $buildTargets $buildingApp"'"
bazel build $buildTargets $buildingApp