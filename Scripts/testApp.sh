#!/bin/sh

app=$1

prefix="@//Modules/"

# Sanitizing App string to lower case
sanitizedApp=$(tr '[A-Z]' '[a-z]' <<< $app)

# Defining dependencies path and app to be built
if [ -z "$sanitizedApp" ] || [ $sanitizedApp = "picpay" ] || [ $sanitizedApp = "pf" ] ; then
    buildBazelFilePath="Modules/pfDeps.bzl"
    testTargets="//AppPicPay/PicPayTests:PicPayTests "
else
    buildBazelFilePath="Modules/pjDeps.bzl"
    testTargets="//AppEmpresas/PicPayEmpresasTests:PicPayEmpresasTests "
fi

# Retrieving all module directories
while read line; do
    if [[ $line == *"#"* || $line == *"["* || $line == *"]"* ]]; then
        continue;
    fi
    
    allModules+="$line "
done < $buildBazelFilePath

# Sanitizing Modules string
modules="${allModules//$prefix/}"
modules="${modules//\"/}"
modules="${modules//\,/}"

# Converting string to array
modules=($modules)

# Creating new string with all module test targets to be tested
for module in "${modules[@]}"; do
    testTargets+="//Modules/$module:$module"
    testTargets+="Tests "
done

# Running unit tests of all modules and main apps
echo Testing App and ${#modules[@]} modules
echo Running command: "'"set -o pipefail \&\& bazel test $testTargets"'"
set -o pipefail && bazel test $testTargets