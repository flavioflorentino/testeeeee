version=$1

brew uninstall -q bazel || (echo "bazel not installed")
curl -fLO "https://github.com/bazelbuild/bazel/releases/download/$version/bazel-$version-installer-darwin-x86_64.sh"
chmod +x "bazel-$version-installer-darwin-x86_64.sh"
./bazel-$version-installer-darwin-x86_64.sh --user
rm "bazel-$version-installer-darwin-x86_64.sh"

export PATH="$PATH:$HOME/bin"