#!/bin/sh

# Get conig of test
config=$1
cache=$2
hasCache="${cache:-false}"

# Retrieving all module directories
allModules=$(find Modules -type d -depth 1)

# Removing Modules prefix
modules="${allModules//Modules\//}"

# Converting string to array
modules=($modules)

# Creating new string with all module test targets to be tested
for module in "${modules[@]}"; do
    # Ignoring empty modules if exists
    if [ -z "$module" ]; then
        continue
    fi
    testTargets+="//Modules/$module:$module"
    testTargets+="Tests "
done

# Adding PicPay and PicPay Empresas test targets
testTargets+="//AppPicPay/PicPayTests:PicPayTests "
testTargets+="//AppEmpresas/PicPayEmpresasTests:PicPayEmpresasTests"

# Running unit tests of all modules and main apps
echo Running tests in PicPay, PicPay Empresas and in ${#modules[@]} modules
echo Running command: "'"bazel test $testTargets"'"

# Defining dependencies path and app to be built
if [ ! -z "${config}" ] && [ "${config}" = "ci" ];
then
    echo "Running tests in $config and config: --remote_upload_local_results=$hasCache"
    set -o pipefail && bazel test --config=$config --remote_upload_local_results=$hasCache $testTargets
else
    echo "Running tests not ci"
    set -o pipefail && bazel test $testTargets
fi