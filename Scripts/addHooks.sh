#!/bin/sh

# Jump to repository root
cd "$(git rev-parse --show-toplevel)"

echo "#!/bin/bash" > .git/hooks/post-checkout
echo "" >> .git/hooks/post-checkout
echo "set -e" >> .git/hooks/post-checkout
echo "[[ \$1 != \$2 && \$3 == 1 ]] && (make generate)"  >> .git/hooks/post-checkout
chmod u+x .git/hooks/post-checkout
echo "\nScript .git/hook/post-checkout successfully installed."

echo "#!/bin/bash" > .git/hooks/post-merge
echo "" >> .git/hooks/post-merge
echo "set -e" >> .git/hooks/post-merge
echo "(make generate)"  >> .git/hooks/post-merge
chmod u+x .git/hooks/post-merge
echo "Script .git/hook/post-merge successfully installed."
	
