#!/bin/sh

# Jump to repository root
cd "$(git rev-parse --show-toplevel)"

moduleName=$1

# Verify if folder already exists
[ ! -d "./Modules/$moduleName" ] && echo "Creating $moduleName module\n" || { echo "$moduleName folder already exists"; exit 1; }

# Copy and enten on template folder
cp -R ModuleTemplate "Modules/$moduleName"
cd "Modules/$moduleName"

# Rename module file and folder name templates
rename(){
  while read oldName
  do    
    newName=$(echo $oldName | sed "s/<module_name>/$moduleName/")
    mv "$oldName" "$newName"
  done
}

find . -type d -name "<module_name>*" | rename
find . -type f -name "<module_name>*" | rename

# Rename module content templates
find . ! -name ".*" -type f -exec sh -c "sed -i '' 's/<module_name>/$1/g' '{}'" \;

# Jump to repository root
cd "$(git rev-parse --show-toplevel)"

# Integrate on Podfile
echo "\ntarget '$moduleName' do\n  project 'Modules/$moduleName/$moduleName.project'\n\n  target '${moduleName}Sample' do\n    inherit! :complete\n    platform :ios, '13.0'\n  end\n\n  target '${moduleName}Tests' do\n    inherit! :search_paths\n  end\nend" >> Podfile


# Integrate on project.yml
sed -i '' '1,/dependencies:/s/dependencies:/&\
      - framework: '"$moduleName"'.framework\
        implicit: true/' AppPicPay/project.yml

sed -i '' '1,/projectReferences:/s/projectReferences:/&\
  '"$moduleName"':\
    path: ..\/Modules\/'"$moduleName"'\/'"$moduleName"'.xcodeproj/' AppPicPay/project.yml

sed -i '' '1,/coverageTargets: \[PicPay, /s/coverageTargets: \[PicPay, /&'"$moduleName"'\/'"$moduleName"', /' AppPicPay/project.yml

sed -i '' '1,/randomExecutionOrder: true/s/randomExecutionOrder: true/&\
        - name: '"$moduleName"'\/'"$moduleName"Tests'\
          parallelizable: false\
          randomExecutionOrder: true/' AppPicPay/project.yml

# Integrate on swiftlint.yml
sed -i '' '1,/excluded:/s/excluded:/&\
  - '"$moduleName"'Sample\
  - '"$moduleName"'Tests/' .swiftlint.yml

sed -i '' '1,/# Swiftlint - Danger workaround/s/# Swiftlint - Danger workaround/&\
  - Modules\/'"$moduleName"'\/'"$moduleName"'Sample\
  - Modules\/'"$moduleName"'\/'"$moduleName"'Tests/' .swiftlint.yml