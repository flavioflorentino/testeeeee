#!/bin/ksh

# Declaring variables for parameters received from Makefile
app=$1
device=$2
scheme=$3

# Get scheme from parameter or default value 'Debug'
runningScheme="${scheme:-Debug}"

# Get device from parameter or default value '11Pro'
chosenDevice="${device:-11Pro}"

# Making module name lowercase
lowerApp=$(tr '[A-Z]' '[a-z]' <<< $app)

#  Check whether app variable was set, default app is PicPay
if [ -z "$app" ]; then
    runningApp="//AppPicPay/PicPay"
else
# Check whether app is PicPay Empresas
    if [ $lowerApp == "biz" ] || [ $lowerApp = "picpayempresas" ] || [ $lowerApp = "pj" ]; then
        runningApp="//AppEmpresas/PicPayEmpresas"
    else
# If app is not PicPay Empresas, default is PicPay
        runningApp="//AppPicPay/PicPay"
    fi
fi

runningApp+=":$runningScheme"

# Creating dictionary of possible devices to start
devices=(["11"]="11" ["11Pro"]="11 Pro" ["11ProMax"]="11 Pro Max" ["12"]="12" ["12Pro"]="12 Pro" ["12ProMax"]="12 Pro Max" ["8"]="8" ["8Plus"]="8 Plus" ["SE"]="SE")

# Using device key passed as parameter to create a device name used to start a simulator
runningDevice="iPhone ${devices[$chosenDevice]}"

# Running selected app with selected device
echo Running command: "'"bazel run $runningApp --ios_simulator_device=$runningDevice --disk_cache=./bazel-cache --verbose_failures"'"
bazel run $runningApp --ios_simulator_device="$runningDevice" --disk_cache=./bazel-cache --verbose_failures