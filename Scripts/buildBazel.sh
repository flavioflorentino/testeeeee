#!/bin/sh

scheme=$1
runningScheme="${scheme:-Debug}"

# Retrieving all module directories
allModules=$(find Modules -type d -depth 1)

# Removing Modules prefix
modules="${allModules//Modules\//}"

# Converting string to array
modules=($modules)

# Creating new string with all module test targets to be tested
for module in "${modules[@]}"; do
    buildTargets+="//Modules/$module "
done

# Adding PicPay and PicPay Empresas test targets
buildTargets+="//AppPicPay/PicPay:$runningScheme "
buildTargets+="//AppEmpresas/PicPayEmpresas:$runningScheme"

# Running unit tests of all modules and main apps
echo Building PicPay, PicPay Empresas and ${#modules[@]} modules
echo Running command: "'"bazel build $buildTargets"'"
bazel build $buildTargets