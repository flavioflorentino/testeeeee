
import os

# Import WebClient from Python SDK (github.com/slackapi/python-slack-sdk)
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError



class SlackUser(object):
    name = ""
    first_name = ""
    last_name = ""
    email = ""

    def __init__(self, name, first_name, last_name, email):
        self.name = name
        self.first_name = first_name
        self.last_name = last_name
        self.email = email

# Put users into the array
def tranform_to_local_object(users_array):
    users_store = []
    for user in users_array:
        if (user["deleted"] == False) and (user["is_bot"] == False) and (user["is_app_user"] == False):
            profile = user["profile"]
            if ("email" in profile) and ("@picpay.com" in profile["email"]):
                name = profile["real_name"]
                splited_name = name.split()
                first_name = splited_name[0]
                splited_name.pop(0)
                last_name = ' '.join(map(str, splited_name))

                slackUser = SlackUser(name, first_name, last_name, profile["email"])
                users_store.append(slackUser)
        
    return users_store

def save_users_in_file(users_array):
    text_file = open("../picpay_lovers_beta.csv", "w")
    test_group = os.environ.get("PILOT_BETA_PICPAY_LOVER")
    for user in users_array:
        # John,Appleseed,appleseed_john@mac.com,group-1
        string_file = '{},{},{},{}\n'.format(user.first_name, user.last_name, user.email, test_group)
        text_file.write(string_file)
    
    text_file.close()

try:
    # Call the users.list method using the WebClient
    client = WebClient(token=os.environ.get("SLACK_BOT_TOKEN"))
    result = client.users_list()
    users = tranform_to_local_object(result["members"])
    save_users_in_file(users)

except SlackApiError as e:
    logger.error("Error creating conversation: {}".format(e))