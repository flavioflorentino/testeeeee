#!/bin/sh

# Jump to repository root
cd "$(git rev-parse --show-toplevel)"

# Install Homebrew dependencies
installHomebrew='/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"'
command -v brew >/dev/null 2>&1 || eval $installHomebrew

echo "\nInstalling Bitrise"
brew update
brew install bitrise || (brew upgrade bitrise && brew cleanup bitrise)

# Generate project
echo "\nGenerating project..."
make clean
make generate

export HOMEBREW_PACKAGES='swiftgen swiftlint xcodegen sourcery'

echo "\nTo manually generate the project run the command 'make generate'\n"
echo "Bitrise instalado com sucesso!\n"
