#!/usr/bin/env python
# coding: utf-8

import os
import subprocess
import json

GIST_ID="cb43057172f1aa4f62241de3b33a3e01"
GITHUB_TOKEN="7a8db08aae58d4716093be7ba720ea7b4dce5d24"

CURRENT_DIR=os.getcwd()
CHANGELOG_HISTORY=CURRENT_DIR + "/AppEmpresas/CHANGELOG-HISTORY.md"
CHANGELOG_GENERATED=CURRENT_DIR + "/AppEmpresas/CHANGELOG-GENERATED.md"
CHANGELOG_JSON=CURRENT_DIR + "/AppEmpresas/CHANGELOG.json"

FIRST_TAG="pj@v1.2.12.."
 
def setup_changelog():
    os.system(
        "command -v git-chglog >/dev/null 2>&1 || " +
        "(brew tap git-chglog/git-chglog; brew install git-chglog)"
    )

def generate_changelog():
    os.system(
        "git fetch --tags -f;" +
        "git-chglog --tag-filter-pattern '(?i)pj@.*' -c ./.chglog/pj_config.yml " + FIRST_TAG + " > '" + CHANGELOG_GENERATED + "'"
    )

def read_changelog():
    with open(CHANGELOG_HISTORY, 'r') as history:
        with open(CHANGELOG_GENERATED, 'r') as generated:
            return generated.read() + '\n' + history.read()
    
def send_changelog_to_gist(changelog):
    gist_content = {
        "description": "PicPay Empresas-iOS Changelog",
        "files": {
            "CHANGELOG.md": {
                "content": str(changelog)
            }
        }
    }

    open(CHANGELOG_JSON, "w").write(json.dumps(gist_content))

    CURL_COMMAND = (
        (
            "curl --verbose \'https://api.github.com/gists/%s\' " + 
            "-X PATCH  -H \'Authorization: Token %s\'" + 
            " -H \'Content-Type: text/json; charset=utf-8\'" + 
            " --data \'@%s\'"
        ) % 
        (
            GIST_ID, 
            GITHUB_TOKEN, 
            CHANGELOG_JSON
        )
    )

    os.system(CURL_COMMAND)

if __name__ == '__main__':
    setup_changelog()
    generate_changelog()
    send_changelog_to_gist(read_changelog())