#!/bin/sh

# Declaring variables for parameters received from Makefile
module=$1
scheme=$2

# Get scheme from parameter or default value 'Debug'
runningScheme="${scheme:-Debug}"

# Making module name lowercase
lowerModule=$(tr '[A-Z]' '[a-z]' <<< $module)

# Check whether module is empty or PicPay
if [ -z "$module" ] || [ $lowerModule = "picpay" ] || [ $lowerModule = "pf" ]; then
    buildTarget="//AppPicPay/PicPay:$runningScheme"
# Check whether module is PicPay Empresas
elif [ $lowerModule == "biz" ] || [ $lowerModule = "picpayempresas" ] || [ $lowerModule = "pj" ]; then
    buildTarget="//AppEmpresas/PicPayEmpresas:$runningScheme"
# If module is not PicPay or PicPay Empresas it is a specific module
else
    buildTarget="//Modules/$module"
fi

# Building module with bazel 
echo Running command: "'"bazel build $buildTarget"'"
bazel build $buildTarget