help:
	@echo "  Usage:\n    \033[36m make <target>\n\n \033[0m Targets:"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "     \033[36m%-30s\033[0m %s\n", $$1, $$2}'

setuptools: ## Install project required tools
	@./Scripts/setup.sh
	
setupbitrise: ## Install bitrise cli
	@./Scripts/bitrise.sh

generate: ## Generate projects, workspace and install pods
	@$(MAKE) generateprojects
	@$(MAKE) installpods
	
generateprojects: ## Generate only .xcodeproj projects using Xcodegen
	@$(MAKE) generatesources
	
	@echo "\nGenerating modules projects"
	@find Modules -type d -depth 1 -exec sh -c "cd {}; [ -f ./project.yml ] && xcodegen" \;
	
	@echo "\nGenerating main projects"
	@(cd AppEmpresas; xcodegen)
	@(cd AppPicPay; xcodegen)
	
generatesources: ## Generate source files with Swiftgen and Sourcery
	@echo "\nGenerating source files with Swiftgen and Sourcery"
	@find . -iname "*.generated.swift" -exec sh -c "git rm --cached {} --ignore-unmatch" \; 
	
	@(cd AppPicPay; mkdir -p PicPay/Generated; swiftgen)
	@(cd AppEmpresas; mkdir -p PicPayEmpresas/Generated; swiftgen)
	
	@(cd Modules; find . -type d -depth 1 -exec sh -c "cd {}; [ -f ./swiftgen.yml ] && (mkdir -p {}/Generated; swiftgen)" \;)
	@find Modules -type d -depth 1 -exec sh -c "cd {}; [ -f pre-generate.sh ] && sh ./pre-generate.sh" \;
		
installpods: ## Install Cocoapods dependencies and generate workspace
	@echo "\nRunning bundle install"
	@bundle install --quiet
	
	@echo "\nInstalling Pods"
	@bundle exec pod install || bundle exec pod install --repo-update
	@./Scripts/cocoaPodsAppTargetsWorkarround.sh
	
template: ## Install and update MVVM-C template to Xcode
	@sh -c "cd ArchitectureTemplate; swiftc install.swift -o ./install; sudo ./install"
	@rm ArchitectureTemplate/install
	
module: ## Create a new module | Usage: `make module name=NewModuleName`
	@./Scripts/makeModuleTemplate.sh $(name)
	@$(MAKE) clean
	@$(MAKE) generate

mocks: ## Generate Spies, Mocks and Fakes from protocols
	@./Scripts/generateMocks.sh
	
clean: ## Cleanup projects
	-@rm -Rf ./PicPay.xcworkspace
	-@rm -Rf ./AppPicPay/PicPay.xcodeproj 
	-@rm -Rf ./AppEmpresas/PicPayEmpresas.xcodeproj 
	-@find Modules -maxdepth 2 -name "*.xcodeproj" -exec rm -r {} \;

cleanall: ## Cleanup projects and pods
	bundle exec pod deintegrate AppPicPay/PicPay.xcodeproj
	bundle exec pod deintegrate AppEmpresas/PicPayEmpresas.xcodeproj
	@$(MAKE) clean
	@git clean -fdx

addhooks: ## Add hooks to `generate` after a successfull checkout, pull or merge
	@./Scripts/addHooks.sh
	
removehooks: ## Remove post-merge and post-checkout hooks
	rm .git/hooks/post-merge .git/hooks/post-checkout 
	
# Unlisted

uploadpfchangelog: # Upload changelog to gist
	@python ./Scripts/pf_changelog.py	

uploadpjchangelog: # Upload changelog to gist
	@python ./Scripts/pj_changelog.py	


# Bazel	
PREFIX := /usr/local
PRODUCT := xchammer.app
BAZEL_VERSION := 3.7.0

bazel_generate: ## Create all Xcode project files and install pods using bazel
	@$(MAKE) generate
	@$(MAKE) install_bazel_pods

install_bazel:
	@./Scripts/installBazel.sh $(BAZEL_VERSION)

setup_bazel: ## Install bazel if needed and then runs bazelGenerate
	@$(MAKE) install_bazel		
	# @mkdir -p $(PREFIX)/bin
	# @ditto Tools/$(PRODUCT) $(PREFIX)/bin/$(PRODUCT)
	# @ln -s $(PREFIX)/bin/$(PRODUCT)/Contents/MacOS/xchammer $(PREFIX)/bin/xchammer
	
	@$(MAKE) bazel_generate	

install_bazel_pods: ## Install pods using bazel
	@[ ! -d "./Pods" ] && (echo "Pods are not installed..."; make generate) || exit 0
	@rsync -r Pods.bazel/ Pods && echo "Installed Cocoapods 'BUILD.bazel' files successfully" || echo "Error installing Cocoapods 'BUILD.bazel' files"

# mchammer:
# 	@bazel clean
# 	-@rm -Rf business-ios.xcodeproj
# 	-@rm -Rf bazel-bin/business-ios.xcodeproj
# 	@bazel build //:business-ios --verbose_failures --sandbox_debug
# 	@open business-ios.xcodeproj
	
dep_graph_pf:
	bazel query 'filter("AppPicPay|Modules", kind("ios_application|apple_framework|swift_library", deps(//AppPicPay/PicPay:Debug)))' --output graph > graph.in; dot -Tpng < graph.in > pfdepgraph.png

dep_graph_pj:
	bazel query 'kind("ios_application|apple_framework|swift_library", deps(//AppEmpresas/PicPayEmpresas:Debug))' --output graph > graph.in; dot -Tpng < graph.in > pjdepgraph.png

# Commands for build modules with bazel
# make build ~> make build //AppPicPay/PicPay:Debug
# make build module=Loan ~> make build //Modules/Loan
build: ## Build an specific module with a given scheme using bazel
	@./Scripts/buildModuleBazel.sh $(module) $(scheme)

build_pf: ## Build PicPay app module and it's dependencies using bazel
	@./Scripts/buildApp.sh

build_pj: ## Build PicPay Empresas app module and it's dependencies using bazel
	@./Scripts/buildApp.sh pj

build_pf_release: ## Build PicPay app module and it's dependencies in Production scheme using bazel
	@./Scripts/buildApp.sh pf Release

build_pj_release: ## Build PicPay Empresas app module and it's dependencies in Production scheme using bazel
	@./Scripts/buildApp.sh pj Release

# make build_all ~> make build //Modules/Address ... //Modules/VendingMachine //AppPicPay/PicPay:Debug
build_all:  ## Build all modules with a given scheme using bazel
	@./Scripts/buildBazel.sh $(scheme)

# Commands for run apps (PicPay | PicPay Empresas) with bazel
# make run ~> make run app=PicPay device=iPhone 11 Pro scheme=Debug
run: ## Run an app (PicPay | PicPay Empresas) with a given device and scheme using bazel
	@./Scripts/runBazel.sh $(app) $(device) $(scheme)

run_pf: ## Run PicPay app with iPhone 12 Pro and Debug scheme using bazel
	@$(MAKE) run app=PicPay device=12ProMax

run_pj:  ## Run PicPay Empresas app with iPhone 8 and Debug scheme using bazel
	@$(MAKE) run app=biz device=12ProMax

run_pf_release: ## Run PicPay app with iPhone 12 Pro and Production scheme using bazel
	@$(MAKE) run app=PicPay device=12ProMax scheme=Release

run_pj_release: ## Run PicPay Empresas app with iPhone 8 and Production scheme using bazel
	@$(MAKE) run app=biz device=12ProMax scheme=Release

# Commands for testing a module with bazel
# make test ~> make test //AppPicPay/PicPay:PicPayTests
# make test module=Loan ~> make test //Modules/Loan:LoanTests
test: ## Test an specific module using bazel
	@./Scripts/testModuleBazel.sh $(module)

test_pf: ## Test PicPay and it's dependencies using bazel
	@./Scripts/testApp.sh

test_pj: ## Test PicPay Empresas and it's dependencies using bazel
	@./Scripts/testApp.sh pj

# make testAll ~> make test //Modules/Address/AddressTests ... //Modules/VendingMachineTests //AppPicPay/PicPay:PicPayTests
test_all: ## Test all targets using bazel
	@./Scripts/testBazel.sh

test_all_ci:
	@./Scripts/testBazel.sh ci true

# Clean all projects, workspace and cached files
bazel_clean: ## Clears Xcode projects and then clears bazel cache
	@$(MAKE) cleanall
	bazel clean

mchammer:
	# @bazel clean
	-@rm -Rf business-ios.xcodeproj
	-@rm -Rf bazel-bin/business-ios.xcodeproj
	@bazel build //:PicPay-Empresas --verbose_failures --sandbox_debug
	@open business-ios.xcodeproj
