platform :ios do
    current_fastfile = "\n\n==== [LANE FROM APOLLO FASTFILE] ====\n\n"
    app_path = "./Modules/UI"

    lane :apollo_beta do |options|
        print(current_fastfile)
        is_readonly = options[:readonly]
            
        apollo_update_build_number
        clear_derived_data
        setup_match_store_dev(readonly: is_readonly)

        build_app(
            workspace: "PicPay.xcworkspace",
            scheme: "UISample",
            configuration: "Release",
            output_directory: "./build",
            output_name: "Apollo-Sample.ipa",
            include_symbols: true,
            include_bitcode: false,
            export_options: {
                method: "app-store",
                provisioningProfiles: { 
                    "com.picpay.apollo.sample" => "match AppStore com.picpay.apollo.sample",
                }
            }
        )
        
        pilot(
            skip_waiting_for_build_processing: false,
            distribute_external: true,
            notify_external_testers: true,
            ipa: "./build/Apollo-Sample.ipa",
            changelog: "Ajustes e melhorias",
            groups: [ENV["PILOT_BETA_GROUP_PROD_APOLLO"]],
            beta_app_review_info: {
                contact_email: "mobile@picpay.com",
                contact_first_name: "PicPay",
                contact_last_name: "Mobile",
                contact_phone: "27999666898",
                notes: "Correções, melhorias e muitos mais ;)"
            }
        )

        slack_beta
    end    

    lane :apollo_update_build_number do |options|
        print(current_fastfile)
        build_number = ENV['BITRISE_BUILD_NUMBER'].nil? ? prompt(text: "Digite o numero do build") : ENV['BITRISE_BUILD_NUMBER']
        full_build_number = "#{get_info_plist_value(path: "#{app_path}/UISample/Info.plist", key: "CFBundleShortVersionString")}.#{build_number}"
        set_info_plist_value(path: "#{app_path}/UISample/Info.plist", key: "CFBundleVersion", value: full_build_number)
    end

    lane :apollo_generate_next_version do 
        print(current_fastfile)
        path_configs = "Modules/UI/Configs"
        apollo_version = get_version_number(
            xcodeproj: "Modules/UI/UI.xcodeproj",
            target: "UISample"
        )

        array_numbers = apollo_version.split('.')
        lastNumber = Integer(array_numbers.last) + 1
        array_numbers[array_numbers.count - 1] = "#{lastNumber}"
        update_version = array_numbers.join(".")

        puts "current version: #{apollo_version}"
        puts "update Version: #{update_version}"

        update_xcconfig_value(
            path: "#{path_configs}/DebugApp.xcconfig",
            name: "MARKETING_VERSION",
            value: update_version
        )
        update_xcconfig_value(
            path: "#{path_configs}/HomologApp.xcconfig",
            name: "MARKETING_VERSION",
            value: update_version
        )
        update_xcconfig_value(
            path: "#{path_configs}/ProdApp.xcconfig",
            name: "MARKETING_VERSION",
            value: update_version
        )
    end
end