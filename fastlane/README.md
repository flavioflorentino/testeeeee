fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
### appconnect_api_key
```
fastlane appconnect_api_key
```


----

## iOS
### ios setup_add_device
```
fastlane ios setup_add_device
```

### ios setup_match_dev
```
fastlane ios setup_match_dev
```

### ios setup_match_store
```
fastlane ios setup_match_store
```

### ios setup_match_store_dev
```
fastlane ios setup_match_store_dev
```

### ios pf_new_relic
```
fastlane ios pf_new_relic
```

### ios pf_beta
```
fastlane ios pf_beta
```

### ios pf_beta_dev
```
fastlane ios pf_beta_dev
```

### ios pf_update_build_number
```
fastlane ios pf_update_build_number
```

### ios pf_generate_next_version
```
fastlane ios pf_generate_next_version
```

### ios pf_beta_dev_picpay_lover
```
fastlane ios pf_beta_dev_picpay_lover
```

### ios import_picpay_lovers_from_slack_to_csv_file
```
fastlane ios import_picpay_lovers_from_slack_to_csv_file
```

### ios pj_new_relic
```
fastlane ios pj_new_relic
```

### ios pj_beta
```
fastlane ios pj_beta
```

### ios pj_beta_dev
```
fastlane ios pj_beta_dev
```

### ios pj_update_build_number
```
fastlane ios pj_update_build_number
```

### ios pj_generate_next_version
```
fastlane ios pj_generate_next_version
```

### ios apollo_beta
```
fastlane ios apollo_beta
```

### ios apollo_update_build_number
```
fastlane ios apollo_update_build_number
```

### ios apollo_generate_next_version
```
fastlane ios apollo_generate_next_version
```

### ios add_device
```
fastlane ios add_device
```
register device name: device:
### ios match_dev
```
fastlane ios match_dev
```
Refresh development app provisioning profiles
### ios match_store
```
fastlane ios match_store
```
Refresh appstore app provisioning profiles
### ios match_store_dev
```
fastlane ios match_store_dev
```

### ios new_relic
```
fastlane ios new_relic
```

### ios beta
```
fastlane ios beta
```
Envia um novo build para o testflight.
### ios promove_beta_picpay_lover
```
fastlane ios promove_beta_picpay_lover
```
Envia para PicPay Lovers versão da dev
### ios beta_dev
```
fastlane ios beta_dev
```

### ios biz_new_relic
```
fastlane ios biz_new_relic
```

### ios biz_beta
```
fastlane ios biz_beta
```
Envia um novo build do PicPayEmpresas para o testflight.
### ios biz_beta_dev
```
fastlane ios biz_beta_dev
```
Envia um novo build do PicPayEmpresas Homolog para o testflight.
### ios upload_changelogs
```
fastlane ios upload_changelogs
```
Upload generated changelogs
### ios beta_apollo
```
fastlane ios beta_apollo
```
Apollo beta
### ios release_and_tag
```
fastlane ios release_and_tag
```
release and tag versions
### ios update_version_apps
```
fastlane ios update_version_apps
```
update version for PF and PJ
### ios auto_update_version_pf
```
fastlane ios auto_update_version_pf
```
Auto update version for next release PF
### ios auto_update_version_pj
```
fastlane ios auto_update_version_pj
```
Auto update version for next release PJ
### ios upload_pf_changelog
```
fastlane ios upload_pf_changelog
```
Upload generated PicPay PF changelog
### ios upload_pj_changelog
```
fastlane ios upload_pj_changelog
```
Upload generated PicPay Empresas PJ changelog
### ios slack_beta
```
fastlane ios slack_beta
```

### ios update_build_number
```
fastlane ios update_build_number
```

### ios update_biz_build_number
```
fastlane ios update_biz_build_number
```

### ios test_all
```
fastlane ios test_all
```
Runs all the tests

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
